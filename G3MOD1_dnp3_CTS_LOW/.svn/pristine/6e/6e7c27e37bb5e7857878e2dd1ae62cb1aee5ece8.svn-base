/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name            Details
 *   --------------------------------------------------------------------------
 *  12/01/15      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MCMTestIOManager.h"
#include "ModuleInfoCreator.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
factoryInfoBlkStr factoryInfo;

static strutureMapStr factoryInfoElementPtrTable[] = 
{																	//sizeof(((struct A*)0)->arr);
	{(void*)&factoryInfo.factoryInfoData.InfoBlockVersionMajor,		sizeof(((NVRAMInfoStr*)NULL)->InfoBlockVersionMajor)	},
	{(void*)&factoryInfo.factoryInfoData.InfoBlockVersionMinor,		sizeof(((NVRAMInfoStr*)NULL)->InfoBlockVersionMinor)	},
	{(void*)&factoryInfo.factoryInfoData.moduleType,				sizeof(((NVRAMInfoStr*)NULL)->moduleType)				},
	{(void*)&factoryInfo.factoryInfoData.moduleFeatureMajor,		sizeof(((NVRAMInfoStr*)NULL)->moduleFeatureMajor)		},
	{(void*)&factoryInfo.factoryInfoData.moduleFeatureMinor,		sizeof(((NVRAMInfoStr*)NULL)->moduleFeatureMinor)		},
	{(void*)&factoryInfo.factoryInfoData.batchNumber,				sizeof(((NVRAMInfoStr*)NULL)->batchNumber)				},
	{(void*)&factoryInfo.factoryInfoData.supplierId,				sizeof(((NVRAMInfoStr*)NULL)->supplierId)				},
	{(void*)&factoryInfo.factoryInfoData.assemblyNo,				sizeof(((NVRAMInfoStr*)NULL)->assemblyNo)				},
	{(void*)&factoryInfo.factoryInfoData.assemblyRev,				sizeof(((NVRAMInfoStr*)NULL)->assemblyRev)				},
	{(void*)&factoryInfo.factoryInfoData.serialNumber,				sizeof(((NVRAMInfoStr*)NULL)->serialNumber)			    },
	{(void*)&factoryInfo.factoryInfoData.subAssemblyNo,				sizeof(((NVRAMInfoStr*)NULL)->subAssemblyNo)			},
	{(void*)&factoryInfo.factoryInfoData.subAssemblyRev,			sizeof(((NVRAMInfoStr*)NULL)->subAssemblyRev)			},
	{(void*)&factoryInfo.factoryInfoData.subAssemblySerialNumber,	sizeof(((NVRAMInfoStr*)NULL)->subAssemblySerialNumber)  },
	{(void*)&factoryInfo.factoryInfoData.subAssemblyBatchNumber,	sizeof(((NVRAMInfoStr*)NULL)->subAssemblyBatchNumber)	},
	{(void*)&factoryInfo.factoryInfoData.buildDay,					sizeof(((NVRAMInfoStr*)NULL)->buildDay)				    },
	{(void*)&factoryInfo.factoryInfoData.buildMonth,				sizeof(((NVRAMInfoStr*)NULL)->buildMonth)				},
	{(void*)&factoryInfo.factoryInfoData.buildYear,					sizeof(((NVRAMInfoStr*)NULL)->buildYear)				}
};
 
lu_int8_t MCMFactoryInfoString[128];

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR SendFactoryInfoToMCM( lu_uint8_t *XMLfilename,
							   lu_int8_t *MCMFactoryInfoStringFile
							 )
{
	SB_ERROR retError;
	lu_int8_t *factoryInfoBufferPtr;
	lu_uint8_t *factoryInfoHexArray,i;
	NVRAMInfoStr MCMFactoryInfo;
	FILE *filePtr;

	factoryInfoBufferPtr = (lu_int8_t*)&MCMFactoryInfo;

	if(strncmp((lu_int8_t*)XMLfilename,"erase.xml",9) != 0)
	{		
		retError = FetchAndPackFactoryInfoFromXML( XMLfilename,
												   FACTORYINFO_FIELD_MAX,
												   factoryInfoMap,
												   factoryInfoElementPtrTable
												 );

		for(i = 0; i < FACTORYINFO_FIELD_MAX; i++)
		{
			memcpy(factoryInfoBufferPtr , (lu_uint8_t*)factoryInfoElementPtrTable[i].elementPtr, factoryInfoElementPtrTable[i].size);
			factoryInfoBufferPtr += factoryInfoElementPtrTable[i].size;
		}
	}

	factoryInfoHexArray = (lu_uint8_t*)&MCMFactoryInfo;

	//fprintf(filePtr, "%d ", sizeof(NVRAMInfoStr));

	filePtr = fopen(MCMFactoryInfoStringFile, "w");

	for(i = 0; i < sizeof(NVRAMInfoStr); i++)
	{
		fprintf(filePtr, "%02X", factoryInfoHexArray[i]);
	}

	fclose(filePtr);

	return retError;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */