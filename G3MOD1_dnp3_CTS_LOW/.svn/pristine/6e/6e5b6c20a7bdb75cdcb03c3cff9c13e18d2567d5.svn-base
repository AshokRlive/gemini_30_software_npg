%{
/****************************************************************************************
* Gemini V3.0 Switch Controller State Machine
****************************************************************************************/
%}

%class DualSwitch
%header DualSwitchFSM.h

%start DualSwitchMap::InitState
%map DualSwitchMap
%%

//--------------------------------------------------------------------------------------------------------------------------
InitState
{
//  Event                Guard condition                        State to transition to.      Action
//---------------------------------------------------------------------------------------------- 

// Disbale CheckRelayError() since it is unreliable and gives supurious errors! see #2660
//	TimerTickEvent		[ CheckRelayError(fsm) ]				OperationErrorState				{}
	
	StartEvent													IdleState						{InitAction();}
	
	ConfigEvent														nil							{}
	
	Default			                                                nil      					{}
}

//--------------------------------------------------------------------------------------------------------------------------
IdleState
{
//  Transition or												Next State or
//  Event                Guard condition                        State to transition to.      	Action(s)
//------------------------------------------------------------------------------------------------------------------------ 

	SelectLocalEvent	[ SelectLocal(fsm) ]					SelectedLocalState				{LocalSelectedAction();}

	SelectLocalEvent	[ !SelectLocal(fsm) ]					OperationErrorState				{}

	SelectRemoteEvent	[ SelectRemote(fsm) ]					SelectedRemoteState				{RemoteSelectedAction();}
	
	SelectRemoteEvent	[ !SelectRemote(fsm) ]					OperationErrorState				{}
	
	CancelLocalEvent											     nil      					{ResetAction(); }
	
	CancelRemoteEvent											     nil      					{ResetAction(); }

// Disbale CheckRelayError() since it is unreliable and gives supurious errors! see #2660
//	TimerTickEvent		[ CheckRelayError(fsm) ]				OperationErrorState				{}
						
	OperateEvent												OperationErrorState				{OperateErrorAction();}

	ConfigEvent													OperationErrorState				{ConfigErrorAction(); }
	
	Default			                                                nil				            {}
}

//--------------------------------------------------------------------------------------------------------------------------
SelectedLocalState
{
//  Event                Guard condition                        State to transition to.      Action
//---------------------------------------------------------------------------------------------- 

	OperateEvent		[ OperateLocal(fsm) ]					DelayedOperationState			{DelayedOperationAction();}
	
	TimerTickEvent		[ SelectTimeout(fsm)	]				OperationErrorState				{SelectTimedOutAction();}
	
	TimerTickEvent		[ ErrorCode(fsm) ]						OperationErrorState				{}
	
	OperateEvent		[ OperateRemote(fsm) ]					OperationErrorState				{OperateErrorAction(); }
	
	SelectLocalEvent											OperationErrorState				{SelectErrorAction(); }

	SelectRemoteEvent											OperationErrorState				{SelectErrorAction(); }

	CancelLocalEvent											OperationErrorState				{CancelErrorAction(); }
	
	CancelRemoteEvent											OperationErrorState				{CancelErrorAction(); }
	
	ConfigEvent													OperationErrorState				{ConfigErrorAction(); }

	TimerTickEvent      [ OperationComplete(fsm) & IsCheckPos(fsm) ]				OperationErrorState				{CompleteErrorAction();}
	
	Default					                                        nil			                {}
}

//--------------------------------------------------------------------------------------------------------------------------
SelectedRemoteState
{
//  Event                Guard condition                        State to transition to.      Action
//---------------------------------------------------------------------------------------------- 

	OperateEvent		[ OperateRemote(fsm) ]					DelayedOperationState			{DelayedOperationAction();}
	
	TimerTickEvent		[ SelectTimeout(fsm) ]					OperationErrorState				{SelectTimedOutAction();}
	
	TimerTickEvent		[ ErrorCode(fsm) ]						OperationErrorState				{}
	
	OperateEvent		[ OperateLocal(fsm) ]					OperationErrorState				{OperateErrorAction(); }
	
	SelectLocalEvent											OperationErrorState				{SelectErrorAction(); }
						
	SelectRemoteEvent											OperationErrorState				{SelectErrorAction(); }
						
	CancelLocalEvent											OperationErrorState				{CancelErrorAction(); }
	
	CancelRemoteEvent											OperationErrorState				{CancelErrorAction(); }

	ConfigEvent													OperationErrorState				{ConfigErrorAction(); }

	TimerTickEvent      [ OperationComplete(fsm) & IsCheckPos(fsm) ]				OperationErrorState				{CompleteErrorAction();}
	
	Default					                                        nil			                {}
}

//--------------------------------------------------------------------------------------------------------------------------
DelayedOperationState
Entry																							{ OperationCommittedAction();}
{
//  Event                Guard condition                        State to transition to.      Action
//---------------------------------------------------------------------------------------------- 

	TimerTickEvent		[ DelayTimeout(fsm) ]					PreOperatingState				{PowerMotorAction();}

	CancelLocalEvent											    nil			   				{CancelErrorAction(); }
	
	CancelRemoteEvent											    nil			   				{CancelErrorAction(); }
		
	OperateEvent												    nil			   				{OperateErrorAction(); }
	
	SelectLocalEvent											    nil			   				{SelectErrorAction(); }

	SelectRemoteEvent											    nil			   				{SelectErrorAction(); }

	ConfigEvent													    nil			   				{ConfigErrorAction(); }

	TimerTickEvent      [ OperationComplete(fsm) & IsCheckPos(fsm) ]				OperationErrorState				{CompleteErrorAction();}
	
	Default					                                        nil			                {}
}

//--------------------------------------------------------------------------------------------------------------------------
PreOperatingState
{
//  Event                Guard condition                        State to transition to.      Action
//---------------------------------------------------------------------------------------------- 

	TimerTickEvent		[ PreOpDelayTimeout(fsm) ]				OperatingState					{StartMotorAction();}

	CancelLocalEvent											    nil			   				{CancelErrorAction(); }
	
	CancelRemoteEvent											    nil			   				{CancelErrorAction(); }
		
	OperateEvent												    nil			   				{OperateErrorAction(); }
	
	SelectLocalEvent											    nil			   				{SelectErrorAction(); }

	SelectRemoteEvent											    nil			   				{SelectErrorAction(); }

	ConfigEvent													    nil			   				{ConfigErrorAction(); }

	TimerTickEvent      [ OperationComplete(fsm) & IsCheckPos(fsm) ]				OperationErrorState				{CompleteErrorAction();}
	
	Default					                                        nil			                {}
}

//--------------------------------------------------------------------------------------------------------------------------
OperatingState																			
{
//  Event                Guard condition                        State to transition to.      Action
//--------------------------------------------------------------------------------------------------------------------------

	TimerTickEvent      [ OperationComplete(fsm) ]				OperationOverrunState			{OperationOverrunAction();}
	
	TimerTickEvent		[ OperationTimeout(fsm)  ]              OperationErrorState				{OperationTimeoutAction();}
	
	TimerTickEvent													nil							{OperatingFeedbackAction();}

	CancelLocalEvent											    nil							{CancelledAction();}
	
	CancelRemoteEvent											    nil							{CancelledAction();}

	OperateEvent												    nil			   				{OperateErrorAction(); }
	
	SelectLocalEvent											    nil			   				{SelectErrorAction(); }

	SelectRemoteEvent											    nil			   				{SelectErrorAction(); }

	ConfigEvent														nil							{ConfigErrorAction(); }

	Default					                                        nil			                {}
}

//--------------------------------------------------------------------------------------------------------------------------
OperationOverrunState
//Entry																							{RelayErrorAction();}
{
//  Event                Guard condition                        State to transition to.      Action
//---------------------------------------------------------------------------------------------- 

	TimerTickEvent		[OverrunTimeout(fsm) & !ErrorCode(fsm)]  OperationCompleteState			{OperationCompleteAction();}
	
	TimerTickEvent		[OverrunTimeout(fsm) & ErrorCode(fsm)]	 OperationErrorState			{OperationCompleteAction();}	
	
	CancelLocalEvent	[ CancelLocal(fsm) ]					    nil			   				{CancelledAction();}
	
	CancelRemoteEvent	[ CancelRemote(fsm) ]					    nil			   				{CancelledAction();}
	
	OperateEvent												    nil			   				{OperateErrorAction(); }
	
	SelectLocalEvent											    nil			   				{SelectErrorAction(); }

	SelectRemoteEvent											    nil			   				{SelectErrorAction(); }

	ConfigEvent													    nil			   				{ConfigErrorAction(); }

	Default					                                        nil			                {}

}

//--------------------------------------------------------------------------------------------------------------------------
OperationCompleteState
Entry																							{ OperationUnCommittedAction();}
{
//  Event                Guard condition                        State to transition to.      Action
//---------------------------------------------------------------------------------------------- 

	CancelLocalEvent	[ CancelLocal(fsm) ]					IdleState						{ResetAction();}
	
	CancelRemoteEvent	[ CancelRemote(fsm) ]					IdleState						{ResetAction();}
	
	OperateEvent												OperationErrorState				{OperateErrorAction(); }
	
	SelectLocalEvent											OperationErrorState				{SelectErrorAction(); }

	SelectRemoteEvent											OperationErrorState				{SelectErrorAction(); }

	ConfigEvent													OperationErrorState				{ConfigErrorAction(); }

// Disbale CheckRelayError() since it is unreliable and gives supurious errors! see #2660	
//	TimerTickEvent		[ CheckRelayError(fsm) ]				OperationErrorState				{RelayErrorAction(); }

	Default					                                        nil			                {}
}


//--------------------------------------------------------------------------------------------------------------------------
OperationErrorState
Entry																							{ OperationUnCommittedAction();}
{
//  Event                Guard condition                        State to transition to.      Action
//---------------------------------------------------------------------------------------------- 

	CancelLocalEvent	[ CancelLocal(fsm) & !ErrorCode(fsm)]	IdleState						{ResetAction();}
	
	CancelRemoteEvent	[ CancelRemote(fsm)& !ErrorCode(fsm)]	IdleState						{ResetAction();}
	
	CancelLocalEvent	[ CancelLocal(fsm) & ErrorCode(fsm)]	    nil							{ErrorAction();}
	
	CancelRemoteEvent	[ CancelRemote(fsm)& ErrorCode(fsm)]	    nil							{ErrorAction();}
	
	
	OperateEvent												    nil			                {OperateErrorAction(); }
	
	SelectLocalEvent											    nil			                {SelectErrorAction(); }

	SelectRemoteEvent											    nil			                {SelectErrorAction(); }

	ConfigEvent													    nil			   				{ConfigErrorAction(); }

	Default					                                        nil			                {}
}

//--------------------------------------------------------------------------------------------------------------------------
Default
{
	Default		                                                   nil	                       {}
}
%%
