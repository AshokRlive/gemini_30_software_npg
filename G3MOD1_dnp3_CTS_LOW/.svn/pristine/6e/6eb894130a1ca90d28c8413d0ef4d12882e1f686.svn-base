/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.filetransfer;

import java.nio.ByteBuffer;

import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.RequestPayload;

class PL_Send_WriteFileFrag extends RequestPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_C_FileWriteFrag;
  private final byte[] fragment;
  private final int size;
  private final int offset;


  PL_Send_WriteFileFrag(byte[] fragment, int size, int offset) {
    super(PayloadType.MINIMUM_SIZE, PAYLOAD_SIZE);
    if (fragment == null)
      throw new NullPointerException("fragment must not be null");

    this.size = size;
    this.offset = offset;
    this.fragment = fragment;
  }

  @Override
  public byte[] toBytes() {
    // Calculate payload size
    int actualPayloadSize = PAYLOAD_SIZE + fragment.length;

    ByteBuffer buf = createByteBuffer(actualPayloadSize);
    buf.putInt(offset); // Fragment Offset
    buf.putInt(size); // Fragment Size
    buf.put(fragment, 0, size);

    return buf.array();
  }
}
