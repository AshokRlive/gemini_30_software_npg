/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typemanagement;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;
import org.fordiac.ide.model.Palette.PaletteEntry;
import org.fordiac.ide.model.libraryElement.FBType;

/**
 * An EditorInput for opening FBType files with specified content. The equals
 * method is adapted that EditorInput is equal to another EditorInput if the
 * content is equal.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class FBTypeEditorInput implements IEditorInput {

	private final FBType fbType;

	private final PaletteEntry entry;

	/**
	 * Constructor of UntypedEditorInput.
	 * 
	 * @param fbType the content
	 * @param name the name
	 * @param toolTip the tool tip
	 * @param path the path
	 * @param entry the group
	 */
	public FBTypeEditorInput(final FBType fbType, final PaletteEntry entry) {
		this.fbType = fbType;
		this.entry = entry;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IEditorInput#exists()
	 */
	public boolean exists() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IEditorInput#getImageDescriptor()
	 */
	public ImageDescriptor getImageDescriptor() {
		return ImageDescriptor.getMissingImageDescriptor();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IEditorInput#getName()
	 */
	public String getName() {
		return fbType.getName() == null ? "" : fbType.getName(); //$NON-NLS-1$
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IEditorInput#getPersistable()
	 */
	public IPersistableElement getPersistable() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IEditorInput#getToolTipText()
	 */
	public String getToolTipText() {
		return fbType.getComment() == null ? "" : fbType.getComment() + " (" + entry.getFile().getProjectRelativePath().toString() + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.IAdaptable#getAdapter(java.lang.Class)
	 */
	public Object getAdapter(@SuppressWarnings("rawtypes") final Class adapter) {
		return null;
	}

	/**
	 * Gets the content.
	 * 
	 * @return the content
	 */
	public FBType getContent() {
		return fbType;
	}

	

	/**
	 * Gets the group.
	 * 
	 * @return the group
	 */
	public PaletteEntry getPaletteEntry() {
		return entry;
	}

	/**
	 * Equals.
	 * 
	 * @param obj the obj
	 * 
	 * @return <code>true</code> if <code>content</code> is equal.
	 */
	@Override
	public boolean equals(final Object obj) {
		if (obj instanceof FBTypeEditorInput) {
			FBTypeEditorInput input = (FBTypeEditorInput) obj;
			return fbType.equals(input.getContent())
					&& entry.getFile().equals(input.getPaletteEntry().getFile());
		} else {
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return fbType.hashCode();
	}

}
