/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:IEC61499Logic.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14 Jul 2017     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MCMAPP_SRC_AUTOMATION_IEC61499_IEC61499LOGIC_H_
#define MCMAPP_SRC_AUTOMATION_IEC61499_IEC61499LOGIC_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "AutomationLogic.h"
#include "G3DBServer.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
class IEC61499Logic: public AutomationLogic, private g3db::IRequestHandler
{
public:
    IEC61499Logic(Mutex* mutex,
                       GeminiDatabase& database,
                       AutomationLogic::Config& config
                       );
   virtual ~IEC61499Logic();

protected:
//   /* Implementation of AutomationLogic*/
   virtual void handleInputChange(lu_uint32_t index, POINT_TYPE type, PointData *newPointData);


private:
   /* Implementation of IRequestHandler*/
   virtual lu_bool_t handle(g3db::DBQuery& query);

   /** Gets the ID of IEC61499 database. */
   const lu_char_t* getDBId()
   {
       return getResName();
   }
};

#endif /* MCMAPP_SRC_AUTOMATION_IEC61499_IEC61499LOGIC_H_ */

/*
 *********************** End of file ******************************************
 */
