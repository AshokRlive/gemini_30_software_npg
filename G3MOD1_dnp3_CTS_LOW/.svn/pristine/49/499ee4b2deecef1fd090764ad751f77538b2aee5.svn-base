/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.fields.domain;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocolModule;
import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocolSession;
import com.lucy.g3.rtu.config.protocol.master.shared.factory.IMasterProtocolDeviceFactory;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBSession;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * A factory for creating FieldDevice objects.
 */
public final class FieldDeviceFactory implements IMasterProtocolDeviceFactory {

  public static final FieldDeviceFactory INSTANCE = new FieldDeviceFactory();


  private FieldDeviceFactory() {
  }

  @Override
  public IMasterProtocolModule createMasterDevice(MODULE_ID id, IMasterProtocolSession session) {
    Preconditions.checkNotNull(session, "session is null");

    if (session instanceof MMBSession) {
      return new MMBDevice(id, (MMBSession) session);
    }

    return null;
  }
}
