/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MCM channel interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   04/08/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_F074184F_6E00_4cb2_AE5B_DFDECC5B499F__INCLUDED_)
#define EA_F074184F_6E00_4cb2_AE5B_DFDECC5B499F__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "IChannel.h"
#include "Logger.h"
#include "MCMIOMap.h"
#include "InputDigitalCANChannel.h"
#include "InputAnalogueCANChannel.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class MCMChannel : public IChannel
{

public:
    MCMChannel(CHANNEL_TYPE type, lu_uint16_t id);

    /**
     * \brief Send the stored configuration to the channel
     *
     * Custom implementation that applies a given Channel configuration
     *
     * \param conf Digital Input channel configuration
     *
     * \return Error code
     */
    virtual IOM_ERROR configure(InputDigitalCANChannelConf &conf);

    /**
     * \brief Send the stored configuration to the channel
     *
     * Custom implementation that applies a given Channel configuration
     *
     * \param conf Digital Input channel configuration
     *
     * \return Error code
     */
    virtual IOM_ERROR configure(InputAnalogueCANChannelConf &conf);

    /**
     * \brief Update the value of the channel
     *
     * This function should be periodically called in order to update the
     * status/value of the channel, or forced to update otherwise (when
     * supported by the channel).
     *
     * \param timestamp Current time
     */
    virtual void update(TimeManager::TimeStr &timestamp, const lu_bool_t forced = LU_FALSE);

    /**
     * \brief Read the channel value
     *
     * Default implementation: operation unsupported
     *
     * \param dataPtr where the data is saved
     *
     * \return Error code
     */
    virtual IOM_ERROR read(ValueStr& valuePtr);

    /**
     * \brief Write to the channel
     *
     * Default implementation: operation unsupported
     *
     * \param dataPtr Data to write
     *
     * \return Error code
     */
    virtual IOM_ERROR write(ValueStr& valuePtr);

protected:
    Logger& log;
    TimeManager::TimeStr timeStamp;     //Time stamp of the value reading
    InputDigitalCANChannelConf channelConf;     //channel configuration
    InputAnalogueCANChannelConf channelAnalogConf;  //analogue channel configuration
};
#endif // !defined(EA_F074184F_6E00_4cb2_AE5B_DFDECC5B499F__INCLUDED_)

/*
 *********************** End of file ******************************************
 */

