/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.ui.panels;

import java.awt.BorderLayout;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.jdesktop.swingx.JXTaskPane;
import org.jdesktop.swingx.JXTaskPaneContainer;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfig;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleFPMSettings;
import com.lucy.g3.rtu.config.module.shared.domain.IModuleSettingsEditor;


/**
 * The Class FPMSettingsPanel.
 */
public class FPMSettingsPanel extends JPanel implements IModuleSettingsEditor{
  private ModuleFPMSettings settings;
  private JScrollPane scrollPane;
  
  public FPMSettingsPanel(ModuleFPMSettings settings) {
    this.settings = Preconditions.checkNotNull(settings, "settings must not be null");
    
    initComponents();
  }
  
  private void initComponents() {
    JXTaskPaneContainer container = new JXTaskPaneContainer();
    JXTaskPane pane;
    String title;
    
    FPIConfig[] fpis = settings.getAllFpi();
    for (int i = 0; i < fpis.length; i++) {
      title = String.format("FPI-%c Settings", 'A' + i);
      pane = new JXTaskPane(title);
      FPIConfigPanel panel = new FPIConfigPanel(fpis[i]);
      UIUtils.setOpaqueForAll(panel, JPanel.class, false);
      pane.add(panel);
      pane.setTitle(title);
      container.add(pane);
    }
    
    setLayout(new BorderLayout());
    this.add(container, BorderLayout.CENTER);
    
  }
  
  @Override
  public JComponent getEditorComopnent() {
    if(scrollPane == null) {
      scrollPane = new JScrollPane(this);
      scrollPane.setBorder(null);
      UIUtils.increaseScrollSpeed(scrollPane);
    }
    
    return scrollPane;
  }

  @Override
  public String getEditorTitle() {
    return "FPM Settings";
  }

}

