/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.cert.manager.gui;

import static com.lucy.g3.cert.manager.gui.KeyStoreActions.*;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.security.KeyStoreException;
import java.text.MessageFormat;
import java.util.ResourceBundle;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.log4j.Logger;

import com.lucy.g3.cert.manager.KeyStoreManager;
import com.lucy.g3.cert.manager.gui.KeyStoreActions.ActionCallback;


public class KeyStoreManagerFrame extends JFrame {

  private Logger log = Logger.getLogger(KeyStoreManagerFrame.class);
  private ResourceBundle RB = Resources.RB;
  
  private final KeyStoreManager manager;
  private final KeyStoreActions actions;
  
  public KeyStoreManagerFrame(KeyStoreManager ksManager) {
    this.manager = ksManager;
    this.actions = new KeyStoreActions(ksManager, new ActionCallback() {
      @Override
      public String getSelectedType() {
        return ((KeyStoreTable)tableKeystore).getSelectedType();
      }
      
      @Override
      public String getSelectedAlias() {
        return ((KeyStoreTable)tableKeystore).getSelectedAlias();
      }

      @Override
      public Window getParent() {
        return KeyStoreManagerFrame.this;
      }
    });
    
    
    initComponents();
    loadKeyStore();
     
    updateTitle();
    ksManager.addPropertyChangeListener(new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        updateTitle();
        loadKeyStore();
      }
    });
    
    setIconImage(Resources.getIcon("CertApp.Icon.image").getImage());
  }

  private void loadKeyStore() {
    try {
      // Keep original selected index
      int selectedRow  = -1;
      if(tableKeystore != null)
        selectedRow  = tableKeystore.getSelectedRow();
      
      ((KeyStoreTable)this.tableKeystore).load(manager.getKeyStore());
      
      
      // Restore selection
      if(selectedRow >= 0) {
        if(selectedRow >= tableKeystore.getRowCount()) {
          selectedRow = tableKeystore.getRowCount()-1;
        }
        tableKeystore.getSelectionModel().setSelectionInterval(selectedRow, selectedRow);
      }
    } catch (KeyStoreException e) {
      log.error(e.getMessage());
    }
  }

  private void createUIComponents() {
    tableKeystore = initTable();
    menuFile = new JMenu();
    menuFile.add(actions.getAction(ACTION_NEWKEYSTORE));
    menuFile.add(actions.getAction(ACTION_OPENKEYSTORE));
    menuFile.add(actions.getAction(ACTION_SAVEKEYSTORE));
    menuFile.add(actions.getAction(ACTION_SAVEKEYSTOREAS));
    menuFile.addSeparator();
    menuFile.add(actions.getAction(ACTION_EXIT));
    
    menuTools = new JMenu();
    menuTools.add(actions.getAction(ACTION_IMPORTTRUSTEDCERT));
    menuTools.add(actions.getAction(ACTION_KEYSTOREREPORT));
    menuTools.addSeparator();
    menuTools.add(actions.getAction(ACTION_SETKEYSTOREPASSWORD));
    
    menuExamine = new JMenu();
    menuExamine.add(actions.getAction(ACTION_EXAMINECERT));
    menuExamine.add(actions.getAction(ACTION_EXAMINECERTSSL));
  }
  
  private JTable initTable()
  {
    // The table itself
    final KeyStoreTable table = new KeyStoreTable(actions);

    // Install listener to keep track of last selected alias
    table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

      @Override
      public void valueChanged(ListSelectionEvent e) {
        if (!e.getValueIsAdjusting()) {
          String alias = table.getSelectedAlias();
          if (alias != null) {
            updateStatusBar();
          }
        }
      }
    });
    
    return table;
  }
  
  private void updateStatusBar() {
    setDefaultStatusBarText();
  }
  
  private void setStatusBarText(String text) {
    labelStatus.setText(text);
  }
  
  /**
   * Set the text in the staus bar to reflect the status of the currently loaded keystore.
   */
  private void setDefaultStatusBarText()
  {
      int iSize = manager.getEntriesSize();
      if(iSize < 0) {
        setStatusBarText("");
        return;
      }

      String sType = manager.getKeyStoreType();
      String sProv = manager.getKeyStoreProvider();
      setStatusBarText(MessageFormat.format("Keystore type: {0}, provider: {1}, size: 1 {2}", sType, sProv, 
          iSize == 1 ? "entry":"entries"));
  }
  
  /**
   * Update the application's controls dependent on the state of its keystore.
   */
  private void updateTitle()
  {// Application name
    String sAppName = RB.getString("CertApp.Title");

    // No keystore loaded so just display the application name
    if (manager == null)
    {
      setTitle(sAppName);
    }
    else
    {
      File fKeyStore = manager.getKeyStoreFile();

      if (fKeyStore == null)
      {
        // A newly created keystore is loaded - display Untitled string and application name
        setTitle(MessageFormat.format("[{0}] - {1}", RB.getString("CertApp.Untitled"), sAppName));
      }
      else
      {
        // Keystore loaded - display keystore file path, "modified" indicator, and application name
        String modInd = manager.isChanged() ? RB.getString("CertApp.Modified") : "";
        setTitle(MessageFormat.format("{0}{1} - {2}", fKeyStore, modInd, sAppName));
      }
    }
  }
  
  private void thisWindowClosing(WindowEvent e) {
    actions.exit();
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    menuBar1 = new JMenuBar();
    labelStatus = new JLabel();
    scrollPane1 = new JScrollPane();

    //======== this ========
    setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    addWindowListener(new WindowAdapter() {

      @Override
      public void windowClosing(WindowEvent e) {
        thisWindowClosing(e);
      }
    });
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    //======== menuBar1 ========
    {

      //======== menuFile ========
      {
        menuFile.setText("File");
        menuFile.setMnemonic('F');
      }
      menuBar1.add(menuFile);

      //======== menuTools ========
      {
        menuTools.setText("Tools");
        menuTools.setMnemonic('T');
      }
      menuBar1.add(menuTools);

      //======== menuExamine ========
      {
        menuExamine.setText("Examine");
        menuExamine.setMnemonic('E');
      }
      menuBar1.add(menuExamine);
    }
    setJMenuBar(menuBar1);

    //---- labelStatus ----
    labelStatus.setText(" ");
    contentPane.add(labelStatus, BorderLayout.SOUTH);

    //======== scrollPane1 ========
    {

      //---- tableKeystore ----
      tableKeystore.setMinimumSize(new Dimension(400, 300));
      scrollPane1.setViewportView(tableKeystore);
    }
    contentPane.add(scrollPane1, BorderLayout.CENTER);

    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JMenuBar menuBar1;
  private JMenu menuFile;
  private JMenu menuTools;
  private JMenu menuExamine;
  private JLabel labelStatus;
  private JScrollPane scrollPane1;
  private JTable tableKeystore;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
