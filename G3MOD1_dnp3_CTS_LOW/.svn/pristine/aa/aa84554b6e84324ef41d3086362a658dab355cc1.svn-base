/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.prefs;

import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;

import com.lucy.g3.iec61131.ide.ui.AbstractManager;
import com.lucy.g3.iec61131.ide.ui.IEC61131Context;

/**
 *
 */
public class PreferencesManager extends AbstractManager{
  public final static String ACTION_KEY_EDIT = "editPrefs";
  
  public enum Key { 
    KEY_WORKING_DIR     ,
    KEY_IL_COMPILER_PATH, 
    KEY_CROSS_COMPILER_PATH ,
    KEY_CROSS_COMPILER_PREFIX,
    KEY_POLLING_RATE    , 
    KEY_PREFS_EDITOR_DIR ,
    KEY_SOURCE_FILE_DIR ,
    KEY_SHOW_TAB_SPACE,
    KEY_CONVERT_TABS_TO_SPACES,
  }
  
  public static final String USER_HOME = System.getProperty("user.home");
  public String[] DEFAULT_VALUES = {
      ".\\target", // KEY_WORKING_DIR     , 
      USER_HOME+"\\AppData\\Local\\Lucy Electric\\Gemini 3\\IEC-61131\\IEC-61131_ILCompiler.jar",// KEY_IL_COMPILER_PATH, 
      USER_HOME+"\\AppData\\Local\\Lucy Electric\\Gemini 3\\IEC-61131\\arm-eabi\\bin\\",// KEY_C_COMPILER_PATH ,
      "arm-eabi-",      // KEY_C_COMPILER_PATH ,
      "500",            // KEY_POLLING_RATE, ms
      System.getProperty("user.dir"), // KEY_PREFS_EDITOR_DIR
      "", // KEY_SOURCE_FILE_DIR
      "true", //KEY_SHOW_TAB_SPACE
      "false", //KEY_CONVERT_TAB_TO_SPACE
  }; 
  

  private final Preferences prefs = Preferences.userRoot().node(this.getClass().getName());

  public PreferencesManager(IEC61131Context context) {
    super(context);
  }
  
  @org.jdesktop.application.Action
  public void editPrefs() {
    new PreferencesEditor(context.getParent(), this).setVisible(true);
  }
  
  public void addPreferenceChangeListener(PreferenceChangeListener pcl){
    prefs.addPreferenceChangeListener(pcl);
  }
  
  public void put(Key key, String value) {
    if(key == null)
      throw new IllegalArgumentException("key must not be null");
    
    prefs.put(key.name(), value);
  }
  
  public String get(Key key) {
    if(key == null)
      throw new IllegalArgumentException("key must not be null");
    
    return prefs.get(key.name(), DEFAULT_VALUES[key.ordinal()]);
  }
  
  public String getDefault(Key key) {
    if(key == null)
      throw new IllegalArgumentException("key must not be null");
    return DEFAULT_VALUES[key.ordinal()];
  }
  
  public String getWorkingDir() {
    String workingDir = get(Key.KEY_WORKING_DIR);
    if(workingDir == null || workingDir.trim().isEmpty())
      workingDir = ".";
    
    return workingDir;
  }
  
  public String getBuildingDir() {
    return get(Key.KEY_WORKING_DIR) +"\\target\\";
  }
  
  public String getILCompilerPath() {
    return get(Key.KEY_IL_COMPILER_PATH);
  }
  
  public String getCrossCompilerPath() {
    return get(Key.KEY_CROSS_COMPILER_PATH);
  }

  public String getCrossCompilerPrefix() {
    return get(Key.KEY_CROSS_COMPILER_PREFIX);
  }

  
  public Preferences getPrefs() {
    return prefs;
  }
  
}
