/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.dialogs;

import java.awt.Component;
import java.awt.Frame;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.prefs.Preferences;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;

import com.jgoodies.common.base.Preconditions;


/**
 * The utilities for dialog operation.
 */
public final class DialogUtils {

  private DialogUtils() {
  }

  /**
   * Show a frame window in application context.
   */
  public static void showWindow(JFrame window) {
    window.setExtendedState(Frame.NORMAL);
    //window.setLocationRelativeTo(WindowUtils.getMainFrame());
    window.setVisible(true);
  }

  /**
   * Show a dialog window in application context.
   */
  public static void showWindow(JDialog window) {
    Application app = Application.getInstance();
    if (app instanceof SingleFrameApplication) {
      ((SingleFrameApplication) app).show(window);
    } else {
      window.setVisible(true);
    }
  }

  /* Preference for saving user data. */
  private static final Preferences PREF = Preferences.userRoot();

  
  private static final String DIR_KEY = "FileChoosingDir";


  public static File showFileChooseDialog(Component parent,
      String title,
      String approveBtnText,
      FileFilter... filter) {
    return showFileChooseDialog(parent, title, approveBtnText, null, filter);
  }

  public static File showFileChooseDialog(Component parent,
      String title,
      String approveBtnText,
      String initialFileName,
      FileFilter... filter) {
    return showFileChooseDialog(parent, null, title, approveBtnText, initialFileName, filter);
  }
  
  /**
   * Shows a dialog to choose a file.
   *
   * @param parent
   *          the parent component
   * @return the chosen file. Null if no file has been chosen.
   */
  public static File showFileChooseDialog(Component parent,
      File dir,
      String title,
      String approveBtnText,
      String initialFileName,
      FileFilter... filter) {
    // Get previous directory
    if(dir == null)
      dir = new File(PREF.get(DIR_KEY, System.getProperty("user.dir")));
    if (!dir.isDirectory() || !dir.canRead()) {
      dir = null;
    }

    JFileChooser fc = new JFileChooser(dir);
    fc.setDialogTitle(title);
    fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fc.setAcceptAllFileFilterUsed(true);

    if (initialFileName != null) {
      fc.setSelectedFile(new File(initialFileName));
    }

    if (filter != null) {
      for (int i = 0; i < filter.length; i++) {
        if (filter[i] != null) {
          fc.addChoosableFileFilter(filter[i]);
          fc.setFileFilter(filter[i]);
        }
      }
    }

    FileChooserRunner runner = new FileChooserRunner(parent, fc, approveBtnText);
    if (SwingUtilities.isEventDispatchThread()) {
      runner.run();
    } else {
      try {
        SwingUtilities.invokeAndWait(runner);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return runner.chosenFile;
  }

  public static File showFileChooseDialog(Component parent,
      String title,
      FileFilter... filter) {
    return showFileChooseDialog(parent, title, "Select", filter);
  }

  public static File showFileChooseDialog(Component parent) {
    return showFileChooseDialog(parent, "Choose a file", (FileFilter) null);
  }

  /**
   * Shows a dialog to choose a file.
   *
   * @param parent
   *          the parent component
   * @return the chosen file. Null if no file has been chosen.
   */
  public static File showDirectoryChooseDialog(Component parent,
      String title,
      String approveBtnText) {
    // Get previous directory
    File dir = new File(PREF.get(DIR_KEY, System.getProperty("user.dir")));
    if (!dir.isDirectory() || !dir.canRead()) {
      dir = null;
    }

    JFileChooser fc = new JFileChooser(dir);
    fc.setDialogTitle(title);
    fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

    FileChooserRunner runner = new FileChooserRunner(parent, fc, approveBtnText);
    if (SwingUtilities.isEventDispatchThread()) {
      runner.run();
    } else {
      try {
        SwingUtilities.invokeAndWait(runner);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return runner.chosenFile;
  }
  
  


  private static class FileChooserRunner implements Runnable {

    private final Component parent;
    private final JFileChooser fc;
    private final String approveBtnText;
    private File chosenFile = null;


    public FileChooserRunner(Component parent, JFileChooser fc, String approveBtnText) {
      this.parent = parent;
      this.fc = fc;
      this.approveBtnText = approveBtnText;
    }

    @Override
    public void run() {
      // Show dialog
      int option = fc.showDialog(parent, approveBtnText);

      // Dialog approved
      if (JFileChooser.APPROVE_OPTION == option) {
        // Save current directory
        PREF.put(DIR_KEY, fc.getCurrentDirectory().toString());

        String destFile = fc.getSelectedFile().getAbsolutePath();

        /* Add file extension as necessary */
        FileFilter ff = fc.getFileFilter();
        if (ff instanceof FileNameExtensionFilter) {
          boolean hasExt = false;
          String[] exts = ((FileNameExtensionFilter) ff).getExtensions();
          for (int i = 0; i < exts.length; i++) {
            if (destFile.endsWith(exts[i])) {
              hasExt = true;
              break;
            }
          }

          if (hasExt == false) {
            destFile = destFile + "." + exts[0];
          }
        }

        chosenFile = new File(destFile);
      }

    }
  }
  
  /**
   * Shows a dialog to save string content to a file.
   * @param targetName target file path
   * @param content the string to be saved
   * @return true if saving succeeds 
   */
  public static boolean showSavingDialog(Component parent, final String targetName, final String content) {
    Preconditions.checkNotBlank(targetName, "targetName must not be blank");

    boolean succeeded = false;

    File destFile = showFileChooseDialog(parent,
        String.format("Save %s to a file", targetName),
        "Save",
        new FileNameExtensionFilter(targetName + " file(.txt)", "txt"));

    if (destFile != null) {
      String filePath = destFile.getPath();
      if (!filePath.endsWith(".txt")) {
        filePath += ".txt";
      }
      File targetFile = new File(filePath);
      FileWriter fw = null;
      try {
        fw = new FileWriter(targetFile);
        fw.write(content);

        // Show success dialog
        JOptionPane.showMessageDialog(parent,
            String.format("The %s has been saved to:\n%s", targetName, targetFile),
            "Done",
            JOptionPane.INFORMATION_MESSAGE);
        succeeded = true;

      } catch (IOException e) {
        // Show fail dialog
        JOptionPane.showMessageDialog(parent,
            String.format("Fail to save %s. \nReason: %s", targetName, e.getMessage()),
            "Error",
            JOptionPane.ERROR_MESSAGE);

      } finally {
        if (fw != null) {
          try {
            fw.close();
          } catch (IOException e) {
            // Nothing to throw
          }
        }
      }
    }

    return succeeded;
  }
  
  public static File showOpenDialog(Component parent, String title, JFileChooser chooser) {
    return showDialog(parent, title, chooser, "Open");
  }
  
  public static File showSaveDialog(Component parent, String title, JFileChooser chooser) {
    return showDialog(parent, title, chooser, "Save");
  }
  
  private static  File fLastDir;
  public static File showDialog(Component parent, String title, JFileChooser chooser, String appoveBtnText) {
    if(fLastDir == null)
      fLastDir = new File(System.getProperty("user.dir"));
    
    chooser.setCurrentDirectory(fLastDir);

    chooser.setDialogTitle(title);
    chooser.setMultiSelectionEnabled(false);
    chooser.setAcceptAllFileFilterUsed(true);

    int iRtnValue = chooser.showDialog(parent, appoveBtnText);
    if (iRtnValue == JFileChooser.APPROVE_OPTION)
    {
      File selectedFile = chooser.getSelectedFile();
      fLastDir = selectedFile.getParentFile();
      return selectedFile;
    }
    return null;
  }
}

