/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Virtual Point Factory interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   12/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#if !defined(EA_74F08449_7FF4_4080_8E07_87B6B86EF0CD__INCLUDED_)
#define EA_74F08449_7FF4_4080_8E07_87B6B86EF0CD__INCLUDED_


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "IPoint.h"


/* Forward Declaration */
class Mutex;
class GeminiDatabase;

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class IPointFactory
{

public:
    IPointFactory() {};

    virtual ~IPointFactory() {};

    /**
     * \brief Configures the common mutex lock
     *
     * \param mutex Reference to the common mutex
     */
    virtual void setMutexLock(Mutex& mutex) = 0;

    /**
     * \brief Return the total number of points the factory can create
     *
     * \return Points number
     */
    virtual lu_uint16_t getPointNumber() = 0;

	/**
	 * \brief Create a new virtual point
	 *
	 * \param pointID Virtual point point ID
	 *
	 * \return Pointer to the new virtual point. NULL in case of error.
	 */
    virtual IPoint *newPoint(PointIdStr pointID) = 0;

};
#endif // !defined(EA_74F08449_7FF4_4080_8E07_87B6B86EF0CD__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
