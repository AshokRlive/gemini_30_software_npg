<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_jr2_3xy_1q">
  <title>Edit a double binary input virtual point</title>
  <refbody>
    <section><menucascade>
        <uicontrol>Configuration</uicontrol>
        <uicontrol>Virtual Points</uicontrol>
        <uicontrol>Double Binary Input</uicontrol>
        <uicontrol>Edit</uicontrol>
      </menucascade></section>
    <section>
      <p>A <uicontrol>Double Binary Input</uicontrol> virtual point composes in a 2-bit value the
        meaning of the source data.</p>
      <p>It may have the source of its raw value in two <i>digital input channels</i> (physical
        connection) or a <i>control logic indication point</i> (logical value).</p>
      <note type="tip">You can create several virtual points from the same channels.</note>
      <simpletable id="simpletable-msv_wzz_yp">
        <strow>
          <stentry><uicontrol>Source [0]</uicontrol></stentry>
          <stentry>The channel or control logic point that acts as a raw value source for the
              <i>least significant bit</i>. <p><uicontrol>Select</uicontrol> - For a channel-based
              virtual point, click the list, and then select the channel to be used.</p></stentry>
        </strow>
        <strow>
          <stentry><uicontrol>Source [1]</uicontrol></stentry>
          <stentry>The channel or control logic point that acts as a raw value source for the
              <i>most significant bit</i>.<p><uicontrol>Select</uicontrol> - For a channel-based
              virtual point, click the list, and then select the channel to be used.</p></stentry>
        </strow>
        <strow/>
        <strow>
          <stentry><uicontrol>Description</uicontrol></stentry>
          <stentry>The <uicontrol>Description</uicontrol> is customised by default. When not
            customised, it uses its default value, usually the name of <uicontrol>Source
              [0]</uicontrol>.<p>To change the description, select the
                <uicontrol>Customise</uicontrol> check box, and then edit the text in the
            box.</p></stentry>
        </strow>
      </simpletable>
    </section>
    <section>
      <title id="title-gfw_n3f_dq">Parameters</title>
      <p>Chatter is defined as multiple unwanted digital inputs caused by a faulty sensor or input
        relay, for example. These unwanted signals can be disabled to avoid nuisance alarms at the
        Control Centre. </p>
      <table id="table-rhw_n3f_dq">
        <tgroup cols="2">
          <colspec colnum="1" colname="col1" colwidth="*"/>
          <colspec colnum="2" colname="col2" colwidth="*"/>
          <thead>
            <row>
              <entry>Option</entry>
              <entry>Description</entry>
            </row>
          </thead>
          <tbody>
            <row>
              <entry><uicontrol>Chatter No</uicontrol></entry>
              <entry>The number of value changes that are allowed inside a specified amount of time.
                Any number of changes over this amount will disable the point and report its
                  state.<p>Set <uicontrol>Chatter No</uicontrol> to 0 in order to disable chatter
                  detection.</p></entry>
            </row>
            <row>
              <entry><uicontrol>Chatter Time</uicontrol></entry>
              <entry>When <uicontrol>Chatter No</uicontrol> is set to a valid value, this field
                specifies the amount of time (in seconds) used to detect the changes.</entry>
            </row>
            <row>
              <entry><uicontrol>Debounce</uicontrol></entry>
              <entry>The number of milliseconds after an input change in order to consider the 2-bit
                value as settled down.<p>Since digital inputs don't change at exactly the same time,
                  this allows waiting for both inputs to be updated before reporting the resulting
                  2-bit value.</p></entry>
            </row>
          </tbody>
        </tgroup>
      </table>
    </section>
    <section>
      <title>Double Binary Values</title>
      <table id="table-oqq_rhq_sq">
        <tgroup cols="2">
          <colspec colnum="1" colname="col1" colwidth="*"/>
          <colspec colnum="2" colname="col2" colwidth="*"/>
          <thead>
            <row>
              <entry>Option</entry>
              <entry>Description</entry>
            </row>
          </thead>
          <tbody>
            <row>
              <entry><uicontrol>Customise</uicontrol></entry>
              <entry>Select the check box to customise the values required for the resulting 2-bit
                value. This way, a combination of input values can yield specific double-binary
                values.</entry>
            </row>
            <row>
              <entry><uicontrol>Source [0]</uicontrol></entry>
              <entry>The <i>least</i> significant bit for the input.</entry>
            </row>
            <row>
              <entry><uicontrol>Source [1]</uicontrol></entry>
              <entry>The <i>most</i> significant bit for the input.</entry>
            </row>
            <row>
              <entry><uicontrol>Custom Value</uicontrol></entry>
              <entry>The value to be used for the combination of the sources instead of the direct
                value extracted from the input bits.</entry>
            </row>
          </tbody>
        </tgroup>
      </table>
    </section>
  </refbody>
</reference>
