#ifndef _G3DBPROCINT_H_
#define _G3DBPROCINT_H_


/******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "../../../common/G3DBSocket/client/G3DBClient.h"
#include <resource.h>
#include <forte_string.h>
#include <forte_bool.h>
#include "extevhan.h"
#include <esfb.h> // CEventSourceFB
#include "device.h"
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
typedef enum
{
    TYPE_DIGITAL     = g3db::VALUE_TYPE_DIGITAL,
    TYPE_ANALOGUE    = g3db::VALUE_TYPE_ANALOGUE
}TYPE;

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class CG3DBProcessInterface : public CEventSourceFB, CExternalEventHandler, private g3db::IEventHandler{

  public:
    CG3DBProcessInterface(CResource *paSrcRes, const SFBInterfaceSpec *paInterfaceSpec,
         const CStringDictionary::TStringId paInstanceNameId, TForteByte *paFBConnData, TForteByte *paFBVarsData) :
           CEventSourceFB(paSrcRes, paInterfaceSpec, paInstanceNameId, paFBConnData, paFBVarsData),
		   CExternalEventHandler(paSrcRes->getDevice().getDeviceExecution())
		   {
        m_subscribed = false;
   setEventChainExecutor(paSrcRes->getResourceEventExecution());
     }

    virtual ~CG3DBProcessInterface(){deinitialise();}

    /*Override*/
    virtual lu_bool_t handle(g3db::DBEvent& event);

    /*=== CExternalEventHandler Implementation ===*/
    void enableHandler(void){};
    void disableHandler(void){};
    void setPriority(int){};
    int getPriority(void) const {return 0;}
    size_t getIdentifier() const {return 0;}
  protected:
    bool initialise(bool subscribeEvent = false);
    bool deinitialise();
    bool readPoint(const lu_uint16_t group, const lu_uint16_t id,
                    CIEC_WSTRING& status, CIEC_ANY_NUM& value,CIEC_BOOL& quality, CIEC_WSTRING& type);
    bool writePoint();

    bool getInput(  const TYPE type,
                    const lu_uint16_t id,
                    CIEC_WSTRING& status,
                    CIEC_ANY_NUM& value,
                    CIEC_BOOL& quality);

    bool setOutput( const TYPE type,
                    const lu_uint16_t id,
                    const CIEC_ANY_NUM& value,
                    const lu_bool_t online,
                    CIEC_WSTRING& status);

    bool getParameter(const lu_uint16_t id,
                        CIEC_WSTRING& status,
                        CIEC_DINT& value);

    bool operateControl(const lu_uint16_t id,
                    CIEC_BOOL& operate,
                    CIEC_WSTRING& status);

    /*Handles the change of database input.*/
    lu_bool_t handleInputEvent(g3db::DBEvent& event,
                    const TYPE type, const lu_uint16_t id,
                    CIEC_WSTRING& status, CIEC_ANY_NUM& value,
                    CIEC_BOOL& quality,
                    CIEC_BOOL& qualifier,
                    const TEventID indication);
  private:
    void subscribe();
    void unsubscribe();

    bool m_subscribed;

    static g3db::G3DBClient g3dbclient;

    /** String Constants */
    static const char * const scmOK;
    static const char * const scmNotInitialised;
    static const char * const scmCommsError;
    static const char * const scmCouldNotRead;
    static const char * const scmCouldNotWrite;
    static const char * const typeDigital;
    static const char * const typeAnalogue;

//    /** Handle input event published by G3DBServer*/
//    class InputEventHandler: public g3db::IEventHandler
//    {
//    public:
//        InputEventHandler(
//                        const TYPE type,
//                        const lu_uint16_t id,
//                        CIEC_WSTRING& status,
//                        CIEC_ANY_NUM& value,
//                        CIEC_BOOL& quality,
//                        const TEventID& outputEvent);
//        virtual lu_bool_t handle(g3db::DBEvent& event);
//    private:
//        const TYPE m_type;
//        const lu_uint16_t m_id;
//        CIEC_WSTRING& m_status;
//        CIEC_ANY_NUM& m_value;
//        CIEC_BOOL& m_quality;
//        const TEventID& m_outputEvent;
//    };
};


#endif /* _G3DBPROCINT_H_ */
