/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.lucy.g3.itemlist.IItemListManager.AddItemException;
import com.lucy.g3.rtu.config.clogic.switchgear.SwitchGearLogic;
import com.lucy.g3.rtu.config.shared.manager.ConfigFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.virtualpoint.shared.VirtualPointPlugin;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IPseudoPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointManager;

public class CLogicManagerImplTest {

  private CLogicManager manager;
  private ICLogic logicA, logicB;
  private IConfig config;

  
  @Before
  public void setUp() throws Exception {
    CLogicPlugin.init();
    VirtualPointPlugin.init();
    config = ConfigFactory.getInstance().create();
    
    manager = config.getConfigModule(CLogicManager.CONFIG_MODULE_ID);
    logicA = new SwitchGearLogic();
    logicB = new SwitchGearLogic();
  }

  @After
  public void tearDown() throws Exception {
    VirtualPointPlugin.shutdown();
    manager = null;
    CLogicPlugin.destroy();
  }

  @Test
  public void testAddLogic() throws Exception {
    int oldSize = manager.getSize();
    manager.add(logicA);
    manager.add(logicB);

    assertEquals(oldSize + 2, manager.getSize());
  }

  @Test
  public void testGetLogic() throws Exception {
    manager.add(logicA);

//    assertTrue(manager.getLogicByType(logicA.getType()).contains(logicA));
    assertEquals(logicA, manager.getLogicByGroup(logicA.getGroup()));
  }

  @Test
  public void testLogicGroupUpdated() {
    int count = manager.getSize();
    try {
      manager.add(logicA);
      manager.add(logicB);
      assertEquals(count + 1, logicA.getGroup());
      assertEquals(count + 2, logicB.getGroup());
    } catch (Exception e) {
      e.printStackTrace();
      fail("Exception not expected");
    }
  }

  @Test(expected = AddItemException.class)
  public void testAddSameLogic() {
    manager.add(logicA);
    manager.add(logicA);
  }


  @Test
  public void testAddLogicPoints() throws AddItemException {
    VirtualPointManager pointsMgr =  config .getConfigModule(VirtualPointManager.CONFIG_MODULE_ID);
    
    manager.add(logicA);
    manager.add(logicB);

    IPseudoPoint[] logicPoints = logicA.getAllPoints();
    assertTrue(pointsMgr.getAllPoints().containsAll(Arrays.asList(logicPoints)));

    logicPoints = logicB.getAllPoints();
    assertTrue(pointsMgr.getAllPoints().containsAll(Arrays.asList(logicPoints)));
  }

  @Test(expected = AddItemException.class)
  public void testAddNullElement() {
    ArrayList<ICLogic> clogic = new ArrayList<ICLogic>();
    clogic.add(null);
    clogic.add(null);
    clogic.add(null);

    manager.addAll(clogic);

  }
}
