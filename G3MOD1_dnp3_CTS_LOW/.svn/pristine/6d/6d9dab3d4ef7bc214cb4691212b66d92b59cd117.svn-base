/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.config.wizard.createfromfile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

import org.apache.log4j.Logger;
import org.netbeans.spi.wizard.ResultProgressHandle;
import org.netbeans.spi.wizard.WizardPanelNavResult;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.general.GeneralConfigUtility;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.xml.serialization.ProgressHandlerAdapter;
import com.lucy.g3.rtu.config.xml.serialization.XmlDataReader;

/**
 * This Wizard Result is for parsing the selected template file into ConfigData,
 * and store it into settings in context.
 */
class ConfigFileParser extends WizardPanelNavResult {

  public static final String KEY_CONFIG_DATA = "configData";

  private final File configFile;


  public ConfigFileParser(File configFile) {
    super(true, false);
    this.configFile = Preconditions.checkNotNull(configFile,
        "configFile is null");
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  @Override
  public void start(Map settings, ResultProgressHandle progress) {
    ProgressHandlerAdapter progHandler = new ProgressHandlerAdapter(progress);

    InputStream is = null;
    try {
      progress.setBusy("Parsing configuration file...");

      if (is == null) {
        is = new FileInputStream(configFile);
      }
      
      //TODO replace with Apache IO Utilities.
      String xmlstring = getStringFromInputStream(is);
      
      IConfig data = new XmlDataReader(progHandler).readFromString(xmlstring);
      GeneralConfigUtility.setFilePath(data, null);

      progress.setBusy("Parsing completed.");

      // Store parsing result
      settings.put(KEY_CONFIG_DATA, data);

      progress.finished(data);

    } catch (Exception e) {
      Logger.getLogger(getClass()).error("Fail to create ConfigData", e);
      progress.failed(
          "Cannot parse the configuration file. Please select another!",
          true);

    } finally {
      if (is != null) {
        try {
          is.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }
  
  private static String getStringFromInputStream(InputStream is) {

    BufferedReader br = null;
    StringBuilder sb = new StringBuilder();

    String line;
    try {

      br = new BufferedReader(new InputStreamReader(is));
      while ((line = br.readLine()) != null) {
        sb.append(line);
      }

    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (br != null) {
        try {
          br.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }

    return sb.toString();

  }
}
