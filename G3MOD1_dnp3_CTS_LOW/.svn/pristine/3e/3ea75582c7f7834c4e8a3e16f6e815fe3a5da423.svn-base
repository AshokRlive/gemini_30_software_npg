/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.domain;

/**
 * The configuration settings of a {@linkplain IProtocolChannel}.
 */
public interface IProtocolChannelConf {

  String PROPERTY_CHANNEL_NAME = "channelName";


  String getChannelName();

  void setChannelName(String channelName);

  String getChannelConfSummaryText();

  /**
   * Gets the serial configuration for this channel.
   *
   * @return serial configuration. Null if this channel is not a serial channel.
   */
  ProtocolChannelSerialConf getSerialConf();

  /**
   * Gets the TCP/IP configuration for this channel.
   *
   * @return TCP/IP configuration. Null if this channel is not a TCP/IP channel.
   */
  ProtocolChannelTCPConf getTcpConf();

  /**
   * Gets the channel.
   *
   * @return non-null channel.
   */
  IProtocolChannel<?> getChannel();

  /**
   * <p>
   * Releases the resource used by the channel, e.g. ports.
   * </p>
   * This method is usually called when deleting a channel.
   */
  void releaseResources();

  Object getLinkType();
  
  void setLinkType(Object type);

}
