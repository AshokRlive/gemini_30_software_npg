/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.ui.wizards.addmodule;

import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;

import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.rtu.config.module.canmodule.domain.CANModuleManager;
import com.lucy.g3.rtu.config.module.res.ModuleEnums;
import com.lucy.g3.rtu.config.module.res.ModuleResource;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

class ModuleTypeSelection extends WizardPage {

  static final String KEY_MODULE_TYPE = "MODULE_TYPE";
  static final String KEY_MODULE_ID = "MODULE_ID";

  private final HashMap<MODULE, DefaultComboBoxModel<MODULE_ID>> moduleTypeIdMap =
      new LinkedHashMap<MODULE, DefaultComboBoxModel<MODULE_ID>>();

  private CANModuleManager manager;


  public ModuleTypeSelection(CANModuleManager manager) {
    this.manager = manager;

    initComponents();
    this.comboBoxModuleType.setName(KEY_MODULE_TYPE);
    this.comboBoxModuleID.setName(KEY_MODULE_ID);

    updateModuleIDs();
  }

  private void createUIComponents() {
    MODULE[] allTypes = ModuleEnums.getSupportedTypes();
    for (MODULE mType : allTypes) {
      MODULE_ID[] ids = this.manager.getAvailableIDsByType(mType);
      if (ids != null && ids.length > 0) {
        this.moduleTypeIdMap.put(mType, new DefaultComboBoxModel<MODULE_ID>(ids));
      }
    }
    Set<MODULE> typeItems = this.moduleTypeIdMap.keySet();
    this.comboBoxModuleType = new JComboBox<MODULE>(typeItems.toArray(new MODULE[typeItems.size()]));
    this.comboBoxModuleType.setRenderer(new CombModuleTypeRenderer());
  }

  private void updateModuleIDs() {
    MODULE type = (MODULE) this.comboBoxModuleType.getSelectedItem();
    if (type != null) {
      this.comboBoxModuleID.setModel(this.moduleTypeIdMap.get(type));
    }

    // "ID" choices would be invisible if there is no more than one available id
    boolean visibleIDChoice = this.comboBoxModuleID.getItemCount() > 1;
    this.comboBoxModuleID.setVisible(visibleIDChoice);
    this.label2.setVisible(visibleIDChoice);

    putWizardData(KEY_MODULE_ID, this.comboBoxModuleID.getSelectedItem());
  }

  @Override
  protected String validateContents(Component component, Object event) {
    if (this.comboBoxModuleType.getSelectedItem() == null) {
      return "No module type selected.";
    }
    if (this.comboBoxModuleID.isVisible() && this.comboBoxModuleID.getSelectedItem() == null) {
      return "Mo module id selected.";
    }

    return null;
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    label1 = new JLabel();
    label2 = new JLabel();
    comboBoxModuleID = new JComboBox<>();

    // ======== this ========
    setLayout(new FormLayout(
        "default, $lcgap, [120dlu,pref]",
        "2*(default, $lgap), default"));

    // ---- label1 ----
    label1.setText("Module Type:");
    add(label1, CC.xy(1, 1));

    // ---- label2 ----
    label2.setText("Module ID:");
    add(label2, CC.xy(1, 3));

    // ---- comboBoxModuleType ----
    comboBoxModuleType.addItemListener(new ItemListener() {

      @Override
      public void itemStateChanged(ItemEvent e) {
        updateModuleIDs();
      }
    });
    add(comboBoxModuleType, CC.xy(3, 1));
    add(comboBoxModuleID, CC.xy(3, 3));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel label1;
  private JLabel label2;
  private JComboBox<MODULE> comboBoxModuleType;
  private JComboBox<MODULE_ID> comboBoxModuleID;


  // JFormDesigner - End of variables declaration //GEN-END:variables

  private static class CombModuleTypeRenderer extends DefaultListCellRenderer {

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value,
        int index, boolean isSelected, boolean cellHasFocus) {
      super.getListCellRendererComponent(list, value, index, isSelected,
          cellHasFocus);
      MODULE type = (MODULE) value;
      if (type != null) {
        this.setText(ModuleResource.INSTANCE.getModuleTypeShortName(type));
      }
      return this;
    }
  }
}
