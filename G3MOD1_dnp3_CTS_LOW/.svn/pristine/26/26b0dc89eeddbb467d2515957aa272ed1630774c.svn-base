  <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>4DIAC Properties</title>
	<link rel="stylesheet" type="text/css" href="../help.css">
</head>

<body>

<h1>4DIAC Properties</h1>
<h2>4DIAC-IDE Preferences</h2>
<p>Before starting with the engineering process of IEC 61499 <i>Applications</i>, you should set the preferences for the 4DIAC-IDE under <i>Window/Preferences/4DIAC</i>. There are different preference pages where various parameter settings for the 4DIAC-IDE can be made, like different colors, connection routers or the location of the FORTE executable. Please set the location within <i>FBRT Preferences</i> and <i>FORTE Preferences</i> if you want to start FBDK or FORTE within the Deployment perspective of 4DIAC-IDE. For FBRT specify the <span class="code">path/fbrt.jar</span> and for FORTE <span class="code">path/forte.exe</span>.</p> 

<img src="./img/4DIACpreferences.png" alt="4DIAC-IDE Preferences"/>

<h2>4DIAC Symbols and Element Properties</h2>
<p>Selecting an element (e.g. <i>Systems</i>, <i>Devices</i>, <i>Resources</i>, <i>Applications</i>, <i>Function Blocks</i>, ...) and pressing the right mouse button opens the context menu with several menus to manipulate the specific element. The following general toolbar items are provided:</p>

<ul type="square">
	<li><img src="./img/hideEvent.png" alt="Hide Events"/> and <img src="./img/hideData.png" alt="Hide Data"/> allow to either hide the event or the data connections in the <i>Application</i> Editor</li>
	<li><img src="./img/startMon.gif" alt="Select System for Monitoring"/> allows to start <i>Systems</i> for monitoring. This can also be achieved by right clicking on the <i>System</i> and choosing <a href=""><i>Monitor System</i></a>.</li>
	<li><img src="./img/icon_print.png" alt="Print Icon"/> allows to print the <i>Applications</i> and the Automation Hardware</li>
	<li><img src="./img/icon_save.png" alt="Save Icon"/> allows to save changes from the toolbar or the file (Save or Save All is used) but also by the key <span class="code">Ctrl + s</span></li>
	<li><img src="./img/undoredo.png" alt="Undo/Redo Icon"/> provides undo and/or redo of last changes</li>
	<li><img src="./img/zoom.png" alt="Zoom Functionality"/> provides zoom functionality in the toolbar or the menu entries in the context menu of an editor but also by pressing <span class="code">Ctrl</span> and scrolling the mouse wheel</li>
	<li>Function Block instances and <i>Devices</i> respectively can be deleted using the <span class="code">Del</span> key or the context menu entry Delete.</li>
	<li>Instance names of Function Blocks, <i>Resources</i> or <i>Devices</i> can be changed by double click that enables the instance name field to be editable. It is also possible to change the instance name in the Properties View at the bottom of the window.
	<br><img src="./img/instanceName.png" alt="change instance name by double click or property view"/></li> 
</ul>

</body>
</html>