/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.ui;

import org.eclipse.emf.common.util.EList;
import org.fordiac.ide.model.libraryElement.SubApp;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mapped Sub App View</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.fordiac.ide.model.ui.MappedSubAppView#getInterfaceElements <em>Interface Elements</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.MappedSubAppView#getApplicationSubApp <em>Application Sub App</em>}</li>
 *   <li>{@link org.fordiac.ide.model.ui.MappedSubAppView#getSubApp <em>Sub App</em>}</li>
 * </ul>
 *
 * @see org.fordiac.ide.model.ui.UiPackage#getMappedSubAppView()
 * @model
 * @generated
 */
public interface MappedSubAppView extends View {
	/**
	 * Returns the value of the '<em><b>Interface Elements</b></em>' containment reference list.
	 * The list contents are of type {@link org.fordiac.ide.model.ui.MappedSubAppInterfaceElementView}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interface Elements</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interface Elements</em>' containment reference list.
	 * @see org.fordiac.ide.model.ui.UiPackage#getMappedSubAppView_InterfaceElements()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<MappedSubAppInterfaceElementView> getInterfaceElements();

	/**
	 * Returns the value of the '<em><b>Application Sub App</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.fordiac.ide.model.ui.SubAppView#getMappedSubApp <em>Mapped Sub App</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Application Sub App</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Application Sub App</em>' reference.
	 * @see #setApplicationSubApp(SubAppView)
	 * @see org.fordiac.ide.model.ui.UiPackage#getMappedSubAppView_ApplicationSubApp()
	 * @see org.fordiac.ide.model.ui.SubAppView#getMappedSubApp
	 * @model opposite="mappedSubApp"
	 * @generated
	 */
	SubAppView getApplicationSubApp();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.MappedSubAppView#getApplicationSubApp <em>Application Sub App</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Application Sub App</em>' reference.
	 * @see #getApplicationSubApp()
	 * @generated
	 */
	void setApplicationSubApp(SubAppView value);

	/**
	 * Returns the value of the '<em><b>Sub App</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub App</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub App</em>' reference.
	 * @see #setSubApp(SubApp)
	 * @see org.fordiac.ide.model.ui.UiPackage#getMappedSubAppView_SubApp()
	 * @model
	 * @generated
	 */
	SubApp getSubApp();

	/**
	 * Sets the value of the '{@link org.fordiac.ide.model.ui.MappedSubAppView#getSubApp <em>Sub App</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sub App</em>' reference.
	 * @see #getSubApp()
	 * @generated
	 */
	void setSubApp(SubApp value);

} // MappedSubAppView
