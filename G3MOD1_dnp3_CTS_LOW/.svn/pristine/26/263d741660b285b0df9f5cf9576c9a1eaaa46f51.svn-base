function [OC, CurrentParam, VoltageParam, Power, HardwareModel, AMMConfig, Phasor] = InitAMMGxSwitch()
%#codegen
% Initialize AMM configuration parameters
% 
% 

HardwareModel = struct ('VoltagesOnSwitchA', uint32(0),...
                        'VoltagesOnSwitchB', uint32(0),...
                        'CurrentsOnSwitchA', uint32(0),...
                        'CurrentsOnSwitchB', uint32(0));

AMMConfig = struct(  'HardwareModel', HardwareModel,...
                     'NominalFrequency',  single(0),...
                     'NominalSamplingRate', single(0),...
                     'SamplingTime', single(0),...
                     'PointsPerCycle',  single(0),...
                     'MinReliableCurrentAmplitudeLevel', single(0),...
                     'MinReliableVoltageAmplitudeLevel', single(0));                           

% Sine and Cosine Orthogonal Components (Fourier Transform) Structure Arrays
OC = struct( 'Sine', single(zeros(64, int32( WithReferenceTo.LineCA))),...               % Sine component (FT)
            'Cosine', single(zeros(64, int32(WithReferenceTo.LineCA))));%,...             % Cosine component (FT)
%            'rSine', single(zeros(1, int32(WithReferenceTo.LineCA))),...              % Recursive Sine component (FT)
%            'rCosine', single(zeros(1, int32(WithReferenceTo.LineCA))));              % Recursive Cosine component (FT)

%             
CurrentParam = struct ('Mag', single(zeros(64, int32(WithReferenceTo.Neutral))),...                 % Magnitude
                            'Phi', single(zeros(64, int32(WithReferenceTo.Neutral))),...                 % Phase angle
                            'OC', OC,...                                                             % Orthogonal Components (DFT)
                            'TransientMonitor', single(zeros(1, int32(WithReferenceTo.Neutral))),...    % Transient Monitor
                            'FilteredData', single(zeros(64, int32(WithReferenceTo.Neutral))),...        % Filtered ADC measurements
                            'RawData', single(zeros(1280, int32(WithReferenceTo.Neutral))));                       % Raw ADC measurements
                        
%             
VoltageParam = struct ('Mag', single(zeros(64, int32(WithReferenceTo.LineCA))),...            % Magnitude
                           'Phi', single(zeros(64, int32(WithReferenceTo.LineCA))),...              % Phase angle
                           'OC', OC,...                                                         % Orthogonal Components (DFT)
                           'TransientMonitor', single(zeros(1, int32(WithReferenceTo.LineCA))),... % Transient Monitor
                           'FilteredData', single(zeros(64, int32(WithReferenceTo.LineCA))),...     % Filtered ADC measurements
                           'RawData', single(zeros(1280, int32(WithReferenceTo.LineCA))));                    % Raw ADC measurements
                        
%                        
Power = struct(  'Pa', single(zeros(1)), 'Pb', single(zeros(1)), 'Pc', single(zeros(1)),...    % Real Power
                    'Qa', single(zeros(1)), 'Qb', single(zeros(1)), 'Qc', single(zeros(1)),...    % Reactive Power
                    'Sa', single(zeros(1)), 'Sb', single(zeros(1)), 'Sc', single(zeros(1)),...    % Apparent Power
                    'PFa', single(zeros(1)), 'PFb', single(zeros(1)), 'PFc', single(zeros(1)));   % Power Factor

% Gx switch has two sets of Voltages and one set of Currents and Powers
% For code generation purposes the function repmat HAS to be used
% in order to code generate arrays of structures.
% Using a for loop instead of repmat simply does NOT work for code
% generation purposes, while having identical results(/working fine) in Matlab.
%V_EstimatedParam = repmat(temp,1,int32(Channel.ChB));
                                
Phasor = struct( 'V', VoltageParam,...
                 'I', CurrentParam,...
                 'Pwr', Power);
               
% GX Overhead Switch AMM hardware model
% Six Voltages and Four Currents (Currents consist of 3 phases and Neutral)              
% PhasorStr(1) = struct('V', EstimatedParamStr,...
%                       'I', EstimatedParamStr,...
%                       'Pwr', PowerStr);
%                       
% Gx switch has two sets of Voltages and one set of Currents and Powers               
% for i = 1:1:int32(Channel.ChB)
%     PhasorStr.V(i) = PhasorStr.V(1);
% end               
          
end