/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.ui.dialog.editor;

import static com.jgoodies.validation.view.ValidationComponentUtils.setMessageKey;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint.PROPERTY_FILTER0_DEADBAND;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint.PROPERTY_FILTER0_INIT_NOMINAL;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint.PROPERTY_FILTER0_INIT_NOMINAL_ENABLED;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint.PROPERTY_FILTER1_LOWER_ENABLED;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint.PROPERTY_FILTER1_LOWER_HYSTERESIS;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint.PROPERTY_FILTER1_LOWER_LIMIT;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint.PROPERTY_FILTER1_UPPER_ENABLED;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint.PROPERTY_FILTER1_UPPER_HYSTERESIS;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint.PROPERTY_FILTER1_UPPER_LIMIT;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint.PROPERTY_FILTER_TYPE;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint.PROPERTY_OVERFLOW;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint.PROPERTY_OVERFLOW_ENABLED;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint.PROPERTY_OVERFLOW_HYSTERESIS;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint.PROPERTY_RAW_MIN;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint.PROPERTY_SCALED_MAX;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint.PROPERTY_SCALED_MIN;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint.PROPERTY_SCALING_FACTOR;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint.PROPERTY_UNDERFLOW;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint.PROPERTY_UNDERFLOW_ENABLED;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint.PROPERTY_UNDERFLOW_HYSTERESIS;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint.PROPERTY_UNIT;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.text.NumberFormatter;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXTitledSeparator;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.BufferedValueModel;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.l2fprod.common.swing.JCollapsiblePane;
import com.lucy.g3.common.utils.EqualsUtil;
import com.lucy.g3.gui.common.widgets.ext.swing.formatter.StrictNumberFormatter;
import com.lucy.g3.gui.common.widgets.ext.swing.textfield.FormattedTextFieldSupport;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.rtu.config.shared.base.dialogs.IEditorDialogInvoker;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.shared.base.misc.ComponentGroup;
import com.lucy.g3.rtu.config.shared.validation.BufferValidator;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoAnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.HighHighLowLow;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint.FILTER;
import com.lucy.g3.rtu.config.virtualpoint.shared.ui.effects.FadingBGValueChangeHandler;
import com.lucy.g3.rtu.config.virtualpoint.shared.ui.panel.HighHighLowLowPanel;
import com.lucy.g3.rtu.config.virtualpoint.shared.ui.panel.PercentEditorPanel;
import com.lucy.g3.rtu.config.virtualpoint.shared.ui.panel.ScalingPanel;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdAnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.ui.validation.AnalogPointValidator;
import com.lucy.g3.rtu.config.virtualpointsource.domain.IVirtualPointSource;

/**
 * Dialog for editing {@link StdAnaloguePoint} and {@link PseudoAnaloguePoint}.
 */
final class AnaloguePointDialog extends VirtualPointDialog<AnaloguePoint> {

  private Logger log = Logger.getLogger(AnaloguePointDialog.class);

  private final ValueHolder fullRangeVM = new ValueHolder(Double.valueOf(0));

  private final PresentationModel<HighHighLowLow> hihiloloPM;

  private final PresentationModel<AnaloguePoint> pm = getModel();

  {
    hihiloloPM = createPresentationModel(getEditItem().getF2HiHiLoLo());
  }


  public AnaloguePointDialog(Frame parent, IVirtualPointDialogResource resource,
      AnaloguePoint point) {
    super(parent, resource, point);
  }

  public AnaloguePointDialog(Frame parent, IVirtualPointDialogResource resource,
      IEditorDialogInvoker<AnaloguePoint> invoker) {
    super(parent, VirtualPointType.ANALOGUE_INPUT, resource, invoker);
  }

  @Override
  void init() {

    initComponents();
    initComponentAnnotations();
    initCommitOnType();
    initEventHandling();
    initComponentsBinding();
    updateFilterPanel();
  }

  private void initComponentsBinding() {
    BufferedValueModel unitVM = getModel().getBufferedModel(PROPERTY_UNIT);
    card4.bindUnit(unitVM);
    overFlowHysteresis.bindUnit(unitVM);
    underFlowHysteresis.bindUnit(unitVM);
    upperLimitHysteresis.bindUnit(unitVM);
    lowerLimitHysteresis.bindUnit(unitVM);
    deadband.bindUnit(unitVM);

    Bindings.bind(checkBoxOverflow, getModel().getBufferedModel(PROPERTY_OVERFLOW_ENABLED));
    Bindings.bind(checkBoxUnderflow, getModel().getBufferedModel(PROPERTY_UNDERFLOW_ENABLED));

    new ComponentGroup(lblOverflowLimitUnit, tfOverflowLimit, lblOverflowLimitUnit, overFlowHysteresis)
        .bindEnableState(checkBoxOverflow);

    new ComponentGroup(lblUnderflowLimitUnit, tfUnderflowLimit, lblUnderflowLimitUnit, underFlowHysteresis)
        .bindEnableState(checkBoxUnderflow);
  }

  private void updatePercentageVisibility(boolean visible) {
    overFlowHysteresis.setPercentageVisibility(visible);
    underFlowHysteresis.setPercentageVisibility(visible);
    upperLimitHysteresis.setPercentageVisibility(visible);
    lowerLimitHysteresis.setPercentageVisibility(visible);
    card4.setPercentageVisibility(visible);
  }

  private void updateFilterPanel() {
    CardLayout cl = (CardLayout) (panelFilter.getLayout());

    if (radioDeadband.isSelected()) {
      cl.show(panelFilter, "card1");
    } else if (radioUpperLowerLimit.isSelected()) {
      cl.show(panelFilter, "card2");
    } else if (radioHighlow.isSelected()) {
      cl.show(panelFilter, "card4");
    } else if (radioNoneFilter.isSelected()) {
      cl.show(panelFilter, "card5");
    }

    panelFilterContainer.setCollapsed(radioNoneFilter.isSelected());
  }

  private void updateFullRangeValue(double fullRangeValue) {
    log.debug("Updating fullRangeValue...");

    /* Update full range value of all children components */
    overFlowHysteresis.setFullRangeValue(fullRangeValue);
    underFlowHysteresis.setFullRangeValue(fullRangeValue);
    upperLimitHysteresis.setFullRangeValue(fullRangeValue);
    lowerLimitHysteresis.setFullRangeValue(fullRangeValue);
    deadband.setFullRangeValue(fullRangeValue);
    card4.setFullRangeValue(fullRangeValue);

    log.debug("Update fullRangeValue:" + fullRangeValue);
  }

  private void initEventHandling() {

    UIUtils.setAllTextFieldsCaretPositionOnClick(contentPanel);

    new ComponentGroup(tfUpperlimit, lblUpperLimitUnit, upperLimitHysteresis).bindEnableState(cboxUpperLimitEnabled);
    new ComponentGroup(tfLowerlimit, lblLowerLimitUnit, lowerLimitHysteresis).bindEnableState(cboxLowerLimitEnabled);

    /* Hide hysteresis percent if the unit is set to % */
    pm.getBufferedModel(PROPERTY_UNIT).addValueChangeListener(new PropertyChangeListener() {

      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        Object newValue = evt.getNewValue();
        if (newValue != null) {
          updatePercentageVisibility(!newValue.toString().contains("%"));
        }
      }
    });
    Object unit = pm.getBufferedValue(PROPERTY_UNIT);
    if (unit != null) {
      updatePercentageVisibility(!unit.toString().contains("%"));
    }

    /* Update full range value in all components */
    fullRangeVM.addValueChangeListener(new PropertyChangeListener() {

      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        if (!EqualsUtil.areEqual(evt.getOldValue(), evt.getNewValue())) {
          updateFullRangeValue((Double) (evt.getNewValue()));
        }
      }
    });

    /* Update overflow when scaled value changes */
    pm.getBufferedModel(PROPERTY_SCALED_MIN).addValueChangeListener(new PropertyChangeListener() {

      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        setBufferedValue(PROPERTY_UNDERFLOW, evt.getNewValue());
      }
    });

    /* Update underflow when scaled value changes */
    pm.getBufferedModel(PROPERTY_SCALED_MAX).addValueChangeListener(new PropertyChangeListener() {

      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        setBufferedValue(PROPERTY_OVERFLOW, evt.getNewValue());
      }
    });

    installFadingBG();
  }

  private void installFadingBG() {
    FadingBGValueChangeHandler.install(pm.getBufferedModel(PROPERTY_OVERFLOW), tfOverflowLimit);
    FadingBGValueChangeHandler.install(pm.getBufferedModel(PROPERTY_UNDERFLOW), tfUnderflowLimit);
  }

  private void initCommitOnType() {
    FormattedTextFieldSupport.installCommitOnType(tfOverflowLimit);
    FormattedTextFieldSupport.installCommitOnType(tfUnderflowLimit);
    FormattedTextFieldSupport.installCommitOnType(tfUpperlimit);
    FormattedTextFieldSupport.installCommitOnType(tfLowerlimit);
  }

  /**
   * Initialise components keys for displaying validation result.
   */
  private void initComponentAnnotations() {
    // Overflow
    setMessageKey(tfOverflowLimit, PROPERTY_OVERFLOW);
    setMessageKey(tfUnderflowLimit, PROPERTY_UNDERFLOW);

    // DeadBand fields
    setMessageKey(tfInitNorminal, PROPERTY_FILTER0_INIT_NOMINAL);

    // LowerUpperLimitFilter fields
    setMessageKey(tfUpperlimit, PROPERTY_FILTER1_UPPER_LIMIT);
    setMessageKey(tfLowerlimit, PROPERTY_FILTER1_LOWER_LIMIT);

  }

  @Override
  void initForStdPoint() {
    // Handle channel change event
    getModel().getBufferedModel(StdBinaryPoint.PROPERTY_SOURCE)
        .addPropertyChangeListener(BufferedValueModel.PROPERTY_VALUE, new BufferChannelPCL());
  }

  @Override
  void initForPseudoPoint() {
    // Nothing to do
  }

  private void createUIComponents() {
    panelTop = new VirtualPointSourcePanel(pm,
        new ArrayList<IVirtualPointSource>(getAllChannels(ChannelType.ANALOG_INPUT)));

    BufferedValueModel vm;

    scalingPanel = new ScalingPanel(pm, fullRangeVM);
    final double fullRangeValue = scalingPanel.calculateRangeValue();

    /* Overflow field */
    tfOverflowLimit = BasicComponentFactory.createFormattedTextField(
        pm.getBufferedModel(PROPERTY_OVERFLOW), StrictNumberFormatter.createDefault());

    /* Underflow field */
    tfUnderflowLimit = BasicComponentFactory.createFormattedTextField(
        pm.getBufferedModel(PROPERTY_UNDERFLOW), StrictNumberFormatter.createDefault());

    /* Filter radio buttons */
    vm = pm.getBufferedModel(PROPERTY_FILTER_TYPE);
    radioDeadband = BasicComponentFactory.createRadioButton(vm, FILTER.DeadBand, FILTER.DeadBand.getName());
    radioUpperLowerLimit =
        BasicComponentFactory.createRadioButton(vm, FILTER.LowerUpperLimit, FILTER.LowerUpperLimit.getName());
    radioHighlow = BasicComponentFactory.createRadioButton(vm, FILTER.HighHighLowLow, FILTER.HighHighLowLow.getName());
    radioNoneFilter = BasicComponentFactory.createRadioButton(vm, FILTER.None, FILTER.None.getName());

    NumberFormatter doubleFormat = new NumberFormatter();
    doubleFormat.setValueClass(Double.class);

    /* Deadband */
    tfInitNorminal = BasicComponentFactory.createFormattedTextField(
        pm.getBufferedComponentModel(PROPERTY_FILTER0_INIT_NOMINAL), doubleFormat);

    radioInitNorminal = BasicComponentFactory.createRadioButton(
        pm.getBufferedModel(PROPERTY_FILTER0_INIT_NOMINAL_ENABLED), true, "");

    radioUseFirst = BasicComponentFactory.createRadioButton(
        pm.getBufferedModel(PROPERTY_FILTER0_INIT_NOMINAL_ENABLED), false, "");

    deadband =
        new PercentEditorPanel(PROPERTY_FILTER0_DEADBAND, fullRangeValue,
            pm.getBufferedModel(PROPERTY_FILTER0_DEADBAND));

    /* UpperLimit */
    tfUpperlimit = BasicComponentFactory.createFormattedTextField(
        pm.getBufferedModel(PROPERTY_FILTER1_UPPER_LIMIT), doubleFormat);

    /* LowerLimit */
    tfLowerlimit = BasicComponentFactory.createFormattedTextField(
        pm.getBufferedModel(PROPERTY_FILTER1_LOWER_LIMIT), doubleFormat);

    cboxUpperLimitEnabled =
        BasicComponentFactory.createCheckBox(pm.getBufferedModel(PROPERTY_FILTER1_UPPER_ENABLED), "");
    cboxLowerLimitEnabled =
        BasicComponentFactory.createCheckBox(pm.getBufferedModel(PROPERTY_FILTER1_LOWER_ENABLED), "");

    card4 = new HighHighLowLowPanel(fullRangeValue, hihiloloPM);

    /* Unit label */
    vm = pm.getBufferedModel(PROPERTY_UNIT);
    lblUnderflowLimitUnit = BasicComponentFactory.createLabel(vm);
    lblOverflowLimitUnit = BasicComponentFactory.createLabel(vm);

    lblDeadBandUnit2 = BasicComponentFactory.createLabel(vm);

    lblLowerLimitUnit = BasicComponentFactory.createLabel(vm);
    lblUpperLimitUnit = BasicComponentFactory.createLabel(vm);

    overFlowHysteresis =
        new PercentEditorPanel(PROPERTY_OVERFLOW_HYSTERESIS, fullRangeValue,
            pm.getBufferedModel(PROPERTY_OVERFLOW_HYSTERESIS));
    underFlowHysteresis =
        new PercentEditorPanel(PROPERTY_UNDERFLOW_HYSTERESIS, fullRangeValue,
            pm.getBufferedModel(PROPERTY_UNDERFLOW_HYSTERESIS));
    upperLimitHysteresis =
        new PercentEditorPanel(PROPERTY_FILTER1_UPPER_HYSTERESIS, fullRangeValue,
            pm.getBufferedModel(PROPERTY_FILTER1_UPPER_HYSTERESIS));
    lowerLimitHysteresis =
        new PercentEditorPanel(PROPERTY_FILTER1_LOWER_HYSTERESIS, fullRangeValue,
            pm.getBufferedModel(PROPERTY_FILTER1_LOWER_HYSTERESIS));

    // Sync enable state of Nominal components
    PropertyConnector.connectAndUpdate(
        pm.getBufferedModel(PROPERTY_FILTER0_INIT_NOMINAL_ENABLED),
        new ComponentGroup(lblDeadBandUnit2, tfInitNorminal),
        ComponentGroup.PROPERTY_ENABLED);

    // Label Set
    panelLabelSet = createLabelSetPanel();
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    contentPanel = new JPanel();
    sepParam = new JXTitledSeparator();
    sepParam2 = new JXTitledSeparator();
    panel4 = new JPanel();
    checkBoxOverflow = new JCheckBox();
    checkBoxUnderflow = new JCheckBox();
    sepFilter = new JXTitledSeparator();
    JPanel panelFilterType = new JPanel();
    panelFilterContainer = new JCollapsiblePane();
    panelFilter = new JPanel();
    card1 = new JPanel();
    label3 = new JLabel();
    panel1 = new JPanel();
    card2 = new JPanel();
    card5 = new JPanel();

    //======== contentPanel ========
    {
      contentPanel.setBorder(new EmptyBorder(0, 0, 0, 10));
      contentPanel.setLayout(new FormLayout(
        "default:grow, 0dlu",
        "fill:default, $ugap, default, $lgap, default, $ugap, default, $lgap, default, $ugap, 3*(default, $lgap), default"));
      contentPanel.add(panelTop, CC.xy(1, 1));

      //---- sepParam ----
      sepParam.setTitle("Scaling");
      contentPanel.add(sepParam, CC.xy(1, 3, CC.FILL, CC.DEFAULT));
      contentPanel.add(scalingPanel, CC.xy(1, 5));

      //---- sepParam2 ----
      sepParam2.setTitle("Out of Range");
      contentPanel.add(sepParam2, CC.xy(1, 7));

      //======== panel4 ========
      {
        panel4.setLayout(new FormLayout(
          "default, $lcgap, 35dlu, $lcgap, default, $ugap, default",
          "fill:default, $lgap, fill:default"));

        //---- checkBoxOverflow ----
        checkBoxOverflow.setText("Overflow:");
        panel4.add(checkBoxOverflow, CC.xy(1, 1));

        //---- tfOverflowLimit ----
        tfOverflowLimit.setColumns(10);
        panel4.add(tfOverflowLimit, CC.xy(3, 1));
        panel4.add(lblOverflowLimitUnit, CC.xy(5, 1, CC.FILL, CC.DEFAULT));

        //---- overFlowHysteresis ----
        overFlowHysteresis.setDisplayName("Hysteresis:");
        overFlowHysteresis.setFadingEffectEnabled(true);
        panel4.add(overFlowHysteresis, CC.xy(7, 1));

        //---- checkBoxUnderflow ----
        checkBoxUnderflow.setText("Underflow:");
        panel4.add(checkBoxUnderflow, CC.xy(1, 3));

        //---- tfUnderflowLimit ----
        tfUnderflowLimit.setColumns(10);
        panel4.add(tfUnderflowLimit, CC.xy(3, 3));
        panel4.add(lblUnderflowLimitUnit, CC.xy(5, 3, CC.FILL, CC.DEFAULT));

        //---- underFlowHysteresis ----
        underFlowHysteresis.setDisplayName("Hysteresis:");
        underFlowHysteresis.setFadingEffectEnabled(true);
        panel4.add(underFlowHysteresis, CC.xy(7, 3));
      }
      contentPanel.add(panel4, CC.xy(1, 9, CC.FILL, CC.DEFAULT));

      //---- sepFilter ----
      sepFilter.setTitle("Protocol Event Filter");
      contentPanel.add(sepFilter, CC.xy(1, 11));

      //======== panelFilterType ========
      {
        panelFilterType.setLayout(new FlowLayout());

        //---- radioDeadband ----
        radioDeadband.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            updateFilterPanel();
          }
        });
        panelFilterType.add(radioDeadband);

        //---- radioHighlow ----
        radioHighlow.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            updateFilterPanel();
          }
        });
        panelFilterType.add(radioHighlow);

        //---- radioNoneFilter ----
        radioNoneFilter.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            updateFilterPanel();
          }
        });
        panelFilterType.add(radioNoneFilter);
      }
      contentPanel.add(panelFilterType, CC.xy(1, 13, CC.LEFT, CC.DEFAULT));

      //======== panelFilterContainer ========
      {
        Container panelFilterContainerContentPane = panelFilterContainer.getContentPane();
        panelFilterContainerContentPane.setLayout(new BorderLayout());

        //======== panelFilter ========
        {
          panelFilter.setBorder(new EmptyBorder(5, 10, 5, 10));
          panelFilter.setLayout(new CardLayout());

          //======== card1 ========
          {
            card1.setBorder(null);
            card1.setLayout(new FormLayout(
              "default, $lcgap, 35dlu:grow",
              "default, $ugap, default"));

            //---- label3 ----
            label3.setText("Dead band: ");
            label3.setHorizontalAlignment(SwingConstants.RIGHT);
            card1.add(label3, CC.xy(1, 1));

            //---- deadband ----
            deadband.setDisplayName(null);
            deadband.setFadingEffectEnabled(true);
            card1.add(deadband, CC.xywh(2, 1, 2, 1));

            //======== panel1 ========
            {
              panel1.setBorder(new TitledBorder("Nominal"));
              panel1.setLayout(new FormLayout(
                "default, $lcgap, 35dlu, $lcgap, default, $lcgap, default:grow",
                "default, $lgap, default"));

              //---- radioUseFirst ----
              radioUseFirst.setText("Use first value as initial nominal");
              panel1.add(radioUseFirst, CC.xywh(1, 1, 7, 1));

              //---- radioInitNorminal ----
              radioInitNorminal.setText("Specify initial nominal:");
              panel1.add(radioInitNorminal, CC.xy(1, 3));

              //---- tfInitNorminal ----
              tfInitNorminal.setColumns(10);
              panel1.add(tfInitNorminal, CC.xy(3, 3));
              panel1.add(lblDeadBandUnit2, CC.xy(5, 3, CC.FILL, CC.FILL));
            }
            card1.add(panel1, CC.xywh(1, 3, 3, 1));
          }
          panelFilter.add(card1, "card1");

          //======== card2 ========
          {
            card2.setLayout(new FormLayout(
              "default, $lcgap, 35dlu, $lcgap, default, $lcgap, $ugap, $rgap, left:default:grow",
              "default, $lgap, default"));

            //---- cboxUpperLimitEnabled ----
            cboxUpperLimitEnabled.setText("Upper Limit:");
            card2.add(cboxUpperLimitEnabled, CC.xy(1, 1));
            card2.add(tfUpperlimit, CC.xy(3, 1));
            card2.add(lblUpperLimitUnit, CC.xy(5, 1, CC.FILL, CC.FILL));

            //---- upperLimitHysteresis ----
            upperLimitHysteresis.setDisplayName("Hysteresis:");
            upperLimitHysteresis.setFadingEffectEnabled(true);
            card2.add(upperLimitHysteresis, CC.xy(9, 1));

            //---- cboxLowerLimitEnabled ----
            cboxLowerLimitEnabled.setText("Lower Limit:");
            card2.add(cboxLowerLimitEnabled, CC.xy(1, 3));
            card2.add(tfLowerlimit, CC.xy(3, 3));
            card2.add(lblLowerLimitUnit, CC.xy(5, 3, CC.FILL, CC.FILL));

            //---- lowerLimitHysteresis ----
            lowerLimitHysteresis.setDisplayName("Hysteresis:");
            lowerLimitHysteresis.setFadingEffectEnabled(true);
            card2.add(lowerLimitHysteresis, CC.xy(9, 3));
          }
          panelFilter.add(card2, "card2");
          panelFilter.add(card4, "card4");

          //======== card5 ========
          {
            card5.setLayout(new BorderLayout());
          }
          panelFilter.add(card5, "card5");
        }
        panelFilterContainerContentPane.add(panelFilter, BorderLayout.CENTER);
      }
      contentPanel.add(panelFilterContainer, CC.xy(1, 15));
      contentPanel.add(panelLabelSet, CC.xy(1, 17));
    }

    //---- radioUpperLowerLimit ----
    radioUpperLowerLimit.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        updateFilterPanel();
      }
    });
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel contentPanel;
  private VirtualPointSourcePanel panelTop;
  private JXTitledSeparator sepParam;
  private ScalingPanel scalingPanel;
  private JXTitledSeparator sepParam2;
  private JPanel panel4;
  private JCheckBox checkBoxOverflow;
  private JFormattedTextField tfOverflowLimit;
  private JLabel lblOverflowLimitUnit;
  private PercentEditorPanel overFlowHysteresis;
  private JCheckBox checkBoxUnderflow;
  private JFormattedTextField tfUnderflowLimit;
  private JLabel lblUnderflowLimitUnit;
  private PercentEditorPanel underFlowHysteresis;
  private JXTitledSeparator sepFilter;
  private JRadioButton radioDeadband;
  private JRadioButton radioHighlow;
  private JRadioButton radioNoneFilter;
  private JCollapsiblePane panelFilterContainer;
  private JPanel panelFilter;
  private JPanel card1;
  private JLabel label3;
  private PercentEditorPanel deadband;
  private JPanel panel1;
  private JRadioButton radioUseFirst;
  private JRadioButton radioInitNorminal;
  private JFormattedTextField tfInitNorminal;
  private JLabel lblDeadBandUnit2;
  private JPanel card2;
  private JCheckBox cboxUpperLimitEnabled;
  private JFormattedTextField tfUpperlimit;
  private JLabel lblUpperLimitUnit;
  private PercentEditorPanel upperLimitHysteresis;
  private JCheckBox cboxLowerLimitEnabled;
  private JFormattedTextField tfLowerlimit;
  private JLabel lblLowerLimitUnit;
  private PercentEditorPanel lowerLimitHysteresis;
  private HighHighLowLowPanel card4;
  private JPanel card5;
  private LabelSetPanel panelLabelSet;
  private JRadioButton radioUpperLowerLimit;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  @Override
  protected JPanel createMainPanel() {
    return contentPanel;
  }

  @Override
  protected BufferValidator createValidator(PresentationModel<AnaloguePoint> target) {
    return new AnalogPointValidator(target, hihiloloPM);
  }

  private void setBufferedValue(String property, Object value) {
    if (!EqualsUtil.areEqual(value, pm.getBufferedValue(property))) {
      pm.setBufferedValue(property, value);
    }
  }


  private class BufferChannelPCL implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      if (evt.getNewValue() == null || evt.getNewValue() == evt.getOldValue()) {
        return;
      }

      IChannel ch = (IChannel) evt.getNewValue();
      updateScalingFactor(ch);
      updateRawValue(ch);
    }

    private void updateRawValue(IChannel ch) {
      scalingPanel.setRawUnit(ch == null ? null : ch.predefined().getRawUnit());

      double newRawMin = ch.predefined().getRawMin();
      double newRawMax = ch.predefined().getRawMax();

      setBufferedValue(PROPERTY_RAW_MIN, newRawMin);
      setBufferedValue(PROPERTY_RAW_MIN, newRawMax);
    }

    /**
     * Update scaling factor choices in comboBox component. Supposed to be called when channel is changed.
     */
    private void updateScalingFactor(IChannel ch) {
      assert SwingUtilities.isEventDispatchThread();

      /* Gets scaling factor & unit map from current channel. */
      Map<Double, String> umap = ch.predefined().getPredefindScalingUnit();

      // Update scaling factor items
      scalingPanel.setScalingFactorItems(umap, true);

      // Update default scaling factor
      setBufferedValue(PROPERTY_SCALING_FACTOR, ch == null ? 1.0D : ch.predefined().getDefaultScalingFactor());

    }
  }


  @Override
  protected void editingPointChanged(AnaloguePoint newPoint) {
    hihiloloPM.setBean(newPoint.getF2HiHiLoLo());
  }

}
