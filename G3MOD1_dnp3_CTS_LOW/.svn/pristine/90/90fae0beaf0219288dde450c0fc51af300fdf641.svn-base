
package com.lucy.g3.cert.signer.httpserver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.lucy.g3.common.manifest.ManifestInfo;

public class Main {
  private final static String OPT_CONF = "c";
      
  private Main(){}
  
  public static void main(String[] args) {
    String logconf = System.getProperty("log4j.configuration");
    if(logconf != null)
      PropertyConfigurator.configureAndWatch(logconf,1000);
    
    CommandLine cl = proceedArgs(args);
    
    FileInputStream conf = getConfigInputStream(cl.getOptionValue(OPT_CONF));
    SimpleHttpServer server;
    try {
      server = new SimpleHttpServer(conf);
      server.start();
    } catch (IOException e) {
      Logger.getLogger(Main.class).error("Failed to create http server", e);
    }
  }

  private static Options buildOptions() {
    Option help = Option.builder("h")
        .longOpt("help")
        .desc("print this message.")
        .build();
    
    Option config = Option.builder("c")
        .longOpt("config")
        .numberOfArgs(1).optionalArg(false).argName("LOCATION").type(String.class)
        .desc("configuration file path")
        .build();
    
    Option log = Option.builder("l")
        .longOpt("log")
        .numberOfArgs(1).argName("LEVEL")
        .desc("set log level to one of: DEBUG|INFO|WARN|ERROR|FATAL|OFF|ALL")
        .build();
    
    Option version = Option.builder("v")
        .longOpt("version")
        .desc("print version information")
        .build();
    
    Options options = new Options();
    options.addOption(help);
    options.addOption(log);
    options.addOption(version);
    options.addOption(config);
    return options;
  }

  private static CommandLine proceedArgs(String[] args) {
    Options options = buildOptions();
    CommandLineParser parser = new DefaultParser();
    CommandLine line = null;
    try {
      line = parser.parse(options, args, true);
    } catch (ParseException e) {
      exitWithErr(e.getMessage());
    }
    
    if(line.hasOption("h")) {
      printHelp(options);
      exit();
      
    } else if(line.hasOption("v")){
      printVersion();
      exit();
      
    } else if(line.hasOption("l")) {
      Level level = Level.toLevel(line.getOptionValue("l"));
      if(level == null) {
        exitWithErr("Invalid log level:"+line.hasOption("l"));
      } else {
        Logger.getRootLogger().setLevel(level);
      }
    }
    
    return line;
  }

  private static void exit() {
    System.exit(0);
  }

  private static FileInputStream getConfigInputStream(String configPath) {
    if(configPath == null || configPath.trim().isEmpty()) {
      configPath = "http.conf";
    }
    File confFile = new File(configPath);
    if(!confFile.isFile()||!confFile.exists()) {
      exitWithErr("configuration file not found: " + configPath);
    }
    
    FileInputStream in = null;
    try {
      in = new FileInputStream(confFile);
    } catch (FileNotFoundException e) {
      exitWithErr(e.getMessage());
    }
    return in;
  }

  private static void exitWithErr(String err) {
    System.err.println(err);
    System.exit(-1);
  }
  
  private static void printVersion() {
    ManifestInfo manifest = new ManifestInfo("G3 Singer");
    System.out.println(manifest.getFormattedText());
  }

  private static void printHelp(Options options){
    HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp(
        "java -jar {JARNAME}" , 
        options, 
        true);
  }
}

