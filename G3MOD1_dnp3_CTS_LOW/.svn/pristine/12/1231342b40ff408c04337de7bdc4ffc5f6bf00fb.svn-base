/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.subsys.realtime.ui.points;

import java.util.Date;

import javax.swing.ListModel;

import org.apache.log4j.Logger;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.lucy.g3.configtool.subsys.realtime.formatter.PointValueFormatter;
import com.lucy.g3.rtu.comms.service.realtime.points.IPointData;
import com.lucy.g3.rtu.comms.service.realtime.points.ProtocolPointData;
import com.lucy.g3.rtu.comms.service.realtime.points.IPointData.PointStatus;

/**
 * Adapt a list of protocol points to table model which presents the real time
 * information in columns.
 */
public final class RTProtocolPointsTableModel extends AbstractTableAdapter<IPointData>
    implements RTPointsTableModel {

  // @formatter:off
  public static final int COLUMN_GROUPID       = 0;
  public static final int COLUMN_PROTOCOL      = 1;
  public static final int COLUMN_TYPE          = 2;
  public static final int COLUMN_DESCRIPTION   = 3;
  public static final int COLUMN_VALUE         = 4;
  public static final int COLUMN_ONLINE        = 5;
  public static final int COLUMN_FILTER        = 6;
  public static final int COLUMN_OVERFLOW      = 7;
  public static final int COLUMN_CHATTER       = 8;
  public static final int COLUMN_MAP_POINT     = 9;
  public static final int COLUMN_LAST_UPDATE   = 10;
  // @formatter:on

  private Logger log = Logger.getLogger(RTProtocolPointsTableModel.class);

  private static final String[] COLUMN_NAMES = {
      "ID", "Protocol", "Type", "Description", "Value", "Online", "Filtered", "Out of Range", "Chatter",
      "Virtual Point", "Last Update"
  };

  private final PointValueFormatter format = PointValueFormatter.getFormatter();

  private boolean showUnit;


  RTProtocolPointsTableModel(ListModel<IPointData> listModel) {
    super(listModel, COLUMN_NAMES);
  }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    IPointData vpdata = getRow(rowIndex);
    if (vpdata == null) {
      log.fatal("PointData must not be null");
      return null;
    }

    ProtocolPointData ppdata = null;
    if (vpdata instanceof ProtocolPointData) {
      ppdata = (ProtocolPointData) vpdata;
    }
    if (ppdata == null) {
      log.fatal("PointData is not ProtocolPointData");
      return null;
    }

    switch (columnIndex) {
    case COLUMN_GROUPID:
      return ppdata.getProtocolPointID();
    case COLUMN_PROTOCOL:
      return ppdata.getProtocolName();
    case COLUMN_TYPE:
      return ppdata.getProtocolPointType();

    case COLUMN_VALUE:
      return format.format(vpdata, vpdata.getValue(), showUnit);

    case COLUMN_CHATTER:
      return vpdata == null ? null : vpdata.getStatus(PointStatus.CHATTER);

    case COLUMN_FILTER:
      return vpdata == null ? null : vpdata.getStatus(PointStatus.FILTER);

    case COLUMN_OVERFLOW:
      return vpdata == null ? null : vpdata.getStatus(PointStatus.OVERFLOW);

    case COLUMN_ONLINE:
      return vpdata == null ? null : vpdata.getStatus(PointStatus.ONLINE) == Boolean.TRUE ? "Online" : "Offline";

    case COLUMN_MAP_POINT:
      return ppdata.getGroupIDString() + ppdata.getMappedPointDescription();

    case COLUMN_DESCRIPTION:
      return ppdata.getDescription();

    case COLUMN_LAST_UPDATE:
      return vpdata.getTimestamp();

    default:
      assert false : "Should not reach here";
      return PointValueFormatter.INVALID_VALUE;
    }
  }

  @Override
  public IPointData getPointData(int row) {
    if (row < getRowCount() && row >= 0) {
      return getRow(row);
    }
    return null;
  }

  @Override
  public int[] getRegFilterColumns() {
    return new int[] { COLUMN_DESCRIPTION };
  }

  @Override
  public Class<?> getColumnClass(int columnIndex) {
    switch (columnIndex) {
    case COLUMN_FILTER:
    case COLUMN_OVERFLOW:
    case COLUMN_CHATTER:
      return Boolean.class;
    case COLUMN_LAST_UPDATE:
      return Date.class;
    case COLUMN_GROUPID:
      return Long.class;
    default:
      return super.getColumnClass(columnIndex);
    }
  }

  @Override
  public boolean shouldHightlight(int rowIndex, int columnIndex) {
    IPointData point = getPointData(rowIndex);

    if (point == null) {
      return false;
    }

    switch (columnIndex) {
    case COLUMN_VALUE:
      return point.isValueChanged();
    case COLUMN_CHATTER:
      return point.isStatusChanged(PointStatus.CHATTER);
    case COLUMN_FILTER:
      return point.isStatusChanged(PointStatus.FILTER);
    case COLUMN_ONLINE:
      return point.isStatusChanged(PointStatus.ONLINE);
    case COLUMN_OVERFLOW:
      return point.isStatusChanged(PointStatus.OVERFLOW);
    default:
      return false;
    }
  }


  @Override
  public void clearChangeHistory() {
    for (int size = getRowCount(), i = 0; i < size; i++) {
      IPointData vp = getPointData(i);
      if (vp != null) {
        vp.resetValueChanged();
      }
    }

  }

}
