/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Module Protocol Switch Controller library header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   27/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _MODULEPROTOCOLAUXSWITCH_INCLUDED
#define _MODULEPROTOCOLAUXSWITCH_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef struct AuxSwitchConfigDef
{
	/*! Enabled indicator    */
	lu_bool_t		AuxSwitchEnabled;

	lu_uint8_t 		polarity;

	lu_int16_t 		pulseLengthMS;

}AuxSwitchConfigStr;

typedef struct AuxSwitchOperateDef
{
	/*! Channel ID */
	lu_uint8_t		channel;

	/*! Supply time-out duration in seconds */
	lu_uint8_t		SwitchDelay;

	/*! Local or remote operation indicator */
	lu_bool_t		local;
} AuxSwitchOperateStr;

typedef struct AuxSwitchSelectDef
{
	/*! Channel ID */
	lu_uint8_t		channel;

	/*! Time out for operate in seconds */
	lu_uint8_t		SelectTimeout;

	/*! Local or remote operation indicator */
	lu_bool_t		local;
} AuxSwitchSelectStr;

typedef struct AuxSwitchCancelDef
{
	/*! Channel ID */
	lu_uint8_t		channel;

	/*! Local or remote operation indicator */
	lu_bool_t		local;
} AuxSwitchCancelStr;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


#endif /* _MODULEPROTOCOLAUXSWITCH_INCLUDED */

/*
 *********************** End of file ******************************************
 */
