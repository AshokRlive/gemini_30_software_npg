cmake_minimum_required(VERSION 2.8)

# Set debug port
set(DBG_UART_PORT 3)

# Use CodeSourcery GCC as cross-compiler
set(CMAKE_TOOLCHAIN_FILE "${CMAKE_CURRENT_SOURCE_DIR}/../../../commom/cmake/CS_toolchain_arm.cmake")

# Set the name of the project. The project name will be used as the name of
# the final executable
project (IOMAppBootProgNXP)

# Set project-wide variables
set(CMAKE_G3_PATH_ROOT "${CMAKE_CURRENT_SOURCE_DIR}/../../../../../")
set(CMAKE_G3_PATH_COMMON_LIBRARY "${CMAKE_G3_PATH_ROOT}/common/library/")
set(CMAKE_G3_PATH_COMMON_INCLUDE "${CMAKE_G3_PATH_ROOT}/common/include/")

# Import common global variables
include(../../../commom/cmake/global_variables.cmake)

# Set CodeSourcery default flags
include(../../../commom/cmake/CS_NXP_app_flags.cmake)

# Include Doxygen macro
include(../../../commom/cmake/UseDoxygen.cmake)

# Include generated header
include(../../../commom/cmake/Generated_header.cmake)

# Set CPU type (must be uppercase)
# set_cpu_type(LM3S2965)

# Set the path to the Slave Board specific implementation for library dependancies
set(SLAVEBOARD_PATH "${CMAKE_CURRENT_SOURCE_DIR}")    

# Add linker script 
add_linker_script(${CMAKE_CURRENT_SOURCE_DIR}/../../../commom/linkerScript/G3_NXP1768-Application-rom.ld)
add_linker_script(${CMAKE_CURRENT_SOURCE_DIR}/../../../commom/library/systemUtils/src/systemUtils.ld)
add_linker_script(${CMAKE_CURRENT_SOURCE_DIR}/../../../commom/library/CANProtocolFraming/NXP/src/CANProtocolFraming.ld)
add_linker_script(${CMAKE_CURRENT_SOURCE_DIR}/../../../commom/library/CANProtocolCodec/NXP/src/CANProtocolCodec.ld)

# Add custom linker scripter (if necessary)
# add_linker_script (custom_linker_script.ld)

# Include all the library used by the project. The first argument is the
# library relative path. Te second argument is where the Makefiles and
# binaries will be saved. This path is relative to the directory where the
# cmake is called. This allows a full out-of-source build.
add_subdirectory(../../../commom/library/CANProtocolFraming/NXP/src/ common/library/CANProtocolFraming/)
add_subdirectory(../../../commom/library/CANProtocolCodec/NXP/src/ common/library/CANProtocolCodec/)
add_subdirectory(../../../commom/library/systemUtils/src/ common/library/systemUtils/)
add_subdirectory(../../../commom/library/IOManager/NXP/src/ common/library/IOManager/)
add_subdirectory(../../../commom/library/StatusManagerBase/src/ common/library/StatusManager/)
add_subdirectory(../../../commom/library/Calibration/src/ commom/library/Calibration/)
add_subdirectory(../../../commom/library/NVRam/src/ common/library/NVRam/)
add_subdirectory(${CMAKE_G3_PATH_COMMON_LIBRARY}/LinearInterpolation/src/ common/library/LinearInterpolation/)
add_subdirectory(../../../commom/library/Bootloader/src/ commom/library/Bootloader/)
add_subdirectory(../../../commom/library/SlaveBinImage/src/ commom/library/SlaveBinImage/)
add_subdirectory(../../../../../common/library/crc32/src/ common/library/Crc32/)
add_subdirectory(../../../../../common/library/crc16/src/ common/library/Crc16/)

#add_subdirectory(../../../commom/library/FactoryTest/src/ common/library/FactoryTest/)
#add_subdirectory(../../../commom/library/BatteryCharger/ChargerFSM/ common/library/BatteryCharger/)
#add_subdirectory(../../../commom/library/SupplyController/src/ common/library/SupplyController/)
#add_subdirectory(../../../commom/library/FanController/src/ common/library/FanController/)

add_subdirectory(../../../thirdParty/LPC1700CMSIS/Drivers/source/ thirdParty/LPC1700CMSIS/Drivers/source/)
add_subdirectory(../../../thirdParty/LPC1700CMSIS/IEC60335_B_libs/source  thirdParty/LPC1700CMSIS/IEC60335_B_libs/source/)

#add_subdirectory(./CANProtocol/src CANProtocol/src)

# List all the include search path needed by the executable (relative path)
include_directories(../../../../../common/include/) # global include
include_directories(../../../../../common/library/crc32/include/) 
include_directories(../../../../../common/library/crc16/include/) 
include_directories(../../../commom/include/) # SlaveBoards common include
include_directories(../../../commom/library/CANProtocolFraming/include/)
include_directories(../../../commom/library/CANProtocolCodec/include/)
include_directories(../../../commom/library/IOManager/include/)
include_directories(../../../commom/library/IOManager/IO/include/)
include_directories(../../../commom/library/StatusManagerBase/include/)
include_directories(../../../commom/library/systemUtils/include/)
include_directories(${CMAKE_G3_PATH_COMMON_LIBRARY}/LinearInterpolation/include/)
include_directories(../../../commom/library/Calibration/include/)
include_directories(../../../commom/library/Bootloader/include/)
include_directories(../../../commom/library/SlaveBinImage/include/)

# module component libraries
include_directories(../../../commom/library/NVRam/include/)
#include_directories(../../../commom/library/FactoryTest/include/)
#include_directories(../../../commom/library/BatteryCharger/include/)
#include_directories(../../../commom/library/SupplyController/include/)
#include_directories(../../../commom/library/FanController/include/)

# local board includes
include_directories(./CANProtocol/include/)
include_directories(./IOManager/include/)
include_directories(./StatusManager/include/)
include_directories(./Calibration/include/)
include_directories(./BoardBootloader/include/)

# LPC1700CMSIS includes
include_directories(SYSTEM "../../../thirdParty/LPC1700CMSIS/Drivers/include/")
include_directories(SYSTEM "../../../thirdParty/LPC1700CMSIS/IEC60355_B_libs/include/")
include_directories(SYSTEM "../../../thirdParty/LPC1700CMSIS/Core/CM3/DeviceSupport/NXP/LPC17xx/")
include_directories(SYSTEM "../../../thirdParty/LPC1700CMSIS/Core/CM3/CoreSupport/")

# List all the source files needed to build the executable
add_executable(${EXE_NAME} system_LPC17xx.c IOMAppBootProgNXP.c
./CANProtocol/src/CANProtocolDecoder.c
./IOManager/src/BoardIOMap.c
./IOManager/src/BoardIOChanMap.c
./IOManager/src/BoardIO.c
./StatusManager/src/BoardStatusManager.c)

# List all the library needed by the linker)
target_link_libraries (${EXE_NAME} CANProtocolCodec)
target_link_libraries (${EXE_NAME} CANProtocolFraming)
target_link_libraries (${EXE_NAME} NVRam)
target_link_libraries (${EXE_NAME} Calibration)
target_link_libraries (${EXE_NAME} IOManager)
target_link_libraries (${EXE_NAME} StatusManager)
#target_link_libraries (${EXE_NAME} FactoryTest)
#target_link_libraries (${EXE_NAME} BatteryCharger)
#target_link_libraries (${EXE_NAME} SupplyController)
#target_link_libraries (${EXE_NAME} FanController)
target_link_libraries (${EXE_NAME} LinearInterpolation)
target_link_libraries (${EXE_NAME} Bootloader)
target_link_libraries (${EXE_NAME} SlaveBinImage)
target_link_libraries (${EXE_NAME} Crc32)
target_link_libraries (${EXE_NAME} Crc16)
target_link_libraries (${EXE_NAME} systemUtils)
target_link_libraries (${EXE_NAME} lpc17xxlib)

# Generate binary file from executable file
generate_bin()

# Generate Doxygen documentation
gen_doxygen("main" "")

