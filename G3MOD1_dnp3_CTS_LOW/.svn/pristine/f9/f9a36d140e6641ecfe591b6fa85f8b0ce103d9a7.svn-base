/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN Output Digital channel module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   05/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_9D2F86BF_24E1_482e_8FB1_B04982BA992A__INCLUDED_)
#define EA_9D2F86BF_24E1_482e_8FB1_B04982BA992A__INCLUDED_


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
//#include "DigitalCANChannel.h"
#include "OutputCANChannel.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

struct OutputDigitalCANChannelConf
{
public:
    lu_bool_t enable;
    lu_uint16_t pulseLengthMs;
public:
    OutputDigitalCANChannelConf() : enable(0),
                                    pulseLengthMs(0)
    {};
};

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
class OutputDigitalCANChannel : public OutputCANChannel
{

public:
    OutputDigitalCANChannel(lu_uint16_t ID = 0, CANIOModule *board = NULL);
    virtual ~OutputDigitalCANChannel();

    /* === Inherited from CANChannel === */
    virtual IOM_ERROR select(const lu_uint8_t selectTimeout, const lu_bool_t local);
    virtual IOM_ERROR operate(const lu_bool_t value, const lu_bool_t local, const lu_uint8_t delay);
    virtual IOM_ERROR cancel(const lu_bool_t local);

    /**
     * \brief Configure the channel
     *
     * The configuration is stored locally.
     *
     * \param conf Reference to the channel configuration
     *
     * \return error Code
     */
    void setConfig(OutputDigitalCANChannelConf &conf)
    {this->conf = conf;}

    /**
     * \brief Get channel configuration
     *
     * \param configuration where the configuration is saved
     *
     * \return error Code
     */
    IOM_ERROR getConfiguration(OutputDigitalCANChannelConf &configuration);

protected:
    /* === Inherited from CANChannel === */
    virtual IOM_ERROR configure();

private:
    OutputDigitalCANChannelConf conf;
};


#endif // !defined(EA_9D2F86BF_24E1_482e_8FB1_B04982BA992A__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
