/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:XMLParser.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   9 Jul 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef XMLPARSER_H_
#define XMLPARSER_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdexcept>
#include <string>
#include <sstream>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"

#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>

#include "Altova/Altova.h"
#include "Altova/SchemaTypes.h"
#include "Altova/AltovaException.h"

#include "AltovaXML/XmlException.h"
#include "AltovaXML/Node.h"
#include "g3schema/g3schema.h"

using namespace g3schema;
using namespace altova;

#include "ConfigurationManagerError.h"
#include "versions.h" //for EnumValueFromIndexFuncPtr definition

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * IMPORTANT NOTE:
 * In the present class and associated functions, we are using C++ EXCEPTIONS for
 * two intended purposes: FLOW CONTROL and data storage.
 * The exceptions in flow control allows to avoid hundreds of if(parse()!=error) ...
 * that clutter the parser code. It is cleaner to use a try block with all the parser
 * functions needed to be used and stop parsing if an exception occurs.
 * The data storage allows to store the error code and the message of what was wrong 
 * in the parsing procedure at the parsing time, catching later the exception and 
 * showing afterwards the message stored in the exception object.
 * We realise that this is not the fully intended use of the exceptions; but
 * unlike Java, exceptions in C++ are poorly handled and difficult to manage them
 * properly - especially when mixing several types of exceptions.
 * For that reason, please limit the usage of this exception to the parsing procedure
 * only, and NEVER LET AN EXCEPTION UNHANDLED because this might terminate the program
 * on the spot.
 * For usage, take in mind that non-mandatory fields or attributes might throw an
 * exception as well, and it should be catched and handled properly - i.e. not 
 * terminating the parsing of the rest of the fields.
 */

/**
 * \brief Parser Exception class
 */
class ParserException : public std::exception
{
public:
    /**
     * \brief Custom constructor
     *
     * \param exceptionCode Error code associated to the exception.
     * \param message Message of the exception.
     */
    ParserException(const CONFMGR_ERROR exceptionCode, const lu_char_t* message) :
        code(exceptionCode),
        msg(message)
    {};

    virtual ~ParserException() throw() {};

    virtual const char* what() const throw ()
    {
        return msg.c_str();
    }

    virtual CONFMGR_ERROR getID() const throw ()
    {
        return code;
    }

private:
    CONFMGR_ERROR code;
    std::string msg;
};


/**
 * \brief Verify if an Element field from XML config file is present.
 *
 * \param xmlElement XML Element field structure to parse
 *
 * \throw Exception when XML Element field was not found
 *
 * \example Call it using:
 *    try
 *    {
 *        checkXMLElement(xmlElement);
 *    }
 *    catch(const ParserException& e)
 *    {
 *        ...
 *    }
 */
template <typename XMLElement>
void checkXMLElement(XMLElement xmlElement) throw(ParserException)
{
    if(!xmlElement.exists())
    {
        std::stringstream ss;
        ss << "'" << xmlElement.info().GetLocalName() << "' XML element doesn't exist";
        throw ParserException(CONFMGR_ERROR_CONFIG, ss.str().c_str());
    }
}


/**
 * \brief Parse an Element field from XML config file, obtaining the first one.
 *
 * \param xmlElement XML Element field structure to parse
 *
 * \return XML Element field content from the FIRST Element field
 *
 * \throw Exception when XML Element field was not found
 *
 * \example Call it using:
 *    try
 *    {
 *        s.member = getXMLElement<SMemberType>(xmlElement);
 *    }
 *    catch(const ParserException& e)
 *    {
 *        ...
 *    }
 */
template <typename ElementType, typename XMLElement>
ElementType getXMLElement(XMLElement xmlElement) throw(ParserException)
{
    checkXMLElement(xmlElement);
    return static_cast<ElementType>(xmlElement.first());
}


/**
 * \brief Verify and get an optional Element field from XML config file, obtaining the first one.
 *
 * \param xmlElement XML Element field structure to parse
 * \param element Where to store the obtained XML element
 *
 * \example Call it using:
 *    getXMLOptElement(xmlElem.attr, value);
 */
template <typename ElementType, typename XMLElement>
void getXMLOptElement(XMLElement xmlElement, ElementType& element)
{
    if(xmlElement.exists())
    {
        element = static_cast<ElementType>(xmlElement.first());
    }
}


/**
 * \brief Parse an attribute from XML config file, storing it.
 *
 * \param xmlAttr XML attribute structure to parse
 *
 * \return XML attribute content
 *
 * \throw Exception when attribute was not found
 *
 * \example Call it using:
 *    try
 *    {
 *        s.member = getXMLAttr(xmlElem.attr);
 *    }
 *    catch(const ParserException& e)
 *    {
 *        ...
 *    }
 */
template <typename XMLAttr>
static XMLAttr getXMLAttr(XMLAttr xmlAttr) throw(ParserException)
{
    if(!xmlAttr.exists())
    {
        std::stringstream ss;
        ss << "'" << xmlAttr.info().GetLocalName() << "' XML attribute doesn't exist";
        throw ParserException(CONFMGR_ERROR_CONFIG, ss.str().c_str());
    }
    return xmlAttr;
}


/**
 * \brief Try to get optional attribute value.
 *
 * If the attribute doesn't exist, it does nothing, leaving the provided storage
 * unchanged.
 *
 * \param xmlAttr XML attribute to parse
 * \param config Where to store the attribute value, if is present.
 *
 * \example Call it using:
 *    getXMLOptAttr(xmlElem.attr, value);
 */
template <typename Config, typename XMLAttr>
void getXMLOptAttr(XMLAttr xmlAttr, Config& config)
{
    if(xmlAttr.exists())
    {
        config = xmlAttr;
    }
}


/**
 * \brief Try to get optional attribute value, setting default value if not.
 *
 * If the attribute doesn't exist, it uses the provided default value.
 *
 * \param xmlAttr XML attribute to parse
 * \param config Where to store the attribute value, if is present.
 * \param defaultValue Default value to store in case the optional attribute is absent
 *
 * \example Call it using:
 *    getXMLOptAttr(xmlElem.attr, value, 100);
 */
template <typename Config, typename XMLAttr, typename ConfigType>
void getXMLOptAttr(XMLAttr xmlAttr, Config& config, const ConfigType defaultValue)
{
    if(xmlAttr.exists())
    {
        config = xmlAttr;
    }
    else
    {
        config = defaultValue;
    }
}


/**
 * \brief Parse an enumerated attribute from XML config and return its Enum value.
 *
 * This function takes a given XML attribute and parses it in, applying a given
 * conversion function (from generated header files) which will convert the
 * enumerated attribute index to the appropriate value.
 *
 * \param xmlAttr XML attribute structure to parse.
 * \param valueFromIndexPtr function pointer to the Enum function to apply.
 *
 * \return Enum value for the parsed XML attribute
 *
 * \throw  Exception when valueFromIndexPtr returns false (enum index is invalid)
 *
 * \example Being VALUETYPE an Enum generated from an XML enumfile, and having
 *          the VALUETYPE_VALUEfromINDEX() function already generated, call it
 *          using:
 *    try
 *    {
 *        VALUETYPE value = getXMLAttrEnum<VALUETYPE>(xmlElem.attr, VALUETYPE_VALUEfromINDEX);
 *    }
 *    catch(const ParserException& e)
 *    {
 *        ...
 *    }
 */
template <typename Type,typename XMLAttr>
Type getXMLAttrEnum(XMLAttr xmlAttr, const EnumValueFromIndexFuncPtr valueFromIndexPtr) throw(ParserException)
{
    getXMLAttr(xmlAttr);    //Check if exists
    lu_int32_t value;
    if(valueFromIndexPtr(xmlAttr.GetEnumerationValue(), &value) == LU_FALSE)
    {
        std::stringstream ss;
        ss << "'" << xmlAttr.info().GetLocalName() << "' XML attribute Enum is invalid";
        throw ParserException(CONFMGR_ERROR_CONFIG, ss.str().c_str());
    }
    return static_cast<Type>(value);
}


/**
 * \brief Parse an optional enumerated attribute from XML config.
 *
 * This function takes a given XML attribute and parses it in, applying a given
 * conversion function (from generated header files) which will convert the
 * enumerated attribute index to the appropriate value.
 *
 * \param xmlAttr XML attribute structure to parse.
 * \param valueFromIndexPtr function pointer to the Enum function to apply.
 * \param config Where to store the value, if found.
 *
 * \return Enum value for the parsed XML attribute
 *
 * \throw  Exception when valueFromIndexPtr returns false (enum index is invalid)
 */
template <typename Config, typename XMLAttr>
void getXMLOptAttrEnum(XMLAttr xmlAttr, const EnumValueFromIndexFuncPtr valueFromIndexPtr, Config& config) throw(ParserException)
{
    if(xmlAttr.exists())
    {
        lu_int32_t value;
        if(valueFromIndexPtr(xmlAttr.GetEnumerationValue(), &value) == LU_FALSE)
        {
            std::stringstream ss;
            ss << "'" << xmlAttr.info().GetLocalName() << "' XML attribute Enum is invalid";
            throw ParserException(CONFMGR_ERROR_CONFIG, ss.str().c_str());
        }
        config = static_cast<Config>(value);
    }
}


/**
 * \brief Checks that an XML element array is at least the expected size
 *
 * Since the XMLSpy overloaded [] operator does NOT throw an exception (!),
 * please use this function to check if an XML index exists before accessing to
 * it.
 * ===================================================================
 * PLEASE USE THIS FUNCTION BEFORE ANY ACCESS TO AN XML ARRAY!!!!!!!!!
 * ===================================================================
 *
 * \example checkXMLArraySize(points.dbinary, 32, "Double Binaries size mismatch");
 *          ...
 *          //Now is safe to access elements [0..31]
 *          db[0] = points.dbinary[0];      //Safe
 *          db[1] = points.dbinary[100];    //Danger! this breaks the program!!
 *
 * \param element XML element array to check.
 * \param expectedSize Minimum size expected for the array.
 * \param Message to add to the ParserException thrown.
 *
 * \throw Exception when XML element has less elements than expected.
 */
template <typename XMLElement>
static void checkXMLArraySize(XMLElement element,
                              const lu_uint32_t expectedSize,
                              const std::string& errorMessage) throw(ParserException)
{
    if(expectedSize==0 && !element.exists())
        return;

    if(element.exists())
    {
        if(element.count() >= expectedSize)
        {
            return;
        }
    }
    throw ParserException(CONFMGR_ERROR_PARAM, errorMessage.c_str());
}


#endif /* XMLPARSER_H_ */

/*
 *********************** End of file ******************************************
 */
