/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.compiler;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;


/**
 * The Interface of compiler.
 */
public interface ICompiler {
  /**
   * 
   * @param sourceFile the file to be compiled.
   * @param outputFile the file to be generated.
   * @return  exit code.
   * @throws IOException
   * @throws InterruptedException
   */
  int compile(String sourceFile, String outputFile) throws IOException, InterruptedException ;
  
  
  class Resources {
    public static final String COMMON_C_FILENAME = "AutomationSchemeLib.c";
    
    private static String[] COMMON_FILES = {
        "automation/common/AutomationScheme.h",
        "automation/common/AutomationSchemeLib.h",
        "automation/common/"+COMMON_C_FILENAME,
        "automation/common/lu_types.h",
        "automation/common/memory.h",
    };
    
    private Resources() {}
    
    public static void copyTo(String outputDir) throws IOException{
      InputStream is;
      for (int i = 0; i < COMMON_FILES.length; i++) {
        is = ICompiler.class.getClassLoader().getResourceAsStream(COMMON_FILES[i]);
        if(is == null)
          throw new IOException(COMMON_FILES[i]+ " not found!");
        FileUtils.copyInputStreamToFile(is, new File(outputDir, FilenameUtils.getName(COMMON_FILES[i])));
        is.close();
      }
    }
  }
}

