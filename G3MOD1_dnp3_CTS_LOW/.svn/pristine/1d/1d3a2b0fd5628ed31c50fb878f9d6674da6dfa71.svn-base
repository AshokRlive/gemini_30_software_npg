/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port;

import com.jgoodies.common.bean.ObservableBean2;

/**
 * The interface of physical port.
 */
public interface Port extends ObservableBean2 {

  String PROPERTY_PORT_NAME = "portName";

  String PROPERTY_ENABLED = "enabled";


  /**
   * Gets unique port name. e.g. "RS232-A","ETH-0".
   *
   * @return
   */
  String getPortName();

  /**
   * Gets the name used by GUI.
   *
   * @return
   */
  String getDisplayName();

  /**
   * Checks if this port is enabled.
   */
  boolean isEnabled();

  /** Enable/Disable this port. */
  void setEnabled(boolean enabled);
}
