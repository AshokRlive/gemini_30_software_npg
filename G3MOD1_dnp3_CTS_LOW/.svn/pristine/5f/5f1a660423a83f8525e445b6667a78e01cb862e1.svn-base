/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.log;

import java.nio.ByteBuffer;

import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.RequestPayload;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_RUNNINGAPP;

/**
 * The Class PL_Send_LogLevels.
 */
class PL_Send_LogLevels extends RequestPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_C_LogLevel;
  private final Byte subsystem;
  private final Byte loglevel;


  PL_Send_LogLevels(CTH_RUNNINGAPP mode,
      String subsys,
      String loglevel) {
    super(PAYLOAD_SIZE);
    this.subsystem = LogEntry.subSystemDescription2Value(subsys, mode);
    this.loglevel = LogEntry.logLevelDescription2Value(loglevel, mode);

    if (this.subsystem == null) {
      throw new IllegalArgumentException("Cannot parse logging subsystem: " + subsys);
    }
    if (this.loglevel == null) {
      throw new IllegalArgumentException("Cannot parse logging level: " + loglevel);
    }
  }

  @Override
  public byte[] toBytes() {
    ByteBuffer buf = createByteBuffer(PAYLOAD_SIZE);
    buf.put(subsystem);
    buf.put(loglevel);

    return buf.array();
  }
}