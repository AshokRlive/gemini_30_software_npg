/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s101sesn.h
 * description: Slave IEC 60870-5-101 session
 */
#ifndef S101SESN_DEFINED
#define S101SESN_DEFINED

#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/utils/tmwsesn.h"
#include "tmwscl/utils/tmwchnl.h"
#include "tmwscl/i870/s14sesn.h"

/* S101 Session Configuration Info */
typedef struct S101SessionConfigStruct {

  /* Determine whether the session is active or inactive. An inactive 
   * session will not transmit or receive frames.
   */
  TMWTYPES_BOOL active;

  /* The IEC 60870-5-101 specification added dayOfWeek in Ed 2.
   * Some devices cannot handle receiving it. Setting this to TMWDEFS_FALSE
   * will prevent copying dayOfWeek into a message
   */
  TMWTYPES_BOOL useDayOfWeek;

  TMWTYPES_USHORT linkAddress;
  TMWTYPES_UCHAR  asduAddrSize;
  TMWTYPES_UCHAR  infoObjAddrSize;
  TMWTYPES_UCHAR  cotSize;

  /* Maximum size of a Application Specific Data Unit.
   * Maximum size allowed by IEC60870-5-101 is 252.
   * Must be <= I870CHNL_TX_DATA_BUFFER_MAX.
   */
  TMWTYPES_UCHAR                   maxASDUSize;

  /* Bitmask to indicate special diagnostic formatting */
  TMWTYPES_UCHAR diagFormat;

  /* For unbalanced slaves this parameter specifies how long to allow
   * between received frames before declaring this session offline.
   * Unbalanced slaves will never transmit a message that has not been
   * requested from the remote device, hence unbalanced slaves have no
   * way of determining if the remote device is online or not. In most
   * systems however an unbalanced slave will be polled for data at regular
   * intervals. This parameters uses the lack of a data poll to determine
   * that the remote device is offline. Set this parameter to 0 to disable
   * this feature. This parameter is ignored for balanced links and on
   * unbalanced masters.
   */
  TMWTYPES_MILLISECONDS            maxPollDelay;

  /* User registered statistics callback function and parameter */
  TMWSESN_STAT_CALLBACK           pStatCallback;
  void                           *pStatCallbackParam;
 
  /* User registered process request function and parameter allows user to  
   * process ASDU typeIds not supported by the SCL
   */
  S14SESN_PROCESS_REQ_CALLBACK    pProcessRequestCallback;
  void                           *pProcessRequestParam;

  /* User registered build response function and parameter allows user to   
   * send response to ASDU typeIds not supported by the SCL
   */
  S14SESN_BUILD_RESP_CALLBACK     pBuildResponseCallback;
  void                           *pBuildResponseParam;
  
#if S14DATA_SUPPORT_CICNAWAIT
  /* Special configuration to prevent the slave from sending cyclic data before
   * the first CICNA is complete. This behavior is not suggested by the 101 spec.
   * This may be required for some masters, but otherwise should be set to false. 
   * To handle the case where a CICNA request is not received in a reasonable time, 
   * when the timer configured by S101Sector property CyclicFirstPeriod expires, 
   * cyclic data transmission will begin anyway. 
   */
  TMWTYPES_BOOL                   cyclicWaitCICNAComplete;
#endif

} S101SESN_CONFIG;

/* DEPRECATED SHOULD USE s101sesn_getSessionConfig and 
 *  s101sesn_setSessionConfig
 */
#define S101SESN_CONFIG_LINK_ADDRESS        0x00000001L
#define S101SESN_CONFIG_ACTIVE              0x00000002L


/* Include IEC 60870-5-101 slave 'private' session info */
#include "tmwscl/i870/s101sesp.h"

#ifdef __cplusplus
extern "C" {
#endif

  /* function: s101sesn_initConfig 
   * purpose: Initialize 101 slave session configuration data structure.
   *  This routine should be called to initialize all the members of the
   *  data structure. Then the user should modify the data members they
   *  need in user code. Then pass the resulting structure to 
   *  s101sesn_openSession.
   * arguments:
   *  pConfig - pointer to configuration data structure to initialize
   * returns:
   *  void
   */
  TMWDEFS_SCL_API void TMWDEFS_GLOBAL s101sesn_initConfig(
    S101SESN_CONFIG *pConfig);

  /* function: s101sesn_openSession 
   * purpose: Open a 101 slave session
   * arguments:
   *  pChannel - channel to open session on
   *  pConfig - 101 slave configuration data structure
   * returns:
   *  Pointer to new session or TMWDEFS_NULL.
   */
  TMWDEFS_SCL_API TMWSESN * TMWDEFS_GLOBAL s101sesn_openSession(
    TMWCHNL *pChannel,
    const S101SESN_CONFIG *pConfig);

  /* function: s101sesn_getSessionConfig 
   * purpose: Get current configuration from a currently open session
   * arguments:
   *  pSession - session to get configuration from
   *  pConfig - 101 slave configuration data structure to be filled in
   * returns:
   *  TMWDEFS_TRUE if successful
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL s101sesn_getSessionConfig(
    TMWSESN *pSession,
    S101SESN_CONFIG *pConfig);

  /* function: s101sesn_setSessionConfig 
   * purpose: Modify a currently open session
   *  NOTE: normally s101sesn_getSessionConfig() will be called
   *   to get the current config, some values will be changed 
   *   and this function will be called to set the values.
   * arguments:
   *  pSession - session to modify
   *  pConfig - 101 slave configuration data structure
   * returns:
   *  TMWDEFS_TRUE if successful
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL s101sesn_setSessionConfig(
    TMWSESN *pSession,
    const S101SESN_CONFIG *pConfig);

  /* function: s101sesn_modifySession 
   * DEPRECATED FUNCTION, SHOULD USE s101sesn_setSessionConfig
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s101sesn_modifySession(
    TMWSESN *pSession,
    const S101SESN_CONFIG *pConfig, 
    TMWTYPES_ULONG configMask);

  /* function: s101sesn_closeSession 
   * purpose: Close a currently open session
   * arguments:
   *  pSession - session to close
   * returns:
   *  TMWDEFS_TRUE if successfull, else TMWDEFS_FALSE
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL s101sesn_closeSession(
    TMWSESN *pSession);

#ifdef __cplusplus
}
#endif
#endif /* S101SESN_DEFINED */
