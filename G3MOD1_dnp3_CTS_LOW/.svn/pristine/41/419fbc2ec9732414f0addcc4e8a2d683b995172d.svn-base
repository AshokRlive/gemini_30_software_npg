/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.model;


/**
 * The object that implements this interface becomes deletable. 
 */
public interface IDeletable {
  /**
   * Read-only property.
   */
  String PROPERTY_DELETED = "deleted";
  
  
  boolean isDeleted();
  
  /**
   * Checks if users are allowed to delete this object. 
   * @return
   */
  boolean isAllowDelete();
  
  /**
   * Delete this item and remove it from its manager.
   */
  void delete() throws DeleteExceptoin;
  
  String getName();
}

