/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.domain.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.channel.domain.ChannelStub;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.shared.model.INode;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.impl.StdDoubleBinaryPointImpl;

/**
 * The Class StdDoubleBinaryPointTest.
 */
public class StdDoubleBinaryPointTest {

  private StdDoubleBinaryPoint fixture;
  private IChannel ch0;
  private IChannel ch1;


  @Before
  public void setUp() throws Exception {
    fixture = new StdDoubleBinaryPointImpl(0, null);
    ch0 = new ChannelStub(ChannelType.DIGITAL_INPUT, null, "ch0");
    ch1 = new ChannelStub(ChannelType.DIGITAL_INPUT, null, "ch1");
  }

  @After
  public void tearDown() throws Exception {
  }

  private void assertNoConnection(String name, INode node) {
    assertFalse("expected \"" + name + "\" not connected to any target", node.isConnectedToAnyTarget());
    assertFalse("expected \"" + name + "\" not connected to any source", node.isConnectedToAnySource());
  }

  @Test
  public void testSetChannel() {
    assertNoConnection("ch0", ch0);
    assertNoConnection("fixture", fixture);
    assertNull(fixture.getChannel());

    fixture.setChannel(ch0);

    assertTrue("expected ch0, but:" + fixture.getChannel(), fixture.getChannel() == ch0);
    assertTrue(ch0.isConnectedToAnyTarget());
    assertTrue(fixture.isConnectedToAnySource());

    // Delete
    ch0.delete();
    assertNoConnection("ch0", ch0);
    assertNoConnection("fixture", fixture);
  }

  @Test
  public void testSetSecondChannel() {
    assertNoConnection("ch1", ch1);
    assertNoConnection("fixture", fixture);
    assertNull(fixture.getSecondChannel());

    fixture.setSecondChannel(ch1);

    assertTrue(fixture.getSecondChannel() == ch1);
    assertTrue(ch1.isConnectedToAnyTarget());
    assertTrue(fixture.isConnectedToAnySource());

    // Delete
    ch1.delete();
    assertNoConnection("ch1", ch1);
    assertNoConnection("fixture", fixture);
  }

  @Test
  public void setBothChannel() {
    fixture.setChannel(ch0);
    fixture.setSecondChannel(ch1);
    assertTrue(ch0.isConnectedToAnyTarget());
    assertTrue(ch1.isConnectedToAnyTarget());

    assertTrue(ch0 == fixture.getChannel());
    assertTrue(ch1 == fixture.getSecondChannel());

    fixture.setChannel(null);
    assertNull(fixture.getChannel());
    assertTrue(ch1 == fixture.getSecondChannel());

    fixture.setSecondChannel(null);
    assertNull(fixture.getChannel());
    assertNull(fixture.getSecondChannel());

    assertFalse(ch0.isConnectedToAnyTarget());
    assertFalse(ch1.isConnectedToAnyTarget());
  }

  @Test
  public void deleteBothChannel() {
    fixture.setChannel(ch0);
    fixture.setSecondChannel(ch1);

    ch0.delete();
    assertNull(fixture.getChannel());
    assertNull(fixture.getSource());
    assertTrue(ch1 == fixture.getSecondChannel());
    assertTrue(fixture.isConnectedToAnySource());
    assertFalse(ch0.isConnectedToAnyTarget());
    assertFalse(ch0.isConnectedToAnySource());
    assertTrue(ch1.isConnectedToAnyTarget());

    ch1.delete();
    assertNull(fixture.getChannel());
    assertNull(fixture.getSource());
    assertNull(fixture.getSecondChannel());
    assertFalse(fixture.isConnectedToAnySource());
    assertFalse(ch0.isConnectedToAnyTarget());
    assertFalse(ch0.isConnectedToAnySource());
    assertFalse(ch1.isConnectedToAnyTarget());

  }

}
