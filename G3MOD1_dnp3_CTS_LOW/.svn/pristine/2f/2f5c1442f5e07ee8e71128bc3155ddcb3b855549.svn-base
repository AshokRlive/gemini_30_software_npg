/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14mepta.h
 * description: IEC 60870-5-101/104 slave MEPTA/MEPTD support
 *
 * This file supports ASDU types 
 *  17/38 Event of protection equipment with 24/56 bit time tag
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14mepta.h"
#include "tmwscl/i870/s14mem.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14evnt.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/i870util.h"
#include "tmwscl/utils/tmwtarg.h"

#if S14DATA_SUPPORT_MEPTA

/* Forward declaration */
static void TMWDEFS_LOCAL _initEventDesc(
  TMWSCTR *pSector,
  S14EVNT_DESC *pDesc);

/* function: _internalAddEvent
 * purpose: Internal function called by SCL to add an event to the queue.
 *  This will not tell the link layer to ask the application for a message
 *  to send. 
 * arguments:
 *  pSector - pointer to sector structure returned by s14sctr_openSector
 *  cot - cause of transmission
 *  ioa - Information object address
 *  sep -
 *  elapsedTime -
 *  pTimeStamp - pointer to time structures
 * returns:
 *  void *
 */
static void * TMWDEFS_LOCAL _internalAddEvent(
  TMWSCTR *pSector, 
  TMWTYPES_ULONG ioa, 
  TMWTYPES_UCHAR sep,
  TMWTYPES_USHORT elapsedTime,
  TMWDTIME *pTimeStamp)
{
  S14EVNT_DESC desc;
  S14MEPTA_EVENT *pEvent;

  _initEventDesc(pSector, &desc);

  pEvent = (S14MEPTA_EVENT *)s14evnt_addEvent(pSector, I14DEF_COT_SPONTANEOUS, 
    ioa, sep, pTimeStamp, &desc, TMWDEFS_NULL);

  if(pEvent != TMWDEFS_NULL)
  {
    pEvent->elapsedTime = elapsedTime;
  }
  return(pEvent);
}

/* function: _changedFunc 
 * purpose: Determine if the specified point has changed and if so
 *  get the current values and add an event to the queue
 * arguments:
 *  pSector - identifies sector
 * returns:
 *  TMWDEFS_TRUE if an event was added
 *  TMWDEFS_FALSE otherwise
 */
static TMWTYPES_BOOL TMWDEFS_CALLBACK _changedFunc(
  TMWSCTR *pSector,
  void *pPoint, 
  TMWDTIME *pTimeStamp)
{  
  TMWTYPES_USHORT elapsedTime;
  TMWTYPES_UCHAR sep;

  if(s14data_meptaChanged(pPoint, &sep, &elapsedTime))
  { 
    if(_internalAddEvent(pSector, 
      s14data_meptaGetInfoObjAddr(pPoint), sep, elapsedTime,
        pTimeStamp) != TMWDEFS_NULL)
    {
      return(TMWDEFS_TRUE);
    }
  }
  return(TMWDEFS_FALSE);
}

/* function: _eventData 
 * purpose: Insert data from event structure into message to be sent
 * arguments:
 *  pTxData - pointer to transmit data structure
 *  pEvent - pointer to event to be put into message
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _eventData(
  TMWSESN_TX_DATA *pTxData, 
  S14EVNT *pEvent)
{
  S14MEPTA_EVENT *pMEPTAEvent = (S14MEPTA_EVENT *)pEvent;

  /* Store SEP */
  pTxData->pMsgBuf[pTxData->msgLength++] = pEvent->quality;

  /* Store elapsed Time */
  tmwtarg_store16(&pMEPTAEvent->elapsedTime, &pTxData->pMsgBuf[pTxData->msgLength]);
  pTxData->msgLength += 2;
}

/* function: _initEventDesc 
 * purpose: Initialize event descriptor
 * arguments:
 *  pSector - identifies sector
 *  pDesc - pointer to descriptor structure 
 * returns:
 *  void
 */
static void TMWDEFS_LOCAL _initEventDesc(
  TMWSCTR *pSector,
  S14EVNT_DESC *pDesc)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  pDesc->typeId = I14DEF_TYPE_MEPTA1;
  pDesc->cotSpecified = 0;
  pDesc->eventMemType = S14MEM_MEPTA_EVENT_TYPE;
  pDesc->pEventList = &p14Sector->meptaEvents;
  pDesc->eventMode = p14Sector->meptaEventMode;
  pDesc->scanEnabled = p14Sector->meptaScanEnabled;
  pDesc->maxEvents = p14Sector->meptaMaxEvents;
  pDesc->timeFormat = p14Sector->meptaTimeFormat;
  pDesc->doubleTransmission = TMWDEFS_FALSE;
  pDesc->pEventsOverflowedFlag = &p14Sector->meptaEventsOverflowed;
  pDesc->pChangedFunc = _changedFunc;
  pDesc->pGetPointFunc = s14data_meptaGetPoint;
  pDesc->pEventDataFunc = _eventData;
}

/* function: s14mepta_init */
void TMWDEFS_GLOBAL s14mepta_init(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  tmwdlist_initialize(&p14Sector->meptaEvents);
  p14Sector->meptaEventsOverflowed = TMWDEFS_FALSE;
}

/* function: s14mepta_close */
void TMWDEFS_GLOBAL s14mepta_close(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  tmwdlist_destroy(&p14Sector->meptaEvents, s14mem_free);
} 

/* function: s14mepta_addEvent */
void * TMWDEFS_GLOBAL s14mepta_addEvent(
  TMWSCTR *pSector, 
  TMWTYPES_ULONG ioa, 
  TMWTYPES_UCHAR sep,
  TMWTYPES_USHORT elapsedTime,
  TMWDTIME *pTimeStamp)
{
  void *pEvent;
  
  /* This requires a time stamp */
  if(pTimeStamp == TMWDEFS_NULL)
    return(TMWDEFS_NULL);

  TMWTARG_LOCK_SECTION(&pSector->pSession->pChannel->lock);

  pEvent = _internalAddEvent(pSector,
    ioa, sep, elapsedTime, pTimeStamp);

  if(pEvent != TMWDEFS_NULL)
  {
    /* If an event was added tell link layer we have data */ 
    s14event_linkDataReady(pSector);
  }

  TMWTARG_UNLOCK_SECTION(&pSector->pSession->pChannel->lock);
  return(pEvent);
}

/* function: s14mepta_countEvents */
TMWTYPES_USHORT TMWDEFS_GLOBAL s14mepta_countEvents(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  return(s14evnt_countEvents(pSector, &p14Sector->meptaEvents));
}

/* function: s14mepta_scanForChanges */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14mepta_scanForChanges(
  TMWSCTR *pSector)
{
  S14EVNT_DESC desc;

  _initEventDesc(pSector, &desc);

  return(s14evnt_scanForChanges(pSector, &desc));
}

/* function: s14mepta_processEvents */
S14EVNT_STATUS TMWDEFS_GLOBAL s14mepta_processEvents(
  TMWSCTR *pSector, 
  TMWDTIME *pEventTime)
{
  S14EVNT_DESC desc;
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  TMWTYPES_UCHAR typeId = I14DEF_TYPE_MEPTA1;
  if(p14Sector->meptaTimeFormat == TMWDEFS_TIME_FORMAT_24) typeId = I14DEF_TYPE_MEPTA1;
  if(p14Sector->meptaTimeFormat == TMWDEFS_TIME_FORMAT_56) typeId = I14DEF_TYPE_MEPTD1;

  _initEventDesc(pSector, &desc);
  desc.typeId = typeId;
  return(s14evnt_processEvents(pSector, &desc, 3, pEventTime, TMWDEFS_FALSE));
}

#endif /* S14DATA_SUPPORT_MEPTA */
