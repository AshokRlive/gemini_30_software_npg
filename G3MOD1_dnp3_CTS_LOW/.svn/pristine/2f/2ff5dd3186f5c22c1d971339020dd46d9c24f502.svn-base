/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.master.shared.ui;

import javax.swing.Action;
import javax.swing.JPanel;

import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocol;
import com.lucy.g3.rtu.config.protocol.master.shared.manager.MasterProtocolManager;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.shared.ui.AbstractProtocolItemManagerModel;
import com.lucy.g3.rtu.config.protocol.shared.ui.ProtocolItemManagerPanel;
import com.lucy.g3.rtu.config.protocol.shared.ui.wizard.ProtocolAddingWizards;

/**
 * Presentation model of Master Protocol manager.
 */
public final class MasterProtocolManagerModel extends AbstractProtocolItemManagerModel {

  private final MasterProtocolManager manager;

  private final SelectionInList<IMasterProtocol<?>> selectionInList;


  public MasterProtocolManagerModel(MasterProtocolManager manager) {
    this.manager = Preconditions.checkNotNull(manager, "manager is null");
    selectionInList = new SelectionInList<IMasterProtocol<?>>(manager.getItemListModel());
  }

  @Override
  public final SelectionInList<IMasterProtocol<?>> getSelectionInList() {
    return selectionInList;
  }

  @Override
  protected void addActionPerformed() {
    ProtocolAddingWizards.showAddProtocol(manager, ProtocolType.getAllMasterProtocolTypes());
  }

  @Override
  protected void removeActionPerformed() {
    IMasterProtocol<?> sel = selectionInList.getSelection();

    if (sel != null) {
      if (showConfirmRemoveDialog(sel.getProtocolName())) {
        manager.remove(sel);
      }
    }
  }

  public JPanel createPanel(Action viewProtocolAction) {
    return new ProtocolItemManagerPanel(getSelectionInList(),
        getPageActions(), viewProtocolAction);
  }

}
