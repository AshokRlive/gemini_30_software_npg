
<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="en-us" lang="en-us">
<head><meta name="description" content=""/><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><meta name="copyright" content="(C) Copyright 2005"/><meta name="DC.rights.owner" content="(C) Copyright 2005"/><meta name="DC.Type" content="topic"/><meta name="DC.Title" content="Add a comms device"/><meta name="DC.Format" content="XHTML"/><meta name="DC.Identifier" content="topic_evl_jqk_vq"/><link rel="stylesheet" type="text/css" href="../oxygen-webhelp/resources/css/commonltr.css"><!----></link><title>Add a comms device</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><link rel="stylesheet" type="text/css" href="../oxygen-webhelp/resources/css/commonltr.css"><!----></link><link rel="stylesheet" type="text/css" href="../oxygen-webhelp/resources/css/webhelp_topic.css"><!----></link><link rel="stylesheet" type="text/css" href="../oxygen-webhelp/resources/skins/skin.css"><!----></link><script type="text/javascript"><!--
          
          var prefix = "../index.html";
          
          --></script><script type="text/javascript" src="../oxygen-webhelp/resources/js/jquery-1.8.2.min.js"><!----></script><script type="text/javascript" src="../oxygen-webhelp/resources/js/jquery.cookie.js"><!----></script><script type="text/javascript" src="../oxygen-webhelp/resources/js/jquery-ui.custom.min.js"><!----></script><script type="text/javascript" charset="utf-8" src="../oxygen-webhelp/resources/js/webhelp_topic.js"><!----></script></head>
<body onload="highlightSearchTerm()" class="frmBody">
<table class="nav"><tbody><tr><td colspan="2"><div id="printlink"><a href="javascript:window.print();" title="printThisPage"/></div><div id="permalink"><a href="#" title="linkToThis"/></div></td></tr><tr><td width="75%"/><td><div class="navheader"/></td></tr></tbody></table>

  <div class="nested0" id="topic_evl_jqk_vq">
    <h1 class="topictitle1">Add a comms device</h1>

    <div>
      <ol id="topic_evl_jqk_vq__ol-xhy_cbg_wq">
        <li>On the <span class="uicontrol">Add a New Comms Device</span> page, select a <span class="uicontrol">Device
            Type</span>:<ul id="topic_evl_jqk_vq__ul-zs4_sbg_wq">
            <li><span class="uicontrol">PSTN</span> (Public Switched Telephone Network)</li>

            <li><span class="uicontrol">GPRS Modem</span> (General Packet Radio Service)</li>

            <li><span class="uicontrol">Paknet Pad</span> (Vodafone Packet Radio Service)</li>

          </ul>
</li>

        <li>In the <span class="uicontrol">Device Name</span> box, type a meaningful name for the comms
          device.</li>

        <li>Click <span class="uicontrol">Next</span>.</li>

      </ol>

      <div class="section"><h2 class="sectiontitle">Add a PSTN comms device</h2>
        
        <p>For each group of settings, follow the instructions on screen. You will find extra
          information about some of the settings in the table below.</p>

        
<div class="tablenoborder"><table cellpadding="4" cellspacing="0" summary="" id="topic_evl_jqk_vq__table-ksk_qfg_wq" frame="border" border="1" rules="all">
            
            
            <thead align="left">
              <tr>
                <th class="cellrowborder" rowspan="1" colspan="1" valign="top" id="d53e75"> Setting </th>

                <th class="cellrowborder" rowspan="1" colspan="1" valign="top" id="d53e78"> Description </th>

              </tr>

            </thead>

            <tbody>
              <tr>
                <td class="cellrowborder" colspan="1" valign="top" headers="d53e75 "><span class="uicontrol">Initialisation
                  String</span></td>

                <td class="cellrowborder" rowspan="1" colspan="1" valign="top" headers="d53e78 ">Type the string that is sent to the
                  modem to initialise it.<p>This string is sent to the modem each time that it is
                    reset.</p>
</td>

              </tr>

              <tr>
                <td class="cellrowborder" colspan="1" valign="top" headers="d53e75 "><span class="uicontrol">Primary Dialling
                  String</span></td>

                <td class="cellrowborder" rowspan="1" colspan="1" valign="top" headers="d53e78 ">Type the string that is used to
                  dial the primary SCADA.</td>

              </tr>

              <tr>
                <td class="cellrowborder" colspan="1" valign="top" headers="d53e75 "><span class="uicontrol">Secondary Dialling
                  String</span></td>

                <td class="cellrowborder" rowspan="1" colspan="1" valign="top" headers="d53e78 ">Type the string that is used to
                  dial the secondary SCADA.</td>

              </tr>

            </tbody>

          </table>
</div>

      </div>

      <div class="section"><h2 class="sectiontitle">Add a GPRS comms device</h2>
        
        <p>For each group of settings, follow the instructions on screen. You will find extra
          information about some of the settings in the table below.</p>

        
<div class="tablenoborder"><table cellpadding="4" cellspacing="0" summary="" id="topic_evl_jqk_vq__table-ols_sfg_wq" frame="border" border="1" rules="all">
            
            
            <thead align="left">
              <tr>
                <th class="cellrowborder" rowspan="1" colspan="1" valign="top" id="d53e141"> Setting </th>

                <th class="cellrowborder" rowspan="1" colspan="1" valign="top" id="d53e144"> Description </th>

              </tr>

            </thead>

            <tbody>
              <tr>
                <td class="cellrowborder" colspan="1" valign="top" headers="d53e141 "><span class="uicontrol">Access Point Name</span></td>

                <td class="cellrowborder" rowspan="3" colspan="1" valign="top" headers="d53e144 ">Type the APN host name and
                  credentials for the GPRS connection.<p>These details are supplied by the SIM
                    provider.</p>
</td>

              </tr>

              <tr>
                <td class="cellrowborder" colspan="1" valign="top" headers="d53e141 "><span class="uicontrol">User Name</span></td>

              </tr>

              <tr>
                <td class="cellrowborder" colspan="1" valign="top" headers="d53e141 "><span class="uicontrol">Password</span></td>

              </tr>

            </tbody>

          </table>
</div>

      </div>

      <div class="section"><h2 class="sectiontitle">Add a Paknet Radio-Pad comms device</h2>
        
        <p>For each group of settings, follow the instructions on screen. You will find extra
          information about some of the settings in the table below.</p>

        
<div class="tablenoborder"><table cellpadding="4" cellspacing="0" summary="" id="topic_evl_jqk_vq__table-q3t_5fg_wq" frame="border" border="1" rules="all">
            
            
            <thead align="left">
              <tr>
                <th class="cellrowborder" rowspan="1" colspan="1" valign="top" id="d53e201"> Setting </th>

                <th class="cellrowborder" rowspan="1" colspan="1" valign="top" id="d53e204"> Description </th>

              </tr>

            </thead>

            <tbody>
              <tr>
                <td class="cellrowborder" colspan="1" valign="top" headers="d53e201 "><span class="uicontrol">Main NUA</span></td>

                <td class="cellrowborder" rowspan="2" colspan="1" valign="top" headers="d53e204 ">Type the unique 14-digit Network
                  User Address (NUA) for both serial ports on the Paknet Radio-Pad.<p>The NUA of
                    each port is printed on a label on the Radio-Pad packaging box.</p>
</td>

              </tr>

              <tr>
                <td class="cellrowborder" colspan="1" valign="top" headers="d53e201 "><span class="uicontrol">Backup NUA</span></td>

              </tr>

              <tr>
                <td class="cellrowborder" colspan="1" valign="top" headers="d53e201 "><span class="uicontrol">Connection
                  Timeout</span></td>

                <td class="cellrowborder" rowspan="1" colspan="1" valign="top" headers="d53e204 ">Type the length of time in
                  milliseconds to wait for a response to a connection request.</td>

              </tr>

              <tr>
                <td class="cellrowborder" colspan="1" valign="top" headers="d53e201 "><span class="uicontrol">Regular Call
                  Interval</span></td>

                <td class="cellrowborder" rowspan="1" colspan="1" valign="top" headers="d53e204 ">Type the interval in minutes at
                  which to regularly call the SCADA.<p>To disable this setting, set the interval to
                    0 minutes.</p>
</td>

              </tr>

              <tr>
                <td class="cellrowborder" colspan="1" valign="top" headers="d53e201 "><span class="uicontrol">Modem Inactivity
                  Timeout</span></td>

                <td class="cellrowborder" rowspan="1" colspan="1" valign="top" headers="d53e204 ">If there are no communications, the
                  length of time to wait before resetting the modem.<p>Type the waiting time in
                    minutes.</p>
</td>

              </tr>

              <tr>
                <td class="cellrowborder" colspan="1" valign="top" headers="d53e201 "><span class="uicontrol">Connection Inactivity
                    Timeout</span></td>

                <td class="cellrowborder" rowspan="1" colspan="1" valign="top" headers="d53e204 ">If no communications are exchanged
                  after a new connection is made, the length of time to wait before resetting the
                    modem.<p>Type the waiting time in seconds.</p>
</td>

              </tr>

              <tr>
                <td class="cellrowborder" colspan="1" valign="top" headers="d53e201 "><span class="uicontrol">Dialling Retry
                  Delays</span></td>

                <td class="cellrowborder" rowspan="1" colspan="1" valign="top" headers="d53e204 ">A list of periods of time to wait
                  before retrying the connection to the SCADA.<p>Type a comma-separated list of
                    values in minutes.</p>
</td>

              </tr>

            </tbody>

          </table>
</div>

      </div>

    </div>

  </div>

<div class="navfooter"><!----></div>
</body>
</html>