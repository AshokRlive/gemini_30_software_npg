/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: HIHILOLOFilter.cpp 30 Jun 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/Points/src/HIHILOLOFilter.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Analogue Upper-Upper/Upper/Lower/Lower-Lower limit filter.
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 30 Jun 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   30 Jun 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "HIHILOLOFilter.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
bool HIHILOLOFilter::Config::isValid()
{
    update();   //recalculate recovery thresholds

    /* Check boundaries */
    for (lu_uint32_t i = 0; i < Limits; ++i)
    {
        if(limit[i].hysteresis < 0)
        {
            return false;
        }
        if(!limit[i].active)
        {
            continue; //skip inactive limits
        }
        if(i < Config::HalfLimit)
        {
            /* Low/LowLow limits */
            //Check hysteresis over own threshold
            if(limit[i].recovery < limit[i].threshold)
            {
                return false;
            }
            //Check hysteresis over next threshold
            for (lu_uint32_t j = i+1; j < Limits; ++j)
            {
                if(limit[j].active)
                {
                    if(limit[i].recovery >= limit[j].threshold)
                    {
                        return false;
                    }
                    break; //no need to check the rest of thresholds
                }
            }
        }
        else
        {
            /* High/HighHigh limits */
            //Check hysteresis over own threshold
            if(limit[i].recovery > limit[i].threshold)
            {
                return false;
            }
            //Check hysteresis over previous threshold
            for (lu_int32_t j = i-1; j <= 0; --j)
            {
                if(limit[j].active)
                {
                    if(limit[i].recovery <= limit[j].threshold)
                    {
                        return false;
                    }
                    break; //no need to check the rest of thresholds
                }
            }
        }
    }
    return true;
}


void HIHILOLOFilter::Config::update()
{
    for (lu_uint32_t i = 0; i < Limits; ++i)
    {
        if(i < Config::HalfLimit)
        {
            /* Low/LowLow limits */
            limit[i].recovery = limit[i].threshold + limit[i].hysteresis;
        }
        else
        {
            /* High/HighHigh limits */
            limit[i].recovery = limit[i].threshold - limit[i].hysteresis;
        }
    }
}


HIHILOLOFilter::HIHILOLOFilter(HIHILOLOFilter::Config& config) :
                                                            m_limits(config)
{}

AFILTER_RESULT HIHILOLOFilter::run(lu_float32_t value)
{
    bool eventing = false;
    if(status == AFILTER_RESULT_NA)
    {
        //Initial value
        updateRegion(value);    //Set initial region
        status = AFILTER_RESULT_OUT_LIMIT_THR;  //report first value
    }
    else
    {
        /* Check value between limits (regions) and its relationship with the
         * previous value (for hysteresis) */
        if( m_limits.limit[LOLO].active &&
            (m_region != REGION_LOLO) && (value < m_limits.limit[LOLO].threshold) )
        {
            //entering LowLow region
            m_region = REGION_LOLO;
            eventing = true;
        }
        else if( m_limits.limit[LOLO].active &&
                (m_region == REGION_LOLO) && (value >= m_limits.limit[LOLO].recovery) )
        {
            //exiting LowLow region
            updateRegion(value);
            eventing = true;
        }
        else if( m_limits.limit[LO].active &&
                (m_region > REGION_LO) && (value < m_limits.limit[LO].threshold) )
        {
            //entering Low region
            m_region = REGION_LO;
            eventing = true;
        }
        else if( m_limits.limit[LO].active &&
                (m_region == REGION_LO) && (value >= m_limits.limit[LO].recovery) )
        {
            //exiting Low region
            updateRegion(value);
            eventing = true;
        }
        else if( m_limits.limit[HI].active &&
                (m_region < REGION_HI) && (value > m_limits.limit[HI].threshold) )
        {
            //entering High region
            m_region = REGION_HI;
            eventing = true;
        }
        else if( m_limits.limit[HI].active &&
                (m_region == REGION_HI) && (value <= m_limits.limit[HI].recovery) )
        {
            //exiting High region
            updateRegion(value);
            eventing = true;
        }
        else if( m_limits.limit[HIHI].active &&
                (m_region < REGION_HIHI) && (value > m_limits.limit[HIHI].threshold) )
        {
            //entering HighHigh region
            m_region = REGION_HIHI;
            eventing = true;
        }
        else if( m_limits.limit[HIHI].active &&
                (m_region == REGION_HIHI) && (value <= m_limits.limit[HIHI].recovery) )
        {
            //exiting HighHigh region
            updateRegion(value);
            eventing = true;
        }
        else
        {
            //Already in a region
            if(status == AFILTER_RESULT_IN_LIMIT_THR)
                status = AFILTER_RESULT_IN_LIMIT;   //Already entered into limit
            if(status != AFILTER_RESULT_IN_LIMIT)
                status = AFILTER_RESULT_IN_LIMIT_THR;   //Entering in limit
        }

        if(eventing)
        {
            status = AFILTER_RESULT_OUT_LIMIT_THR;
        }
    }

    return status;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void HIHILOLOFilter::updateRegion(const lu_float32_t value)
{
    if( m_limits.limit[LOLO].active && (value < m_limits.limit[LOLO].threshold) )
    {
        m_region = REGION_LOLO;
    }
    else if( m_limits.limit[LO].active && (value < m_limits.limit[LO].threshold) )
    {
        m_region = REGION_LO;
    }
    else if( m_limits.limit[HIHI].active && (value > m_limits.limit[HIHI].threshold) )
    {
        m_region = REGION_HIHI;
    }
    else if( m_limits.limit[HI].active && (value > m_limits.limit[HI].threshold) )
    {
        m_region = REGION_HI;
    }
    else
    {
        m_region = REGION_MID;
    }
}


/*
 *********************** End of file ******************************************
 */
