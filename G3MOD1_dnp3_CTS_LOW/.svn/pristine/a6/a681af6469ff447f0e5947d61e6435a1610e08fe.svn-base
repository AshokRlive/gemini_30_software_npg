/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */



#if !defined(EA_3BA343EF_D90D_4541_914A_F9845793945C__INCLUDED_)
#define EA_3BA343EF_D90D_4541_914A_F9845793945C__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "IChannel.h"
#include "QueueMgr.h"
#include "QueueObj.h"

/* Forward declaration */
class ChannelObserver;
class IChannel;

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */



/**
 * Channel observer queue manager
 */
typedef QueueMgr<ChannelObserver> ChannelObserverQueue;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */



class ChannelObserver : public QueueObj<ChannelObserver>
{

public:
    ChannelObserver() {};
    virtual ~ChannelObserver() {};

    /**
     * \brief notify that a channel has been updated
     *
     * The subject is passed as a parameter: the observer can monitor multiple
     * channels at the same time
     *
     * \param subject The changed channel.
     *
     * \return None
     */
    virtual void update(IChannel *subject) = 0;
};

#endif // !defined(EA_3BA343EF_D90D_4541_914A_F9845793945C__INCLUDED_)

/*
 *********************** End of file ******************************************
 */



