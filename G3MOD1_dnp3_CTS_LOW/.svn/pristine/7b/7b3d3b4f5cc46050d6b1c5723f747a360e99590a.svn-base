/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.export.sys;

import java.util.ArrayList;

import org.fordiac.ide.model.libraryElement.AutomationSystem;

/**
 * The Interface ISystemExport.
 * 
 * @author gebenh
 */
public interface ISystemExport {

	/**
	 * Export.
	 * 
	 * @param system the system
	 * @param destinationPath the destination path
	 * @param overwriteWithoutWarning the overwrite without warning
	 */
	public void export(AutomationSystem system, String destinationPath,
			boolean overwriteWithoutWarning);

	/**
	 * Gets the export filter name.
	 * 
	 * @return the name of the ExportFilter
	 */
	String getExportFilterName();

	/**
	 * Gets the export filter description.
	 * 
	 * @return the description of the exportfilter
	 */
	String getExportFilterDescription();

	/**
	 * Warnings occured during export.
	 * 
	 * @return the warnings
	 */
	ArrayList<String> getWarnings();

	/**
	 * Errors occured during export.
	 * 
	 * @return the Errors
	 */
	ArrayList<String> getErrors();

	/**
	 * Infos occured during export.
	 * 
	 * @return the Infos
	 */
	ArrayList<String> getInfos();

}
