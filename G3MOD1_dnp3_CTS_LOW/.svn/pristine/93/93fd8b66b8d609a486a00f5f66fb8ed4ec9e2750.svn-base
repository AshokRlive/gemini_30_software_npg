/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   28/09/11      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _CRC16_INCLUDED
#define _CRC16_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

extern lu_uint16_t crc16;

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Init crc16 to 0xffff
 *
 *   Call to init crc before starting the calculation
 *
 *   \param none
 *
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void crc16_init(void);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   CCITT (0xFFFF) CRC16
 *
 *   \param parameterName Description 
 *    (set crc16 to 0xffff - crc16_init() before you begin the calculation)
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
extern void crc16_byte(lu_uint8_t data);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *   (set crc16 to 0xffff - crc16_init() before you begin the calculation)
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
extern void crc16_calc16(lu_uint8_t *data_p, lu_uint16_t len);

/******************************************************************************
*   \brief Brief description
*
*   Detailed description
*
*   crc-16 Modbus
*
*   \param parameterName Description
*
*
*   \return
*
******************************************************************************
*/
lu_uint16_t crc16_modbus_calc16(lu_uint8_t *data_p, lu_uint8_t len);

#endif /* _CRC16_INCLUDED */

/*
 *********************** End of file ******************************************
 */
