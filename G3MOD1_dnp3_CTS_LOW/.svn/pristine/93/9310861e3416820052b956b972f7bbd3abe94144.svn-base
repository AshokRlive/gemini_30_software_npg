package com.lucy.g3.iec61850.scl.systemcorp;

import java.io.IOException;
import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.FeatureMap;

import com.lucy.g3.iec61850.scl.internal.ResourceUtils;
import com.lucy.g3.iec61850.model.scl.DocumentRoot;
import com.lucy.g3.iec61850.model.scl.SCLFactory;
import com.lucy.g3.iec61850.model.scl.SCLPackage;
import com.lucy.g3.iec61850.model.scl.TDAI;
import com.lucy.g3.iec61850.model.scl.TIED;
import com.lucy.g3.iec61850.model.scl.TPrivate;
import com.lucy.g3.iec61850.model.scl.util.SCLResourceFactoryImpl;
import com.lucy.g3.iec61850.model.systemcorp.SystemCorpPackage;
import com.lucy.g3.iec61850.scl.systemcorp.internal.SystemCorpObject;



public class SystemCorpIntegration {

	private Logger log = Logger.getLogger(SystemCorpIntegration.class);
	
	
	protected DocumentRoot root;
	
	
	public static void main(String[] args) throws Exception {
		

		
	}
	

	public SystemCorpIntegration() {
		initialiseResource ();
		ResourceUtils.check();
	}
	
	
	/**
	 * Define E-Core dependencies
	 */
	public void initialiseResource () {
		ResourceSet resourceSet = new ResourceSetImpl();
    	log.debug("Register Factory...");
    	resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put
			(Resource.Factory.Registry.DEFAULT_EXTENSION, 
					new SCLResourceFactoryImpl());
    	log.debug("Register package...");
		resourceSet.getPackageRegistry().put
			(SCLPackage.eNS_URI, 
					SCLPackage.eINSTANCE);
		
		ResourceUtils.setResourceManager(resourceSet);
		
    	
    }
	
	public EObject loadFile(String filePath) throws Exception {
		return root = (DocumentRoot) ResourceUtils.loadObject(filePath);
	}
	
	public void save(String filePath) throws IOException {
		ResourceUtils.saveObject(root, filePath);
	}
	
	private void runTransformation() {
		
		if (root == null)
			throw new IllegalStateException();
		
		EList<TIED> ieds = root.getSCL().getIED();
		
		// for each IED in the configuration
		for (TIED ied : ieds) {

			TreeIterator<EObject> dataObject = ied.eAllContents();

			while (dataObject.hasNext()) {

				EObject dis = dataObject.next();

				// remove existing SystemCorp elements
				if (dis instanceof TPrivate) {
					if (((TPrivate) dis).getType().equals(SystemCorpPackage.eNS_PREFIX))
						EcoreUtil.remove(dis);
				}

				// add private SystemCorp elements to Data Attribute Instances
				if (dis instanceof TDAI) {
					TDAI dataAttrInstance = (TDAI) dis;
					dataAttrInstance.getPrivate().add(injectPrivate());

				}
			}

		}
		
		
	}
	
	
    private TPrivate injectPrivate() {
    	
    	TPrivate newPrivate = SCLFactory.eINSTANCE.createTPrivate();
		
    	newPrivate.setType(SystemCorpPackage.eNS_PREFIX);
				
		FeatureMap privateMixed = newPrivate.getMixed();
		EStructuralFeature sysFeature = 
				ResourceUtils.getMetaData().demandFeature(SystemCorpPackage.eNS_URI, "GenericPrivateObject", true);
		// FeatureMapUtil.addText(privateMixed, "n ");
		privateMixed.add(sysFeature, SystemCorpObject.generate());
		
		return newPrivate;
    }
    
    
    public static void transform (String inputFile, String outputFile) throws Exception {
		
    	SystemCorpIntegration integrate = new SystemCorpIntegration();
    	   	
    	integrate.loadFile(inputFile);
    	integrate.runTransformation();
    	
    	ResourceUtils.saveObject(integrate.root, outputFile);   	  	
    	
	}
    
    public static void transform (String file) throws Exception {
    	transform(file, file);
    }

}
