/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.fields.manager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.device.fields.FieldDevicePlugin;
import com.lucy.g3.rtu.config.device.fields.domain.FieldDeviceFactory;
import com.lucy.g3.rtu.config.device.fields.manager.FieldDeviceManager;
import com.lucy.g3.rtu.config.protocol.master.shared.MasterProtocolPlugin;
import com.lucy.g3.rtu.config.protocol.master.shared.manager.MasterProtocolManager;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMB;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBChannel;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigModule;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * The Class FieldDeviceManagerImplTest.
 */
public class FieldDeviceManagerImplTest implements IConfig {

  private MasterProtocolManager masterProtoMgr;
  private FieldDeviceManager fixture;
  private MMB mmb;


  @Before
  public void setUp() throws Exception {
    masterProtoMgr = MasterProtocolPlugin.INSTANCE.create(null);
    masterProtoMgr.add(new MMB(FieldDeviceFactory.INSTANCE));
    mmb = (MMB) masterProtoMgr.getModbusMaster();
    
    fixture = FieldDevicePlugin.getInstance().create(this);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAddSession() {
    assertEquals(0, fixture.getDeviceSize());

    final int chnlNum = 3;
    final int sesnNum = 2;

    for (int i = 0; i < chnlNum; i++) {
      MMBChannel ch = mmb.addChannel("");
      for (int j = 0; j < sesnNum; j++) {
        ch.addSession("");
      }
    }

    assertEquals(chnlNum * sesnNum, fixture.getDeviceSize());
  }

  @Test
  public void testRemoveSession() {
    MMBChannel ch = createSession(3);

    ch.removeAll(ch.getAllItems());
    assertEquals(0, fixture.getDeviceSize());
  }

  @Test
  public void testRemoveChannel() {
    MMBChannel ch = createSession(3);

    ch.delete();
    assertEquals(0, fixture.getDeviceSize());
  }

  @Test
  public void testRemoveChannel2() {
    MMBChannel ch = createSession(3);

    mmb.remove(ch);
    assertEquals(0, fixture.getDeviceSize());
  }

  @Test
  public void testRemoveProtocol() {
    createSession(3);

    mmb.delete();
    assertEquals(0, fixture.getDeviceSize());
  }

  private MMBChannel createSession(int number) {
    MMBChannel ch = mmb.addChannel("");
    for (int i = 0; i < number; i++) {
      ch.addSession("");
    }
    assertEquals(number, fixture.getDeviceSize());
    return ch;
  }

  @Test
  public void testDeviceIDUpdate() {
    MMBChannel ch = createSession(3);

    assertNotNull(fixture.getDevice(MODULE.MODULE_MBDEVICE, MODULE_ID.MODULE_ID_0));
    assertNotNull(fixture.getDevice(MODULE.MODULE_MBDEVICE, MODULE_ID.MODULE_ID_1));
    assertNotNull(fixture.getDevice(MODULE.MODULE_MBDEVICE, MODULE_ID.MODULE_ID_2));
    assertNull(fixture.getDevice(MODULE.MODULE_MBDEVICE, MODULE_ID.MODULE_ID_3));

    ch.remove(ch.getAllItems().iterator().next());

    assertNotNull(fixture.getDevice(MODULE.MODULE_MBDEVICE, MODULE_ID.MODULE_ID_0));
    assertNotNull(fixture.getDevice(MODULE.MODULE_MBDEVICE, MODULE_ID.MODULE_ID_1));
    assertNull(fixture.getDevice(MODULE.MODULE_MBDEVICE, MODULE_ID.MODULE_ID_2));
    assertNull(fixture.getDevice(MODULE.MODULE_MBDEVICE, MODULE_ID.MODULE_ID_3));

  }
  // @Test
  // public void testAddRemoveChannel() {
  // MMBChannel ch = mmb.addChannel("ch");
  // ch.add(item)
  // }

  @Override
  public <T extends IConfigModule> T getConfigModule(String id) {
    if(id.equals(MasterProtocolManager.CONFIG_MODULE_ID))
      return (T) masterProtoMgr;
    else
      return null;
  }

  @Override
  public Collection<IConfigModule> getAllConfigModules() {
    return null;
  }

}
