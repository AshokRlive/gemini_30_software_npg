/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.domain.source;

import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;

/**
 * The source for SCADA input points.
 */
public class ScadaPointInput extends AbstractScadaPointSource<VirtualPoint> {

  public ScadaPointInput(VirtualPoint rawSource) {
    super(rawSource);
  }

  @Override
  public int getGroup() {
    return rawSource.getGroup();
  }

  @Override
  public int getId() {
    return rawSource.getId();
  }

  @Override
  protected String generateName() {
    return rawSource.getName();
  }

  @Override
  public String getDescription() {
    return rawSource.getDescription();
  }

  @Override
  public VirtualPoint getRawSource() {
    return rawSource;
  }

  @Override
  public boolean isAnalogue() {
    return rawSource.getType().isAnalogue();
  }

  @Override
  public String getType() {
    return rawSource.getType().getDescription();
  }

}
