/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Virtual point database implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stddef.h>
#include <cstring>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "PointDB.h"
#include "LockingMutex.h"
#include "Debug.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

PointDB::PointDB() : mutex(LU_TRUE),
                     dbSize(-1),
                     db(NULL)
{

}


PointDB::~PointDB()
{
    IPoint *pointPtr;

    if(db != NULL)
    {
        /* Deallocate all the points */
        for(lu_int32_t i = 0; i < dbSize; ++i)
        {
            pointPtr = db[i];

            if(pointPtr != NULL)
            {
                delete pointPtr;
                db[i] = NULL;   //prevent further use of this point while deleting
            }
        }

        /* Deallocate the database */
        delete db;
        db = NULL;
    }
}


GDB_ERROR PointDB::init(lu_uint16_t size)
{
    GDB_ERROR ret = GDB_ERROR_NONE;

    /* Enter critical region */
    LockingMutex lMutex(mutex);

    if(db == NULL)
    {
        /* Create the database */
        db = new IPoint*[size];

        /* Save db size */
        dbSize = size;

        /* Initialize the database */
        memset(db, 0, sizeof(IPoint*) * dbSize);
    }
    else
    {
        ret = GDB_ERROR_INITIALIZED;
    }

    return ret;
}

lu_uint32_t PointDB::getSize()
{
    if(db == NULL)
    {
        return 0;
    }
    else
    {
        return dbSize;
    }
}

GDB_ERROR PointDB::addPoint(IPoint *pointPtr)
{
    PointIdStr pointID;
    GDB_ERROR ret = GDB_ERROR_NONE;

    if(pointPtr == NULL)
    {
        return GDB_ERROR_PARAM;
    }

    /* Enter critical region */
    LockingMutex lMutex(mutex);

    if(db == NULL)
    {
        return GDB_ERROR_NOT_INITIALIZED;
    }

    /* Get point ID */
    pointID = pointPtr->getID();

    /* check pointID (used as an index in the db)
     * against the db size.
     * The pointID must be smaller then the db size
     */
    if( (pointID.group != POINT_DB_GROUP) || (pointID.ID >= dbSize) )
    {
        ret = GDB_ERROR_PARAM;
    }

    /* Add the point to the db. Generate an error if the
     * slot is not empty
     */
    if(ret == GDB_ERROR_NONE)
    {
        if(db[pointID.ID] == NULL)
        {
            db[pointID.ID] = pointPtr;
        }
        else
        {
            ret = GDB_ERROR_POINT_EXIST;
        }
    }

    return ret;
}


IPoint *PointDB::getPoint(lu_uint16_t ID)
{
    /* Probably unnecessary */
    LockingMutex lMutex(mutex);

    if( (db == NULL  ) ||
        (ID >= dbSize)
      )
    {
        return NULL;
    }

    /* Return a reference to the virtual point */
    return  db[ID];
}

GDB_ERROR PointDB::stop()
{
    lu_int32_t i;

    if (db != NULL)
    {
        /* Loop around the points, stopping them */
        for (i = 0; i < dbSize; i++)
        {
            if (db[i] != NULL)
            {
                db[i]->stopUpdate();
            }
        }
    }

    return GDB_ERROR_NONE;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
