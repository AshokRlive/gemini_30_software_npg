/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.preferences.impl;

import java.math.RoundingMode;

import com.lucy.g3.configtool.preferences.IPreferenceView;
import com.lucy.g3.xml.gen.common.IEC870Enum.LU_IOA_FORMAT;

/**
 *
 */
public class RealtimePreference extends AbstractPreference {

  private static final long serialVersionUID = -1008747587144252150L;

  public final static String ID = "RealtimePreference";
  
  /** Minimum updating milliseconds period for real-time display. */
  public static final int MIN_PERIOD_MS = 200;
  public static final int MAX_PERIOD_MS = 20000;

  /** Minimum fraction digits of float point value. */
  public static final int MIN_RT_FRACTION_DIGITS = 1;

  /** Maximum fraction digits of float point value. */
  public static final int MAX_RT_FRACTION_DIGITS = 40;

  /** Default fraction digits of float point value. */
  public static final int DEFAULT_RT_FRACTION_DIGITS = 4;
  
  
  public static final String PROPERTY_UPDATE_PERIOD_MS = "updatePeriod";

  /** Fraction digits for float value. */
  public static final String PROPERTY_RT_FRACTION_DIGITS = "rtDigits";

  /** Rounding mode for float value. */
  public static final String PROPERTY_RT_FRACTION_ROUND_MODE = "rtRoundMode";

  public static final String PROPERTY_IOA_FORMAT = "ioaFormat";
  
  
  /* The rate of scheduling a periodic task (ms) */
  private int updatePeriod = 500;

  private int rtDigits = DEFAULT_RT_FRACTION_DIGITS;

  private RoundingMode rtRoundMode = RoundingMode.HALF_UP;
  
  private LU_IOA_FORMAT ioaFormat = LU_IOA_FORMAT.LU_IOA_FORMAT_NONE; 

  public RealtimePreference() {
    super(ID);
  }



  public int getUpdatePeriod() {
    return updatePeriod;
  }

  public void setUpdatePeriod(int updatePeriod) {
    if (updatePeriod < MIN_PERIOD_MS) {
      return;
    }

    Object oldValue = getUpdatePeriod();
    this.updatePeriod = updatePeriod;
    firePropertyChange(PROPERTY_UPDATE_PERIOD_MS, oldValue, updatePeriod);
  }

  /**
   * Gets the Fraction digits for float value. This option field is used by
   * real-time display.
   */
  public int getRtDigits() {
    return rtDigits;
  }

  /**
   * Sets the bits of fraction digits for float values. This option field is
   * used by real-time display.
   *
   * @param rtBits
   *          range between {@code MIN_RT_FRACTION_DIGITS} and
   *          {@code MAX_RT_FRACTION_DIGITS}
   */
  public void setRtDigits(int rtBits) {
    if (rtBits < MIN_RT_FRACTION_DIGITS || rtBits > MAX_RT_FRACTION_DIGITS) {
      return;
    }

    Object oldValue = getRtDigits();
    this.rtDigits = rtBits;
    firePropertyChange(PROPERTY_RT_FRACTION_DIGITS, oldValue, rtBits);
  }

  /**
   * Gets the rounding mode for float value. This option field is used by
   * real-time display.
   *
   * @return {@link RoundingMode}
   */
  public RoundingMode getRtRoundMode() {
    return rtRoundMode;
  }

  /**
   * Changes the rounding mode for float point value.
   *
   * @param rtRoundMode
   *          non-null {@link RoundingMode}
   */
  public void setRtRoundMode(RoundingMode rtRoundMode) {
    if (rtRoundMode == null) {
      return;
    }

    Object oldValue = getRtRoundMode();
    this.rtRoundMode = rtRoundMode;
    firePropertyChange(PROPERTY_RT_FRACTION_ROUND_MODE, oldValue, rtRoundMode);
  }
  

  public LU_IOA_FORMAT getIoaFormat() {
    return ioaFormat;
  }

  
  public void setIoaFormat(LU_IOA_FORMAT ioaFormat) {
    Object oldValue = this.ioaFormat;
    this.ioaFormat = ioaFormat;
    firePropertyChange(PROPERTY_IOA_FORMAT, oldValue, ioaFormat);
  }



  @Override
  public IPreferenceView createView(Object owner) {
    return new RealtimeView(owner, this); 
  }
}
