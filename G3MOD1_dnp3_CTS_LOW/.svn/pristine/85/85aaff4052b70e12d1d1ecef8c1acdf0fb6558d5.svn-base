/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/
package com.lucy.g3.rtu.config.module.canmodule.domain;

import java.text.ParseException;
import java.util.Arrays;

import com.g3schema.ns_g3module.FPIChannelT;
import com.jgoodies.binding.beans.Model;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfigFault.FPIConfigCurrentFault;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfigFault.FPIConfigEarthFault;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfigFault.FPIConfigPhaseFault;
import com.lucy.g3.rtu.config.shared.base.math.Ratio;

public class FPIConfig extends Model {

    /**
     * The name of property {@value} . <li>Value type: {@link Long}.</li> <li>
     * Value unit: mins.</li>
     */
    public static final String PROPERTY_SELF_RESET_MIN = "selfResetMin";


    /**
     * The name of property {@value} for enabling this FPI channel. <li>Value
     * type: {@link Boolean}.</li>
     */
    public static final String PROPERTY_ENABLED = "enabled";

    /**
     * The name of property {@value} . <li>Value type: {@link CTRatio}.</li>
     */
    public static final String PROPERTY_CTRATIO = "ctratio";
    
    /**
     * The name of read-only property {@value} . <li>Value type: {@link FPIConfigPhaseFault}.</li>
     */
    public static final String PROPERTY_PHASEFAULT = "phaseFault";
    
    /**
     * The name of read-only property {@value} . <li>Value type: {@link FPIConfigFault}.</li>
     */
    public static final String PROPERTY_EARCHFAULT = "earchFault";
    
    public static final String PROPERTY_SENSITIVE_EARCHFAULT = "sensitiveEarchFault";
    
    private static final CTRatio[] DEFAULT_CTRATIOS = {
        new CTRatio(500, 1),
        new CTRatio(1000, 1),
    };


    private long selfResetMin = 360;
    private boolean enabled = true;
    private CTRatio ctratio = new CTRatio(500, 1);

    private final IChannel fpiResetChannel;
    
    public final FPIConfigPhaseFault phaseFault = new FPIConfigPhaseFault();
    public final FPIConfigEarthFault earchFault = new FPIConfigEarthFault();
    public final FPIConfigEarthFault sensitiveEarthFault = new FPIConfigEarthFault();
    public final FPIConfigCurrentFault   currentPresence = new FPIConfigCurrentFault();
    public final FPIConfigCurrentFault   currentAbsence = new FPIConfigCurrentFault();
    
    public FPIConfig(IChannel fpiResetChannel) {
      super();
      this.fpiResetChannel = fpiResetChannel;
      
      /* Set defaults */
      earchFault.setMinFaultDurationMs(50); // Minimum Fault Duration: 50ms
      earchFault.setTimedFaultCurrent(90); // Timed Fault Current: 90 A
      earchFault.setInstantFaultCurrent(0);// Instant Fault Current: 0 A
      
      sensitiveEarthFault.setMinFaultDurationMs(0); 
      sensitiveEarthFault.setTimedFaultCurrent(8); 
    }

    private static Long convertMinsToMs(Long mins) {
      return mins * 60000;
    }

    private static Long convertMsToMins(Long ms) {
      return ms / 60000;
    }
    
    public FPIConfigPhaseFault getPhaseFault() {
      return phaseFault;
    }

    
    public FPIConfigFault getEarchFault() {
      return earchFault;
    }

    public FPIConfigFault getSensitiveEarchFault() {
      return sensitiveEarthFault;
    }

    public long getSelfResetMin() {
      return selfResetMin;
    }
    
    
    public void setSelfResetMin(long selfResetMin) {
      Object oldValue = this.selfResetMin;
      this.selfResetMin = selfResetMin;
      firePropertyChange(PROPERTY_SELF_RESET_MIN, oldValue, selfResetMin);
    }

    public boolean isEnabled() {
      return enabled;
    }

    public void setEnabled(boolean enabled) {
      Object oldValue = this.enabled;
      this.enabled = enabled;
      firePropertyChange(PROPERTY_ENABLED, oldValue, enabled);
    }

    public CTRatio getCtratio() {
      return ctratio;
    }

    public void setCtratio(CTRatio ctratio) {
      if (ctratio == null) {
        return;
      }

      Object oldValue = this.ctratio;
      this.ctratio = ctratio;
      firePropertyChange(PROPERTY_CTRATIO, oldValue, ctratio);
    }

    public static CTRatio[] getDefaultCTRatio() {
      return Arrays.copyOf(DEFAULT_CTRATIOS, DEFAULT_CTRATIOS.length);
    }

    public void writeToXML(FPIChannelT xml) {
      xml.selfResetMs.setValue(convertMinsToMs(getSelfResetMin()));
      xml.enabled.setValue(isEnabled());
      xml.CTRatioDividend.setValue(getCtratio().dividend);
      xml.CTRatioDivisor.setValue(getCtratio().divisor);
      
      /*
       * Note: we must apply CT Ratio before write current. See #2220
       */
      phaseFault.writeToXML(getCtratio(), xml.phaseFault.append());
      earchFault.writeToXML(getCtratio(), xml.earthFault.append());
      sensitiveEarthFault.writeToXML(getCtratio(), xml.sensitiveEarthFault.append());
      currentAbsence.writeToXML(getCtratio(), xml.currentAbsence.append());
      currentPresence.writeToXML(getCtratio(), xml.currentPresence.append());
    }
    

    public void readFromXML(FPIChannelT xml_cfg) {
      setSelfResetMin(convertMsToMins(xml_cfg.selfResetMs.getValue()));
      setEnabled(xml_cfg.enabled.getValue());
      CTRatio ratio = new CTRatio(xml_cfg.CTRatioDividend.getValue(), xml_cfg.CTRatioDivisor.getValue());
      setCtratio(ratio);
      phaseFault.readFromXML(ratio, xml_cfg.phaseFault.first());
      earchFault.readFromXML(ratio, xml_cfg.earthFault.first());
      
      if(xml_cfg.sensitiveEarthFault.exists())
        sensitiveEarthFault.readFromXML(ratio, xml_cfg.sensitiveEarthFault.first());
      
      if(xml_cfg.currentAbsence.exists())
        currentAbsence.readFromXML(ratio, xml_cfg.currentAbsence.first());
      
      if(xml_cfg.currentPresence.exists())
        currentPresence.readFromXML(ratio, xml_cfg.currentPresence.first());      
    }


    public IChannel getResetChannel() {
      return fpiResetChannel;
    }

    public static final class CTRatio extends Ratio {

      public CTRatio(long dividend, long divisor) {
        super(dividend, divisor);
        Preconditions.checkArgument(dividend >= 0, "dividend must be >= 0");
        Preconditions.checkArgument(divisor > 0, "divisor  must be > 0");
      }

      public double calculateScaingFactor() {
        return (dividend / divisor) / 10000D;
      }
      
      public static CTRatio parseFromString(String ctRatioStr) throws ParseException {
        Ratio ratio = Ratio.parseFromString(ctRatioStr);
        return new CTRatio(ratio.dividend, ratio.divisor);
      }
      
      /**
       * The FPM channel configuration for FPI-A / FPI-B should have additional
       * parameters for CT ratio. E.g. 500:1 ; 1000:1 500:4 , etc. This parameters
       * will only be used within the configuration tool and is not passed on to the
       * FPM, but should be stored in the XML. The parameters for the current
       * thresholds in FPI-A / FPI-B channels should be passed in units of 0.1ma, as
       * per the units for the phase current channels. So if a CT of 500:1 is fitted
       * the physical current for a CT current of 500AMPS at the switch gear will
       * result in a 1 AMP current at the physical input, resulting in 10000 (ma x
       * 0.1) reported in the channel for phase current. So to set a threshold of
       * 250 AMPS with a CT 500:1 the configuration should be set to 2500. The
       * wizard for adding the FPM should ask for the CT ratio for FPI-A / FPI-B
       * (default is 500:1) and this setting should be used to set the correct
       * scaling factor for the virtual points mapped to phase current channels. So
       * for a CT of 500:1 the scaling factor should be set to (500 / 1) / 10,000 =
       * 0.05 scaling factor for a CT ratio 500:1. Also the units for current
       * thresholds should be displayed in Amps, but stored in units of 0.1ma
       * physical channel current, since this is what the FPM needs. The customer
       * wants to see Amps in the configuration tool, this will need to be converted
       * using the CT ratio parameter. Note CT ratio should be per channel, that is
       * FPI-A / FPI-B should have seperate CT ratio config. 3. The self reset time
       * mins should be passed the FPM as time in ms. So MCM needs to multiply by
       * 1000 before passing onto FPM. Being able to set reset time in ms on the FPM
       * card in ms in useful for testing, even though it cannot be set vi config
       * tool. Test can message can be generated for testing purpose.
       * 
       * @see open project <a href="http://10.11.253.18/work_packages/2220">#2220</a>
       */
      public static long applyCTRatio(CTRatio ctratio, long current) {
        long dividend = ctratio.dividend;
        long divisor = ctratio.divisor;
        long result = (current * 10000) / (dividend / divisor);
        return result;
      }
      
      public static long applyCTRatio2(CTRatio ctratio, long current) {
        long dividend = ctratio.dividend;
        long divisor = ctratio.divisor;
        long result = ((current * dividend) / divisor) / 10000;
        return result;
      }
    }
  }