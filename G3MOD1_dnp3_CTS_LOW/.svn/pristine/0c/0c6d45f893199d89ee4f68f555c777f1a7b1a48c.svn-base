/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.wizards.unsolretry;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyVetoException;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.text.JTextComponent;

import org.apache.log4j.Logger;
import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.common.base.Strings;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.view.ValidationComponentUtils;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Constraints;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session.UnsolRetry;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.common.UnsolRetryVisibilityUpdater;
import com.lucy.g3.rtu.config.shared.base.misc.ComponentGroup;
import com.lucy.g3.xml.gen.common.DNP3Enum.UNSOLICITED_RETRY_MODE;

/**
 * Initial wizard page.
 */
class StepInitial extends WizardPage {
  static final String ID = "StepInitial";
  static final String KEY_RETRY_MODE = "mode";
  
  static final String KEY_MAX_RETRY = "Parameter.MaxRetry";
  static final String KEY_RETRY_DELAY = "Parameter.RetryDelay";
  static final String KEY_OFFLINE_DELAY = "Parameter.offlineDelay";
  
  private Logger log = Logger.getLogger(StepInitial.class);
  
  private final UnsolRetryVisibilityUpdater updater;
  
  private final UnsolRetry bean;
  
  private SDNP3Constraints constraints = SDNP3Constraints.INSTANCE;
  
  StepInitial(SDNP3Session.UnsolRetry bean) {
    super(ID, "Select Retry Mode");
    this.bean = bean;
    initComponents();
    initComponentsNames();
    
    updater = new UnsolRetryVisibilityUpdater(
        new ComponentGroup(lblMaxRetryTimes,tfMaxRetryTimes), 
        new ComponentGroup(lblRetryDelay, tfRetryDelay,lblms1),  
        new ComponentGroup(lblOfflineDelay, tfOfflineDelay,lblms3)  
        );
    
    UNSOLICITED_RETRY_MODE mode = (UNSOLICITED_RETRY_MODE) combMode.getSelectedItem();
    updateDescription(mode);
    updateVisibility(mode);
  }
  
  
  private void initComponentsNames() {
    combMode.setName(KEY_RETRY_MODE);
    tfMaxRetryTimes.setName(KEY_MAX_RETRY);
    tfRetryDelay.setName(KEY_RETRY_DELAY);
    tfOfflineDelay.setName(KEY_OFFLINE_DELAY);
    
    tfMaxRetryTimes.setText(String.valueOf(bean.getUnsolMaxRetries()));
    tfRetryDelay.setText(String.valueOf(bean.getUnsolRetryDelay()));
    tfOfflineDelay.setText(String.valueOf(bean.getUnsolOfflineRetryDelay()));
    
    lblMaxRetryTimes.setText(SDNP3Session.UnsolRetry.LABEL_MAXIMUM_RETRY_TIMES);
    lblRetryDelay.setText(SDNP3Session.UnsolRetry.LABEL_RETRY_DELAY);
    lblOfflineDelay.setText(SDNP3Session.UnsolRetry.LABEL_OFFLINE_RETRY_DELAY);
    lblms1.setText(SDNP3Session.UnsolRetry.LABEL_MS);
    lblms3.setText(SDNP3Session.UnsolRetry.LABEL_MS);
  }

  
  private void updateVisibility(UNSOLICITED_RETRY_MODE mode) {
    updater.update(mode);
  }
  
  private void updateDescription(UNSOLICITED_RETRY_MODE mode) {
    String description = null;
    

    if (mode != null) {
      description = "";
      switch (mode) {
      case UNSOLICITED_RETRY_MODE_NO_RETRY:
        description += "The unsolicited event is generated once.";
        break;
        
      case UNSOLICITED_RETRY_MODE_SIMPLE_RETRY:
        description += "After the unsolicited request, it is retried the specified number of times.";
        break;
        
      case UNSOLICITED_RETRY_MODE_WITH_OFFLINE:
        description += "After the unsolicited request, it is retried the specified number of times, "
            + "then retried these times after an offline time.";
        break;
        
      case UNSOLICITED_RETRY_MODE_UNLIMITED:
        description += "Will send an unlimited stream of retries until acknowledged.";
        break;

      default:
        break;
      }
    }
    
    if (Strings.isBlank(description) == false) {
      description = "( " + description + " )";
    }
    taDescription.setText(description);
  }

  @Override
  protected String validateContents(Component comp, Object event) {
    if (comp == null)
      return null;
    
    if (comp == combMode) {
      if (combMode.getSelectedItem() == null) {
        return "Please select a mode!";
      }
    }

    String error = null;

    // Validate all text fields
    if (comp instanceof JTextField) {
      // Reset background
      ((JTextComponent) comp).setBackground(Color.white);
      
      // Check input is a number
      Long value = null;
      JTextField field = (JTextField) comp;
      String input = field.getText();
      try {
        value = Long.valueOf(input);
      } catch (Throwable e) {
        error = "Invalid input: " + input;
      }
      
      // Check value not negative
      if (value != null && value < 0) {
        error = "Negative value is not allowed: " + value;
      }
      
      // Check value range under current mode
      UNSOLICITED_RETRY_MODE mode = (UNSOLICITED_RETRY_MODE) getWizardData(StepInitial.KEY_RETRY_MODE);
      if (mode != null) {
        switch (mode) {
        case UNSOLICITED_RETRY_MODE_SIMPLE_RETRY:
        case UNSOLICITED_RETRY_MODE_WITH_OFFLINE:
          // Check not zero
          if (comp == tfMaxRetryTimes && value != null && value == 0) {
            error = "Max Retry Times cannot be zero";
          }
          break;
          
        default:
          break;
        }
      }
      
      // Validate range with Constraint class
      if (value != null) {
        String[] valueProperties = new String[] {
            UnsolRetry.PROPERTY_UNSOLCONFIRMTIMEOUT,
            UnsolRetry.PROPERTY_UNSOLMAXRETRIES,
            UnsolRetry.PROPERTY_UNSOLOFFLINERETRYDELAY,
            UnsolRetry.PROPERTY_UNSOLRETRYDELAY,
        };
        for (String propertyName : valueProperties) {
          String msg = validateRange(value, propertyName);
          if (msg != null) {
            error = msg;
          }
        }
      }
      
      // Set error background
      if (error != null) {
        ValidationComponentUtils.setErrorBackground((JTextComponent) comp);
      }
    }
    
    return error;
  }
    
  private String validateRange(long value, String propertyName) {  
    final String prefix = SDNP3Constraints.PREFIX_SESSION;
    try {
      constraints.checkPropertyBoundary(bean, 0, value, prefix, propertyName);
      return null;
    } catch (PropertyVetoException e) {
      log.error("Validation failed:" + e.getMessage());
      return "The value is out of range:" + value;
    }
  }
   

  private void createUIComponents() {
    combMode = new JComboBox<Object>(UNSOLICITED_RETRY_MODE.values());
    combMode.addItem(null);
    combMode.setSelectedItem(bean.getMode());
  }


  private void combModeItemStateChanged(ItemEvent e) {
    UNSOLICITED_RETRY_MODE mode = (UNSOLICITED_RETRY_MODE) combMode.getSelectedItem();
    updateDescription(mode);
    updateVisibility(mode);
  }


  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    label1 = new JLabel();
    taDescription = new JTextArea();
    panel1 = new JPanel();
    lblMaxRetryTimes = new JLabel();
    tfMaxRetryTimes = new JTextField();
    lblRetryDelay = new JLabel();
    tfRetryDelay = new JTextField();
    lblms1 = new JLabel();
    lblOfflineDelay = new JLabel();
    tfOfflineDelay = new JTextField();
    lblms3 = new JLabel();

    //======== this ========
    setLayout(new FormLayout(
        "default, $lcgap, default:grow, $lcgap, default",
        "default, $rgap, fill:default, $pgap, top:default"));

    //---- label1 ----
    label1.setText("Retry Mode:");
    add(label1, CC.xy(1, 1));

    //---- combMode ----
    combMode.addItemListener(new ItemListener() {

      @Override
      public void itemStateChanged(ItemEvent e) {
        combModeItemStateChanged(e);
      }
    });
    add(combMode, CC.xy(3, 1));

    //---- taDescription ----
    taDescription.setEditable(false);
    taDescription.setOpaque(false);
    taDescription.setLineWrap(true);
    taDescription.setWrapStyleWord(true);
    taDescription.setFont(UIManager.getFont("Label.font"));
    add(taDescription, CC.xywh(1, 3, 3, 1));

    //======== panel1 ========
    {
      panel1.setLayout(new FormLayout(
          "default, $lcgap, default:grow, $lcgap, default",
          "2*(default, $lgap), default"));

      //---- lblMaxRetryTimes ----
      lblMaxRetryTimes.setText("RetryTimes");
      panel1.add(lblMaxRetryTimes, CC.xy(1, 1));
      panel1.add(tfMaxRetryTimes, CC.xy(3, 1));

      //---- lblRetryDelay ----
      lblRetryDelay.setText("RetryDelay");
      panel1.add(lblRetryDelay, CC.xy(1, 3));
      panel1.add(tfRetryDelay, CC.xy(3, 3));

      //---- lblms1 ----
      lblms1.setText("ms");
      panel1.add(lblms1, CC.xy(5, 3));

      //---- lblOfflineDelay ----
      lblOfflineDelay.setText("OfflineDelay");
      panel1.add(lblOfflineDelay, CC.xy(1, 5));
      panel1.add(tfOfflineDelay, CC.xy(3, 5));

      //---- lblms3 ----
      lblms3.setText("ms");
      panel1.add(lblms3, CC.xy(5, 5));
    }
    add(panel1, CC.xywh(1, 5, 5, 1));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel label1;
  private JComboBox<Object> combMode;
  private JTextArea taDescription;
  private JPanel panel1;
  private JLabel lblMaxRetryTimes;
  private JTextField tfMaxRetryTimes;
  private JLabel lblRetryDelay;
  private JTextField tfRetryDelay;
  private JLabel lblms1;
  private JLabel lblOfflineDelay;
  private JTextField tfOfflineDelay;
  private JLabel lblms3;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
