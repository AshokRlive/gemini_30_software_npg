/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Equipment Fault Handler (alarm) interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   26/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_F1A83037_37BF_49ec_966F_460BEBEA5FE8__INCLUDED_)
#define EA_F1A83037_37BF_49ec_966F_460BEBEA5FE8__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "ControlLogic.h"
#include "ControlLogicDigitalPoint.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Logic Gate control logic.
 *
 * This Control Logic applies a logic operation to all of its inputs.
 * The number of inputs is variable and are added dynamically.
 * The result is offered in a digital pseudo point.
 */
class LogicGateControlLogic : public ControlLogic
{
public:
    struct Config : public ControlLogicConf
    {
    public:
        /* Amounts of different types of Control Logic's Pseudo Points */
        static const lu_uint16_t BINARYPOINT_NUM = LOGIC_GATE_POINT_TYPE_BINARY_COUNT;
        static const lu_uint32_t ALLPOINT_NUM = LOGIC_GATE_POINT_EnumSize;
    public:
        LOGIC_GATE_OPERATE logic;
        ControlLogicDigitalPoint::Config bPoint[BINARYPOINT_NUM]; //Control Logic digital points
    };

public:
    LogicGateControlLogic( Mutex* mutex                   ,
                           GeminiDatabase& database       ,
                           LogicGateControlLogic::Config& conf
                         );
    virtual ~LogicGateControlLogic();

    /* == Inherited from ControlLogic == */
    virtual GDB_ERROR init();
    virtual GDB_ERROR getInputValue(lu_uint16_t inputPoint, PointData* data);

    /**
     * \brief Add a new point to the monitor engine
     *
     * \param point Point to add
     *
     * \return Error code
     */
    GDB_ERROR addPoint(PointIdStr point);

protected:
    /* == Inherited from ControlLogic == */
    virtual void newData(PointIdStr pointID, PointData& pointData);

private:
    LOGIC_GATE_OPERATE logic;   //Logic operator to use in this Logic gate

    /* List of Observers over the Virtual Point Inputs */
    MasterMutex listMutex;   // Mutex to protect the point observers List
    std::vector<DIObserver*> dObservers;    //Input point observers List
};

#endif // !defined(EA_F1A83037_37BF_49ec_966F_460BEBEA5FE8__INCLUDED_)


/*
 *********************** End of file ******************************************
 */
