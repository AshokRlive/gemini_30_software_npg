/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ModuleAlarmExtension.cpp 11 Apr 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/IOModule/src/ModuleAlarmExtension.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 11 Apr 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11 Apr 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "ModuleAlarmExtension.h"
#include "ModuleAlarmFactory.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

ModuleAlarmExtension::ModuleAlarmExtension() : alarmsInhibit(LU_FALSE)
{}

ModuleAlarmExtension::~ModuleAlarmExtension()
{
    MasterLockingMutex lMutex(alarmMutex, LU_TRUE);
    for(ModuleAlarmList::iterator it = alarmList.begin(); it != alarmList.end(); ++it)
    {
        delete *it; //remove object
    }
    alarmList.clear();  //remove pointers
}


lu_uint8_t ModuleAlarmExtension::alarmStatus()
{
    lu_uint8_t result = 0;

    if(alarmsInhibit == LU_TRUE)
    {
        return 0;
    }
    MasterLockingMutex lMutex(alarmMutex);
    for(ModuleAlarmList::iterator it = alarmList.begin(),
                                 end = alarmList.end(); it != end; ++it)
    {
        if( (*it)->isActive() == LU_TRUE)
        {
            //Active alarm found: enable related bit
            switch ((*it)->severity)
            {
                case SYS_ALARM_SEVERITY_CRITICAL:
                    result |= MODULE_BOARD_ERROR_ALARM_CRITICAL;
                    break;
                case SYS_ALARM_SEVERITY_ERROR:
                    result |= MODULE_BOARD_ERROR_ALARM_ERROR;
                    break;
                case SYS_ALARM_SEVERITY_WARNING:
                    result |= MODULE_BOARD_ERROR_ALARM_WARNING;
                    break;
                case SYS_ALARM_SEVERITY_INFO:
                    result |= MODULE_BOARD_ERROR_ALARM_INFO;
                    break;
                default:
                    break;
            }
        }
    }
    return result;
}


ModuleAlarm ModuleAlarmExtension::getAlarm(const ModuleAlarm::AlarmIDStr id)
{
    if(alarmsInhibit == LU_FALSE)
    {
        MasterLockingMutex lMutex(alarmMutex);
        /* Search alarmList vector for alarms with this severity */
        for(ModuleAlarmList::iterator it = alarmList.begin(), end = alarmList.end(); it != end; ++it)
        {
            if( (*it)->checkID(id) == LU_TRUE)
            {
                //Alarm found
                return *(*it);
            }
        }
    }
    ModuleAlarm result;
    return result;  //invalidated element
}


ModuleAlarmLister ModuleAlarmExtension::getAlarms()
{
    ModuleAlarmLister lister;   //List handler to return

    if(alarmsInhibit == LU_TRUE)
    {
        return lister;  //already empty list
    }
    MasterLockingMutex lMutex(alarmMutex);
    lister.addList(alarmList);
    return lister;
}


ModuleAlarmLister ModuleAlarmExtension::getAlarms(const SYS_ALARM_SEVERITY severity)
{
    ModuleAlarmLister lister;
    ModuleAlarmList sevAlarmList = getAlarmList(severity);
    lister.addList(sevAlarmList);
    return lister;
}


IOM_ERROR ModuleAlarmExtension::activateAlarm(const SYS_ALARM_SUBSYSTEM subSystem,
                                              const lu_uint32_t alarmCode
                                              )
{
    ModuleAlarm::AlarmIDStr id(subSystem, alarmCode);
    return activateAlarm(id);
}

IOM_ERROR ModuleAlarmExtension::activateAlarm(const SYS_ALARM_SUBSYSTEM subSystem,
                                              const lu_uint32_t alarmCode,
                                              const SYS_ALARM_SEVERITY  severity,
                                              const lu_uint16_t         parameter
                                              )
{
    ModuleAlarm::AlarmIDStr id(subSystem, alarmCode);
    return activateAlarm(id, severity, parameter);
}

IOM_ERROR ModuleAlarmExtension::activateAlarm(const ModuleAlarm::AlarmIDStr id)
{
    MasterLockingMutex lMutex(alarmMutex);
    for(ModuleAlarmList::iterator it = alarmList.begin(), end = alarmList.end(); it != end; ++it)
    {
        if( (*it)->checkID(id) == LU_TRUE)
        {
            //Alarm found: update
            (*it)->activate(); //Update alarm state
            return IOM_ERROR_NONE;  //stop search
        }
    }
    //not found: add new one
    ModuleAlarm* newAlarm = ModuleAlarmFactory::createAlarm(id);
    if(newAlarm != NULL)
    {
        //Alarm is a registered (valid) one: add to the list
        newAlarm->activate();
        alarmList.push_back(newAlarm);
    }
    else
    {
        /* Error creating alarm that the factory doesn't have */
        return IOM_ERROR_NOT_FOUND;
    }
    return IOM_ERROR_NONE;
}

IOM_ERROR ModuleAlarmExtension::activateAlarm(const ModuleAlarm::AlarmIDStr id,
                                              const SYS_ALARM_SEVERITY  severity,
                                              const lu_uint16_t         parameter)
{
    if(id.isValid() == LU_FALSE)
        return IOM_ERROR_PARAM;

    MasterLockingMutex lMutex(alarmMutex);
    for(ModuleAlarmList::iterator it = alarmList.begin(), end = alarmList.end(); it != end; ++it)
    {
        if( (*it)->checkID(id) == LU_TRUE)
        {
            //Alarm found: update
            (*it)->activate(); //Update alarm state
            (*it)->severity = severity;
            (*it)->parameter = parameter;
            return IOM_ERROR_NONE;  //stop search
        }
    }
    //not found: add new one
    ModuleAlarm* newAlarm = ModuleAlarmFactory::createAlarm(id);
    if(newAlarm != NULL)
    {
        //Alarm is a registered (valid) one: add to the list
        newAlarm->severity = severity;
        newAlarm->parameter = parameter;
        newAlarm->activate();
        alarmList.push_back(newAlarm);
    }
    else
    {
        /* Error creating alarm that the factory doesn't have */
        return IOM_ERROR_NOT_FOUND;
    }
    return IOM_ERROR_NONE;
}


void ModuleAlarmExtension::deactivateAlarm(const SYS_ALARM_SUBSYSTEM subSystem,
                                           const lu_uint32_t alarmCode)
{
    ModuleAlarm::AlarmIDStr id(subSystem, alarmCode);
    deactivateAlarm(id);
}

void ModuleAlarmExtension::deactivateAlarm(const ModuleAlarm::AlarmIDStr id)
{
    if(id.isValid() == LU_FALSE)
        return; //not valid: do nothing

    MasterLockingMutex lMutex(alarmMutex);
    for(ModuleAlarmList::iterator it = alarmList.begin(), end = alarmList.end(); it != end; ++it)
    {
        if( (*it)->checkID(id) == LU_TRUE)
        {
            //Alarm found: update
            (*it)->deactivate(); //Update alarm state
            return;  //stop search
        }
    }
    return; //not found: do nothing
}


IOM_ERROR ModuleAlarmExtension::ackAlarms()
{
    MasterLockingMutex lMutex(alarmMutex);
    /* Notify to all alarms in the list */
    for(ModuleAlarmList::iterator it = alarmList.begin(), end = alarmList.end(); it != end; ++it)
    {
        (*it)->acknowledge();  //acknowledge alarm
    }
    return IOM_ERROR_NONE;
}


IOM_ERROR ModuleAlarmExtension::ackAlarm(const ModuleAlarm::AlarmIDStr id)
{
    if(id.isValid() == LU_FALSE)
        return IOM_ERROR_PARAM;

    MasterLockingMutex lMutex(alarmMutex);
    for(ModuleAlarmList::iterator it = alarmList.begin(), end = alarmList.end(); it != end; ++it)
    {
        if( (*it)->checkID(id) == LU_TRUE)
        {
            //Alarm found: update
            (*it)->acknowledge(); //Update alarm state
            return IOM_ERROR_NONE;  //stop search
        }
    }
    return IOM_ERROR_NONE; //not found: do nothing
}


IOM_ERROR ModuleAlarmExtension::clearAlarm(const ModuleAlarm::AlarmIDStr id)
{
    MasterLockingMutex lMutex(alarmMutex);
    for(ModuleAlarmList::iterator it = alarmList.begin(), end = alarmList.end(); it != end; ++it)
    {
        if( (*it)->checkID(id) == LU_TRUE)
        {
            //Alarm found: remove from list (mark for deletion)
            (*it)->invalidate();
            return IOM_ERROR_NONE;  //stop search
        }
    }
    return IOM_ERROR_NONE; //not found: do nothing
}


void ModuleAlarmExtension::inhibitAlarms()
{
    alarmsInhibit = LU_TRUE;
}


void ModuleAlarmExtension::grantAlarms()
{
    if(alarmsInhibit == LU_TRUE)
    {
        MasterLockingMutex lMutex(alarmMutex);
        //Clear all invalid or inactive alarms
        for(ModuleAlarmList::iterator it = alarmList.begin(), end = alarmList.end(); it != end; ++it)
        {
            if( (*it)->getState() == SYS_ALARM_STATE_REACTIVATE_LATCHED)
            {
                //Reactivated. We need to leave it as Activated, so we delete the old and add as new
                ModuleAlarm* mAlarm = new ModuleAlarm( (*it)->getID() );
                mAlarm->severity = (*it)->severity;
                mAlarm->parameter = (*it)->parameter;
                delete (*it);   //remove old
                *it = mAlarm;   //add new
                activateAlarm(mAlarm->getID(), mAlarm->severity, mAlarm->parameter);   //add as new
            }
            else
            if( (*it)->isActive() == LU_FALSE )
            {
                //Clear and remove from list
                (*it)->invalidate();    //mark for deletion
            }
        }
        packAlarms();
        alarmsInhibit = LU_FALSE; //grant alarm listing
    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
ModuleAlarmList ModuleAlarmExtension::getAlarmList(const SYS_ALARM_SEVERITY severity)
{
    if( (alarmsInhibit == LU_TRUE) || (severity >= SYS_ALARM_SEVERITY_LAST) )
    {
        return emptyAlarmList;
    }
    ModuleAlarmList sevAlarmList;

    /* Search alarmList vector for alarms with this severity */
    for(ModuleAlarmList::iterator it = alarmList.begin(), end = alarmList.end(); it != end; ++it)
    {
        if( (*it)->severity == severity )
        {
            //found alarm
            sevAlarmList.push_back(*it);    //Add to return list
        }
    }
    return sevAlarmList;
}


void ModuleAlarmExtension::packAlarms()
{
    MasterLockingMutex lMutex(alarmMutex);

    //We do not use iterators here to avoid iterator re-adjustment
    size_t j = 0;
    for (size_t i = 0; i < alarmList.size(); ++i)
    {
        if( alarmList[i]->isValid() == LU_TRUE )    //copy valid
        {
            alarmList[j++] = alarmList[i];    //copies a pointer and overrides invalid ones
        }
        else
        {
            delete alarmList[i];    //remove this element
        }
    }
    alarmList.resize(j); //Trim vector to new size, discarding copies of pointers
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
