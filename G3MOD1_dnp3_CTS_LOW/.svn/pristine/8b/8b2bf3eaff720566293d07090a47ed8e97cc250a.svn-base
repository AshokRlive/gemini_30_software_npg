/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: OutputCANChannel.cpp 9 Oct 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/CANIOModule/src/channel/OutputCANChannel.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 9 Oct 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   9 Oct 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "OutputCANChannel.h"
#include "CANIOModule.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

OutputCANChannel::OutputCANChannel(CHANNEL_TYPE type, lu_uint16_t ID, CANIOModule* board) :
                                                                CANChannel(type, ID, board)
{}


void OutputCANChannel::setActive(lu_bool_t onlineStatus)
{
    /* Output channels are active (online) when its module is online */
    LU_UNUSED(onlineStatus);
    //Output channels do not require to update observers
}


lu_bool_t OutputCANChannel::isActive()
{
    /* Output channels are active (online) when its module is online */
    if(board != NULL)
    {
        IOModuleInfoStr modInfo;
        board->getInfo(modInfo);
        flags.online = modInfo.status.active;
    }
    else
    {
        flags.online = LU_FALSE;
    }
    return flags.online;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
IOM_ERROR OutputCANChannel::configure()
{
    /* Default implementation: Do Nothing */
    return IOM_ERROR_NONE;
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
