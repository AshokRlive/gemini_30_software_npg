/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: c_template.c 1577 2012-07-23 20:18:25Z fryers_j $
 *               $HeadURL:
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1577 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-07-23 21:18:25 +0100 (Mon, 23 Jul 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14/11/14     venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <windows.h>
#include <conio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "mainDll.h"
#include "CmdParser.h"
#include "ModuleProtocol.h"
#include "CANProtocol.h"
#include "ADE78xxAPI.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define ADE78XX_24ZP_SIGNUM     (0x08000000)
#define ADE78XX_24ZP_PAD        (0xFF000000)

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

lu_uint32_t *readRegValue;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR FPMV2ADE78xxSPIEnable( MODULE      module, 
							    MODULE_ID   moduleId,
								lu_uint8_t  sspBus,
								lu_uint8_t  csPort,
								lu_uint8_t  csPin,
								lu_uint8_t  pm0Port,
								lu_uint8_t  pm0Pin,
								lu_uint8_t  pm1Port,
								lu_uint8_t  pm1Pin,
								lu_uint8_t  resetPort,
								lu_uint8_t  resetPin,
								lu_uint8_t  irq0Port,
								lu_uint8_t  irq0Pin,
								lu_uint8_t  irq1Port,
								lu_uint8_t  irq1Pin
						     )
{
	SB_ERROR retError;
	TestADE78xxSPIEnableStr ade78xxSpiParam;

	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);

	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BLTST_1;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BLTST_1_ADE78xx_ENABLE_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	ade78xxSpiParam.sspBus     = sspBus;
	ade78xxSpiParam.csPort     = csPort;
	ade78xxSpiParam.csPin      = csPin;
	ade78xxSpiParam.pm0Port    = pm0Port;
	ade78xxSpiParam.pm0Pin     = pm0Pin;
	ade78xxSpiParam.pm1Port    = pm1Port;
	ade78xxSpiParam.pm1Pin     = pm1Pin;
	ade78xxSpiParam.resetPort  = resetPort;
	ade78xxSpiParam.resetPin   = resetPin;
	ade78xxSpiParam.irq0Port   = irq0Port;
	ade78xxSpiParam.irq0Pin    = irq0Pin;
	ade78xxSpiParam.irq1Port   = irq1Port;
	ade78xxSpiParam.irq1Pin    = irq1Pin;
		
	CANMsgFiller((lu_uint8_t*)&ade78xxSpiParam,sizeof(TestADE78xxSPIEnableStr));

	retError = LucyCANSendMainDll();
		
	return retError;
}

SB_ERROR FPMV2ADE78xxSPIReadReg( MODULE       module, 
								 MODULE_ID    moduleId,
								 lu_uint8_t   sspBus,
								 lu_uint8_t   csPort,
								 lu_uint8_t   csPin,
								 lu_uint16_t  reg,
								 lu_uint8_t   length,
								 lu_uint32_t  *readValuePtr
						       )
{
	SB_ERROR retError;
	TestADE78xxSPIReadRegStr readRegParam;

	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);

	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BLTST_1;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BLTST_1_ADE78xx_READ_REG_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	readRegParam.sspBus  = sspBus;
	readRegParam.csPort  = csPort;
	readRegParam.csPin   = csPin;
	readRegParam.reg     = reg;
	readRegParam.length  = length;
	readRegParam.value   = 0;
		
	CANMsgFiller((lu_uint8_t*)&readRegParam,sizeof(TestADE78xxSPIReadRegStr));

	retError = LucyCANSendMainDll();

	*readValuePtr = *readRegValue;
	
	return retError;
}

SB_ERROR FPMV2ADE78xxSPIWriteReg( MODULE       module, 
								  MODULE_ID    moduleId,
								  lu_uint8_t   sspBus,
								  lu_uint8_t   csPort,
								  lu_uint8_t   csPin,
								  lu_uint16_t  reg,
								  lu_uint8_t   length,
							      lu_uint32_t  value
						        )
{
	SB_ERROR retError;
	TestADE78xxSPIWriteRegStr writeRegParam;
	lu_uint16_t byteCount = 0;

	moduleId       = 0;
	
	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);

	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BLTST_1;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BLTST_1_ADE78xx_WRITE_REG_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	writeRegParam.sspBus = sspBus;
	writeRegParam.csPort = csPort;
	writeRegParam.csPin  = csPin;
	writeRegParam.reg    = reg;
	writeRegParam.length = length; 
	writeRegParam.value  = value;
		
	memcpy(&argsFromCommand.msgBuff[0], &writeRegParam, sizeof(TestADE78xxSPIWriteRegStr));
	
	argsFromCommand.msgBuffLen = sizeof(TestADE78xxSPIWriteRegStr);

	retError = LucyCANSendMainDll();
		
	return retError;
}

SB_ERROR FPMV2TestsCANProtocolDll(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR retError = SB_ERROR_PARAM;
		
	LU_UNUSED(time);

	if(flagDecoderPtr->parserFlag == PARSER_FLAG_SINGLE_COMMAND)
	{
		switch (msgPtr->messageType)
		{
		case MODULE_MSG_TYPE_BLTST_1:
			switch(msgPtr->messageID)
			{
				case MODULE_MSG_ID_BLTST_1_ADE78xx_ENABLE_R: 
					if(msgPtr->msgLen == sizeof(lu_uint8_t))
						{
							retError = (SB_ERROR)(*msgPtr->msgBufPtr);
						}
					break;

				case MODULE_MSG_ID_BLTST_1_ADE78xx_READ_REG_R: 
					if(msgPtr->msgLen == sizeof(lu_uint32_t))
						{
							readRegValue = (lu_uint32_t*)msgPtr->msgBufPtr;
							retError = SB_ERROR_NONE;
						}
					break;

				case MODULE_MSG_ID_BLTST_1_ADE78xx_WRITE_REG_R: 
					if(msgPtr->msgLen == sizeof(lu_uint8_t))
						{
							retError = (SB_ERROR)(*msgPtr->msgBufPtr);
						}
					break;
				
				default:
						retError = SB_ERROR_CANC_NOT_HANDLED;
					break;
			}
			break;

			default:
					retError = SB_ERROR_CANC_NOT_HANDLED;
				break;
		}
	  }
	else
	{
		retError = SB_ERROR_CANC_NOT_HANDLED;
	}
	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
*/