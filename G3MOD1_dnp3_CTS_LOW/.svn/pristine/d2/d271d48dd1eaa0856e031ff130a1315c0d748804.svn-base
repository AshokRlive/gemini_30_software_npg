/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.console;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTextPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import org.apache.log4j.Logger;


/**
 * A component for displaying messages and accepting users command input.
 */
public class SimpleConsole implements ActionListener, IConsole{

  private static final String STYLE_ERROR = "error";
  private static final String STYLE_SUCCESS = "success";
  private static final String STYLE_REGULAR = "regular";
  private static final String STYLE_TIME = "time";

  private final Logger log;

  private final StyledDocument styleDoc;

  private SimpleDateFormat timeFormatter = new SimpleDateFormat("h:mm:s:S a");

  private final JTextPane outputPane = new JTextPane();

  private final JMenuItem btnClear = new JMenuItem("Clear");
  private final JMenuItem btnCopy = new JMenuItem("Copy");

  private String successMark = "Command Accepted";

  public SimpleConsole() {
    this(Logger.getLogger(SimpleConsole.class));
  }

  public SimpleConsole(Logger log) {
    if(log == null)
      throw new IllegalArgumentException("log must not be null");
    this.log = log;

    outputPane.setEditable(false);
    outputPane.setPreferredSize(new Dimension(200, 200));
    JPopupMenu popup = new JPopupMenu();
    popup.add(btnClear);
    popup.add(btnCopy);
    outputPane.setComponentPopupMenu(popup);
    btnClear.addActionListener(this);
    btnCopy.addActionListener(this);

    styleDoc = outputPane.getStyledDocument();
    initDocStyles(styleDoc);
    styleDoc.addDocumentListener(new DocumentListener() {

      @Override
      public void insertUpdate(DocumentEvent e) {
        Document d = e.getDocument();
        outputPane.select(d.getLength(), d.getLength());
      }

      @Override
      public void removeUpdate(DocumentEvent e) {
      }

      @Override
      public void changedUpdate(DocumentEvent e) {
      }
    });
  }

  private void initDocStyles(StyledDocument doc) {
    Style def = StyleContext.getDefaultStyleContext().
        getStyle(StyleContext.DEFAULT_STYLE);

    Style regular = doc.addStyle(STYLE_REGULAR, def);
    StyleConstants.setFontSize(regular, 11);
    StyleConstants.setFontFamily(regular, "Arial");

    Style s = doc.addStyle("italic", regular);
    StyleConstants.setItalic(s, true);

    s = doc.addStyle("bold", regular);
    StyleConstants.setBold(s, true);

    s = doc.addStyle(STYLE_ERROR, regular);
    StyleConstants.setForeground(s, Color.red);

    s = doc.addStyle(STYLE_SUCCESS, regular);
    StyleConstants.setForeground(s, Color.GREEN.darker());

    s = doc.addStyle(STYLE_TIME, regular);
    StyleConstants.setFontFamily(s, Font.MONOSPACED);
  }

  // Gets current system time
  private String getTime() {
    return String.format("[%-14s] - ", timeFormatter.format(Calendar.getInstance().getTime()));
  }

  @Override
  public JTextPane getComponent() {
    return outputPane;
  }

  @Override
  public void appendMsg(String msg) {
    if (msg == null || msg.trim().isEmpty()) {
      return;
    }
    msg = removeTerminator(msg);
    log.info(msg);

    try {
      styleDoc.insertString(styleDoc.getLength(), getTime(), styleDoc.getStyle(STYLE_TIME));
      styleDoc.insertString(styleDoc.getLength(), msg + "\n", styleDoc.getStyle(STYLE_REGULAR));
    } catch (BadLocationException e) {
      e.printStackTrace();
    }
  }
  @Override
  public void appendSuccess(String msg) {
    if (msg == null || msg.trim().isEmpty()) {
      return;
    }
    
    msg = removeTerminator(msg);
    
    log.info(msg);

    try {
      styleDoc.insertString(styleDoc.getLength(), getTime(), styleDoc.getStyle(STYLE_TIME));
      styleDoc.insertString(styleDoc.getLength(), msg, styleDoc.getStyle(STYLE_REGULAR));
      
      if(successMark != null && !successMark.trim().isEmpty())
        styleDoc.insertString(styleDoc.getLength(), " ["+successMark+"]\n", styleDoc.getStyle(STYLE_SUCCESS));
      else
        styleDoc.insertString(styleDoc.getLength(), " \n", styleDoc.getStyle(STYLE_SUCCESS));
    } catch (BadLocationException e) {
      e.printStackTrace();
    }
  }

  private String removeTerminator(String msg) {
    msg = msg.replace("\n", "").replace("\r", "");
    return msg.substring(0, 1).toUpperCase() + msg.substring(1);
  }
  @Override
  public void appendBlankLine() {
    try {
      styleDoc.insertString(styleDoc.getLength(), "\n", styleDoc.getStyle(STYLE_REGULAR));
    } catch (BadLocationException e) {
      log.error(e);
    }
  }
  @Override
  public void appendErr(String msg) {
    if (msg == null || msg.trim().isEmpty()) {
      return;
    }
    
    msg = removeTerminator(msg);
    log.error(msg);

    try {
      styleDoc.insertString(styleDoc.getLength(), getTime(), styleDoc.getStyle(STYLE_TIME));
      styleDoc.insertString(styleDoc.getLength(), msg + "\n", styleDoc.getStyle(STYLE_ERROR));
    } catch (BadLocationException e) {
      log.error(e);
    }
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    Object source = e.getSource();
    if (source == btnClear) {
      outputPane.setText("");
    }
    else if (source == btnCopy) {
      String content = outputPane.getSelectedText();
      if(content == null || content.isEmpty())
        content = outputPane.getText();
      
      if (!content.isEmpty()) {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Clipboard clipboard = toolkit.getSystemClipboard();
        StringSelection strSel = new StringSelection(content);
        clipboard.setContents(strSel, null);
      }
    }
  }

  /**
   * 
   * @param mark a message appended to success message.
   */
  public void setSuccessMark(String mark) {
    this.successMark = mark;
  }

}
