/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       HMI buttons handler public interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24/02/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_3B5A0A03_E7C8_4ec4_B2EC_504A61E9BCBF__INCLUDED_)
#define EA_3B5A0A03_E7C8_4ec4_B2EC_504A61E9BCBF__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "HMIManagerCommon.h"

/* Forward Declaration */
class ScreenManager;


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Convert a RAW button event from the HMI module channel to an
 * internal "high level" event.
 *
 * The mediator design pattern has been used.
 * The ScreenManager class is the mediator
 */
class ButtonHandler
{

public:
    /**
     * \brief Custom constructor
     *
     * \pram mediator Reference to the mediator
     *
     * \return None
     */
    ButtonHandler(ScreenManager &mediator);
    virtual ~ButtonHandler();

    /**
     * \brief New RAW event
     *
     * If a new event is detected the mediator is notified
     *
     * \param state RAW button state. Its a bitmask. See HMI_BUTTON_BIT_MASK enum
     *
     * \return None
     */
    void buttonEventRAW(lu_uint32_t state);

private:
    ScreenManager &mediator;

    lu_uint32_t RAWState;
    buttonStatus buttonsStatus[HMI_M_BUTTON_IDX_LAST];

};
#endif // !defined(EA_3B5A0A03_E7C8_4ec4_B2EC_504A61E9BCBF__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
