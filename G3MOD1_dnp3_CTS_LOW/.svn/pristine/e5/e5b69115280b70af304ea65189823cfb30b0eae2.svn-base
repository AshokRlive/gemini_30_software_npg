/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.ui.wizard.addcomms;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceType;


class SelectCommsTypePage extends WizardPage {
  public final static String KEY_SELECTED_TYPE ="selectedType";
  
  public SelectCommsTypePage() {
    super("Select a type");
    
    initComponents();
  }

  private void initComponents() {
    DefaultFormBuilder builder = AddCommsDeviceWizard.createBuilder(this);
    
    ButtonGroup bg = new ButtonGroup();
    
    CommsDeviceType[] types = CommsDeviceType.values();
    JRadioButton radio;
    for (int i = 0; i < types.length; i++) {
      radio = new JRadioButton(types[i].getDescription());
      bg.add(radio);
      radio.setName(types[i].name());
      radio.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          JRadioButton r = (JRadioButton) e.getSource();
          if(r.isSelected()) {
            CommsDeviceType t = CommsDeviceType.valueOf(r.getName());
            putWizardData(KEY_SELECTED_TYPE, t);
          }
        }
      });
      builder.append(i ==0 ? "Device Type:":"", radio);
      builder.nextLine();
    }
  }
  
}

