/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.cert.manager.gui;

import static com.lucy.g3.cert.manager.gui.Resources.RB;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Window;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;

import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;


/**
 * Modal dialog used for entering and confirming a password.
 */
public class DGetNewPassword
    extends AbstractDialog
{
	/** First password entry password field */
	private JPasswordField m_jpfFirst;

	/** Password confirmation entry password field */
	private JPasswordField m_jpfConfirm;

	/** Stores new password entered */
	private char[] m_cPassword;

  private JPanel contentPanel = new JPanel(new BorderLayout());

	/**
	 * Creates new DGetNewPassword dialog.
	 * 
	 * @param parent Parent window
	 * @param sTitle The dialog's title
	 */
	public DGetNewPassword(Window parent, String sTitle)
	{
		super(parent);
		setTitle((sTitle == null) ? RB.getString("DGetNewPassword.Title") : sTitle);
		setModal(true);
		initComponents();
		pack();
	}

	/**
	 * Get the password set in the dialog.
	 * 
	 * @return The password or null if none was set
	 */
	public char[] getPassword()
	{
		if (m_cPassword == null)
		{
			return null;
		}
		char[] copy = new char[m_cPassword.length];
		System.arraycopy(m_cPassword, 0, copy, 0, copy.length);
		return copy;
	}

	/**
	 * Initialize the dialog's GUI components.
	 */
	private void initComponents()
	{
		getContentPane().setLayout(new BorderLayout());

		JLabel jlFirst = new JLabel(RB.getString("DGetNewPassword.jlFirst.text"));
		JLabel jlConfirm = new JLabel(RB.getString("DGetNewPassword.jlConfirm.text"));
		m_jpfFirst = new JPasswordField(15);
		m_jpfConfirm = new JPasswordField(15);
		jlFirst.setLabelFor(m_jpfFirst);
		jlConfirm.setLabelFor(m_jpfConfirm);

		JPanel jpPassword = new JPanel(new GridLayout(2, 2, 5, 5));
		jpPassword.add(jlFirst);
		jpPassword.add(m_jpfFirst);
		jpPassword.add(jlConfirm);
		jpPassword.add(m_jpfConfirm);
		jpPassword.setBorder(new EmptyBorder(5, 5, 5, 5));

		contentPanel.add(jpPassword, BorderLayout.CENTER);
	}

	/**
	 * Check for the following:
	 * <ul>
	 * <li>That the user has supplied and confirmed a password.
	 * <li>That the passwords match.
	 * </ul>
	 * Store the new password in this object.
	 * 
	 * @return True, if the user's dialog entry matches the above criteria, false otherwise
	 */
	private boolean checkPassword()
	{
		String sFirstPassword = new String(m_jpfFirst.getPassword());
		String sConfirmPassword = new String(m_jpfConfirm.getPassword());

		if (sFirstPassword.equals(sConfirmPassword))
		{
			m_cPassword = sFirstPassword.toCharArray();
			return true;
		}

		JOptionPane.showMessageDialog(this, RB.getString("PasswordsNoMatch.message"), getTitle(),
		    JOptionPane.WARNING_MESSAGE);

		return false;
	}

	@Override
	public void ok()
	{
		if (checkPassword())
		{
			super.ok();
		}
	}

  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {
    return contentPanel;
  }

  @Override
  protected JPanel createButtonPanel() {
    return super.createOKCancelButtonPanel();
  }
}
