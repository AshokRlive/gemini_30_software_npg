/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.login;

import java.nio.ByteBuffer;

import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.ReplyPayload;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.shared.LoginResult;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.USER_LEVEL;


/**
 * The Class PL_Reply_Login.
 */
class PL_Reply_Login extends ReplyPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_R_Login;

  private LoginResult loginResult;


  PL_Reply_Login() {
    super(PAYLOAD_SIZE);
  }

  public LoginResult getLoginResult() {
    return loginResult;
  }

  @Override
  public void parseBytes(byte[] payload) throws SerializationException {
    

    ByteBuffer buf = createByteBuffer(payload);

    // Read user level
    short userLevelValue = NumUtils.convert2Unsigned(buf.get());
    USER_LEVEL level = USER_LEVEL.forValue(userLevelValue);
    if (level == null) {
      log.error("Unrecognized user level enum value:" + userLevelValue);
    }

    // Read session ID
    short sessionID = buf.getShort();
    loginResult = new LoginResult(level, sessionID);
  }

  @Override
  public String toString() {
    return "PL_Reply_Login [loginResult=" + loginResult + "]";
  }
}