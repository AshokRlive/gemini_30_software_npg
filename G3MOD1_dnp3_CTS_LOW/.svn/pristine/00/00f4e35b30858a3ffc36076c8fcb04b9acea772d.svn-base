/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdp.model.impl;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.lucy.g3.common.utils.ZipUtil;
import com.lucy.g3.common.version.FeatureVersion;
import com.lucy.g3.common.version.ReleaseType;
import com.lucy.g3.common.version.SoftwareVersion;
import com.lucy.g3.common.version.SystemAPIVersion;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.SLAVE_IMAGE_TYPE;
import com.lucy.g3.xml.gen.common.versions.VERSION_TYPE;

/**
 * A factory for creating SoftwareEntry objects.
 */
public final class SoftwareEntryFactory {

  public static final String MCM_VERSION_FILE = "version.properties";

  public static final String VERSION_PATTERN = "[TDR]\\d+\\.\\d+\\.\\d+";
  public static final String SYSEM_API_PATTERN = "\\d+\\.\\d+";

  private static Pattern numberPtn = Pattern.compile("\\d+");

  private static final int SLAVE_IMAGE_BASE_OFFSET = 0xcc;

  private static final int MODULE_TYPE_OFFSET = SLAVE_IMAGE_BASE_OFFSET + 4 + 4 + 4 + 4 + 2;

  private static final int FEATURE_OFFSET = 10;


  private SoftwareEntryFactory() {
  }

  // Read an integer from litter endian stream
  private static int readInt(InputStream in) throws IOException {
    int ch1 = in.read();
    int ch2 = in.read();
    int ch3 = in.read();
    int ch4 = in.read();
    if ((ch1 | ch2 | ch3 | ch4) < 0)
      throw new EOFException("Invliad length of input stream");
    return ((ch4 << 24) + (ch3 << 16) + (ch2 << 8) + (ch1 << 0));
  }

  // Read a short from litter endian stream
  private static short readShort(InputStream in) throws IOException {
    int ch1 = in.read();
    int ch2 = in.read();
    if ((ch1 | ch2) < 0)
      throw new EOFException("Invliad length of input stream");
    return (short) ((ch2 << 8) + (ch1 << 0));
  }

  public static SoftwareEntry create(File swFile) throws Exception {
    if(swFile == null)
      throw new IllegalArgumentException("swFile must not be null");

    if (!swFile.isFile() || !swFile.exists()) {
      throw new IllegalArgumentException("Firmware file \"" + swFile.getAbsolutePath() + "\" is not found");
    }

    String name = swFile.getName().toLowerCase();
    if (name.endsWith(".zip")) {
      return createMCMEntry(swFile);
      
    } else if (name.endsWith(".bin")) {
      return createSlaveEntry(swFile);
      
    } else
      throw new IllegalArgumentException("Unrecognized file extension: " + name);
  }

  private static SoftwareEntry createSlaveEntry(File slavefile) throws Exception {
    FileInputStream is = null;
    try {
      is = new FileInputStream(slavefile);
      BufferedInputStream in = new BufferedInputStream(is);

      int skip = MODULE_TYPE_OFFSET;
      long skipped = in.skip(skip);
      assert skipped == skip;

      MODULE type = MODULE.forValue(readShort(in));

      skip = FEATURE_OFFSET;
      skipped = in.skip(skip);
      assert skipped == skip;

      SLAVE_IMAGE_TYPE slaveType = SLAVE_IMAGE_TYPE.forValue(readShort(in));

      FeatureVersion feature = new FeatureVersion(readInt(in), readInt(in));

      SystemAPIVersion systemAPI = new SystemAPIVersion(readInt(in), readInt(in));

      VERSION_TYPE relType = VERSION_TYPE.forValue(in.read());
      SoftwareVersion version = new SoftwareVersion(
          new ReleaseType(relType.getDescription(), relType.getValue())
          , readInt(in), readInt(in), in.read());

      SoftwareEntry entry = new SoftwareEntry(slavefile.getName(), type, version, systemAPI, slaveType, feature);
      entry.setEntryFile(slavefile);
      
      return entry;

    } catch (Exception e) {
      e.printStackTrace();
      throw e;

    } finally {
      try {
        is.close();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  private static SoftwareEntry createMCMEntry(File mcmFile)
      throws Exception {
    byte[] versionStream = ZipUtil.getZipEntryStreamByName(MCM_VERSION_FILE, mcmFile);

    if (versionStream == null || versionStream.length <= 0) {
      throw new Exception("Invalid MCM firmware file. \"" + MCM_VERSION_FILE + " is missing!");
    }
    Properties prop = new Properties();

    // load a properties file
    prop.load(new ByteArrayInputStream(versionStream));

    String mcmVersion = prop.getProperty("Version");
    String mcmSystemAPI = prop.getProperty("SystemAPI");
    String mcmFeature = prop.getProperty("Feature");

    return createMCMEntry(mcmFile, mcmVersion, mcmSystemAPI, mcmFeature);
  }

  private static SoftwareEntry createMCMEntry(File mcmFile, String mcmVersion, String mcmSystemAPI, String mcmFeature)
      throws Exception {

    mcmVersion = mcmVersion.trim();
    mcmSystemAPI = mcmSystemAPI.trim();
    mcmFeature = mcmFeature.trim();

    // Verify arguments
    if (mcmFile == null || !mcmFile.isFile() || !mcmFile.exists()) {
      throw new IllegalArgumentException("MCM firmware file \"" + mcmFile
          + "\" doesn't exist!");
    }

    if (!mcmVersion.matches(VERSION_PATTERN)) {
      throw new IllegalArgumentException("MCM firmware version is invalid: " + mcmVersion);
    }

    if (!mcmSystemAPI.matches(SYSEM_API_PATTERN)) {
      throw new IllegalArgumentException("MCM firmware system API is invalid: " + mcmSystemAPI);
    }

    if (!mcmFeature.matches(FeatureVersion.PATTERN)) {
      throw new IllegalArgumentException("MCM firmware feature is invalid: " + mcmFeature);
    }

    char relType = mcmVersion.charAt(0);

    int major = 0;
    int minor = 0;
    int patch = 0;

    // Parse Software Version
    Matcher matcher = numberPtn.matcher(mcmVersion);
    if (matcher.find())
      major = Integer.valueOf(matcher.group());
    if (matcher.find())
      minor = Integer.valueOf(matcher.group());
    if (matcher.find())
      patch = Integer.valueOf(matcher.group());
    
    VERSION_TYPE relEnum = convertLetter2VersionType(relType);
    SoftwareVersion version = new SoftwareVersion(
        new ReleaseType(relEnum.getDescription(), relEnum.getValue())
        , major, minor, patch);

    // Parse System API
    matcher = numberPtn.matcher(mcmSystemAPI);
    if (matcher.find())
      major = Integer.valueOf(matcher.group());
    if (matcher.find())
      minor = Integer.valueOf(matcher.group());
    SystemAPIVersion systemAPI = new SystemAPIVersion(major, minor);

    // Parse feature
    matcher = numberPtn.matcher(mcmFeature);
    if (matcher.find())
      major = Integer.valueOf(matcher.group());
    if (matcher.find())
      minor = Integer.valueOf(matcher.group());
    FeatureVersion feature = new FeatureVersion(major, minor);

    SoftwareEntry entry = new SoftwareEntry(mcmFile.getName(), MODULE.MODULE_MCM, version, systemAPI, 
        SLAVE_IMAGE_TYPE.SLAVE_IMAGE_TYPE_APPLICATION, feature);
    entry.setEntryFile(mcmFile);
    return entry;
  }

  private static VERSION_TYPE convertLetter2VersionType(char letter) {
    switch (letter) {
    case 'D':
      return VERSION_TYPE.VERSION_TYPE_DEBUG;

    case 'B':
      return VERSION_TYPE.VERSION_TYPE_BETA;

    case 'R':
      return VERSION_TYPE.VERSION_TYPE_RELEASE;

    case 'T':
      return VERSION_TYPE.VERSION_TYPE_TEST;

    case 'U':
      return VERSION_TYPE.VERSION_TYPE_UNKNOWN;

    default:
      return VERSION_TYPE.VERSION_TYPE_UNKNOWN;
    }

  }
}
