/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.workbench;

import java.awt.event.ActionListener;

import javax.swing.Action;
import javax.swing.JComponent;

/**
 *
 */
public interface IWorkbenchStatusBar {

  JComponent getComponent();
  
  void setHost(String hostAddress);
  String getHost();
  
  /**
   * Sets the current status.
   * @param status application running status.
   * @param mode application running mode.
   */
  void setStatus(Status status, Mode mode);
  
  /**
   * Sets actions to be triggered from a context menu.
   * @param contextActions actions to be displayed in a context menu when clicking on the status bar.
   */
  void setContextActions(Action[] contextActions);

  enum Mode {
    NONE(""),
    OUTSERVICE("Out of service"),
    NORMAL("Normal"),
    UPGRADE("Upgrade Mode"),
    DEBUG("Debugging");
    
    private final String name;

    private Mode(String name){
      this.name= name;
    }
    
    public String getName(){
      return name;
    }
  }
  
  enum Status {
    NONE("", ""),
    CONNECTED("Connected","The application is connected to RTU."),
    DISCONNECTED("Disconnected","The application is disconnected from RTU."),
    CONNECTION_LOST("Connection Failed", "Connection was failed.");
    
    private final String name;
    private final String descriptoin;

    private Status(String name, String description){
      this.name= name;
      this.descriptoin = description;
    }
    
    public String getDescription() {
      return descriptoin;
    }
    
    public String getName(){
      return name;
    }
    
  }

  /**
   * @param action action when click on security indicator.
   */
  void setSecurityActionListener(ActionListener action);

  /**
   * 
   * @param isSecure
   * @param tips
   */
  void setSecurityLabel(boolean isSecure, String tips);
}

