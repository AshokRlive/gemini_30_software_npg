/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Timed scheduler interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#if !defined(EA_81A255C7_4D0A_46cc_8862_072F6554D320__INCLUDED_)
#define EA_81A255C7_4D0A_46cc_8862_072F6554D320__INCLUDED_



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <vector>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "GlobalDefs.h"
#include "MainAppEnum.h"
#include "Timer.h"
#include "Thread.h"
#include "QueueMgr.h"
#include "QueueObj.h"
#include "Pipe.h"
#include "TimeManager.h"
#include "Logger.h"

/* Forward declaration */
class TimedJob;

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
typedef QueueMgr<TimedJob> TimedJobQueue;

enum TIMED_JOB_ERROR
{
    TIMED_JOB_ERROR_NONE = 0,
    TIMED_JOB_ERROR_PARAM   ,
    TIMED_JOB_ERROR_REMOVE  ,

    TIMED_JOB_ERROR_LAST
};

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/**
 * The job will be delayed for, at least, timer ms before being executed. If
 * timerInterval is different from zero the job will be rescheduled indefinitely
 * using timerInterval as a delay. A job can be cancelled and deallocated at
 * run time using the "cancel" function.
 */
class TimedJob : public QueueObj<TimedJob>
{
public:

    /**
     * \brief Custom constructor
     *
     * The initial timer is incremented by the scheduler timer granularity. This
     * is necessary to guarantee an initial timer expiration of, at least,
     * timer ms: the job is added to the scheduler queue at random time so the
     * tick may be already partially elapsed.
     * The timerInterval is left untouched because the job is
     * rescheduled exactly when the scheduler internal tick starts.
     *
     * \param timer Initial task delay in ms
     * \param timerInterval Periodic timer interval in ms. If 0 the job is
     * executed only one time and then removed form the scheduler and released
     */
    TimedJob(lu_uint32_t timer, lu_uint32_t timerInterval);

    virtual ~TimedJob() {};

	/**
	 * \brief Try to execute the scheduled operation
	 * 
	 * The user defined operation is executed only if the timer/timerInterval is
	 * elapsed. This function should be periodically called by the scheduler.
	 * 
	 * \param dTime Time in ms elapsed from the previous call
	 * \param timePtr Current absolute time
	 * 
	 * \return Error code. TIMED_JOB_ERROR_REMOVE forces the scheduler to remove the
	 * job from the scheduling queue
	 */
	TIMED_JOB_ERROR run(lu_uint32_t dTime, TimeManager::TimeStr* timePtr);

protected:
	/**
	 * \brief User define operation
	 * 
	 * This method is called by the run public method only if the configured
	 * timer is expired
	 * 
	 * \param timePtr Current absolute time
	 *
	 * \return none
	 */
    virtual void job(TimeManager::TimeStr* timePtr) = 0;

private:
    /** Initial timer interval in ms */
    lu_uint32_t timerInitial;
    /** Periodic timer interval in ms */
    lu_uint32_t timerInterval;
    lu_uint32_t counter;
};


/**
 * The queued jobs execution is delayed accordingly to the internal timer of the
 * job. If the job is a "one shoot" job the job is removed from the and deleted.
 * If it is periodic job, the job is rescheduled using the job interval time.
 * The granularity of the scheduler is defined by the JobSchedulerTick constant.
 */
class TimedJobScheduler : public Thread
{

public:
    /**
     * \param schedType Scheduler type used by the internal thread
     * \param priority thread priority
     */
    TimedJobScheduler(SCHED_TYPE schedType, lu_uint32_t priority);

    virtual ~TimedJobScheduler();

	/**
	 * \brief Add a new job to the scheduler queue.
	 * 
	 * Job is added to the job queue and will be run when appointed to.
	 *
	 * IMPORTANT NOTE: Object pointed will be DELETED after the job is finished.
	 *                  DO NOT use the job pointer -- except when using it for
	 *                  reference in order to request to cancel it.
	 *
	 * \param jobPtr pointer to the new job
	 * 
	 * \return error Code
	 */
    GDB_ERROR addJob(TimedJob* jobPtr);

    /**
     * \brief Remove a job from the scheduler queue.
     *
     * Job is removed and deleted before execution.
     * This method ensures a job cannot be deleted while running.
     * The method stores a request to cancel a Job, and the Job Scheduler thread
     * will be in charge of cancelling (do not run) and deleting the Job -- both
     * its entry in the job queue and the Job object itself.
     * If the given Job pointer is not valid, or the job is already finished,
     * nothing happens -- request is simply removed from the list if the job is
     * not in the job queue.
     *
     * IMPORTANT NOTE: Object pointed will be DELETED, and given pointer is set
     *                  to NULL.
     *
     * usage example:
     *      DerivedTimedJob* p = new DerivedTimedJob(1000, 0);
     *      addJob(p);
     *      cancelJob(p);
     *      //Now p is NULL, and the created object has been destroyed.
     *
     * \param jobPtr Pointer to the job.
     */
    template <typename TJob>
    void cancelJob(TJob*& jobPtr)
    {
        /* TODO: pueyos_a - check beforehand if the job is in the queue */
        if(jobPtr == NULL)
        {
            return; //Nothing to do
        }
        /* Please note that it is storing pointers and not objects */
        rqCancelPipe.writeP((lu_uint8_t*)(&jobPtr), sizeof(jobPtr));
        jobPtr = NULL;
    }


    /**
     * Scheduler periodic tick in ms
     */
    static const lu_uint32_t JobSchedulerTick = 100;

    THREAD_ERR stop();
    THREAD_ERR join();

protected:
    /**
     * \brief Thread main loop
     *
     * This function is called automatically when a thread is started
     */
    virtual void threadBody();

private:
    void getCancelRequests();

private:
    typedef std::vector<TimedJob*> JobVector;

private:
    TimedJobQueue jobs;
	Timer tickTimer;
	Logger& log;

	/* Job Cancel control */
	Pipe rqCancelPipe;      //Stores requests for cancelling a Job
	JobVector cancelList;   //List of jobs pending to be cancelled
};

#endif // !defined(EA_81A255C7_4D0A_46cc_8862_072F6554D320__INCLUDED_)


/*
 *********************** End of file ******************************************
 */
