/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name            Details
 *   --------------------------------------------------------------------------
 *  05/02/16      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "mainDll.h"
#include "CmdParser.h"
#include "ModuleProtocol.h"
#include "CANProtocol.h"
#include "I2CHumiditySensorTests.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

TestI2cTHumidityReadRspStr *humitidyReadResp;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR I2CHumiditySensorReadTemperatureDll( MODULE      module, 
										      MODULE_ID   moduleId, 
										      lu_uint8_t  chan,
											  lu_int32_t  *temperaturePtr	
											)
{
	SB_ERROR retError;
	TestI2cHumidityReadStr humidityRead; 
	
	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);
	
	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BLTST_1;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BLTST_1_I2C_HUMIDITY_READ_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	humidityRead.i2cChan = chan;
		
	CANMsgFiller((lu_uint8_t*)&humidityRead,sizeof(TestI2cHumidityReadStr));

	retError = LucyCANSendMainDll();

	if(humitidyReadResp != NULL)
	{
		*temperaturePtr = humitidyReadResp->temperature;
	}

	return retError;
}

SB_ERROR I2CHumiditySensorReadHumidityDll( MODULE      module, 
										   MODULE_ID   moduleId, 
										   lu_uint8_t  chan,
										   lu_uint32_t *humidityPtr
										 )
{
	SB_ERROR retError;
	TestI2cHumidityReadStr humidityRead; 
	
	/* Set defaults for Can Header */
	CANHeaderFiller(module, moduleId);
	
	argsFromCommand.CANHeader.messageType = MODULE_MSG_TYPE_BLTST_1;
	argsFromCommand.CANHeader.messageID   = MODULE_MSG_ID_BLTST_1_I2C_HUMIDITY_READ_C;

	argsFromCommand.parserFlag = PARSER_FLAG_SINGLE_COMMAND;

	humidityRead.i2cChan = chan;
		
	CANMsgFiller((lu_uint8_t*)&humidityRead,sizeof(TestI2cHumidityReadStr));

	retError = LucyCANSendMainDll();

	if(humitidyReadResp != NULL)
	{
		*humidityPtr = humitidyReadResp->humidity;
	}

	return retError;
}

SB_ERROR I2CHumiditySensorTestsCANProtocolDll(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR retError = SB_ERROR_NONE;
		
	LU_UNUSED(time);

	if(flagDecoderPtr->parserFlag == PARSER_FLAG_SINGLE_COMMAND)
	{
		switch (msgPtr->messageType)
		{
			case MODULE_MSG_TYPE_BLTST_1:
				switch(msgPtr->messageID)
				{
					case MODULE_MSG_ID_BLTST_1_I2C_HUMIDITY_READ_R: 
						if(msgPtr->msgLen == sizeof(TestI2cTHumidityReadRspStr))
						{
							humitidyReadResp = (TestI2cTHumidityReadRspStr*)msgPtr->msgBufPtr;
							retError = SB_ERROR_NONE;
						}
						break;

					default:
						retError = SB_ERROR_CANC_NOT_HANDLED;
						break;
				}
				break;
			default:
				printf("\n Error in Downloading ");
				retError = SB_ERROR_CANC_NOT_HANDLED;
				break;
		}
	}
	else
	{
		retError = SB_ERROR_CANC_NOT_HANDLED;
	}
	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */