/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _CAN_FRAMING_INCLUDED
#define _CAN_FRAMING_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "errorCodes.h"

#include "KvaserCan.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*!
 * Structure used to define a CAN message
 */
typedef struct CANFramingMsgDef
{
    /* Buffer ID. Used by the CAN Framing library */
    lu_uint8_t  ID         ;
    /* Message type */
    lu_uint8_t  messageType;
    /* Message ID */
    lu_uint8_t  messageID  ;
    /* Source address */
    lu_uint8_t  deviceIDSrc;
    /* Source device type */
    lu_uint8_t  deviceSrc  ;
    /* Destination address */
    lu_uint8_t  deviceIDDst;
    /* Destination device type */
    lu_uint8_t  deviceDst  ;

    /* Message length */
    lu_uint32_t  msgLen    ;
    /* Message buffer */
    lu_uint8_t *msgBufPtr  ;
}CANFramingMsgStr;

/*!
 * CAN Framing callback signature
 */
typedef void (*CANFramingCB)(CANFramingMsgStr *msgPtr);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Initialize CAN Framing modules
 *
 *   \param device Device type (It used for source field of the CAN header)
 *   \param deviceID Device address (It used for sourceID field of the
 *                   CAN header)
 *
 *   \return Error code
 *
 ******************************************************************************
 */
extern SB_ERROR CANFramingInit( lu_uint8_t        device    ,
		                        lu_uint8_t        deviceID  ,
								lu_uint32_t       *handlePtr
		                      );

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
extern SB_ERROR CANFramingSendMsg(CANFramingMsgStr *msgPtr);

/*!
 ******************************************************************************
 *   \brief Manage the reception of a fragment
 *
 *   Only one "long" message at a time can be received. When a full message is
 *   correctly received the message is forwarded. The "long" message buffer
 *   should be released using the CANFramingReleaseMsg function.
 *   This function should be called with the ISR.
 *
 *   \param CANMsgPtr Message received.
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
extern SB_ERROR CANFramingRecvFragment(KVASER_CAN_MSG_TYPE *CANMsgPtr);

extern CANFramingMsgStr* CANFramingRecvMsg(void);

#endif /* _CAN_FRAMING_INCLUDED */

/*
 *********************** End of file ******************************************
 */