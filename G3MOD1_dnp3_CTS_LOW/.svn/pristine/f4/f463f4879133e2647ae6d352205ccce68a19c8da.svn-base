/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.generator.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.apache.log4j.Logger;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicGenerator;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.clogic.ISupportOffLocalRemote;
import com.lucy.g3.rtu.config.clogic.batterytest.BatteryTest;
import com.lucy.g3.rtu.config.clogic.digitaloutputcontrol.DigitalOutputControl;
import com.lucy.g3.rtu.config.clogic.dummyswitch.DummySwitch;
import com.lucy.g3.rtu.config.clogic.fantest.FanTest;
import com.lucy.g3.rtu.config.clogic.fpireset.FPIReset;
import com.lucy.g3.rtu.config.clogic.fpitest.FPITest;
import com.lucy.g3.rtu.config.clogic.offlocalremote.OffLocalRemote;
import com.lucy.g3.rtu.config.clogic.switchgear.SwitchGearLogic;
import com.lucy.g3.rtu.config.clogic.utils.CLogicUtility;
import com.lucy.g3.rtu.config.constants.SwitchIndex;
import com.lucy.g3.rtu.config.generator.AbstractConfigGenerator;
import com.lucy.g3.rtu.config.generator.ConfigGeneratorRegistry;
import com.lucy.g3.rtu.config.generator.GenerateOptions;
import com.lucy.g3.rtu.config.generator.GenerateOptions.GenerateOption;
import com.lucy.g3.rtu.config.hmi.manager.HMIScreenManager;
import com.lucy.g3.rtu.config.hmi.ui.IHMIGenerator;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleDSM;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleFPM;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleIOM;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleMCM;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModulePSM;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleSCM;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleSCM_MK2;
import com.lucy.g3.rtu.config.module.iomodule.domain.IOModule;
import com.lucy.g3.rtu.config.module.iomodule.manager.IIOModuleGenerator;
import com.lucy.g3.rtu.config.module.res.ModuleEnums;
import com.lucy.g3.rtu.config.module.shared.ModuleRepository;
import com.lucy.g3.rtu.config.module.shared.domain.ISwitchModule;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.module.shared.domain.SwitchModuleOutput;
import com.lucy.g3.rtu.config.module.shared.manager.IModuleManager;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IStandardPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointManager;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.impl.AbstractAnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSet;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSetManager;
import com.lucy.g3.rtu.config.virtualpoint.shared.util.VirtualPointFinder;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdAnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.impl.VirtualPointFactory;
import com.lucy.g3.rtu.config.virtualpoint.standard.ui.page.IVirtualPointGenerator;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumFPM.FPM_CH_AINPUT;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumFPM.FPM_CH_FPI;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumPSM.PSM_CH_BCHARGER;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumPSM.PSM_CH_DINPUT;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumSCM.SCM_CH_DINPUT;
import com.lucy.g3.xml.gen.api.IChannelEnum;
import com.lucy.g3.xml.gen.api.IXmlEnum.DefaultCreateOption;
import com.lucy.g3.xml.gen.common.ControlLogicDef.DOL_OUTPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.OLR_POINT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.SGL_INPUT;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;


/**
 *
 */
class ConfigGenerator extends AbstractConfigGenerator 
implements ICLogicGenerator, IIOModuleGenerator,IVirtualPointGenerator{
  private Logger log = Logger.getLogger(ConfigGenerator.class);
  
  private CLogicManager manager;
  

  @Override
  public void setConfig(IConfig config) {
    super.setConfig(config);
    if(config!=null) {
      manager = config.getConfigModule(CLogicManager.CONFIG_MODULE_ID);
    }
  }

  @Override
  public void createBatteryChargerLogic(IChannel batChargerChnl) {
    addLogicToConfig(new BatteryTest(batChargerChnl));
  }
  
  @Override
  public void createCANModule(IModuleManager<?> manager, MODULE type, int quantity) {
    MODULE_ID[] ids = ModuleEnums.getSupportedIDs();

    for (int i = 0; i < quantity && i < ids.length; i++) {
      try {
        if(manager != null)
        manager.addModule(type, ids[i]);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  
  @Override
  public Module createCANModule(IModuleManager<?> manager, MODULE type, MODULE_ID id) {
    try {
      return manager.addModule(type, id);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
  
  @Override
  public void createCLogic(Module... module) {

    for (int i = 0; i < module.length; i++) {
      if (module[i] == null) {
        continue;
      }

      if (module[i] instanceof ModuleMCM) {
        createCLogic((ModuleMCM) module[i]);
      } else if (module[i] instanceof ModuleSCM) {
        createCLogic((ModuleSCM) module[i]);
      } else if (module[i] instanceof ModuleSCM_MK2) {
        createCLogic((ModuleSCM_MK2) module[i]);
      } else if (module[i] instanceof ModuleIOM) {
        createCLogic((ModuleIOM) module[i]);
      } else if (module[i] instanceof ModulePSM) {
        createCLogic((ModulePSM) module[i]);
      } else if (module[i] instanceof ModuleDSM) {
        createCLogic((ModuleDSM) module[i]);
      } else if (module[i] instanceof ModuleFPM) {
        createCLogic((ModuleFPM) module[i]);
      } else {
        this.log.info("No control logic created for module : "
            + module[i]);
      }
    }
  }
  
  public void createCLogic(ModuleDSM module) {
    Preconditions.checkNotNull(module, "module must not be null");
    SwitchIndex[] indexes = SwitchIndex.values();
    for (int i = 0; i < indexes.length; i++) {
      try {
        createSwitchLogic(module, indexes[i]);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }
  
  public void createCLogic(ModuleFPM module) {
    createFPITestLogic(module.getChByEnum(FPM_CH_FPI.FPM_CH_FPI_1_TEST));
    createFPITestLogic(module.getChByEnum(FPM_CH_FPI.FPM_CH_FPI_2_TEST));

    createFPIResetLogic(module.getChByEnum(FPM_CH_FPI.FPM_CH_FPI_1_RESET));
    createFPIResetLogic(module.getChByEnum(FPM_CH_FPI.FPM_CH_FPI_2_RESET));
  }
  
  public void createCLogic(ModuleIOM module) {
    Preconditions.checkNotNull(module, "module must not be null");

    createDigitialOutputLogic(module);
  }
  
  public void createCLogic(ModuleMCM module) {
    Preconditions.checkNotNull(module, "module must not be null");
    addLogicToConfig(new DummySwitch());
  }

  
  public void createCLogic(ModulePSM module) {
    Preconditions.checkNotNull(module, "module must not be null");
    if (module.getSettings().isFanFitted()) {
      createFanTestLogic(module.getFanTestChannel());
    }

    createBatteryChargerLogic(module.getChByEnum(PSM_CH_BCHARGER.PSM_CH_BCHARGER_BATTERY_TEST));
  }

  
  public void createCLogic(ModuleSCM module) {
    Preconditions.checkNotNull(module, "module must not be null");
    createSwitchLogic(module);
  }
  
  public void createCLogic(ModuleSCM_MK2 module) {
    Preconditions.checkNotNull(module, "module must not be null");
    createSwitchLogic(module);
  }

  
  @Override
  public void createDefaultLabelSets(ValueLabelSetManager valueLabelManager) {
    ValueLabelSet set;

    set = new ValueLabelSet("Binary Label");
    set.addLabel(1D, "On");
    set.addLabel(0D, "Off");
    valueLabelManager.add(set);

    set = new ValueLabelSet("Switch Status");
    set.addLabel(0D, "Intermediate");
    set.addLabel(1D, "Open");
    set.addLabel(2D, "Close");
    set.addLabel(3D, "Indeterminate");
    valueLabelManager.add(set);
  }

  @Override
  public void createDefaultVirtualPoints(Module module) {
    createVirtualPoints(GenerateOptions.genDefaultVPoint(), module);
  }

  
  @Override
  public void createDigitalOutputLogic(IChannel doutputChnl) {
    addLogicToConfig(new DigitalOutputControl(doutputChnl));
  }
  
  @Override
  public void createDigitialOutputLogic(Module module) {
    ArrayList<ICLogic> allogic = new ArrayList<ICLogic>();
    
    if(module instanceof IOModule) {
      IChannel[] digitalOutputChs = ((IOModule)module).getChannels(ChannelType.DIGITAL_OUTPUT);
      if (digitalOutputChs != null) {
        /* Create Digital Output Logic for each digital output channel of IOM */
        for (IChannel ch : digitalOutputChs) {
          DigitalOutputControl dol = new DigitalOutputControl();
          dol.getOutput(DOL_OUTPUT.DOL_OUTPUT_CHANNEL).setValue(ch);
          allogic.add(dol);
        }
      }
  
      addLogicToConfig(allogic);
    }
  }
  
  @Override
  public void createFanTestLogic(IChannel fanTestChnl) {
    addLogicToConfig(new FanTest(fanTestChnl));
  }

  public void createFPIResetLogic(IChannel outputChannel) {
    ModuleRepository moduleRepo =  manager.getOwner().getConfigModule(ModuleRepository.CONFIG_MODULE_ID);
    FPIReset fpiResetLogic = new FPIReset();
    Module psm = moduleRepo.getModule(MODULE.MODULE_PSM, MODULE_ID.MODULE_ID_0);
    IStandardPoint binaryPoint = getBinaryPoint(psm, PSM_CH_DINPUT.PSM_CH_DINPUT_AC_OFF);
    if (binaryPoint != null) {
      fpiResetLogic.setInputVPoint(binaryPoint);
    }
    fpiResetLogic.setOutputChannel(outputChannel);
    addLogicToConfig(fpiResetLogic);
  }

  public void createFPITestLogic(IChannel outputChannel) {
    FPITest fpiTestLogic = new FPITest();
    fpiTestLogic.setOutputChannel(outputChannel);
    addLogicToConfig(fpiTestLogic);
  }

  @Override
  public void createFullVirtualPoints(Module module) {
    createVirtualPoints( GenerateOptions.genFullVPoint(), module);
  }

  @Override
  public void createScreens(IOperableLogic newCreated) {
    IHMIGenerator hmiGen = ConfigGeneratorRegistry.getInstance().getGenerator(config, IHMIGenerator.class);
    HMIScreenManager manager = config.getConfigModule(HMIScreenManager.CONFIG_MODULE_ID);
    hmiGen.generateScreens(manager, newCreated);
  }

  @Override
  public void createScreens(Module module) {
    
  }
  
  @Override
  public IStandardPoint createStandardVirtualPoint(IChannel ch) {
    return createVirtualPoint(ch);
  }
  
  @Override
  public void createSwitchLogic(ISwitchModule module) {
    Collection<SwitchModuleOutput> outputs = module.getAllSwitchOutputs();
    for (SwitchModuleOutput output : outputs) {
      createSwitchLogic(output);
    }
  }
  
  public void createSwitchLogic(ModuleDSM module, SwitchIndex index) {
    Preconditions.checkNotNull(module, "module is null");
    Preconditions.checkNotNull(index, "index is null");

    SwitchModuleOutput output = module.getSwitchOutput(index);
    SwitchGearLogic logic = new SwitchGearLogic(output);

    /* Configure input point "SGL_INPUT_STATUS" */
    StdDoubleBinaryPoint inputStatusPoint = getDoubleBinaryPoint(module,
        output.getOpenEnumForInput(),
        output.getCloseEnumForInput());
    inputStatusPoint.setDescription("Input status - " + index);
    logic.getInput(SGL_INPUT.SGL_INPUT_STATUS).setValue(inputStatusPoint);

    addLogicToConfig(logic);
  }
  
  @Override
  public void createSwitchLogic(SwitchModuleOutput output) {
    if (!output.isUsable()) {
      log.error("Unusable switch: " + output);
      return;
    }
    
    SwitchGearLogic clogic = new SwitchGearLogic(output);
    doConfig(clogic);
    addLogicToConfig(clogic);
  }
  
  private void doConfig(SwitchGearLogic clogic){
    SwitchModuleOutput output = clogic.getOutputSCM();
    if(output == null) {
      log.warn("Cannot configure " +clogic.getName()+ " cause its output is not specified!");
      return;
    }
    
    Module module = output.getModule();

    // Configure input point "SGL_INPUT_STATUS"
    StdDoubleBinaryPoint inputStatusPoint = getDoubleBinaryPoint(module,
        output.getOpenEnumForInput(),
        output.getCloseEnumForInput());
    if(inputStatusPoint != null)
    inputStatusPoint.setDescription("Input status");
    clogic.getInput(SGL_INPUT.SGL_INPUT_STATUS).setValue(inputStatusPoint);

    // Configure input point "SGL_INPUT_ACTUATORDISABLED"
    clogic.getInput(SGL_INPUT.SGL_INPUT_ACTUATORDISABLED).setValue(
        getBinaryPoint(module,
            SCM_CH_DINPUT.SCM_CH_DINPUT_ACTUATOR_DISABLED));

    // Configure input point "SGL_INPUT_EARTHED"
    clogic.getInput(SGL_INPUT.SGL_INPUT_EARTHED).setValue(
        getBinaryPoint(module,
            SCM_CH_DINPUT.SCM_CH_DINPUT_SWITCH_EARTHED));

    // Configure input point "SGL_INPUT_LOWINSULATION"
    clogic.getInput(SGL_INPUT.SGL_INPUT_LOWINSULATION).setValue(
        getBinaryPoint(module,
            SCM_CH_DINPUT.SCM_CH_DINPUT_INSULTATION_LOW));
  }
  

  @Override
  public IStandardPoint createVirtualPoint(IChannel ch) {
    Preconditions.checkNotNull(ch, "ch is null");

    IStandardPoint point = null;

    switch (ch.getType()) {
    case ANALOG_INPUT:
      StdAnaloguePoint apoint = VirtualPointFactory.createAnalogPoint();
      apoint.setChannel(ch);
      apoint.setDescription(ch.getDescription());
      configureScalingFactor(apoint);
      apoint.resetAllHysteresis();
      point = apoint;
      break;

    case DIGITAL_INPUT:
      StdBinaryPoint bpoint = VirtualPointFactory.createBinaryPoint();
      bpoint.setChannel(ch);
      bpoint.setDescription(ch.getDescription());
      point = bpoint;
      break;

    default:
      throw new UnsupportedOperationException("Unsupported to create point for channel: " + ch);
    }

    return point;
  }

  
  @Override
  public void createVirtualPoints(GenerateOptions option, Module... module) {
    Preconditions.checkNotNull(module, "module must not be null");
    Preconditions.checkNotNull(option, "option must not be null");

    ArrayList<IStandardPoint> points = new ArrayList<IStandardPoint>();

    for (int i = 0; i < module.length; i++) {

      if (module[i] == null) {
        continue;
      }

      // Add analogue points
      if(module[i] instanceof IOModule) {
        IChannel[] chs = ((IOModule)module[i]).getChannels(ChannelType.ANALOG_INPUT);
        points.addAll(createVirtualPoints(option, Arrays.asList(chs), false));
  
        // Add binary points
        chs = ((IOModule)module[i]).getChannels(ChannelType.DIGITAL_INPUT);
        points.addAll(createVirtualPoints(option, Arrays.asList(chs), false));
  
        // TODO Create double binary points
      }
    }
    
    VirtualPointManager manager = config.getConfigModule(VirtualPointManager.CONFIG_MODULE_ID);
    manager.addAllPoints(points, true);
  }

  private void addLogicToConfig(Collection<ICLogic> logicList) {
    PseudoDoubleBinaryPoint olrStatus = manager.getOLRStatus();

    for (ICLogic logic : logicList) {
      // Configure Off/local/remote input
      if (logic != null && logic instanceof ISupportOffLocalRemote) {
        ((ISupportOffLocalRemote) logic).setInputOLR(olrStatus);
      }

      try {
        manager.add(logic);
        this.log.info("Added logic: " + logic);
      } catch (Exception e) {
        this.log.error("Failed to add logic: " + logic, e);
      }
    }
  }
  

  private void addLogicToConfig(ICLogic logic) {
    try {
      // Configure Off/local/remote input
      if (logic != null && logic instanceof ISupportOffLocalRemote) {
        PseudoDoubleBinaryPoint olrStatus = manager.getOLRStatus();
        ((ISupportOffLocalRemote) logic).setInputOLR(olrStatus);
      }

      manager.add(logic);
      this.log.info("Added logic: " + logic);
    } catch (Exception e) {
      this.log.error("Failed to add logic: " + logic, e);
    }
  }

  /**
   * Configure the default scaling for a analogue point. Currently, only FPM
   * points should be configured based on its CT Ratio.
   */
  private void configureScalingFactor(StdAnaloguePoint ap) {
    IChannel ch = ap.getChannel();
    if (ch == null) {
      /* Cannot configure scaling factor cause no channel found. */
      return;
    }
    Double factor;

    Module module = ch.getOwnerModule();
    if (module instanceof ModuleFPM) {
      ModuleFPM fpm = (ModuleFPM) module;
      IChannelEnum chEnum = ch.getEnum();

      /* Configure FPM points scaling factor using CT ratio 0 */
      if ((chEnum == FPM_CH_AINPUT.FPM_CH_AINPUT_FPI1_PHASE_A_CURRENT)
          || (chEnum == FPM_CH_AINPUT.FPM_CH_AINPUT_FPI1_PHASE_B_CURRENT)
          || (chEnum == FPM_CH_AINPUT.FPM_CH_AINPUT_FPI1_PHASE_C_CURRENT)
          || (chEnum == FPM_CH_AINPUT.FPM_CH_AINPUT_FPI1_PHASE_N_CURRENT)) {
        factor = fpm.getSettings().getFpi(0).getCtratio().calculateScaingFactor();
        // ap.setScalingFactor(factor);
        ap.setUnit("A");

        /* Configure FPM points scaling factor using CT ratio 1 */
      } else if ((chEnum == FPM_CH_AINPUT.FPM_CH_AINPUT_FPI2_PHASE_A_CURRENT)
          || (chEnum == FPM_CH_AINPUT.FPM_CH_AINPUT_FPI2_PHASE_B_CURRENT)
          || (chEnum == FPM_CH_AINPUT.FPM_CH_AINPUT_FPI2_PHASE_C_CURRENT)
          || (chEnum == FPM_CH_AINPUT.FPM_CH_AINPUT_FPI2_PHASE_N_CURRENT)) {
        factor = fpm.getSettings().getFpi(1).getCtratio().calculateScaingFactor();
        // ap.setScalingFactor(factor);
        ap.setUnit("A");
      }
    }

    /*
     * Configure all other points using default scaling factor defined in XML.
     */
    factor = ch.predefined().getDefaultScalingFactor();
    if (factor == null) {
      factor = 1.0D;
    }
    String unit = ch.predefined().getUnitForScaling(factor);
    ap.setScalingFactor(factor);
    ap.setUnit(unit);

    try {
      double rawMin = ch.predefined().getRawMin();
      double rawMax = ch.predefined().getRawMax();
      double scaling = ch.predefined().getDefaultScalingFactor();
      double offset = 0;
      
      AbstractAnaloguePoint.calculateScaling(ap, rawMin, rawMax, scaling, offset, unit);
    } catch (Exception e) {
      log.error("Please make sure all required data is in the Module Protocol xml",e);
    }
  }
  

  private Collection<IStandardPoint> createVirtualPoints( GenerateOptions option, Collection<IChannel> chs,
      boolean addToManager) {
    Preconditions.checkNotNull(option, "option must not be null");

    ArrayList<IStandardPoint> points = new ArrayList<IStandardPoint>();

    if (chs != null) {
      for (IChannel ch : chs) {
        if (ch == null) {
          continue;
        }

        GenerateOption genOption = option.getOption(GenerateOptions.Target.VirtualPoint);
        if (genOption == null) {
          continue;
        }

        switch (genOption) {
        case GenerateDefault:
          DefaultCreateOption defOption = ch.predefined().getDefaultCreateOption();
          if (defOption != DefaultCreateOption.DoNotCreatePoint) {
            points.add(createVirtualPoint(ch));
          }
          break;

        case GenerateFull:
          points.add(createVirtualPoint(ch));
          break;

        default:
          break;
        }

      }

      if (addToManager) {
        getVirtualPointManager(config).addAllPoints(points, true);
      }
    }

    return points;
  }
  

  /**
   * Find an existing binary point. If the point is not found, a new point will
   * created and returned.
   *
   * @param module
   *          the module that owns the point.
   */
  private IStandardPoint getBinaryPoint(Module module, IChannelEnum chEnum) {
    VirtualPointManager vpointMgr = manager.getVirtualPointsManager();
    IStandardPoint vp = VirtualPointFinder.findStdVirtualPoint(module, chEnum,
        vpointMgr.getAllPoints());
    if (vp != null) {
      return vp;
    }

    // Point not found, create new one.
    IChannel ch = null;
    if (module instanceof IOModule)
      ch = ((IOModule)module).getChByEnum(chEnum);
    IStandardPoint newPoint = null;
    if (ch != null) {
      newPoint = createVirtualPoint(ch);
      newPoint.setDescription(ch.getDescription());
      vpointMgr.addPoint(newPoint);
    }

    return newPoint;
  }


  /**
   * Find an existing double point. If the point is not found, a new point will
   * be created and returned.
   *
   * @param module
   *          the module that owns the point.
   */
  private StdDoubleBinaryPoint getDoubleBinaryPoint(Module module, IChannelEnum chEnum0, IChannelEnum chEnum1) {
    VirtualPointManager pointMgr = manager.getVirtualPointsManager();
    Collection<VirtualPoint> allPoints = pointMgr.getAllPoints();
    for (VirtualPoint vp : allPoints) {
      if (vp != null && vp instanceof StdDoubleBinaryPoint) {
        IChannel vpch0 = ((StdDoubleBinaryPoint) vp).getChannel();
        IChannel vpch1 = ((StdDoubleBinaryPoint) vp).getSecondChannel();

        if (vpch0 != null && vpch1 != null
            && vpch0.getOwnerModule() == module
            && vpch1.getOwnerModule() == module) {
          if (vpch0.getEnum() == chEnum0 && vpch1.getEnum() == chEnum1) {
            return (StdDoubleBinaryPoint) vp;
          } else if (vpch0.getEnum() == chEnum1 && vpch1.getEnum() == chEnum0) {
            return (StdDoubleBinaryPoint) vp;
          }
        }
      }
    }

    // Point not found, create new one.
    IChannel ch0 = null;
    IChannel ch1 = null;
    if (module instanceof IOModule) {
      ch0 = ((IOModule)module).getChByEnum(chEnum0);
      ch1 = ((IOModule)module).getChByEnum(chEnum1);
    }
    
    StdDoubleBinaryPoint newPoint = null;
    if (ch0 != null && ch1 != null) {
      newPoint = VirtualPointFactory.createDoubleBinaryPoint();
      newPoint.setChannel(ch0);
      newPoint.setSecondChannel(ch1);

      pointMgr.addPoint(newPoint);
      this.log.info(String.format("Create double binary point from ch0: %s, ch1: %s", ch0, ch1));
    }

    if (newPoint == null)
      log.error("No double point found for channel enum 0:"+chEnum0.getName()+" channel enum 1:"+chEnum1.getName());
    
    return newPoint;
  }


  private VirtualPointManager getVirtualPointManager (IConfig owner) {
    return  owner.getConfigModule(VirtualPointManager.CONFIG_MODULE_ID);
  }

  @Override
  public void configure(ICLogic clogic) {
    if(clogic == null)
      return;
    
    if(clogic instanceof SwitchGearLogic) {
      doConfig((SwitchGearLogic)clogic);
    }
  }

}

