/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ModuleAlarmLister.h 27 May 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/IOModule/include/ModuleAlarmLister.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 27 May 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   27 May 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MODULEALARMLISTER_H__INCLUDED
#define MODULEALARMLISTER_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "ModuleAlarm.h"
#include "LockingMutex.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Manages a copy of the alarm list
 *
 * This class manages a list of the alarm list, restricting access to the
 * original list and managing the copy/destruction of the list
 */
class ModuleAlarmLister
{
public:
    ModuleAlarmLister();
    virtual ~ModuleAlarmLister();

    /**
     * \brief Add a copy of the alarm to the alarm list
     *
     * \param mAlarm Module Alarm to add
     */
    virtual void add(ModuleAlarm& mAlarm);

    /**
     * \brief Add a copy of all the alarms in the given list to the alarm list
     *
     * \param inList Module Alarm list to add
     */
    virtual void addList(ModuleAlarmList& inList);

    /**
     * \brief Get the amount of alarms in the alarm list
     *
     * \return Amount of alarms
     */
    virtual size_t size();

    /**
     * \brief Get one alarm from the copied alarm list
     *
     * Please keep in mind that it gets a copy of the alarm kept in this object,
     * and not the original, so altering it alters this internal list but not
     * the original entry in the original list.
     *
     * \param pos position of the alarm in the list
     *
     * \return Reference of the module alarm present in the list at the given position
     */
    virtual ModuleAlarm& operator[](lu_uint32_t pos);

private:
    MasterMutex accessMutex;    //Mutex to control access to the internal alarm list
    ModuleAlarmList alarmList;  //Internal alarm list
};

#endif /* MODULEALARMLISTER_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
