/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: MonitorLinkLayer 23-Aug-2013 11:38:06 andrews_s $
 *               $HeadURL: C:\sw_dev\gemini_30_software\Development\G3\RTU\MCM\Common\MonitorLinkLayer\include\MonitorLinkLayer.cpp $
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MonitorLinkLayer module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: andrews_s	$: (Author of last commit)
 *       \date   $Date: 23-Aug-2013 11:38:06	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   23-Aug-2013 11:38:06  andrews_s    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdio.h>
#include <unistd.h>   // usleep()
#include <errno.h>
#include <sys/socket.h>
#include <sys/un.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MonitorLinkLayer.h"
#include "Timer.h"
#include "Thread.h"
#include "LogMessage.h"
#include "timeOperations.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */


MonitorLinkLayer::MonitorLinkLayer( SCHED_TYPE         schedType,
                                    lu_uint32_t        priority,
                                    const lu_char_t*   sockName
                                  ) :
                                      Thread(schedType, priority, LU_FALSE, sockName),
                                      protocolLayerPtr(NULL),
                                      socketHandle(-1),
                                      rxBufPos(0),
                                      connected(LU_FALSE),
                                      connectionOnlineStatus(LU_FALSE)

{
  if (sockName != NULL)
  {
      strncpy(socketName, sockName, SOCKET_NAME_MAX);
  }
}

MonitorLinkLayer::~MonitorLinkLayer()
{
    stop();
}

void MonitorLinkLayer::setProtocolLayer(AbstractMonitorProtocolLayer* monitorProtocolLayerPtr)
{
    protocolLayerPtr = monitorProtocolLayerPtr;
}

THREAD_ERR MonitorLinkLayer::start()
{
    if (isRunning() == LU_TRUE)
    {
        return THREAD_ERR_INVALID_STATE;
    }

    /* Start thread */
    if (Thread::start() != THREAD_ERR_NONE)
    {
        return THREAD_ERR_INVALID_STATE;
    }

    return THREAD_ERR_NONE;
}

THREAD_ERR MonitorLinkLayer::stop()
{
    /* Wait thread termination */
    Thread::stop();
    Thread::join();

    return THREAD_ERR_NONE;
}


lu_int32_t MonitorLinkLayer::sendMessage(const AppMsgStr& message)
{
    lu_int32_t   ret;
    LockingMutex lMutex(sendMutex);  // Send is in two parts - protect with mutex
    if ((socketHandle < 0) || (getConnection() != LU_TRUE))
    {
        return MONLINK_ERROR_CONNECTION;
    }

    // Send the packet: header then payload
    lu_uint8_t* packet = new lu_uint8_t[sizeof(AppMsgHeaderStr) + message.header.payLoadLen];
    memcpy(packet, (lu_uint8_t*)&(message.header), sizeof(AppMsgHeaderStr));
    memcpy(packet + sizeof(AppMsgHeaderStr), message.payload, message.header.payLoadLen);

    ret = send(socketHandle, packet, sizeof(AppMsgHeaderStr) + message.header.payLoadLen, 0);
    delete packet;

    // If either send fails we should close the connection
    if (ret < 0)
    {
        if ((errno == ECONNRESET) || (errno == ENOTCONN) || (errno == EPIPE))
        {
            closeConnection();
        }
    }

    return ret;
}


lu_int32_t MonitorLinkLayer::processMessage(AppMsgStr& message)
{
    if (protocolLayerPtr == NULL)
    {
        return MONLINK_ERROR_PROTOCOL;
    }

    return protocolLayerPtr->decode(&message);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void MonitorLinkLayer::threadBody()
{
    lu_int32_t ret;
    struct timespec timeNow;
    struct timespec pingTime;
    struct timespec pingTimeout;

    setConnection(LU_FALSE);

    // Initialise the ping timer
    clock_gettimespec(&timeNow);
    pingTime = pingTimeout = timeNow;
    timespec_add_ms(&pingTime,    PING_INTERVAL_MS);
    timespec_add_ms(&pingTimeout, PING_TIMEOUT_MS);


    while (isRunning() == LU_TRUE)
    {
        if (getConnection() == LU_FALSE)
        {
            setConnectionOnlineStatus(LU_FALSE);

            // Attempt to make a connection
            if (makeConnection() != 0)
            {
                if (isInterrupting() == LU_TRUE)
                {
                    continue;
                }

                // Slow down the reconnect rate
                lucy_usleep(1000000);
            }
        }
        else
        {
            // Update time now
            clock_gettimespec(&timeNow);

            // Wait for a packet to be received
            ret = receiveMessage(500);
            if (isInterrupting() == LU_TRUE)
            {
                continue;
            }
            if (ret > 0)
            {
                // Call the virtual processMessage method
                if (processMessage(rxMsg) == 0)
                {
                    setConnectionOnlineStatus(LU_TRUE);

                    // Delay sending the next ping to (now + interval) upon receipt of a valid message
                    pingTimeout = pingTime = timeNow;
                    timespec_add_ms(&pingTime,    PING_INTERVAL_MS);
                    timespec_add_ms(&pingTimeout, PING_TIMEOUT_MS);
                }
            }
            else if (ret < 0)
            {
                // Error; close connection
                logMessageError(SUBSYSTEM_ID_MT_HANDLER, "Error receiving packet");
                closeConnection();
            }

            // Check ping time
            // Pings will only be sent on an inactive connection to ensure there's a healthy connection
            // Usually the other end of the socket will be pinging anyway which means we don't have to
            if (checkTimeout(&timeNow, &pingTime) == LU_TRUE)
            {
                protocolLayerPtr->send_ping_msg();
                timespec_add_ms(&pingTime, PING_INTERVAL_MS);
            }

            // Valid message received timeout
            if (checkTimeout(&timeNow, &pingTimeout) == LU_TRUE)
            {
                setConnectionOnlineStatus(LU_FALSE);
            }

        }
    }

    /* Close any open socket */
    closeConnection();
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void MonitorLinkLayer::setConnection(lu_bool_t newStatus)
{
    LockingMutex sMutex(statusMutex); //protect "connected"
    connected = newStatus;
}


lu_bool_t MonitorLinkLayer::getConnection()
{
    LockingMutex sMutex(statusMutex); //protect "connected"
    return connected;
}


lu_int32_t MonitorLinkLayer::makeConnection()
{
    struct sockaddr_un serverAddress;
    lu_int32_t         ret;


    if (getConnection() == LU_TRUE)
    {
        return MONLINK_ERROR_CONNECTION;
    }

    // Create a new socket
    // (POSIX 2001 says the state of a socket after a connect failure is unspecified)
    socketHandle = socket(AF_UNIX, SOCK_STREAM, 0);
    if (socketHandle < 0)
    {
        return MONLINK_ERROR_CONNECTION;
    }

    memset(&serverAddress, 0, sizeof(struct sockaddr_un));
    serverAddress.sun_family = AF_UNIX;
    strncpy(serverAddress.sun_path, socketName, sizeof(serverAddress.sun_path) -1);

    ret = connect(socketHandle, (struct sockaddr *)&serverAddress, sizeof(serverAddress.sun_path)-1);
    if (ret == 0)
    {
        setConnection(LU_TRUE);
        logMessageInfo(SUBSYSTEM_ID_MT_HANDLER, "Connected to [%s]!\n", socketName);
    }
    else
    {
        closeConnection();
    }

    return ret;
}


void MonitorLinkLayer::closeConnection()
{
    if (socketHandle >= 0)
    {
        LockingMutex sMutex(sendMutex);
        LockingMutex rMutex(receiveMutex);
        close(socketHandle);
        socketHandle = -1;
    }

    if (getConnection() == LU_TRUE)
    {
        logMessageInfo(SUBSYSTEM_ID_MT_HANDLER, "Closed connection to [%s]", socketName);
        setConnection(LU_FALSE);
    }

    setConnectionOnlineStatus(LU_FALSE);
}


void MonitorLinkLayer::setConnectionOnlineStatus(lu_bool_t status)
{
    LockingMutex sMutex(statusMutex); //protect "connectionOnlineStatus"
    if (connectionOnlineStatus != status)
    {
        connectionOnlineStatus = status;

        if (protocolLayerPtr != NULL)
        {
            protocolLayerPtr->notify_connection_status(connectionOnlineStatus);
        }
    }
}


lu_int32_t MonitorLinkLayer::receiveMessage(lu_int32_t timeoutMs)
{
    lu_int32_t    ret;
    LockingMutex  lMutex(receiveMutex);  // Receive is in two parts - protect with mutex

    rxBufPos = 0;

    if ((ret = receiveChunk(sizeof(AppMsgHeaderStr), timeoutMs)) > 0)
    {
        // Check the first byte is an STX
        if (*rxBuffer == XMSG_STX)
        {
            // Copy the header information in to the rxMsg member
            memcpy(&rxMsg, rxBuffer, XMSG_CONFIG_HEADER_LEN);

            // Default the payload pointer to NULL
            rxMsg.payload = NULL;

            if (rxMsg.header.payLoadLen > 0)
            {
                if ((ret = receiveChunk(rxMsg.header.payLoadLen, timeoutMs)) > 0)
                {
                    // Point to the payload to the rxBuffer
                    rxMsg.payload = (void *)&(rxBuffer[XMSG_CONFIG_HEADER_LEN]);
                }
            }
        }
        else
        {
            return MONLINK_ERROR_FORMAT;
        }
    }

    return ret;
}


lu_int32_t MonitorLinkLayer::receiveChunk(lu_int32_t chunkSize, lu_int32_t timeout)
{
    lu_int32_t ret;
    fd_set masterReadSet;   //Set of file descriptors for read
    fd_set readfds;         //copy of the set -- to be modified by select()
    lu_int16_t maxfd;
    struct timeval tv;
    lu_int32_t rxBytes = 0;

    maxfd = socketHandle + 1;
    FD_ZERO(&masterReadSet);
    FD_SET(socketHandle, &masterReadSet);

    while (rxBytes < chunkSize)
    {
        // Adjust the microsecond timeout to a timeval
        tv.tv_sec = timeout / 1000;
        tv.tv_usec = (timeout % 1000) * 1000;

        readfds = masterReadSet;    //Copy master set
        ret = select(maxfd, &readfds, NULL, NULL, &tv);

        if (ret > 0)
        {
            if (FD_ISSET(socketHandle, &readfds) > 0)
            {
                ret = recv(socketHandle, (void *) (rxBuffer + rxBufPos), chunkSize - rxBytes, 0);
                if (ret > 0)
                {
                    rxBufPos += ret;
                    rxBytes = rxBufPos;
                }
                else if (ret == 0)
                {
                    closeConnection();
                    return MONLINK_ERROR_CONNECTION;
                }
            }
        }
        else if (ret == 0)
        {
            // Timeout
            return MONLINK_ERROR_NONE;
        }
        else
        {
            // Error
            closeConnection();
            return MONLINK_ERROR_CONNECTION;
        }
    }

    return rxBytes;
}

/*
 *********************** End of file ******************************************
 */


