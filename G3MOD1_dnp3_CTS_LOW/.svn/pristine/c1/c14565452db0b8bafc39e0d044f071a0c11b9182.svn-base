/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.page;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.Action;
import javax.swing.ListModel;

import org.jdesktop.application.Application;

import com.jgoodies.binding.list.SelectionInList;

/**
 * An abstract page that contains children pages and also it provides an "view"
 * action for viewing the selected item in the item list of the page.
 */
public abstract class AbstractListPage extends AbstractPage {

  private static final String PROPERTY_SELECTION_NOT_EMPTY = "selectionNotEmpty";

  private static final String ACTION_VIEW_SELECTION = "viewSelection";


  public AbstractListPage(Object data, String description) {
    super(data, description);
  }

  public AbstractListPage(Object data) {
    super(data);
  }

  /**
   * View selected session.
   */
  @org.jdesktop.application.Action(enabledProperty = PROPERTY_SELECTION_NOT_EMPTY)
  public final void viewSelection() {
    Object source = getSelectionInList().getSelection();
    if (source != null) {
      fireNavigationEvent(new NavigationEvent(source));
    }
  }

  protected final Action getViewAction() {
    return Application.getInstance().getContext().getActionMap(AbstractListPage.class, this).get(ACTION_VIEW_SELECTION);
  }

  // Bean getter
  public boolean isSelectionNotEmpty() {
    return !getSelectionInList().isSelectionEmpty();
  }

  @Override
  protected void init() throws Exception {
    observeSelectionList(getSelectionInList(), PROPERTY_SELECTION_NOT_EMPTY);
  }

  /**
   * Observes a list and fires a property event if the selection in the list is
   * not empty.
   *
   * @param list
   *          the list to be observed
   * @param property
   *          the name of boolean property which is used to fire event
   */
  private void observeSelectionList(SelectionInList<?> list, String property) {
    list.addPropertyChangeListener(
        SelectionInList.PROPERTY_SELECTION_EMPTY,
        new EmptySelectionListener(PROPERTY_SELECTION_NOT_EMPTY));
  }

  private void fireSelectionEvent(String property, Boolean oldValue, Boolean newValue) {
    firePropertyChange(property, oldValue, newValue);
  }

  protected abstract SelectionInList<?> getSelectionInList();

  protected abstract ListModel<?> getSelsectionListModel();

  @Override
  public final ListModel<?> getChildrenDataList() {
    return getSelsectionListModel();
  }


  private class EmptySelectionListener implements PropertyChangeListener {

    private final String property;


    public EmptySelectionListener(String property) {
      this.property = property;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      // Get "empty" selection flag values
      Boolean oldValue = (Boolean) evt.getOldValue();
      Boolean newValue = (Boolean) evt.getNewValue();

      // Convert to "selection" flags.
      if (oldValue != null) {
        oldValue = !oldValue;
      }

      if (newValue != null) {
        newValue = !newValue;
      }

      fireSelectionEvent(property, oldValue, newValue);
    }
  }
}
