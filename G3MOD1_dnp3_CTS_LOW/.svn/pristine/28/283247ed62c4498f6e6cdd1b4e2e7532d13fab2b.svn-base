/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.page;

import javax.swing.JComponent;

/**
 * A <code> PageViewer</code> object is a container component that is for
 * viewing a {@code Page}.
 */
public interface IPageViewer {

  /**
   * Shows a page in this PageViewer's container component.
   *
   * @param v
   *          the page to be showed.
   */
  void show(Page v);

  /**
   * Gets the current showed page object.
   *
   * @return Page object. <code>Null</code> if no page has been showed.
   */
  Page getShowedPage();

  /**
   * Clear current showed paged.
   */
  void clear();

  /**
   * Gets this <code>PageViewer</code>'s component, which should be a container
   * for showing a {@code Page}.
   *
   * @return non-null <code>JComponent</code>.
   */
  JComponent getComponent();
}
