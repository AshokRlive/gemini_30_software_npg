/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:CommsDeviceManager.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   21 Aug 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef COMMSDEVICEMANAGER_H_
#define COMMSDEVICEMANAGER_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <vector>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "CommsDevice.h"
#include "DialupModem.h"
#include "Thread.h"
#include "Logger.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class CommsDeviceManager : private Thread
{
public:
    CommsDeviceManager();
    virtual ~CommsDeviceManager();


    /**
     * \brief Add a CommsDevice to the CommsDevice Vector
     *
     * \param commsDevicePtr    Pointer to CommsDevice to add
     *
     * \return Error Code
     */
    lu_int32_t addCommsDevice(CommsDevice *commsDevicePtr);


    void startDeviceManger();

    void stopDeviceManager();

    /**
     * \brief Get pointer to Modem by deviceID
     *
     * \param deviceID - ID of device to get
     *
     * \return Pointer to Modemr NULL
     */
    DialupModem* getDialupModem(lu_uint32_t deviceId);

private:
    void threadBody();

private:
    Logger& log;

    typedef std::vector<CommsDevice*> CommsDeviceVector;

    CommsDeviceVector commsDevices;
};

#endif /* COMMSDEVICEMANAGER_H_ */

/*
 *********************** End of file ******************************************
 */
