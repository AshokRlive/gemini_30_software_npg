/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MBMmisc.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15 Jul 2014     shetty_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "MBMmisc.hpp"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief This function formats/shuffles the readings from the 2 registers
 *          pointed to by pvaluearray(word shuffle).
 *
 *          It also does a byte shift of word 1(register 1) and a byte shift of
 *          word 2(register 2)
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_int32_t GetFormattedValue(void *ptr,TMWTYPES_USHORT *pvaluearray)
{
    lu_int32_t formattedvalue=0;
    switch((*(DB *)ptr).device.wordswap)
    {
        case true :
        formattedvalue |= ((lu_int32_t)pvaluearray[1]<< 16);
        formattedvalue |= ((lu_int32_t)pvaluearray[0]);
        break;
        case false :
        formattedvalue |= ((lu_int32_t)pvaluearray[0]<< 16);
        formattedvalue |= ((lu_int32_t)pvaluearray[1]);
        break;
    }
    if ((*(DB *)ptr).device.byteswap)
    {
        lu_int32_t byte1=0,byte2=0,byte3=0,byte4=0;
        byte1 = (lu_int32_t)((formattedvalue & 0xFF000000) >> 8);
        byte2 = (lu_int32_t)((formattedvalue & 0x00FF0000) << 8);
        byte3 = (lu_int32_t)((formattedvalue & 0x0000FF00) >> 8);
        byte4 = (lu_int32_t)((formattedvalue & 0x000000FF) << 8);
        formattedvalue=0;
        formattedvalue |= byte1 ;
        formattedvalue |= byte2 ;
        formattedvalue |= byte3 ;
        formattedvalue |= byte4 ;
    }
    return formattedvalue;
}

 /*!
  ******************************************************************************
  *   \brief
  *
  *   Detailed description
  *
  *   \param
  *
  *
  *   \return
  *
  ******************************************************************************
  */

 lu_uint8_t GetBitValue(lu_uint16_t regValue,lu_uint8_t regType,lu_uint16_t bitMask)
 {
     lu_uint8_t digitalValue=0;
     if((regType == mbm::REG_TYPE_COIL_STATUS) || (regType == mbm::REG_TYPE_INPUT_STATUS))
         digitalValue = (lu_uint8_t)regValue;
     else if ((regType == mbm::REG_TYPE_HOLDING_REGISTERS) || (regType == mbm::REG_TYPE_INPUT_REGISTERS))
                     digitalValue = (lu_uint8_t)((regValue & bitMask)/bitMask) ;
     return digitalValue;
 }
/*
 *********************** End of file ******************************************
 */
