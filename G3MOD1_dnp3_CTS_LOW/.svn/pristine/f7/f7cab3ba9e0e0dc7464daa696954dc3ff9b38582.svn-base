/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief System time public header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   03/06/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _SYSTEM_STATUS_INCLUDED
#define _SYSTEM_STATUS_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "errorCodes.h"
#include "ModuleProtocol.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */



/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Retrieve the Power Save Mode
 *
 *   \return Power Save Mode
 *
 ******************************************************************************
 */
extern lu_bool_t SSGetPowerSaveMode(void);

/*!
 ******************************************************************************
 *   \brief Update the Power Save Mode
 *
 *   \param Power save mode
 *
 *
 *   \return None
 *
 ******************************************************************************
 */
extern void SSSetPowerSaveMode(lu_bool_t powerSaveMode);

/*!
 ******************************************************************************
 *   \brief Retrieve the Bootloader mode
 *
 *   \return Bootloader  Mode
 *
 ******************************************************************************
 */
extern lu_bool_t SSGetBootloaderMode(void);

/*!
 ******************************************************************************
 *   \brief Update the Bootloader mode
 *
 *   \param Bootloader Mode
 *
 *
 *   \return None
 *
 ******************************************************************************
 */
extern void SSSetBootloaderMode(lu_bool_t bootloaderMode);

/*!
 ******************************************************************************
 *   \brief Retrieve the Factory mode
 *
 *   \return Factory Mode
 *
 ******************************************************************************
 */
extern lu_bool_t SSGetFactoryMode(void);

/*!
 ******************************************************************************
 *   \brief Update the Factory mode
 *
 *   \param Factory Mode
 *
 *
 *   \return None
 *
 ******************************************************************************
 */
extern void SSSetFactoryMode(lu_bool_t factoryMode);

/*!
 ******************************************************************************
 *   \brief Retrieve the board status
 *
 *   \return Board Status
 *
 ******************************************************************************
 */
extern MODULE_BOARD_STATUS SSGetBStatus(void);

/*!
 ******************************************************************************
 *   \brief Update the status
 *
 *   \param status New status
 *
 *   \return None
 *
 ******************************************************************************
 */
extern void SSSetBStatus(MODULE_BOARD_STATUS status);

/*!
 ******************************************************************************
 *   \brief Retrieve the board error bitmap
 *
 *   \return Board error bitmap
 *
 ******************************************************************************
 */
extern MODULE_BOARD_ERROR SSGetBError(void);

/*!
 ******************************************************************************
 *   \brief Set/Reset a value in the board error bitmap
 *
 *   \param type Bit to set/reset
 *   \param val 0: reset the bit - 1: set the bit
 *
 *   \return None
 *
 ******************************************************************************
 */
extern void SSSetBError (MODULE_BOARD_ERROR type, lu_bool_t val);

/*!
 ******************************************************************************
 *   \brief Retrieve the full status (status and error bits) of the system
 *
 *   \param status The board status is saved here
 *   \param error The board error bitmap is saved here
 *
 *   \return None
 *
 ******************************************************************************
 */
extern void SSGetFullStatus(MODULE_BOARD_STATUS *status, MODULE_BOARD_ERROR *error, lu_uint32_t *serialNumber, lu_uint32_t *supplierId);

/*!
 ******************************************************************************
 *   \brief Get the board serial number
 *
 *   \return Serial number
 *
 ******************************************************************************
 */
extern lu_uint32_t SSGetSerial(void);

/*!
 ******************************************************************************
 *   \brief Set the board serial number
 *
 *   \param Serial Board serial number
 *
 *   \return none
 *
 ******************************************************************************
 */
extern void SSSetSerial(lu_uint32_t serialNumber);

/*!
 ******************************************************************************
 *   \brief Get the board Supplier ID
 *
 *   \return Supplier ID
 *
 ******************************************************************************
 */
extern lu_uint32_t SSGetSupplierId(void);

/*!
 ******************************************************************************
 *   \brief Set the board Supplier ID
 *
 *   \param Supplier ID
 *
 *   \return none
 *
 ******************************************************************************
 */
extern void SSSetSupplierId(lu_uint32_t supplierId);

/*!
 ******************************************************************************
 *   \brief Set the board Supplier ID from a string to numeric
 *
 *   \param Supplier ID (string ptr)
 *
 *   \return none
 *
 ******************************************************************************
 */
extern void SSSetSupplierIdStr(lu_char_t *supplierIdPtr);

extern lu_uint32_t SSGetFeatureRevisionMinor(void);
extern void SSSetFeatureRevisionMinor(lu_uint32_t featureRevisionMinor);

extern lu_uint32_t SSGetFeatureRevisionMajor(void);
extern void SSSetFeatureRevisionMajor(lu_uint32_t featureRevisionMajor);


extern lu_uint32_t SSGetAppSystemAPIMajor(void);
extern void SSSetAppSystemAPIMajor(lu_uint32_t systemAPIMajor);

extern lu_uint32_t SSGetAppSystemAPIMinor(void);
extern void SSSetAppSystemAPIMinor(lu_uint32_t systemAPIMinor);

extern lu_uint32_t SSGetAppSvnRevision(void);
extern void SSSetAppSvnRevision(lu_uint32_t svnRevision);

extern lu_uint32_t SSGetBootSvnRevision(void);
extern void SSSetBootSvnRevision(lu_uint32_t svnRevision);

extern lu_uint32_t SSGetAppSoftwareVersionMajor(void);
extern void SSSetAppSoftwareVersionMajor(lu_uint32_t softwareVersionMajor);

extern lu_uint32_t SSGetAppSoftwareVersionMinor(void);
extern void SSSetAppSoftwareVersionMinor(lu_uint32_t softwareVersionMinor);

extern lu_uint32_t SSGetAppSoftwareVersionPatch(void);
extern void SSSetAppSoftwareVersionPatch(lu_uint32_t softwareVersionPatch);

extern lu_uint8_t SSGetAppSoftwareReleaseType(void);
extern void SSSetAppSoftwareReleaseType(lu_uint8_t softwareReleaseType);


extern lu_uint32_t SSGetBootSystemAPIMajor(void);
extern void SSSetBootSystemAPIMajor(lu_uint32_t systemAPIMajor);

extern lu_uint32_t SSGetBootSystemAPIMinor(void);
extern void SSSetBootSystemAPIMinor(lu_uint32_t systemAPIMinor);

extern lu_uint32_t SSGetBootSoftwareVersionMajor(void);
extern void SSSetBootSoftwareVersionMajor(lu_uint32_t softwareVersionMajor);

extern lu_uint32_t SSGetBootSoftwareVersionMinor(void);
extern void SSSetBootSoftwareVersionMinor(lu_uint32_t softwareVersionMinor);

extern lu_uint32_t SSGetBootSoftwareVersionPatch(void);
extern void SSSetBootSoftwareVersionPatch(lu_uint32_t softwareVersionPatch);

extern lu_uint8_t SSGetBootSoftwareReleaseType(void);
extern void SSSetBootSoftwareReleaseType(lu_uint8_t softwareReleaseType);

#endif /* _SYSTEM_STATUS_INCLUDED */

/*
 *********************** End of file ******************************************
 */
