/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpointsource.domain;

import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.shared.model.INode;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * <p>
 * The object that implements this interface can be used as the input of a
 * virtual point.
 * </p>
 * TODO consider moved this to virtual point module and change it to composition
 * structure
 */
public interface IVirtualPointSource extends INode {

  /**
   * Checks if this source is mapped to a module.
   *
   * @param module
   * @return
   */
  boolean checkSourceModule(Module module);

  /**
   * Checks if this source is mapped to a module type.
   *
   * @param module
   * @return
   */
  boolean checkSourceModule(MODULE module);

  /**
   * Tries to get the module this source belongs to.
   *
   * @return
   */
  Module getSourceModule();

  String getSourceName();
}
