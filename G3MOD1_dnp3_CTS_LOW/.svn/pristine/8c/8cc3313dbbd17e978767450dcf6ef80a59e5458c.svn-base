/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCMMonitor
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Defines a Digital Input
 *
 *    CURRENT REVISION
 *
 *               $Revision::               $: (Revision of last commit)
 *               $Author::                 $: (Author of last commit)
 *       \date   $Date::                   $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25/07/13      andrews_s   Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <errno.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "MonitorPointMapping.h"
#include "IOManager.h"
#include "DigitalInput.h"
#include "LogMessage.h"
#include "timeOperations.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static IO_ERROR digitalInputGPIOUpdate(DigitalInputType *digitalInputPtr);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

IO_ERROR digitalInputClose(DigitalInputType *digitalInputPtr)
{
    if (digitalInputPtr == NULL)
    {
        return IO_ERROR_NULL;
    }

    return closeGPIO(digitalInputPtr->fd);
}

IO_ERROR digitalInputInit(lu_uint8_t address, DigitalInputType* digitalInputPtr, MCMMonDIConfigStr *configPtr)
{
  if ((digitalInputPtr == NULL) || (configPtr == NULL))
  {
      return IO_ERROR_NULL;
  }

  // Initialise the analogue input attributes
  digitalInputPtr->channel                       = address;
  digitalInputPtr->fd                            = NULL;

  digitalInputPtr->config                        = configPtr;

  digitalInputPtr->value                         = LU_FALSE;
  digitalInputPtr->lastValue                     = LU_FALSE;

  digitalInputPtr->flags                         = IO_FLAG_INVALID | IO_FLAG_OFF_LINE;
  digitalInputPtr->lastFlags                     = IO_FLAG_INVALID | IO_FLAG_OFF_LINE;
  digitalInputPtr->retries                       = 0;

  digitalInputPtr->nextScanTime                  = TIMESPEC_ZERO;  // Default to scan immediately
  digitalInputPtr->currentScanIntervalMs         = configPtr->scanIntervalMs;


  // If there's a bespoke update function for this DI, return immediately
  if (digitalInputPtr->config->update_func != NULL)
  {
      return IO_ERROR_NONE;
  }

  // Initialise the GPIO if we're not a virtual channel
  if (initGPIO(digitalInputPtr->config->gpioNum, GPIOLIB_DIRECTION_INPUT, &(digitalInputPtr->fd)) != 0)
  {
      logMessageWarn(SUBSYSTEM_ID_IO_MANAGER, "Failed to init GPIO Input [%d]\n", digitalInputPtr->config->gpioNum);
      return IO_ERROR_READ;
  }

  // Get a fresh reading so all GPIO inputs are valid at startup
  return digitalInputUpdate(digitalInputPtr);
}

IO_ERROR digitalInputSetValue(DigitalInputType* digitalInputPtr, lu_bool_t newValue)
{
    if (digitalInputPtr == NULL)
    {
        return IO_ERROR_NULL;
    }

    /* Clear all flags to bring point online and clear INVALID flag*/
    digitalInputPtr->flags = IO_FLAG_NONE;

    digitalInputPtr->lastValue = digitalInputPtr->value;
    digitalInputPtr->value = newValue;

    return IO_ERROR_NONE;
}

IO_ERROR digitalInputSetRawValue(DigitalInputType* digitalInputPtr, lu_bool_t newRawValue)
{
    if (digitalInputPtr == NULL)
    {
        return IO_ERROR_NULL;
    }

    digitalInputPtr->rawValue = newRawValue;

    return digitalInputSetValue(digitalInputPtr, (digitalInputPtr->config->invert) ? !newRawValue : newRawValue);
}

IO_ERROR digitalInputUpdate(DigitalInputType *digitalInputPtr)
{
    if ((digitalInputPtr == NULL) || (digitalInputPtr->config == NULL))
    {
        return IO_ERROR_NULL;
    }

    // Clear INVALID flag once we've updated the value/flags
    digitalInputPtr->flags &= ~IO_FLAG_INVALID;

    if (digitalInputPtr->config->update_func != NULL)
    {
        return digitalInputPtr->config->update_func(digitalInputPtr);
    }

    // Default to the standard GPIO Update function if none is specified
    return digitalInputGPIOUpdate(digitalInputPtr);
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Updates a Digital Input from a GPIO
 *
 *   \param analogueInputPtr    Analogue Input Pointer
 *
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
static IO_ERROR digitalInputGPIOUpdate(DigitalInputType *digitalInputPtr)
{
  lu_bool_t  tempVal = LU_FALSE;
  lu_int32_t ret;


  if ((digitalInputPtr == NULL) || (digitalInputPtr->config == NULL))
  {
      return IO_ERROR_NULL;
  }

  if ((ret = readGPIO(digitalInputPtr->fd, &tempVal)) <= 0)
  {
      if (digitalInputPtr->retries < MAX_DI_READ_RETRIES)
      {
          digitalInputPtr->retries++;
          logMessageInfo(SUBSYSTEM_ID_IO_MANAGER, "Unable to read DI GPIO:[%d] ret:[%d] value:[%i] try[%d]", digitalInputPtr->config->gpioNum, ret, tempVal, digitalInputPtr->retries);

          // Return with no error here
          return IO_ERROR_NONE;
      }
      else
      {
          // Only log the message if the value was previously ONLINE
          if ((digitalInputPtr->flags & IO_FLAG_OFF_LINE) == 0)
          {
              logMessageWarn(SUBSYSTEM_ID_IO_MANAGER, "Unable to read DI [%d] marking as OFFLINE", digitalInputPtr->config->gpioNum );
          }

          // Close the file descriptor
          digitalInputClose(digitalInputPtr);
          digitalInputPtr->fd = NULL;

          // Try to reopen the DI file - ignore errors
          digitalInputInit(digitalInputPtr->channel, digitalInputPtr, digitalInputPtr->config);

          // Unable to read the DI value file, set the OFFLINE flag
          digitalInputPtr->flags |= IO_FLAG_OFF_LINE;

          // Never stop trying
          digitalInputPtr->retries = 0;
      }

      return IO_ERROR_READ;
  }

  if (digitalInputPtr->flags & IO_FLAG_OFF_LINE)
  {
      logMessageInfo(SUBSYSTEM_ID_IO_MANAGER, "DI [%d] Recovered!", digitalInputPtr->config->gpioNum);
  }

  digitalInputPtr->retries = 0;

  // Update the Value
  digitalInputSetRawValue(digitalInputPtr, tempVal);

#if 0
      printf("DI: GPIO[%d] : Val:[%d]\n", digitalInputPtr->config->gpioNum, digitalInputPtr->value);
#endif

  // Clear the Off-line Flag
  digitalInputPtr->flags &= ~IO_FLAG_OFF_LINE;

  return IO_ERROR_NONE;
}


/*
 *********************** End of file ******************************************
 */
