/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.swing.renderer;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

import com.jgoodies.common.base.Preconditions;

/**
 * ListCellRenderer for boolean value.
 */
public class BooleanListRenderer extends DefaultListCellRenderer {

  private final String textTrue;
  private final String textFalse;
  private final String textNull;


  public BooleanListRenderer(String textTrue, String textFalse,
      String textNull) {
    this.textTrue = Preconditions.checkNotNull(textTrue, "textTrue is null");
    this.textFalse = Preconditions.checkNotNull(textFalse, "textFalse is null");
    this.textNull = textNull;
  }

  public BooleanListRenderer() {
    this("Yes", "No", "");
  }

  @Override
  public Component getListCellRendererComponent(JList<?> list, Object value,
      int index, boolean isSelected, boolean cellHasFocus) {
    Component render = super.getListCellRendererComponent(list, value, index, isSelected,
        cellHasFocus);

    if (value != null && value instanceof Boolean) {
      setText(((Boolean) value == true) ? textTrue : textFalse);
    } else if (value == null) {
      setText(textNull);
    }

    return render;
  }
}