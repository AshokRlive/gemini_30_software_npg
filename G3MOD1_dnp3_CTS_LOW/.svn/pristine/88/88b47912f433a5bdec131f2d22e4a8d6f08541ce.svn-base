<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE concept
  PUBLIC "-//OASIS//DTD DITA Concept//EN" "concept.dtd">
<concept id="topic_4" xml:lang="en-GB"><title id="title__Toc410888778">Configuration page overview</title><conbody><p>The following tables summarise the commands on the <wintitle>Configuration</wintitle> page.</p>
    <table id="table-o1g_wtj_2r">
      <tgroup cols="2">
        <colspec colnum="1" colname="col1" colwidth="*"/>
        <colspec colnum="2" colname="col2" colwidth="*"/>
        <thead>
          <row>
            <entry>New Configuration</entry>
            <entry> </entry>
          </row>
        </thead>
        <tbody>
          <row>
            <entry><b>New from scratch</b></entry>
            <entry>Create a new configuration using a wizard.</entry>
          </row>
          <row>
            <entry><b>Load from a file</b></entry>
            <entry>Load a new configuration from a file.</entry>
          </row>
        </tbody>
      </tgroup>
    </table>
    <table id="table-zys_v54_gr">
      <tgroup cols="2">
        <colspec colnum="1" colname="col1" colwidth="*"/>
        <colspec colnum="2" colname="col2" colwidth="*"/>
        <thead>
          <row>
            <entry>Read/Write Configuration</entry>
            <entry> </entry>
          </row>
        </thead>
        <tbody>
          <row>
            <entry><b>Read</b></entry>
            <entry>Download the configuration from the RTU and load it into the Configuration
              Tool.</entry>
          </row>
          <row>
            <entry><b>Write</b></entry>
            <entry>Upload the configuration from the Configuration Tool to the RTU.</entry>
          </row>
          <row>
            <entry><b>Erase</b></entry>
            <entry>
              <p>Delete all configurationinformation from the RTU.</p>
              <p>This command is mainly used for development.</p>
            </entry>
          </row>
        </tbody>
      </tgroup>
    </table>
    <table id="table-snm_y54_gr">
      <tgroup cols="2">
        <colspec colnum="1" colname="col1" colwidth="*"/>
        <colspec colnum="2" colname="col2" colwidth="*"/>
        <thead>
          <row>
            <entry>Export Configuration</entry>
            <entry> </entry>
          </row>
        </thead>
        <tbody>
          <row>
            <entry><b>Save</b></entry>
            <entry>Save the configuration to a file with the existing name and format.</entry>
          </row>
          <row>
            <entry><b>Save As</b></entry>
            <entry>Save the configuration to a file with a new name or format.</entry>
          </row>
          <row>
            <entry><b>Export as HTML</b></entry>
            <entry>Save the configuration to an HTML-format file.</entry>
          </row>
        </tbody>
      </tgroup>
    </table></conbody></concept>