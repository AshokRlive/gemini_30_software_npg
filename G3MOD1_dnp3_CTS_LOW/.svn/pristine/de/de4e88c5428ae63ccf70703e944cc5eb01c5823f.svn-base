/*******************************************************************************
 * Copyright (c) 2007 - 2013 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.actions;

import org.eclipse.gef.ui.actions.WorkbenchPartAction;
import org.eclipse.ui.IWorkbenchPart;
import org.fordiac.ide.fbt.typeeditor.VarCreationFactory;
import org.fordiac.ide.fbt.typeeditor.commands.CreateInputVariableCommand;
import org.fordiac.ide.model.data.DataType;
import org.fordiac.ide.model.libraryElement.FBType;

public class CreateInputVariableAction extends WorkbenchPartAction {
	private static final String ID_PREFIX = "INPUT_";

	/** The fb type. */
	private FBType fbType;

	private DataType dataType;

	public CreateInputVariableAction(IWorkbenchPart part, FBType fbType,
			DataType dataType) {
		super(part);

		setId(getID(dataType.getName()));
		setText(dataType.getName());

		this.fbType = fbType;
		this.dataType = dataType;
	}

	@Override
	protected boolean calculateEnabled() {
		return (null != fbType);
	}

	@Override
	public void run() {
		CreateInputVariableCommand cmd = new CreateInputVariableCommand(
				VarCreationFactory.createNewVarDeclaration(dataType), fbType, -1);
		execute(cmd);
	}

	public static String getID(String name) {
		return ID_PREFIX + name;
	}

}
