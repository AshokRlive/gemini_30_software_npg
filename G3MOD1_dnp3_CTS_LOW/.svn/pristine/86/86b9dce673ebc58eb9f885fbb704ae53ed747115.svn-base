/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Linear interpolation unit test cases module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   02/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <embUnit/embUnit.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "LinearInterpolation.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define MAX_1DU16S16_TEST		2

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

//void lnInterp1DU16U16Test_setUp(void);
//void lnInterp1DU16U16Test_tearDown(void);

//void lnInterp1DU16U16Test_lnInterp1DU16U16(void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


LnInterpTable1DU16S16Str testTable1dU16S16[MAX_1DU16S16_TEST] = {
		/*
		 * Input value ,       Output value
		 */
		{216,                  -40},
		{1496,                 125}
};




/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

static void lnInterp1DU16S16Test_setUp(void)
{

}

static void lnInterp1DU16S16Test_tearDown(void)
{

}

static void lnInterp1DU16S16Test_limits(void)
{
	TEST_ASSERT_EQUAL_INT(-40,
			              lnInterp1DU16S16(&testTable1dU16S16[0],
			            		  	  	   MAX_1DU16S16_TEST,
			            		           216
			            		          )
			             );

}

static void lnInterp1DU16S16Test_interp(void)
{
	lu_int16_t interpolatedValue;
	lu_bool_t testok = LU_FALSE;

	interpolatedValue = lnInterp1DU16S16(&testTable1dU16S16[0], MAX_1DU16S16_TEST, 724);

	if(interpolatedValue <= 25 && interpolatedValue >= 24)
	{
		testok = LU_TRUE;
	}
	else
	{
		testok = LU_FALSE;
	}

	/*TEST_ASSERT_EQUAL_INT(interpolatedValue,
				              lnInterp1DU16S16(&testTable1dU16S16[0],
				            		  	  	   MAX_1DU16S16_TEST,
				            		           724
				            		          )
				             );*/

	TEST_ASSERT_MESSAGE(testok, "Test FAIL for Linear Interpolation");
}

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

TestRef lnInterp1DU16S16Test_tests(void)
{
	EMB_UNIT_TESTFIXTURES(fixtures) {
		new_TestFixture("lnInterp_test1DU16S16_limits",lnInterp1DU16S16Test_limits),
		new_TestFixture("lnInterp_test1DU16S16_interp",lnInterp1DU16S16Test_interp),


	};
	EMB_UNIT_TESTCALLER( LinearInterpTest,
			             "LinearInterpolationTest",
			             lnInterp1DU16S16Test_setUp,
			             lnInterp1DU16S16Test_tearDown,
			             fixtures
			            );

	return (TestRef)&LinearInterpTest;
}



/*
 *********************** End of file ******************************************
 */
