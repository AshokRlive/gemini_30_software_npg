/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:DialerModem.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   21 Aug 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "DialupModem.h"
#include "DialupModemSM.h"
#include "CommsTrafficSniffer.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
DialupModem::DialupModem(DialupModemConfig config):CommsDevice(config.id),
                                                    m_config(config),
                                                    mp_sniffer(NULL),
                                                    mp_stateMachine(NULL),
                                                    mp_powerSupplyChan(NULL)
{
    mp_sniffer      = new CommsTrafficSniffer();
    mp_stateMachine = new DialupModemSM(*this);

}

DialupModem::~DialupModem()
{
    delete mp_sniffer;
    delete mp_stateMachine;
}

void DialupModem::init()
{
    mp_stateMachine->enterStartState();
}


bool DialupModem::connect()
{
    return mp_stateMachine->connect();
}

void DialupModem::disconnect()
{
    mp_stateMachine->disconnect();
}

void DialupModem::sniffRcvData(lu_int8_t* dataPtr, lu_uint32_t len)
{
    SNIFF_STATE state = mp_sniffer->sniffRcvData(dataPtr,len);

    switch (state) {
        case SNIFF_STATE_NEW_CONNECTION :
            log.info("MODEM:CONNECT\n");
            mp_stateMachine->connect();
            break;

        case SNIFF_STATE_CLR:
            log.info("MODEM:DISCONNECT\n");
            mp_stateMachine->disconnect();
            break;

        case SNIFF_STATE_PROTOCOL_DATA:
            break;

        case SNIFF_STATE_ERROR:
            log.warn("MODEM:ERROR\n");
            break;

        default:
            log.warn("No operation for SNIFF_STATE:%i",state);
            break;
    }
}

void DialupModem::setDTR(bool activate)
{
    m_config.port.setDTR(activate);
}

void DialupModem::tickEvent(lu_uint32_t ticks)
{
    mp_stateMachine->tickEvent(ticks);
}

void DialupModem::checkTimer()
{
    mp_stateMachine->checkTimer();
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
