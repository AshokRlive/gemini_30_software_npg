/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.object;

import java.util.ArrayList;
import java.util.Arrays;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session.GlobalVariationsConfig;
import com.lucy.g3.xml.gen.api.IXmlEnum;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP1;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP10;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP2;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP20;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP21;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP22;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP23;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP3;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP30;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP32;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP4;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP40;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP42;


/**
 * The constants of DNP3 object variation.
 */
public class DNP3ObjVarConsts {
  
  public static IXmlEnum getEventVariationEnum(DNP3ObjGroup type, int variationValue) {
    switch (type) {
    case BinaryInput:
      return VARIATION_GROUP2.forValue(variationValue);
    case DoubleBinaryInput:
      return VARIATION_GROUP4.forValue(variationValue);
    case AnalogInput:
      return VARIATION_GROUP32.forValue(variationValue);
    case AnalogOutput:
      return VARIATION_GROUP42.forValue(variationValue);
    case Counter:
      return VARIATION_GROUP22.forValue(variationValue);
    case FrozenCounter:
      return VARIATION_GROUP23.forValue(variationValue);
    case BinaryOutput:
      return null;
    default:
      throw new IllegalArgumentException("Unsupported type: " + type);
    }
  }

  public static IXmlEnum[] getEventVariationEnums(DNP3ObjGroup type) {
    IXmlEnum[] items;
    switch (type) {
    case Counter:
      items = VARIATION_GROUP22.values();
      break;
    case FrozenCounter:
      items = VARIATION_GROUP23.values();
      break;
    case BinaryInput:
      items = VARIATION_GROUP2.values();
      break;
    case DoubleBinaryInput:
      items = VARIATION_GROUP4.values();
      break;
    case AnalogInput:
      items = VARIATION_GROUP32.values();
      break;
    case BinaryOutput:
      //items = VARIATION_GROUP11.values();
      items = new IXmlEnum[0];
      break;
    case AnalogOutput:
      items = VARIATION_GROUP42.values(); 
      break;
    default:
      throw new IllegalArgumentException("Unsupported type: " + type);
    }

    ArrayList<IXmlEnum> list = new ArrayList<IXmlEnum>(Arrays.asList(items));
    list.add(0, EMPTY_VARIATION);
    return list.toArray(new IXmlEnum[list.size()]);
  }

  public static IXmlEnum getStaticVariationEnum(DNP3ObjGroup type, int variationValue) {
    switch (type) {
    case BinaryInput:
      return VARIATION_GROUP1.forValue(variationValue);
    case DoubleBinaryInput:
      return VARIATION_GROUP3.forValue(variationValue);
    case Counter:
      return VARIATION_GROUP20.forValue(variationValue);
    case FrozenCounter:
      return VARIATION_GROUP21.forValue(variationValue);
    case AnalogInput:
      return VARIATION_GROUP30.forValue(variationValue);
    case AnalogOutput:
      return VARIATION_GROUP40.forValue(variationValue);
    case BinaryOutput:
      return VARIATION_GROUP10.forValue(variationValue);
    default:
      throw new IllegalArgumentException("Unsupported type: " + type);
    }
  }

  public static IXmlEnum[] getStaticVariationEnums(DNP3ObjGroup type) {
    IXmlEnum[] items;

    switch (type) {
    case Counter:
      items = VARIATION_GROUP20.values();
      break;
    case FrozenCounter:
      items = VARIATION_GROUP21.values();
      break;
    case BinaryInput:
      items = VARIATION_GROUP1.values();
      break;
    case DoubleBinaryInput:
      items = VARIATION_GROUP3.values();
      break;
    case AnalogInput:
      items = VARIATION_GROUP30.values();
      break;
    case AnalogOutput:
      items = VARIATION_GROUP40.values();
      break;
    case BinaryOutput:
      items = VARIATION_GROUP10.values();
      break;
    default:
      throw new IllegalArgumentException("Unsupported type: " + type);
    }

    ArrayList<IXmlEnum> list = new ArrayList<IXmlEnum>(Arrays.asList(items));
    list.add(0, EMPTY_VARIATION);
    return list.toArray(new IXmlEnum[list.size()]);
  }

  public static long getGlobalStaticVariation(GlobalVariationsConfig global, DNP3ObjGroup type) {
    Preconditions.checkNotNull(global, "global var is null");
    Preconditions.checkNotNull(type,   "type is null");
    
    switch (type) {
    case AnalogInput:
      return global.getObj30().getValue();
    case BinaryInput:
      return global.getObj01().getValue();
    case DoubleBinaryInput:
      return global.getObj03().getValue();
    case Counter:
      return global.getObj20().getValue();
    case FrozenCounter:
      return global.getObj21().getValue();
      
    case AnalogOutput:
      return global.getObj40().getValue();
    case BinaryOutput:
      return global.getObj10().getValue();
      
    default:
      throw new UnsupportedOperationException("Unsupported point type: " + type);
    }
  }

  public static long getGlobalEventVariation(GlobalVariationsConfig global, DNP3ObjGroup type) {
    Preconditions.checkNotNull(global, "global var is null");
    Preconditions.checkNotNull(type,   "type is null");
    
    switch (type) {
    case AnalogInput:
      return global.getObj32().getValue();
    case BinaryInput:
      return global.getObj02().getValue();
    case DoubleBinaryInput:
      return global.getObj04().getValue();
    case Counter:
      return global.getObj22().getValue();
    case FrozenCounter:
      return global.getObj23().getValue();
    case AnalogOutput:
      return global.getObj42().getValue();
    case BinaryOutput:
      return 0; // NOT SUPPORTED
    default:
      throw new UnsupportedOperationException("Unsupported point type: " + type);
    }
  }

  /**
   * A special variation enum constant which indicates no variation will be
   * applied.
   */
  public static final IXmlEnum EMPTY_VARIATION = new IXmlEnum() {

    @Override
    public String getDescription() {
      return "Unspecified default variation. " +
          "The default variation in DNP3 global settings will be applied.";
    }

    @Override
    public String toString() {
      return "Unspecified";
    }

    @Override
    public int getValue() {
      return 0;
    }
  };

}

