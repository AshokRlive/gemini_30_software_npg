/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic;

import java.util.Collection;
import java.util.List;

import javax.swing.ListModel;

import com.lucy.g3.rtu.config.shared.manager.IConfigModule;

/**
 * The Interface ICLogicRepository.
 */
public interface CLogicRepository extends IConfigModule {
  
  String CONFIG_MODULE_ID = "CLogicManager";
  
  Collection<ICLogic> getAllClogic();

  /**
   * Gets all control logic in the specific types.
   *
   * @param types
   *          the types of logic to be retrieved.
   * @return a Non-null collection of logic. The collection is empty if the
   *         logic not found.
   */
  Collection<ICLogic> getLogicByType(int... typeIds);

  /**
   * Gets all operable control logics that can be operated. e.g. Switchgear
   * Logic, Dummy Switch Logic, and Digital Output Logic.
   */
  List<IOperableLogic> getOperableCLogic();
  /**
   * Gets a logic in the specific group.
   *
   * @param group
   *          the group of logic to be retrieved.
   * @return a found logic or <code>null</code> if not found.
   */
  ICLogic getLogicByGroup(int group);

  ListModel<ICLogic>getAllClogicListModel();

}
