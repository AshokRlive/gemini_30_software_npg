Version 1.1.1 (15.03.2012)
-------------------------------
Changes
* performance imporvement: specific version of Adjustable Connection Router (with no jumplinks)
* fixed bug# 3503761: Deleting all servcie sequences leaves trace in view 
* fixed bug# 3499453: can't export adapters 
* fixed bug# 349300: export BFB with Adapters ST
* final fix for Bug# 3484667: Import of system is only partial
* fixed bug# 3458062: "System twice in System Manager" is broken again 
* fixed bug#  3458062: System twice in System Manager 
* worked on Bug#3484667: Import of system is only partial
* Import of systems not located in the workspace (check copy into workspace on import) works 
* fixed bug# 3493029: Source-File empty after export 
* fixed bug# 3484896: DnD of directories into FB network editors
* fixed bug# 3492120: export BFB with Adapters 
* fixed event-trigger command
* reuse socket for cleaning up of remote device
* fixed bug# 3490769: new created FBs are displayed wrong 
* CHG default column with for the virtualdns view 
* fixed bug# 3488111 Profile name holobloc has to be entered in all capitals
* fixed bug# 3487241 Moving of FBs in Application Editor doesn't work correctly
* fixed bug# 3488203; Export of default value assignment to array variable


Version 1.1 (15.02.2012)
-------------------------------
Changes
* Reworked type handling making 4DIAC-IDE more robust
  *	New tree based type navigator
  * Allows Drag and Drop creation of FBs in Application, Sub-Application, Composite FB editors
* Monitoring and debugging infrastructure
* FB Tester


Version 1.0 (15.09.2011)
-------------------------------

Changes
* Online Edit
* Application editor 
	* New routing algorithm
	* Virtual DNS
	* Automatic coloring of mapped FBs
* Function block editing perspective 
	* Adapter Support 
		* Adapter editor
		* Better adapter handling in the interface editor
		* Adapter support in ECC
	* Inline edit of many FB elements
	* FB outline view
* Deployment perspective: clear resource/device
* Merge editor for C++ export
* Bugfixes and usability improvements
* Infrastructure 
	* Switch to �Feature Build� in preparation for an Update Site
	* Switch to Eclipse 3.7
* Smaller Improvements 
	* Show FB in application
	* Focus on predecessor
	* Beautification of ECC editor 
* Added 4DIAC-FBLIB 

Version 0.3 (31.07.2009)
-------------------------------

New Features
   * Symbol for drawing connections will not be displayed for the source and target of a selected connection ? makes reconnect easier for the user
   * FORTE export - if a file already exists, it is possible to open it in a compare/merge editor
   * Instance names - check whether it is a valid "Identifier" regarding to the IEC 61499 speci-fication
   * Timeout for the deployment can be specified in the preferences
   * Corner dimension of FBs can be specified (in preferences page) - restart of the editor is necessary
   * Clean device / delete resource functionality in "Download Selection" ? context menu of the elements
   * Usage of Eclipse Log mechanisms
   * Improved usability

Changes and Bug-Fixes
   * FIX problem with save (data were corrupted) ? it can occur that old systems developed with previous versions cannot be loaded correctly with this version 0.3
   * FIX FORTE CPP export - the export of ST ELSEIF was false ("{" instead of "(")
   * FIX FORTE CPP export - comments in ST algorithms were not processed
   * FIX FORTE CPP export - bugfix in �for� statement emitter
   * FIX creating devices with resources failed if tool type library did not contain the resource type
   * FIX refresh problem in System Manager if a new System was created
   * FIX refresh problem if a new FB Type was created in a project specific library
   * Changing the background colour now changes the background colour of all selected ele-ments (if multiple elements are selected)
   * Disabled auto scrolling if a connection was selected
   * Allow moving of Function Blocks by selecting a (in-/output) port disabled
   * Migration to Eclipse 3.5 (Galileo)


Version 0.3 RC2 (23.12.2008)
-------------------------------

New Features
   * Hide/display of event- and/or data connections
   * Select the "SelecitonBorderColor" in the PreferencePage
   * Allow moving of Function Blocks by selecting a (in/output) port
   * Specify general Information on systems
   * Not available groups specified in the new FBType wizard are now created (needs to be "dot-separated")
   * Check on unique name of ECC states
   * Display children of SubApps in the SystemTree
   * Display comment and type of an in-/output in the Interface Editor of the FBType Editor

Changes and Bug-Fixes
   * FIX problem in ECC-Editor - it was not possible to close the dialog if only one element was selected
   * FIX device export bug in FORTE C++ export filter
   * Improvements in displaying the opposite connection end in resource editor
   * Various other bug fixes


Version 0.3 RC1 (21.11.2008)
-------------------------------

New Features
   * ServiceInterface Function Block Type Editor
   * Composite Function Block Type Editor
   * Add new FBTypes to project specific type library
   * Manipulate Palettes in Type Management Perspective
   * Delete systems and applications in the SystemTreeView
   * Grid, ruler and snap-to-grid functionality added
   * Export IEC 61499 system in the format of the Compliance Profile for Feasibility Demonstra-tions
   * Import applications of IEC 61499 System (Compliance Profile for Feasibility Demonstra-tions) files (*.sys)
   * Save as image (in the Editor Context Menu)

Changes and Bug-Fixes
   * FIX problems if export filter was called several times
   * FIX minor problems in the C++ FORTE export filter
   * FIX problems with spaces in paths
   * Various other bug fixes