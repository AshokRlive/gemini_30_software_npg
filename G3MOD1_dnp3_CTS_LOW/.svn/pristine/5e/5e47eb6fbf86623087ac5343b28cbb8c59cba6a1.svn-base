/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.config.wizard.createfromscratch;

import java.awt.Component;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import org.netbeans.spi.wizard.WizardController;
import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;

/**
 * Wizard page. Choose slave protocol stack.
 */
class NewConfigStep5 extends WizardPage {

  public static final String KEY_SELECT_SDNP3 = "selectSDNP3";
  public static final String KEY_SELECT_S104 = "selectS104";
  public static final String KEY_SELECT_S101 = "selectS101";
  public static final String KEY_GEN_PROTOCOL_POINTS_NONE = "genProtocolPointsNon";


  public NewConfigStep5() {
    initComponents();
    initComponentNames();
    setForwardNavigationMode(WizardController.MODE_CAN_CONTINUE_OR_FINISH);
  }

  public static String getDescription() {
    return "SCADA Protocol";
  }

  @Override
  protected String validateContents(Component component, Object event) {
    String result = null;
    if (result == null) {
      setForwardNavigationMode(WizardController.MODE_CAN_CONTINUE);
    }
    return result;
  }

  /**
   * Give a name to the component so its value can be accessed when producing
   * wizard result.
   *
   * @see NewConfigFromScratchWizard.NewConfigResult
   */
  private void initComponentNames() {
    radioButtonDNP3.setName(KEY_SELECT_SDNP3);
    radioButtonIEC104.setName(KEY_SELECT_S104);
    radioButtonIEC101.setName(KEY_SELECT_S101);
    radioButtonNone.setName(KEY_GEN_PROTOCOL_POINTS_NONE);

  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    JLabel label1 = new JLabel();
    radioButtonDNP3 = new JCheckBox();
    radioButtonIEC104 = new JCheckBox();
    radioButtonIEC101 = new JCheckBox();
    panelPointsOption = new JPanel();
    label2 = new JLabel();
    radioButtonYes = new JRadioButton();
    radioButtonNone = new JRadioButton();

    //======== this ========
    setLayout(new FormLayout(
        "10dlu, default:grow",
        "default, $lgap, default, $rgap, 2*(default, $lgap), default, $pgap, 2*(default, $lgap), default"));

    //---- label1 ----
    label1.setText("Select SCADA Protocol:");
    add(label1, CC.xywh(1, 1, 2, 1));

    //---- radioButtonDNP3 ----
    radioButtonDNP3.setText("DNP3");
    radioButtonDNP3.setSelected(true);
    add(radioButtonDNP3, CC.xy(2, 3));

    //---- radioButtonIEC104 ----
    radioButtonIEC104.setText("IEC 60870-5-104");
    add(radioButtonIEC104, CC.xy(2, 5));

    //---- radioButtonIEC101 ----
    radioButtonIEC101.setText("IEC 60870-5-101");
    add(radioButtonIEC101, CC.xy(2, 7));

    //======== panelPointsOption ========
    {
      panelPointsOption.setLayout(new FormLayout(
          "10dlu, default:grow",
          "2*(default, $lgap), default"));

      //---- label2 ----
      label2.setText("Generate Protocol Points?");
      panelPointsOption.add(label2, CC.xywh(1, 1, 2, 1));

      //---- radioButtonYes ----
      radioButtonYes.setText("Yes");
      radioButtonYes.setSelected(true);
      panelPointsOption.add(radioButtonYes, CC.xy(2, 3));

      //---- radioButtonNone ----
      radioButtonNone.setText("No");
      panelPointsOption.add(radioButtonNone, CC.xy(2, 5));
    }
    add(panelPointsOption, CC.xywh(1, 11, 2, 1));

    //---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(radioButtonYes);
    buttonGroup1.add(radioButtonNone);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JCheckBox radioButtonDNP3;
  private JCheckBox radioButtonIEC104;
  private JCheckBox radioButtonIEC101;
  private JPanel panelPointsOption;
  private JLabel label2;
  private JRadioButton radioButtonYes;
  private JRadioButton radioButtonNone;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
