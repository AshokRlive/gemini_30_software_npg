<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE task PUBLIC "-//OASIS//DTD DITA Task//EN" "task.dtd">
<task id="task_nt5_gvt_yq">
    <title>Set the inhibit value for a control</title>
    <shortdesc>You can set the inhibit value for a control on the <uicontrol>DSM</uicontrol> and
            <uicontrol>SCM</uicontrol> modules.</shortdesc>
    <taskbody>
        <context><menucascade>
                <uicontrol>Configuration</uicontrol>
                <uicontrol>Modules</uicontrol>
                <uicontrol>SCM</uicontrol>
                <uicontrol>Detail</uicontrol>
            </menucascade></context>
        <steps>
            <step>
                <cmd>In the navigation pane, click the <uicontrol>Configuration</uicontrol>
                    tab.</cmd>
            </step>
            <step>
                <cmd>Under <uicontrol>Configuration</uicontrol>, open the <uicontrol>Modules
                        node</uicontrol>.</cmd>
            </step>
            <step>
                <cmd>In the navigation pane, click the name of the module.</cmd>
            </step>
            <step>
                <cmd>On the <uicontrol>Switch Output</uicontrol> tab, click the
                        <uicontrol>Inhibit</uicontrol> box for the control.</cmd>
            </step>
            <step>
                <cmd>In the <b>Inhibit Setting</b> dialog box, select the check boxes next to the
                    channels that will inhibit the control.</cmd>
                <info/>
            </step>
        </steps>
        <result props="exclude">
            <note type="important">The inhibit is done at the module level, so it is independent of
                the switchgear "logical" configuration.</note>
        </result>
    </taskbody>
</task>
