/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.base.math;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.common.base.Preconditions;

/**
 * A bean is for storing bit mask and can bound to GUI using JGooide binding.
 */
public class BitMask extends Model {

  public static final String PROPERTY_BIT_0 = "bit0";
  public static final String PROPERTY_BIT_1 = "bit1";
  public static final String PROPERTY_BIT_2 = "bit2";
  public static final String PROPERTY_BIT_3 = "bit3";
  public static final String PROPERTY_BIT_4 = "bit4";
  public static final String PROPERTY_BIT_5 = "bit5";
  public static final String PROPERTY_BIT_6 = "bit6";
  public static final String PROPERTY_BIT_7 = "bit7";
  public static final String PROPERTY_BIT_8 = "bit8";
  public static final String PROPERTY_BIT_9 = "bit9";
  public static final String PROPERTY_BIT_10 = "bit10";
  public static final String PROPERTY_BIT_11 = "bit11";
  public static final String PROPERTY_BIT_12 = "bit12";
  public static final String PROPERTY_BIT_13 = "bit13";
  public static final String PROPERTY_BIT_14 = "bit14";
  public static final String PROPERTY_BIT_15 = "bit15";
  public static final String PROPERTY_BIT_16 = "bit16";

  public static final String PROPERTY_VALUE = "value";

  private static final String[] PROPERTIES = {
      PROPERTY_BIT_0,
      PROPERTY_BIT_1,
      PROPERTY_BIT_2,
      PROPERTY_BIT_3,
      PROPERTY_BIT_4,
      PROPERTY_BIT_5,
      PROPERTY_BIT_6,
      PROPERTY_BIT_7,
      PROPERTY_BIT_8,
      PROPERTY_BIT_9,
      PROPERTY_BIT_10,
      PROPERTY_BIT_11,
      PROPERTY_BIT_12,
      PROPERTY_BIT_13,
      PROPERTY_BIT_14,
      PROPERTY_BIT_15,
      PROPERTY_BIT_16,
  };

  public static final int MAX_BIT_NUMBER = PROPERTIES.length;

  private final boolean[] bitValues;

  private long value;

  private final int numberOfBits;

  private String label = "Bit";


  /**
   * Constructs a bit mask.
   *
   * @param numberOfBits
   *          the number of bits. Must be from 0(excluded) to
   *          {@value #MAX_BIT_NUMBER}.
   */
  public BitMask(int numberOfBits) {
    super();
    Preconditions.checkArgument(numberOfBits > 0 && numberOfBits <= MAX_BIT_NUMBER,
        "bitsNumber must be in the range:[0," + MAX_BIT_NUMBER + "]");

    this.numberOfBits = numberOfBits;
    this.bitValues = new boolean[numberOfBits];

    setValue(0);// Set default
  }

  public int getNumberOfBits() {
    return numberOfBits;
  }

  public long getValue() {
    return value;
  }

  public void setValue(long bitMask) {
    setValue(bitMask, true);
  }

  private void setValue(final long bitMask, boolean updateBitValues) {
    Object oldValue = this.value;
    this.value = bitMask;
    firePropertyChange(PROPERTY_VALUE, oldValue, bitMask);

    if (updateBitValues) {
      for (int i = 0; i < bitValues.length; i++) {
        setBit(i, (1 & (bitMask >> i)) == 1, false);
      }
    }
  }

  private void setBit(int index, boolean newBitValue, boolean updateBitMask) {
    Object oldValue = bitValues[index];
    bitValues[index] = newBitValue;
    firePropertyChange(PROPERTIES[index], oldValue, newBitValue);

    if (updateBitMask) {
      long newMaskValue;
      if (newBitValue) {
        newMaskValue = value | (1 << index);
      } else {
        newMaskValue = value & (~(1 << index)); 
      }
      
      
      setValue(newMaskValue, false);
    }
  }

  public boolean getBit(int index) {
    return bitValues[index];
  }

  // Getters & Setters
  public void setBit0(boolean value) {
    setBit(0, value, true);
  }

  public boolean getBit0() {
    return getBit(0);
  }

  public void setBit1(boolean value) {
    setBit(1, value, true);
  }

  public boolean getBit1() {
    return getBit(1);
  }

  public void setBit2(boolean value) {
    setBit(2, value, true);
  }

  public boolean getBit2() {
    return getBit(2);
  }

  public void setBit3(boolean value) {
    setBit(3, value, true);
  }

  public boolean getBit3() {
    return getBit(3);
  }

  public void setBit4(boolean value) {
    setBit(4, value, true);
  }

  public boolean getBit4() {
    return getBit(4);
  }

  public void setBit5(boolean value) {
    setBit(5, value, true);
  }

  public boolean getBit5() {
    return getBit(5);
  }

  public void setBit6(boolean value) {
    setBit(6, value, true);
  }

  public boolean getBit6() {
    return getBit(6);
  }

  public void setBit7(boolean value) {
    setBit(7, value, true);
  }

  public boolean getBit7() {
    return getBit(7);
  }

  public void setBit8(boolean value) {
    setBit(8, value, true);
  }

  public boolean getBit8() {
    return getBit(8);
  }

  public void setBit9(boolean value) {
    setBit(9, value, true);
  }

  public boolean getBit9() {
    return getBit(9);
  }

  public void setBit10(boolean value) {
    setBit(10, value, true);
  }

  public boolean getBit10() {
    return getBit(10);
  }

  public void setBit11(boolean value) {
    setBit(11, value, true);
  }

  public boolean getBit11() {
    return getBit(11);
  }

  public void setBit12(boolean value) {
    setBit(12, value, true);
  }

  public boolean getBit12() {
    return getBit(12);
  }

  public void setBit13(boolean value) {
    setBit(13, value, true);
  }

  public boolean getBit13() {
    return getBit(13);
  }

  public void setBit14(boolean value) {
    setBit(14, value, true);
  }

  public boolean getBit14() {
    return getBit(14);
  }

  public void setBit15(boolean value) {
    setBit(15, value, true);
  }

  public boolean getBit15() {
    return getBit(15);
  }
  
  public void setBit16(boolean value) {
    setBit(16, value, true);
  }
  
  public boolean getBit16() {
    return getBit(16);
  }

  public static String getPropertyName(int index) {
    Preconditions.checkArgument(index >= 0 && index < MAX_BIT_NUMBER,
        "index must be in the range:[0," + MAX_BIT_NUMBER + ")");
    return PROPERTIES[index];
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String bitMaskLabel) {
    this.label = bitMaskLabel;
  }

}
