/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.psupply;

import java.util.Arrays;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.clogic.OperationMode;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOChannel;
import com.lucy.g3.rtu.config.clogic.impl.PredefinedCLogic;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumPSM.PSM_CH_PSUPPLY;
import com.lucy.g3.xml.gen.common.ControlLogicDef.POWER_SUPPLY_INPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.POWER_SUPPLY_OUTPUT;

/**
 * Implementation of Threshold Logic.
 */
public class PowerSupplyControl extends PredefinedCLogic<PowerSupplyControlSettings> implements IOperableLogic{
  public static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.POWER_SUPPLY, "Power Supply Control")
      .desp("The control logic for enabling/disabling a power channel.")
      .opMode(OperationMode.OPEN_CLOSE)
      .get();

  private final CLogicIOChannel[] outputs;
  
  public PowerSupplyControl() {
    super(TYPE, POWER_SUPPLY_INPUT.values(), null);
    this.outputs = initOutputs();
  }
  
  private CLogicIOChannel[] initOutputs() {
    POWER_SUPPLY_OUTPUT[] outputEnums = POWER_SUPPLY_OUTPUT.values();
    CLogicIOChannel[] outputs = new CLogicIOChannel[POWER_SUPPLY_OUTPUT.values().length]; 
    for (int i = 0; i < outputs.length; i++) {
      outputs[i] = new CLogicIOChannel(this, outputEnums[i].getDescription(),
          false,
          true,
          new ChannelType[] { ChannelType.PSM_CH_PSUPPLY},
          null,// Any module
          new Enum<?>[]{
        PSM_CH_PSUPPLY.PSM_CH_PSUPPLY_COMMS_PRIM,
        PSM_CH_PSUPPLY.PSM_CH_PSUPPLY_COMMS_SEC,
        PSM_CH_PSUPPLY.PSM_CH_PSUPPLY_24V_AUX
      });
    }
    
    return outputs;
  }


  @Override
  public CLogicIO<?>[] getOutputs() {
    return Arrays.copyOf(outputs, outputs.length);
  }

  @Override
  public Module getSourceModule() {
    return null;
  }

  @Override
  protected PowerSupplyControlSettings createSettings() {
    return new PowerSupplyControlSettings(this);
  }

  
}
