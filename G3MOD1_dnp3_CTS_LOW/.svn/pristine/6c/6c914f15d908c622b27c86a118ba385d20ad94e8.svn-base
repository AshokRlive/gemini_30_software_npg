/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.ui.wizard.addcomms;

import java.awt.Component;
import java.util.Collection;

import javax.swing.JComboBox;
import javax.swing.JTextField;

import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.common.base.Strings;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.view.ValidationComponentUtils;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.port.serial.ISerialPort;


class SelectPortPage extends WizardPage {
  /**
   * 
   */
  private static final String UNNAMED_DEVICE = "Unnamed Device";
  public final static String KEY_DEVICE_NAME ="deviceName";
  public final static String KEY_PORT ="selectedPort";
  
  private final PortsManager ports;
  private JTextField tfName;
  private JComboBox<Object> cboxPorts;
  
  public SelectPortPage(PortsManager ports) {
    super("Select a port");
    this.ports = ports;
    initComponents();
  }

  private void initComponents() {
    DefaultFormBuilder builder = AddCommsDeviceWizard.createBuilder(this);
    
    /* Device name*/
    tfName = new JTextField();
    tfName.setName(KEY_DEVICE_NAME);
    tfName.setText(UNNAMED_DEVICE);
    ValidationComponentUtils.setMandatoryBackground(tfName);
    builder.append("Device Name:", tfName);
    builder.nextLine();
    
    builder.appendParagraphGapRow();
    builder.nextLine();
    
    /* Serial port*/
    Collection<ISerialPort> allSerialPorts = ports.getSerialManager().getAllItems();
    cboxPorts = new JComboBox<>(allSerialPorts.toArray());
    cboxPorts.insertItemAt("Not selected", 0);
    cboxPorts.setSelectedIndex(0);
    cboxPorts.setName(KEY_PORT);
    builder.append("Select a port:", cboxPorts);
  }

  @Override
  protected String validateContents(Component component, Object event) {
    /*Validate device name*/
    String devName = (String) getWizardData(KEY_DEVICE_NAME);
    if(Strings.isBlank(devName)) {
      ValidationComponentUtils.setErrorBackground(tfName);
      return "The device name cannot be blank!";
    }else if(UNNAMED_DEVICE.equals(devName)){
      ValidationComponentUtils.setErrorBackground(tfName);
      return "Please provide a name for the device.";
    } else{
      ValidationComponentUtils.setMandatoryBackground(tfName);
    }
    
    /* Validate port*/
    Object portItem = getWizardData(KEY_PORT);
    if(portItem == null || !(portItem instanceof ISerialPort)) {
      return "Please select a port for the comms device!";
    } else if(((ISerialPort)portItem).isInUse() && !((ISerialPort)portItem).allowMutipleUser()) {
      return "The selected port is already in use!";
    }
 
    return null;
  }
  
  
  
}

