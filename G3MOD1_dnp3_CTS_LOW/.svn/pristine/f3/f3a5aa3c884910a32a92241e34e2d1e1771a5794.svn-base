/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: NvramFactoryInfoRead.c 3512 2013-07-08 11:35:05Z saravanan_v $
 *               $HeadURL:
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 3512 $: (Revision of last commit)
 *               $Author: saravanan_v $: (Author of last commit)
 *       \date   $Date: 2013-07-08 12:35:05 +0100 (Mon, 08 Jul 2013) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   30/08/12     venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <windows.h>
#include <conio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "ModuleProtocol.h"
#include "CANProtocol.h"
#include "NVRAMDef.h"

#include "KvaserCan.h"
#include "CmdParser.h"
#include "CANProtocolFraming.h"
#include "CANProtocolCodec.h"

#include "NvramPSMOptionsRead.h"
#include "NvramXMLParser.h"

#include "crc32.h"
#include "roxml.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
*/

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
batteryOptionsBlkStr readBatteryOptions;

static strutureMapStr batteryOptionsElementPtrTable[] = 
{																
	{
		(void*)&readBatteryOptions.batteryOptions.BatteryChemistry,         
		sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryChemistry)				  
	},

	{
		(void*)&readBatteryOptions.batteryOptions.BatteryCellNominalVolts,	     
		sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryCellNominalVolts)           
	},

	{
		(void*)&readBatteryOptions.batteryOptions.BatteryCellFullChargeVolts,	 
		sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryCellFullChargeVolts)	       
	},

	{
		(void*)&readBatteryOptions.batteryOptions.BatteryCellMaxCapacity,	    
		sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryCellMaxCapacity)	           
	},
	
	{	
		(void*)&readBatteryOptions.batteryOptions.BatteryCellChargingCurrent,	
		sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryCellChargingCurrent)	       
	},

	{
		(void*)&readBatteryOptions.batteryOptions.BatteryCellChargingVolts,	
		sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryCellChargingVolts)	       
	},
	
	{
		(void*)&readBatteryOptions.batteryOptions.BatteryCellLeakage,		     
		sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryCellLeakage)	               
	},
	
	{
		(void*)&readBatteryOptions.batteryOptions.BatteryCellFloatCharge,		
		sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryCellFloatCharge)	           
	},
	
	{	
		(void*)&readBatteryOptions.batteryOptions.BatteryCellChargingMethod,	
		sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryCellChargingMethod)	       
	},
	
	{
		(void*)&readBatteryOptions.batteryOptions.BatteryPackNoOfCols,		    
		sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackNoOfCols)	               
	},
	
	{
		(void*)&readBatteryOptions.batteryOptions.BatteryPackNoOfRows,		    
		sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackNoOfRows)	               
	},
	
	{
		(void*)&readBatteryOptions.batteryOptions.BatteryPackMaxChargeTemp,	
		sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackMaxChargeTemp)	       
	},
	
	{
		(void*)&readBatteryOptions.batteryOptions.BatteryPackMaxChargeTime,	
		sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackMaxChargeTime)	  
	},
	
	{
		(void*)&readBatteryOptions.batteryOptions.BatteryPackTimeBetweenCharge, 
		sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackTimeBetweenCharge) 
	},
	
	{
		(void*)&readBatteryOptions.batteryOptions.BatteryPackOverideTime,		 
		sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackOverideTime)	      
	},
	
	{
		(void*)&readBatteryOptions.batteryOptions.BatteryPackChargeTrigger,	 
		sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackChargeTrigger)	  
	},
	
	{
		(void*)&readBatteryOptions.batteryOptions.BatteryPackThresholdVolts,	 
		sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackThresholdVolts)						
	},
	
	{
		(void*)&readBatteryOptions.batteryOptions.BatteryPackLowLevel,						
		sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackLowLevel)							
	},
	
	{
		(void*)&readBatteryOptions.batteryOptions.BatteryTestTimeMinutes,				   
		sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryTestTimeMinutes)				
	},
	
	{
		(void*)&readBatteryOptions.batteryOptions.BatteryTestCurrentMinimum,	           
		sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryTestCurrentMinimum)				
	},
	
	{
		(void*)&readBatteryOptions.batteryOptions.BatteryTestSuspendTimeMinutes,          
		sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryTestSuspendTimeMinutes)         
	},
	
	{
		(void*)&readBatteryOptions.batteryOptions.BatteryTestMaximumDurationTimeMinutes,   
		sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryTestMaximumDurationTimeMinutes) 
	},
	
	{
		(void*)&readBatteryOptions.batteryOptions.BatteryTestDischargeCapacity,            
		sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryTestDischargeCapacity)          
	},
	
	{
		(void*)&readBatteryOptions.batteryOptions.BatteryTestThresholdVolts,            
		sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryTestThresholdVolts)          
	},
	
	{
		(void*)&readBatteryOptions.batteryOptions.BatteryTestLoadType,            
		sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryTestLoadType)          
	},

	{
		(void*)&readBatteryOptions.batteryOptions.BatteryPackShutdownLevelVolts,            
		sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackShutdownLevelVolts)          
	},

	{
		(void*)&readBatteryOptions.batteryOptions.BatteryPackDeepDischargeVolts,            
		sizeof(((NVRAMDefOptBatStr*)NULL)->BatteryPackDeepDischargeVolts)          
	}


};
/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR NvramReadBatteryOptionsPrepare(ParsingArgsStr *argsFromCommand)
{
	SB_ERROR retError = SB_ERROR_NONE;

	if(argsFromCommand->nvramAddr == ID_NVRAM_ADDRESS)
	{
		readBatteryOptions.nvRamAddr.addr.addr    = BATT_ID_NVRAM_ADDRESS;
		readBatteryOptions.nvRamAddr.addr.i2cChan = BATT_ID_NVRAM_I2C_CHANNEL;
		readBatteryOptions.nvRamAddr.addr.offset  = NVRAM_BATT_ID_BLK_OPTS_OFFSET;
		readBatteryOptions.nvRamAddr.size         = NVRAM_BATT_ID_BLK_OPTS_SIZE;
	}
	
	if(argsFromCommand->nvramAddr == APP_NVRAM_ADDRESS)
	{
		readBatteryOptions.nvRamAddr.addr.addr    = BATT_DATA_NVRAM_ADDRESS;
		readBatteryOptions.nvRamAddr.addr.i2cChan = BATT_DATA_NVRAM_I2C_CHANNEL;
		readBatteryOptions.nvRamAddr.addr.offset  = NVRAM_BATT_DATA_BLK_OPTS_OFFSET;
		readBatteryOptions.nvRamAddr.size         = NVRAM_BATT_ID_BLK_OPTS_SIZE;
	}

	memcpy((lu_uint8_t*)&argsFromCommand->msgBuff[0], (lu_uint8_t*)&readBatteryOptions, sizeof(TestNVRAMReadBuffStr));

	return retError;
}

lu_bool_t NvramReadBatteryOptions(ParsingArgsStr *argsFromCommand)
{
	lu_bool_t finish;
	SB_ERROR retValue = SB_ERROR_NONE;
	flagDecoderPtr    = argsFromCommand;
	
	/* Sending the firmware block via CAN bus */
	retValue = CANCSendFromMCM( MODULE_MSG_TYPE_BLTST,
								MODULE_MSG_ID_BLTST_NVRAM_READ_BUFFER_C,
								argsFromCommand->CANHeader.deviceDst,
								argsFromCommand->CANHeader.deviceIDDst,
								sizeof(TestNVRAMReadBuffStr),
								(lu_uint8_t*)&argsFromCommand->msgBuff
							  );
	Sleep(1000);

	finish = LU_FALSE;

	return finish;
}

SB_ERROR LucyCANSendNvramReadBatteryOptionsProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR retError = SB_ERROR_CANC_NOT_HANDLED;
	
	LU_UNUSED(time);

	if(flagDecoderPtr->parserFlag == PARSER_FLAG_NVRAM_READ_BATT_OPTS)
	{
		switch (msgPtr->messageType)
		{
			case MODULE_MSG_TYPE_BLTST:
				switch(msgPtr->messageID)
				{
					case MODULE_MSG_ID_BLTST_NVRAM_READ_BUFFER_R: 
						if(msgPtr->msgLen == NVRAM_APP_BLK_INFO_SIZE)
						{
							memcpy(&readBatteryOptions.factoryBlkHeader, msgPtr->msgBufPtr, sizeof(batteryOptionsBlkStr)); 
							retError = BundleNVRAMDataToXMLFile( msgPtr->msgBufPtr,
																 (lu_int8_t*)flagDecoderPtr->destinationFileName,
																 BATTERY_OPTIONS_FIELD_MAX,
																 &batteryOptionsElementPtrTable[0],
																 &batteryOptionsMap[0]	
															   );
						}
						break;
						
					default:
						retError = SB_ERROR_CANC_NOT_HANDLED;
						break;
				}
				break;

			default:
				retError = SB_ERROR_CANC_NOT_HANDLED;
				break;
		}
	}
	else
	{
		retError = SB_ERROR_CANC_NOT_HANDLED;
	}

	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
*/