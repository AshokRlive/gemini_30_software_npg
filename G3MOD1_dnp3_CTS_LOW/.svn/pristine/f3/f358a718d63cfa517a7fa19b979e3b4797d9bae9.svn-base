/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief Basic components public header file
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author$: (Author of last commit)
 *       \date   $Date$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   16/06/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#if !defined(EA_606E221B_C580_4bf1_99AD_AF92D0E96FEA__INCLUDED_)
#define EA_606E221B_C580_4bf1_99AD_AF92D0E96FEA__INCLUDED_


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Mutex.h"
#include "Semaphore.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */



/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Automatically lock a mutex
 *
 * This class provides a way to automatically manage mutex acquisition and release.
 * Note that when exiting the critical section, the mutex is released automatically.
 */
class LockingMutex
{

public:
    /**
     * \brief Lock the Mutex at class creation time.
     *
     * Note that Mutex must be initialised by the caller.
     *
     * \param mtx Mutex to lock
     */
    inline LockingMutex(Mutex& mtx) : mutex(mtx) {mutex.lock();}

    /**
     * \brief When the object is destroyed the mutex is unlocked
     */
    inline ~LockingMutex() {mutex.unlock();}

private:
    Mutex& mutex;

};



/**
 * \brief Automatically lock a mutex with master priority
 *
 * This class provides a way to automatically manage mutex acquisition and release.
 * Note that when exiting the critical section, the mutex is released automatically.
 */
class MasterLockingMutex
{

public:
    /**
     * \brief Lock the Mutex at class creation time.
     *
     * Note that Mutex must be initialised by the caller.
     *
     * \param mtx Mutex (RW Lock) to lock
     * \param isMaster Role of the caller
     */
    inline MasterLockingMutex(MasterMutex& mtx, lu_bool_t master = LU_FALSE) :
                                                                rwMutex(mtx),
                                                                acquired(LU_FALSE)
    {
        /* NOTE: when trying to acquire the lock, the function (not errno) could
         * return one of the following codes:
         * EDEADLK: Lock already owned by this thread. It is therefore safe to
         *          do not acquire the lock -- it will be released by the first
         *          call (that makes it recursive)
         * EINVAL:  Invalid (uninitialised) lock.
         * EAGAIN:  Max number of locks reached -- What to do?
         */
        /* TODO: pueyos_a - deal with lock acquisition failure when retcode==EAGAIN (max number of locks reached)??? */
        if(master == LU_TRUE)
        {
            int retcode = rwMutex.wlock();    //Master locks with writer priority
            if(retcode == 0)
            {
                acquired = LU_TRUE;
            }
        }
        else
        {
            int retcode = rwMutex.rlock();    //Reader locks
            if(retcode == 0)
            {
                acquired = LU_TRUE;
            }
        }
    }

    /**
     * \brief When the object is destroyed the mutex locked during the creation is
     * unlocked
     */
    inline ~MasterLockingMutex()
    {
        if(acquired)
        {
            rwMutex.unlock();
        }
    }

private:
    MasterMutex& rwMutex;
    lu_bool_t acquired;     //Lock has been successfully acquired
};


#endif // !defined(EA_606E221B_C580_4bf1_99AD_AF92D0E96FEA__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
