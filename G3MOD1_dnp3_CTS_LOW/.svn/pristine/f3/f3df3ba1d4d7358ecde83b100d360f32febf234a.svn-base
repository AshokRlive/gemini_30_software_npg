/*! \file
*******************************************************************************
*       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
*                       - Automation Dept.
*******************************************************************************
*    FILE NAME:
*               $Id$
*               $HeadURL$
*
*    DESCRIPTION:
*       \brief  Brief description of this template module
*
*       Detailed description of this template module
*
*    CURRENT REVISION
*
*               $Rev:: $: (Revision of last commit)
*               $Author:: $: (Author of last commit)
*       \date   $Date:: $: (Date of last commit)
*******************************************************************************
*/

/*
*******************************************************************************
*    REVISION HISTORY
*
*   Date        EAR/Reason      Name        Details
*   ---------------------------------------------------------------------------
*   29/10/08                    walde_s     Initial version.
*
*******************************************************************************
*/

/*
*******************************************************************************
*   Copyright
*       This Document is the property of Lucy Switchgear Ltd.
*       It must not be reproduced, in whole or in part, or otherwise
*       disclosed without prior consent in writing from
*       Lucy Switchgear Ltd.
*
*******************************************************************************
*/

#ifndef SP_TRANSPORT_LAYER_H
#define SP_TRANSPORT_LAYER_H 1

/***************************************************/
/* I2C Transport Layer protocol                    */
/*                                                 */
/*  data alignment: 1 byte                         */
/*  Endianess (for multi byte fields): big endian  */
/*  CRC-CCITT (0xFFFF)                             */
/*                                                 */
/*  packet structure:                              */
/*   - header                                      */
/*   - payload                                     */
/*   - trailer                                     */
/***************************************************/

/*
*******************************************************************************
* INCLUDES - Standard Library
*******************************************************************************
*/

/*
*******************************************************************************
* INCLUDES - Project Specific
*******************************************************************************
*/

#include "lu_types.h"

/*
*******************************************************************************
* EXPORTED - Public Definitions
*******************************************************************************
*/

/* Frame delimiter */
#define SP_TRS_START_FRAME              0x55

/* Supported services */
#define SP_TRS_CMD_PROTOCOL             0x00

#define SP_TRS_MAX_PAYLOAD_SIZE         0x80

/* General utility */
#define SP_TRS_OVERHEAD                 (sizeof(SPTrsHeader) + sizeof(SPTrsTrailer))

/*
*******************************************************************************
* EXPORTED - Public Types
*******************************************************************************
*/


#pragma pack(1)
/*!
 * Serial Protocol Transport Layer protocol Header
 */
typedef struct
{
    /* Synchronization frame */
    lu_uint8_t frame;
    /* Payload service ID */
    lu_uint8_t service;
    /* Payload size */
    lu_uint8_t size;
}SPTrsHeader;

/*!
 *  I2C Transport Layer protocol Trailer
 */
typedef struct
{
    /* Header and payload CRC16 */
    lu_uint16_t crc16;
}SPTrsTrailer;
#pragma pack()

/*
*******************************************************************************
* EXPORTED - Public Constants
*******************************************************************************
*/


/*
*******************************************************************************
* EXPORTED - Public Variables
*******************************************************************************
*/




/*
*******************************************************************************
* EXPORTED - Functions
*******************************************************************************
*/




#endif /* SP_TRANSPORT_LAYER_H */

/*
*********************** End of file *******************************************
*/
