function DirectionalityConfig = ConfigDirectionality(DirectionalityConfig)
%#codegen
% Configure Directionality parameters
%
%

coder.inline('always');

% antipode = Exact opposite point on a circle
antipodeCharacteristicAngle = single(0); 

% Enable directionality function
% DirectionalityConfig.Enabled = uint32(1);
% 
% % Solidly and low impedance ground network
% DirectionalityConfig.SolidlyGroundNetwork = uint32(1);
% 
% % Resonant ground network
% DirectionalityConfig.ResonantGroundNetwork = uint32(0);
% 
% % Isolated ground network
% DirectionalityConfig.IsolatedGroundNetwork = uint32(0);
% 
% % Enable Wattmetric Method for Earth Faults only
% DirectionalityConfig.EnableWattmetricMthd = uint32(0);
%                                     
% % Angle of connection
% DirectionalityConfig.AngleOfConnection = single(90);

% Characteristics angle
% For solidly (and low resistance) ground distribution systems 
% this value is typically set to 45 degrees for phase faults
% DirectionalityConfig.CharacteristicAnglePF = single(45);

% Earth Fault Characteristic Angle
% Characteristic Angle for Vector comparison method with Solidly Ground Network
if ((DirectionalityConfig.EnableWattmetricMthd == 0) &&...
    (DirectionalityConfig.SolidlyGroundNetwork == 1))

    DirectionalityConfig.CharacteristicAngleEF = single(45);
    
% Characteristic Angle for Vector comparison method with Resonant Ground Network  
elseif ((DirectionalityConfig.EnableWattmetricMthd == 0) &&...
         (DirectionalityConfig.ResonantGroundNetwork == 1))
     
    DirectionalityConfig.CharacteristicAngleEF = single(90);
    
% Characteristic Angle for Vector comparison method with Isolated Ground Network  
elseif ((DirectionalityConfig.EnableWattmetricMthd == 0) &&...
         (DirectionalityConfig.IsolatedGroundNetwork == 1))
     
    DirectionalityConfig.CharacteristicAngleEF = single(0);
    
% Characteristic Angle for Wattmetric method with Resonant Ground Network 
elseif ((DirectionalityConfig.EnableWattmetricMthd == 1) &&...
         (DirectionalityConfig.ResonantGroundNetwork == 1))
     
    DirectionalityConfig.CharacteristicAngleEF = single(90);

% Characteristic Angle for Wattmetric method with Isolated Ground Network 
elseif ((DirectionalityConfig.EnableWattmetricMthd == 1) &&...
         (DirectionalityConfig.IsolatedGroundNetwork == 1))
     
    DirectionalityConfig.CharacteristicAngleEF = single(0);   
end

% Minimum phase angle in forward direction
DirectionalityConfig.MinFwdAngle = single(85);
DirectionalityConfig.MinFwdAngle = mod(DirectionalityConfig.MinFwdAngle, 360);
DirectionalityConfig.MinFwdAngle = single(DirectionalityConfig.MinFwdAngle +...
                                DirectionalityConfig.CharacteristicAnglePF);

% Maximum phase angle in forward direction                                    
DirectionalityConfig.MaxFwdAngle = single(85);
DirectionalityConfig.MaxFwdAngle = mod(DirectionalityConfig.MaxFwdAngle, 360);
DirectionalityConfig.MaxFwdAngle = single(DirectionalityConfig.CharacteristicAnglePF -...
                                        DirectionalityConfig.MaxFwdAngle);
DirectionalityConfig.MaxFwdAngle = mod(DirectionalityConfig.MaxFwdAngle, 360);

% Minimum phase angle in reverse direction
DirectionalityConfig.MinRvrsAngle = single(85);
DirectionalityConfig.MinRvrsAngle = mod(DirectionalityConfig.MinRvrsAngle, 360);
% Find the anitpode (exact opposite) of the Characteristic Angle
antipodeCharacteristicAngle = single(180 + DirectionalityConfig.CharacteristicAnglePF);
antipodeCharacteristicAngle = mod(antipodeCharacteristicAngle, 360);
% Normalize the minimum phase angle in reverse direction with respect to
% 360 degrees
DirectionalityConfig.MinRvrsAngle = single(antipodeCharacteristicAngle +...
                                        DirectionalityConfig.MinRvrsAngle);
DirectionalityConfig.MinRvrsAngle = mod(DirectionalityConfig.MinRvrsAngle, 360);

% Maximum phase angle in reverse direction
DirectionalityConfig.MaxRvrsAngle = single(85);
DirectionalityConfig.MaxRvrsAngle = mod(DirectionalityConfig.MaxRvrsAngle, 360);
DirectionalityConfig.MaxRvrsAngle = single(antipodeCharacteristicAngle -...
                                        DirectionalityConfig.MaxRvrsAngle);
DirectionalityConfig.MaxRvrsAngle = mod(DirectionalityConfig.MaxRvrsAngle, 360);

% MaxFwdAngle and MaxRvrsAngle non-operation total angle
% DirectionalityConfigStr.MaxNonOpAngle = 180 - DirectionalityConfigStr.MaxFwdAngle -...
%                                         DirectionalityConfigStr.MaxRvrsAngle;

% MaxFwdAngle and MaxRvrsAngle non-operation total angle
% DirectionalityConfigStr.MinNonOpAngle = 180 + DirectionalityConfigStr.MinFwdAngle +...
%                                         DirectionalityConfigStr.MinRvrsAngle; 
                                    
% Voltage memory time in milliseconds
DirectionalityConfig.VoltageMemTimeMS = uint32(100);

end