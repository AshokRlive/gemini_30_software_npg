/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.domain;

import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.shared.model.INode;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSetRef;
import com.lucy.g3.rtu.config.virtualpointsource.domain.IVirtualPointSource;
import com.lucy.g3.xml.gen.api.IXmlEnum.DefaultCreateOption;

/**
 * <p>
 * Interface of virtual point.
 * </p>
 */
public interface VirtualPoint extends INode, IValidation, IVirtualPointSource {
  String CLIENT_PROPERTY_FG_COLOR = "forgroundColor";
  
  /**
   * The name of property {@value} .
   */
  String PROPERTY_SOURCE = "source";

  /**
   * The name of property {@value} .
   */
  String PROPERTY_ID = "id";

  /**
   * The name of property {@value} .
   */
  String PROPERTY_GROUP = "group";

  /**
   * The name of property {@value} .
   */
  String PROPERTY_CUSTOM_DESCRIPTION_ENABLED = "customDescriptionEnabled";


  /**
   * Gets the type of this point.
   *
   * @return non-null type enumeration.
   */
  VirtualPointType getType();

  /**
   * Gets the id of this point.
   */
  int getId();

  /**
   * Gets the group value of this point.
   *
   * @return integer group value.
   */
  int getGroup();

  /**
   * Gets the group and id of this point .
   *
   * @return formatted text. <em>E.g. [0,1]</em>.
   */
  String getGroupID();

  /**
   * Gets the content of this point in HTML format.
   *
   * @return HTML formatted text.
   */
  String getHTMLSummary();

  /**
   * Gets the source this virtual point maps to.
   *
   * @return null if this point is not mapped to a source
   */
  IVirtualPointSource getSource();

  ValueLabelSetRef getLabelSetRef();

  boolean isCustomDescriptionEnabled();

  /**
   * Gets the option for creating points.
   */
  DefaultCreateOption getDefaultCreateOption();
}
