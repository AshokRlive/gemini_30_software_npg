/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.app;

import java.util.LinkedHashMap;

import com.lucy.g3.common.manifest.ManifestInfo;
import com.lucy.g3.gui.common.widgets.utils.ResourceUtils;
import com.lucy.g3.system.config.SystemProperties;
import com.lucy.g3.xml.gen.common.versions;


public class ConfigToolVersion {

  private static ConfigToolVersion INSTANCE;
  
  private final ManifestInfo manifest;
  
  public static ConfigToolVersion getInstance() {
    if(INSTANCE == null) {
      INSTANCE = new ConfigToolVersion();
    }
    
    return INSTANCE;
  }
  
  private ConfigToolVersion() {
    manifest = new ManifestInfo(ResourceUtils.getAppName());
    
    String G3ProtocolVersion = versions.G3_CONFIG_SYSTEM_API_MAJOR + "." + versions.G3_CONFIG_SYSTEM_API_MINOR;
    String SchemalVersion = versions.SCHEMA_MAJOR + "." + versions.SCHEMA_MINOR;
    String ModuleSystemAPIVersion = versions.MODULE_SYSTEM_API_VER_MAJOR + "." + versions.MODULE_SYSTEM_API_VER_MINOR;
    String profile = System.getProperty(SystemProperties.PROPERTY_PROFILE_NAME);
    if(profile == null)
      profile = "Default";
    
    LinkedHashMap<String, String> extraProperties = new LinkedHashMap<String, String>();
    extraProperties.put("Schema Version", SchemalVersion);
    extraProperties.put("Configuration Protocol Version", G3ProtocolVersion);
    extraProperties.put("Module System API Version", ModuleSystemAPIVersion);
    extraProperties.put("Java Running Environment", System.getProperty("java.version"));
    extraProperties.put("Profile", profile);
    
    
    
    manifest.readFromManifest();
    manifest.addProperties(extraProperties);
  }
  
  public ManifestInfo getManifest() {
    return manifest;
  }
}

