/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: AutomationLogic.cpp 28 Jun 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/CLogic/src/AutomationLogic.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Maximum, minimum, and average values calculation Control Logic.
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 28 Jun 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   28 Jun 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "AutomationLogic.h"
#include "LockingMutex.h"
#include "dbg.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
AutomationLogic::AutomationLogic( Mutex* mutex,
                                        GeminiDatabase& g3database,
                                        AutomationLogic::Config& config,
                                        CONTROL_LOGIC_TYPE type,
                                        AutomationManager* manager
                                        ) :
                                        OperatableControlLogic(mutex, g3database, config, type),
                                        m_schemeID(config.schemeID),
                                        m_autoStart(config.autoStart),
                                        mp_constants(NULL),
                                        m_constantsNum(config.constantsNum),
                                        mp_inputs(NULL),
                                        m_inputNum(config.inputsNum),
                                        mp_outputs(NULL),
                                        m_outputNum(config.outputsNum),
                                        mp_manager(manager)
{
    strncpy(m_libFileName, config.libSchemeFile, LIBRARY_FILE_NAME_LEN);
    strncpy(m_resName,     config.resName, LIBRARY_FILE_NAME_LEN);

    /* Allocate Control Logic virtual points */
    clPoint.reserve(config.allPointNum);  //Adjust vector size to do not waste memory

    for(lu_uint32_t id = 0; id < config.bPointNum; ++id)
    {
        clPoint.push_back(new ControlLogicDigitalPoint(mutex, database, config.bPoint[id],POINT_TYPE_BINARY));
    }

    for(lu_uint32_t id = 0; id < config.dbPointNum; ++id)
    {
        clPoint.push_back(new ControlLogicDigitalPoint(mutex, database, config.dbPoint[id],POINT_TYPE_DBINARY));
    }

    for(lu_uint32_t id = 0; id < config.aPointNum; ++id)
    {
        clPoint.push_back(new ControlLogicAnaloguePoint(mutex, database, config.aPoint[id]));
    }

    for(lu_uint32_t id = 0; id < config.cPointNum; ++id)
    {
        clPoint.push_back(new ControlLogicCounterPoint(mutex, database, config.cPoint[id]));
    }

    /* Create constants */
    mp_constants = new lu_int32_t[m_constantsNum];
    for(lu_uint32_t i = 0; i < m_constantsNum; ++i)
    {
        mp_constants[i] = config.constants[i];
    }

    /* Create inputs */
    mp_inputs = new LogicInput*[m_inputNum];
    for(lu_uint32_t i = 0; i < m_inputNum; ++i)
    {
    	mp_inputs[i] = new LogicInput(this, config.inputs[i], i);
    }

    /* Create outputs*/
    mp_outputs = new LogicOutput*[m_outputNum];
    for(lu_uint32_t i = 0; i < m_outputNum; ++i)
    {
    	mp_outputs[i] = new LogicOutput(config.outputs[i]);
    }
}


AutomationLogic::~AutomationLogic()
{
	//Prevent further point updates
    stopUpdate();

	// Stop automation
    stop();

    if (mp_manager != NULL)
    {
        delete mp_manager;
        mp_manager = NULL;
    }

    /* Release resources*/
    delete[] mp_constants;

    for(lu_uint32_t i = 0; i < m_inputNum; ++i)
    {
        detachObserver(mp_inputs[i]);   //detaches and deletes the observer
    }
    delete[] mp_inputs;

    for(lu_uint32_t i = 0; i < m_outputNum; ++i)
    {
        delete mp_outputs[i];
        mp_outputs[i] = NULL;
    }
    delete[] mp_outputs;
}

GDB_ERROR AutomationLogic::init()
{
    GDB_ERROR ret = initCheck();
    if(ret == GDB_ERROR_NONE)
    {
        ret = activateCLPoints();   //Initialise Control Logic points as well
    }
    if(ret == GDB_ERROR_NOT_ENABLED)
    {
        log.warn("Automation Logic %d is disabled", m_schemeID);
        DBG_WARN("Automation Logic %d is disabled", m_schemeID);
        return GDB_ERROR_NONE;
    }
    if(ret != GDB_ERROR_NONE)
    {
        return ret;
    }

    startCLPoints(true);
    initialised = true;

	/* Initialise input points */
	for(lu_uint32_t i = 0; i < m_inputNum; ++i)
	{
	    ret = attachObserver(mp_inputs[i]);
        if(ret != GDB_ERROR_NONE)
        {
            break;  //The input has failed to attach: generate an error
        }
	}
    if(ret != GDB_ERROR_NONE)
    {
        //Detach all already attached and disable Control Logic indications
        for(lu_uint32_t j = 0; j < m_inputNum; ++j)
        {
            detachObserver(mp_inputs[j]);
        }
        initialised = false;
        startCLPoints(false);
        log.error("Failed to initialise AutomationLogic. err: %s",GDB_ERROR_ToSTRING(ret));
        DBG_ERR  ("Failed to initialise AutomationLogic. err: %s",GDB_ERROR_ToSTRING(ret));
    }
    else
	{
        /* Initialise engine points to online */
        mp_manager->init();

        /* Start automation if configured as "Auto Start".*/
        if (m_autoStart)
        {
            database.addJob(new AutoStartJob(*this));
        }
        else
        {
            log.warn("Automation Scheme is not auto started (%s). ", getResName());
            DBG_WARN("Automation Scheme is not auto started (%s). ", getResName());
        }
    }

    return ret;
}



GDB_ERROR AutomationLogic::setPointValue(lu_uint16_t index, lu_int32_t value, bool online)
{
    if(index >= clPoint.size())
        return GDB_ERROR_NOPOINT;

    TimeManager::TimeStr timeStamp = timeManager.now();
    PointFlags ptFlags(PointFlags::VALID, online);

    POINT_TYPE type = getPointType(index);
    if(type == POINT_TYPE_ANALOGUE)
    {
        lu_float32_t fvalue = *((lu_float32_t*)(&value));
        return setValue(index, fvalue, timeStamp, ptFlags, LU_TRUE);
    }
    else if(type == POINT_TYPE_BINARY||type == POINT_TYPE_DBINARY||type == POINT_TYPE_COUNTER)
    {
        return setValue(index, value, timeStamp, ptFlags, LU_TRUE);
    }
    else
    {
        return GDB_ERROR_NOPOINT;
    }
}

GDB_ERROR AutomationLogic::getPointValue(lu_uint16_t index, lu_int32_t* valuePtr, bool* online)
{
    GDB_ERROR err;

    if(index >= clPoint.size())
        return GDB_ERROR_NOPOINT;

    IPoint* pointCL = clPoint.at(index);
    PointData* data = NULL;

    POINT_TYPE type = pointCL->getType();
    switch(type)
    {
        case POINT_TYPE_ANALOGUE:
            data = new PointDataFloat32;
            break;

        case POINT_TYPE_BINARY:
        case POINT_TYPE_DBINARY:
            data = new PointDataUint8;
            break;

        case POINT_TYPE_COUNTER:
            data = new PointDataCounter32;
            break;

        default:
            data = NULL;
            break;
    }
    err = pointCL->getValue(data);

    if(err == GDB_ERROR_NONE)
    {
        if(type == POINT_TYPE_ANALOGUE)
        {
            lu_float32_t fval = *data;
            *valuePtr = *((lu_int32_t*)(&fval));
        }
        else
        {
            *valuePtr = *data;
        }
        *online = data->getFlags().isOnline();
    }
    else
    {
        log.error("Failed to get point[%d] value. GDB_ERROR:%d", index, err);
    }

    delete data;
    return err;
}

GDB_ERROR AutomationLogic::start()
{
    GDB_ERROR err = GDB_ERROR_NONE;

    if(!initialised)
    {
        return GDB_ERROR_NOT_INITIALIZED;
    }
    if(!enabled)
    {
        return GDB_ERROR_NONE;
    }

    /* Get inhibit*/
    PointDataCounter32 inhibit;
    if(isInhibited())
    {
       err = GDB_ERROR_INHIBIT;
    }

    /* Check inhibit*/
    if(err == GDB_ERROR_INHIBIT)
    {
        log.warn("Automation Scheme is not started due to inhibition. ");
        DBG_WARN("Automation Scheme is not started due to inhibition. ");
        logEvent(EVENT_TYPE_AUTO_INHIBITED);
    }
    else
    {
        if(mp_manager->start() == LU_FALSE)
        {
            err = GDB_ERROR_ADDJOB;
        }
    }

    return err;
}

GDB_ERROR AutomationLogic::stop()
{
    if(mp_manager != NULL)
    {
        if(mp_manager->stop() == LU_TRUE)
        {
            logEvent(EVENT_TYPE_AUTO_STOPPED);
        }
        return GDB_ERROR_NONE;
    }
    return GDB_ERROR_NOT_SUPPORTED;
}

GDB_ERROR AutomationLogic::operateOutput(lu_uint16_t index, lu_uint16_t opCode)
{
    GDB_ERROR err = GDB_ERROR_NOPOINT;

	if(index < m_outputNum)
	{
		OLR_STATE olr = OLR_STATE_OFF;
		database.getLocalRemote(olr);

		SwitchLogicOperation sglOperation;
		sglOperation.operation = static_cast<SWITCH_OPERATION>(opCode);
		sglOperation.local = olr == OLR_STATE_LOCAL;
		sglOperation.duration = 0;

		err = database.startOperation(mp_outputs[index]->outputLogicId, sglOperation);
	}

	if(err != GDB_ERROR_NONE)
	{
	    log.error("Output operation failed. Index: %d, logic id:%d, resource:%s",
	                    index,m_outputNum, getResName());
	}
	return err;
}

GDB_ERROR AutomationLogic::getInputValue(lu_uint16_t index, PointData* data)
{
    if( (data == NULL) || (index >= m_inputNum))
    {
        return GDB_ERROR_PARAM;
    }
    if(mp_inputs[index] != NULL)
    {
        return database.getValue(mp_inputs[index]->getPointID(), data);
    }
    return GDB_ERROR_NOPOINT;
}

POINT_TYPE AutomationLogic::getInputType(lu_uint16_t index)
{
    if(index >= m_inputNum)
    {
        return POINT_TYPE_INVALID;
    }
    else
    {
        return mp_inputs[index]->getInputType();
    }
}

POINT_TYPE AutomationLogic::getPointType (lu_uint16_t index)
{
    if(index >= clPoint.size())
        return POINT_TYPE_INVALID;

    IPoint* pointCL = clPoint.at(index);
    if(pointCL != NULL)
        return pointCL->getType();
    else
        return POINT_TYPE_INVALID;
}

GDB_ERROR AutomationLogic::getConstantValue(lu_uint16_t index, lu_int32_t& value)
{
    if( (index >= m_constantsNum)
      )
    {
        return GDB_ERROR_PARAM;
    }

    value =	mp_constants[index];
    return GDB_ERROR_NONE;
}

const lu_char_t* AutomationLogic::getLibName()
{
    return &m_libFileName[0];
}

const lu_char_t* AutomationLogic::getResName()
{
    return &m_resName[0];
}

lu_int32_t AutomationLogic::getSchemeID()
{
    return m_schemeID;;
}


void AutomationLogic::logEvent(const EVENT_TYPE_AUTOMATION evtType)
{
    SLEventEntryStr event;

    // Leave event manager to set the timestamp
    event.timestamp.sec = 0;
    event.timestamp.nsec = 0;

    event.eventClass = CTEventAutoStr::EventClass;
    event.payload.eventAuto.eventType = static_cast<lu_uint16_t>(evtType);
    eventLog->logEvent(event);
}
/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
GDB_ERROR AutomationLogic::startOp(const SwitchLogicOperation &operation, const GDB_ERROR errorCode)
{
    LU_UNUSED(errorCode);

    GDB_ERROR err = GDB_ERROR_NONE;

    if(operation.operation == SWITCH_OPERATION_OPEN)
    {
        err = stop();
    }
    else if(operation.operation == SWITCH_OPERATION_CLOSE)
    {
        err = start();
    }

    if(err != GDB_ERROR_NONE)
    {
        log.error("Failed to start/stop Automation.GDB_ERROR: %s",GDB_ERROR_ToSTRING(err));
        DBG_ERR("Failed to start/stop Automation.GDB_ERROR: %s",GDB_ERROR_ToSTRING(err));
    }

    return err;
}

void AutomationLogic::handleInputChange(lu_uint32_t index, POINT_TYPE type, PointData *newPointData)
{
    LU_UNUSED(type);

    /* Handle inhibit value change */
    if (index == AUTO_INHIBIT_INDEX)
    {
        lu_int8_t val = *((PointDataUint8*)(newPointData));
        bool inhibited = (val == AUTO_INHIBIT_TRUE);
        mp_manager->setAutoInhibited(inhibited);

        if (inhibited )
        {
            stop();
        }
        else if(isAuotstartEnabled())
        {
            start();
        }
    }
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 * Check inhibit and start/stop automation scheme.
 */
void AutomationLogic::LogicInput::update(PointIdStr pointID, PointData *newPointData)
{
	if(pointID == m_pid)
	{
	    // Update type
	    if(m_type == POINT_TYPE_INVALID)
	        m_type = mp_logic -> getInputType(m_index);

	    mp_logic->handleInputChange(m_index, m_type, newPointData);
	}
}


PointIdStr AutomationLogic::LogicInput::getPointID()
{
	return m_pid;
}

POINT_TYPE AutomationLogic::LogicInput::getInputType()
{
   if(m_type == POINT_TYPE_INVALID)
       m_type = mp_logic -> getDB().getPointType(getPointID());
   return m_type;
}

bool AutomationLogic::isInhibited(){
    PointDataUint8 val;
    if(getInputValue(AUTO_INHIBIT_INDEX, &val) == GDB_ERROR_NONE) {
        lu_int8_t inhibitValue = val;
        if(inhibitValue == AUTO_INHIBIT_TRUE)
        {
            return true;
        }
    }

    return false;
}
/*
 *********************** End of file ******************************************
 */
