/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: mmbsim.h
 * description: Simulates a MB master database.
 */
#include "tmwscl/modbus/mbdiag.h"

#include "tmwscl/utils/tmwcnfg.h"

#if TMWCNFG_USE_SIMULATED_DB

#include "tmwscl/modbus/mmbsim.h"
#include "tmwscl/modbus/mmbmem.h"

#include "tmwscl/utils/tmwtarg.h"
#include "tmwscl/utils/tmwmsim.h"
#include "tmwscl/utils/tmwsim.h"

/* Call update callback if set */
static void _callCallback(
  MMBSIM_DATABASE *pDbHandle,
  TMWSIM_EVENT_TYPE type,
  TMWTYPES_UCHAR dataType,
  TMWTYPES_USHORT address)
{
  if(pDbHandle->pUpdateCallback != TMWDEFS_NULL)
  {
    pDbHandle->pUpdateCallback(pDbHandle->pUpdateCallbackParam, type, dataType, address);
  }
}

/* Delete all points in the database */
static void TMWDEFS_LOCAL _clearDb(
  void *pHandle)
{
  void *pPoint;
  MMBSIM_DATABASE *pDbHandle = (MMBSIM_DATABASE *)pHandle;
  _callCallback(pDbHandle, TMWSIM_CLEAR_DATABASE, 0, 0);
 
  while((pPoint = tmwsim_tableFindPointByIndex(&pDbHandle->discreteInputs, 0)) != TMWDEFS_NULL)
  {
    mmbsim_deleteDiscreteInput(pDbHandle, (TMWTYPES_USHORT)tmwsim_getPointNumber((TMWSIM_POINT *)pPoint));  
  } 
  tmwsim_tableDestroy(&pDbHandle->discreteInputs);
  
  while((pPoint = tmwsim_tableFindPointByIndex(&pDbHandle->coils, 0)) != TMWDEFS_NULL)
  {
    mmbsim_deleteCoil(pDbHandle, (TMWTYPES_USHORT)tmwsim_getPointNumber((TMWSIM_POINT *)pPoint));  
  } 
  tmwsim_tableDestroy(&pDbHandle->coils);

  while((pPoint = tmwsim_tableFindPointByIndex(&pDbHandle->inputRegisters, 0)) != TMWDEFS_NULL)
  {
    mmbsim_deleteInputRegister(pDbHandle, (TMWTYPES_USHORT)tmwsim_getPointNumber((TMWSIM_POINT *)pPoint));  
  } 
  tmwsim_tableDestroy(&pDbHandle->inputRegisters);
   
  while((pPoint = tmwsim_tableFindPointByIndex(&pDbHandle->holdingRegisters, 0)) != TMWDEFS_NULL)
  {
    mmbsim_deleteHoldingRegister(pDbHandle, (TMWTYPES_USHORT)tmwsim_getPointNumber((TMWSIM_POINT *)pPoint));  
  } 
  tmwsim_tableDestroy(&pDbHandle->holdingRegisters);
  
  while((pPoint = tmwsim_tableFindPointByIndex(&pDbHandle->deviceIds, 0)) != TMWDEFS_NULL)
  {
    mmbsim_deleteDeviceId(pDbHandle, (TMWTYPES_UCHAR)tmwsim_getPointNumber((TMWSIM_POINT *)pPoint));  
  } 
  tmwsim_tableDestroy(&pDbHandle->deviceIds);
}

/* function: mmbsim_init */
void * TMWDEFS_GLOBAL mmbsim_init(
  TMWSESN *pSession)
{
  MMBSIM_DATABASE *pDbHandle;

  TMWTARG_UNUSED_PARAM(pSession);

  pDbHandle = (MMBSIM_DATABASE *)mmbmem_alloc(MMBMEM_SIM_DATABASE_TYPE);
  if(pDbHandle != TMWDEFS_NULL)
  {  
    memset(pDbHandle, 0, sizeof(MMBSIM_DATABASE));

    tmwsim_tableCreate(&pDbHandle->discreteInputs);
    tmwsim_tableCreate(&pDbHandle->coils);
    tmwsim_tableCreate(&pDbHandle->inputRegisters);
    tmwsim_tableCreate(&pDbHandle->holdingRegisters);
    tmwsim_tableCreate(&pDbHandle->deviceIds);
    
    pDbHandle->exceptionStatus = 0;

    pDbHandle->pUpdateCallback = TMWDEFS_NULL;
    pDbHandle->pUpdateCallbackParam = TMWDEFS_NULL;
  }

  return(pDbHandle);
}

/* function: mmbsim_close */
void TMWDEFS_GLOBAL mmbsim_close(
  void *pDbHandle)
{
  _clearDb(pDbHandle);
  mmbmem_free(pDbHandle);
}

/* Clear database */
void TMWDEFS_GLOBAL mmbsim_clear(
  void *pHandle)
{
  MMBSIM_DATABASE *pDbHandle = (MMBSIM_DATABASE *)pHandle;

  _clearDb(pHandle);

  /* recreate these so that add individual points can be done */
  tmwsim_tableCreate(&pDbHandle->discreteInputs);
  tmwsim_tableCreate(&pDbHandle->coils);
  tmwsim_tableCreate(&pDbHandle->inputRegisters);
  tmwsim_tableCreate(&pDbHandle->holdingRegisters); 
  tmwsim_tableCreate(&pDbHandle->deviceIds); 
}

/* Set update callback and parameter */
void mmbsim_setCallback(
  void *pHandle,
  MMBSIM_CALLBACK_FUNC pUpdateCallback,
  void *pUpdateCallbackParam)
{
  MMBSIM_DATABASE *pDbHandle = (MMBSIM_DATABASE *)pHandle;
  pDbHandle->pUpdateCallback = pUpdateCallback;
  pDbHandle->pUpdateCallbackParam = pUpdateCallbackParam;
}

/* function: mmbsim_getPointNumber */
TMWTYPES_USHORT TMWDEFS_GLOBAL mmbsim_getPointNumber(
  void *pPoint)
{
  return((TMWTYPES_USHORT)tmwsim_getPointNumber((TMWSIM_POINT *)pPoint));
}

/* function: mmbsim_addCoil */
TMWSIM_POINT *TMWDEFS_GLOBAL mmbsim_addCoil(
  void *pHandle,
  TMWTYPES_USHORT pointNumber)
{
  TMWSIM_POINT *pPoint;
  MMBSIM_DATABASE *pDbHandle = (MMBSIM_DATABASE *)pHandle;
  pPoint = tmwsim_tableAdd(&pDbHandle->coils, pointNumber);
  if(pPoint != TMWDEFS_NULL)
  { 
    tmwsim_initBinary(pPoint, pHandle, pointNumber);
    _callCallback(pDbHandle, TMWSIM_POINT_ADD, MBDEFS_OBJ_COIL, pointNumber);
  }
  return(pPoint);
}

/* function: mmbsim_deleteCoil */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_deleteCoil(
  void *pHandle,
  TMWTYPES_USHORT pointNumber)
{
  MMBSIM_DATABASE *pDbHandle = (MMBSIM_DATABASE *)pHandle;
  _callCallback(pDbHandle, TMWSIM_POINT_DELETE, MBDEFS_OBJ_COIL, pointNumber);
  return(tmwsim_tableDelete(&pDbHandle->coils, pointNumber));
}

/* function: mmbsim_coilLookupPoint */
TMWSIM_POINT *TMWDEFS_GLOBAL mmbsim_coilLookupPoint(
  void *pHandle,
  TMWTYPES_USHORT pointNumber)
{
  MMBSIM_DATABASE *pDbHandle = (MMBSIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPoint(&pDbHandle->coils, pointNumber));
}

/* function: mmbsim_coilRead */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_coilRead(
  void *pPoint,
  TMWTYPES_UCHAR *pValue)
{
  if(pPoint == TMWDEFS_NULL)
    return(TMWDEFS_FALSE);

  if(tmwsim_getBinaryValue((TMWSIM_POINT *)pPoint))
    *pValue = TMWDEFS_TRUE;
  else
    *pValue = TMWDEFS_FALSE;

  return(TMWDEFS_TRUE);
}

/* function: mmbsim_storeCoils */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_storeCoils(
  void *pHandle,
  TMWTYPES_USHORT startAddr,
  TMWTYPES_USHORT length,
  TMWTYPES_UCHAR *pValueArray)
{
  MMBSIM_DATABASE *pDbHandle = (MMBSIM_DATABASE *)pHandle;
  TMWTYPES_BOOL bVal;
  TMWTYPES_USHORT i;

  for(i = 0; i < length; i++)
  {
    TMWTYPES_SHORT point = i + startAddr;
    TMWSIM_POINT *pPoint = mmbsim_coilLookupPoint(pDbHandle, point);
    /* If the point does not exist, add it */
    if(pPoint == TMWDEFS_NULL)
    {
      pPoint = mmbsim_addCoil(pDbHandle, point);
      if(pPoint == TMWDEFS_NULL)
        return(TMWDEFS_FALSE);
    }

    /* Set the value */
    bVal = (TMWTYPES_BOOL)((pValueArray[i/8] >> i%8) & 0x01);
    tmwsim_setBinaryValue(pPoint, bVal, TMWDEFS_CHANGE_NONE);
    _callCallback((MMBSIM_DATABASE *)pHandle, TMWSIM_POINT_UPDATE, MBDEFS_OBJ_COIL, point);
  }
  return(TMWDEFS_TRUE);
}

/* function: mmbsim_addDiscreteInput */
TMWSIM_POINT *TMWDEFS_GLOBAL mmbsim_addDiscreteInput(
  void *pHandle,
  TMWTYPES_USHORT pointNumber)
{
  TMWSIM_POINT *pPoint;
  MMBSIM_DATABASE *pDbHandle = (MMBSIM_DATABASE *)pHandle;
  pPoint = tmwsim_tableAdd(&pDbHandle->discreteInputs, pointNumber);
  if(pPoint != TMWDEFS_NULL)
  { 
    tmwsim_initBinary(pPoint, pHandle, pointNumber);
    _callCallback(pDbHandle, TMWSIM_POINT_ADD, MBDEFS_OBJ_DISCRETE_INPUT_REGISTER, pointNumber);
  }
  return(pPoint);
}

/* function: mmbsim_deleteDiscreteInput */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_deleteDiscreteInput(
  void *pHandle,
  TMWTYPES_USHORT pointNumber)
{
  MMBSIM_DATABASE *pDbHandle = (MMBSIM_DATABASE *)pHandle;
  _callCallback(pDbHandle, TMWSIM_POINT_DELETE, MBDEFS_OBJ_DISCRETE_INPUT_REGISTER, pointNumber);
  return(tmwsim_tableDelete(&pDbHandle->discreteInputs, pointNumber));
}

/* function: mmbsim_discreteInputLookupPoint */
TMWSIM_POINT *TMWDEFS_GLOBAL mmbsim_discreteInputLookupPoint(
  void *pHandle,
  TMWTYPES_USHORT pointNumber)
{
  MMBSIM_DATABASE *pDbHandle = (MMBSIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPoint(&pDbHandle->discreteInputs, pointNumber));
}

/* function: mmbsim_discreteInputRead */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_discreteInputRead(
  void *pPoint,
  TMWTYPES_UCHAR *pValue)
{
  if(pPoint == TMWDEFS_NULL)
    return(TMWDEFS_FALSE);

  if(tmwsim_getBinaryValue((TMWSIM_POINT *)pPoint))
    *pValue = TMWDEFS_TRUE;
  else
    *pValue = TMWDEFS_FALSE;

  return(TMWDEFS_TRUE);
}

/* function: mmbsim_storeDiscreteInputs */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_storeDiscreteInputs(
  void *pHandle,
  TMWTYPES_USHORT startAddr,
  TMWTYPES_USHORT length,
  TMWTYPES_UCHAR *pValueArray)
{
  MMBSIM_DATABASE *pDbHandle = (MMBSIM_DATABASE *)pHandle;
  TMWTYPES_BOOL bVal;
  TMWTYPES_USHORT i;

  for(i = 0; i < length; i++)
  {
    TMWTYPES_USHORT point = i + startAddr;
    TMWSIM_POINT *pPoint = mmbsim_discreteInputLookupPoint(pDbHandle, point);
    /* If the point does not exist, add it */
    if(pPoint == TMWDEFS_NULL)
    {
      pPoint = mmbsim_addDiscreteInput(pDbHandle, point);
      if(pPoint == TMWDEFS_NULL)
        return(TMWDEFS_FALSE);
    }

    /* Set the value */
    bVal = (TMWTYPES_BOOL)((((pValueArray[i/8] >> i%8) & 0x01) != 0) ? TMWDEFS_TRUE : TMWDEFS_FALSE);
    tmwsim_setBinaryValue(pPoint, bVal, TMWDEFS_CHANGE_NONE);
    _callCallback((MMBSIM_DATABASE *)pHandle, TMWSIM_POINT_UPDATE, MBDEFS_OBJ_DISCRETE_INPUT_REGISTER, point);
  }

  return(TMWDEFS_TRUE);
}

/* function: mmbsim_addInputRegister */
TMWSIM_POINT *TMWDEFS_GLOBAL mmbsim_addInputRegister(
  void *pHandle,
  TMWTYPES_USHORT pointNumber)
{ 
  TMWSIM_POINT *pPoint;
  MMBSIM_DATABASE *pDbHandle = (MMBSIM_DATABASE *)pHandle;
  pPoint = tmwsim_tableAdd(&pDbHandle->inputRegisters, pointNumber);
  if(pPoint != TMWDEFS_NULL)
  {
    tmwsim_initAnalog(pPoint, pHandle, pointNumber, 0, 65535, 65535, 0);
    _callCallback(pDbHandle, TMWSIM_POINT_ADD, MBDEFS_OBJ_INPUT_REGISTER, pointNumber);
  }

  return(pPoint);
}

/* function: mmbsim_deleteInputRegister */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_deleteInputRegister(
  void *pHandle,
  TMWTYPES_USHORT pointNumber)
{
  MMBSIM_DATABASE *pDbHandle = (MMBSIM_DATABASE *)pHandle;
  _callCallback(pDbHandle, TMWSIM_POINT_DELETE, MBDEFS_OBJ_INPUT_REGISTER, pointNumber);
  return(tmwsim_tableDelete(&pDbHandle->inputRegisters, pointNumber));
}

/* function: mmbsim_inputRegisterLookupPoint */
TMWSIM_POINT *TMWDEFS_GLOBAL mmbsim_inputRegisterLookupPoint(
  void *pHandle,
  TMWTYPES_USHORT pointNumber)
{
  MMBSIM_DATABASE *pDbHandle = (MMBSIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPoint(&pDbHandle->inputRegisters, pointNumber));
}

/* function: mmbsim_inputRegisterRead */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_inputRegisterRead(
  void *pPoint,
  TMWTYPES_USHORT *pValue)
{
  if(pPoint == TMWDEFS_NULL)
    return(TMWDEFS_FALSE);

  *pValue = (TMWTYPES_USHORT)tmwsim_getAnalogValue((TMWSIM_POINT *)pPoint);
  return(TMWDEFS_TRUE);
}

/* function: mmbsim_storeInputRegister */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_storeInputRegisters(
  void *pHandle,
  TMWTYPES_USHORT startAddr,
  TMWTYPES_USHORT length,
  TMWTYPES_USHORT *pValueArray)
{
  MMBSIM_DATABASE *pDbHandle = (MMBSIM_DATABASE *)pHandle;
  TMWTYPES_USHORT value;
  TMWTYPES_USHORT i;

  for(i = 0; i < length; i++)
  {
    TMWTYPES_USHORT point = i + startAddr;

    /* Get point to set, if the point does not exist, add it */
    TMWSIM_POINT *pPoint = mmbsim_inputRegisterLookupPoint(pDbHandle, point);   
    if(pPoint == TMWDEFS_NULL)
    {
      pPoint = mmbsim_addInputRegister(pDbHandle, point);
      if(pPoint == TMWDEFS_NULL)
        return(TMWDEFS_FALSE);
    }

    value = *(pValueArray + i);
   
    /* Set the value */
    tmwsim_setAnalogValue((TMWSIM_POINT *)pPoint, value, TMWDEFS_CHANGE_NONE);
    _callCallback(pDbHandle, TMWSIM_POINT_UPDATE, MBDEFS_OBJ_INPUT_REGISTER, point);
  }     

  return(TMWDEFS_TRUE);
}

/* function: mmbsim_addHoldingRegister */
TMWSIM_POINT *TMWDEFS_GLOBAL mmbsim_addHoldingRegister(
  void *pHandle,
  TMWTYPES_USHORT pointNumber)
{
  TMWSIM_POINT *pPoint;
  MMBSIM_DATABASE *pDbHandle = (MMBSIM_DATABASE *)pHandle;
  pPoint = tmwsim_tableAdd(&pDbHandle->holdingRegisters, pointNumber);
  if(pPoint != TMWDEFS_NULL)
  {
    tmwsim_initAnalog(pPoint, pHandle, pointNumber, 0, 65535, 65535, 0);
    _callCallback(pDbHandle, TMWSIM_POINT_ADD, MBDEFS_OBJ_HOLDING_REGISTER, pointNumber);
  }
  return(pPoint);
}

/* function: mmbsim_deleteHoldingRegister */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_deleteHoldingRegister(
  void *pHandle,
  TMWTYPES_USHORT pointNumber)
{
  MMBSIM_DATABASE *pDbHandle = (MMBSIM_DATABASE *)pHandle;
  _callCallback(pDbHandle, TMWSIM_POINT_DELETE, MBDEFS_OBJ_HOLDING_REGISTER, pointNumber);
  return(tmwsim_tableDelete(&pDbHandle->holdingRegisters, pointNumber));
}

/* function: mmbsim_holdingRegisterLookupPoint */
TMWSIM_POINT *TMWDEFS_GLOBAL mmbsim_holdingRegisterLookupPoint(
  void *pHandle,
  TMWTYPES_USHORT pointNumber)
{
  MMBSIM_DATABASE *pDbHandle = (MMBSIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPoint(&pDbHandle->holdingRegisters, pointNumber));
}

/* function: mmbsim_holdingRegisterRead */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_holdingRegisterRead(
  void *pPoint,
  TMWTYPES_USHORT *pValue)
{
  if(pPoint == TMWDEFS_NULL)
    return(TMWDEFS_FALSE);

  *pValue = (TMWTYPES_USHORT)tmwsim_getAnalogValue((TMWSIM_POINT *)pPoint);
  return(TMWDEFS_TRUE);
}

/* function: mmbsim_storeHoldingRegister */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_storeHoldingRegisters(
  void *pHandle,
  TMWTYPES_USHORT startAddr,
  TMWTYPES_USHORT length,
  TMWTYPES_USHORT *pValueArray)
{
  MMBSIM_DATABASE *pDbHandle = (MMBSIM_DATABASE *)pHandle;
  TMWTYPES_USHORT value;
  TMWTYPES_USHORT i;

  for(i = 0; i < length; i++)
  {
    TMWTYPES_USHORT point = i + startAddr;

    /* Get point to set, if the point does not exist, add it */
    TMWSIM_POINT *pPoint = mmbsim_holdingRegisterLookupPoint(pDbHandle, point);   
    if(pPoint == TMWDEFS_NULL)
    {
      pPoint = mmbsim_addHoldingRegister(pDbHandle, point);
      if(pPoint == TMWDEFS_NULL)
        return(TMWDEFS_FALSE);
    }

    value = *(pValueArray + i);
    /* Set the value */
    tmwsim_setAnalogValue((TMWSIM_POINT *)pPoint, value, TMWDEFS_CHANGE_NONE);
    _callCallback(pDbHandle, TMWSIM_POINT_UPDATE, MBDEFS_OBJ_HOLDING_REGISTER, point); 
  }

  return(TMWDEFS_TRUE);
}

/* function: mmbsim_storeExceptionStatus */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_storeExceptionStatus(
  void *pHandle,
  TMWTYPES_UCHAR value)
{
  MMBSIM_DATABASE *pDbHandle = (MMBSIM_DATABASE *)pHandle;
  pDbHandle->exceptionStatus = value;
  return(TMWDEFS_TRUE);
}

/* function: mmbsim_exceptionStatusRead */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_exceptionStatusRead(
  void *pHandle,
  TMWTYPES_UCHAR *pValue)
{
  MMBSIM_DATABASE *pDbHandle = (MMBSIM_DATABASE *)pHandle;
   *pValue = pDbHandle->exceptionStatus;
  return(TMWDEFS_TRUE);
}

/* function: mmbsim_addDeviceId */
TMWSIM_POINT *TMWDEFS_GLOBAL mmbsim_addDeviceId(
  void *pHandle,
  TMWTYPES_UCHAR objectId)
{
  TMWSIM_POINT *pPoint;
  MMBSIM_DATABASE *pDbHandle = (MMBSIM_DATABASE *)pHandle;
  pPoint = tmwsim_tableAdd(&pDbHandle->deviceIds, objectId);
  if(pPoint != TMWDEFS_NULL)
  {
    tmwsim_initString(pPoint, pHandle, objectId);
    _callCallback(pDbHandle, TMWSIM_POINT_ADD, MBDEFS_OBJ_DEVICE_ID, objectId);
  }
  return(pPoint);
}

/* function: mmbsim_deleteDeviceId */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_deleteDeviceId(
  void *pHandle,
  TMWTYPES_UCHAR objectId)
{
  MMBSIM_DATABASE *pDbHandle = (MMBSIM_DATABASE *)pHandle;
  _callCallback(pDbHandle, TMWSIM_POINT_DELETE, MBDEFS_OBJ_DEVICE_ID, objectId);
  return(tmwsim_tableDelete(&pDbHandle->deviceIds, objectId));
}

/* function: mmbsim_deviceIdLookupPoint */
TMWSIM_POINT *TMWDEFS_GLOBAL mmbsim_deviceIdLookupPoint(
  void *pHandle,
  TMWTYPES_UCHAR objectId)
{
  MMBSIM_DATABASE *pDbHandle = (MMBSIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPoint(&pDbHandle->deviceIds, objectId));
}

/* function: mmbsim_deviceIdRead */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_deviceIdRead(
  void *pPoint,
  TMWTYPES_UCHAR *pLength,
  TMWTYPES_UCHAR *pBuf)
{
  if(pPoint != TMWDEFS_NULL)
  {
    TMWTYPES_UCHAR maxLength = *pLength;
    return(tmwsim_getStringValue((TMWSIM_POINT *)pPoint, maxLength, pBuf, pLength));
  }
  return(TMWDEFS_FALSE);
}

/* function: mmbsim_storeDeviceId */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_storeDeviceId(
  void *pHandle,
  TMWTYPES_UCHAR objectId,
  TMWTYPES_UCHAR length,
  TMWTYPES_UCHAR *pBuf)
{
  MMBSIM_DATABASE *pDbHandle = (MMBSIM_DATABASE *)pHandle;
  TMWSIM_POINT *pPoint;
  pPoint = mmbsim_deviceIdLookupPoint(pHandle, objectId);
  if (pPoint == TMWDEFS_NULL)
  {
    pPoint = mmbsim_addDeviceId(pHandle, objectId);
  } 
  if(pPoint != TMWDEFS_NULL)
  {
    tmwsim_setStringValue(pPoint, pBuf, length, TMWDEFS_CHANGE_REMOTE_OP);
    _callCallback((MMBSIM_DATABASE *)pHandle, TMWSIM_POINT_UPDATE, MBDEFS_OBJ_DEVICE_ID, objectId);

    return TMWDEFS_TRUE;
  }

  return TMWDEFS_FALSE;
}

/* routine: mmbsim_saveDatabase */
TMWTYPES_CHAR * TMWDEFS_GLOBAL mmbsim_saveDatabase(
  TMWSESN *pSession)
{
  TMWTYPES_CHAR buf[256];
  TMWSIM_POINT *pPoint;
  MMBSIM_DATABASE *pDbHandle;
  MMBSESN *pMBSession = (MMBSESN *)pSession;
  TMWTYPES_CHAR *result = TMWDEFS_NULL;

  result = tmwtarg_appendString(result, "<?xml version=\"1.0\"?>\n");
  result = tmwtarg_appendString(result, "<tmw:mbdata\n");
  result = tmwtarg_appendString(result, " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n");
  result = tmwtarg_appendString(result, " xmlns:tmw=\"http://www.TriangleMicroWorks.com/TestHarness/Schemas/mbdata\">\n");
  result = tmwtarg_appendString(result, " <device>\n");

  pDbHandle = (MMBSIM_DATABASE *)pMBSession->mb.pDbHandle;
  pPoint = tmwsim_tableGetFirstPoint(&pDbHandle->coils);
  while(pPoint != TMWDEFS_NULL)
  {
    TMWTYPES_CHAR *desc = tmwsim_getDescription(pPoint);

    result = tmwtarg_appendString(result, "   <coil>\n");

    tmwtarg_snprintf(buf, sizeof(buf), "    <address>%d</address>\n", mmbsim_getPointNumber(pPoint));
    result = tmwtarg_appendString(result, buf);

    if(desc && (strlen(desc) > 0))
    {
      tmwtarg_snprintf(buf, sizeof(buf), "    <description>%s</description>\n", desc);
      result = tmwtarg_appendString(result, buf);
    }
    
    result = tmwtarg_appendString(result, "   </coil>\n");

    pPoint = tmwsim_tableGetNextPoint(&pDbHandle->coils, pPoint);
  }

  pPoint = tmwsim_tableGetFirstPoint(&pDbHandle->discreteInputs);
  while(pPoint != TMWDEFS_NULL)
  {
    TMWTYPES_CHAR *desc = tmwsim_getDescription(pPoint);

    result = tmwtarg_appendString(result, "   <discreteInput>\n");

    tmwtarg_snprintf(buf, sizeof(buf), "    <address>%d</address>\n", mmbsim_getPointNumber(pPoint));
    result = tmwtarg_appendString(result, buf);

    if(desc && (strlen(desc) > 0))
    {
      tmwtarg_snprintf(buf, sizeof(buf), "    <description>%s</description>\n", desc);
      result = tmwtarg_appendString(result, buf);
    }
    
    result = tmwtarg_appendString(result, "   </discreteInput>\n");

    pPoint = tmwsim_tableGetNextPoint(&pDbHandle->discreteInputs, pPoint);
  }

  pPoint = tmwsim_tableGetFirstPoint(&pDbHandle->inputRegisters);
  while(pPoint != TMWDEFS_NULL)
  {
    TMWTYPES_CHAR *desc = tmwsim_getDescription(pPoint);

    result = tmwtarg_appendString(result, "   <inputRegister>\n");

    tmwtarg_snprintf(buf, sizeof(buf), "    <address>%d</address>\n", mmbsim_getPointNumber(pPoint));
    result = tmwtarg_appendString(result, buf);

    if(desc && (strlen(desc) > 0))
    {
      tmwtarg_snprintf(buf, sizeof(buf), "    <description>%s</description>\n", desc);
      result = tmwtarg_appendString(result, buf);
    }
    
    result = tmwtarg_appendString(result, "   </inputRegister>\n");
     
    pPoint = tmwsim_tableGetNextPoint(&pDbHandle->inputRegisters, pPoint);
  }

  pPoint = tmwsim_tableGetFirstPoint(&pDbHandle->holdingRegisters);
  while(pPoint != TMWDEFS_NULL)
  {
    TMWTYPES_CHAR *desc = tmwsim_getDescription(pPoint);

    result = tmwtarg_appendString(result, "   <holdingRegister>\n");

    tmwtarg_snprintf(buf, sizeof(buf), "    <address>%d</address>\n", mmbsim_getPointNumber(pPoint));
    result = tmwtarg_appendString(result, buf);

    if(desc && (strlen(desc) > 0))
    {
      tmwtarg_snprintf(buf, sizeof(buf), "    <description>%s</description>\n", desc);
      result = tmwtarg_appendString(result, buf);
    }
    
    result = tmwtarg_appendString(result, "   </holdingRegister>\n");

    pPoint = tmwsim_tableGetNextPoint(&pDbHandle->holdingRegisters, pPoint);
  }

  result = tmwtarg_appendString(result, " </device>\n");
  result = tmwtarg_appendString(result, "</tmw:mbdata>\n");
  return(result);
}
#endif /* TMWCNFG_USE_SIMULATED_DB */