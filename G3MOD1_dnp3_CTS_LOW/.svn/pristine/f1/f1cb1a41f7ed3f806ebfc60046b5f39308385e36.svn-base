/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: MonitorProtocolLayer.cpp 23-Aug-2013 11:38:15 andrews_s $
 *               $HeadURL: C:\sw_dev\gemini_30_software\Development\G3\RTU\MCM\Common\MonitorProtocol\include\AbstractMonitorProtocolLayer.cpp $
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       AbstractMonitorProtocolLayer module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: andrews_s	$: (Author of last commit)
 *       \date   $Date: 23-Aug-2013 11:38:15	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   23-Aug-2013 11:38:15  andrews_s    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <string.h>  // strncpy

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "AbstractMonitorProtocolLayer.h"
#include "LogMessage.h"
#include "crc32.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */



AbstractMonitorProtocolLayer::AbstractMonitorProtocolLayer( APP_ID appID,
                SCHED_TYPE   schedType,
                lu_uint32_t  dataSocketPriority) :
                    applicationID(appID)
{
    DataLink    = new MonitorLinkLayer(schedType, dataSocketPriority,    XMSG_DATA_SOCK_NAME);

    if (DataLink != NULL)
    {
        DataLink->setProtocolLayer(this);
        DataLink->start();
    }
}


AbstractMonitorProtocolLayer::~AbstractMonitorProtocolLayer()
{}


AbstractMonitorProtocolLayer::MONPROTOCOL_ERROR  AbstractMonitorProtocolLayer::insertCRC(AppMsgStr *msg)
{
    lu_uint32_t calcCRC32 = 0;
    lu_uint8_t *ptr;
    lu_uint16_t i;

    if (msg == NULL)
    {
        return MONPROTOCOL_ERROR_INIT;
    }

    // Clear the CRC prior to calculation
    msg->header.crc32 = (lu_uint32_t) 0x00000000;

    // Initialise the CRC
    crc32_init(&calcCRC32);

    // As the AppMsgStr structure is not in contiguous memory we will have
    // to calculate the CRC byte by byte across the header and payload

    // Start calculating the header portion
    ptr = (lu_uint8_t *) msg;
    for (i = 0; (ptr != NULL) && (i < sizeof(AppMsgHeaderStr)); i++)
    {
        crc32_byte(*(ptr++), &calcCRC32);
    }

    // Continue calculating the payload portion
    ptr = (lu_uint8_t *) msg->payload;
    for (i = 0; (ptr != NULL) && (i < msg->header.payLoadLen); i++)
    {
        crc32_byte(*(ptr++), &calcCRC32);
    }

    // Set the CRC
    calcCRC32 ^= 0XFFFFFFFFL;
    msg->header.crc32 = calcCRC32;

    return MONPROTOCOL_ERROR_NONE;
}


AbstractMonitorProtocolLayer::MONPROTOCOL_ERROR AbstractMonitorProtocolLayer::send_req_ai_msg(lu_uint16_t channel)
{
    AppMsgStr     msg;
    PLGetAIValStr payload;

    if (DataLink == NULL)
    {
        return MONPROTOCOL_ERROR_INIT;
    }
    build_header(&(msg.header), (lu_uint16_t) XMSG_CMD_GET_AI_VAL, (lu_uint16_t)sizeof(PLGetAIValStr));

    // Set the channel only, the other member are not relevant
    payload.channel = channel;
    msg.payload = (void *) &payload;

    // Insert the CRC
    insertCRC(&msg);

    return (DataLink->sendMessage(msg) > 0) ? MONPROTOCOL_ERROR_NONE : MONPROTOCOL_ERROR_SEND;
}


AbstractMonitorProtocolLayer::MONPROTOCOL_ERROR AbstractMonitorProtocolLayer::send_req_app_ver_msg()
{
    AppMsgStr     msg;

    if (DataLink == NULL)
    {
        return MONPROTOCOL_ERROR_INIT;
    }
    build_header(&(msg.header), (lu_uint16_t) XMSG_CMD_GET_APP_VER, 0);

    msg.payload = NULL;

    // Insert the CRC
    insertCRC(&msg);

    return (DataLink->sendMessage(msg) > 0) ? MONPROTOCOL_ERROR_NONE : MONPROTOCOL_ERROR_SEND;
}


AbstractMonitorProtocolLayer::MONPROTOCOL_ERROR AbstractMonitorProtocolLayer::send_req_di_msg(lu_uint16_t channel)
{
    AppMsgStr     msg;
    PLGetDIValStr payload;

    if (DataLink == NULL)
    {
        return MONPROTOCOL_ERROR_INIT;
    }
    build_header(&(msg.header), (lu_uint16_t) XMSG_CMD_GET_DI_VAL, (lu_uint16_t)sizeof(PLGetDIValStr));

    // Set the channel only, the other member are not relevant
    payload.channel = channel;
    msg.payload = (void *) &payload;

    // Insert the CRC
    insertCRC(&msg);

    return (DataLink->sendMessage(msg) > 0) ? MONPROTOCOL_ERROR_NONE : MONPROTOCOL_ERROR_SEND;
}


AbstractMonitorProtocolLayer::MONPROTOCOL_ERROR AbstractMonitorProtocolLayer::send_set_ip_msg(lu_char_t *device, lu_uint32_t ipAddress, lu_uint32_t mask, lu_uint32_t gwAddress)
{
    AppMsgStr     msg;
    PLSetIPStr    payload;

    if (DataLink == NULL)
    {
        return MONPROTOCOL_ERROR_INIT;
    }
    build_header(&(msg.header), (lu_uint16_t) XMSG_CMD_SET_IP, (lu_uint16_t)sizeof(PLSetIPStr));

    // Default to 'eth0' if no device given
    if (device == NULL)
    {
        strncpy(payload.device, "eth0", sizeof(payload.device));
    }
    else
    {
        strncpy(payload.device, device, sizeof(payload.device));
    }

    payload.ipAddr = ipAddress;
    payload.mask   = mask;
    payload.gwAddr = gwAddress;

    msg.payload = (void *) &payload;

    // Insert the CRC
    insertCRC(&msg);

    return (DataLink->sendMessage(msg) > 0) ? MONPROTOCOL_ERROR_NONE : MONPROTOCOL_ERROR_SEND;
}


AbstractMonitorProtocolLayer::MONPROTOCOL_ERROR AbstractMonitorProtocolLayer::send_ping_msg()
{
    AppMsgStr     msg;

    if (DataLink == NULL)
    {
        return MONPROTOCOL_ERROR_INIT;
    }
    build_header(&(msg.header), (lu_uint16_t) XMSG_CMD_REQ_ALIVE, 0);

    msg.payload = NULL;

    // Insert the CRC
    insertCRC(&msg);

    return (DataLink->sendMessage(msg) > 0) ? MONPROTOCOL_ERROR_NONE : MONPROTOCOL_ERROR_SEND;
}


AbstractMonitorProtocolLayer::MONPROTOCOL_ERROR AbstractMonitorProtocolLayer::send_req_exit(XMSG_EXIT_ACTION action, XMSG_EXIT_CODE reason)
{
    AppMsgStr     msg;
    PLExitStr     payload;

    if (DataLink == NULL)
    {
        return MONPROTOCOL_ERROR_INIT;
    }
    build_header(&(msg.header), (lu_uint16_t) XMSG_CMD_REQ_EXIT, (lu_uint16_t)sizeof(PLExitStr));

    payload.action = action;
    payload.reason = reason;

    msg.payload = (void *) &payload;

    // Insert the CRC
    insertCRC(&msg);

    return (DataLink->sendMessage(msg) > 0) ? MONPROTOCOL_ERROR_NONE : MONPROTOCOL_ERROR_SEND;
}


AbstractMonitorProtocolLayer::MONPROTOCOL_ERROR AbstractMonitorProtocolLayer::send_notify_exit(APP_EXIT_CODE exitCode, lu_uint32_t timoutMs)
{
    AppMsgStr       msg;
    PLNotifyExitStr payload;

    if (DataLink == NULL)
    {
        return MONPROTOCOL_ERROR_INIT;
    }
    build_header(&(msg.header), (lu_uint16_t) XMSG_CMD_NOTIFY_EXIT, (lu_uint16_t)sizeof(PLNotifyExitStr));

    payload.exitCode  = exitCode;
    payload.timeoutMs = timoutMs;

    msg.payload = (void *) &payload;

    // Insert the CRC
    insertCRC(&msg);

    return (DataLink->sendMessage(msg) > 0) ? MONPROTOCOL_ERROR_NONE : MONPROTOCOL_ERROR_SEND;
}


AbstractMonitorProtocolLayer::MONPROTOCOL_ERROR AbstractMonitorProtocolLayer::send_http_restart()
{
    AppMsgStr     msg;

    if (DataLink == NULL)
    {
        return MONPROTOCOL_ERROR_INIT;
    }
    build_header(&(msg.header), (lu_uint16_t) XMSG_CMD_HTTP_RESTART, 0);
    msg.payload = NULL;

    // Insert the CRC
    insertCRC(&msg);

    return (DataLink->sendMessage(msg) > 0) ? MONPROTOCOL_ERROR_NONE : MONPROTOCOL_ERROR_SEND;
}


AbstractMonitorProtocolLayer::MONPROTOCOL_ERROR AbstractMonitorProtocolLayer::send_OK_LED_state(OK_LEDSTATE ledState)
{
    AppMsgStr       msg;
    PLSetOKLEDStr   payload;

    if (DataLink == NULL)
    {
        return MONPROTOCOL_ERROR_INIT;
    }
    build_header(&(msg.header), (lu_uint16_t) XMSG_CMD_SET_OK_LED, (lu_uint16_t)sizeof(PLSetOKLEDStr));

    payload.ledState  = ledState;

    msg.payload = (void *) &payload;

    // Insert the CRC
    insertCRC(&msg);

    return (DataLink->sendMessage(msg) > 0) ? MONPROTOCOL_ERROR_NONE : MONPROTOCOL_ERROR_SEND;
}

AbstractMonitorProtocolLayer::MONPROTOCOL_ERROR AbstractMonitorProtocolLayer::send_ping_reply_msg()
{
    AppMsgDef msg;

    if (DataLink == NULL)
    {
        return MONPROTOCOL_ERROR_INIT;
    }
    build_header(&msg.header, XMSG_REP_REQ_ALIVE, 0);
    msg.payload = NULL;

    // Insert the CRC
    insertCRC(&msg);

    return (DataLink->sendMessage(msg) > 0) ? MONPROTOCOL_ERROR_NONE : MONPROTOCOL_ERROR_SEND;
}

AbstractMonitorProtocolLayer::MONPROTOCOL_ERROR  AbstractMonitorProtocolLayer::send_req_offLocalRemote_msg(XMSG_VALUE_OLR_CODE stateOLR)
{
    AppMsgStr         msg;
    PLSetOLRStr payload;

    if (DataLink == NULL)
    {
        return MONPROTOCOL_ERROR_INIT;
    }
    build_header(&(msg.header), (lu_uint16_t) XMSG_CMD_SET_OLR_VAL, (lu_uint16_t)sizeof(PLSetOLRStr));

    payload.OLRState  = stateOLR;

    msg.payload = (void *) &payload;

    // Insert the CRC
    insertCRC(&msg);

    return (DataLink->sendMessage(msg) > 0) ? MONPROTOCOL_ERROR_NONE : MONPROTOCOL_ERROR_SEND;
}

AbstractMonitorProtocolLayer::MONPROTOCOL_ERROR  AbstractMonitorProtocolLayer::send_req_setWdogKick_msg(lu_bool_t start)
{
    AppMsgStr         msg;
    PLSetKickWdogStr  payload;

    if (DataLink == NULL)
    {
        return MONPROTOCOL_ERROR_INIT;
    }
    build_header(&(msg.header), (lu_uint16_t) XMSG_CMD_SET_KICK_WDOG, (lu_uint16_t)sizeof(PLSetKickWdogStr));

    payload.kick = start;

    msg.payload = (void *) &payload;

    // Insert the CRC
    insertCRC(&msg);

    return (DataLink->sendMessage(msg) > 0) ? MONPROTOCOL_ERROR_NONE : MONPROTOCOL_ERROR_SEND;
}




/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

lu_int32_t AbstractMonitorProtocolLayer::getCmdId(AppMsgStr *msg)
{
    if (msg == NULL)
    {
        return MONPROTOCOL_ERROR_FORMAT;
    }

    return (msg->header.cmd & (~XMSG_FLAGS_NACK));
}


void *AbstractMonitorProtocolLayer::getPayload(AppMsgStr *msg)
{
    if (msg == NULL)
    {
        return NULL;
    }

    return msg->payload;
}


AbstractMonitorProtocolLayer::MONPROTOCOL_ERROR  AbstractMonitorProtocolLayer::checkValid(AppMsgStr *message)
{
    lu_uint32_t      rxCRC32;    // Received CRC
    lu_uint32_t      calcCRC32;  // Calculated CRC
    lu_uint8_t      *ptr;
    lu_uint16_t      i;

    if (message == NULL)
    {
        return MONPROTOCOL_ERROR_FORMAT;
    }

    if (message->header.stx != XMSG_STX)
    {
        return MONPROTOCOL_ERROR_FORMAT;
    }

    if (message == NULL)
    {
        return MONPROTOCOL_ERROR_FORMAT;
    }

    // Clear the CRC prior to calculation
    // Initialise the received CRC to 0 before recalculating the CRC
    rxCRC32 = message->header.crc32;
    message->header.crc32 = (lu_uint32_t) 0;

    // Initialise the CRC
    crc32_init(&calcCRC32);

    // As the AppMsgStr structure is not in contiguous memory we will have
    // to calculate the CRC byte by byte across the header and payload

    // Start calculating the header portion
    ptr = (lu_uint8_t *) message;
    for (i = 0; (ptr != NULL) && (i < sizeof(AppMsgHeaderStr)); i++)
    {
        crc32_byte(*(ptr++), &calcCRC32);
    }

    // Continue calculating the payload portion
    ptr = (lu_uint8_t *) message->payload;
    for (i = 0; (ptr != NULL) && (i < message->header.payLoadLen); i++)
    {
        crc32_byte(*(ptr++), &calcCRC32);
    }

    // Set the CRC
    calcCRC32 ^= 0XFFFFFFFFL;

    // Compare the received CRC with the calculated one
    if (rxCRC32 != calcCRC32)
    {
        logMessageError(SUBSYSTEM_ID_MT_PROTOCOL, "CRC mismatch rx:[0x%08x] calc:[0x%08x]!!!\n", rxCRC32, calcCRC32);

        return MONPROTOCOL_ERROR_FORMAT;
    }

    return MONPROTOCOL_ERROR_NONE;
}


lu_int32_t AbstractMonitorProtocolLayer::decode_rep_ai_msg(AppMsgStr *msg)
{
    if (msg == NULL)
    {
        return MONPROTOCOL_ERROR_FORMAT;
    }

    if (msg->payload == NULL)
    {
        return MONPROTOCOL_ERROR_FORMAT;
    }

    PLGetAIValStr *payload = (PLGetAIValStr *) msg->payload;

    notify_rep_ai_val(payload->channel, payload->value, payload->flags);

    return MONPROTOCOL_ERROR_NONE;
}


lu_int32_t AbstractMonitorProtocolLayer::decode_rep_di_msg(AppMsgStr *msg)
{
    if (msg == NULL)
    {
        return MONPROTOCOL_ERROR_FORMAT;
    }

    if (msg->payload == NULL)
    {
        return MONPROTOCOL_ERROR_FORMAT;
    }

    PLGetDIValStr *payload = (PLGetDIValStr *) msg->payload;

    notify_rep_di_val(payload->channel, payload->value, payload->flags);

    return MONPROTOCOL_ERROR_NONE;
}


lu_int32_t AbstractMonitorProtocolLayer::decode_rep_app_ver_msg(AppMsgStr *msg)
{
    if (msg == NULL)
    {
        return MONPROTOCOL_ERROR_FORMAT;
    }

    if (msg->payload == NULL)
    {
        return MONPROTOCOL_ERROR_FORMAT;
    }

    PLAppVerStr *payload = (PLAppVerStr *) msg->payload;

    notify_rep_app_ver(payload->relType, payload->version, payload->patch);

    return MONPROTOCOL_ERROR_NONE;
}


lu_int32_t AbstractMonitorProtocolLayer::decode_req_ping_msg()
{
    notify_req_ping();

    return MONPROTOCOL_ERROR_NONE;
}


lu_int32_t AbstractMonitorProtocolLayer::decode_rep_ping_msg()
{
    notify_rep_ping();

    return MONPROTOCOL_ERROR_NONE;
}


lu_int32_t AbstractMonitorProtocolLayer::decode_rep_set_ip(AppMsgStr *msg)
{
    if (msg == NULL)
    {
        return MONPROTOCOL_ERROR_FORMAT;
    }

    if (msg->payload == NULL)
    {
        return MONPROTOCOL_ERROR_FORMAT;
    }

    PLSetIPStr *payload = (PLSetIPStr *) msg->payload;
    lu_bool_t   success = (msg->header.cmd | XMSG_FLAGS_NACK) ? LU_FALSE : LU_TRUE;

    notify_rep_set_ip(success, payload->device, payload->ipAddr, payload->mask, payload->gwAddr);

    return MONPROTOCOL_ERROR_NONE;
}


lu_int32_t AbstractMonitorProtocolLayer::decode_rep_set_ppp(AppMsgStr *msg)
{
    lu_bool_t   success = (msg->header.cmd | XMSG_FLAGS_NACK) ? LU_FALSE : LU_TRUE;

    notify_rep_set_ppp(success);

    return MONPROTOCOL_ERROR_NONE;
}


lu_int32_t AbstractMonitorProtocolLayer::decode_req_exit(AppMsgStr *msg)
{
    if (msg == NULL)
    {
        return MONPROTOCOL_ERROR_FORMAT;
    }

    if (msg->payload == NULL)
    {
        return MONPROTOCOL_ERROR_FORMAT;
    }

    PLExitStr *payload = (PLExitStr *) msg->payload;

    notify_req_exit(payload->action, payload->reason);

    return MONPROTOCOL_ERROR_NONE;
}

lu_int32_t  AbstractMonitorProtocolLayer::decode_rep_set_kick_wdog(AppMsgStr *msg)
{
    if (msg == NULL)
    {
        return MONPROTOCOL_ERROR_FORMAT;
    }

    if (msg->payload == NULL)
    {
        return MONPROTOCOL_ERROR_FORMAT;
    }

    PLSetKickWdogStr *payload = (PLSetKickWdogStr *) msg->payload;

    notify_rep_kick_wdog(payload->kick);

    return MONPROTOCOL_ERROR_NONE;
}


lu_int32_t AbstractMonitorProtocolLayer::decode(AppMsgStr *message)
{
    // Check the message is valid (STX, CRC etc
    if (checkValid(message) != 0)
    {
        return MONPROTOCOL_ERROR_FORMAT;
    }

    switch(getCmdId(message))
    {
        case XMSG_REP_GET_AI_VAL:
            decode_rep_ai_msg(message);
            break;

        case XMSG_REP_GET_DI_VAL:
            decode_rep_di_msg(message);
            break;

        case XMSG_REP_GET_APP_VER:
            decode_rep_app_ver_msg(message);
            break;

        case XMSG_CMD_REQ_ALIVE:
            decode_req_ping_msg();
            break;

        case XMSG_REP_REQ_ALIVE:
            decode_rep_ping_msg();
            break;

        case XMSG_REP_SET_IP:
            decode_rep_set_ip(message);
            break;

        case XMSG_REP_SET_PPP:
            decode_rep_set_ppp(message);
            break;

        case XMSG_CMD_REQ_EXIT:
            decode_req_exit(message);
            break;

        case XMSG_REP_SET_KICK_WDOG:
            decode_rep_set_kick_wdog(message);

// TODO: SKA - Add more decoders!

        default:
            return MONPROTOCOL_ERROR_FORMAT;
    }

    return MONPROTOCOL_ERROR_NONE;
}


lu_int32_t AbstractMonitorProtocolLayer::build_header(AppMsgHeaderStr *header, lu_uint16_t command, lu_uint16_t payloadLen)
{
    if (header == NULL)
    {
        return MONPROTOCOL_ERROR_FORMAT;
    }

    header->stx = XMSG_STX;

// TODO SKA: Version Numbers are ignored for now!
    header->apiVersion.major = 0;
    header->apiVersion.minor = 0;

    // CRC is filled in later
    header->crc32      = 0x00000000;

    header->appId      = getAppID();
    header->cmd        = command;
    header->payLoadLen = payloadLen;

    return MONPROTOCOL_ERROR_NONE;
}

/*
 *********************** End of file ******************************************
 */

