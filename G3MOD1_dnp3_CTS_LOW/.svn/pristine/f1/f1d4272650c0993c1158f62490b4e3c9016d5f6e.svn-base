/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

import com.lucy.g3.rtu.config.constants.SwitchIndex;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.module.shared.domain.ISupportChangeID;
import com.lucy.g3.rtu.config.module.shared.domain.ISwitchModule;
import com.lucy.g3.rtu.config.module.shared.domain.SwitchModuleOutput;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumSCMMK2.SCM_MK2_CH_AINPUT;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumSCMMK2.SCM_MK2_CH_DINPUT;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumSCMMK2.SCM_MK2_CH_DOUT;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumSCMMK2.SCM_MK2_CH_SWOUT;
import com.lucy.g3.xml.gen.api.IChannelEnum;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * The implementation of Switch Control Module.
 */
public final class ModuleSCM_MK2 extends AbstractCANModule implements ICANModule, ISupportChangeID, ISwitchModule {

  private final SwitchModuleOutput output;
  
  private final ModuleSCM_MK2Settings settings = new ModuleSCM_MK2Settings();
  
  public ModuleSCM_MK2() {
    this(MODULE_ID.MODULE_ID_0);
  }

  public ModuleSCM_MK2(MODULE_ID id) {
    super(id, MODULE.MODULE_SCM_MK2);

    output = new SwitchModuleOutput(this, SwitchIndex.SwitchA,
        SCM_MK2_CH_DINPUT.SCM_MK2_CH_DINPUT_SWITCH_OPEN   ,
        SCM_MK2_CH_DINPUT.SCM_MK2_CH_DINPUT_SWITCH_CLOSED ,
        SCM_MK2_CH_SWOUT.SCM_MK2_CH_SWOUT_OPEN_SWITCH     ,
        SCM_MK2_CH_SWOUT.SCM_MK2_CH_SWOUT_CLOSE_SWITCH     );
    
    initDefaultConfig();
  }

  
  public SwitchModuleOutput getOutput() {
    return output;
  }

  private void initDefaultConfig() {
    /*
     * F1787:Insulation Low + Actuator Disable inputs to be inverted by default
     */
//    ICANChannel ch;
//    ch = getChByEnum(SCM_MK2_CH_DINPUT.SCM_MK2_CH_DINPUT_INSULTATION_LOW);
//    ch.setParameter(ICANChannel.PARAM_EXTEQUITPINVERT, Boolean.TRUE);
//
//    ch = getChByEnum(SCM_MK2_CH_DINPUT.SCM_MK2_CH_DINPUT_ACTUATOR_DISABLED);
//    ch.setParameter(ICANChannel.PARAM_EXTEQUITPINVERT, Boolean.TRUE);

  }

  @Override
  protected HashMap<ChannelType, ICANChannel[]> initChannels() {
    HashMap<ChannelType, ICANChannel[]> chMap = new HashMap<ChannelType, ICANChannel[]>();

    // Initialise analogue input channels
    chMap.put(ChannelType.ANALOG_INPUT,
        createChannels(ChannelType.ANALOG_INPUT, SCM_MK2_CH_AINPUT.values()));

    // Initialise digital input channels
    chMap.put(ChannelType.DIGITAL_INPUT,
        createChannels(ChannelType.DIGITAL_INPUT, SCM_MK2_CH_DINPUT.values()));

    // Initialise Switch output channels
    chMap.put(ChannelType.SWITCH_OUT,
        createChannels(ChannelType.SWITCH_OUT, SCM_MK2_CH_SWOUT.values()));

    // Initialise digital output channels
    chMap.put(ChannelType.DIGITAL_OUTPUT,
        createChannels(ChannelType.DIGITAL_OUTPUT, SCM_MK2_CH_DOUT.values()));

    /*
     * F1787: Switch Output Inhibit bit mask bits setting for Earthed,Insulation
     * low, Actuator disabled .
     */
//    final int[] inhibitBits = new int[3];
//    inhibitBits[0] = SCM_MK2_CH_DINPUT.SCM_MK2_CH_DINPUT_SWITCH_EARTHED.getID();
//    inhibitBits[1] = SCM_MK2_CH_DINPUT.SCM_MK2_CH_DINPUT_ACTUATOR_DISABLED.getID();
//    inhibitBits[2] = SCM_MK2_CH_DINPUT.SCM_MK2_CH_DINPUT_INSULTATION_LOW.getID();

    // Initialise switch Output channels' inhibit mask
    final ICANChannel[] inputChs = chMap.get(ChannelType.DIGITAL_INPUT);
    ICANChannel[] swoutChs = chMap.get(ChannelType.SWITCH_OUT);
    for (int j = 0; j < swoutChs.length; j++) {
      Inhibit inhibit = new Inhibit(inputChs);

//      for (int i = 0; i < inhibitBits.length; i++) {
//        inhibit.setBitValue(inhibitBits[i], true);
//      }
      swoutChs[j].setParameter(ICANChannel.PARAM_INHIBI, inhibit);
    }

    return chMap;
  }


  @Override
  public void setId(MODULE_ID moduleID) {
    super.modifyId(moduleID);
  }

  @Override
  public SwitchModuleOutput getSwitchOutput(SwitchIndex index) {
    return index == SwitchIndex.SwitchA ? output : null;
  }

  @Override
  public Collection<SwitchModuleOutput> getAllSwitchOutputs() {
    return Arrays.asList(new SwitchModuleOutput[] { output });
  }

  @Override
  public boolean isSingleSwitch() {
    return true;
  }
  
  @Override
  protected CANChannel createChannel(ChannelType type, Enum<? extends IChannelEnum> e, boolean readonly) {
    CANChannel chnl = super.createChannel(type, e, readonly);
    
    if(type == ChannelType.SWITCH_OUT) {
      chnl.removeParameter(ICANChannel.PARAM_PULSELENGTH);
      chnl.addParameter(ICANChannel.PARAM_MOTOR_OVER_DRIVE_MS, 0L);
      chnl.addParameter(ICANChannel.PARAM_MOTOR_REVERSE_DRIVE_MS, 0L);
    }
    return chnl;
  }

  @Override
  public Object getOpenChannel(SwitchIndex index) {
    return getChByEnum(SCM_MK2_CH_SWOUT.SCM_MK2_CH_SWOUT_OPEN_SWITCH);
  }

  @Override
  public Object getCloseChannel(SwitchIndex index) {
    return getChByEnum(SCM_MK2_CH_SWOUT.SCM_MK2_CH_SWOUT_CLOSE_SWITCH);
  }

  @Override
  public ModuleSCM_MK2Settings getSettings() {
    return settings;
  }

}