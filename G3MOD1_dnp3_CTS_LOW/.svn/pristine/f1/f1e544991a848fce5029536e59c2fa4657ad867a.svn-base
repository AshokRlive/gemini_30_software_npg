/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.swing.textfield;

import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.ParseException;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.text.DefaultFormatter;
import javax.swing.text.DefaultFormatterFactory;

import org.apache.log4j.Logger;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.validation.view.ValidationComponentUtils;
import com.lucy.g3.gui.common.widgets.ext.swing.formatter.BoundaryNumberFormatter;

/**
 * This class is for improving standard JFormattedTextField when it is used in
 * Bindings. It gives JFormattedTextField these new features:
 * <p>
 * 1.Commit user typed text immediately.Originally it requires changing focus or
 * pressing Enter key to commit.
 * </p>
 * <p>
 * 2.Indicate error with highlighted background if user input cannot be
 * committed as it is invalid.
 * </p>
 * <p>
 * 3.Lock focus if user input is invalid, which means user cannot move to next
 * component there is an error with user input.
 * </p>
 */
public class FormattedTextFieldSupport {

  private Constraints constraints;


  public FormattedTextFieldSupport(Constraints constraints) {
    this.constraints = Preconditions.checkNotNull(constraints, "constraints is null");
  }

  /**
   * Sets the boundary of the number text field. If the user input is out of
   * boundary, value will not be committed, at the time, error background will
   * be shown to user.
   *
   * @param property
   *          the property name in Constraint property file.
   */
  public void setBoundary(JFormattedTextField ftf, String prefix, String property) {
    ftf.setFormatterFactory(new DefaultFormatterFactory(
        new BoundaryNumberFormatter(
            constraints.getMin(prefix, property),
            constraints.getMax(prefix, property))));

    FormattedTextFieldSupport.installCommitOnType(ftf);
  }

  public static void setBoundary(JFormattedTextField ftf, long min, long max) {
    ftf.setFormatterFactory(new DefaultFormatterFactory(new BoundaryNumberFormatter(min, max)));
  }

  public static void installCommitOnType(JFormattedTextField ftf) {
    installCommitOnType(ftf, true);
  }

  /**
   * Change a JFormattedTextField's behaviour.
   *
   * @param focusLocked
   *          the focus will be locked down for invalid edit texts using the
   *          FormattedTextFieldVerifier.
   */
  public static void installCommitOnType(JFormattedTextField ftf, boolean focusLocked) {
    if (ftf == null) {
      return;
    }

    if (focusLocked) {
      ftf.setInputVerifier(new FormattedTextFieldVerifier());
    }

    /* Change background on valid */
    PropertyChangeListener[] old = ftf.getPropertyChangeListeners("editValid");
    if (old == null || old.length == 0) {
      ftf.addPropertyChangeListener("editValid", new EditValidPCL(ftf));
    }

    /* Customise formatter to commit change on valid edit */
    AbstractFormatter formatter = ftf.getFormatter();
    if (formatter == null) {
      formatter = new DefaultFormatter();
      ftf.setFormatterFactory(new DefaultFormatterFactory(formatter));
      formatter = ftf.getFormatter();
    }

    if (formatter != null && formatter instanceof DefaultFormatter) {
      ((DefaultFormatter) formatter).setCommitsOnValidEdit(true);
      ((DefaultFormatter) formatter).setAllowsInvalid(true);
      ((DefaultFormatter) formatter).setOverwriteMode(false);
    } else {
      Logger.getLogger(FormattedTextFieldSupport.class)
          .error("Fail to set commitonValidEdit. Unsupported formatter: " + formatter);
    }

  }


  private static class EditValidPCL implements PropertyChangeListener {

    private final Color orginBG;


    public EditValidPCL(JFormattedTextField ftf) {
      this.orginBG = ftf.getBackground();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      if ("editValid".equals(evt.getPropertyName())) {
        JFormattedTextField jtf = (JFormattedTextField) evt.getSource();
        boolean valid = (Boolean) (evt.getNewValue());

        if (valid) {
          jtf.setBackground(orginBG);
        } else {
          ValidationComponentUtils.setErrorBackground(jtf);
        }
      }
    }
  }

  /**
   * Verifies the input of a <code>JFormattedTextField</code> and checks whether
   * the edited text is valid or not. Useful to lock focus down while the edited
   * text is invalid.
   */
  public static final class FormattedTextFieldVerifier extends InputVerifier {

    @Override
    public boolean verify(JComponent input) {
      JFormattedTextField ftf = (JFormattedTextField) input;
      JFormattedTextField.AbstractFormatter formatter = ftf
          .getFormatter();
      if (formatter != null) {
        String text = ftf.getText();
        try {
          formatter.stringToValue(text);
          return true;
        } catch (ParseException pe) {
          return false;
        }
      }
      return true;
    }
  }
}
