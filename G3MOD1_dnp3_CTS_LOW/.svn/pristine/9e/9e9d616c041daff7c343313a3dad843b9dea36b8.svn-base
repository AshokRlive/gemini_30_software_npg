/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:Main.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   8 May 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdio.h>
#include <stdlib.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MBMController.h"
#include "MBMDebug.h"
#include "zhelpers.hpp"
#include "MBDeviceManager.h"
#include "DummyMBConfigRepo.h"
#include "MBDeviceFactory.h"
#include "MBDeviceManager.h"
#include <string.h>
#include "mbm_helpers.h"
#include <embUnit/embUnit.h>
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define MB_DUMP_EVENT 0

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

using namespace mbm;

static MBDeviceManager* fixture;

void static dumpMB(){
     // Create context with 1 IO thread
     zmq::context_t* context = new zmq::context_t(1);

    // Create SUB socket to subscribe MB channel events
     zmq::socket_t* subscriber = new zmq::socket_t(*context, ZMQ_SUB);
     subscriber->connect (MB_HOST_ADDRESS_SUB);

     mbm::MsgHeader header;
     header.type = mbm::MSG_TYPE_EVENT;

     header.id = mbm::MSG_ID_EVENT_ANALOGUE;
     subscriber->setsockopt(ZMQ_SUBSCRIBE, &header, sizeof(header));//Subscribe to all events.

     //subscriber->setsockopt(ZMQ_SUBSCRIBE, "", 0);//Subscribe to all events.
     s_dump(*subscriber);

}

static void setUp(void)
{
    DummyMBConfigRepo* repo  = new DummyMBConfigRepo() ;
    MBDeviceFactory* factory = new MBDeviceFactory(*repo);
    fixture = new MBDeviceManager();
    fixture->init(*factory);
    delete factory;
    delete repo;
}

static void tearDown(void)
{
    delete fixture;
}


static void testMBDeviceManagerInit(void)
{
    PRINT_SEPERATOR;

    // Check module created
    lu_uint16_t num;
    num = fixture->getTotalDeviceNum();
    TEST_ASSERT_EQUAL_INT(DummyMBConfigRepo::DEVICE_NUM, num);

    // Check module not null
    mbm::MBDevice* module = NULL;
    for (int i = 0; i < num; ++i) {
        module = fixture->getMBDevice((MODULE_ID)i);
        if(module == NULL)
        {
            std::stringstream sstm;
            sstm << "Module is null at: " << i;
            TEST_FAIL(sstm.str().c_str());
        }
    }

    // Check channel created
    module = fixture->getMBDevice((MODULE_ID)0);
    TEST_ASSERT_EQUAL_INT(DummyMBConfigRepo::CHS_NUM, module->getChannelNum(CHANNEL_TYPE_AINPUT));
    TEST_ASSERT_EQUAL_INT(DummyMBConfigRepo::CHS_NUM, module->getChannelNum(CHANNEL_TYPE_DINPUT));
}

static void testGetModule(void)
{
    PRINT_SEPERATOR;

    IIOModule* module;
    for (int i = 0; i < DummyMBConfigRepo::DEVICE_NUM; ++i) {
        module = fixture->getModule(MODULE_MBDEVICE,static_cast<MODULE_ID>(i));
        TEST_ASSERT_NOT_NULL(module);
    }
}

static void testGetMBDevice(void)
{
    PRINT_SEPERATOR;

    mbm::MBDevice* device;
    mbm::DeviceConf* conf;
    for (int i = 0; i < DummyMBConfigRepo::DEVICE_NUM; ++i) {
        device = fixture->getMBDevice(i);
        TEST_ASSERT_NOT_NULL(device);
        device->printID();
        device->printConfig();

        conf = device->getConfig();
        DBG_INFO("Device:%i  conf pointer:%p",device->getIndex(), conf);
        TEST_ASSERT_EQUAL_INT(i, device->getIndex());
        TEST_ASSERT_EQUAL_INT(i, conf->ref.deviceId);
    }
}

static void testGetAllModules(void)
{
    PRINT_SEPERATOR;

    std::vector<IIOModule*> moduleList;
    moduleList = fixture->getAllModules();
    assert(moduleList.size() != 0);

    assert(IOM_ERROR_NONE == fixture->getAllModules(&deviceTable, &deviceNum));

    DBG_INFO("moduleNum:%i",moduleList.size());
    TEST_ASSERT_EQUAL_INT(fixture->getTotalDeviceNum(), moduleList.size());

    for (int i = 0; i < moduleList.size(); ++i) {
        TEST_ASSERT_NOT_NULL(moduleList[i]);
    }
}


static void testGetChannel(void)
{
    PRINT_SEPERATOR;
    IChannel* ch = NULL;

    for (int i = 0; i < DummyMBConfigRepo::DEVICE_NUM; ++i) {
        for (int j = 0; j < DummyMBConfigRepo::CHS_NUM; ++j) {
            ch = fixture->getChannel(MODULE_MBDEVICE,
                            static_cast<MODULE_ID>(i),CHANNEL_TYPE_AINPUT,j);
            if(ch == NULL)
            {
                DBG_ERR("Device:%i channel:%i is null",i , j);
            }
            TEST_ASSERT_NOT_NULL(ch);
        }
    }


}

static void testHandleAEvent(void)
{
    PRINT_SEPERATOR;

    // Handle event
    mbm::ChannelEvent aevent;
    aevent.channel.channelId = 0;
    aevent.channel.channelType = 1;
    aevent.channel.device.deviceId = 0;
    aevent.channel.device.deviceType = 0;
    fixture->handleEvent(aevent);
}


static void testControllerWithConfig(void)
{
    PRINT_SEPERATOR;

    fixture->start();
    sleep(3);
    fixture->stopAllModules();
}



static void testControllerWithoutConfig(void)
{
    PRINT_SEPERATOR;
    mbm::MBDeviceManager mgr;
    mgr.start();
    sleep(3);
    mgr.stopAllModules();
}



TestRef ModbusDeviceManagerTest_tests(void)
{
    EMB_UNIT_TESTFIXTURES(fixtures) {
        //new_TestFixture("testMBDeviceManagerInit",testMBDeviceManagerInit),
        new_TestFixture("testGetMBDevice",testGetMBDevice),
        new_TestFixture("testGetModule",testGetModule),
        new_TestFixture("testGetAllModules",testGetAllModules),
        new_TestFixture("testGetChannel",testGetChannel),
        new_TestFixture("testHandleAEvent",testHandleAEvent),
        new_TestFixture("testControllerWithoutConfig",testControllerWithoutConfig),
        new_TestFixture("testControllerWithConfig",testControllerWithConfig),
    };
    EMB_UNIT_TESTCALLER(ModbusDeviceManagerTest_tests,"ModbusDeviceManagerTest_tests",setUp,tearDown,fixtures);

    return (TestRef)&ModbusDeviceManagerTest_tests;;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */



/*
 *********************** End of file ******************************************
 */
