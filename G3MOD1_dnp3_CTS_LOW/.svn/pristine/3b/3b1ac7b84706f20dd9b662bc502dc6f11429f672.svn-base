/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       G3 Module Manager interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   08/08/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#if !defined(EA_E4745210_49A0_4607_B68A_3877D8323F53__INCLUDED_)
#define EA_E4745210_49A0_4607_B68A_3877D8323F53__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "IIOModuleManager.h"
#include "CANIOModuleManager.h"
#include "MCMIOModuleManager.h"
#include "ConfigurationManager.h"
#include "MonitorProtocolManager.h"
#include "IMemoryManager.h"
#include "ISupportPowerSaving.h"
#include "ISupportBootloaderMode.h"
#include "ModuleStatusMonitor.h"

#if MODBUS_MASTER_SUPPORT
#include "MBDeviceManager.h"
#endif

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Container for all the MCM module managers
 *
 * Using Design Pattern terminology this is a facade.
 * All the requests are forwarded to all the necessary
 * module managers underneath
 */
class ModuleManager : public IIOModuleManager,
                        public ISupportPowerSaving,
                        public ISupportBootloaderMode
{

public:
    /**
     * \brief Custom constructor
     *
     * \param schedType Thread scheduler type
     * \param priority Thread priority
     * \param factory Pointer to the CAN Module factory class
     * \param cManager Configuration manager reference
     * \param memManager Reference to the Memory Manager
     * \param monProtocol Reference to the Monitor Protocol Manager
     */
    ModuleManager( SCHED_TYPE schedType,
                   lu_uint32_t priority,
                   CANIOModuleFactory *factory,
                   ConfigurationManager &cManager,
                   IMemoryManager& memManager,
                   MonitorProtocolManager& monProtocol
                 );
    virtual ~ModuleManager();

    /**
     * \brief Start module discovery procedure
     */
    virtual IOM_ERROR discover();

    /* Override*/
    virtual void setBootloaderMode(lu_bool_t mode);

    /* Override*/
    virtual void setPowerSaveMode(lu_bool_t mode);

    /**
     * \brief Grant alarm publishing
     *
     * Removes inhibition of module alarms.
     */
    virtual void grantAlarms();


#if DEBUG_HBEAT_ENABLED
    void setHBeatEnable(lu_uint8_t x);
#endif


    /**
     * \brief Create a module
     *
     * Creates a module even if it is not connected
     *
     * \param module Module type
     * \param moduleID Module ID number
     * \param iFace Interface where it is supposed to be connected
     *
     * \return pointer to the module. NULL if it wasn't successful.
     */
    virtual IIOModule *createModule(MODULE module,
                                    MODULE_ID moduleID,
                                    COMM_INTERFACE iFace = COMM_INTERFACE_UNKNOWN);


    /**
     * \brief Get a module reference  \param module Module type
     *
     * \param moduleId Module Identifier (address)
     *
     * \return Pointer to the module. NULL if the module doesn't exist
     */
    virtual IIOModule *getModule(MODULE module, MODULE_ID moduleID);

    MCMIOModule* getMCM();

    virtual std::vector<IIOModule*> getAllModules();

    /**
     * \brief Get a channel reference
     *
     * \param module Module type
     * \param moduleId Module Identifier (address)
     * \param chType Channel type
     * \param chNumber Channel Number
     *
     * \return Pointer to the channel. NULL if the channel/module doesn't exist
     */
    virtual IChannel *getChannel( MODULE module,
                                  MODULE_ID moduleID,
                                  CHANNEL_TYPE chType,
                                  lu_uint32_t chNumber
                                );

    /**
     * \brief Restart all CAN modules
     *
     * Restart all the present slave modules by using a broadcast message.
     *
     * \return Error code
     */
    virtual IOM_ERROR restartAllModules();

    /**
     * \brief Stop all modules updating
     *
     * Used when the application is closing to prevent updates
     *
     * \return Error code
     */
    virtual IOM_ERROR stopAllModules();

#if MODBUS_MASTER_SUPPORT
    mbm::MBDeviceManager& getMBDeviceManager()
    {
        return mbdeviceManager;
    }
#endif

private:
#if MODBUS_MASTER_SUPPORT
    mbm::MBDeviceManager  mbdeviceManager;
#endif
    CANIOModuleManager canManager;
    MCMIOModuleManager mcmManager;
    ModuleStatusMonitor moduleStatus;

    lu_bool_t powerSaving;  //Power Saving mode status
    lu_bool_t statusHMI;    //HMI connected/disconnected status

};
#endif // !defined(EA_E4745210_49A0_4607_B68A_3877D8323F53__INCLUDED_)


/*
 *********************** End of file ******************************************
 */
