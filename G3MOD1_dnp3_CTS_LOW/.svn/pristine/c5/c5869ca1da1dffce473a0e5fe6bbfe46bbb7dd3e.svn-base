/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Factory Test I2C Humidity Sensor driver module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   05/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lpc17xx_i2c.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"

#include "BoardIOAPII2C.h"
#include "FactoryTestI2CHumidity.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define AM2315_I2CADDR		0x5c
#define AM2315_READREG		0x03

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR readTempHumidity(TestI2cHumidityReadStr *paramsPtr, lu_int32_t *tempPtr, lu_int32_t *humidPtr);


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR FactoryTestI2CHumidityRead(TestI2cHumidityReadStr *i2cHumidityParamsPtr)
{
	SB_ERROR                    retError;
	TestI2cTHumidityReadRspStr  resp;
	static lu_uint8_t initOnceFlag = LU_FALSE;

	if(initOnceFlag == LU_FALSE)
	{
		/* Init the I2C peripheral & configure IO pins */
		retError = initialiseBoardIOAPII2C(i2cHumidityParamsPtr->i2cChan, 100000);
		initOnceFlag = LU_TRUE;
	}

	/* Do the I2C transfer on the selected bus */
	if (retError == SB_ERROR_NONE)
	{
		retError = readTempHumidity(i2cHumidityParamsPtr, &resp.temperature, &resp.humidity);
	}

	resp.status = retError;

	/* deInit the I2C peripheral */
	//deInitialiseBoardIOAPII2C(i2cHumidityParamsPtr->i2cChan);

	/* Send CAN reply message */
	retError = CANCSendMCM( MODULE_MSG_TYPE_BLTST_1,
							MODULE_MSG_ID_BLTST_1_I2C_HUMIDITY_READ_R,
							sizeof(TestI2cTHumidityReadRspStr),
							(lu_uint8_t *)&resp
						   );

	return SB_ERROR_NONE;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR readTempHumidity(TestI2cHumidityReadStr *paramsPtr, lu_int32_t *tempPtr, lu_int32_t *humidPtr)
{
	SB_ERROR                retError;
	LPC_I2C_TypeDef			*i2cChanType;
	I2C_M_SETUP_Type		txferCfg;
	lu_int32_t 				temperature;
	lu_int32_t 				humiditity;
	lu_uint8_t 				txData[8];
	lu_uint8_t 				rxData[8];

	retError = SB_ERROR_NONE;

	i2cChanType = getI2CChannelBoardIOAPII2C(paramsPtr->i2cChan, &retError);
	if (retError == SB_ERROR_NONE)
	{
		/* Configuration */
		txferCfg.sl_addr7bit     	    = AM2315_I2CADDR;
		txferCfg.tx_data		  		= txData;
		txferCfg.tx_length       		= 3;
		txferCfg.rx_data		 	 	= rxData;
		txferCfg.rx_length       		= 8;
		txferCfg.retransmissions_max 	= 2;

		txData[0] = AM2315_READREG;
		txData[1] = 0x0;
		txData[2] = 4;

		if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
		{
			retError = SB_ERROR_I2C_FAIL;
		}
		else
		{
#if 0
			 /* Must wait for transfer to complete!! */
			 for (count = 0; count < 10000; count++)
			 {
				 if (I2C_MasterTransferComplete(i2cChanType) == TRUE)
					 break;
			 }
#endif
			if (rxData[0] != AM2315_READREG || rxData[1] != 4)
			{
				retError = SB_ERROR_I2C_FAIL;
			}
			else
			{
				humiditity  = rxData[2];
				humiditity *= 256;
				humiditity += rxData[3];
				humiditity /= 10;

				temperature  = rxData[4] & 0x7f;
				temperature *= 256;
				temperature += rxData[5];
				temperature /= 10;
				if (rxData[4] >> 7)
				{
					temperature = - temperature;
				}

				*humidPtr = humiditity;
				*tempPtr  = temperature;
			}
		}
	}
	return retError;
}

/*
 *********************** End of file ******************************************
 */
