/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.manager;

import com.lucy.g3.itemlist.IItemListManager;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocol;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.validation.domain.IContainerValidation;
import com.lucy.g3.rtu.config.shared.validation.domain.IContainerValidator;

/**
 * The Interface IProtocolManager.
 *
 * @param <ProtocolT>
 *          the type of protocol stack
 */
public interface IProtocolManager<ProtocolT extends IProtocol<?>> extends IItemListManager<ProtocolT>, IContainerValidation {

  /**
   * Checks if a protocol exists.
   *
   * @param protocolName
   *          the protocol name
   * @return true, if successful
   */
  boolean hasProtocol(ProtocolType type);

  void registerObserver(IProtocolObserver observer);

  void deregisterObserver(IProtocolObserver observer);

  void notifySessionAdded(IProtocolSession session);

  void notifySessionRemoved(IProtocolSession session);

  @Override
  IContainerValidator getValidator();
  
  IConfig getOwner();

  String getName();
  
  ProtocolT getProtocolByType(ProtocolType type);
}
