/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.shared.domain;

import static com.jgoodies.common.base.Preconditions.checkNotNull;

import java.util.Collection;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.config.module.res.ModuleResource;
import com.lucy.g3.rtu.config.shared.model.impl.AbstractNode;
import com.lucy.g3.rtu.config.shared.model.impl.ConnectEvent;
import com.lucy.g3.rtu.config.shared.model.impl.NodeType;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * This abstract class provides a basic implementation of {@link Module}.
 * <p>
 * {@linkplain #initChannels()} needs to be implemented to initialise module
 * channels.
 * </p>
 */
public abstract class AbstractModule extends AbstractNode implements Module {

  private Logger log = Logger.getLogger(AbstractModule.class);

  private MODULE_ID id;
  private final MODULE type;

  private String fullName;
  private String shortName;

  private final ModuleIcon icons;

  protected IValidator validator;


  protected AbstractModule(MODULE_ID id, MODULE type) {
    super(NodeType.MODULE);
    this.type = checkNotNull(type, "type must not be null");
    this.id = checkNotNull(id, "id must not be null");
    this.icons = ModuleIcon.getInstance(type);

    // Initialise the name of this module
    updateNames();
  }

  @Override
  public final MODULE getType() {
    return type;
  }

  @Override
  public final MODULE_ID getId() {
    return id;
  }

  protected final void modifyId(MODULE_ID moduleID) {
    if (moduleID == null) {
      log.error("Module id cannot be set to null");
      return;
    }

    if (id == moduleID) {
      return;
    }

    // if(ModuleFactory.instance.getMaxModuleNumber(getType()) <= 1){
    // log.warn("Not allowed to change module ID:"+this);
    // return;
    // }

    Object oldValue = getId();
    id = moduleID;
    firePropertyChange(PROPERTY_MODULE_ID, oldValue, moduleID);

    updateNames();
  }

  protected final void updateNames() {
    String shortName = createShortName();
    String fullName = createFullName();
    setShortName(shortName);
    setFullName(fullName);
    setDescription(fullName);

    setName(shortName);
  }

  @Override
  public final String getShortName() {
    return shortName;
  }

  private void setShortName(String shortName) {
    Object oldValue = getShortName();
    this.shortName = shortName;
    firePropertyChange(PROPERTY_SHORTNAME, oldValue, shortName);
  }

  /**
   * Create a short name from this module.
   */
  protected String createShortName() {
    return ModuleResource.INSTANCE.getModuleShortName(getType(), getId());
  }

  /**
   * Create a full name from this module.
   */
  protected String createFullName() {
    return ModuleResource.INSTANCE.getModuleFullName(getType(), getId());
  }

  @Override
  public final String getFullName() {
    return fullName;
  }

  private void setFullName(String fullName) {
    Object oldValue = getFullName();
    this.fullName = fullName;
    firePropertyChange(PROPERTY_FULLNAME, oldValue, fullName);
  }

  @Override
  public void delete() {
    if (this instanceof ISwitchModule) {
      Collection<SwitchModuleOutput> outputs = ((ISwitchModule) this).getAllSwitchOutputs();
      for (SwitchModuleOutput output : outputs) {
        output.delete();
      }
    }

    super.delete();
  }


  @Override
  public final ModuleIcon getIcons() {
    return icons;
  }

  @Override
  public final IValidator getValidator() {
    return validator;
  }

  @Override
  protected void handleConnectEvent(ConnectEvent event) {
    // No handler
  }

  @Override
  public final String toString() {
    return getShortName();
  }

  @Override
  public final boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (obj == this) {
      return true;
    }
    if (obj.getClass() != getClass()) {
      return false;
    }

    AbstractModule t = (AbstractModule) obj;
    if (t.getType() == getType() && t.getId() == getId()) {
      return true;
    }
    return false;
  }

}