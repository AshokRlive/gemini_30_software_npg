/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.controller.task;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.Task;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.comms.service.realtime.status.IRTUStatusAPI;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.OUTSERVICEREASON;

/**
 * Task for changing the state of RTU service.
 */
public class RTUSetServiceTask extends Task<Void, Void> {

  private final IRTUStatusAPI serviceModeCmd;

  private JFrame parent = null;

  private boolean enabled;

  private OUTSERVICEREASON reason;


  public RTUSetServiceTask(Application app, IRTUStatusAPI serviceModeCmd, boolean enabled,
      OUTSERVICEREASON reason) {

    super(app);
    this.serviceModeCmd = Preconditions.checkNotNull(serviceModeCmd, "serviceModeCmd is null");
    this.enabled = enabled;
    this.reason = reason;

    if (app != null && app instanceof SingleFrameApplication) {
      parent = ((SingleFrameApplication) app).getMainFrame();
    }

    if (enabled) {
      setTitle("Enable RTU Service");
    } else {
      setTitle("Disable RTU Service");
      setDescription("Disable RTU Service cause: " + reason);
    }
  }

  @Override
  protected void succeeded(Void result) {
    if (enabled) {
      setMessage("RTU is now in service");
    } else {
      setMessage("RTU is now out of service");
    }
  }

  @Override
  protected void failed(final Throwable cause) {
    JOptionPane.showMessageDialog(parent, cause.getMessage(),
        "Fail", JOptionPane.ERROR_MESSAGE);
  }

  @Override
  protected Void doInBackground() throws Exception {
    serviceModeCmd.cmdSetServiceEnabled(enabled, reason);
    serviceModeCmd.cmdGetServiceStatus();
    return null;
  }

}
