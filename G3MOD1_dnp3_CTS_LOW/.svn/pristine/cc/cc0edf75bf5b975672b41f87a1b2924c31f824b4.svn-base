/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ModuleStatusMonitor.cpp 22 Jul 2015 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/MCMIOModule/src/ModuleStatusMonitor.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       RTU Module status monitoring class.
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 22 Jul 2015 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   22 Jul 2015   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "ModuleStatusMonitor.h"
#include "lu_types.h"
#include "IIOModule.h"
#include "SysAlarm/SysAlarmSystemEnum.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
void ModuleStatusMonitor::update()
{
    IOModuleInfoStr moduleInfo;
    IIOModule* module;
    IIOModule* mcm = moduleManager.getModule(MODULE_MCM, MODULE_ID_0);
    if(mcm == NULL)
    {
        return;
    }
    mcm->getInfo(moduleInfo);
    if(moduleInfo.status.active == LU_FALSE)
    {
        return; //When MCM module is inactive, nothing to do with ANY module
    }
    //Check slave modules for not working as expected
    for (lu_uint32_t moduleType = 0; (moduleType < MODULE_LAST); ++moduleType)
    {
        if( (moduleType == MODULE_MCM) || (moduleType == MODULE_HMI) )
        {
            continue;   //skip modules not involved
        }
        for (lu_uint32_t moduleID = 0; (moduleID < MODULE_ID_LAST); ++moduleID)
        {
            module = moduleManager.getModule((MODULE)moduleType, (MODULE_ID)moduleID);
            if(module != NULL)
            {
                /* Check module status for alarm raising */
                module->getInfo(moduleInfo);
                if((moduleInfo.status.active != LU_TRUE) &&
                   (moduleInfo.status.configured == LU_TRUE)
                  )
                {

                    //Raise alarm only when module not OK but is expected by configuration
                    mcm->activateAlarm(SYS_ALARM_SUBSYSTEM_SYSTEM, SYSALC_SYSTEM_SLAVE_MODULE);
                    return; //No need to keep searching
                }
            }
        }
    }

    //de-raise alarm
//    mcm->deactivateAlarm(SYS_ALARM_SUBSYSTEM_SYSTEM, SYSALC_SYSTEM_SLAVE_MODULE);

    /* TODO: pueyos_a - remove alarm when in startup time... */
    ModuleAlarm::AlarmIDStr alID(SYS_ALARM_SUBSYSTEM_SYSTEM, SYSALC_SYSTEM_SLAVE_MODULE);
    mcm->clearAlarm(alID);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
