/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCMMonitor
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       External Coms Manager
 *
 *    CURRENT REVISION
 *
 *               $Revision::               $: (Revision of last commit)
 *               $Author::                 $: (Author of last commit)
 *       \date   $Date::                   $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24/07/13      andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifdef __cplusplus
extern "C" {
#endif
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define NSEC_PER_SEC      1000000000
#define NSEC_PER_MSEC     1000000

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
typedef void(*timer_handler)(lu_int32_t, siginfo_t *, void *);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Initialise a signal timer
 *
 *   NOTE:
 *   siginfo_t->sigev_value.sival_int will be set to the pid of  calling process.
 *   This can be checked in the timer_handler function to check the source of
 *   the signal.
 *
 *   #Only async-safe functions should be used in the handler function.
 *
 *   \param signal      -   Signal Number to use for Timer
 *          handler#    -   Pointer to handler function
 *          periodic    -   Boolean: one-shot or periodic
 *          durationMs  -   Duration of the Timer
 *          timerIdPtr  -   Pointer to a timerID
 *
 *
 *   \return 0 on no error, else error code
 *
 ******************************************************************************
 */
lu_int32_t initTimer(lu_int32_t signal, timer_handler handler, lu_bool_t periodic, lu_uint32_t durationMs, timer_t *timerIdPtr);

/*!
 ******************************************************************************
 *   \brief Update a timer's duration
 *
 *   Can be used from the handler function to restart the timer.  To stop
 *   a Timer set the duration to 0.
 *
 *   \param timerId     -   timer ID as returned from initTimer
 *          periodic    -   Boolean: one-shot or periodic
 *          durationMs  -   Duration of the timer
 *
 *
 *   \return N/A
 *
 ******************************************************************************
 */
lu_int32_t updateTimer(timer_t timerid, lu_bool_t periodic, lu_uint32_t durationMs);


#ifdef __cplusplus
}
#endif


/*
 *********************** End of file ******************************************
 */
