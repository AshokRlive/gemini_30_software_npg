/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:AutomationExample.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   26 Apr 2016     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdarg.h>
#include <stdio.h>
#include <memory.h>
#include <stdlib.h>
#include <pthread.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "AutomationExample.h"
extern "C" {
    #include "dbgserver/Debugger.h"
    #include "dbgserver/IDebuggerProtocol.h"
}

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
static AS_ERROR s_cb_get_constant   (void* context, lu_uint32_t index, lu_int32_t* pValue);
static AS_ERROR s_cb_get_input      (void* context, lu_uint32_t index, lu_uint32_t* pValue);
static AS_ERROR s_cb_set_point      (void* context, lu_uint32_t index, lu_uint32_t value);
static AS_ERROR s_cb_operate        (void* context, lu_uint32_t index, lu_uint16_t operationCode);


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
lu_int32_t debugger_get_variable(void* invoker, const DebugVarRef ref)
{
    return 0;
}


AutomationExample::AutomationExample():ctxt(NULL)
{

}

AutomationExample::~AutomationExample()
{

}

void AutomationExample::run(lu_char_t* sharedLibFilePath)
{
    /* Initialise engine*/
    AutomationContext* ctxt;
    ctxt = (AutomationContext*)calloc(1, sizeof(AutomationContext));
    lu_bool_t initialised;
	initialised = automationengine_init(ctxt, sharedLibFilePath);
	if(initialised == LU_TRUE)
	{
        /* Initialise debugger*/
        ctxt -> dbgctxt = debugger_init(DEFAULT_DEBUG_PORT, 0 ,"", NULL);
        if(ctxt -> dbgctxt == NULL)
        {
            logger_err("Failed to initialise debugger.");
            return;
        }
        else
        {
            /* Launch debugger thread*/
            launchDebuggerThread();
        }

		(ctxt->registerCallback)(AS_CALLBACK_ID_GET_CONSTANT    , (void*)s_cb_get_constant);
		(ctxt->registerCallback)(AS_CALLBACK_ID_GET_INPUT       , (void*)s_cb_get_input);
		(ctxt->registerCallback)(AS_CALLBACK_ID_SET_POINT       , (void*)s_cb_set_point);
		(ctxt->registerCallback)(AS_CALLBACK_ID_OPERATE         , (void*)s_cb_operate);

        automationengine_run(ctxt);
	}
	else
	{
        logger_info("failed to initialise automation engine.");

	}

}

void logger_err(const lu_char_t* msg,  ...)
{
	char buffer[256];
	memset(buffer,0, 256);

    va_list args;
    va_start (args, msg);

    vsprintf(buffer, msg, args);
    printf("[ERROR]AUTOMATION >> %s\n", buffer);

    va_end (args);
}

void logger_info(const lu_char_t* msg, ...)
{
	char buffer[256];
	memset(buffer,'\0',256);

    va_list args;
    va_start (args, msg);

    vsprintf(buffer, msg, args);
    printf("[INFO ]AUTOMATION >> %s\n", buffer);

    va_end (args);
}
/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/* Implementation of all callbacks.*/

static AS_ERROR s_cb_get_constant   (void* context, lu_uint32_t index, lu_int32_t* pValue)
{
    logger_info("s_cb_get_constant() called. index:%d",index);
	return AS_ERROR_NONE;

}

static AS_ERROR s_cb_get_input      (void* context, lu_uint32_t index, lu_uint32_t* pValue)
{
	LU_UNUSED(context);
	LU_UNUSED(index);
	LU_UNUSED(pValue);
	logger_info("s_cb_get_input() called");
	return AS_ERROR_NONE;
}

static AS_ERROR s_cb_set_point      (void* context, lu_uint32_t index, lu_uint32_t value)
{
	LU_UNUSED(context);
	LU_UNUSED(index);
	LU_UNUSED(value);
	logger_info("s_cb_set_point() called");
	return AS_ERROR_NONE;
}

static AS_ERROR s_cb_operate        (void* context, lu_uint32_t index, lu_uint16_t operationCode)
{
	LU_UNUSED(context);
	LU_UNUSED(index);
	LU_UNUSED(operationCode);
	logger_info("s_cb_operate() called");
	return AS_ERROR_NONE;
}

void* AutomationExample::debugThreadRoutine(void* ptr)
{
    logger_info("debug_routine()");
    AutomationExample* examplePtr = (AutomationExample* )ptr;
    lu_int32_t ret = debugger_run(examplePtr->ctxt->dbgctxt);

    return (void*)ret;
}

void* AutomationExample::launchDebuggerThread()
{
    int rc;
    pthread_t handle;
    /* Run debug server in a separate thread*/
    rc = pthread_create(&handle, NULL, &(AutomationExample::debugThreadRoutine), this);
    if (rc)
    {
        logger_err("return code from pthread_create() is %d\n", rc);
    }
    return (void*)handle;
}

/* End of the implementation of all callbacks.*/

/*
*********************** End of file ******************************************
 */
