/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui;

import java.awt.BorderLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.Action;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ListModel;

import org.jdesktop.swingx.JXTaskPane;
import org.jdesktop.swingx.JXTaskPaneContainer;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ComponentsFactory;
import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceManager;
import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceUser;
import com.lucy.g3.rtu.config.device.comms.domain.ICommsDevice;
import com.lucy.g3.rtu.config.port.PortsUtility;
import com.lucy.g3.rtu.config.port.ui.common.SerialPortSelectPane;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Channel;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3ChannelCommsConf;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3ChannelConf;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3ChannelTCPConf;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Constraints;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.connmgr.ConnectionManager;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.connmgr.ConnectionManagerPanel;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolChannelSerialConf;
import com.lucy.g3.rtu.config.protocol.shared.ui.ProtocolItemManagerPanel;
import com.lucy.g3.rtu.config.protocol.shared.ui.panels.HintPanel;
import com.lucy.g3.rtu.config.shared.base.page.AbstractConfigListPage;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.xml.gen.common.DNP3Enum.DNPLINK_NETWORK_TYPE;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LINKCNFM;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LINTCP_MODE;

/**
 * SDNP3 channel configuration page.
 */
public final class SDNP3ChannelPage extends AbstractConfigListPage {

  private final PresentationModel<SDNP3ChannelConf> channelConfigPM;
  private final PresentationModel<SDNP3ChannelCommsConf> commsConfigPM;
  private final PresentationModel<CommsDeviceUser> commsSelectionPM;
  private final PresentationModel<SDNP3ChannelTCPConf> tcpConfigPM;
  private final PresentationModel<ProtocolChannelSerialConf> serialConfigPM;

  private final SDNP3Channel channel;
  private final SDNP3SessionManagerModel sessionMgrModel;
  private final ConnectionManager connectionManager;

  private final IConfig config;
  
  public SDNP3ChannelPage(SDNP3Channel sdnp3Chnl, IConfig config) {
    super(sdnp3Chnl);
    this.channel = Preconditions.checkNotNull(sdnp3Chnl, "sdnp3Chnl is null");
    this.config = Preconditions.checkNotNull(config, "config is null");

    SDNP3ChannelConf chlcfg = sdnp3Chnl.getChannelConfig();
    this.connectionManager = chlcfg.getConnectionManager();

    channelConfigPM = new PresentationModel<SDNP3ChannelConf>(chlcfg);
    tcpConfigPM = new PresentationModel<SDNP3ChannelTCPConf>(chlcfg.getTcpConf());
    serialConfigPM = new PresentationModel<ProtocolChannelSerialConf>(chlcfg.getSerialConf());
    commsConfigPM = new PresentationModel<SDNP3ChannelCommsConf>(chlcfg.getCommsConf());
    commsSelectionPM = new PresentationModel<CommsDeviceUser>(chlcfg.getCommsConf().getCommsDeviceSelection());
    sessionMgrModel = new SDNP3SessionManagerModel(sdnp3Chnl);

    /* Bind node name to channel name and page title */
    ValueModel vm = channelConfigPM.getModel(SDNP3ChannelConf.PROPERTY_CHANNEL_NAME);
    PropertyConnector.connectAndUpdate(vm, this, PROPERTY_NODE_NAME);
    PropertyConnector.connectAndUpdate(vm, this, PROPERTY_TITLE);
  }
  
  // @formatter:off
  private void createUIComponents() {
    // ====== Panels ======
    panelHint = new HintPanel(SDNP3ChannelPage.class);
    panelSessionList = new ProtocolItemManagerPanel(
        sessionMgrModel.getSelectionInList(),
        sessionMgrModel.getPageActions(),
        getViewAction());
    panelConnectManager = new ConnectionManagerPanel(connectionManager, config);
    panelSerial = new SerialPortSelectPane(
        serialConfigPM.getModel(ProtocolChannelSerialConf.PROPERTY_SERIAL_PORT),
        PortsUtility.getSerialPortsListModel(config));

    // ====== Channel  ======
    ComponentsFactory factory = new ComponentsFactory(
        SDNP3Constraints.INSTANCE,
        SDNP3Constraints.PREFIX_CHANNEL,
        channelConfigPM);

    tfChannelName        = factory.createTextField(SDNP3ChannelConf.PROPERTY_CHANNEL_NAME);
    ftfTransmitted       = factory.createNumberField(SDNP3ChannelConf.PROPERTY_TXFRAMESIZE);
    ftfReceived          = factory.createNumberField(SDNP3ChannelConf.PROPERTY_RXFRAMESIZE);
    ftfRecTimeout        = factory.createNumberField(SDNP3ChannelConf.PROPERTY_RXFRAMETIMEOUT);
    ftfTransmitted2      = factory.createNumberField(SDNP3ChannelConf.PROPERTY_TXFRAGMENTSIZE);
    ftfReceived2         = factory.createNumberField(SDNP3ChannelConf.PROPERTY_RXFRAGMENTSIZE);
    ftfConfirmTimout     = factory.createNumberField(SDNP3ChannelConf.PROPERTY_CONFIRMTIMEOUT);
    ftfRetries           = factory.createNumberField(SDNP3ChannelConf.PROPERTY_MAXRETRIES);
    ftfOfflinePollPeriod = factory.createNumberField(SDNP3ChannelConf.PROPERTY_OFFLINE_POLL_PERIOD);
    cbBroadcastEnable    = factory.createCheckBox(SDNP3ChannelConf.PROPERTY_BROADCAST_ENABLED);
    comboConfirmMode     = factory.createComboBox(SDNP3ChannelConf.PROPERTY_CONFIRMMODE, LU_LINKCNFM.values());
    comboNetworkType     = factory.createComboBox(SDNP3ChannelConf.PROPERTY_NETWORKTYPE, DNPLINK_NETWORK_TYPE.values());

    // ====== TCP ======
    factory = new ComponentsFactory(
        SDNP3Constraints.INSTANCE,
        SDNP3Constraints.PREFIX_CHANNEL_TCP,
        tcpConfigPM);

    ftfPort             = factory.createNumberField(SDNP3ChannelTCPConf.PROPERTY_IP_PORT);
    ftfConnTimeout      = factory.createNumberField(SDNP3ChannelTCPConf.PROPERTY_IP_CONNECT_TIMEOUT);
    tfMasterAddress     = factory.createNumberField(SDNP3ChannelTCPConf.PROPERTY_MASTER_ADDRESS);
    ftfUDPPort          = factory.createNumberField(SDNP3ChannelTCPConf.PROPERTY_LOCAL_UDP_PORT);
    ftfDestUDPPort      = factory.createNumberField(SDNP3ChannelTCPConf.PROPERTY_DEST_UDP_PORT);
    ftfUnsoliUDPPort    = factory.createNumberField(SDNP3ChannelTCPConf.PROPERTY_INIT_UNSOL_UDP_PORT);
    comboConnectionMode = factory.createComboBox(SDNP3ChannelTCPConf.PROPERTY_MODE, LU_LINTCP_MODE.values());
    cbValidateMIP       = factory.createCheckBox(SDNP3ChannelTCPConf.PROPERTY_VALIDATE_MASTER_IP);
    cbDisOnNewSync      = factory.createCheckBox(SDNP3ChannelTCPConf.PROPERTY_DISCONNECT_ON_NEW_SYNC);
    checkBoxAnyIP       = factory.createCheckBox(SDNP3ChannelTCPConf.PROPERTY_ANY_IP_ENABLED);
    ftfDualEndPort      = factory.createNumberField(SDNP3ChannelTCPConf.PROPERTY_DUAL_ENDPOINT_IP_PORT);
    ftfIPAddress        = factory.createIpField(
        SDNP3ChannelTCPConf.PROPERTY_IP_ADDRESS,
        SDNP3ChannelTCPConf.PROPERTY_ANY_IP_ENABLED);


    // ====== Comms Devices ======
    factory =  new ComponentsFactory(
        SDNP3Constraints.INSTANCE,
        SDNP3Constraints.PREFIX_CHANNEL_COMMS,
        commsConfigPM);

    
    chkCommsClass1   = factory.createCheckBox(SDNP3ChannelCommsConf.PROPERTY_CONNECT_ON_CLASS1);
    chkCommsClass2   = factory.createCheckBox(SDNP3ChannelCommsConf.PROPERTY_CONNECT_ON_CLASS2);
    chkCommsClass3   = factory.createCheckBox(SDNP3ChannelCommsConf.PROPERTY_CONNECT_ON_CLASS3);
    
    CommsDeviceManager commsMgr =  config.getConfigModule(CommsDeviceManager.CONFIG_MODULE_ID);
    SelectionInList<ICommsDevice> selectionList = new SelectionInList<>(
        commsMgr.getItemListModel(), 
        commsSelectionPM.getModel(CommsDeviceUser.PROPERTY_COMMS_DEVICE));
    comboCommsDevice = BasicComponentFactory.createComboBox(selectionList);
  }
  // @formatter:on

  private void populate() {
    ioConfigPane.add(panelIONetworkType);
    ioConfigPane.add(panelIOConnection);
    ioConfigPane.add(panelIOUDP);
    ioConfigPane.add(panelSerial);
    ioConfigPane.add(panelCommsDevice);
    ioConfigPane.add(panelConnectManager);

    dataLinkPane.add(panelAppLayer);
    dataLinkPane.add(panelDatalinkLayer);

    // Set default collapsed
    ((JXTaskPane) panelIOUDP).setCollapsed(true);
  }

  private void updateTCPMode() {
    Object selectedItem = comboConnectionMode.getSelectedItem();
    if (selectedItem == null) {
      selectedItem = channel.getChannelConfig().getTcpConf().getMode();
    }
    if (selectedItem == LU_LINTCP_MODE.LU_LINTCP_MODE_SERVER) {
      checkBoxAnyIP.setVisible(true);
      labelPort.setText("Listening Port:");
      labelDualEndpointPort.setVisible(false);
      ftfDualEndPort.setVisible(false);
      labelConTimeout.setVisible(false);
      labelCTMS.setVisible(false);
      ftfConnTimeout.setVisible(false);
    } else if (selectedItem == LU_LINTCP_MODE.LU_LINTCP_MODE_CLIENT) {
      checkBoxAnyIP.setVisible(false);
      checkBoxAnyIP.setSelected(false);
      labelPort.setText("Connection Port:");
      labelDualEndpointPort.setVisible(false);
      ftfDualEndPort.setVisible(false);
      labelConTimeout.setVisible(false);
      labelCTMS.setVisible(false);
      ftfConnTimeout.setVisible(false);

    } else if (selectedItem == LU_LINTCP_MODE.LU_LINTCP_MODE_DUAL_ENDPOINT) {
      checkBoxAnyIP.setVisible(false);
      checkBoxAnyIP.setSelected(false);
      labelPort.setText("Listening Port:");
      labelDualEndpointPort.setVisible(true);
      labelDualEndpointPort.setText("Connection Port:");
      ftfDualEndPort.setVisible(true);
      labelConTimeout.setVisible(true);
      labelCTMS.setVisible(true);
      ftfConnTimeout.setVisible(true);
    }
    
    cbDisOnNewSync.setVisible(selectedItem == LU_LINTCP_MODE.LU_LINTCP_MODE_SERVER
        || selectedItem == LU_LINTCP_MODE.LU_LINTCP_MODE_DUAL_ENDPOINT);
  }

  private void updateNetworkType() {
    DNPLINK_NETWORK_TYPE selectNetworkType = (DNPLINK_NETWORK_TYPE) comboNetworkType.getSelectedItem();
    if (selectNetworkType == DNPLINK_NETWORK_TYPE.DNPLINK_NETWORK_SERIAL) {
      panelSerial.setVisible(true);
      panelIOUDP.setVisible(false);
      panelIOConnection.setVisible(false);
      panelCommsDevice.setVisible(false);
      panelConnectManager.setVisible(false);
      labelMasterAddress.setVisible(false);
      tfMasterAddress.setVisible(false);

    } else if (selectNetworkType == DNPLINK_NETWORK_TYPE.DNPLINK_NETWORK_COMMS_DEVICE) {
      panelSerial.setVisible(false);
      panelIOUDP.setVisible(false);
      panelIOConnection.setVisible(false);
      panelCommsDevice.setVisible(true);
      panelConnectManager.setVisible(false);
      labelMasterAddress.setVisible(false);
      tfMasterAddress.setVisible(false);

    } else if (selectNetworkType == DNPLINK_NETWORK_TYPE.DNPLINK_NETWORK_TCP_ONLY) {
      panelSerial.setVisible(false);
      panelIOUDP.setVisible(false);
      panelIOConnection.setVisible(true);
      panelConnectManager.setVisible(true);
      panelCommsDevice.setVisible(false);
      labelMasterAddress.setVisible(connectionManager.isEnabled());
      tfMasterAddress.setVisible(connectionManager.isEnabled());

    } else if (selectNetworkType == DNPLINK_NETWORK_TYPE.DNPLINK_NETWORK_TCP_UDP) {
      panelSerial.setVisible(false);
      panelIOUDP.setVisible(true);
      panelIOConnection.setVisible(true);
      panelConnectManager.setVisible(true);
      panelCommsDevice.setVisible(false);
      labelMasterAddress.setVisible(false);
      tfMasterAddress.setVisible(false);

    } else if (selectNetworkType == DNPLINK_NETWORK_TYPE.DNPLINK_NETWORK_UDP_ONLY) {
      panelSerial.setVisible(false);
      panelIOUDP.setVisible(true);
      panelIOConnection.setVisible(false);
      panelConnectManager.setVisible(false);
      panelCommsDevice.setVisible(false);
      labelMasterAddress.setVisible(false);
      tfMasterAddress.setVisible(false);
    }
  }

  private void comboNetworkTypeItemStateChanged(ItemEvent e) {
    updateNetworkType();
  }

  private void comboConnectionModeItemStateChanged() {
    updateTCPMode();
  }

  @Override
  public ListModel<SDNP3Session> getSelsectionListModel() {
    return channel.getItemListModel();
  }

  @Override
  public Action[] getContextActions() {
    return sessionMgrModel.getNodeActions();
  }

  @Override
  protected void init() throws Exception {
    super.init();
    initComponents();
    populate();
    updateNetworkType();
    updateTCPMode();
    comboConnectionMode.setSelectedItem(2);
  }

  @Override
  protected SelectionInList<?> getSelectionInList() {
    return sessionMgrModel.getSelectionInList();
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    tabbedPane1 = new JTabbedPane();
    JScrollPane scrollPane1 = new JScrollPane();
    ioConfigPane = new JXTaskPaneContainer();
    JScrollPane scrollPane0 = new JScrollPane();
    dataLinkPane = new JXTaskPaneContainer();
    panelIONetworkType = new JXTaskPane("General");
    labelChnName = new JLabel();
    labelNetworktype = new JLabel();
    panelIOConnection = new JXTaskPane("TCP Connection");
    labelMode = new JLabel();
    labelIPAdd = new JLabel();
    panel4 = new JPanel();
    labelPort = new JLabel();
    labelMasterAddress = new JLabel();
    labelDualEndpointPort = new JLabel();
    labelConTimeout = new JLabel();
    labelCTMS = new JLabel();
    panelIOUDP = new JXTaskPane("UDP Port");
    labelLocal = new JLabel();
    labelDest = new JLabel();
    labelinitUDPport = new JLabel();
    panelAppLayer = new JXTaskPane("Application Layer");
    labelFragmentTrans = new JLabel();
    labeloctets2 = new JLabel();
    labeloctets3 = new JLabel();
    labelFragmentRec = new JLabel();
    panelDatalinkLayer = new JXTaskPane("Data Link Layer");
    labelRxFrameSize = new JLabel();
    labeloctets0 = new JLabel();
    labelTxFrameSize = new JLabel();
    labeloctets1 = new JLabel();
    labelRxFrameTimeout = new JLabel();
    labelMs0 = new JLabel();
    labelConfirmMode = new JLabel();
    label5 = new JLabel();
    labelMs1 = new JLabel();
    labelRetries = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();
    panelCommsDevice = new JXTaskPane("Comms Device");
    label11 = new JLabel();
    lblConnectOnClass2 = new JLabel();
    panel1 = new JPanel();

    //======== this ========
    setLayout(new BorderLayout());

    //======== tabbedPane1 ========
    {

      //======== scrollPane1 ========
      {
        scrollPane1.setBorder(null);
        scrollPane1.setViewportView(ioConfigPane);
      }
      tabbedPane1.addTab("Channel", scrollPane1);

      //======== scrollPane0 ========
      {
        scrollPane0.setBorder(null);
        scrollPane0.setViewportView(dataLinkPane);
      }
      tabbedPane1.addTab("Channel Settings", scrollPane0);
      tabbedPane1.addTab("Sessions", panelSessionList);
    }
    add(tabbedPane1, BorderLayout.CENTER);
    add(panelHint, BorderLayout.SOUTH);

    //======== panelIONetworkType ========
    {
      panelIONetworkType.setLayout(new FormLayout(
        "default, [80dlu,default]",
        "default, $ugap, fill:default"));

      //---- labelChnName ----
      labelChnName.setText("Channel Name:");
      panelIONetworkType.add(labelChnName, CC.xy(1, 1));
      panelIONetworkType.add(tfChannelName, CC.xy(2, 1));

      //---- labelNetworktype ----
      labelNetworktype.setText("Network Type: ");
      panelIONetworkType.add(labelNetworktype, CC.xy(1, 3));

      //---- comboNetworkType ----
      comboNetworkType.addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent e) {
          comboNetworkTypeItemStateChanged(e);
        }
      });
      panelIONetworkType.add(comboNetworkType, CC.xy(2, 3));
    }

    //======== panelIOConnection ========
    {
      panelIOConnection.setLayout(new FormLayout(
        "right:[100dlu,default], $lcgap, [80dlu,default], $ugap, [40dlu,default]",
        "5*(default, $lgap), default"));

      //---- labelMode ----
      labelMode.setText("Mode:");
      panelIOConnection.add(labelMode, CC.xy(1, 1));

      //---- comboConnectionMode ----
      comboConnectionMode.addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent e) {
          comboConnectionModeItemStateChanged();
        }
      });
      panelIOConnection.add(comboConnectionMode, CC.xy(3, 1));

      //---- cbDisOnNewSync ----
      cbDisOnNewSync.setText("Disconnect on New SYN");
      panelIOConnection.add(cbDisOnNewSync, CC.xy(5, 1));

      //---- labelIPAdd ----
      labelIPAdd.setText("IP Address:");
      panelIOConnection.add(labelIPAdd, CC.xy(1, 3));
      panelIOConnection.add(ftfIPAddress, CC.xy(3, 3));

      //======== panel4 ========
      {
        panel4.setOpaque(false);
        panel4.setLayout(new FormLayout(
          "default, $lcgap, default",
          "default"));

        //---- checkBoxAnyIP ----
        checkBoxAnyIP.setText("Any");
        checkBoxAnyIP.setOpaque(false);
        panel4.add(checkBoxAnyIP, CC.xy(1, 1));

        //---- cbValidateMIP ----
        cbValidateMIP.setText("Validate");
        cbValidateMIP.setOpaque(false);
        cbValidateMIP.setToolTipText("Validate Master IP");
        panel4.add(cbValidateMIP, CC.xy(3, 1));
      }
      panelIOConnection.add(panel4, CC.xy(5, 3));

      //---- labelPort ----
      labelPort.setText("Port:");
      panelIOConnection.add(labelPort, CC.xy(1, 5));
      panelIOConnection.add(ftfPort, CC.xy(3, 5));

      //---- labelMasterAddress ----
      labelMasterAddress.setText("Master Address:");
      panelIOConnection.add(labelMasterAddress, CC.xy(1, 7));
      panelIOConnection.add(tfMasterAddress, CC.xy(3, 7));

      //---- labelDualEndpointPort ----
      labelDualEndpointPort.setText("Dual End Point IP Port:");
      panelIOConnection.add(labelDualEndpointPort, CC.xy(1, 9));
      panelIOConnection.add(ftfDualEndPort, CC.xy(3, 9));

      //---- labelConTimeout ----
      labelConTimeout.setText("Connection Timeout:");
      panelIOConnection.add(labelConTimeout, CC.xy(1, 11));
      panelIOConnection.add(ftfConnTimeout, CC.xy(3, 11));

      //---- labelCTMS ----
      labelCTMS.setText("ms");
      panelIOConnection.add(labelCTMS, CC.xy(5, 11));
    }

    //======== panelIOUDP ========
    {
      panelIOUDP.setLayout(new FormLayout(
        "right:default, $lcgap, [100dlu,default]",
        "2*(default, $lgap), default"));

      //---- labelLocal ----
      labelLocal.setText("Local UDP Port:");
      panelIOUDP.add(labelLocal, CC.xy(1, 1));

      //---- ftfUDPPort ----
      ftfUDPPort.setColumns(8);
      panelIOUDP.add(ftfUDPPort, CC.xy(3, 1));

      //---- labelDest ----
      labelDest.setText("Destination UDP port:");
      panelIOUDP.add(labelDest, CC.xy(1, 3));

      //---- ftfDestUDPPort ----
      ftfDestUDPPort.setColumns(8);
      panelIOUDP.add(ftfDestUDPPort, CC.xy(3, 3));

      //---- labelinitUDPport ----
      labelinitUDPport.setText("Initial Unsolicited UDP Port:");
      panelIOUDP.add(labelinitUDPport, CC.xy(1, 5));

      //---- ftfUnsoliUDPPort ----
      ftfUnsoliUDPPort.setColumns(8);
      panelIOUDP.add(ftfUnsoliUDPPort, CC.xy(3, 5));
    }

    //======== panelAppLayer ========
    {
      panelAppLayer.setLayout(new FormLayout(
        "right:default, $lcgap, left:default, $lcgap, pref",
        "default, $lgap, default"));

      //---- labelFragmentTrans ----
      labelFragmentTrans.setText("Transmitted Fragment:");
      panelAppLayer.add(labelFragmentTrans, CC.xy(1, 1));

      //---- labeloctets2 ----
      labeloctets2.setText("Bytes");
      panelAppLayer.add(labeloctets2, CC.xy(5, 1));

      //---- labeloctets3 ----
      labeloctets3.setText("Bytes");
      panelAppLayer.add(labeloctets3, CC.xy(5, 3));

      //---- ftfReceived2 ----
      ftfReceived2.setColumns(8);
      panelAppLayer.add(ftfReceived2, CC.xy(3, 3));

      //---- ftfTransmitted2 ----
      ftfTransmitted2.setColumns(8);
      panelAppLayer.add(ftfTransmitted2, CC.xy(3, 1));

      //---- labelFragmentRec ----
      labelFragmentRec.setText("Received Fragment:");
      panelAppLayer.add(labelFragmentRec, CC.xy(1, 3));
    }

    //======== panelDatalinkLayer ========
    {
      panelDatalinkLayer.setLayout(new FormLayout(
        "right:default, $lcgap, left:default, $lcgap, pref, 10dlu, right:default, $lcgap, left:default, $lcgap, default",
        "6*(default, $lgap), default"));

      //---- labelRxFrameSize ----
      labelRxFrameSize.setText("Transmitted Frame:");
      panelDatalinkLayer.add(labelRxFrameSize, CC.xy(1, 1));

      //---- ftfTransmitted ----
      ftfTransmitted.setColumns(8);
      panelDatalinkLayer.add(ftfTransmitted, CC.xy(3, 1));

      //---- labeloctets0 ----
      labeloctets0.setText("Bytes");
      panelDatalinkLayer.add(labeloctets0, CC.xy(5, 1));

      //---- labelTxFrameSize ----
      labelTxFrameSize.setText("Received Frame:");
      panelDatalinkLayer.add(labelTxFrameSize, CC.xy(1, 3));

      //---- ftfReceived ----
      ftfReceived.setColumns(8);
      panelDatalinkLayer.add(ftfReceived, CC.xy(3, 3));

      //---- labeloctets1 ----
      labeloctets1.setText("Bytes");
      panelDatalinkLayer.add(labeloctets1, CC.xy(5, 3));

      //---- labelRxFrameTimeout ----
      labelRxFrameTimeout.setText("Receive Timeout:");
      panelDatalinkLayer.add(labelRxFrameTimeout, CC.xy(7, 3));

      //---- ftfRecTimeout ----
      ftfRecTimeout.setColumns(8);
      panelDatalinkLayer.add(ftfRecTimeout, CC.xy(9, 3));

      //---- labelMs0 ----
      labelMs0.setText("ms");
      panelDatalinkLayer.add(labelMs0, CC.xy(11, 3));

      //---- labelConfirmMode ----
      labelConfirmMode.setText("Confirm Mode:");
      panelDatalinkLayer.add(labelConfirmMode, CC.xy(1, 5));
      panelDatalinkLayer.add(comboConfirmMode, CC.xywh(3, 5, 3, 1, CC.FILL, CC.DEFAULT));

      //---- label5 ----
      label5.setText("Confirm Timeout:");
      panelDatalinkLayer.add(label5, CC.xy(1, 7));

      //---- ftfConfirmTimout ----
      ftfConfirmTimout.setColumns(8);
      panelDatalinkLayer.add(ftfConfirmTimout, CC.xy(3, 7));

      //---- labelMs1 ----
      labelMs1.setText("ms");
      panelDatalinkLayer.add(labelMs1, CC.xy(5, 7));

      //---- ftfRetries ----
      ftfRetries.setColumns(8);
      panelDatalinkLayer.add(ftfRetries, CC.xy(3, 9));

      //---- labelRetries ----
      labelRetries.setText("Maximum Retries:");
      panelDatalinkLayer.add(labelRetries, CC.xy(1, 9));

      //---- label9 ----
      label9.setText("Offline Poll Period:");
      panelDatalinkLayer.add(label9, CC.xy(1, 11));

      //---- ftfOfflinePollPeriod ----
      ftfOfflinePollPeriod.setColumns(8);
      panelDatalinkLayer.add(ftfOfflinePollPeriod, CC.xy(3, 11));

      //---- label10 ----
      label10.setText("ms");
      panelDatalinkLayer.add(label10, CC.xy(5, 11));

      //---- cbBroadcastEnable ----
      cbBroadcastEnable.setText("Broadcast Address Enable");
      cbBroadcastEnable.setOpaque(false);
      panelDatalinkLayer.add(cbBroadcastEnable, CC.xywh(3, 13, 7, 1));
    }

    //======== panelCommsDevice ========
    {
      panelCommsDevice.setLayout(new FormLayout(
        "default, $lcgap, 128dlu",
        "default, $lgap, default"));

      //---- label11 ----
      label11.setText("Comms Device:");
      panelCommsDevice.add(label11, CC.xy(1, 1));
      panelCommsDevice.add(comboCommsDevice, CC.xy(3, 1, CC.FILL, CC.DEFAULT));

      //---- lblConnectOnClass2 ----
      lblConnectOnClass2.setText("Connect on Class:");
      panelCommsDevice.add(lblConnectOnClass2, CC.xy(1, 3));

      //======== panel1 ========
      {
        panel1.setOpaque(false);
        panel1.setLayout(new FormLayout(
          "2*(default, $lcgap), default",
          "default"));

        //---- chkCommsClass1 ----
        chkCommsClass1.setText("Class 1");
        chkCommsClass1.setOpaque(false);
        panel1.add(chkCommsClass1, CC.xy(1, 1));

        //---- chkCommsClass2 ----
        chkCommsClass2.setText("Class 2");
        chkCommsClass2.setOpaque(false);
        panel1.add(chkCommsClass2, CC.xy(3, 1));

        //---- chkCommsClass3 ----
        chkCommsClass3.setText("Class 3");
        chkCommsClass3.setOpaque(false);
        panel1.add(chkCommsClass3, CC.xy(5, 1));
      }
      panelCommsDevice.add(panel1, CC.xy(3, 3));
    }

    //---- panelSerial ----
    panelSerial.setTitle("Physical Layer");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JTabbedPane tabbedPane1;
  private JXTaskPaneContainer ioConfigPane;
  private JXTaskPaneContainer dataLinkPane;
  private JPanel panelSessionList;
  private JPanel panelHint;
  private JPanel panelIONetworkType;
  private JLabel labelChnName;
  private JFormattedTextField tfChannelName;
  private JLabel labelNetworktype;
  private JComboBox<Object> comboNetworkType;
  private JPanel panelIOConnection;
  private JLabel labelMode;
  private JComboBox<Object> comboConnectionMode;
  private JCheckBox cbDisOnNewSync;
  private JLabel labelIPAdd;
  private JFormattedTextField ftfIPAddress;
  private JPanel panel4;
  private JCheckBox checkBoxAnyIP;
  private JCheckBox cbValidateMIP;
  private JLabel labelPort;
  private JFormattedTextField ftfPort;
  private JLabel labelMasterAddress;
  private JFormattedTextField tfMasterAddress;
  private JLabel labelDualEndpointPort;
  private JFormattedTextField ftfDualEndPort;
  private JLabel labelConTimeout;
  private JFormattedTextField ftfConnTimeout;
  private JLabel labelCTMS;
  private JPanel panelIOUDP;
  private JLabel labelLocal;
  private JFormattedTextField ftfUDPPort;
  private JLabel labelDest;
  private JFormattedTextField ftfDestUDPPort;
  private JLabel labelinitUDPport;
  private JFormattedTextField ftfUnsoliUDPPort;
  private JPanel panelAppLayer;
  private JLabel labelFragmentTrans;
  private JLabel labeloctets2;
  private JLabel labeloctets3;
  private JFormattedTextField ftfReceived2;
  private JFormattedTextField ftfTransmitted2;
  private JLabel labelFragmentRec;
  private JPanel panelDatalinkLayer;
  private JLabel labelRxFrameSize;
  private JFormattedTextField ftfTransmitted;
  private JLabel labeloctets0;
  private JLabel labelTxFrameSize;
  private JFormattedTextField ftfReceived;
  private JLabel labeloctets1;
  private JLabel labelRxFrameTimeout;
  private JFormattedTextField ftfRecTimeout;
  private JLabel labelMs0;
  private JLabel labelConfirmMode;
  private JComboBox<Object> comboConfirmMode;
  private JLabel label5;
  private JFormattedTextField ftfConfirmTimout;
  private JLabel labelMs1;
  private JFormattedTextField ftfRetries;
  private JLabel labelRetries;
  private JLabel label9;
  private JFormattedTextField ftfOfflinePollPeriod;
  private JLabel label10;
  private JCheckBox cbBroadcastEnable;
  private JPanel panelCommsDevice;
  private JLabel label11;
  private JComboBox<Object> comboCommsDevice;
  private JLabel lblConnectOnClass2;
  private JPanel panel1;
  private JCheckBox chkCommsClass1;
  private JCheckBox chkCommsClass2;
  private JCheckBox chkCommsClass3;
  private ConnectionManagerPanel panelConnectManager;
  private SerialPortSelectPane panelSerial;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
