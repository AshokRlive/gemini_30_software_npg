<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference
  PUBLIC "-//OASIS//DTD DITA Reference//EN" "http://docs.oasis-open.org/dita/v1.1/OS/dtd/reference.dtd" >
   <reference xml:lang="en-us" id="DDHuman-Machine-Interface">
   <title>Human Machine Interface page</title>
   <shortdesc>The HMI (Human Machine Interface) is an optional module that can be used in place of a
      computer to provide basic local control and monitoring of the Gemini 3 RTU.</shortdesc>
   <prolog>
      <metadata>
         <keywords>
            <indexterm>HMI</indexterm>
            <indexterm>Human Machine Interface</indexterm>
            <indexterm>modules<indexterm>Human Machine Interface</indexterm>
            </indexterm>
         </keywords>
      </metadata>
   </prolog>
   <refbody>
      <refsyn>You are here: <menucascade>
            <uicontrol>Configuration</uicontrol>
            <uicontrol>Modules</uicontrol>
            <uicontrol>HMI</uicontrol>
         </menucascade></refsyn>
      <section>
         <title><uicontrol>Analogue Input Channel</uicontrol></title>
         <table id="table-bdg_qzy_cr">
            <tgroup cols="2">
               <colspec colname="col1" colwidth="*"/>
               <colspec colname="col2" colwidth="*"/>
               <thead>
                  <row>
                     <entry morerows="0" namest="col1" nameend="col1">Option</entry>
                     <entry morerows="0" namest="col2" nameend="col2">Description</entry>
                  </row>
               </thead>
               <tbody>
                  <row>
                     <entry morerows="0" namest="col1" nameend="col1">
                        <uicontrol>HMI Analogue Input Channels</uicontrol>
                     </entry>
                     <entry morerows="0" namest="col2" nameend="col2">Three analogue values are
                        available from the HMI: temperature, 3.3v supply and 5v supply rails. The
                        interval between these measurements can be set.</entry>
                  </row>
               </tbody>
            </tgroup>
         </table>
      </section>
      <section>
         <title><uicontrol>HMI Settings</uicontrol></title>
         <p>This table allows the toggle times for the <uicontrol>Off/Local/Remote</uicontrol>
            switch to be set.</p>
         <note>The times in the option descriptions are examples to help you understand how the
            toggle times interact.</note>
         <table id="table-j5b_5zy_cr">
            <tgroup cols="2">
               <colspec colname="col1" colwidth="*"/>
               <colspec colname="col2" colwidth="*"/>
               <thead>
                  <row>
                     <entry morerows="0" namest="col1" nameend="col1">Option</entry>
                     <entry morerows="0" namest="col2" nameend="col2">Description</entry>
                  </row>
               </thead>
               <tbody>
                  <row>
                     <entry morerows="0" namest="col1" nameend="col1">
                        <uicontrol>Local Remote Toggle Time</uicontrol>
                     </entry>
                     <entry morerows="0" namest="col2" nameend="col2">Press the
                           <uicontrol>MODE</uicontrol> button for more than half a second, but less
                        than 3 seconds, to toggle between the <uicontrol>Local</uicontrol> and
                           <uicontrol>Remote</uicontrol> states.</entry>
                  </row>
                  <row>
                     <entry morerows="0" namest="col1" nameend="col1"><uicontrol>Off Toggle
                           Time</uicontrol></entry>
                     <entry morerows="0" namest="col2" nameend="col2">Press the
                           <uicontrol>MODE</uicontrol> button for three seconds or longer to select
                        the <uicontrol>Off</uicontrol> state.</entry>
                  </row>
                  <row>
                     <entry namest="col1" nameend="col1"><uicontrol>Open/Close Press
                           Time</uicontrol></entry>
                     <entry namest="col2" nameend="col2">Press the <uicontrol>ACT</uicontrol> and
                           <uicontrol>OPEN</uicontrol> or <uicontrol>CLOSE</uicontrol> buttons
                        simultaneously for a minimum of three seconds to start the
                           <uicontrol>OPEN</uicontrol> or <uicontrol>CLOSE</uicontrol>
                        operation.</entry>
                  </row>
                  <row>
                     <entry namest="col1" nameend="col1"><uicontrol>Button Click</uicontrol></entry>
                     <entry morerows="0" namest="col2" nameend="col2">Select the check box to enable
                        a click sound to be heard when a button on the HMI is pressed.</entry>
                  </row>
                  <row>
                     <entry morerows="0" namest="col1" nameend="col1"><uicontrol>Enable
                           Off/Local/Remote Button</uicontrol>
                     </entry>
                     <entry morerows="0" namest="col2" nameend="col2">Select the check box to allow
                        the <uicontrol>MODE</uicontrol> button on the HMI to be used.</entry>
                  </row>
               </tbody>
            </tgroup>
         </table>
      </section>
      <section>
         <title><uicontrol>HMI Menu</uicontrol></title>
         <p>This table allows the information on the LCD display to be defined. This includes all
            analogue and digital measurements as well as control logics.</p>
         <table id="table-jh2_5zy_cr">
            <tgroup cols="2">
               <colspec colname="col1" colwidth="*"/>
               <colspec colname="col2" colwidth="*"/>
               <thead>
                  <row>
                     <entry morerows="0" namest="col1" nameend="col1">Option</entry>
                     <entry morerows="0" namest="col2" nameend="col2">Description</entry>
                  </row>
               </thead>
               <tbody>
                  <row>
                     <entry morerows="0" namest="col1" nameend="col1">
                        <uicontrol>Add</uicontrol>
                     </entry>
                     <entry morerows="0" namest="col2" nameend="col2">
                        <p>Add a new menu item:</p>
                        <ul id="ul-gx2_szz_cr">
                           <li><uicontrol>Sub Menu</uicontrol></li>
                           <li><uicontrol>Control Screen</uicontrol></li>
                           <li><uicontrol>Data Screen</uicontrol></li>
                           <li><uicontrol>Phase Screen</uicontrol></li>
                           <li><uicontrol>Info Screen</uicontrol></li>
                        </ul>
                     </entry>
                  </row>
                  <row>
                     <entry namest="col1" nameend="col1"><uicontrol>Edit</uicontrol></entry>
                     <entry namest="col2" nameend="col2">Modify the selected menu item.</entry>
                  </row>
                  <row>
                     <entry namest="col1" nameend="col1"><uicontrol>Remove</uicontrol></entry>
                     <entry namest="col2" nameend="col2">Delete the selected menu item.</entry>
                  </row>
                  <row>
                     <entry namest="col1" nameend="col1"><uicontrol>Rebuild HMI</uicontrol></entry>
                     <entry namest="col2" nameend="col2">Delete the current HMI menu, and then build
                        a new one.</entry>
                  </row>
                  <row>
                     <entry namest="col1" nameend="col1"><uicontrol>Validate</uicontrol></entry>
                     <entry namest="col2" nameend="col2">Validate the configuration of the HMI
                        menu.</entry>
                  </row>
               </tbody>
            </tgroup>
         </table>
      </section>
   </refbody>
</reference>
