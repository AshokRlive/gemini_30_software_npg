/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater.sdp.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.base.Strings;
import com.lucy.g3.common.utils.ZipUtil;
import com.lucy.g3.rtu.updater.sdp.IModuleSoftware;
import com.lucy.g3.rtu.updater.sdp.ISoftwareFilter;
import com.lucy.g3.rtu.updater.sdp.ISoftwarePackage;
import com.lucy.g3.rtu.updater.sdp.SoftwarePackageUtils;


public class SoftwarePackage implements ISoftwarePackage {
  public static final String SDP_XML_NAME = "SDP.xml";
  
  private final File sdpFile;
  private final String version;
  private final IModuleSoftware[] entries;
  private final String[] configFiles;
  
  SoftwarePackage(File sdpFile, String version, IModuleSoftware[] entries, String[] configFiles) {
    super();
    this.sdpFile = sdpFile;
    this.version = version;
    this.entries = entries;
    this.configFiles = configFiles;
  }

  @Override
  public IModuleSoftware[] getAvailable(ISoftwareFilter filter) {
    ArrayList<IModuleSoftware> found = new ArrayList<>();
    
    if(filter != null) {
      for (IModuleSoftware entry : entries) {
        if(filter.isAccepted(entry)) {
          found.add(entry);
        }
      }
    } else {
      found.addAll(Arrays.asList(entries));
    }
    
    return found.toArray(new IModuleSoftware[found.size()]);
  }

  @Override
  public String getVersion() {
    return version;
  }

  @Override
  public File getFile() {
    return sdpFile;
  }

  @Override
  public void extract(String relativePath, String destDir) throws IOException {
    Preconditions.checkNotBlank(relativePath, "relativePath must not be blank");
    Preconditions.checkNotBlank(destDir, "destination must not be blank");
    ZipUtil.unzip(sdpFile.getPath(), relativePath, destDir);
  }
  
  @Override
  public String printContent() {
    StringBuilder sb = new StringBuilder();
    
    sb.append("SDP version: ");
    sb.append(Strings.isBlank(version)?"N/A":version);
    sb.append("\n");
    
    sb.append("==== Software Entries ==== \n");
    for (IModuleSoftware entry : entries) {
      sb.append(String.format("%-16s", entry.getName()));
      sb.append(" Type:");
      sb.append(entry.getSupportedType());
      sb.append(" System API:");
      sb.append(entry.getSupportedSystemAPI().toString());
      sb.append(" Version:");
      sb.append(entry.getSoftwareVersion().toString());
      sb.append(" Feature:");
      sb.append(Arrays.toString(entry.getSupportedFeature()));
      sb.append("\n");
    }
    sb.append("==== End of Software Entries ==== \n");
    
    if(configFiles != null && configFiles.length > 0) {
      sb.append(" ==== Config Entries ==== \n");
      for (int i = 0; i < configFiles.length; i++) {
        sb.append("Config: ");
        sb.append(configFiles[i]);
        sb.append("\n");
      }
      sb.append(" ==== End of Config Entries ==== \n");
    } else {
      sb.append("No config file found in SDP.\n");
    }
    
    return sb.toString();
  }

  @Override
  public IModuleSoftware getLastest(ISoftwareFilter filter) {
    return SoftwarePackageUtils.findLatest(getAvailable(filter));
  }
}

