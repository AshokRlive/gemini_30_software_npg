
#include <stdio.h>

typedef char *STRING;

#include "custom.h"

/*  This file is provided so that users of PC-Lint can customize
    their software.  All rights are reserved by Gimpel Software

    Modified for SAS/C(tm) 3.01H - BRIAN, VMSI, 1988
 */

/*  i_open( name, delimeter, ordinal, prefix, parent, line )
    is called to open an include file.  name is the name
    that was extracted from between either quote delimeters
    or angle bracket delimeters or, if neither of these was present,
    the characters following #include after skipping whitespace.
    The delimeter is, accordingly, either '<' or '"' or 0 if no delimeter
    was found. When an include file is found, i_open is at first called
    with ordinal == 1 and prefix == NULL.  If i_open returns NULL,
    indicating failure to find the file,
    i_open is called again with ordinal == 2 and with prefix ==
    the first -i parameter (specifying an include directory).
    This is repeated until i_open returns a non NULL or the -i
    list is exhausted.  If the -i list is exhausted and there is
    still no success, i_open is called one last time with ordinal
    == 0 and prefix == NULL.  If NULL is returned this time, error
    No. 7 is reported.

    parent is the name of the current file being processed. i.e. the
    name of the including file.  line is the current include line.

    i_open() is also called to open modules (files not included within
    other files).  In this case parent == NULL and delimeter is 0.
    line probably contains garbage.  The cycling through -i directories
    is still supported, however.

    This argument list has been made deliberately more general
    than was required in order to handle the diverse requiremnets
    of a variety of systems.
 */

 /*
    08/12/88 - BRIAN - Modifications for  SAS/C Compiler 3.01H
                       under CMS.
 */

#include <lclib.h> /* 8/12/88 - BRIAN - Need quiet function. */

/*lint +fvr */
STRING strcpy();
STRING strcat();
/*lint -fvr */

/*lint -e715  unused paramters are OK here */

FILE * i_open( name, delimeter, ordinal, prefix, parent, line )
    STRING name;
    int delimeter;
    int ordinal;
    STRING prefix;
    STRING parent;
    STRING line;
    {
    char buffer[150];
    FILE *f = NULL;
    char *p; /* 8/12/88 - BRIAN - Declaration added */

    quiet(1); /* 8/12/88 - BRIAN - Make fopen() shutup */

    if( delimeter == 0 && parent )
        {
        /* do not accept delimeter == 0 unless this is a module
           in which case parent == NULL  */
        if( ordinal == 1 ) errorno(12);  /* issue only one message */
        /* Note the above error is in addition to error 7 */
        }

    /* 8/12/88 - BRIAN - Following lines added */
    else if( delimeter == '"' && ordinal == 1)
        {
        /* SAS/C tries quoted form 1st as a CMS style pathname */
        f = fopen( name, "r" );
	}
    else if( (delimeter == '<' && ordinal == 1) ||
             (delimeter == '"' && ordinal == 0) )
        {
        /* SAS/C tries bracket form 1st or quoted form 2nd as a */
        /* member of a GLOBALed MACLIB */
        strcpy(buffer, "%MACLIB(MEMBER ");
        p = buffer+strlen(buffer);
        strcat(buffer, name);
        while ( *p && *p!='.' && *p!=' ' ) /* upto blank or . */
            p++;
        *p++ = ')';
        *p = '\0';
        f = fopen( buffer, "r" );
        }
    /* 8/12/88 - BRIAN - Preceding lines added */

    else if( ordinal )  /* ignore last call */
        {
        if( prefix )
            {
	    strcpy( buffer, prefix );
            strcat( buffer, name );
            }
        else strcpy( buffer, name );
        f = fopen( buffer, "r" );
        }
    return f;
    }

/*lint -restore Restore error settings */
