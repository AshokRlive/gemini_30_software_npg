/*******************************************************************************
 * Copyright (c) 2007 - 2012 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.util.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.Compiler;

/**
 * The Class ChangeCompilerProductCommand.
 */
public class ChangeCompilerProductCommand extends Command {
	
	
	/** The new Compiler value. */
	private Compiler compiler;
	
	private String newProduct;
	private String oldProduct;


	public ChangeCompilerProductCommand(final Compiler compiler, final String newProduct) {
		super();
		this.compiler = compiler;
		this.newProduct = newProduct;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		oldProduct = compiler.getProduct();
		redo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		compiler.setProduct(oldProduct);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		compiler.setProduct(newProduct);
	}

}
