/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board specific IO MAP/Board Manager include header
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _BOARDIOMAP_INCLUDED
#define _BOARDIOMAP_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "IOManager.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define BOARD_IO_MAX_IDX (IO_ID_LAST)

// NOTE - Comment out until F board released.
//#define SCM_V_XXE		1

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/* Board Specific IO ID's */
typedef enum
{
	/* processor GPIO */

	/**************************************/
	/* Processor Peripheral function pins */
	/**************************************/
	IO_ID_CAN1_RX	                       =0 ,
	IO_ID_CAN1_TX	                          ,

	IO_ID_UART_DEBUG_TX	                      ,
	IO_ID_UART_DEBUG_RX	                      ,

	IO_ID_LED_SCL                             ,
	IO_ID_LED_SDA                             ,

	IO_ID_I2C1_CLK                            ,
	IO_ID_I2C1_SDA                            ,

	/*********************************/
	/* Physically connected IO Pins  */
	/* (Some Virt for NXP dev board) */
	/* (But will be physical on PSM) */
	/*********************************/
	IO_ID_LOCAL_LINE                          ,
	IO_ID_REMOTE_LINE                         ,

	IO_ID_AUTO_CONFIG_IN                      ,
	IO_ID_AUTO_CONFIG_OUT                     ,

	IO_ID_FACTORY                             ,

	IO_ID_HWDOG_1HZ_KICK                      ,

	IO_ID_APPRAM_WEN                          ,

	IO_ID_LED_CTRL_OK_GREEN                   ,
	IO_ID_LED_CTRL_OK_RED                     ,
	IO_ID_LED_CTRL_CAN_GREEN                  ,
	IO_ID_LED_CTRL_CAN_RED                    ,
	IO_ID_LED_CTRL_EXTINGUISH                 ,

	IO_ID_MOD_SEL1							  ,
	IO_ID_MOD_SEL2							  ,
	IO_ID_MOD_SEL3							  ,
	IO_ID_MOD_SEL4							  ,

	IO_ID_SWITCH_OPEN_INPUT					  ,
	IO_ID_SWITCH_CLOSED_INPUT				  ,
	IO_ID_SWITCH_EARTHED_INPUT				  ,
	IO_ID_ACTUATOR_DISABLED_INPUT			  ,
	IO_ID_INSULTATION_LOW_INPUT				  ,
	IO_ID_EXTERNAL_FPI_INPUT				  ,
	IO_ID_SPARE1_INPUT						  ,
	IO_ID_SPARE2_INPUT						  ,

	IO_ID_RELAY_FB_OPEN_UP_CLOSED			  ,
	IO_ID_RELAY_FB_OPEN_DN_CLOSED             ,
	IO_ID_RELAY_FB_OPEN_LK_CLOSED             ,
	IO_ID_RELAY_FB_CLOSE_UP_CLOSED            ,
	IO_ID_RELAY_FB_CLOSE_DN_CLOSED			  ,
	IO_ID_RELAY_FB_CLOSE_LK_CLOSED			  ,
	IO_ID_RELAY_FB_OUTPUT_1_UP_CLOSED		  ,
	IO_ID_RELAY_FB_OUTPUT_1_DN_CLOSED		  ,
	IO_ID_RELAY_FB_OUTPUT_1_LK_CLOSED         ,
	IO_ID_RELAY_FB_OUTPUT_2_UP_CLOSED		  ,
	IO_ID_RELAY_FB_OUTPUT_2_DN_CLOSED		  ,
	IO_ID_RELAY_FB_OUTPUT_2_LK_CLOSED		  ,

	IO_ID_VMOTOR_FB_ON						  ,

	IO_ID_RELAY_CTRL_VMOTOR_A				  ,
	IO_ID_RELAY_CTRL_VMOTOR_B				  ,

	IO_ID_RELAY_OPEN_UP_COMMAND               ,
	IO_ID_RELAY_OPEN_DN_COMMAND               ,
	IO_ID_RELAY_OPEN_LK_COMMAND				  ,
	IO_ID_RELAY_CLOSE_UP_COMMAND			  ,
	IO_ID_RELAY_CLOSE_DN_COMMAND			  ,
	IO_ID_RELAY_CLOSE_LK_COMMAND              ,
	IO_ID_RELAY_OUTPUT_1_UP_COMMAND			  ,
	IO_ID_RELAY_OUTPUT_1_DN_COMMAND			  ,
	IO_ID_RELAY_OUTPUT_1_LK_COMMAND			  ,
	IO_ID_RELAY_OUTPUT_2_UP_COMMAND			  ,
	IO_ID_RELAY_OUTPUT_2_DN_COMMAND			  ,
	IO_ID_RELAY_OUTPUT_2_LK_COMMAND			  ,

	IO_ID_I_MEASURE                           ,
	IO_ID_TEMP_MEASURE                        ,

	/* I2C Expanders */
	IO_ID_LED_FP_OPEN						  ,
	IO_ID_LED_FP_CLOSED						  ,
	IO_ID_LED_FP_EARTH						  ,
	IO_ID_LED_FP_ACTUATOR_DISABLED			  ,
	IO_ID_LED_FP_INSULATION_LOW				  ,
	IO_ID_LED_FP_EXTERNAL_FPI				  ,
	IO_ID_LED_FP_SPARE_INPUT_1				  ,
	IO_ID_LED_FP_SPARE_INPUT_2				  ,
	IO_ID_LED_FP_COMMAND_OPEN				  ,
	IO_ID_LED_FP_COMMAND_CLOSE				  ,
	IO_ID_LED_FP_MOTOR_SUPPLY				  ,
	IO_ID_LED_FP_OUTPUT_1				      ,
	IO_ID_LED_FP_OUTPUT_2				      ,
	IO_ID_COL1_GREEN                          ,
	IO_ID_COL2_RED                            ,
	/* SPI Digi pots */


	/* SPI ADC */

	IO_ID_SYS_HEALTHY                         ,
	IO_ID_WD_HANDSHAKE                        ,

	IO_ID_IP_LED_PWR_EN                       ,
	IO_ID_LED_PWR_FAULT                       ,

	IO_ID_TEMP_SEN_ALERT                      ,

	IO_ID_F_P_I2C_P_E_CHECK					  ,

	/* NOVRAM */
	IO_ID_APPLICATION_NVRAM                   ,
	IO_ID_IDENTITY_NVRAM                      ,

	/******************************/
	/* Virtual IO ID's go here... */
	/******************************/

	IO_ID_VIRT_ON_ELAPSED_TIME                ,
	
	IO_ID_LAST
} IO_ID;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

extern const IOMapStr BoardIOMap[BOARD_IO_MAX_IDX + 1];

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */



#endif /* _BOARDIOMAP_INCLUDED */

/*
 *********************** End of file ******************************************
 */
