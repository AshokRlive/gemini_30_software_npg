/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: mbrlink.h
 * description: Modbus Link Layer.
 */
#ifndef MBRLINK_DEFINED
#define MBRLINK_DEFINED

#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/utils/tmwchnl.h"

typedef struct MBRLinkConfig
{
  int reserved;
} MBRLINK_CONFIG;

#ifdef __cplusplus
extern "C" {
#endif

  /* function: mbrlink_initConfig
   * purpose: 
   * arguments:
   *  none
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL mbrlink_initConfig(
    MBRLINK_CONFIG *pConfig);

  /* function: mbrlink_getNeededBytes
   * purpose: return the number of bytes required to finish the
   *  current block
   * arguments:
   *  pCallbackParam - callback data, contains link layer context
   * returns
   *  number of characters to read
   */
  TMWTYPES_USHORT TMWDEFS_CALLBACK mbrlink_getNeededBytes(
    void *pCallbackParam);

  /* function: mbrlink_parseBytes
   * purpose: parse incoming data
   * arguments:
   *  pCallbackParam - callback data, contains link layer context
   *  recvBuf - received characters
   *  numBytes - number of bytes   
   *  firstByteTime - time that first byte was received. Could be zero
   *   if target layer does not fill this in. In that case it will be calculated
   *   in this function.
   * returns
   *  void
   * NOTE: This should only be called when an entire message has been received  
   * Since there is no length in the message this code will assume the entire 
   * message has been received. If this is a partial message it will appear to 
   * be a CRC error.
   */
  void TMWDEFS_CALLBACK mbrlink_parseBytes(
    void *pCallbackParam,
    TMWTYPES_UCHAR *recvBuf,
    TMWTYPES_USHORT numBytes,
    TMWTYPES_MILLISECONDS firstByteTime);

  /* function: mbrlink_transmitFrame 
   * purpose:
   * arguments:
   * returns:
   *  void
   */
  void TMWDEFS_CALLBACK mbrlink_transmitFrame(
    void *pContext,
    TMWSESN_TX_DATA *pTxDescriptor);

#ifdef __cplusplus
}
#endif
#endif /* MBLINK_DEFINED */
