/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.util;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.widgets.Text;

/**
 * The listener interface for receiving identifierVerify events.
 * The class that is interested in processing a identifierVerify
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addIdentifierVerifyListener<code> method. When
 * the identifierVerify event occurs, that object's appropriate
 * method is invoked.
 * 
 * @see IdentifierVerifyEvent
 */
public class IdentifierVerifyListener implements VerifyListener {

	/* (non-Javadoc)
	 * @see org.eclipse.swt.events.VerifyListener#verifyText(org.eclipse.swt.events.VerifyEvent)
	 */
	@Override
	public void verifyText(VerifyEvent event) {
		if (!event.doit) { // other verifylisteners which are first can
			// already
			// set it
			return;
		}
		// Get the character typed
		char myChar = event.character;
		String text = ((Text) event.widget).getText();
		if (event.keyCode == SWT.DEL || event.keyCode == SWT.BS) {
			return;
		}
		if ((myChar == '_' || isIdentifierChar(myChar)) && text.length() == 0) {
			event.doit = true;
		} else if (myChar == SWT.NULL) {
			event.doit = true;
		} else if ((isIdentifierChar(myChar) || Character.isDigit(myChar) || myChar == '_')
				&& text.length() >= 1) {
			event.doit = true;
		} else {
			event.doit = false;
		}
	}

	/**
	 * Checks if is valid identifier.
	 * 
	 * @param identifier the identifier
	 * 
	 * @return true, if is valid identifier
	 */
	public static boolean isValidIdentifier(String identifier) {
		return (null == isValidIdentifierWithErrorMessage(identifier));
	}
	/**
	 * Checks if is valid identifier.
	 * 
	 * @param identifier the identifier
	 * 
	 * @return null if it is an valid identifier otherwise an Error message
	 */
	public static String isValidIdentifierWithErrorMessage(String identifier) {
		if (identifier.length() < 1) {
			return "Length < 1";
		}
		char firstChar = identifier.charAt(0);
		if (firstChar != '_' && !isIdentifierChar(firstChar)) {
			return "Identifier has to start with '_' or a character";
		}
		for (int i = 0; i < identifier.length(); i++) {
			Character myChar = identifier.charAt(i);
			if ((!isIdentifierChar(myChar) && !Character.isDigit(myChar) && myChar != '_')) {
				return "The char: " + myChar + " is not allowed within identifiers";
			}
		}
		return null;
	}
	
	private static boolean isIdentifierChar(char character){
		char toLower = Character.toLowerCase(character);
		return (('a' == toLower) || 
				('b' == toLower) || 
				('c' == toLower) || 
				('d' == toLower) || 
				('e' == toLower) || 
				('f' == toLower) || 
				('g' == toLower) || 
				('h' == toLower) || 
				('i' == toLower) || 
				('j' == toLower) || 
				('k' == toLower) || 
				('l' == toLower) || 
				('m' == toLower) || 
				('n' == toLower) || 
				('o' == toLower) || 
				('p' == toLower) || 
				('q' == toLower) || 
				('r' == toLower) || 
				('s' == toLower) || 
				('t' == toLower) || 
				('u' == toLower) || 
				('v' == toLower) || 
				('w' == toLower) || 
				('x' == toLower) || 
				('y' == toLower) || 
				('z' == toLower));
	}
	
}
