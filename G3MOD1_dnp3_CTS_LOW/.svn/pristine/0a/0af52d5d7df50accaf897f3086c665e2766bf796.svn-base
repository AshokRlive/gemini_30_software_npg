/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/


/* file: s14flsna.c
 * description: IEC 60870-5-101 slave FLSNA (File Transfer - 
 *  Last section, last segment) functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14flsna.h"
#include "tmwscl/i870/s14file.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/i14diag.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/utils/tmwtarg.h"

#if S14DATA_SUPPORT_FILE

/* function: s14flsna_processRequest */
void TMWDEFS_CALLBACK s14flsna_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{ 

  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  /* Store originator address */
  p14Sector->fileOriginator = pMsg->origAddress;
  
  /* Diagnostics */
  I14DIAG_SHOW_STORE_TYPE(pSector, pMsg);

  if(pMsg->cot != I14DEF_COT_FILE_XFER)
  { 
    /* Build and send a Negative Activation Confirmation */
    s14sesn_sendNegActCon(pSector, pMsg, I14DEF_COT_UNKNOWN_COT);
    return;
  }
  
  /* Parse Information Object Address */
  i870util_readInfoObjAddr(pSector->pSession, pMsg, &p14Sector->fileIOA);
  
  /* Read name of file */
  tmwtarg_get16(&pMsg->pRxData->pMsgBuf[pMsg->offset], &p14Sector->fileName);
  pMsg->offset += 2;

  /* Read name of section */
  p14Sector->fileSectionName = pMsg->pRxData->pMsgBuf[pMsg->offset++];

  /* Read Last section or segment qualifier */
  p14Sector->fileLSQ = pMsg->pRxData->pMsgBuf[pMsg->offset++];

  /* Read Checksum */
  p14Sector->fileCHS = pMsg->pRxData->pMsgBuf[pMsg->offset++];  
  
  /* Diagnostics */
  I14DIAG_SHOW_FLSNA(pSector, p14Sector->fileIOA, p14Sector->fileName, 
    p14Sector->fileSectionName, p14Sector->fileLSQ, p14Sector->fileCHS);

 
  p14Sector->fileState = S14FILE_RCV_FLSNA_STATE;
}


#endif /* S14DATA_SUPPORT_FILE */
