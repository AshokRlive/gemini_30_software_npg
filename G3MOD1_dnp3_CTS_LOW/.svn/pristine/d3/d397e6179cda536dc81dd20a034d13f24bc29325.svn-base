/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.iomodule.manager.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.channel.util.ChannelFinder;
import com.lucy.g3.rtu.config.module.iomodule.domain.IOModule;
import com.lucy.g3.rtu.config.module.shared.domain.Module;


/**
 * Extended channel finder to find channel in IO module.
 */
public class ChannelFinderExt extends ChannelFinder{
  
  public static Collection<IChannel> findChannelInModules(Collection<? extends Module> modules, ChannelType... type) {
    Preconditions.checkNotNull(modules, "modules is null");

    ArrayList<IChannel> foundChls = new ArrayList<IChannel>();

    for (Module m : modules) {
      if (m != null && m instanceof IOModule) {
        for (ChannelType t : type) {
           foundChls.addAll(Arrays.asList(((IOModule)m).getChannels(t))); 
        }
      }
    }
    return foundChls;
  }
}

