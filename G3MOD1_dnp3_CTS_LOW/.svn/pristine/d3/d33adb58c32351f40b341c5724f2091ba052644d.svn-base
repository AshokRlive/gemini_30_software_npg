/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 * 				$Id: StringUtil.h 1469 2012-06-20 20:51:28Z fryers_j $
 *              $HeadURL: http://10.11.0.6:81/svn/gemini_30_software/trunk/Development/G3/common/template/StringUtil.h $
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Common string manipulation functions
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      wang_p      Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef STRINGUTIL_H_
#define STRINGUTIL_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <sstream>
#include <iomanip>
#include <iostream>
#include <cstring>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/**
 * \brief Convert any value to std::string
 *
 * \param t Value to convert
 *
 * \return Resulting string
 */
template <class T>
inline std::string to_string(const T &t)
{
    std::stringstream ss;
    ss << t;
    return ss.str();
}

/* A char is automatically converted to a character and not to the value: make conversion as needed */
inline std::string to_string(unsigned char &t)
{
    return to_string(static_cast<lu_uint32_t>(t));
}

inline std::string to_string(char &t)
{
    return to_string(static_cast<lu_uint32_t>(t));
}

/**
 * \brief convert any value to std::string in hex format.
 *
 * \param i Value to convert.
 *
 * \return Resulting string.
 */
template< typename T >
inline std::string to_hex_string_value(const T i )
{
  std::stringstream stream;
  stream << std::setfill ('0') << std::setw(sizeof(T)*2)
         << std::hex << static_cast<lu_uint64_t>(i);
  return stream.str();
}

/**
 * Convert an array to a string in hex format.
 *
 * All values are returned together, without any separator.
 *
 * \param a Array to print.
 * \param len Number of elements in the array.
 *
 * \return Resulting string.
 */
template< typename T >
std::string to_hex_string_value( const T a[], std::size_t len )
{
    if( len == 0 ) return "" ;

    std::stringstream ss ;
    for( std::size_t i = 0 ; i < len ; ++i )
    {
        ss << std::setfill ('0') << std::setw(sizeof(T)*2)
           << std::hex << static_cast<lu_uint32_t>(a[i]);
    }

    return ss.str();
}

/**
 * \brief convert any value to std::string in hex format with trailing "0x".
 *
 * \param i Value to convert.
 *
 * \return Resulting string.
 */
template< typename T >
inline std::string to_hex_string(const T i )
{
  std::stringstream stream;
  stream << "0x"
         << std::setfill ('0') << std::setw(sizeof(T)*2)
         << std::hex << static_cast<lu_uint64_t>(i);
  return stream.str();
}

/**
 * Convert an array to a string in hex format with trailing "0x".
 *
 * \param a Array to print.
 * \param len Number of elements in the array.
 *
 * \return Resulting string.
 */
template< typename T >
std::string to_hex_string( const T a[], std::size_t len )
{
    if( len == 0 ) return "[]" ;

    std::stringstream ss ;
    ss << '[' ;
    for( std::size_t i = 0 ; i < len ; ++i )
    {
        ss << "0x"
           << std::setfill ('0') << std::setw(sizeof(T)*2)
           << std::hex << static_cast<lu_uint32_t>(a[i]) << ",";
    }

    std::string result = ss.str();
    // remove the last ", " and add a ']'
    return result.substr( 0, result.size()-1 ) + ']' ;
}


/**
 * \brief Convert a string to upper case.
 *
 * Note that this can not be used in UTF-8 or wide-char strings.
 *
 * \param str String to convert. It is modified.
 */
void toUpper(std::string& str);

/**
 * \brief Convert a string to lower case.
 *
 * Note that this can not be used in UTF-8 or wide-char strings.
 *
 * \param str String to convert. It is modified.
 */
void toLower(std::string& str);


/**
 * \brief trim_front, trim_back, and trim_all remove trailing/ending spaces and/or newlines from a string
 *
 * Note that this can not be used in UTF-8 or wide-char strings.
 *
 * \param source C++ String to modify.
 * \param removeNewLine Removes all newLine characters.
 * \param removeSpaces Removes all space and tab characters.
 *
 * \return string with undesired characters removed.
 */
std::string trim_front(const std::string& source,
                      const bool removeNewLine = true,
                      const bool removeSpaces = true);
std::string trim_back(const std::string& source,
                      const bool removeNewLine = true,
                      const bool removeSpaces = true);
std::string trim_all(const std::string& source,
                     const bool removeNewLine = true,
                     const bool removeSpaces = true);


/**
 * \brief Copies safely a C++ String into an already-defined C String
 *
 * This copies the content that fits into the C String, taking into account that
 * the source string could be bigger or smaller in size than the destination C
 * String.
 *
 * The content may be truncated if needed, and a NULL terminator is automatically
 * added at the end of the resulting C String.
 *
 * Note that this can not be used in UTF-8 or wide-char strings.
 *
 * \param source C++ String to copy.
 * \param cString Char array where to copy the source string content.
 * \param cStringSize Max size of the C String char array.
 *
 * \return none. cString is modified.
 */
inline void stringToCString(const std::string source, lu_char_t* cString, const size_t cStringSize)
{
    strcpy( cString, source.substr(0, LU_MIN(cStringSize - 1, source.size())).c_str() );
}


#endif /* STRINGUTIL_H_ */

/*
 *********************** End of file ******************************************
 */
