/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870PointType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSourceRepository;
import com.lucy.g3.rtu.config.protocol.slave.shared.util.ScadaProtocolUtility;


/**
 * A class for finding mapping resources for IEC870.
 */
public class IEC870MappingResources {
  private static Logger log = Logger.getLogger(IEC870MappingResources.class);
  
  /**
   * FindS available resource that can be mapped to IEC870 points.
   */
  public static final ArrayList<ScadaPointSource> getMappingRes(ScadaPointSourceRepository provider,
      IEC870PointType type, IScadaIoMap<?> iomap, boolean spareOnly) {
    
    Collection<? extends ScadaPointSource> res;
    
    if (provider != null) {
      switch (type) {
      case MMEA:
      case MMEB:
      case MMEC:
        res = provider.getAnalogueInputSources();
        break;
      case MSP:
        res = provider.getBinaryInputSources();
        break;
      case MDP:
        res = provider.getDoubleBinaryInputSources();
        break;
      case MIT:
        res = provider.getCounterSources();
        break;
      case CSC:
      case CDC:
        res = provider.getDigitalOutputSouces();
        break;

      default:
        throw new IllegalStateException("Unsupported IEC870 point Type: " + type);
      }
    } else {
      res = new ArrayList<ScadaPointSource>(0);
      log.error("No resource provoider!");
    }

    ArrayList<ScadaPointSource> ret = new ArrayList<ScadaPointSource>(res);
    if(ret.isEmpty()) {
      log.warn("No IEC870 mapping resource found!");
    }
    
    ret = spareOnly ? ScadaProtocolUtility.findSpareSource(ret, iomap, type) : ret;
    if(ret.isEmpty()) {
      log.warn("No spare IEC870 mapping resource found!");
    }
    
    return ret;
  }
  
}

