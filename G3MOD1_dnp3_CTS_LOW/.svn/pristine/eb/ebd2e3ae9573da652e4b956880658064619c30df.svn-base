/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.help;

import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.InputStream;
import java.net.URI;
import java.util.Properties;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

import org.apache.log4j.Logger;

class Help {
  /**
   * The root id for accessing help path of the main help.
   */
  public static final String ROOT_ID = "ROOT";

  final static String PROPERTY_HELP_PATH = "HelpID-Map"; 

  public static final String KEYF1 = "F1";
  public static final String HELPF1 = "HelpID-F1";

  private static final String HELP_MAPPING_FILE = "help/Mapping.txt";

  private Logger log = Logger.getLogger(Help.class);
  
  private final Properties map;
  private ContextAction hca;
  private ButtonAction hba;
  private String root; // root path
  private Icon icon = new ImageIcon(Help.class.getResource("/com/lucy/g3/help/resources/help-icon.png"));
  private final Cursor cursor = new Cursor(Cursor.CROSSHAIR_CURSOR);;


  /* Base constructor */
  public Help() {
    root = HelpRoot.getDefaultRoot();
    map = initMap();

    hca = new ContextAction();
    hba = new ButtonAction();

  }

  private Properties initMap() {

    Properties map = new Properties();

    try {

      ClassLoader cl = Help.class.getClassLoader();
      InputStream is = cl.getResourceAsStream(HELP_MAPPING_FILE);
      map.load(is);

    } catch (Throwable t) {
      log.error("Unable to load file :" + HELP_MAPPING_FILE);
    }

    return map;
  }

  /* Get the help icon */
  public Icon getIcon() {
    return icon;
  }

  /* Get the help cursor */
  public Cursor getCursor() {
    return cursor;
  }

  /* Set the root help path */
  public void setRoot(String root) {
    this.root = root;
  }

  /* Get the root help path */
  public String getRoot() {
    return root;
  }


  /* Register the F1 key to (highest) component */
  public void register(JComponent component) {

    String id = getPath(ROOT_ID); // FIXME

    InputMap imap = component.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    ActionMap amap = component.getActionMap();
    KeyStroke keys = KeyStroke.getKeyStroke(KEYF1);
    Action act = new FoneAction(id);

    imap.put(keys, HELPF1);
    amap.put(HELPF1, act);
  }

  /* Register a context help button */
  public void register(AbstractButton button) {
    button.addActionListener(hca);
  }

  /* Register a button with a help page */
  public void register(AbstractButton button, String id) {
    button.addActionListener(hba);
    button.putClientProperty(PROPERTY_HELP_PATH, getPath(id));
  }

  /* Register a Swing component to help */
  public void register(JComponent component, String id) {
    component.putClientProperty(PROPERTY_HELP_PATH, getPath(id));
  }


  /* Action for F1 */
  private class FoneAction extends AbstractAction {

    private final String key;


    FoneAction(String key) {
      this.key = key;
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
      showHelp(key);
    }
  }
  /* End internal Help F1 class */

  /* ActionListener for context help */
  private class ContextAction implements ActionListener {

    /* Get and pass ID on context click */
    @Override
    public void actionPerformed(ActionEvent evt) {
      String id = HelpContext.getContext(evt);
      showHelp(id);
    }

  }
  /* End internal HelpContext class */

  /* ActionListener for an AbstractButton */
  private class ButtonAction implements ActionListener {

    /* Get and pass ID on click */
    @Override
    public void actionPerformed(ActionEvent evt) {
      AbstractButton btn = (AbstractButton) evt.getSource();
      String id = (String) btn.getClientProperty(PROPERTY_HELP_PATH);
      showHelp(id);
    }

  }

  /* Get the mapping for an id */
  private String getPath(String id) {

    String path = map.getProperty(id);

    if (path == null) {
      path = id;
    }

    return path;
  }

  /* Display the selected help on action */
  void showHelp(String key) {

    try {
      callHelp(key);
    } catch (Throwable t) {
      try {
        callHelp(ROOT_ID);
      } catch (Throwable tt) {
        ShowMessage.warning("Unable to open the help page \"%1$s\".", key);
      }
    }

  }

  /* Attempt to display help in the browser */
  private void callHelp(String id) throws Throwable {

    id = root + id;
    id = id.replace(" ", "%20");

    URI uri = new URI(id);
    Desktop desk = Desktop.getDesktop();

    desk.browse(uri);

  }

  /* End */
}
