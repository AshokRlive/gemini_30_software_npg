/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i870util.c
 * description: Generic IEC 60870-5 session
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/i870util.h"
#include "tmwscl/i870/i870sesn.h"
#include "tmwscl/utils/tmwtarg.h"

/* function: i870util_messageSize */
TMWTYPES_USHORT TMWDEFS_GLOBAL i870util_messageSize(
  TMWSESN *pSession, 
  TMWTYPES_BOOL indexed, 
  TMWTYPES_UCHAR numObjects,
  TMWTYPES_USHORT dataSize)
{
  I870SESN *pI870Session = (I870SESN *)pSession;
  
  /* Calculate Application Header Size */
  TMWTYPES_USHORT size = (TMWTYPES_USHORT)(2 + pI870Session->cotSize + pI870Session->asduAddrSize);

  /* If indexed each information object contains an address, if sequential only the
   *  first information object contains an address
   */
  if(indexed)
    size = (TMWTYPES_USHORT)(size + (numObjects * (pI870Session->infoObjAddrSize + dataSize)));
  else
    size = (TMWTYPES_USHORT)(size + (pI870Session->infoObjAddrSize + (numObjects * dataSize)));

  return(size);
}

/* function: i870util_validateMessageSize */
TMWTYPES_BOOL TMWDEFS_GLOBAL i870util_validateMessageSize(
  TMWSESN *pSession,
  I870UTIL_MESSAGE *pMsg,
  TMWTYPES_SHORT dataSize)
{
  /* Variable length messages */
  if(dataSize < 0)
    return(TMWDEFS_TRUE);

  return((TMWTYPES_BOOL)(pMsg->pRxData->msgLength == 
    i870util_messageSize(pSession, pMsg->indexed, pMsg->quantity, dataSize)));
}

/* function: i870util_validateMinLen */ 
TMWTYPES_BOOL TMWDEFS_GLOBAL i870util_validateMinLen(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg, 
  TMWTYPES_USHORT length)
{
  /* Protect against badly formatted message */
  if((pMsg->offset + length) > pMsg->pRxData->msgLength)
  { 
    I870DIAG_ERROR(TMWDEFS_NULL, TMWDEFS_NULL, pSector, I870DIAG_INVALID_SIZE); 
    return(TMWDEFS_FALSE);
  }
  return TMWDEFS_TRUE;
}

/* function: i870util_buildMessageHeader */
void TMWDEFS_GLOBAL i870util_buildMessageHeader(
  TMWSESN *pSession,
  TMWSESN_TX_DATA *pTxData, 
  TMWTYPES_UCHAR typeId, 
  TMWTYPES_UCHAR cot, 
  TMWTYPES_UCHAR originatorAddress, 
  TMWTYPES_USHORT sectorAddress)
{
  I870SESN *pI870Session = (I870SESN *)pSession;

  /* Initialize structure */
  pTxData->msgLength = 0;

  /* Store type id */
  pTxData->pMsgBuf[pTxData->msgLength++] = typeId;

  /* Assume indexed and quantity 1 */
  pTxData->pMsgBuf[pTxData->msgLength++] = 1;

  /* Store first byte of cause of transmission */
  pTxData->pMsgBuf[pTxData->msgLength++] = cot;
  
  /* If COT size is 2 set the 2nd byte to the originator address */
  if(pI870Session->cotSize == 2)
    pTxData->pMsgBuf[pTxData->msgLength++] = originatorAddress;

  /* Store Command Address of ASDU (sector address */
  switch(pI870Session->asduAddrSize)
  {
  case 1:
    pTxData->pMsgBuf[pTxData->msgLength] = (TMWTYPES_UCHAR)sectorAddress;
    pTxData->msgLength = (TMWTYPES_USHORT)(pTxData->msgLength + 1);
    break;

  case 2:
    tmwtarg_store16(&sectorAddress, &pTxData->pMsgBuf[pTxData->msgLength]);
    pTxData->msgLength = (TMWTYPES_USHORT)(pTxData->msgLength + 2);
    break;
  }
}

/* function: i870util_readMessageHeader */
TMWTYPES_BOOL TMWDEFS_GLOBAL i870util_readMessageHeader(
  TMWSESN *pSession,
  I870UTIL_MESSAGE *pMsg, 
  TMWSESN_RX_DATA *pRxData)
{
  I870SESN *pI870Session = (I870SESN *)pSession;

  /* Make sure there is enough data in the received message */
  int minLength = 4;
  if(pI870Session->cotSize == 2)
    minLength++;
  if(pI870Session->asduAddrSize == 2)
    minLength++;
  if(pRxData->msgLength < minLength)
  {
   I870DIAG_ERROR(pSession->pChannel, pSession, TMWDEFS_NULL, I870DIAG_INVALID_SIZE);
   return TMWDEFS_FALSE;
  }

  /* Initialize message structure */
  pMsg->offset = 0;
  pMsg->pRxData = pRxData;

  /* Parse message type id */
  pMsg->typeId = pMsg->pRxData->pMsgBuf[pMsg->offset++];

  /* Parse Indexed/Sequential bit and quantity */
  pMsg->quantity = (TMWTYPES_UCHAR)(pMsg->pRxData->pMsgBuf[pMsg->offset] & 0x7f);
  pMsg->indexed = (TMWTYPES_UCHAR)((pMsg->pRxData->pMsgBuf[pMsg->offset++] & 0x80) == 0);

  /* Parse Cause of Transmission */
  pMsg->cot = pMsg->pRxData->pMsgBuf[pMsg->offset++];

  /* 2nd byte of COT (if enabled) is the originator address */
  pMsg->origAddress = 0;
  if(pI870Session->cotSize == 2)
    pMsg->origAddress = pMsg->pRxData->pMsgBuf[pMsg->offset++];

  /* Parse Command Address of ASDU (sector address) */
  switch(pI870Session->asduAddrSize)
  {
  case 1:
    pMsg->asduAddress = pMsg->pRxData->pMsgBuf[pMsg->offset];
    pMsg->offset = (TMWTYPES_USHORT)(pMsg->offset + 1);
    break;

  case 2:
    tmwtarg_get16(&pMsg->pRxData->pMsgBuf[pMsg->offset], &pMsg->asduAddress);
    pMsg->offset = (TMWTYPES_USHORT)(pMsg->offset + 2);
    break;
  }

  /* Initialize next IOA to an invalid address */
  pMsg->nextIOA = 0;

  return TMWDEFS_TRUE;
}

/* function: i870util_storeInfoObjAddr */
void TMWDEFS_GLOBAL i870util_storeInfoObjAddr(
  TMWSESN *pSession,
  TMWSESN_TX_DATA *pTxData, 
  TMWTYPES_ULONG ioa)
{
  I870SESN *pI870Session = (I870SESN *)pSession;
  TMWTYPES_USHORT tmpAddress;

  switch(pI870Session->infoObjAddrSize)
  {
  case 1:
    pTxData->pMsgBuf[pTxData->msgLength++] = (TMWTYPES_UCHAR)ioa;
    break;

  case 2:
    tmpAddress = (TMWTYPES_USHORT)ioa;
    tmwtarg_store16(&tmpAddress, &pTxData->pMsgBuf[pTxData->msgLength]);
    pTxData->msgLength += 2;
    break;

  case 3:
    tmwtarg_store24(&ioa, &pTxData->pMsgBuf[pTxData->msgLength]);
    pTxData->msgLength += 3;
    break;
  }
}

/* function: i870util_readInfoObjAddr */
void TMWDEFS_GLOBAL i870util_readInfoObjAddr(
  TMWSESN *pSession,
  I870UTIL_MESSAGE *pMsg, 
  TMWTYPES_ULONG *pIOA)
{
  I870SESN *pI870Session = (I870SESN *)pSession;
  TMWTYPES_USHORT tmpAddress;

  /* If indexed addressing, or this is the first information object,
   * read information object address from message, else increment
   * previous information object address.
   */
  if(pMsg->indexed || (pMsg->nextIOA == 0))
  {
    switch(pI870Session->infoObjAddrSize)
    {
    case 1:
      pMsg->nextIOA = pMsg->pRxData->pMsgBuf[pMsg->offset++];
      break;

    case 2:
      tmwtarg_get16(&pMsg->pRxData->pMsgBuf[pMsg->offset], &tmpAddress);
      pMsg->nextIOA = tmpAddress;
      pMsg->offset += 2;
      break;

    case 3:
      tmwtarg_get24(&pMsg->pRxData->pMsgBuf[pMsg->offset], &pMsg->nextIOA);
      pMsg->offset += 3;
      break;
    }
  }
  else
  {
    pMsg->nextIOA += 1;
  }

  /* Set return value */
  *pIOA = pMsg->nextIOA;
}

/* function: i870util_storeHourDateInMessage */
void TMWDEFS_GLOBAL i870util_storeHourDateInMessage(
  TMWTYPES_UCHAR *pBuf,
  TMWDTIME *pDateTime,
  TMWTYPES_BOOL useDayOfWeek)
{
  TMWTYPES_UCHAR tempVal;

  /* Summertime and hour */
  tempVal = pDateTime->hour;
  if(pDateTime->dstInEffect)
    tempVal |= 0x80;
  *pBuf++ = tempVal;

  /* Day of month and day of week (note: Edition 1 IEC 60870-5-101 clause 7.2.6.18 said
   * day of week must be set to 0, Edition 2 says it is now optional) 
   */
  tempVal = pDateTime->dayOfMonth;
  if(useDayOfWeek)
    tempVal |= (pDateTime->dayOfWeek<<5);
  *pBuf++ = tempVal;

  /* Month */
  *pBuf++ = pDateTime->month;

  /* Year */
  *pBuf++ = (TMWTYPES_UCHAR)(pDateTime->year % 100);
}

/* function: i870util_writeHourDateInMessage */
void TMWDEFS_GLOBAL i870util_writeHourDateInMessage(
  TMWSESN_TX_DATA *pTxData,
  TMWDTIME *pDateTime)
{
  I870SESN *pI870Session = (I870SESN *)pTxData->pSession;
  i870util_storeHourDateInMessage(&pTxData->pMsgBuf[pTxData->msgLength], pDateTime, pI870Session->useDayOfWeek);
  pTxData->msgLength += 4;
}

/* function: i870util_store24BitTime */
void TMWDEFS_GLOBAL i870util_store24BitTime(
  TMWTYPES_UCHAR *pBuf, 
  TMWDTIME *pDateTime,
  TMWTYPES_BOOL res1BitSupport)
{
  /* Store seconds and milliseconds */
  tmwtarg_store16(&pDateTime->mSecsAndSecs, pBuf);
  pBuf += 2;

  /* Store minutes and invalid */
  *pBuf = pDateTime->minutes;

  /* If setting of RES1 bit is supported and this is substituted time
   * set the bit as specified in 101 edition 2 
   */
  if(res1BitSupport && !pDateTime->genuineTime) 
  {
    *pBuf |= 0x40;
  }

  if(pDateTime->invalid) 
  {
    *pBuf |= 0x80;
  }
}

/* function: i870util_write24BitTime */
void TMWDEFS_GLOBAL i870util_write24BitTime(
  TMWSESN_TX_DATA *pTxData, 
  TMWDTIME *pDateTime,
  TMWTYPES_BOOL res1BitSupport)
{
  i870util_store24BitTime(&pTxData->pMsgBuf[pTxData->msgLength], pDateTime, res1BitSupport);
  pTxData->msgLength += 3;
}

/* function: i870util_store56BitTime */
void TMWDEFS_GLOBAL i870util_store56BitTime(
  TMWTYPES_UCHAR *pBuf,
  TMWDTIME *pDateTime,
  TMWTYPES_BOOL res1BitSupport,
  TMWTYPES_BOOL useDayOfWeek)
{
  i870util_store24BitTime(pBuf, pDateTime, res1BitSupport);
  i870util_storeHourDateInMessage(pBuf+3, pDateTime, useDayOfWeek);
}

/* function: i870util_write56BitTime */
void TMWDEFS_GLOBAL i870util_write56BitTime(
  TMWSESN_TX_DATA *pTxData,
  TMWDTIME *pDateTime,
  TMWTYPES_BOOL res1BitSupport)
{
  i870util_write24BitTime(pTxData, pDateTime, res1BitSupport);
  i870util_writeHourDateInMessage(pTxData, pDateTime);
}

/* function: i870util_read24BitTime */
void TMWDEFS_GLOBAL i870util_read24BitTime(
  I870UTIL_MESSAGE *pMsg, 
  TMWDTIME *pDateTime)
{
  /* Milliseconds and seconds */
  tmwtarg_get16(&pMsg->pRxData->pMsgBuf[pMsg->offset], &pDateTime->mSecsAndSecs);
  pMsg->offset += 2;

  /* Invalid, genuineTime and minutes */
  pDateTime->invalid = (TMWTYPES_BOOL)((pMsg->pRxData->pMsgBuf[pMsg->offset] & 0x80) != 0);
  pDateTime->genuineTime = (TMWTYPES_BOOL)((pMsg->pRxData->pMsgBuf[pMsg->offset] & 0x40) == 0);
  pDateTime->minutes = (TMWTYPES_UCHAR)(pMsg->pRxData->pMsgBuf[pMsg->offset++] & 0x3f);
}

/* function: i870util_read56BitTime */
void TMWDEFS_GLOBAL i870util_read56BitTime(
  I870UTIL_MESSAGE *pMsg, 
  TMWDTIME *pDateTime)
{
  i870util_read24BitTime(pMsg, pDateTime);

  /* Summertime and hour */
  pDateTime->dstInEffect = (TMWTYPES_BOOL)((pMsg->pRxData->pMsgBuf[pMsg->offset] & 0x80) != 0);
  pDateTime->hour = (TMWTYPES_UCHAR)(pMsg->pRxData->pMsgBuf[pMsg->offset++] & 0x1f);

  /* Day of Month */
  pDateTime->dayOfMonth = (TMWTYPES_UCHAR)(pMsg->pRxData->pMsgBuf[pMsg->offset] & 0x1f);

  /* Day of week */
  pDateTime->dayOfWeek = (TMWTYPES_UCHAR)((pMsg->pRxData->pMsgBuf[pMsg->offset++] & 0xe0) >> 5);

  /* Month */
  pDateTime->month = (TMWTYPES_UCHAR)(pMsg->pRxData->pMsgBuf[pMsg->offset++] & 0x0f);

  /* Year */
  pDateTime->year = (TMWTYPES_USHORT)((pMsg->pRxData->pMsgBuf[pMsg->offset++] & 0x7f) + 2000);

  /* It is unlikely 70 meaning 2070 was sent, more likely it was 1970 */
  if(pDateTime->year >= 2070)
    pDateTime->year -= 100;

  pDateTime->qualifier = TMWDTIME_ABSOLUTE;
}


/* The following functions are just used by 103 */

/* function: i870util_writeInfoObjectId */
void TMWDEFS_GLOBAL i870util_writeInfoObjectId(
  TMWSESN_TX_DATA *pTxData, 
  TMWTYPES_UCHAR functionType, 
  TMWTYPES_UCHAR infoNumber)
{
  pTxData->pMsgBuf[pTxData->msgLength++] = functionType;
  pTxData->pMsgBuf[pTxData->msgLength++] = infoNumber;
}

/* function: i870util_readInfoObjectId */
void TMWDEFS_GLOBAL i870util_readInfoObjectId(
  I870UTIL_MESSAGE *pMsg, 
  TMWTYPES_UCHAR *pFunctionType, 
  TMWTYPES_UCHAR *pInfoNumber)
{
  *pFunctionType = (TMWTYPES_UCHAR)(pMsg->pRxData->pMsgBuf[pMsg->offset++]);
  *pInfoNumber   = (TMWTYPES_UCHAR)(pMsg->pRxData->pMsgBuf[pMsg->offset++]);
}
 
/* function: i870util_store32BitTime */
void TMWDEFS_GLOBAL i870util_store32BitTime(
  TMWTYPES_UCHAR *pBuf, 
  TMWDTIME *pDateTime)
{
  i870util_store24BitTime(pBuf, pDateTime, TMWDEFS_FALSE);
  pBuf += 3;

  /* Summertime and hour */
  *pBuf = pDateTime->hour;
  if(pDateTime->dstInEffect) *pBuf |= 0x80;
}

/* function: i870util_write32BitTime */
void TMWDEFS_GLOBAL i870util_write32BitTime(
  TMWSESN_TX_DATA *pTxData, 
  TMWDTIME *pDateTime)
{
  i870util_write24BitTime(pTxData, pDateTime, TMWDEFS_FALSE);

  /* Summertime and hour */
  pTxData->pMsgBuf[pTxData->msgLength] = pDateTime->hour;
  if(pDateTime->dstInEffect) pTxData->pMsgBuf[pTxData->msgLength] |= 0x80;

  pTxData->msgLength++;
}

/* function: i870util_read32BitTime */
void TMWDEFS_GLOBAL i870util_read32BitTime(
  I870UTIL_MESSAGE *pMsg, 
  TMWDTIME *pDateTime)
{
  i870util_read24BitTime(pMsg, pDateTime);

  pDateTime->dstInEffect = (TMWTYPES_UCHAR)((pMsg->pRxData->pMsgBuf[pMsg->offset] & 0x80) != 0);
  
  pDateTime->hour = (TMWTYPES_UCHAR)(pMsg->pRxData->pMsgBuf[pMsg->offset++] & 0x1f);
}
 


