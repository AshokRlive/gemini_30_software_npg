<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<artifactId>xslt.example</artifactId>
	<groupId>com.lucy</groupId>
	<version>0.0.1-SNAPSHOT</version>
	<packaging>pom</packaging>
	<description>this is an example Maven project to transform XML</description>
    <properties>
        <g3common.dir>${basedir}/../../../</g3common.dir>
        <output.dir>${basedir}/output/</output.dir>
    </properties>
    
	<build>
	<plugins>
	
		<!-- Check XML folder exist -->
		<plugin>
	        <groupId>org.apache.maven.plugins</groupId>
	        <artifactId>maven-enforcer-plugin</artifactId>
	        <version>1.3.1</version>
	        <executions>
	          <execution>
	            <phase>validate</phase>
	            <id>enforce-common-exist</id>
	            <goals>
	              <goal>enforce</goal>
	            </goals>
	            <configuration>
	              <rules>
	                <requireFilesExist>
	                  <files>
	                   <file>${g3common.dir}/xml</file>
	                  </files>
	                </requireFilesExist>
	              </rules>
	              <fail>true</fail>
	            </configuration>
	          </execution>
	        </executions>
	      </plugin>

		
			<!-- Transform XML using XSLT -->
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>xml-maven-plugin</artifactId>
				<version>1.0</version>
				<executions>
				 	<execution>
				 		<id>validateXML</id>
				 		<phase>validate</phase>
			            <goals>
			              <goal>validate</goal>
			            </goals>
		          	</execution>
					<execution>
						<id>transformXML</id>
						<phase>generate-sources</phase>
						<goals>
							<goal>transform</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<forceCreation>true</forceCreation>
					<validationSets>
						<validationSet>
							<dir>${g3common.dir}/xml/</dir>
							  <excludes>
				              	<exclude>**/*.xsd</exclude> <!-- skip schema file in xml folder -->
				              	<exclude>NVRAMDef/*.xml</exclude> <!-- skip NVRAMDef folder -->
				              	<exclude>SlaveBoardIOMap/*.xml</exclude> <!-- skip NVRAMDef folder -->
				              </excludes>
				               <systemId>${g3common.dir}/xml/common.xsd</systemId>
						</validationSet>
					</validationSets>
					          
					<transformationSets>
					
						<!-- Transform XML to Java-->
						<transformationSet>
							<dir>${g3common.dir}/xml/</dir>
							<includes>
								<include>**/*.xml</include>
							</includes>
							<excludes>
								<!-- <exclude>LogLevelDef_Monitor.xml</exclude> -->
								<!-- <exclude>LogLevelDef_Updater.xml</exclude> -->
							</excludes>
							<outputDir>${output.dir}java</outputDir>
							<stylesheet>${g3common.dir}/xslt/xml2java.xsl</stylesheet>
							<fileMappers>
								<fileMapper
									implementation="org.codehaus.plexus.components.io.filemappers.FileExtensionMapper">
									<targetExtension>.java</targetExtension>
								</fileMapper>
							</fileMappers>
							<parameters>
								<!--Customise "package" in generated java source -->
								<parameter>
									<name>package</name>
									<value>com.generated;</value>
								</parameter>
							</parameters>
						</transformationSet>

						<!-- Transform XML to C-->
						<transformationSet>
							<dir>${g3common.dir}/xml/</dir>
							<includes>
								<include>**/*.xml</include>
							</includes>
							<outputDir>${output.dir}C</outputDir>
							<stylesheet>${g3common.dir}/xslt/xml2C_header.xsl</stylesheet>
							<fileMappers>
								<fileMapper
									implementation="org.codehaus.plexus.components.io.filemappers.FileExtensionMapper">
									<targetExtension>.h</targetExtension>
								</fileMapper>
							</fileMappers>
						</transformationSet>
						
						<!-- Transform XML to Schema-->
						<transformationSet>
							<dir>${g3common.dir}/xml/</dir>
							<includes>
							     <include>DNP3Enum.xml</include>
                                <include>IEC104Enum.xml</include>
                                <include>MCMConfigEnum.xml</include>
                                <include>ModBusEnum.xml</include>
                                <include>ModuleProtocolEnum.xml</include>
                                <include>ProtocolStackCommonEnum.xml</include>
							</includes>
							<outputDir>${output.dir}Schema</outputDir>
							<stylesheet>${g3common.dir}/xslt/xml2xsd.xsl</stylesheet>
							<fileMappers>
								<fileMapper
									implementation="org.codehaus.plexus.components.io.filemappers.FileExtensionMapper">
									<targetExtension>.xsd</targetExtension>
								</fileMapper>
							</fileMappers>
						</transformationSet>
						
						<!-- Transform XML to boardArray-->
						<transformationSet>
							<dir>${g3common.dir}/xml/</dir>
							<includes>
								<include>SlaveBoardIOMap/*.xml</include>
							</includes>
							<outputDir>${output.dir}BoardArray</outputDir>
							<stylesheet>${g3common.dir}/xslt/xml2C_boardArray.xsl</stylesheet>
							<fileMappers>
								<fileMapper
									implementation="org.codehaus.plexus.components.io.filemappers.FileExtensionMapper">
									<targetExtension>.h</targetExtension>
								</fileMapper>
							</fileMappers>
						</transformationSet>
						
						<!-- Transform XML to MCMVersion-->
						<transformationSet>
							<dir>${g3common.dir}/xml/</dir>
							<includes>
								<include>versions.xml</include>
							</includes>
							<outputDir>${output.dir}Version</outputDir>
							<stylesheet>${g3common.dir}/xslt/MCMVersion.xsl</stylesheet>
							<fileMappers>
								<fileMapper
									implementation="org.codehaus.plexus.components.io.filemappers.FileExtensionMapper">
									<targetExtension>.properties</targetExtension>
								</fileMapper>
							</fileMappers>
						</transformationSet>

					</transformationSets>
				</configuration>
			</plugin>

			
			<!-- Clean generated files -->
			<plugin>
				<artifactId>maven-clean-plugin</artifactId>
				<version>2.5</version>
				<configuration>
					<filesets>
						<fileset>
							<directory>${output.dir}</directory>
							<includes>
							<include>**/*</include>
							</includes>
						</fileset>
					</filesets>
				</configuration>
			</plugin>

		</plugins>
	</build>

</project>
