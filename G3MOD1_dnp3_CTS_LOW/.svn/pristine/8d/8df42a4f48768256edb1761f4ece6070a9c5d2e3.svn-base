/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14mctna.c
 * description:  Slave Configuration Table functionality.
 *  Private ASDU 245 not in 101 standard
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14mctna.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14mem.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/i870util.h"
#include "tmwscl/utils/tmwmem.h"
#include "tmwscl/utils/tmwtarg.h"

#if S14DATA_SUPPORT_MCTNA

/* function: s14mctna_readIntoResponse */
S14DBAS_GROUP_STATUS TMWDEFS_CALLBACK s14mctna_readIntoResponse(
  TMWSESN_TX_DATA *pTxData, 
  TMWSCTR *pSector, 
  TMWTYPES_UCHAR cot, 
  TMWDEFS_GROUP_MASK groupMask, 
  TMWTYPES_ULONG ioa,
  TMWTYPES_USHORT *pPointIndex)
{
  void *pPoint;
  TMWTYPES_UCHAR quantity = 0;
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  TMWTARG_UNUSED_PARAM(groupMask);
  TMWTARG_UNUSED_PARAM(pPointIndex);

  pPoint = s14data_mctnaLookupPoint(p14Sector->i870.pDbHandle, ioa);
  if(pPoint == TMWDEFS_NULL)
    return(S14DBAS_GROUP_STATUS_NO_DATA);

  i870util_buildMessageHeader(pSector->pSession,
      pTxData, I14DEF_TYPE_MCTNA1, cot, 0, p14Sector->i870.asduAddress);
   
  i870util_storeInfoObjAddr(pSector->pSession, pTxData, ioa);

  quantity = s14data_mctnaGetValues(pPoint, 100, &pTxData->pMsgBuf[pTxData->msgLength]);
  pTxData->msgLength = (TMWTYPES_USHORT)(pTxData->msgLength + quantity);

  /* Set VSQ to number of CTI elements */
  pTxData->pMsgBuf[1] = 0x80 + quantity;

  return(S14DBAS_GROUP_STATUS_COMPLETE);
}

#endif /* S14DATA_SUPPORT_MCTNA */
