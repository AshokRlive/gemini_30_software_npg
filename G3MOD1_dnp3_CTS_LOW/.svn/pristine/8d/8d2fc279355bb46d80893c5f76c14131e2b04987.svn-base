/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:DialerModem.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   21 Aug 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef DIALERMODEM_H_
#define DIALERMODEM_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <string>
#include <iostream>
#include <string.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IIOModuleManager.h"
#include "CommsDevice.h"
#include "Port.h"
#include "ModemCommon.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class IDialupModemSM;       //Forward declaration
class CommsTrafficSniffer;  //Forward declaration

/**
 * Implementation of PSTN/PAKNET modem.
 */
class DialupModem : public CommsDevice
{
public:
    struct DialupModemConfig : public DialupModemCfg
    {
        DialupModemConfig(Port& x) : DialupModemCfg(x)
        {};
    };
public:
    DialupModem(DialupModemConfig config);
    virtual ~DialupModem();

    const DialupModemConfig& getConfig()
    {
        return m_config;
    }

    CommsTrafficSniffer& getSniffer()
    {
        return *mp_sniffer;
    }

    /************************************/
    /*	CommsDevice API                 */
    /************************************/
    virtual void init();

    virtual bool connect();

    virtual void disconnect();

    virtual void sniffRcvData(lu_int8_t* dataPtr, lu_uint32_t len);

    void setDTR(bool activate);

    virtual void tickEvent(lu_uint32_t ticks);

    virtual void checkTimer();

    virtual void setPowerSupplyChan(IChannel* chan) { mp_powerSupplyChan = chan; }
    virtual IChannel* getPowerSupplyChan()          { return mp_powerSupplyChan; }

private:
    const DialupModemConfig     m_config;
    CommsTrafficSniffer*        mp_sniffer;
    IDialupModemSM*             mp_stateMachine;

    IChannel*                   mp_powerSupplyChan;
};

#endif /* DIALERMODEM_H_ */

/*
 *********************** End of file ******************************************
 */
