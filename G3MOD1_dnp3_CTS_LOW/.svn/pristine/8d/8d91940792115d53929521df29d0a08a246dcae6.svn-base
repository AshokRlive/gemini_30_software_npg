<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference PUBLIC "-//OASIS//DTD DITA Reference//EN" "reference.dtd">
<reference id="reference_fh5_pxy_1q">
  <title>Edit a counter virtual point</title>
  <refbody>
    <section><menucascade>
        <uicontrol>Configuration</uicontrol>
        <uicontrol>Virtual Points</uicontrol>
        <uicontrol>Counter</uicontrol>
        <uicontrol>Edit</uicontrol>
      </menucascade></section>
    <section>A virtual point <uicontrol>Counter</uicontrol> counts changes in a source during a
      period of time, offering positive integer values only.<p>A virtual point counter can count
        from a <i>binary input virtual point</i> (a standard virtual point counter), or it can be
        automatically generated from a <i>control logic indication point</i> (a pseudo point
        counter).</p><note type="tip">You can create several counter virtual points from the same
        binary input.</note><p>In the case of a pseudo point counter, it is not possible to modify
        the <uicontrol>Source</uicontrol> and <uicontrol>Description</uicontrol>.</p></section>
    <section
      conref="../components/Basic-Virtual-Point-Settings.dita#ReusableComponent-qfq_m5s_cr/section-rfq_m5s_cr"/>
    <section>
      <title id="title-d3l_2rf_dq">Input Mode</title>
      <p>Since a counter point counts events happening in an associated binary digital point, in
        this section you define what is considered a countable event.</p>
      <table id="table-cdj_dgt_1q">
        <tgroup cols="2">
          <colspec colnum="1" colname="col1" colwidth="*"/>
          <colspec colnum="2" colname="col2" colwidth="*"/>
          <thead>
            <row>
              <entry>Option</entry>
              <entry>Description</entry>
            </row>
          </thead>
          <tbody>
            <row>
              <entry><uicontrol>Edge-driven</uicontrol></entry>
              <entry>Sets to count when the binary input changes its value.<p>It can be configured
                  to count when the signal rises (0 to 1), falls (1 to 0), or on any signal
                  change.</p></entry>
            </row>
            <row>
              <entry><uicontrol>Pulse-driven</uicontrol></entry>
              <entry>Sets to count when the binary input changes and completes a valid pulse.
                  <note>A valid pulse is a positive pulse longer than the configured pulse
                  width.</note></entry>
            </row>
          </tbody>
        </tgroup>
      </table>
    </section>
    <section>
      <title id="title-uj1_frf_dq">Parameters</title>
      <table id="table-hn1_frf_dq">
        <tgroup cols="2">
          <colspec colnum="1" colname="col1" colwidth="*"/>
          <colspec colnum="2" colname="col2" colwidth="*"/>
          <thead>
            <row>
              <entry>Option</entry>
              <entry>Description</entry>
            </row>
          </thead>
          <tbody>
            <row>
              <entry><uicontrol>Reset Value</uicontrol></entry>
              <entry>The starting value of the counter.<p>When the counter rolls over, it goes back
                  to this value. It is similar to an offset.</p></entry>
            </row>
            <row>
              <entry><uicontrol>Auto Reset</uicontrol></entry>
              <entry>Configures the counter in a way that when its input point goes offline, the
                counter is set back to the <uicontrol>Reset Value</uicontrol> as soon as the point
                comes back online.</entry>
            </row>
            <row>
              <entry><uicontrol>Rollover Value</uicontrol></entry>
              <entry>The maximum value allowed for the counter.<p>When this value is reached, the
                  count value will be set back to the <uicontrol>Reset
                Value</uicontrol>.</p></entry>
            </row>
            <row>
              <entry><uicontrol>Event Step</uicontrol></entry>
              <entry>Specify how many counts should be made before the value is reported.<p>For
                  example, a value of 10 will not report any value unless the internal count reaches
                  10, 20 and so on.</p></entry>
            </row>
            <row props="exclude">
              <entry><uicontrol>Persistent</uicontrol></entry>
              <entry>Select the check box to allow the counter to keep its value even if the RTU is
                shut down.<note>The number of persistent counters is limited, so when the limit is
                  reached, the Configuration Tool does not allow you to configure any more counters
                  as persistent.</note></entry>
            </row>
          </tbody>
        </tgroup>
      </table>
    </section>
    <section>
      <title id="title-l4z_hrf_dq">Freezing</title>
      <table id="table-grz_hrf_dq">
        <tgroup cols="2">
          <colspec colnum="1" colname="col1" colwidth="*"/>
          <colspec colnum="2" colname="col2" colwidth="*"/>
          <thead>
            <row>
              <entry>Option</entry>
              <entry>Description</entry>
            </row>
          </thead>
          <tbody>
            <row>
              <entry><b>Freezing</b></entry>
              <entry>Select the check box to set the counter to freeze, so that it holds a copy of
                the counting value at the specified time and meanwhile keeps counting in the
                background. <p>The value of the counter then reflects only the "frozen" value until
                  another freeze event occurs.</p></entry>
            </row>
            <row>
              <entry><uicontrol>Freezing Time</uicontrol></entry>
              <entry>
                <p><uicontrol>Freezing Time</uicontrol> – Specifies the period during which the
                  counter counts internally. After this period of time, it takes a snapshot of the
                  current value and offers it as a public value.</p>
                <p><uicontrol>Interval</uicontrol> – Counts exactly the minutes specified in the
                  configuration, independently of the RTU's real-time clock synchronisation.</p>
                <p><uicontrol>From O’Clock</uicontrol> – Is specified as a time mark in the
                  real-time clock, counting minutes from the o'clock time, and being affected by any
                  time synchronisation.</p>
                <p>Freezing occurs repeatedly within a time interval. For example, a 20-minute
                  interval will freeze the counter at 12:00, 12:20, 12:40, 13:00, and so on.</p>
              </entry>
            </row>
            <row>
              <entry/>
              <entry>
                <p><uicontrol>Freezing Option</uicontrol> – Determines the behaviour of the counter
                  after freezing a counter value:</p>
                <ul id="ul-d5z_hrf_dq">
                  <li><uicontrol>Freeze Only</uicontrol> – The counter simply freezes a value and
                    keep counting in the background from the last value.</li>
                  <li><uicontrol>Freeze then reset</uicontrol> – After freezing the value, the
                    background count goes back to the <uicontrol>Reset Value</uicontrol>.</li>
                </ul>
              </entry>
            </row>
          </tbody>
        </tgroup>
      </table>
    </section>
  </refbody>
</reference>
