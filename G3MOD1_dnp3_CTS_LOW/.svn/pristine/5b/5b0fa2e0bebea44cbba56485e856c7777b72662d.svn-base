/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN Fan test custom channel implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   27/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "FanCANChannel.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

FanCANChannel::FanCANChannel(lu_uint16_t ID, CANIOModule *board) :
                                OutputCANChannel(CHANNEL_TYPE_FAN, ID, board)
{}

IOM_ERROR FanCANChannel::fanTest(const lu_uint8_t duration)
{
    IIOModule::sendReplyStr message;
    PayloadRAW payload;
    FanTestStr fanTestCmd;

    if(checkActive("operate") != IOM_ERROR_NONE)
    {
        return IOM_ERROR_OFFLINE;
    }

    /* Prepare command */
    fanTestCmd.channel      = this->getID().id;
    fanTestCmd.testDuration = duration;

    payload.payloadPtr = reinterpret_cast<lu_uint8_t*>(&fanTestCmd);
    payload.payloadLen = sizeof(fanTestCmd);

    message.messageType  = MODULE_MSG_TYPE_CMD;
    message.messageID    = MODULE_MSG_ID_CMD_FAN_CH_TEST_C;
    message.messagePtr   = &payload;
    message.rMessageType = MODULE_MSG_TYPE_CMD;
    message.rMessagePtr  = NULL;
    message.rMessageID   = MODULE_MSG_ID_CMD_FAN_CH_TEST_R;
    message.timeout      = CANChannel::CAN_MSG_TIMEOUT;

    log.info("%s %s/%s send fan test command(duration %i)",
                __AT__, moduleID.toString().c_str(), this->getName(), duration
              );

    return sendCANSendReply(message);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
