/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.modules;

import static com.lucy.g3.rtu.comms.protocol.ISerializable.Buffer.createByteBuffer;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.bean.Bean;
import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.common.version.FeatureVersion;
import com.lucy.g3.common.version.ReleaseType;
import com.lucy.g3.common.version.SoftwareVersion;
import com.lucy.g3.common.version.SystemAPIVersion;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.xml.gen.common.MainAppEnum.FSM_STATE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_BOARD_ERROR;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_BOARD_STATUS;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;
import com.lucy.g3.xml.gen.common.SysAlarmCodeEnum.SYS_ALARM_SEVERITY;
import com.lucy.g3.xml.gen.common.versions.VERSION_TYPE;


/**
 * JavaBeans Class for storing a module's information of RTU, including firmware
 * version, serial number, status, etc. Those information should be received
 * from RTU and decoded in G3 protocol.
 */
public class ModuleInfo extends Bean {

  private static Logger log = Logger.getLogger(ModuleInfo.class);
  /* Bit positions for module internal error */
  private static final int ERROR_BIT_TSYNCH = MODULE_BOARD_ERROR.MODULE_BOARD_ERROR_TSYNCH.getValue();
  private static final int ERROR_BIT_CRITIAL = MODULE_BOARD_ERROR.MODULE_BOARD_ERROR_ALARM_CRITICAL.getValue();
  private static final int ERROR_BIT_INFO = MODULE_BOARD_ERROR.MODULE_BOARD_ERROR_ALARM_INFO.getValue();
  private static final int ERROR_BIT_WARN = MODULE_BOARD_ERROR.MODULE_BOARD_ERROR_ALARM_WARNING.getValue();
  private static final int ERROR_BIT_ERROR = MODULE_BOARD_ERROR.MODULE_BOARD_ERROR_ALARM_ERROR.getValue();

  // ====== State Texts ======
  private static final String TEXT_NO_ERROR = "None";
  private static final String TEXT_OK = "Active";
  private static final String TEXT_NOT_OK = "Inactive ";
  private static final String TEXT_NOT_CONFIGURED = "[Not Configured] ";
  private static final String TEXT_NOT_PRESENT = "[Not Present] ";
  private static final String TEXT_NOT_REGISTERED = "[Not Registered] ";
  private static final String TEXT_POWER_SAVING_MODE = "[Power Saving Mode] ";

  // ====== Property names ======
  public static final String PROPERTY_MODULE_TYPE = "moduleType";
  public static final String PROPERTY_MODULE_ID = "moduleID";
  public static final String PROPERTY_VERSION = "version";
  public static final String PROPERTY_FEATUREVERSION = "featureVersion";
  public static final String PROPERTY_SYSTEMAPI = "systemAPI";
  public static final String PROPERTY_BOOTAPI = "bootAPI";
  public static final String PROPERTY_BOOTVERSION = "bootVersion";
  public static final String PROPERTY_SERIALNO = "serialNo";
  public static final String PROPERTY_SUPPLIER_ID = "supplierID";
  public static final String PROPERTY_INTERNALSTATE = "internalState";
  public static final String PROPERTY_FSM_STATE = "fsmState";
  public static final String PROPERTY_STATE_OK = "ok";
  public static final String PROPERTY_STATE_CONFIGURED = "configured";
  public static final String PROPERTY_STATE_PRESENT = "present";
  public static final String PROPERTY_STATE_DETECTED = "detected";
  public static final String PROPERTY_STATE_REGISTERED = "registered";
  public static final String PROPERTY_STATE_DISABLED = "disabled";
  public static final String PROPERTY_STATE_TIMESYNC = "timeSync";
  public static final String PROPERTY_OUT_OF_SYNCH = "outOfSynch";

  // ====== Module Attributes ======
  private final MODULE_ID moduleID;
  private final MODULE moduleType;

  // ====== Module Info ======
  private SoftwareVersion version;
  private FeatureVersion featureVersion;
  private SystemAPIVersion systemAPI;
  private SystemAPIVersion bootAPI;
  private SoftwareVersion bootVersion;
  private long serialNo;
  private int  supplierID;

  // ====== Module Status flag ======
  private boolean ok;
  private boolean configured;
  private boolean present;
  private boolean detected;
  private boolean registered;
  private boolean disabled;
  private boolean timeSync;
  private MODULE_BOARD_STATUS internalState;
  private FSM_STATE fsmState;

  // ====== Module Alarm Masks ======
  /** Slave modules internal error codes (bitmap). */
  private byte errorMask = 0;

  /** A list of Error Enums decoded from current {@link errorMask}. */
  private ArrayList<SYS_ALARM_SEVERITY> errorFlags = new ArrayList<SYS_ALARM_SEVERITY>();

  private String errorText = TEXT_NO_ERROR;

  // ====== Other Fields ======
  /**
   * Boolean property indicates if this module info has been out of synch. If it
   * is set to true, it means all properties of this module info is not
   * synchronised with RTU.
   */
  private boolean outOfSynch = false;

  private int hashCode = -1;

  private final ModuleDetail detail;


  // Constructor
  public ModuleInfo(MODULE type, MODULE_ID id) {
    moduleType = Preconditions.checkNotNull(type, "type must not be null");
    moduleID = Preconditions.checkNotNull(id, "id must not be null");

    detail = new ModuleDetail(moduleType, moduleID);
    updateErrors(errorMask);
  }

  public ModuleDetail getDetail() {
    return detail;
  }

  public MODULE getModuleType() {
    return moduleType;
  }

  public MODULE_ID getModuleID() {
    return moduleID;
  }

  public boolean isMCM() {
    return moduleType == MODULE.MODULE_MCM;
  }

  @Override
  public String toString() {
    return String.format("%s %s", moduleType.getDescription(), moduleID.getDescription());
  }

  // ----------------------[ RealTime Property Accessor
  // ]--------------------------

  public boolean isOutOfSynch() {
    return outOfSynch;
  }

  public void setOutOfSynch(boolean outOfSynch) {
    Object oldValue = isOutOfSynch();
    this.outOfSynch = outOfSynch;
    firePropertyChange(PROPERTY_OUT_OF_SYNCH, oldValue, outOfSynch);
  }

  public SoftwareVersion getVersion() {
    return version;
  }

  private void setVersion(SoftwareVersion version) {
    Object oldValue = getVersion();
    this.version = version;
    firePropertyChange(PROPERTY_VERSION, oldValue, version);
  }

  public FeatureVersion getFeatureVersion() {
    return featureVersion;
  }

  private void setFeatureVersion(FeatureVersion featureVersion) {
    Object oldValue = getFeatureVersion();
    this.featureVersion = featureVersion;
    firePropertyChange(PROPERTY_FEATUREVERSION, oldValue, featureVersion);
  }

  public SystemAPIVersion getSystemAPI() {
    return systemAPI;
  }

  private void setSystemAPI(SystemAPIVersion systemAPI) {
    Object oldValue = getSystemAPI();
    this.systemAPI = systemAPI;
    firePropertyChange(PROPERTY_SYSTEMAPI, oldValue, systemAPI);
  }

  public SystemAPIVersion getBootAPI() {
    return bootAPI;
  }

  private void setBootAPI(SystemAPIVersion bootAPI) {
    Object oldValue = getBootAPI();
    this.bootAPI = bootAPI;
    firePropertyChange(PROPERTY_BOOTAPI, oldValue, bootAPI);
  }

  public SoftwareVersion getBootVersion() {
    return bootVersion;
  }

  private void setBootVersion(SoftwareVersion bootVer) {
    Object oldValue = getBootVersion();
    bootVersion = bootVer;
    firePropertyChange(PROPERTY_BOOTVERSION, oldValue, bootVer);
  }

  public long getSerialNo() {
    return serialNo;
  }

  private void setSerialNo(long serialNo) {
    Object oldValue = getSerialNo();
    this.serialNo = serialNo;
    firePropertyChange(PROPERTY_SERIALNO, oldValue, serialNo);
  }

  public int getSupplierID() {
    return supplierID;
  }

  private void setSupplierID(int supplierID) {
    Object oldValue = this.supplierID;
    this.supplierID = supplierID;
    firePropertyChange(PROPERTY_SUPPLIER_ID, oldValue, supplierID);
  }

  public MODULE_BOARD_STATUS getInternalState() {
    return internalState;
  }

  private void setInternstate(MODULE_BOARD_STATUS internalState) {
    Object oldValue = getInternalState();
    this.internalState = internalState;
    firePropertyChange(PROPERTY_INTERNALSTATE, oldValue, this.internalState);
  }

  public FSM_STATE getFsmState() {
    return fsmState;
  }

  private void setFsmState(FSM_STATE fsmState) {
    Object oldValue = getFsmState();
    this.fsmState = fsmState;
    firePropertyChange(PROPERTY_FSM_STATE, oldValue, this.fsmState);
  }

  public boolean isDetected() {
    return detected;
  }

  private void setDetected(boolean detected) {
    Object oldValue = isDetected();
    this.detected = detected;
    firePropertyChange(PROPERTY_STATE_DETECTED, oldValue, detected);
  }

  public boolean isRegistered() {
    return registered;
  }

  private void setRegistered(boolean registered) {
    Object oldValue = isRegistered();
    this.registered = registered;
    firePropertyChange(PROPERTY_STATE_REGISTERED, oldValue, registered);
  }

  /**
   * Checks if this module is in power saving mode. Since MCM doesn't report
   * power saving status to ConfigTool, this method is required to detect if a
   * module is in power saving mode by checking present status against internal
   * status.
   */
  private boolean isPowerSaving() {
    return getModuleType() == MODULE.MODULE_HMI
        && isPresent()
        && getInternalState() == MODULE_BOARD_STATUS.MODULE_BOARD_STATUS_INVALID;
  }

  public String getAllStatusText() {
    if (ok) {
      return TEXT_OK;
    }
    if (!present) {
      return TEXT_NOT_PRESENT;
    } else {
      StringBuilder sb = new StringBuilder();
      sb.append(TEXT_NOT_OK);

      sb.append("  ");

      if (isPowerSaving()) {
        sb.append(TEXT_POWER_SAVING_MODE);

      } else {
        if (!configured) {
          sb.append(TEXT_NOT_CONFIGURED);
        }

        if (!registered) {
          sb.append(TEXT_NOT_REGISTERED);
        }
      }
      return sb.toString();
    }

  }

  public boolean isOk() {
    return ok;
  }

  private void setOk(boolean ok) {
    Object oldValue = isOk();
    this.ok = ok;
    firePropertyChange(PROPERTY_STATE_OK, oldValue, ok);
  }

  public boolean isConfigured() {
    return configured;
  }

  private void setConfigured(boolean configured) {
    Object oldValue = isConfigured();
    this.configured = configured;
    firePropertyChange(PROPERTY_STATE_CONFIGURED, oldValue, configured);
  }

  public boolean isDisabled() {
    return disabled;
  }

  private void setDisabled(boolean disabled) {
    Object oldValue = isDisabled();
    this.disabled = disabled;
    firePropertyChange(PROPERTY_STATE_DISABLED, oldValue, disabled);
  }

  public boolean isPresent() {
    return present;
  }

  private void setPresent(boolean present) {
    Object oldValue = isPresent();
    this.present = present;
    firePropertyChange(PROPERTY_STATE_PRESENT, oldValue, present);
  }

  /**
   * Indicates if this module has alarms.
   */
  public boolean hasError() {
    return errorFlags.size() > 0;
  }

  public ArrayList<SYS_ALARM_SEVERITY> getErrorFlags() {
    return new ArrayList<SYS_ALARM_SEVERITY>(errorFlags);
  }

  /**
   * Get the error in text format.
   *
   * @return string composed of a list of errors. "None" if there is no error.
   */
  public String getErrorText() {
    return errorText;
  }

  public boolean isTimeSync() {
    return timeSync;
  }

  private void setTimeSync(boolean timeSync) {
    Object oldValue = isTimeSync();
    this.timeSync = timeSync;
    firePropertyChange(PROPERTY_STATE_TIMESYNC, oldValue, timeSync);
  }

  /**
   * Sets a new error mask and update the internal error ENUMs which can be
   * accessed with {@code getErrorEnums()}.
   */
  public void setErrorMask(byte errorMask) {
    if (this.errorMask != errorMask) {
      this.errorMask = errorMask;
      updateErrors(errorMask);
    }
  }

  /**
   * Converts error mask to error states.
   */
  public ArrayList<SYS_ALARM_SEVERITY> parseErrorMask(byte errorMask) {

    ArrayList<SYS_ALARM_SEVERITY> errorFlags = new ArrayList<SYS_ALARM_SEVERITY>();

    if ((ERROR_BIT_CRITIAL & errorMask) != 0) {
      errorFlags.add(SYS_ALARM_SEVERITY.SYS_ALARM_SEVERITY_CRITICAL);
    }

    if ((ERROR_BIT_INFO & errorMask) != 0) {
      errorFlags.add(SYS_ALARM_SEVERITY.SYS_ALARM_SEVERITY_INFO);
    }

    if ((ERROR_BIT_ERROR & errorMask) != 0) {
      errorFlags.add(SYS_ALARM_SEVERITY.SYS_ALARM_SEVERITY_ERROR);
    }

    if ((ERROR_BIT_WARN & errorMask) != 0) {
      errorFlags.add(SYS_ALARM_SEVERITY.SYS_ALARM_SEVERITY_WARNING);
    }

    return errorFlags;
  }

  private void updateErrors(byte errorMask) {
    if (errorFlags != null) {
      errorFlags.clear();
    }
    errorFlags = parseErrorMask(errorMask);
    setTimeSync((ERROR_BIT_TSYNCH & errorMask) == 0);// Update timesync flag
    determineErrorText();
  }

  private void determineErrorText() {
    if (errorFlags.size() > 0) {
      StringBuffer sb = new StringBuffer();
      for (int i = 0; i < errorFlags.size(); i++) {
        sb.append(errorFlags.get(i).getDescription());
        if (i < errorFlags.size() - 1) {
          sb.append(", ");
        }
      }
      errorText = sb.toString();
    } else {
      errorText = TEXT_NO_ERROR;
    }
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (obj == this) {
      return true;
    }
    if (obj.getClass() != getClass()) {
      return false;
    }

    ModuleInfo info = (ModuleInfo) obj;
    return info.getModuleType() == getModuleType() && info.getModuleID() == getModuleID();
  }

  @Override
  public final int hashCode() {
    if (hashCode < 0) {
      String hasCode = String.valueOf(moduleType.getValue())
          + String.valueOf(moduleID.getValue());
      hashCode = Integer.valueOf(hasCode);
    }
    return hashCode;
  }

  public void copyFrom(ModuleInfo m) {
    if (m == null) {
      return;
    }

    assert m.moduleType == moduleType;
    assert m.moduleID == moduleID;

    setBootAPI(m.bootAPI);
    setBootVersion(m.bootVersion);
    setConfigured(m.configured);
    setDetected(m.detected);
    setDisabled(m.disabled);
    setErrorMask(m.errorMask);
    setFeatureVersion(m.featureVersion);
    setFsmState(m.fsmState);
    setInternstate(m.internalState);
    setOk(m.ok);
    setPresent(m.present);
    setRegistered(m.registered);
    setSerialNo(m.serialNo);
    setSupplierID(m.supplierID);
    setSystemAPI(m.systemAPI);
    setTimeSync(m.timeSync);
    setVersion(m.version);
  }

  public static long decodeModuleSerialNo (int serial) {
    return serial & ~(0xFFFFFFFF<<25);
  }
  
  public static int decodeSupplierID (int serial) {
    return (serial & 0xFFFFFFFF<<25) >> 25;
  }
  
  public static ModuleInfo parseBytes(byte[] moduleInfoBytes) {
    assert moduleInfoBytes.length == G3Protocol.PLSize_R_ModuleInfo;

    ModuleInfo minfo = null;

    ByteBuffer buf = createByteBuffer(moduleInfoBytes);

    short moduleTypeValue = NumUtils.convert2Unsigned(buf.get());// MODULE
    short moduleIDValue = NumUtils.convert2Unsigned(buf.get());// MODULE_ID
    int moduleSerial = buf.getInt();

    // Firmware version
    short firmwareRelease = NumUtils.convert2Unsigned(buf.get());// VERSION_TYPE
    long firmareMajor = NumUtils.convert2Unsigned(buf.getInt());
    long firmwareMinor = NumUtils.convert2Unsigned(buf.getInt());
    short firmwarePatch = NumUtils.convert2Unsigned(buf.get());

    // Feature version
    long featureMajor = NumUtils.convert2Unsigned(buf.getInt());
    long featureMinor = NumUtils.convert2Unsigned(buf.getInt());

    // System API
    long apiMajor = NumUtils.convert2Unsigned(buf.getInt());
    long apiMinor = NumUtils.convert2Unsigned(buf.getInt());

    // Boot API
    long bootApiMajor = NumUtils.convert2Unsigned(buf.getInt());
    long bootApiMinor = NumUtils.convert2Unsigned(buf.getInt());

    // Boot version
    short bootFwRelease = NumUtils.convert2Unsigned(buf.get());// VERSION_TYPE
    long bootFwMajor = NumUtils.convert2Unsigned(buf.getInt());
    long bootFwMinor = NumUtils.convert2Unsigned(buf.getInt());
    short bootFwPatch = NumUtils.convert2Unsigned(buf.get());

    // Status
    short internalState = NumUtils.convert2Unsigned(buf.get());// MODULE_BOARD_STATUS
    byte errorMask = buf.get();
    byte statusCode = buf.get();

    // FSM state
    byte fsmState = buf.get();

    // System API
    SystemAPIVersion api = new SystemAPIVersion(apiMajor, apiMinor);

    // Feature Revision
    FeatureVersion feature = new FeatureVersion(featureMajor, featureMinor);

    // Software Version
    VERSION_TYPE vType = VERSION_TYPE.forValue(firmwareRelease);
    ReleaseType releaseType = new ReleaseType(vType.getDescription(), vType.getValue());
    SoftwareVersion firmware = new SoftwareVersion(releaseType,
        firmareMajor, firmwareMinor, firmwarePatch);

    // Bootloader System API
    SystemAPIVersion bootApi = new SystemAPIVersion(bootApiMajor, bootApiMinor);

    // Bootloader Software Version
    vType = VERSION_TYPE.forValue(bootFwRelease);
    releaseType = new ReleaseType(vType.getDescription(), vType.getValue());
    SoftwareVersion bootVer = new SoftwareVersion(releaseType,
        bootFwMajor, bootFwMinor, bootFwPatch);

    MODULE moduleType = MODULE.forValue(moduleTypeValue);
    MODULE_ID moduleID = MODULE_ID.forValue(moduleIDValue);

    if (moduleType != null && moduleID != null) {
      minfo = new ModuleInfo(moduleType, moduleID);
      minfo.setSerialNo(decodeModuleSerialNo(moduleSerial));
      minfo.setSupplierID(decodeSupplierID(moduleSerial));
      minfo.setVersion(firmware);
      minfo.setFeatureVersion(feature);
      minfo.setSystemAPI(api);
      minfo.setBootVersion(bootVer);
      minfo.setBootAPI(bootApi);
      MODULE_BOARD_STATUS internalStateEnum = MODULE_BOARD_STATUS.forValue(internalState);
      minfo.setInternstate(internalStateEnum);
      minfo.setErrorMask(errorMask);

      /* Decode status code */
      minfo.setOk(NumUtils.getBitsValue(statusCode, 0, 0) == 1);
      minfo.setRegistered(NumUtils.getBitsValue(statusCode, 1, 1) == 1);
      minfo.setConfigured(NumUtils.getBitsValue(statusCode, 2, 2) == 1);
      minfo.setPresent(NumUtils.getBitsValue(statusCode, 3, 3) == 1);
      minfo.setDetected(NumUtils.getBitsValue(statusCode, 4, 4) == 1);
      minfo.setDisabled(NumUtils.getBitsValue(statusCode, 5, 5) == 1);

      /* Decode FSM state */
      FSM_STATE fsmstateEnum = FSM_STATE.forValue(NumUtils.convert2Unsigned(fsmState));
      minfo.setFsmState(fsmstateEnum);

      if (fsmstateEnum == null) {
        log.error("Cannot decode FSM_STATE: " + fsmState);
      }
      if (internalStateEnum == null) {
        log.error("Cannot decode MODULE_BOARD_STATUS: " + internalState);
      }

    } else {
      log.error(String.format("Unreoginized Module. Type:%d ID:%d ",
          moduleTypeValue, moduleTypeValue));
    }

    return minfo;
  }

}