<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE concept
  PUBLIC "-//OASIS//DTD DITA Concept//EN" "concept.dtd">
<concept id="topic_5_5_4" xml:lang="en-GB"><title>DNP3 Channel overview</title><conbody><p>The following table describes the commands on the <wintitle>DNP3 Channel</wintitle> page.</p>
    <section>
      <title>Channel tab</title>
      <table id="table-prs_xxj_2r">
        <tgroup cols="2">
          <colspec colnum="1" colname="col1" colwidth="*"/>
          <colspec colnum="2" colname="col2" colwidth="*"/>
          <thead>
            <row>
              <entry>Command</entry>
              <entry>Description</entry>
            </row>
          </thead>
          <tbody>
            <row>
              <entry><b>Channel Name</b></entry>
              <entry>The name that is displayed on the DNP3 page.</entry>
            </row>
            <row>
              <entry><b>Network Type</b></entry>
              <entry>
                <p>In the dropdown list, select one of the following:</p>
                <ul id="ul-h5s_xxj_2r">
                  <li>Serial</li>
                  <li>Comms Device</li>
                  <li>TCP Only</li>
                  <li>TCP/UDP</li>
                  <li>UDP Only</li>
                </ul>
              </entry>
            </row>
          </tbody>
        </tgroup>
      </table>
    </section>
    <section>
      <title>Channel Settings tab</title>
      <p>DNP protocol information is sent in short pieces called frames. The size of each frame can
        be configured using the frame size fields in the Data Link Layer table.</p>
      <p>Sometimes the DNP message frames are quite long, so they are split up into smaller pieces
        called fragments to allow more reliable communication through the transmission
        ../images.</p>
      <p>Confirmation that each frame has been successfully sent is determined by the Confirm Mode
        selected in the Data Link Layer table.</p>
      <table id="table-w43_zxj_2r">
        <tgroup cols="2">
          <colspec colnum="1" colname="col1" colwidth="*"/>
          <colspec colnum="2" colname="col2" colwidth="*"/>
          <thead>
            <row>
              <entry>Command</entry>
              <entry>Description</entry>
            </row>
          </thead>
          <tbody>
            <row>
              <entry><b>Application Layer</b></entry>
              <entry>Configure the size of each DNP fragment in bytes using the fragment size
                fields.</entry>
            </row>
            <row>
              <entry><b>Data Link Layer</b></entry>
              <entry>Use this table to configure addressing and error detection and correction
                before sending the DNP frame over the physical layer.</entry>
            </row>
          </tbody>
        </tgroup>
      </table>
    </section>
    <section>
      <title>Sessions tab</title>
      <table id="table-kzd_1yj_2r">
        <tgroup cols="2">
          <colspec colnum="1" colname="col1" colwidth="*"/>
          <colspec colnum="2" colname="col2" colwidth="*"/>
          <thead>
            <row>
              <entry>Command</entry>
              <entry>Description</entry>
            </row>
          </thead>
          <tbody>
            <row>
              <entry><b>Add Session</b></entry>
              <entry>Add a new session to the list.</entry>
            </row>
            <row>
              <entry><b>Remove Session</b></entry>
              <entry>Delete the selected session from the list.</entry>
            </row>
            <row>
              <entry><b>View</b></entry>
              <entry>Display the selected session for viewing and editing.</entry>
            </row>
          </tbody>
        </tgroup>
      </table>
    </section></conbody></concept>