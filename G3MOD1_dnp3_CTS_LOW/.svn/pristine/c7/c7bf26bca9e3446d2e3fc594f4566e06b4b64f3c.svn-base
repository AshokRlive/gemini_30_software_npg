/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/


/* file: s14fscnb.c
 * description: IEC 60870-5-101 slave FSCNB (File Transfer -File Ready)
 *  functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14fscnb.h"
#include "tmwscl/i870/s14file.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/i14diag.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/utils/tmwtarg.h"

#if S14DATA_SUPPORT_FSCNB

/* function: s14fscnb_processRequest */
void TMWDEFS_CALLBACK s14fscnb_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  /* Store originator address */
  p14Sector->fileOriginator = pMsg->origAddress;

  /* Diagnostics */
  I14DIAG_SHOW_STORE_TYPE(pSector, pMsg);
   
  if(pMsg->cot != I14DEF_COT_FILE_XFER)
  { 
    /* Build and send a Negative Activation Confirmation */
    s14sesn_sendNegActCon(pSector, pMsg, I14DEF_COT_UNKNOWN_COT);
    return;
  }
  
  /* Parse Information Object Address */
  i870util_readInfoObjAddr(pSector->pSession, pMsg, &p14Sector->fileIOA);
  
  /* Read name of file */
  tmwtarg_get16(&pMsg->pRxData->pMsgBuf[pMsg->offset], &p14Sector->fileName);
  pMsg->offset += 2;

  /* Read range start time */
  i870util_read56BitTime(pMsg, &p14Sector->fileRangeStartTime);
  
  /* Read range stop time */
  i870util_read56BitTime(pMsg, &p14Sector->fileRangeStopTime);
 
  /* Diagnostics */
  I14DIAG_SHOW_FSCNB(pSector, p14Sector->fileIOA, p14Sector->fileName);
  I14DIAG_SHOW_TIME(pSector, &p14Sector->fileRangeStartTime);
  I14DIAG_SHOW_TIME(pSector, &p14Sector->fileRangeStopTime);

  /* set state to select file */
  p14Sector->fileState = S14FILE_QUERYLOG_STATE;
}

#endif /* S14DATA_SUPPORT_FSCNB */
