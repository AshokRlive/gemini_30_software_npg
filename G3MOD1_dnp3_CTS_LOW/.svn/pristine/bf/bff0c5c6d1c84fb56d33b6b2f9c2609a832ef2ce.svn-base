/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.model.listeners;

import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.model.libraryElement.AutomationSystem;

/**
 * The Interface IApplicationListener.
 * 
 * @author Gerhard Ebenhofer, gerhard.ebenhofer@profactor.at
 */
public interface IApplicationListener {

	/**
	 * New application added.
	 * 
	 * @param system the system
	 * @param app the app
	 */
	public void newApplicationAdded(AutomationSystem system, Application app);

	/**
	 * Application removed.
	 * 
	 * @param system the system
	 * @param app the app
	 */
	public void applicationRemoved(AutomationSystem system, Application app);
}
