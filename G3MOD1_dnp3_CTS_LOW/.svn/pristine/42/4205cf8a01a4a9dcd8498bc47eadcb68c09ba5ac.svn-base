/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import java.util.Collection;

import javax.swing.ListModel;

import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.module.shared.domain.SwitchModuleOutput;
import com.lucy.g3.rtu.config.module.shared.manager.ExistModuleException;
import com.lucy.g3.rtu.config.module.shared.manager.IModuleManager;
import com.lucy.g3.rtu.config.module.shared.manager.MaxModuleNumException;
import com.lucy.g3.rtu.config.shared.manager.IConfigModule;
import com.lucy.g3.rtu.config.shared.validation.AbstractContainerValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.IContainerValidation;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * Interface for managing all G3 CAN module instances and Backplane settings.
 */
public interface CANModuleManager extends IModuleManager<ICANModule>, IConfigModule, IContainerValidation {

  String CONFIG_MODULE_ID = "CANModuleManager";

  /**
   * Adds a module.
   *
   * @param type
   *          module type
   * @param id
   *          module id
   * @return created module. Null if module type is not supported or module is
   *         already existed.
   */
  @Override
  ICANModule addModule(MODULE type, MODULE_ID id)
      throws MaxModuleNumException, ExistModuleException;

  /**
   * Checks if a module exists.
   */
  boolean contains(MODULE type, MODULE_ID id);

  /**
   * Gets the number of module in a specific type.
   */
  int getModuleNum(MODULE moduleType);

  /**
   * Gets spare module IDs.
   */
  MODULE_ID[] getAvailableIDsByType(MODULE type);

  /**
   * Gets the number of modules these are mounted on backplane.
   */
  int getBackPlaneModuleNum();

  /**
   * Checks if there is spare backplane slot for adding new module.
   *
   */
  boolean hasAvailableSlot();

  /**
   * Checks if the number of a specific type module has reached maximum.
   *
   * @return if true, it means we cannot add any more module in this type.
   */
  boolean hasReachedMaxNum(MODULE moduleType);

  /**
   * Gets the total number of backplane slots.
   *
   */
  int getTotalBackPlaneSlotNum();

  /**
   * Gets current backplane type.
   *
   */
  BackPlaneType getBackplaneType();

  /**
   * Set backplane type.
   *
   */
  void setBackplane(BackPlaneType backplane) throws MaxModuleNumException;

  @Override
  Collection<SwitchModuleOutput> getAllSwitchModuleOutputs();

  @Override
  AbstractContainerValidator getValidator();
  
  ListModel<IChannel> getPowerSupplyChannelsListModel();

}
