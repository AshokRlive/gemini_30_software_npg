/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.gef;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Viewport;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.AccessibleEditPart;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.ExposeHelper;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.ui.parts.ScrollingGraphicalViewer;

/**
 * The Class AdvancedScrollingGraphicalViewer.
 */
public class AdvancedScrollingGraphicalViewer extends ScrollingGraphicalViewer {

	/**
	 * Extends the superclass implementation to scroll the native Canvas control
	 * after the super's implementation has completed.
	 * 
	 * @param part the part
	 * 
	 * @see org.eclipse.gef.EditPartViewer#reveal(org.eclipse.gef.EditPart)
	 */
	@Override
	public void reveal(EditPart part) {
		// super.reveal(part);
		// do the stuff from GraphicalViewer reveal;
		if (part == null) {
			return;
		}
		EditPart current = part.getParent();
		while (current != null) {
			ExposeHelper helper = (ExposeHelper) current
					.getAdapter(ExposeHelper.class);
			if (helper != null) {
				helper.exposeDescendant(part);
			}
			current = current.getParent();
		}
		AccessibleEditPart acc = (AccessibleEditPart) part
				.getAdapter(AccessibleEditPart.class);
		if (acc != null) {
			getControl().getAccessible().setFocus(acc.getAccessibleID());
		}

		Viewport port = getFigureCanvas().getViewport();
		IFigure target = ((GraphicalEditPart) part).getFigure();
		// do not correct viewport for connections
		// FIXME -> make it setable in e.g. preferences;
		if (!(part instanceof ConnectionEditPart)) {
			Rectangle exposeRegion = target.getBounds().getCopy();
			target = target.getParent();
			while (target != null && target != port) {
				target.translateToParent(exposeRegion);
				target = target.getParent();
			}
			exposeRegion.expand(5, 5);

			Dimension viewportSize = port.getClientArea().getSize();

			Point topLeft = exposeRegion.getTopLeft();
			Point bottomRight = exposeRegion.getBottomRight().translate(
					viewportSize.getNegated());
			Point finalLocation = new Point();
			if (viewportSize.width < exposeRegion.width) {
				finalLocation.x = Math.min(bottomRight.x, Math.max(topLeft.x, port
						.getViewLocation().x));
			} else {
				finalLocation.x = Math.min(topLeft.x, Math.max(bottomRight.x, port
						.getViewLocation().x));
			}

			if (viewportSize.height < exposeRegion.height) {
				finalLocation.y = Math.min(bottomRight.y, Math.max(topLeft.y, port
						.getViewLocation().y));
			} else {
				finalLocation.y = Math.min(topLeft.y, Math.max(bottomRight.y, port
						.getViewLocation().y));
			}

			getFigureCanvas().scrollSmoothTo(finalLocation.x, finalLocation.y);
		}
	}

}
