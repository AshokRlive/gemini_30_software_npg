/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.shared;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * The utility class for encoding/decoding RTU Time.
 */
public class RTUTimeDecoder {

  // ====== Time Format ======
  private static final String TIME_FORMAT_EVENT_LOG = "dd-MMM-yyyy HH:mm:ss.SSS";
  private static final String TIME_FORMAT_RTU_TIME = "dd-MMM-yyyy HH:mm:ss.SSS z";// "dd-MMM-yyyy HH:mm:ss.SSS z";
  private static final String TIME_FORMAT_RTU_POINT = "dd-MMM-yyyy HH:mm:ss.SSS"; //"yyyy.MM.dd 'at' HH:mm:ss z"

  public static Date decode(int sec, int min, int hrs, int mday, int month, int year) {
    return convertToUTCTime(sec, min, hrs, mday, month, year);
  }

  public static Date decode(long millis) {
    return convertToUTCTime(millis);
  }

  public static String formatRTUTime(Date date) {
    return convertUTCTimeToString(date, TIME_FORMAT_RTU_TIME);
  }

  public static String formatEventLog(Date date) {
    return convertUTCTimeToString(date, TIME_FORMAT_EVENT_LOG);
  }

  public static String formatPoint(Date date) {
    return convertUTCTimeToString(date, TIME_FORMAT_RTU_POINT);
  }

  public static DateFormat getRTUTimeFormatter() {
    return getRTUTimeFormatter(TIME_FORMAT_RTU_TIME);
  }

  public static DateFormat getRTUTimeFormatter(String rtuTimeFormat) {
    DateFormat formatter = new SimpleDateFormat(rtuTimeFormat);
    formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
    return formatter;
  }
  
  public static Date convertToUTCTime(int sec, int min, int hrs, int mday, int month, int year) {
    Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
    cal.set(year, month, mday, hrs, min, sec);
    return cal.getTime();
  }
  
  /**
   * Converts milliseconds to UTC time.
   *
   * @param millis
   *          the millis date the milliseconds since January 1, 1970, 00:00:00 GMT
   * @return the UTC time
   */
  public static Date convertToUTCTime(long millis) {
    Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
    cal.setTimeInMillis(millis);
    return cal.getTime();
  }
  
  private static String convertUTCTimeToString(Date date, String format){
    DateFormat formatter = new SimpleDateFormat(format);
    formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
    return formatter.format(date);
  }
}
