/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.ui;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.border.CompoundBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;
import org.jdesktop.swingx.JXTitledSeparator;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.common.context.IContext;
import com.lucy.g3.configtool.rtu.config.ConfigurationManager;
import com.lucy.g3.gui.common.widgets.ext.swing.splitbutton.JSplitButton;
import com.lucy.g3.gui.common.widgets.ext.swing.splitbutton.SplitButtonActionAdapter;
import com.lucy.g3.gui.common.widgets.utils.ResourceUtils;
import com.lucy.g3.gui.framework.page.AbstractPage;
import com.lucy.g3.rtu.config.general.GeneralConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfig;

/**
 * A viewable page which contains buttons for configuration management(e.g.open,
 * close, read, write...).
 */
public class ConfigurationRoot extends AbstractPage {
  private final PresentationModel<GeneralConfig> pm = new PresentationModel<>();
  private final IContext context;


  public ConfigurationRoot(IContext context) {
    super(null);
    this.context = context;

    setNodeIcon(ResourceUtils.getImg("rootNode.icon", getClass()));
    setNodeName(ResourceUtils.getString(getClass(), "rootNode.text"));
  }

  @Override
  protected void init() {
    initComponents();
    initComponentBindings();
    initEventHandling();
  }

  private void initComponentBindings() {
    Bindings.bind(lblDescript, pm.getModel(GeneralConfig.PROPERTY_CONFIG_DESCRIPTION));
    Bindings.bind(lblVersion, pm.getModel(GeneralConfig.PROPERTY_CONFIG_VERSION));
  }
  
  private void initEventHandling() {
    ConfigurationManager configMgr = context.getComponent(ConfigurationManager.SUBSYSTEM_ID);
    
    // Update the label of info text 
    configMgr.addPropertyChangeListener(ConfigurationManager.PROPERTY_CONFIG_DATA,
        new PropertyChangeListener() {
          @Override
          public void propertyChange(PropertyChangeEvent evt) {
            configDataChanged((IConfig)evt.getNewValue());
          }
        });
    configDataChanged(configMgr.getConfigData());
    
    // Update the text of lblFrom
    configMgr.addPropertyChangeListener(ConfigurationManager.PROPERTY_CONFIG_FROM,
        new PropertyChangeListener() {
          @Override
          public void propertyChange(final PropertyChangeEvent evt) {
            SwingUtilities.invokeLater(new Runnable() {
              @Override
              public void run() {
                lblFrom.setText((String)evt.getNewValue());            
              }
            });
          }
        });
    lblFrom.setText(configMgr.getConfigFrom());
  }
  
  private void configDataChanged(IConfig config) {
    pm.release();
    pm.setBean(null);
    
    if(config != null) {
      pm.setBean((GeneralConfig) config.getConfigModule(GeneralConfig.CONFIG_MODULE_ID));
    }
  }

  @Override
  public boolean isScrollable() {
    return true;
  }

  private void createUIComponents() {
    Application app = Application.getInstance();

    ApplicationActionMap actionMap;

    ConfigurationManager configMgr = context.getComponent(ConfigurationManager.SUBSYSTEM_ID);
    actionMap = app.getContext().getActionMap(configMgr);

    // ====== Create split button "New" ======
    btnNew = new JSplitButton();
    Action action = actionMap.get(ConfigurationManager.ACTION_CREATE_FROM_SCRATCH);
    ((JSplitButton) btnNew).addSplitButtonActionListener(new SplitButtonActionAdapter(action));
    JPopupMenu popup = new JPopupMenu();
    popup.add(action);
    action = actionMap.get(ConfigurationManager.ACTION_OPEN_CONFIG);
    popup.add(action);
    ((JSplitButton) btnNew).setPopupMenu(popup);

    // ====== Create button "Save" ======
    action = actionMap.get(ConfigurationManager.ACTION_SAVE_CONFIG);
    btnSave = new JButton(action);
    btnSave.setIcon(null);
    lblSave = new JLabel((String) action.getValue(Action.SHORT_DESCRIPTION));

    // ====== Create button "Save As" ======
    action = actionMap.get(ConfigurationManager.ACTION_SAVEAS_CONFIG);
    btnSaveAs = new JButton(action);
    btnSaveAs.setIcon(null);
    lblSaveAs = new JLabel((String) action.getValue(Action.SHORT_DESCRIPTION));

    // ====== Create button "Read" ======
    action = actionMap.get(ConfigurationManager.ACTION_READ_CONFIG);
    btnRead = new JButton(action);
    btnRead.setIcon(null);
    lblRead = new JLabel((String) action.getValue(Action.SHORT_DESCRIPTION));

    // ====== Create button "Write" ======
    action = actionMap.get(ConfigurationManager.ACTION_WRITE_CONFIG);
    btnWrite = new JButton(action);
    btnWrite .setIcon(null);
    lblWrite = new JLabel((String) action.getValue(Action.SHORT_DESCRIPTION));

    // ====== Create button "Erase" ======
    action = actionMap.get(ConfigurationManager.ACTION_ERASE_CONFIG);
    btnErase = new JButton(action);
    lblErase = new JLabel((String) action.getValue(Action.SHORT_DESCRIPTION));

    // ====== Create button "Print" ======
    action = actionMap.get(ConfigurationManager.ACTION_PRINT_COFIG);
    btnPrint = new JButton(action);
    lblPrint = new JLabel((String) action.getValue(Action.SHORT_DESCRIPTION));
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel panel1;
  private JLabel lblVersion;
  private JLabel lblDescript;
  private JLabel lblFrom;
  private JXTitledSeparator separator1;
  private JButton btnNew;
  private JLabel lblNew;
  private JXTitledSeparator separator3;
  private JButton btnRead;
  private JLabel lblRead;
  private JButton btnWrite;
  private JLabel lblWrite;
  private JXTitledSeparator separator2;
  private JButton btnSave;
  private JLabel lblSave;
  private JButton btnSaveAs;
  private JLabel lblSaveAs;
  private JButton btnPrint;
  private JLabel lblPrint;
  private JButton btnErase;
  private JLabel lblErase;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    panel1 = new JPanel();
    JLabel label2 = new JLabel();
    lblVersion = new JLabel();
    JLabel label3 = new JLabel();
    lblDescript = new JLabel();
    JLabel label1 = new JLabel();
    lblFrom = new JLabel();
    separator1 = new JXTitledSeparator();
    lblNew = new JLabel();
    separator3 = new JXTitledSeparator();
    separator2 = new JXTitledSeparator();

    //======== this ========
    setBorder(Borders.DIALOG_BORDER);
    setLayout(new FormLayout(
        "[60dlu,default], $lcgap, default:grow",
        "fill:default, $ugap, default, $lgap, fill:default, $pgap, bottom:default, $lgap, 2*(fill:default, $ugap), fill:default, $pgap, bottom:default, $lgap, 2*(fill:default, $ugap), fill:default"));

    //======== panel1 ========
    {
      panel1.setBorder(new CompoundBorder(
          new TitledBorder("Configuration Information"),
          Borders.DLU2_BORDER));
      panel1.setLayout(new FormLayout(
          "default, $lcgap, default:grow, $ugap, default, $lcgap, default:grow, $ugap, default, $lcgap, default:grow",
          "fill:default"));

      //---- label2 ----
      label2.setText("Version:");
      panel1.add(label2, CC.xy(1, 1));
      panel1.add(lblVersion, CC.xy(3, 1));

      //---- label3 ----
      label3.setText("Description:");
      panel1.add(label3, CC.xy(5, 1));
      panel1.add(lblDescript, CC.xy(7, 1));

      //---- label1 ----
      label1.setText("Loaded From:");
      panel1.add(label1, CC.xy(9, 1));
      panel1.add(lblFrom, CC.xy(11, 1));
    }
    add(panel1, CC.xywh(1, 1, 3, 1));

    //---- separator1 ----
    separator1.setTitle("New Configuration");
    add(separator1, CC.xywh(1, 3, 3, 1));

    //---- btnNew ----
    btnNew.setText("New...");
    btnNew.setToolTipText(null);
    add(btnNew, CC.xy(1, 5));

    //---- lblNew ----
    lblNew.setText("Create a new configuration for RTU.");
    add(lblNew, CC.xy(3, 5));

    //---- separator3 ----
    separator3.setTitle("RTU Configuration");
    add(separator3, CC.xywh(1, 7, 3, 1));

    //---- btnRead ----
    btnRead.setText("Read");
    btnRead.setToolTipText(null);
    add(btnRead, CC.xy(1, 9));
    add(lblRead, CC.xy(3, 9));

    //---- btnWrite ----
    btnWrite.setText("Write");
    btnWrite.setToolTipText(null);
    add(btnWrite, CC.xy(1, 11));
    add(lblWrite, CC.xy(3, 11));

    //---- separator2 ----
    separator2.setTitle("Export Configuration");
    add(separator2, CC.xywh(1, 15, 3, 1));

    //---- btnSave ----
    btnSave.setText("Save");
    btnSave.setToolTipText(null);
    add(btnSave, CC.xy(1, 17));
    add(lblSave, CC.xy(3, 17));

    //---- btnSaveAs ----
    btnSaveAs.setText("Save As...");
    btnSaveAs.setToolTipText(null);
    add(btnSaveAs, CC.xy(1, 19));
    add(lblSaveAs, CC.xy(3, 19));

    //---- btnPrint ----
    btnPrint.setText("Export As HTML...");
    btnPrint.setToolTipText(null);
    btnPrint.setVisible(false);
    add(btnPrint, CC.xy(1, 21));

    //---- lblPrint ----
    lblPrint.setVisible(false);
    add(lblPrint, CC.xy(3, 21));

    //---- btnErase ----
    btnErase.setText("Erase");
    btnErase.setToolTipText(null);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }

  @Override
  public boolean hasError() {
    return false;
  }
}
