/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.events;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Date;

import org.apache.log4j.Logger;

import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.rtu.comms.shared.RTUTimeDecoder;
import com.lucy.g3.xml.gen.common.EventTypeDef;
import com.lucy.g3.xml.gen.common.EventTypeDef.EVENTCLOGIC_VALUE;
import com.lucy.g3.xml.gen.common.EventTypeDef.EVENTCOMM_ACTION;
import com.lucy.g3.xml.gen.common.EventTypeDef.EVENTCOMM_DEVICE;
import com.lucy.g3.xml.gen.common.EventTypeDef.EVENTOLR_VALUE;
import com.lucy.g3.xml.gen.common.EventTypeDef.EVENTSWITCH_VALUE;
import com.lucy.g3.xml.gen.common.EventTypeDef.EVENT_CLASS;
import com.lucy.g3.xml.gen.common.EventTypeDef.EVENT_READ_CONFIG_VALUE;
import com.lucy.g3.xml.gen.common.EventTypeDef.EVENT_REQUEST_VALUE;
import com.lucy.g3.xml.gen.common.EventTypeDef.EVENT_STARTUP_VALUE;
import com.lucy.g3.xml.gen.common.EventTypeDef.EVENT_TYPE_AUTOMATION;
import com.lucy.g3.xml.gen.common.EventTypeDef.EVENT_TYPE_CLOGIC;
import com.lucy.g3.xml.gen.common.EventTypeDef.EVENT_TYPE_CLOGICTYPE;
import com.lucy.g3.xml.gen.common.EventTypeDef.EVENT_TYPE_DNP3SCADA;
import com.lucy.g3.xml.gen.common.EventTypeDef.EVENT_TYPE_S101;
import com.lucy.g3.xml.gen.common.EventTypeDef.EVENT_TYPE_S104;
import com.lucy.g3.xml.gen.common.EventTypeDef.EVENT_TYPE_SWITCH;
import com.lucy.g3.xml.gen.common.EventTypeDef.EVENT_TYPE_SYSTEM;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * This class represents the content and structure of RTU System Event which is
 * transferred over G3 Protocol. (See G3Protocol Document)
 */
public final class EventEntry {

  private static final Logger LOGGER = Logger.getLogger(EventEntry.class);

  public static final int PAYLOAD_LENGTH = 9; // bytes;

  private Date timestamp;

  private EVENT_CLASS eventClass;

  private Object eventType;

  private String status;

  private Long pointID;

  private Number pointValue;

  private int index;


  private EventEntry() {
  }

  public EVENT_CLASS getEventClass() {
    return eventClass;
  }

  public Object getEventType() {
    return eventType;
  }

  public String getStatus() {
    return status;
  }

  public Long getPointID() {
    return pointID;
  }

  public Number getPointValue() {
    return pointValue;
  }

  public Date getTimestamp() {
    return timestamp;
  }

  public int getIndex() {
    return index;
  }

  public void setIndex(int index) {
    this.index = index;
  }

  public String getFormattedEventText() {

    return String.format("%-5d %s [%s] %s %s %s %s",
        index,
        RTUTimeDecoder.formatEventLog(timestamp),
        eventClass,
        eventType,
        pointID,
        pointValue != null ? pointValue : "",
        status);
  }

  public static EventEntry parseBytes(byte[] eventEntryBytes, ByteOrder order) {
    EventEntry eventEntry = new EventEntry();
    try {
      eventEntry.parse(eventEntryBytes, order);
      return eventEntry;
    } catch (Exception e) {
      LOGGER.error("Failed to parse event entry", e);
      return null;
    }
  }

  /**
   * Decode event entry bytes stream.
   *
   * @return the event entry instance. Null the bytes cannot be decoded.
   */
  private void parse(byte[] eventEntryBytes, ByteOrder order) throws Exception {
    ByteBuffer buf = ByteBuffer.wrap(eventEntryBytes).asReadOnlyBuffer();
    buf.order(order);

    // ====== Begin parsing header ======
    // Parse time stamp
    long secs = buf.getInt();
    long nanosecond = buf.getInt();
    long millisecond = secs * 1000 + nanosecond / 1000000;
    this.timestamp = RTUTimeDecoder.decode(millisecond);

    // Parse event type
    int eventClassValue = buf.get();
    this.eventClass = EVENT_CLASS.forValue(eventClassValue);
    assertEnumMustNotNull("EVENT_CLASS", eventClass, eventClassValue);

    // ====== Begin parsing payload ======
    buf = buf.slice().asReadOnlyBuffer();
    buf.order(order);

    switch (eventClass) {

    case EVENT_CLASS_DNP3SCADA_POINT:
      decodeDnp3PointEvent(buf);
      break;

    case EVENT_CLASS_CLOGIC:
      decodeLogicEvent(buf);
      break;

    case EVENT_CLASS_COMM:
      decodeCommEvent(buf);
      break;

    case EVENT_CLASS_OLR:
      decodeOLREvent(buf);
      break;

    case EVENT_CLASS_DATETIME:
      decodeDateTimeEvent(buf);
      break;

    case EVENT_CLASS_SWITCH:
      decodeSwitchEvent(buf);
      break;

    case EVENT_CLASS_SYSTEM:
      decodeSystemEvent(buf);
      break;

    case EVENT_CLASS_S101_POINT:
      decodeS101PointEvent(buf);
      break;
      
    case EVENT_CLASS_S104_POINT:
      decodeS104PointEvent(buf);
      break;
      
    case EVENT_CLASS_AUTOMATION:
      decodeAutoEvent(buf);
      break;

    default:
      throw new Exception("Unsupported event class:" + eventClass);
    }
  }

  private void decodeAutoEvent(ByteBuffer buf)  throws Exception{
    short evtTypeVal = buf.getShort();
    EVENT_TYPE_AUTOMATION evtType = EVENT_TYPE_AUTOMATION.forValue(evtTypeVal);
    assertEnumMustNotNull("EVENT_TYPE_AUTOMATION", evtType, evtTypeVal);
    this.eventType = evtType;
  }

  private void decodeLogicEvent(ByteBuffer buf) throws Exception {
    int eventtypeValue = NumUtils.convert2Unsigned(buf.getShort());
    int logictypeValue = NumUtils.convert2Unsigned(buf.getShort());
    int eventvalue = buf.getInt();
    EVENT_TYPE_CLOGIC eventtype = EVENT_TYPE_CLOGIC.forValue(eventtypeValue);
    EVENT_TYPE_CLOGICTYPE logictype = EVENT_TYPE_CLOGICTYPE.forValue(logictypeValue);
    EVENTCLOGIC_VALUE opcode = EVENTCLOGIC_VALUE.forValue(eventvalue);

    assertEnumMustNotNull("EVENT_TYPE_CLOGIC", eventtype, eventtypeValue);
    assertEnumMustNotNull("EVENT_TYPE_CLOGICTYPE", logictype, logictypeValue);
    assertEnumMustNotNull("EVENTCLOGIC_VALUE", opcode, eventvalue);

    eventType = eventtype;
    status = String.format("%s %s ", logictype, opcode);
  }

  private void decodeCommEvent(ByteBuffer buf) throws Exception {
    int deviceValue = buf.getInt();
    int actionValue = buf.getInt();

    EVENTCOMM_DEVICE device = EVENTCOMM_DEVICE.forValue(deviceValue);
    EVENTCOMM_ACTION action = EVENTCOMM_ACTION.forValue(actionValue);

    assertEnumMustNotNull("EVENTCOMM_DEVICE", device, deviceValue);
    assertEnumMustNotNull("EVENTCOMM_ACTION", action, actionValue);

    eventType = device;
    status = String.valueOf(action);
  }

  private void decodeOLREvent(ByteBuffer buf) throws Exception {
    short olrValue = NumUtils.convert2Unsigned(buf.get());
    EVENTOLR_VALUE olr = EVENTOLR_VALUE.forValue(olrValue);
    assertEnumMustNotNull("EVENTOLR_VALUE", olr, olrValue);

    eventType = olr;
    pointValue = olr.getValue();
  }

  private void decodeDateTimeEvent(ByteBuffer buf) {
    long new_secs = NumUtils.convert2Unsigned(buf.getInt());
    long new_milli = NumUtils.convert2Unsigned(buf.getInt());
    long millis = new_secs * 1000 + new_milli;
    Date new_date = RTUTimeDecoder.decode(millis);

    eventType = "Set Date Time";
    status = new_date.toString();
  }

  private void decodeSwitchEvent(ByteBuffer buf) throws Exception {
    int eventVal = NumUtils.convert2Unsigned(buf.getShort());
    int midVal = NumUtils.convert2Unsigned(buf.get());
    int mtypeVal = NumUtils.convert2Unsigned(buf.get());
    int swStatusVal = buf.getInt();
    int swIdxVal = NumUtils.convert2Unsigned(buf.get());

    EVENT_TYPE_SWITCH event = EVENT_TYPE_SWITCH.forValue(eventVal);
    EVENTSWITCH_VALUE swStatus = EVENTSWITCH_VALUE.forValue(swStatusVal);
    //SwitchIndex idx = SwitchIndex.forValue(swIdxVal);
    char idx = (char) ('A' + swIdxVal);
    
    MODULE_ID mid = MODULE_ID.forValue(midVal);
    MODULE mtype = MODULE.forValue(mtypeVal);

    assertEnumMustNotNull("EVENT_TYPE_SWITCH", event, eventVal);
    assertEnumMustNotNull("EVENTSWITCH_VALUE", swStatus, swStatusVal);

    if (midVal == EventTypeDef.DummySwitchID) {
      eventType = String.format("[Dummy Switch] %s", event);
    } else {
      assertEnumMustNotNull("MODULE_ID", mid, midVal);
      assertEnumMustNotNull("MODULE_TYPE", mtype, mtypeVal);

      if (mtype == MODULE.MODULE_DSM) {
        eventType = String.format("[%s%s - %s] %s",
            mtype,
            mid,
            idx,
            event);
      } else {
        eventType = String.format("[%s%s] %s",
            mtype,
            mid,
            event);
      }
    }

    status = String.valueOf(swStatus);
  }

  private void decodeSystemEvent(ByteBuffer buf) throws Exception {
    int sysTypeValue = buf.getInt();
    int systemValue = buf.getInt();

    EVENT_TYPE_SYSTEM sysType = EVENT_TYPE_SYSTEM.forValue(sysTypeValue);
    assertEnumMustNotNull("EVENT_TYPE_SYSTEM", sysType, sysTypeValue);
    eventType = sysType;

    switch (sysType) {
    case EVENT_TYPE_SYSTEM_STARTUP:
      EVENT_STARTUP_VALUE startupValue = EVENT_STARTUP_VALUE.forValue(systemValue);
      status = String.valueOf(startupValue);
      assertEnumCouldBeNull("EVENT_STARTUP_VALUE", startupValue, systemValue);
      break;

    case EVENT_TYPE_SYSTEM_READ_CONFIG:
      EVENT_READ_CONFIG_VALUE configValue = EVENT_READ_CONFIG_VALUE.forValue(systemValue);
      status = String.valueOf(configValue);
      assertEnumCouldBeNull("EVENT_READ_CONFIG_VALUE", configValue, systemValue);
      break;

    case EVENT_TYPE_SYSTEM_REBOOTREQ:
    case EVENT_TYPE_SYSTEM_RESTARTREQ:
      EVENT_REQUEST_VALUE requestValue = EVENT_REQUEST_VALUE.forValue(systemValue);
      assertEnumCouldBeNull("EVENT_REQUEST_VALUE", requestValue, systemValue);
      status = String.format("From: %s", requestValue);
      break;

    case EVENT_TYPE_SYSTEM_DOOR_OPEN:
      pointValue = systemValue;
      status = systemValue == 1 ? "Open" : "Close";
      break;

    default:
      break;
    }
  }

  private void decodeDnp3PointEvent(ByteBuffer buf) throws Exception {
    int pointTypeValue = buf.getShort();
    EVENT_TYPE_DNP3SCADA pointType = EVENT_TYPE_DNP3SCADA.forValue(pointTypeValue);
    assertEnumMustNotNull("EVENT_TYPE_DNP3SCADA", pointType, pointTypeValue);
    eventType = pointType;

    pointID = Long.valueOf(NumUtils.convert2Unsigned(buf.getShort()));
    switch (pointType) {
    case EVENT_TYPE_DNP3SCADA_ANALOGUE:
    case EVENT_TYPE_DNP3SCADA_ANLGOUT:
      pointValue = buf.getFloat();
      status = decodeDnp3APointStatus(buf.get());
      break;

    case EVENT_TYPE_DNP3SCADA_DOUBLEBINARY:
      pointValue = NumUtils.getBitsValue(buf.get(), 1, 0);
      buf.get();
      buf.get();
      buf.get();
      status = decodeDnp3DPointStatus(buf.get());
      break;

    case EVENT_TYPE_DNP3SCADA_SINGLEBINARY:
      pointValue = NumUtils.getBitsValue(buf.get(), 0, 0);
      buf.get();
      buf.get();
      buf.get();
      status = decodeDnp3DPointStatus(buf.get());
      break;

    case EVENT_TYPE_DNP3SCADA_COUNTER:
      pointValue = NumUtils.convert2Unsigned(buf.getInt());
      status = decodeDnp3CPointStatus(buf.get());
      break;

    default:
      throw new Exception("Unsupported dnp3 point type:" + pointType);
    }
  }

  private String decodeDnp3DPointStatus(byte status) {
    boolean online = (NumUtils.getBitsValue(status, 0, 0) == 1);
    boolean restart = (NumUtils.getBitsValue(status, 1, 1) == 1);
    boolean commlost = (NumUtils.getBitsValue(status, 2, 2) == 1);
    boolean remoteforced = (NumUtils.getBitsValue(status, 3, 3) == 1);
    boolean localforced = (NumUtils.getBitsValue(status, 4, 4) == 1);
    boolean chatter = (NumUtils.getBitsValue(status, 5, 5) == 1);

    StringBuffer sb = new StringBuffer();
    if (!online) {
      sb.append(" Offline");
    }

    if (restart) {
      sb.append(" Restart");
    }

    if (commlost) {
      sb.append(" Comm lost");
    }

    if (remoteforced) {
      sb.append(" Remote forced");
    }

    if (localforced) {
      sb.append(" Local forced");
    }

    if (chatter) {
      sb.append(" Chatter");
    }
    return sb.toString();
  }

  private String decodeDnp3APointStatus(byte status) {
    boolean online = (NumUtils.getBitsValue(status, 0, 0) == 1);
    boolean restart = (NumUtils.getBitsValue(status, 1, 1) == 1);
    boolean commlost = (NumUtils.getBitsValue(status, 2, 2) == 1);
    boolean remoteforced = (NumUtils.getBitsValue(status, 3, 3) == 1);
    boolean localforced = (NumUtils.getBitsValue(status, 4, 4) == 1);
    boolean overrange = (NumUtils.getBitsValue(status, 5, 5) == 1);
    boolean referror = (NumUtils.getBitsValue(status, 6, 6) == 1);

    StringBuffer sb = new StringBuffer();
    if (!online) {
      sb.append(" Offline");
    }

    if (restart) {
      sb.append(" Restart");
    }

    if (commlost) {
      sb.append(" Comm lost");
    }

    if (remoteforced) {
      sb.append(" Remote forced");
    }

    if (localforced) {
      sb.append(" Local forced:");
    }

    if (overrange) {
      sb.append(" Overrange");
    }

    if (referror) {
      sb.append(" Reference Error");
    }
    return sb.toString();
  }

  private String decodeDnp3CPointStatus(byte status) {
    boolean online = (NumUtils.getBitsValue(status, 0, 0) == 1);
    boolean restart = (NumUtils.getBitsValue(status, 1, 1) == 1);
    boolean commlost = (NumUtils.getBitsValue(status, 2, 2) == 1);
    boolean remoteforced = (NumUtils.getBitsValue(status, 3, 3) == 1);
    boolean localforced = (NumUtils.getBitsValue(status, 4, 4) == 1);
    boolean rollover = (NumUtils.getBitsValue(status, 5, 5) == 1);
    boolean discontinuity = (NumUtils.getBitsValue(status, 6, 6) == 1);

    StringBuffer sb = new StringBuffer();
    if (!online) {
      sb.append(" Offline");
    }

    if (restart) {
      sb.append(" Restart");
    }

    if (commlost) {
      sb.append(" Comm lost");
    }

    if (remoteforced) {
      sb.append(" Remote forced");
    }

    if (localforced) {
      sb.append(" Local forced");
    }

    if (rollover) {
      sb.append(" Rollover");
    }

    if (discontinuity) {
      sb.append(" Discontinuity");
    }

    return sb.toString();
  }

  private void decodeS104PointEvent(ByteBuffer buf) throws Exception {
    int pointTypeValue = buf.get(); // 1 byte point type
    EVENT_TYPE_S104 pointType = EVENT_TYPE_S104.forValue(pointTypeValue);
    assertEnumMustNotNull("EVENT_TYPE_S104", pointType, pointTypeValue);
    eventType = pointType;

    pointID = NumUtils.convert2Unsigned(buf.getInt()); // 4 bytes IOA
    switch (pointType) {
    case EVENT_TYPE_S104_ANALOGUE_SCALED:
      pointValue = buf.getShort();// 2 bytes signed integer point value
      buf.getShort(); // Discard unused 2 bytes
      status = decodeIEC870APointStatus(buf.get());
      break;

    case EVENT_TYPE_S104_ANALOGUE_NORMAL:
    case EVENT_TYPE_S104_ANALOGUE_FLOAT:
      pointValue = buf.getFloat();
      status = decodeIEC870APointStatus(buf.get());
      break;

    case EVENT_TYPE_S104_DOUBLEBINARY:
      pointValue = NumUtils.getBitsValue(buf.get(), 1, 0);
      buf.get();
      buf.get();
      buf.get();
      status = decodeIEC870DPointStatus(buf.get());
      break;

    case EVENT_TYPE_S104_SINGLEBINARY:
      pointValue = NumUtils.getBitsValue(buf.get(), 0, 0);
      buf.get();
      buf.get();
      buf.get();
      status = decodeIEC870DPointStatus(buf.get());
      break;

    case EVENT_TYPE_S104_COUNTER:
      pointValue = NumUtils.convert2Unsigned(buf.getInt());
      status = decodeIEC870CPointStatus(buf.get());
      break;

    default:
      throw new Exception("Unsupported S104 point type:" + pointType);
    }

  }
  private void decodeS101PointEvent(ByteBuffer buf) throws Exception {
    int pointTypeValue = buf.get(); // 1 byte point type
    EVENT_TYPE_S101 pointType = EVENT_TYPE_S101.forValue(pointTypeValue);
    assertEnumMustNotNull("EVENT_TYPE_S101", pointType, pointTypeValue);
    eventType = pointType;
    
    pointID = NumUtils.convert2Unsigned(buf.getInt()); // 4 bytes IOA
    switch (pointType) {
    case EVENT_TYPE_S101_ANALOGUE_SCALED:
      pointValue = buf.getShort();// 2 bytes signed integer point value
      buf.getShort(); // Discard unused 2 bytes
      status = decodeIEC870APointStatus(buf.get());
      break;
      
    case EVENT_TYPE_S101_ANALOGUE_NORMAL:
    case EVENT_TYPE_S101_ANALOGUE_FLOAT:
      pointValue = buf.getFloat();
      status = decodeIEC870APointStatus(buf.get());
      break;
      
    case EVENT_TYPE_S101_DOUBLEBINARY:
      pointValue = NumUtils.getBitsValue(buf.get(), 1, 0);
      buf.get();
      buf.get();
      buf.get();
      status = decodeIEC870DPointStatus(buf.get());
      break;
      
    case EVENT_TYPE_S101_SINGLEBINARY:
      pointValue = NumUtils.getBitsValue(buf.get(), 0, 0);
      buf.get();
      buf.get();
      buf.get();
      status = decodeIEC870DPointStatus(buf.get());
      break;
      
    case EVENT_TYPE_S101_COUNTER:
      pointValue = NumUtils.convert2Unsigned(buf.getInt());
      status = decodeIEC870CPointStatus(buf.get());
      break;
      
    default:
      throw new Exception("Unsupported S101 point type:" + pointType);
    }
    
  }

  private String decodeIEC870DPointStatus(byte status) {
    boolean iv = (NumUtils.getBitsValue(status, 7, 7) == 1);
    boolean nt = (NumUtils.getBitsValue(status, 6, 6) == 1);
    boolean sb = (NumUtils.getBitsValue(status, 5, 5) == 1);
    boolean bl = (NumUtils.getBitsValue(status, 4, 4) == 1);

    StringBuffer buf = new StringBuffer();
    if (nt) {
      buf.append(" Not Topical");
    }

    if (iv) {
      buf.append(" Invalid");
    }

    if (sb) {
      buf.append(" Substituted");
    }

    if (bl) {
      buf.append(" Blocked");
    }

    return buf.toString();
  }

  private String decodeIEC870APointStatus(byte status) {
    boolean iv = (NumUtils.getBitsValue(status, 7, 7) == 1);
    boolean nt = (NumUtils.getBitsValue(status, 6, 6) == 1);
    boolean sb = (NumUtils.getBitsValue(status, 5, 5) == 1);
    boolean bl = (NumUtils.getBitsValue(status, 4, 4) == 1);
    boolean ov = (NumUtils.getBitsValue(status, 0, 0) == 1);

    StringBuffer buf = new StringBuffer();
    if (nt) {
      buf.append(" Not Topical");
    }

    if (iv) {
      buf.append(" Invalid");
    }

    if (ov) {
      buf.append(" Overflow");
    }

    if (sb) {
      buf.append(" Substituted");
    }

    if (bl) {
      buf.append(" Blocked");
    }

    return buf.toString();
  }

  private String decodeIEC870CPointStatus(byte status) {
    int seq = NumUtils.getBitsValue(status, 0, 4);
    boolean cy = (NumUtils.getBitsValue(status, 5, 5) == 1);
    boolean ca = (NumUtils.getBitsValue(status, 6, 6) == 1);
    boolean iv = (NumUtils.getBitsValue(status, 7, 7) == 1);

    StringBuffer buf = new StringBuffer();

    buf.append(String.format("[Sequence Number: %-2d]", seq));

    if (iv) {
      buf.append(" Invalid");
    }

    if (cy) {
      buf.append(" Carry");
    }

    if (ca) {
      buf.append(" Counter Adjusted");
    }

    return buf.toString();
  }

  private static void assertEnumMustNotNull(String enumName, Enum<?> e, Number value) throws Exception {
    if (e == null) {
      throw new Exception("Cannot decode value: " + value + " into enum:" + enumName);
    }
  }

  private static void assertEnumCouldBeNull(String enumName, Enum<?> e, Number value) {
    if (e == null) {
      LOGGER.error("Cannot decode value: " + value + " into enum:" + enumName);
    }
  }
}
