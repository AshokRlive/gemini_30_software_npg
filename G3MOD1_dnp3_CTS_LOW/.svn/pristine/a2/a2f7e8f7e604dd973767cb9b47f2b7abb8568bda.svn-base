/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: MemServiceNVRAM.cpp 04-Jan-2013 09:41:05 pueyos_a $
 *               $HeadURL: U:\G3\RTU\MCM\src\MemoryManager\src\MemServiceNVRAM.cpp $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Header module for handling user NVRAM storage.
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 04-Jan-2013 09:41:06	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   04-Jan-2013 09:41:06  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_794D7F0E_9426_4323_AD12_1FDA7AB5F4E5__INCLUDED_)
#define EA_794D7F0E_9426_4323_AD12_1FDA7AB5F4E5__INCLUDED_



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "LockingMutex.h"
#include "MemServiceNVRAMAbstract.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Class for handling user data storage in NVRAM
 */
class MemServiceNVRAM : public MemServiceNVRAMAbstract
{

public:
    MemServiceNVRAM(const lu_char_t* MEMDEVICE_APP_PATH);
	virtual ~MemServiceNVRAM();

	//Inherited from IMemService
    virtual void setSize(IMemoryManager::STORAGE_VAR variable, size_t size);
    virtual IMemService::MEMORYERROR init();
    virtual lu_bool_t read(IMemoryManager::STORAGE_VAR variable, lu_uint8_t* value, size_t* sizeRead);
    virtual lu_bool_t write(IMemoryManager::STORAGE_VAR variable, lu_uint8_t* value);
    virtual IMemService::MEMORYERROR invalidate();
    virtual void flush();
    virtual void close();

private:
    /* structure for tracking of variables stored in memory */
    struct MemNVRAMElemStr
    {
        IMemoryManager::STORAGE_VAR varEnum;    //Type of variable
        size_t varSize;         //size of variable in bytes
        lu_uint32_t address;    //memory address at the non-volatile memory device
        lu_bool_t invalid;      //flag stating if value is not valid yet (fail read/not written).
    };

private:
    const lu_char_t *MemoryDevice;    //Memory device path
    MasterMutex initAccess;     //Mutex for initialisation only
    lu_bool_t updPending;       //flag stating if any value has changed but not written to storage.

    lu_uint8_t *scratchBuffer;  //buffer to store what should be written to memory.
    lu_uint32_t bufSize;        //size of both buffers (including header).
    lu_uint32_t dataSize;       //size of data stored in the buffer (excluding header).
    lu_bool_t valid;            //validity of the buffer
    lu_uint32_t initTry;    //Count down attempts for initialisation

    MemNVRAMElemStr memIndex[IMemoryManager::STORAGE_VAR_LAST];  //memory storage index table
};


#endif // !defined(EA_794D7F0E_9426_4323_AD12_1FDA7AB5F4E5__INCLUDED_)

/*
 *********************** End of file ******************************************
 */

