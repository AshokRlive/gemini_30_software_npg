/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.fordiac.ide.application.ApplicationPlugin;
import org.fordiac.ide.application.Messages;
import org.fordiac.ide.application.editors.ApplicationEditorInput;
import org.fordiac.ide.application.editors.FBNetworkEditor;
import org.fordiac.ide.model.libraryElement.Application;
import org.fordiac.ide.model.libraryElement.I4DIACElement;
import org.fordiac.ide.model.libraryElement.impl.ApplicationImpl;
import org.fordiac.ide.util.OpenListener;

/**
 * An Action which opens the <code>ApplicationEditor</code> for the specified
 * Model.
 * 
 * @author Gerhard Ebenhofer (gerhard.ebenhofer@profactor.at)
 */
public class OpenApplicationEditorAction extends OpenListener {

	/** The app. */
	private Application app;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.IObjectActionDelegate#setActivePart(org.eclipse.jface.action
	 * .IAction, org.eclipse.ui.IWorkbenchPart)
	 */
	@Override
	public void setActivePart(final IAction action,
			final IWorkbenchPart targetPart) {
		// nothing to do
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	@Override
	public void run(final IAction action) {
		ApplicationEditorInput input = new ApplicationEditorInput(app);

		IWorkbenchPage activePage = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage();
		try {
			editor = activePage.openEditor(input, FBNetworkEditor.class.getName());			
		} catch (PartInitException e) {
			editor = null;
			ApplicationPlugin.getDefault().logError(
					Messages.OpenApplicationEditorAction_ERROR_OpenApplicationEditor, e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.IActionDelegate#selectionChanged(org.eclipse.jface.action
	 * .IAction, org.eclipse.jface.viewers.ISelection)
	 */
	@Override
	public void selectionChanged(final IAction action, final ISelection selection) {
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection structuredSel = (IStructuredSelection) selection;
			if (structuredSel.getFirstElement() instanceof Application) {
				app = (Application) structuredSel.getFirstElement();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.fordiac.systemmanagement.OpenListener#supportsObject(java.lang.Class)
	 */
	@Override
	public boolean supportsObject(final Class<? extends I4DIACElement> clazz) {
		return clazz != null && clazz.equals(ApplicationImpl.class);
	}

	/* (non-Javadoc)
	 * @see org.fordiac.ide.util.OpenListener#getOpenListenerAction()
	 */
	@Override
	public Action getOpenListenerAction() {
		return new OpenListenerAction(this);
	}
}
