/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: c_template.c 1469 2012-06-20 20:51:28Z fryers_j $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/common/template/c_template.c $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "errorCodes.h"
#include "crc32.h"
#include "SlaveBinImageDef.h"
#include "SlaveBinImage.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */





/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR SlaveBinImageCheckIntegrity(lu_uint8_t *imageAddressPtr)
{
	SB_ERROR retError;
	lu_uint32_t crc32;
	SlaveBinImageHeaderStr *binImageHeaderPtr;
	lu_uint8_t *dataPtr;
	lu_uint32_t size;
	retError = SB_ERROR_CRC;

	binImageHeaderPtr  = (SlaveBinImageHeaderStr *)(imageAddressPtr + SLAVE_IMAGE_BASE_OFFSET);


	if (binImageHeaderPtr->magicSyncCode == SLAVE_IMAGE_MAGIC_SYNC)
	{
		/* Check Header CRC first */
		dataPtr  = imageAddressPtr;
		dataPtr += SLAVE_IMAGE_BASE_OFFSET;
		dataPtr += sizeof(binImageHeaderPtr->headerCRC32);

		crc32_calc32(dataPtr,
					 (sizeof(SlaveBinImageHeaderStr) - sizeof(binImageHeaderPtr->headerCRC32)),
					 &crc32
					);

		if (binImageHeaderPtr->headerCRC32 == crc32)
		{
			/* Check Pre-Header CRC */
			dataPtr  = imageAddressPtr;

			size     = SLAVE_IMAGE_BASE_OFFSET;

			crc32_calc32(dataPtr,
						 size,
						 &crc32
						);

			if (binImageHeaderPtr->preHeaderCRC32 == crc32)
			{
				/* Check Post-Header CRC */
				dataPtr  = imageAddressPtr;
				dataPtr += SLAVE_IMAGE_BASE_OFFSET;
				dataPtr += SLAVE_IMAGE_HEAD_SIZE;


				size     = binImageHeaderPtr->imageSize;
				size    -= SLAVE_IMAGE_BASE_OFFSET;
				size    -= SLAVE_IMAGE_HEAD_SIZE;

				crc32_calc32(dataPtr,
							 size,
							 &crc32
							);

				if (binImageHeaderPtr->postHeaderCRC32 == crc32)
				{
					retError = SB_ERROR_NONE;
				}
			}
		}
	}

	return retError;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
