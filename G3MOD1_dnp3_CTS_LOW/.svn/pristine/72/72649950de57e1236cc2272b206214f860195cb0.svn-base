/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port.eth;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.itemlist.IItemListManager.AddItemException;
import com.lucy.g3.rtu.config.port.eth.EthPortManager;
import com.lucy.g3.rtu.config.port.eth.EthernetPort;
import com.lucy.g3.rtu.config.port.eth.IEthernetPort;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.ETHERNET_PORT;

/**
 * The Class PortsManagerImplTest.
 */
public class EthPortManagerTest {

  private EthPortManager fixture;


  @Before
  public void setUp() throws Exception {
    fixture = new EthPortManager();
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test(expected = AddItemException.class)
  public void testAddSamePort() {
    // Exception expected when adding non-custom port
    fixture.add(new EthernetPort(ETHERNET_PORT.ETHERNET_PORT_CONFIG, true));
  }

  @Test
  public void testExistingPortCannotBeRemoved() {
    Collection<IEthernetPort> exitsingPorts = fixture.getEthPorts();

    for (IEthernetPort port : exitsingPorts) {
      assertFalse(fixture.remove(port));
      assertTrue(fixture.contains(port));
    }

  }

}
