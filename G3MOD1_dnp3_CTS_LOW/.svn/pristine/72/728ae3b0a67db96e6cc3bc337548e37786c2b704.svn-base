/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.systemconfiguration.commands;

import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.ui.IEditorPart;
import org.fordiac.ide.model.ui.SegmentView;
import org.fordiac.ide.util.Utils;

/**
 * The Class SegmentSetConstraintCommand.
 */
public class SegmentSetConstraintCommand extends Command {

	/** The Constant MOVE_LABEL. */
	private static final String MOVE_LABEL = "Move/Resize";

	/** The new bounds. */
	private Rectangle newBounds;

	/** The old bounds. */
	private Rectangle oldBounds;

	/** The request. */
	private ChangeBoundsRequest request;

	/** The editor. */
	private IEditorPart editor;

	/** The segment view. */
	private SegmentView segmentView;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canUndo()
	 */
	@Override
	public boolean canUndo() {
		return editor.equals(Utils.getCurrentActiveEditor());

	}

	/**
	 * Instantiates a new segment set constraint command.
	 */
	public SegmentSetConstraintCommand() {
		setLabel(MOVE_LABEL);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		Object type = request.getType();
		// make sure the Request is of a type we support: (Move or
		// Move_Children/Resize or Resize_Children)
		return RequestConstants.REQ_MOVE.equals(type)
				|| RequestConstants.REQ_MOVE_CHILDREN.equals(type)
				|| RequestConstants.REQ_RESIZE.equals(type)
				|| RequestConstants.REQ_RESIZE_CHILDREN.equals(type)
				|| RequestConstants.REQ_ALIGN_CHILDREN.equals(type);
	}

	/**
	 * Sets the new Position and Dimension of the affected Segment.
	 * 
	 * @see #redo()
	 */
	@Override
	public void execute() {
		editor = Utils.getCurrentActiveEditor();
		if (segmentView.getPosition() == null || segmentView.getSize() == null) {
			oldBounds = new Rectangle(10, 10, 50, 20);
		} else {
			oldBounds = new Rectangle(segmentView.getPosition().getX(),
					segmentView.getPosition().getY(), segmentView.getSize()
							.getWidth(), segmentView.getSize().getHeight());
		}
		redo();
	}

	/**
	 * Redo.
	 * 
	 * @see #execute()
	 */
	@Override
	public void redo() {
		segmentView.getPosition().setX(newBounds.x);
		segmentView.getPosition().setY(newBounds.y);

		segmentView.getSize().setWidth(newBounds.width);
		segmentView.getSize().setHeight(-1);

	}

	/**
	 * Restores the old position/Dimension.
	 */
	@Override
	public void undo() {
		// Position pos = new PositionImpl(oldBounds.x, oldBounds.y);
		// Dimension size = UiFactory.eINSTANCE.createDimension();
		// size.setHeight(oldBounds.height);
		// size.setWidth(oldBounds.width);
		// segment.setPosition(pos);
		// segment.setDimension(size);
	}

	/**
	 * Gets the new bounds.
	 * 
	 * @return the new bounds
	 */
	public Rectangle getNewBounds() {
		return newBounds;
	}

	/**
	 * Sets the new bounds.
	 * 
	 * @param newBounds the new new bounds
	 */
	public void setNewBounds(final Rectangle newBounds) {
		this.newBounds = newBounds;
	}

	/**
	 * Gets the request.
	 * 
	 * @return the request
	 */
	public ChangeBoundsRequest getRequest() {
		return request;
	}

	/**
	 * Sets the request.
	 * 
	 * @param request the new request
	 */
	public void setRequest(final ChangeBoundsRequest request) {
		this.request = request;
	}

	/**
	 * Gets the segment view.
	 * 
	 * @return the segment view
	 */
	public SegmentView getSegmentView() {
		return segmentView;
	}

	/**
	 * Sets the segment view.
	 * 
	 * @param segmentView the new segment view
	 */
	public void setSegmentView(final SegmentView segmentView) {
		this.segmentView = segmentView;
	}

}
