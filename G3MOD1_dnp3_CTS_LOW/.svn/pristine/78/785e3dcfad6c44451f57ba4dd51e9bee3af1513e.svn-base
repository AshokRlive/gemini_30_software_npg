/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.fbt.typeeditor.commands;

import org.eclipse.gef.commands.Command;
import org.fordiac.ide.model.libraryElement.Event;
import org.fordiac.ide.model.libraryElement.VarDeclaration;
import org.fordiac.ide.model.libraryElement.With;

/**
 * The Class DeleteWithCommand.
 */
public class DeleteWithCommand extends Command {

	/** The with. */
	With with;

	/** The old var decl. */
	VarDeclaration oldVarDecl;

	/** The event. */
	Event event;

	/**
	 * Instantiates a new delete with command.
	 * 
	 * @param with the with
	 */
	public DeleteWithCommand(final With with) {
		this.with = with;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		event = (Event) with.eContainer();
		oldVarDecl = with.getVariables();
		with.setVariables(null);
		event.getWith().remove(with);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		event.getWith().add(with);
		with.setVariables(oldVarDecl);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		with.setVariables(null);
		event.getWith().remove(with);
	}

}
