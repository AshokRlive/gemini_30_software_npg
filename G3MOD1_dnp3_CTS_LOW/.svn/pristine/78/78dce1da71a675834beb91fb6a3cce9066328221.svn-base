/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: MCMAlarmChannelObserver.cpp 9 Jan 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/MCMIOModule/src/MCMAlarmChannelObserver.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MCM Module Channels monitoring unit for Enabling related Module Alarm.
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 9 Jan 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   9 Jan 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>     //use of abs()
#include <limits>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MCMAlarmChannelObserver.h"
#include "MCMIOModule.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

MCMAlarmChannelObserver::MCMAlarmChannelObserver(//lu_uint32_t id,
                                    IChannel& channelRef,
                                    CHANNELALARMTYPE typeAlarm,
                                    ModuleAlarm alarmRef,
                                    IIOModule& MCMModule
                                    ) :
                                        moduleMCM(MCMModule),
                                        channel(channelRef),
                                        alarmType(typeAlarm),
                                        alarm(alarmRef)
{
    /*Start channel observer */
    channel.attach(this);
}

MCMAlarmChannelObserver::~MCMAlarmChannelObserver()
{
    channel.detach(this);
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

void MCMAlarmChannelObserver::update(IChannel *subject)
{
    lu_bool_t newAlarmState;    //Alarm state to be set
    lu_uint16_t parameter = alarm.parameter;  //Default alarm parameter
    //incoming update from input channel
    switch (alarmType)
    {
        case CHANNELALARMTYPE_ALARMFLAG:
        {
            //Check channel's alarm flag only
            IODataFloat32 data; //any type will do
            IChannel::ValueStr value(data);
            subject->read(value);
            newAlarmState = value.flags.alarm;
            break;
        }
        case CHANNELALARMTYPE_ALARMFLAG_AI:
        {
            //Check channel's alarm flag and store value in parameter
            IODataFloat32 data;
            IChannel::ValueStr value(data);
            subject->read(value);
            newAlarmState = value.flags.alarm;
            if(newAlarmState == LU_TRUE)
            {
                lu_float32_t parValue = data;
                parValue *= (parValue < 0)? 0 :     //discard negative values
                                            1000;   //convert analogue value to milliUnits
                if(parValue > std::numeric_limits<lu_uint16_t>::max())
                {
                    parameter = std::numeric_limits<lu_uint16_t>::max(); //max parameter value reached
                }
                else
                {
                    parameter = parValue;   //strip decimal point value
                }
            }
            break;
        }
        case CHANNELALARMTYPE_ALARMFLAG_AI_POS:
        case CHANNELALARMTYPE_ALARMFLAG_AI_NEG:
        {
            //Check channel's alarm flag and positive/negative value, and store value in parameter
            IODataFloat32 data;
            IChannel::ValueStr value(data);
            subject->read(value);
            lu_float32_t parValue = data;   //we use 32 bit for channel value and parameter calculations
            /* When channel has its alarm flag, enable one alarm depending on the channel value */
            newAlarmState = (alarmType==CHANNELALARMTYPE_ALARMFLAG_AI_POS)?
                            ( (parValue > 0) && (value.flags.alarm == LU_TRUE) ) :  //alarm for overflow
                            ( (parValue < 0) && (value.flags.alarm == LU_TRUE) );   //alarm for underflow
            if(newAlarmState == LU_TRUE)
            {
                parValue = abs(parValue);   //Get absolute value and strip decimal point value
                if(parValue > std::numeric_limits<lu_uint16_t>::max())
                {
                    parameter = std::numeric_limits<lu_uint16_t>::max(); //max parameter value reached
                }
                else
                {
                    parameter = parValue;
                }
            }
            break;
        }
        case CHANNELALARMTYPE_DIVALUE:
        {
            //Check channel's value only
            IODataUint8 data;
            IChannel::ValueStr value(data);
            subject->read(value);
            newAlarmState = (lu_bool_t)data;
            break;
        }
        case CHANNELALARMTYPE_DIVALUEINV:
        {
            //Check channel's value only
            IODataUint8 data;
            IChannel::ValueStr value(data);
            subject->read(value);
            lu_uint8_t valueDI = data;
            newAlarmState = (valueDI == 0)? LU_TRUE : LU_FALSE;    //original value inverted
            break;
        }
        default:
            return;
            break;
    }

    /* Cast the generic module reference provided to an specific MCM Module
     *  (gets NULL if the reference is not an MCM Module).
     */
    MCMIOModule* mcm = dynamic_cast<MCMIOModule*>(&moduleMCM);
    if(mcm != NULL)
    {
        //report state to alarm
        if(newAlarmState == LU_TRUE)
        {
            mcm->activateAlarm(alarm.getID(), alarm.severity, parameter);
        }
        else
        {
            mcm->deactivateAlarm(alarm.getID());
        }
    }
}

/*
 *********************** End of file ******************************************
 */
