/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.ui.panels;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleSCMSettings;
import com.lucy.g3.rtu.config.module.shared.domain.IModuleSettingsEditor;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.LED_COLOUR;

/**
 * SCM settings panel.
 */
public class SCMSettingsPanel extends JPanel implements IModuleSettingsEditor{
  private final ModuleSCMSettings settings;
  
  public SCMSettingsPanel(ModuleSCMSettings settings) {
    this.settings = settings;
    initComponents();
  }

  private void initComponents() {
    FormLayout layout = new FormLayout("right:default, $lcgap, [80dlu,default], $lcgap, default:grow", "default");
    DefaultFormBuilder builder = new DefaultFormBuilder(layout, this);
    builder.setDefaultDialogBorder();

    PresentationModel<ModuleSCMSettings> scmModel = new PresentationModel<ModuleSCMSettings>(settings);
    ValueModel vm;

    vm = scmModel.getModel(ModuleSCMSettings.PROPERTY_OPEN_COLOUR);
    ComboBoxAdapter<LED_COLOUR> combModel = new ComboBoxAdapter<LED_COLOUR>(LED_COLOUR.values(), vm);
    @SuppressWarnings("unchecked")
    JComponent comp = new JComboBox<LED_COLOUR>(combModel);
    builder.append("Switch Open Colour:", comp);
    builder.nextLine();

    vm = scmModel.getModel(ModuleSCMSettings.PROPERTY_ALLOWFORCEDOPERATION);
    comp = BasicComponentFactory.createCheckBox(vm,
        "Allow Force Operation");
    comp.setToolTipText(ModuleSCMSettings.FORCE_OPERATION_HINT);
    builder.append("", comp, 3);
    builder.nextLine();
  }

  @Override
  public JComponent getEditorComopnent() {
    return this;
  }

  @Override
  public String getEditorTitle() {
    return "SCM Settings";
  }

}
