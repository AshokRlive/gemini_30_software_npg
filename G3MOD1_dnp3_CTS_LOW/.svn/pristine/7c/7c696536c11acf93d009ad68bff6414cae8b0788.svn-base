/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:ModuleManager.h
 *
 *    FILE TYPE: c++ header source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   3/09/13      wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MODULEMANAGER_H_
#define MODULEMANAGER_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Thread.h"
#include "CANFraming.h"
#include "CANPeriodicData.h"
#include "Logger.h"
#include "IIOModuleManager.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *   ModuleManager
 *   \return 
 *
 ******************************************************************************
 */

class CANPeriodicData;

class ModuleManager: public IIOModuleManager,public Thread
{
public:
    friend class CANPeriodicData;

    const static lu_uint32_t THREAD_PRIORITY = Thread::PRIO_MIN +30;

    ModuleManager();
    virtual ~ModuleManager();

    /**Inherit*/
    virtual IIOModule *getModule(MODULE module, MODULE_ID moduleID);
    virtual std::vector<IIOModule*> getAllModules();
    virtual void resetAllSlaveModules();
    virtual void setHBeatEnable(lu_uint8_t x);
    virtual lu_bool_t isHBeatEnabled();

protected:

    /*Inherit*/
    virtual void threadBody();

private:
    /**
     * Initialise CAN filter. Called when CANFraming is created.
     */
    void initCANFilter();

    /**
     * \brief Get a module from this manager by ID and type.
     * 
     *  If the module does not exist, create the module and add it to this manager.
     *
     * \param module Module type
     * \param moduleID Module ID number
     * \param iFace Interface where the Module is supposed to be connected
     *
     * \return pointer to the module. NULL if failed.
     */
    IIOModule *createSlaveModule(MODULE module, MODULE_ID moduleID,COMM_INTERFACE iface);

    /**
     * \brief Receive a message from CAN bus and then decode it.
     */
    void receiveMessage(CANFraming &socket);

    /**
     * \brief Decode a received message from CAN slaves.
     */
    IOM_ERROR decodeMessage(ModuleMessage* messagePtr, COMM_INTERFACE iFace);

    /**
     * \brief Tick event execution handler.
     */
    IOM_ERROR tickEvent(lu_uint32_t dTime);

private:
    static const lu_uint32_t modulesSize = MODULE_LAST * MODULE_ID_BRD;

    static const COMM_INTERFACE PrimaryCanInterface   = COMM_INTERFACE_CAN0;
    static const COMM_INTERFACE SecondaryCanInterface = COMM_INTERFACE_CAN1;

    static const lu_uint32_t CanTimeoutSec  = 0   ;
    static const lu_uint32_t CanTimeoutUsec = 1000;

    CANFraming    canPSocket;
    CANFraming    canSSocket;
    RAWSmallModuleMessage hBeatMsg;//Heart Beat message

    lu_uint32_t hBeatCounter;   //Tick counter to check before generating a new HBeat
    lu_uint8_t timeCounter;     //Counter to be added to the HBeat payload

    Mutex mutex;
    Mutex mutexHBeat;

    CANPeriodicData *periodicThread;

    Logger& log;

    IIOModule *modules[MODULE_LAST][MODULE_ID_BRD];
};


#endif /* MODULEMANAGER_H_ */

/*
 *********************** End of file ******************************************
 */
