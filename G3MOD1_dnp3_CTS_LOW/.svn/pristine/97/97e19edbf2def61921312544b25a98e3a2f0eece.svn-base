/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.filetransfer;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.SocketTimeoutException;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.comms.client.AbstractCommsTask;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.shared.ICommsTaskInvoker;
import com.lucy.g3.security.support.SecurityUtils;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_TRANSFERTYPE;

/**
 * This task writes files to RTU based on G3 Configuration Protocol.
 */
public class FileWritingCommsTask extends AbstractCommsTask {
  private final static int MAX_RETRIES = 3;
  
  private Logger log = Logger.getLogger(FileWritingCommsTask.class);

  private File[] files;// files to be written to RTU
  
  private String targetPath = null; 

  private CTH_TRANSFERTYPE type;

  private final FileTransfer fileTransfer;

  private final int filefragSize = G3Protocol.CTMsg_MaxPayloadSize - G3Protocol.PLSize_C_FileWriteFrag;

  private FileEndInfo[] endInfo;

  public FileWritingCommsTask(ICommsTaskInvoker invoker, 
      FileTransfer fileTransfer, CTH_TRANSFERTYPE type, File... files) {
    super(invoker);
    this.fileTransfer = fileTransfer;
    this.files = files;
    this.type = type == null ? CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_GENERIC : type;
  }
  
  /**
   * Set the path on RTU for receiving the file. If null, the default file name
   * will be used.
   *
   * @param targetPath   the file path relative to the default receiving folder 
   *          which is decided by the CTH_TRANSFERTYPE.
   */
  public void setTargetPath(String targetPath) {
    this.targetPath = targetPath;
  }

  public FileEndInfo writeFile(CTH_TRANSFERTYPE type, File file) throws Exception {
    return writeFile(type, file, targetPath);
  }
  
  private FileEndInfo writeFile(CTH_TRANSFERTYPE type, File file, String targetPath) throws Exception {
    BufferedInputStream fileStream = null;
    boolean tranferring = false;
    FileEndInfo endInfo = null;
    int offset = 0;
    final String path = targetPath == null ? file.getName() : targetPath;
    
    int fileSize = (int) file.length();
    try {

      int fileCRC32 = (int) (SecurityUtils.calculateCRC32(file.getAbsolutePath()) & 0x00FFFFFFFF);
      final int fragmentNum = fileSize / filefragSize + 1;
      int fragmentIndex = 0;
      fileStream = new BufferedInputStream(new FileInputStream(file));

      // Send START message
      log.debug("Starting File Transfer. Size:" + fileSize + "  File name:" + path + "  Fragment number:" + fragmentNum);
      tranferring = true;
      fileTransfer.cmdFileWriteStart(fileSize, G3Protocol.DEFAULT_FILE_PERMISSION, fileCRC32, false, path, type);

      while (!isCancelled()) {
        int availableSize = fileStream.available();
        // Reach file end, end transferring
        if (availableSize <= 0) {
          endInfo = fileTransfer.cmdFileWriteFinish(false);
          endInfo.setFileName(file.getName());
          endInfo.setType(type);
          tranferring = false;
          break;
        }

        // Send one fragment
        byte[] fragmentBuffer = new byte[availableSize >= filefragSize ? filefragSize : availableSize];

        int fragmentSize = fileStream.read(fragmentBuffer);
        int retries = 0;
        do {
          try {
            fileTransfer.cmdFileWriteFragment(fragmentBuffer, fragmentSize, offset);
            break;
          }catch(SocketTimeoutException e){
            retries ++;
            if(retries > MAX_RETRIES)
              throw e;
            else {
              Throwable cause = e.getCause();
              log.error(String.format("Failed to write file fragment cause: %s. [Retrying:%d]", 
                  cause == null ? e.getMessage() : cause.getMessage(), retries));
            }
          }
        } while(true);
        
        offset += fragmentSize;
        fragmentIndex++;
        int progress = (fragmentIndex * 100 / fragmentNum);
        setProgress(progress);
        setMessage(String.format("Writing file: %s\nProgress: %s%%", path, progress));
      }

    } catch (Exception e) {
      log.error("Failed to transfer file", e);
      throw e;
    } catch (Throwable t) {
      log.fatal("Failed to transfer file", t);
    } finally {
      /* Cancel file transferring */
      if (tranferring) {
        try {
          log.info("Cancelling file transferring");
          fileTransfer.cmdFileWriteFinish(true);
        } catch (Exception e) {
          log.error("Fail to cancel file writing ", e);
        }
      }

      /* Close file */
      if (fileStream != null) {
        try {
          fileStream.close();
        } catch (Exception e) {
          log.error(e);
        }
      }

    }
    log.debug("File transferring is completed. Cancelled:" + isCancelled());
    return endInfo;
  }

  @Override
  public void run() throws Exception {
    if (files == null) {
      throw new IllegalArgumentException("No file to be transfered");
    }

    endInfo = new FileEndInfo[files.length];
    for (int i = 0; i < files.length; i++) {
      if (isCancelled()) {
        break;
      }

      setMessage("Writing file:" + files[i].getName());
      endInfo[i] = writeFile(type, files[i]);
    }
  }

  public FileEndInfo[] getEndInfo() {
    return endInfo;
  }
  
}
