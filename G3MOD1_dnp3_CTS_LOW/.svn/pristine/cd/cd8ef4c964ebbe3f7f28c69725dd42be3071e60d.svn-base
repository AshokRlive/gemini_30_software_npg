/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.domain.impl;

import com.g3schema.ns_vpoint.AnaloguePointT;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.shared.model.NotConnectibleException;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.impl.AbstractAnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointReadSupport;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointWriteSupport;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdAnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.validation.StandardPointValidator;
import com.lucy.g3.rtu.config.virtualpointsource.domain.IVirtualPointSource;
import com.lucy.g3.xml.gen.api.IXmlEnum.DefaultCreateOption;

/**
 * Implementation of {@linkplain StdAnaloguePoint}.
 */
public final class StdAnaloguePointImpl extends AbstractAnaloguePoint implements StdAnaloguePoint {

  private final IValidator validator;


  StdAnaloguePointImpl(int id, String description) {
    super(STANDARD_POINT_GROUP, id, description);
    validator = new StandardPointValidator(this);
  }

  /**
   * Duplicate a standard analogue point. Property "Channel" and "filter" are shallow copied.
   */
  @Override
  public StdAnaloguePoint duplicate() {
    StdAnaloguePointImpl copy = new StdAnaloguePointImpl(0, getDescription());
    copy.setScalingFactor(getScalingFactor());
    copy.setUnit(getUnit());
    copy.setOffset(getOffset());
    copy.setOverflow(getOverflow());
    copy.setUnderflow(getUnderflow());
    copy.setOverflowHysteresis(getOverflowHysteresis());
    copy.setUnderflowHysteresis(getUnderflowHysteresis());
    copy.setRawMin(getRawMin());
    copy.setRawMax(getRawMax());
    copy.setScaledMax(getScaledMax());
    copy.setScaledMin(getScaledMin());
    copy.setChannel(getChannel());
    copy.setFilterType(getFilterType());
    copy.setF0Deadband(getF0Deadband());
    copy.setF0InitNominal(getF0InitNominal());
    copy.setF0InitNominalEnabled(isF0InitNominalEnabled());
    copy.setF1LowerHysteresis(getF1LowerHysteresis());
    copy.setF1UpperHysteresis(getF1UpperHysteresis());
    copy.setF1Lowerlimit(getF1Lowerlimit());
    copy.setF1Upperlimit(getF1Upperlimit());
    copy.getF2HiHiLoLo().copyFrom(getF2HiHiLoLo());

    return copy;
  }

  @Override
  public DefaultCreateOption getDefaultCreateOption() {
    return getChannel() == null ? null : getChannel().predefined().getDefaultCreateOption();
  }

  @Override
  public String getRawUnit() {
    IChannel ch = getChannel();
    return ch != null ? ch.predefined().getRawUnit() : "";
  }

  @Override
  public IChannel getChannel() {
    return (IChannel) getSource();
  }

  @Override
  public void setChannel(IChannel channel) {
    setSource(channel);
  }

  @Override
  public void setSource(IVirtualPointSource source)
      throws NotConnectibleException {
    SourceChecker.checkChannel(this, source);
    super.setSource(source);
  }

  @Override
  public IValidator getValidator() {
    return validator;
  }

  @Override
  public void writeToXML(AnaloguePointT xml, VirtualPointWriteSupport support) {
    super.writeToXML(xml);

    support.writeChannelRef(this.getName(), xml.channel.append(), this.getChannel());
  }

  @Override
  public void readFromXML(AnaloguePointT xml, VirtualPointReadSupport support) {
    super.readFromXML(xml, support);

    IChannel ch = support.getChannelByRef(xml.channel.first(), ChannelType.ANALOG_INPUT);
    setChannel(ch);
  }

}