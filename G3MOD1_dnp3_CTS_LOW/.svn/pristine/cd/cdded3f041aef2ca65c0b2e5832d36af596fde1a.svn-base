/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.shared.manager;

import java.util.Collection;

import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.module.shared.domain.SwitchModuleOutput;
import com.lucy.g3.rtu.config.shared.manager.IListConfigModule;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

public interface IModuleManager<T extends Module> extends IListConfigModule<T> {
  
  /**
   * Gets all existing modules including CAN module, Field Device module, etc.
   *
   * @return
   */
  Collection<T> getAllModules();

  /**
   * Gets modules in specific types. If the type is empty or null, return all
   * existing modules.
   */
  Collection<T> getModulesByType(MODULE... types);

  T getModule(MODULE type, MODULE_ID id);
  
  Collection<SwitchModuleOutput> getAllSwitchModuleOutputs();

  Module addModule(MODULE type, MODULE_ID module_ID);
}
