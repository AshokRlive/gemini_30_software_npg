/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board specific status manager module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "CANProtocolCodec.h"
#include "StatusManager.h"
#include "BoardStatusManager.h"

#include "IOManagerIOUpdate.h"
#include "IOManagerIntAnalogIOUpdate.h"
#include "BoardIO.h"

#include "BatteryCharger.h"
#include "SupplyController.h"
#include "FanController.h"

//MG START

#include "lpc17xx_adc.h"
#include "lpc17xx_pinsel.h"

//MG END

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

static SB_ERROR generateEventTest(void);

static lu_uint16_t readADC(TestADCPinChanStr *adcPinChanPtr)
{
TestADCReadStr      adcRead;
lu_uint32_t         adcRate;
lu_uint8_t          adcChan;
ADC_TYPE_INT_OPT    adcTypIntOpt;

switch (adcPinChanPtr->chan)
{
    case 0:
        adcTypIntOpt = ADC_ADINTEN0;
        break;

    case 1:
        adcTypIntOpt = ADC_ADINTEN1;
        break;

    case 2:
        adcTypIntOpt = ADC_ADINTEN2;
        break;

    case 3:
        adcTypIntOpt = ADC_ADINTEN3;
        break;

    case 4:
        adcTypIntOpt = ADC_ADINTEN4;
        break;

    case 5:
        adcTypIntOpt = ADC_ADINTEN5;
        break;

    case 6:
        adcTypIntOpt = ADC_ADINTEN6;
        break;

    case 7:
        adcTypIntOpt = ADC_ADINTEN7;
        break;

    default:
        return SB_ERROR_PARAM;
    }

    adcChan = (adcPinChanPtr->chan & 0x07);

    /* Setup and configure pin */
    configAdcPin(adcChan);

    /* Setup ADC rate - default to 200khz */
    adcRate = 200000;
    if (adcPinChanPtr->rateKhz <= 200)
    {
        adcRate = (1000 * adcPinChanPtr->rateKhz);
    }
    ADC_Init(LPC_ADC, adcRate);

    ADC_IntConfig(LPC_ADC, adcTypIntOpt, DISABLE);

    /* Select channel */
    ADC_ChannelCmd(LPC_ADC, adcChan, ENABLE);

    /* Do the ADC conversion */
    ADC_StartCmd(LPC_ADC, ADC_START_NOW);

    /* Wait for conversion & get value */
    while (!(ADC_ChannelGetStatus(LPC_ADC, adcChan, ADC_DATA_DONE)));

    adcRead.value = ADC_ChannelGetData(LPC_ADC, adcChan);

    ADC_ChannelCmd(LPC_ADC, adcChan, DISABLE);

    ADC_DeInit(LPC_ADC);

    return adcRead.value;
}
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


static const SMThreadStr boardThreadList[] =
{
	/* run						 cyclic				period(ms)        */
	{CANCDecode, 				 0, 					0 				      }, // Run all the time

	{IOManagerIORunUpdateGPIO, 	 0, 					IOMAN_UPDATE_GPIO_MS  }, // run every 10ms
	{IOManagerIntAnalogIOUpdate, 0, 					IOMAN_UPDATE_INAI_MS  }, // run every 15 ms
	{IOManagerIOAIEventUpdate,   0,                     IOMAN_UPDATE_INAI_EVENT_MS},

	{BatteryChargerTick, 		 0, 					BATT_CH_TICK_MS 	  }, // run every 1000ms

	{SupplyControlTick, 		 0, 					SUPPLY_CON_TICK_MS 	  }, // run every 1000ms

	{FanControllerTick, 		 0, 					FAN_CON_TICK_MS 	  }, // run every 1000ms

	//{generateEventTest,         0,                  1000                  }, //

	{(SMThreadRun)0L, 	    	0, 					0   			      }  // end of table
};



/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

void BoardStatusManagerExecutive(void)
{
	BatteryChargerInit();
	FanControllerInit();
	SupplyControlInit();

	/* Call status manager - Never return!! */
	StatusManagerRun(&boardThreadList[0]);
}


static SB_ERROR generateEventTest(void)
{
    /* Generate an event */
    static ModuleDEventStr eventD = {0, 0, {0, 0}};
    static ModuleAEventStr eventA = {0, 0, {0, 0}};
    static TestADCPinChanStr adcPinChan = {2, 1};
    lu_uint32_t i, j;

    for(j = 0; j < 2; j++)
    {
        eventD.time     = CANCGetTime();
        eventD.value   ^= 1;
        for(i = 0; i < PSM_CH_DINPUT_LAST; ++i)
        {
            eventD.channel = i;
            CANCSendMCM( MODULE_MSG_TYPE_EVENT,
                         MODULE_MSG_ID_EVENT_DIGITAL,
                         sizeof(eventD),
                         (lu_uint8_t*)(&eventD)
                       );
        }

        eventA.value   += 100;
        eventA.time     = eventD.time;
        for(i = 0; i < PSM_CH_AINPUT_LAST; ++i)
        {
            eventA.channel  = i;
            CANCSendMCM( MODULE_MSG_TYPE_EVENT,
                         MODULE_MSG_ID_EVENT_ANALOGUE,
                         sizeof(eventA),
                         (lu_uint8_t*)(&eventA)
                       );
        }
    }

#if 0
    eventA.channel  = 0;
    eventA.value   += 100;
    eventA.time     = CANCGetTime();
    CANCSendMCM( MODULE_MSG_TYPE_EVENT,
                 MODULE_MSG_ID_EVENT_ANALOGUE,
                 sizeof(eventA),
                 (lu_uint8_t*)(&eventA)
               );
#endif

    return SB_ERROR_NONE;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
