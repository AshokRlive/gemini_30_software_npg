/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.swing.spinner;

import java.awt.event.ActionEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.Action;
import javax.swing.JSpinner;

/**
 * A helper class to add mouse wheel support to JSpinner. You can call
 * {@link #installMouseWheelSupport(javax.swing.JSpinner)} to add the support
 * and {@link #uninstallMouseWheelSupport(javax.swing.JSpinner)} to remove the
 * support.
 */
public class SpinnerWheelSupport {

  public static final String CLIENT_PROPERTY_MOUSE_WHEEL_LISTENER = "mouseWheelListener";
  protected static final String ACTION_NAME_INCREMENT = "increment";
  protected static final String ACTION_NAME_DECREMENT = "decrement";


  public static void installMouseWheelSupport(final JSpinner spinner) {
    MouseWheelListener mwl = new MouseWheelListener() {

      @Override
      public void mouseWheelMoved(MouseWheelEvent e) {
        if (spinner == null || !spinner.isEnabled()) {
          return;
        }
        
        int rotation = e.getWheelRotation();
        if (rotation < 0) {
          Action action = spinner.getActionMap().get(ACTION_NAME_INCREMENT);
          if (action != null) {
            action.actionPerformed(new ActionEvent(e.getSource(), 0, ACTION_NAME_INCREMENT));
          }
        
        } else if (rotation > 0) {
          Action action = spinner.getActionMap().get(ACTION_NAME_DECREMENT);
          if (action != null) {
            action.actionPerformed(new ActionEvent(e.getSource(), 0, ACTION_NAME_DECREMENT));
          }
        }
      }
    };
    spinner.addMouseWheelListener(mwl);
    spinner.putClientProperty(CLIENT_PROPERTY_MOUSE_WHEEL_LISTENER, mwl);
  }

  public static void uninstallMouseWheelSupport(final JSpinner spinner) {
    MouseWheelListener mwl = (MouseWheelListener) spinner.getClientProperty(CLIENT_PROPERTY_MOUSE_WHEEL_LISTENER);
    if (mwl != null) {
      spinner.removeMouseWheelListener(mwl);
    }
  }
}
