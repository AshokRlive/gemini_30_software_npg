<?xml version="1.0" encoding="ISO-8859-1" standalone="yes"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
  <head>
    <title>SMC Manual: Section 1b</title>
    <meta name="Author" content="Charles W. Rapp"/>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
    <link rel="stylesheet" type="text/css" href="style.css"/>
    <link rel="stylesheet" type="text/css" media="print" href="print.css"/>
    <base target="SmcManPageFrame"/>
  </head>
  <body>
    <div class="heading">
      <!-- Put Section Title Here -->
      The Task FSM
    </div>
    <!-- Put manual text here. -->
    <div class="main-body">
      <p>
        The Task FSM diagram is:
      </p>
      <center>
        <img src="./Images/SmcTaskMap.png" alt="Task FSM"/>
      </center>
      <p>
        The Task's states are:
      </p>
      <ul>
        <li>
          <code>
            <b>Running:</b>
          </code> the task is actively doing
          work. The task is allowed to run for a specified time
          limit.
        </li>
        <li>
          <code>
            <b>Suspended:</b>
          </code> the task is waiting to
          run again since it is not yet completed.
        </li>
        <li>
          <code>
            <b>Stopped:</b>
          </code> the task has either
          completed running or externally stopped.
        </li>
        <li>
          <code>
            <b>Blocked:</b>
          </code> the uncompleted task is
          externally prevented from running again. It will stay
          in this state until either stopped or unblocked.
        </li>
        <li>
          <code>
            <b>Stopping:</b>
          </code> the task is cleaning up
          allocated resources before entering the stop state.
        </li>
        <li>
          <code>
            <b>Deleted:</b>
          </code> the task is completely
          stopped and all associated resources returned. The task
          may now be safely deleted. This is the FSM end state.
        </li>
      </ul>
      <p>
        Some notes on this FSM:
      </p>
      <ul>
        <li>
          The <code>Task</code> object starts in the
          <code>Suspended</code> state.
        </li>
        <li>
          The transitions match the TaskEventListener interface's
          methods. 
        </li>
        <li>
          The <code>Stopped</code> state is reached when either
          the <code>Running</code> task completes or is
          externally stopped.
        </li>
        <li>
          The <code>Stop</code>, <code>Block</code> and
          <code>Delete</code> transitions do not start in any
          specified state. More on that in
          <a href="./SmcManSec1d.htm">coding up the FSM</a>.
        </li>
      </ul>
      <p>
        Now the problem is: how to take this FSM and put it into
        your code? The first step in that is encoding the FSM in
        the SMC language.
      </p>
      <center>
        Next:
        <a href="./SmcManSec1c.htm">
          Creating an SMC .sm File
        </a>
      </center>
    </div>
    <!-- This is the page's footer. -->
    <div class="footer">
      Copyright � 2000 - 2009. Charles W. Rapp.  All rights reserved.
    </div>
  </body>
</html>
