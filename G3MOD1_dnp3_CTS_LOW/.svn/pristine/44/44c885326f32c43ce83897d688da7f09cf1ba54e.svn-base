/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.impl;

import java.util.ArrayList;
import java.util.Collection;

import javax.swing.ListModel;

import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IPseudoPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IUserChangeable;

/**
 * The Interface IVariablePoints.
 */
public interface ICLogicPointsManager extends IUserChangeable{
  int MAX_SIZE = 100;

  IPseudoPoint[] getAllPoints();

  ListModel<IPseudoPoint> getListModel();

  void removeAll();
  
  void removeAll(Collection<IPseudoPoint> removeItems);

  /**
   * Add points to the end of the list.
   * @param type
   * @param num
   * @param label
   * @return
   */
  Collection<IPseudoPoint> addPoint(VirtualPointType type, boolean userCreated, int num, String ...label);
  
  void setPoints(Collection<IPseudoPoint> points);

  int getSize();

  void addObserver(ICLogicPointsManagerObserver observer);
  
  void removeObserver(ICLogicPointsManagerObserver observer);
  

  /**
   * Observer of logic point manager.
   */
  interface ICLogicPointsManagerObserver {

    void pointsAdded(Collection<IPseudoPoint> addedItems);

    void pointsRemoved(Collection<IPseudoPoint> removedItems);
  }


  IPseudoPoint getByID(int index);

}
