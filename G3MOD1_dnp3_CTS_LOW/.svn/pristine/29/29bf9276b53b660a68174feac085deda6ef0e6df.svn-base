<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>Installation</title>
	<link rel="stylesheet" type="text/css" href="../help.css">
</head>

<body>
<h1>Installation</h1>
<p>To work with 4DIAC you need two main parts: 
<ol>
  <li><b>4DIAC-IDE</b> the 4DIAC engineering tool for IEC 61499</li>
  <li><b>4DIAC-RTE (FORTE)</b> the IEC 61499 run-time environment from 4DIAC</li>
</ol> 
</p>


<a name="4DIAC-IDE"></a>
<h2>4DIAC-IDE</h2>
<p>The installation of 4DIAC-IDE is independent from the used operating system. 
In order to run 4DIAC-IDE you at least require <a href="http://www.oracle.com/technetwork/java/javase/downloads/index.html">Java 8</a>.</p>

<p>To install 4DIAC-IDE you simply download the latest version for your operating system under <a href="http://www.fordiac.org/14.0.html">downloads</a>. 
Unzip it to any desired folder and start the 4DIAC-IDE. It already contains a function block library, some sample applications and also pre-build versions of FORTE. 
If you only want to use available Function Blocks you are ready to go.</p>
		
<p>In the 4DIAC-IDE you can set some preferences under <b>Window &rarr; Preferences &rarr; 4DIAC</b>.
See <a href="../4diacIDE/properties.html">Useful 4DIAC Preferences</a> for detailed instructions.</p>


<div class="infobox"><div>
<h1>Building your own 4DIAC-IDE from source</h1>
<p>Running 4DIAC-IDE from source has the great advantage that you can easily keep up with the developments performed in 
the Mercurial repository. 
In case you want to run 4DIAC-IDE from source follow the Installation steps under 
<a href="../development/installFromSource.html">4DIAC-IDE Installation from Source</a></p>
</div></div>


<a name="FORTE"></a>
<h2>FORTE</h2>
<p>For conducting first experiments with 4DIAC you can use the pre-build version of FORTE which comes along in the runtimes directory of the 4diac-IDE package.
<a href="../tutorials/flipFlop_tutorial.html">Flip Flop Tutorial</a> will direct you making a simple application with 4diac-IDE and using built-in FORTE.
However if you want to develop your own Function Blocks or you want to run FORTE on different control devices, then you have to download and set up FORTE for the specific platform you are using</a>.
<h2 id="BuildFORTE">Setting up FORTE for different platforms</h2>
<ol>		
		<ul type="square">
			<li><a href="./windowsUnix.html">Setting up FORTE for Windows and Unix based systems</a></li>
			<li><a href="./legoMindstormEv3.html">Setting up  FORTE for Lego Mindstorms EV3</a></li>
			<li><a href="./mindstorms.html">Setting up  FORTE for Lego Mindstorms</a></li>
			<li><a href="./raspi.html">Setting up  FORTE for RaspberryPi</a></li>
			<li><a href="./raspberrySPS.html">Setting up  FORTE for RaspberryPi SPS</a></li>
			<li><a href="./wago.html">Setting up FORTE for Wago PFCs SPS</a></li>
			<li><a href="./icnova.html">Setting up FORTE for ICNova</a></li>				
		</ul>
	
</ol>
</p>
</body>
</html>