/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: MemServiceEvent.cpp 13-Feb-2013 11:39:52 pueyos_a $
 *               $HeadURL: U:\G3\RTU\MCM\src\MemoryManager\src\MemServiceEvent.cpp $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Header module for Event storing Memory Service Provider definitions.
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 13-Feb-2013 11:39:52	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   13-Feb-2013 11:39:52  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_1C0EF2F6_9F31_415a_8067_BAF360FC28A8__INCLUDED_)
#define EA_1C0EF2F6_9F31_415a_8067_BAF360FC28A8__INCLUDED_



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "Thread.h"
#include "LockingMutex.h"
#include "Pipe.h"
#include "IMemService.h"
#include "IEventLogManager.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * Class MemServiceEvent
 *
 * This class stores Events in FRAM using file operations.
 *
 * All the entries are stored in a circular file containing fixed-size entries
 * so the file marks which entry was the latest one. Being circular means that
 * the first entry of the file could be actually placed in the middle of the
 * buffer.
 *
 * Latest event entry is always marked as latest, then later removing the
 * previous mark; so a new entry has two writes, one for adding the new entry,
 * and another one for removing previous mark. By doing this operation, we
 * ensure that the first mark found is always valid: if 1st write fails, 2nd
 * mark says the entry may be broken, thus making it invalid.
 *
 * Since some memory drivers doesn't support EOF directly, the entry at the
 * bottom of the file will be marked as EOB (End of Buffer) as well,
 * independently of where the latest entry is located.
 *
 * The object doesn't understands the meaning of any event entry; it only
 * stores entries, sized as specified by setSize(). The maximum amount of
 * entries is calculated from setSize() as well.
 *
 * Event entry size (bytes) must be specified before buffer size (maximum
 * amount of events). When buffer size is not specified, it uses the maximum
 * amount of memory, as stated in memConfig.memorySize; it may leave a small
 * space at the end that not fits an entire event entry.
 */
class MemServiceEvent : public IMemService, private Thread
{

public:
	MemServiceEvent(const lu_char_t *memoryDeviceName);
	virtual ~MemServiceEvent();

	/**
	 * \brief set the size of Event Log elements
	 *
     * Allows to configure the size of the elements of the Event Log. It is
     * possible to specify the configuration in various optional ways:
     * a) memory size + event size:
     *      Specify the size in bytes of the memory and the size of one event
     * b) event size + amount of events:
     *      Specify the maximum amount of events to store and the size of one event
     * c) memory size + event size + amount of events
     *      Like (a) option, but you can limit the amount of events to a lesser value.
	 *
	 * Settings should be set in the order above for any of the 3 options --
	 * e.g.: set the memory size, and then the event size afterwards.
	 *
	 * \param variable Element to set
	 * \param size Size of this element. Depending on the element, could be in bytes or amount of them.
	 */
    virtual void setSize(IMemoryManager::STORAGE_VAR variable, size_t size);
    virtual IMemService::MEMORYERROR init();

    /* NOTE: parameter sizeRead returns the amount of event entries recovered */
    virtual lu_bool_t read(IMemoryManager::STORAGE_VAR variable, lu_uint8_t* value, size_t* sizeRead);
    virtual lu_bool_t write(IMemoryManager::STORAGE_VAR variable, lu_uint8_t* value);

    virtual IMemService::MEMORYERROR invalidate();
    virtual void flush();
    virtual void close();

private:
    Pipe pipeFIFO;              //FIFO pipe to detach event storing from memory writing
    const lu_char_t *MemoryDevice;    //Device access: device location name/specification
    FILE *memDevice;            //Memory device

    /* Note: To protect the high-level Critical Sections (CS) from starving, we
     * use a mechanism for "granting" access to low-level operations: the
     * MasterLockingMutex.
     */
    MasterMutex mainAccessLock;     //Mutex lock to grant memory access to priority operations.
//    Mutex memAccessLock;        //Mutex lock to avoid memory accesses concurrently.

    lu_uint32_t eventSize;      //Size of an event
    lu_uint32_t eventEntrySize; //Size of an event file entry (including last entry mark)
    LogPosCtrlStr eventPos;     //Keeps track of the event entries to help file to be circular.
    fpos_t fInitpos;            //Initial position of file (usually equivalent to SEEK_SET)
    fpos_t fInsertpos;          //file position of latest event entry
    fpos_t fWritepos;           //Write position in file
    EventFileStr prevFileEntry; //previous entry header (keeps a copy in memory)
    lu_bool_t prepared;         //Flag for early initial state. LU_FALSE when device was not yet opened/created.
    lu_bool_t dirty;            //States when there is data pending to be written. LU_TRUE when is pending.

protected:
    /**
     * \brief Thread main loop
     *
     * This function is called automatically when a thread is started
     */
    virtual void threadBody();

private:
    /**
     * \brief Write event to device.
     *
     * This method writes a complete event to non-volatile memory by adding a
     * header to control its position at the circular file.
     *
     * \param value Pointer to a buffer that contains the event to store.
     *
     * \return operation result: LU_FALSE when writing failed.
     */
    lu_bool_t writeToMemory(lu_uint8_t* value);

    /**
     * \brief Update header of an event.
     *
     * This method updates the file status of an event by writing it in is
     * header.
     *
     * \param filePos Address where the header+event is stored
     * \param latest Mark this entry as latest, effectively erasing the previous one
     * \param invalidate Mark this entry as invalid. By default do not.
     * \param eob Mark this entry as End of Buffer. By default do not.
     */
    void updateHeader(fpos_t &filePos,
                        EVENTFILEDEF_LATEST_VAL latest,
                        EVENTFILEDEF_EOB_VAL eob = EVENTFILEDEF_EOB_VAL_NORMAL,
                        EVENTFILEDEF_INVALID_VAL invalidate = EVENTFILEDEF_INVALID_VAL_VALID
                        );

    /**
     * \brief Discard data in memory and starts a new event log.
     *
     * \param FTITLE title entry to show in the log message.
     *
     * \return error: LU_TRUE when access is denied after creation trying
     */
    lu_bool_t discardMemLog(const lu_char_t *FTITLE);

    /**
     * \brief Reset all the internal file tracking to its default.
     */
    void clearFile();

};


#endif // !defined(EA_1C0EF2F6_9F31_415a_8067_BAF360FC28A8__INCLUDED_)

/*
 *********************** End of file ******************************************
 */

