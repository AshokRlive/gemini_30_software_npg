/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.ui.wizards.addmodule;

import java.util.Map;

import javax.swing.JComponent;

import org.apache.log4j.Logger;
import org.netbeans.spi.wizard.WizardController;
import org.netbeans.spi.wizard.WizardException;
import org.netbeans.spi.wizard.WizardPanelProvider;

import com.lucy.g3.rtu.config.module.canmodule.domain.CANModuleManager;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANModule;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleFPM;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleIOM;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModulePSM;
import com.lucy.g3.rtu.config.module.iomodule.manager.IIOModuleGenerator;
import com.lucy.g3.rtu.config.module.shared.domain.ISwitchModule;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

class ModulePanelProvider extends WizardPanelProvider {

  static final String KEY_TYPE_ID_PANEL = "type-id";
  static final String KEY_VIRTUAL_POINT_PANEL = "virtual-point-panel";
  static final String KEY_FPM_CT_RATIO = "fpm-ct-ratio";
  private CANModuleManager manager;
  private ModuleTypeSelection typePanel;
  private SCMOptionsPanel scmPanel;
  private IOMOptionsPanel iomPanel;
  private PSMOptionsPanel psmPanel;
  private FPMOptionsPanel fpmPanel;
  private FPMOptionsPanel2 fpmPanel2;
  private VirtualPointPanel vpPanel;
  private IIOModuleGenerator configurator;


  protected ModulePanelProvider(String title, String[] initialSteps,
      String[] initialStepsDescriptions, CANModuleManager manager,
      IIOModuleGenerator configurator) {
    super(title, initialSteps, initialStepsDescriptions);
    this.manager = manager;
    this.configurator = configurator;
  }

  protected ModulePanelProvider(String title, String singleStep,
      String description, CANModuleManager manager, IIOModuleGenerator configurator) {
    super(title, singleStep, description);
    this.manager = manager;
    this.configurator = configurator;
  }

  @Override
  protected Object finish(@SuppressWarnings("rawtypes") Map settings) throws WizardException {
    /* Create new module */
    MODULE moduleType = (MODULE) settings.get(ModuleTypeSelection.KEY_MODULE_TYPE);
    MODULE_ID moduleId = (MODULE_ID) settings.get(ModuleTypeSelection.KEY_MODULE_ID);
    ICANModule module = manager.addModule(moduleType, moduleId);

    /* Generate control logics */
    generateCLogics(settings, module);

    /* Generate points */
    if (settings.get(VirtualPointPanel.KEY_NAME_GEN_VPOINT_FULL) == Boolean.TRUE) {
      configurator.createFullVirtualPoints(module);
    } else if (settings.get(VirtualPointPanel.KEY_NAME_GEN_VPOINT_DEF) == Boolean.TRUE) {
      configurator.createDefaultVirtualPoints(module);
    }

    /* Generate HMI screens */
    configurator.createScreens(module);

    return settings;
  }

  private void generateCLogics(@SuppressWarnings("rawtypes") Map settings, ICANModule module) {
    if (configurator == null) {
      Logger.getLogger(getClass()).error("configurator is missing");
      return;
    }

    // SCM
    if (module instanceof ISwitchModule) {
      Boolean createSCMSwitchgearLogic = (Boolean) settings.get(SCMOptionsPanel.KEY_CREATE_SCM_SWITCHGEAR_CLOGIC);

      if (createSCMSwitchgearLogic == Boolean.TRUE
          || SCMOptionsPanel.createSCMSwitchgearCLogicDefault) {
        configurator.createSwitchLogic((ISwitchModule) module);
      }
    }

    // IOM
    if (module instanceof ModuleIOM) {
      Boolean createIOMDigitalOutputs = (Boolean) settings.get(IOMOptionsPanel.KEY_CREATE_IOM_DIGITAL_OUTPUTS);
      if (createIOMDigitalOutputs == Boolean.TRUE
          || IOMOptionsPanel.createIOMDigitalOutputsDefault) {
        configurator.createDigitialOutputLogic(module);
      }
    }

    // FPM
    if (module instanceof ModuleFPM) {/*
      Boolean createFPITest = (Boolean) settings.get(FPMOptionsPanel.KEY_CREATE_FPI_TEST);
      if (createFPITest == null) {
        if (FPMOptionsPanel.createFPITest) {
          configurator.createFPITestLogic(((ModuleFPM) module).getChByEnum(FPM_CH_FPI.FPM_CH_FPI_1_TEST));
          configurator.createFPITestLogic(((ModuleFPM) module).getChByEnum(FPM_CH_FPI.FPM_CH_FPI_2_TEST));
        }
      }
      else if (createFPITest) {
        configurator.createFPITestLogic(((ModuleFPM) module).getChByEnum(FPM_CH_FPI.FPM_CH_FPI_1_TEST));
        configurator.createFPITestLogic(((ModuleFPM) module).getChByEnum(FPM_CH_FPI.FPM_CH_FPI_2_TEST));
      }

      Boolean createFPIReset = (Boolean) settings.get(FPMOptionsPanel.KEY_CREATE_FPI_RESET);
      if (createFPIReset == null) {
        if (FPMOptionsPanel.createFPIReset) {
          configurator.createFPIResetLogic(((ModuleFPM) module).getChByEnum(FPM_CH_FPI.FPM_CH_FPI_1_RESET));
          configurator.createFPIResetLogic(((ModuleFPM) module).getChByEnum(FPM_CH_FPI.FPM_CH_FPI_2_RESET));
        }
      }
      else if (createFPIReset) {
        configurator.createFPIResetLogic(((ModuleFPM) module).getChByEnum(FPM_CH_FPI.FPM_CH_FPI_1_RESET));
        configurator.createFPIResetLogic(((ModuleFPM) module).getChByEnum(FPM_CH_FPI.FPM_CH_FPI_2_RESET));
      }

      CTRatio ctRatio = (CTRatio) settings.get(FPMOptionsPanel2.KEY_FPM_CTRATIO);
      if (ctRatio != null) {
        ((ModuleFPM) module).getFpiCfgCh0().setCtratio(ctRatio);
        ((ModuleFPM) module).getFpiCfgCh1().setCtratio(ctRatio);
      }

    */}

    // PSM
    if (module instanceof ModulePSM) {/*
      Boolean createBatteryChargerCLogic = (Boolean) settings.get(PSMOptionsPanel.KEY_CREATE_BATTERY_CHARGER_CLOGIC);
      if (createBatteryChargerCLogic != null) {
        if (createBatteryChargerCLogic) {
          if (PSMOptionsPanel.createBatteryChargerCLogicDefault) {
            configurator.createBatteryChargerLogic(((ModulePSM) module).getChByEnum(PSM_CH_BCHARGER.PSM_CH_BCHARGER_BATTERY_TEST));
          }
        }
        else if (createBatteryChargerCLogic) {
          configurator.createBatteryChargerLogic(((ModulePSM) module).getChByEnum(PSM_CH_BCHARGER.PSM_CH_BCHARGER_BATTERY_TEST));
        }

        Boolean createFanTestCLogic = (Boolean) settings.get(PSMOptionsPanel.KEY_CREATE_FAN_TEST_CLOGIC);
        if (createFanTestCLogic == null) {
          if (PSMOptionsPanel.createFanTestCLogicDefault) {
            configurator.createFanTestLogic(((ModulePSM) module).getFanTestChannel());
          }
        }
        else if (createFanTestCLogic) {
          configurator.createFanTestLogic(((ModulePSM) module).getFanTestChannel());
        }
      }
    */}
  }

  @Override
  protected JComponent createPanel(WizardController controller, String id, @SuppressWarnings("rawtypes") Map settings) {

    if (id.equals(KEY_TYPE_ID_PANEL)) {
      if (typePanel == null) {
        typePanel = new ModuleTypeSelection(manager);
      }
      return typePanel;
    }

    else if (id.equals(MODULE.MODULE_SCM.name()) 
        || id.equals(MODULE.MODULE_DSM.name()) 
        || id.equals(MODULE.MODULE_SCM_MK2.name()) ) {
      if (scmPanel == null) {
        scmPanel = new SCMOptionsPanel();
      }
      return scmPanel;
    }

    else if (id.equals(MODULE.MODULE_IOM.name())) {
      if (iomPanel == null) {
        iomPanel = new IOMOptionsPanel();
      }
      return iomPanel;
    }

    else if (id.equals(MODULE.MODULE_PSM.name())) {
      if (psmPanel == null) {
        psmPanel = new PSMOptionsPanel();
      }
      return psmPanel;
    }

    else if (id.equals(MODULE.MODULE_FPM.name())) {
      if (fpmPanel == null) {
        fpmPanel = new FPMOptionsPanel();
      }
      return fpmPanel;
    }

    else if (id.equals(KEY_FPM_CT_RATIO)) {
      if (fpmPanel2 == null) {
        fpmPanel2 = new FPMOptionsPanel2();
      }
      return fpmPanel2;
    }

    else if (id.equals(KEY_VIRTUAL_POINT_PANEL)) {
      if (vpPanel == null) {
        vpPanel = new VirtualPointPanel();
      }
      return vpPanel;
    } else {
      return null;
    }

  }

}
