<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE reference
  PUBLIC "-//OASIS//DTD DITA Reference//EN" "http://docs.oasis-open.org/dita/v1.1/OS/dtd/reference.dtd" >
<reference xml:lang="en-us" id="BDModbus-Channel">
   <title>Modbus Channel page</title>
   <shortdesc>The Modbus protocol is a messaging structure that is used to establish
      master-slave/client-server communication between intelligent devices.</shortdesc>
   <prolog>
      <metadata/>
   </prolog>
   <refbody>
      <refsyn>You are here: <menucascade>
            <uicontrol>Configuration</uicontrol>
            <uicontrol>Field Devices</uicontrol>
            <uicontrol>Modbus</uicontrol>
         </menucascade> > <i>channel</i></refsyn>
      <section>
         <title>Channel tab</title>
         <table frame="topbot" rowsep="1" colsep="1" id="table-y4n_sjz_1q">
            <tgroup cols="2" align="left">
               <tbody>
                  <row>
                     <entry><uicontrol>Channel Name</uicontrol></entry>
                     <entry>The name that is displayed on the <uicontrol>Modbus</uicontrol>
                        page.</entry>
                  </row>
                  <row>
                     <entry><uicontrol>Modbus Type</uicontrol></entry>
                     <entry>In the dropdown list, select one of the following:<ul id="ul-fkl_1m2_fq">
                           <li><uicontrol>TCP</uicontrol> - The Modbus protocol is used on top of
                              Ethernet-TCP/IP.</li>
                           <li><uicontrol>ASCII</uicontrol> - Each 8-bit byte in a message is sent
                              as two ASCII characters.<p>The main advantage of this mode is that it
                                 allows time intervals of up to one second to occur between
                                 characters without causing an error.</p></li>
                           <li><uicontrol>RTU</uicontrol> - The Modbus protocol is used on top of a
                              serial line with an RS-232, RS-485 or similar physical
                                 interface.<p>Each 8-bit byte in a message contains two 4-bit
                                 hexadecimal characters.</p><p>The main advantage of this mode is
                                 that its greater character density allows better data throughput
                                 than ASCII for the same baud rate.</p></li>
                        </ul></entry>
                  </row>
               </tbody>
            </tgroup>
         </table>
         <p><uicontrol>TCP Connection</uicontrol> table</p>
         <table id="table-jyx_q42_fq">
            <tgroup cols="2">
               <colspec colname="col1" colwidth="*"/>
               <colspec colname="col2" colwidth="*"/>
               <thead>
                  <row>
                     <entry morerows="0" namest="col1" nameend="col1">Option</entry>
                     <entry morerows="0" namest="col2" nameend="col2">Description</entry>
                  </row>
               </thead>
               <tbody>
                  <row>
                     <entry namest="col1" nameend="col1"><uicontrol>IP Address</uicontrol></entry>
                     <entry namest="col2" nameend="col2">The IP address of the Modbus device to
                        communicate with.</entry>
                  </row>
                  <row>
                     <entry namest="col1" nameend="col1"><uicontrol>Port</uicontrol></entry>
                     <entry namest="col2" nameend="col2">The port number that the Modbus device is
                        listening on.<p>The default value is port 502.</p></entry>
                  </row>
                  <row>
                     <entry namest="col1" nameend="col1"><uicontrol>Connection
                        Timeout</uicontrol></entry>
                     <entry namest="col2" nameend="col2">The timeout for a response to a connection
                           request.<p>The default value is 5000 milliseconds.</p></entry>
                  </row>
               </tbody>
            </tgroup>
         </table>
         <p><uicontrol>Serial Link</uicontrol> table</p>
         <table id="table-ovv_bg4_bq">
            <tgroup cols="2">
               <colspec colname="col1" colwidth="*"/>
               <colspec colname="col2" colwidth="*"/>
               <thead>
                  <row>
                     <entry morerows="0" namest="col1" nameend="col1">Option</entry>
                     <entry morerows="0" namest="col2" nameend="col2">Description</entry>
                  </row>
               </thead>
               <tbody>
                  <row>
                     <entry namest="col1" nameend="col1"><uicontrol>Serial Port</uicontrol></entry>
                     <entry namest="col2" nameend="col2">In the list, click a serial port.</entry>
                  </row>
                  <row>
                     <entry namest="col1" nameend="col1"><uicontrol>Baud Rate</uicontrol></entry>
                     <entry namest="col2" nameend="col2" morerows="4">These are view-only
                           settings.<p>You can configure the port settings being used on the RS-232
                           A | RS-232 B | RS-485 tabs on the <xref href="Ports.dita#AIDPorts"
                        />.</p></entry>
                  </row>
                  <row>
                     <entry namest="col1" nameend="col1"><uicontrol>Data Bits</uicontrol></entry>
                  </row>
                  <row>
                     <entry namest="col1" nameend="col1"><uicontrol>Stop Bits</uicontrol></entry>
                  </row>
                  <row>
                     <entry namest="col1" nameend="col1"><uicontrol>Parity</uicontrol></entry>
                  </row>
                  <row>
                     <entry namest="col1" nameend="col1"><uicontrol>Port Mode</uicontrol></entry>
                  </row>
               </tbody>
            </tgroup>
         </table>
      </section>
      <section>
         <title>Channel Settings tab</title>
         <p><b>Settings table</b></p>
         <table id="table-n2b_yfy_cq">
            <tgroup cols="2">
               <colspec colname="col1" colwidth="*"/>
               <colspec colname="col2" colwidth="*"/>
               <thead>
                  <row>
                     <entry morerows="0" namest="col1" nameend="col1"> Option </entry>
                     <entry morerows="0" namest="col2" nameend="col2"> Description </entry>
                  </row>
               </thead>
               <tbody>
                  <row>
                     <entry morerows="0" namest="col1" nameend="col1">
                        <uicontrol>Max Queue Size</uicontrol>
                     </entry>
                     <entry namest="col2" nameend="col2">The maximum request message number in the
                        transmission queue.<p>Enter a value of 0 to allow an unlimited
                           number.</p><p>The default value is 1000.</p></entry>
                  </row>
                  <row>
                     <entry namest="col1" nameend="col1"><uicontrol>Receive Frame
                           Timeout</uicontrol></entry>
                     <entry namest="col2" nameend="col2">The timeout period while waiting for a
                        complete frame after receiving frame synchronization.<p>The default value is
                           1000 milliseconds.</p></entry>
                  </row>
                  <row>
                     <entry namest="col1" nameend="col1"><uicontrol>Channel Response
                           Timeout</uicontrol></entry>
                     <entry namest="col2" nameend="col2">How long to wait for a response to a
                        request that has been transmitted.<p>The default value is 1000
                           milliseconds.</p></entry>
                  </row>
               </tbody>
            </tgroup>
         </table>
      </section>
      <section>
         <title>Sessions tab</title>
      </section>
      <table>
         <tgroup cols="2">
            <colspec colname="col1" colwidth="*"/>
            <colspec colname="col2" colwidth="*"/>
            <thead>
               <row>

                  <entry morerows="0" namest="col1" nameend="col1"> Command </entry>


                  <entry morerows="0" namest="col2" nameend="col2"> Description </entry>

               </row>
            </thead>
            <tbody>
               <row>

                  <entry morerows="0" namest="col1" nameend="col1">
                     <uicontrol>Add Session</uicontrol>
                  </entry>
                  <entry morerows="0" namest="col2" nameend="col2">Adds a new session to the
                     list.</entry>

               </row>
               <row>

                  <entry morerows="0" namest="col1" nameend="col1">
                     <uicontrol>Remove Session</uicontrol>
                  </entry>
                  <entry morerows="0" namest="col2" nameend="col2">Deletes the selected session from
                     the list.</entry>

               </row>
               <row>

                  <entry morerows="0" namest="col1" nameend="col1">
                     <uicontrol>View</uicontrol>
                  </entry>
                  <entry morerows="0" namest="col2" nameend="col2">Displays the selected session for
                     viewing and editing. </entry>

               </row>
            </tbody>
         </tgroup>
      </table>
   </refbody>
</reference>
