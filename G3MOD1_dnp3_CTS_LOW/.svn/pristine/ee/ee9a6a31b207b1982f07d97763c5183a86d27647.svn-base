/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;

import org.apache.log4j.Logger;

import com.g3schema.ns_sdnp3.DNP3LinuxIoT;
import com.g3schema.ns_sdnp3.SDNP3ChnlConfT;
import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceManager;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.connmgr.ConnectionManager;
import com.lucy.g3.rtu.config.protocol.shared.impl.AbstractProtocolChannelConf;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointReadSupport;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointWriteSupport;
import com.lucy.g3.xml.gen.common.DNP3Enum.DNPLINK_NETWORK_TYPE;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LINKCNFM;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LINTCP_MODE;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LINTCP_ROLE;

/**
 * The configuration of {@link SDNPChannel}.
 */
public final class SDNP3ChannelConf extends AbstractProtocolChannelConf {

  // @formatter:off
  public static final String PROPERTY_NETWORKTYPE        = "networkType";
  public static final String PROPERTY_RXFRAMESIZE        = "rxFrameSize";
  public static final String PROPERTY_TXFRAMESIZE        = "txFrameSize";
  public static final String PROPERTY_RXFRAMETIMEOUT     = "rxFrameTimeout";
  public static final String PROPERTY_CONFIRMMODE        = "confirmMode";
  public static final String PROPERTY_CONFIRMTIMEOUT     = "confirmTimeout";
  public static final String PROPERTY_MAXRETRIES         = "maxRetries";
  public static final String PROPERTY_RXFRAGMENTSIZE     = "rxFragmentSize";
  public static final String PROPERTY_TXFRAGMENTSIZE     = "txFragmentSize";
  public static final String PROPERTY_BROADCAST_ENABLED  = "broadcastEnabled";
  public static final String PROPERTY_OFFLINE_POLL_PERIOD = "offlinePollPeriod";
  public static final String PROPERTY_SESSION_MASTER_SUPPORTED = "sessionMasterSupported";
  // @formatter:on

  private Logger log = Logger.getLogger(SDNP3ChannelConf.class);

  private SDNP3Constraints c = SDNP3Constraints.INSTANCE;

  private long rxFrameSize = c.getDefault(SDNP3Constraints.PREFIX_CHANNEL, "rxFrameSize");
  private long txFrameSize = c.getDefault(SDNP3Constraints.PREFIX_CHANNEL, "txFrameSize");
  private long rxFrameTimeout = c.getDefault(SDNP3Constraints.PREFIX_CHANNEL, "rxFrameTimeout");
  private long confirmTimeout = c.getDefault(SDNP3Constraints.PREFIX_CHANNEL, "confirmTimeout");
  private long maxRetries = c.getDefault(SDNP3Constraints.PREFIX_CHANNEL, "maxRetries");
  private long rxFragmentSize = c.getDefault(SDNP3Constraints.PREFIX_CHANNEL, "rxFragmentSize");
  private long txFragmentSize = c.getDefault(SDNP3Constraints.PREFIX_CHANNEL, "txFragmentSize");
  private LU_LINKCNFM confirmMode = LU_LINKCNFM.LU_LINKCNFM_SOMETIMES;
  private boolean broadcastEnabled = false;
  private DNPLINK_NETWORK_TYPE networkType = DNPLINK_NETWORK_TYPE.DNPLINK_NETWORK_TCP_UDP;
  private long offlinePollPeriod = c.getDefault(SDNP3Constraints.PREFIX_CHANNEL, "offlinePollPeriod");

  private final SDNP3ChannelSerialConf serialConfig;
  private final SDNP3ChannelTCPConf tcpConfig;
  private final SDNP3ChannelCommsConf commsConfig;

  
  private boolean sessionMasterSupported;

  public SDNP3ChannelConf(SDNP3Channel channel, String channelName) {
    super(channel, channelName);
    serialConfig = new SDNP3ChannelSerialConf(channel);
    tcpConfig = new SDNP3ChannelTCPConf();
    commsConfig = new SDNP3ChannelCommsConf(this);

    tcpConfig.setIpPort(20000);
    tcpConfig.setDualEndPointIpPort(20000);
    tcpConfig.setMode(LU_LINTCP_MODE.LU_LINTCP_MODE_SERVER);
    tcpConfig.setRole(LU_LINTCP_ROLE.LU_LINTCP_ROLE_OUTSTATION);

    setNetworkType(DNPLINK_NETWORK_TYPE.DNPLINK_NETWORK_TCP_ONLY);
    updateSessionMasterSupported();
    
    tcpConfig.getConnectionManager().addPropertyChangeListener(
        ConnectionManager.PROPERTY_ENABLED,
        new PropertyChangeListener() {

          @Override
          public void propertyChange(PropertyChangeEvent evt) {
            updateSessionMasterSupported();
          }
        });
    
    addPropertyChangeListener(PROPERTY_NETWORKTYPE, new PropertyChangeListener() {
      
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        updateSessionMasterSupported();
      }
    });
  }

  public ConnectionManager getConnectionManager() {
    return tcpConfig.getConnectionManager();
  }
  
  public DNPLINK_NETWORK_TYPE getNetworkType() {
    return networkType;
  }

  public void setNetworkType(DNPLINK_NETWORK_TYPE networkType) {
    if (networkType == null) {
      log.error("networkType cannot be set to null");
      return;
    }

    Object oldValue = getNetworkType();
    this.networkType = networkType;
    firePropertyChange(PROPERTY_NETWORKTYPE, oldValue, networkType);

    // Clear serial port selection.
    if (networkType != DNPLINK_NETWORK_TYPE.DNPLINK_NETWORK_SERIAL)
    {
      serialConfig.releaseSerialPort();
      serialConfig.setActive(false);
    } else {
      serialConfig.setActive(true);
    }

    // Clear comms device selection.
    if (networkType != DNPLINK_NETWORK_TYPE.DNPLINK_NETWORK_COMMS_DEVICE)
    {
      try {
        commsConfig.getCommsDeviceSelection().setCommsDevice(null);
      } catch (PropertyVetoException e) {
        e.printStackTrace();
      }
    }
  }

  public long getRxFrameSize() {
    return rxFrameSize;
  }

  public void setRxFrameSize(long rxFrameSize) throws PropertyVetoException {
    checkBoundary(this.rxFrameSize, rxFrameSize, SDNP3Constraints.PREFIX_CHANNEL, "rxFrameSize");

    Object oldValue = getRxFrameSize();
    this.rxFrameSize = rxFrameSize;
    firePropertyChange(PROPERTY_RXFRAMESIZE, oldValue, rxFrameSize);
  }

  public long getTxFrameSize() {
    return txFrameSize;
  }

  public void setTxFrameSize(long txFrameSize) throws PropertyVetoException {
    checkBoundary(this.txFrameSize, txFrameSize, SDNP3Constraints.PREFIX_CHANNEL, "txFrameSize");

    Object oldValue = getTxFrameSize();
    fireVetoableChange(PROPERTY_TXFRAMESIZE, this.txFrameSize, txFrameSize);
    this.txFrameSize = txFrameSize;
    firePropertyChange(PROPERTY_TXFRAMESIZE, oldValue, txFrameSize);

  }

  public long getRxFrameTimeout() {
    return rxFrameTimeout;
  }

  public void setRxFrameTimeout(long rxFrameTimeout) throws PropertyVetoException {
    checkBoundary(this.rxFrameTimeout, rxFrameTimeout, SDNP3Constraints.PREFIX_CHANNEL, "rxFrameTimeout");

    Object oldValue = getRxFrameTimeout();
    this.rxFrameTimeout = rxFrameTimeout;
    firePropertyChange(PROPERTY_RXFRAMETIMEOUT, oldValue, rxFrameTimeout);
  }

  public long getOfflinePollPeriod() {
    return offlinePollPeriod;
  }

  public void setOfflinePollPeriod(long offlinePollPeriod) {
    Object oldValue = this.offlinePollPeriod;
    this.offlinePollPeriod = offlinePollPeriod;
    firePropertyChange(PROPERTY_OFFLINE_POLL_PERIOD, oldValue, offlinePollPeriod);
  }

  public LU_LINKCNFM getConfirmMode() {
    return confirmMode;
  }

  public void setConfirmMode(LU_LINKCNFM confirmMode) {
    Object oldValue = getConfirmMode();
    this.confirmMode = confirmMode;
    firePropertyChange(PROPERTY_CONFIRMMODE, oldValue, confirmMode);
  }

  public long getConfirmTimeout() {
    return confirmTimeout;
  }

  public void setConfirmTimeout(long confirmTimeout) throws PropertyVetoException {
    checkBoundary(this.confirmTimeout, confirmTimeout, SDNP3Constraints.PREFIX_CHANNEL, "confirmTimeout");

    Object oldValue = getConfirmTimeout();
    this.confirmTimeout = confirmTimeout;
    firePropertyChange(PROPERTY_CONFIRMTIMEOUT, oldValue, confirmTimeout);
  }

  public long getMaxRetries() {
    return maxRetries;
  }

  public void setMaxRetries(long maxRetries) throws PropertyVetoException {
    checkBoundary(this.maxRetries, maxRetries, SDNP3Constraints.PREFIX_CHANNEL, "maxRetries");

    Object oldValue = getMaxRetries();
    this.maxRetries = maxRetries;
    firePropertyChange(PROPERTY_MAXRETRIES, oldValue, maxRetries);
  }

  public long getRxFragmentSize() {
    return rxFragmentSize;
  }

  public void setRxFragmentSize(long rxFragmentSize) throws PropertyVetoException {
    checkBoundary(this.rxFragmentSize, rxFragmentSize, SDNP3Constraints.PREFIX_CHANNEL, "rxFragmentSize");

    Object oldValue = getRxFragmentSize();
    this.rxFragmentSize = rxFragmentSize;
    firePropertyChange(PROPERTY_RXFRAGMENTSIZE, oldValue, rxFragmentSize);
  }

  public long getTxFragmentSize() {
    return txFragmentSize;
  }

  public void setTxFragmentSize(long txFragmentSize) throws PropertyVetoException {
    checkBoundary(this.txFragmentSize, txFragmentSize, SDNP3Constraints.PREFIX_CHANNEL, "txFragmentSize");

    Object oldValue = getTxFragmentSize();
    this.txFragmentSize = txFragmentSize;
    firePropertyChange(PROPERTY_TXFRAGMENTSIZE, oldValue, txFragmentSize);
  }

  public boolean isBroadcastEnabled() {
    return broadcastEnabled;
  }

  public void setBroadcastEnabled(boolean broadcastEnabled) {
    Object oldValue = isBroadcastEnabled();
    this.broadcastEnabled = broadcastEnabled;
    firePropertyChange(PROPERTY_BROADCAST_ENABLED, oldValue, broadcastEnabled);
  }

  public boolean isSessionMasterSupported() {
    return sessionMasterSupported;
  }
  
  private void updateSessionMasterSupported(){
    setSessionMasterSupported(!(tcpConfig.getConnectionManager().isEnabled() == true && isTCP()));
  }
  
  private boolean isTCP() {
    return getNetworkType() == DNPLINK_NETWORK_TYPE.DNPLINK_NETWORK_TCP_ONLY ||
        getNetworkType() == DNPLINK_NETWORK_TYPE.DNPLINK_NETWORK_TCP_UDP;
        
  }
  
  private void setSessionMasterSupported(boolean sessionMasterSupported) {
    Object oldValue = this.sessionMasterSupported;
    this.sessionMasterSupported = sessionMasterSupported;
    firePropertyChange(PROPERTY_SESSION_MASTER_SUPPORTED, oldValue, sessionMasterSupported);
  }

  private void checkBoundary(long oldValue, long newValue, String prefix, String property)
      throws PropertyVetoException {
    c.checkPropertyBoundary(this, oldValue, newValue, prefix, property);
  }

  @Override
  public String getChannelConfSummaryText() {
    return getNetworkType() == DNPLINK_NETWORK_TYPE.DNPLINK_NETWORK_SERIAL ?
        getSerialConfigAsString() : "";
  }

  @Override
  public SDNP3ChannelSerialConf getSerialConf() {
    return serialConfig;
  }

  @Override
  public SDNP3ChannelTCPConf getTcpConf() {
    return tcpConfig;
  }

  public SDNP3ChannelCommsConf getCommsConf() {
    return commsConfig;
  }

  public void writeToXML(SDNP3ChnlConfT xml, VirtualPointWriteSupport support) {
    xml.enabled.setValue(getChannel().isEnabled());
    xml.uuid.setValue(getChannel().getUUID());
    
    xml.chnlName.setValue(getChannelName());
    xml.rxFrameSize.setValue(getRxFrameSize());
    xml.txFrameSize.setValue(getTxFrameSize());
    xml.rxFrameTimeout.setValue(getRxFrameTimeout());
    xml.confirmTimeout.setValue(getConfirmTimeout());
    xml.confirmMode.setValue(getConfirmMode().name());
    xml.maxRetries.setValue(getMaxRetries());
    xml.rxFragmentSize.setValue(getRxFragmentSize());
    xml.txFragmentSize.setValue(getTxFragmentSize());
    xml.broadcast.setValue(isBroadcastEnabled());
    xml.offlinePollPeriodMs.setValue(getOfflinePollPeriod());
    xml.networkType.setValue(getNetworkType().name());

    /* Linux IO */
    DNP3LinuxIoT xml_linuxIo = xml.linuxIo.append();

    /* Export serial config only if it is selected*/
    if(getNetworkType() == DNPLINK_NETWORK_TYPE.DNPLINK_NETWORK_SERIAL)
      getSerialConf().writeToXML(xml_linuxIo.serial.append(), support.getResult());
    
    /* Export other config always in order to keep all the configuration*/
    if(getNetworkType() == DNPLINK_NETWORK_TYPE.DNPLINK_NETWORK_COMMS_DEVICE)
      getCommsConf().writeToXML(support.getResult(), xml_linuxIo.commsDevice.append());
    
    /* Export tpc always to keep all settings.*/
    getTcpConf().writeToXML(xml_linuxIo.tcp.append(), support);
  }

  public void readFromXML(SDNP3ChnlConfT xml, PortsManager portManager, CommsDeviceManager commsDevMgr,
      VirtualPointReadSupport support) throws PropertyVetoException {
    if (xml.enabled.exists()) {
      getChannel().setEnabled(xml.enabled.getValue());
    }
    
    if (xml.uuid.exists()) 
      getChannel().setUUID(xml.uuid.getValue());
    
    /* Channel Config */
    setNetworkType(DNPLINK_NETWORK_TYPE.valueOf(xml.networkType.getValue()));
    setChannelName(xml.chnlName.getValue());
    setTxFragmentSize(xml.txFragmentSize.getValue());
    setRxFragmentSize(xml.rxFragmentSize.getValue());
    setTxFrameSize(xml.txFrameSize.getValue());
    setRxFrameSize(xml.rxFrameSize.getValue());
    setRxFrameTimeout(xml.rxFrameTimeout.getValue());
    setConfirmMode(LU_LINKCNFM.valueOf(xml.confirmMode.getValue()));
    setConfirmTimeout(xml.confirmTimeout.getValue());
    setMaxRetries(xml.maxRetries.getValue());
    setBroadcastEnabled(xml.broadcast.getValue());
    if (xml.offlinePollPeriodMs.exists()) {
      setOfflinePollPeriod(xml.offlinePollPeriodMs.getValue());
    }

    /* Linux IO - Serial */
    DNP3LinuxIoT xml_linuxIo = xml.linuxIo.first();
    if (xml_linuxIo.serial.exists()) {
      getSerialConf().readFromXML(xml_linuxIo.serial.first(), portManager);
    }

    /* Linux IO - TCP */
    if (xml_linuxIo.tcp.exists()) {
      getTcpConf().readFromXML(xml_linuxIo.tcp.first(), support);
    }

    /* Linux IO - CommsDevice */
    if (xml_linuxIo.commsDevice.exists()) {
      getCommsConf().readFromXML(commsDevMgr, xml_linuxIo.commsDevice.first());
    }
  }

  @Override
  public Object getLinkType() {
    return getNetworkType();
  }

  @Override
  public void setLinkType(Object type) {
    setNetworkType((DNPLINK_NETWORK_TYPE) type);
  }

}
