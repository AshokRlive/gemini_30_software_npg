/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.application.actions;

import java.util.Collections;
import java.util.List;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.ui.actions.Clipboard;
import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.actions.ActionFactory;
import org.fordiac.ide.application.commands.PasteCommand;
import org.fordiac.ide.application.editparts.FBNetworkEditPart;
import org.fordiac.ide.application.editparts.UISubAppNetworkEditPart;
import org.fordiac.ide.gef.editparts.IDiagramEditPart;

/**
 * The Class PasteEditPartsAction.
 */
public class PasteEditPartsAction extends SelectionAction {

	private static final String PASTE_TEXT = "Paste";

	/**
	 * Instantiates a new paste edit parts action.
	 * 
	 * @param editor the editor
	 */
	public PasteEditPartsAction(IWorkbenchPart editor) {
		super(editor);
	}

	@Override
	protected boolean calculateEnabled() {
		IDiagramEditPart part = getFBNetworkEditPart();
		return null != part && !getClipboardContents().isEmpty();
	}

	protected Command createPasteCommand() {
		IDiagramEditPart part = getFBNetworkEditPart();
		if(null != part){
			return new PasteCommand(getClipboardContents(), part);			
		}
		return new CompoundCommand();
	}

	@SuppressWarnings("rawtypes")
	private List getClipboardContents() {
		List list = Collections.EMPTY_LIST;
		Object obj = Clipboard.getDefault().getContents();
		if (obj instanceof List) {
			list = (List) obj;
		}
		return list;
	}

	@Override
	protected void init() {
		setId(ActionFactory.PASTE.getId());
		setText(PASTE_TEXT);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		execute(createPasteCommand());
	}

	@SuppressWarnings("rawtypes")
	protected IDiagramEditPart getFBNetworkEditPart() {
		List selection = getSelectedObjects();
		if (selection != null && selection.size() >= 1 && selection.get(0) instanceof EditPart) {
			EditPart part = (EditPart)selection.get(0);
			while(null != part){
				if(isCompatiblePasteTargetEditPart(part)){
					return (IDiagramEditPart)part;
				}
				part = part.getParent();
			}
		}		
		return null;
	}

	protected boolean isCompatiblePasteTargetEditPart(EditPart part) {
		return (part instanceof FBNetworkEditPart) || (part instanceof UISubAppNetworkEditPart);
	}

}
