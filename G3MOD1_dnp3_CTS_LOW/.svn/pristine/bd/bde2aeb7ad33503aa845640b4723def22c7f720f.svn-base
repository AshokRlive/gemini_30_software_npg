/*! \file
*******************************************************************************
*       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
*                       - Automation Dept.
*******************************************************************************
*    FILE NAME:
*               $Id: SCMAppBootProgNXP.c 7889 2015-01-23 09:23:49Z fryers_j $
*               $HeadURL: http://10.11.0.6:81/svn/gemini_30_software/trunk/Development/G3/RTU/slaveBoards/SCMBoard/AppBootProg/src/SCMAppBootProgNXP.c $
*
*    DESCRIPTION:
*       \brief  Interprocess Communication Module: implementation
*
*       Detailed description of this template module
*
*    CURRENT REVISION
*
*               $Rev:: $: (Revision of last commit)
*               $Author:: $: (Author of last commit)
*       \date   $Date:: $: (Date of last commit)
*******************************************************************************
*/


/*
*******************************************************************************
*   Copyright
*       This Document is the property of Lucy Switchgear Ltd.
*       It must not be reproduced, in whole or in part, or otherwise
*       disclosed without prior consent in writing from
*       Lucy Switchgear Ltd.
*
*******************************************************************************
*/

/*
*******************************************************************************
* INCLUDES - Standard Library
*******************************************************************************
*/


/*
*******************************************************************************
* INCLUDES - Project Specific
*******************************************************************************
*/

#include "system_LPC17xx.h"
#include "lpc17xx_pinsel.h"
#include "debug_frmwrk.h"
#include "systemTime.h"
#include "CANProtocol.h"
#include "CANProtocolFraming.h"
#include "CANProtocolCodec.h"

#include "systemStatus.h"
#include "systemAlarm.h"
#include "SysAlarm/SysAlarmSystemEnum.h"
#include "versions.h"
#include "svnRevision.h"

/* Board specific includes */
#include "CANProtocolDecoder.h"
#include "BoardStatusManager.h"

#include "IOManager.h"
#include "BoardIOMap.h"
#include "NVRam.h"

#include "SlaveBinImage.h"
#include "SlaveBinImageDef.h"

/*
*******************************************************************************
* LOCAL - Private Definitions
*******************************************************************************
*/

#define DEVICE    MODULE_SCM_MK2

#define SERIAL    0x22222222

/*
*******************************************************************************
* LOCAL - Private Types
*******************************************************************************
*/

/*
*******************************************************************************
* LOCAL - Private Constants
*******************************************************************************
*/

/*
*******************************************************************************
* LOCAL - Private Variables
*******************************************************************************
*/



/*
*******************************************************************************
* EXPORTED - Public Variables
*******************************************************************************
*/

static CANFramingMapStr CANMapping =
{
    GPIO_INVALID_PIN ,/* peripheralCAN     */
    GPIO_INVALID_PIN ,/* peripheralGPIO    */
    PINSEL_PORT_0    ,/* gpioPortBase      */
    PINSEL_PIN_22    ,/* CanTxPin          */
    PINSEL_FUNC_3    ,/* CanTxConfigurePin */
    PINSEL_PIN_21    ,/* CanRxPin          */
    PINSEL_FUNC_3    ,/* CanRxConfigurePin */
    LPC_CAN1_BASE    ,/* CanBase           */
    CAN_IRQn          /* CanInt            */
};



const volatile __attribute__ ((section (".exebindata"))) SlaveBinImageHeaderStr slaveBinImageHeader =
{
		0x0,  // headerCRC32 CRC32
		SLAVE_IMAGE_MAGIC_SYNC,
		0x0,  // preHeaderCRC32 CRC32
		0x0, // postHeaderCRC32 CRC32
		SLAVE_IMAGE_VERSION,
		DEVICE,
		(lu_uint32_t)&__rom_size,
		(lu_uint32_t)&__rom_start, // APP Image Address
		SLAVE_BIN_ARCH_ARM_NXP_LPC1768,
		SLAVE_IMAGE_TYPE_APP_BOOT_PROGRAM,
		{SCM_MK2_FEATURE_VERSION_MAJOR, SCM_MK2_FEATURE_VERSION_MINOR},
		{MODULE_SYSTEM_API_VER_MAJOR, MODULE_SYSTEM_API_VER_MINOR},
		{SCM_MK2_SOFTWARE_VERSION_RELTYPE, {SCM_MK2_SOFTWARE_VERSION_MAJOR, SCM_MK2_SOFTWARE_VERSION_MINOR}, SCM_MK2_SOFTWARE_VERSION_PATCH},
		_SVN_REVISION,
		{
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0
		}
};

/*
*******************************************************************************
* LOCAL - Function Prototypes
*******************************************************************************
*/


/*!
*******************************************************************************
* EXPORTED:
*   \name
*******************************************************************************
* @{
*/

int main(void)
{
	lu_uint8_t        deviceID;
	SB_ERROR		  retError;
    SUIRQPriorityStr  CANPriority;
    SUIRQPriorityStr  TSynchPriority;
    SUIRQPriorityStr  timePriority;
    NVRAMInfoStr *nvramInfoBlkPtr;
    NVRamInitParamsStr nvRamInitParams;
    SlaveBinImageHeaderStr *slaveBinImageHeaderPtr;
    SlaveBinImageHeaderStr *bootloaderBinImageHeaderPtr;
    lu_uint8_t             *bootloaderSrcPtr;
    lu_uint8_t             *bootloaderDstPtr;

    /* System specific initialisation (set PLL and CPU speed) */

    slaveBinImageHeaderPtr = (SlaveBinImageHeaderStr *)&slaveBinImageHeader;

    SystemInit();

    /* Enable debug serial port (COM0) */
    debug_frmwrk_init();

    /* Initialise IRQ priorities */
    CANPriority.group          = SU_IRQ_GROUP_0;
    CANPriority.subPriority    = SU_IRQ_SUB_PRIORITY_0;
    TSynchPriority.group       = SU_IRQ_GROUP_7;
    TSynchPriority.subPriority = SU_IRQ_SUB_PRIORITY_3;
    timePriority.group         = SU_IRQ_GROUP_7;
    timePriority.subPriority   = SU_IRQ_SUB_PRIORITY_0;

    /* Initialise IRQ priority */
    SUInitIRQPriority();

    /* Initialise System Time */
    STInit(timePriority);

    /* Init SysAlarm components */
    SysAlarmInitEvents();

    /* Set board serial number */
    SSSetSerial(SERIAL + deviceID);
    SSSetSupplierId(0);

    /* Set module status */
    SSSetBStatus(MODULE_BOARD_STATUS_APP_BOOTL_INIT);

    /* reset Alarm buffer pointers */
    ClearAlarmEvents();

    /* Set Application SVN Revision */
    SSSetAppSvnRevision(slaveBinImageHeaderPtr->svnRevsion);

    /* Set Module System API */
    SSSetAppSystemAPIMajor(slaveBinImageHeaderPtr->systemAPI.major);
    SSSetAppSystemAPIMinor(slaveBinImageHeaderPtr->systemAPI.minor);

    /* Set Module feature revision */
    SSSetFeatureRevisionMajor(slaveBinImageHeaderPtr->featureRevision.major);
    SSSetFeatureRevisionMinor(slaveBinImageHeaderPtr->featureRevision.minor);

    /* Set Module software version */
    SSSetAppSoftwareReleaseType(slaveBinImageHeaderPtr->softwareVersion.relType);
    SSSetAppSoftwareVersionMajor(slaveBinImageHeaderPtr->softwareVersion.version.major);
    SSSetAppSoftwareVersionMinor(slaveBinImageHeaderPtr->softwareVersion.version.minor);
    SSSetAppSoftwareVersionPatch(slaveBinImageHeaderPtr->softwareVersion.patch);

    /* Initialise system specific IOManager - GPIO,etc IOMap / IO Channel map */
    IOManagerInit();

    /* Visual Indication of an error */
	retError = IOManagerSetValue(IO_ID_LED_CTRL_OK_GREEN, 0);
	retError = IOManagerSetValue(IO_ID_LED_CTRL_OK_RED, 1);

#if BUILD != Release
    /* Check image integrity of self first!!! */
	if (SlaveBinImageCheckIntegrity(slaveBinImageHeaderPtr->appImageAddress) != SB_ERROR_NONE)
#endif
	{
		bootloaderSrcPtr                = slaveBinImageHeaderPtr->appImageAddress;
		bootloaderSrcPtr               += slaveBinImageHeaderPtr->imageSize;

		/* Set srcAddr to bootloader image to program (Follow the binary image of the app!) */
		if (SlaveBinImageCheckIntegrity(bootloaderSrcPtr) == SB_ERROR_NONE)
		{
			/* Integrity of image stored in Flash after this application is OK */
			BoardStatusSetBootloaderSrc(bootloaderSrcPtr, slaveBinImageHeaderPtr);
		}
	}

	bootloaderDstPtr            = 0;
    bootloaderBinImageHeaderPtr = (SlaveBinImageHeaderStr *)SLAVE_IMAGE_BASE_OFFSET;

    /* Bootloader API */
	SSSetBootSystemAPIMajor(0);
	SSSetBootSystemAPIMajor(0);

	/* Bootloader software version */
	SSSetBootSoftwareReleaseType(VERSION_TYPE_UNKNOWN);
	SSSetBootSoftwareVersionMajor(0);
	SSSetBootSoftwareVersionMinor(0);
	SSSetBootSoftwareVersionPatch(0);

    if (SlaveBinImageCheckIntegrity((lu_uint8_t *)bootloaderDstPtr) == SB_ERROR_NONE)
    {
    	/* Set Bootloader SVN Revision */
    	SSSetBootSvnRevision(bootloaderBinImageHeaderPtr->svnRevsion);

    	/* Bootloader API */
    	SSSetBootSystemAPIMajor(bootloaderBinImageHeaderPtr->systemAPI.major);
    	SSSetBootSystemAPIMinor(bootloaderBinImageHeaderPtr->systemAPI.minor);

    	/* Bootloader software version */
    	SSSetBootSoftwareReleaseType(bootloaderBinImageHeaderPtr->softwareVersion.relType);
    	SSSetBootSoftwareVersionMajor(bootloaderBinImageHeaderPtr->softwareVersion.version.major);
    	SSSetBootSoftwareVersionMinor(bootloaderBinImageHeaderPtr->softwareVersion.version.minor);
    	SSSetBootSoftwareVersionPatch(bootloaderBinImageHeaderPtr->softwareVersion.patch);
    }

    /* Initialise the I2C for the NVRAM */
    nvRamInitParams.ioIdBatTemp          = IO_ID_NA;
    nvRamInitParams.ioIdBatteryIdNVRam   = IO_ID_NA;
    nvRamInitParams.ioIdBatteryDataNVRam = IO_ID_NA;
    retError = NVRamInit(&nvRamInitParams);

    /* Read Board Identity NVRAM */
    retError = NVRamIndenityGetMemoryPointer(NVRAM_ID_BLK_INFO, (lu_uint8_t **)&nvramInfoBlkPtr);

    if (retError != SB_ERROR_NONE)
    {
    	/* Major error - no factory info is available */
		SysAlarmSetLogEvent(LU_TRUE,
							SYS_ALARM_SEVERITY_WARNING,
							SYS_ALARM_SUBSYSTEM_SYSTEM,
							SYSALC_SYSTEM_PRIMARY_FACTORY_NVRAM,
							0
						   );

    	/* Read Backup Board Identity from APP NVRAM */
    	retError = NVRamApplicationGetMemoryPointer(NVRAM_APP_BLK_INFO, (lu_uint8_t **)&nvramInfoBlkPtr);
    }

    if (retError != SB_ERROR_NONE)
    {
    	/* Major error - no factory info is available */
		SysAlarmSetLogEvent(LU_TRUE,
							SYS_ALARM_SEVERITY_ERROR,
							SYS_ALARM_SUBSYSTEM_SYSTEM,
							SYSALC_SYSTEM_FACTORY_NVRAM,
							0
						   );
    }
    else
    {
    	/* Set serial no / , etc */
    	SSSetSerial(nvramInfoBlkPtr->serialNumber);
    	SSSetSupplierIdStr(nvramInfoBlkPtr->supplierId);

    	/* Set feature revision */
    	SSSetFeatureRevisionMajor(nvramInfoBlkPtr->moduleFeatureMajor);
    	SSSetFeatureRevisionMinor(nvramInfoBlkPtr->moduleFeatureMinor);
    }

    /* Get board ID for CAN codec */
    retError = boardIOGetModuleID(&deviceID);

    /* Initialize CAN Codec */
    CANCodecInit( SystemCoreClock,
                  CAN_BUS_BAUDRATE,
                  DEVICE,
                  deviceID,
                  &CANMapping,
                  CANPriority,
                  TSynchPriority,
                  BoardCANProtocolDecoder
                );

    /* Register CAN decoder */
    SysAlarmInitCanDecoder();

    /* Initialise CAN filter for IOManager */
    IOManagerInitCANFilter();

    /* Call scheduler executive */
    BoardStatusManagerExecutive();

    return 0;
}

/*!
*******************************************************************************
* @}
*/


/*!
*******************************************************************************
* LOCAL:
*   \name Private Functions
*******************************************************************************
* @{
*/


/*!
*******************************************************************************
* @}
*/


/*
*********************** End of file *******************************************
*/
