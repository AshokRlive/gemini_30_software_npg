/*******************************************************************************
 * Copyright (c) 2007 - 2010 4DIAC - consortium.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 *******************************************************************************/
package org.fordiac.ide.deployment.ui.perspectives;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;
import org.eclipse.ui.console.IConsoleConstants;

/**
 * The Class DeploymentPerspective.
 */
public class DeploymentPerspective implements IPerspectiveFactory {

	/** The factory. */
	private IPageLayout factory;

	/**
	 * Instantiates a new deployment perspective.
	 */
	public DeploymentPerspective() {
		// empty constructor
	}

	/*
	 * (non-Javadoc)
	 * 
	 * 
	 * 
	 * 
	 * @seeorg.eclipse.ui.IPerspectiveFactory#createInitialLayout(org.eclipse.ui.
	 * IPageLayout)
	 */
	public void createInitialLayout(final IPageLayout factory) {
		this.factory = factory;
		factory.setEditorAreaVisible(false);
		addViews();
		addActionSets();
		addNewWizardShortcuts();
		addPerspectiveShortcuts();
		addViewShortcuts();
	}

	/**
	 * Adds the views.
	 */
	private void addViews() {

		// IFolderLayout bottom = factory.createFolder("bottomLeft",
		// //$NON-NLS-1$
		// IPageLayout.BOTTOM, 0.75f, factory.getEditorArea());
		// bottom.addView(IPageLayout.ID_PROP_SHEET); // NON-NLS-1
		IFolderLayout topRight = factory.createFolder("topRight", //$NON-NLS-1$
				IPageLayout.RIGHT, 0.5f, factory.getEditorArea());
		topRight.addView("org.fordiac.ide.deployment.ui.views.Output"); //$NON-NLS-1$

		IFolderLayout bottomRight = factory.createFolder("bottomRight", //$NON-NLS-1$
				IPageLayout.BOTTOM, 0.65f, "topRight");
		bottomRight.addView(IConsoleConstants.ID_CONSOLE_VIEW);

		IFolderLayout topLeft = factory.createFolder("topLeft", //$NON-NLS-1$
				IPageLayout.LEFT, 0.5f, factory.getEditorArea());
		topLeft
				.addView("org.fordiac.ide.deployment.ui.views.DownloadSelectionTreeView");//$NON-NLS-1$

		IFolderLayout bottomLeft = factory.createFolder("bottomLeft",
				IPageLayout.BOTTOM, 0.75f, "topLeft");
		bottomLeft
				.addView("org.fordiac.ide.runtime.views.RuntimeLauncherView");
		
	}
	

	/**
	 * Adds the action sets.
	 */
	private void addActionSets() {
		// currently not used
	}

	/**
	 * Adds the perspective shortcuts.
	 */
	private void addPerspectiveShortcuts() {
		factory.addPerspectiveShortcut("org.fordiac.ide.SystemPerspective");
		factory
				.addPerspectiveShortcut("org.fordiac.ide.deployment.ui.perspectives.DeploymentPerspective"); //$NON-NLS-1$
		factory
				.addPerspectiveShortcut("org.fordiac.ide.fbt.typemanagement.perspective1");
	}

	/**
	 * Adds the new wizard shortcuts.
	 */
	private void addNewWizardShortcuts() {
		factory
				.addNewWizardShortcut("org.fordiac.systemmanagement.ui.wizard.NewSystemWizard");
		factory
				.addNewWizardShortcut("org.fordiac.systemmanagement.ui.wizard.NewApplicationWizard");

		factory
				.addNewWizardShortcut("org.fordiac.ide.fbt.typemanagement.wizards.NewFBTypeWizard");
	}

	/**
	 * Adds the view shortcuts.
	 */
	private void addViewShortcuts() {
		// currently not used
	}

}
