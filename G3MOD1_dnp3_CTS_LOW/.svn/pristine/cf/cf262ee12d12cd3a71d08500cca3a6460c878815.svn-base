/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: MCMMonitorAIChannel.cpp 23 Dec 2013 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/MCMIOModule/src/MCMMonitorAIChannel.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       This is an implementation of an MCM Analogue Input Channel that depends
 *       on MCM Monitor communication to get its value and status.
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 23 Dec 2013 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   23 Dec 2013   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MCMMonitorAIChannel.h"
#include "timeOperations.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define MCMMON_AIPOLLDELAY  10000

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

MCMMonitorAIChannel::MCMMonitorAIChannel(IChannel::ChannelIDStr ID,
                                        lu_uint8_t monID,
                                        MonitorProtocolManager& monProtocol,
                                        lu_uint32_t samplingTime_ms
                                        ) :
                                            MCMChannel(ID.type, ID.id),
                                            MonitorObserver(MON_CHANNEL_TYPE_AI, monID),
                                            monitorProtocol(monProtocol),
                                            timeManager(TimeManager::getInstance()),
                                            monitorID(monID),
                                            samplingTime(samplingTime_ms),
                                            currentValue(0),
                                            waitingReply(LU_FALSE),
                                            accessLock(LU_FALSE)   //not shared
{
    monitorProtocol.attach(this);
}

MCMMonitorAIChannel::~MCMMonitorAIChannel()
{
    monitorProtocol.detach(this);
    stopUpdate();
}


void MCMMonitorAIChannel::update(TimeManager::TimeStr &timeNow, const lu_bool_t forced)
{
    //NOTE: This is a point update request, namely from a tickEvent() method
    if(samplingTime == 0)
    {
        //non-sampling channels are pollable-only
        if(forced == LU_TRUE)
        {
            //Ask for channel update
            flags.restart = LU_TRUE;
            waitingReply = LU_TRUE;
            monitorProtocol.send_req_ai_msg(monitorID);
            timeManager.getTime(sampleTime);
        }
        return;
    }
    /* NOTE: Monitor channels are always updated, even if they are not configured
     * for that in channelAnalogConf.enable (XML Configuration)
     */
    if( (forced == LU_TRUE) ||
        (timespec_elapsed_ms(&(sampleTime.relatime), &(timeNow.relatime)) > samplingTime)
        )
    {
        //Ask for channel update only when sampling time reached
        monitorProtocol.send_req_ai_msg(monitorID);
        timeManager.getTime(sampleTime);
    }
}


IOM_ERROR MCMMonitorAIChannel::read(IChannel::ValueStr& valuePtr)
{
    if(valuePtr.dataPtr == NULL)
    {
        return IOM_ERROR_PARAM;
    }

    LockingMutex lMutex(mutex);

    /* Use the last updated value */
    *(valuePtr.dataPtr) = currentValue;
    valuePtr.dataPtr->setTime(timeStamp);
    valuePtr.flags = flags;
    if( (samplingTime == 0) && (waitingReply == LU_TRUE) )
    {
        valuePtr.flags.restart = LU_FALSE;   //not polled yet: set as offline
    }

    return IOM_ERROR_NONE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

void MCMMonitorAIChannel::update(lu_float32_t value, lu_uint8_t flagStatus)
{
    //NOTE: This is a point update event, usually from an incoming message from MCM Monitor
    IChannel::FlagsStr newFlags = flags;

    //Prevent updating -- use the same value for all observers
    MasterLockingMutex lMutex(accessLock, LU_TRUE);

//    LockingMutex lMutex(mutex);

    //A message has been received:
    newFlags.online = !(flagStatus & XMSG_VALUE_FLAG_OFF_LINE);
    newFlags.alarm = (flagStatus & XMSG_VALUE_FLAG_ALARM);
    newFlags.outRange = (flagStatus & XMSG_VALUE_FLAG_OUT_OF_RANGE);
    newFlags.filtered = (flagStatus & XMSG_VALUE_FLAG_FILTERED);

    if( (currentValue != value) || (newFlags != flags) )
    {
        //value or status change: store and notify
        timeManager.getTime(timeStamp);
        currentValue = value;
        flags = newFlags;
        flags.restart = LU_FALSE;   //not the initial status anymore

        updateObservers();  //Notify
    }
}


void MCMMonitorAIChannel::updateOnline(lu_bool_t online)
{
    update(currentValue, (online==LU_TRUE) ? 0 : XMSG_VALUE_FLAG_OFF_LINE);
}


/*
 *********************** End of file ******************************************
 */
