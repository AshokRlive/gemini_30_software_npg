/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: SwitchCANChannelSCM2.h 8 Sep 2016 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/RTU/MCM/MCMApp/src/CANIOModule/include/channel/SwitchCANChannelSCM2.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       SCM_MK2-specific Switch Out Channel public interface.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 8 Sep 2016 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   8 Sep 2016   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef SWITCHCANCHANNELSCM2_H__INCLUDED
#define SWITCHCANCHANNELSCM2_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "SwitchCANChannel.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief SCM_MK2-specific switch operation channel
 *
 * This is the specific implementation for operating digital output CAN channels
 * specialised in switch operations.
 *
 * Note that this channel uses the same CAN message as DSM for configuration.
 */
class SwitchCANChannelSCM2: public SwitchCANChannel
{
public:
    /* Switch Channel configuration parameters */
    struct SwitchConf
    {
    public:
        lu_bool_t enable;
        lu_uint16_t overrunMS;
        lu_uint16_t opTimeoutS;
        lu_uint32_t inhibit;
        lu_uint32_t motorOverDrive_ms;      //Forces actuator past limit switch
        lu_uint32_t motorReverseDrive_ms;   //Reverts overdrive to release tension
    public:
        SwitchConf() :
                    enable(LU_FALSE),
                    overrunMS(0),
                    opTimeoutS(0),
                    inhibit(0),
                    motorOverDrive_ms(0),
                    motorReverseDrive_ms(0)
        {};
        void reset()
        {
            *this = SwitchConf();   //re-initialise
        };
    };

public:
    /**
     * \brief Custom constructor
     */
    SwitchCANChannelSCM2(lu_uint16_t ID = 0, CANIOModule *board = NULL) :
                                                SwitchCANChannel(ID, board)
    {};

    /**
     * \brief Configure the channel
     *
     * The configuration is stored locally.
     *
     * \param conf Reference to the channel configuration
     *
     * \return error Code
     */
    void setConfig(SwitchCANChannelSCM2::SwitchConf &conf){this->config = conf;}

    /**
     * \brief Get channel configuration
     *
     * \param configuration where the configuration is saved
     *
     * \return error Code
     */
    IOM_ERROR getConfiguration(SwitchCANChannelSCM2::SwitchConf &configuration);

    /* TODO: pueyos_a - [SCM2] Add motor jogging operation */

protected:
    /* === Inherited from SwitchCANChannel === */
    virtual IOM_ERROR configure();

private:
    SwitchCANChannelSCM2::SwitchConf config;
};


#endif /* SWITCHCANCHANNELSCM2_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
