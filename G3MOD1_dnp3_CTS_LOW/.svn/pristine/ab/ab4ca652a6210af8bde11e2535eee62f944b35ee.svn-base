/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.validation;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.apache.log4j.Logger;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.common.base.Strings;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.ValidationResultModel;
import com.jgoodies.validation.view.ValidationResultViewFactory;

/**
 * Abstract class for validating the buffered value in a
 * {@linkplain PresentationModel}.
 */
public abstract class BufferValidator {

  static Logger log = Logger.getLogger(BufferValidator.class);

  protected final PresentationModel<?> target;


  public BufferValidator(PresentationModel<?> target) {
    super();
    this.target = target;
  }

  /**
   * Validates the buffered value in presentation mode and store the results in
   * the given result object.
   *
   * @param result
   *          non-null object for storing validation result.
   */
  public abstract void validate(ValidationResult result);

  // ================== Help Methods ===================

  public static String checkStringEmpty(Object target) {
    String result = null;
    if (target == null || target.toString().isEmpty()) {
      result = " is empty";
    }

    return result;
  }

  /**
   * Validate a property value and check if it is in the specified boundary[min,
   * max]. If not, add a Error message to the given validation support. <br>
   * <b>Note: </b>Boundary is defined in Double
   *
   * @param support
   *          record all validated result
   * @param target
   *          target to be validated
   * @param property
   *          the property name
   * @param min
   *          minimum of the property value
   * @param max
   *          maximum of the property value
   */
  public static void validatePropertyBoundary(ValidationResult support,
      PresentationModel<?> target,
      String property,
      double min, double max) {

    // Check if property exist in target model
    if (target.getBufferedModel(property) == null) {
      log.info("Property not found: " + property);
      return;
    }

    // Get value of the property
    Object value = target.getBufferedValue(property);
    boolean valid = false;

    // Check if value in boundary
    try {
      double targetNum = ((Number) value).doubleValue();
      if (targetNum >= min && targetNum <= max) {
        valid = true;
      }
    } catch (Exception e) {
    }

    if (valid == false) {
      log.info(property + " is out of range  [" + min + ", " + max + " ]");
      support.addError(property + " value is out of range [ " + min + ", " + max + " ]", property);
    }

  }

  /**
   * <b>Note: </b>Boundary is defined in Integer
   *
   * @see #validatePropertyBoundary
   */
  public static void validatePropertyBoundary(ValidationResult support,
      PresentationModel<?> target,
      String property,
      int min, int max) {

    // Check if property exist in target model
    if (target.getBufferedModel(property) == null) {
      log.info("Property not found: " + property);
      return;
    }

    // Get value of the property
    Object value = target.getBufferedValue(property);
    boolean valid = false;

    // Check if value in boundary
    try {
      int targetNum = ((Number) value).intValue();
      if (targetNum >= min && targetNum <= max) {
        valid = true;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    if (valid == false) {
      log.info(property + " is out of range  [" + min + ", " + max + " ]");
      support.addError(property + " value is out of range [ " + min + "," + max + " ]", property);
    }

  }

  /**
   * Validate a property and check if it is empty or null. If true, put the
   * Error result into Validation Support.
   */
  public static void validatePropertyEmpty(ValidationResult support,
      PresentationModel<?> target,
      String property
      ) {

    // Check if property exist in target model
    if (target.getBufferedModel(property) == null) {
      log.info("Property not found: " + property);
      return;
    }

    // Get value of the property
    Object value = target.getBufferedValue(property);

    if (value == null || value.toString().isEmpty()) {
      support.addError(property + " is empty", property);
      log.info("Validation error: " + property + " is empty");
    }
  }

  /**
   * Validate a property and check if it is empty or null. If true, put the
   * Error result into Validation Support.
   */
  public static void validatePropertyStringLength(ValidationResult support,
      PresentationModel<?> target,
      String property,
      int min, int max,
      boolean allowEmpty
      ) {

    // Check if property exist in target model
    if (target.getBufferedModel(property) == null) {
      log.info("Property not found: " + property);
      return;
    }

    // Get value of the property
    String value = (String) target.getBufferedValue(property);
    if (Strings.isBlank(value)) {
      if (!allowEmpty) {
        String err = String.format("%s is empty", property);
        support.addError(err, property);
        log.info("Validation error: " + err);
      }
      return;
    }

    if (value.length() > max || value.length() < min) {
      String err = String.format("%s length is over range[%d,%d]", property, min, max);
      support.addError(err, property);
      log.info("Validation error: " + err);
    }
  }

  /**
   * Validate a property and check if it is empty or null. If true, put the
   * Error result into Validation Support.
   */
  public static void validatePropertyEmpty(ValidationResult result,
      PresentationModel<?> target,
      String property,
      String propertyAlias
      ) {

    // Check if property exist in target model
    if (target.getBufferedModel(property) == null) {
      log.info("Property not found: " + property);
      return;
    }

    // Get value of the property
    Object value = target.getBufferedValue(property);

    if (value == null || value.toString().isEmpty()) {
      result.addError(propertyAlias + " is empty");
      log.info("Validation error: " + property + " is empty");
    }
  }

  public static void showErrorReport(Component component, ValidationResultModel validationModel) {
    JComponent report = ValidationResultViewFactory.createReportList(validationModel);
    report.setPreferredSize(new Dimension(400, 200));
    JPanel content = new JPanel(new BorderLayout());
    content.add(report, BorderLayout.CENTER);
    content.add(new JLabel("Please fix the following errors:"), BorderLayout.NORTH);
    JOptionPane.showMessageDialog(component, content,
        "Invalid Configuration",
        JOptionPane.PLAIN_MESSAGE);
  }


  public static final class NullValidator extends BufferValidator {

    public NullValidator() {
      super(null);
    }

    @Override
    public void validate(ValidationResult result) {
      // Do nothing.
    }

  }

}
