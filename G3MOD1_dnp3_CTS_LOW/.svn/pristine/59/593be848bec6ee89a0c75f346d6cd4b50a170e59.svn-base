/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14file.h
 * description: This file is intended for internal SCL use only.
 *  IEC 60870-5-101 slave File Transfer functionality.
 */
#ifndef S14FILE_DEFINED
#define S14FILE_DEFINED

#include "tmwscl/i870/s14sesn.h"
#include "tmwscl/i870/s14sctr.h"

  typedef enum 
  {
    S14FILE_IDLE_STATE,
    S14FILE_SELECT_STATE,
    S14FILE_QUERYLOG_STATE,
    S14FILE_CALL_STATE,
    S14FILE_SECTION_SELECT_STATE,
    S14FILE_SECTION_CALL_STATE,
    S14FILE_SECTION_DEACTIVATE_STATE,
    S14FILE_RCV_ACK_STATE,
    S14FILE_RCV_FILE_READY_STATE,
    S14FILE_RCV_SECTION_READY_STATE,
    S14FILE_RCV_FLSNA_STATE,
    S14FILE_DELETE_STATE 
  } 
  S14FILE_STATE_ENUM;

/* Maximum lengths from IEC 60870-5-1-1 Edition 2 section 7.4.11.3.2  */

/* Maximum length of a segment  */
#define S14FILE_MAX_SEGMENT_LENGTH 240

/* Maximum length of a section */
#define S14FILE_MAX_SECTION_LENGTH 64000


#ifdef __cplusplus
extern "C" {
#endif

  /* function: s14file__calculateChecksum */
  TMWTYPES_UCHAR TMWDEFS_GLOBAL s14file_calculateChecksum(
    TMWTYPES_UCHAR oldChecksum,
    TMWTYPES_UCHAR *pData,
    TMWTYPES_UCHAR dataLength);

  /* function: s14file_buildFSCNA */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14file_buildFSCNA(
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot, 
    TMWTYPES_UCHAR scq);

  /* function: s14file_buildFDRTA */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14file_buildFDRTA(
    TMWSCTR *pSector);

  /* function: s14file_buildResponse */
  TMWTYPES_BOOL TMWDEFS_CALLBACK s14file_buildResponse( 
    TMWSCTR *pSector, 
    TMWTYPES_BOOL buildResponse);

#ifdef __cplusplus
}
#endif
#endif /* S14FILE_DEFINED */
