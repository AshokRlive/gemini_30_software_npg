/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.xml;

import java.util.Collection;

import org.apache.log4j.Logger;

import com.g3schema.ns_clogic.CLogicInputsT;
import com.g3schema.ns_clogic.CLogicOutputsT;
import com.g3schema.ns_clogic.CLogicParametersT;
import com.g3schema.ns_clogic.ControlLogicT;
import com.g3schema.ns_clogic.ControlLogicsT;
import com.g3schema.ns_common.ControlLogicRefT;
import com.g3schema.ns_common.OutputChannelRefT;
import com.g3schema.ns_common.OutputModuleRefT;
import com.g3schema.ns_common.VirtualPointRefT;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOPoint;
import com.lucy.g3.rtu.config.clogic.utils.CLogicUtility;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.module.shared.domain.SwitchModuleOutput;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoAnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoCounterPoint;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.xml.gen.common.ControlLogicDef.CONTROL_LOGIC_TYPE;

/**
 * XmlWriter for control logic.
 */
public class CLogicXmlWriter {

  private Logger log = Logger.getLogger(CLogicXmlWriter.class);
  private ValidationResult result;


  public CLogicXmlWriter(ValidationResult result) {
    this.result = result == null ? new ValidationResult() : result;
  }

  public void write(ControlLogicsT xml, Collection<ICLogic> clogics) {
    for (ICLogic c : clogics) {
      if (c != null) {
        try {
          writeCLogic(xml, c);
        } catch (Exception e) {
          log.error("Fail to export logic:" + c, e);
        }
      }
    }
  }

  private void writeCLogic(ControlLogicsT xml_clogics, ICLogic logic) {
    CONTROL_LOGIC_TYPE rawType = CONTROL_LOGIC_TYPE.forValue(logic.getType().getId());
    if (rawType == null) {
      log.error("Unsupported type: " + logic.getType());
      return;
    }

    ControlLogicT xml_clogic = xml_clogics.clogic.append();

    xml_clogic.group.setValue(logic.getGroup());
    xml_clogic.name.setValue(logic.getCustomName() == null ? "" : logic.getCustomName());
    xml_clogic.logicType.setValue(rawType.getValue());
    xml_clogic.enabled.setValue(logic.isEnabled());
    xml_clogic.uuid.setValue(logic.getUUID());

    // Binary points
    {
      PseudoBinaryPoint[] bpoints = CLogicUtility.findBinaryPoints(logic.getAllPoints());
      if (bpoints != null) {
        for (int i = 0; i < bpoints.length; i++) {
          bpoints[i].writeToXML(xml_clogic.binaryPoints.append());
        }
      }
    }

    // Double Binary points
    {
      PseudoDoubleBinaryPoint[] dpoints = CLogicUtility.findDoubleBinaryPoints(logic.getAllPoints());
      if (dpoints != null) {
        for (int i = 0; i < dpoints.length; i++) {
          dpoints[i].writeToXML(xml_clogic.dbinaryPoints.append());
        }
      }
    }

    // Analogue points
    {
      PseudoAnaloguePoint[] apoints = CLogicUtility.findAnalogPoints(logic.getAllPoints());
      if (apoints != null) {
        for (int i = 0; i < apoints.length; i++) {
          apoints[i].writeToXML(xml_clogic.analoguePoints.append());
        }
      }
    }

    // Counter points
    {
      PseudoCounterPoint[] cpoints = CLogicUtility.findCounterPoints(logic.getAllPoints());
      if (cpoints != null) {
        for (int i = 0; i < cpoints.length; i++) {
          cpoints[i].writeToXML(xml_clogic.counterPoints.append());
        }
      }
    }

    // Inputs
    {
      CLogicIO<VirtualPoint>[] inputs = logic.getInputs();
      if (inputs != null) {
        for (int i = 0; i < inputs.length; i++) {
          writeCLogicInput(inputs[i], xml_clogic.inputs.append());
        }
      }
    }

    // Outputs
    {
      CLogicIO<?>[] outputs = logic.getOutputs();
      if (outputs != null) {
        for (int i = 0; i < outputs.length; i++) {
          writeCLogicOutput(outputs[i], xml_clogic.outputs.append());
        }
      }
    }

    // Parameters
    CLogicParametersT xml_param = xml_clogic.parameters.append();
    logic.writeParamToXML(xml_param);

    // Remove parameter node if no parameter has been written
    if (xml_param.getNode().getChildNodes().getLength() == 0) {
      xml_clogic.parameters.remove();
    }

  }

  private boolean writeCLogicInput(CLogicIO<VirtualPoint> input, CLogicInputsT xml_input) {
    VirtualPoint point = input.getValue();
    CLogicIOPoint logicInput = (CLogicIOPoint) input; // TODO IMPROVE DESIGN
    xml_input.name.setValue(logicInput.getLabel());
    xml_input.type.setValue(logicInput.getSupportedType().ordinal());
    
    if (point != null) {
      VirtualPointRefT xml_point = xml_input.inputPoint.append();
      xml_point.pointGroup.setValue(point.getGroup());
      xml_point.pointID.setValue(point.getId());
      return true;

    } else {
      xml_input.none.append();

      if (!input.isOptional()) {
        String msg = "the mandatory input \""
            + input.getLabel() + "\" is not configured in \"" + input.getOwner() + "\"";
        log.error(msg);
        result.addError(msg);
        return false;

      } else if (input.isOptional()) {
        String msg = "the input \""
            + input.getLabel() + "\" is not configured in \"" + input.getOwner() + "\"";
        log.warn(msg);
        result.addWarning(msg);
        return false;
      } else {
        return false;
      }
    }
  }

  private void writeCLogicOutput(CLogicIO<?> cLogicIO, CLogicOutputsT xml_output) {
    Object value = cLogicIO.getValue();
    xml_output.name.setValue(cLogicIO.getLabel());
    
    if (value == null) {
      xml_output.none.append();
      if (!cLogicIO.isOptional()) {
        String msg = "the mandatory output \""
            + cLogicIO.getName() + "\" is not configured in \"" + cLogicIO.getOwner() + "\"";
        log.error(msg);
        result.addError(msg);
      }

    } else {
      if (value instanceof IChannel) {
        IChannel ch = (IChannel) value;
        OutputChannelRefT xml_channel = xml_output.outputChannel.append();
        xml_channel.moduleType.setValue(ch.getOwnerModule().getType().name());
        xml_channel.moduleID.setValue(ch.getOwnerModule().getId().name());
        xml_channel.channelID.setValue(ch.getId());
        xml_channel.channelType.setValue(ChannelType.convertEnum(ch.getType()).getValue());
      
      } else if (value instanceof Module) {
        Module module = (Module) value;
        OutputModuleRefT xml_module = xml_output.outputModule.append();
        xml_module.moduleType.setValue(module.getType().name());
        xml_module.moduleID.setValue(module.getId().name());
      
      } else if (value instanceof ICLogic) {
        ControlLogicRefT xml_logic = xml_output.outputLogic.append();
        xml_logic.setValue(((ICLogic) value).getGroup());
      
      } else if (value instanceof SwitchModuleOutput) {
        SwitchModuleOutput output = (SwitchModuleOutput) value;
        Module module = output.getModule();

        OutputModuleRefT xml_module = xml_output.outputModule.append();
        xml_module.moduleType.setValue(module.getType().name());
        xml_module.moduleID.setValue(module.getId().name());

        xml_module.switchID.setValue(output.getIndex().ordinal());
        
      } else if(value instanceof VirtualPoint) {
        VirtualPointRefT xml_point = xml_output.outputPoint.append();
        xml_point.pointGroup.setValue(((VirtualPoint)value).getGroup());
        xml_point.pointID.setValue(((VirtualPoint)value).getId());
      }
    }

  }
}
