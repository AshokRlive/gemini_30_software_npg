/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.ui.dialog.editor;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jdesktop.swingx.JXTitledSeparator;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.common.base.Strings;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.view.ValidationComponentUtils;
import com.l2fprod.common.swing.JCollapsiblePane;
import com.lucy.g3.gui.common.widgets.ext.swing.textfield.FormattedTextFieldSupport;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.DigitalPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSet;
class DelayedEventReportPanel extends JPanel {
  private final ValueModel vm0;
  private final ValueModel vm1;
  private final ValueModel vm2;
  private final ValueModel vm3;
  
  private final ValueModel vmEnable;
  
  /**
   * Required by JFormdesigner.
   */
  @SuppressWarnings("unused")
  private DelayedEventReportPanel() {
     this.vm0 = new ValueHolder(Integer.valueOf(100));   
     this.vm1 = new ValueHolder(Integer.valueOf(100));     
     this.vm2 = new ValueHolder(Integer.valueOf(100));     
     this.vm3 = new ValueHolder(Integer.valueOf(100));
     this.vmEnable = new ValueHolder(true);
     
     initComponents();
     initComponentsStates();
  }
  
  public DelayedEventReportPanel(ValueModel vmEnable, ValueModel vm0, ValueModel vm1) {
    this(vmEnable, vm0, vm1, null, null);
  }
  
  public DelayedEventReportPanel(ValueModel vmEnable, ValueModel vm0, ValueModel vm1, ValueModel vm2,ValueModel vm3) {
    this.vm0 = vm0;
    this.vm1 = vm1;
    this.vm2 = vm2;
    this.vm3 = vm3;
    this.vmEnable = vmEnable;
    
    initComponents();
    initComponentsStates();
  }
  
  public void setLabelSet(ValueLabelSet labelSet) {
    lblValue0.setText(getValueWithLabel(0, labelSet));
    lblValue1.setText(getValueWithLabel(1, labelSet));
    lblValue2.setText(getValueWithLabel(2, labelSet));
    lblValue3.setText(getValueWithLabel(3, labelSet));
  }

  private static String getValueWithLabel(int value, ValueLabelSet labelSet) {
    String label = labelSet == null ? null : labelSet.getLabel((double) value);
    return Strings.isBlank(label) ? String.valueOf(value) : String.format("%s (%s)",value, label);
  }
  
  private void initComponentsStates() {
    ((JCollapsiblePane)panelCollapse).setCollapsed(!cboxEnable.isSelected());
    
    lblUnit0.setVisible(ftfValue0.isVisible());
    lblValue0.setVisible(ftfValue0.isVisible());
    
    lblUnit1.setVisible(ftfValue1.isVisible());
    lblValue1.setVisible(ftfValue1.isVisible());
    
    lblUnit2.setVisible(ftfValue2.isVisible());
    lblValue2.setVisible(ftfValue2.isVisible());
    
    lblUnit3.setVisible(ftfValue3.isVisible());
    lblValue3.setVisible(ftfValue3.isVisible());
  }

  private JFormattedTextField createField(ValueModel vm, String propertyName) {
    JFormattedTextField field;
    if(vm != null) {
      //BoundaryNumberFormatter unsingedFormat = BoundaryNumberFormatter.createUnsginedLong();
      field = BasicComponentFactory.createFormattedTextField(vm, NumberFormat.getInstance());
      FormattedTextFieldSupport.installCommitOnType(field);
      ValidationComponentUtils.setMessageKey(field, propertyName);
    } else {
      field = new JFormattedTextField();
      field.setVisible(false);
    }
    
    return field;
  }

  private void cboxEnableDelayActionPerformed(ActionEvent e) {
    ((JCollapsiblePane)panelCollapse).setCollapsed(!cboxEnable.isSelected());
  }

  private void createUIComponents() {
    ftfValue0 = createField(vm0, DigitalPoint.PROPERTY_DELAY0);
    ftfValue1 = createField(vm1, DigitalPoint.PROPERTY_DELAY1);
    ftfValue2 = createField(vm2, DigitalPoint.PROPERTY_DELAY2);
    ftfValue3 = createField(vm3, DigitalPoint.PROPERTY_DELAY3);
    
    panelCollapse = new JCollapsiblePane();
    ((JCollapsiblePane)panelCollapse).setCollapsed(true);
    
    cboxEnable = BasicComponentFactory.createCheckBox(vmEnable, "Enabled");
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    JXTitledSeparator sepParam3 = new JXTitledSeparator();
    label1 = new JLabel();
    label6 = new JLabel();
    lblValue0 = new JLabel();
    lblUnit0 = new JLabel();
    lblValue1 = new JLabel();
    lblUnit1 = new JLabel();
    lblValue2 = new JLabel();
    lblUnit2 = new JLabel();
    lblValue3 = new JLabel();
    lblUnit3 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setLayout(new FormLayout(
        "default:grow",
        "fill:default, $lgap, top:default, $lgap, fill:default"));

    //---- sepParam3 ----
    sepParam3.setTitle("Delayed Event Report");
    add(sepParam3, CC.xy(1, 1));

    //---- cboxEnable ----
    cboxEnable.setText("Enabled");
    cboxEnable.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        cboxEnableDelayActionPerformed(e);
      }
    });
    add(cboxEnable, CC.xy(1, 3));

    //======== panelCollapse ========
    {
      panelCollapse.setLayout(new FormLayout(
          "10dlu, $lcgap, default, $ugap, center:default, $lcgap, default",
          "5*(default, $lgap), default"));

      //---- label1 ----
      label1.setText("Reported Value");
      panelCollapse.add(label1, CC.xy(3, 1));

      //---- label6 ----
      label6.setText("Delayed Period");
      panelCollapse.add(label6, CC.xy(5, 1));

      //---- lblValue0 ----
      lblValue0.setText("0");
      panelCollapse.add(lblValue0, CC.xy(3, 3));

      //---- ftfValue0 ----
      ftfValue0.setColumns(8);
      panelCollapse.add(ftfValue0, CC.xy(5, 3));

      //---- lblUnit0 ----
      lblUnit0.setText("seconds");
      panelCollapse.add(lblUnit0, CC.xy(7, 3));

      //---- lblValue1 ----
      lblValue1.setText("1");
      panelCollapse.add(lblValue1, CC.xy(3, 5));

      //---- ftfValue1 ----
      ftfValue1.setColumns(8);
      panelCollapse.add(ftfValue1, CC.xy(5, 5));

      //---- lblUnit1 ----
      lblUnit1.setText("seconds");
      panelCollapse.add(lblUnit1, CC.xy(7, 5));

      //---- lblValue2 ----
      lblValue2.setText("2");
      panelCollapse.add(lblValue2, CC.xy(3, 7));

      //---- ftfValue2 ----
      ftfValue2.setColumns(8);
      panelCollapse.add(ftfValue2, CC.xy(5, 7));

      //---- lblUnit2 ----
      lblUnit2.setText("seconds");
      panelCollapse.add(lblUnit2, CC.xy(7, 7));

      //---- lblValue3 ----
      lblValue3.setText("3");
      panelCollapse.add(lblValue3, CC.xy(3, 9));

      //---- ftfValue3 ----
      ftfValue3.setColumns(8);
      panelCollapse.add(ftfValue3, CC.xy(5, 9));

      //---- lblUnit3 ----
      lblUnit3.setText("seconds");
      panelCollapse.add(lblUnit3, CC.xy(7, 9));
    }
    add(panelCollapse, CC.xy(1, 5));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JCheckBox cboxEnable;
  private JPanel panelCollapse;
  private JLabel label1;
  private JLabel label6;
  private JLabel lblValue0;
  private JFormattedTextField ftfValue0;
  private JLabel lblUnit0;
  private JLabel lblValue1;
  private JFormattedTextField ftfValue1;
  private JLabel lblUnit1;
  private JLabel lblValue2;
  private JFormattedTextField ftfValue2;
  private JLabel lblUnit2;
  private JLabel lblValue3;
  private JFormattedTextField ftfValue3;
  private JLabel lblUnit3;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
