/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import java.util.HashMap;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.module.shared.domain.ISupportChangeID;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumFPM.FPM_CH_AINPUT;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumFPM.FPM_CH_DINPUT;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumFPM.FPM_CH_FPI;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * The implementation of Fault Detect Module.
 */
public final class ModuleFPM extends AbstractCANModule implements ICANModule, ISupportChangeID {
  private final ModuleFPMSettings settings;
  
  public ModuleFPM() {
    this(MODULE_ID.MODULE_ID_0);
  }

  public ModuleFPM(MODULE_ID id) {
    super(id, MODULE.MODULE_FPM);
    this.settings = new ModuleFPMSettings(
        getChByEnum(FPM_CH_FPI.FPM_CH_FPI_1_RESET),
        getChByEnum(FPM_CH_FPI.FPM_CH_FPI_2_RESET)
        );
  }

  

  @Override
  protected HashMap<ChannelType, ICANChannel[]> initChannels() {
    HashMap<ChannelType, ICANChannel[]> chMap = new HashMap<ChannelType, ICANChannel[]>();

    // Initialise Analogue output channels
    chMap.put(ChannelType.ANALOG_INPUT,
        createChannels(ChannelType.ANALOG_INPUT, FPM_CH_AINPUT.values()));

    // Initialise Analogue output channels
    chMap.put(ChannelType.DIGITAL_INPUT,
        createChannels(ChannelType.DIGITAL_INPUT, FPM_CH_DINPUT.values()));

    // Initialise digital output channels
    chMap.put(ChannelType.FPI,
        createChannels(ChannelType.FPI, FPM_CH_FPI.values()));

    return chMap;
  }

  @Override
  public void setId(MODULE_ID moduleID) {
    super.modifyId(moduleID);
  }


  @Override
  public ModuleFPMSettings getSettings() {
    return settings;
  }

  public FPIConfig[] getAllFPIs() {
    return settings.getAllFpi();
  }
}