/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:Device.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11 Jun 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef IDEVICE_H_
#define IDEVICE_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IIOModule.h"

#ifndef DBG_ERR
#define DBG_ERR(M, ...)
#endif

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define UNSUPPORT_OPERATION()\
                DBG_ERR("Unsupported operation:%s",__PRETTY_FUNCTION__)

#define UNSUPPORT_OPERATION_WITH_RET()\
                DBG_ERR("Unsupported operation:%s",__PRETTY_FUNCTION__);\
                return IOM_ERROR_NOT_SUPPORTED

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
class IDevice: public IIOModule
{
public:
    IDevice(MODULE type, MODULE_ID id):IIOModule(type,id)
    {}

    virtual ~IDevice()
    {}


    /************************************/
    /*		IIOModule Interface         */
    /************************************/

    virtual IOM_ERROR sendMessage(ModuleMessage *messagePtr,
                    lu_uint32_t retries = 0)
    {
        LU_UNUSED(messagePtr);
        LU_UNUSED(retries);

        UNSUPPORT_OPERATION_WITH_RET();
    }

    virtual IOM_ERROR sendMessage(MODULE_MSG_TYPE messageType,
                    MODULE_MSG_ID messageID,
                    PayloadRAW *messagePtr,
                    lu_uint32_t retries = 0)
    {
        LU_UNUSED(messageType);
        LU_UNUSED(messageID);
        LU_UNUSED(messagePtr);
        LU_UNUSED(retries);
        UNSUPPORT_OPERATION_WITH_RET();
    }

    virtual IOM_ERROR sendMessage(sendReplyStr *messagePtr,
                    lu_uint32_t retries = 0)
    {
        LU_UNUSED(messagePtr);
        LU_UNUSED(retries);
        UNSUPPORT_OPERATION_WITH_RET();
    }

    virtual IOM_ERROR decodeMessage(ModuleMessage *message)
    {
        LU_UNUSED(message);
        UNSUPPORT_OPERATION_WITH_RET();
    }

    virtual IOM_ERROR tickEvent(lu_uint32_t dTime)
    {
        LU_UNUSED(dTime);
        UNSUPPORT_OPERATION_WITH_RET();
    }

    virtual void disableTimeout(lu_bool_t disable)
    {
        LU_UNUSED(disable);
        UNSUPPORT_OPERATION();
    }

    virtual IOM_ERROR getInfo(IOModuleInfoStr& moduleInfo)
    {
        LU_UNUSED(moduleInfo);
        UNSUPPORT_OPERATION_WITH_RET();
    }

    virtual IOM_ERROR getDetails(IOModuleDetailStr& moduleInfo)
    {
        LU_UNUSED(moduleInfo);
        UNSUPPORT_OPERATION_WITH_RET();
    }

    void getSystemAPI(ModuleVersionStr& sysAPI)
    {
        //Default behaviour
        sysAPI.major = 0;
        sysAPI.minor = 0;
    }


    virtual IOM_ERROR ping(lu_uint32_t timeout,
                    lu_uint32_t payloadSize = 8)
    {
        LU_UNUSED(timeout);
        LU_UNUSED(payloadSize);
        //TODO to be implemented by wang_p
        UNSUPPORT_OPERATION_WITH_RET();
    }

    virtual IOM_ERROR getTime(TimeManager::TimeStr& time,
                    lu_uint32_t timeout)
    {
        LU_UNUSED(time);
        LU_UNUSED(timeout);
        UNSUPPORT_OPERATION_WITH_RET();
    }

    virtual void stop()
    {
        UNSUPPORT_OPERATION();
    }

    virtual ModuleUID getModuleUID()
    {
        UNSUPPORT_OPERATION();
        return DEFAULT_MODULE_UID;
    }

    virtual lu_bool_t getStatus(MSTATUS status)
    {
        LU_UNUSED(status);
        UNSUPPORT_OPERATION();
        return LU_FALSE;
    }
};

#endif /* IDEVICE_H_ */

/*
 *********************** End of file ******************************************
 */
