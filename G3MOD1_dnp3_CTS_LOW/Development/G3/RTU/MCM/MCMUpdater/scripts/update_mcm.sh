#!/bin/sh

# Define path and file values to be used here:

MCMFile=$1
if [ -n "$MCMFile" ];then
	echo "MCM File:$MCMFile"
else
	echo "MCM file not specified. Default file name will be used."
	MCMFile="MCM.zip"
fi

# Main RTU software path
GEMINI3_FOLDER="/usr/local/gemini/"

# Application (and backup) folders:
GEMINI3_APP_FOLDER=${GEMINI3_FOLDER}"application/"
BACKUP_FOLDER=${GEMINI3_FOLDER}"previous-app/"

# "Update flag" files (If these files are present on G3_APP_ROOT, 
#the script enters in special mode):
FLAG_UPDATING=".updatingMCM"

# "Update" folders and files:
UPDATE_FOLDER=${GEMINI3_FOLDER}"update/"
G3_FW_MCM=${UPDATE_FOLDER}"${MCMFile}"

fBackup()
{
	#Backup if application is available
	if [ "`ls $GEMINI3_APP_FOLDER`" != "" ]
	then
		# Backup application to "previous" folder
		if [ ! -d $BACKUP_FOLDER ]
		then
			#Create directory
			mkdir $BACKUP_FOLDER
		fi
		
		#Clear backup directory
		if [ "`ls $BACKUP_FOLDER`" != "" ]
		then
			rm -r ${BACKUP_FOLDER}*
		fi
		
		echo "Backing up MCM application..."
		cp -r ${GEMINI3_APP_FOLDER}* ${BACKUP_FOLDER}
		echo "Backing up completed!"
	else
		echo "Backup has not been done since there is no application found."
	fi
}

fRestore()
{
	# restore application from backup ("previous" folder)
	if [ ! -d $GEMINI3_APP_FOLDER ]
	then
		#Create directory
		mkdir $GEMINI3_APP_FOLDER
	fi
	
 if [ -d $BACKUP_FOLDER ] && [ "`ls $BACKUP_FOLDER`" != "" ]
	then
		echo "Restoring MCM application from backup..."
		cp -r ${BACKUP_FOLDER}* ${GEMINI3_APP_FOLDER}
	else
		echo "Cannot restore cause the backup application is not found!"
	fi
}

fFailedAndRevert()
{
	#clean upload folder
	if [ "`ls $UPDATE_FOLDER`" != "" ]
	then
		rm -r ${UPDATE_FOLDER}*
	fi

	# remove App 
	if [ "`ls $GEMINI3_APP_FOLDER`" != "" ]
	then
		rm -r ${GEMINI3_APP_FOLDER}*
	fi
	
	# restore from backup
	fRestore
	
	# End UPDATE mode
	rm $FLAG_UPDATING
}

# ---ACTION STARTS HERE!

# make dirs as necessary
echo "Creating directories..."
mkdir -p ${GEMINI3_APP_FOLDER} ${BACKUP_FOLDER} ${UPDATE_FOLDER}

# always change to the main directory
#cd ${GEMINI3_FOLDER}

if [ -f $FLAG_UPDATING ]
then
	# File is present: Previous update failed
	#fFailedAndRevert #TODO; when needs to revert!
	echo "Upgrading is in progress"
    exit 2
fi

#echo Starting preliminary checks before launching application.
# ensure no application folder is empty
#CHECKAPP="ls ${GEMINI3_APP_FOLDER}"
#CHECKPREV="ls ${BACKUP_FOLDER}"
#if [ "`${CHECKAPP}`" = "" -o "`${CHECKPREV}`" = "" ]
#then
#	if [ "`${CHECKAPP}`" = "" ]
#	then
#		# app is not on the application folder: restore
#		fRestore
#	else
#		# no backup present: backup now
#		fBackup
#	fi
#fi

#Always backup previous application
#fBackup

echo Start updating MCM...

# Check if MCM firmware exists
if [ -f $G3_FW_MCM ]
then
	# the process starts
	touch $FLAG_UPDATING
	
	# Remove the files that might be running, otherwise the "unzip" command would fail.
	rm -f ${GEMINI3_APP_FOLDER}*.axf
	rm -f ${GEMINI3_APP_FOLDER}*.sh

	# Update to new application
	unzip -oq $G3_FW_MCM -d $GEMINI3_APP_FOLDER
	if [ $? -eq 0 ]
	then
		# zip file extracted successfully
		
		# Change files to executable to overcome unzip problem. 
		#This problem prevents unzipped file to write the right file attributes.
		#BusyBox 1.15 unzip's problem is fixed in BusyBox 1.17.0 and later.
		#See bug 1045 on https://bugs.busybox.net/show_bug.cgi?id=1045
		#And YES, BusyBox's chmod seems to do not work properly with the -R option. Even having *.sh files in gemini folder.
		#(despite been reported on https://bugs.busybox.net/show_bug.cgi?id=13 ).
		chmod +x ${GEMINI3_APP_FOLDER}*.sh
		chmod +x ${GEMINI3_APP_FOLDER}*.axf
		# End UPDATE mode
		rm $FLAG_UPDATING

		# Delete original zip file
		#rm $G3_FW_MCM
		echo "Finish updating"
		#
		# -*-*-*-*- IMPORTANT NOTE: -*-*-*-*-
		#If needed to add something to this script, please consider adding it to 
		#the post_update_MCM.sh script instead. Keep in mind that this script
		#is not updated until after it is finished, so any change will not take 
		#effect until running the update again.
		#Remember: --For updating, the MCM always run the OLD update script!!!-- 
		#
		exit 0
		else
			echo "Fail to update MCM. Trying to restore from backup..."
			# failed extracting zip file
			fFailedAndRevert
			exit 3
	fi
	
else
	echo "Fail to upgrade MCM. MCM firmware not found at:${G3_FW_MCM}"
	exit 1
fi

# Go to the G3 folder
#cd ${GEMINI3_APP_FOLDER}

