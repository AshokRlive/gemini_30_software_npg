#!/bin/sh
# This is a shell script to put RTU into HAT mode.

echo "Entering into HAT mode..." 
./MCMConsole.axf<<EOF
FactoryHAT
exit
EOF

echo "Waiting for MCM to enter into HAT mode"
sleep 7 

echo "Set OFF/LOCAL mode"
./MCMConsole.axf<<EOF
setOLR 1
exit
EOF

echo "Disable Heartbeat"
./MCMConsole.axf<<EOF
hb 0
exit
EOF
