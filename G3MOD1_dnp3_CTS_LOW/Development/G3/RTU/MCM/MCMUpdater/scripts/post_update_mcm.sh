#!/bin/sh

# Variables required when creating localtime software link
mount_rw_cmd="mount -o remount,rw /"
mount_ro_cmd="mount -o remount,ro /"
localtime_file=/etc/localtime
application_path=/usr/local/gemini/application


# Create a softlink from /etc/localtime to our application folder
fCreateLocalTimeLink()
{
  # Convert /etc/localtime to a softlink to the application folder
  eval ${mount_rw_cmd}
  rm -f ${localtime_file}
  ln -s ${application_path}/localtime ${localtime_file}
  eval ${mount_ro_cmd}
}


# Check if /etc/localtime is a softlink
fPrepareLocalTimeLink()
{
	if [ -L ${localtime_file} ]
	then
	  soflink_path=`readlink ${localtime_file}`

	  # Check where /etc/localtime is pointing and correct as necessary
	  if [ "${soflink_path}" != "${application_path}/localtime" ]
	  then
		fCreateLocalTimeLink
	  fi
	else
	  fCreateLocalTimeLink
	fi
}


# ---ACTION STARTS HERE!

# Make scripts executable in secondary folders
chmod +x $application_path/scripts/*.sh
chmod +x $application_path/bin/*

# Create time zone file for locale support
fPrepareLocalTimeLink

#finish: delete this file
rm -f $0
