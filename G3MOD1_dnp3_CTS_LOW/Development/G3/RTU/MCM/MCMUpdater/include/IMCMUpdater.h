/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:IMCMUpdater.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   1 Oct 2013     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef IMCMUPDATER_H_
#define IMCMUPDATER_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"

#include "MonitorProtocol.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
struct LaunchParams
{
public:
    bool console;
    bool factory;
public:
    LaunchParams() : console(false), factory(false)
    {}
};


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
class IMCMUpdater
{
public:
    static LaunchParams launchParams;

public:

    IMCMUpdater() {};
    virtual ~IMCMUpdater() ;

    virtual void launch(LaunchParams launchParams) = 0;

    virtual void exit() = 0 ;

    virtual void restart(APP_EXIT_CODE exitcode = APP_EXIT_CODE_RESTART)= 0 ;

    virtual void rebootRTU()= 0 ;

    virtual void shutdownRTU()= 0 ;

    virtual lu_uint32_t getExitCode() = 0;
};

#endif /* IMCMUPDATER_H_ */

/*
 *********************** End of file ******************************************
 */
