/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 * 				$Id: c_template.c 1469 2012-06-20 20:51:28Z fryers_j $
 *              $HeadURL: http://10.11.0.6:81/svn/gemini_30_software/trunk/Development/G3/common/template/c_template.c $
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MCMUPDATER_H_
#define MCMUPDATER_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "IMCMUpdater.h"

#include "Console.h"
#include "ConfigurationToolHandler.h"
#include "MonitorProtocolHandler.h"
#include "ModuleManager.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
LaunchParams IMCMUpdater::launchParams;

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Main Updater application
 *
 * This application is in charge to update the firmware of the Gemini3 modules,
 * including the MCM itself.
 *
 * This object represents the MCM Updater application itself.
 */
class MCMUpdater : public IMCMUpdater
{
public:
    MCMUpdater();
    virtual ~MCMUpdater();

    virtual void launch(LaunchParams launchParams);

    virtual void exit();

    virtual void restart(APP_EXIT_CODE exitcode);

    virtual void rebootRTU();

    virtual void shutdownRTU();

    virtual lu_uint32_t getExitCode();

private:
    void initMemManager();
    void exit(APP_EXIT_CODE exitCode);

    ModuleManager    *moduleMgr;
    UpgradeManager   *upgradeMgr;
    CTH::ConfigurationToolHandler *cth;
    MonitorProtocolHandler *monitorHdl;
    Console *console;

    lu_bool_t running;

    APP_EXIT_CODE exitCode;

};

#endif /* MCMUPDATER_H_ */

/*
 *********************** End of file ******************************************
 */
