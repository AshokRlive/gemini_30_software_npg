/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:ModuleManagerTest.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24 Sep 2013     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <iostream>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "ModuleManager.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

using namespace std;
/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

void testStop()
{
    lu_bool_t failed = LU_FALSE;

    ModuleManager test;

    if(test.isJoinable() == LU_FALSE)
    {
        cout<<"Not joinable!"<<endl;
        failed = LU_TRUE;
    }

    if(test.isRunning() == LU_TRUE)
    {
        cout<<"State error1!"<<endl;
        failed = LU_TRUE;
    }

    test.start();
    if(test.isRunning() != LU_TRUE)
    {
        cout<<"State error2!"<<endl;
        failed = LU_TRUE;
    }

    test.stop();
    if(test.isInterrupting() == LU_FALSE)
    {
        cout<<"State error3!"<<endl;
        failed = LU_TRUE;
    }

    test.join();
    if(test.isFinished() != LU_TRUE)
    {
        cout<<"State error4!"<<endl;
        failed = LU_TRUE;
    }



    if(failed == LU_TRUE)
        cout<<"testStop failed"<<endl;
    else
        cout<<"testStop success"<<endl;
}
/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
int main(int argc, char ** argv)
{
    SIG_BlockAllExceptTerm();

    testStop();
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
