/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 * 				$Id: c_template.c 1469 2012-06-20 20:51:28Z fryers_j $
 *              $HeadURL: http://10.11.0.6:81/svn/gemini_30_software/trunk/Development/G3/common/template/c_template.c $
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "MCMUpdater.h"
#include "SlaveUpgradeTask.h"
#include "CANIOModule.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

int main(int argc, char ** argv)
{
     SIG_BlockAllExceptTerm();

    Logger::setAllLogLevel(LOG_LEVEL_DEBUG);
    CANIOModule* module = new CANIOModule(MODULE_SCM, MODULE_ID_0, COMM_INTERFACE_CAN0);
    SlaveUpgradeTask t(module, G3_FW_SLAVE,0);

    printf("Task joinable:%i\n",t.isJoinable());
    t.start();
    t.join();
    t.stop();
    printf("Task completed\n");



}


/*
 *********************** End of file ******************************************
 */
