/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: HATTrick.h 25 Aug 2015 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMUpdater/src/HardwareAccessTest/include/HATTrick.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Common definitions and tools for the Hardware Access Test functionality.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 25 Aug 2015 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25 Aug 2015 pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef HATTRICK_H__INCLUDED
#define HATTRICK_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <netinet/in.h>
#include <unistd.h>
#include <errno.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
typedef enum
{
    HATMESSAGE_CAN,
    HATMESSAGE_GPIO,
    HATMESSAGE_SERIAL,
    HATMESSAGE_INFOSDP,
    HATMESSAGE_INFOSVN,
    HATMESSAGE_LAST
} HATMESSAGE;

struct HATHeader
{
    HATMESSAGE type;
    lu_uint8_t size;
};

struct HATMessagePayload
{
    lu_uint32_t size;
    lu_char_t* payload;
};
struct HATMessage
{
    HATHeader header;
    HATMessagePayload payload;
};

struct HATMessageReply
{
    HATHeader header;
    HATMessagePayload payload;
};


struct HATCANMessage
{
    lu_uint32_t iFace;
    lu_uint32_t timeout_ms;
    lu_uint32_t replySize;
    HATMessagePayload message;
};

typedef enum
{
    HATGPIOTYPE_DIGITAL,
    HATGPIOTYPE_ANALOGUE,
    HATGPIOTYPE_LAST
} HATGPIOTYPE;

struct HATGPIOMessage
{
    HATGPIOTYPE type;
    lu_uint8_t gpio;
    lu_bool_t writeOp;
    union value
    {
        lu_uint8_t digital;
        lu_float32_t analogue;  //THERE IS NO SUCH THING LIKE ANALOGUE GPIO
    } value;
};

struct HATGPIOReply
{
    lu_uint32_t type;
    union value
    {
        lu_uint8_t digital;
        lu_float32_t analogue;
    } value;
};

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
/*namespace HATSpace
{

bool reply(const lu_int32_t sockFD, const HATMessageReply& message, const lu_uint32_t msgSize)
{
    if (sockFD < 0)
    {
        return false;   //socket not available
    }
    if (write(sockFD, &message, msgSize) < 0)
    {
        return false;   //error sending
    }
    return true;
}


}  namespace HATSpace */

#endif /* HATTRICK_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
