/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:LoggerTest.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   10 Dec 2013     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <string>
#include <stdio.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include <CTHMessage.h>
#include <Logger.h>
#include <BufferLogger.h>


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


int main(int argc, char ** argv)
{
    Logger& log = Logger::getLogger(SUBSYSTEM_ID_CT_HANDLER);
    Logger::setAllLogLevel(LOG_LEVEL_DEBUG);
    BufferLogger& buflogger = Logger::getBufferLogger();
    while(true)
    {
        CTMsg_R_LogList* reply = new CTMsg_R_LogList(0,0);

       lu_char_t *logPtr = reinterpret_cast<lu_char_t *>(&(*reply)[0]);
       lu_uint32_t paySize;
       if(buflogger.getLogMessage(logPtr, CTMsg_MaxPayloadSize, &paySize) == 0)
       {
           reply->setEntries(paySize);
       }

       log.debug( "Composed replied CTH message.%s",reply->toString().c_str());
       delete reply;
    }

}


/*
 *********************** End of file ******************************************
 */
