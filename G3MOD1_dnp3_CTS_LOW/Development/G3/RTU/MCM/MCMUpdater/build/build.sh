#!/bin/bash
# Build and copy unstripped axf to export path, for sending to the MCM itself
# parameters are the ones used by make, such as "all" or "clean"
APP="Debug/*.axf"
DEST="/exports/${USER}/gemini/application/"

cd Debug
make $1

if [ $? -eq 0 ]
then
    # successful make
    cd ..
    cp -v $APP $DEST
    cp -v Debug/common/hat_protocol_lib/examples/*.axf $DEST
    cp -v Debug/HATTest/*.axf $DEST
    cp -v ../scripts/* $DEST
    echo copy done!
fi




#cd ../../../../common/library/hat_protocol_lib/example/build/Debug/
#make
#cp -v client_example ${DEST}/hat/
#cp -v server ${DEST}/hat/
#cd -l

