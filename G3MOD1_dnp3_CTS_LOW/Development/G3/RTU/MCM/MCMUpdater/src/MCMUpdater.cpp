/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MCMUpdater.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   01/09/13      wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>     /* exit, EXIT_FAILURE */
#include <unistd.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "MCMUpdater.h"
#include "svnRevision.h"
#include "HMIPower.h"
#include "FactoryInfo.h"
#include "MemoryManager.h"
#include "Logger.h"
#include "HardwareAccessTest.h"
#include "dbg.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
#define EXIT_TIMEOUT_MS 2000
#define SLAVE_INIT_TIME_US 3000000

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
Logger& log = Logger::getLogger(SUBSYSTEM_ID_MAIN, "MCMUpdater"); //Set Logger identity for all loggers in this executable

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

IMCMUpdater::~IMCMUpdater()
{

}

MCMUpdater::MCMUpdater():running(LU_FALSE),
                         exitCode(APP_EXIT_CODE_EXIT_MCM_MONITOR)
{
    initMemManager();
    moduleMgr	= new ModuleManager();
    upgradeMgr 	= new UpgradeManager(*moduleMgr);
    cth 		= new CTH::ConfigurationToolHandler(*moduleMgr,*upgradeMgr,this);

    monitorHdl  = new MonitorProtocolHandler(this);
    console		= new Console(*moduleMgr,*upgradeMgr,*monitorHdl,this);

    log.debug("MCMUpdater created");
}

MCMUpdater::~MCMUpdater()
{
    delete console;
    delete cth;
    delete upgradeMgr;
    delete moduleMgr;
    delete monitorHdl;

    running = LU_FALSE;
    log.debug("MCMUpdater destroyed");
}


void MCMUpdater::launch(LaunchParams launchParams)
{
    if(running == LU_FALSE)
    {
        // Always enable HMI power
        HMIPower hmiPwr;

        running = LU_TRUE;
        Logger::g_logger.info("-- MCMUpdater started --");
        moduleMgr->start();
        if(launchParams.factory)
        {
#if HAT_SUPPORT
            /* Factory HAT mode functionality */
            log.info("MCMUpdater started in HAT mode");
            DBG_INFO("MCMUpdater started in HAT mode");
            HardwareAccessTest::getInstance().startService();
#else
            DBG_INFO("MCMUpdater HAT Disabled!!!");
#endif
        }
        /* Wait slave module to init*/
        lucy_usleep(SLAVE_INIT_TIME_US);
        
        cth->startService();
        console->start(launchParams.console);
    }
    else
    {
        DBG_ERR("Cannot start MCMUpdater since it is already running");
    }
}


lu_uint32_t MCMUpdater::getExitCode()
{
    return exitCode;
}


void MCMUpdater::rebootRTU()
{
    log.info("Attempt to reboot RTU... ");
    exit(APP_EXIT_CODE_REBOOT_RTU);
}

void MCMUpdater::shutdownRTU()
{

    log.info("Attempt to shutdown RTU... ");
    exit(APP_EXIT_CODE_SHUTDOWN_RTU);
}

void MCMUpdater::restart(APP_EXIT_CODE exitcode)
{
    log.info("Attempt to restart MCMUpdater... ");
    exit(exitcode);
}

void MCMUpdater::exit()
{
    log.info("Attempt to exit MCMUpdater and start MCMApp... ");
    exit(APP_EXIT_CODE_EXIT_MCM_MONITOR);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void MCMUpdater::initMemManager()
{
    MemoryManager memoryManager;

    //Silently initialise ALL of the rest of memory managers
    memoryManager.init();

    /* Read NVRAM MCM Factory info and check feature revision compatibility */
    bool readOK = false;
    FactoryInfo::Info fInfo;
    if(memoryManager.read(IMemoryManager::STORAGE_VAR_FEATUREREV,
                           (lu_uint8_t*)&(fInfo.featureRevision)) == LU_FALSE)
    {
        log.error("error reading NVRAM's feature revision");
        readOK = true;
    }
    if(memoryManager.read(IMemoryManager::STORAGE_VAR_MODULEUID,
                           (lu_uint8_t*)&(fInfo.moduleUID)) == LU_FALSE)
    {
        log.error("error reading NVRAM's module UID");
    }
    FactoryInfo::setInfo(fInfo);

    if(readOK)
    {
        if( (fInfo.featureRevision.major != MCM_FEATURE_VERSION_MAJOR) ||
            (fInfo.featureRevision.minor != MCM_FEATURE_VERSION_MINOR)
            )
        {
            //feature revision mismatch
            Logger::g_logger.error("Feature revision mismatch: MCM=%i.%i, Updater=%i,%i",
                                    fInfo.featureRevision.major, fInfo.featureRevision.minor,
                                    MCM_FEATURE_VERSION_MAJOR, MCM_FEATURE_VERSION_MINOR
                                    );
        }
    }

    /* Note: NVRAM Info is stored now in the FactoryInfo Static object, so the
     *       Memory Manager is not needed anymore and can be closed.
     */
}


void MCMUpdater::exit(APP_EXIT_CODE exitCode)
{
    log.info("Exiting updater application with code:%i.... ", exitCode);
    this->exitCode = exitCode;
    running = LU_FALSE;

    cth->stopService();
    DBG_INFO("Stopped configtool handler.");

    moduleMgr->stop();
    DBG_INFO("Stopped module manager.");
    
#if HAT_SUPPORT
    HardwareAccessTest::getInstance().stopService();
    log.info("Stopped HAT.");
#endif

    console->stop();
    DBG_INFO("Stopped console.");

    monitorHdl->send_notify_exit(exitCode, EXIT_TIMEOUT_MS);
    DBG_INFO("Notified monitor updater exit.");
}


static LaunchParams commandLineArgs(int argc, char *argv[])
{
    LaunchParams params;
    lu_int32_t arg;

    if (argc > 1)
    {
        fprintf(stdout, "\nMCMUpdater - (c) Lucy Electric\n");
    }

    // Handle command line arguments
    while ((arg = getopt(argc, argv, "hvcf")) != -1)
    {
        switch (arg)
        {
            case 'v':
                fprintf(stdout, "v%i.%i.%i svn:%ld\n",
                                MCM_UPDATER_SOFTWARE_VERSION_MAJOR,
                                MCM_UPDATER_SOFTWARE_VERSION_MINOR,
                                MCM_UPDATER_SOFTWARE_VERSION_PATCH,
                                _SVN_REVISION);
                exit(EXIT_SUCCESS);
                break;

            case 'c':
                params.console = true;
                break;

            case 'f':
                params.factory = true;
                break;

            default:
                fprintf(stdout,"Options:\n"
                                "-h Display this help\n"
                                "-v Display version\n"
                                "-c Start internal console\n"
                                "-f Factory mode (HAT support)\n"
                                );
                exit(EXIT_FAILURE);
        }
    }
    return params;
}

/**
 * ******* Main ****************
 */
int main(int argc, char ** argv)
{
    SIG_BlockAllExceptTerm();

    // Handle Command Line Arguments
    IMCMUpdater::launchParams = commandLineArgs(argc, argv);

    Logger::setAllLogLevel(LOG_LEVEL_WARNING);
    Logger::getBufferLogger();  //Initialise buffer logger

    MCMUpdater updater;
    updater.launch(IMCMUpdater::launchParams);
    return updater.getExitCode();
}

/*
 *********************** End of file ******************************************
 */
