/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Command Line Interpreter module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   09/05/12      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/utsname.h>
#include <iostream>
#include <cstdlib>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/un.h>      // AF_UNIX

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "GlobalDefinitions.h"
#include "timeOperations.h"
#include "Utilities.h"
#include "RTUInfo.h"
#include "CTHandlerCommon.h"
#include "IIOModule.h"
#include "MCMIOModule.h"
#include "CANIOModule.h"
#include "HardwareAccessTest.h"
#include "Console.h"
#include "dbg.h"
#include "ControlLogicDef.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define LAUNCH_HAT_WITHOUT_RESTART 0
/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
#define MODULE_TYPE_LENGTH 3 //The length of a module ID, e.g. SCM1,PSM2
/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */


Console::Console(IIOModuleManager &moduleMgr, UpgradeManager &upgMgr , MonitorProtocolHandler &monitorHdl,IMCMUpdater* app):
                                moduleMgr (moduleMgr),
                                upgradeMgr(upgMgr),
                                monitorHdl(monitorHdl),
                                mcmUpdater(app),
                                log(Logger::getLogger(SUBSYSTEM_ID_CONSOLE)),
                                cmdList(NULL),
                                cmdListSize(0),
                                exitConsole(LU_FALSE)

{
    //Fill in module map for search module type enum from user string
    for (lu_uint32_t i = 0; i < MODULE_LAST; ++i)
    {
        mTypeMap[MODULE_ToSTRING(i)] = (MODULE)i;
    }

    cmdList = new CommandCLI*[MAX_CMD_AMOUNT];
    memset(cmdList, 0x00 , MAX_CMD_AMOUNT*sizeof(CommandCLI*));
    cmdList[cmdListSize++] = new CLICAbout                (*this);
    cmdList[cmdListSize++] = new CLICIdentity             (*this);
    cmdList[cmdListSize++] = new CLICStatus               (*this);
    cmdList[cmdListSize++] = new CLICCANStats             (*this);
    cmdList[cmdListSize++] = new CLICGetLogLevel          (*this);
    cmdList[cmdListSize++] = new CLICSetLogLevel          (*this);
    cmdList[cmdListSize++] = new CLICModuleList           (*this);
    cmdList[cmdListSize++] = new CLICModuleVersion        (*this);
    cmdList[cmdListSize++] = new CLICUpdate               (*this);
    cmdList[cmdListSize++] = new CLICMonitorTest          (*this);
    cmdList[cmdListSize++] = new CLICEraseFirmware        (*this);
    cmdList[cmdListSize++] = new CLICRestartModule        (*this);
    cmdList[cmdListSize++] = new CLICExit                 (*this);
    cmdList[cmdListSize++] = new CLICRestart              (*this);
    cmdList[cmdListSize++] = new CLICReboot               (*this);
    cmdList[cmdListSize++] = new CLICShutdown             (*this);
    cmdList[cmdListSize++] = new CLICFactoryHAT           (*this);
    cmdList[cmdListSize++] = new CLICHeartBeat            (*this);
    cmdList[cmdListSize++] = new CLICSetOLR               (*this);

    /* Initialise clients list */
    MasterLockingMutex mMutex(clientAccess, LU_TRUE);
    for (lu_uint32_t i = 0; i < MAXCLIENTS; ++i)
    {
        clients[i] = NULL;
    }
}

Console::~Console()
{
    stop();
    if(*cmdList != NULL)
    {
        //delete all instances of command handlers
        for(int i = 0; i < MAX_CMD_AMOUNT; i++)
        {
            delete cmdList[i];
        }
        delete cmdList;
    }
}


void Console::stop()
{
    exitConsole = LU_TRUE;
    //remove connected clients
    MasterLockingMutex mMutex(clientAccess, LU_TRUE);
    for (lu_uint32_t i = 0; i < MAXCLIENTS; ++i)
    {
        if(clients[i] != NULL)
        {
            clients[i]->stop();
            clients[i] = NULL;
        }
    }
}


void Console::start(lu_bool_t internalConsole)
{
    lu_int32_t sockFd;      //Listening socket
    lu_int32_t accSockFd;   //Accepted socket
    lu_int32_t optVal = 1;
    struct sockaddr_un servAddr;

    if(internalConsole == LU_TRUE)
    {
        /* Internal console: a thread that never stops by itself */
        clients[0] = new CommandLineInterpreter(cmdList, cmdListSize, fileno(stdin), fileno(stdout));
        clients[0]->start();
    }

    /* TODO: pueyos_a - Deal properly with socket initialisation errors: retry in suitable errors/situations */

    // Create a local socket
    unlink(CONSOLESOCKETNAME);
    sockFd = socket(AF_UNIX, SOCK_STREAM, 0);
    if(sockFd < 0)
    {
        log.error("Unable to create socket for Console: error %i (%s).", errno, strerror(errno));

        /* If unable to create socket, we don't want to stop the app */
        while(exitConsole == LU_FALSE)
        {
            lucy_usleep(EXITWAIT_US);
        }
        return;
    }

    memset(&servAddr, 0, sizeof(struct sockaddr_un));
    servAddr.sun_family = AF_UNIX;
    strncpy(servAddr.sun_path, CONSOLESOCKETNAME, sizeof(servAddr.sun_path)-1);

    // Reuse address
    setsockopt(sockFd, SOL_SOCKET, SO_REUSEADDR, &optVal, sizeof(optVal));

    // Set to non-blocking mode
    lu_int32_t sockFlags = fcntl(sockFd, F_GETFL);
    fcntl(sockFd, F_SETFL, sockFlags | O_NONBLOCK);

    if (bind(sockFd, (struct sockaddr*)&servAddr, sizeof(servAddr)) < 0)
    {
        close(sockFd);
        log.error("Error %i binding to the Console socket (%s).", errno, strerror(errno));

        /* If unable to bind to socket, we don't want to stop the app */
        while(exitConsole == LU_FALSE)
        {
            lucy_usleep(EXITWAIT_US);
        }
        return;
    }

    if( listen(sockFd, 0) < 0 )
    {
        close(sockFd);
        log.error("Error %i setting the Console socket in listening mode (%s).\n",
                    errno, strerror(errno)
                    );

        /* If unable to listen socket, we don't want to stop the app */
        while(exitConsole == LU_FALSE)
        {
            lucy_usleep(EXITWAIT_US);
        }
        return;
    }

    fd_set             readfds;
    const struct timeval ACCEPTTOUT = ms_to_timeval(SOCKETLISTENTOUT_MS);
    struct timeval     tout;
    lu_int32_t         ret;

    //Variables to control re-printing of accept errors:
    const lu_uint32_t ACCEPTMSGREPRINT = (3600000/SOCKETLISTENTOUT_MS); //3600 secs = 1 hour
    lu_uint32_t acceptfail = 0;

    /* accept connections until exit request */
    while(exitConsole == LU_FALSE)
    {
        FD_ZERO(&readfds);
        FD_SET(sockFd, &readfds);
        tout = ACCEPTTOUT;
        ret = select(sockFd + 1, &readfds, NULL, NULL, &tout);
        if (ret > 0)
        {
            if (FD_ISSET(sockFd, &readfds) > 0)
            {
                /* Check if there is any client slot available */
                MasterLockingMutex mMutex(clientAccess);
                lu_uint32_t pos;
                for (pos = 0; pos < MAXCLIENTS; ++pos)
                {
                    if(clients[pos] == NULL)
                    {
                        break;  //found empty slot
                    }
                }
                if(pos == MAXCLIENTS)
                {
                    //no empty slots found: refuse connection
                    accSockFd = accept(sockFd, NULL, NULL);
                    if(accSockFd < 0)
                    {
                        //Please note this is not an important error
                        log.info("Error refusing Console Connection %i (%s).\n",
                                    errno, strerror(errno));
                    }
                    else
                    {
                        log.info("Console Connection refused: no client slots available.\n");
                        close(accSockFd);
                    }
                }
                else
                {
                    if ((accSockFd = accept(sockFd, NULL, NULL)) < 0)
                    {
                        /* Accept failure: ignore retries and socket blocking notification */
                        if ((errno != EAGAIN) && (errno != EWOULDBLOCK))
                        {
                            if(acceptfail++ == 0)
                            {
                                log.error("Error %i when accepting Console connection (%s).\n",
                                            errno, strerror(errno));
                            }
                            if(acceptfail > ACCEPTMSGREPRINT)
                            {
                                acceptfail = 0; //next time, re-issue the error message
                            }
                        }
                    }
                    else
                    {
                        /* Accepted connection: associate a CLI */
                        clients[pos] = new CommandLineInterpreter(cmdList, cmdListSize, accSockFd, accSockFd);
                        THREAD_ERR resThr = clients[pos]->start();
                        if(resThr != THREAD_ERR_NONE)
                        {
                            log.error("Error %i starting console client thread.\n", resThr);
                        }
                    }
                }
            }
        }

        /* Review sockets to see if any of them died */
        MasterLockingMutex mMutex(clientAccess);
        for (lu_uint32_t i = 0; i < MAXCLIENTS; ++i)
        {
            if(clients[i] != NULL)
            {
                if(clients[i]->isRunning() == LU_FALSE)
                {
                    //Client died: get rid of it
                    accSockFd = clients[i]->getInConsole();
                    delete clients[i];
                    clients[i] = NULL;
                    close(accSockFd);   //close associated input/output socket
                }
            }
        }
    }
    close(sockFd);  //close listening socket
    return;
}


/* =============== CLI Command call implementations ============== */
void Console::CLICUpdate::call(CommandLineInterpreter& cli)
{
    std::string userInput;

    IIOModule *module = console.selectModule(cli);
    if(module == NULL)
        return;

    /*Check upgrading state*/
    lu_bool_t upgrading = console.upgradeMgr.isUpgrading();
    if(upgrading == LU_TRUE)
    {
        /*Invalid state*/
        cli.message("Upgrading is in progress. Please try later.\n");
        return;
    }

    /*List all firmware*/
    std::string script("echo \"-Available firmware files-\";ls ");
    script += +G3_FW_FOLDER;
    system(script.c_str());

    /*Get user input firmware file name*/
    std::string fwFile; //Firmware file name
    if(cli.ask("Input firmware file name: ", userInput) == LU_FALSE
           || userInput.size() <= 0 )
    {
       cli.message("Invalid firmware file name:' %s'\n", userInput.c_str());
       return;
    }
    std::stringstream ss(userInput);
    ss >> userInput;
    fwFile = G3_FW_FOLDER + userInput;

    /*Check file exist*/
    if(checkFileExistence(fwFile) == LU_FALSE)
    {
        cli.message("File not found: '%s'\n", fwFile.c_str());
        return;
    }


    /*Start upgrading*/
    cli.message("Started programming module: '%s' with file '%s'.\nPlease wait patiently...\n",
                    module->getModuleName(), fwFile.c_str());
    UPG_ERR ret = console.upgradeMgr.upgradeModule(fwFile,
                    module->getID(),
                    module->getModuleUID(),true);

    if(ret != UPG_ERR_NONE)
    {
        cli.message("Failed to update the firmware of the module '%s'. err:%d\n", module->getModuleName(),ret);
    } else {
        cli.message("Succeeded to update the firmware of the module '%s'.\n", module->getModuleName());
    }
}


void Console::CLICExit::call(CommandLineInterpreter& cli)
{
    LU_UNUSED(cli);
    console.mcmUpdater->exit();
}

void Console::CLICRestart::call(CommandLineInterpreter& cli)
{
    LU_UNUSED(cli);
    console.mcmUpdater->restart(APP_EXIT_CODE_START_MCM_UPDATER);
}

void Console::CLICReboot::call(CommandLineInterpreter& cli)
{
    LU_UNUSED(cli);
    console.mcmUpdater->rebootRTU();
}

void Console::CLICShutdown::call(CommandLineInterpreter& cli)
{
    LU_UNUSED(cli);
    console.mcmUpdater->shutdownRTU();
}

void Console::CLICFactoryHAT::call(CommandLineInterpreter& cli)
{
#if HAT_SUPPORT

#if LAUNCH_HAT_WITHOUT_RESTART
    bool started = false;
    HardwareAccessTest& hat = HardwareAccessTest::getInstance();
    if(hat.isInService())
    {
    	hat.stopService();
    }
    else
    {
    	hat.startService();
    	started = true;
    }
    cli.message("Factory HAT service %s\n", (started)? "started" : "stopped");
#else
    cli.message("Restarting to go into HAT mode...\n");
    console.mcmUpdater->restart(APP_EXIT_CODE_START_MCM_UPDATER_HAT);
#endif

#else
    cli.message("HAT is not supported!\n");
#endif
}


void Console::CLICHeartBeat::call(CommandLineInterpreter& cli)
{
    lu_uint32_t mode;
    cli.message("Enables or disables the sending of the HeartBeat message to the Slave Modules.\n"
                    "CAUTION: This command affects the communication with the Slave Modules.\n"
                    "Set to 1 to enable, 0 to disable: ");
    if(cli.askAndCheck(&mode, 0, 1) == LU_TRUE)
    {
        console.moduleMgr.setHBeatEnable(mode);
    }
    else
    {
        cli.message("Cancelled.\n");
    }
}


void Console::CLICIdentity::call(CommandLineInterpreter& cli)
{
    cli.message(" Running %s\n", getMCMUpdaterVersionString().c_str());
}


void Console::CLICAbout::call(CommandLineInterpreter& cli)
{
    /* Get MCM firmware version string values */
    cli.message("%s\n"
                " OS version: %s\n"
                " Filesystem version: %s\n",
                    getMCMUpdaterVersionString().c_str(),
                    getOSVersion().c_str(),
                    getRootFSVersion().c_str()
                    );

    /* Get 3 main APIs revision numbers */
    cli.message("Main APIs revision numbers: \n"
                    "\tModule System API version: %d.%d\n"
                    "\tSchema API version: %d.%d\n"
                    "\tG3 Configuration Tool Protocol API version: %d.%d\n",
                    MODULE_SYSTEM_API_VER_MAJOR,
                    MODULE_SYSTEM_API_VER_MINOR,
                    SCHEMA_MAJOR,
                    SCHEMA_MINOR,
                    G3_CONFIG_SYSTEM_API_MAJOR,
                    G3_CONFIG_SYSTEM_API_MINOR
                    );

    /* CPU serial num */
    cli.message("CPU serial Number: [%016llX]\n", getCPUSerialNum());

#if HAT_SUPPORT
    /* HAT protocol status */
    HardwareAccessTest& hat = HardwareAccessTest::getInstance();
    cli.message("Factory HAT protocol %s.\n", (hat.isInService())? "enabled" : "disabled");
#endif
}


void Console::CLICStatus::call(CommandLineInterpreter& cli)
{
    /* Get Current application-related various statuses */
    lu_bool_t mode = console.moduleMgr.isHBeatEnabled();
    cli.message("Automatic CAN Heartbeat is: %s\n", (mode == LU_TRUE)? "ON" : "OFF");

#if HAT_SUPPORT
    HardwareAccessTest& hat = HardwareAccessTest::getInstance();
    cli.message("HAT protocol is: %s\n", (hat.isInService())? "ON" : "OFF");
#endif
}


void Console::CLICCANStats::call(CommandLineInterpreter& cli)
{
    IIOModule *module = console.selectModule(cli);
    if(module != NULL)
    {
        IOM_ERROR res = IOM_ERROR_PARAM;
        typedef std::vector<MCMIOModule::StatsCANStr> CANStatsList;
        CANStatsList infoList;
        if(module->isMCM() == LU_TRUE)
        {
            MCMIOModule *mcmModule = dynamic_cast<MCMIOModule*>(module);
            if(mcmModule != NULL)
            {
                /* MCM Module does report CAN stats to Config Tool having 2 CAN ports */
                res = mcmModule->getCANStats(infoList);
            }
        }
        else
        {
            CANIOModule *canModule = dynamic_cast<CANIOModule*>(module);
            if(canModule != NULL)
            {
                CanStatsStr info;
                res = canModule->getCANStats(info);
                MCMIOModule::StatsCANStr entry;
                entry = info;
                if(res == IOM_ERROR_NONE)
                {
                    entry.validateAll();   //Sets all entries coming from CAN module as valid ones
                    infoList.push_back(entry);   //Compose 1-element vector for reporting
                }
            }
        }

        if(res != IOM_ERROR_NONE)
        {
            cli.message("Error getting CAN stats of module %s. Error %i: %s",
                        module->getModuleName(), res, IOM_ERROR_ToSTRING(res));
        }
        else
        {
            lu_uint32_t canID = 0;
            for(CANStatsList::iterator it = infoList.begin(), end = infoList.end(); it != end; ++it)
            {
                cli.message("CAN%u:\n", canID++);
                cli.message((*it).toString());
            }
        }
    }
    else
    {
        cli.message("Invalid module. Cancelled.\n");
    }
}


void Console::CLICModuleVersion::call(CommandLineInterpreter& cli)
{
    std::vector<IIOModule*> moduleList;
    moduleList = console.moduleMgr.getAllModules();
    cli.message("Modules found: %i\n", moduleList.size());
    IOModuleInfoStr moduleInfo;
    for(std::vector<IIOModule*>::iterator it=moduleList.begin(); it!=moduleList.end(); ++it)
    {
        (*it)->getInfo(moduleInfo);
        cli.message("  %s \n", moduleInfo.getVersionString().c_str());
    }
}


void Console::CLICModuleList::call(CommandLineInterpreter& cli)
{
    std::vector<IIOModule*> moduleList;
    moduleList = console.moduleMgr.getAllModules();
    cli.message("Modules found: %i\n", moduleList.size());
    IOModuleInfoStr moduleInfo;
    for(std::vector<IIOModule*>::iterator it=moduleList.begin(); it!=moduleList.end(); ++it)
    {
        (*it)->getInfo(moduleInfo);
        cli.message("  %s \n", moduleInfo.getStatusString().c_str());
    }
    cli.message("\n"
                "Symbol: OK=run|R=registered|C=configured|P=present|D=detected|S=disabled|X=has config.\n"
                "\n");
}


void Console::CLICRestartModule::call(CommandLineInterpreter& cli)
{
    /* Select a module*/
    IIOModule *targetModule = console.selectModule(cli);
    if(targetModule != NULL)
    {
        targetModule-> restart(MD_RESTART_WARM);
    }
}

void Console::CLICEraseFirmware::call(CommandLineInterpreter& cli){

    UPG_ERR result;

    /* Select a module*/
    IIOModule *targetModule = console.selectModule(cli);
    if(targetModule == NULL)
        return;

    /* Check upgrading state*/
    lu_bool_t upgrading = console.upgradeMgr.isUpgrading();
    if(upgrading == LU_TRUE)
    {
       /*Invalid state*/
       cli.message("Upgrading is in progress. Please try later.\n");
       return;
    }

    /* Erase */
    IOModuleIDStr moduleID = targetModule->getID();
    result = console.upgradeMgr.eraseModule(moduleID);
    if(result == UPG_ERR_NONE)
    {
        cli.message("Done! Erase command has been sent to the module %s.\n", moduleID.toString().c_str());
    }
    else
    {
        cli.message("Fail to erase module %s. Error (UPG_ERR): %i\n", moduleID.toString().c_str(), result);
    }

}

void Console::CLICGetLogLevel::call(CommandLineInterpreter& cli)
{
    cli.performGettingLogLevel();
}


void Console::CLICSetLogLevel::call(CommandLineInterpreter& cli)
{
    if (cli.performSettingLogLevel() == LU_TRUE)
    {
        cli.message("Setting Log Level: Done\n");
    }
}

void Console::CLICMonitorTest::call(CommandLineInterpreter& cli)
{
    LU_UNUSED(cli);
    console.monitorHdl.send_ping_msg();
}

void Console::CLICSetOLR::call(CommandLineInterpreter& cli)
{
    static OLR_STATE currentState = OLR_STATE_LAST;

    lu_uint32_t olrValue;
    XMSG_VALUE_OLR_CODE olrCode;
    AbstractMonitorProtocolLayer::MONPROTOCOL_ERROR err;

    cli.message("\tCurrent Off/Local/Remote status: %s (%d)\n",
                (currentState == OLR_STATE_LAST)? "Unknown" : OLR_STATE_ToSTRING(currentState),
                currentState
                );
    cli.message("\tSet Off/Local/Remote (Off(%d), Local(%d), Remote(%d); (9) to cancel): ",
                OLR_STATE_OFF, OLR_STATE_LOCAL, OLR_STATE_REMOTE
                );
    OLR_STATE minVal = std::min(OLR_STATE_OFF, OLR_STATE_LOCAL);
    minVal = std::min(minVal, OLR_STATE_REMOTE);
    OLR_STATE maxVal = std::max(OLR_STATE_OFF, OLR_STATE_LOCAL);
    maxVal = std::max(maxVal, OLR_STATE_REMOTE);

    if(cli.askAndCheck(&olrValue, minVal, maxVal) == LU_TRUE)
    {

            switch (olrValue)
            {
                case 0:
                    olrCode = XMSG_VALUE_OLR_CODE_OFF;
                    break;
                case 1:
                    olrCode = XMSG_VALUE_OLR_CODE_LOCAL;
                    break;
                case 2:
                    olrCode = XMSG_VALUE_OLR_CODE_REMOTE;
                    break;
                default:
                    cli.message("\tInvalid input mode:%d ",olrValue);
                    return;
            }

        err = console.monitorHdl.send_req_offLocalRemote_msg(olrCode);

        if(err == AbstractMonitorProtocolLayer::MONPROTOCOL_ERROR_NONE)
        {
            /* Read back status to check change */
            lucy_usleep(1000000);    //Leave time for inputs to settle down
            currentState = static_cast<OLR_STATE>(olrValue);

            cli.message("\nOffLocalRemote is set to: %s\n",
                        (currentState == OLR_STATE_LAST)? "Unknown" : OLR_STATE_ToSTRING(currentState));
        }
        else
        {
            cli.message("Operation error: %d\n", err);
        }
    }
    else
    {
        cli.message("Cancelled.\n");
    }
}
/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
IIOModule* Console::selectModule(CommandLineInterpreter& cli)
{
    std::string userInput;

    /*Get user input module*/
    if (cli.ask("Input a target module(e.g.\"SCM1\"): ", userInput) == LU_FALSE)
    {
        cli.message("Invalid input!\n");
        return NULL;
    }

    if (userInput.size() < MODULE_TYPE_LENGTH)
    {
        cli.message("Invalid input module %s; Expected format:\"SCM1\"\n", userInput.c_str());
        return NULL;
    }

    /*Convert module type string to enum*/
    std::string mType = userInput.substr(0, MODULE_TYPE_LENGTH);
    toUpper(mType);
    std::map<std::string, MODULE>::iterator it = mTypeMap.find(mType);
    if (it == mTypeMap.end())
    {
        cli.message("Invalid input module type: %s\n", mType.c_str());
        return NULL;
    }

    /*Convert module ID string to int*/
    lu_uint16_t mIDValue;
    std::string mID = userInput.substr(MODULE_TYPE_LENGTH,
                    userInput.size() - MODULE_TYPE_LENGTH);
    if (mID.length() > 0)
        mIDValue = atoi(mID.c_str());
    else
        mIDValue = 1; //Default used id if user didn't specify.

    /*Retrieve module*/
    IOModuleIDStr mIDStr;
    mIDStr.type = it->second;
    mIDStr.id = static_cast<MODULE_ID>(mIDValue - 1);
    IIOModule* module = moduleMgr.getModule(mIDStr);
    if (module == NULL)
    {
        /*Module not found*/
        cli.message("Module '%s' not found\n", mIDStr.toString().c_str());
    }
    return module;
}




/*
 *********************** End of file ******************************************
 */
