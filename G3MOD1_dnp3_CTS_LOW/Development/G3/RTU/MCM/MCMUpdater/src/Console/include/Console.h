/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Command Line Interpreter header module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   09/05/12      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(_G3_MCM_UPDATER_CONSOLE_H_)
#define _G3_MCM_UPDATER_CONSOLE_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <map>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "LockingMutex.h"
#include "CommandLineInterpreter.h"
#include "IIOModuleManager.h"
#include "UpgradeManager.h"
#include "MonitorProtocolHandler.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define MAX_CMD_AMOUNT 30

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * Command Line Interpreter - for debug / testing use only.
 */
class Console
{
public:
    /**
     * \brief Custom constructor
     *
     * \param database Reference to the Gemini database
     * \param gmoduleManager reference to the module manager
     *
     * \return None
     */
	Console(IIOModuleManager &moduleMgr, UpgradeManager &upgMgr,MonitorProtocolHandler &monitorHdl,IMCMUpdater* app);
	~Console();

	/**
     * \brief Start the Command Line Interpreter
     */
    void start(lu_bool_t internalConsole = LU_FALSE);

    /**
     * \brief Stop the Command Line Interpreter
     */
    void stop();

private:
    /* =============== Command definitions ============== */
    COMMANDCLASS(CLICAbout,             "about",        "info", LU_FALSE,   "About application");
    COMMANDCLASS(CLICIdentity,          "identity",     "id",   LU_FALSE,   "Current MCM running application");
    COMMANDCLASS(CLICStatus,            "status",       "",     LU_FALSE,   "Status of the application");
    COMMANDCLASS(CLICModuleList,        "mlist",        "",     LU_FALSE,   "Status list of modules connected.");
    COMMANDCLASS(CLICModuleVersion,     "mversion",     "",     LU_FALSE,   "List of module's Version and revision numbers.");
    COMMANDCLASS(CLICCANStats,          "canstats",     "can",  LU_FALSE,   "CAN bus statistics.");
    COMMANDCLASS(CLICGetLogLevel,       "getloglevel",  "gll",  LU_FALSE,   "List the log level of all subsystems.");
    COMMANDCLASS(CLICSetLogLevel,       "setloglevel",  "sll",  LU_FALSE,   "Set a log level for a specified subsystem.");
    COMMANDCLASS(CLICUpdate,            "upgrade",      "up",   LU_FALSE,   "Update the firmware of a specific module.");
    COMMANDCLASS(CLICExit,              "quit",         "",     LU_FALSE,   "Exit updater application");
    COMMANDCLASS(CLICRestart,           "restart",      "",     LU_FALSE,   "Restart updater application");
    COMMANDCLASS(CLICReboot,            "reboot",       "",     LU_FALSE,   "Reboot RTU");
    COMMANDCLASS(CLICShutdown,          "shutdown",     "",     LU_FALSE,   "Shut down RTU");
    COMMANDCLASS(CLICEraseFirmware,     "erase",        "",     LU_FALSE,   "Erase module firmware");
    COMMANDCLASS(CLICRestartModule,     "mrestart",     "",     LU_FALSE,   "Restart a selected module");
    COMMANDCLASS(CLICFactoryHAT,        "FactoryHAT",   "hat",  LU_FALSE,   "Enable/Disable factory HAT test mode");
    COMMANDCLASS(CLICHeartBeat,         "heartbeat",    "hb",   LU_FALSE,   "[Test] Enable/disable Slave Module HeartBeat.");
    COMMANDCLASS(CLICMonitorTest,       "monitortest",  "",     LU_FALSE,   "[Test] Ping monitor.");
    COMMANDCLASS(CLICSetOLR,            "setOLR",       "olr",  LU_FALSE,   "[Test] Set Off/Local/Remote.");

private:
    static const lu_uint32_t MAXCLIENTS = 5;    //Max number of clients allowed to connect to the Console Server
    static const lu_uint32_t SOCKETLISTENRETRYTIME_US = 500000; //Retry time for socket retry
    static const lu_uint32_t EXITWAIT_US = 500000; //Retry time to check for exiting the server
    static const lu_uint32_t SOCKETLISTENTOUT_MS = 100;  //Accept checking timeout

private:
    /**
     * \brief Ask user to select a module.
     */
    IIOModule* selectModule(CommandLineInterpreter& cli);

    IIOModuleManager   &moduleMgr;
    UpgradeManager     &upgradeMgr;
    MonitorProtocolHandler &monitorHdl;
    IMCMUpdater         *mcmUpdater;
    Logger& log;

    CommandCLI** cmdList; //list of supported commands
    lu_uint32_t  cmdListSize; //size of supported commands

    std::map<std::string, MODULE> mTypeMap;//Storing pairs of module enum and string

    lu_bool_t exitConsole;      //Exits the main loop that accepts connections
    MasterMutex clientAccess;   //Mutex to control access to the clients array
    CommandLineInterpreter* clients[MAXCLIENTS];    //Array of connected Consoles
};

#endif // !defined(_G3_MCM_UPDATER_CONSOLE_H_)

/*
 *********************** End of file ******************************************
 */
