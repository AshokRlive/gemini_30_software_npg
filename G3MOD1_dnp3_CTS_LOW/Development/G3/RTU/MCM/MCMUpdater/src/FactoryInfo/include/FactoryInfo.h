/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: FactoryInfo.h 9 Jan 2015 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/currentTrunk/G3/RTU/MCM/MCMUpdater/src/ModuleManager/include/FactoryInfo.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Factory Info holder object.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 9 Jan 2015 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   9 Jan 2015 pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef FACTORYINFO_H__INCLUDED
#define FACTORYINFO_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "versions.h"
#include "IOModuleCommon.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * Monostate Pattern class that keeps the factory information. Any instance
 * of this class will contain the same information.
 *
 * You can think as the FactoryInfo object as if it was a namespace
 */
class FactoryInfo
{
public:
    struct Info
    {
        ModuleUID moduleUID;    //SupplierID + Serial Number
        ModuleVersionStr featureRevision;
    public:
        Info& operator=(const Info& info)
        {
            moduleUID = info.moduleUID;
            featureRevision.major = info.featureRevision.major;
            featureRevision.minor = info.featureRevision.minor;
            return *this;
        }
    };

public:
    FactoryInfo() {};
    ~FactoryInfo() {};

    /**
     * Methods for get/set the whole factory information
     */
    static FactoryInfo::Info getInfo() {return m_Info;}
    static void setInfo(const FactoryInfo::Info info) {FactoryInfo::m_Info = info;}
    /**
     * Methods for get/set the factory information separately
     */
    static ModuleUIDStr getModuleUID() {return m_Info.moduleUID;}
    static void setModuleUID(const ModuleUIDStr mUID) {m_Info.moduleUID = mUID;}
    static ModuleVersionStr getFeatureRev() {return m_Info.featureRevision;}
    static void setFeatureRev(const ModuleVersionStr featureRev) {m_Info.featureRevision = featureRev;}

private:
    static FactoryInfo::Info m_Info;
};


#endif /* FACTORYINFO_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
