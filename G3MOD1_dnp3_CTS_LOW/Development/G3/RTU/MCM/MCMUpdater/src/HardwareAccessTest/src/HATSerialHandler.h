/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: HATSerialHandler.h 25 Aug 2015 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMUpdater/src/HardwareAccessTest/include/HATSerialHandler.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Serial port handler for HAT protocol messages
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 25 Aug 2015 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25 Aug 2015   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef HATSERIALHANDLER_H__INCLUDED
#define HATSERIALHANDLER_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <vector>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "hat_protocol.h"
#include "Logger.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief HardwareAccessTest object for Serial Port handler for HAT functionality
 *
 * This object is in charge of offering a proxy access to the Serial port.
 */
class HATSerialHandler
{
public:
    enum PORT_ID
    {
        PORT_ID_485,
        PORT_ID_232A,
        PORT_ID_232B,
        PORT_ID_LAST
    };
    struct SerialConfStr
    {
    public:
        lu_uint32_t baudrate;
        lu_uint8_t dataBits;
        lu_uint8_t stopBits;
        lu_uint8_t parity;
        lu_uint8_t portMode;
    public:
        SerialConfStr() :   baudrate(115200),
                            dataBits(8),
                            stopBits(1),
                            parity(0),
                            portMode(0)
        {};
    };

public:
    HATSerialHandler();
    virtual ~HATSerialHandler();

    /**
     * \brief Handles a Serial Config HAT message
     *
     * \param config Configuration message
     *
     * \return error code
     */
    virtual HAT_ERR handleSerialConfig(const HatSerialCfg& config);

    /**
     * \brief Handles a HAT message request for sending data over a serial port
     *
     * \param send Message containing data to send
     *
     * \return error code
     */
    virtual HAT_ERR handleSerialSend(const HatSerialSend& send);

    /**
     * \brief Handles a HAT message request for reading data from a serial port
     *
     * \param request Message containing the request
     * \param reply Where to store the data received and the result
     *
     * \return None. reply.status is modified accordingly to the operation outcome
     */
    virtual void handleSerialRead(const HatSerialRcvRq& request, HatSerialRcv& reply);

    /**
     * \brief Handles a HAT message request for write & read to/from a serial port
     *
     * This does a write and then waits to read over the serial port without
     * closing the file descriptor.
     * It is suitable for serial loopback.
     *
     * \param request Message containing data to send
     * \param reply Where to store the data received and the result
     *
     * \return None. reply.status is modified accordingly to the operation outcome
     */
    virtual void handleSerialSendReply(const HatSerialSend& request, HatSerialRcv& reply);


private:
    /**
     * \brief Open serial port
     *
     * \param portID Opens a serial port by its ID, checking for errors
     *
     * \return Serial port file descriptor. -1 when error.
     */
    lu_int32_t openSerial(const HAT_SERIAL_PORT_ID portID);

    /**
     * \brief Read data from serial port
     *
     * \param fd File descriptor number for the serial port
     * \param request Request message
     * \param reply Received data
     */
    template <typename RequestType>
    void readData(const lu_int32_t fd,
                    const RequestType& request,
                    HatSerialRcv& reply);

    /**
     * \brief Write data to the serial port
     *
     * \param fd File descriptor number for the serial port
     * \param toSend Data to write to the serial port
     *
     * \return Serial port file descriptor. -1 when error.
     */
    HAT_ERR writeData(const lu_int32_t fd, const HatSerialSend& toSend);

    /**
     * \brief Re-configure the serial port
     *
     * \param portConf Serial port configuration
     *
     * \return Outcome of the operation. true when success
     */
    HAT_ERR configure(const HatSerialCfg& portConf);

private:
    Logger&     log;

};


#endif /* HATSERIALHANDLER_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
