/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: HardwareAccessTest.h 24 Aug 2015 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMUpdater/src/HardwareAccessTest/include/HardwareAccessTest.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       HardwareAccessTest provides access to the hardware layer, mainly for
 *       testing purposes.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 24 Aug 2015 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24 Aug 2015   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef HARDWAREACCESSTEST_H__INCLUDED
#define HARDWAREACCESSTEST_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "Logger.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Hardware Access Test layer
 *
 * Main goals for this objects are to provide access to the RTU hardware, acting as
 * CAN gateway and backdoor command processor for diagnostics and test purposes.
 * Use it for the good, son.
 */
class HardwareAccessTest
{
public:
    static HardwareAccessTest&  getInstance()
    {
        static HardwareAccessTest instance;
        return instance;
    }

    static const lu_uint32_t SOCKETLISTENTOUT_MS = 100;  //Accept checking timeout
    static const lu_uint32_t MESSAGEBUFFERSIZE = 256;   //Incoming message buffer size

    void startService();
    void stopService();
    bool isInService();

private:
    /**
     * Custom constructor
     *
     * \param schedType Scheduler type to use for the thread
     * \param priority Thread priority
     * \param usePort TCP port number to use
     */
    HardwareAccessTest();
    ~HardwareAccessTest();

    HardwareAccessTest(HardwareAccessTest const&);// Don't Implement
    void operator=(HardwareAccessTest const&);    // Don't implement


private:
    Logger& log;

};



#endif /* HARDWAREACCESSTEST_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
