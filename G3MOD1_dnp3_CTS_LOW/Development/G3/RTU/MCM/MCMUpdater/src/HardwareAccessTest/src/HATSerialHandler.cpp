/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: HATSerialHandler.cpp 25 Aug 2015 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMUpdater/src/HardwareAccessTest/src/HATSerialHandler.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 25 Aug 2015 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25 Aug 2015   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdio.h>   /* Standard input/output definitions */
#include <unistd.h> /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "HATSerialHandler.h"
#include "FileUtil.h"
#include "timeOperations.h"
#include "MCMIOMap.h"
#include "dbg.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/**
 * \brief Macro for helping converting baud values to speed constants
 *
 * Use: BAUD_TO_SPEED(75, B50)
 *      else BAUD_TO_SPEED(115200, B57600)
 *      ...
 *
 * \param baudVal Max Value of bauds as integer
 * \param speed Value of bauds as speed_t (i.e. Bnnnn defined in termios.h) constant
 */
#define BAUD_TO_SPEED(baudVal, speed)   \
    if (portConf.baudrate < baudVal)    \
    {                                   \
        baud = speed;                   \
    }
/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 * brief Local configuration of the serial port
 */
struct serialDTRGPIOStr
{
    HAT_SERIAL_PORT_ID id;          //Serial port ID
    const char* serialPathName;     //System's name for this serial port
    lu_int32_t  DTRGPIONum;         //GPIO for DTR - not currently used
};

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 * \brief System-specific serial port device paths
 */
static serialDTRGPIOStr serialPortCfg[] =
{
    {HAT_SERIAL_PORT_ID_LAST, "",             -1 }, //Reserved for DEBUG port
    {HAT_SERIAL_PORT_ID_232A, "/dev/ttymxc4", MCMOutputGPIO_CONTROL_DTR},
    {HAT_SERIAL_PORT_ID_232B, "/dev/ttymxc1", MCMOutputGPIO_CONFIG_DTR},
    {HAT_SERIAL_PORT_ID_485,  "/dev/ttymxc3", -1 }
};


/*
 * \brief Debug prints the termios-style port configuration
 *
 * \param header Text to show beforehand
 * \param portID Port ID index
 * \param cfg Configuration to print, in POSIX termios format
 */
void dbg_PrintSerialCfg(const lu_char_t* header, const lu_uint32_t portID, const termios cfg)
{
    DBG_INFO("%s Port %i, '%s' @%i|%i, %i%c%i - cflag=0x%x",
                header,
                portID,
                serialPortCfg[portID].serialPathName,
                cfg.c_ispeed,
                cfg.c_ospeed,
                ((cfg.c_cflag & CSIZE) == CS7)? 7 : 8,
                ((cfg.c_cflag & PARENB) && (cfg.c_cflag & PARODD))? 'O' :
                    (cfg.c_cflag & PARENB)? 'E' : 'N',
                (cfg.c_cflag & CSTOPB)? 2 : 1,
                cfg.c_cflag
                );
}


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
HATSerialHandler::HATSerialHandler() :
                            log(Logger::getLogger(SUBSYSTEM_ID_TEST))
{
    //Initialise all serial ports to default values
    HatSerialCfg portConf;
    portConf.portID = HAT_SERIAL_PORT_ID_232A;
    portConf.baudrate = HAT_SERIAL_BAUD_RATE_115200;
    portConf.dataBits = HAT_SERIAL_DATA_BITS_8;
    portConf.stopBits = HAT_SERIAL_STOP_BITS_1;
    portConf.parity = HAT_SERIAL_PARITY_NONE;
    portConf.portMode = HAT_SERIAL_MODE_NONE;
    configure(portConf);
    portConf.portID = HAT_SERIAL_PORT_ID_232B;
    configure(portConf);
    portConf.portID = HAT_SERIAL_PORT_ID_485;
    configure(portConf);
}



HATSerialHandler::~HATSerialHandler()
{}


HAT_ERR HATSerialHandler::handleSerialConfig(const HatSerialCfg& config)
{
    return configure(config);
}


HAT_ERR HATSerialHandler::handleSerialSend(const HatSerialSend& toSend)
{
    HAT_ERR ret;
    lu_int32_t fd = openSerial(static_cast<HAT_SERIAL_PORT_ID>(toSend.portID));
    ret = writeData(fd, toSend);
    close(fd);
    return ret;
}


void HATSerialHandler::handleSerialRead(const HatSerialRcvRq& request, HatSerialRcv& reply)
{
    reply.portID = request.portID;
    reply.status = HAT_ERR_SOCKET_READ_ERR;

    lu_int32_t fd = openSerial(static_cast<HAT_SERIAL_PORT_ID>(request.portID));
    readData(fd, request, reply);
    close(fd);
}


void HATSerialHandler::handleSerialSendReply(const HatSerialSend& request,
                                                HatSerialRcv& reply)
{
    HAT_ERR ret;
    lu_int32_t fd = openSerial(static_cast<HAT_SERIAL_PORT_ID>(request.portID));
    ret = writeData(fd, request);
    if(ret == HAT_ERR_NONE)
    {
        readData(fd, request, reply);
    }
    else
    {
        reply.status = ret;
    }
    close(fd);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
lu_int32_t HATSerialHandler::openSerial(const HAT_SERIAL_PORT_ID portID)
{
    lu_int32_t fd = -1;

    if(portID >= HAT_SERIAL_PORT_ID_LAST)
    {
        log.error("Given port %i does not exist", portID);
    }
    else
    {
        fd = open(serialPortCfg[portID].serialPathName, O_RDWR | O_NOCTTY );
        if(fd < 0)
        {
            log.error("Unable to open serial port %i (%s)",
                        portID,
                        serialPortCfg[portID].serialPathName);
        }
    }
    return fd;
}

template <typename RequestType>
void HATSerialHandler::readData(const lu_int32_t fd,
                                const RequestType& request,
                                HatSerialRcv& reply)
{
    lu_int32_t res;
    FDVector fds;
    FDVector result;
    timeval timeout = ms_to_timeval(request.timeoutMs);

    reply.portID = request.portID;
    reply.status = HAT_ERR_SOCKET_READ_ERR;
    reply.rcvSize = 0;

    if(fd < 0)
    {
        reply.status = HAT_ERR_INIT;
        return;
    }

    reply.status = HAT_ERR_NONE;
    fds.push_back(fd);

    res = fileSelect(timeout, fds, result);
    if(res == 0)
    {
        //Timed out
        reply.status = HAT_ERR_NO_REPLY;
    }
    else if(res > 0)
    {
        res = read(fd, reply.rcvBuf, HAT_SERIAL_MAX_RCV);
        if(res > 0)
        {
            reply.rcvSize = res;
        }
    }
}


HAT_ERR HATSerialHandler::writeData(const lu_int32_t fd, const HatSerialSend& toSend)
{
    lu_int32_t result;
    if(fd < 0)
    {
        return HAT_ERR_INIT;
    }

    DBG_INFO("Sending to FD %i data(%u):'%s'", fd, toSend.sendSize, toSend.sendBuf);

    result = write(fd, toSend.sendBuf, toSend.sendSize);
    return (result < (lu_int64_t)toSend.sendSize)? HAT_ERR_SOCKET_WRITE_ERR : HAT_ERR_NONE;
}


HAT_ERR HATSerialHandler::configure(const HatSerialCfg& portConf)
{
    HAT_ERR ret = HAT_ERR_NONE;
    lu_int32_t fd;

    fd = openSerial(static_cast<HAT_SERIAL_PORT_ID>(portConf.portID));
    if(fd < 0)
    {
        return HAT_ERR_INIT;
    }

    /* configure port */
    struct termios cfg;

    tcgetattr(fd, &cfg);    //Get current config

    dbg_PrintSerialCfg("PortWas ", portConf.portID, cfg);

    //Data bits
    cfg.c_cflag &= ~CSIZE; //clear bits
    cfg.c_cflag |= (portConf.dataBits == HAT_SERIAL_DATA_BITS_7)? CS7 : CS8;

    //Parity
    cfg.c_cflag &= ~(PARENB | PARODD); //Disable parity
    switch (portConf.parity)
    {
        case HAT_SERIAL_PARITY_EVEN:
            cfg.c_cflag |= PARENB;
            break;
        case HAT_SERIAL_PARITY_ODD:
            cfg.c_cflag |= PARENB;
            cfg.c_cflag |= PARODD;
            break;
        case HAT_SERIAL_PARITY_NONE:
        default:
            break;
    }

    //Stop bits
    if (portConf.stopBits == HAT_SERIAL_STOP_BITS_2)
    {
        cfg.c_cflag |= CSTOPB;
    }
    else
    {
        cfg.c_cflag &= ~CSTOPB;
    }

    //Mode
    cfg.c_cflag &= ~CRTSCTS;    //default to NONE
    if (portConf.portMode == HAT_SERIAL_MODE_HARDWARE)
    {
        cfg.c_cflag &= ~CRTSCTS;    //set to HARDWARE FLOW CONTROL
    }

    /* Convert from baud integer to POSIX baud enum */
    speed_t baud;
    BAUD_TO_SPEED(     50      , B0    )
    else BAUD_TO_SPEED(75      , B50   )
    else BAUD_TO_SPEED(110     , B75   )
    else BAUD_TO_SPEED(134     , B110  )
    else BAUD_TO_SPEED(150     , B134  )
    else BAUD_TO_SPEED(200     , B150  )
    else BAUD_TO_SPEED(300     , B200  )
    else BAUD_TO_SPEED(600     , B300  )
    else BAUD_TO_SPEED(1200    , B600  )
    else BAUD_TO_SPEED(1800    , B1200 )
    else BAUD_TO_SPEED(2400    , B1800 )
    else BAUD_TO_SPEED(4800    , B2400 )
    else BAUD_TO_SPEED(9600    , B4800 )
    else BAUD_TO_SPEED(19200   , B9600 )
    else BAUD_TO_SPEED(38400   , B19200  )
    else BAUD_TO_SPEED(57600   , B38400  )
    else BAUD_TO_SPEED(115200  , B57600  )
    else BAUD_TO_SPEED(230400  , B115200 )
    else BAUD_TO_SPEED(460800  , B230400 )
    else BAUD_TO_SPEED(500000  , B460800 )
    else BAUD_TO_SPEED(576000  , B500000 )
    else BAUD_TO_SPEED(921600  , B576000 )
    else BAUD_TO_SPEED(1000000L, B921600 )
    else BAUD_TO_SPEED(1152000L, B1000000)
    else BAUD_TO_SPEED(1500000L, B1152000)
    else BAUD_TO_SPEED(2000000L, B1500000)
    else BAUD_TO_SPEED(2500000L, B2000000)
    else BAUD_TO_SPEED(3000000L, B2500000)
    else BAUD_TO_SPEED(3500000L, B3000000)
    else BAUD_TO_SPEED(4000000L, B3500000)
    else baud = __MAX_BAUD;

    cfsetispeed(&cfg, baud);
    cfsetospeed(&cfg, baud);

    cfg.c_cc[VTIME] = 10;   //VTIME is the time to wait for chars to be available to read
                            //It is a byte in 0.1 secs interval (255 = 25.5 secs)
    cfg.c_cflag |= (CLOCAL | CREAD);    //Enable proper port ownership and data reading
    cfg.c_lflag &= ~(ICANON | ECHO | ECHOE);    //Raw input mode
    cfg.c_oflag &= ~OPOST;                      //Raw output mode

    dbg_PrintSerialCfg("Request ", portConf.portID, cfg);

    if(tcsetattr(fd, TCSANOW, &cfg) != 0)
    {
        log.error("Unable to configure serial port %s. Error %s",
                    serialPortCfg[portConf.portID].serialPathName,
                    strerror(errno));
        ret = HAT_ERR_BADPARAMS;
    }
    close(fd);
    return ret;
}


/*
 *********************** End of file ******************************************
 */
