/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:HATHandler.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   10 Sep 2015     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "HATHandler.h"
#include "lu_types.h"
#include "dbg.h"
#include "assert.h"
#include "hat_utils.h"
#include "FactoryInfo.h"
#include "HATCANHandler.h"
#include "HATGPIOHandler.h"
#include "HATSerialHandler.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static HAT_ERR handleGPIORead(const HAT_GPIO_TYPE type,
                              const HatMessage& request,
                              HatMessage& reply);

/**
 * \brief Assign (casts) payload of the message to the request
 *
 * \param request HAT message that contains the request (as payload)
 * \param reqPtr Pointer to request that should be assigned to point to the payload
 *
 * \return true when payload size matches with request, and reqPtr updated
 *
 * \example Call it with:
 *              HatXXXReq* reqPointer;
 *              if(copyRequest(incomingHATmsg, reqPointer))
 *              {
 *                  //All OK, use reqPointer
 *              }
 */
template <typename T> static bool copyRequest(const HatMessage& request, T*& reqPtr);

/**
 * \brief Copies content of structure into HAT message payload
 *
 * \param rply Reply that should be assigned to the payload
 * \param reply HAT message that will contain the reply (as payload)
 *
 * \return true when payload size matches with request, and reqPtr updated
 *
 * \example Call it with:
 *              HatXXXRep reply;
 *              //...Compose reply
 *              if(copyReply(replyPointer, outgoingHATmsg))
 *              {
 *                  //All OK, use outgoingHATmsg
 *              }
 */
template <typename ReplyType>
static void copyReply(const ReplyType& rply, HatMessage& reply);

/* debug functions for printing CAN message data */
static void dbg_PrintCANSend(const HatCANSend& req);
static void dbg_PrintCANReceive(const HatCANReceive& reply);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
//Unique instances of the message handlers
static HATCANHandler canHandler;
static HATGPIOHandler gpioHandler;
static HATSerialHandler serialHandler;

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
HAT_ERR hat_handler_impl(const HatMessage* request, HatMessage* reply)
{
    DBG_TRACK("hat_handler_impl() called. id:%d", request->header.id);

    if( (request == NULL) || (reply == NULL) )
    {
        return HAT_ERR_BADPARAMS;
    }
    HAT_ERR ret = HAT_ERR_NONE;

    reply->header.id   = (HAT_MSG_ID)(request->header.id + 1);

    switch (request->header.id)
    {
        case HAT_MSG_ID_HATVERSION_C:
        {
            HatProtocolVersion versionHAT;
            versionHAT.version.major = HAT_PROTOCOL_VERSION_MAJOR;
            versionHAT.version.minor = HAT_PROTOCOL_VERSION_MINOR;
            copyReply(versionHAT, *reply);
        }
        break;

        case HAT_MSG_ID_VERSION_C:
        {
            HatVersion version;
            strcpy(version.versionAPI, "API1.0");
            strcpy(version.versionSDP, "SDPI2.0");
            strcpy(version.versionSVN, "SVN3.0" );
            copyReply(version, *reply);
        }
        break;

        case HAT_MSG_ID_CAN_PROXY_C:
        {
            HatCANSend* req;
            HatCANReceive rply;

            rply.status = HAT_ERR_BADPARAMS;

            if(!copyRequest(*request, req))
            {
                return HAT_ERR_INVALID_PAYLOAD_SIZE;
            }

            dbg_PrintCANSend(*req);

            ret = canHandler.handleCANSend(*req, rply);
            if(ret != HAT_ERR_NONE)
            {
                DBG_ERR("failed to handle CAN send"); //Note that handleCANSend updates reply.status
            }
            copyReply(rply, *reply);

            dbg_PrintCANReceive(rply);
        }
        break;

        case HAT_MSG_ID_MCM_IO_READ_DI_C:
        {
            ret = handleGPIORead(HAT_GPIO_TYPE_DIGITAL_INPUT, *request, *reply);
        }
        break;
//        case HAT_MSG_ID_MCM_IO_WRITE_C:

        case HAT_MSG_ID_MCM_SERIAL_CONF_C:
        {
            HatSerialCfg* req = NULL;
            HatResult rply;
            rply.status = HAT_ERR_BADPARAMS;
            if(!copyRequest(*request, req))
            {
                return HAT_ERR_INVALID_PAYLOAD_SIZE;
            }
            ret = serialHandler.handleSerialConfig(*req);
            rply.status = ret;
            copyReply(rply, *reply);
        }
        break;

        case HAT_MSG_ID_MCM_SERIAL_WRITE_C:
        {
            HatSerialSend* req = NULL;
            HatResult rply;
            rply.status = HAT_ERR_BADPARAMS;
            if(!copyRequest(*request, req))
            {
                return HAT_ERR_INVALID_PAYLOAD_SIZE;
            }
            ret = serialHandler.handleSerialSend(*req);
            rply.status = ret;
            copyReply(rply, *reply);
        }
        break;

        case HAT_MSG_ID_MCM_SERIAL_READ_C:
        {
            HatSerialRcvRq* req = NULL;
            HatSerialRcv rply;
            rply.status = HAT_ERR_BADPARAMS;
            if(!copyRequest(*request, req))
            {
                return HAT_ERR_INVALID_PAYLOAD_SIZE;
            }
            serialHandler.handleSerialRead(*req, rply);
            copyReply(rply, *reply);
        }
        break;

        case HAT_MSG_ID_MCM_SERIAL_READWRITE_C:
        {
            HatSerialSend* req = NULL;
            HatSerialRcv rply;
            rply.status = HAT_ERR_BADPARAMS;
            if(!copyRequest(*request, req))
            {
                return HAT_ERR_INVALID_PAYLOAD_SIZE;
            }
            serialHandler.handleSerialSendReply(*req, rply);
            copyReply(rply, *reply);
        }
        break;

        default:
            DBG_ERR("unsupported message, id:%d", request->header.id);
            ret = HAT_ERR_UNSUPPORTED;
    }

    DBG_TRACK("hat_handler_impl() exit");
    return ret;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
static HAT_ERR handleGPIORead(const HAT_GPIO_TYPE type, const HatMessage& request, HatMessage& reply)
{
    if(request.payloadSize != sizeof(HatPinIOReadReq))
    {
        DBG_ERR("invalid hat payload size:%d, expected:%d",
                        request.payloadSize,
                        sizeof(HatPinIOReadReq));
        return HAT_ERR_INVALID_PAYLOAD_SIZE;
    }
    HatPinIOReadReq* req = (HatPinIOReadReq*)(request.payload);
    HatPinIOData rply;
    if(gpioHandler.handleGPIORead(type, *req, rply) == LU_FALSE)
    {
        DBG_ERR("failed to handle GPIO Read operation");
        //Note that handleGPIORead updates reply.status
    }
    memcpy(reply.payload, &rply, sizeof(rply));
    reply.payloadSize = sizeof(rply);
    return HAT_ERR_NONE;
}


template <typename T>
static bool copyRequest(const HatMessage& request, T*& reqPtr)
{
    if(request.payloadSize != sizeof(T))
    {
        DBG_ERR("invalid HAT payload size:%d, expected:%d",
                    request.payloadSize, sizeof(T));
        return false;
    }
    reqPtr = (T*)(request.payload);
    return true;
}


template <typename ReplyType>
static void copyReply(const ReplyType& rply, HatMessage& reply)
{
    memcpy(reply.payload, &rply, sizeof(rply));
    reply.payloadSize = sizeof(rply);
}


static void dbg_PrintCANSend(const HatCANSend& request)
{
    DBG_INFO("print CANSend. canbus:%d timeout:%d sendSize:%d",
                 request.canBus,
                 request.timeoutMs,
                 request.sendSize);

}

static void dbg_PrintCANReceive(const HatCANReceive& reply)
{
    DBG_INFO("print CANReceive. canbus:%d rcvSize:%d status:%d",
                    reply.canBus,
                    reply.rcvSize,
                    reply.status);

    if(reply.rcvSize == sizeof(ModuleInfoStr))
    {
        ModuleInfoStr* minfo = (ModuleInfoStr* )(reply.rcvBuf);
        DBG_INFO("print ModuleInfo. mtype:%d serial:%d supplier:%d",
                    minfo->moduleType,
                    minfo->moduleUID.serialNumber,
                    minfo->moduleUID.supplierId
                    );
    }

}


/*
 *********************** End of file ******************************************
 */
