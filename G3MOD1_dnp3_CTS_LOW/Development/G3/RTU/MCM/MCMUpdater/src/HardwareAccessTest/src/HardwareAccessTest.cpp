/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: HardwareAccessTest.cpp 24 Aug 2015 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMUpdater/src/HardwareAccessTest/src/HardwareAccessTest.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 24 Aug 2015 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24 Aug 2015   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <errno.h>
#include <netinet/in.h>
#include <exception>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "HardwareAccessTest.h"
#include "RTUInfo.h"
#include "svnRevision.h"
#include "hat_server.h"

#include "HATHandler.h"
#include "dbg.h"
#include "Thread.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
class HATThread: private Thread
{
public:
    HATThread();
    THREAD_ERR stop();
    THREAD_ERR start();
protected:
    void threadBody();
};

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static HATThread* hatThread = NULL;

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

HardwareAccessTest::HardwareAccessTest() :
        log(Logger::getLogger(SUBSYSTEM_ID_CONSOLE))
{
}

HardwareAccessTest::~HardwareAccessTest()
{
    stopService();
}


bool HardwareAccessTest::isInService()
{
	return hatThread != NULL;
}

void HardwareAccessTest::startService()
{
	if(hatThread == NULL) {
	    hatThread = new HATThread();
	    hatThread->start();
	    log.warn("HAT is started");
	}
}

void HardwareAccessTest::stopService()
{
	if(hatThread != NULL){
	   hatThread->stop();

	   delete hatThread;
	   hatThread = NULL;
	}

}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
HATThread::HATThread() :
                Thread(SCHED_TYPE_FIFO, Thread::PRIO_MIN, LU_TRUE, "HAT")
{
}

THREAD_ERR HATThread::start()
{
    return Thread::start();
}

THREAD_ERR HATThread::stop()
{
    THREAD_ERR err = Thread::stop();

    hat_server_close();

    DBG_INFO("HAT is stopped");
    return err;
}

void HATThread::threadBody()
{
    DBG_INFO("HAT is started");

    if(hat_server_init(5000, &hat_handler_impl) == HAT_ERR_NONE)
    {
        hat_server_run();
    }
    else
    {
        DBG_ERR("Failed to initialise server!");
    }

    DBG_INFO("Exit HAT thread!");
}

/*
 *********************** End of file ******************************************
 */
