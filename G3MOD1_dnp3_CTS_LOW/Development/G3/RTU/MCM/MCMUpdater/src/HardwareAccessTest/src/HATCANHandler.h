/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: HATCANHandler.h 25 Aug 2015 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMUpdater/src/HardwareAccessTest/include/HATCANHandler.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       HardwareAccessTest object that handles CAN access.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 25 Aug 2015 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25 Aug 2015   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef HATCANHANDLER_H__INCLUDED
#define HATCANHANDLER_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "CANFraming.h"
#include "hat_protocol.h"
#include "Logger.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief HardwareAccessTest object that handles CAN access
 *
 * This object is in charge of offering a proxy access to the CAN bus.
 */
class HATCANHandler
{
public:
    HATCANHandler();
    virtual ~HATCANHandler();

    virtual HAT_ERR handleCANSend(HatCANSend& send, HatCANReceive& receive);

private:
    CANFraming& getCANFraming(const int canBus)
    {
		switch (canBus)
		{
		case 0:
			return *mp_can0;

		case 1:
			return *mp_can1;

		default:
			return *mp_can0;
		}
    }

	void setFilter(HatCANSend& send);

private:
    CANFraming* mp_can0;
    CANFraming* mp_can1;
    Logger& log;

};


#endif /* HATCANHANDLER_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
