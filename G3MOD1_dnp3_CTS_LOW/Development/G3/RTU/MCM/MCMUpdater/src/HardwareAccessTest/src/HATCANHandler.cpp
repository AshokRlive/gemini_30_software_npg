/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: HATCANHandler.cpp 25 Aug 2015 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMUpdater/src/HardwareAccessTest/src/HATCANHandler.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 25 Aug 2015 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25 Aug 2015   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "HATCANHandler.h"

#define SUBSYSTEM_NAME "HATCANHandler"  //Defined for dbg.h
#include "dbg.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

HATCANHandler::HATCANHandler() : mp_can0(NULL),
                                mp_can1(NULL),
                                log(Logger::getLogger(SUBSYSTEM_ID_CANFRAMING))
{
	CANInterface::getInstance(COMM_INTERFACE_CAN0).setEnabled(LU_TRUE);
	CANInterface::getInstance(COMM_INTERFACE_CAN1).setEnabled(LU_TRUE);

    mp_can0 = new CANFraming(COMM_INTERFACE_CAN0,LU_FALSE);
    mp_can1 = new CANFraming(COMM_INTERFACE_CAN1,LU_FALSE);
}


HATCANHandler::~HATCANHandler()
{
    delete mp_can0;
    delete mp_can1;
}


void HATCANHandler::setFilter(HatCANSend& send)
{
	CanFilterTable filterTable;
	struct can_filter filter;
	CANHeaderStr  *tempCanHeader, *rawCanHeader;

	filter.can_id = 0;
	filter.can_mask = 0;

    rawCanHeader = reinterpret_cast<CANHeaderStr*>(send.sendBuf);

	//Filter:
	tempCanHeader = (CANHeaderStr*) ((&filter.can_id));
	tempCanHeader->messageType = rawCanHeader->messageType;
	tempCanHeader->messageID   = rawCanHeader->messageID + 1;
	tempCanHeader->deviceSrc   = rawCanHeader->deviceDst;
	tempCanHeader->deviceIDSrc = rawCanHeader->deviceIDDst;
	tempCanHeader->deviceDst   = rawCanHeader->deviceSrc;
	tempCanHeader->deviceIDDst = rawCanHeader->deviceIDSrc;

	//Mask:
	tempCanHeader = (CANHeaderStr*) ((&filter.can_mask));
	tempCanHeader->messageType = CANHEADERDEF_MASK_MESSAGETYPE; //exactly the specified message type
	tempCanHeader->messageID = CANHEADERDEF_MASK_MESSAGEID; //exactly the specified message ID
	tempCanHeader->deviceSrc = CANHEADERDEF_MASK_DEVICE; // and so on...
	tempCanHeader->deviceIDSrc = CANHEADERDEF_MASK_DEVICEID;
	tempCanHeader->deviceDst = CANHEADERDEF_MASK_DEVICE;
	tempCanHeader->deviceIDDst = CANHEADERDEF_MASK_DEVICEID;
	tempCanHeader->fragment = 0; //accept fragmented & non-fragmented messages
	filterTable.setTable(&filter, 1);

	getCANFraming(send.canBus).setFilter(filterTable);
}


HAT_ERR HATCANHandler::handleCANSend(HatCANSend& send, HatCANReceive& receive)
{
    HAT_ERR res = HAT_ERR_INIT;
	CANHeaderStr rawCanHeader;

    //rawCanHeader = reinterpret_cast<CANHeaderStr*>(send->sendBuf);
	memcpy(&rawCanHeader, send.sendBuf, sizeof(rawCanHeader));

    PayloadRAW mMsgPayload; // module message payload
    mMsgPayload.payloadPtr = send.sendBuf  + sizeof(CANHeaderStr);
    mMsgPayload.payloadLen = send.sendSize - sizeof(CANHeaderStr);

    RAWModuleMessage moduleMsg(&mMsgPayload);
    moduleMsg.header.messageType   = (MODULE_MSG_TYPE)rawCanHeader.messageType;
    moduleMsg.header.messageID     = (MODULE_MSG_ID)  rawCanHeader.messageID;
    moduleMsg.header.destination   = (MODULE)         rawCanHeader.deviceDst;
    moduleMsg.header.destinationID = (MODULE_ID)      rawCanHeader.deviceIDDst;
    moduleMsg.header.source        = (MODULE)         rawCanHeader.deviceSrc;
    moduleMsg.header.sourceID      = (MODULE_ID)      rawCanHeader.deviceIDSrc;

    log.info("writing CAN...dst:%d dstid:%d src:%d,srcid:%d msgid:%d msgtype:%d, payloadSize:%d",
    		moduleMsg.header.destination,
			moduleMsg.header.destinationID,
    		moduleMsg.header.source,
			moduleMsg.header.sourceID,
			moduleMsg.header.messageID,
			moduleMsg.header.messageType,
			mMsgPayload.payloadLen
			);

    /* Set filter for receiving reply*/
	setFilter(send);


    /* Write and wait for a reply */
	CANFraming& canSocket = getCANFraming(send.canBus);
    CAN_ERROR ret = canSocket.write(&moduleMsg);

    if (ret == CAN_ERROR_NONE)
    {
    	log.info("writing CAN successfully");

    	struct timeval tout = ms_to_timeval(send.timeoutMs);
    	ModuleMessage *msgRead;

    	ret = canSocket.read(&msgRead, &tout);
		if (ret == CAN_ERROR_NONE)
		{
			DBG_INFO("received CAN reply");

			receive.canBus = send.canBus;

			// Copy header
			CANHeaderStr replyHeader;
			const lu_uint32_t headerSize = sizeof(CANHeaderStr);
			replyHeader.messageType = msgRead->header.messageType   ;
			replyHeader.messageID   = msgRead->header.messageID     ;
			replyHeader.deviceDst   = msgRead->header.destination   ;
			replyHeader.deviceIDDst = msgRead->header.destinationID ;
			replyHeader.deviceSrc   = msgRead->header.source        ;
			replyHeader.deviceIDSrc = msgRead->header.sourceID      ;
            memcpy(receive.rcvBuf,  &replyHeader, headerSize);

			// Copy payload
			const lu_uint32_t maxPayloadSize = HAT_CAN_MAX_SIZE - headerSize;
			(*msgRead).getPayload(&mMsgPayload);
			int len = mMsgPayload.payloadLen > maxPayloadSize ? maxPayloadSize : mMsgPayload.payloadLen;
	    	memcpy(receive.rcvBuf+headerSize, mMsgPayload.payloadPtr, len);
	    	receive.rcvSize = len+headerSize;

            res = HAT_ERR_NONE;
		}
		else
		{
			log.error("Handling CAN Send::reading CAN err:%d! timeout:%d",ret, send.timeoutMs);
			receive.rcvSize = 0;
			res = HAT_ERR_NO_REPLY;
		}
    }
    else
    {
        log.error("Handling CAN Send::failed to write CAN, err:%d",ret);
        res = HAT_ERR_CAN_SEND_FAILURE;
    }

    receive.status = res;
    DBG_INFO("Handling CAN Send::finished");
    return res;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
