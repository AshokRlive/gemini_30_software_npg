/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: HATGPIOHandler.cpp 25 Aug 2015 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMUpdater/src/HardwareAccessTest/src/HATGPIOHandler.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 25 Aug 2015 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25 Aug 2015   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "HATGPIOHandler.h"
#include "MCMIOMap.h"

#define SUBSYSTEM_NAME "HATGPIOHandler"  //Defined for dbg.h
#include "dbg.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static const lu_char_t* findAIName(const HatIOPinIDStr gpioID)
{
    switch (GPIO(gpioID.bank, gpioID.id))
    {
        case 0:
            return MCMANALOGUEINPUT_VLOGIC;
            break;
        case 1:
            return MCMANALOGUEINPUT_5V;
            break;
        case 2:
            return MCMANALOGUEINPUT_3V3;
            break;
        case 3:
            return MCMANALOGUEINPUT_VLOCAL;
            break;
        case 4:
            return MCMANALOGUEINPUT_VREMOTE;
            break;
        case 5:
            return MCMANALOGUEINPUT_CORETEMP;
            break;
        default:
            break;
    }
    return "";
}


static const lu_char_t* findAIPath(const HatIOPinIDStr gpioID)
{
    switch (GPIO(gpioID.bank, gpioID.id))
    {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
            return MCMANALOGUEINPUT_DEVICE1;
            break;
        case 5:
            return MCMANALOGUEINPUT_DEVICE0;
            break;
        default:
            break;
    }
    return "";
}

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

HATGPIOHandler::HATGPIOHandler()
{}

HATGPIOHandler::~HATGPIOHandler()
{}


lu_bool_t HATGPIOHandler::handleGPIORead(const HAT_GPIO_TYPE type, HatPinIOReadReq& request, HatPinIOData& reply)
{
    lu_int32_t ret;
    FILE *fdValue;              //GPIO value file
    lu_uint8_t gpio = GPIO(request.gpio.bank, request.gpio.id);
    reply.gpio = request.gpio;

    /* TODO: pueyos_a - check req feat revision */
    //reply.status = HAT_ERR_BADVERSION;

    switch (type)
    {
    case HAT_GPIO_TYPE_DIGITAL_INPUT:
        /* Initialise GPIO as input */
        ret = initGPIO(gpio, /*Unused*/GPIOLIB_DIRECTION_INPUT, &fdValue);
        if( (ret != 0) || (fdValue < 0) )
        {
            reply.status = HAT_ERR_INIT;
            return LU_FALSE;
        }
        lu_bool_t rawValue;
        ret = readGPIO(fdValue, &rawValue);
        if(ret <= 0)
        {
            reply.status = HAT_ERR_NO_REPLY;
        }
        closeGPIO(fdValue);
        reply.status = HAT_ERR_NONE;
        reply.value.dValue = rawValue;
        break;

    case HAT_GPIO_TYPE_ANALOGUE_INPUT:
        /* Initialise GPIO as input */
        ret = initAI(findAIName(request.gpio), findAIPath(request.gpio), &fdValue);
        if( (ret != 0) || (fdValue < 0) )
        {
            reply.status = HAT_ERR_INIT;
            return LU_FALSE;
        }
        lu_int32_t intValue;
        ret = readAIInt(fdValue, &intValue);
        if(ret <= 0)
        {
            reply.status = HAT_ERR_NO_REPLY;
        }
        closeGPIO(fdValue);
        reply.status = HAT_ERR_NONE;
        reply.value.iValue = intValue;
        break;

    default:
        reply.status = HAT_ERR_BADPARAMS;
        break;
    }
    return (reply.status == HAT_ERR_NONE)? LU_TRUE : LU_FALSE;
}


lu_bool_t HATGPIOHandler::handleGPIOWrite(const HAT_GPIO_TYPE type, HatPinIOData& request, HatResult& reply)
{
    lu_int32_t ret;
    FILE *fdValue;              //GPIO value file
    lu_uint8_t gpio = GPIO(request.gpio.bank, request.gpio.id);

    /* TODO: pueyos_a - check req feat revision */
    //reply.status = HAT_ERR_BADVERSION;

    switch (type)
    {
    case HAT_GPIO_TYPE_DIGITAL_OUTPUT:
        /* Initialise GPIO as output */
        ret = initGPIO(gpio, /*Unused*/GPIOLIB_DIRECTION_OUTPUT, &fdValue);
        if( (ret != 0) || (fdValue < 0) )
        {
            reply.status = HAT_ERR_INIT;
            return LU_FALSE;
        }
        ret = writeGPIO(fdValue, request.value.dValue);
        if(ret <= 0)
        {
            reply.status = HAT_ERR_NO_REPLY;
            return LU_FALSE;
        }
        reply.status = HAT_ERR_NONE;
        break;

    default:
        reply.status = HAT_ERR_BADPARAMS;
        break;
    }
    return (reply.status == HAT_ERR_NONE)? LU_TRUE : LU_FALSE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
