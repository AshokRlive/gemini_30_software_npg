/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   01/09/13      wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_D3100DEC_3501_4cbe_B01C_7B9F2DC90573__INCLUDED_)
#define EA_D3100DEC_3501_4cbe_B01C_7B9F2DC90573__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "StringUtil.h"

#include "lu_types.h"

#include "Table.h"

#include "IOModuleCommon.h"
#include "ModuleMessage.h"
#include "ModuleProtocol.h"

/* Forward declaration */
class IIOModule;


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef Table<IIOModule*> IOModuleTable;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class IIOModule
{
public:
    struct sendReplyStr
    {
        /** Message type */
        MODULE_MSG_TYPE  messageType;
        /** Message ID */
        MODULE_MSG_ID    messageID;
        /** Payload */
        PayloadRAW      *messagePtr;
        /** Expected message type */
        MODULE_MSG_TYPE  rMessageType;
        /** Expected message id */
        MODULE_MSG_ID    rMessageID;
        /** where the reply message is saved */
        ModuleMessage   *rMessagePtr;
        /** Reply timeout */
        struct timeval timeout;
    };

    IIOModule( MODULE type,MODULE_ID id)
                             :mID(type,id)
    {
        std::string n = genModuleName(type,id);
        mName = new lu_char_t[n.size() + 1];
        strcpy(mName,n.c_str());

        /*Init module status*/
        memset(&mStatus,0x00, sizeof(mStatus));
        memset(&mInfo,  0x00, sizeof(mInfo));
    };

    virtual ~IIOModule(){
        delete[] mName;
    };

	/**
	 * \brief Send a message to the module
	 * 
	 * \param messagePtr Message to send
	 * 
	 * \return Error code
	 */
	virtual IOM_ERROR sendMessage(ModuleMessage *messagePtr) = 0;

    /**
      * \brief Send a message to the module
      *
      * \param messageType Message type
      * \param messageID Message ID
      * \param message Payload
      *
      * \return Error code
      */
	virtual IOM_ERROR sendMessage( MODULE_MSG_TYPE  messageType,
	                               MODULE_MSG_ID    messageID,
	                               PayloadRAW      *messagePtr
	                             ) = 0;

	/**
     * \brief Send a message to the module and wait for a reply
     *
     * \param messagePtr Structure containing the message to send and the
     * expected reply parameters
     *
     * \return Error code
     */
    virtual IOM_ERROR sendMessage(sendReplyStr *messagePtr) = 0;

	/**
	 * \brief Decode a module message
	 * 
	 * If necessary the module/channels internal state is updated
	 * 
	 * \param message Message to decode
	 * 
	 * \return Error code
	 */
	virtual IOM_ERROR decodeMessage(ModuleMessage *message) = 0;

	/**
	 * \brief Notify the timer tick is expired
	 * 
	 * This function should be periodically called in order to keep the internal logic
	 * updated (internal timers, etc,...)
	 * 
	 * \param dTime Time elapsed from the previous tick (in ms)
	 * 
	 * \return Error code
	 */
	virtual IOM_ERROR tickEvent(lu_uint32_t dTime) = 0;


	/**
	 * \brief Return the module global information
	 *
	 * \param version Where the module information are saved
	 *
	 * \return Error code
	 */
	virtual void getInfo(IOModuleInfoStr& version) = 0;

    /**
     * \brief Return module information details
     *
     * \param version Where the module information is saved
     */
    virtual void getDetails(IOModuleDetailStr &moduleInfo) = 0;

	virtual ModuleUID getModuleUID() = 0;

	virtual lu_bool_t isReadyForUpgrade() = 0;

	virtual std::string getStateName() = 0;

    /**
     * \brief Send a restart command to all the slave modules
     *
     * \param type Type of restart desired (as warm, cold, etc)
     *
     * \return error code
     */
	virtual IOM_ERROR restart(MD_RESTART type) = 0;

	/**
     * \brief Ping a module
     *
     * If the received payload is different from the sent payload
     * an error is reported
     *
     * \param payloadSize Payload size
     * \param timeout Timeout in ms (maximum timeout 1000 ms)
     *
     * \return Error code
     */
    virtual IOM_ERROR ping(lu_uint32_t timeout, lu_uint32_t payloadSize = 8) = 0;


    const lu_char_t*  getModuleName()
    {
        return mName;
    };

    /**
     * \brief Checks if this module is present on bus.
     */
    lu_bool_t isPresent(){return mStatus.present;};


    /**
     * \brief Get Module ID
     *
     * \return Module ID (type and address)
     */
    const IOModuleIDStr getID()
    {
        return mID;
    };

    lu_bool_t isMCM()
    {
        if(mID.type == MODULE_MCM)
            return LU_TRUE;
        else
            return LU_FALSE;
    };

protected:
    const IOModuleIDStr   mID;  //Module ID
    ModuleInfoStr   mInfo;      //Module information
    IOModuleStatus  mStatus;    //Module status flags

private:
    lu_char_t*      mName ; //Module name
};


#endif // !defined(EA_D3100DEC_3501_4cbe_B01C_7B9F2DC90573__INCLUDED_)

/*
 *********************** End of file ******************************************
 */




