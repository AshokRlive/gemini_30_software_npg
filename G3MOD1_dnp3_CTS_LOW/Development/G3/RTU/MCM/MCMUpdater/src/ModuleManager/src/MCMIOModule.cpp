/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MCM IO Module implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   05/08/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "versions.h"
#include "MCMIOModule.h"
#include "svnRevision.h"
#include "CANInterface.h"
#include "FactoryInfo.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
MCMIOModule::StatsCANStr& MCMIOModule::StatsCANStr::operator=(const CanStatsStr& canStats)
{
    canError            = canStats.canError;
    canBusError         = canStats.canBusError;
    canArbitrationError = canStats.canArbitrationError;
    canDataOverrun      = canStats.canDataOverrun;
    canBusOff           = canStats.canBusOff;
    canErrorPassive     = canStats.canErrorPassive;
    canTxCount          = canStats.canTxCount;
    canRxCount          = canStats.canRxCount;
    canRxLost           = canStats.canRxLost;
    return *this;
}


void MCMIOModule::StatsCANStr::validateAll()
{
    validCANError = 1;
    validCANBusError = 1;
    validCANArbitrationError = 1;
    validCANDataOverrun = 1;
    validCANBusOff = 1;
    validCANErrorPassive = 1;
    validCANTxCount = 1;
    validCANRxCount = 1;
    validCANRxLost = 1;
}


std::string MCMIOModule::StatsCANStr::toString()
{
    std::stringstream ss;
    putValidValue(ss, "Error",           validCANError,          canError);
    putValidValue(ss, "BusError",        validCANBusError,       canBusError);
    putValidValue(ss, "ArbitError",      validCANArbitrationError, canArbitrationError);
    putValidValue(ss, "DataOverrun",     validCANDataOverrun,    canDataOverrun);
    putValidValue(ss, "BusOff",          validCANBusOff,         canBusOff      );
    putValidValue(ss, "ErrorPassive",    validCANErrorPassive,   canErrorPassive);
    putValidValue(ss, "TxCount",         validCANTxCount,        canTxCount     );
    putValidValue(ss, "RxCount",         validCANRxCount,        canRxCount     );
    putValidValue(ss, "RxLost",          validCANRxLost,         canRxLost      );
    return ss.str();
}


MCMIOModule::MCMIOModule(MODULE_ID moduleID)
                        :IIOModule(MODULE_MCM,moduleID),
                         log(Logger::getLogger(SUBSYSTEM_ID_MODULE))

{
     mStatus.registered = LU_TRUE;
     mStatus.ok         = LU_TRUE;
     mStatus.configured = LU_FALSE;
     mStatus.present    = LU_TRUE;
     mStatus.detected   = LU_TRUE;
     mStatus.disabled   = LU_FALSE;
     mStatus.hasConfig  = LU_TRUE;

     /* Read moduleUID from NVRAM factory info */
     mStatus.moduleUID = FactoryInfo::getModuleUID();
     mInfo.moduleUID = FactoryInfo::getModuleUID();

     mInfo.featureRevision = FactoryInfo::getFeatureRev(); //Feat. rev. from NVRAM factory info */

     mInfo.application.software.relType = MCM_SOFTWARE_VERSION_RELTYPE;
     mInfo.application.software.version.major = MCM_SOFTWARE_VERSION_MAJOR;
     mInfo.application.software.version.minor = MCM_SOFTWARE_VERSION_MINOR;
     mInfo.application.software.patch = MCM_SOFTWARE_VERSION_PATCH;

     mInfo.application.systemAPI.major = MODULE_SYSTEM_API_VER_MAJOR;
     mInfo.application.systemAPI.minor = MODULE_SYSTEM_API_VER_MINOR;

     mInfo.bootloader.systemAPI.major = 0;//Not available
     mInfo.bootloader.systemAPI.minor = 0;//Not available
     mInfo.bootloader.software.relType = 0;
     mInfo.bootloader.software.version.major = 0;
     mInfo.bootloader.software.version.minor = 0;
     mInfo.bootloader.software.patch = 0;

     mInfo.svnRevisionBoot = _SVN_REVISION;
     mInfo.svnRevisionApp = _SVN_REVISION;
}


MCMIOModule::~MCMIOModule()
{
}

IOM_ERROR MCMIOModule::sendMessage(ModuleMessage *messagePtr)
{
    LU_UNUSED(messagePtr);

    return IOM_ERROR_NOT_SUPPORTED;
}


IOM_ERROR MCMIOModule::sendMessage( MODULE_MSG_TYPE  messageType,
                                    MODULE_MSG_ID    messageID  ,
                                    PayloadRAW      *messagePtr
                                  )
{
    LU_UNUSED(messageType);
    LU_UNUSED(messageID);
    LU_UNUSED(messagePtr);

    return IOM_ERROR_NOT_SUPPORTED;
}


IOM_ERROR MCMIOModule::sendMessage(sendReplyStr* messagePtr)
{
    LU_UNUSED(messagePtr);

    return IOM_ERROR_NOT_SUPPORTED;
}


IOM_ERROR MCMIOModule::decodeMessage(ModuleMessage* message)
{
    LU_UNUSED(message);

    return IOM_ERROR_NOT_SUPPORTED;
}



void MCMIOModule::getInfo(IOModuleInfoStr& moduleInfo)
{
    /* Fill the moduleInfo structure */
    moduleInfo.id = mID;
    moduleInfo.status = mStatus;
    moduleInfo.moduleInfo = mInfo;
    moduleInfo.hBeatInfo.error = 0;
    moduleInfo.hBeatInfo.moduleUID = mInfo.moduleUID;
    moduleInfo.hBeatInfo.status = MODULE_BOARD_STATUS_ON_LINE;
}

void MCMIOModule::getDetails(IOModuleDetailStr &moduleInfo)
{
    /* TODO: pueyos_a - set correct MCMUpdater architecture coming from the ELF spec (use file command or similar) */
    moduleInfo.architecture = 0;
    moduleInfo.svnRevisionBoot = 0;
    moduleInfo.svnRevisionApp = _SVN_REVISION;
    /* TODO: pueyos_a - Set uptime for MCMUpdater (MCM Module) */
    moduleInfo.uptime = 0;
}

std::string MCMIOModule::getStateName()
{
    return "NoState";
}

ModuleUID MCMIOModule::getModuleUID()
{
    return mInfo.moduleUID;
}

lu_bool_t MCMIOModule::isReadyForUpgrade()
{
    return LU_TRUE;
}

IOM_ERROR MCMIOModule::ping(lu_uint32_t timeout, lu_uint32_t payloadSize)
{
    LU_UNUSED(timeout);
    LU_UNUSED(payloadSize);

    return IOM_ERROR_NOT_SUPPORTED;
}


IOM_ERROR MCMIOModule::tickEvent(lu_uint32_t dTime)
{
    /* Do nothing */
    LU_UNUSED(dTime);
    return IOM_ERROR_NONE;
}


IOM_ERROR MCMIOModule:: restart(MD_RESTART type)
{
    /* Do nothing. MCM is the master module and is restarted in a different way.*/
    LU_UNUSED(type);
    return IOM_ERROR_NOT_SUPPORTED;
}

IOM_ERROR MCMIOModule::getCANStats(std::vector<StatsCANStr>& statsList)
{
    StatsCANStr canStats;
    /* TODO: pueyos_a - get the rest of CAN stats for the MCM Updater */
//    TimeManager::TimeStr timestamp;
//    lu_uint32_t canList[2][5] = {
//                        {
//                            MCM_CH_INTERNAL_AINPUT_CAN0_ERROR_COUNT,
//                            MCM_CH_INTERNAL_AINPUT_CAN0_RXDATAOVERRUN,
//                            MCM_CH_INTERNAL_AINPUT_CAN0_TXCOUNT,
//                            MCM_CH_INTERNAL_AINPUT_CAN0_RXCOUNT,
//                            MCM_CH_INTERNAL_AINPUT_CAN0_RXLOST
//                        },
//                        {
//                            MCM_CH_INTERNAL_AINPUT_CAN1_ERROR_COUNT,
//                            MCM_CH_INTERNAL_AINPUT_CAN1_RXDATAOVERRUN,
//                            MCM_CH_INTERNAL_AINPUT_CAN1_TXCOUNT,
//                            MCM_CH_INTERNAL_AINPUT_CAN1_RXCOUNT,
//                            MCM_CH_INTERNAL_AINPUT_CAN1_RXLOST
//                        }
//                    };

    /* TODO: pueyos_a - get the rest of CAN stats for the MCM Updater */
//    TimeManager::getInstance().getTime(timestamp);
    statsList.clear();

    /* iterate through CAN 0 and CAN1 */
    for (lu_uint32_t i = 0; i < 2; ++i)
    {
        /* TODO: pueyos_a - get the rest of CAN stats for the MCM Updater */
//        std::vector<lu_uint32_t> listCAN(canList[i][0], canList[i][5]);
//        askCANxStats(listCAN, timestamp);

        /* Get some parameters directly from the interface */
        COMM_INTERFACE iface;
        iface = (i==0)? COMM_INTERFACE_CAN0 :
                        COMM_INTERFACE_CAN1;
        CANInterface& canIface = CANInterface::getInstance(iface);
        struct can_device_stats statsCAN;
        if(can_get_device_stats(canIface.getName(), &statsCAN) != 0)
        {
            return IOM_ERROR_NOT_FOUND;
        }
        canStats.canBusError = statsCAN.bus_error;
        canStats.validCANBusError = 1;
        canStats.canArbitrationError = statsCAN.arbitration_lost;
        canStats.validCANArbitrationError = 1;
        canStats.canBusOff = statsCAN.bus_off;
        canStats.validCANBusOff = 1;
        canStats.canErrorPassive = statsCAN.error_passive;
        canStats.validCANErrorPassive = 1;

        /* TODO: pueyos_a - get the rest of CAN stats for the MCM Updater */
//        getCANxStats(listCAN, timestamp, canStats);

        statsList.push_back(canStats);
    }
    return IOM_ERROR_NONE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
template<typename T> void MCMIOModule::StatsCANStr::putValidValue( std::stringstream &ss,
                                                                        const char *name,
                                                                        const lu_uint16_t valid,
                                                                        T value
                                                                        )
{
    ss << " " << name << ":";
    if(valid)
        ss << value;
    else
        ss << "--";
    ss << std::endl;
}

/* TODO: pueyos_a - get the rest of CAN stats for the MCM Updater */
//IOM_ERROR MCMIOModule::askCANxStats(std::vector<lu_uint32_t>& channelIDList,
//                                    TimeManager::TimeStr timestamp
//                                    )
//{
//    for(std::vector<lu_uint32_t>::iterator it = channelIDList.begin(), end = channelIDList.end(); it != end; ++it)
//    {
//        monitorHdl.send_req_ai_msg(*it);
////        if(askMonAIChannel(static_cast<MCM_CH_INTERNAL_AINPUT>(*it), timestamp) != IOM_ERROR_NONE)
//        {
//            return IOM_ERROR_OFFLINE;
//        }
//    }
//    return IOM_ERROR_NONE;
//}
//
//IOM_ERROR MCMIOModule::getCANxStats(std::vector<lu_uint32_t>& channelIDList,
//                                    TimeManager::TimeStr timestamp,
//                                    StatsCANStr& canStats
//                                    )
//{
//    IODataFloat32 value;
//    IChannel::ValueStr valueRead(value);
//    TimeManager::TimeStr timeNow;   //current time for timeout calculation
//    lu_uint32_t updateCount;    //Counts down updates of values
//
//    TimeManager& timeManager = TimeManager::getInstance();
//    updateCount = channelIDList.size();
//    do
//    {
//        for(std::vector<lu_uint32_t>::iterator it = channelIDList.begin(),
//                                               end = channelIDList.end(); it != end; ++it)
//        {
//            if(IntAIChannel[*it]->read(valueRead) == IOM_ERROR_NONE)
//            {
//                if(valueRead.flags.restart == LU_FALSE)
//                {
//                    //valid updated read
//                    --updateCount;
//                    switch(*it)
//                    {
//                        case MCM_CH_INTERNAL_AINPUT_CAN0_ERROR_COUNT  :
//                        case MCM_CH_INTERNAL_AINPUT_CAN1_ERROR_COUNT  :
//                            canStats.canError = *(valueRead.dataPtr);
//                            canStats.validCANError = 1;
//                            break;
//                        case MCM_CH_INTERNAL_AINPUT_CAN0_RXDATAOVERRUN:
//                        case MCM_CH_INTERNAL_AINPUT_CAN1_RXDATAOVERRUN:
//                            canStats.canDataOverrun = *(valueRead.dataPtr);
//                            canStats.validCANDataOverrun = 1;
//                            break;
//                        case MCM_CH_INTERNAL_AINPUT_CAN0_TXCOUNT      :
//                        case MCM_CH_INTERNAL_AINPUT_CAN1_TXCOUNT      :
//                            canStats.canTxCount = *(valueRead.dataPtr);
//                            canStats.validCANTxCount = 1;
//                            break;
//                        case MCM_CH_INTERNAL_AINPUT_CAN0_RXCOUNT      :
//                        case MCM_CH_INTERNAL_AINPUT_CAN1_RXCOUNT      :
//                            canStats.canRxCount = *(valueRead.dataPtr);
//                            canStats.validCANRxCount = 1;
//                            break;
//                        case MCM_CH_INTERNAL_AINPUT_CAN0_RXLOST       :
//                        case MCM_CH_INTERNAL_AINPUT_CAN1_RXLOST       :
//                            canStats.canRxLost = *(valueRead.dataPtr);
//                            canStats.validCANRxLost = 1;
//                            break;
//                        default:
//                            return IOM_ERROR_NOT_FOUND;
//                    }
//                }
//            }
//        }
//        timeManager.getTime(timeNow);
//    } while((updateCount > 0) || (timestamp.elapsed_ms(timeNow) > CANSTATS_MONREPLYTOUT_MS) );
//
//    if(updateCount != 0)
//    {
//        return IOM_ERROR_OFFLINE;
//    }
//    return IOM_ERROR_NONE;
//}


/*
 *********************** End of file ******************************************
 */

