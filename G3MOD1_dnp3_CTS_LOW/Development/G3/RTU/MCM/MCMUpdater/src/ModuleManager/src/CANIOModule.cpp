/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <cstdlib>
#include <cstring>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "LockingMutex.h"
#include "CANIOModule.h"
#include "IMCMUpdater.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/**
 * Heart Beat timeout threshold in ms
 */
static const lu_uint32_t HBeatTimeoutThr = 2000;
static const lu_uint32_t MInfoTimeoutThr = 2000;


const CANIOModule::STATE CANIOModule::transitionTable[STATE_LAST][EVENT_LAST] = /* transitionTable[Event][CurrentState] => NextState */
{
   /*MINFO_RECEIVED, HBEAT_RECEIVED,     MINFO_TIMEOUT, HBEAT_TIMEOUT, MSTATUS_INVALID,MSTATUS_READY*/
   { STATE_LAST,     STATE_WAITING_INFO, STATE_LAST,    STATE_LAST,    STATE_LAST,     STATE_LAST         }, /* CREATED          */
   { STATE_STARTING, STATE_LAST,         STATE_LAST,    STATE_OFFLINE, STATE_LAST,     STATE_LAST         }, /* WAITING_INFO     */
   { STATE_LAST,     STATE_LAST,         STATE_LAST,    STATE_OFFLINE, STATE_INVALID,  STATE_READY_FOR_UPG}, /* STARTING         */
   { STATE_LAST,     STATE_LAST,         STATE_LAST,    STATE_OFFLINE, STATE_LAST,     STATE_LAST         }, /* READY_FOR_UPG    */
   { STATE_CREATED,  STATE_CREATED,      STATE_CREATED, STATE_CREATED, STATE_CREATED,  STATE_CREATED      }, /* INVALID          */
   { STATE_LAST,     STATE_WAITING_INFO, STATE_LAST,    STATE_LAST,    STATE_LAST,     STATE_LAST         }  /* OFFLINE          */
};

const lu_char_t* CANIOModule::EVENT_NAME[CANIOModule::EVENT_LAST] =
 {
     "MINFO_RECEIVED "    ,
     "HBEAT_RECEIVED "    ,
     "MINFO_TIMEOUT  "    ,
     "HBEAT_TIMEOUT  "    ,
     "MSTATUS_INVALID"    ,
     "MSTATUS_READY  "
 };

 const lu_char_t* CANIOModule::STATE_NAME[CANIOModule::STATE_LAST] =
{
     "CREATED      ",
     "WAITING_INFO ",
     "STARTING     ",
     "READY_FOR_UPG",
     "INVALID      ",
     "OFFLINE      "
};

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

CANIOModule::CANIOModule( MODULE                          module       ,
                          MODULE_ID                       id           ,
                          COMM_INTERFACE                  iFace
                          ) : IIOModule(module, id)       ,
                              mutex(LU_TRUE)              ,
                              SRmutex(LU_TRUE)            ,
                              curState(STATE_CREATED)     ,
                              hBeatTimeout(0)             ,
                              mInfoTimeout(0)             ,
                              socket(iFace, LU_FALSE)     ,
                              mInfoSocket(iFace, LU_FALSE),
                              log(Logger::getLogger(SUBSYSTEM_ID_MODULE)),
                              fsmState(FSM_STATE_CREATED)

{
    memset(&hBeatInfo, 0x00, sizeof(hBeatInfo));
    initSockets();
}

void CANIOModule::initSockets()
{
    CanFilterTable filterTable;

    /* Set filter for MInfoR message reception */
    struct can_filter defaultFilter;
    CANHeaderStr *CANHeaderPtr;
    defaultFilter.can_id   = 0;
    defaultFilter.can_mask = 0;
    CANHeaderPtr = (CANHeaderStr*)(&defaultFilter.can_id);
    CANHeaderPtr->messageType = MODULE_MSG_TYPE_LPRIO;
    CANHeaderPtr->messageID = MODULE_MSG_ID_LPRIO_MINFO_R;
    CANHeaderPtr->deviceSrc = mID.type;
    CANHeaderPtr->deviceIDSrc = mID.id;
    CANHeaderPtr->deviceDst = MODULE_MCM;
    CANHeaderPtr->deviceIDDst = MODULE_ID_0;
    CANHeaderPtr->fragment = 1; //accept fragmented messages
    CANHeaderPtr = (CANHeaderStr*)(&defaultFilter.can_mask);
    CANHeaderPtr->messageType = CANHEADERDEF_MASK_MESSAGETYPE; //exactly the specified message type
    CANHeaderPtr->messageID = CANHEADERDEF_MASK_MESSAGEID;     //exactly the specified message ID
    CANHeaderPtr->deviceSrc = CANHEADERDEF_MASK_DEVICE;     // and so on...
    CANHeaderPtr->deviceIDSrc = CANHEADERDEF_MASK_DEVICEID;
    CANHeaderPtr->deviceDst = CANHEADERDEF_MASK_DEVICE;
    CANHeaderPtr->deviceIDDst = CANHEADERDEF_MASK_DEVICEID;
    CANHeaderPtr->fragment = 1; //accept fragmented messages
    filterTable.setTable(&defaultFilter, 1);
    if(mInfoSocket.setFilter(filterTable) != CAN_ERROR_NONE)
    {
        log.error("Error setting ModuleInfo filter");
    }

    /* Clear Send/Receive filter */
    filterTable.setTable(NULL, 0);
    socket.setFilter(filterTable);
}

IOM_ERROR CANIOModule::tickEvent(lu_uint32_t dTime)
{
    hBeatTimeout += dTime;
    mInfoTimeout += dTime;

    state_process();

    return IOM_ERROR_NONE;
}

IOM_ERROR CANIOModule::decodeMessage(ModuleMessage* message)
{
    IOM_ERROR ret = IOM_ERROR_NONE;

    if(message == NULL)
    {
        return IOM_ERROR_PARAM;
    }

    switch(message->header.messageType)
    {
        case MODULE_MSG_TYPE_LPRIO:
            switch(message->header.messageID)
            {
                case MODULE_MSG_ID_LPRIO_HBEAT_S:
                    handleHBeat(message);
                break;
                case MODULE_MSG_ID_LPRIO_MINFO_R:
                    handleMInfoR(message);
                break;
                default:
                    ret = IOM_ERROR_NOT_SUPPORTED;
                break;
            }
        break;

        default:
            ret = IOM_ERROR_NOT_SUPPORTED;
        break;
    }
    return ret;
}

IOM_ERROR CANIOModule::sendMessage(ModuleMessage* messagePtr)
{
    IOM_ERROR ret = IOM_ERROR_NONE;

    if(messagePtr == NULL)
    {
        return IOM_ERROR_NOT_SUPPORTED;
    }

    CAN_ERROR canRet = socket.write(messagePtr);
    switch(canRet)
    {
        case CAN_ERROR_NONE:
            //Successful write
            break;
        case CAN_ERROR_INIT:
            //Socket/Interface not initialised: ignore
            ret = IOM_ERROR_SEND;
            break;
        case CAN_ERROR_DOWN:
            //Interface is down: ignore
            ret = IOM_ERROR_SEND;
            break;
        default:
            ret = IOM_ERROR_SEND;
            break;
    }
    return ret;
}

IOM_ERROR CANIOModule::sendMessage( MODULE_MSG_TYPE  messageType,
                                    MODULE_MSG_ID    messageID,
                                    PayloadRAW      *messagePtr
                                  )
{
    if(messagePtr == NULL)
    {
        return IOM_ERROR_NOT_SUPPORTED;
    }

    /* allocate a message */
    RAWModuleMessage message(messagePtr);

    /* Fill the header */
    message.header.messageType   = messageType;
    message.header.messageID     = messageID  ;
    message.header.source        = MODULE_MCM ;
    message.header.sourceID      = MODULE_ID_0;
    message.header.destination   = mID.type;
    message.header.destinationID = mID.id;

    /* Send the message */
    return sendMessage(&message);
}

IOM_ERROR CANIOModule::sendMessage(sendReplyStr *messagePtr)
{
    IOM_ERROR ret = IOM_ERROR_NONE;
    CAN_ERROR canRet;
    CanFilterTable filterTable;
    struct can_filter filter;
    CANHeaderStr *CANHeaderPtr;

    if( (messagePtr                 == NULL) ||
        (messagePtr->messagePtr     == NULL)
      )
    {
        return IOM_ERROR_PARAM;
    }

    /* Only one send/reply message at a time can be sent */
    LockingMutex lMutex(SRmutex);

    /* Initialise socketCAN filter*/
    filter.can_id   = 0;
    filter.can_mask = 0;
    CANHeaderPtr = (CANHeaderStr*)(&filter.can_id);
    CANHeaderPtr->messageType = messagePtr->rMessageType;
    CANHeaderPtr->messageID = messagePtr->rMessageID;
    CANHeaderPtr = (CANHeaderStr*)(&filter.can_mask);
    CANHeaderPtr->messageType = CANHEADERDEF_MASK_MESSAGETYPE;	//we expect exactly the reply
    CANHeaderPtr->messageID   = CANHEADERDEF_MASK_MESSAGEID;
    filterTable.setTable(&filter, 1);
    socket.setFilter(filterTable);

    /* Send message */
    ret = sendMessage( messagePtr->messageType,
                       messagePtr->messageID  ,
                       messagePtr->messagePtr
                     );

    /* If the message has been sent correctly wait for a reply */
    if(ret == IOM_ERROR_NONE)
    {
        canRet = socket.read(&messagePtr->rMessagePtr, &messagePtr->timeout);
        switch(canRet)
        {
            case CAN_ERROR_NONE:
                //Successful read
                break;
            case CAN_ERROR_INIT:
                //Socket/Interface not initialised
                return IOM_ERROR_NOT_ENABLED;
                break;
            case CAN_ERROR_DOWN:
                //Interface is down
                return IOM_ERROR_NOT_ENABLED;
                break;
            case CAN_ERROR_TIMEOUT:
                ret = IOM_ERROR_TIMEOUT;
                break;
            default:
                ret = IOM_ERROR_READ;
                break;
        }
    }

    /* Clear receive filter */
    filterTable.setTable(NULL, 0);
    socket.setFilter(filterTable);

    return ret;
}

void CANIOModule::getInfo(IOModuleInfoStr& moduleFullInfo)
{
    LockingMutex lMutex(mutex);
    /* Fill the moduleFullInfo structure */
    moduleFullInfo.id = mID;
    moduleFullInfo.status = mStatus;
    moduleFullInfo.hBeatInfo = hBeatInfo;
    moduleFullInfo.moduleInfo = mInfo;
    moduleFullInfo.fsm = fsmState;
}

void CANIOModule::getDetails(IOModuleDetailStr &moduleDetails)
{
    moduleDetails.architecture = mInfo.systemArch;
    moduleDetails.svnRevisionBoot = mInfo.svnRevisionBoot;
    moduleDetails.svnRevisionApp = mInfo.svnRevisionApp;
}

ModuleUID CANIOModule::getModuleUID()
{
    return hBeatInfo.moduleUID;
}

lu_bool_t CANIOModule::isReadyForUpgrade()
{
    return curState == STATE_READY_FOR_UPG ? LU_TRUE : LU_FALSE;
}

std::string CANIOModule::getStateName()
{
    switch(curState)
    {
        case STATE_CREATED         : return "Initial state";
        case STATE_WAITING_INFO    : return "Waiting module info response";
        case STATE_STARTING        : return "Waiting module ready";
        case STATE_READY_FOR_UPG   : return "Ready for upgrading";
        case STATE_INVALID         : return "Invalid state for upgrading,need reset";
        case STATE_OFFLINE         : return "No heart-beat response";
        default                    : return "Illegal state";
    }

}

IOM_ERROR CANIOModule::ping(lu_uint32_t timeout, lu_uint32_t payloadSize)
{
    IOM_ERROR ret;
    sendReplyStr message;
    lu_uint8_t buffer[MODULE_MESSAGE_LENGTH];
    PayloadRAW payload;

    /* Reduce the payload size to the maximusendMessagem size allowed
     * by the protocol
     */
    if(payloadSize > MODULE_MESSAGE_LENGTH)
    {
        payloadSize = MODULE_MESSAGE_LENGTH;
    }

    /* Check the timeout */
    if(timeout > 1000)
    {
        timeout = 1000;
    }

    /* Fill the buffer with random data */
    for(lu_uint32_t i = 0; i < payloadSize; ++i)
    {
        buffer[i] = (lu_uint8_t)rand();
    }

    /* Prepare message */
    payload.payloadLen = payloadSize;
    payload.payloadPtr = buffer;
    message.messageType     = MODULE_MSG_TYPE_LPRIO;
    message.messageID       = MODULE_MSG_ID_LPRIO_PING_C;
    message.messagePtr      = &payload;
    message.rMessageType    = MODULE_MSG_TYPE_LPRIO;
    message.rMessageID      = MODULE_MSG_ID_LPRIO_PING_R;
    message.timeout.tv_sec  = 0;
    message.timeout.tv_usec = timeout*1000;

    /* Send message */
    ret = sendMessage(&message);

    if(ret == IOM_ERROR_NONE)
    {
        /* Get payload */
        (message.rMessagePtr)->getPayload(&payload);

        if( (payload.payloadLen != payloadSize)                   ||
            (memcmp(payload.payloadPtr, buffer, payloadSize) != 0)
           )
        {
            /* Wrong payload */
            ret = IOM_ERROR_PAYLOAD;
        }

        /* Release reply message */
        delete (message.rMessagePtr);
    }

    return ret;
}


void CANIOModule::requestModuleInfo()
{
    log.info("%s sending ModuleInfo request...", getModuleName());
    CAN_ERROR ret;
    RAWSmallModuleMessage message(0);

    message.header.messageType   = MODULE_MSG_TYPE_LPRIO;
    message.header.messageID     = MODULE_MSG_ID_LPRIO_MINFO_C;
    message.header.source        = MODULE_MCM;
    message.header.sourceID      = MODULE_ID_0;
    message.header.destination   = mID.type;
    message.header.destinationID = mID.id;

    ret = mInfoSocket.write(&message);
    switch(ret)
    {
        case CAN_ERROR_NONE:
            //Successful write
            break;
        case CAN_ERROR_INIT:
            //Socket/Interface not initialised: ignore
            return;
            break;
        case CAN_ERROR_DOWN:
            //Interface is down: ignore
            return;
            break;
        default:
            log.error("%s send message error: %i - Line: %i",getModuleName(), ret, __LINE__);
            break;
    }

    //Note: ModuleInfo reply might appear at ModuleManager::handleEvent() later.
}


IOM_ERROR CANIOModule::getCANStats(CanStatsStr &stats)
{
    IOM_ERROR result = IOM_ERROR_SEND;
    IOM_ERROR sendMsg;

    /* Prepare CAN stats request message */
    IIOModule::sendReplyStr message;
    PayloadRAW payload_C,payload_R;

    payload_C.payloadPtr = NULL;
    payload_C.payloadLen = 0;

    lu_uint32_t timeout = 2000; //ms of timeout
    message.messageType     = MODULE_MSG_TYPE_LPRIO;
    message.messageID       = MODULE_MSG_ID_LPRIO_CAN_STATS_C;
    message.messagePtr      = &payload_C;
    message.rMessageType    = MODULE_MSG_TYPE_LPRIO;
    message.rMessageID      = MODULE_MSG_ID_LPRIO_CAN_STATS_R;
    message.timeout.tv_sec  = 0;
    message.timeout.tv_usec = timeout*1000;

    sendMsg = sendMessage(&message);
    if(sendMsg != IOM_ERROR_NONE)
    {
        log.error("%s Error sending message to %s - Error code %i: %s",
                        __AT__, this->getModuleName(),
                        sendMsg, IOM_ERROR_ToSTRING(sendMsg)
                      );
        return IOM_ERROR_SEND;
    }

    // Read reply message
    (message.rMessagePtr)->getPayload(&payload_R);

    // Verify reply message payload
    if(payload_R.payloadLen != MODULE_MESSAGE_SIZE(CanStatsStr))
    {
         /* Wrong payload */
        log.error("%s Error requesting CAN stats from %s - "
                        "Invalid payload size: %i",
                        __AT__, getModuleName(), payload_R.payloadLen
                      );
        result = IOM_ERROR_PAYLOAD;
    }
    else
    {
        // Fill out output with reply message content
        CanStatsStr *replyPtr = reinterpret_cast<CanStatsStr*>(payload_R.payloadPtr);
        stats = *replyPtr;
        result = IOM_ERROR_NONE;
    }
    //Free message payload
    delete (message.rMessagePtr);

    return result;
}


IOM_ERROR CANIOModule::restart(MD_RESTART type)
{

    if(type >= MD_RESTART_LAST)
    {
        return IOM_ERROR_PARAM;
    }

    if(hBeatInfo.status == MODULE_BOARD_STATUS_APP_BOOTL_PROG)
    {
        // When programming the BootLoader, never send any restart command
        return IOM_ERROR_NOT_ENABLED;
    }

    IOM_ERROR ret;
    MDCmdRestartStr message;
    PayloadRAW payload;

    /* Prepare message */
    message.type = type;
    payload.payloadLen = sizeof(message);
    payload.payloadPtr = reinterpret_cast<lu_uint8_t*>(&message);

    /* Send message */
    ret = sendMessage( MODULE_MSG_TYPE_MD_CMD,
                       MODULE_MSG_ID_MD_CMD_RESTART,
                       &payload
                     );

    if(ret != IOM_ERROR_NONE)
    {
        log.error("%s fail to send restart command (Type:%i) to all modules, error %i: %s",
                    getModuleName(), type, ret, IOM_ERROR_ToSTRING(ret)
                    );
    }
    else
    {
        log.info("%s sent restart command (Type:%i)",getModuleName(),type);

        /* Transit state*/
        LockingMutex lMutex(mutex);
        state_leave(curState);
        state_enter(STATE_OFFLINE);
    }


    return ret;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void CANIOModule::state_leave(STATE oldState)
{
    log.debug("%s leave state: %s ",getModuleName(),STATE_NAME[curState] );

    switch(oldState)
    {
        case STATE_READY_FOR_UPG:
            mStatus.ok = LU_FALSE;
            break;
        default:
            break;
    }
}

void CANIOModule::state_enter(STATE newState)
{
    /* Invalid new state*/
    if(newState >= STATE_LAST || newState < STATE_CREATED)
       return;

    log.debug("%s enter state: %s ", getModuleName(),STATE_NAME[newState] );

    curState = newState;

    switch (newState)
    {
        case STATE_CREATED:
            memset(&mStatus,    0x00, sizeof(mStatus));
            memset(&mInfo,      0x00, sizeof(mInfo));
            memset(&hBeatInfo,  0x00, sizeof(hBeatInfo));
            fsmState = FSM_STATE_CREATED;
        break;

        case STATE_WAITING_INFO:
            requestModuleInfo();
            mInfoTimeout = 0;
            fsmState = FSM_STATE_WAITING_INFO;
        break;

        case STATE_STARTING:
            fsmState = FSM_STATE_INITIALISING;
        break;

        case STATE_READY_FOR_UPG:
            mStatus.ok = LU_TRUE;
            fsmState = FSM_STATE_ACTIVE;
        break;

        case STATE_INVALID:
            fsmState = FSM_STATE_ILLEGAL;
            restart(MD_RESTART_WARM);
        break;

        case STATE_OFFLINE:
            fsmState = FSM_STATE_OFFLINE;
        break;

        default:/*Do nothing*/
            fsmState = FSM_STATE_DISABLED;
        break;
    }
}

void CANIOModule::state_process()
{

    switch (curState)
    {
        case STATE_STARTING:
            /*Heart beat timeout*/
            if(hBeatTimeout > HBeatTimeoutThr)
            {
                fire_event(EVENT_HBEAT_TIMEOUT);
            }
            else if(hBeatInfo.status == MODULE_BOARD_STATUS_BOOT_READY)
            {
                fire_event(EVENT_MSTATUS_READY);
            }
            else if (hBeatInfo.status != MODULE_BOARD_STATUS_BOOT_POST
                && hBeatInfo.status!= MODULE_BOARD_STATUS_BOOT_READY
                && hBeatInfo.status != MODULE_BOARD_STATUS_INVALID)
            {
                fire_event(EVENT_MSTATUS_INVALID);
            }
        break;

        case STATE_READY_FOR_UPG:
            /*Heart beat timeout*/
            if(hBeatTimeout > HBeatTimeoutThr)
            {
                fire_event(EVENT_HBEAT_TIMEOUT);
            }
            else if (hBeatInfo.status != MODULE_BOARD_STATUS_BOOT_READY)
            {
                fire_event(EVENT_MSTATUS_INVALID);
            }
        break;

        case STATE_WAITING_INFO:
            /*Heart beat timeout*/
            if(hBeatTimeout > HBeatTimeoutThr)
            {
                fire_event(EVENT_HBEAT_TIMEOUT);
            }
            /*Module info reply timeout, re-send request.*/
            else if(mInfoTimeout > MInfoTimeoutThr)
            {
                log.error("%s Module Info reply timeout!!!",getModuleName());
                requestModuleInfo();
                mInfoTimeout = 0;
            }

        break;

        case STATE_CREATED:
        case STATE_OFFLINE:
        case STATE_INVALID:
        default:/* Do nothing */
        break;
    }
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void CANIOModule::fire_event(EVENT event)
{
	if(IMCMUpdater::launchParams.factory == LU_TRUE){
		// Do not fire event in hat mode cause the state machine is not supposed
		// to be run and control slave modules.
		return;
	}

    if(event >= EVENT_LAST || event < EVENT_MINFO_RECEIVED)
        return;

    log.debug("%s fire event: %s ",getModuleName(),EVENT_NAME[event] );

    LockingMutex lMutex(mutex);

    /*Handle event*/
    switch(event)
    {
        case EVENT_HBEAT_RECEIVED:
             hBeatTimeout = 0;
             mStatus.detected    = LU_TRUE;  //module reported something at least once
             mStatus.present     = LU_TRUE;  //module is communicating
        break;

        case EVENT_HBEAT_TIMEOUT:
             mStatus.present     = LU_FALSE;  //module is communicating
        break;

        case EVENT_MINFO_TIMEOUT    :
        case EVENT_MSTATUS_INVALID  :
        case EVENT_MSTATUS_READY    :
        default:
            /*Do nothing*/
            break;
    }

    /* Transit states*/
    STATE nextState = transitionTable[curState][event];
    if(curState != nextState && nextState != STATE_LAST)
    {
        state_leave(curState);
        state_enter(nextState);
    }
}

void CANIOModule::handleHBeat(ModuleMessage *message)
{
    PayloadRAW payload;

    /* Get payload */
    MODULE_ERROR err = message->getPayload(&payload);
    if( err!= MODULE_ERROR_NONE)
    {
        log.error("[%s] Fail to get replied heartbeat payload. MODULE_ERROR:%d",
                        getModuleName(),
                        err
                  );
        return;
    }

    /* Validate message */
    lu_uint32_t PL_SIZE = sizeof(ModuleHBeatSStr);
    if(payload.payloadLen != PL_SIZE)
    {
        log.error("[%s] Heart Beat message payload size error.Expected:%i Actual: %i.",
                        getModuleName(),
                        PL_SIZE,
                        payload.payloadLen
                  );
        return;
    }

    /* Decode message */
    memcpy(&hBeatInfo, payload.payloadPtr, PL_SIZE);

    /* Update Heart Beat timeout */
    fire_event(EVENT_HBEAT_RECEIVED);
}


void CANIOModule::handleMInfoR(ModuleMessage *message)
{
    PayloadRAW payload;

    /* Get payload */
    MODULE_ERROR err = message->getPayload(&payload);
    if( err != MODULE_ERROR_NONE)
    {
        log.error("[%s] Fail to get replied moduleInfo payload. MODULE_ERROR:%d",
                           getModuleName(),
                           err
                     );
        return;
    }

    /* Validate message */
    lu_uint32_t PL_SIZE = sizeof(ModuleInfoStr);
    if(payload.payloadLen !=  PL_SIZE)
    {
        log.error("[%s] Module Info payload size error. Expected %i Actual:%i",
                    getModuleName(),
                    PL_SIZE,
                    payload.payloadLen
                );
        return;
    }

    /* Store module info */
    memcpy(&mInfo, payload.payloadPtr, PL_SIZE);
    ModuleUID moduleUID(mInfo.moduleUID);
    log.info("Module Info received from %s, UID: %s.", getModuleName(), moduleUID.toString().c_str());

    fire_event(EVENT_MINFO_RECEIVED);
}

/*
 *********************** End of file ******************************************
 */

