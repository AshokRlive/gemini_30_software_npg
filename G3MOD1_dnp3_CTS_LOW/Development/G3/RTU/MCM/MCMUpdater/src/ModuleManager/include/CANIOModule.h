/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN Module class
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   01/09/13      wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_AB764129_BD3C_4b6e_8B4B_ACEF0CC9F57C__INCLUDED_)
#define EA_AB764129_BD3C_4b6e_8B4B_ACEF0CC9F57C__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Mutex.h"
#include "IIOModule.h"
#include "CANFraming.h"
#include "Logger.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * This class implements IOModule interface and manages the state of a CAN module.
 * It also provides a default implementation of the decodeMessage method that
 * understands the common messages:
 * - Heart Beat
 * - Module Version
 */
class CANIOModule : public IIOModule
{

public:
    static const lu_uint32_t CAN_MSG_TIMEOUT_MSEC = 50;

    typedef enum
    {
        STATE_CREATED          = 0,//Initial state
        STATE_WAITING_INFO        ,//Waiting module info response
        STATE_STARTING            ,//Waiting module ready
        STATE_READY_FOR_UPG       ,//Ready for upgrading
        STATE_INVALID             ,//Invalid state for upgrading,need reset
        STATE_OFFLINE             ,//No heart-beat response
        STATE_LAST           //Never entered state
    }STATE;


    typedef enum
    {
        EVENT_MINFO_RECEIVED  = 0, //Module Info received
        EVENT_HBEAT_RECEIVED     , //Heart beat received
        EVENT_MINFO_TIMEOUT      , //Module Info receiving timeout
        EVENT_HBEAT_TIMEOUT      , //Heart beat timeout
        EVENT_MSTATUS_INVALID    , //Module status is invalid
        EVENT_MSTATUS_READY      ,  //Module status is BOOT_READY
        EVENT_LAST
    }EVENT;

    static const lu_char_t* STATE_NAME[STATE_LAST];

    static const lu_char_t* EVENT_NAME[EVENT_LAST];

    static const STATE transitionTable[STATE_LAST][EVENT_LAST];

    CANIOModule( MODULE                          module       ,
                 MODULE_ID                       id           ,
                 COMM_INTERFACE                  iFace
               );
    virtual ~CANIOModule() {};

    /*********************/
    /* IModule interface */
    /*********************/

    /**Inherit */
    virtual IOM_ERROR decodeMessage(ModuleMessage* message);

    /**Inherit */
    virtual IOM_ERROR sendMessage(ModuleMessage* messagePtr);

    /**Inherit */
    virtual IOM_ERROR sendMessage( MODULE_MSG_TYPE  messageType,
                                    MODULE_MSG_ID    messageID,
                                    PayloadRAW      *messagePtr
                                  );

    /**Inherit */
    virtual IOM_ERROR sendMessage(sendReplyStr *messagePtr);

    /**Inherit */
    virtual IOM_ERROR tickEvent(lu_uint32_t dTime);

    /**Inherit */
    virtual void getInfo(IOModuleInfoStr& version);
    virtual void getDetails(IOModuleDetailStr& moduleInfo);

    /**Inherit */
    virtual IOM_ERROR ping(lu_uint32_t timeout, lu_uint32_t payloadSize = 8);

    /**Inherit */
    virtual ModuleUID getModuleUID();

    /**Inherit */
    virtual lu_bool_t isReadyForUpgrade();

    /**Inherit */
    virtual IOM_ERROR restart(MD_RESTART type);

    /**Inherit */
    virtual std::string getStateName();


    void requestModuleInfo();
    IOM_ERROR getCANStats(CanStatsStr &stats);

protected:

    /*
     * \brief Do processing on what is means to leave the current state
     * might be dependent on the new state.
     */
    virtual void state_leave(STATE newState);

    /*
     * \brief Do processing on what is means to leave the current state
     * might be dependent on the new state.
     */
    virtual void state_enter(STATE newState);

    /*
     * Handle current state.
     */
    virtual void state_process();


private:
    void initSockets();
    void fire_event(EVENT event);

    void handleMInfoR(ModuleMessage *message);

    void handleHBeat(ModuleMessage *message);

    Mutex mutex;    //Protects HeartBeat handling
    Mutex SRmutex;  //Protects Send/Reply message handling
    STATE curState; //Current state of this state machine
    lu_uint32_t   hBeatTimeout; //Heart beat timeout (in ms)
    lu_uint32_t   mInfoTimeout; //Module info timeout (in ms)

    CANFraming socket;      //The socket for sending message
    CANFraming mInfoSocket; //The socket for receiving module info

    ModuleHBeatSStr hBeatInfo;//Received heart-beat information

    Logger& log;

    FSM_STATE fsmState;

};

#endif // !defined(EA_AB764129_BD3C_4b6e_8B4B_ACEF0CC9F57C__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
