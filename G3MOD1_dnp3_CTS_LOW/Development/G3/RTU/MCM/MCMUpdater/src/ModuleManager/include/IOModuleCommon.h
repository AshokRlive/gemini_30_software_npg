/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       IO Module common definitions
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   18/11/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_4FDDA2AC_5C95_4196_964B_10661D5781EC__INCLUDED_)
#define EA_4FDDA2AC_5C95_4196_964B_10661D5781EC__INCLUDED_


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <sstream>
#include <iomanip>
#include <string.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "ModuleProtocol.h"
#include "MainAppEnum.h"
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
std::string genModuleName(MODULE type , MODULE_ID id);
std::string version2String(ModuleVersionStr version);
std::string version2String(ModuleSoftwareVersionStr version);

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/**
 * C++ Expansion of the original Module Unique ID structure
 */
struct ModuleUID : public ModuleUIDStr
{
public:
    ModuleUID(const ModuleUIDStr& moduleUID)
    {
        *this = moduleUID;
    }
    ModuleUID()
    {
        serialNumber = DEFAULT_MODULE_UID.serialNumber;
        supplierId = DEFAULT_MODULE_UID.supplierId;
    }
    bool operator==(const ModuleUIDStr& moduleUID) const
    {
        return ((moduleUID.serialNumber == serialNumber) &&
                (moduleUID.supplierId == supplierId) );
    }
    bool operator!=(const ModuleUIDStr& moduleUID) const
    {
        return !(*this == moduleUID);
    }
    ModuleUID& operator=(const ModuleUIDStr& moduleUID)
    {
        serialNumber = moduleUID.serialNumber;
        supplierId = moduleUID.supplierId;
        return *this;
    }
    const std::string toString()
    {
        std::stringstream ss;
        ss << std::setw(2) << std::setfill('0') << (lu_uint32_t)supplierId << "-"
           << std::setw(6) << std::setfill('0') << (lu_uint32_t)serialNumber;
        return ss.str();
    }
};

struct IOModuleStatus
{
    lu_bool_t ok;           //module is running OK
    lu_bool_t registered;   //The module is present in the registration DB
    lu_bool_t configured;   //The module has been configured (configuration sent)
    lu_bool_t present;      //The module is connected and present on the bus
    lu_bool_t detected;     //The module has ever been detected in the bus
    lu_bool_t disabled;     //The module has been disabled
    lu_bool_t hasConfig;    //The module is present in the configuration file
    ModuleUID moduleUID;    //Module's unique ID (serial)
};

struct IOModuleIDStr
{

public:
    IOModuleIDStr()
    {
        type = MODULE_LAST;
        id = MODULE_ID_LAST;
    }

    IOModuleIDStr(MODULE _type, MODULE_ID _id)
    {
        type = _type;
        id = _id;
    }

    MODULE type;
    MODULE_ID id;

    std::string toString();
};

struct IOModuleInfoStr
{

public:
    IOModuleIDStr id;
    IOModuleStatus status;
    FSM_STATE fsm; // The state of Finite State Machine
    ModuleHBeatSStr hBeatInfo;
    ModuleInfoStr   moduleInfo;

    std::string getVersionString();
    std::string getStatusString();
};

struct IOModuleDetailStr
{
public:
    IOModuleIDStr mid;          //Module ID
    lu_uint32_t architecture;   //System Architecture of the Module
    lu_uint32_t svnRevisionBoot;//BootLoader SVN revision
    lu_uint32_t svnRevisionApp; //Application SVN revision
    lu_uint32_t uptime;         //Module uptime (in seconds)
};

/* Use this structure for references to moduleType:moduleID:ChannelID references */
struct ChannelRef
{

public:
    MODULE moduleType;
    MODULE_ID moduleID;
    lu_uint32_t channelID;
};

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */


#endif // !defined(EA_4FDDA2AC_5C95_4196_964B_10661D5781EC__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
