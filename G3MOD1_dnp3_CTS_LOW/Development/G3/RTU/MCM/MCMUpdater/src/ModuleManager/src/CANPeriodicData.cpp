/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN period data manager implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   01/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "CANPeriodicData.h"
#include "IMCMUpdater.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define MS_2_TICK(ms) ((ms * 1000) / CyclicTimerUsec)


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/* Module's tick event period */
static const lu_uint32_t CyclicTimerSec  = 0     ;
static const lu_uint32_t CyclicTimerUsec = 100000;

/* HeartBeat message period */
static const lu_uint32_t HBeatTickTimeout = MS_2_TICK(500);

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

CANPeriodicData::CANPeriodicData( ModuleManager &modules)
                                    : Thread(SCHED_TYPE_FIFO, THREAD_PRIORITY, LU_FALSE,"CANPeriodicTh"),
                                    modules(modules),
                                    cyclicTimer(CyclicTimerSec, CyclicTimerUsec, Timer::TIMER_TYPE_PERIODIC),
                                    hBeatMsg(sizeof(ModuleHBeatMStr)),
                                    timeCounter(0),
                                    mutexHBeat(LU_TRUE),
                                    enabledHB(true),
                                    log(Logger::getLogger(SUBSYSTEM_ID_MODULEMGR))
{
    ModuleHBeatMStr *hBeatPayloadPtr = reinterpret_cast<ModuleHBeatMStr *>(&(hBeatMsg.payload));
    hBeatPayloadPtr->bootloaderMode = 1;
    hBeatPayloadPtr->powerSaveMode  = 0;
    hBeatPayloadPtr->timeCounter  = 0;

    /* Prepare Heart Beat broadcast message */
    hBeatMsg.header.messageType   = MODULE_MSG_TYPE_LPRIO;
    hBeatMsg.header.messageID     = MODULE_MSG_ID_LPRIO_HBEAT_M;
    hBeatMsg.header.destination   = MODULE_BRD;
    hBeatMsg.header.destinationID = MODULE_ID_BRD;
    hBeatMsg.header.source        = MODULE_MCM;
    hBeatMsg.header.sourceID      = MODULE_ID_0;

    /* Initialize Heart Beat counter */
    hBeatCounter = 0;
}

CANPeriodicData::~CANPeriodicData()
{
    stop(); //stop thread
    join();
    log.debug("CANPeriodicData destroyed");
}


void CANPeriodicData::setHBeatEnable(lu_uint8_t x)
{
    enabledHB = (x!=0);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void CANPeriodicData::threadBody()
{
    lu_bool_t isReset = LU_FALSE;

    /* start timer */
    if(cyclicTimer.start() < 0)
    {
        log.fatal("System timer unavailable");
        return;
    }

    while(isInterrupting() == LU_FALSE)
    {
        /* Wait for timer timeout */
        cyclicTimer.wait();

        /* Send Heart Beat broadcast*/
        if(++hBeatCounter >= HBeatTickTimeout)
        {
            hBeatCounter = 0;
            if(enabledHB)
            {
                generateHBeat();
            }
        }

        /*Reset the board after the first heart beat generated*/
        if(isReset == LU_FALSE)
        {
            modules.resetAllSlaveModules();
            isReset = LU_TRUE;
        }

        modules.tickEvent(CyclicTimerUsec/1000);
    }

    log.debug("CANPeriodicData thread body Exited");
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


void CANPeriodicData::generateHBeat()
{
    RAWSmallModuleMessage hBeatMsgCopy; //store a copy to avoid on-the-fly payload modifications

    mutexHBeat.lock();
    hBeatMsgCopy = hBeatMsg;
    mutexHBeat.unlock();

    ModuleHBeatMStr *hBeatPayload = reinterpret_cast<ModuleHBeatMStr *>(hBeatMsgCopy.payload);
    /* Get internal time Counter and increase it */
    hBeatPayload->timeCounter = timeCounter;

    log.debug("Generate Heart Beat with bits: PowerSave=%i, Bootloader=%i, TimeCounter=%i.",
                hBeatPayload->powerSaveMode,
                hBeatPayload->bootloaderMode,
                hBeatPayload->timeCounter
              );

    /* Send message socketA*/
    modules.canPSocket.write(&hBeatMsgCopy);

    /* Send message socketB */
    modules.canSSocket.write(&hBeatMsgCopy);

    timeCounter = (timeCounter + 1) % HBEAT_MAXTIMECOUNTER;
    return;
}

/*
 *********************** End of file ******************************************
 */
