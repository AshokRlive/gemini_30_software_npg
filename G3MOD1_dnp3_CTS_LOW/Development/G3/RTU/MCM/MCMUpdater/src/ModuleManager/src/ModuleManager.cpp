/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 * 				$Id: c_template.c 1469 2012-06-20 20:51:28Z fryers_j $
 *              $HeadURL: http://10.11.0.6:81/svn/gemini_30_software/trunk/Development/G3/common/template/c_template.c $
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <errno.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "LockingMutex.h"
#include "ModuleManager.h"
#include "CANIOModule.h"
#include "MCMIOModule.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */


ModuleManager::ModuleManager():
    Thread(SCHED_TYPE_FIFO, THREAD_PRIORITY, LU_FALSE,"ModuleManagerTh"),
    canPSocket(PrimaryCanInterface, LU_FALSE),
    canSSocket(SecondaryCanInterface, LU_FALSE),
    log(Logger::getLogger(SUBSYSTEM_ID_MODULEMGR))
{
    /*Init module arrays to be null*/
    memset(modules, 0, sizeof(modules));

    /* Create MCM by default*/
    modules[MODULE_MCM][MODULE_ID_0] = new MCMIOModule(MCM_MODULE_ID);

    initCANFilter();

    periodicThread = new CANPeriodicData(*this);
}

ModuleManager::~ModuleManager()
{
    stop();
    join();

    delete periodicThread;

    log.debug("ModuleManager destroyed");
}

IIOModule* ModuleManager::getModule(MODULE module, MODULE_ID moduleID)
{
    if( (module == MODULE_BRD) ||(module >= MODULE_LAST) ||
          (moduleID  >= MODULE_ID_BRD)
        )
      {
          log.error("Unsupported CAN Module: %i:%i", module, moduleID);
          return NULL;
      }

    if(module >= 0 && module < MODULE_LAST && moduleID >=0 && moduleID < MODULE_ID_BRD)
        return  modules[module][moduleID];
    else
        return NULL;
}


std::vector<IIOModule*> ModuleManager::getAllModules()
{
    std::vector<IIOModule*> ret;

    IIOModule* modulePtr;

    LockingMutex lockingMutex(mutex);

    /* Scan the local database */
    for(lu_uint32_t moduleIdx = 0; moduleIdx < MODULE_LAST; ++moduleIdx)
    {
        for(lu_uint32_t moduleID = 0; moduleID < MODULE_ID_BRD; ++moduleID)
        {
            /* If the module is valid save a reference in the output buffer */
            modulePtr = modules[moduleIdx][moduleID];
            if(modulePtr != NULL)
            {
                ret.push_back(modulePtr);
            }
        }
    }
    return ret;
}


void ModuleManager::resetAllSlaveModules()
{
    CAN_ERROR canRet;
    RAWSmallModuleMessage restartMsg(sizeof(MDCmdRestartStr)); //store a copy to avoid on-the-fly payload modifications

    /* Prepare Heart Beat broadcast message */
    restartMsg.header.messageType   = MODULE_MSG_TYPE_MD_CMD;
    restartMsg.header.messageID     = MODULE_MSG_ID_MD_CMD_RESTART;
    restartMsg.header.destination   = MODULE_BRD;
    restartMsg.header.destinationID = MODULE_ID_BRD;
    restartMsg.header.source        = MODULE_MCM;
    restartMsg.header.sourceID      = MODULE_ID_0;

    MDCmdRestartStr *RestPayload = reinterpret_cast<MDCmdRestartStr *>(restartMsg.payload);
    RestPayload->type = MD_RESTART_WARM;
    restartMsg.payloadLen = sizeof(MDCmdRestartStr);

    log.info("Reseting all slave boards...");

    /* Send message socketA */
    canRet =canPSocket.write(&restartMsg);
    if(canRet != CAN_ERROR_NONE)
    {
        log.error("HeartBeat: %s Generate Restart error: %i",canPSocket.getName(),canRet);
    }

    /* Send message socketB */
    canRet = canSSocket.write(&restartMsg);
    if(canRet != CAN_ERROR_NONE)
    {
       log.error("HeartBeat: %s Generate Restart error: %i",canSSocket.getName(),canRet);
    }
}

void ModuleManager::setHBeatEnable(lu_uint8_t x)
{
    periodicThread->setHBeatEnable(x);
}

lu_bool_t ModuleManager::isHBeatEnabled()
{
    return periodicThread->isHBeatEnabled();
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
void ModuleManager::threadBody()
{
    fd_set rfds;
    lu_int32_t maxFD;
    lu_int32_t retval;

    /* Enable can interfaces for starting communication*/
    CANInterface::getInstance(COMM_INTERFACE_CAN0).setEnabled(LU_TRUE);
    CANInterface::getInstance(COMM_INTERFACE_CAN1).setEnabled(LU_TRUE);

    /* Start periodic thread to send heart beat to slaves*/
    THREAD_ERR ret = periodicThread->start();
    if(ret != THREAD_ERR_NONE)
        log.fatal("Fail to start periodic thread. THREAD_ERR:%i",ret);

    /* start main loop:
     * -Wait for messages or timeout
     */
    while(isInterrupting() == LU_FALSE)
    {
        struct timeval timeoutLocal = {0, 50000};

        /* Initialise file descriptors set to be used by select */
        /* NOTE: Always check the File Descriptors before select() since they
         * can change at any time
         */
        FD_ZERO(&rfds); //Reset the file descriptors waiting list

        /* Add socketA file descriptor to waiting list */
        lu_int32_t canFDA = canPSocket.getFD();
        if(canFDA >= 0)
        {
            FD_SET(canFDA, &rfds);
        }

        /* Add socketB file descriptor to waiting list */
        lu_int32_t canFDB = canSSocket.getFD();
        if(canFDB >= 0)
        {
            FD_SET(canFDB, &rfds);
        }

        /* Get maximum file descriptor number */
        maxFD = LU_MAX(canFDA, canFDB);

        /* Wait for incoming messages */
        retval = select(maxFD + 1, &rfds, NULL, NULL, &timeoutLocal);
        if(isInterrupting() == LU_TRUE)
        {
            continue;   //exit thread
        }

        if (retval < 0)
        {
           /* Error. Save error code */
           lu_int32_t errTmp = errno;
           log.error("select error(%i). Line: %i",errTmp, __LINE__);
           continue;
        }
        else if(retval > 0)
        {
            /* No errors. Read from the FDs */
            if(canFDA >= 0)
            {
                if(FD_ISSET(canFDA, &rfds))
                {
                    /* CAN message available for canPSocket */
                    receiveMessage(canPSocket);
                }
            }
            if(canFDB >= 0)
            {
                if(FD_ISSET(canFDB, &rfds))
                {
                    /* CAN message available for canSSocket */
                    receiveMessage(canSSocket);
                }
            }
        }
    }

    /* Stop heart-beat generating*/
    periodicThread->stop();
    periodicThread->join();

    /* Reset all slaves to switch to application mode*/
    resetAllSlaveModules();

    log.debug("ModuleManager thread body Exit");
}



/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/* == WARNING ==: Any message handled here should NOT be bigger than a CAN frame,
    * since this socket handles messages from ALL the Slave modules in only ONE socket
    * (one socket per CAN bus).
    * This way, fragmented messages could be scrambled at any time.
    * In need, the message type handling for that specific message type could be moved
    * to CANIOModule, adding its own CANFraming socket and filter (one socket per module).
    */
void ModuleManager::initCANFilter()
{

   /* Initialise socketCAN filter */
   CanFilterTable filterTable;
   const lu_uint32_t TABLESIZE = 2;
   struct can_filter filter[TABLESIZE];
   CANHeaderStr *CANHeaderPtr;

   /* Accept HBeat */
   filter[0].can_id   = 0;
   filter[0].can_mask = 0;
   CANHeaderPtr = (CANHeaderStr*)(&filter[0].can_id);
   CANHeaderPtr->messageType = MODULE_MSG_TYPE_LPRIO;
   CANHeaderPtr->messageID = MODULE_MSG_ID_LPRIO_HBEAT_S;
   CANHeaderPtr = (CANHeaderStr*)(&filter[0].can_mask);
   CANHeaderPtr->messageType = CANHEADERDEF_MASK_MESSAGETYPE;
   CANHeaderPtr->messageID = CANHEADERDEF_MASK_MESSAGEID;

   /* Accept Module Info */
   filter[1].can_id   = 0;
   filter[1].can_mask = 0;
   CANHeaderPtr = (CANHeaderStr*)(&filter[1].can_id);
   CANHeaderPtr->messageType = MODULE_MSG_TYPE_LPRIO;
   CANHeaderPtr->messageID = MODULE_MSG_ID_LPRIO_MINFO_R;
   CANHeaderPtr = (CANHeaderStr*)(&filter[1].can_mask);
   CANHeaderPtr->messageType = CANHEADERDEF_MASK_MESSAGETYPE;
   CANHeaderPtr->messageID = CANHEADERDEF_MASK_MESSAGEID;

   filterTable.setTable(filter, TABLESIZE);
   canPSocket.setFilter(filterTable);
   canSSocket.setFilter(filterTable);
}

IIOModule* ModuleManager::createSlaveModule(MODULE module, MODULE_ID moduleID,COMM_INTERFACE iface)
{
    IIOModule* modulePtr = NULL;

    if( (module == MODULE_BRD) ||(module >= MODULE_LAST) ||
        (module == MODULE_MCM) ||(moduleID  >= MODULE_ID_BRD)
      )
    {
        log.error("Unsupported CAN Module: %i:%i",module, moduleID);
        return NULL;
    }

    if( ((lu_uint32_t)module >= (lu_uint32_t)MODULE_LAST) || 
        ((lu_uint32_t)moduleID >= (lu_uint32_t)MODULE_ID_LAST) 
        )
    {
        log.error("%s - Unknown module [%u,%u]!", __AT__, module, moduleID);
        return NULL;
    }

    /* Identify critical region scope */
    {
        LockingMutex lockingMutex(mutex);

        /* Valid ids. Get the module */
        modulePtr = modules[module][moduleID];

        /* allocate a new board? */
        if(modulePtr == NULL)
        {
            modulePtr = new CANIOModule(module,moduleID,iface);
            modules[module][moduleID] = modulePtr;

            log.info("Allocate new CAN Module: %s", modulePtr->getModuleName());
        }
    }

    return modulePtr;
}

IOM_ERROR ModuleManager::decodeMessage(ModuleMessage* messagePtr, COMM_INTERFACE iFace)
{
    IOM_ERROR ret = IOM_ERROR_NONE;
    IIOModule* modulePtr = NULL;

    if(messagePtr == NULL)
    {
        return IOM_ERROR_PARAM;
    }

    /* Get the destination board. If it doesn't exist create one */
    modulePtr = getModule(messagePtr->header.source, messagePtr->header.sourceID);
    if(modulePtr == NULL)
    {
        modulePtr = createSlaveModule(
                        messagePtr->header.source  ,
                        messagePtr->header.sourceID,
                        iFace);
    }


    /* Forward message */
    if(modulePtr != NULL)
    {
        modulePtr->decodeMessage(messagePtr);
    }

    return ret;
}


void ModuleManager::receiveMessage(CANFraming &socket)
{
    struct timeval timeout;
    ModuleMessage *messagePtr = NULL;
    CAN_ERROR canRet;

    /* Initialise can socket timeout */
    timeout.tv_sec  = CanTimeoutSec ;
    timeout.tv_usec = CanTimeoutUsec;

    canRet = socket.read(&messagePtr, &timeout);

    switch(canRet)
    {
    case CAN_ERROR_NONE:
        //Successful read
        /*Log received message*/
        if(log.isDebugEnabled())
            log.debug("CAN Event received: %s",messagePtr->header.toString().c_str());
        /* Forward the message to the boards */
        decodeMessage(messagePtr, socket.getIface());
        /* De-allocate the message */
        delete messagePtr;
        break;
    case CAN_ERROR_INIT:
        //Socket/Interface not initialised: ignore
        break;
    case CAN_ERROR_DOWN:
        //Interface is down: ignore
        break;
    case CAN_ERROR_ERR_FRAME:
        /* Handle error frame */
        log.error("CAN framing error");
        break;
    default:
        /* General error */
        log.error("Socket read error: %i", canRet);
        break;
    }
}

IOM_ERROR ModuleManager::tickEvent(lu_uint32_t dTime)
{
    lu_uint32_t i;
    IIOModule **modulePtrPtr;

    /* Lock database access */
    LockingMutex lockingMutex(mutex);

    /* Forward the event to all the available boards */
    for( i = 0, modulePtrPtr = &modules[0][0];
         i< modulesSize;
         ++i, ++modulePtrPtr
       )
    {
        if((*modulePtrPtr) != NULL)
        {
            (*modulePtrPtr)->tickEvent(dTime);
        }
    }
    return  IOM_ERROR_NONE;
}

/*
 *********************** End of file ******************************************
 */
