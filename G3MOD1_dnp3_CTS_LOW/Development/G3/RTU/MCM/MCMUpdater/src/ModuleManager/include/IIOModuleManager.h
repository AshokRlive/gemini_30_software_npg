/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       ModuleManager interface definition
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   21/06/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_97CE67EF_12B4_4af3_AC59_7FD1BB615C00__INCLUDED_)
#define EA_97CE67EF_12B4_4af3_AC59_7FD1BB615C00__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <vector>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IIOModule.h"
#include "CANProtocol.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class IIOModuleManager
{

public:
    IIOModuleManager() {}

    virtual ~IIOModuleManager() {}


	/**
	 * \brief Get a module reference
	 * 
	 * \param module Module type
	 * \param moduleId Module Identifier (address)
	 * 
	 * \return Pointer to the module. NULL if the module doesn't exist
	 */
	virtual IIOModule *getModule(MODULE module, MODULE_ID moduleID) = 0;

	virtual IIOModule *getModule(IOModuleIDStr mID)
	{
	    return getModule(mID.type,mID.id);
	}

    /**
     * \brief Get list of all modules
     *
     * \return Module list. Empty list when error.
     */
    virtual std::vector<IIOModule*> getAllModules() = 0;

	virtual void resetAllSlaveModules() = 0;

	virtual void setHBeatEnable(lu_uint8_t x) = 0;
	
	virtual lu_bool_t isHBeatEnabled() = 0;
};


#endif // !defined(EA_97CE67EF_12B4_4af3_AC59_7FD1BB615C00__INCLUDED_)


/*
 *********************** End of file ******************************************
 */
