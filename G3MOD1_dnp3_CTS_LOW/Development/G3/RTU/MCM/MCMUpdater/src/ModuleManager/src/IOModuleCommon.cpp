/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 * 				$Id: c_template.c 1469 2012-06-20 20:51:28Z fryers_j $
 *              $HeadURL: http://10.11.0.6:81/svn/gemini_30_software/trunk/Development/G3/common/template/c_template.c $
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <sstream>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "IOModuleCommon.h"
#include "StringUtil.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static lu_char_t getReleaseTypeSymbol(VERSION_TYPE releaseType);
static lu_char_t getReleaseTypeSymbol(lu_uint8_t releaseType);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
std::string genModuleName(MODULE type , MODULE_ID id)
{
    std::ostringstream ss;
    ss << MODULE_ToSTRING(type);

    // Append ID for multiple modules
    if(type != MODULE_MCM && type != MODULE_PSM && type != MODULE_HMI)
    {
        ss << (lu_int32_t)(id) + 1;
    }else{
        ss << " "; //append space for aligning purpose
    }

    return ss.str();
}

std::string version2String(ModuleVersionStr version)
{
    std::stringstream ss;
    lu_uint32_t major = version.major;
    lu_uint32_t minor = version.minor;
    ss << major << "." << minor;
    return ss.str();
}

std::string version2String(ModuleSoftwareVersionStr version)
{
    std::stringstream ss;
    lu_uint32_t major = version.version.major;
    lu_uint32_t minor = version.version.minor;
    lu_uint32_t patch = version.patch;
    ss << getReleaseTypeSymbol(version.relType)
       << "-" << major << "." << minor << "." << patch;
    return ss.str();
}


std::string IOModuleInfoStr::getVersionString()
{
 std::stringstream ss;
 ss << id.toString()
    << " Firmware:"         << version2String(moduleInfo.application.software)
    << " SVN" << moduleInfo.svnRevisionApp
    << "\t"
    << " Feature: "         << version2String(moduleInfo.featureRevision)
    << " System API: "      << version2String(moduleInfo.application.systemAPI)
    << " Bootloader Firmware: "<< version2String(moduleInfo.bootloader.software)
    << " Bootloader API: "     << version2String(moduleInfo.bootloader.systemAPI)
    << " SVN" << moduleInfo.svnRevisionBoot;
 return ss.str();
}


std::string IOModuleInfoStr::getStatusString()
{
  std::stringstream ss;
  ss << id.toString() << "\t"
     << (status.ok != LU_TRUE?         "  ": "OK") << "|"
     << (status.registered != LU_TRUE? " " : "R" ) << "|"
     << (status.configured != LU_TRUE? " " : "C" ) << "|"
     << (status.present != LU_TRUE?    " " : "P" ) << "|"
     << (status.detected != LU_TRUE?   " " : "D" ) << "|"
     << (status.disabled != LU_TRUE?   " " : "S" ) << "|"
     << (status.hasConfig != LU_TRUE?  " " : "X" ) << " - "
     << " Status:" << to_string(hBeatInfo.status);
     ss << " Errors:" << std::setw(2) << std::setfill(' ') << std::left << to_string(hBeatInfo.error);
     ModuleUID mUID(hBeatInfo.moduleUID);  //Convert from ModuleUIDStr to ModuleUID (.toString() use)
     ss << " S/N:" << std::right << mUID.toString();
  return ss.str();
}

std::string IOModuleIDStr::toString()
{
    return genModuleName(type,id);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/**
 * \brief Gives the character that represents the version type from its code.
 *
 * \param releaseType Release type code.
 *
 * \return character that represents the release type.
 */
static lu_char_t getReleaseTypeSymbol(VERSION_TYPE releaseType)
{
    lu_char_t relType;
    relType = *VERSION_TYPE_ToSTRING(releaseType);
    relType=(relType=='U')? 'X' : relType;    //convert Unknown to 'X'
    return relType;
}

/**
 * \brief Overloaded version of getReleaseTypeSymbol(VERSION_TYPE) to support int values.
 *
 * \param releaseType Release type code.
 *
 * \return character that represents the release type.
 */
static lu_char_t getReleaseTypeSymbol(lu_uint8_t releaseType)
{
    return getReleaseTypeSymbol(static_cast<VERSION_TYPE>(releaseType));
}




/*
 *********************** End of file ******************************************
 */
