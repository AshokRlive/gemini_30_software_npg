/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:HMIDetector.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17 Feb 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MCMIOMap.h"
#include "HMIPower.h"
#include "CANInterface.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
HMIPower::HMIPower():
                log(Logger::getLogger(SUBSYSTEM_ID_MODULE)),
                pwerEnableFD(NULL)
{
    init();
}

HMIPower::~HMIPower()
{
    // Set interface DOWN
#if POLLING_CAN_INTERFACE_STATE
    CANInterface::getInstance(COMM_INTERFACE_CAN1).down();
#endif

    // Set HMI power OFF
    if (pwerEnableFD != NULL)
    {
        writeGPIO(pwerEnableFD, LU_FALSE);
        closeGPIO(pwerEnableFD);
        pwerEnableFD = NULL;
    }

    log.debug("HMIDetector destroyed");
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

void HMIPower::init()
{
    lu_int32_t ret;

    // Init GPIO
    ret = initGPIO(MCMOutputGPIO_HMI_PWR_ENABLE, GPIOLIB_DIRECTION_OUTPUT, &pwerEnableFD);
    if(ret != 0)
       log.error("Failed to init HMI detection GPIO error %i",ret);

    // Set HMI power ON
    if(pwerEnableFD != NULL)
    {
        writeGPIO(pwerEnableFD,LU_TRUE);
    }

#if POLLING_CAN_INTERFACE_STATE
    // Set interface UP
    CANInterface::getInstance(COMM_INTERFACE_CAN1).up();
#endif
}

/*
 *********************** End of file ******************************************
 */
