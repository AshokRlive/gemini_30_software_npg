/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MCM IO Module interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   05/08/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_DDA5A6A2_D7E4_475d_9B6F_477BCD6989C4__INCLUDED_)
#define EA_DDA5A6A2_D7E4_475d_9B6F_477BCD6989C4__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <vector>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "IIOModule.h"
#include "ModuleProtocol.h"
#include "Logger.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define MCM_MODULE_ID MODULE_ID_0   //MCM module id

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class MCMIOModule : public IIOModule
{
public:
    struct StatsCANStr : public CanStatsStr
    {
        lu_uint16_t validCANError : 1;
        lu_uint16_t validCANBusError : 1;
        lu_uint16_t validCANArbitrationError : 1;
        lu_uint16_t validCANDataOverrun : 1;
        lu_uint16_t validCANBusOff : 1;
        lu_uint16_t validCANErrorPassive : 1;
        lu_uint16_t validCANTxCount : 1;
        lu_uint16_t validCANRxCount : 1;
        lu_uint16_t validCANRxLost : 1;
        lu_uint16_t unused : 7;
    public:
        StatsCANStr() :
                         validCANError(0),
                         validCANBusError(0),
                         validCANArbitrationError(0),
                         validCANDataOverrun(0),
                         validCANBusOff(0),
                         validCANErrorPassive(0),
                         validCANTxCount(0),
                         validCANRxCount(0),
                         validCANRxLost(0),
                         unused(0)
        {};
        StatsCANStr& operator=(const CanStatsStr& canStats);
        void validateAll();
        std::string toString();
    private:
        template<typename T> static void putValidValue( std::stringstream &ss,
                                                        const char *name,
                                                        const lu_uint16_t valid,
                                                        T value
                                                        );
    };

public:
    /**
     * Custom constructor
     *
     * Initialises the MCM module
     *
     * \param moduleID MCM Module ID
     * \param cfgMgr Reference to the Configuration Manager
     */
    MCMIOModule(MODULE_ID moduleID);
    virtual ~MCMIOModule();

    /* === Inherited from IIOModule === */
    virtual IOM_ERROR sendMessage(ModuleMessage* messagePtr);
    virtual IOM_ERROR sendMessage(MODULE_MSG_TYPE messageType, MODULE_MSG_ID messageID, PayloadRAW* messagePtr);
    virtual IOM_ERROR sendMessage(sendReplyStr* messagePtr);
    virtual IOM_ERROR decodeMessage(ModuleMessage* message);
    virtual void getInfo(IOModuleInfoStr& version);
    virtual void getDetails(IOModuleDetailStr &moduleInfo);
    virtual ModuleUID getModuleUID();
    virtual std::string getStateName();
    virtual lu_bool_t isReadyForUpgrade();
    virtual IOM_ERROR restart(MD_RESTART type);
    virtual IOM_ERROR ping(lu_uint32_t timeout, lu_uint32_t payloadSize = 8);
    virtual IOM_ERROR tickEvent(lu_uint32_t dTime) ;

    /**
     * \brief Return module CAN statistics
     *
     * \param stats Where the module's all CAN stats are saved
     *
     * \return Error code
     */
    virtual IOM_ERROR getCANStats(std::vector<StatsCANStr>& stats);

private:
    Logger& log;
};


#endif // !defined(EA_DDA5A6A2_D7E4_475d_9B6F_477BCD6989C4__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
