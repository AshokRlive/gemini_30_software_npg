/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MonitorProtocolLayer.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   1 Oct 2013     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MonitorProtocolHandler.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
MonitorProtocolHandler::MonitorProtocolHandler(IMCMUpdater* mcmUpdater)
                            :AbstractMonitorProtocolLayer(APP_ID_MCMUPDATER, SCHED_TYPE_FIFO),
                             mcmUpdater(mcmUpdater),
                             log(Logger::getLogger(SUBSYSTEM_ID_MT_HANDLER))

{

}

MonitorProtocolHandler::~MonitorProtocolHandler()
{

}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void MonitorProtocolHandler:: notify_req_exit(lu_int8_t action, lu_int8_t reason)
{
    LU_UNUSED(action);
    LU_UNUSED(reason);

    log.warn("Received monitor request: exit");
    mcmUpdater->exit();
}


void MonitorProtocolHandler::notify_rep_ping()
{
    log.info("Received monitor reply: ping");
}
/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
