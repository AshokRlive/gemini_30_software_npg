/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: MemoryManager.cpp 26-Oct-2012 10:04:56 pueyos_a $
 *               $HeadURL: Y:\workspace\G3Trunk\G3\RTU\MCM\src\MemoryManager\src\MemoryManager.cpp $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MemoryManager header module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 26-Oct-2012 10:04:56	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   26-Oct-2012 10:04:56  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(MEMORY_MANAGER_H__INCLUDED_)
#define MEMORY_MANAGER_H__INCLUDED_



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "AbstractMemoryManager.h"
#include "Logger.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * Class MemoryManager
 */
class MemoryManager : public AbstractMemoryManager
{
public:
    MemoryManager();
    virtual ~MemoryManager();

    /* ==Inherited from IMemoryManager== */
    virtual IMemoryManager::MEMERROR init(IMemoryManager::MEM_TYPE memService);
    virtual void init();

protected:
    /* ==Implements for AbstractMemoryManager== */
    IMemService* createService(IMemoryManager::MEM_TYPE type);

private:
    Logger& log;
};


#endif // !defined(MEMORY_MANAGER_H__INCLUDED_)

/*
 *********************** End of file ******************************************
 */

