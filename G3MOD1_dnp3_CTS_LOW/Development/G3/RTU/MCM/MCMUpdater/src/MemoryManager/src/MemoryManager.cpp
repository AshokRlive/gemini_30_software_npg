/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: MemoryManager.cpp 26-Oct-2012 10:04:56 pueyos_a $
 *               $HeadURL: Y:\workspace\G3Trunk\G3\RTU\MCMUpdater\src\MemoryManager\src\MemoryManager.cpp $
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MemoryManager module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 26-Oct-2012 10:04:56	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   26-Oct-2012 10:04:56  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "MemoryManager.h"
#include "IOModuleCommon.h" //for Storage type size
#include "NVRAMDefInfo.h"   //for Storage type size

//Memory Service Providers
#include "MemServiceNVRAMID.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
MemoryManager::MemoryManager(): log(Logger::getLogger(SUBSYSTEM_ID_MEMORYMAN))
{}

MemoryManager::~MemoryManager()
{}


IMemoryManager::MEMERROR MemoryManager::init(IMemoryManager::MEM_TYPE memService)
{
    return AbstractMemoryManager::init(memService);
}

void MemoryManager::init()
{
    /* Set storage members size */
    setSize(IMemoryManager::STORAGE_VAR_ID, sizeof(NVRAMInfoStr));
    setSize(IMemoryManager::STORAGE_VAR_MODULEUID, sizeof(ModuleUID));
    setSize(IMemoryManager::STORAGE_VAR_FEATUREREV, sizeof(BasicVersionStr));

    /* specifically initialise NVRAM memory before the rest */
    /* NVRAM ID Memory initialisation */
    IMemoryManager::MEMERROR memResult;
    memResult = init(IMemoryManager::MEM_TYPE_NVRAMID);
    switch(memResult)
    {
        case IMemoryManager::MEMERROR_NONE:
            break;
        case IMemoryManager::MEMERROR_ACCESS:
            //Primary Non-volatile ID Memory failed
            log.error("Primary Non-volatile ID Memory failed");
            break;
        case IMemoryManager::MEMERROR_ACCESSBKP:
            //Secondary Non-volatile ID Memory failed
            log.error("Secondary Non-volatile ID Memory failed");
            break;
        default:
            //Both NVRAM ID memories failed
            log.error("ID information not available: Primary AND Secondary Non-volatile ID Memory failed");
            break;
    }
    AbstractMemoryManager::init();
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
IMemService* MemoryManager::createService(IMemoryManager::MEM_TYPE type)
{
    switch(type)
    {
        case IMemoryManager::MEM_TYPE_NVRAMID:
            return MemoryManagerFactory::create<MemServiceNVRAMID>(MEMDEVICE_IDROM, MEMDEVICE_IDAPPRAM);
            break;
        default:
            break;
    }
    return NULL;    //No Service Provider available for this type
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */


