/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *    CURRENT REVISION
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   02/09/13      wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <iostream>
#include <fstream>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "GlobalDef.h"
#include "ConfigurationToolHandler.h"
#include "versions.h"
#include "RTUInfo.h"
#include "CANIOModule.h"
#include "MCMIOModule.h"
#include "dbg.h"

#if CT_ZMQ_SUPPORT
#include "ZMQLinkLayer.h"
#else
#include "HttpCTHLinkLayer.h"
#endif
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define HTTP_CTH_LINKLAYER_THREAD   Thread::PRIO_MIN + 1
#define CGI_SOCKET_NAME     "/var/lib/lighttpd/sockets/g3.comm.socket"
#define DEFAULT_SITENAME    "Gemini3 RTU"

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static void getSystemAPI(CTMsgPL_R_SystemAPI &sysAPIPtr);
static std::string getCGISocketPath();
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
std::string cgipath = getCGISocketPath();
#if CT_ZMQ_SUPPORT
static ZMQLinkLayer cthLinkLayer(CTMsg_MaxSize,
                SCHED_TYPE_FIFO,
                HTTP_CTH_LINKLAYER_THREAD,
				cgipath.c_str());
#else
/*HttpLinklayer instance*/
static HttpCTHLinkLayer cthLinkLayer(CTMsg_MaxSize,
                                    SCHED_TYPE_FIFO,
                                    HTTP_CTH_LINKLAYER_THREAD,
									cgipath.c_str());
#endif


namespace CTH{

/**
 *  Supported configuration tool message.
 *     TYPE                         ID
 *  CTMsgType_Login     :CTMsgID_C_UserLogin
 *                       CTMsgID_C_CheckAlive
 *  CTMsgType_RTUInfo   :CTMsgID_C_GetSysAPI
 *                       CTMsgID_C_GetRTUInfo
 *  CTMsgType_Upgrade   :CTMsgID_C_UpgradeModeOff
 *                       CTMsgID_C_UpgradeModule
 *                       CTMsgID_C_GetUpgradeState
 *  CTMsgType_Module    :CTMsgID_C_ModuleInfo
 *                       CTMsgID_C_ModuleDetail
 *                       CTMsgID_C_ModuleCANStats
 *  CTMsgType_RTUControl:CTMsgID_C_Reset
 *                       CTMsgID_C_Restart
 *                       CTMsgID_C_Shutdown
 */
ConfigurationToolHandler::ConfigurationToolHandler(IIOModuleManager &gModuleMgr,
                                                    UpgradeManager &gUpgradeMgr,
                                                    IMCMUpdater* mcmUpdater
                                                   ) :
									AbstractCTHandler(cthLinkLayer),
                                    moduleMgr(gModuleMgr),
                                    upgradeMgr(gUpgradeMgr),
                                    mcmUpdater(mcmUpdater),
                                    logReqHdl()
{}

ConfigurationToolHandler::~ConfigurationToolHandler()
{
    DBG_INFO("Destroyed ConfigurationToolHandler.");
}

void ConfigurationToolHandler::startService()
{
    cthLinkLayer.startServer();
}

void ConfigurationToolHandler::stopService()
{
    cthLinkLayer.stopServer(LU_FALSE);
}


CTHMessage* ConfigurationToolHandler::decodePayload(const CTMessageHeader &header,
                                                    const CTMessagePayload &payload)
{
    CTHMessage* reply = NULL;

    /* Decode payload*/
    switch (header.messageType)
    {
        case CTMsgType_Login:
            reply = decodeMessageLogin(header, payload);
        break;

        case CTMsgType_RTUInfo:
            reply = decodeMessageRTUInfo(header, payload);
        break;

        case CTMsgType_Upgrade:
            reply = decodeMessageUpgrade(header, payload);
        break;

        case CTMsgType_Module:
            reply = decodeMessageModuleInfo(header,payload);
        break;

        case CTMsgType_RTUControl:
            reply = decodeMessageRTUControl(header,payload);
        break;

        case CTMsgType_Log:
            reply = logReqHdl.decodeMessageLog(header,payload);
        break;


        /* ---------Unsupported Message Type-----------*/

        default:
           reply = createAckNack(header, CTH_NACK_UNSUPPORTED);
    }

    return reply;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

CTHMessage* ConfigurationToolHandler::decodeMessageLogin(const CTMessageHeader &header,
                                                        const CTMessagePayload &payload)
{
    log.info("%s \"%s\" message received.", __AT__, CTMsgID_Login_ToSTRING(header.messageID));

    /* Decode message ID */
    switch(header.messageID)
    {
        case CTMsgID_C_CheckAlive:
        {
            /* Check alive*/
            if(payload.payloadLen != 0)
            {
                return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
            }
            else
            {
                /* Reply checkAlive Info message */
                CTMsg_R_AliveInfo* chkAlive
                        = new CTMsg_R_AliveInfo(header.requestID, replyID.newID());
                CTMsgPL_R_AliveInfo *chkAlivePL
                        = reinterpret_cast<CTMsgPL_R_AliveInfo *>(&(*chkAlive)[0]);
                chkAlivePL->runningApp = CTH_RUNNINGAPP_UPDATER;
                chkAlive->setEntries(1);
                return chkAlive;
            }
        }
        break;

        case CTMsgID_C_UserLogin:
        {
            /* User ID check*/
            if(payload.payloadLen <= sizeof(CTMsgPL_C_UserLogin))
            {
                return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
            }
            else
            {
                /*Reply User Info message*/
                CTMsg_R_UserInfo* userInfo
                    = new CTMsg_R_UserInfo(header.requestID, replyID.newID());
                CTMsgPL_R_UserInfo *userInfoPL
                    = reinterpret_cast<CTMsgPL_R_UserInfo *>(&(*userInfo)[0]);
                userInfoPL->sessionID = 0;// TODO: PW - generate&manager session ID
                userInfoPL->userLevel = (lu_uint8_t)USER_LEVEL_ADMIN; // Admin by default
                userInfo->setEntries(1);
                return userInfo;
            }
        }
        break;

        case CTMsgID_C_GetMDAlgorithm:
        {
           /* Compose return message */
           CTMsg_R_MDAlgorithm* reply = new CTMsg_R_MDAlgorithm(header.requestID, replyID.newID());
           CTMsgPL_R_MDAlgorithm* rPayload = reinterpret_cast<CTMsgPL_R_MDAlgorithm*>(reply->firstEntry());
           memset(rPayload->mdAlgorithm, 0, (rPayload->ALGORITHM_LENGTH));
           strncpy(rPayload->mdAlgorithm, "MD5", rPayload->ALGORITHM_LENGTH);
           reply->setEntries(1);      /* Set the number of valid entries */
           return reply;
        }
        break;

        default:
            /* ---------Unsupported Message IDs-----------*/
            log.warn("%s Unsupported message[MsgID: %i] received.", __AT__, header.messageID);
            return createAckNack(header, CTH_NACK_UNSUPPORTED);

    }//endswitch(header.messageID)
}


CTHMessage* ConfigurationToolHandler::decodeMessageRTUInfo(const CTMessageHeader &header,
                                                           const CTMessagePayload &payload
                                                           )
{
    log.info("%s \"%s\" message received.", __AT__, CTMsgID_RTUInfo_ToSTRING(header.messageID));

    /* Decode message ID */
    switch(header.messageID)
    {
        case CTMsgID_C_GetSysAPI:
        {
            /* Get G3 System API version message */
            if(payload.payloadLen != 0)
            {
                return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
            }
            /* Reply system API message*/
            else
            {
                CTMsg_R_SystemAPI* sysAPI
                        = new CTMsg_R_SystemAPI(header.requestID, replyID.newID());
                CTMsgPL_R_SystemAPI *sysAPIPL
                        = reinterpret_cast<CTMsgPL_R_SystemAPI *>(&(*sysAPI)[0]);
                getSystemAPI(*sysAPIPL);
                sysAPI->setEntries(1);
                log.debug("%s Get versions: cfgAPI=%d.%d, schema=%d.%d, MCM=%d.%d",
                           __AT__,
                           sysAPIPL->configG3.systemAPI.major,
                           sysAPIPL->configG3.systemAPI.minor,
                           sysAPIPL->schema.major,
                           sysAPIPL->schema.minor,
                           sysAPIPL->MCMModule.systemAPI.major,
                           sysAPIPL->MCMModule.systemAPI.minor
                           );
                return sysAPI;
            }
        }
        break;

        case CTMsgID_C_GetRTUInfo:
            /* Get RTU info message */
            return decodeGetRTUInfo(header, payload);

        case CTMsgID_C_GetSDPVersion:
           return decodeGetSDPVersion(header, payload);

        default:
            /* ---------Unsupported Message IDs-----------*/
            log.warn("%s Unsupported message[MsgID: %i] received.", __AT__, header.messageID);
            return createAckNack(header, CTH_NACK_UNSUPPORTED);

    }//endswitch(header.messageID)
}


CTHMessage* ConfigurationToolHandler::decodeMessageRTUControl(const CTMessageHeader &header,
                                                              const CTMessagePayload &payload)
{
    log.info("%s \"%s\" message received.", __AT__, CTMsgID_RTUInfo_ToSTRING(header.messageID));
    CTH_ACKNACKCODE result= CTH_ACK;

    /* Decode message ID */
    switch(header.messageID)
    {
        case CTMsgID_C_Reset:
        {
            /* Reset the system*/
            if(payload.payloadLen != 0)
            {
                result = CTH_NACK_PAYLOAD;
            }
            else
            {
                mcmUpdater->rebootRTU();
                result = CTH_ACK;
            }
        }
        break;

        case CTMsgID_C_Restart:
        {
            /* Restart Application*/
            if(payload.payloadLen != 0)
            {
                result = CTH_NACK_PAYLOAD;
            }
            else
            {
                mcmUpdater->restart();
                result = CTH_ACK;
            }
        }
        break;

        case CTMsgID_C_Shutdown:
        {
            /*  System Shutdown*/
            if(payload.payloadLen != 0)
            {
                result = CTH_NACK_PAYLOAD;
            }
            else
            {
                //Log Event
                mcmUpdater->shutdownRTU();
                result = CTH_ACK;
            }
        }
        break;

        case CTMsgID_C_EraseConfig:
        {
            /*Erase RTU configuration*/
        	return fileTransfer.eraseConfig(header,payload);
        }
        break;

        case CTMsgID_C_EraseCommissioningCache:
	    {
		   /*Erase RTU commissioning cache*/
		    return fileTransfer.eraseCommissioningCache(header,payload);
	    }
	    break;

        default:
            /*Unsupported message type*/
            log.warn("%s Unsupported message[MsgID: %i] received.", __AT__, header.messageID);
            return createAckNack(header, CTH_NACK_UNSUPPORTED);

    }//endswitch(header.messageID)
    return createAckNack(header, result);
}


CTHMessage* ConfigurationToolHandler::decodeSetSDPVersion(const CTMessageHeader &header,
                    const CTMessagePayload &payload)
{

    const lu_char_t *FTITLE = "ConfigurationToolHandler::decodeMessageSDPVersion:";
    CTH_ACKNACKCODE result = CTH_ACK;


    /* Invalid payload */
    if( payload.payloadLen == 0)
    {
        log.error("%s Invalid payload size:%d", FTITLE, payload.payloadLen);
        result = CTH_NACK_PAYLOAD;
    }
    /* Compose reply message */
    else
    {
        lu_char_t* sdpVersion = new lu_char_t[payload.payloadLen];
        memcpy(sdpVersion, payload.payloadPtr,payload.payloadLen);

        std::ofstream sdpfile;
        sdpfile.open (SDP_VERSION_FILENAME);
        if (sdpfile.is_open())
        {
            sdpfile << sdpVersion <<"\n";
            sdpfile.close();
        }
        delete[] sdpVersion;
    }

    return createAckNack(header,result);
}




CTHMessage* ConfigurationToolHandler::decodeMessageUpgrade(const CTMessageHeader &header,
                                                         const CTMessagePayload &payload)
{
    log.info("%s \"%s\" message received.", __AT__, CTMsgID_Upgrade_ToSTRING(header.messageID));
    CTH_ACKNACKCODE result= CTH_ACK;

    /* Decode message ID */
    switch(header.messageID)
    {
        case CTMsgID_C_UpgradeModeOff:
            /* Exit upgrading mode*/
            //TODO send message to monitor
            mcmUpdater->exit();     //Exit MCM udpater application
        break;

        case CTMsgID_C_UpgradeModeOn:
            //Do nothing since it is already in upgrade mode
            break;

        case CTMsgID_C_UpgradeModule:
            /* Start Module upgrade procedure */
            return decodeUpgradeModuleMessage(header,payload);
        break;

        case CTMsgID_C_GetUpgradeState:
            /* Get RTU upgrade status */
            return decodeUpgradeStatusMessage(header,payload);
        break;

        case CTMsgID_C_EraseFirmware:
            /* Erase firmware*/
            return decodeEraseFirmwareMessage(header,payload);
        break;

        case CTMsgID_C_SDPVersion:
            return decodeSetSDPVersion(header,payload);

        default:
            /* ---------Unsupported Message IDs-----------*/
            log.warn("%s Unsupported message[MsgID: %i] received.", __AT__, header.messageID);
            return createAckNack(header, CTH_NACK_UNSUPPORTED);

    }//endswitch(header.messageID)
    return createAckNack(header,result);
}


CTHMessage* ConfigurationToolHandler::decodeMessageModuleInfo(const CTMessageHeader &header,
                                                            const CTMessagePayload &payload)
{
    log.info("%s \"%s\" message received.", __AT__, CTMsgID_Module_ToSTRING(header.messageID));
    CTH_ACKNACKCODE result= CTH_ACK;

    switch(header.messageID)
    {
        case CTMsgID_C_ModuleInfo:
            if(payload.payloadLen != 0)
            {
                result = CTH_NACK_PAYLOAD;  //Invalid payload
            }
            else
            {
                CTMsg_R_ModuleInfo* mInfo
                        = new CTMsg_R_ModuleInfo(header.requestID, replyID.newID());
                CTH_ERROR err = getModuleInfo(*mInfo, moduleMgr);
                if (err == CTH_ERROR_NONE)
                {
                    return mInfo;
                }
                else
                {
                    /* An error occurred - Send back a NACK */
                    delete mInfo;
                    log.error("%s Fail to get module info[Err:%i]. Send NACK",
                                    __AT__, err
                                  );
                    result =  CTH_NACK_NOMODULE;
                }
            }
        break;

        case CTMsgID_C_ModuleDetail:
        {
            if(payload.payloadLen != sizeof(CTMsgPL_C_ModuleID))
            {
                return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
            }
            /* Decode valid payload */
            CTMsgPL_C_ModuleID* moduleRef = reinterpret_cast<CTMsgPL_C_ModuleID*>(payload.payloadPtr);
            MODULE moduleType = (MODULE)moduleRef->moduleType;
            MODULE_ID moduleID = (MODULE_ID)moduleRef->moduleID;

            /* Get data */
            IIOModule *module = moduleMgr.getModule(moduleType, moduleID);  //Retrieve module
            if(module == NULL)
            {
                // Module not found!
                log.error("%s Error getting details of module %i:%i.", __AT__, moduleType, moduleID+1);
                return createAckNack(header, CTH_NACK_NOMODULE);
            }
            IOModuleDetailStr info;
            module->getDetails(info);

            /* Compose return message */
            CTMsg_R_ModuleDetail* reply = new CTMsg_R_ModuleDetail(header.requestID, replyID.newID());
            CTMsgPL_R_ModuleDetail* rPayload = reinterpret_cast<CTMsgPL_R_ModuleDetail*>(reply->firstEntry());

            //Extract the module information
            rPayload->architecture = info.architecture;
            rPayload->svnRevisionBoot = info.svnRevisionBoot;
            rPayload->svnRevisionApp = info.svnRevisionApp;
            rPayload->uptime = info.uptime;
            reply->setEntries(1);
            log.info("%s replies with %s", __AT__, rPayload->toString().c_str());
            return reply;
        }
        break;

        case CTMsgID_C_ModuleCANStats:
        {
            if(payload.payloadLen != sizeof(CTMsgPL_C_ModuleID))
            {
                return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
            }

            /* Decode valid payload */
            CTMsgPL_C_ModuleID* moduleRef = reinterpret_cast<CTMsgPL_C_ModuleID*>(payload.payloadPtr);
            MODULE moduleType = (MODULE)moduleRef->moduleType;
            MODULE_ID moduleID = (MODULE_ID)moduleRef->moduleID;

            /* Get data */
            IIOModule *module = moduleMgr.getModule(moduleType, moduleID);  //Retrieve module
            if(module == NULL)
            {
                // Module not found!
                log.error("%s Error getting module %i:%i for CAN stats.",
                                __AT__, moduleType, moduleID+1);
                return createAckNack(header, CTH_NACK_NOMODULE);
            }

            IOM_ERROR res = IOM_ERROR_PARAM;
            typedef std::vector<MCMIOModule::StatsCANStr> CANStatsList;
            CANStatsList infoList;

            if(module->isMCM() == LU_TRUE)
            {
                MCMIOModule *mcmModule = dynamic_cast<MCMIOModule*>(module);
                if(mcmModule != NULL)
                {
                    /* MCM Module does report CAN stats to Config Tool having 2 CAN ports */
                    res = mcmModule->getCANStats(infoList);
                }
            }
            else
            {
                CANIOModule *canModule = dynamic_cast<CANIOModule*>(module);
                if(canModule != NULL)
                {
                    CanStatsStr info;
                    res = canModule->getCANStats(info);
                    if(res != IOM_ERROR_NONE)
                    {
                        MCMIOModule::StatsCANStr entry;
                        entry = info;
                        entry.validateAll();   //Sets all entries coming from CAN module as valid ones
                        infoList.push_back(entry);   //Compose 1-element vector for reporting
                    }
                }
            }
            if(res != IOM_ERROR_NONE)
            {
                log.error("%s Error getting CAN stats of module %s [%i:%i]. Error %i: %s",
                            __AT__, module->getModuleName(), moduleType, moduleID+1, res, IOM_ERROR_ToSTRING(res));
                return createAckNack(header, CTH_NACK_NOMODULE);
            }

            /* Compose return message */
            CTMsg_R_ModuleCANStats* reply = new CTMsg_R_ModuleCANStats(header.requestID, replyID.newID());
            CTMsgPL_R_ModuleCANStats* rPayload = reinterpret_cast<CTMsgPL_R_ModuleCANStats*>(reply->firstEntry());

            for(CANStatsList::iterator it = infoList.begin(), end = infoList.end(); it != end; ++it)
            {
                //Extract the module information
                rPayload->canArbitrationError = (*it).canArbitrationError;
                rPayload->validCANArbitrationError = (*it).validCANArbitrationError;
                rPayload->canBusError = (*it).canBusError;
                rPayload->validCANBusError = (*it).validCANBusError;
                rPayload->canBusOff = (*it).canBusOff;
                rPayload->validCANBusOff = (*it).validCANBusOff;
                rPayload->canDataOverrun = (*it).canDataOverrun;
                rPayload->validCANDataOverrun = (*it).validCANDataOverrun;
                rPayload->canError = (*it).canError;
                rPayload->validCANError = (*it).validCANError;
                rPayload->canErrorPassive = (*it).canErrorPassive;
                rPayload->validCANErrorPassive = (*it).validCANErrorPassive;
                rPayload->canTxCount = (*it).canTxCount;
                rPayload->validCANTxCount = (*it).validCANTxCount;
                rPayload->canRxCount = (*it).canRxCount;
                rPayload->validCANRxCount = (*it).validCANRxCount;
                rPayload->canRxLost = (*it).canRxLost;
                rPayload->validCANRxLost = (*it).validCANRxLost;
                log.info("%s replies with %s", __AT__, rPayload->toString().c_str());
                rPayload++;
            }
            reply->setEntries(infoList.size());
            return reply;
        }
        break;
        default:
            log.warn("%s Unsupported message[MsgID: %i] received.", __AT__, header.messageID);
            return createAckNack(header, CTH_NACK_UNSUPPORTED);
    }
    return createAckNack(header, result);
}


CTHMessage* ConfigurationToolHandler::decodeUpgradeModuleMessage(const CTMessageHeader &header,
                                                       const CTMessagePayload &payload)
{
    CTH_ACKNACKCODE result = CTH_ACK;
    /* Invalid payload */
    if( payload.payloadLen != sizeof(CTMsgPL_C_ModuleRef) )
    {
        result = CTH_NACK_PAYLOAD;
    }
    /*Invalid state*/
    else if(upgradeMgr.isUpgrading())
    {
        result = CTH_NACK_UPGRADESTATE;
    }
    else
    {
        CTMsgPL_C_ModuleRef *moduleRef
                      = reinterpret_cast<CTMsgPL_C_ModuleRef *>(payload.payloadPtr);
        IOModuleIDStr mID;
        mID.type = (MODULE)moduleRef->moduleType;
        mID.id   = (MODULE_ID)moduleRef->moduleID;

        std::string fwFile = (mID.type == MODULE_MCM)?G3_FW_MCM :G3_FW_SLAVE;

        /* Note that the upgrade procedure uses only the serial number, not the complete module UID */
        ModuleUID moduleUID;
        moduleUID.serialNumber = moduleRef->serial;
        UPG_ERR ret = upgradeMgr.upgradeModule(fwFile, mID, moduleUID);
        result = static_cast<CTH_ACKNACKCODE>(ret);
    }
    return createAckNack(header,result);
}


CTHMessage* ConfigurationToolHandler::decodeUpgradeStatusMessage(const CTMessageHeader &header,
                const CTMessagePayload &payload)

{
    const lu_char_t *FTITLE = "ConfigurationToolHandler::decodeUpgradeStatusMessage:";
    CTH_ACKNACKCODE result = CTH_ACK;

    /* Invalid payload */
    if( payload.payloadLen != 0 )
    {
        result = CTH_NACK_PAYLOAD;
    }
    /* Compose reply message */
    else
    {
        CTMsg_R_UpgradeState* reply
                            = new CTMsg_R_UpgradeState(header.requestID, replyID.newID());
        CTMsgPL_R_UpgradeState *upgState
                            = reinterpret_cast<CTMsgPL_R_UpgradeState *>(&(*reply)[0]);

        upgState->value = upgradeMgr.getProgress();
        upgState->state = upgradeMgr.getState();
        reply->setEntries(1);

        log.info("%s Upgrade is in state 0x%02x, value %d", FTITLE,
                        upgState->state,
                        upgState->value
                        );
        return reply;
    }
    return createAckNack(header,result);
}


CTHMessage* ConfigurationToolHandler::decodeEraseFirmwareMessage(
                                            const CTMessageHeader &header,
                                            const CTMessagePayload &payload)

{
    const lu_char_t *FTITLE = "ConfigurationToolHandler::decodeEraseFirmwareMessage:";
    CTH_ACKNACKCODE result = CTH_ACK;


    /* Invalid payload */
    if( payload.payloadLen == 0 || payload.payloadLen%(sizeof(CTMsgPL_C_ModuleRef)) != 0)
    {
        log.error("%s Invalid payload size:%d", FTITLE, payload.payloadLen);
        result = CTH_NACK_PAYLOAD;
    }
    /* Compose reply message */
    else
    {
        lu_uint8_t  moduleNum = payload.payloadLen/(sizeof(CTMsgPL_C_ModuleRef));
        CTMsg_R_EraseFirmware *reply = new CTMsg_R_EraseFirmware(header.requestID, replyID.newID());;
        lu_uint8_t eraseModuleNum = 0;
        UPG_ERR err;

        CTMsgPL_C_EraseFirmware* req_mid = (CTMsgPL_C_EraseFirmware*)(payload.payloadPtr);

        IOModuleIDStr mid;

        /* Erase modules*/
        for(lu_uint8_t i = 0; i < moduleNum; i++)
        {
            mid.id = (MODULE_ID)(req_mid[i].moduleID);
            mid.type = (MODULE)req_mid[i].moduleType;
            log.info("%s Erasing module:%s",FTITLE,mid.toString().c_str());
            err = upgradeMgr.eraseModule(mid);
            if(err == UPG_ERR_NONE)
            {
                (*reply)[eraseModuleNum].moduleID = req_mid[i].moduleID;
                (*reply)[eraseModuleNum].moduleType = req_mid[i].moduleType;
                (*reply)[eraseModuleNum].serial = req_mid[i].serial;
                eraseModuleNum++;
            }
        }
        log.info("%s %i modules' firmware haven been erased",FTITLE,eraseModuleNum);
        reply->setEntries(eraseModuleNum);

        return reply;
    }

    return createAckNack(header,result);
}


CTH_ERROR ConfigurationToolHandler::getModuleInfo(CTMsg_R_ModuleInfo &info,
                                            IIOModuleManager& moduleManager)
{
    /* Get the system modules */
    std::vector<IIOModule*> moduleList;
    moduleList = moduleManager.getAllModules();
    if (moduleList.empty())
    {
        return CTH_ERROR_MODULES;    //Error
    }

    info.setEntries(moduleList.size());   //Set the number of entries
    lu_uint32_t moduleNum = info.getEntries();//Get the number of entries we can effectively store

    /* Modules retrieved correctly: Extract the modules information */
    for(lu_uint32_t i = 0; i < moduleNum; ++i)
    {
        IOModuleInfoStr moduleInfo;

        /* Get module info */
        moduleList[i]->getInfo(moduleInfo);

        /* Convert Module info into the Configuration tool module info */
        CTMsgPL_R_ModuleInfo *CTModuleInfoTmpPtr = &info[i];
        CTModuleInfoTmpPtr->type            = moduleInfo.id.type;
        CTModuleInfoTmpPtr->id              = moduleInfo.id.id;
        CTModuleInfoTmpPtr->serial          = moduleInfo.hBeatInfo.moduleUID.serialNumber;  //Serial number only
        CTModuleInfoTmpPtr->moduleStatus    = moduleInfo.hBeatInfo.status;
        CTModuleInfoTmpPtr->moduleErrors    = moduleInfo.hBeatInfo.error;
        /* Module-related Version/revision values */
        ModuleInfoStr* mInfoPtr = &(moduleInfo.moduleInfo);
        CTModuleInfoTmpPtr->software.relType        = mInfoPtr->application.software.relType;
        CTModuleInfoTmpPtr->software.version.major  = mInfoPtr->application.software.version.major;
        CTModuleInfoTmpPtr->software.version.minor  = mInfoPtr->application.software.version.minor;
        CTModuleInfoTmpPtr->software.patch          = mInfoPtr->application.software.patch;
        CTModuleInfoTmpPtr->sysAPI.major            = mInfoPtr->application.systemAPI.major;
        CTModuleInfoTmpPtr->sysAPI.minor            = mInfoPtr->application.systemAPI.minor;
        CTModuleInfoTmpPtr->feature.major           = mInfoPtr->featureRevision.major;
        CTModuleInfoTmpPtr->feature.minor           = mInfoPtr->featureRevision.minor;
        CTModuleInfoTmpPtr->bootloaderAPI.major     = mInfoPtr->bootloader.systemAPI.major;
        CTModuleInfoTmpPtr->bootloaderAPI.minor     = mInfoPtr->bootloader.systemAPI.minor;
        CTModuleInfoTmpPtr->bootloaderSw.relType    = mInfoPtr->bootloader.software.relType;
        CTModuleInfoTmpPtr->bootloaderSw.version.major = mInfoPtr->bootloader.software.version.major;
        CTModuleInfoTmpPtr->bootloaderSw.version.minor = mInfoPtr->bootloader.software.version.minor;
        CTModuleInfoTmpPtr->bootloaderSw.patch      = mInfoPtr->bootloader.software.patch;
        /* status of the module */
        CTModuleInfoTmpPtr->ok          = moduleInfo.status.ok;
        CTModuleInfoTmpPtr->registered  = moduleInfo.status.registered;
        CTModuleInfoTmpPtr->configured  = moduleInfo.status.configured;
        CTModuleInfoTmpPtr->present     = moduleInfo.status.present;
        CTModuleInfoTmpPtr->detected    = moduleInfo.status.detected;
        CTModuleInfoTmpPtr->disabled     = LU_FALSE;
        CTModuleInfoTmpPtr->configXML   = moduleInfo.status.hasConfig;//Deprecated
        CTModuleInfoTmpPtr->unused      = 0x00;
        CTModuleInfoTmpPtr->fsm      = moduleInfo.fsm; //No available
    }
    return CTH_ERROR_NONE;
}


CTHMessage* ConfigurationToolHandler::decodeGetRTUInfo(const CTMessageHeader &header,
                                                const CTMessagePayload &payload
                                                )
{
    if( payload.payloadLen != 0 )
    {
        return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
    }

    /* Compose return message */
    CTMsg_R_RTUInfo* reply = new CTMsg_R_RTUInfo(header.requestID, replyID.newID());
    CTMsgPL_R_RTUInfo *rPayload = reinterpret_cast<CTMsgPL_R_RTUInfo *>(reply->firstEntry());

    /* Get RootFS version values */
    std::string rootFS = getRootFSVersion();
    strncpy(rPayload->revisionRootFS, getRootFSVersion().c_str(), rPayload->rootFSlength);

    /* CPU serial num */
    rPayload->serialCPU = getCPUSerialNum();

    /* TODO: AP - cause of last reset */
//    if(system("last -x") != 0)
//    {
        rPayload->lastReset = CTH_LASTRESET_UNKNOWN;
//    }
//    else
//    {
//        rPayload->lastReset = CTH_LASTRESET_;
//    }

    /* get MCM firmware string */
    memset(rPayload->revisionMCM, 0, (rPayload->revMCMlength));
    strncpy(rPayload->revisionMCM, getMCMUpdaterVersionString().c_str(), rPayload->revMCMlength);

    /* Get Kernel version string */
    memset(rPayload->revisionKernel, 0, (rPayload->revKernellength));
    strncpy(rPayload->revisionKernel, getOSVersion().c_str(), rPayload->revKernellength);

    /* Get RTU site name */
    std::string siteName = DEFAULT_SITENAME;

    /* Set message size */
    reply->setEntries(1);

    /* Append sitename string at the end of the message */
    reply->appendBytes((lu_uint8_t*)siteName.c_str(),
                    (siteName.length()+1)*sizeof(char)/sizeof(lu_uint8_t));

    if(log.isDebugEnabled() == LU_TRUE)
    {
        log.debug("%s Get RTU info: %s", rPayload->toString().c_str());
    }
    return reply;
}


}//namespace CTH


static void getSystemAPI(CTMsgPL_R_SystemAPI &sysAPIPtr)
{
    /* get MCM Module subsystem API */
    sysAPIPtr.MCMModule.systemAPI.major = MODULE_SYSTEM_API_VER_MAJOR;
    sysAPIPtr.MCMModule.systemAPI.minor = MODULE_SYSTEM_API_VER_MINOR;

    /* get config subsystem API */
    sysAPIPtr.configG3.systemAPI.major = G3_CONFIG_SYSTEM_API_MAJOR;
    sysAPIPtr.configG3.systemAPI.minor = G3_CONFIG_SYSTEM_API_MINOR;

    /* get Schema version */
    sysAPIPtr.schema.major = SCHEMA_MAJOR;
    sysAPIPtr.schema.minor = SCHEMA_MINOR;
}

static std::string getCGISocketPath()
{
	std::string cgiSockPath(CGI_SOCKET_NAME);

	#ifdef TARGET_BOARD_G3_SYSTEMPROC
	    {
	    	lu_char_t* lighttpdRoot;
	    	lighttpdRoot = getenv("LIGHTTPD_ROOT");
	    	if(lighttpdRoot == 0)
	    	{
	    		printf("Environment variable: LIGHTTPD_ROOT not found!\n");
	    		exit(-1);
	    	}
	    	cgiSockPath.insert(0, lighttpdRoot) ;
	    }
	#endif

	return cgiSockPath;
}
/*
 *********************** End of file ******************************************
 */
