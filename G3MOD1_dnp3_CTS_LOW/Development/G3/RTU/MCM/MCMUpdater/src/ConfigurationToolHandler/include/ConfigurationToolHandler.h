/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Configuration Tool Handler public interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   02/09/13      wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */
#ifndef _G3_MCM_UPDATER_CONFIGURATIONTOOLHANDLER_H_
#define _G3_MCM_UPDATER_CONFIGURATIONTOOLHANDLER_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "AbstractCTHandler.h"
#include "UpgradeManager.h"
#include "IIOModuleManager.h"
#include "IMCMUpdater.h"
#include "LogReqHandler.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

namespace CTH
{
/**
 * ConfigurationToolHandler(CTH) is for decoding request messages from a client(ConfigTool)
 *  and reply with encoded messages.
 */
class ConfigurationToolHandler: public AbstractCTHandler
{

public:
    ConfigurationToolHandler(IIOModuleManager &moduleMgr,
                                UpgradeManager &upgradeMgr,
                                IMCMUpdater* mcmUpdater
                                );
    virtual ~ConfigurationToolHandler();

    void startService();
    void stopService();

private:
    /**
     * \brief Inherited method. Decode a CT request into a reply message.
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return reply message
     */
    virtual CTHMessage* decodePayload(const CTMessageHeader &header,
                    const CTMessagePayload &payload);
    /**
     * \brief Decode a RTData type message
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return reply message
     */
    CTHMessage* decodeMessageRTUInfo(const CTMessageHeader &header,
                    const CTMessagePayload &payload);

    /**
     * \brief Decode a Users handling type message
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return reply message.
     */
    CTHMessage* decodeMessageLogin(const CTMessageHeader &header,
                    const CTMessagePayload &payload);

    /**
     * \brief Decode an Update Firmware type message
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return reply message.
     */
    CTHMessage* decodeMessageUpgrade(const CTMessageHeader &header,
                    const CTMessagePayload &payload);


    CTHMessage* decodeMessageModuleInfo(const CTMessageHeader &header,
                    const CTMessagePayload &payload);


    /**
     * \brief Starts a module's software Update process.
     *
     * Apply to a given slave module the updates contained in the specified
     * Distribution Package File.
     *
     * \param payload Reference to the message payload
     *
     * \return Error code
     */
    CTHMessage* decodeUpgradeModuleMessage(const CTMessageHeader &header,
                    const CTMessagePayload &payload);

    /**
     * \brief Gets the Upgrade process status.
     *
     * Returns the current state of the upgrade process.
     *
     * \param payload Reference to the message payload
     * \param message Reference to the message to be returned.
     *
     * \return Error code
     */
    CTHMessage* decodeUpgradeStatusMessage(const CTMessageHeader &header,
                    const CTMessagePayload &payload);


    CTHMessage* decodeEraseFirmwareMessage(const CTMessageHeader &header,
                    const CTMessagePayload &payload);


    CTHMessage* decodeMessageRTUControl(const CTMessageHeader &header,
                                        const CTMessagePayload &payload);

    CTH_ERROR getModuleInfo(CTMsg_R_ModuleInfo &info,
                    IIOModuleManager& moduleManager);

    CTHMessage* decodeGetRTUInfo(const CTMessageHeader &header,
                                    const CTMessagePayload &payload
                                    );

    CTHMessage* decodeSetSDPVersion(const CTMessageHeader &header,
                    const CTMessagePayload &payload);

    IIOModuleManager& moduleMgr;

    UpgradeManager& upgradeMgr;

    IMCMUpdater* mcmUpdater;

    LogReqHandler logReqHdl;
    };
}
#endif /* _G3_MCM_UPDATER_CONFIGURATIONTOOLHANDLER_H_ */

/*
 *********************** End of file ******************************************
 */
