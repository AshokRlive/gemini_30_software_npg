/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MCMUpgradeTask.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   23 Sep 2013     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <fcntl.h>
#include <cstdlib>
#include <unistd.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MCMUpgradeTask.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define RUN_UPG_SCRIPT "sh update_mcm.sh"

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
MCMUpgradeTask::MCMUpgradeTask(std::string fwFileName)
                        :fwFileName(fwFileName),
                        fileID(-1),
                        progress(MIN_PROGRESS_VALUE),
                        error(UPG_ERR_NONE),
                        log(Logger::getLogger(SUBSYSTEM_ID_UPGMGR))
{
}
/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
void MCMUpgradeTask::threadBody()
{
    lu_int32_t ret;
    system("echo \"Calling MCM upgrade script...\"");

    std::string shScript = RUN_UPG_SCRIPT;
    shScript +=" ";
    shScript +=fwFileName;
    ret = system(shScript.c_str());

    if (ret == 0)
    {
        log.info("MCM upgrade script SUCCESS.");
        finished(LU_TRUE);
    }
    else
    {
        log.error("MCM upgrade script FAILED: %i",ret);
        if(observer != NULL)
        {
            observer->reportError(UPG_ERR_SYSTEM);
        }
        finished(LU_FALSE);
    }
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void MCMUpgradeTask::finished(lu_bool_t success)
{
    if(success == LU_TRUE && DEL_FWFILE_AFTER_UPG)
    {
        /*Delete firmware file*/
        unlink(fwFileName.c_str());
    }

    if(observer != NULL)
    {
        observer->finished(success);
    }
}
/*
 *********************** End of file ******************************************
 */
