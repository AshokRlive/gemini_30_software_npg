/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MCMUpgradeTask.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   23 Sep 2013     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MCMUPGRADETASK_H_
#define MCMUPGRADETASK_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "UpgradeTask.h"
#include "Logger.h"
#include "MCMIOModule.h"
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */

class MCMUpgradeTask:public UpgradeTask
{
public:
    MCMUpgradeTask(std::string fwFileName);

    virtual ~MCMUpgradeTask()
    {};

    virtual UPG_ERR getError()
    {
        return error;
    }

protected:
    /**
     * \brief Thread main loop
     *
     * This function is called automatically when a thread is started
     */
    virtual void threadBody();

private :
    void finished(lu_bool_t success);

    std::string fwFileName;      //Firmware file name
    lu_int32_t fileID;           //Firmware file descriptor

    lu_int32_t progress;
    UPG_ERR error;

    Logger& log;
};

#endif /* MCMUPGRADETASK_H_ */

/*
 *********************** End of file ******************************************
 */
