/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Handler for a slave module upgrade procedure.
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14/08/12      pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#if !defined(EA_8550901D_715B_4e30_AA5E_ED5A1FA32BF1__INCLUDED_)
#define EA_8550901D_715B_4e30_AA5E_ED5A1FA32BF1__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <string>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "Thread.h"
#include "IIOModuleManager.h"
#include "ModuleProtocolEnum.h"
#include "Logger.h"
#include "UpgradeTask.h"
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Slave modules upgrade procedure.
 *
 * Upgrades an Slave Module using the provided file and CAN Bootloader messages.
 */
class SlaveUpgradeTask : public UpgradeTask
{
public:
    /**
    * \brief private constructor
    */
   SlaveUpgradeTask(IIOModule* module,
               std::string fwFileName,
               ModuleUID moduleUID);

    virtual ~SlaveUpgradeTask();

    virtual UPG_ERR getError()
    {
        return error;
    }

protected:
    /**
     * \brief Thread main loop
     *
     * This function is called automatically when a thread is started
     */
    virtual void threadBody();

private:
    UPG_ERR sendFile();

    void updateProgress(lu_uint8_t newProgress);

    void reportError(UPG_ERR err);

    void finished(lu_bool_t success);

    inline lu_bool_t sendFileBlock(lu_uint16_t blockOffset,lu_uint8_t*blockBuf, lu_uint32_t blockSize);


    IIOModule *slaveModule;      //Slave module board to be upgraded
    std::string fwFileName;      //Firmware file name
    ModuleUID m_moduleUID;        //Expected module serial number for upgrading

    lu_int32_t fileID;           //Firmware file descriptor

    lu_int32_t progress;
    UPG_ERR error;

    Logger& log;

};

#endif // !defined(EA_8550901D_715B_4e30_AA5E_ED5A1FA32BF1__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
