/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Handler for a slave module upgrade procedure.
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14/08/12      pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <cstdio>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "SlaveUpgradeTask.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define FREAD_BUF_SIZE 256    //Size of the file reading block (in bytes).
                              //Must match with the block size in BootloaderFirmwareStr.

#define MODULE_MSG_TIMEOUT 2 //(seconds) Timeout for sending message to module
#define MAX_RETRY_TIMES 3    //Max times of retrying for sending message to module

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
SlaveUpgradeTask::SlaveUpgradeTask(IIOModule* slaveModule,
                                    std::string fwFileName,
                                    ModuleUID moduleUID
                                  ):
                                  slaveModule(slaveModule),
                                  fwFileName(fwFileName),
                                  m_moduleUID(moduleUID),
                                  fileID(-1),
                                  progress(MIN_PROGRESS_VALUE),
                                  error(UPG_ERR_NONE),
                                  log(Logger::getLogger(SUBSYSTEM_ID_UPGMGR))
{
}

SlaveUpgradeTask::~SlaveUpgradeTask()
{
    close(fileID);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void SlaveUpgradeTask::threadBody()
{
    const lu_char_t *FTITLE = "SlaveUpgradeTask::threadBody:";

    UPG_ERR err = UPG_ERR_NONE;

    /*Check if serial number matches: we only check the serial in this case! */
    if(slaveModule->getModuleUID().serialNumber != m_moduleUID.serialNumber)
    {
         log.error( "%s Module %s serial numbers does NOT match (given: %s, module has: %s).",
                     FTITLE,
                     slaveModule->getModuleName(),
                     m_moduleUID.toString().c_str(),
                     slaveModule->getModuleUID().toString().c_str()
                   );
         err = UPG_ERR_MISMATCH_SERIAL;
    }
    /*Check state*/
    else if(slaveModule->isReadyForUpgrade() == LU_FALSE)
    {
        log.error("%s Module %s is NOT ready for upgrading. State: %s",
                   FTITLE,
                   slaveModule->getModuleName(),
                   slaveModule->getStateName().c_str()
                 );
        err = UPG_ERR_INVALID_STATE;
    }
    else
    {
        err = sendFile();
        close(fileID);
    }

    reportError(err);
    if(err == UPG_ERR_NONE)
    {
        log.info("Module %s was upgraded successfully.",
                  slaveModule->getModuleName()
                );
        finished(LU_TRUE);

        // Restart module after programming
        slaveModule -> restart(MD_RESTART_WARM);
    }
    else
    {
        log.error("Module: %s was not upgraded.",
                  slaveModule->getModuleName()
                 );
        finished(LU_FALSE);
    }

}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
UPG_ERR SlaveUpgradeTask::sendFile()
{

    lu_uint16_t blockCount = 0;
    lu_uint8_t buffer[FREAD_BUF_SIZE];
    UPG_ERR err = UPG_ERR_NONE;

    /* Open file*/
    fileID = open(fwFileName.c_str(), O_RDONLY);
    if(fileID < 0)
    {
        log.error( "Error %i opening firmware file: %s",
                    errno,
                    fwFileName.c_str()
                 );
        return UPG_ERR_FILE_READ;
    }

    /* Get file size*/
    const lu_uint32_t fileSize = lseek(fileID, 0, SEEK_END);
    if(fileSize <= 0)
    {
        log.error( "Empty firmware file: %s",
                   fwFileName.c_str()
                 );
        return UPG_ERR_FILE_READ;
    }

    /* Set offset position to the beginning*/
    lseek(fileID, 0, SEEK_SET);

    log.info( "Start programming slave module: %s",slaveModule->getModuleName());

    lu_int32_t readBytes = 0;
    lu_uint32_t filePos = 0;
    /*Sending blocks*/
    while(isInterrupting() == LU_FALSE)
    {
       readBytes = read(fileID, buffer, FREAD_BUF_SIZE);//Read firmware file chunk
       if(readBytes == 0)
       {
           /*Reach file end*/
           log.debug("%s firmware transfer reached file end.",slaveModule->getModuleName());
           err = UPG_ERR_NONE;
           break;
       }
       else if(readBytes < 0)
       {
           /*File read error*/
           err = UPG_ERR_FILE_READ;
           break;
       }

       if(readBytes < FREAD_BUF_SIZE)
       {
           /* Padding the rest to FF's*/
           memset( (buffer + readBytes), 0xFF, (FREAD_BUF_SIZE - readBytes) );
       }


       if(sendFileBlock(blockCount,buffer,FREAD_BUF_SIZE) == LU_FALSE)
       {
           /*Failed to send file block, terminate process*/
           err = UPG_ERR_TRANSMIT;
           break;
       }
       else
       {
           /*Succeeded to send file block*/
           blockCount++;
           filePos += readBytes;  //increase offset
           progress = (filePos * MAX_PROGRESS_VALUE) / fileSize;
           if(progress >= MAX_PROGRESS_VALUE)
           {
               progress = MAX_PROGRESS_VALUE;
           }
           updateProgress(progress);
       }
    }

    return err;
}

inline lu_bool_t SlaveUpgradeTask::sendFileBlock(lu_uint16_t blockOffset,
                                                    lu_uint8_t*blockBuf,
                                                    lu_uint32_t blockSize)
{
     IIOModule::sendReplyStr moduleMsg;
     BootloaderFirmwareStr moduleMsgPL; // Module message payload
     PayloadRAW rawPayload;

     /* Send message */
     lu_int32_t retry = 0;  //CAN message sending retry times
     do
     {
         memcpy(moduleMsgPL.firmwareBlock256, blockBuf, blockSize);
         moduleMsgPL.blockOffset = blockOffset;
         rawPayload.payloadPtr = reinterpret_cast<lu_uint8_t *>(&moduleMsgPL);
         rawPayload.payloadLen = sizeof(BootloaderFirmwareStr);

         /* Prepare message */
         moduleMsg.messageType     = MODULE_MSG_TYPE_BL;
         moduleMsg.messageID       = MODULE_MSG_ID_BL_WRITE_FIRMWARE_C;
         moduleMsg.messagePtr      = &rawPayload;
         moduleMsg.rMessageType    = MODULE_MSG_TYPE_BL;
         moduleMsg.rMessageID      = MODULE_MSG_ID_BL_WRITE_FIRMWARE_R;
         moduleMsg.timeout.tv_sec  = MODULE_MSG_TIMEOUT;
         moduleMsg.timeout.tv_usec = 0;

         IOM_ERROR ret = slaveModule->sendMessage(&moduleMsg);
         if(ret == IOM_ERROR_NONE)
         {
             /*Debug*/
             log.debug(
                             "Send firmware to %s [Retry: %i] - Error %i: %s",
                             slaveModule->getModuleName(),
                             retry,
                             ret, IOM_ERROR_ToSTRING(ret)
                           );

             /*Get replied message payload */
             (moduleMsg.rMessagePtr)->getPayload(&rawPayload);

             if(rawPayload.payloadLen != MODULE_MESSAGE_SIZE(ModStatusReplyStr))
             {
                 /*Wrong payload size*/
                 log.error(
                                 "Error Sending firmware to %s [Retry %i]"
                                 " -Invalid reply payload size: %i, expected %i",
                                 slaveModule->getModuleName(),
                                 retry,
                                 rawPayload.payloadLen,
                                 MODULE_MESSAGE_SIZE(ModStatusReplyStr)
                               );
             }
             else
             {
                 /*Parse payload*/
                 ModStatusReplyStr *replyPtr = reinterpret_cast<ModStatusReplyStr*>(rawPayload.payloadPtr);
                 if(replyPtr->status != REPLY_STATUS_OKAY)
                 {
                     lu_char_t buffer[80];
                     snprintf (buffer, 80,
                                       "Error Sending firmware to %s [Retry %i] - Reply status: %i %s",
                                       slaveModule->getModuleName(),
                                       MAX_RETRY_TIMES + 1 - retry,
                                       replyPtr->status,
                                       (retry > 0)? "Trying again." : "Operation failed.");
                     buffer[79] = '\0'; //Truncate if bigger
                     if(retry >= MAX_RETRY_TIMES)
                         log.error(buffer);
                     else
                         log.warn(buffer);
                 }
                 else
                 {
                     /*Success*/
                     delete (moduleMsg.rMessagePtr);// Release reply message
                     return LU_TRUE;
                 }
             }
             delete (moduleMsg.rMessagePtr);// Release reply message
         }
         else
         {
             log.error("Error Sending firmware writing message to module %s [Retry: %i] - Error %i: %s",
                         slaveModule->getModuleName(),
                         retry, ret, IOM_ERROR_ToSTRING(ret)
                       );
         }

         retry ++;
     } while (retry < MAX_RETRY_TIMES);

     return LU_FALSE;
}


void SlaveUpgradeTask::updateProgress(lu_uint8_t newProgress)
{
    progress = newProgress;

    if(observer != NULL)
    {
        observer->updateProgress(newProgress);
    }
}


void SlaveUpgradeTask::finished(lu_bool_t success)
{
    if(success == LU_TRUE && DEL_FWFILE_AFTER_UPG)
    {
        /*Delete firmware file*/
        unlink(fwFileName.c_str());
    }

    if(observer != NULL)
    {
        observer->finished(success);
    }
}


void SlaveUpgradeTask::reportError(UPG_ERR err)
{
    error = err;

    if(error != UPG_ERR_NONE)
    {
        log.error( "Slave upgrade report error: 0x%X, module:%s",
                      error,
                      slaveModule->getModuleName()
                    );
    }

    if(observer != NULL)
    {
        observer->reportError(err);
    }
}

/*
 *********************** End of file ******************************************
 */
