/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Firmware update for Configuration Tool Handler
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   20/08/12      pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#if !defined(EA_CAAF4B75_BB6E_4992_BE8F_4FB1EC80C65D__INCLUDED_)
#define EA_CAAF4B75_BB6E_4992_BE8F_4FB1EC80C65D__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include "string.h"

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "IIOModuleManager.h"
#include "Logger.h"
#include "UpgradeTask.h"
#include "UpgradeCommon.h"
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
/**
 * \brief Configuration Tool Handler firmware upgrade module.
 *
 * Procedures related to Firmware upgrade via configuration tool.
 */
class UpgradeManager:public UpgradeTaskObserver
{

public:
    /**
     * \brief Custom constructor
     *
     * \param moduleMgr Reference to the module manager object
     */
    UpgradeManager(IIOModuleManager &moduleMgr);

    virtual ~UpgradeManager();

    /**Inherit*/
    virtual void updateProgress(lu_uint8_t newProgress);

    /**Inherit*/
    virtual void reportError(UPG_ERR err);

    /**Inherit*/
    virtual void finished(lu_bool_t success);

    virtual UPG_ERR upgradeModule(const std::string fileName,
                    const IOModuleIDStr mID,
                    const ModuleUID moduleUID,
                    bool waitToFinish = false);

    /**
     * Erase the firmware of a module.
     */
    virtual UPG_ERR eraseModule(IOModuleIDStr mID);


    /**
     * Gets the progress status if it is available.
     * (See G3ConfigProtocol.doc)
     *
     * \return progress value - When the upgrading is in progress and there is no
     *  error, it is a value in [0..100], -1 means there is an error during upgrade.
     *
     */
    lu_uint8_t getProgress();

    /*
     * \brief Gets the current upgrading state.
     */
    UPGRADE_STATE getState();

    /*
     * \brief Checks if any upgrading is in progress.
     */
    lu_bool_t isUpgrading();


private:
    IIOModuleManager &moduleMgr;    //Reference to the module manager
    lu_uint8_t progressValue;       // Upgrade progress
    UPGRADE_STATE upgState;         // Upgrade state
    UpgradeTask* upgTask;           //Upgrade task thread pointer

    Logger& log;         //System log

};

#endif // !defined(EA_CAAF4B75_BB6E_4992_BE8F_4FB1EC80C65D__INCLUDED_)
/*
 *********************** End of file ******************************************
 */
