/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Firmware Upgrade manager implementation for the Configuration Tool
 *       Handler.
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   20/08/12      pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <fcntl.h>
#include <sys/stat.h>
#include <iostream>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "UpgradeManager.h"
#include "Utilities.h"
#include "SlaveUpgradeTask.h"
#include "MCMUpgradeTask.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static UpgradeTask* create(IIOModule* module,
                              std::string fwFileName,
                              ModuleUID moduleUID,
                              UPG_ERR* result
                          );

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

UpgradeManager::UpgradeManager(IIOModuleManager &moduleMgr) :
                                        UpgradeTaskObserver(),
                                        moduleMgr(moduleMgr),
                                        progressValue(0),
                                        upgState(UPGRADE_STATE_IDLE),
                                        upgTask(NULL),
                                        log(Logger::getLogger(SUBSYSTEM_ID_UPGMGR))
{
}

UpgradeManager::~UpgradeManager() {
    delete upgTask;
}


lu_bool_t UpgradeManager::isUpgrading()
{
    return upgState == UPGRADE_STATE_UPGRADING? LU_TRUE :LU_FALSE;
}

lu_uint8_t UpgradeManager::getProgress()
{
    return progressValue;
}

UPGRADE_STATE UpgradeManager::getState()
{
    return upgState;
}


UPG_ERR UpgradeManager::upgradeModule(const std::string fileName,
                                    const IOModuleIDStr mID,
                                    const ModuleUID moduleUID,
                                    bool waitToFinish)
{
    if(isUpgrading())
    {
        return UPG_ERR_IN_PROGRESS;
    }

    if(checkFileExistence(fileName) == LU_FALSE)
    {
        log.error("Firmware file not found:%s",fileName.c_str());
        return UPG_ERR_FILE_READ;
    }

    /*Clearance*/
    if(upgTask != NULL)
    {
        upgTask->detach(this);
        delete upgTask;
        upgTask = NULL;
    }

    IIOModule *module = moduleMgr.getModule(mID.type, mID.id);
    UPG_ERR result;
    upgTask = create(module, fileName, moduleUID, &result);

    if(upgTask == NULL)
    {
        return result;
    }

    /*Start module upgrade thread*/
    upgTask->attach(this);
    upgState = UPGRADE_STATE_UPGRADING;
    THREAD_ERR ret = upgTask ->start();
    if(ret != THREAD_ERR_NONE)
    {
        upgState = UPGRADE_STATE_IDLE;
        log.error("Cannot start upgrading thread.THREAD_ERR:%i",ret);
        return UPG_ERR_SYSTEM;
    }
    else
    {
        if(waitToFinish)
        {
            upgTask->join();
            return upgTask->getError();
        }else {
            return UPG_ERR_NONE;
        }
    }
}


void UpgradeManager::updateProgress(lu_uint8_t newProgress)
{
    log.info("Upgrading progress: %02i%%",newProgress);
    this-> progressValue = newProgress;

}

void UpgradeManager::reportError(UPG_ERR err)
{
    if(err != UPG_ERR_NONE)
        log.error("Reported upgrade error: %i %s",err,__AT__);

    switch(err)
    {
        case UPG_ERR_NONE: break;
        case UPG_ERR_IN_PROGRESS    :  upgState = UPGRADE_STATE_ERR_SYSTEM;    break;
        case UPG_ERR_FILE_READ      :  upgState = UPGRADE_STATE_ERR_FILE;      break;
        case UPG_ERR_NO_MODULE      :  upgState = UPGRADE_STATE_ERR_BOARD;     break;
        case UPG_ERR_MISMATCH_SERIAL:  upgState = UPGRADE_STATE_ERR_BOARD;     break;
        case UPG_ERR_TRANSMIT       :  upgState = UPGRADE_STATE_ERR_SENDING;   break;
        case UPG_ERR_INVALID_STATE  :  upgState = UPGRADE_STATE_ERR_SYSTEM;    break;
        case UPG_ERR_SYSTEM         :  upgState = UPGRADE_STATE_ERR_SYSTEM;    break;
        case UPG_ERR_PARAM          :  upgState = UPGRADE_STATE_ERR_SYSTEM;    break;
        case UPG_ERR_CAN            :  upgState = UPGRADE_STATE_ERR_CAN;       break;
        default                     :  upgState = UPGRADE_STATE_ERR_SYSTEM;    break;
    }
}

void UpgradeManager::finished(lu_bool_t success){
    log.info("Upgrading finished: %s",(success == LU_TRUE)?"succeed":"failed");

    if(success == LU_TRUE)
        this->upgState = UPGRADE_STATE_COMPLETED;
}

UPG_ERR UpgradeManager::eraseModule(IOModuleIDStr mID)
{
    const lu_char_t* mIDStr = mID.toString().c_str();

    UPG_ERR result = UPG_ERR_UNSUPPORTED;

    if(isUpgrading())
    {
        return UPG_ERR_IN_PROGRESS;
    }

    IIOModule* module = moduleMgr.getModule(mID);
    if(module == NULL)
    {
        log.error("Module not found");
        result = UPG_ERR_NO_MODULE;
    }
    else if(module -> isPresent() == LU_FALSE)
    {
        log.error("Module not present on bus:%s",module->getModuleName());
        result = UPG_ERR_NO_MODULE;
    }
    /* Erase a slave module */
    else if(module->isMCM() == LU_FALSE)
    {
       IOM_ERROR ret;
       IIOModule::sendReplyStr message;
       lu_uint32_t timeout;        //timeout for CAN message reply
       PayloadRAW payload;
       payload.payloadPtr = NULL;
       payload.payloadLen = 0;

       /* Prepare message */
       timeout = 2000; //ms of timeout
       message.messageType     = MODULE_MSG_TYPE_BL;
       message.messageID       = MODULE_MSG_ID_BL_ERASE_FIRMWARE_C;
       message.messagePtr      = &payload;
       message.rMessageType    = MODULE_MSG_TYPE_BL;
       message.rMessageID      = MODULE_MSG_ID_BL_ERASE_FIRMWARE_R;
       message.timeout.tv_sec  = 0;
       message.timeout.tv_usec = timeout*1000;

       /* Send message */
       ret = module->sendMessage(&message);

       if(ret == IOM_ERROR_NONE)
       {
           /* Get payload */
           (message.rMessagePtr)->getPayload(&payload);
           if(payload.payloadLen == MODULE_MESSAGE_SIZE(ModStatusReplyStr))
           {
               ModStatusReplyStr *replyPtr = reinterpret_cast<ModStatusReplyStr*>(payload.payloadPtr);
               if(replyPtr->status != REPLY_STATUS_OKAY)
               {
                   log.error("Error Clearing slave module % firmware. "
                               "Command reply status: %i. Operation failed.",
                               mIDStr, replyPtr->status
                               );
                   result = UPG_ERR_SYSTEM;
               }
               else
               {
                   // Successful write
                   log.info("Slave module %s firmware successfully cleared.",mIDStr);

                   // Restart after erase
                   module-> restart(MD_RESTART_WARM);
                   result = UPG_ERR_NONE;
               }
           }
           else
           {
               /* Wrong payload */
               log.error("Error Clearing slave module %s firmware. Invalid reply from slave.",mIDStr);
               result = UPG_ERR_INVALID_STATE;
           }
           /* Release reply message */
           delete (message.rMessagePtr);
       }
       else
       {
           log.error("Error Clearing slave module %s firmware. Error %i: %s.",
                       mIDStr, ret, IOM_ERROR_ToSTRING(ret)
                       );
           result = UPG_ERR_NO_MODULE;
       }
    }
    else
    {
        result = UPG_ERR_UNSUPPORTED;
    }
    return result;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
/**
* Factory method to instantiate UpgradeTask object.
*\return UpgradeTask pointer. NULL if creating process failed.
*/
static UpgradeTask* create(IIOModule* module,
              std::string fwFileName,
              ModuleUID moduleUID,
              UPG_ERR* result)
{
    *result = UPG_ERR_NONE;

    if (module == NULL)
    {
        *result = UPG_ERR_NO_MODULE;
        return NULL;
    }

    if(module->isMCM() == LU_TRUE)
        return new MCMUpgradeTask(fwFileName);
    else
        return new SlaveUpgradeTask(module, fwFileName, moduleUID);
}


/*
 *********************** End of file ******************************************
 */
