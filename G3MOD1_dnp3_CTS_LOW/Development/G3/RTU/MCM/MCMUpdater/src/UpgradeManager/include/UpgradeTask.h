/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:UpgradeTask.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   23 Sep 2013     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef UPGRADETASK_H_
#define UPGRADETASK_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Thread.h"
#include "UpgradeCommon.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define UPGRADE_THREAD_PRIO  Thread::PRIO_MIN + 2 // Thread priority

#define MAX_PROGRESS_VALUE 100 //Max progress value
#define MIN_PROGRESS_VALUE 0   //Min progress value


#define DEL_FWFILE_AFTER_UPG 1  //Delete the firmware file after
                                //the upgrading task is completed successfully.

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Slave modules upgrade handler Observer
 *
 * This class is used by the Slave modules upgrade handler to notify the upper
 * level layer that new data is available.
 */
class UpgradeTaskObserver
{

public:
    UpgradeTaskObserver() {}

    virtual ~UpgradeTaskObserver() {}

    /**
     * \brief Notify the availability of new data
     *
     * \param request Reference to the new data available
     *
     * \return none
     */
    virtual void updateProgress(lu_uint8_t newProgress) = 0;

    virtual void reportError(UPG_ERR err) = 0;

    virtual void finished(lu_bool_t success) = 0;
};


class UpgradeTask : public Thread
{
public :
    UpgradeTask():
        Thread(SCHED_TYPE_FIFO, UPGRADE_THREAD_PRIO, LU_FALSE),
        observer(NULL)
    {}
    virtual ~UpgradeTask()
    {
        observer = NULL;
    }

    virtual UPG_ERR getError() = 0;

    /**
    * \brief Register an observer in the link layer
    *
    *  Only one observer is supported. If an observer is already registered
    *  an error is returned.
    *
    * \param observer Pointer to the observer to register
    *
    * \return Error Code
    */
   virtual UPG_ERR attach(UpgradeTaskObserver *observer)
    {
        /* Check input parameters */
        if (observer == NULL)
        {
            return UPG_ERR_PARAM;
        }

        /* If no observer already registered register the new one */
        this->observer = observer;
        return UPG_ERR_NONE;
    }

   /**
    * \brief Deregister an observer in the link layer
    *
    * \param observer Pointer to the observer to deregister
    *
    * \return Error Code
    */
   virtual UPG_ERR detach(UpgradeTaskObserver *observer)
   {
       /* Check input parameters */
       if(observer == NULL)
       {
           return UPG_ERR_PARAM;
       }

       /* Check if the observer we are de-registering is
        * the same is currently registered
        */
       if(this->observer == observer)
       {
          /* Deregister observer */
           this->observer = NULL;
           return UPG_ERR_NONE;
       }
       else
       {
           return UPG_ERR_PARAM;
       }
   }

protected:
   UpgradeTaskObserver *observer;
};

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */



#endif /* UPGRADETASK_H_ */

/*
 *********************** End of file ******************************************
 */
