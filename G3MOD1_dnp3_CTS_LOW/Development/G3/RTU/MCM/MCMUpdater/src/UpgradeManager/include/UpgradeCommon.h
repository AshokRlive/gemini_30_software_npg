/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:UpgradeCommon.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   18 Sep 2013     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef G3UPDATER_UPGRADECOMMON_H_
#define G3UPDATER_UPGRADECOMMON_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "G3ConfigProtocol.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

typedef enum
{
    UPG_ERR_NONE            = CTH_ACK                       ,
    UPG_ERR_IN_PROGRESS     = CTH_NACK_UPG_IN_PROGRESS      ,
    UPG_ERR_FILE_READ       = CTH_NACK_UPG_FILE_READ        ,
    UPG_ERR_NO_MODULE       = CTH_NACK_UPG_NO_MODULE        ,
    UPG_ERR_MISMATCH_SERIAL = CTH_NACK_UPG_WRONG_SERIAL     ,
    UPG_ERR_INVALID_STATE   = CTH_NACK_UPG_INVALID_STATE    ,
    UPG_ERR_TRANSMIT        = CTH_NACK_UPG_TRANSMIT         ,
    UPG_ERR_PARAM           = CTH_NACK_UPG_PARAM            ,
    UPG_ERR_SYSTEM          = CTH_NACK_SYSTEM               ,
    UPG_ERR_CAN             = CTH_NACK_UPG_CAN              ,
    UPG_ERR_UNSUPPORTED     = CTH_NACK_UNSUPPORTED
}UPG_ERR;


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */



#endif /* G3UPDATER_ */

/*
 *********************** End of file ******************************************
 */
