/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MBModuleSingleThread.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief This file has the functions to create a database,create channels
 *        & sessions, send & process requests and everything the Modbus
 *        master needs to do.
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   9 May 2014     shetty_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


 /*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MBModule.hpp"
#include "bitOperations.h"
#include "timeOperations.h" //lucy_usleep()

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define TMW_LINUX_TARGET    1
#define N0_CONFIG_RECEIVED  0

#define TWO_REGISTERS       2
#define ONE_REGISTER        1

#define readReqStatus       1
#define writeReqStatus      2

#define DEV_OFFLINE         0
#define DEV_ONLINE          1


#define dbPtr (*(DB *)pHandle)
#define ReadReqUserCBParam (*(responseInfoStruct *)(ReadRequestsPTR[Requestindex].pUserCallbackParam))
#define INCREMENT_REQUEST_COUNTER()                                      \
    if(Requestindex >= (NoOfInputChnl-1))                                \
        Requestindex=0;/*reset request index once all requests are sent*/  \
    else                                                                 \
        Requestindex++;//increment this index to send next request in processRequests

#define START_UNSUPPORTED mbm::MSG_TYPE_CMD,mbm::MSG_ID_CMD_START_R ,\
                          mbm::MSG_ERR_UNSUPPORTED
#define START_ACK         mbm::MSG_TYPE_CMD,mbm::MSG_ID_CMD_START_R,\
                          mbm::MSG_ERR_NONE
#define WRT_ACK         mbm::MSG_TYPE_WRITE,mbm::MSG_ID_WRITE_CHANNEL_R, \
                          mbm::WRITE_SUCCESS
#define WRT_FAILURE     mbm::MSG_TYPE_WRITE,mbm::MSG_ID_WRITE_CHANNEL_R, \
                          mbm::WRITE_FAILURE
#define WRT_TIMEOUT     mbm::MSG_TYPE_WRITE,mbm::MSG_ID_WRITE_CHANNEL_R, \
                          mbm::WRITE_TIMEOUT
#define WRT_CANCELLED   mbm::MSG_TYPE_WRITE,mbm::MSG_ID_WRITE_CHANNEL_R, \
                          mbm::WRITE_CANCELLED

//definitions for the serial ports on the MCM
#define RS485  "/dev/ttymxc3"
#define RS232A "/dev/ttymxc4"
#define RS232B "/dev/ttymxc1"
#define USB    "/dev/ttyUSB0"
/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
/* Structure for timing of requests */
typedef struct mRequestInfo
{
  TMWTYPES_MILLISECONDS  interval;
  TMWTYPES_MILLISECONDS  lastRead;
} MY_REQUEST_INFO;


typedef struct{
  TMWCHNL *sclChannelPTR;
  char    *portname;
}sclchannelInfo;


typedef struct responseinfostruct {
  lu_uint16_t devInd;
  lu_uint16_t chnlIndex;// analog or digital channel index
  lu_uint8_t  chnlType;
}responseInfoStruct;

///*structure to hold the device statistics info*/
//typedef struct devicestats {
//    lu_uint64_t respStatusSuccess;
//    lu_uint64_t respStatusFailure;
//    lu_uint64_t respStatusTimeout;
//    lu_uint64_t respStatusCanceled;
//}deviceStats;

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static void initTMW (void);
static void serialDeviceInfo (void);
static lu_bool_t checkChannelCreated (char *);
static void openChnlsSessns (lu_uint16_t);
static void initRequests (void);
static void closeChannelsSessions(void);

static MY_REQUEST_INFO  *initMySession(lu_uint32_t);
static TMWTYPES_BOOL    timeToSendRequest(TMWTYPES_MILLISECONDS *,
                                         TMWTYPES_MILLISECONDS);
static void processRequests(void);
static void updateRegFlagStatus(void * ,lu_uint16_t ,lu_uint16_t ,lu_bool_t);
void updateDevStatus(lu_uint16_t, lu_bool_t);

void readRegCmdStatusCallback(void *pUserCallbackParam, MBCHNL_RESPONSE_INFO *pResponse);
void writeRegCmdStatusCallback(void *pUserCallbackParam, MBCHNL_RESPONSE_INFO *pResponse);


/* Diagnostics functions */
static void readSuccessInfo (void *, MBCHNL_RESPONSE_INFO *);
static void writeSuccessInfo(void *, MBCHNL_RESPONSE_INFO *);
static void dbgSuccessInfo(mbm::ChannelConf&,lu_uint8_t,lu_uint8_t);

void publishSessionStatus(lu_uint16_t);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

mbm::DeviceNum NumOfDevice= {0,0};

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static lu_uint16_t  Requestindex      = 0;//index to requests array
static lu_uint16_t  NoOfSerialChannels= 0;
static lu_uint16_t  ChannelInfoindex  = 0;
static lu_uint16_t  SessionInfoIndex  = 0;//delete if not used

static lu_uint16_t RequestStatusPending = LU_FALSE;
/*primary requests is when requesting for registers/values defined in the register list,
 * secondary requests is for requests received from the command line.
 */
static lu_uint16_t PrimaryRequest = LU_TRUE;
static lu_uint16_t RequestRetryCounter = 0;


/* Context pointers */
static TMWAPPL          *ApplContextPTR = NULL;
static TMWSESN          **SessionsPTR   = NULL;
static sclchannelInfo   *ChannelInfoPTR = NULL;


static mRequestInfo     *ReadReqTimeInfoPTR = NULL;
static MMBBRM_REQ_DESC  *ReadRequestsPTR = NULL;
static MMBBRM_REQ_DESC  *WriteRequestsPTR = NULL;

Status *DevstatusPTR = NULL;
/*StateInverted is a flag which ensures that the if loops are entered only
 * on state change and not everytime updateDevStatus is called.
 */
static lu_bool_t *StateInverted = NULL;

static responseInfoStruct *ReadRespInfoStructPTR = NULL;
static responseInfoStruct *WriteRespInfoStructPTR = NULL;

/*pointer to channelstats for each MB channel*/
struct mbm::devicestats *DeviceStatsStructPTR = NULL;

/* Config structures */
LINIO_CONFIG     IOCnfg;
TMWTARG_CONFIG   targConfig;
TMWPHYS_CONFIG   physConfig;
MBLINK_CONFIG    linkConfig;
MMBSESN_CONFIG   sesnConfig;

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
extern void blockSignals();

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 *   \brief Main thread where the functions to initialize the various context
 *          pointers, count number of serial devices & the number of each of the
 *          types,intialize the timers,initialize the requests & request info,
 *          open channels and sessions,process requests.
 *
 *          The timerchecking & check for any input is also done here.
 *
 *
 *   \param None
 *
 *   \return None
 *
 ******************************************************************************
 */
void * MBMThread(void *arg)
{
    LU_UNUSED(arg);

    blockSignals();

    /* exit if no config is received*/
    if((NumOfDevice.tcpDevNum || NumOfDevice.serialDevNum)== N0_CONFIG_RECEIVED)
    {
        sendResponse(START_UNSUPPORTED);
        LogWarning("S: Server cannot start without config!");
        return(NULL);
    }

    DBG_INFO("S: MBMThread Started,THREAD ID : %lu", (unsigned long)pthread_self());
    sendResponse(START_ACK);
    MbmStartFlag=LU_TRUE;//MbmStartFlag is set to true

    serialDeviceInfo();//count no of serial devices of each type(RS232A,RS232B,
                       //USB,RS485) and open one channel for each

    initTMW(); //init req descriptors,req info,timer,application context

    for (lu_uint16_t devInd = 0; devInd < TotalDevices; devInd++)
    {
        if(!checkChannelCreated(DBptr[devInd].device.conf.serialDevConf.serialconf.portName))
        {
            openChnlsSessns(devInd);
        }
    }

    initRequests();


    while (MbmStartFlag)
    {
        /*
         * Now that everything is set up, start a "main loop" that sends and
         * processes requests.
         */
        processRequests();

        tmwpltmr_checkTimer();

        /* Process any data returned by the Slave.
         * Note that mmbappl_checkForInput will be called
         * many times in between calls to mmbbrm_xxx()*/
        tmwappl_checkForInput(ApplContextPTR);

        if(RequestStatusPending == LU_FALSE && (!ReadCmdReqVector.empty()) )
        {
            readRegCmd();
        }
        if(!WriteCmdReqVector.empty())
        {
            writeRegCmd();
        }
 //       checkSessionStatus();

        lucy_usleep(5000);

    }

    closeChannelsSessions();

    return NULL;
}




/*
 ******************************************************************************
 *   \brief This function is used to count the number of serial devices of each
 *          type & hence the number of channels(1 for each port name).
 *          This info is required to open the corresponding number of channels
 *          & dynamically allocate the memory to ChannelInfoPTR.
 *
 *
 *   \param None
 *
 *   \return None
 *
 ******************************************************************************
 */
void serialDeviceInfo(void)
{
    static lu_uint16_t  noOf485devices    = 0;
    static lu_uint16_t  noOfRS232Adevices = 0;
    static lu_uint16_t  noOfRS232Bdevices = 0;
    static lu_uint16_t  noOfUSBdevices    = 0;

    for(lu_uint16_t devInd = 0; devInd < TotalDevices; devInd++)
    {
        if (strcmp((DBptr[devInd].device.conf.serialDevConf.serialconf.portName),
                        RS485) == 0)
            noOf485devices++;
        if (strcmp((DBptr[devInd].device.conf.serialDevConf.serialconf.portName),
                        RS232A) == 0)
            noOfRS232Adevices++;
        if (strcmp((DBptr[devInd].device.conf.serialDevConf.serialconf.portName),
                        RS232B) == 0)
            noOfRS232Bdevices++;
        if (strcmp((DBptr[devInd].device.conf.serialDevConf.serialconf.portName),
                        USB) == 0)
            noOfUSBdevices++;
    }

    if(noOf485devices)
        NoOfSerialChannels++;
    if(noOfRS232Adevices)
        NoOfSerialChannels++;
    if(noOfRS232Bdevices)
        NoOfSerialChannels++;
    if(noOfUSBdevices)
        NoOfSerialChannels++;

}




/*
 ******************************************************************************
 *   \brief Declare & Initalize the context pointers
 *          Context Pointers:application context,requests,requests info,channels,
 *                           sessions
 *
 *          Initialize MB SCL. This inlcudes:
 *           - initialize polled timer
 *           - initialize application context
 *
 *   \param None
 *
 *   \return None
 *
 ******************************************************************************
 */
void initTMW(void)
{
    /* Context pointers */

    ReadReqTimeInfoPTR  = new mRequestInfo    [NoOfInputChnl];
    ReadRequestsPTR     = new MMBBRM_REQ_DESC [NoOfInputChnl];
    WriteRequestsPTR    = new MMBBRM_REQ_DESC [NoOfOutputChnl];

    ReadRespInfoStructPTR  = new responseInfoStruct [NoOfInputChnl];
    WriteRespInfoStructPTR = new responseInfoStruct [NoOfOutputChnl];

    /*Total Number of channels to be open is 1 for each serial device type
     *  + as many TCP devices since each TCP device needs one channel*/
    ChannelInfoPTR = new sclchannelInfo [NumOfDevice.tcpDevNum + NoOfSerialChannels];
    SessionsPTR    = new TMWSESN* [TotalDevices];

     DevstatusPTR = new Status[TotalDevices] ();
     StateInverted = new lu_bool_t[TotalDevices]();

     DeviceStatsStructPTR = new struct mbm::devicestats[TotalDevices]();

#if (MB_MODULE_RUNNING_MODE == MB_MODULE_IN_PROCESS)
    tmwtimer_initialize();
#endif

    /* Create separate application context*/
    ApplContextPTR = tmwappl_initApplication();



}




/*
 ******************************************************************************
 *   \brief Intialize request descriptors and request info for each request
 *          These are in a linear array.
 *
 *   \param None
 *
 *   \return None
 *
 ******************************************************************************
 */
void initRequests(void)
{

    /* Initialize Request Descriptors for AI,DI & DO channels*/
    for (lu_uint16_t devIndex = 0; devIndex < TotalDevices; devIndex++)
    {
        static lu_uint16_t readRequestindex=0;

        for (lu_uint16_t chlindex = 0;chlindex< DBptr[devIndex].device.aichlNum
                                                                   ; chlindex++)
        {
            mmbbrm_initReqDesc(&ReadRequestsPTR[readRequestindex], SessionsPTR[devIndex]);
            if (chlindex < (DBptr[devIndex].device.aichlNum))
            {
                ReadRespInfoStructPTR[readRequestindex].chnlType = ANALOG_INPUT_CHANNEL;
                ReadRespInfoStructPTR[readRequestindex].chnlIndex = chlindex;
                ReadRespInfoStructPTR[readRequestindex].devInd = devIndex;
            }
            ReadRequestsPTR[readRequestindex].pUserCallback = readSuccessInfo;
            ReadRequestsPTR[readRequestindex].pUserCallbackParam = (void *)&ReadRespInfoStructPTR[readRequestindex];
            readRequestindex++;
        }

        for (lu_uint16_t chlindex = 0;chlindex < DBptr[devIndex].device.dichlNum
                                                                   ; chlindex++)
        {
            mmbbrm_initReqDesc(&ReadRequestsPTR[readRequestindex], SessionsPTR[devIndex]);
            if (chlindex < (DBptr[devIndex].device.dichlNum))
            {
                ReadRespInfoStructPTR[readRequestindex].chnlType = DIGITAL_INPUT_CHANNEL;
                ReadRespInfoStructPTR[readRequestindex].chnlIndex = chlindex;
                ReadRespInfoStructPTR[readRequestindex].devInd = devIndex;
            }
            ReadRequestsPTR[readRequestindex].pUserCallback = readSuccessInfo;
            ReadRequestsPTR[readRequestindex].pUserCallbackParam = (void *)&ReadRespInfoStructPTR[readRequestindex];
            readRequestindex++;
        }

   /* Initialize Request Descriptors for digital output channels*/
        for (lu_uint16_t chlindex = 0;chlindex < DBptr[devIndex].device.dochlNum
                                                               ; chlindex++)
        {
            static lu_uint16_t writeRequestindex=0;
            mmbbrm_initReqDesc(&WriteRequestsPTR[writeRequestindex], SessionsPTR[devIndex]);
            if (chlindex < (DBptr[devIndex].device.dochlNum))
            {
                WriteRespInfoStructPTR[writeRequestindex].chnlType = DIGITAL_OUTPUT_CHANNEL;
                WriteRespInfoStructPTR[writeRequestindex].chnlIndex = chlindex;
                WriteRespInfoStructPTR[writeRequestindex].devInd = devIndex;
            }
            WriteRequestsPTR[writeRequestindex].pUserCallback = writeSuccessInfo;
            WriteRequestsPTR[writeRequestindex].pUserCallbackParam = (void *)&WriteRespInfoStructPTR[writeRequestindex];
            writeRequestindex++;
        }
    }

    /*Initialize Time request info for each request*/
    for (lu_uint16_t devIndex = 0; devIndex < TotalDevices; devIndex++)
    {
        static lu_uint16_t readRequestindex=0;
        for(lu_uint16_t aiInd=0; aiInd<(DBptr[devIndex].device.aichlNum); aiInd++)
        {
            ReadReqTimeInfoPTR[readRequestindex] = *(initMySession(DBptr[devIndex].aiChannel[aiInd].channelconf.pollingRate));
            readRequestindex++;
        }
        for(lu_uint16_t diInd=0; diInd<(DBptr[devIndex].device.dichlNum); diInd++)
        {
            ReadReqTimeInfoPTR[readRequestindex] = *(initMySession(DBptr[devIndex].diChannel[diInd].channelconf.pollingRate));
            readRequestindex++;
        }
    }
}




/*
 ******************************************************************************
 *   \brief Opens Channel for the device being sent as the db_index.
 *          creates one channel for each serial port and scans through the DB
 *          for any devices with the same port name and creates these as sessns.
 *
 *          For a tcp device it opens one channel and one session.
 *
 *   \param index to the device in the DB to create a channel and sessions for.
 *
 *   \return None
 *
 ******************************************************************************
 */
void openChnlsSessns (lu_uint16_t db_index)
{
    if(DBptr[db_index].device.ref.deviceType == mbm::DEVICE_TYPE_SERIAL)
    {
        /* Initialize the target config,channel,IOconfig Structure
         * Call tmwtarg_initConfig,mbchnl_initConfig, liniotarg_initConfig
         * to initialize with default values, then overwrite values as needed.
         */
        tmwtarg_initConfig (&targConfig);
        mbchnl_initConfig (&linkConfig, &physConfig);
        liniotarg_initConfig (&IOCnfg);
        /*
         * Link Conf
         */
        linkConfig.type = (MBLINK_TYPE)DBptr[db_index]
                                     .device.conf.serialDevConf.mblinkconf.type;
        linkConfig.maxQueueSize = DBptr[db_index].device
                                    .conf.serialDevConf.mblinkconf.maxQueueSize;
        linkConfig.rxFrameTimeout = DBptr[db_index].device
                                  .conf.serialDevConf.mblinkconf.rxFrameTimeout;
        linkConfig.channelResponseTimeout = DBptr[db_index].device
                          .conf.serialDevConf.mblinkconf.channelResponseTimeout;

        /*
         * IO Conf
         */
        IOCnfg.type = LINIO_TYPE_232;

        /*All the IOcnfg is filled using the info from the last device cfg received
        since only one channel is configured with various sessions. */
        /* port to open*/
        strcpy(IOCnfg.lin232.portName, DBptr[db_index].device
                                       .conf.serialDevConf.serialconf.portName);

        /* name displayed in analyzer window*/
        strcpy(IOCnfg.lin232.chnlName, DBptr[db_index].device
                                       .conf.serialDevConf.serialconf.chnlName);

        IOCnfg.lin232.baudRate    = (LIN232_BAUD_RATE)DBptr[db_index]
                                 .device.conf.serialDevConf.serialconf.baudRate;
        IOCnfg.lin232.numDataBits = (LIN232_DATA_BITS)DBptr[db_index]
                              .device.conf.serialDevConf.serialconf.numDataBits;
        IOCnfg.lin232.numStopBits = (LIN232_STOP_BITS)DBptr[db_index]
                              .device.conf.serialDevConf.serialconf.numStopBits;
        IOCnfg.lin232.parity      = (LIN232_PARITY)DBptr[db_index]
                                   .device.conf.serialDevConf.serialconf.parity;
        IOCnfg.lin232.portMode    = (LIN232_PORT_MODE)DBptr[db_index]
                                 .device.conf.serialDevConf.serialconf.portMode;
        IOCnfg.lin232.bModbusRTU  = DBptr[db_index]
                               .device.conf.serialDevConf.serialconf.bModbusRTU;
        targConfig.numCharTimesBetweenFrames = 4;/*TBC*/


        /* Open channel*/
        ChannelInfoPTR[ChannelInfoindex].sclChannelPTR =
                   mbchnl_openChannel(ApplContextPTR, TMWDEFS_NULL, TMWDEFS_NULL
                              , &linkConfig, &physConfig, &IOCnfg, &targConfig);

        /* Open port name field just for reference when checking for chnl open*/
        ChannelInfoPTR[ChannelInfoindex].portname = DBptr[db_index].device
                                        .conf.serialDevConf.serialconf.portName;


        /*Initialise and open the Sessions for all serial device on the same
          port with the same port name*/

        for (lu_uint16_t serInd = 0; serInd < TotalDevices; serInd++)
        {
            if(strcmp(DBptr[serInd].device.conf.serialDevConf.serialconf
                        .portName,ChannelInfoPTR[ChannelInfoindex].portname)==0)
            {
                DBG_INFO("S: Opening Serial session %d",SessionInfoIndex);
                mmbsesn_initConfig(&sesnConfig);
                sesnConfig.slaveAddress =
                                (unsigned short)DBptr[serInd].device.conf
                                           .serialDevConf.sesnconf.slaveAddress;
                sesnConfig.defaultResponseTimeout =
                                DBptr[serInd].device.conf.serialDevConf
                                               .sesnconf.defaultResponseTimeout;

                sesnConfig.active = DBptr[serInd].device.active;
                SessionsPTR[serInd] = (TMWSESN *) mmbsesn_openSession
                                (ChannelInfoPTR[ChannelInfoindex].sclChannelPTR,
                                &sesnConfig,(void *)&(DBptr[serInd]));
                SessionsPTR[serInd]->pUserData = malloc(sizeof(lu_uint16_t));
                *((lu_uint16_t *)(SessionsPTR[serInd]->pUserData)) = SessionInfoIndex;
                SessionInfoIndex++;
            }
        }
        ChannelInfoindex++;
    }
    else if(DBptr[db_index].device.ref.deviceType==mbm::DEVICE_TYPE_TCP)
    {
        /* Initialize the target config,channel,IOconfig Structure
         * Call tmwtarg_initConfig,mbchnl_initConfig, liniotarg_initConfig
         * to initialize with default values, then overwrite values as needed.
         */
        tmwtarg_initConfig (&targConfig);
        mbchnl_initConfig (&linkConfig, &physConfig);
        liniotarg_initConfig (&IOCnfg);
        /*
         * Link Conf
         */
        linkConfig.type                   = (MBLINK_TYPE)DBptr[db_index].device
                                            .conf.tcpDevConf.mblinkconf.type;
        linkConfig.maxQueueSize           = DBptr[db_index].device.conf
                                            .tcpDevConf.mblinkconf.maxQueueSize;
        linkConfig.rxFrameTimeout         = DBptr[db_index].device.conf
                                          .tcpDevConf.mblinkconf.rxFrameTimeout;
        linkConfig.channelResponseTimeout = DBptr[db_index].device.conf
                                  .tcpDevConf.mblinkconf.channelResponseTimeout;

        /*
         * IO Conf
         */
        IOCnfg.type = LINIO_TYPE_TCP;

        /* name displayed in analyser window */
        strcpy (IOCnfg.linTCP.chnlName, DBptr[db_index].device.conf.
                                                   tcpDevConf.tcpconf.chnlName);
        /* TCP/IP address of the remote device*/
        strcpy(IOCnfg.linTCP.ipAddress, DBptr[db_index].device.conf.
                                                  tcpDevConf.tcpconf.ipAddress);

        /* The default ipPort for modbus is 502.
         * However for a Linux user-mode server you have to pick a port above 1024:
         * The ports from 1 to 1023 are privileged ports available to root code only.
         */
        IOCnfg.linTCP.ipPort = TMWTYPES_USHORT(DBptr[db_index].device.conf.
                                                     tcpDevConf.tcpconf.ipPort);

        /* Open one channel for each TCP/IP device*/
        ChannelInfoPTR[ChannelInfoindex].sclChannelPTR = mbchnl_openChannel
                           (ApplContextPTR, TMWDEFS_NULL, TMWDEFS_NULL,
                            &linkConfig, &physConfig, &IOCnfg, &targConfig);
        ChannelInfoPTR[ChannelInfoindex].portname=IOCnfg.linTCP.ipAddress;

        /* Initialise and open the Sessions*/
        mmbsesn_initConfig(&sesnConfig);
        sesnConfig.slaveAddress = DBptr[db_index].device.conf
                                              .tcpDevConf.sesnconf.slaveAddress;
        sesnConfig.defaultResponseTimeout = DBptr[db_index].device.conf
                                    .tcpDevConf.sesnconf.defaultResponseTimeout;

        sesnConfig.active = DBptr[db_index].device.active;
        DBG_INFO("S: Opening TCP session %d",SessionInfoIndex);
        SessionsPTR[db_index]=(TMWSESN *)mmbsesn_openSession
                                (ChannelInfoPTR[ChannelInfoindex].sclChannelPTR
                               ,&sesnConfig,(void *)&(DBptr[db_index]));
        SessionsPTR[db_index]->pUserData = malloc(sizeof(lu_uint16_t));
        *((lu_uint16_t *)(SessionsPTR[db_index]->pUserData)) = SessionInfoIndex;
        SessionInfoIndex++;
        ChannelInfoindex++;
    }
}




/*
 ******************************************************************************
 *   \brief  Checks if the channel (referred by the port name parameter) is
 *           open or not. If the port name is not a serial port name it returns
 *           false since a new TCP channel has to be created for each device
 *
 *   \param  pointer to receive the port name
 *
 *   \return true is channel is open
 *           false if channel is not open
 *
 ******************************************************************************
 */
lu_bool_t checkChannelCreated(char * portName)
{
if ((strcmp(portName,RS485)  == 0) ||
    (strcmp(portName,RS232A) == 0) ||
    (strcmp(portName,RS232B) == 0) ||
    (strcmp(portName,USB)    == 0))
{
    lu_bool_t channelStatus=false;
    for (lu_uint16_t chnlInd = 0; chnlInd < ChannelInfoindex; chnlInd++)
    {
        if(strcmp(ChannelInfoPTR[chnlInd].portname,portName)==0)
           channelStatus=LU_TRUE;
    }
    return channelStatus;
}
else
    return false;
}


/*
 ******************************************************************************
 *   \brief  send requests,process any data returned by the slaves.
 *
 *           Sending only one request at a  time , if it is time to send a req &
 *           RequestStatusPending is false.
 *
 *           If reply is successful then readSuccessInfo & UpdateRegCallback
 *           is called ,  if unsuccessful then readSuccessInfo is called.
 *
 *
 *   \param None
 *
 *   \return None
 *
 ******************************************************************************
 */

void processRequests (void)
{
 if( NoOfInputChnl != NULL && (PrimaryRequest == LU_TRUE) )
 {
     if(ReadReqUserCBParam.chnlType == ANALOG_INPUT_CHANNEL)
     {
         /* Check if it's time to send a request & request status is not pending*/
         if ((timeToSendRequest(&ReadReqTimeInfoPTR[Requestindex].lastRead,
                                  ReadReqTimeInfoPTR[Requestindex].interval)) &&
               RequestStatusPending == LU_FALSE &&
               DBptr[ReadReqUserCBParam.devInd].device.active == true)
         {
             RequestStatusPending = LU_TRUE;
             switch(DBptr[ReadReqUserCBParam.devInd].aiChannel
                             [ReadReqUserCBParam.chnlIndex].channelconf.regType)
             {
                 case (mbm::REG_TYPE_COIL_STATUS):
                 mmbbrm_readCoils(&ReadRequestsPTR[Requestindex],
                                  DBptr[ReadReqUserCBParam.devInd].aiChannel
                                  [ReadReqUserCBParam.chnlIndex].channelconf.reg,
                                   ONE_REGISTER);
                 break;
                 case (mbm::REG_TYPE_INPUT_STATUS):
                 mmbbrm_readDiscreteInputs(&ReadRequestsPTR[Requestindex],
                                  DBptr[ReadReqUserCBParam.devInd].aiChannel
                                 [ReadReqUserCBParam.chnlIndex].channelconf.reg,
                                  ONE_REGISTER);
                 break;
                 case (mbm::REG_TYPE_HOLDING_REGISTERS):
                 if((DBptr[ReadReqUserCBParam.devInd].aiChannel
                     [ReadReqUserCBParam.chnlIndex].channelconf.datatype ==
                                               mbm::REG_DATA_TYPE_FLOAT32) ||
                     (DBptr[ReadReqUserCBParam.devInd].aiChannel
                     [ReadReqUserCBParam.chnlIndex].channelconf.datatype ==
                                               mbm::REG_DATA_TYPE_INT32)   ||
                     (DBptr[ReadReqUserCBParam.devInd].aiChannel
                     [ReadReqUserCBParam.chnlIndex].channelconf.datatype ==
                                               mbm::REG_DATA_TYPE_UINT32))
                     mmbbrm_readHoldingRegisters(&ReadRequestsPTR[Requestindex],
                             DBptr[ReadReqUserCBParam.devInd].aiChannel
                             [ReadReqUserCBParam.chnlIndex].channelconf.reg,
                             TWO_REGISTERS);
                 else if((DBptr[ReadReqUserCBParam.devInd].aiChannel
                               [ReadReqUserCBParam.chnlIndex].channelconf.datatype
                                          == mbm::REG_DATA_TYPE_FLOAT16) ||
                         (DBptr[ReadReqUserCBParam.devInd].aiChannel
                               [ReadReqUserCBParam.chnlIndex].channelconf.datatype
                                          == mbm::REG_DATA_TYPE_INT16)    ||
                         (DBptr[ReadReqUserCBParam.devInd].aiChannel
                               [ReadReqUserCBParam.chnlIndex].channelconf.datatype
                                          == mbm::REG_DATA_TYPE_UINT16))
                     mmbbrm_readHoldingRegisters(&ReadRequestsPTR[Requestindex],
                             DBptr[ReadReqUserCBParam.devInd].aiChannel
                             [ReadReqUserCBParam.chnlIndex].channelconf.reg,
                             ONE_REGISTER);
                 break;
                 case (mbm::REG_TYPE_INPUT_REGISTERS):
                 if((DBptr[ReadReqUserCBParam.devInd].aiChannel
                          [ReadReqUserCBParam.chnlIndex].channelconf.datatype ==
                                              mbm::REG_DATA_TYPE_FLOAT32) ||
                    (DBptr[ReadReqUserCBParam.devInd].aiChannel
                          [ReadReqUserCBParam.chnlIndex].channelconf.datatype ==
                                              mbm::REG_DATA_TYPE_INT32)   ||
                    (DBptr[ReadReqUserCBParam.devInd].aiChannel
                          [ReadReqUserCBParam.chnlIndex].channelconf.datatype ==
                                              mbm::REG_DATA_TYPE_UINT32))
                     mmbbrm_readInputRegisters(&ReadRequestsPTR[Requestindex],
                             DBptr[ReadReqUserCBParam.devInd].aiChannel
                             [ReadReqUserCBParam.chnlIndex].channelconf.reg,
                             TWO_REGISTERS);
                 else if((DBptr[ReadReqUserCBParam.devInd].aiChannel
                          [ReadReqUserCBParam.chnlIndex].channelconf.datatype ==
                                              mbm::REG_DATA_TYPE_FLOAT16) ||
                         (DBptr[ReadReqUserCBParam.devInd].aiChannel
                          [ReadReqUserCBParam.chnlIndex].channelconf.datatype ==
                                              mbm::REG_DATA_TYPE_INT16)   ||
                         (DBptr[ReadReqUserCBParam.devInd].aiChannel
                          [ReadReqUserCBParam.chnlIndex].channelconf.datatype ==
                                             mbm::REG_DATA_TYPE_UINT16))
                     mmbbrm_readInputRegisters(&ReadRequestsPTR[Requestindex],
                             DBptr[ReadReqUserCBParam.devInd].aiChannel
                             [ReadReqUserCBParam.chnlIndex].channelconf.reg,
                             ONE_REGISTER);
                 break;
             }
         }
         else if(RequestStatusPending == LU_FALSE)
         {
             INCREMENT_REQUEST_COUNTER();
         }
     }


     else if(ReadReqUserCBParam.chnlType == DIGITAL_INPUT_CHANNEL )
     {
         if((timeToSendRequest(&ReadReqTimeInfoPTR[Requestindex].lastRead,
                            ReadReqTimeInfoPTR[Requestindex].interval)) &&
                            RequestStatusPending == LU_FALSE &&
             DBptr[ReadReqUserCBParam.devInd].device.active == true)
         {
             RequestStatusPending = LU_TRUE;
             switch(DBptr[ReadReqUserCBParam.devInd].diChannel
                          [ReadReqUserCBParam.chnlIndex].channelconf.regType)
             {
             case (mbm::REG_TYPE_COIL_STATUS):
             mmbbrm_readCoils(&ReadRequestsPTR[Requestindex],
                         DBptr[ReadReqUserCBParam.devInd].diChannel
                         [ReadReqUserCBParam.chnlIndex].channelconf.reg,
                         ONE_REGISTER);
             break;
             case (mbm::REG_TYPE_INPUT_STATUS):
             mmbbrm_readDiscreteInputs(&ReadRequestsPTR[Requestindex],
                         DBptr[ReadReqUserCBParam.devInd].diChannel
                         [ReadReqUserCBParam.chnlIndex].channelconf.reg,
                         ONE_REGISTER);
             break;
             case (mbm::REG_TYPE_HOLDING_REGISTERS):
                 mmbbrm_readHoldingRegisters(&ReadRequestsPTR[Requestindex],
                         DBptr[ReadReqUserCBParam.devInd].diChannel
                         [ReadReqUserCBParam.chnlIndex].channelconf.reg,
                         ONE_REGISTER);
             break;
             case (mbm::REG_TYPE_INPUT_REGISTERS):
                 mmbbrm_readInputRegisters(&ReadRequestsPTR[Requestindex],
                         DBptr[ReadReqUserCBParam.devInd].diChannel
                         [ReadReqUserCBParam.chnlIndex].channelconf.reg,
                         ONE_REGISTER);
             break;
             }
         }
         else if(RequestStatusPending == LU_FALSE)
         {
             INCREMENT_REQUEST_COUNTER();
         }
     }
 }
}

/*
 ******************************************************************************
 *   \brief  callback function definition goes here.
 *
 *   \param The SCL calls this function with pHandle as the pointer to the data
 *          base. startAddr is the start address of the registers from which data
 *          is received. Quantity is the number of registers read from.
 *          pValueArray is a pointer to an array containing the readings from the
 *          registers.
 *
 *   \return None
 *
 ******************************************************************************
 */
void UpdateRegCallback( void *pHandle,
                TMWTYPES_USHORT startAddr,
                TMWTYPES_USHORT quantity,
                TMWTYPES_USHORT *pValueArray,
                mbm::REG_TYPE regType)
{
  if(MbmStartFlag && (PrimaryRequest == LU_TRUE))
  {

      /*Increment Requestindex if response is received and change RequestStatusPending
       * flag to false
       */

      RequestStatusPending = LU_FALSE;

      INCREMENT_REQUEST_COUNTER();

      lu_float32_t tempReading;

      /*Scan through all devices and registers to see which register address the
       * response is too.
       */
      for(lu_uint16_t aiInd = 0; aiInd < dbPtr.device.aichlNum; aiInd++)
      {
          if(dbPtr.aiChannel[aiInd].channelconf.reg == startAddr)
          {
              if(dbPtr.aiChannel[aiInd].channelconf.regType == regType)
              {

                  if (quantity==ONE_REGISTER)
                  {
                      if( dbPtr.aiChannel[aiInd].channelconf.datatype == mbm::REG_DATA_TYPE_INT16)
                      {
                          dbPtr.aiChannel[aiInd].currentAnalogRegReading.regValueInt32 =
                                                      (lu_int32_t) (* (lu_int16_t *) (pValueArray));

                      }
                      if( dbPtr.aiChannel[aiInd].channelconf.datatype == mbm::REG_DATA_TYPE_UINT16)
                      {
                          dbPtr.aiChannel[aiInd].currentAnalogRegReading.regValueInt32=
                                                          (lu_int32_t)pValueArray[0];

                      }
                      tempReading = (lu_float32_t)(dbPtr.aiChannel[aiInd].currentAnalogRegReading.regValueInt32);
                  }

                  else if(quantity==TWO_REGISTERS)
                  {
                      dbPtr.aiChannel[aiInd].currentAnalogRegReading.regValueInt32 = 0;

                      dbPtr.aiChannel[aiInd].currentAnalogRegReading.regValueInt32  =
                                                             GetFormattedValue(pHandle,pValueArray);

                      switch( dbPtr.aiChannel[aiInd].channelconf.datatype)
                      {
                      case mbm::REG_DATA_TYPE_FLOAT32 :
                          tempReading = (dbPtr.aiChannel[aiInd].currentAnalogRegReading.regValueFloat32);
                          break;
                      case mbm::REG_DATA_TYPE_UINT32 :
                          tempReading = (lu_float32_t)(dbPtr.aiChannel[aiInd].currentAnalogRegReading.regValueUint32);
                          break;
                      case mbm::REG_DATA_TYPE_INT32  :
                          tempReading = (lu_float32_t)(dbPtr.aiChannel[aiInd].currentAnalogRegReading.regValueInt32);
                          break;
                      default :
                          DBG_ERR("Unsupported register type");
                          break;
                      }
                  }

                  /*Applying scaling and offset*/
                tempReading =  ( tempReading  *
                                              dbPtr.aiChannel[aiInd].channelconf.scalingFactor)+
                                              dbPtr.aiChannel[aiInd].channelconf.offset;


                 dbPtr.aiChannel[aiInd].currentAnalogRegReading.regValueFloat32 = tempReading;


                  if ((dbPtr.aiChannel[aiInd].currentAnalogRegReading.regValueInt32 !=
                                  dbPtr.aiChannel[aiInd].previousAnalogRegReading.regValueInt32)
                      && (dbPtr.aiChannel[aiInd].regUpdateFlag == LU_TRUE))
                  {
                      DBG_INFO("Register value changed from: %d to %d",
                               dbPtr.aiChannel[aiInd].previousAnalogRegReading.regValueInt32,
                               dbPtr.aiChannel[aiInd].currentAnalogRegReading.regValueInt32);

                      /*Publish value*/
                      publishCallbackPTR publishRegPTR=Publish_Value;
                      (*publishRegPTR)(pHandle,ANALOG_INPUT_CHANNEL,aiInd);
                  }

                  else
                  {
                      updateRegFlagStatus((void*)pHandle,aiInd,ANALOG_INPUT_CHANNEL,LU_TRUE);
                  }

                  dbPtr.aiChannel[aiInd].previousAnalogRegReading.regValueInt32 =
                                   dbPtr.aiChannel[aiInd].currentAnalogRegReading.regValueInt32;
              }
          }
      }
      for(lu_uint16_t diInd = 0; diInd < dbPtr.device.dichlNum; diInd++)
      {
          if(dbPtr.diChannel[diInd].channelconf.reg == startAddr)
          {
              if(dbPtr.diChannel[diInd].channelconf.regType == regType)
              {
                  dbPtr.diChannel[diInd].currentDigitalRegReading =
                                  GetBitValue((lu_uint16_t)pValueArray[0],
                                  (lu_uint8_t)dbPtr.diChannel[diInd].channelconf
                                  .regType,(lu_uint16_t)dbPtr.diChannel[diInd]
                                  .channelconf.bitmask);


                  if ((dbPtr.diChannel[diInd].currentDigitalRegReading !=
                                 dbPtr.diChannel[diInd].previousDigitalRegReading)
                      && (dbPtr.diChannel[diInd].regUpdateFlag == LU_TRUE))
                  {


                      DBG_INFO("Register value changed from: %d to %d",
                              dbPtr.diChannel[diInd].previousDigitalRegReading,
                              dbPtr.diChannel[diInd].currentDigitalRegReading);

                      /*Publish value*/
                      publishCallbackPTR publishRegPTR = Publish_Value;
                      (*publishRegPTR)(pHandle, DIGITAL_INPUT_CHANNEL, diInd);
                  }

                  else

                  {
                      updateRegFlagStatus((void*)pHandle,diInd,DIGITAL_INPUT_CHANNEL,LU_TRUE);
                  }

                  dbPtr.diChannel[diInd].previousDigitalRegReading =
                                  dbPtr.diChannel[diInd].currentDigitalRegReading;
              }
          }
      }
  }
}




/*
 ******************************************************************************
 *  initMySession
 *  Initialize session command interval values
 *
 *
 *   \return pointer to intialized request info for each session/device
 *
 ******************************************************************************
 */

MY_REQUEST_INFO * initMySession(lu_uint32_t pollingRate)
{
    mRequestInfo *p;
    p=new mRequestInfo;
    p->interval = pollingRate ;
    p->lastRead = 0;
    return p;
}




/*
 ******************************************************************************
 *   \brief close all sessions and channels
 *
 *   \param
 *
 *   \return pointer to intialized request info for each session/device
 *
 ******************************************************************************
 */
void closeChannelsSessions(void)
{
    /*
      * close all open sessions and channels
      */
     for (lu_uint16_t i = 0; i < TotalDevices; i++)
     {
         free(SessionsPTR[i]->pUserData);
         mmbsesn_closeSession(SessionsPTR[i]);
     }

     for(lu_uint16_t i =0; i < (NumOfDevice.tcpDevNum + NoOfSerialChannels); i++)
     {
         mbchnl_closeChannel(ChannelInfoPTR[i].sclChannelPTR);
     }

}





/*
 ******************************************************************************
 *   \brief Simple diagnostic output function, registered with the Source Code
 *          Library
 *
 *   \param
 *
 *   \return None
 *
 ******************************************************************************
 */


/*
 ******************************************************************************
 *   \brief The following functions are called by the SCL.
 *   This function will be called when request completes.
 *
 *    Increment the request index if request is unsuccessful after 3 retries
 *    Reset the RequestRetryCounter to 0, if request is received or when
 *    DBptr[respInfo.devInd].device.numOfRetries have been reached.
 *
 *    If reply is unsuccessful & if pResponse->status == 1, which
 *    indicates that particular register is not configured correctly/offline
 *    then function INCREMENT_REQUEST_COUNTER() is called.
 *
 *    If reply is unsuccessful & if pResponse->status == 2, which
 *    indicates device is offline then function updateDevStatus
 *    is called.
 *
 *   \param pResponse->status will indicate if request was successful, timed out
 *   or otherwise failed.
 *
 *   \return None
 *
 ******************************************************************************
 */
void readSuccessInfo(void *pUserCallbackParam, MBCHNL_RESPONSE_INFO *pResponse)
{

    responseInfoStruct respInfo = *(responseInfoStruct *)pUserCallbackParam;

    if(pResponse->status == MBCHNL_RESP_STATUS_SUCCESS)
    {
        //increment respStatusSuccess counter
        DeviceStatsStructPTR[*((lu_uint16_t *)(pResponse->pSession->pUserData))].respStatusSuccess++;
        RequestRetryCounter = 0; //reset RequestRetryCounter when valid response
                                 //  is got

       updateDevStatus(respInfo.devInd , DEV_ONLINE);

        if(respInfo.chnlType == ANALOG_INPUT_CHANNEL)
        {
            dbgSuccessInfo(DBptr[respInfo.devInd].aiChannel[respInfo.chnlIndex].channelconf,
                           pResponse->status,readReqStatus);
        }
        else if(respInfo.chnlType == DIGITAL_INPUT_CHANNEL)
        {
            dbgSuccessInfo(DBptr[respInfo.devInd].diChannel[respInfo.chnlIndex].channelconf,
                           pResponse->status,readReqStatus);
        }


      }

    else if((pResponse->status == MBCHNL_RESP_STATUS_FAILURE) ||
            (pResponse->status == MBCHNL_RESP_STATUS_TIMEOUT) ||
            (pResponse->status == MBCHNL_RESP_STATUS_CANCELED))
    {
        /*reset the lastRead time stamp so that the same failed request is sent
          immidiately*/
        if(RequestRetryCounter < DBptr[respInfo.devInd].device.numOfRetries)
            ReadReqTimeInfoPTR[Requestindex].lastRead = 0;

        RequestRetryCounter++;

        RequestStatusPending = LU_FALSE;

        /*Enter this loop only if the number of retries have been completed*/
        if(RequestRetryCounter > DBptr[respInfo.devInd].device.numOfRetries) //send offline after number of retries

        {
            RequestRetryCounter = 0;

            /*if register read fails (return status = 1) then increment request
             * index by 1
             */
            if(pResponse->status == MBCHNL_RESP_STATUS_FAILURE ||
                              pResponse->status == MBCHNL_RESP_STATUS_CANCELED)
            {
                if(pResponse->status == MBCHNL_RESP_STATUS_FAILURE)
                {
                    //increment respStatusFailure counter
                    DeviceStatsStructPTR[*((lu_uint16_t *)(pResponse->pSession->pUserData))].respStatusFailure++;
                }

                if(pResponse->status == MBCHNL_RESP_STATUS_CANCELED)
                {
                    //increment respStatusCanceled counter
                    DeviceStatsStructPTR[*((lu_uint16_t *)(pResponse->pSession->pUserData))].respStatusCanceled++;
                }

                if(respInfo.chnlType == ANALOG_INPUT_CHANNEL)
                {
                    dbgSuccessInfo(DBptr[respInfo.devInd].aiChannel[respInfo.chnlIndex].channelconf,
                                               pResponse->status,readReqStatus);

                    updateRegFlagStatus((void *)&DBptr[respInfo.devInd],
                                    respInfo.chnlIndex,ANALOG_INPUT_CHANNEL,LU_FALSE);
                }
                else if(respInfo.chnlType == DIGITAL_INPUT_CHANNEL)
                {
                    dbgSuccessInfo(DBptr[respInfo.devInd].diChannel[respInfo.chnlIndex].channelconf,
                                               pResponse->status,readReqStatus);

                    updateRegFlagStatus((void *)&DBptr[respInfo.devInd],
                                   respInfo.chnlIndex,DIGITAL_INPUT_CHANNEL,LU_FALSE);
                }

                /*Move to the next request*/
                INCREMENT_REQUEST_COUNTER();
            }

            /*if register read timesout (return status = 2) then send the device ID
             * to the updateDevStatus function so that it can
             * make all the channels/registers in that device offline.
             */
            else if(pResponse->status == MBCHNL_RESP_STATUS_TIMEOUT )
            {
                //increment respStatusTimeout counter
                DeviceStatsStructPTR[*((lu_uint16_t *)(pResponse->pSession->pUserData))].respStatusTimeout++;
                updateDevStatus(respInfo.devInd , DEV_OFFLINE);

                if(respInfo.chnlType == ANALOG_INPUT_CHANNEL)
                {
                    dbgSuccessInfo(DBptr[respInfo.devInd].aiChannel[respInfo.chnlIndex].channelconf,
                                               pResponse->status,readReqStatus);
                }
                else if(respInfo.chnlType == DIGITAL_INPUT_CHANNEL)
                {
                    dbgSuccessInfo(DBptr[respInfo.devInd].diChannel[respInfo.chnlIndex].channelconf,
                                               pResponse->status,readReqStatus);
                }

            }

        }
    }
    publishSessionStatus(respInfo.devInd);
}

/*
 ******************************************************************************
 *   \brief  This function will be called when the write request completes.
 *
 *   \param pResponse->status will indicate if request was successful, timed out
 *   cancelled or otherwise failed.
 *
 *   \return None
 *
 ******************************************************************************
 */
void writeSuccessInfo(void *pUserCallbackParam, MBCHNL_RESPONSE_INFO *pResponse)
{
    responseInfoStruct respInfo = *(responseInfoStruct *)pUserCallbackParam;
    if(pResponse->status == 0)
        sendResponse(WRT_ACK);
    if(pResponse->status == 1)
        sendResponse(WRT_FAILURE);
    if(pResponse->status == 2)
        sendResponse(WRT_TIMEOUT);
    if(pResponse->status == 3)
        sendResponse(WRT_CANCELLED);
    dbgSuccessInfo(DBptr[respInfo.devInd].doChannel[respInfo.chnlIndex]
                  ,pResponse->status,writeReqStatus);
}

/*
 ******************************************************************************
 *   \brief  DBG info for the read and write status
 *
 *   \param  ChannelConf,ReqStatus & reqType(Write or Read Request)
 *
 *   \return None
 *
 ******************************************************************************
 */

void dbgSuccessInfo(mbm::ChannelConf &channelconf,lu_uint8_t reqStatus,lu_uint8_t reqType)
{
    if(reqType == readReqStatus)
    {
        DBG_INFO("S: %s register address : %d ,Read Request Status :%d ",
                        channelconf.channel.toString().c_str(),
                        channelconf.reg,
                        reqStatus);
    }
    else if(reqType == writeReqStatus)
    {
        DBG_INFO("S: %s register address : %d ,Write Request Status :%d",
                        channelconf.channel.toString().c_str(),
                        channelconf.reg,
                        reqStatus);
    }
}



/*
 ******************************************************************************
 *   \brief update the regUpdateFlag (basically invert it) and pulish the value
 *
 *   \param ptr is the pointer to the database,ChnlIndex is the channel index to
 *              the current channel. ChannelType is either the digital or analog
 *              channel.Flagstatus(Register update flag) is true or false.
 *
 *   \return void.
 ******************************************************************************
 */
void updateRegFlagStatus(void *ptr,lu_uint16_t chnlIndex,lu_uint16_t chnlType
                                                        ,lu_bool_t flagStatus)
{
    if(chnlType == ANALOG_INPUT_CHANNEL)
    {
        if(((DB *)ptr)->aiChannel[chnlIndex].regUpdateFlag != flagStatus)
        {
            ((DB *)ptr)->aiChannel[chnlIndex].regUpdateFlag = flagStatus;
            publishCallbackPTR publishRegPTR = Publish_Value;
            (*publishRegPTR)((void *)ptr,ANALOG_INPUT_CHANNEL,chnlIndex);
        }
    }
    else if(chnlType == DIGITAL_INPUT_CHANNEL)
    {
        if(((DB *)ptr)->diChannel[chnlIndex].regUpdateFlag != flagStatus)
        {
            ((DB *)ptr)->diChannel[chnlIndex].regUpdateFlag = flagStatus;
            publishCallbackPTR publishRegPTR=Publish_Value;
            (*publishRegPTR)((void *)ptr,DIGITAL_INPUT_CHANNEL,chnlIndex);
        }
    }
}




/*
 ******************************************************************************
 *   \brief timeToSendRequest : See if it is time to send a request.Note that
 *          this simplified example does not take into account the fact that
 *          the lastTime field could roll over
 *
 *   \param lastTimePTR is the pointer to the last time in the ReadReqTimeInfoPTR
 *          & the interval is the time periods(poll periods) at which the registers
 *          have to be read.
 *
 *   \return true or false
 ******************************************************************************
 */
TMWTYPES_BOOL timeToSendRequest(TMWTYPES_MILLISECONDS *lastTimePTR,
                                                 TMWTYPES_MILLISECONDS interval)
{
    TMWTYPES_MILLISECONDS currentTime;
    TMWTYPES_BOOL returnVal;

    currentTime = tmwtarg_getMSTime();
    if (TMWTYPES_MILLISECONDS (currentTime - *lastTimePTR) >= interval)
    {
        *lastTimePTR = currentTime;
        returnVal = TMWDEFS_TRUE;
    }
    else
    {
        returnVal = TMWDEFS_FALSE;
    }
    return (returnVal);
}


/*
 ******************************************************************************
 *   \brief Calls the getWriteInfo func which fills the writeInfo structure(which
 *          has the index of the WriteRequestsPTR array for the particular write
 *          request & the register address to write to). Once it has this Info
 *          it sends the request by calling the mmbbrm_writeSingleCoil function.
 *
 *   \param
 *
 *   \return true or false (not yet implenmented)
 ******************************************************************************
 */
void SendWriteReq(mbm::ChannelValue channelvalue)
{
     WriteInfo writeinfo;
     writeinfo = getWriteInfo(channelvalue);
     mmbbrm_writeSingleCoil (&WriteRequestsPTR[writeinfo.writeRequestsIndex],
                             writeinfo.registerAddress, channelvalue.chValue);
}


///*
// ******************************************************************************
// *   \brief Calls the getWriteInfo func which fills the writeInfo structure(which
// *          has the index of the WriteRequestsPTR array for the particular write
// *          request & the register address to write to). Once it has this Info
// *          it sends the request by calling the mmbbrm_writeSingleCoil function.
// *
// *   \param
// *
// *   \return true or false (not yet implenmented)
// ******************************************************************************
// */
//void ReplyStats(void)
//{
//    zmq_send()
//    for (lu_uint16_t devID = 0; devID < TotalDevices; devID++)
//    {
//        DBG_INFO("Session[%d] : Read Success : %llu, Failures : %llu, Timeouts : %llu, Cancelled : %llu ",
//                       devID,  DeviceStatsStructPTR[devID].respStatusSuccess,
//                       DeviceStatsStructPTR[devID].respStatusFailure,
//                       DeviceStatsStructPTR[devID].respStatusTimeout,
//                       DeviceStatsStructPTR[devID].respStatusCanceled);
//
//    }
//}

/*
 ******************************************************************************
 *   \brief Changes the polling rate of all the requests in the device to online
 *          or offline polling rate
 *
 *          Updates all the registers in the device to offline.
 *
 *   \param device index which is offline
 ******************************************************************************
 */
void updateDevStatus(lu_uint16_t devInd , lu_bool_t devStatus)
{
//    /*StateInverted is a flag which ensures that the if loops are entered only
//     * on state change and not everytime updateDevStatus is called.
//     */
//    static lu_bool_t *StateInverted = new lu_bool_t[TotalDevices]();

    if(devStatus == DEV_OFFLINE )
    {
        /*set Requestindex to first register index of that particular device,
         * as stored in DevReqIndexPtr array
         */
        Requestindex = DevReqIndexPtr[devInd];
        for(lu_uint16_t channel_index = 0 ;channel_index < DBptr[devInd].device.aichlNum ; channel_index++)
        {
            /*set polling rate to offline polling rate*/
            ReadReqTimeInfoPTR[Requestindex].interval= DBptr[devInd].device.offLinePollingRate;
            ReadReqTimeInfoPTR[Requestindex].lastRead = tmwtarg_getMSTime();
            updateRegFlagStatus((void *)&DBptr[devInd], channel_index ,ANALOG_INPUT_CHANNEL, LU_FALSE);
            INCREMENT_REQUEST_COUNTER();
        }
        for(lu_uint16_t channel_index = 0 ;channel_index < DBptr[devInd].device.dichlNum ; channel_index++)
        {
            /*set polling rate to offline polling rate*/
            ReadReqTimeInfoPTR[Requestindex].interval=DBptr[devInd].device.offLinePollingRate;
            ReadReqTimeInfoPTR[Requestindex].lastRead = tmwtarg_getMSTime();
            updateRegFlagStatus((void *)&DBptr[devInd], channel_index ,DIGITAL_INPUT_CHANNEL, LU_FALSE);
            INCREMENT_REQUEST_COUNTER();
        }
        DevstatusPTR[devInd].statusFlag = LU_FALSE;
        StateInverted[devInd] = true ;
    }
    else if (devStatus == DEV_ONLINE && StateInverted[devInd] == true)
    {
        /*set Requestindex to first register index of that particular device,
         * as stored in DevReqIndexPtr array
         */
        Requestindex = DevReqIndexPtr[devInd];
        for(lu_uint16_t channel_index = 0 ;channel_index < DBptr[devInd].device.aichlNum ; channel_index++)
        {
            /*set polling rate to online polling rate*/
            ReadReqTimeInfoPTR[Requestindex].interval=DBptr[devInd].aiChannel[channel_index].channelconf.pollingRate;
            ReadReqTimeInfoPTR[Requestindex].lastRead = 0;
            INCREMENT_REQUEST_COUNTER();
        }
        for(lu_uint16_t channel_index = 0 ;channel_index < DBptr[devInd].device.dichlNum ; channel_index++)
        {
            /*set polling rate to online polling rate*/
            ReadReqTimeInfoPTR[Requestindex].interval=DBptr[devInd].diChannel[channel_index].channelconf.pollingRate;
            ReadReqTimeInfoPTR[Requestindex].lastRead = 0;
            INCREMENT_REQUEST_COUNTER();
        }
        StateInverted[devInd] = false ;
    }
    else if (devStatus == DEV_ONLINE)
    {
        DevstatusPTR[devInd].statusFlag = LU_TRUE;
    }
}


/*
 ******************************************************************************
 *   \brief publishes status of session/device intially (using 1st if loop) &
 *          and whenever there is a status change it publishes
 *
 *   \param None
 *
 *   \return None
 *
 ******************************************************************************
 */
void publishSessionStatus(lu_uint16_t devID)
{
    /*Channel Index is fixed to 0, because 0th digital channel is used for online
     * offline status
     */
    const lu_uint16_t chnlIndex = 0;
    static lu_bool_t *initialPollFlag = NULL ;//pointer to array of flags to check if its intial
    //poll or not

    //allocate memory only once, hence check for NULL
    if (!initialPollFlag)
    {
        initialPollFlag = new lu_bool_t[TotalDevices];
        for(lu_uint16_t i=0 ; i < TotalDevices ;i++)
        {
            initialPollFlag[i] = LU_TRUE;
        }
    }
    if(initialPollFlag[devID] == LU_TRUE)
    {
        /* in this if loop we check if the device is offline ,we do not
         * publish offline status till the number of retires are not completed.
         * if the device is online we publish immediately and increment the
         * device index.
         */
        if(DevstatusPTR[devID].statusFlag == LU_FALSE && (RequestRetryCounter < DBptr[devID].device.numOfRetries))
        {
            return;
        }
        if(DBptr[devID].device.active == LU_TRUE )
        {
            clock_gettime(CLOCK_REALTIME, &DevstatusPTR[devID].timeStamp);
            DevstatusPTR[devID].deviceID   = DBptr[devID].device.ref.deviceId;
            DevstatusPTR[devID].deviceType = DBptr[devID].device.ref.deviceType;
            publishCallbackPTR publishRegPTR = Publish_Value;
            (*publishRegPTR)((void *)&DevstatusPTR[devID],SESSION_STATUS_CHANNEL,chnlIndex);
            DBG_INFO("S: Intial Session %d Status %d!!!!!!!!!!!!!!!!!!!!",devID,
                            DevstatusPTR[devID].statusFlag);

        }
        initialPollFlag[devID] = LU_FALSE;
    }

    else if(initialPollFlag[devID] == LU_FALSE)
    {
        if (DevstatusPTR[devID].statusFlag != DevstatusPTR[devID].previousStatusFlag)
        {
            clock_gettime(CLOCK_REALTIME, &DevstatusPTR[devID].timeStamp);
            DevstatusPTR[devID].deviceID   = DBptr[devID].device.ref.deviceId;
            DevstatusPTR[devID].deviceType = DBptr[devID].device.ref.deviceType;
            publishCallbackPTR publishRegPTR = Publish_Value;
            (*publishRegPTR)((void *)&DevstatusPTR[devID],SESSION_STATUS_CHANNEL,chnlIndex);
            DBG_INFO("S: Session %d status has changed from %d to %d!!!!!!!!!!!!!!!!!!!!",devID,DevstatusPTR[devID].previousStatusFlag,
                            DevstatusPTR[devID].statusFlag);
        }
        DevstatusPTR[devID].previousStatusFlag = DevstatusPTR[devID].statusFlag;
    }
}


/*********************Following commands are for Support from console**********/

/*
 ******************************************************************************
 *   \brief sends read request(got from console) to the modbus device, If there
 *    is a pending request from the console.
 *
 *   \param None
 *
 *   \return None
 *
 ******************************************************************************
 */
void readRegCmd()
{
    static mbm::devRegInfo devRegInfo;

    {// take mutex to read from global vector
    LockingMutex lmutex(mutex);
    devRegInfo = ReadCmdReqVector.back();
    ReadCmdReqVector.pop_back();
    }

    if (devRegInfo.deviceId > TotalDevices)
    {
        readRegCmdResponse(NULL, mbm::READ_FAILURE);
    }
    else
    {
        if((SessionsPTR[(devRegInfo.deviceId-1)])->active)
        {
            PrimaryRequest = LU_FALSE;
            MMBBRM_REQ_DESC reqdesc;
            mmbbrm_initReqDesc(&reqdesc, SessionsPTR[(devRegInfo.deviceId-1)]);
            reqdesc.pUserCallback = readRegCmdStatusCallback;
            reqdesc.pUserCallbackParam = &devRegInfo;
            switch(devRegInfo.regType)
            {
                case (mbm::REG_TYPE_COIL_STATUS):
                mmbbrm_readCoils(&reqdesc,
                            devRegInfo.startAddress,
                            devRegInfo.quantity);
                break;
                case (mbm::REG_TYPE_INPUT_STATUS):
                mmbbrm_readDiscreteInputs(&reqdesc,
                                devRegInfo.startAddress,
                                devRegInfo.quantity);
                break;
                case (mbm::REG_TYPE_HOLDING_REGISTERS):
                    mmbbrm_readHoldingRegisters(&reqdesc,
                                    devRegInfo.startAddress,
                                    devRegInfo.quantity);
                break;
                case (mbm::REG_TYPE_INPUT_REGISTERS):
                    mmbbrm_readInputRegisters(&reqdesc,
                                    devRegInfo.startAddress,
                                    devRegInfo.quantity);
                break;
                default:
                    DBG_INFO("S: Read of this register type is not supported");
                break;
            }
        }
        else if ( ! ((SessionsPTR[(devRegInfo.deviceId-1)])->active) )
        {
            readRegCmdResponse(NULL, mbm::READ_FAILURE);
        }
    }
}
/*
 ******************************************************************************
 *   \brief callback which is called when the readRegCmd() function sends a register
 *          read request.
 *
 *   \param pointer to parameter(mbm::devRegInfo) sent from readRegCmd() & pointer to the response
 *   info(status) for read request.
 *
 *   \return None
 *
 ******************************************************************************
 */
void readRegCmdStatusCallback (void *pUserCallbackParam, MBCHNL_RESPONSE_INFO *pResponse)
{
    DBG_INFO("S: Response(status) for register read on DeviceID : %d has returned with Status : %d\n",(*(mbm::devRegInfo *)pUserCallbackParam).deviceId, pResponse->status);
    if(pResponse->status != MBCHNL_RESP_STATUS_SUCCESS)
    {
        PrimaryRequest = LU_TRUE;
        readRegCmdResponse(NULL, (mbm::READ_ERR)pResponse->status);
    }
}

/*
 ******************************************************************************
 *   \brief callback which is called when the readRegCmd() successeds
 *
 *   \param The SCL calls this function with pHandle as the pointer to the data
 *          base. startAddr is the start address of the registers from which data
 *          is received. Quantity is the number of registers read from.
 *          pValueArray is a pointer to an array containing the readings from the
 *          registers.
 *
 *   \return None
 *
 ******************************************************************************
 */
void readRegCmdValueCallback( void *pHandle,
                TMWTYPES_USHORT startAddr,
                TMWTYPES_USHORT quantity,
                TMWTYPES_USHORT *pValueArray,
                mbm::REG_TYPE regType)
{
    if(PrimaryRequest == LU_FALSE)
    {
        ((DB *)pHandle)->readRegCmdvalue.clear();
        if( (regType == mbm::REG_TYPE_COIL_STATUS) || (regType == mbm::REG_TYPE_INPUT_STATUS) )
        {
            lu_uint32_t count = 0;//count for incrementing pValueArray index for every 16 count.
            lu_uint32_t countValue = 0 ; // index for valueArray
            for(lu_uint16_t i= 0 ;i<quantity;i++)
            {
                ((DB *)pHandle)->readRegCmdvalue.push_back(LU_GETBIT(pValueArray[countValue],count));
                count++;
                if(count == 16)
                {
                    count = 0;
                    countValue++;
                }
            }
        }
        else if( (regType == mbm::REG_TYPE_HOLDING_REGISTERS) || (regType == mbm::REG_TYPE_INPUT_REGISTERS) )
        {
            for(lu_uint16_t i= 0 ;i<quantity;i++)
            {
            ((DB *)pHandle)->readRegCmdvalue.push_back(pValueArray[i]);
            }
        }

        /*just for debug*/
        DBG_INFO("S: Response(values) for register read on DeviceID :  %d are :",((DB *)pHandle)->device.ref.deviceId);
        for (mbm::registervalues::const_iterator i = ((DB *)pHandle)->readRegCmdvalue.begin(); i != ((DB *)pHandle)->readRegCmdvalue.end(); ++i)
        {
            DBG_INFO("%d",*i);
        }
        readRegCmdResponse(pHandle, mbm::READ_SUCCESS);
        PrimaryRequest = LU_TRUE;
    }
}

/*
 ******************************************************************************
 *   \brief sends write request(got from console) to the modbus device, If there
 *    is a pending request from the console.
 *
 *   \param None
 *
 *   \return None
 *
 ******************************************************************************
 */
void writeRegCmd()
{

    mbm::writeRegValueStr writedevRegInfo;

    // take mutex to read from global vector
    {
        LockingMutex lmutex(mutex);
        writedevRegInfo = WriteCmdReqVector.back();
        WriteCmdReqVector.pop_back();
    }

    if (writedevRegInfo.devreginfo.deviceId > TotalDevices)
    {
        writeRegCmdResponse(mbm::WRITE_FAILURE);
    }
    else
    {
        if((SessionsPTR[(writedevRegInfo.devreginfo.deviceId-1)])->active)
        {
            MMBBRM_REQ_DESC reqdesc;
            mmbbrm_initReqDesc(&reqdesc, SessionsPTR[(writedevRegInfo.devreginfo.deviceId-1)]);
            reqdesc.pUserCallback = writeRegCmdStatusCallback;
            reqdesc.pUserCallbackParam = &writedevRegInfo;
            switch(writedevRegInfo.devreginfo.regType)
            {
                case (mbm::REG_TYPE_COIL_STATUS):
                    if(writedevRegInfo.values.size() == 1)
                    {
                    mmbbrm_writeSingleCoil(&reqdesc,
                            writedevRegInfo.devreginfo.startAddress,
                            writedevRegInfo.values[0]);
                    }
                    else
                    {
                        lu_uint32_t count = 0;//count for incrementing pValueArray index for every 16 count.
                        lu_uint32_t countValue = 0 ; // index for value
                        lu_uint8_t *value = new lu_uint8_t[writedevRegInfo.devreginfo.quantity / 8]();
                        for(lu_uint16_t i= 0 ;i < writedevRegInfo.devreginfo.quantity;i++)
                        {
                            LU_SETBITVALUE(value[countValue], (count),writedevRegInfo.values[i]);
                            count++;
                            if(count == 8)
                            {
                                count = 0;
                                countValue++;
                            }
                        }
                        mmbbrm_writeMultipleCoils(&reqdesc,writedevRegInfo.devreginfo.startAddress,
                                        writedevRegInfo.devreginfo.quantity,value);
                        delete value;
                    }
                break;
                case (mbm::REG_TYPE_HOLDING_REGISTERS):
                    if(writedevRegInfo.values.size() == 1)
                    {
                    mmbbrm_writeSingleRegister(&reqdesc,
                                    writedevRegInfo.devreginfo.startAddress,
                                    writedevRegInfo.values[0]);
                    }
                    else
                    {
                    mmbbrm_writeMultipleRegisters(&reqdesc, writedevRegInfo.devreginfo.startAddress,
                                                   writedevRegInfo.devreginfo.quantity,&writedevRegInfo.values[0]);
                    }
                break;
                default:
                    DBG_INFO("S: Write of this register type is not supported");
                break;
            }
        }
        else if (!(SessionsPTR[writedevRegInfo.devreginfo.deviceId-1])->active )
        {
            writeRegCmdResponse(mbm::WRITE_FAILURE);
        }
    }
}

/*
 ******************************************************************************
 *   \brief callback which is called when the writeRegCmd() function sends a register
 *          write request.
 *
 *   \param pointer to parameter(mbm::devRegInfo) sent from writeRegCmd() & pointer to the response
 *   info(status) for read request.
 *
 *   \return None
 *
 ******************************************************************************
 */
void writeRegCmdStatusCallback(void *pUserCallbackParam, MBCHNL_RESPONSE_INFO *pResponse)
{
    DBG_INFO("S: Response(status) for register write on DeviceID : %d has returned with Status : %d\n",(*(mbm::writeRegValueStr *)pUserCallbackParam).devreginfo.deviceId, pResponse->status);
    switch(pResponse->status)
    {
        case MBCHNL_RESP_STATUS_SUCCESS:
            writeRegCmdResponse(mbm::WRITE_SUCCESS);
        break;
        case MBCHNL_RESP_STATUS_FAILURE:
            writeRegCmdResponse(mbm::WRITE_FAILURE);
        break;
        case MBCHNL_RESP_STATUS_TIMEOUT:
            writeRegCmdResponse(mbm::WRITE_TIMEOUT);
        break;
        case MBCHNL_RESP_STATUS_CANCELED:
            writeRegCmdResponse(mbm::WRITE_CANCELLED);
        break;
        default :
            writeRegCmdResponse(mbm::WRITE_FAILURE);
        break;

    }

}

/*********************** End of file ******************************************
 */
