/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MBMLog.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   8 Aug 2014     shetty_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "MBMLog.hpp"
#include "MBMConfigProto.h"

#define IS_RUNNING_IN_THREAD  (MB_MODULE_RUNNING_MODE == MB_MODULE_IN_THREAD)

#if IS_RUNNING_IN_THREAD
//#include "LogLevelDef.h"
#include "Logger.h"
#endif
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
#if IS_RUNNING_IN_THREAD
static Logger& log = Logger::getLogger(SUBSYSTEM_ID_MODBUS_MASTER) ;
#endif

/*!
 ******************************************************************************
 *   \brief Log Error function
 *          Logs the error for the the config tool & debug.
 *
 *   Detailed description
 *
 *   \param errorMsg string to be printed
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void LogError(const lu_char_t *errorMsg)
{
#if IS_RUNNING_IN_THREAD
    log.error(errorMsg);
#endif
    DBG_ERR("%s",errorMsg);
}

/*!
 ******************************************************************************
 *   \brief Log Error function(Overloaded function)
 *          Logs the error for the the config tool along with a filename,
 *          line no & debug.
 *
 *   Detailed description
 *
 *   \param errorMsg string to be printed
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void LogError(const lu_char_t *errorMsg,lu_uint16_t lineNo,const lu_char_t * fileName)
{
    if((errorMsg != NULL) && (fileName != NULL))
    {
#if IS_RUNNING_IN_THREAD
        log.error("%s at Line : %d in File %s",errorMsg,lineNo,fileName);
#endif
        DBG_ERR("%s",errorMsg);
    }
}



/*!
 ******************************************************************************
 *   \brief Log Warning function
 *
 *   Detailed description
 *
 *   \param warningMsg string to be printed
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void LogWarning(const lu_char_t *warningMsg)
{
#if IS_RUNNING_IN_THREAD
    log.warn(warningMsg);
#endif
    DBG_WARN("%s",warningMsg);
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
