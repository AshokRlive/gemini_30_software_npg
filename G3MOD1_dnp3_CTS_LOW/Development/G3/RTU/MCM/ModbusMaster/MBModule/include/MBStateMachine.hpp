/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MBM_Control.hpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   9 May 2014     shetty_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef MBMCONTROL_HPP_
#define MBMCONTROL_HPP_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <zmq.hpp>
#include <signal.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "MBMServer.hpp"
#include "Semaphore.h"
#include "MBMDebug.hpp"
#include "MBMLog.hpp"
#include "MBModule.hpp"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef enum
{
     MBM_PROCESS_REQUEST=0,
     MBM_APPLY_CONFIG            ,
     MBM_APPLY_COMMAND           ,
     MBM_READ                    ,
     MBM_WRITE                   ,
     MBM_STOPPING                ,
     MBM_STOPPED                 ,
}StateType;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
extern StateType ControlMBM_STATE;

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/**
 * ModBus virtual module thread entry point.
 *
 * \param ctx: ZMQ context.
 */
void* MBModuleEntryPoint(void* ctx);

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */



#endif /* MBMCONTROL_HPP_ */

/*
 *********************** End of file ******************************************
 */
