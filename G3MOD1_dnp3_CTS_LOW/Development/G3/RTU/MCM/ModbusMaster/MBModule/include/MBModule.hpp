/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MBMapplication.hpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   9 May 2014     shetty_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef MBMMODULE_HPP_
#define MBMMODULE_HPP_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

/* Header files for MBMaster*/
extern "C" {
#include "tmwscl/utils/tmwappl.h"
#include "tmwscl/utils/tmwdb.h"
#include "tmwscl/utils/tmwphys.h"
#include "tmwscl/utils/tmwpltmr.h"
#include "tmwscl/utils/tmwtarg.h"
#include "tmwscl/utils/lu_multi_timer.h"

#include "tmwscl/modbus/mbchnl.h"
#include "tmwscl/modbus/mblink.h"
#include "tmwscl/modbus/mmbbrm.h"
#include "tmwscl/modbus/mmbsesn.h"
#include <stdio.h>
#include <signal.h>
#include <sys/time.h>
#include "LinIoTarg/liniotarg.h"
}

#include "MBMConfigProto.h"
#include "MBMServer.hpp"
#include "MBMDB.hpp"
#include "MBMmisc.hpp"
#include "MBMDebug.hpp"
#include "lu_types.h"
#include "MBMdatabase.h"
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define ANALOG_INPUT_CHANNEL      1
#define DIGITAL_INPUT_CHANNEL     2
#define DIGITAL_OUTPUT_CHANNEL    3

#define SESSION_STATUS_CHANNEL     4

/*Used to get the absolute file name*/
#define FILE (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
struct Status
{
public:
    bool statusFlag;
    bool previousStatusFlag;
    struct timespec timeStamp;
    lu_uint8_t deviceID;
    lu_uint8_t deviceType;

public:
    Status() : statusFlag(LU_FALSE),
               previousStatusFlag(LU_FALSE)
    {}
};

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
extern mbm::DeviceNum NumOfDevice;
extern Status *DevstatusPTR;
extern struct mbm::devicestats *DeviceStatsStructPTR;
/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */
void* MBMThread(void *arg);
void SendWriteReq(mbm::ChannelValue);
void readRegCmd();
void writeRegCmd();
//void readRegCmd(mbm::readDevRegInfo& devRegInfo);
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


#endif /* MBMMODULE_HPP_ */

/*
 *********************** End of file ******************************************
 */
