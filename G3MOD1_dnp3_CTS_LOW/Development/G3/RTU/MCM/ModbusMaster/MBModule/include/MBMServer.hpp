/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MBMserver.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   8 May 2014     shetty_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef MBMSERVER_HPP_
#define MBMSERVER_HPP_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <exception>
#include <zmq.h>
#include "LockingMutex.h"
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "MBMConfigProto.h"
#include "MBStateMachine.hpp"
#include "MBModule.hpp"
#include "MBMdatabase.h"
#include "MBMLog.hpp"
#include "MBMDB.hpp"
#include "MBMDebug.hpp"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define CHANNEL_CONFIG 1
#define DEVICE_CONFIG 2

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
/*Flag representing if the MBM master has received the start from the MCM client app*/
 extern lu_bool_t MbmStartFlag;
 extern Mutex mutex;
 extern std::vector<mbm::devRegInfo> ReadCmdReqVector;
 extern std::vector<mbm::writeRegValueStr> WriteCmdReqVector;
/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */
void Publish_Value(void *ptr,lu_uint16_t channelType,lu_uint16_t channelIndex);
typedef void (*publishCallbackPTR)(void *ptr,lu_uint16_t channelType,lu_uint16_t channelIndex);
lu_uint8_t SetServerContext(void* contextPtr);
lu_uint8_t InitReqSocket(void);
lu_uint8_t InitPubSocket(void);
lu_uint8_t CloseServer (void);
void RecvRequest(void);
void HandleCommand(void);
void HandleConfig(void);
void HandleWrite(void);
void HandleRead(void);
void HandleStop(void);
void sendResponse(mbm::MSG_TYPE,mbm::MSG_ID,lu_uint8_t);
void readRegCmdResponse(void *ptr, mbm::READ_ERR  status);
void writeRegCmdResponse (mbm::WRITE_ERR status);
//void create_ChannelDBThread(void);
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


#endif /* MBMSERVER_HPP_ */

/*
 *********************** End of file ******************************************
 */
