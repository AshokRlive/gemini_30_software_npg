/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MBMDB.hpp
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   1 Aug 2014     shetty_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef MBMDB_HPP_
#define MBMDB_HPP_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MBMDebug.hpp"
#include "lu_types.h"
#include "MBMServer.hpp"
#include "MBMConfigProto.h"
#include "MBModule.hpp"
#include "MBMdatabase.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
typedef struct WRITEINFO
{
    lu_uint16_t writeRequestsIndex;
    lu_uint32_t registerAddress;
}WriteInfo;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

extern DB *DBptr; //pointer to the database containing device & channel config
extern lu_uint16_t  TotalDevices;     //Total number of devices
extern lu_uint16_t  NoOfInputChnl;    //total no of input G3 channels
extern lu_uint16_t  NoOfOutputChnl;   //total no of output G3 channels
extern lu_uint16_t *DevReqIndexPtr;   //array containing start request index for
                                      //each device

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */
void PopulateDB(void*,lu_uint8_t);
WriteInfo getWriteInfo(mbm::ChannelValue);
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */

#endif /* MBMDB_HPP_ */

/*
 *********************** End of file ******************************************
 */
