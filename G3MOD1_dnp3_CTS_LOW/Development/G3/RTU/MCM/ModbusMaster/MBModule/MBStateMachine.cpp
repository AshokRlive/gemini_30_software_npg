/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MBMStateMachine.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief InitControl(Initialize intial state) to process requests from
 *              MBM client application.
 *              Control function which changes states between different operating
 *              states
 *              MBMControllerEntryPoint which is equivalent to the main function!
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   9 May 2014     shetty_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MBStateMachine.hpp"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
StateType ControlMBM_STATE;

/* Synchronisation condition shared between Modbus Module and Main App.*/
Semaphore semMBMInit;

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 *   \brief Just intializes the intial state to process requests from the
 *          MCM client
 *
 *   \param None
 *
 *   \return None
 *
 ******************************************************************************
 */

static void InitControl(void)
{
    ControlMBM_STATE=MBM_PROCESS_REQUEST;
}

/*
 ******************************************************************************
 *   \brief  Main Control Thread
 *
 *   Function to process the requests coming in from the MCM client
 *   and change the state
 *
 *     The different states are :
 *     MBM_PROCESS_REQUEST
 *     MBM_APPLY_CONFIG
 *     MBM_APPLY_COMMAND
 *     MBM_POLL
 *     MBM_STOP
 *
 *
 *   \param None
 *
 *   \return None
 *
 *****************************************************************************/


static void Control(void)
{
    switch(ControlMBM_STATE)
    {
        case(MBM_PROCESS_REQUEST):
                        RecvRequest();
        break;
        case(MBM_APPLY_CONFIG):
                        HandleConfig();
        break;
        case(MBM_APPLY_COMMAND):
                        HandleCommand();
        break;
        case(MBM_READ):
                        HandleRead();
        break;
        case(MBM_WRITE):
                        HandleWrite();
        break;
        case(MBM_STOPPING):
                        HandleStop();
        break;

        default:
            ;//
    }

}

/*
 ******************************************************************************
 *   \brief Block signals
 *
 *   \param None
 *
 *   \return None
 *
 ******************************************************************************
 */

void blockSignals()
{
    /*Block all signal*/
    sigset_t signalMask;
    sigfillset(&signalMask);
    pthread_sigmask(SIG_BLOCK, &signalMask, NULL);

    /*Unblock TERM, INT*/
    sigemptyset(&signalMask);
    sigaddset(&signalMask, SIGTERM);
    sigaddset(&signalMask, SIGINT);
    sigprocmask(SIG_UNBLOCK, &signalMask, NULL);
}

/*
 ******************************************************************************
 *   \brief This is effectively the main function which is called to start the
 *          MBM module.
 *
 *   \param None
 *
 *   \return Integer:exit status
 *
 ******************************************************************************
 */
void* MBModuleEntryPoint(void* context)
{
     blockSignals();

     if(SetServerContext(context))
     //Initialise the ZMQ server context for this process
     {
         return (void *)RETURN_FAILURE;
     }

     if(InitReqSocket())//Initialise Req Socket
     {
         return (void *)RETURN_FAILURE;
     }

     if(InitPubSocket())//Initialise publish Socket
     {
         return (void *)RETURN_FAILURE;
     }

     InitControl(); //Initialise the starting state of the MBM control


     /*Signal Main App Modbus Module is ready for communication*/
     semMBMInit.post();

     while(ControlMBM_STATE != MBM_STOPPED)
     {
         Control();
     }

     CloseServer();

     DBG_INFO("S: MB module exit.");

     return (void *)RETURN_SUCCESS;
}

/*
 *********************** End of file ******************************************
 */
