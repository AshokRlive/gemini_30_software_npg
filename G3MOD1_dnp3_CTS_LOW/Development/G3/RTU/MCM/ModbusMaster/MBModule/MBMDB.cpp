/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MBMDB.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   1 Aug 2014     shetty_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "MBMDB.hpp"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define DevConfCast static_cast<mbm::DeviceConf*>(dbPTR)
#define ChnlConfCast static_cast<mbm::ChannelConf*>(dbPTR)

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static void dbgRcvdChnlInfo(mbm::ChannelConf);
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
lu_uint16_t  NoOfOutputChnl  = 0;
lu_uint16_t  NoOfInputChnl   = 0;
lu_uint16_t  *DevReqIndexPtr = 0; // dynamic array pointing to the starting
                                  // request index of each device
lu_uint16_t  TotalDevices    = 0;
DB *DBptr=NULL;
/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static lu_bool_t   DBinit   = false; //flag to show if DB is initialized
WriteInfo writeinfo = { 0,0 };
/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
/*
 ******************************************************************************
 *   \brief Store the device & channel config in the database array. The DB is
 *          constructed in this function.
 *
 *   \param pointer to the device and channel config structures being received
 *          & cnfgType tells if it is device config or channel config
 *
 *   \return None
 *
 ******************************************************************************
 */

void PopulateDB(void *dbPTR,lu_uint8_t cnfgType)
{
    /*Variables for referencing the DB*/
    static lu_uint16_t devInd = 0;  //Index(loop through devices)
    static lu_uint16_t aiInd  = 0;  //analog input channel index
    static lu_uint16_t diInd  = 0;  //digital input channel index
    static lu_uint16_t doInd  = 0;  //digital output channel index

    if(cnfgType == DEVICE_CONFIG)
    {
        if(DBinit == false)
        {
            TotalDevices= NumOfDevice.serialDevNum + NumOfDevice.tcpDevNum;
            DevReqIndexPtr = new lu_uint16_t[TotalDevices];
            DBptr=new DB[TotalDevices];
            DBinit = LU_TRUE; // DB is intialized flag
        }
        else if(DBinit != false)
            devInd++;
        DBptr[devInd].aiChannel = NULL;
        DBptr[devInd].diChannel = NULL;
        DBptr[devInd].doChannel = NULL;

        DBptr[devInd].device    = *(DevConfCast);

        /*decrement number of input channels by 1 since 1st channel by
         * default is online/offline status
         */

        DBptr[devInd].device.dichlNum--;

        DBG_INFO("S: Device config content:\n %s",
                                       DBptr[devInd].device.toString().c_str());
        DBptr[devInd].aiChannel = new MBMDBStr[(DevConfCast->aichlNum)];
        DBptr[devInd].diChannel = new MBMDBStr[(DevConfCast->dichlNum)];
        DBptr[devInd].doChannel = new mbm::ChannelConf[(DevConfCast->dochlNum)];

        /*DevReqIndexPtr contains starting register index for each device*/
        DevReqIndexPtr[devInd]  = NoOfInputChnl;

        NoOfInputChnl = DBptr[devInd].device.aichlNum+DBptr[devInd].device.dichlNum
                                                                 +NoOfInputChnl;
        NoOfOutputChnl = DBptr[devInd].device.dochlNum + NoOfOutputChnl;

        aiInd = 0;
        diInd = 0;
        doInd = 0;
    }
    else if (cnfgType == CHANNEL_CONFIG)
    {
        /*check to see if device exists*/
        if(DBptr)
        {
            /* Check to see if the device ID & device type in the recd channel
             * config, match the latest  device in the DB
             */
            if(ChnlConfCast->channel.device.deviceId ==
                                       DBptr[devInd].device.ref.deviceId &&
                   ChnlConfCast->channel.device.deviceType ==
                                           DBptr[devInd].device.ref.deviceType)
            {

                if(ChnlConfCast->channel.channelType==mbm::MBDEVICE_CHANNEL_AI)
                {
                    dbgRcvdChnlInfo(*ChnlConfCast);//print rcvd channel config
                    DBptr[devInd].aiChannel[aiInd].channelconf=*(ChnlConfCast);
                    aiInd++;
                }
                if(ChnlConfCast->channel.channelType==mbm::MBDEVICE_CHANNEL_DI
                                && ChnlConfCast->channel.channelId != 0)
                {
                    dbgRcvdChnlInfo(*ChnlConfCast);//print rcvd channel config
                    DBptr[devInd].diChannel[diInd].channelconf=*(ChnlConfCast);
                    diInd++;
                }
                if(ChnlConfCast->channel.channelType==mbm::MBDEVICE_CHANNEL_DO)
                {
                    dbgRcvdChnlInfo(*ChnlConfCast);//print rcvd channel config
                    DBptr[devInd].doChannel[doInd]=*(ChnlConfCast);
                    doInd++;
                }

            }
            else
            {
                DBG_ERR("S: Config received for wrong device type/device ID");
            }
        }
        else
        {
            DBG_ERR("S: Channel config received without any device config");
        }
    }
}

/*
 ******************************************************************************
 *   \brief Determines & fills the writeinfo structure from the channelvalue
 *          structure fields. The writeinfo structure contains the
 *          writerequestindex and the register address for the write request.
 *
 *   \param ChannelValue
 *
 *   \return writeInfo structure
 *
 ******************************************************************************
 */
WriteInfo getWriteInfo(mbm::ChannelValue channelvalue)
{
    writeinfo.writeRequestsIndex = 0;
    for(lu_uint8_t devInd=0; devInd < TotalDevices; devInd++)
    {
       if( DBptr[devInd].device.ref.deviceId !=
                                             channelvalue.chRef.device.deviceId)
           writeinfo.writeRequestsIndex = DBptr[devInd].device.dochlNum +
                                                   writeinfo.writeRequestsIndex;
       else if( DBptr[devInd].device.ref.deviceId ==
                                             channelvalue.chRef.device.deviceId)
       {
           writeinfo.writeRequestsIndex = channelvalue.chRef.channelId +
                                                  writeinfo.writeRequestsIndex;
           writeinfo.registerAddress = DBptr[devInd].
                                    doChannel[channelvalue.chRef.channelId].reg;
           break;
       }
    }
    return writeinfo;
}



/*
 ******************************************************************************
 *   \brief prints out the recevied channel config
 *
 *   \param sends the channel name
 *
 *   \return
 ******************************************************************************
 */
void dbgRcvdChnlInfo(mbm::ChannelConf channelconf)
{
    DBG_INFO("S: receive channel config. %s",channelconf.toString().c_str());
    LU_UNUSED(channelconf);
}



/*
 *********************** End of file ******************************************
 */
