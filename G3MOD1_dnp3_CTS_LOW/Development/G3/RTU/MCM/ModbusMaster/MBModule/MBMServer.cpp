/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MBMserver.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief All the communication (using ZMQ) which includes preparing the
 *              context pointers,sockets(reply & request), processing requests
 *              from MCM clients, changing states based on incoming requests &
 *              send appropriate responses is all done in this file.
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   8 May 2014     shetty_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "MBMServer.hpp"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*Definition for the number of bytes after which the requests being read from
  the MCM client using zmq_recv,shall be truncated. 1000 bytes is selected
  as a safe value since the MAX message length received in our application is
  around 600 bytes(from the the device and channel config)*/
#define MAX_CONFIG_MSG_LENGTH 1000

#define ZMQ_MSG_INIT_SUCCESS   0
#define ZMQ_BLOCKING_MODE      0
#define ZMQ_RECVMSG_FAIL      -1
#define ZMQ_GETSOCKOPT_SUCCESS 0
#define ZMQ_SUCCESS            0

#define dbPtr (*(DB *)ptr)
#define PING_CHANNEL_ID        0

/*ZMQ response macros*/
#define PING_ACK               mbm::MSG_TYPE_CMD,mbm::MSG_ID_CMD_PING_R,      \
                               mbm::MSG_ERR_NONE
#define START_NACK             mbm::MSG_TYPE_CMD,mbm::MSG_ID_CMD_START_R,     \
                               mbm::MSG_ERR_UNSUPPORTED
#define STOP_ACK               mbm::MSG_TYPE_CMD,mbm::MSG_ID_CMD_STOP_R,      \
                               mbm::MSG_ERR_NONE
#define DEVICE_NUMBER_ACK      mbm::MSG_TYPE_CFG,mbm::MSG_ID_CFG_DEVICE_NUM_R,\
                               mbm::MSG_ERR_NONE
#define CHANNEL_CFG_ACK        mbm::MSG_TYPE_CFG,mbm::MSG_ID_CFG_CHANNEL_R,   \
                               mbm::MSG_ERR_NONE
#define CHANNEL_CFG_NACK       mbm::MSG_TYPE_CFG,mbm::MSG_ID_CFG_CHANNEL_R ,  \
                               mbm::MSG_ERR_UNSUPPORTED
#define DEVICE_CFG_ACK         mbm::MSG_TYPE_CFG,mbm::MSG_ID_CFG_DEVICE_R,    \
                               mbm::MSG_ERR_NONE
#define DEVICE_CFG_NACK        mbm::MSG_TYPE_CFG,mbm::MSG_ID_CFG_DEVICE_R ,   \
                               mbm::MSG_ERR_UNSUPPORTED
#define INVALID_ACK            mbm::MSG_TYPE_INVALID,mbm::MSG_ID_INVALID,     \
                               mbm::MSG_ERR_NONE
#define WRITE_ACK              mbm::MSG_TYPE_WRITE,mbm::MSG_ID_WRITE_CHANNEL_R, \
                               mbm::MSG_ERR_NONE
#define READSTATS_NACK         mbm::MSG_TYPE_READ,mbm::MSG_ID_READ_STATS_R,   \
                               mbm::MSG_ERR_UNSUPPORTED


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
 static void clearPendingMsgQueue(void);

 /*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static mbm::MsgHeader MsgHeader; //Structure contains the request received
                                 //from the client
static pthread_t channelDBID;

/*ZMQ pointers to context ,reply(responder) & publish sockets*/
void *ContextPtr   = NULL;
void *ResponderPtr = NULL;
void *PublisherPtr = NULL;

/*Flag (Global) indicating if the MBM master has received the start message from
  the MCM client*/
lu_bool_t MbmStartFlag=false;

Mutex mutex;
std::vector<mbm::devRegInfo> ReadCmdReqVector;
std::vector<mbm::writeRegValueStr> WriteCmdReqVector;
/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


 /*
 ******************************************************************************
 *   \brief Prepare our *ContextPtr
 *
 *
 *   \param handle from the main function to the newly created context if
 *          successful
 *
 *   \return RETURN_SUCCESS(0) if context passed is valid
 *           RETURN_FAILURE(1) if context passed is NULL
 *
 ******************************************************************************
 */

lu_uint8_t SetServerContext(void* contextPtr)
{
    if(contextPtr == NULL)
    {
        LogError("Context received is NULL");
        return RETURN_FAILURE;
    }
    ContextPtr = contextPtr;
    DBG_INFO("MBModule context init: %p",contextPtr);
    return RETURN_SUCCESS;
}


/*!
 ******************************************************************************
 *   \brief Prepare the Reply/request Socket
 *
 *
 *   \param
 *
 *   \return RETURN_SUCCESS(0) if request socket is created
 *           RETURN_FAILURE(1) if publish socket is not created
 *
 ******************************************************************************
 */

lu_uint8_t InitReqSocket(void)
{
    ResponderPtr  = zmq_socket (ContextPtr, ZMQ_REP);
    lu_uint8_t rc = zmq_bind (ResponderPtr, MB_HOST_ADDRESS_REP);
    if (rc != ZMQ_SUCCESS)
    {
        LogError("REQ Socket creation Failed");
        return(RETURN_FAILURE);
    }
    DBG_INFO("S: Request server running:%s",MB_HOST_ADDRESS_REP);
    return RETURN_SUCCESS;
}


/*!
 ******************************************************************************
 *   \brief Prepare the publish Socket
 *
 *   \Detailed description
 *
 *   \param None
 *
 *   \return RETURN_SUCCESS(0) if publish socket is created
 *           RETURN_FAILURE(1) if publish socket is not created
 *
 ******************************************************************************
 */

lu_uint8_t InitPubSocket(void)
{
    PublisherPtr = zmq_socket (ContextPtr, ZMQ_PUB);
    lu_uint8_t rc = zmq_bind (PublisherPtr, MB_HOST_ADDRESS_SUB);
    if (rc != ZMQ_SUCCESS)
    {
            LogError("PUB Socket creation Failed");
            return(RETURN_FAILURE);
    }
    DBG_INFO("S: Publish server running:%s",MB_HOST_ADDRESS_SUB);
    return RETURN_SUCCESS;
}


/*!
 ******************************************************************************
 *   \brief close the ZMQ sockets and context
 *          check if the sockets and context exists before closing them
 *
 *   \Detailed description
 *
 *   \param None
 *
 *   \return RETURN_SUCCESS(0) if sockets are closed
 *           RETURN_FAILURE(1) if sockets close fail
 *
 ******************************************************************************
 */

lu_uint8_t CloseServer(void)
{
    lu_uint8_t returnValue;

    DBG_INFO("S: Closing MB Module server...");
    if(PublisherPtr)
    {
        lu_uint8_t rc = zmq_close(PublisherPtr);
        if(rc != ZMQ_SUCCESS)
        {
            LogError("Failed to close PUB socket");
            returnValue = RETURN_FAILURE;
        }
        else if(rc == ZMQ_SUCCESS)
        {
            DBG_INFO("S: Publish socket closed");
            returnValue = RETURN_SUCCESS;
        }
        PublisherPtr=NULL;
    }

    if(ResponderPtr)
    {
        lu_uint8_t rc = zmq_close(ResponderPtr);
        if(rc != ZMQ_SUCCESS)
        {
            LogError("Failed to close REQ socket");
            returnValue = RETURN_FAILURE;
        }
        else if(rc == ZMQ_SUCCESS)
        {
            DBG_INFO("S: Request socket closed");
            returnValue = RETURN_SUCCESS;
        }
        ResponderPtr=NULL;
    }
    return returnValue;
}



/*!
 ******************************************************************************
 *   \brief Recieve the request from the client and changes the state based
 *          on the type field of the message header.
 *
 *   Note : zmq_recv is a blocking fucntion
 *
 *   \param None
 *
 *   \return None
 *
 ******************************************************************************
 */

void RecvRequest(void)
{
    DBG_INFO("S: Server waiting for request");
    lu_int32_t nbytes = 0;
    zmq_msg_t request;

    do
    {
        nbytes = zmq_msg_init (&request);
        if (nbytes != ZMQ_MSG_INIT_SUCCESS)
        {
            LogError ("S: ZMQ Error: Msg Init failed",__LINE__,FILE);
        }
        /*Block until a message is available to be received from socket*/
        nbytes = zmq_msg_recv (&request, ResponderPtr, ZMQ_BLOCKING_MODE);
    } while (nbytes == ZMQ_RECVMSG_FAIL);
    memcpy(&MsgHeader, zmq_msg_data (&request), nbytes);

    switch (MsgHeader.type)
    {
        case  mbm::MSG_TYPE_CMD  :
            DBG_INFO("S: Received Command : %d",MsgHeader.type);
            ControlMBM_STATE=MBM_APPLY_COMMAND;
        break;
        case  mbm::MSG_TYPE_CFG  :
            DBG_INFO("S: Received Config : %d",MsgHeader.type);
            ControlMBM_STATE=MBM_APPLY_CONFIG;
        break;
        case  mbm::MSG_TYPE_READ:
            DBG_INFO("S: Received Read : %d",MsgHeader.type);
            ControlMBM_STATE=MBM_READ; // TODO TBD
        break;
        case  mbm::MSG_TYPE_WRITE:
            DBG_INFO("S: Received Write : %d",MsgHeader.type);
            ControlMBM_STATE=MBM_WRITE; // TODO TBD
        break;
        case  mbm::MSG_TYPE_EVENT :
            DBG_INFO("S: Received Event : %d",MsgHeader.type);
        break;
        case  mbm::MSG_TYPE_INVALID :
            DBG_INFO("S: Received null : %d",MsgHeader.type);
        break;
        default:
            LogError ("Invalid Request",__LINE__,FILE);
            clearPendingMsgQueue();
            ControlMBM_STATE=MBM_PROCESS_REQUEST;
        break;
    }
}


/*!
 ******************************************************************************
 *   \brief Used to check the id field of the msgheader, take suitable action,
 *          send the appropriate response and go back to initial receiving
 *          request state
 *
 *   \param None
 *
 *   \return None
 *
 ******************************************************************************
 */

void HandleCommand(void)
{
    switch (MsgHeader.id)
    {
        case  mbm::MSG_ID_CMD_PING_C  :
            DBG_INFO("S: Received Ping");
            sendResponse(PING_ACK);
            ControlMBM_STATE=MBM_PROCESS_REQUEST;
        break;
        case  mbm::MSG_ID_CMD_START_C  :
            DBG_INFO("S: Received Start");
            if(!MbmStartFlag)
                pthread_create(&channelDBID,NULL,&MBMThread,&MbmStartFlag);
            else
            /*error since MBM is already started*/
            {
                DBG_INFO("S: Multiple start messages received!!!");
                sendResponse(START_NACK);
            }
            ControlMBM_STATE=MBM_PROCESS_REQUEST;
         break;
         case  mbm::MSG_ID_CMD_STOP_C :
             DBG_INFO("S: Received stop command: %d", MsgHeader.id);
             ControlMBM_STATE=MBM_STOPPING;
         break;
         default:
             LogError ("Invalid Command",__LINE__,FILE);
             clearPendingMsgQueue();
             ControlMBM_STATE=MBM_PROCESS_REQUEST;
         break;
    }
}


/******************************************************************************
 *   \brief Receive the config structures from the MCM client and call the
 *          PopulateDB function from MCMapplication.cpp to apply the received
 *          config
 *          The number of devices (serial and tcp devices) is also got from the
 *          config sent from the MCM client and applied.
 *
 *   \param
 *
 *   \return None
 *
 ******************************************************************************
 */

void HandleConfig(void)
{
    mbm::ChannelConf channelcfg;
    mbm::DeviceConf devicecfg;
    zmq_msg_t request;
    /*Initalize the request message struct*/
    size_t size = MAX_CONFIG_MSG_LENGTH;

    zmq_msg_init_size (&request, size);
    lu_int16_t nbytes = zmq_msg_init (&request);
    if (nbytes != ZMQ_MSG_INIT_SUCCESS)
    {
        LogError ("S: ZMQ Error: Msg Init failed",__LINE__,FILE);
    }

    /*Block until a message is available to be received from socket*/
    nbytes = zmq_msg_recv (&request, ResponderPtr, ZMQ_BLOCKING_MODE);
    if(nbytes == ZMQ_RECVMSG_FAIL)
    {
        LogError ("S: ZMQ Error: Failed to receive msg",__LINE__,FILE);
    }
    switch (MsgHeader.id)
    {
        case mbm::MSG_ID_CFG_DEVICE_NUM_C :
            DBG_INFO("S: Received no of devices config info");
            memcpy(&NumOfDevice, zmq_msg_data (&request), nbytes);
            sendResponse(DEVICE_NUMBER_ACK);
            ControlMBM_STATE = MBM_PROCESS_REQUEST;
        break;
        case mbm::MSG_ID_CFG_CHANNEL_C :
            if(!MbmStartFlag)
            {
                DBG_INFO("S: Received Channel cfg");
                memcpy(&channelcfg, zmq_msg_data(&request), nbytes);
                sendResponse(CHANNEL_CFG_ACK);
                PopulateDB(&channelcfg, CHANNEL_CONFIG);//save Channel config in
            }                                           //  DB
            else if(MbmStartFlag)
            {
                DBG_ERR("S: MBM already configured!");
                sendResponse(CHANNEL_CFG_NACK);
            }
            ControlMBM_STATE = MBM_PROCESS_REQUEST;
        break;
        case mbm::MSG_ID_CFG_DEVICE_C   :
            if(!MbmStartFlag)
            {
                DBG_INFO("S: Received Device cfg");
                memcpy(&devicecfg, zmq_msg_data(&request), nbytes);
                sendResponse(DEVICE_CFG_ACK);
                PopulateDB(&devicecfg, DEVICE_CONFIG);//save dev config in DB
            }
            else if(MbmStartFlag)
            {
                DBG_ERR("S: MBM already configured!");
                sendResponse(DEVICE_CFG_NACK);
            }
            ControlMBM_STATE = MBM_PROCESS_REQUEST;
        break;
        default:
            LogError ("Invalid Config",__LINE__,FILE);
            clearPendingMsgQueue();
            ControlMBM_STATE = MBM_PROCESS_REQUEST;
        break;
    }
    zmq_msg_close (&request);
}


/*!
 ******************************************************************************
 *   \brief TBC,not yet implemented
 *
 *   \param None
 *
 *   \return None
 *
 *******************************************************************************
 */

void HandleRead(void)
{
    switch(MsgHeader.id)
    {
        case mbm::MSG_ID_READ_CHANNEL_C:
        {
             mbm::ChannelRef Channelref;
             zmq_msg_t request;
             lu_int32_t nbytes = zmq_msg_init (&request);
             assert (nbytes == ZMQ_MSG_INIT_SUCCESS);
             //Block until a message is available to be received from socket
             nbytes = zmq_msg_recv (&request, ResponderPtr, ZMQ_BLOCKING_MODE);
             assert (nbytes != -1);
             memcpy(&Channelref, zmq_msg_data(&request), nbytes);
             DBG_INFO("S: Channel Poll recieved for device ID : %d & channel ID : %d",
                              Channelref.device.deviceId, Channelref.channelId);
             zmq_msg_close (&request);
             ControlMBM_STATE = MBM_PROCESS_REQUEST;
        }
        break;
        case mbm::MSG_ID_READ_STATS_C:
        {
          if( DeviceStatsStructPTR != NULL)
          {
                lu_uint8_t devID = 1;//init to 1 since this is the min devID and wont cause
                                        //problems accessing the arrays below.
                zmq_msg_t request;
                lu_int32_t nbytes = zmq_msg_init (&request);
                mbm::MsgHeader replyMsgHeader(mbm::MSG_TYPE_READ,mbm::MSG_ID_READ_STATS_R);

                if (nbytes != ZMQ_MSG_INIT_SUCCESS)
                {
                    LogError ("S: ZMQ Error: Msg Init failed",__LINE__,FILE);
                }
                //Block until a message is available to be received from socket
                nbytes = zmq_msg_recv (&request, ResponderPtr, ZMQ_BLOCKING_MODE);
                if (nbytes == ZMQ_RECVMSG_FAIL)
                {
                    LogError ("S: ZMQ Error: Failed to receive msg",__LINE__,FILE);
                }
                memcpy(&devID, zmq_msg_data (&request), nbytes);

                DBG_INFO("S: Read Stats request received for device : %d",devID);

                /*just to make sure we dont access the DeviceStatsStructPTR out of bounds*/
                if (devID <= TotalDevices)
                {
                DBG_INFO("S: Session[%d] : Success : %llu, Failures : %llu, Timeouts : %llu, Cancelled : %llu ",
                               devID,  DeviceStatsStructPTR[devID-1].respStatusSuccess,
                               DeviceStatsStructPTR[devID-1].respStatusFailure,
                               DeviceStatsStructPTR[devID-1].respStatusTimeout,
                               DeviceStatsStructPTR[devID-1].respStatusCanceled);
                }
                /*send stats response using zmq_send*/
                nbytes = zmq_send (ResponderPtr,&replyMsgHeader ,
                                                 sizeof(replyMsgHeader), ZMQ_SNDMORE);
                if (nbytes != sizeof(replyMsgHeader))
                {
                    LogError ("S: ZMQ Error: Failed to send ZMQ response",__LINE__,FILE);
                }

                /*just to make sure we dont access the DeviceStatsStructPTR out of bounds*/
                if (devID <= TotalDevices)
                {
                    nbytes = zmq_send(ResponderPtr, &DeviceStatsStructPTR[devID-1],
                                      sizeof(DeviceStatsStructPTR[devID-1]), 0);
                    if (nbytes != sizeof(DeviceStatsStructPTR[devID-1]))
                    {
                        LogError ("S: ZMQ Error: Failed to send msg",__LINE__,FILE);
                    }
                }
                /*if stats for devID is wrong just send READ_FAILURE*/
                else
                {
                    mbm::MsgAck ack;
                    ack.errorno = mbm::READ_FAILURE;
                    nbytes = zmq_send(ResponderPtr,&ack,
                                                     sizeof(mbm::MsgAck) , 0);
                    if (nbytes !=  sizeof(mbm::MsgAck))
                    {
                        LogError ("S: ZMQ Error: Failed to send msg",__LINE__,FILE);
                    }
                }
                zmq_msg_close (&request);
                ControlMBM_STATE = MBM_PROCESS_REQUEST;
          }
          else
          {
              DBG_ERR("S: Status info for devices not avaliable without start");
              clearPendingMsgQueue();
              ControlMBM_STATE = MBM_PROCESS_REQUEST;
          }

        }
        break;
        case mbm::MSG_ID_READ_CMDLINE_C:
        {
            mbm::devRegInfo readRegInfo;

            zmq_msg_t request;

            /*receive request*/
            lu_int32_t nbytes = zmq_msg_init (&request);
            assert (nbytes == ZMQ_MSG_INIT_SUCCESS);

            //Block until a message is available to be received from socket
            nbytes = zmq_msg_recv (&request, ResponderPtr, ZMQ_BLOCKING_MODE);
            assert (nbytes != -1);

            memcpy(&readRegInfo, zmq_msg_data(&request), nbytes);
            DBG_INFO("S: Read Request for %s",
                            readRegInfo.toString().c_str());
//            readRegCmd(readRegInfo);


            LockingMutex lmutex(mutex);
            ReadCmdReqVector.push_back(readRegInfo);
            zmq_msg_close (&request);
            ControlMBM_STATE = MBM_PROCESS_REQUEST;
        }
        break;
        default:
            LogError ("Invalid Read",__LINE__,FILE);
            clearPendingMsgQueue();
            ControlMBM_STATE = MBM_PROCESS_REQUEST;
        break;
    }
    /*IMPLEMENT LATER*/
}


/*!
 ******************************************************************************
 *   \brief HandleWrite
 *          This function stores the incoming channelvalue structure which
 *          contains write info, calls the SendWriteReq fucntion which writes to
 *          the register.It sends the ack message back to the client & changes
 *          the state to MBM_PROCESS_REQUEST.
 *
 *   \param None
 *
 *   \return None
 *
 *******************************************************************************
 */

void HandleWrite(void)
{
    switch(MsgHeader.id)
    {
        case mbm::MSG_ID_WRITE_CHANNEL_C:
        {
            mbm::ChannelValue channelvalue;
            zmq_msg_t request;
            lu_int32_t nbytes = zmq_msg_init (&request);
            if (nbytes != ZMQ_MSG_INIT_SUCCESS)
            {
                LogError ("S: ZMQ Error: Msg Init failed",__LINE__,FILE);
            }
            //Block until a message is available to be received from socket
            nbytes = zmq_msg_recv (&request, ResponderPtr, ZMQ_BLOCKING_MODE);
            if (nbytes == ZMQ_RECVMSG_FAIL)
            {
                LogError ("S: ZMQ Error: Failed to receive msg",__LINE__,FILE);
            }
            memcpy(&channelvalue, zmq_msg_data (&request), nbytes);
            DBG_INFO("S: Channel write recieved for device ID : %d & channel ID : %d",
                     channelvalue.chRef.device.deviceId,channelvalue.chRef.channelId);
            SendWriteReq(channelvalue);
            zmq_msg_close (&request);
            ControlMBM_STATE = MBM_PROCESS_REQUEST;
        }
        break;
        case mbm::MSG_ID_WRITE_CMDLINE_C:
        {
            mbm::writeRegValueStr writeRegValue;

            zmq_msg_t request;

            /*receive request*/
            lu_int32_t nbytes = zmq_msg_init (&request);
            assert (nbytes == ZMQ_MSG_INIT_SUCCESS);

            //Block until a message is available to be received from socket
            nbytes = zmq_msg_recv (&request, ResponderPtr, ZMQ_BLOCKING_MODE);
            assert (nbytes != -1);

            mbm::writeRegValueStr* regValue = (mbm::writeRegValueStr*)(zmq_msg_data(&request));
            writeRegValue.values = regValue->values;
            writeRegValue.devreginfo = regValue->devreginfo;

            if(writeRegValue.values.size() != regValue->devreginfo.quantity)
            {
                DBG_ERR("error");
            }

            DBG_INFO("S: Write Request for %s with values : ",
                            writeRegValue.devreginfo.toString().c_str());
            for (mbm::registervalues::const_iterator i = writeRegValue.values.begin(); i != writeRegValue.values.end(); ++i)
            {
                   DBG_INFO("%d",*i);
            }
           // writeRegCmd(writeRegValue);

            { //Mutex Scope
                LockingMutex lmutex(mutex);
                WriteCmdReqVector.push_back(writeRegValue);
            }

            zmq_msg_close (&request);
            ControlMBM_STATE = MBM_PROCESS_REQUEST;
        }
        break;
        default:
            LogError ("Invalid Write",__LINE__,FILE);
            clearPendingMsgQueue();
            ControlMBM_STATE = MBM_PROCESS_REQUEST;
        break;
    }
}


/*!
 ******************************************************************************
 *   \brief Sets MbmStartFlag to false, sends STOP_ACK.
 *          Changes the state to MBM_STOPPED
 *
 *   \param None
 *
 *   \return None
 *
 *******************************************************************************
 */

void HandleStop(void)
{
    MbmStartFlag = false;
    pthread_join(channelDBID,NULL);
    sendResponse(STOP_ACK);
    ControlMBM_STATE = MBM_STOPPED;
}


/*
 ******************************************************************************
 *   \brief This function sends a reponse to the MCM client
 *
 *   \param MSG_TYPE,MSG_ID & MSG_ERR which are defined in ModbusMasterProto.hpp
 *
 *   \return None
 *
 *******************************************************************************
 */

void sendResponse(mbm::MSG_TYPE msgType,mbm::MSG_ID msgId,lu_uint8_t msgErr)
{
    struct mbm::MsgHeader respHeader;
    mbm::MsgAck respContent;
    respHeader.id = msgId;
    respHeader.type = msgType;
    respContent.errorno = msgErr;
    lu_int32_t nbytes= zmq_send (ResponderPtr, &respHeader,
                                               sizeof(respHeader), ZMQ_SNDMORE);
    if (nbytes != sizeof(respHeader))
    {
        LogError ("S: ZMQ Error: Failed to send msg",__LINE__,FILE);
    }
    nbytes = zmq_send (ResponderPtr, &respContent,sizeof(respContent), 0);
    if (nbytes != sizeof(respContent))
    {
        LogError ("S: ZMQ Error: Failed to send msg",__LINE__,FILE);
    }
    DBG_INFO("S: Sent Response: %d:%d:%d", respHeader.type,
                                             respHeader.id,respContent.errorno);
}

/*!
 ******************************************************************************
 *   \brief Clears any unwanted messages present in the receive queue
 *
 *   \param
 *
 *   \return None
 *
 *******************************************************************************
 */

void clearPendingMsgQueue(void)
{
    lu_int64_t   more = 0;
    size_t       moreMsgSize = sizeof more;
    lu_int32_t nbytes =zmq_getsockopt (ResponderPtr, ZMQ_RCVMORE, &more,   \
                                       &moreMsgSize);
    if(more)
    {
        DBG_INFO("S: Clearing message of size %i", moreMsgSize);
    }

    while(more)
    {
        zmq_msg_t part;
        nbytes = zmq_msg_init (&part);
        if (nbytes != ZMQ_MSG_INIT_SUCCESS)
        {
            LogError ("S: ZMQ Error: Msg Init failed",__LINE__,FILE);
        }
        /* Block until a message is available to be received from socket*/
        nbytes = zmq_msg_recv (&part, ResponderPtr, ZMQ_BLOCKING_MODE);
        if (nbytes == ZMQ_RECVMSG_FAIL)
        {
            LogError ("S: ZMQ Error: Failed to receive msg",__LINE__,FILE);
        }
        /* Determine if more message parts are to follow*/
        nbytes = zmq_getsockopt (ResponderPtr, ZMQ_RCVMORE, &more, &moreMsgSize);
        if(nbytes != ZMQ_GETSOCKOPT_SUCCESS)
        {
            LogError ("S: ZMQ Error: Get sock opt error",__LINE__,FILE);
        }
        zmq_msg_close (&part);
    }
    sendResponse(INVALID_ACK);
}


/*!
 ******************************************************************************
 *   \brief Publishes the value of the register reading.
 *
 *   \param
 *
 *   \return None
 *
 *******************************************************************************
 */

void Publish_Value(void *ptr,lu_uint16_t channelType,lu_uint16_t chnlIndex)
{
    mbm::MsgHeader publishMessageHeader;
    publishMessageHeader.type = mbm::MSG_TYPE_EVENT;
    struct mbm::ChannelEvent event;
    if(channelType==ANALOG_INPUT_CHANNEL)
    {
        clock_gettime(CLOCK_REALTIME, &dbPtr.aiChannel[chnlIndex].timeStamp);
        event.channel.channelId        = dbPtr.aiChannel[chnlIndex]
                                        .channelconf.channel.channelId;
        event.channel.channelType      = dbPtr.aiChannel[chnlIndex]
                                        .channelconf.channel.channelType;
        event.channel.device.deviceType= dbPtr.aiChannel[chnlIndex]
                                        .channelconf.channel.device.deviceType;
        event.channel.device.deviceId  = dbPtr.aiChannel[chnlIndex]
                                        .channelconf.channel.device.deviceId;
        event.time.timestamp           = dbPtr.aiChannel[chnlIndex].timeStamp;
        event.isOnline                 = dbPtr.aiChannel[chnlIndex]
                                        .regUpdateFlag;
        publishMessageHeader.id        = mbm::MSG_ID_EVENT_ANALOGUE;
        /*set analogue value to previousAnalogRegReading if point is offline */
        if(event.isOnline == LU_TRUE)
            event.analogValue              = dbPtr.aiChannel[chnlIndex]
                                                       .currentAnalogRegReading.regValueInt32;
        else if(event.isOnline == LU_FALSE)
        event.analogValue              = dbPtr.aiChannel[chnlIndex].previousAnalogRegReading.regValueInt32;
        DBG_INFO("analog value to be published is %d, online status %d",
                                              event.analogValue,event.isOnline);
    }
    else if(channelType==DIGITAL_INPUT_CHANNEL)
    {
        clock_gettime(CLOCK_REALTIME, &dbPtr.diChannel[chnlIndex].timeStamp);
        event.channel.channelId        = dbPtr.diChannel[chnlIndex]
                                         .channelconf.channel.channelId;
        event.channel.channelType      = dbPtr.diChannel[chnlIndex]
                                         .channelconf.channel.channelType;
        event.channel.device.deviceType= dbPtr.diChannel[chnlIndex]
                                         .channelconf.channel.device.deviceType;
        event.channel.device.deviceId  = dbPtr.diChannel[chnlIndex]
                                         .channelconf.channel.device.deviceId;
        event.time.timestamp           = dbPtr.diChannel[chnlIndex].timeStamp;
        event.isOnline                 = dbPtr.diChannel[chnlIndex]
                                         .regUpdateFlag;
        publishMessageHeader.id        = mbm::MSG_ID_EVENT_DIGITAL;

        /*set digital value to previousDigitalRegReading if point is offline */
        if(event.isOnline == LU_TRUE)
            event.digitalValue              = dbPtr.diChannel[chnlIndex]
                                                      .currentDigitalRegReading;
        else if(event.isOnline == LU_FALSE)
            event.digitalValue              = dbPtr.diChannel[chnlIndex]
                                                              .previousDigitalRegReading;

        DBG_INFO("digital value to be published is %d, online status %d:"
                                            ,event.digitalValue,event.isOnline);
    }
    else if (channelType==SESSION_STATUS_CHANNEL)
    {
        event.channel.channelId        = PING_CHANNEL_ID;
        event.channel.channelType      = mbm::MBDEVICE_CHANNEL_DI;
        event.channel.device.deviceType= (*(Status*)ptr).deviceType;
        event.channel.device.deviceId  = (*(Status*)ptr).deviceID;
        event.time.timestamp           = (*(Status*)ptr).timeStamp;
        event.isOnline                 = LU_TRUE;
        publishMessageHeader.id        = mbm::MSG_ID_EVENT_DIGITAL;
        event.digitalValue             = (*(Status*)ptr).statusFlag;
        DBG_INFO("Device ID: %d , Online Status : %d, Value:%d  ",
                        event.channel.device.deviceId,event.isOnline,event.digitalValue);
    }
    /*Publish channel event using zmq_send*/
    lu_int32_t nbytes = zmq_send (PublisherPtr,&publishMessageHeader ,
                                     sizeof(publishMessageHeader), ZMQ_SNDMORE);
    if (nbytes != sizeof(publishMessageHeader))
    {
        LogError ("S: ZMQ Error: Failed to pulish value",__LINE__,FILE);
    }
    nbytes= zmq_send (PublisherPtr, &event, sizeof(event), 0);
    if (nbytes != sizeof(event))
    {
        LogError ("S: ZMQ Error: Failed to pulish value",__LINE__,FILE);
    }
}


void readRegCmdResponse (void* ptr, mbm::READ_ERR status)
{
    mbm::MsgHeader respHeader;
    mbm::readRegValueStr readRegValue;

    respHeader.id = mbm::MSG_ID_READ_CMDLINE_R;
    respHeader.type = mbm::MSG_TYPE_READ;

    readRegValue.readErr = status;

    if(status == mbm::READ_SUCCESS)
    {
        readRegValue.values = ((DB*)ptr)->readRegCmdvalue;
    }
        /*send response*/
    lu_int32_t nbytes= zmq_send (ResponderPtr, &respHeader,
                                               sizeof(respHeader), ZMQ_SNDMORE);
    if (nbytes != sizeof(respHeader))
    {
        LogError ("S: ZMQ Error: Failed to send msg",__LINE__,FILE);
    }

    nbytes = zmq_send(ResponderPtr, &readRegValue,
                      sizeof(readRegValue), 0);
    if (nbytes != sizeof(readRegValue))
    {
        LogError ("S: ZMQ Error: Failed to send msg",__LINE__,FILE);
    }

    ControlMBM_STATE = MBM_PROCESS_REQUEST;
}


void writeRegCmdResponse (mbm::WRITE_ERR status)
{
    mbm::MsgHeader respHeader;

    respHeader.id = mbm::MSG_ID_WRITE_CMDLINE_R;
    respHeader.type = mbm::MSG_TYPE_WRITE;

        /*send response*/
    lu_int32_t nbytes= zmq_send (ResponderPtr, &respHeader,
                                               sizeof(respHeader), ZMQ_SNDMORE);
    if (nbytes != sizeof(respHeader))
    {
        LogError ("S: ZMQ Error: Failed to send msg",__LINE__,FILE);
    }

    nbytes = zmq_send(ResponderPtr, &status,
                      sizeof(status), 0);
    if (nbytes != sizeof(status))
    {
        LogError ("S: ZMQ Error: Failed to send msg",__LINE__,FILE);
    }

    ControlMBM_STATE = MBM_PROCESS_REQUEST;
}


/*
 *********************** End of file ******************************************
 */
