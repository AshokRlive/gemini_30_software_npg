#!/bin/bash

# Remove Debug directory
rm -rf Debug

# Create Debug directory
mkdir Debug

# Run cmake and create Debug Makefiles
cd Debug
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ../../src/
cd ..

# Remove Release directory
rm -rf Release

# Create Release directory
mkdir Release

# Run cmake and create Release Makefiles
cd Release
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release ../../src/
cd ..

# Remove Test directory
#rm -rf Test

# Create Test directory
#mkdir Test

# Run cmake and create Test Makefiles
#cd Test
#cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ../../test/
cd ..
