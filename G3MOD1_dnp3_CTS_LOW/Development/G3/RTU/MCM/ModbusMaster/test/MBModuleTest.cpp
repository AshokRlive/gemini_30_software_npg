#include <embUnit/embUnit.h>
//#include "MBDebug.h"
#include "MBMmisc.hpp"
#include "MBMdatabase.h"

DB *ptr = NULL;
static void setUp(void)
{
    ptr = new DB;
}

static void tearDown(void)
{
    delete ptr;
    ptr =NULL;
}

/* Test Case noswap*/
static void testGetFormattedValue_Noswap(void)
{
    ptr->device.byteswap=0;
    ptr->device.wordswap=0;

    TMWTYPES_USHORT values[10][2]=
    {
        // Input
        0,0,
        1,0,
        0,1,
        1,1,
        1234,0,
        0,1234,
    };

    lu_int32_t result[10]=
    {
      // result
       0,
       0x00010000,
       1,
       0x00010001,
       0x04D20000,
       0X000004D2
    };

    for (int i = 0; i < 10; ++i)
    {
        DBG_INFO("testGetFormattedValue_Noswap:%i",i);
        TEST_ASSERT_EQUAL_INT(result[i],GetFormattedValue(ptr,values[i]));
    }
}

/* Test Case byteswap*/
static void testGetFormattedValue_Wordswap(void)
{
    ptr->device.byteswap=0;
    ptr->device.wordswap=1;

    TMWTYPES_USHORT values[10][2]=
    {
        // Input
        0,0,
        1,0,
        0,1,
        1,1,
        36543,36543,
        0,36543
    };

    lu_int32_t result[10]=
    {
      // result
       0,
       1,
       0x00010000,
       0x00010001,
       0X8EBF8EBF,
       0X8EBF0000
    };


    //DBG_INFO("%i",values[2][2]);
    for (int i = 0; i < 10; ++i)
    {
        DBG_INFO("testGetFormattedValue_Wordswap:%i",i);
        TEST_ASSERT_EQUAL_INT(result[i],GetFormattedValue(ptr,values[i]));
    }
}


/* Test Case wordswap*/
static void testGetFormattedValue_Byteswap(void)
{
    ptr->device.byteswap=1;
    ptr->device.wordswap=0;

    TMWTYPES_USHORT values[10][2]=
    {
        // Input
        0,0,
        1,0,
        0,1,
        65535,0,
        0,65535,
        41120,41120
    };

    lu_int32_t result[10]=
    {
      // result
       0,
       0x01000000,
       0x00000100,
       0xFFFF0000,
       0X0000FFFF,
       0XA0A0A0A0
    };


    //DBG_INFO("%i",values[2][2]);
    for (int i = 0; i < 10; ++i)
    {
        DBG_INFO("testGetFormattedValue_Byteswap:%i",i);
        TEST_ASSERT_EQUAL_INT(result[i],GetFormattedValue(ptr,values[i]));
    }
}


/* Test Case bothswap*/
static void testGetFormattedValue_Bothswap(void)
{
    ptr->device.byteswap = 1;
    ptr->device.wordswap = 1;

    TMWTYPES_USHORT values[10][2]=
    {
        // Input
        0,0,
        1,0,
        0,1,
        65535,0,
        0,65535,
        41120,41120
    };

    lu_int32_t result[10]=
    {
      // result
       0,
       0x00000100,
       0x01000000,
       0x0000FFFF,
       0XFFFF0000,
       0XA0A0A0A0
    };


    //DBG_INFO("%i",values[2][2]);
    for (int i = 0; i < 10; ++i)
    {
        DBG_INFO("testGetFormattedValue_Bothswap:%i",i);
        TEST_ASSERT_EQUAL_INT(result[i],GetFormattedValue(ptr,values[i]));
    }

}

/****************Tests for GetBitValue************************************/
/* Test Case bothswap*/
static void testGetBitValue(void)
{

    lu_uint16_t values[20][3]=
    {
        // Input
        0,0,16,
        0,0,32768,
        1,0,0,
        1,0,32768,
        0,1,0,
        0,0,1,
        0,1,0,
        65535,2,32768,
        65535,2,16384,
        65535,2,8,
        0,2,32768,
        0,2,64,
        0,2,16,
        11211,2,16384,
        11211,2,32768,
        11211,2,64,
        65525,3,8,
        65525,3,4,
    };

    lu_uint8_t result[20]=
    {
      // result
       0,
       0,
       1,
       1,
       0,
       0,
       0,
       1,
       1,
       1,
       0,
       0,
       0,
       0,
       0,
       1,
       0,
       1,
    };


    //DBG_INFO("%i",values[2][2]);
    for (int i = 0; i < 20; ++i)
    {
        DBG_INFO("testGetBitValue:%i",i);
        TEST_ASSERT_EQUAL_INT(result[i],GetBitValue(values[i][0],values[i][1],values[i][2]));
    }

}



/* Test Suite*/
TestRef MBModuleTest_tests(void)
{
    EMB_UNIT_TESTFIXTURES(fixtures)
    {
        /* Test Cases*/
        new_TestFixture("testGetFormattedValue_Noswap",testGetFormattedValue_Noswap),
        new_TestFixture("testGetFormattedValue_Wordswap",testGetFormattedValue_Wordswap),
        new_TestFixture("testGetFormattedValue_Byteswap",testGetFormattedValue_Byteswap),
        new_TestFixture("testGetFormattedValue_Byteswap",testGetFormattedValue_Bothswap),
        new_TestFixture("testGetBitValue",testGetBitValue)
        // ... more test cases
    };
    EMB_UNIT_TESTCALLER(MBModuleTest_tests,"MBModuleTest_tests",setUp,tearDown,fixtures);
    return (TestRef)&MBModuleTest_tests;
}
