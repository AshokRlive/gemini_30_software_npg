cmake_minimum_required(VERSION 2.8)

add_library( TMWModbus STATIC
                tmwscl/modbus/mbalink.c
                tmwscl/modbus/mbchnl.c
                tmwscl/modbus/mbdiag.c
                tmwscl/modbus/mblink.c
                tmwscl/modbus/mbmem.c
                tmwscl/modbus/mbplink.c
                tmwscl/modbus/mbrlink.c
                tmwscl/modbus/mbsesn.c
                tmwscl/modbus/mbtlink.c
                tmwscl/modbus/mmbbrm.c
                tmwscl/modbus/mmbdata.c
                tmwscl/modbus/mmbmem.c
                tmwscl/modbus/mmbsesn.c
                tmwscl/modbus/mmbsim.c
#                tmwscl/utils/tmwappl.c
#                tmwscl/utils/tmwchnl.c
#                tmwscl/utils/tmwdb.c
#                tmwscl/utils/tmwdiag.c
#                tmwscl/utils/tmwdlist.c
#                tmwscl/utils/tmwdtime.c
#                tmwscl/utils/tmwlink.c
#                tmwscl/utils/tmwmem.c
#                tmwscl/utils/tmwmsim.c
#                tmwscl/utils/tmwphys.c
#                tmwscl/utils/tmwphysd.c
#                tmwscl/utils/tmwpltmr.c
#                tmwscl/utils/tmwsctr.c
#                tmwscl/utils/tmwsesn.c
#                tmwscl/utils/tmwsim.c
#                tmwscl/utils/tmwtarg.c
#                tmwscl/utils/tmwtargp.c
#                tmwscl/utils/tmwtimer.c
#                tmwscl/utils/tmwtprt.c
#                tmwscl/utils/tmwvrsn.c
#                tmwscl/utils/tmwappl.c
#                tmwscl/utils/tmwchnl.c
#                tmwscl/utils/tmwdb.c
#                tmwscl/utils/tmwdiag.c
#                tmwscl/utils/tmwdlist.c
#                tmwscl/utils/tmwdtime.c
#                tmwscl/utils/tmwlink.c
#                tmwscl/utils/tmwmem.c
#                tmwscl/utils/tmwmsim.c
#                tmwscl/utils/tmwphys.c
#                tmwscl/utils/tmwphysd.c
#                tmwscl/utils/tmwpltmr.c
#                tmwscl/utils/tmwsctr.c
#                tmwscl/utils/tmwsesn.c
#                tmwscl/utils/tmwsim.c
#                tmwscl/utils/tmwtarg.c
#                tmwscl/utils/tmwtargp.c
#                tmwscl/utils/tmwtimer.c
#                tmwscl/utils/tmwtprt.c
#                tmwscl/utils/tmwvrsn.c
#                LinIoTarg/liniotarg.c
#                LinIoTarg/liniodiag.c
#                LinIoTarg/lintcp.c
#                LinIoTarg/lin232.c
           ) 
#Add custom build flag for the protocol stack
SET(CMAKE_C_FLAGS  "${CMAKE_C_FLAGS} -DTMW_LINUX_TARGET")


# Access to the TMW utils code
include_directories(${CMAKE_G3_PATH_MCM_THIRDPARTY}/TMWCommon)

#Include current directory
include_directories(.)

# Generate Doxygen documentation
#gen_doxygen("TMProtocolStack" "")

  