/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2009 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

// MBMaster.cpp : Sample Modbus Master console application.
//

#if defined(TMW_LINUX_TARGET) 
#else
#include "../SampleApps/stdafx.h"
#endif

extern "C" {
#include "tmwscl/utils/tmwappl.h"
#include "tmwscl/utils/tmwdb.h"
#include "tmwscl/utils/tmwphys.h"
#include "tmwscl/utils/tmwpltmr.h"
#include "tmwscl/utils/tmwtarg.h"

#include "tmwscl/modbus/mbchnl.h"
#include "tmwscl/modbus/mblink.h"
#include "tmwscl/modbus/mmbbrm.h"
#include "tmwscl/modbus/mmbsesn.h"

#if defined(TMW_WTK_TARGET) 
#include <windows.h>
#include "WinIoTarg/include/winiotarg.h"
#endif

#if defined(TMW_LINUX_TARGET)
#include "LinIoTarg/liniotarg.h"
#endif
}

#if !TMWCNFG_MULTIPLE_TIMER_QS

/* This can be changed to open multiple sessions */
#define NUM_SESSIONS 1

typedef struct mySessionInfo 
{
  TMWTYPES_MILLISECONDS  readHoldingInterval;
  TMWTYPES_MILLISECONDS  lastReadHolding;
  TMWTYPES_MILLISECONDS  readCoilInterval;
  TMWTYPES_MILLISECONDS  lastReadCoil;
} MY_SESSION_INFO;


/* Config structures */
#if defined(TMW_WTK_TARGET) 
WINIO_CONFIG IOCnfg;
#endif
#if defined(TMW_LINUX_TARGET)
LINIO_CONFIG IOCnfg;
#endif
TMWTARG_CONFIG targConfig;
TMWPHYS_CONFIG physConfig;
MBLINK_CONFIG  linkConfig;
MMBSESN_CONFIG mmbSesnConfig;

/* Context pointers */
TMWAPPL *pApplContext;
TMWCHNL *pSclChannel;
TMWSESN *pSessions[NUM_SESSIONS];

MMBBRM_REQ_DESC requests[NUM_SESSIONS];

/* The following code defines application - specific functions
 * that are used in this example.
 * 
 * These functions begin with "my" to distinguish them from
 * Source Code Library functions, and are defined at the end
 * of the example in order to emphasize the typical
 * interface with the Source Code Library.
 */
MY_SESSION_INFO  *myInitMySession(void);
TMWTYPES_BOOL     myTimeToSendRequest(TMWTYPES_MILLISECONDS *pLastTime, TMWTYPES_MILLISECONDS interval);
void              mySleep(int milliseconds);
void              myBrmCallbackFcn(void *pUserCallbackParam, MBCHNL_RESPONSE_INFO *pResponse);
void              myPutDiagString(const TMWDIAG_ANLZ_ID *pAnlzId, const TMWTYPES_CHAR *pString);

/* Main entry point
 */
int main(int argc, char* argv[]) 
{
  mySessionInfo *pMyRequests;
  bool run = true;
  bool useSerial = false;
  bool useRTU = false;
  
  TMWTARG_UNUSED_PARAM(argc);
  TMWTARG_UNUSED_PARAM(argv);
  
  TMWTYPES_ULONG i;
  
#if TMW_WTK_TARGET || TMW_LINUX_TARGET
#if TMWCNFG_SUPPORT_DIAG 
  /* Register function to display diagnostic strings to console 
   * This is only necessary if using the Windows or Linux target layer.
   * If implementing a new target layer, tmwtarg_putDiagString()
   * should be modified if diagnostic output is desired.
   */
  tmwtargp_registerPutDiagStringFunc(myPutDiagString);
#endif
#endif

  /* Initialize MB SCL. This inlcudes:
   *  - initialize polled timer
   *  - initialize async database
   *  - initialize application context 
   */
  tmwtimer_initialize();
  tmwdb_init(10000); 
  pApplContext = tmwappl_initApplication();
  
  /* Initialize the Target congig Structure
   * Call tmwtarg_initConfig to initialize with default values,
   * then overwrite values as needed.
   */
  tmwtarg_initConfig(&targConfig);

  /* Initialize the Channel Structure
   * Call mbchnl_initConfig to initialize with default values,
   * then overwrite values as needed.
   */
  mbchnl_initConfig(&linkConfig, &physConfig);
  
  /* Initialize IO Config Structure
   * Call WinIoTarg_initConfig to initialize default values, then overwrite
   * values as needed.
   */
#if defined(TMW_WTK_TARGET)  
  WinIoTarg_initConfig(&IOCnfg);
  if(useSerial)
  {
    IOCnfg.type = WINIO_TYPE_232;

    /* port to open */
    strcpy(IOCnfg.win232.portName, "COM1");

    /* name displayed in analyzer window */
    strcpy(IOCnfg.win232.chnlName, "MB Master");

    strcpy(IOCnfg.win232.baudRate, "9600");
    IOCnfg.win232.numDataBits = WIN232_DATA_BITS_8;
    IOCnfg.win232.numStopBits = WIN232_STOP_BITS_1;
    IOCnfg.win232.parity      = WIN232_PARITY_EVEN;
    IOCnfg.win232.portMode    = WIN232_MODE_NONE;

    if(useRTU)
    {
      linkConfig.type          = MBLINK_TYPE_RTU;    
      IOCnfg.win232.bModbusRTU = TMWDEFS_TRUE;
      targConfig.numCharTimesBetweenFrames = 5;
    }
    else
    {
      linkConfig.type = MBLINK_TYPE_ASCII;  
    }
  }
  else
  {
    IOCnfg.type = WINIO_TYPE_TCP;
    
    /* name displayed in analyzer window */
    strcpy(IOCnfg.winTCP.chnlName, "MB Master");

    /* TCP/IP address of remote device  
     * 127.0.0.1 is the loopback address and will attempt to connect to an outstation listener on this computer
     */
    strcpy(IOCnfg.winTCP.ipAddress, "127.0.0.1");

    IOCnfg.winTCP.ipPort = 502;
    IOCnfg.winTCP.mode   = WINTCP_MODE_CLIENT;
    linkConfig.type      = MBLINK_TYPE_TCP;
  }
#endif

#if defined(TMW_LINUX_TARGET) 
  liniotarg_initConfig(&IOCnfg);
  if(useSerial)
  {
    IOCnfg.type = LINIO_TYPE_232;

    /* port to open */
    strcpy(IOCnfg.lin232.portName, "/dev/ttyS0");

    /* name displayed in analyzer window */
    strcpy(IOCnfg.lin232.chnlName, "MB Master");

    IOCnfg.lin232.baudRate    = LIN232_BAUD_9600;
    IOCnfg.lin232.numDataBits = LIN232_DATA_BITS_8;
    IOCnfg.lin232.numStopBits = LIN232_STOP_BITS_1;
    IOCnfg.lin232.parity      = LIN232_PARITY_EVEN;
    IOCnfg.lin232.portMode    = LIN232_MODE_NONE; 
    if(useRTU)
    {
      linkConfig.type          = MBLINK_TYPE_RTU;    
      IOCnfg.lin232.bModbusRTU = TMWDEFS_TRUE;
      targConfig.numCharTimesBetweenFrames = 5;
    }
    else
    {
      linkConfig.type = MBLINK_TYPE_ASCII;  
    }
  }
  else
  {
    // use TCP since it makes sample easier to use on a PC
    IOCnfg.type = LINIO_TYPE_TCP;

    /* name displayed in analyzer window */
    strcpy(IOCnfg.linTCP.chnlName, "MB Master");

    /* TCP/IP address of remote device  
     * 127.0.0.1 is the loopback address and will attempt to connect to an outstation listener on this computer
     */
    strcpy(IOCnfg.linTCP.ipAddress, "127.0.0.1");
  
    /* The default ipPort for modbus is 502. 
     * However for a Linux user-mode server you have to pick a port above 1024: 
     * The ports from 1 to 1023 are privileged ports available to root code only. 
     */
    IOCnfg.linTCP.ipPort = 1502;
    IOCnfg.linTCP.mode   = LINTCP_MODE_CLIENT;
    linkConfig.type      = MBLINK_TYPE_TCP;
  }
#endif


  /* Open the Channel
   */
  pSclChannel = mbchnl_openChannel(pApplContext, TMWDEFS_NULL, TMWDEFS_NULL,
    &linkConfig, &physConfig, &IOCnfg, &targConfig);
  
  if(pSclChannel == TMWDEFS_NULL)
  {
    /* Failed to open channel */
    printf("Failed to open channel, exiting program \n");
    
    /* Sleep for 10 seconds before exiting */
    mySleep(10000);
    return (1);
  }

  /* Initialize and open the Sessions
  */
  mmbsesn_initConfig(&mmbSesnConfig);
  for (i = 0; i < NUM_SESSIONS; i++)
  {
    mmbSesnConfig.slaveAddress = (unsigned short)(1 + i);
    pSessions[i] = mmbsesn_openSession(pSclChannel, &mmbSesnConfig, TMWDEFS_NULL);
    if(pSessions[i] == TMWDEFS_NULL)
    {
      /* Failed to open */
      printf("Failed to open session, exiting program \n");
    
      /* Sleep for 10 seconds before exiting */
      mySleep(10000);
      return (1);
    }

  }
  
  /* Initialize Request Descriptors
   */
  for (i = 0; i < NUM_SESSIONS; i++)
  {
    mmbbrm_initReqDesc(&requests[i], pSessions[i]);
    requests[i].pUserCallback = myBrmCallbackFcn;
    requests[i].pUserCallbackParam = (void *)i;
  }
  
  /* Now that everything is set up, start a "main loop"
   * that sends and processes requests.
   * Note that this simple example only processes one
   * session on one channel.
   */
  pMyRequests = myInitMySession();
  
  while (run == TMWDEFS_TRUE) 
  {
    /* See if it's time to send a request.
     * This simple demo only issues the following requests:
     *  - Read Holding Registers
     *  - Read Coils
     */
    if(myTimeToSendRequest(&pMyRequests->lastReadHolding, pMyRequests->readHoldingInterval))
    {
      for (int i = 0; i < NUM_SESSIONS; i++)
      {
        printf("Read Holding Registers for Session %d\n", i);
        mmbbrm_readHoldingRegisters(&requests[i],0,100);
      }
    }
    
    if(myTimeToSendRequest(&pMyRequests->lastReadCoil, pMyRequests->readCoilInterval))
    {
      for (int i = 0; i < NUM_SESSIONS; i++)
      {
        printf("Read Coils for Session %d\n", i);
        mmbbrm_readCoils(&requests[i],0,100);
      }
    }
    
    /* Process any data returned by the Slave.
     * Note that mmbappl_checkForInput will be called
     * many times in between calls to mmbbrm_xxx()
     */
    tmwappl_checkForInput(pApplContext);
    
    /* Check timers
     */
    tmwpltmr_checkTimer();
    
    /* Sleep for 50 milliseconds */
    mySleep(50);
  }
  
  /* Of course, we'll never get to here, since we're in an
   * infinite loop above, but for clarity,  
   * close all open sessions and channels
   */
  for (i = 0; i < NUM_SESSIONS; i++)
  {
    mmbsesn_closeSession(pSessions[i]);
  }
  
  mbchnl_closeChannel(pSclChannel);
  
  return (0);
}

/* myInitMySession
 * Initialize session command interval values
 */
MY_SESSION_INFO * myInitMySession(void) 
{
  mySessionInfo *p;
  p = new mySessionInfo;
  p->readHoldingInterval = 60000;  /* Read Holding Registers once per minute */
  p->readCoilInterval    = 5000;   /* Read Coils once every 5 seconds        */
  
  p->lastReadCoil = 0;
  p->lastReadHolding = 0;
  
  return p;
}

/*
 * myTimeToSendRequest
 * See if it is time to send a request
 *
 * Note that this simplified example does not take into account
 * the fact that the lastTime field could roll over.
 */
TMWTYPES_BOOL myTimeToSendRequest(TMWTYPES_MILLISECONDS *pLastTime, TMWTYPES_MILLISECONDS interval) 
{
  TMWTYPES_MILLISECONDS currentTime;
  TMWTYPES_BOOL returnVal;

  currentTime = tmwtarg_getMSTime();
  if (currentTime >= (*pLastTime + interval))
  {
    *pLastTime = currentTime;
    returnVal = TMWDEFS_TRUE;
  }
  else
  {
    returnVal = TMWDEFS_FALSE;
  }
  return (returnVal);
}

/* Sample function to sleep for x milliseconds */
void  mySleep(int milliseconds)
{
#if defined(TMW_WTK_TARGET) 
    WinIoTarg_Sleep(milliseconds);
#endif
#if defined(TMW_LINUX_TARGET) 
    Sleep(milliseconds*100);
#endif
}

/* The following functions are called by the SCL */
/* my_brmCallbackFcn
 * Example user request callback function
 * This function will be called when request completes. 
 * pResponse->status will indicate if request was successful, timed out or 
 * otherwise failed.
 */
void myBrmCallbackFcn(void *pUserCallbackParam, MBCHNL_RESPONSE_INFO *pResponse)
{
  TMWTYPES_ULONG index = (TMWTYPES_ULONG)pUserCallbackParam;

  printf("in BRM Callback Function, Session %lu status is %d\n", index, pResponse->status);
}

#if TMWCNFG_SUPPORT_DIAG
/* Simple diagnostic output function, registered with the Source Code Library */
void myPutDiagString(const TMWDIAG_ANLZ_ID *pAnlzId,const TMWTYPES_CHAR *pString)
{
  TMWDIAG_ID id = pAnlzId->sourceId;
 
  /* Commented out to turn off verbose diagnostics */
  /* Print all diagnostics */
  /* printf((char *)pString); 
   * return; 
   */

  /* Display errors, application level and user level diagnostics  
   * Add other layers such as target as desired          
   */
  if((TMWDIAG_ID_ERROR & id) 
    ||(TMWDIAG_ID_APPL & id)
    ||(TMWDIAG_ID_USER & id)
    /*||(TMWDIAG_ID_TARGET & id)*/
    )
  {
    printf( "%s", (char *)pString);  
    return;
  }
}
#endif
#endif
