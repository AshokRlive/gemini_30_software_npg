/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: mblink.h
 * description: Modbus Link Layer.
 */
#ifndef MBLINK_DEFINED
#define MBLINK_DEFINED

#ifndef __midl
  #include "tmwscl/utils/tmwdefs.h"
  #include "tmwscl/utils/tmwchnl.h"

  #include "tmwscl/modbus/mbcnfg.h"
  #include "tmwscl/modbus/mbalink.h"
  #include "tmwscl/modbus/mbrlink.h"
  #include "tmwscl/modbus/mbtlink.h"
  #include "tmwscl/modbus/mbplink.h"
#endif

#define MBLINK_INVALID_LENGTH 0xffff

#define MBLINK_BROADCAST_ADDRESS 0

typedef enum MBLINK_TYPE_ENUM 
{
  MBLINK_TYPE_TCP = 0,
  MBLINK_TYPE_ASCII,
  MBLINK_TYPE_RTU,
  MBLINK_TYPE_PLUS
} MBLINK_TYPE;

#ifndef __midl
  typedef struct mblinkConfig {
    MBLINK_TYPE type;
    /* Maximum number of request messages that will be queued on a modbus master 
     * Setting this to 0 allows an unlimited number of requests to be queued.
     * Setting this to 1 allows only 1 outstanding request at a time,
     * Setting this to a number greater than 1 specifies that number of 
     *   outstanding and queued requests
     */
    TMWTYPES_USHORT maxQueueSize;
     
    /* Maximum amount of time to wait for a complete frame after
     * receiving first characters of frame.
     */
    TMWTYPES_MILLISECONDS rxFrameTimeout;

   /* For a Modbus master how long to wait for a response to a request that has  
    * actually been transmitted.
    * This value can be made shorter than the 
    * MMBSESN_CONFIG defaultResponseTimeout 
    * to quickly determine if a particular device is not responding, without 
    * causing requests to other devices on the same channel to also time out.
    * NOTE: This is not used by a Modbus slave.
    */
   TMWTYPES_MILLISECONDS  channelResponseTimeout;

  #if MBCNFG_SUPPORT_TCP
    MBTLINK_CONFIG tlink;
  #endif

  #if MBCNFG_SUPPORT_ASCII
    MBALINK_CONFIG alink;
  #endif

  #if MBCNFG_SUPPORT_RTU
    MBRLINK_CONFIG rlink;
  #endif

  #if MBCNFG_SUPPORT_MBP
    MBPLINK_CONFIG plink;
  #endif

  } MBLINK_CONFIG;

  /* Define bit masks used to specify which configuration parameters
  * should be modified in a call to mblink_modifyChannel.
  */
  #define MBLINK_CONFIG_FUTURE      0x00000000

  #define MBLINK_TX_FRAME_SIZE  525
  #define MBLINK_RX_FRAME_SIZE  525
  #define MBLINK_RX_BINARY_FRAME_SIZE 260

  /* Prototype for user callback function */
  typedef void (*MBLINK_TX_CALLBACK_FUNC)(
    void *pCallbackParam,
    TMWSESN_TX_DATA *pTxData);

  typedef struct MBLinkContext {
    /* Generic TMW context, must be first entry */
    TMWLINK_CONTEXT tmw;

    /* MBLink parameters */
    MBLINK_TYPE type;
    TMWLINK_INTERFACE linkInterface;

    /* Tx Parameters */
    TMWTYPES_UCHAR txFrameBuffer[MBLINK_TX_FRAME_SIZE];
    TMWPHYS_TX_DESCRIPTOR physTxDescriptor;

    /* Rx Parameters */
    /* Buffer to hold frame as it is received, could be ascii which is larger */
    TMWTYPES_USHORT rxOffset;
    TMWTYPES_UCHAR  rxBuffer[MBLINK_RX_FRAME_SIZE];

    TMWSESN_RX_DATA rxFrame;

    /* Binary Frame Buffer is in binary format, without error check info 
      (extra size for ascii not needed)  
    */
    TMWTYPES_UCHAR rxBinaryFrameBuffer[MBLINK_RX_BINARY_FRAME_SIZE];

  #if MBCNFG_SUPPORT_TRANSACTION_ID
    /* Transaction Id for Modbus TCP */
    TMWTYPES_USHORT transactionId;
  #endif
    
    /* modbus tcp slave normally configured as 0xff, should echo the unit identifier. */
    TMWTYPES_UCHAR unitId;

    /* Delimiters for Modbus Ascii. These are configurable.
    *  MBFC 8, diagnostic function can change the delimiters 
    */ 
    TMWTYPES_UCHAR delim1;
    TMWTYPES_UCHAR delim2;

    TMWTYPES_MILLISECONDS rxFrameTimeout;
    TMWTIMER rxFrameTimer;

    /* User callback function to be called when fragment is given to link layer
     * Most implementations will not need to provide this callback function. This 
     * allows the message to be modified before it is transmitted for test purposes,
     * for example to modify the function code or to introduce errors.
     */
    MBLINK_TX_CALLBACK_FUNC pUserTxCallback;
    void *pCallbackParam;

  } MBLINK_CONTEXT;


  #ifdef __cplusplus
  extern "C" {
  #endif

    /* function: mblink_initConfig
    * purpose: 
    * arguments:
    *  none
    * returns:
    *  void
    */
    TMWDEFS_SCL_API void TMWDEFS_GLOBAL mblink_initConfig(
      MBLINK_CONFIG *pConfig);

    /* function: mblink_initChannel 
    * purpose: 
    * arguments:
    * returns:
    */
    TMWTYPES_BOOL TMWDEFS_GLOBAL mblink_initChannel(
      TMWCHNL *pChannel,
      const MBLINK_CONFIG *pConfig);

    /* function: mblink_modifyChannel 
    */
    TMWTYPES_BOOL TMWDEFS_GLOBAL mblink_modifyChannel(
      TMWCHNL *pChannel,
      const MBLINK_CONFIG *pConfig,
      TMWTYPES_ULONG configMask);

    /* function: mblink_deleteChannel
    * purpose:
    * arguments:
    * returns:
    *  void
    */
    TMWTYPES_BOOL TMWDEFS_GLOBAL mblink_deleteChannel(
      TMWCHNL *pChannel);

    /* function: mblink_findSession
    * purpose:
    * arguments:
    * returns: pointer to modbus session
    */
    TMWSESN * TMWDEFS_GLOBAL mblink_findSession(
      void *pContext,
      TMWTYPES_USHORT address);

    TMWSESN * TMWDEFS_GLOBAL mblink_monitorModeFindSession(
      TMWLINK_CONTEXT *pLinkContext,
      TMWTYPES_USHORT addr);

    /* function: mblink_registerCallback 
     * purpose: register user callback function to be called when fragment is 
     *  given to link layer. Most implementations will not need to provide this 
     *  callback function. This allows the message to be modified before it is 
     *  transmitted for test purposes, for example to modify the function code 
     *  or to introduce errors.
     * arguments:
     *  pChannel -
     *  pUserTxCallback -  User provided callback function
     *  pCallbackParam - User provided callback parameter
     * returns:
     *  void
     */
    TMWDEFS_SCL_API void TMWDEFS_GLOBAL mblink_registerCallback(
      TMWCHNL *pChannel,
      MBLINK_TX_CALLBACK_FUNC pUserTxCallback,
      void *pCallbackParam);

  #ifdef __cplusplus
  }
  #endif
#endif // __midl
#endif /* MBLINK_DEFINED */
