/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: mbplink.h
 * description: Modbus Plus Link Layer.
 */
#ifndef MBPLINK_DEFINED
#define MBPLINK_DEFINED

#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/utils/tmwchnl.h"

typedef struct MBPLinkConfig
{
  int reserved;
} MBPLINK_CONFIG;

#ifdef __cplusplus
extern "C" {
#endif

  /* function: mbplink_initConfig  
   * purpose: Initialize a modbus plus link configuration data structure
   * arguments:
   *  pConfig - pointer to modbus link configuration structure to be
   *   initialized.
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL mbplink_initConfig(
    MBPLINK_CONFIG *pConfig);

  /* function: mbplink_getNeededBytes
   * purpose: return the number of bytes required to finish the
   *  current block
   * arguments:
   *  pCallbackParam - callback data, contains link layer context
   * returns
   *  number of characters to read
   */
  TMWTYPES_USHORT TMWDEFS_CALLBACK mbplink_getNeededBytes(
    void *pCallbackParam);

  /* function: mbplink_parseBytes
   * purpose: parse incoming data
   * arguments:
   *  pCallbackParam - callback data, contains link layer context
   *  recvBuf - pointer to received characters
   *  numBytes - number of bytes pointed to by recvBuf
   *  firstByteTime - time that first byte was received. Could be zero
   *   if target layer does not fill this in. In that case it will be calculated
   *   in this function.
   * returns
   *  void
   */
  void TMWDEFS_CALLBACK mbplink_parseBytes(
    void *pCallbackParam,
    TMWTYPES_UCHAR *recvBuf,
    TMWTYPES_USHORT numBytes,
    TMWTYPES_MILLISECONDS firstByteTime);

  /* function: mbplink_transmitFrame 
   * purpose:
   * arguments:
   * returns:
   *  void
   */
  void TMWDEFS_CALLBACK mbplink_transmitFrame(
    void *pContext,
    TMWSESN_TX_DATA *pTxDescriptor);

#ifdef __cplusplus
}
#endif
#endif /* MBPLINK_DEFINED */
