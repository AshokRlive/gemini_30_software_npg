/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: mmbdata.h
 * description: This file defines the interface between the Triangle 
 *  MicroWorks, Inc. MB master source code library and the target database.
 */
#ifndef MMBDATA_DEFINED
#define MMBDATA_DEFINED

#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/utils/tmwdtime.h"
#include "tmwscl/modbus/mmbsesn.h"


#ifdef __cplusplus
extern "C" {
#endif

  /* function: mmbdata_init
   * purpose: Initialize MB Master database on specified session
   * arguments:
   *  pSession - pointer to session on which to create database
   *  pUserHandle - user specified database handle passed to session open
   * returns:
   *  pointer to database handle for future database calls
   */
  void * TMWDEFS_GLOBAL mmbdata_init(
    TMWSESN *pSession, 
    void *pUserHandle);

  /* function: mmbdata_close
   * purpose: Close target database. After this call the database
   *  handle will be invalid.
   * arguments:
   *  pHandle - database handle returned from mmbdata_init
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL mmbdata_close(
    void *pHandle);

  /* function: mmbdata_storeCoils
   * purpose: Store binary coil value
   * arguments:
   *  pHandle - database handle returned from mmbdata_init
   *  startAddr - address of first point to store
   *  quantity - number of coil values to store
   *  pValueArray - pointer to array of coil values to store,
   *   one bit per coil. This array will be large enough
   *   to hold quantity bits.
   * returns:
   *  TMWDEFS_TRUE if successful, else TMWDEFS_FALSE
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL mmbdata_storeCoils(
    void *pHandle,
    TMWTYPES_USHORT startAddr,
    TMWTYPES_USHORT quantity,
    TMWTYPES_UCHAR *pValueArray);

  /* function: mmbdata_storeDiscreteInputs
   * purpose: Store binary input value
   * arguments:
   *  pHandle - database handle returned from mmbdata_init
   *  startAddr - address of first point to store
   *  quantity - number of discrete input values to store 
   *  pValueArray - pointer to array of discrte input values to store,
   *   one bit per input. This array will be large enough
   *   to hold quantity bits.
   * returns:
   *  TMWDEFS_TRUE if successful, else TMWDEFS_FALSE
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL mmbdata_storeDiscreteInputs(
    void *pHandle,
    TMWTYPES_USHORT startAddr,
    TMWTYPES_USHORT quantity,
    TMWTYPES_UCHAR *pValueArray);

  /* function: mmbdata_storeInputRegisters
   * purpose: Store input register value
   * arguments:
   *  pHandle - database handle returned from mmbdata_init
   *  startAddr - address of first point to store
   *  quantity - number of input register values to store
   *  pValueArray - pointer to array of values to store, 16 bits each.
   * returns:
   *  TMWDEFS_TRUE if successful, else TMWDEFS_FALSE
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL mmbdata_storeInputRegisters(
    void *pHandle,
    TMWTYPES_USHORT startAddr,
    TMWTYPES_USHORT quantity,
    TMWTYPES_USHORT *pValueArray);

  
  /* function: mmbdata_storeHoldingRegister
   * purpose: Store holding register value
   * arguments:
   *  pHandle - database handle returned from mmbdata_init
   *  startAddr - address of first point to store
   *  quantity - number of holding register values to store
   *  pValueArray - pointer to array of values to store, 16 bits each.
   * returns:
   *  TMWDEFS_TRUE if successful, else TMWDEFS_FALSE
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL mmbdata_storeHoldingRegisters(
    void *pHandle,
    TMWTYPES_USHORT startAddr,
    TMWTYPES_USHORT quantity,
    TMWTYPES_USHORT *pValueArray);
  
  /* function: mmbdata_storeExceptionStatus
   * purpose: Store exception status value
   * arguments:
   *  pHandle - database handle returned from mmbdata_init
   *  value - 8 bit exception status value
   * returns:
   *  TMWDEFS_TRUE if successful, else TMWDEFS_FALSE
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL mmbdata_storeExceptionStatus(
    void *pHandle,
    TMWTYPES_UCHAR value);

  /* function: mmbdata_storeDiagnosticResponse
   * purpose: Store diagnostic response information
   * arguments:
   *  pHandle - database handle returned from mmbdata_init
   *  subFunction - sub-function code
   *  length - length in bytes of data
   *  pData - pointer to data returned in response
   * returns:
   *  TMWDEFS_TRUE if successful, else TMWDEFS_FALSE
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL mmbdata_storeDiagnosticResponse(
    void *pHandle,
    TMWTYPES_USHORT subFunction,
    TMWTYPES_UCHAR length,
    TMWTYPES_UCHAR *pdata);

  /* function: mmbdata_storeDeviceId
   * purpose: Store read device identification response information
   * arguments:
   *  pHandle - database handle returned from mmbdata_init
   *  objectId - object id
   *  length - length in bytes of data
   *  pData - pointer to data returned in response
   * returns:
   *  TMWDEFS_TRUE if successful, else TMWDEFS_FALSE
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL mmbdata_storeDeviceId(
    void *pHandle,
    TMWTYPES_UCHAR objectId,
    TMWTYPES_UCHAR length,
    TMWTYPES_UCHAR *pData);

#ifdef __cplusplus
}
#endif
#endif /* MMBDATA_DEFINED */
