/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: mmbmem.h
 * description: Master Modbus memory allocation.
 */
#ifndef MMBMEM_DEFINED
#define MMBMEM_DEFINED

#include "tmwscl/utils/tmwcnfg.h"
#include "tmwscl/utils/tmwmem.h"
#include "tmwscl/modbus/mbcnfg.h"
#include "tmwscl/modbus/mmbcnfg.h"
#include "tmwscl/modbus/mbmem.h"

/* Memory allocation defines */
typedef enum MMBMemAllocType {
  MMBMEM_MMBSESN_TYPE,
#if TMWCNFG_USE_SIMULATED_DB
  MMBMEM_SIM_DATABASE_TYPE,
#endif
  MMBMEM_ALLOC_TYPE_MAX
} MMBMEM_ALLOC_TYPE;

typedef struct {
  /* Specify max number of master modbus sessions */
  TMWTYPES_UINT numSessions;

  /* Specify the number of master modbus simulated databases in use. The SCL will
   * allocate a new simulated database for each session. These are not
   * needed once your actual database is implemented.
   */
  TMWTYPES_UINT numSimDbases;
} MMBMEM_CONFIG;

#ifdef __cplusplus
extern "C" 
{
#endif
 
  /* routine: mmbmem_initConfig
   * purpose:  initialize specified memory configuration structure,
   *  indicating the number of buffers of each structure type to 
   *  put in each memory pool. These will be initialized according 
   *  to the compile time defines. The user can change the desired
   *  fields and call mmbmem_initMemory()
   * arguments:
   *  pMMBConfig - pointer to memory configuration structure to be filled in
   *  pMBConfig - pointer to memory configuration structure to be filled in
   *  pTmwConfig - pointer to memory configuration structure to be filled in
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL mmbmem_initConfig(
    MMBMEM_CONFIG *pMMBConfig,
    MBMEM_CONFIG  *pMBConfig,
    TMWMEM_CONFIG *pTmwConfig);

  /* routine: mmbmem_initMemory
 * purpose: memory management init function. Can be used
   *  to modify the number of buffers that will be allowed in each
   *  buffer pool. This can only be used when TMWCNFG_USE_DYNAMIC_MEMORY
   *  is set to TMWDEFS_TRUE
   *  NOTE: This should be called before calling tmwappl_initApplication()
   * arguments:
   *  pMMBConfig - pointer to memory configuration structure to be used
   *  pMBConfig - pointer to memory configuration structure to be used
   *  pTmwConfig - pointer to memory configuration structure to be used
   * returns:
   *  TMWDEFS_TRUE if successful
   *  TMWDEFS_FALSE otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL mmbmem_initMemory(
    MMBMEM_CONFIG *pMMBConfig,
    MBMEM_CONFIG  *pMBConfig,
    TMWMEM_CONFIG *pTmwConfig);

  /* routine: mmbmem_init
   * purpose: INTERNAL memory management init function.
   *  NOTE: user should call mmbmem_initMemory() to modify the number
   *  of buffers allowed for each type.
   * arguments:
   *  pConfig - pointer to memory configuration structure to be used
   * returns:
   *  TMWDEFS_TRUE if successful
   *  TMWDEFS_FALSE otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL mmbmem_init(
    MMBMEM_CONFIG *pConfig);

  /* function: mmbmem_alloc
   * purpose:  Allocate memory  
   * arguments: 
   *  type - enum value indicating what structure to allocate
   * returns:
   *   TMWDEFS_NULL if allocation failed
   *   void * pointer to allocated memory if successful
   */
  void * TMWDEFS_GLOBAL mmbmem_alloc(
    MMBMEM_ALLOC_TYPE type);

  /* function: mmbmem_free
   * purpose:  Deallocate memory
   * arguments: 
   *  pBuf - pointer to buffer to be deallocated
   * returns:    
   *   void  
   */
  void TMWDEFS_GLOBAL mmbmem_free(
    void *pBuf);

  /* function: mmbmem_getUsage
   * purpose:  Determine memory usage for each type of memory
   *    managed by this file.
   * arguments: 
   *  index: index of pool, starting with 0 caller can call
   *    this function repeatedly, while incrementing index. When
   *     index is larger than number of pools, this function
   *     will return TMWDEFS_FALSE
   *  pName: pointer to a char pointer to be filled in
   *  pStruct: pointer to structure to be filled in.
   * returns:    
   *  TMWDEFS_TRUE  if successfully returned usage statistics.
   *  TMWDEFS_FALSE if failure because index is too large.
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL mmbmem_getUsage(
    TMWTYPES_UCHAR index,
    const TMWTYPES_CHAR **pName,
    TMWMEM_POOL_STRUCT *pStruct);

#ifdef __cplusplus
}
#endif
#endif /* MMBMEM_DEFINED */

