/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: mbtlink.h
 * description: Modbus Link Layer.
 */
#include "tmwscl/modbus/mbdiag.h"
#include "tmwscl/modbus/mbdefs.h"
#include "tmwscl/modbus/mbtlink.h"
#include "tmwscl/modbus/mblink.h"
#include "tmwscl/modbus/mbcnfg.h"

#include "tmwscl/utils/tmwtarg.h"

#if MBCNFG_SUPPORT_TCP

/* function: mbtlink_initConfig */
void TMWDEFS_GLOBAL mbtlink_initConfig(
  MBTLINK_CONFIG *pConfig)
{
  pConfig->reserved = 1;
}

/* function: mbtlink_getNeededBytes */
TMWTYPES_USHORT TMWDEFS_CALLBACK mbtlink_getNeededBytes(
  void *pCallbackParam)
{  
  TMWTYPES_USHORT length;
  MBLINK_CONTEXT *pLinkContext = (MBLINK_CONTEXT *)pCallbackParam;

  if(pLinkContext->rxOffset < 6)
    return (8 - pLinkContext->rxOffset);

  length = MBDEFS_MAKEWORD(pLinkContext->rxBuffer[4], pLinkContext->rxBuffer[5]);
  return ((length+6) - pLinkContext->rxOffset);
}


/* function: _findSession */
TMWSESN * TMWDEFS_LOCAL _findSession(
  void *pContext,
  TMWTYPES_USHORT address)
{
  TMWLINK_CONTEXT *pLinkContext = (TMWLINK_CONTEXT *)pContext;
  TMWSESN *pSession = TMWDEFS_NULL;

  while((pSession = (TMWSESN *)tmwdlist_getAfter(
    &pLinkContext->sessions, (TMWDLIST_MEMBER *)pSession)) != TMWDEFS_NULL)
  {
    /* if slave and address is configured as 0xff as it should be for modbus tcp allow any address to match */
    if(((pSession->type == TMWTYPES_SESSION_TYPE_SLAVE) && (pSession->destAddress == 0xff))
      ||(pSession->destAddress == address))
      return(pSession);
  }

  return(TMWDEFS_NULL);
}

static void TMWDEFS_LOCAL _msgReceived(
  MBLINK_CONTEXT *pLinkContext)
{
  TMWTYPES_UCHAR *pRxBuf = pLinkContext->rxBuffer;
  TMWTYPES_UCHAR *pFrameBuf = pLinkContext->rxBinaryFrameBuffer;
  TMWSESN *pSession; 
  TMWTYPES_USHORT length;
#if MBCNFG_SUPPORT_TRANSACTION_ID
  TMWTYPES_USHORT transactionId;
#endif

  /* Parse Frame */

#ifdef TMW_SUPPORT_MONITOR
  if(pLinkContext->tmw.pChannel->pPhysContext->monitorMode)
  {
    pSession = mblink_monitorModeFindSession((TMWLINK_CONTEXT *)pLinkContext, (TMWTYPES_USHORT)(pRxBuf[MBTLINK_UNIT_ID_BYTE]));
  }
  else
#endif
  pSession = _findSession(pLinkContext, (TMWTYPES_USHORT)(pRxBuf[MBTLINK_UNIT_ID_BYTE]));
  if(pSession == TMWDEFS_NULL)
  {
    char buf[64];
    /* Log Error */
    /* Diagnostics */
    tmwtarg_snprintf(buf, sizeof(buf), "Modbus TCP - session %d not found", pRxBuf[MBTLINK_UNIT_ID_BYTE]);
    TMWDIAG_ERROR(buf);
    pLinkContext->rxOffset = 0;
    return;
  }

  tmwsesn_setOnline(pSession, TMWDEFS_TRUE);

  MBDIAG_LINK_FRAME_RECEIVED(pLinkContext->tmw.pChannel, pSession,
    pLinkContext->rxBuffer, pLinkContext->rxOffset);

  /* Update statistics */
  TMWCHNL_STAT_CALLBACK_FUNC(
    pLinkContext->tmw.pChannel,
    TMWCHNL_STAT_FRAME_RECEIVED, TMWDEFS_NULL);

#if MBCNFG_SUPPORT_TRANSACTION_ID
  transactionId = MBDEFS_MAKEWORD(pLinkContext->rxBuffer[MBTLINK_TRANS_ID_BYTE_1],
    pLinkContext->rxBuffer[MBTLINK_TRANS_ID_BYTE_2]);

#ifdef TMW_SUPPORT_MONITOR
  /* If in analyzer or listen only mode, dont check transactionId */
  if(!pLinkContext->tmw.pChannel->pPhysContext->monitorMode)
#endif
  {
  /* save transaction ID for response message if this is a slave device 
   * or validate received id it if this is a master device                    
   */
  if(pSession->type == TMWTYPES_SESSION_TYPE_SLAVE)
  {
    pLinkContext->transactionId = transactionId;
    pLinkContext->unitId = pRxBuf[MBTLINK_UNIT_ID_BYTE];
  }
  else if(pLinkContext->transactionId == transactionId)
  {
    pLinkContext->transactionId++;
  }
  else
  {
    pLinkContext->rxOffset = 0;
#if TMWCNFG_SUPPORT_DIAG
    MBDIAG_ERROR(TMWDEFS_NULL, pSession,"Modbus TCP - Incorrect Transaction Id received");
#endif
    return;
  }
  } /* end of if not monitorMode */
#endif

  pLinkContext->rxFrame.pSession = pSession;
  if(pLinkContext->rxOffset < 8)
  {
    /* Not enough bytes, discard and
     * Start looking for a new frame 
     */
    pLinkContext->rxOffset = 0;
    return;
  }
  
  pLinkContext->rxFrame.msgLength = pLinkContext->rxOffset - MBTLINK_FC_BYTE;
  memcpy(pFrameBuf, &(pRxBuf[MBTLINK_FC_BYTE]), pLinkContext->rxFrame.msgLength);
  
  /* If actual length does not match length in link header send up an error 
   * so that correct exception code error can be returned 
   */
  length = MBDEFS_MAKEWORD(pLinkContext->rxBuffer[4], pLinkContext->rxBuffer[5]);
  if (pLinkContext->rxOffset != (length + 6))
  {
    pLinkContext->rxFrame.msgLength = MBLINK_INVALID_LENGTH;
  }
  
  /* Modbus TCP does not support broadcast */
  pLinkContext->rxFrame.isBroadcast = TMWDEFS_FALSE;

  /* Send received frame to the transport layer and reset state */
  if(pLinkContext->tmw.pParseFunc)
  {
    if(pSession->active)
    {
      pLinkContext->tmw.pParseFunc(
        pLinkContext->tmw.pCallbackParam,
        pSession, &pLinkContext->rxFrame);
    }
  }

  /* Start looking for a new frame */
  pLinkContext->rxOffset = 0;
}

/* function: _rxFrameTimeout
 * purpose: handle frame timeout
 * arguments:
 *  pCallbackParam - Link layer context
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _rxFrameTimeout(
  void *pCallbackParam)
{
  MBLINK_CONTEXT *pLinkContext = (MBLINK_CONTEXT *)pCallbackParam;
  
  TMWDIAG_ERROR("Modbus TCP - receive frame timeout");

  _msgReceived(pLinkContext);
}

/* function: mbtlink_parseBytes */
void TMWDEFS_CALLBACK mbtlink_parseBytes(
  void *pCallbackParam,
  TMWTYPES_UCHAR *recvBuf,
  TMWTYPES_USHORT numBytes,
  TMWTYPES_MILLISECONDS firstByteTime)
{
  MBLINK_CONTEXT *pLinkContext = (MBLINK_CONTEXT *)pCallbackParam;
  TMWTYPES_USHORT length = 0;

  if(pLinkContext->rxOffset == 0)
  {
    if(firstByteTime == 0)
    {
      pLinkContext->rxFrame.firstByteTime = tmwtarg_getMSTime();
    }
    else
    {
      pLinkContext->rxFrame.firstByteTime = firstByteTime;
    }          
    
    /* If frame timeout is specified start timer */
    if(pLinkContext->rxFrameTimeout != 0)
    {
      tmwtimer_start(&pLinkContext->rxFrameTimer,
        pLinkContext->rxFrameTimeout, pLinkContext->tmw.pChannel,
        _rxFrameTimeout, pLinkContext);
    }  
  } 
  
  /* Make sure received bytes won't overflow buffer */
  if((pLinkContext->rxOffset + numBytes) > MBLINK_RX_FRAME_SIZE)
  {
    TMWDIAG_ERROR("Modbus TCP - discarded receive chars");
    pLinkContext->rxOffset = 0;
    return;
  }

  memcpy(&pLinkContext->rxBuffer[pLinkContext->rxOffset], recvBuf, numBytes);
  pLinkContext->rxOffset += numBytes;

  if(pLinkContext->rxOffset < 8)
    return;

  length = MBDEFS_MAKEWORD(pLinkContext->rxBuffer[4], pLinkContext->rxBuffer[5]);

  if (pLinkContext->rxOffset >= length + 6)
  {
    tmwtimer_cancel(&pLinkContext->rxFrameTimer);
    _msgReceived(pLinkContext);
  } 
}

/* function: mbtlink_transmitFrame */
void TMWDEFS_CALLBACK mbtlink_transmitFrame(
  void *pContext,
  TMWSESN_TX_DATA *pTxDescriptor)
{
  MBLINK_CONTEXT *pLinkContext = (MBLINK_CONTEXT *)pContext;
  TMWTYPES_USHORT length = pTxDescriptor->msgLength + 1;

  /* If the session is not active simply return */
  if(!pTxDescriptor->pSession->active)
  {
#if TMWCNFG_SUPPORT_DIAG
    MBDIAG_ERROR(pTxDescriptor->pChannel, pTxDescriptor->pSession,"Modbus TCP - Attempt to send frame to inactive session");
#endif
    return;
  }

  /* Header */
#if MBCNFG_SUPPORT_TRANSACTION_ID
  pLinkContext->txFrameBuffer[MBTLINK_TRANS_ID_BYTE_1] = MBDEFS_HIBYTE(pLinkContext->transactionId);
  pLinkContext->txFrameBuffer[MBTLINK_TRANS_ID_BYTE_2] = MBDEFS_LOBYTE(pLinkContext->transactionId);
#else
  pLinkContext->txFrameBuffer[MBTLINK_TRANS_ID_BYTE_1] = 0;
  pLinkContext->txFrameBuffer[MBTLINK_TRANS_ID_BYTE_2] = 0;
#endif
  pLinkContext->txFrameBuffer[MBTLINK_PROT_ID_BYTE_1]  = 0;
  pLinkContext->txFrameBuffer[MBTLINK_PROT_ID_BYTE_2]  = 0;
  pLinkContext->txFrameBuffer[MBTLINK_LENGTH_BYTE_1]   = MBDEFS_HIBYTE(length);
  pLinkContext->txFrameBuffer[MBTLINK_LENGTH_BYTE_2]   = MBDEFS_LOBYTE(length);
  if(pTxDescriptor->pSession->type == TMWTYPES_SESSION_TYPE_SLAVE)
    /* For slave, echo the address that was received in the request */
    pLinkContext->txFrameBuffer[MBTLINK_UNIT_ID_BYTE] = pLinkContext->unitId;
  else
    /* For master, use the configured address */
    pLinkContext->txFrameBuffer[MBTLINK_UNIT_ID_BYTE] = (TMWTYPES_UCHAR)pTxDescriptor->pSession->destAddress;

  memcpy(&(pLinkContext->txFrameBuffer[MBTLINK_FC_BYTE]), pTxDescriptor->pMsgBuf, pTxDescriptor->msgLength);

  pLinkContext->physTxDescriptor.pCallbackParam = pTxDescriptor;
  pLinkContext->physTxDescriptor.beforeTxCallback = TMWDEFS_NULL;
  pLinkContext->physTxDescriptor.afterTxCallback = mbchnl_afterTxCallback;
  pLinkContext->physTxDescriptor.failedTxCallback = TMWDEFS_NULL;
  pLinkContext->physTxDescriptor.numBytesToTx = pTxDescriptor->msgLength + 7;
  pLinkContext->physTxDescriptor.pTxBuffer = pLinkContext->txFrameBuffer;

  /* Update statistics */
  TMWCHNL_STAT_CALLBACK_FUNC(
    pLinkContext->tmw.pChannel,
    TMWCHNL_STAT_FRAME_SENT, TMWDEFS_NULL);

  MBDIAG_LINK_FRAME_SENT(pLinkContext->tmw.pChannel, pTxDescriptor->pSession,
    pLinkContext->physTxDescriptor.pTxBuffer,
    pLinkContext->physTxDescriptor.numBytesToTx);

  pTxDescriptor->pSession->pChannel->pPhys->pPhysTransmit(
    pTxDescriptor->pSession->pChannel->pPhysContext, &pLinkContext->physTxDescriptor);
}

#endif /* MBCNFG_SUPPORT_TCP */
