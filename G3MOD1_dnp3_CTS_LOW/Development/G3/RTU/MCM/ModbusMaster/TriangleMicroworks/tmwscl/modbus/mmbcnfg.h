/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: mmbcnfg.h
 * description: Master Modbus configuration definitions
 */
#ifndef MMBCNFG_DEFINED
#define MMBCNFG_DEFINED

#include "tmwscl/utils/tmwcnfg.h"


/* Specify the maximum number of master Modbus sessions that can be open at a  
 * given time.
 */
#define MMBCNFG_NUMALLOC_SESNS         TMWCNFG_MAX_SESSIONS

#if TMWCNFG_USE_SIMULATED_DB
/* Specify the number of simulated databases in use. The TMW simulators
 * will allocate a new simulated database for each master session.
 */
#define MMBCNFG_NUMALLOC_SIM_DATABASES TMWCNFG_MAX_SIM_DATABASES
#endif

#endif /* MMBCNFG_DEFINED */

