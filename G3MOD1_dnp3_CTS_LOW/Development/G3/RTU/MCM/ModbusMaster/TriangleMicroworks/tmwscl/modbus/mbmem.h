/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                            MPP_COMMAND_PRODUCT                            */
/*                                                                           */
/* FILE NAME:    mbmem.h                                                     */
/* DESCRIPTION:  Master Modbus memory allocation routines                    */
/* PROPERTY OF:  Triangle MicroWorks, Inc. Raleigh, North Carolina USA       */
/*               www.TriangleMicroWorks.com  (919) 870-5101                  */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*****************************************************************************/
#ifndef MBMEM_DEFINED
#define MBMEM_DEFINED

#include "tmwscl/utils/tmwcnfg.h"
#include "tmwscl/utils/tmwmem.h"
#include "tmwscl/modbus/mbcnfg.h"

/* Memory allocation defines */
typedef enum MBMemAllocType {
  MBMEM_LINK_CONTEXT_TYPE,
  MBMEM_CHNL_TX_DATA_TYPE,
  MBMEM_ALLOC_TYPE_MAX
} MBMEM_ALLOC_TYPE;

typedef struct {
 /* Specify the number of link layer contexts to allocate. The TMW SCL needs
  * one per channel.
  */
  TMWTYPES_UINT numLinkContexts;

  /* Specify the number of transmit data structures to allocate. This is
   * dependent on how may application layer requests will be buffered per
   * session. For a modbus slave this would be 1 per session. For a master
   * if queing multiple requests is allowed, 1 would be required for each
   * request.
   */
  TMWTYPES_UINT numTxDatas;
} MBMEM_CONFIG;

#ifdef __cplusplus
extern "C" 
{
#endif

  /* routine: mbmem_initConfig
   * purpose:  INTERNAL function to initialize the memory configuration 
   *  structure indicating how many of each structure type to put in
   *  the memory pool. These will be initialized according 
   *  to the compile time defines.
   *  NOTE: user should call mmbmem_initConfig() or smbmem_initConfig()
   *  to modify the number of buffers allowed for each type.
   * arguments:
   *  pConfig - pointer to memory configuration structure to be filled in
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL mbmem_initConfig(
    MBMEM_CONFIG   *pConfig);

  /* routine: mbmem_init
   * purpose: INTERNAL memory management init function.
   *  NOTE: user should call mmbmem_initMemory() or smbmem_initMemory() 
   *  to modify the number of buffers allowed for each type.
   * arguments:
   *  pConfig - pointer to memory configuration structure to be used
   * returns:
   *  TMWDEFS_TRUE if successful
   *  TMWDEFS_FALSE otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL mbmem_init(
    MBMEM_CONFIG *pConfig);

  /* function: mbmem_alloc
   * purpose:  Allocate memory  
   * arguments: 
   *  type - enum value indicating what structure to allocate
   * returns:
   *   TMWDEFS_NULL if allocation failed
   *   void * pointer to allocated memory if successful
   */
  void * TMWDEFS_GLOBAL mbmem_alloc(
    MBMEM_ALLOC_TYPE type);

  /* function: mbmem_free
   * purpose:  Deallocate memory
   * arguments: 
   *  pBuf - pointer to buffer to be deallocated
   * returns:    
   *   void  
   */
  void TMWDEFS_GLOBAL mbmem_free(
    void *pBuf);

  /* function: mbmem_getUsage
   * purpose:  Determine memory usage for each type of memory
   *    managed by this file.
   * arguments: 
   *  index: index of pool, starting with 0 caller can call
   *    this function repeatedly, while incrementing index. When
   *     index is larger than number of pools, this function
   *     will return TMWDEFS_FALSE
   *  pName: pointer to a char pointer to be filled in
   *  pStruct: pointer to structure to be filled in.
   * returns:    
   *  TMWDEFS_TRUE  if successfully returned usage statistics.
   *  TMWDEFS_FALSE if failure because index is too large.
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL mbmem_getUsage(
    TMWTYPES_UCHAR index,
    const TMWTYPES_CHAR **pName,
    TMWMEM_POOL_STRUCT *pStruct);

#ifdef __cplusplus
}
#endif
#endif /* MBMEM_DEFINED */

