/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: mbplink.h
 * description: Modbus Link Layer.
 */
#include "tmwscl/modbus/mbdiag.h"
#include "tmwscl/modbus/mbplink.h"
#include "tmwscl/modbus/mblink.h"
#include "tmwscl/utils/tmwtarg.h"

#if MBCNFG_SUPPORT_MBP

/* function: mbplink_initConfig */
void TMWDEFS_GLOBAL mbplink_initConfig(
  MBPLINK_CONFIG *pConfig)
{
  pConfig->reserved = 1;
}

/* function: mbplink_getNeededBytes */
TMWTYPES_USHORT TMWDEFS_CALLBACK mbplink_getNeededBytes(
  void *pCallbackParam)
{
  TMWTARG_UNUSED_PARAM(pCallbackParam);

  /* Read as many bytes as many as you can */
  return(MBLINK_RX_FRAME_SIZE);
}

/* function: mbplink_parseBytes */
void TMWDEFS_CALLBACK mbplink_parseBytes(
  void *pCallbackParam,
  TMWTYPES_UCHAR *recvBuf,
  TMWTYPES_USHORT numBytes,
  TMWTYPES_MILLISECONDS firstByteTime)
{
  MBLINK_CONTEXT *pLinkContext = (MBLINK_CONTEXT *)pCallbackParam;
  
  if(pLinkContext->rxOffset == 0)
  {
    if(firstByteTime == 0)
    {
      pLinkContext->rxFrame.firstByteTime = tmwtarg_getMSTime();
    }
    else
    {
      pLinkContext->rxFrame.firstByteTime = firstByteTime;
    }
  }

  /* Make sure received bytes won't overflow buffer */
  if((pLinkContext->rxOffset + numBytes) > MBLINK_RX_FRAME_SIZE)
  {
    TMWDIAG_ERROR("Modbus Plus - discarded receive chars");
    pLinkContext->rxOffset = 0;
    return;
  }

  memcpy(&pLinkContext->rxBuffer[pLinkContext->rxOffset], recvBuf, numBytes);
  pLinkContext->rxOffset += numBytes;

  if (pLinkContext->rxOffset >= 2)
  {
    TMWTYPES_UCHAR *pRxBuf = pLinkContext->rxBuffer;
    TMWTYPES_UCHAR *pFrameBuf = pLinkContext->rxBinaryFrameBuffer;
    TMWTYPES_UCHAR addr;
    TMWSESN *pSession;

    /* Parse Frame */
    addr = 1;
    /* What about a slave MBP session? slave address should always be 0 */

    pSession = mblink_findSession(pLinkContext, addr);
    if(pSession == TMWDEFS_NULL)
    {
      char buf[64];
      /* Log Error */
      /* Diagnostics */
      tmwtarg_snprintf(buf, sizeof(buf), "Modbus Plus - session %d not found", addr);
      TMWDIAG_ERROR(buf);
      pLinkContext->rxOffset = 0;
      return;
    }

    tmwsesn_setOnline(pSession, TMWDEFS_TRUE);

    MBDIAG_LINK_FRAME_RECEIVED(pLinkContext->tmw.pChannel, pSession, 
      pLinkContext->rxBuffer, pLinkContext->rxOffset);

    /* Update statistics */
    TMWCHNL_STAT_CALLBACK_FUNC(
      pLinkContext->tmw.pChannel,
      TMWCHNL_STAT_FRAME_RECEIVED, TMWDEFS_NULL);

    pLinkContext->rxFrame.pSession = pSession;
    pLinkContext->rxFrame.msgLength = pLinkContext->rxOffset - 1;
    memcpy(pFrameBuf, &(pRxBuf[1]), pLinkContext->rxFrame.msgLength);
    
    /* Modbus Plus does not support broadcast */
    pLinkContext->rxFrame.isBroadcast = TMWDEFS_FALSE;

    /* Send received frame to the transport layer and reset state */
    if(pLinkContext->tmw.pParseFunc)
    {
      if(pSession->active)
      {
        pLinkContext->tmw.pParseFunc(
          pLinkContext->tmw.pCallbackParam,
          pSession, &pLinkContext->rxFrame);
      }
    }

    /* Start looking for a new frame */
    pLinkContext->rxOffset = 0;
  }
}

/* function: mbplink_transmitFrame */
void TMWDEFS_CALLBACK mbplink_transmitFrame(
  void *pContext,
  TMWSESN_TX_DATA *pTxDescriptor)
{
  MBLINK_CONTEXT *pLinkContext = (MBLINK_CONTEXT *)pContext;

  /* If the session is not active simply return */
  if(!pTxDescriptor->pSession->active)
  {
#if TMWCNFG_SUPPORT_DIAG
    MBDIAG_ERROR(pLinkContext->tmw.pChannel, pTxDescriptor->pSession, "Modbus Plus - Attempt to send frame to inactive session");
#endif
    return;
  }

  pLinkContext->txFrameBuffer[0] = 0;
  memcpy(&(pLinkContext->txFrameBuffer[1]), pTxDescriptor->pMsgBuf, pTxDescriptor->msgLength);


  pLinkContext->physTxDescriptor.pCallbackParam = pTxDescriptor;
  pLinkContext->physTxDescriptor.beforeTxCallback = TMWDEFS_NULL;
  pLinkContext->physTxDescriptor.afterTxCallback = mbchnl_afterTxCallback;
  pLinkContext->physTxDescriptor.failedTxCallback = TMWDEFS_NULL;
  pLinkContext->physTxDescriptor.numBytesToTx = pTxDescriptor->msgLength + 1; 
  pLinkContext->physTxDescriptor.pTxBuffer = pLinkContext->txFrameBuffer;
    /* pTxDescriptor->pMsgBuf; */

  /* Update statistics */
  TMWCHNL_STAT_CALLBACK_FUNC(
    pLinkContext->tmw.pChannel,
    TMWCHNL_STAT_FRAME_SENT, TMWDEFS_NULL);

	MBDIAG_LINK_FRAME_SENT(pLinkContext->tmw.pChannel, pTxDescriptor->pSession, 
    pLinkContext->physTxDescriptor.pTxBuffer, 
   	pLinkContext->physTxDescriptor.numBytesToTx);

  pTxDescriptor->pSession->pChannel->pPhys->pPhysTransmit(
    pTxDescriptor->pSession->pChannel->pPhysContext, &pLinkContext->physTxDescriptor);
}

#endif /* MBCNFG_SUPPORT_MBP */