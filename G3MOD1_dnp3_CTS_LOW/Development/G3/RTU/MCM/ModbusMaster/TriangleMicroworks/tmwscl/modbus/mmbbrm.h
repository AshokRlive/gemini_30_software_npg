/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: mmbbrm.h
 * description: Master Modbus Build Request Module
 */

/* The mmbbrm_xxx functions are the funtions used by a master session to
    initiate a message.  A pointer to the request structure is returned if 
    the function is successful, otherwise a null pointer is returned.  
    These functions are asynchronous and do not return the result.  The
    datalink functions pass any retrieved response or data back to the session
    when the response arrives.  The current implementation for MB provides
    queing of requests, but the protocol prohibits a new request from being sent
    until any outstanding request is received or a timeout occurs for/on the
    associated channel (note that sessions may share a channel in the case of 
    a multidrop link).
*/

#ifndef MMBBRM_DEFINED
#define MMBBRM_DEFINED

#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/modbus/mbchnl.h"

/* Structure to hold parameters common to all requests */
typedef struct MMBBRMReqDescStruct{
  
  /* Define where this request should be sent. Only one of pChannel or
   * pSession should be set. The other should be TMWDEFS_NULL. If pChannel
   * is not TMWDEFS_NULL this request will be sent to the broadcast address.
   */

  /* Session to issue this request to */
  TMWSESN *pSession;

  /* Channel to broadcast this request to */
  TMWCHNL *pChannel;

  /* Request description */
  const char *pMsgDescription;
  
  /* Priority for this request. This can be used to insert a write 
   * request ahead of a number of read requests for example. Priorities 
   * above 200 are normally reserved for internal SCL use. (although 
   * currently none are used by SCL).
   */
  TMWTYPES_UCHAR priority;

  /* Absolute response timeout, maximum amount of time, in milliseconds, 
   * that we will wait for a response to this request before terminating
   * the request. This period starts as soon as the request is submitted.
   */
  TMWTYPES_MILLISECONDS responseTimeout;

  /* User callback. Will be called on completion or failure */
  void *pUserCallbackParam;
  MBCHNL_CALLBACK_FUNC pUserCallback;

} MMBBRM_REQ_DESC;

#ifdef __cplusplus
extern "C" {
#endif

  /* function: mmbbrm_initReqDesc
   * purpose: Initialize a request descriptor
   * arguments: 
   *  pReqDesc - request descriptor containing generic parameters for
   *   a request
   *  pSession - session this request will be sent to
   * returns:
   *  TMWDEFS_TRUE if successful, else TMWDEFS_FALSE
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL mmbbrm_initReqDesc(
    MMBBRM_REQ_DESC *pReqDesc,
    TMWSESN *pSession);

  /* function: mmbbrm_initBroadcastDesc
   * purpose: Initialize a broadcast request descriptor
   * arguments: 
   *  pReqDesc - request descriptor containing generic parameters for
   *   a request
   *  pChannel - channle this request will be broadcast on
   * returns:
   *  TMWDEFS_TRUE if successful, else TMWDEFS_FALSE
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL mmbbrm_initBroadcastDesc(
    MMBBRM_REQ_DESC *pReqDesc,
    TMWCHNL *pChannel);

  /* function: mmbbrm_readCoils 
   * purpose: Issue a read Coils request to specified device
   * arguments:
   *  pRequestDesc - request descriptor containing generic parameters for
   *   this request
   *  start - Address of first coil
   *  quantity - Number of coils to read
   * returns:
   *  Pointer to tx data structure or TMWDEFS_NULL if request failed to be sent
   */
  TMWDEFS_SCL_API TMWSESN_TX_DATA * TMWDEFS_GLOBAL mmbbrm_readCoils(
    MMBBRM_REQ_DESC *pRequestDesc, 
    TMWTYPES_USHORT start,
    TMWTYPES_USHORT quantity);

  /* function: mmbbrm_readDiscreteInputs
   * purpose: Issue a read Discrete Inputs request to specified device
   * arguments:
   *  pRequestDesc - request descriptor containing generic parameters for
   *   this request
   *  start - Address of first discrete input
   *  quantity - Number of Discretes to read
   * returns:
   *  Pointer to tx data structure or TMWDEFS_NULL if request failed to be sent
   */
  TMWDEFS_SCL_API TMWSESN_TX_DATA * TMWDEFS_GLOBAL mmbbrm_readDiscreteInputs(
    MMBBRM_REQ_DESC *pRequestDesc, 
    TMWTYPES_USHORT start,
    TMWTYPES_USHORT quantity);

  /* function: mmbbrm_readHoldingRegisters 
   * purpose: Issue a read holding registers request to specified device
   * arguments:
   *  pRequestDesc - request descriptor containing generic parameters for
   *   this request
   *  start - Address of first Holding register
   *  quantity -
   * returns:
   *  Pointer to tx data structure or TMWDEFS_NULL if request failed to be sent
   */
  TMWDEFS_SCL_API TMWSESN_TX_DATA * TMWDEFS_GLOBAL mmbbrm_readHoldingRegisters(
    MMBBRM_REQ_DESC *pRequestDesc, 
    TMWTYPES_USHORT start,
    TMWTYPES_USHORT quantity);

  /* function: mmbbrm_readInputRegisters 
   * purpose: Issue a read input registers request to specified device
   * arguments:
   *  pRequestDesc - request descriptor containing generic parameters for
   *   this request
   *  start - Address of first Input Register to read
   *  quantity - Number of Input Registers to read
   * returns:
   *  Pointer to tx data structure or TMWDEFS_NULL if request failed to be sent
   */
  TMWDEFS_SCL_API TMWSESN_TX_DATA * TMWDEFS_GLOBAL mmbbrm_readInputRegisters(
    MMBBRM_REQ_DESC *pRequestDesc, 
    TMWTYPES_USHORT start,
    TMWTYPES_USHORT quantity);

  /* function: mmbbrm_writeSingleCoil 
   * purpose: Issue a write single coil request to specified device
   * arguments:
   *  pRequestDesc - request descriptor containing generic parameters for
   *   this request
   *  start - Address of coil to be written or forced
   *  value - Value to write to coil where 
   *   TMWDEFS_TRUE is ON and
   *   TMWDEFS_FALSE is OFF
   * returns:
   *  Pointer to tx data structure or TMWDEFS_NULL if request failed to be sent
   */
  TMWDEFS_SCL_API TMWSESN_TX_DATA * TMWDEFS_GLOBAL mmbbrm_writeSingleCoil(
    MMBBRM_REQ_DESC *pRequestDesc, 
    TMWTYPES_USHORT start,
    TMWTYPES_BOOL value);

  /* function: mmbbrm_writeMultipleCoils 
   * purpose: Issue a write multiple coil request to specified device
   * arguments:
   *  pRequestDesc - request descriptor containing generic parameters for
   *   this request
   *  start - Address of first coil to be written or forced
   *  quantity - number of coils to be written or forced
   *  pValueArray - array of bytes containing values to be written to coils
   *   1 bit per coil, padded with bits set to zero in last byte. As specified
   *   in Modbus specification. 
   * returns:
   *  Pointer to tx data structure or TMWDEFS_NULL if request failed to be sent 
   */
  TMWDEFS_SCL_API TMWSESN_TX_DATA * TMWDEFS_GLOBAL mmbbrm_writeMultipleCoils(
    MMBBRM_REQ_DESC *pRequestDesc, 
    TMWTYPES_USHORT start,
    TMWTYPES_USHORT quantity,
    TMWTYPES_UCHAR* pValueArray);

  /* function: mmbbrm_writeSingleRegister
   * purpose: Issue a write single register request to specified device
   * arguments:
   *  pRequestDesc - request descriptor containing generic parameters for
   *   this request
   *  start - Address of register to write
   *  value - Value to be written
   * returns:
   *  Pointer to tx data structure or TMWDEFS_NULL if request failed to be sent
   */
  TMWDEFS_SCL_API TMWSESN_TX_DATA * TMWDEFS_GLOBAL mmbbrm_writeSingleRegister(
    MMBBRM_REQ_DESC *pRequestDesc, 
    TMWTYPES_USHORT start,
    TMWTYPES_USHORT value);
  
  /* function: mmbbrm_readExceptionStatus
   * purpose: Issue a read exception status request to specified device
   * arguments:
   *  pRequestDesc - request descriptor containing generic parameters for
   *   this request
   * returns:
   *  Pointer to tx data structure or TMWDEFS_NULL if request failed to be sent
   */
  TMWDEFS_SCL_API TMWSESN_TX_DATA * TMWDEFS_GLOBAL mmbbrm_readExceptionStatus(
    MMBBRM_REQ_DESC *pRequestDesc);
  
   /* function: mmbbrm_diagnostics 
    * purpose: Issue a diagnostic request to specified device
    * arguments:
    *  pRequestDesc - request descriptor containing generic parameters for
    *   this request
    *  subFunction - Diagnostic sub function code to send
    *  dataSize - Size in bytes of sub function specific data to send
    *  pData - pointer to array of bytes to send
    * returns:
    *  Pointer to tx data structure or TMWDEFS_NULL if request failed to be sent
    */
  TMWDEFS_SCL_API TMWSESN_TX_DATA * TMWDEFS_GLOBAL mmbbrm_diagnostics(
    MMBBRM_REQ_DESC *pRequestDesc,
    TMWTYPES_USHORT subFunction,
    TMWTYPES_UCHAR  dataSize,
    TMWTYPES_UCHAR  *pData);

  /* function: mmbbrm_writeMultipleRegisters
   * purpose: Issue a write multiple registers request to specified device
   * arguments:
   *  pRequestDesc - request descriptor containing generic parameters for
   *   this request
   *  start - Address of first holding register
   *  quantity - Number of registers to be written
   *  pValueArray - pointer to array of unsigned short values to be written to registers
   * returns:
   *  Pointer to tx data structure or TMWDEFS_NULL if request failed to be sent
   */
  TMWDEFS_SCL_API TMWSESN_TX_DATA * TMWDEFS_GLOBAL mmbbrm_writeMultipleRegisters(
    MMBBRM_REQ_DESC *pRequestDesc, 
    TMWTYPES_USHORT start,
    TMWTYPES_USHORT quantity,
    TMWTYPES_USHORT* pValueArray);

  /* function: mmbbrm_ReadWriteMultipleRegisters 
   * purpose: Issue a read holding registers request to specified device
   * arguments:
   *  pRequestDesc - request descriptor containing generic parameters for
   *   this request
   *  readStart - Address of first Holding Register to read
   *  QuantityToRead - Number of Holding Registers to read
   *  writeStart - Address of first Holding Register to write
   *  QuantityToWrite - Number of Holding Registers to write
   *  pValueArray - Array of unsigned short values to write
   * returns:
   *  Pointer to tx data structure or TMWDEFS_NULL if request failed to be sent
   */
  TMWDEFS_SCL_API TMWSESN_TX_DATA * TMWDEFS_GLOBAL mmbbrm_ReadWriteMultipleRegisters(
  MMBBRM_REQ_DESC *pRequestDesc, 
  TMWTYPES_USHORT readStart,
  TMWTYPES_USHORT quantityToRead,
  TMWTYPES_USHORT writeStart,
  TMWTYPES_USHORT quantityToWrite,
  TMWTYPES_USHORT* pValueArray);

  /* function: mmbbrm_readDeviceId 
   * purpose: Issue a read Device Id request to specified device
   * arguments:
   *  pRequestDesc - request descriptor containing generic parameters for
   *   this request
   *  readDevIdCode - Read Device Id Code 1-4
   *   1: request to get the basic device identification (stream access) 
   *   2: request to get the regular device identification (stream access) 
   *   3: request to get the extended device identification (stream access) 
   *   4: request to get one specific identification object
   *  objectId - Object Id of device identification object to read. 0-0xff  
   *    0 with stream access means read all device identification objects
   * returns:
   *  Pointer to tx data structure or TMWDEFS_NULL if request failed to be sent
   */
  TMWDEFS_SCL_API TMWSESN_TX_DATA * TMWDEFS_GLOBAL mmbbrm_readDeviceId(
    MMBBRM_REQ_DESC *pRequestDesc,
    TMWTYPES_UCHAR readDevIdCode,
    TMWTYPES_UCHAR objectId);

  /* function: mmbbrm_sendCustomPdu 
   * purpose: Issue a custom request
   * arguments:
   *  pRequestDesc - request descriptor containing generic parameters for
   *   this request
   *  fc - function code for request
   *  pData - data for request
   *  length - length of data
   * returns:
   *  Pointer to tx data structure or TMWDEFS_NULL if request failed to be sent
   */
  TMWDEFS_SCL_API TMWSESN_TX_DATA * TMWDEFS_GLOBAL mmbbrm_sendCustomPdu(
    MMBBRM_REQ_DESC *pRequestDesc,
    TMWTYPES_UCHAR fc,
    TMWTYPES_UCHAR *pData,
    TMWTYPES_UCHAR length);

#ifdef __cplusplus
}
#endif
#endif /* MMBBRM_DEFINED */
