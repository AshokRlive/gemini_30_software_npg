/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: mmbsesn.h
 * description: Implement a Modbus Master session
 */
#include "tmwscl/modbus/mbdiag.h"
#include "tmwscl/modbus/mbsesn.h"
#include "tmwscl/modbus/mmbsesn.h"
#include "tmwscl/modbus/mbchnl.h"
#include "tmwscl/modbus/mmbdata.h"
#include "tmwscl/modbus/mmbmem.h"
#include "tmwscl/modbus/mbdefs.h"
#include "tmwscl/utils/tmwtarg.h"

/* Internal functions */

/* function: _processFrame     
 * purpose:   Process response received from modbus slave
 * arguments:  
 *  pSession - pointer to session structure 
 *  pRxFrame - pointer to response rx data structure
 * returns:
 *   TMWDEFS_TRUE if response was processed without error
 *   TMWDEFS_FALSE otherwise
 */
static TMWTYPES_BOOL TMWDEFS_GLOBAL _processFrame(
  TMWSESN *pSession,
  TMWSESN_RX_DATA *pRxFrame);

/* function: _processReadCoils    
 * purpose:   Process response to read coils request
 * arguments:  
 *  pSession - pointer to session structure
 *  pRequest - pointer to request tx data structure
 *  pResponse - pointer to response rx data structure
 * returns:
 *   TMWDEFS_TRUE if response was processed without error
 *   TMWDEFS_FALSE otherwise
 */
static TMWTYPES_BOOL TMWDEFS_LOCAL _processReadCoils(
  TMWSESN *pSession,
  MBCHNL_TX_DATA  *pRequest,
  TMWSESN_RX_DATA *pResponse)
{
  TMWTYPES_UCHAR  *pData;
  TMWTYPES_USHORT byteCount;
  TMWTYPES_USHORT quantity;
  TMWTYPES_USHORT start;
  TMWTYPES_USHORT offset = 1;
  TMWTYPES_USHORT i;

  MBSESN *pMBSession = (MBSESN *)pSession;

  start = MBDEFS_MAKEWORD(pRequest->tmw.pMsgBuf[1], pRequest->tmw.pMsgBuf[2]);
  quantity = MBDEFS_MAKEWORD(pRequest->tmw.pMsgBuf[3], pRequest->tmw.pMsgBuf[4]);

  byteCount = pResponse->pMsgBuf[offset++];

  if((byteCount+2) > pResponse->msgLength)
  {
    /* Log Error */
    mbchnl_discardInvLen(pSession);
    return(TMWDEFS_FALSE);
  }

  if(byteCount != ( (quantity/8) + ((quantity%8)?1:0) ))
  {
#if TMWCNFG_SUPPORT_DIAG
    /* Log Error */
    MBDIAG_ERROR(pRequest->tmw.pChannel, pSession, "Unexpected byte quantity in response");
#endif
    return(TMWDEFS_FALSE);
  }

  pData = &pResponse->pMsgBuf[offset];
  for(i = 0; i < quantity; i++)
  {
    TMWTYPES_BOOL bVal; 
    bVal = (pData[i/8] >> i%8) & 0x01;
    MBDIAG_SHOW_COIL(pSession, (TMWTYPES_USHORT)(start + i), bVal);
  }

  mmbdata_storeCoils(pMBSession->pDbHandle, start, quantity, pData);

  mbchnl_cleanupMessage((TMWSESN_TX_DATA*)pRequest, 
    pResponse, MBCHNL_RESP_STATUS_SUCCESS, 0);

  return(TMWDEFS_TRUE);
}

/* function: _processReadDiscreteInputs
 * purpose:   Process response to read discrete input request
 * arguments:  
 *  pSession - pointer to session structure
 *  pRequest - pointer to request tx data structure
 *  pResponse - pointer to response rx data structure
 * returns:
 *   TMWDEFS_TRUE if response was processed without error
 *   TMWDEFS_FALSE otherwise
 */
static TMWTYPES_BOOL TMWDEFS_LOCAL _processReadDiscreteInputs(
  TMWSESN *pSession,
  MBCHNL_TX_DATA  *pRequest,
  TMWSESN_RX_DATA *pResponse)
{
  TMWTYPES_UCHAR  *pData;
  TMWTYPES_USHORT byteCount;
  TMWTYPES_USHORT quantity;
  TMWTYPES_USHORT start;
  TMWTYPES_USHORT offset = 1;
  TMWTYPES_USHORT i;

  MBSESN *pMBSession = (MBSESN *)pSession;

  start = MBDEFS_MAKEWORD(pRequest->tmw.pMsgBuf[1], pRequest->tmw.pMsgBuf[2]);
  quantity = MBDEFS_MAKEWORD(pRequest->tmw.pMsgBuf[3], pRequest->tmw.pMsgBuf[4]);

  byteCount = pResponse->pMsgBuf[offset++];

  if((byteCount+2) > pResponse->msgLength)
  {
    /* Log Error */
    mbchnl_discardInvLen(pSession);
    return(TMWDEFS_FALSE);
  }

  if(byteCount != ( (quantity/8) + ((quantity%8)?1:0) ))
  {
#if TMWCNFG_SUPPORT_DIAG
    /* Log Error */
    MBDIAG_ERROR(pRequest->tmw.pChannel, pSession, "Unexpected byte quantity in response");
#endif
    return(TMWDEFS_FALSE);
  }

  pData = &pResponse->pMsgBuf[offset];
  for(i = 0; i < quantity; i++)
  { 
    TMWTYPES_BOOL bVal;
    bVal = (pData[i/8] >> i%8) & 0x01;
    MBDIAG_SHOW_DISCRETE_INPUT(pSession, (TMWTYPES_USHORT)(start + i), bVal);
  }

  mmbdata_storeDiscreteInputs(pMBSession->pDbHandle, start, quantity, pData);

  /* Remove request from queue and notify user if requested */
  mbchnl_cleanupMessage((TMWSESN_TX_DATA*)pRequest, 
    pResponse, MBCHNL_RESP_STATUS_SUCCESS, 0);

  return(TMWDEFS_TRUE);
}

/* function: _processReadHoldingRegisters 
 * purpose:   Process response to read holding registers request
 * arguments:  
 *  pSession - pointer to session structure
 *  pRequest - pointer to request tx data structure
 *  pResponse - pointer to response rx data structure
 * returns:
 *   TMWDEFS_TRUE if response was processed without error
 *   TMWDEFS_FALSE otherwise
 */
static TMWTYPES_BOOL TMWDEFS_LOCAL _processReadHoldingRegisters(
  TMWSESN *pSession,
  MBCHNL_TX_DATA  *pRequest,
  TMWSESN_RX_DATA *pResponse)
{
  TMWTYPES_USHORT tmpData[125];
  TMWTYPES_USHORT byteCount;
  TMWTYPES_USHORT quantity;
  TMWTYPES_USHORT start;
  TMWTYPES_USHORT offset = 1;
  TMWTYPES_USHORT i;

  MBSESN *pMBSession = (MBSESN *)pSession;

  start = MBDEFS_MAKEWORD(pRequest->tmw.pMsgBuf[1], pRequest->tmw.pMsgBuf[2]);
  quantity = MBDEFS_MAKEWORD(pRequest->tmw.pMsgBuf[3], pRequest->tmw.pMsgBuf[4]);

  byteCount = pResponse->pMsgBuf[offset++];

  if((byteCount+2) > pResponse->msgLength)
  {
    /* Log Error */
    mbchnl_discardInvLen(pSession);
    return(TMWDEFS_FALSE);
  }

  if(byteCount != (quantity * 2))
  {
#if TMWCNFG_SUPPORT_DIAG
    /* Log Error */
    MBDIAG_ERROR(pRequest->tmw.pChannel, pSession,"Unexpected byte quantity in response");
#endif
    return(TMWDEFS_FALSE);
  }

  for(i = 0; i < quantity; i++)
  {
    tmpData[i] = MBDEFS_MAKEWORD(pResponse->pMsgBuf[offset], pResponse->pMsgBuf[offset+1]);
    offset += 2;

    MBDIAG_SHOW_HOLDING_REGISTER(pSession, (TMWTYPES_USHORT)(start + i), tmpData[i]);
  }

  mmbdata_storeHoldingRegisters(pMBSession->pDbHandle, start, quantity, tmpData);
  
  /* Remove request from queue and notify user if requested */
  mbchnl_cleanupMessage((TMWSESN_TX_DATA*)pRequest, 
    pResponse, MBCHNL_RESP_STATUS_SUCCESS, 0);

  return(TMWDEFS_TRUE);
}

/* function: _processReadInputRegisters 
 * purpose:   Process response to read input register request
 * arguments:  
 *  pSession - pointer to session structure
 *  pRequest - pointer to request tx data structure
 *  pResponse - pointer to response rx data structure
 * returns:
 *   TMWDEFS_TRUE if response was processed without error
 *   TMWDEFS_FALSE otherwise
 */
static TMWTYPES_BOOL TMWDEFS_LOCAL _processReadInputRegisters(
  TMWSESN *pSession,
  MBCHNL_TX_DATA  *pRequest,
  TMWSESN_RX_DATA *pResponse)
{
  TMWTYPES_USHORT tmpData[125];
  TMWTYPES_USHORT byteCount;
  TMWTYPES_USHORT quantity;
  TMWTYPES_USHORT start;
  TMWTYPES_USHORT offset = 1;
  TMWTYPES_USHORT i;

  MBSESN *pMBSession = (MBSESN *)pSession;

  start = MBDEFS_MAKEWORD(pRequest->tmw.pMsgBuf[1], pRequest->tmw.pMsgBuf[2]);
  quantity = MBDEFS_MAKEWORD(pRequest->tmw.pMsgBuf[3], pRequest->tmw.pMsgBuf[4]);

  byteCount = pResponse->pMsgBuf[offset++];
  
  if((byteCount+2) > pResponse->msgLength)
  {
    /* Log Error */
    mbchnl_discardInvLen(pSession);
    return(TMWDEFS_FALSE);
  }

  if(byteCount != (quantity * 2))
  {
#if TMWCNFG_SUPPORT_DIAG
    /* Log Error */
    MBDIAG_ERROR(pRequest->tmw.pChannel, pSession, "Unexpected byte quantity in response");
#endif
    return(TMWDEFS_FALSE);
  }

  for(i = 0; i < quantity; i++)
  {
    tmpData[i] = MBDEFS_MAKEWORD(pResponse->pMsgBuf[offset], pResponse->pMsgBuf[offset+1]);
    offset += 2;

    MBDIAG_SHOW_INPUT_REGISTER(pSession, (TMWTYPES_USHORT)(start + i), tmpData[i]);
  }

  mmbdata_storeInputRegisters(pMBSession->pDbHandle, start, quantity, tmpData);

  /* Remove request from queue and notify user if requested */
  mbchnl_cleanupMessage((TMWSESN_TX_DATA*)pRequest, 
    pResponse, MBCHNL_RESP_STATUS_SUCCESS, 0);

  return(TMWDEFS_TRUE);
}

/* function: _processWriteSingleCoil 
 * purpose:   Process response to write single coil request
 * arguments:  
 *  pSession - pointer to session structure
 *  pRequest - pointer to request tx data structure
 *  pResponse - pointer to response rx data structure
 * returns:
 *   TMWDEFS_TRUE if response was processed without error
 *   TMWDEFS_FALSE otherwise
 */
static TMWTYPES_BOOL TMWDEFS_LOCAL _processWriteSingleCoil(
  TMWSESN *pSession,
  MBCHNL_TX_DATA  *pRequest,
  TMWSESN_RX_DATA *pResponse)
{
  TMWTARG_UNUSED_PARAM(pSession);
  
  mbchnl_cleanupMessage((TMWSESN_TX_DATA*)pRequest, 
    pResponse, MBCHNL_RESP_STATUS_SUCCESS, 0);

  return(TMWDEFS_TRUE);
}

/* function: _processWriteSingleRegister 
 * purpose:   Process response to write single register request
 * arguments:  
 *  pSession - pointer to session structure
 *  pRequest - pointer to request tx data structure
 *  pResponse - pointer to response rx data structure
 * returns:
 *   TMWDEFS_TRUE if response was processed without error
 *   TMWDEFS_FALSE otherwise
 */
static TMWTYPES_BOOL TMWDEFS_LOCAL _processWriteSingleRegister(
  TMWSESN *pSession,
  MBCHNL_TX_DATA  *pRequest,
  TMWSESN_RX_DATA *pResponse)
{
  TMWTARG_UNUSED_PARAM(pSession);

  mbchnl_cleanupMessage((TMWSESN_TX_DATA*)pRequest, 
    pResponse, MBCHNL_RESP_STATUS_SUCCESS, 0);

  return(TMWDEFS_TRUE);
}

/* function: _processReadExceptionStatus   
 * purpose:   Process response to read exception status request
 * arguments:  
 *  pSession - pointer to session structure
 *  pRequest - pointer to request tx data structure
 *  pResponse - pointer to response rx data structure
 * returns:
 *   TMWDEFS_TRUE if response was processed without error
 *   TMWDEFS_FALSE otherwise
 */
static TMWTYPES_BOOL TMWDEFS_LOCAL _processReadExceptionStatus(
  TMWSESN *pSession,
  MBCHNL_TX_DATA  *pRequest,
  TMWSESN_RX_DATA *pResponse)
{
  TMWTYPES_UCHAR  status;
  MBSESN *pMBSession = (MBSESN *)pSession;

  status = pResponse->pMsgBuf[1];
  
  MBDIAG_SHOW_EXCEPT_STATUS(pSession, status);

  mmbdata_storeExceptionStatus(pMBSession->pDbHandle, status);

  mbchnl_cleanupMessage((TMWSESN_TX_DATA*)pRequest, 
    pResponse, MBCHNL_RESP_STATUS_SUCCESS, 0);

  return(TMWDEFS_TRUE);
}

/* function: _processDiagnostics   
 * purpose:   Process response to diagnostics request
 * arguments:  
 *  pSession - pointer to session structure
 *  pRequest - pointer to request tx data structure
 *  pResponse - pointer to response rx data structure
 * returns:
 *   TMWDEFS_TRUE if response was processed without error
 *   TMWDEFS_FALSE otherwise
 */
static TMWTYPES_BOOL TMWDEFS_LOCAL _processDiagnostics(
  TMWSESN *pSession,
  MBCHNL_TX_DATA  *pRequest,
  TMWSESN_RX_DATA *pResponse)
{
  MBSESN *pMBSession = (MBSESN *)pSession;

  TMWTYPES_USHORT subFunction = MBDEFS_MAKEWORD(pRequest->tmw.pMsgBuf[1], pRequest->tmw.pMsgBuf[2]);

  if(pResponse->msgLength < 4)
  {
    /* Log Error */
    mbchnl_discardInvLen(pSession);
    return(TMWDEFS_FALSE);
  }

  /* We use the actual length of data rcvd, there is no length in the response. */
  MBDIAG_SHOW_DIAGS_RESPONSE(pSession, subFunction, (TMWTYPES_UCHAR)(pResponse->msgLength-3), &pResponse->pMsgBuf[3]);

  mmbdata_storeDiagnosticResponse(pMBSession->pDbHandle, subFunction, (TMWTYPES_UCHAR)(pResponse->msgLength-3),
    &pResponse->pMsgBuf[3]);

  mbchnl_cleanupMessage((TMWSESN_TX_DATA*)pRequest, 
    pResponse, MBCHNL_RESP_STATUS_SUCCESS, 0);

  return(TMWDEFS_TRUE);
}

/* function: _processWriteMultipleCoils
 * purpose:   Process response to write coils request
 * arguments:  
 *  pSession - pointer to session structure
 *  pRequest - pointer to request tx data structure
 *  pResponse - pointer to response rx data structure
 * returns:
 *   TMWDEFS_TRUE if response was processed without error
 *   TMWDEFS_FALSE otherwise
 */
static TMWTYPES_BOOL TMWDEFS_LOCAL _processWriteMultipleCoils(
  TMWSESN *pSession,
  MBCHNL_TX_DATA  *pRequest,
  TMWSESN_RX_DATA *pResponse)
{
  TMWTARG_UNUSED_PARAM(pSession);
  
  /* Remove request from queue and notify user if requested */
  mbchnl_cleanupMessage((TMWSESN_TX_DATA*)pRequest, 
    pResponse, MBCHNL_RESP_STATUS_SUCCESS, 0);

  return(TMWDEFS_TRUE);
}

/* function: _processWriteMultipleHoldingRegisters 
 * purpose:   Process response to write holding registers request
 * arguments:  
 *  pSession - pointer to session structure
 *  pRequest - pointer to request tx data structure
 *  pResponse - pointer to response rx data structure
 * returns:
 *   TMWDEFS_TRUE if response was processed without error
 *   TMWDEFS_FALSE otherwise
 */
static TMWTYPES_BOOL TMWDEFS_LOCAL _processWriteMultipleHoldingRegisters(
  TMWSESN *pSession,
  MBCHNL_TX_DATA  *pRequest,
  TMWSESN_RX_DATA *pResponse)
{
  TMWTARG_UNUSED_PARAM(pSession);

  /* Remove request from queue and notify user if requested */
  mbchnl_cleanupMessage((TMWSESN_TX_DATA*)pRequest, 
    pResponse, MBCHNL_RESP_STATUS_SUCCESS, 0);

  return(TMWDEFS_TRUE);
}

/* function: _processReadWriteMultipleRegisters
 * purpose:   Process response to read write multiple holding registers request
 * arguments:  
 *  pSession - pointer to session structure
 *  pRequest - pointer to request tx data structure
 *  pResponse - pointer to response rx data structure
 * returns:
 *   TMWDEFS_TRUE if response was processed without error
 *   TMWDEFS_FALSE otherwise
 */
static TMWTYPES_BOOL TMWDEFS_LOCAL _processReadWriteMultipleRegisters(
  TMWSESN *pSession,
  MBCHNL_TX_DATA  *pRequest,
  TMWSESN_RX_DATA *pResponse)
{
  TMWTYPES_USHORT tmpData[125];
  TMWTYPES_USHORT byteCount;
  TMWTYPES_USHORT quantity;
  TMWTYPES_USHORT start;
  TMWTYPES_USHORT offset = 1;
  TMWTYPES_USHORT i;

  MBSESN *pMBSession = (MBSESN *)pSession;
  
  /* From request */
  start = MBDEFS_MAKEWORD(pRequest->tmw.pMsgBuf[1], pRequest->tmw.pMsgBuf[2]);
  quantity = MBDEFS_MAKEWORD(pRequest->tmw.pMsgBuf[3], pRequest->tmw.pMsgBuf[4]);
    
  byteCount = pResponse->pMsgBuf[offset++];
    
  if((byteCount+2) > pResponse->msgLength)
  {
    /* Log Error */
    mbchnl_discardInvLen(pSession);
    return(TMWDEFS_FALSE);
  }

  if(byteCount != (quantity * 2))
  {
#if TMWCNFG_SUPPORT_DIAG
    /* Log Error */
    MBDIAG_ERROR(pRequest->tmw.pChannel, pSession, "Unexpected byte quantity in response");
#endif
    return(TMWDEFS_FALSE);
  }

  for(i = 0; i < quantity; i++)
  {
    tmpData[i] = MBDEFS_MAKEWORD(pResponse->pMsgBuf[offset], pResponse->pMsgBuf[offset+1]);
    offset += 2;

    MBDIAG_SHOW_HOLDING_REGISTER(pSession, (TMWTYPES_USHORT)(start + i), tmpData[i]);
  }

  mmbdata_storeHoldingRegisters(pMBSession->pDbHandle, start, quantity, tmpData);

  /* Remove request from queue and notify user if requested */
  mbchnl_cleanupMessage((TMWSESN_TX_DATA*)pRequest, 
    pResponse, MBCHNL_RESP_STATUS_SUCCESS, 0);

  return(TMWDEFS_TRUE);
}

/* function: _processReadDeviceId
 * purpose:   Process response to read device identification request
 * arguments:  
 *  pSession - pointer to session structure
 *  pRequest - pointer to request tx data structure
 *  pResponse - pointer to response rx data structure
 * returns:
 *   TMWDEFS_TRUE if response was processed without error
 *   TMWDEFS_FALSE otherwise
 */
static TMWTYPES_BOOL TMWDEFS_LOCAL _processReadDeviceId(
  TMWSESN *pSession,
  MBCHNL_TX_DATA  *pRequest,
  TMWSESN_RX_DATA *pResponse)
{   
  int i;
  TMWTYPES_USHORT index;
  TMWTYPES_UCHAR  readDevIdCode;
  TMWTYPES_UCHAR  moreFollows;
  TMWTYPES_UCHAR  nextObjectId;
  TMWTYPES_UCHAR  conformityLevel;
  TMWTYPES_UCHAR  numberOfObjects;

  MBSESN *pMBSession = (MBSESN *)pSession;

  /* only MEI Type 14 read device identification is supported */
  if(pRequest->tmw.pMsgBuf[1]!=14)
  {
#if TMWCNFG_SUPPORT_DIAG
    MBDIAG_ERROR(pRequest->tmw.pChannel, pSession, "Unexpected MEI Type");
#endif
    return TMWDEFS_FALSE;
  }
   
  if(pResponse->msgLength < 7)
  {
    /* Log Error */
    mbchnl_discardInvLen(pSession);
    return(TMWDEFS_FALSE);
  }

  readDevIdCode = pRequest->tmw.pMsgBuf[2];
  conformityLevel = pResponse->pMsgBuf[3];
  moreFollows = pResponse->pMsgBuf[4];
  nextObjectId = pResponse->pMsgBuf[5];
  numberOfObjects = pResponse->pMsgBuf[6];
  index = 7;

  for(i=0; i<numberOfObjects; i++)
  {
    TMWTYPES_UCHAR  objectId;
    TMWTYPES_UCHAR  length = 0;
    TMWTYPES_BOOL validLength = TMWDEFS_FALSE;

    if((index+2) <= pResponse->msgLength)
    {
      objectId = pResponse->pMsgBuf[index++];
      length = pResponse->pMsgBuf[index++];
      validLength = TMWDEFS_TRUE;
    }

    if((!validLength) 
      ||((index + length) > pResponse->msgLength))
    {
      /* Log Error */
      mbchnl_discardInvLen(pSession);
      return(TMWDEFS_FALSE);
    }

    MBDIAG_SHOW_DEVICEID(pSession, objectId, length, &pResponse->pMsgBuf[index]);
    mmbdata_storeDeviceId(pMBSession->pDbHandle, objectId, length, &pResponse->pMsgBuf[index]);
    index += length;
  }
 
  MBDIAG_SHOW_DEVICEIDDATA(pSession, moreFollows, nextObjectId, conformityLevel);

  if(moreFollows)
  { 
    mbchnl_removeRequest((TMWSESN_TX_DATA*)pRequest);

    /* Change request parameters to continue to read. */
    pRequest->tmw.pMsgBuf[3] = nextObjectId; 
    pRequest->sent = TMWDEFS_FALSE;
    pRequest->priority++;
  
    /* Send message */
    if (mbchnl_sendMessage((TMWSESN_TX_DATA*)pRequest))
    { 
      return(TMWDEFS_TRUE);
    }
  }

  /* Remove request from queue and notify user if requested */
  mbchnl_cleanupMessage((TMWSESN_TX_DATA*)pRequest, 
    pResponse, MBCHNL_RESP_STATUS_SUCCESS, 0);


  return(TMWDEFS_TRUE);
}

/* External functions */

/* function: mmbsesn_initConfig */
void TMWDEFS_GLOBAL mmbsesn_initConfig(
  MMBSESN_CONFIG *pConfig)
{
  pConfig->slaveAddress = 1;
  pConfig->defaultResponseTimeout = 20000;
  pConfig->active = TMWDEFS_TRUE;

  /* User provided statistics callback function */
  pConfig->pStatCallback = TMWDEFS_NULL;
  pConfig->pStatCallbackParam = TMWDEFS_NULL;
}

/* function: mmbsesn_openSession */
TMWSESN * TMWDEFS_GLOBAL mmbsesn_openSession(
  TMWCHNL *pChannel,
  const MMBSESN_CONFIG *pConfig,
  void *pUserHandle)
{
  MMBSESN *pMMBSession;

  /* Initialize memory management if not yet done */
  if(!tmwappl_getInitialized(TMWAPPL_INIT_MMB))
  {
    if(!mmbmem_init(TMWDEFS_NULL))
      return(TMWDEFS_NULL);

    tmwappl_setInitialized(TMWAPPL_INIT_MMB);
  }

  /* Allocate space for session context */
  pMMBSession = (MMBSESN *)mmbmem_alloc(MMBMEM_MMBSESN_TYPE);

  if(pMMBSession == TMWDEFS_NULL)
  {
    return(TMWDEFS_NULL);
  }

  pMMBSession->mb.pProcessFrameFunc = _processFrame;

  /* Configuration */
  pMMBSession->mb.tmw.active = pConfig->active;
  pMMBSession->mb.tmw.destAddress = pConfig->slaveAddress;
  pMMBSession->defaultResponseTimeout = pConfig->defaultResponseTimeout;

  /* Lock channel */
  TMWTARG_LOCK_SECTION(&pChannel->lock);

  /* Initialize master database */
  pMMBSession->mb.pDbHandle = mmbdata_init((TMWSESN *)pMMBSession, pUserHandle);
  if(pMMBSession->mb.pDbHandle == TMWDEFS_NULL)
  {
    /* Log error */
    mmbmem_free(pMMBSession);
  
    /* Unlock channel */
    TMWTARG_UNLOCK_SECTION(&pChannel->lock);
    return(TMWDEFS_NULL);
  }

  /* Initialize generic Modbus session */
  mbsesn_openSession(pChannel, (TMWSESN *)pMMBSession, pConfig->pStatCallback, 
    pConfig->pStatCallbackParam, TMWTYPES_PROTOCOL_MB, TMWTYPES_SESSION_TYPE_MASTER);

  /* Unlock channel */
  TMWTARG_UNLOCK_SECTION(&pChannel->lock);
  return((TMWSESN *)pMMBSession);
}

/* function: mmbsesn_getSessionConfig */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsesn_getSessionConfig(
  TMWSESN *pSession,
  MMBSESN_CONFIG *pConfig)
{
  MMBSESN *pMMBSession = (MMBSESN*)pSession;
  
  pConfig->active              = pSession->active;
  pConfig->slaveAddress        = pSession->destAddress;
  pConfig->pStatCallback       = pSession->pStatCallbackFunc;
  pConfig->pStatCallbackParam  = pSession->pStatCallbackParam;

  pConfig->defaultResponseTimeout = pMMBSession->defaultResponseTimeout;

  return(TMWDEFS_TRUE);
}

/* function: mmbsesn_setSessionConfig */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsesn_setSessionConfig(
  TMWSESN *pSession,
  const MMBSESN_CONFIG *pConfig)
{
  MMBSESN *pMMBSession = (MMBSESN*)pSession;
  
  pSession->active             = pConfig->active;
  pSession->destAddress        = pConfig->slaveAddress;
  pSession->pStatCallbackFunc  = pConfig->pStatCallback;
  pSession->pStatCallbackParam = pConfig->pStatCallbackParam;

  pMMBSession->defaultResponseTimeout = pConfig->defaultResponseTimeout;

  return(TMWDEFS_TRUE);
}

/* function: mmbsesn_modifySession */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsesn_modifySession(
  TMWSESN *pSession,
  const MMBSESN_CONFIG *pConfig,
  TMWTYPES_ULONG configMask)
{
  MMBSESN *pMMBSession = (MMBSESN *)pSession;

  if((configMask & MMBSESN_CONFIG_SLAVE) != 0)
  {
    pSession->destAddress = pConfig->slaveAddress;
  }

  if((configMask & MMBSESN_CONFIG_ACTIVE) != 0)
  {
    pSession->active = pConfig->active;
  }

  if((configMask & MMBSESN_CONFIG_RESP_TIMEOUT) != 0)
  {
    pMMBSession->defaultResponseTimeout = pConfig->defaultResponseTimeout;
  }

  return(TMWDEFS_TRUE);
}

/* function: mmbsesn_closeSession */
void TMWDEFS_GLOBAL mmbsesn_closeSession(
  TMWSESN *pSession)
{
  TMWCHNL *pChannel;
  MMBSESN *pMMBSession = (MMBSESN *)pSession;

  /* Check for NULL since this would be a common error */
  if(pSession == TMWDEFS_NULL)
  {
    return;
  }

  pChannel = (TMWCHNL *)pSession->pChannel;

  /* Lock channel */
  TMWTARG_LOCK_SECTION(&pChannel->lock);

  /* Close generic Modbus session */
  mbsesn_closeSession(pSession);

  /* remove any requests for this session from the request queue */
  mbchnl_deleteMessages(pChannel, pSession);

  mmbdata_close(pMMBSession->mb.pDbHandle);

  /* Free memory */
  mmbmem_free(pMMBSession);

  /* Unlock channel */
  TMWTARG_UNLOCK_SECTION(&pChannel->lock);
}

static TMWTYPES_BOOL TMWDEFS_LOCAL _validateRcvdLength(TMWSESN_RX_DATA *pRxFrame)
{
  TMWTYPES_USHORT msgLength = pRxFrame->msgLength;

  if(msgLength < 1)
    return TMWDEFS_FALSE;

  switch(pRxFrame->pMsgBuf[0])
    {
    /* diagnostics will be further checked when processed */
    case MBDEFS_FC_DIAGNOSTICS:
      if(msgLength < 3)
        return TMWDEFS_FALSE; 

    case MBDEFS_FC_READ_COILS:
    case MBDEFS_FC_READ_DISCRETE_INPUTS:
    case MBDEFS_FC_READ_HOLDING_REGISTERS:
    case MBDEFS_FC_READ_READ_INPUT_REGISTERS:
    case MBDEFS_FC_WRITE_SINGLE_COIL:
    case MBDEFS_FC_WRITE_SINGLE_REGISTER:
      if(msgLength < 5)
        return TMWDEFS_FALSE;

    case MBDEFS_FC_WRITE_MULTIPLE_COILS:
    case MBDEFS_FC_WRITE_MULTIPLE_HOLDING_REGISTERS:
      if(msgLength < 6)
        return TMWDEFS_FALSE;
       
    case MBDEFS_FC_READ_WRITE_MULTIPLE_REGISTERS:
      if(msgLength < 10)
        return TMWDEFS_FALSE;

    case MBDEFS_FC_43_ENCAPSULATED:
      if(msgLength < 2)
        return TMWDEFS_FALSE;
      
    /* MBDEFS_FC_READ_EXCEPTION_STATUS has a length of 1 
     * so it was checked at the start of this function
     */
    default:
      return TMWDEFS_TRUE;
    } 
   
  return TMWDEFS_TRUE;
}


/* function: _processFrame */
static TMWTYPES_BOOL TMWDEFS_GLOBAL _processFrame(
  TMWSESN *pSession,
  TMWSESN_RX_DATA *pRxFrame)
{
  MMBSESN *pMMBSession;
  MBCHNL_TX_DATA *pRequest;
  TMWTYPES_UCHAR fc;
  TMWTYPES_BOOL rtnVal;

  /* every response should have at least 2 bytes */
  if(pRxFrame->msgLength < 2)
  {
    mbchnl_discardInvLen(pSession);
    return(TMWDEFS_FALSE);
  }
  
  pMMBSession = (MMBSESN *)pSession;
  pRequest = (MBCHNL_TX_DATA *)pSession->pChannel->pCurrentMessage;
  fc = pRxFrame->pMsgBuf[0];

#ifdef TMW_SUPPORT_MONITOR
  /* If in analyzer or listen only mode, dont check request of function code */
  if(pSession->pChannel->pPhysContext->monitorMode)
  {
    /* if it is an exception response, simply log error */
    if(fc & 0x80)
    {
      char buf[128];
      tmwtarg_snprintf(buf, sizeof(buf), "Exception response, FC = 0x%02x, Exception Code = 0x%02x",
        fc, pRxFrame->pMsgBuf[1]);
      MBDIAG_ERROR(pSession->pChannel, pSession, buf);
    }
    return(TMWDEFS_TRUE);
  }
#endif

  if(pRequest == TMWDEFS_NULL)
  {
#if TMWCNFG_SUPPORT_DIAG
    MBDIAG_ERROR(pSession->pChannel, pSession, "Response received with no outstanding request");
#endif
    return(TMWDEFS_TRUE);
  }

  /* make sure FC sent was same as FC received, less error bit */
  if(pRequest->tmw.pMsgBuf[0] != (fc & 0x7F))
  {
#if TMWCNFG_SUPPORT_DIAG
    MBDIAG_ERROR(pSession->pChannel, pSession, "Unexpected response function code");
#endif
    return(TMWDEFS_TRUE);
  } 
  
  /* if it is an exception response, simply log error */
  if(fc & 0x80)
  {
#if TMWCNFG_SUPPORT_DIAG
    char buf[128];
    tmwtarg_snprintf(buf, sizeof(buf), "Exception response, FC = 0x%02x, Exception Code = 0x%02x",
            fc, pRxFrame->pMsgBuf[1]);
    MBDIAG_ERROR(pSession->pChannel, pSession, buf);
#endif
    mbchnl_cleanupMessage((TMWSESN_TX_DATA*)pRequest, 
      pRxFrame, MBCHNL_RESP_STATUS_FAILURE, 0);

    return(TMWDEFS_TRUE);
  }

  /* process response */
  switch(fc)
  {
  case MBDEFS_FC_READ_COILS:
      rtnVal = _processReadCoils(pSession, pRequest, pRxFrame);
    break;
  case MBDEFS_FC_READ_DISCRETE_INPUTS:
      rtnVal = _processReadDiscreteInputs(pSession, pRequest, pRxFrame);
    break;
  case MBDEFS_FC_READ_HOLDING_REGISTERS:
      rtnVal = _processReadHoldingRegisters(pSession, pRequest, pRxFrame);
    break;
  case MBDEFS_FC_READ_READ_INPUT_REGISTERS:
      rtnVal = _processReadInputRegisters(pSession, pRequest, pRxFrame);
    break;
  case MBDEFS_FC_WRITE_SINGLE_COIL:
      rtnVal = _processWriteSingleCoil(pSession, pRequest, pRxFrame);
    break;
  case MBDEFS_FC_WRITE_SINGLE_REGISTER:
      rtnVal = _processWriteSingleRegister(pSession, pRequest, pRxFrame);
    break;
  case MBDEFS_FC_READ_EXCEPTION_STATUS:
      rtnVal = _processReadExceptionStatus(pSession, pRequest, pRxFrame);
    break;
  case MBDEFS_FC_DIAGNOSTICS:
      rtnVal = _processDiagnostics(pSession, pRequest, pRxFrame);
    break;
  case MBDEFS_FC_WRITE_MULTIPLE_COILS:
      rtnVal = _processWriteMultipleCoils(pSession, pRequest, pRxFrame);
    break;
  case MBDEFS_FC_WRITE_MULTIPLE_HOLDING_REGISTERS:
      rtnVal = _processWriteMultipleHoldingRegisters(pSession, pRequest, pRxFrame);
    break;
  case MBDEFS_FC_READ_WRITE_MULTIPLE_REGISTERS:
      rtnVal = _processReadWriteMultipleRegisters(pSession, pRequest, pRxFrame);
    break;
  case MBDEFS_FC_43_ENCAPSULATED:
      /* only read device id is currently supported */
      rtnVal = _processReadDeviceId(pSession, pRequest, pRxFrame);
    break;
  default:
#if TMWCNFG_SUPPORT_DIAG
    MBDIAG_ERROR(pRequest->tmw.pChannel, pSession, "Unsupported function code");
#endif
    rtnVal = TMWDEFS_FALSE;
  }

  return(rtnVal);
}

