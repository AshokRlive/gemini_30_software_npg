/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/
/* file: mbmem.c
 * description:  Implementation of Master Modbus specific memory allocation functions.
 */
#include "tmwscl/modbus/mbdiag.h"
#include "tmwscl/utils/tmwtarg.h"
#include "tmwscl/modbus/mbmem.h"
#include "tmwscl/modbus/mbchnl.h"
#include "tmwscl/modbus/mblink.h"

typedef  struct MBmemLinkContext
{
  TMWMEM_HEADER              header;
  MBLINK_CONTEXT             data;
} MBMEM_LINK_CONTEXT;

typedef struct MBmemChnlTxData 
{
  TMWMEM_HEADER              header;
  MBCHNL_TX_DATA             data;
} MBMEM_CHNL_TX_DATA;

static const TMWTYPES_CHAR *_nameTable[MBMEM_ALLOC_TYPE_MAX] = {
  "MBLINK_CONTEXT",
  "MBCHNL_TX_DATA"
};

#if !TMWCNFG_USE_DYNAMIC_MEMORY
/* Use static allocated memory instead of dynamic memory */
static MBMEM_LINK_CONTEXT   mbmem_linkContexts[MBCNFG_NUMALLOC_LINK_CONTEXTS]; 
static MBMEM_CHNL_TX_DATA   mbmem_txDatas[MBCNFG_NUMALLOC_CHNL_TX_DATAS]; 
#endif

static TMWMEM_POOL_STRUCT  _mbmemAllocTable[MBMEM_ALLOC_TYPE_MAX];

#if TMWCNFG_USE_DYNAMIC_MEMORY

void TMWDEFS_GLOBAL mbmem_initConfig(
  MBMEM_CONFIG *pConfig)
{
  pConfig->numLinkContexts = MBCNFG_NUMALLOC_LINK_CONTEXTS;
  pConfig->numTxDatas      = MBCNFG_NUMALLOC_CHNL_TX_DATAS;
}
#endif
 
/* routine: mbmem_init */
TMWTYPES_BOOL TMWDEFS_GLOBAL mbmem_init(
  MBMEM_CONFIG  *pConfig)
{
#if TMWCNFG_USE_DYNAMIC_MEMORY
  /* dynamic memory allocation supported */
  MBMEM_CONFIG  config; 

  /* If caller has not specified memory pool configuration, use the
   * default compile time values 
   */
  if(pConfig == TMWDEFS_NULL)
  {
    pConfig = &config;
    mbmem_initConfig(pConfig);
  }

  if(!tmwmem_lowInit(_mbmemAllocTable, MBMEM_LINK_CONTEXT_TYPE, pConfig->numLinkContexts, sizeof(MBMEM_LINK_CONTEXT), TMWDEFS_NULL))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_mbmemAllocTable, MBMEM_CHNL_TX_DATA_TYPE, pConfig->numTxDatas,      sizeof(MBMEM_CHNL_TX_DATA), TMWDEFS_NULL))
    return TMWDEFS_FALSE;
#else
  /* static memory allocation supported */ 
  if(!tmwmem_lowInit(_mbmemAllocTable, MBMEM_LINK_CONTEXT_TYPE, MBCNFG_NUMALLOC_LINK_CONTEXTS, sizeof(MBMEM_LINK_CONTEXT), (TMWTYPES_UCHAR*)mbmem_linkContexts))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_mbmemAllocTable, MBMEM_CHNL_TX_DATA_TYPE, MBCNFG_NUMALLOC_CHNL_TX_DATAS, sizeof(MBMEM_CHNL_TX_DATA), (TMWTYPES_UCHAR*)mbmem_txDatas))
    return TMWDEFS_FALSE;
#endif
  return TMWDEFS_TRUE;
}

/* function: mbmem_alloc */
void * TMWDEFS_GLOBAL mbmem_alloc(
  MBMEM_ALLOC_TYPE type)
{
  if(type >= MBMEM_ALLOC_TYPE_MAX)
  {
    return(TMWDEFS_NULL);
  }

  return(tmwmem_lowAlloc(&_mbmemAllocTable[type]));
}

/* function: mbmem_free */
void TMWDEFS_GLOBAL mbmem_free(
  void *pBuf)
{    
  TMWMEM_HEADER *pHeader = TMWMEM_GETHEADER(pBuf);
  TMWTYPES_UCHAR   type = TMWMEM_GETTYPE(pHeader);

  if(type >= MBMEM_ALLOC_TYPE_MAX)
  {
    return;
  }

  tmwmem_lowFree(&_mbmemAllocTable[type], pHeader);
}

/* function: mbmem_getUsage */
TMWTYPES_BOOL TMWDEFS_GLOBAL mbmem_getUsage(
  TMWTYPES_UCHAR index,
  const TMWTYPES_CHAR **pName,
  TMWMEM_POOL_STRUCT *pStruct)
{
  if(index >= MBMEM_ALLOC_TYPE_MAX)
  {
    return(TMWDEFS_FALSE);
  }

  *pName = _nameTable[index];
  *pStruct = _mbmemAllocTable[index];
  return(TMWDEFS_TRUE);
}

