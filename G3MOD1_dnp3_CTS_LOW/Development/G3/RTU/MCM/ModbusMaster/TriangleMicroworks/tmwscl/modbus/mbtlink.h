/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: mbtlink.h
 * description: Modbus TCP Link Layer.
 */
#ifndef MBTLINK_DEFINED
#define MBTLINK_DEFINED

#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/utils/tmwchnl.h"

/* Offset of bytes in modbus TCP frame */
#define MBTLINK_TRANS_ID_BYTE_1  0
#define MBTLINK_TRANS_ID_BYTE_2  1
#define MBTLINK_PROT_ID_BYTE_1   2
#define MBTLINK_PROT_ID_BYTE_2   3
#define MBTLINK_LENGTH_BYTE_1    4
#define MBTLINK_LENGTH_BYTE_2    5
#define MBTLINK_UNIT_ID_BYTE     6
#define MBTLINK_FC_BYTE          7

typedef struct MBTLingConfig
{
  int reserved;
} MBTLINK_CONFIG;

#ifdef __cplusplus
extern "C" {
#endif

  /* function: mbtlink_initConfig   
   * purpose: Initialize a modbus TCP link configuration data structure
   * arguments:
   *  pConfig - pointer to modbus link configuration structure to be
   *   initialized.
   * returns:
   *  void
   */ 
  void TMWDEFS_GLOBAL mbtlink_initConfig(
    MBTLINK_CONFIG *pConfig);

  /* function: mbtlink_getNeededBytes
   * purpose: return the number of bytes required to finish the
   *  current block
   * arguments:
   *  pCallbackParam - callback data, contains link layer context
   * returns
   *  number of characters to read
   */
  TMWTYPES_USHORT TMWDEFS_CALLBACK mbtlink_getNeededBytes(
    void *pCallbackParam);

  /* function: mbtlink_parseBytes
   * purpose: parse incoming data
   * arguments:
   *  pCallbackParam - callback data, contains link layer context
   *  recvBuf - received characters
   *  numBytes - number of bytes   
   *  firstByteTime - time that first byte was received. Could be zero
   *   if target layer does not fill this in. In that case it will be calculated
   *   in this function.
   * returns
   *  void
   */
  void TMWDEFS_CALLBACK mbtlink_parseBytes(
    void *pCallbackParam,
    TMWTYPES_UCHAR *recvBuf,
    TMWTYPES_USHORT numBytes,
    TMWTYPES_MILLISECONDS firstByteTime);

  /* function: mbtlink_transmitFrame 
   * purpose: Transmit a frame on a modbus TCP link
   * arguments:
   *  pContext - pointer to a link context structure
   *  pTxDescriptor - pointer to a tx data structure
   * returns:
   *  void
   */
  void TMWDEFS_CALLBACK mbtlink_transmitFrame(
    void *pContext,
    TMWSESN_TX_DATA *pTxDescriptor);

#ifdef __cplusplus
}
#endif
#endif /* MBTLINK_DEFINED */
