/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: mmbdata.c
 * description: This file defines the interface between the Triangle
 *  MicroWorks, Inc. Modbus master source code library and the target database.
 *  The default implementation calls methods in the Modbus Master Database
 *  simulator. These need to be repaced with code to interface with the
 *  device's database.
 *
 * notes: boolean data for Modbus (i.e. Discrete Inputs and Coils) is passed in a 
 *        packed byte format, with the value for the first requested item located
 *        in the lsb of the first byte and continuing into subsequent bytes; for 
 *        example, if coils 5 - 16 (0 based addressing) are to be retrieved/stored
 *        then the data is passed as follows:
 *          _______________________________
 *         |c12|c11|c10|c09|c08|c07|c06|c05|    Byte 0
 *          -------------------------------
 *         | x | x | x | x |c16|c15|c14|c13|    Byte 1
 *          -------------------------------
 *        note that the data is not aligned on 8 bit boundries, even though the 
 *        target database may actually require this; the routine implmentation
 *        should take care to retrive and store the data appropriately; this is 
 *        done for efficiency, since the MB protocol always passes boolean data
 *        this way.
 */

#include "tmwscl/modbus/mbdiag.h"
#include "tmwscl/utils/tmwtarg.h"
#include "tmwscl/utils/tmwdb.h"
#include "tmwscl/modbus/mmbdata.h"
#include "tmwscl/modbus/mmbsim.h"

/* XXX Changed by Lucy */
typedef enum
{
    REG_TYPE_COIL_STATUS        = 0,
    REG_TYPE_INPUT_STATUS,

    REG_TYPE_HOLDING_REGISTERS,
    REG_TYPE_INPUT_REGISTERS
}REG_TYPE;

/* XXX Changed by Lucy */
typedef void (*UpdateregCallbackPTR)( void *pHandle,
                TMWTYPES_USHORT startAddr,
                TMWTYPES_USHORT quantity,
                TMWTYPES_USHORT *pValueArray,
                REG_TYPE regType);

typedef void (*readRegCmdValueCallbackPTR)( void *pHandle,
                TMWTYPES_USHORT startAddr,
                TMWTYPES_USHORT quantity,
                TMWTYPES_USHORT *pValueArray,
                REG_TYPE regType);


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
/* XXX Changed by Lucy */
/* callback function definition goes here */
/*this callback function is used for updating registers which are in the channel
 * list/Database.
 */
extern void UpdateRegCallback( void *pHandle,
                TMWTYPES_USHORT startAddr,
                TMWTYPES_USHORT quantity,
                TMWTYPES_USHORT *pValueArray,
                REG_TYPE regType);

/*this callback function is used for updating registers whose requests are made
 * from the MCMconsole.
 */
extern void readRegCmdValueCallback( void *pHandle,
                TMWTYPES_USHORT startAddr,
                TMWTYPES_USHORT quantity,
                TMWTYPES_USHORT *pValueArray,
                REG_TYPE regType);

#if TMWCNFG_USE_MANAGED_SCL
#undef TMWCNFG_USE_SIMULATED_DB
#define TMWCNFG_USE_SIMULATED_DB TMWDEFS_FALSE
#endif

#if TMWCNFG_USE_MANAGED_SCL
#include "tmwscl/.NET/TMW.SCL/MMBDataBaseWrapper.h"
#endif

/* function: mmbdata_init */
void * TMWDEFS_GLOBAL mmbdata_init(
  TMWSESN *pSession, 
  void *pUserHandle)
{
#if TMWCNFG_USE_SIMULATED_DB
  TMWTARG_UNUSED_PARAM(pUserHandle);
  return(mmbsim_init(pSession));
#elif TMWCNFG_USE_MANAGED_SCL
  return (MMBDatabaseWrapper_Init(pSession, pUserHandle));
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pSession);
  /* XXX Added by Lucy */
  return(pUserHandle);
#endif
}

/* function: mmbdata_close */
void TMWDEFS_GLOBAL mmbdata_close(
  void *pHandle)
{
#if TMWCNFG_USE_SIMULATED_DB
  mmbsim_close(pHandle);
#elif TMWCNFG_USE_MANAGED_SCL
  MMBDatabaseWrapper_Close(pHandle);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
#endif
}

/* function: mmbdata_storeCoils */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbdata_storeCoils(
  void *pHandle,
  TMWTYPES_USHORT startAddr,
  TMWTYPES_USHORT quantity,
  TMWTYPES_UCHAR *pValueArray)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(mmbsim_storeCoils(pHandle, startAddr, quantity, pValueArray));
#elif TMWCNFG_USE_MANAGED_SCL
  return (MMBDatabaseWrapper_StoreCoils(pHandle, startAddr, quantity, pValueArray));
#else
  /* XXX Added by Lucy */
  UpdateregCallbackPTR updateHregPTR=UpdateRegCallback;
  (*updateHregPTR)(pHandle,startAddr,quantity,pValueArray, REG_TYPE_COIL_STATUS);

  /*process read request from console*/
  readRegCmdValueCallbackPTR readRegCmdValueCbPtr=readRegCmdValueCallback;
  (*readRegCmdValueCbPtr)(pHandle,startAddr,quantity,pValueArray,REG_TYPE_COIL_STATUS);
  return(TMWDEFS_FALSE);
#endif
}

/* function: mmbdata_storeDiscreteInputs */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbdata_storeDiscreteInputs(
  void *pHandle,
  TMWTYPES_USHORT startAddr,
  TMWTYPES_USHORT quantity,
  TMWTYPES_UCHAR *pValueArray)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(mmbsim_storeDiscreteInputs(pHandle, startAddr, quantity, pValueArray));
#elif TMWCNFG_USE_MANAGED_SCL
  return (MMBDatabaseWrapper_StoreDiscreteInputs(pHandle, startAddr, quantity, pValueArray));
#else
  /* XXX Added by Lucy */
  UpdateregCallbackPTR updateHregPTR=UpdateRegCallback;
  (*updateHregPTR)(pHandle,startAddr,quantity,pValueArray,REG_TYPE_INPUT_STATUS);
  
  /*process read request from console*/
  readRegCmdValueCallbackPTR readRegCmdValueCbPtr=readRegCmdValueCallback;
  (*readRegCmdValueCbPtr)(pHandle,startAddr,quantity,pValueArray,REG_TYPE_INPUT_STATUS);
 
  return(TMWDEFS_FALSE);
#endif
}

/* function: mmbdata_storeInputRegisters */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbdata_storeInputRegisters(
  void *pHandle,
  TMWTYPES_USHORT startAddr,
  TMWTYPES_USHORT quantity,
  TMWTYPES_USHORT *pValueArray)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(mmbsim_storeInputRegisters(pHandle, startAddr, quantity, pValueArray));
#elif TMWCNFG_USE_MANAGED_SCL
  return (MMBDatabaseWrapper_StoreInputRegisters(pHandle, startAddr, quantity, pValueArray));
#else
    /* XXX Added by Lucy */
    UpdateregCallbackPTR updateHregPTR=UpdateRegCallback;
    (*updateHregPTR)(pHandle,startAddr,quantity,pValueArray,REG_TYPE_INPUT_REGISTERS);

    /*process read request from console*/
    readRegCmdValueCallbackPTR readRegCmdValueCbPtr=readRegCmdValueCallback;
    (*readRegCmdValueCbPtr)(pHandle,startAddr,quantity,pValueArray,REG_TYPE_INPUT_REGISTERS);
  return(TMWDEFS_FALSE);
#endif
}

/* function: mmbdata_storeHoldingRegisters */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbdata_storeHoldingRegisters(
  void *pHandle,
  TMWTYPES_USHORT startAddr,
  TMWTYPES_USHORT quantity,
  TMWTYPES_USHORT *pValueArray)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(mmbsim_storeHoldingRegisters(pHandle, startAddr, quantity, pValueArray));
#elif TMWCNFG_USE_MANAGED_SCL
  return (MMBDatabaseWrapper_StoreHoldingRegisters(pHandle, startAddr, quantity, pValueArray));
#else
  /* XXX Added by Lucy */
  UpdateregCallbackPTR updateHregPTR=UpdateRegCallback;
  (*updateHregPTR)(pHandle,startAddr,quantity,pValueArray,REG_TYPE_HOLDING_REGISTERS);

  /*process read request from console*/
  readRegCmdValueCallbackPTR readRegCmdValueCbPtr=readRegCmdValueCallback;
  (*readRegCmdValueCbPtr)(pHandle,startAddr,quantity,pValueArray,REG_TYPE_HOLDING_REGISTERS);
  return(TMWDEFS_FALSE);
#endif
}

/* function: mmbdata_storeExceptionStatus */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbdata_storeExceptionStatus(
  void *pHandle,
  TMWTYPES_UCHAR value)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(mmbsim_storeExceptionStatus(pHandle, value));
#elif TMWCNFG_USE_MANAGED_SCL
  return (MMBDatabaseWrapper_StoreExceptionStatus(pHandle, value));
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(value);
  return(TMWDEFS_FALSE);
#endif
}

/* function: mmbdata_storeExceptionStatus */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbdata_storeDiagnosticResponse(
  void *pHandle,
  TMWTYPES_USHORT subFunction,
  TMWTYPES_UCHAR length,
  TMWTYPES_UCHAR *pData)
{
#if TMWCNFG_USE_SIMULATED_DB
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(subFunction);
  TMWTARG_UNUSED_PARAM(length);
  TMWTARG_UNUSED_PARAM(pData);
  return(TMWDEFS_FALSE);
#elif TMWCNFG_USE_MANAGED_SCL
  return (MMBDatabaseWrapper_StoreDiagnosticResponse(pHandle, subFunction, length, pData));
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(subFunction);
  TMWTARG_UNUSED_PARAM(length);
  TMWTARG_UNUSED_PARAM(pData);
  return(TMWDEFS_FALSE);
#endif
}

/* function: mmbdata_storeDeviceId */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbdata_storeDeviceId(
  void *pHandle,
  TMWTYPES_UCHAR objectId,
  TMWTYPES_UCHAR length,
  TMWTYPES_UCHAR *pData)
{
#if TMWCNFG_USE_SIMULATED_DB 
  return (mmbsim_storeDeviceId(pHandle, objectId, length, pData));
#elif TMWCNFG_USE_MANAGED_SCL
  return (MMBDatabaseWrapper_StoreDeviceId(pHandle, objectId, length, pData));
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(objectId);
  TMWTARG_UNUSED_PARAM(length);
  TMWTARG_UNUSED_PARAM(pData);
  return(TMWDEFS_FALSE);
#endif
}


