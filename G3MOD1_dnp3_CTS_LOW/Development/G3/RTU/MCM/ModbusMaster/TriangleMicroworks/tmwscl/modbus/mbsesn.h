/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: mbsesn.h
 * description: This file is intended for internal SCL use only.
 *   Definition of a generic Modbus session.
 */
#ifndef MBSESN_DEFINED
#define MBSESN_DEFINED

#include "tmwscl/utils/tmwsesn.h"
#include "tmwscl/utils/tmwdlist.h"
#include "tmwscl/utils/tmwtimer.h"
#include "tmwscl/utils/tmwchnl.h"
#include "tmwscl/modbus/mbdefs.h"

/* Modbus Context */
typedef void          (*MBSESN_PROCESS_INFO_FUNC) (TMWSESN *pSession, TMWSCL_INFO sesnInfo);
typedef TMWTYPES_BOOL (*MBSESN_PROCESS_FRAME_FUNC)(TMWSESN *pSession, TMWSESN_RX_DATA *pRxData);

typedef struct MBSessionStruct {

  /* Generic TMW session, must be first field */
  TMWSESN tmw;

  void *pDbHandle;

  /* Function to process received frame */
  MBSESN_PROCESS_FRAME_FUNC  pProcessFrameFunc;

} MBSESN;

#ifdef __cplusplus
extern "C" {
#endif

  TMWTYPES_BOOL TMWDEFS_GLOBAL mbsesn_openSession(
    TMWCHNL *pChannel,
    TMWSESN *pSession,
    TMWSESN_STAT_CALLBACK pCallback,
    void *pCallbackParam,
    TMWTYPES_PROTOCOL protocol,
    TMWTYPES_SESSION_TYPE type);

  void TMWDEFS_GLOBAL mbsesn_closeSession(
    TMWSESN *pSession);

#ifdef __cplusplus
}
#endif

#endif /* MBSESN_DEFINED */
