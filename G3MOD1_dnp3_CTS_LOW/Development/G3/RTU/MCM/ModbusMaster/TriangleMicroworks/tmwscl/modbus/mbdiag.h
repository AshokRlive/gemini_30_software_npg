/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: mbdiag.h
 * description: Generic MB diagnostics
 */
#ifndef MBDIAG_DEFINED
#define MBDIAG_DEFINED

#include "tmwscl/utils/tmwcnfg.h"
#include "tmwscl/utils/tmwchnl.h"
#include "tmwscl/utils/tmwsesn.h"
#include "tmwscl/utils/tmwdtime.h"
#include "tmwscl/modbus/mbchnl.h"

#if !TMWCNFG_SUPPORT_DIAG

#define MBDIAG_INSERT_QUEUE(pChannel, pSession, description) \
  TMWTARG_UNUSED_PARAM(pSession); TMWTARG_UNUSED_PARAM(description);

#define MBDIAG_REMOVE_MESSAGE(pTxData, status) \
  TMWTARG_UNUSED_PARAM(pTxData); TMWTARG_UNUSED_PARAM(status);

#define MBDIAG_SHOW_MESSAGE(pSession, fc) \
  TMWTARG_UNUSED_PARAM(pSession); TMWTARG_UNUSED_PARAM(fc);

#define MBDIAG_SHOW_COIL(pSession, point, value) \
  TMWTARG_UNUSED_PARAM(pSession); TMWTARG_UNUSED_PARAM(point); TMWTARG_UNUSED_PARAM(value);

#define MBDIAG_SHOW_DISCRETE_INPUT(pSession, point, value) \
  TMWTARG_UNUSED_PARAM(pSession); TMWTARG_UNUSED_PARAM(point); TMWTARG_UNUSED_PARAM(value);

#define MBDIAG_SHOW_INPUT_REGISTER(pSession, point, value) \
  TMWTARG_UNUSED_PARAM(pSession); TMWTARG_UNUSED_PARAM(point); TMWTARG_UNUSED_PARAM(value);

#define MBDIAG_SHOW_HOLDING_REGISTER(pSession, point, value) \
  TMWTARG_UNUSED_PARAM(pSession); TMWTARG_UNUSED_PARAM(point); TMWTARG_UNUSED_PARAM(value);

#define MBDIAG_SHOW_EXCEPT_STATUS(pSession, value) \
  TMWTARG_UNUSED_PARAM(pSession); TMWTARG_UNUSED_PARAM(value);

#define MBDIAG_SHOW_DIAGS_RESPONSE(pSession, subFunction, numBytes, pBuf) \
  TMWTARG_UNUSED_PARAM(pSession); TMWTARG_UNUSED_PARAM(subFunction); TMWTARG_UNUSED_PARAM(numBytes); TMWTARG_UNUSED_PARAM(pBuf);

#define MBDIAG_SHOW_DEVICEID(pSession, objectId, numBytes, pBuf) \
  TMWTARG_UNUSED_PARAM(pSession); TMWTARG_UNUSED_PARAM(objectId); TMWTARG_UNUSED_PARAM(numBytes); TMWTARG_UNUSED_PARAM(pBuf);
 
#define MBDIAG_SHOW_DEVICEIDDATA(pSession, moreFollows, nextObjectId, conformityLevel) \
  TMWTARG_UNUSED_PARAM(pSession); TMWTARG_UNUSED_PARAM(moreFollows); TMWTARG_UNUSED_PARAM(nextObjectId); TMWTARG_UNUSED_PARAM(conformityLevel);

#define MBDIAG_FRAME_SENT(pChannel, pSession, pFrame, numBytes) \
  TMWTARG_UNUSED_PARAM(pChannel); TMWTARG_UNUSED_PARAM(pSession); TMWTARG_UNUSED_PARAM(pFrame); TMWTARG_UNUSED_PARAM(numBytes);

#define MBDIAG_FRAME_RECEIVED(pChannel, pSession, pFrame, numBytes) \
  TMWTARG_UNUSED_PARAM(pChannel); TMWTARG_UNUSED_PARAM(pSession); TMWTARG_UNUSED_PARAM(pFrame); TMWTARG_UNUSED_PARAM(numBytes);

#define MBDIAG_LINK_FRAME_SENT(pChannel, pSession, pFrame, numBytes) \
  TMWTARG_UNUSED_PARAM(pChannel); TMWTARG_UNUSED_PARAM(pSession); TMWTARG_UNUSED_PARAM(pFrame); TMWTARG_UNUSED_PARAM(numBytes);

#define MBDIAG_LINK_FRAME_RECEIVED(pChannel, pSession, pFrame, numBytes) \
  TMWTARG_UNUSED_PARAM(pChannel); TMWTARG_UNUSED_PARAM(pSession); TMWTARG_UNUSED_PARAM(pFrame); TMWTARG_UNUSED_PARAM(numBytes);

#define MBDIAG_ERROR(pChannel, pSession, errorString) \
  TMWTARG_UNUSED_PARAM(pChannel); TMWTARG_UNUSED_PARAM(pSession); TMWTARG_UNUSED_PARAM(errorString);

#define MBDIAG_RESPONSE_ERROR(pRequest, exceptionCode) \
  TMWTARG_UNUSED_PARAM(pRequest); TMWTARG_UNUSED_PARAM(exceptionCode);

#else

#define MBDIAG_INSERT_QUEUE(pChannel, pSession, description) \
  mbdiag_insertQueue(pChannel, pSession, description)

#define MBDIAG_REMOVE_MESSAGE(pTxData, status) \
  mbdiag_removeMessage(pTxData, status)

#define MBDIAG_SHOW_MESSAGE(pSession, pHdr) \
  mbdiag_showObjectHeader(pSession, pHdr)

#define MBDIAG_SHOW_COIL(pSession, point, value) \
  mbdiag_showCoil(pSession, point, value)

#define MBDIAG_SHOW_DISCRETE_INPUT(pSession, point, value) \
  mbdiag_showDiscreteInput(pSession, point, value)

#define MBDIAG_SHOW_INPUT_REGISTER(pSession, point, value) \
  mbdiag_showInputRegister(pSession, point, value)

#define MBDIAG_SHOW_HOLDING_REGISTER(pSession, point, value) \
  mbdiag_showHoldingRegister(pSession, point, value)

#define MBDIAG_SHOW_EXCEPT_STATUS(pSession, value) \
  mbdiag_showExceptionStatus(pSession, value)

#define MBDIAG_SHOW_DIAGS_RESPONSE(pSession, subFunction, numBytes, pBuf) \
  mbdiag_showDiagsResponse(pSession, subFunction, numBytes, pBuf)

#define MBDIAG_SHOW_DEVICEID(pSession, objectId, numBytes, pBuf) \
  mbdiag_showDeviceId(pSession, objectId, numBytes, pBuf)

#define MBDIAG_SHOW_DEVICEIDDATA(pSession, moreFollows, nextObjectId, conformityLevel) \
  mbdiag_showDeviceIdData(pSession, moreFollows, nextObjectId, conformityLevel)

#define MBDIAG_FRAME_SENT(pChannel, pSession, pFrame, numBytes) \
  mbdiag_frameSent(pChannel, pSession, pFrame, numBytes)

#define MBDIAG_FRAME_RECEIVED(pChannel, pSession, pFrame, numBytes) \
  mbdiag_frameReceived(pChannel, pSession, pFrame, numBytes)

#define MBDIAG_LINK_FRAME_SENT(pChannel, pSession, pFrame, numBytes) \
  mbdiag_linkFrameSent(pChannel, pSession, pFrame, numBytes)

#define MBDIAG_LINK_FRAME_RECEIVED(pChannel, pSession, pFrame, numBytes) \
  mbdiag_linkFrameReceived(pChannel, pSession, pFrame, numBytes)

#define MBDIAG_ERROR(pChannel, pSession, errorString) \
  mbdiag_error(pChannel, pSession, errorString)

#define MBDIAG_RESPONSE_ERROR(pRequest, exceptionCode) \
  mbdiag_response_error(pRequest, exceptionCode)

#define _MBDIAG_SEND_MSG    1
#define _MBDIAG_RECEIVE_MSG 2

#ifdef __cplusplus
extern "C" {
#endif

  /* function: mbdiag_insertQueue
   * purpose:
   * arguments:
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL mbdiag_insertQueue(
    TMWCHNL *pChannel,
    TMWSESN *pSession, 
    const char *description);

  /* function: mbdiag_removeMessage
   * purpose:
   * arguments:
   * returns:
   *  void
   */
  void mbdiag_removeMessage(
    TMWSESN_TX_DATA *pTxData, 
    MBCHNL_RESP_STATUS status);

  /* function: mbdiag_showCoil
   * purpose: display binary Coil data
   * arguments:
   *  point - point number
   *  value - value of coil
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL mbdiag_showCoil(
    TMWSESN *pSession,
    TMWTYPES_USHORT point,
    TMWTYPES_USHORT value);

  /* function: mbdiag_showDiscreteInput
   * purpose: display binary Input data
   * arguments:
   *  point - point number
   *  value - value of discrete input
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL mbdiag_showDiscreteInput(
    TMWSESN *pSession,
    TMWTYPES_USHORT point,
    TMWTYPES_USHORT value);

  /* function: mbdiag_showInputRegister
   * purpose: display input register data
   * arguments:
   *  point - point number
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL mbdiag_showInputRegister(
    TMWSESN *pSession,
    TMWTYPES_USHORT point,
    TMWTYPES_USHORT value);

  /* function: mbdiag_showHoldingRegister
   * purpose: display holding register data
   * arguments:
   *  point - point number
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL mbdiag_showHoldingRegister(
    TMWSESN *pSession,
    TMWTYPES_USHORT point,
    TMWTYPES_USHORT value);

  /* function: mbdiag_showExceptionStatus
   * purpose: display exception status value
   * arguments:
   *  pSession - pointer to session
   *  value - exception status value 
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL mbdiag_showExceptionStatus(
    TMWSESN *pSession,
    TMWTYPES_USHORT value);

  /* function: mbdiag_showDiagsResponse
   * purpose: display response from diagnostics request
   * arguments:
   *  pSession - pointer to session
   *  subFunction - sub-function requested
   *  numBytes - length in bytes of returned data
   *  pBuf - pointer to returned data
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL mbdiag_showDiagsResponse(
    TMWSESN *pSession,
    TMWTYPES_USHORT subFunction,
    TMWTYPES_UCHAR numBytes,
    const TMWTYPES_UCHAR *pBuf);
 
  /* function: mbdiag_showDeviceId
   * purpose: display device identification 
   * arguments:
   *  pSession - pointer to session
   *  objectId - object id 
   *  numBytes - length in bytes of data
   *  pBuf - pointer to returned data
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL mbdiag_showDeviceId(
    TMWSESN *pSession,
    TMWTYPES_UCHAR objectId,
    TMWTYPES_UCHAR numBytes,
    const TMWTYPES_UCHAR *pBuf);
 
  /* function: mbdiag_showDeviceIdData
   * purpose: display device identification data
   * arguments:
   *  pSession - pointer to session
   *  objectId - object id 
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL mbdiag_showDeviceIdData(
    TMWSESN *pSession,
    TMWTYPES_UCHAR moreFollows,
    TMWTYPES_UCHAR nextObjectId,
    TMWTYPES_UCHAR conformityLevel);

  /* function: mbdiag_frameSent
   * purpose: display transmitted application layer frame
   * arguments:
   *  channelName - name of channel
   *  pFrame - pointer to frame buffer
   *  numBytes - number of bytes in frame
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL mbdiag_frameSent(
    TMWCHNL *pChannel,
    TMWSESN *pSession,
    const TMWTYPES_UCHAR *pFrame, 
    TMWTYPES_USHORT numBytes);

  /* function: mbdiag_frameReceived
   * purpose: display received application layer frame
   * arguments:
   *  channelName - name of channel
   *  pFrame - pointer to frame buffer
   *  numBytes - number of bytes in frame
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL mbdiag_frameReceived(
    TMWCHNL *pChannel,
    TMWSESN *pSession,
    const TMWTYPES_UCHAR *pFrame, 
    TMWTYPES_USHORT numBytes);

  /* function: mbdiag_linkFrameSent
   * purpose: display link level frame
   * arguments:
   *  channelName - name of channel
   *  pFrame - pointer to frame buffer
   *  numBytes - number of bytes in frame
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL mbdiag_linkFrameSent(
    TMWCHNL *pChannel,
    TMWSESN *pSession,
    const TMWTYPES_UCHAR *pFrame, 
    TMWTYPES_USHORT numBytes);

  /* function: mbdiag_linkFrameReceived
   * purpose: display link level frame
   * arguments:
   *  channelName - name of channel
   *  pFrame - pointer to frame buffer
   *  numBytes - number of bytes in message
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL mbdiag_linkFrameReceived(
    TMWCHNL *pChannel,
    TMWSESN *pSession,
    const TMWTYPES_UCHAR *pFrame, 
    TMWTYPES_USHORT numBytes);

  /* function: mbdiag_error
   * purpose: Display error message
   * arguments:
   *  channelName - name of channel
   *  errorString - error message to display
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL mbdiag_error(
    TMWCHNL *pChannel,
    TMWSESN *pSession,
    const char *errorString);

  /* function: mbdiag_response_error
   * purpose: Display response error message
   * arguments:
   *  pRequest - request data
   *  pTxData  - transimt data
   *  exceptionCode  - error code to display
   * returns:
   *  void
   */
  void mbdiag_response_error(
    TMWSESN_RX_DATA *pRequest, 
    TMWTYPES_UCHAR exceptionCode);

#ifdef __cplusplus
}
#endif
#endif /* TMWCNFG_SUPPORT_DIAG */
#endif /* MBDIAG_DEFINED */
