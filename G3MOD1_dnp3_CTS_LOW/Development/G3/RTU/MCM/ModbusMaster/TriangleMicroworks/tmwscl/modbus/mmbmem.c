/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/
/* file: mmbmem.c
 * description:  Implementation of Master Modbus specific memory allocation functions.
 */
#include "tmwscl/modbus/mbdiag.h"
#include "tmwscl/modbus/mmbmem.h"
#include "tmwscl/modbus/mmbsim.h"
#include "tmwscl/modbus/mbchnl.h"
#include "tmwscl/modbus/mbsesn.h"
#include "tmwscl/modbus/mmbsesn.h"
#include "tmwscl/utils/tmwtarg.h"

typedef struct MMBmemMMBSesn
{
  TMWMEM_HEADER        header;
  MMBSESN              data;
} MMBMEM_MMBSESN;

#if TMWCNFG_USE_SIMULATED_DB
typedef struct MMBmemSimDatabase
{
  TMWMEM_HEADER        header;
  MMBSIM_DATABASE      data;
} MMBMEM_SIM_DATABASE;
#endif

static const TMWTYPES_CHAR *_nameTable[MMBMEM_ALLOC_TYPE_MAX] = {
  "MMBSESN"
#if TMWCNFG_USE_SIMULATED_DB
  ,"MMBSIM_DATABASE"
#endif
};

#if !TMWCNFG_USE_DYNAMIC_MEMORY
/* Use static allocated memory instead of dynamic memory */
static MMBMEM_MMBSESN            mmbmem_mmbsessions[MMBCNFG_NUMALLOC_SESNS]; 
#if TMWCNFG_USE_SIMULATED_DB
static MMBMEM_SIM_DATABASE       mmbmem_simDatabase[MMBCNFG_NUMALLOC_SIM_DATABASES]; 
#endif

#endif

static TMWMEM_POOL_STRUCT _mmbmemAllocTable[MMBMEM_ALLOC_TYPE_MAX];

#if TMWCNFG_USE_DYNAMIC_MEMORY
static void TMWDEFS_LOCAL _initConfig(
  MMBMEM_CONFIG *pConfig)
{
  pConfig->numSessions  = MMBCNFG_NUMALLOC_SESNS;; 
#if TMWCNFG_USE_SIMULATED_DB
  pConfig->numSimDbases = MMBCNFG_NUMALLOC_SIM_DATABASES;
#endif
}

void TMWDEFS_GLOBAL mmbmem_initConfig(
  MMBMEM_CONFIG *pMMBConfig,
  MBMEM_CONFIG  *pMBConfig,
  TMWMEM_CONFIG *pTmwConfig)
{
  tmwmem_initConfig(pTmwConfig);
  mbmem_initConfig(pMBConfig);
  _initConfig(pMMBConfig);
}

/* function: mmbmem_initMemory */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbmem_initMemory(
  MMBMEM_CONFIG *pMMBConfig,
  MBMEM_CONFIG  *pMBConfig,
  TMWMEM_CONFIG *pTmwConfig)
{ 
  /* Initialize memory management if not yet done */
  if(!tmwappl_getInitialized(TMWAPPL_INIT_MMB))
  {
    if(!mmbmem_init(pMMBConfig))
      return TMWDEFS_FALSE;

    tmwappl_setInitialized(TMWAPPL_INIT_MMB);
  }
    
  /* Initialize memory management if not yet done */
  if(!tmwappl_getInitialized(TMWAPPL_INIT_MB))
  {
    if(!mbmem_init(pMBConfig))
      return TMWDEFS_FALSE;

    tmwappl_setInitialized(TMWAPPL_INIT_MB);
  }
  return TMWDEFS_TRUE;
}
#endif

/* routine: mmbmem_init */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbmem_init(
  MMBMEM_CONFIG  *pConfig)
{
#if TMWCNFG_USE_DYNAMIC_MEMORY
  /* dynamic memory allocation supported */
  MMBMEM_CONFIG  config; 

  /* If caller has not specified memory pool configuration, use the
   * default compile time values 
   */
  if(pConfig == TMWDEFS_NULL)
  {
    pConfig = &config;
    _initConfig(pConfig);
  }

  if(!tmwmem_lowInit(_mmbmemAllocTable, MMBMEM_MMBSESN_TYPE,      pConfig->numSessions,  sizeof(MMBMEM_MMBSESN),      TMWDEFS_NULL))
    return TMWDEFS_FALSE;
 #if TMWCNFG_USE_SIMULATED_DB
  if(!tmwmem_lowInit(_mmbmemAllocTable, MMBMEM_SIM_DATABASE_TYPE, pConfig->numSimDbases, sizeof(MMBMEM_SIM_DATABASE), TMWDEFS_NULL))
    return TMWDEFS_FALSE;
#endif

#else
  /* static memory allocation supported */ 
  if(!tmwmem_lowInit(_mmbmemAllocTable, MMBMEM_MMBSESN_TYPE,      MMBCNFG_NUMALLOC_SESNS,         sizeof(MMBMEM_MMBSESN),      (TMWTYPES_UCHAR*)mmbmem_mmbsessions))
    return TMWDEFS_FALSE;
#if TMWCNFG_USE_SIMULATED_DB
  if(!tmwmem_lowInit(_mmbmemAllocTable, MMBMEM_SIM_DATABASE_TYPE, MMBCNFG_NUMALLOC_SIM_DATABASES, sizeof(MMBMEM_SIM_DATABASE), (TMWTYPES_UCHAR*)mmbmem_simDatabase))
    return TMWDEFS_FALSE;
#endif

#endif
  return TMWDEFS_TRUE;
}

/* function: mmbmem_alloc */
void * TMWDEFS_GLOBAL mmbmem_alloc(
  MMBMEM_ALLOC_TYPE type)
{
  if(type >= MMBMEM_ALLOC_TYPE_MAX)
  {
    return(TMWDEFS_NULL);
  }

  return(tmwmem_lowAlloc(&_mmbmemAllocTable[type]));
}

/* function: mmbmem_free */
void TMWDEFS_GLOBAL mmbmem_free(
  void *pBuf)
{    
  TMWMEM_HEADER *pHeader = TMWMEM_GETHEADER(pBuf);
  TMWTYPES_UCHAR   type = TMWMEM_GETTYPE(pHeader);

  if(type >= MMBMEM_ALLOC_TYPE_MAX)
  {
    return;
  }

  tmwmem_lowFree(&_mmbmemAllocTable[type], pHeader);
}

/* function: mmbmem_getUsage */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbmem_getUsage(
  TMWTYPES_UCHAR index,
  const TMWTYPES_CHAR **pName,
  TMWMEM_POOL_STRUCT *pStruct)
{
  if(index >= MMBMEM_ALLOC_TYPE_MAX)
  {
    return(TMWDEFS_FALSE);
  }

  *pName = _nameTable[index];
  *pStruct = _mmbmemAllocTable[index];
  return(TMWDEFS_TRUE);
}

