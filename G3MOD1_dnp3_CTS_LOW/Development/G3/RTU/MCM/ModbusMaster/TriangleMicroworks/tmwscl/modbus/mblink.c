/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: mblink.h
 * description: Modbus Link Layer.
 */
#include "tmwscl/modbus/mbdiag.h"
#include "tmwscl/modbus/mblink.h"
#include "tmwscl/modbus/mbmem.h"
#include "tmwscl/utils/tmwlink.h"
#include "tmwscl/utils/tmwtarg.h"

/* function: _getSessions
 * purpose: return list of sessions on this channel
 * arguments:
 *  pContext - link layer context
 * returns:
 *  pointer to list of sessions
 */
static TMWDLIST * TMWDEFS_CALLBACK _getSessions(
  TMWLINK_CONTEXT *pContext)
{
  return(tmwlink_getSessions(pContext));
}

/* function: _openSession
 * purpose: opens a new session on this channel
 * arguments:
 *  pContext - link layer context
 *  pSession - pointer to session to open
 *  pConfig - currently not used by modbus
 * returns
 *  TMWDEFS_TRUE if successful, else TMWDEFS_FALSE
 */
static TMWTYPES_BOOL TMWDEFS_CALLBACK _openSession(
  TMWLINK_CONTEXT *pContext,
  TMWSESN *pSession,
  void *pConfig)
{
  TMWTARG_UNUSED_PARAM(pConfig);

  /* Open channel if required */
  tmwlink_openChannel(pContext);

  /* Generic session initialization */
  if(!tmwlink_openSession(pContext, pSession))
  {
    return(TMWDEFS_FALSE);
  }

  /* If channel is already open tell application layer we are online */
  if(pContext->isOpen && (pContext->pInfoFunc != TMWDEFS_NULL))
  {
    pContext->pInfoFunc(
      pContext->pCallbackParam, pSession, TMWSCL_INFO_ONLINE);

    /* Session is online */
    tmwsesn_setOnline(pSession, TMWDEFS_TRUE);
  }

  return(TMWDEFS_TRUE);
}

/* function: _closeSession
 * purpose: closes specified session
 * arguments:
 *  pContext - link layer context
 *  pSession - pointer to session to close
 * returns
 *  TMWDEFS_TRUE if successful, else TMWDEFS_FALSE
 */
static TMWTYPES_BOOL TMWDEFS_CALLBACK _closeSession(
  TMWLINK_CONTEXT *pContext,
  TMWSESN *pSession)
{
  /* Protocol specific session shutdown */
  MBLINK_CONTEXT *pLinkContext = (MBLINK_CONTEXT *)pContext;

  /* Pass online info to application level */
  if(pLinkContext->tmw.pInfoFunc != TMWDEFS_NULL)
  {
    pLinkContext->tmw.pInfoFunc(pLinkContext->tmw.pCallbackParam,
      pSession, TMWSCL_INFO_OFFLINE);

    /* Session is offline */
    tmwsesn_setOnline(pSession, TMWDEFS_FALSE);
  }

  /* Generic session shutdown and return */
  return(tmwlink_closeSession(pContext, pSession));
}

/* function: _cancelFrame
 * purpose: cancel frame
 * arguments:
 *  pContext - link layer context returned from mblink_initChannel
 *  pTxDescriptor - data frame to cancel
 * returns
 *  void
 */
static void TMWDEFS_CALLBACK _cancelFrame(
  void *pContext,
  TMWSESN_TX_DATA *pTxDescriptor)
{
  /* implement this */
  TMWTARG_UNUSED_PARAM(pContext);
  TMWTARG_UNUSED_PARAM(pTxDescriptor);
}

/* function: _infoCallback
 * purpose: send TMWSCL_INFO to the layer above. If redundancy is not
 *  enabled, perform some basic retry strategy first to hide details
 *  from application layer. If redundancy is enabled just pass the
 *  information up and let it decide what to do.
 * arguments:
 *  pContext - pointer to link context structure
 *  pSession - pointer to session structure
 *  sclInfo - event such as OFFLINE
 * returns
 *  void
 */
static void TMWDEFS_CALLBACK _infoCallback(
  TMWLINK_CONTEXT *pContext,
  TMWCHNL *pChannel,
  TMWSCL_INFO sclInfo)
{
  MBLINK_CONTEXT *pLinkContext = (MBLINK_CONTEXT *)pContext;
  TMWSESN *pSession1 = TMWDEFS_NULL;
  TMWTARG_UNUSED_PARAM(pChannel);

  /* If there is an application layer info callback */
  if(pLinkContext->tmw.pInfoFunc != TMWDEFS_NULL)
  {
    switch(sclInfo)
    {
    case TMWSCL_INFO_OPENED:
      /* Send online to all current sessions */
      while((pSession1 = (TMWSESN *)tmwdlist_getAfter(
        &pContext->sessions, (TMWDLIST_MEMBER *)pSession1)) != TMWDEFS_NULL)
      {
        pLinkContext->tmw.pInfoFunc(
          pLinkContext->tmw.pCallbackParam,
          pSession1, TMWSCL_INFO_ONLINE);

        /* Session is online */
        tmwsesn_setOnline(pSession1, TMWDEFS_TRUE);
      }

      break;

    case TMWSCL_INFO_CLOSED:
      /* Send offline to all current sessions */
      while((pSession1 = (TMWSESN *)tmwdlist_getAfter(
        &pContext->sessions, (TMWDLIST_MEMBER *)pSession1)) != TMWDEFS_NULL)
      {
        pLinkContext->tmw.pInfoFunc(
          pLinkContext->tmw.pCallbackParam,
          pSession1, TMWSCL_INFO_OFFLINE);

        /* Session is offline */
        tmwsesn_setOnline(pSession1, TMWDEFS_FALSE);
      }

      /* Send channel closed to application layer */
      pLinkContext->tmw.pInfoFunc(
        pLinkContext->tmw.pCallbackParam,
        TMWDEFS_NULL, sclInfo);

      break;

    default:
      /* Something other than channel opened or closed, pass it on to application layer */
      pLinkContext->tmw.pInfoFunc(pLinkContext->tmw.pCallbackParam, TMWDEFS_NULL, sclInfo);
      break;
    }
  }
}

/* function: mblink_initConfig */
void TMWDEFS_GLOBAL mblink_initConfig(
  MBLINK_CONFIG *pConfig)
{
  pConfig->type = MBLINK_TYPE_ASCII;

  /* Unlimited number of requests may be queued */
  pConfig->maxQueueSize = 0;

  pConfig->rxFrameTimeout = TMWDEFS_SECONDS(4);
  pConfig->channelResponseTimeout = TMWDEFS_SECONDS(10);

#if MBCNFG_SUPPORT_ASCII
  mbalink_initConfig(&pConfig->alink);
#endif
#if MBCNFG_SUPPORT_RTU
  mbrlink_initConfig(&pConfig->rlink);
#endif
#if MBCNFG_SUPPORT_TCP
  mbtlink_initConfig(&pConfig->tlink);
#endif
#if MBCNFG_SUPPORT_MBP
  mbplink_initConfig(&pConfig->plink);
#endif
}

/* function: mblink_initChannel */
TMWTYPES_BOOL TMWDEFS_GLOBAL mblink_initChannel(
  TMWCHNL *pChannel,
  const MBLINK_CONFIG *pConfig)
{
  MBLINK_CONTEXT *pLinkContext;

  /* Allocate space for context */
  pLinkContext = (MBLINK_CONTEXT *)mbmem_alloc(MBMEM_LINK_CONTEXT_TYPE);

  pLinkContext->type = pConfig->type;

  pLinkContext->rxOffset = 0;
  pLinkContext->rxFrame.pMsgBuf = pLinkContext->rxBinaryFrameBuffer;

  
  pLinkContext->pUserTxCallback = TMWDEFS_NULL;
  pLinkContext->pCallbackParam = TMWDEFS_NULL;

  /* Set link layer points in channel */
  pLinkContext->tmw.pChannel = pChannel;
  pChannel->pLink = &pLinkContext->linkInterface;
  pChannel->pLinkContext = (TMWLINK_CONTEXT *)pLinkContext;
  pChannel->incrementalTimeout = pConfig->channelResponseTimeout;
  
  pLinkContext->physTxDescriptor.UDPPort = TMWTARG_UDP_NONE;

  pLinkContext->linkInterface.pLinkOpenSession = (TMWLINK_OPEN_SESSION_FUNC)_openSession;
  pLinkContext->linkInterface.pLinkCloseSession = (TMWLINK_CLOSE_SESSION_FUNC)_closeSession;
  pLinkContext->linkInterface.pLinkGetSessions = (TMWLINK_GET_SESSIONS_FUNC)_getSessions;
  pLinkContext->linkInterface.pLinkDataReady = (TMWLINK_DATA_READY_FUNC)TMWDEFS_NULL;
  pLinkContext->linkInterface.pLinkSetCallbacks = (TMWLINK_SET_CALLBACKS_FUNC)tmwlink_setCallbacks;
  pLinkContext->linkInterface.pLinkUpdateMsg = (TMWLINK_UPDATE_MSG_FUNC)TMWDEFS_NULL;
  pLinkContext->linkInterface.pLinkCancel = (TMWLINK_CANCEL_FUNC)_cancelFrame;

  pLinkContext->rxFrameTimeout = pConfig->rxFrameTimeout;
  tmwtimer_init(&pLinkContext->rxFrameTimer);

  switch(pConfig->type)
  {
#if MBCNFG_SUPPORT_TCP
  case MBLINK_TYPE_TCP:
    pLinkContext->linkInterface.pLinkTransmit = (TMWLINK_TRANSMIT_FUNC)mbtlink_transmitFrame;

#if MBCNFG_SUPPORT_TRANSACTION_ID
    pLinkContext->transactionId = 0;
#endif

    /* Initialize generic link layer info */
    tmwlink_initChannel(pChannel, 5000,
      tmwlink_channelCallback, mbtlink_getNeededBytes, mbtlink_parseBytes,
      _infoCallback, TMWDEFS_NULL);

    break;
#endif

#if MBCNFG_SUPPORT_ASCII
  case MBLINK_TYPE_ASCII:
    pLinkContext->linkInterface.pLinkTransmit = (TMWLINK_TRANSMIT_FUNC)mbalink_transmitFrame;

    pLinkContext->delim1 = pConfig->alink.delim1;
    pLinkContext->delim2 = pConfig->alink.delim2;

    /* Initialize generic link layer info */
    tmwlink_initChannel(pChannel, 5000,
      tmwlink_channelCallback, mbalink_getNeededBytes, mbalink_parseBytes,
      _infoCallback, TMWDEFS_NULL);
    break;
#endif

#if MBCNFG_SUPPORT_RTU
  case MBLINK_TYPE_RTU:
    pLinkContext->linkInterface.pLinkTransmit = (TMWLINK_TRANSMIT_FUNC)mbrlink_transmitFrame;

    /* Initialize generic link layer info */
    tmwlink_initChannel(pChannel, 5000,
      tmwlink_channelCallback, mbrlink_getNeededBytes, mbrlink_parseBytes,
      _infoCallback, TMWDEFS_NULL);

    break;
#endif

#if MBCNFG_SUPPORT_MBP
  case MBLINK_TYPE_PLUS:
    pLinkContext->linkInterface.pLinkTransmit = (TMWLINK_TRANSMIT_FUNC)mbplink_transmitFrame;

    /* Initialize generic link layer info */
    tmwlink_initChannel(pChannel, 5000,
      tmwlink_channelCallback, mbplink_getNeededBytes, mbplink_parseBytes,
      _infoCallback, TMWDEFS_NULL);

    break;
#endif

  default:
    return(TMWDEFS_FALSE);
  }

  /* Open channel (try to connect) if monitor mode */
#ifdef TMW_SUPPORT_MONITOR 
  if(pChannel->pPhysContext->monitorMode)
  {
    tmwlink_openChannel((TMWLINK_CONTEXT *)pLinkContext);
  }
#endif

  return(TMWDEFS_TRUE);
}

/* function: mblink_modifyChannel */
TMWTYPES_BOOL TMWDEFS_GLOBAL mblink_modifyChannel(
  TMWCHNL *pChannel,
  const MBLINK_CONFIG *pConfig,
  TMWTYPES_ULONG configMask)
{
  TMWTARG_UNUSED_PARAM(pChannel);
  TMWTARG_UNUSED_PARAM(pConfig);

  return(TMWDEFS_TRUE);
}

/* function: mblink_deleteChannel */
TMWTYPES_BOOL TMWDEFS_GLOBAL mblink_deleteChannel(
  TMWCHNL *pChannel)
{
  MBLINK_CONTEXT *pLinkContext = (MBLINK_CONTEXT *)pChannel->pLinkContext;

  /* Cleanup generic link layer info */
  if(tmwlink_deleteChannel(pChannel) == TMWDEFS_FALSE)
  {
    return(TMWDEFS_FALSE);
  }

  /* Free context */
  mbmem_free(pLinkContext);

  return(TMWDEFS_TRUE);
}

/* function: mblink_findSession */
TMWSESN * TMWDEFS_GLOBAL mblink_findSession(
  void *pContext,
  TMWTYPES_USHORT address)
{
  TMWLINK_CONTEXT *pLinkContext = (TMWLINK_CONTEXT *)pContext;
  TMWSESN *pSession = TMWDEFS_NULL;

  while((pSession = (TMWSESN *)tmwdlist_getAfter(
    &pLinkContext->sessions, (TMWDLIST_MEMBER *)pSession)) != TMWDEFS_NULL)
  {
    if(pSession->destAddress == address)
      return(pSession);
  }

  return(TMWDEFS_NULL);
}

#ifdef TMW_SUPPORT_MONITOR 

TMWSESN * TMWDEFS_GLOBAL mblink_monitorModeFindSession(
  TMWLINK_CONTEXT *pLinkContext,
  TMWTYPES_USHORT addr)
{
  TMWSESN *pSession;
  TMWCHNL_STAT_UNKNOWN_SESN_TYPE data;
  TMWTYPES_UCHAR *pRxBuf = ((MBLINK_CONTEXT *)pLinkContext)->rxBuffer;

  pSession = TMWDEFS_NULL;
  while((pSession = (TMWSESN *)tmwdlist_getAfter(
    &pLinkContext->sessions, (TMWDLIST_MEMBER *)pSession)) != TMWDEFS_NULL)
  {
    if(pSession->destAddress == addr)
    {
      return(pSession);
    }
  }

  /* Session was not found, call statistics function 
  * and then loop again to see if a session was opened 
  */
  data.linkAddress = addr;

  /* testHarness will determine what type, master or slave to open */
  /* data.sessionType = ??? */

  TMWCHNL_STAT_CALLBACK_FUNC(
    pLinkContext->pChannel,
    TMWCHNL_STAT_UNKNOWN_SESSION, &data);

  pSession = TMWDEFS_NULL;
  while((pSession = (TMWSESN *)tmwdlist_getAfter(
    &pLinkContext->sessions, (TMWDLIST_MEMBER *)pSession)) != TMWDEFS_NULL)
  {
    if(pSession->destAddress == addr)
    {
      return(pSession);
    }
  }

  return(TMWDEFS_NULL);
}
#endif

/* function: mblink_registerCallback */
void TMWDEFS_GLOBAL mblink_registerCallback(
  TMWCHNL *pChannel,
  MBLINK_TX_CALLBACK_FUNC pUserTxCallback,
  void *pCallbackParam)
{
  MBLINK_CONTEXT *pLinkContext = (MBLINK_CONTEXT *)pChannel->pLinkContext;
  pLinkContext->pUserTxCallback = pUserTxCallback;
  pLinkContext->pCallbackParam = pCallbackParam;
}

