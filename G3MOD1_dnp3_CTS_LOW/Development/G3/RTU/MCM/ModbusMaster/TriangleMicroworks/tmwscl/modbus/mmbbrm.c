/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: mmbbrm.c
 * description: Master Modbus Build Request Module
 */
#include "tmwscl/modbus/mbdiag.h"
#include "tmwscl/modbus/mmbbrm.h"
#include "tmwscl/modbus/mbchnl.h"
#include "tmwscl/modbus/mbsesn.h"
#include "tmwscl/modbus/mmbsesn.h"
#include "tmwscl/utils/tmwtarg.h"
#include "tmwscl/modbus/mbdefs.h"
#include "tmwscl/modbus/mbmem.h"

/* function: _initTxData */
static TMWTYPES_BOOL TMWDEFS_LOCAL _initTxData(
  MBCHNL_TX_DATA  *pTxData,
  MMBBRM_REQ_DESC *pReqDesc,
  const char *defaultMsgDescription)
{
  
  if(pReqDesc->pSession == TMWDEFS_NULL)
  {
    pTxData->tmw.txFlags |= (TMWSESN_TXFLAGS_NO_RESPONSE | TMWSESN_TXFLAGS_BROADCAST);
  }
  else
  {
    /* Validate session */
    if(pReqDesc->pSession->protocol != TMWTYPES_PROTOCOL_MB)
    {
#if TMWCNFG_SUPPORT_DIAG
      MBDIAG_ERROR(pTxData->tmw.pChannel, pReqDesc->pSession, "Session protocol is invalid for request");
#endif
      return(TMWDEFS_FALSE);
    }

    if(pReqDesc->pSession->type != TMWTYPES_SESSION_TYPE_MASTER)
    {
#if TMWCNFG_SUPPORT_DIAG
      MBDIAG_ERROR(pTxData->tmw.pChannel, pReqDesc->pSession, "Slave session is invalid for request");
#endif
      return(TMWDEFS_FALSE);
    }
  }

  /* Initialize session */
  pTxData->tmw.responseTimeout = pReqDesc->responseTimeout;

  pTxData->priority = pReqDesc->priority;

  /* Initialize user callback info */
  pTxData->pUserCallback = pReqDesc->pUserCallback;
  pTxData->pUserCallbackParam = pReqDesc->pUserCallbackParam;

  /* Initialize message description */
  if(pReqDesc->pMsgDescription != TMWDEFS_NULL)
    pTxData->tmw.pMsgDescription = pReqDesc->pMsgDescription;
  else
    pTxData->tmw.pMsgDescription = defaultMsgDescription;

  return(TMWDEFS_TRUE);
}

/* function: mmbbrm_initReqDesc */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbbrm_initReqDesc(
  MMBBRM_REQ_DESC *pReqDesc,
  TMWSESN *pSession)
{
  MMBSESN *pMMBSession = (MMBSESN *)pSession;

  pReqDesc->pSession = pSession;
  pReqDesc->pChannel = TMWDEFS_NULL;
  if (pMMBSession)
  {
    pReqDesc->responseTimeout = pMMBSession->defaultResponseTimeout;
  }
  else
  {
    pReqDesc->responseTimeout = 20000;
  }
  pReqDesc->priority = 128;
  pReqDesc->pUserCallback = TMWDEFS_NULL;
  pReqDesc->pUserCallbackParam = TMWDEFS_NULL;
  pReqDesc->pMsgDescription = TMWDEFS_NULL;
  return(TMWDEFS_TRUE);
}

/* function: mmbbrm_initBroadcastDesc */
TMWTYPES_BOOL TMWDEFS_GLOBAL mmbbrm_initBroadcastDesc(
  MMBBRM_REQ_DESC *pReqDesc,
  TMWCHNL *pChannel)
{ 
  pReqDesc->pSession = TMWDEFS_NULL;
  pReqDesc->pChannel = pChannel;
  pReqDesc->priority = 128;
  pReqDesc->responseTimeout = 20000;
  pReqDesc->pUserCallback = TMWDEFS_NULL;
  pReqDesc->pUserCallbackParam = TMWDEFS_NULL;
  pReqDesc->pMsgDescription = TMWDEFS_NULL;
  return(TMWDEFS_TRUE);
}

/* function: mmbbrm_readCoils */
TMWSESN_TX_DATA * TMWDEFS_GLOBAL mmbbrm_readCoils(
  MMBBRM_REQ_DESC *pRequestDesc,
  TMWTYPES_USHORT start,
  TMWTYPES_USHORT quantity)
{
  MBCHNL_TX_DATA *pTxData;

  /* Allocate transmit data structure */
  pTxData = (MBCHNL_TX_DATA *)mbchnl_newTxData(pRequestDesc->pSession->pChannel,pRequestDesc->pSession, 5);

  if(pTxData != TMWDEFS_NULL)
  {
    /* Initialize transmit data structure for this request */
    if(!_initTxData(pTxData, pRequestDesc, "Read Coils"))
    {
      mbchnl_freeTxData((TMWSESN_TX_DATA *)pTxData);
      return(TMWDEFS_NULL);
    }

    /* Fill request parameters */
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_FC_READ_COILS; /* MB FC */
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_HIBYTE(start);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_LOBYTE(start);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_HIBYTE(quantity);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_LOBYTE(quantity);

    /* Send message */
    if (!mbchnl_sendMessage((TMWSESN_TX_DATA *)pTxData))
    {
      mbchnl_freeTxData((TMWSESN_TX_DATA *)pTxData);
      return TMWDEFS_NULL;
    }
  }

  return((TMWSESN_TX_DATA *)pTxData);
}

/* function: mmbbrm_readDiscreteInputs */
TMWSESN_TX_DATA * TMWDEFS_GLOBAL mmbbrm_readDiscreteInputs(
  MMBBRM_REQ_DESC *pRequestDesc,
  TMWTYPES_USHORT start,
  TMWTYPES_USHORT quantity)
{
  MBCHNL_TX_DATA *pTxData;

  /* Allocate transmit data structure */
  pTxData = (MBCHNL_TX_DATA *)mbchnl_newTxData(pRequestDesc->pSession->pChannel,pRequestDesc->pSession, 5);

  if(pTxData != TMWDEFS_NULL)
  {
    /* Initialize transmit data structure for this request */
    _initTxData(pTxData, pRequestDesc, "Read Discrete Inputs");

    /* Fill request parameters */
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_FC_READ_DISCRETE_INPUTS; /* MB FC */
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_HIBYTE(start);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_LOBYTE(start);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_HIBYTE(quantity);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_LOBYTE(quantity);

    /* Send message */
    if (!mbchnl_sendMessage((TMWSESN_TX_DATA *)pTxData))
    {
      mbchnl_freeTxData((TMWSESN_TX_DATA *)pTxData);
      return TMWDEFS_NULL;
    }
  }

  return((TMWSESN_TX_DATA *)pTxData);
}

/* function: mmbbrm_readHoldingRegisters */
TMWSESN_TX_DATA * TMWDEFS_GLOBAL mmbbrm_readHoldingRegisters(
  MMBBRM_REQ_DESC *pRequestDesc,
  TMWTYPES_USHORT start,
  TMWTYPES_USHORT quantity)
{
  MBCHNL_TX_DATA *pTxData;

  /* Allocate transmit data structure */
  pTxData = (MBCHNL_TX_DATA *)mbchnl_newTxData(pRequestDesc->pSession->pChannel,pRequestDesc->pSession, 5);

  if(pTxData != TMWDEFS_NULL)
  {
    /* Initialize transmit data structure for this request */
    _initTxData(pTxData, pRequestDesc, "Read Holding Registers");

    /* Fill request parameters */
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_FC_READ_HOLDING_REGISTERS; /* MB FC */
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_HIBYTE(start);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_LOBYTE(start);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_HIBYTE(quantity);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_LOBYTE(quantity);

    /* Send message */
    if (!mbchnl_sendMessage((TMWSESN_TX_DATA *)pTxData))
    {
      mbchnl_freeTxData((TMWSESN_TX_DATA *)pTxData);
      return TMWDEFS_NULL;
    }
 }

  return((TMWSESN_TX_DATA *)pTxData);
}

/* function: mmbbrm_readInputRegisters */
TMWSESN_TX_DATA * TMWDEFS_GLOBAL mmbbrm_readInputRegisters(
  MMBBRM_REQ_DESC *pRequestDesc,
  TMWTYPES_USHORT start,
  TMWTYPES_USHORT quantity)
{
  MBCHNL_TX_DATA *pTxData;

  /* Allocate transmit data structure */
  pTxData = (MBCHNL_TX_DATA *)mbchnl_newTxData(pRequestDesc->pSession->pChannel,pRequestDesc->pSession, 5);

  if(pTxData != TMWDEFS_NULL)
  {
    /* Initialize transmit data structure for this request */
    _initTxData(pTxData, pRequestDesc, "Read Input Registers");

    /* Fill request parameters */
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_FC_READ_READ_INPUT_REGISTERS; /* MB FC */
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_HIBYTE(start);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_LOBYTE(start);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_HIBYTE(quantity);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_LOBYTE(quantity);

    /* Send message */
    if (!mbchnl_sendMessage((TMWSESN_TX_DATA *)pTxData))
    {
      mbchnl_freeTxData((TMWSESN_TX_DATA *)pTxData);
      return TMWDEFS_NULL;
    }
  }

  return((TMWSESN_TX_DATA *)pTxData);
}

/* function: mmbbrm_writeSingleCoil */
TMWSESN_TX_DATA * TMWDEFS_GLOBAL mmbbrm_writeSingleCoil(
  MMBBRM_REQ_DESC *pRequestDesc,
  TMWTYPES_USHORT start,
  TMWTYPES_BOOL value)
{
  MBCHNL_TX_DATA *pTxData;

  /* Allocate transmit data structure */
  pTxData = (MBCHNL_TX_DATA *)mbchnl_newTxData(pRequestDesc->pChannel,pRequestDesc->pSession, 5);

  if(pTxData != TMWDEFS_NULL)
  {
    TMWTYPES_USHORT data = (value) ? 0xFF00 : 0x0000;
    /* Initialize transmit data structure for this request */
    _initTxData(pTxData, pRequestDesc, "Write Single Coil");

    /* Fill request parameters */
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_FC_WRITE_SINGLE_COIL; /* MB FC */
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_HIBYTE(start);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_LOBYTE(start);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_HIBYTE(data);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_LOBYTE(data);

    /* Send message */
    if (!mbchnl_sendMessage((TMWSESN_TX_DATA *)pTxData))
    {
      mbchnl_freeTxData((TMWSESN_TX_DATA *)pTxData);
      return TMWDEFS_NULL;
    }
  }

  return((TMWSESN_TX_DATA *)pTxData);
}

/* function: mmbbrm_writeSingleRegister */
TMWSESN_TX_DATA * TMWDEFS_GLOBAL mmbbrm_writeSingleRegister(
  MMBBRM_REQ_DESC *pRequestDesc,
  TMWTYPES_USHORT start,
  TMWTYPES_USHORT value)
{
  MBCHNL_TX_DATA *pTxData;

  /* Allocate transmit data structure */
  pTxData = (MBCHNL_TX_DATA *)mbchnl_newTxData(pRequestDesc->pChannel,pRequestDesc->pSession, 5);

  if(pTxData != TMWDEFS_NULL)
  {
    /* Initialize transmit data structure for this request */
    _initTxData(pTxData, pRequestDesc, "Write Single Register");

    /* Fill request parameters */
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_FC_WRITE_SINGLE_REGISTER; /* MB FC */
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_HIBYTE(start);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_LOBYTE(start);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_HIBYTE(value);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_LOBYTE(value);

    /* Send message */
    if (!mbchnl_sendMessage((TMWSESN_TX_DATA *)pTxData))
    {
      mbchnl_freeTxData((TMWSESN_TX_DATA *)pTxData);
      return TMWDEFS_NULL;
    }
  }

  return((TMWSESN_TX_DATA *)pTxData);
}

/* function: mmbbrm_readExceptionStatus */
TMWSESN_TX_DATA * TMWDEFS_GLOBAL mmbbrm_readExceptionStatus(
  MMBBRM_REQ_DESC *pRequestDesc)
{
  MBCHNL_TX_DATA *pTxData;

  /* Allocate transmit data structure */
  pTxData = (MBCHNL_TX_DATA *)mbchnl_newTxData(pRequestDesc->pSession->pChannel,pRequestDesc->pSession, 5);

  if(pTxData != TMWDEFS_NULL)
  {
    /* Initialize transmit data structure for this request */
    _initTxData(pTxData, pRequestDesc, "Read Exception Status");

    /* Fill request parameters */
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_FC_READ_EXCEPTION_STATUS; /* MB FC */

    /* Send message */
    if (!mbchnl_sendMessage((TMWSESN_TX_DATA *)pTxData))
    {
      mbchnl_freeTxData((TMWSESN_TX_DATA *)pTxData);
      return TMWDEFS_NULL;
    }
  }

  return((TMWSESN_TX_DATA *)pTxData);
}

/* function: mmbbrm_diagnostics */
TMWSESN_TX_DATA * TMWDEFS_GLOBAL mmbbrm_diagnostics(
  MMBBRM_REQ_DESC *pRequestDesc,
  TMWTYPES_USHORT subFunction,
  TMWTYPES_UCHAR  dataSize,
  TMWTYPES_UCHAR  *pData)
{
  MBCHNL_TX_DATA *pTxData;

  /* Allocate transmit data structure */
  pTxData = (MBCHNL_TX_DATA *)mbchnl_newTxData(pRequestDesc->pSession->pChannel,pRequestDesc->pSession, (TMWTYPES_USHORT)(dataSize+10));

  if(pTxData != TMWDEFS_NULL)
  {
    /* Initialize transmit data structure for this request */
    _initTxData(pTxData, pRequestDesc, "Diagnostics");

    /* Fill request parameters */
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_FC_DIAGNOSTICS; /* MB FC */
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_HIBYTE(subFunction);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_LOBYTE(subFunction);

    memcpy(&pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength], pData, dataSize);
    pTxData->tmw.msgLength += dataSize;

    /* There is no response expected when sending Force Listen Only Mode request */
    if(subFunction == MBDEFS_DIAG_FORCE_LISTEN_ONLY)
      pTxData->tmw.txFlags |= TMWSESN_TXFLAGS_NO_RESPONSE;

    /* Send message */
    if (!mbchnl_sendMessage((TMWSESN_TX_DATA *)pTxData))
    {
      mbchnl_freeTxData((TMWSESN_TX_DATA *)pTxData);
      return TMWDEFS_NULL;
    }
  }

  return((TMWSESN_TX_DATA *)pTxData);
}

/* function: mmbbrm_writeMultipleCoils */
TMWSESN_TX_DATA * TMWDEFS_GLOBAL mmbbrm_writeMultipleCoils(
  MMBBRM_REQ_DESC *pRequestDesc,
  TMWTYPES_USHORT start,
  TMWTYPES_USHORT quantity,
  TMWTYPES_UCHAR *pValueArray)
{
  MBCHNL_TX_DATA *pTxData;
  TMWTYPES_UCHAR byteCount;

  if (quantity > MBDEFS_MMB_MAX_COIL_WRITE)
    return TMWDEFS_NULL;

  byteCount = (TMWTYPES_UCHAR) ((quantity%8) ? quantity/8 + 1 : quantity/8);

  /* Allocate transmit data structure */
  pTxData = (MBCHNL_TX_DATA *)mbchnl_newTxData(pRequestDesc->pChannel,pRequestDesc->pSession, (TMWTYPES_UCHAR) (6 + byteCount));

  if(pTxData != TMWDEFS_NULL)
  {
    int i;
    /* Initialize transmit data structure for this request */
    _initTxData(pTxData, pRequestDesc, "Write Multiple Coils");

    /* Fill request parameters */
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_FC_WRITE_MULTIPLE_COILS; /* MB FC */
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_HIBYTE(start);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_LOBYTE(start);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_HIBYTE(quantity);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_LOBYTE(quantity);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = byteCount;

    for (i=0; i<byteCount; i++)
      pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = pValueArray[i];

    /* Send message */
    if (!mbchnl_sendMessage((TMWSESN_TX_DATA *)pTxData))
    {
      mbchnl_freeTxData((TMWSESN_TX_DATA *)pTxData);
      return TMWDEFS_NULL;
    }
  }

  return((TMWSESN_TX_DATA *)pTxData);
}

/* function: mmbbrm_writeMultipleRegisters */
TMWSESN_TX_DATA * TMWDEFS_GLOBAL mmbbrm_writeMultipleRegisters(
  MMBBRM_REQ_DESC *pRequestDesc,
  TMWTYPES_USHORT start,
  TMWTYPES_USHORT quantity,
  TMWTYPES_USHORT* pValueArray)
{
  MBCHNL_TX_DATA *pTxData;
  TMWTYPES_UCHAR byteCount;

  if (quantity > MBDEFS_MMB_MAX_HREG_WRITE)
    return TMWDEFS_NULL;

  byteCount = (TMWTYPES_UCHAR) quantity * 2;

  /* Allocate transmit data structure */
  pTxData = (MBCHNL_TX_DATA *)mbchnl_newTxData(pRequestDesc->pChannel,pRequestDesc->pSession, (TMWTYPES_UCHAR) (6 + byteCount));

  if(pTxData != TMWDEFS_NULL)
  {
    int i;

    /* Initialize transmit data structure for this request */
    _initTxData(pTxData, pRequestDesc, "Write Multiple Registers");

    /* Fill request parameters */
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_FC_WRITE_MULTIPLE_HOLDING_REGISTERS; /* MB FC */
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_HIBYTE(start);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_LOBYTE(start);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_HIBYTE(quantity);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_LOBYTE(quantity);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = byteCount;

    for (i=0; i<quantity; i++)
    {
      pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_HIBYTE(pValueArray[i]);
      pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_LOBYTE(pValueArray[i]);
    }

    /* Send message */
    if (!mbchnl_sendMessage((TMWSESN_TX_DATA *)pTxData))
    {
      mbchnl_freeTxData((TMWSESN_TX_DATA *)pTxData);
      return TMWDEFS_NULL;
    }
  }

  return((TMWSESN_TX_DATA *)pTxData);
}

/* function: mmbbrm_ReadWriteMultipleRegisters */
TMWSESN_TX_DATA * TMWDEFS_GLOBAL mmbbrm_ReadWriteMultipleRegisters(
  MMBBRM_REQ_DESC *pRequestDesc,
  TMWTYPES_USHORT readStart,
  TMWTYPES_USHORT quantityToRead,
  TMWTYPES_USHORT writeStart,
  TMWTYPES_USHORT quantityToWrite,
  TMWTYPES_USHORT* pValueArray
  )
{
  MBCHNL_TX_DATA *pTxData;
  TMWTYPES_UCHAR numBytesNeeded;

  numBytesNeeded = 10 + quantityToWrite *2;
  /* spec says 118 for both, but I think you may be able to actually read 125, as with FC03 */

  /* note that testing shows that a 984-685E Controller allows a
     max write of 100, but a max read of 125;
     Concept simulator does not support this command, returns EC 1, Illegal Function
  */

  if ((quantityToWrite > MBDEFS_MMB_MAX_HREG_WRITE_FC23) 
    || (quantityToRead > MBDEFS_MMB_MAX_HREG_READ_FC23))
    return (TMWSESN_TX_DATA *) TMWDEFS_NULL;

  /* Allocate transmit data structure */
  pTxData = (MBCHNL_TX_DATA *)mbchnl_newTxData(pRequestDesc->pSession->pChannel, pRequestDesc->pSession, numBytesNeeded);

  if(pTxData != TMWDEFS_NULL)
  {
    int i;
    /* Initialize transmit data structure for this request */
    _initTxData(pTxData, pRequestDesc, "Read/Write Multiple Registers");

    /* Fill request parameters */
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_FC_READ_WRITE_MULTIPLE_REGISTERS; /* MB FC */
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_HIBYTE(readStart);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_LOBYTE(readStart);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_HIBYTE(quantityToRead);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_LOBYTE(quantityToRead);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_HIBYTE(writeStart);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_LOBYTE(writeStart);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_HIBYTE(quantityToWrite);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_LOBYTE(quantityToWrite);
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = (TMWTYPES_UCHAR) (quantityToWrite * 2);

    for (i=0; i<quantityToWrite; i++)
    {
      pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_HIBYTE(pValueArray[i]);
      pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_LOBYTE(pValueArray[i]);
    }

     /* Send message */
    if (!mbchnl_sendMessage((TMWSESN_TX_DATA *)pTxData))
    {
      mbchnl_freeTxData((TMWSESN_TX_DATA *)pTxData);
      return TMWDEFS_NULL;
    }
  }

  return((TMWSESN_TX_DATA *)pTxData);
}

/* function: mmbbrm_readDeviceId */
TMWSESN_TX_DATA * TMWDEFS_GLOBAL mmbbrm_readDeviceId(
  MMBBRM_REQ_DESC *pRequestDesc,
  TMWTYPES_UCHAR readDevIdCode,
  TMWTYPES_UCHAR objectId)
{
  MBCHNL_TX_DATA *pTxData;

  /* Allocate transmit data structure */
  pTxData = (MBCHNL_TX_DATA *)mbchnl_newTxData(pRequestDesc->pSession->pChannel,pRequestDesc->pSession, (TMWTYPES_USHORT)(5));

  if(pTxData != TMWDEFS_NULL)
  {
    /* Initialize transmit data structure for this request */
    _initTxData(pTxData, pRequestDesc, "Read Device Identification");

    /* Fill request parameters */
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = MBDEFS_FC_43_ENCAPSULATED; /* MB FC */
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = 14; /* MEI Type Read Device Id */
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = readDevIdCode;
    pTxData->tmw.pMsgBuf[pTxData->tmw.msgLength++] = objectId;
  
    /* Send message */
    if (!mbchnl_sendMessage((TMWSESN_TX_DATA *)pTxData))
    {
      mbchnl_freeTxData((TMWSESN_TX_DATA *)pTxData);
      return TMWDEFS_NULL;
    }
  }

  return((TMWSESN_TX_DATA *)pTxData);
}

/* function: mmbbrm_sendCustomPdu */
TMWSESN_TX_DATA * TMWDEFS_GLOBAL mmbbrm_sendCustomPdu(
  MMBBRM_REQ_DESC *pRequestDesc,
  TMWTYPES_UCHAR fc,
  TMWTYPES_UCHAR *pData,
  TMWTYPES_UCHAR length)
{
  MBCHNL_TX_DATA *pTxData;

  /* Allocate transmit data structure */
  pTxData = (MBCHNL_TX_DATA *)mbchnl_newTxData(pRequestDesc->pSession->pChannel, pRequestDesc->pSession, length+1); 

  if(pTxData != TMWDEFS_NULL)
  { 
    /* Initialize transmit data structure for this request */
    _initTxData(pTxData, pRequestDesc, "Custom PDU");

    /* Fill request parameters */
    pTxData->tmw.pMsgBuf[0] = fc; /* MB FC */ 
    memcpy(&pTxData->tmw.pMsgBuf[1], pData, length);
    pTxData->tmw.msgLength = length +1;

    pTxData->tmw.txFlags = TMWSESN_TXFLAGS_NO_RESPONSE;
  
    /* Send message */
    if (!mbchnl_sendMessage((TMWSESN_TX_DATA *)pTxData))
    {
      mbchnl_freeTxData((TMWSESN_TX_DATA *)pTxData);
      return TMWDEFS_NULL;
    }
  }

  return((TMWSESN_TX_DATA *)pTxData);
}

