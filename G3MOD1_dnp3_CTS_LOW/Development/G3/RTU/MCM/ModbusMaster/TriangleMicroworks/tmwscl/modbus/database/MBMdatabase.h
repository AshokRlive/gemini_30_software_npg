/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MBMdatabase.hpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   2 Jun 2014     shetty_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef MBMDATABASE_HPP_
#define MBMDATABASE_HPP_
#include <vector>
#ifdef __cplusplus
extern "C" {
#endif
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "sys/time.h"
#include "MBMConfigProto.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*Union to handle different kinds of register types returned by the device*/
typedef union regValue {
    lu_int32_t regValueInt32;
    lu_uint32_t regValueUint32;
    lu_float32_t regValueFloat32;
}RegValue;

typedef struct MBMDatabasedef {
        RegValue currentAnalogRegReading;
        RegValue previousAnalogRegReading;
        lu_uint8_t currentDigitalRegReading;
        lu_uint8_t previousDigitalRegReading;
        lu_uint8_t RequestRetryCounter;
        lu_bool_t  requestStatusPending;
        lu_bool_t  regUpdateFlag;
        struct timespec timeStamp;
        mbm::ChannelConf channelconf;
        MBMDatabasedef(): currentDigitalRegReading(0),previousDigitalRegReading(0),
                         RequestRetryCounter(0),requestStatusPending(0),
                         regUpdateFlag(0){
            currentAnalogRegReading.regValueInt32 = 0,previousAnalogRegReading.regValueInt32 = 0;
        }
}MBMDBStr;



typedef struct db{
       mbm::DeviceConf device;
       MBMDBStr *aiChannel;
       MBMDBStr *diChannel;
       mbm::ChannelConf *doChannel;
       mbm::registervalues readRegCmdvalue;
   }DB;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
typedef void (*UpdateregCallbackPTR)( void *pHandle,
                TMWTYPES_USHORT startAddr,
                TMWTYPES_USHORT quantity,
                TMWTYPES_USHORT *pValueArray);

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/* callback function definition goes here */
void UpdateRegCallback( void *pHandle,
                TMWTYPES_USHORT startAddr,
                TMWTYPES_USHORT quantity,
                TMWTYPES_USHORT *pValueArray,
                mbm::REG_TYPE regType);

void readRegCmdValueCallback( void *pHandle,
                TMWTYPES_USHORT startAddr,
                TMWTYPES_USHORT quantity,
                TMWTYPES_USHORT *pValueArray,
                mbm::REG_TYPE regType);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */



#ifdef __cplusplus
}
#endif

#endif /* MBMDATABASE_HPP_ */

/*
 *********************** End of file ******************************************
 */
