/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: mbchnl.h
 * description: This file contains the implementation of the Modbus Master 
 *  Application Layer.
 */
#include "tmwscl/modbus/mbdiag.h"
#include "tmwscl/utils/tmwtarg.h"
#include "tmwscl/modbus/mbchnl.h"
#include "tmwscl/modbus/mbmem.h"
#include "tmwscl/modbus/mbsesn.h"

/* function: _sendNextFrame */
static void TMWDEFS_LOCAL _sendNextFrame(
  TMWCHNL *pChannel)
{

  /* If there is not an outstanding request, send the first one in the message queue */
  if(pChannel->pCurrentMessage == TMWDEFS_NULL)
  { 
    TMWSESN_TX_DATA *pTxData = (TMWSESN_TX_DATA *)tmwdlist_getFirst(&pChannel->messageQueue);
    if(pTxData != TMWDEFS_NULL)
    {        
      MBLINK_CONTEXT *pLinkContext = (MBLINK_CONTEXT *)pChannel->pLinkContext;
      pChannel->pCurrentMessage = pTxData;

      ((MBCHNL_TX_DATA *)pTxData)->sent = TMWDEFS_TRUE;
 
      MBDIAG_FRAME_SENT(pChannel, pTxData->pSession,
        pTxData->pMsgBuf, pTxData->msgLength);

      /* If user has registered a callback function call it */
      if(pLinkContext->pUserTxCallback != TMWDEFS_NULL)
      {
        pLinkContext->pUserTxCallback(pLinkContext->pCallbackParam, pTxData);
      }

      /* Update statistics */
      TMWSESN_STAT_CALLBACK_FUNC(pTxData->pSession,
        TMWSESN_STAT_ASDU_SENT, TMWDEFS_NULL);

      /* Send frame to link layer */
      pChannel->pLink->pLinkTransmit(
        pChannel->pLinkContext, pTxData);
    }
    return;
  }
}

/* function: _timeoutBody
 * purpose:  
 * arguments:
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _timeoutBody(
  TMWSESN_TX_DATA  *pTxData,
  TMWTYPES_BOOL responseTimeout)
{ 
  MBCHNL_TX_DATA  *pMbTxData = (MBCHNL_TX_DATA *)pTxData;
  TMWCHNL *pChannel = pTxData->pChannel;
 
#if TMWCNFG_SUPPORT_DIAG
  char buf[128];
  /* Diagnostics */
  if(responseTimeout)
  {
    tmwtarg_snprintf(buf, sizeof(buf), "Response timeout for request %s", pTxData->pMsgDescription);
  }
  else
  {
    tmwtarg_snprintf(buf, sizeof(buf), "Channel timeout for request %s", pTxData->pMsgDescription);
  }

  MBDIAG_ERROR(TMWDEFS_NULL, pTxData->pSession, buf);
#endif

  TMWSESN_STAT_CALLBACK_FUNC(pTxData->pSession, TMWSESN_STAT_REQUEST_TIMEOUT, TMWDEFS_NULL);
  
  mbchnl_cleanupMessage(pTxData, TMWDEFS_NULL, MBCHNL_RESP_STATUS_TIMEOUT, 0);
}

/* function: _incrementalTimeout
 * purpose:  
 * arguments:
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _incrementalTimeout(
  void *pParam) 
{
  _timeoutBody((TMWSESN_TX_DATA *)pParam, TMWDEFS_FALSE);
}

/* function: _responseTimeout
 * purpose:  
 * arguments:
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _responseTimeout(
  void *pParam)
{
  _timeoutBody((TMWSESN_TX_DATA *)pParam, TMWDEFS_TRUE);
} 

/* function: _infoCallback */
static void TMWDEFS_CALLBACK _infoCallback(
  void *pParam, 
  TMWSESN *pSession, 
  TMWSCL_INFO sesnInfo)
{
  TMWTARG_UNUSED_PARAM(pParam);
  TMWTARG_UNUSED_PARAM(pSession);
  TMWTARG_UNUSED_PARAM(sesnInfo);
}

/* function mbchnl_afterTxCallback */
void TMWDEFS_CALLBACK mbchnl_afterTxCallback(
  void *pCallbackParam)
{
  TMWSESN_TX_DATA *pTxData = (TMWSESN_TX_DATA *)pCallbackParam;

  /* Slave does not queue transmits on channel queue*/
  if(pTxData->pSession->type == TMWTYPES_SESSION_TYPE_MASTER)
  {
    /* If no response expected, clean up */
    if((pTxData->txFlags & TMWSESN_TXFLAGS_NO_RESPONSE) != 0)
    {
      mbchnl_cleanupMessage((TMWSESN_TX_DATA*)pTxData, 
        TMWDEFS_NULL, MBCHNL_RESP_STATUS_SUCCESS, 0);
    }
    else
    {
      TMWCHNL *pChannel = (TMWCHNL *)pTxData->pChannel;

      /* We are expecting a response, start timer if required */
      if(pChannel->incrementalTimeout != 0)
      {
        tmwtimer_start(&pChannel->incrementalTimer,
          pChannel->incrementalTimeout, pChannel,
          _incrementalTimeout, pTxData);
      }
    }
  }
}

/* function: _processFrameCallback */
static void TMWDEFS_CALLBACK _processFrameCallback(
  void *pParam,
  TMWSESN *pSession, 
  TMWSESN_RX_DATA *pRxFrame)
{
  MBSESN *pMBSession = (MBSESN *)pSession;
  TMWCHNL *pChannel = pSession->pChannel;
  TMWTARG_UNUSED_PARAM(pParam);

  /* Update stats */
  TMWSESN_STAT_CALLBACK_FUNC(pSession,
    TMWSESN_STAT_ASDU_RECEIVED, TMWDEFS_NULL);

  /* Diagnostics */
  if(pRxFrame->msgLength != MBLINK_INVALID_LENGTH)
  {
    MBDIAG_FRAME_RECEIVED(pChannel, pSession,
      pRxFrame->pMsgBuf, pRxFrame->msgLength);
  }
  else
  {
    mbchnl_discardInvLen(pSession);
  }

  /* Call function to process frame*/
  if (!pMBSession->pProcessFrameFunc(pSession, pRxFrame))
  {
#if TMWCNFG_SUPPORT_DIAG
    /* Log error */
    MBDIAG_ERROR(pChannel, pSession, "Failed to process message");
#endif
  }
}
 
/* function: mbchnl_initConfig */
void TMWDEFS_GLOBAL mbchnl_initConfig(
  MBLINK_CONFIG *pLinkConfig, 
  TMWPHYS_CONFIG *pPhysConfig)
{
  /* Call generic initialization routines */
  mblink_initConfig(pLinkConfig);
  tmwphys_initConfig(pPhysConfig);
}

/* function: mbchnl_openChannel */
TMWCHNL * TMWDEFS_GLOBAL mbchnl_openChannel(
  TMWAPPL *pApplContext,
  TMWCHNL_STAT_CALLBACK pCallback,
  void *pCallbackParam,
  const MBLINK_CONFIG *pLinkConfig, 
  const TMWPHYS_CONFIG *pPhysConfig, 
  const void *pIOConfig,
  TMWTARG_CONFIG *pTmwTargConfig)
{  
  TMWCHNL *pChannel;
  
  /* Initialize memory management if not yet done */
  if(!tmwappl_getInitialized(TMWAPPL_INIT_MB))
  { 
    if(!mbmem_init(TMWDEFS_NULL))
      return TMWDEFS_NULL;

    tmwappl_setInitialized(TMWAPPL_INIT_MB);
  }

  pChannel = (TMWCHNL *)tmwmem_alloc(TMWMEM_CHNL_TYPE);
  if(pChannel != TMWDEFS_NULL)
  {
    tmwchnl_initChannel(pApplContext, pChannel, pCallback, pCallbackParam, 
      TMWDEFS_NULL, TMWDEFS_NULL, TMWDEFS_NULL, TMWDEFS_NULL, TMWDEFS_TRUE);
    if(tmwphys_initChannel(pChannel, pPhysConfig, pIOConfig, pTmwTargConfig))
    {
      if(mblink_initChannel(pChannel, pLinkConfig))
      {
        pChannel->pLink->pLinkSetCallbacks(pChannel->pLinkContext, 
          pChannel, _infoCallback, _processFrameCallback, TMWDEFS_NULL,
          TMWDEFS_NULL, TMWDEFS_NULL, TMWDEFS_NULL);

        TMWTARG_LOCK_SECTION(&pApplContext->lock);
        tmwdlist_addEntry(&pApplContext->channels, (TMWDLIST_MEMBER *)pChannel);
        TMWTARG_UNLOCK_SECTION(&pApplContext->lock);

        pChannel->maxQueueSize = pLinkConfig->maxQueueSize;
        return(pChannel);
      }

      tmwphys_deleteChannel(pChannel);
    }

    tmwchnl_deleteChannel(pChannel);
    tmwmem_free(pChannel);
  }

  return(TMWDEFS_NULL);
}

/* function: mbchnl_getChannelConfig  */
TMWTYPES_BOOL TMWDEFS_GLOBAL mbchnl_getChannelConfig(
  TMWCHNL *pChannel,
  MBLINK_CONFIG *pLinkConfig, 
  TMWPHYS_CONFIG *pPhysConfig)
{
  MBLINK_CONTEXT *pLinkContext = (MBLINK_CONTEXT *)pChannel->pLinkContext;
  TMWPHYS_CONTEXT *pPhysContext = (TMWPHYS_CONTEXT *)pChannel->pPhysContext;

  pLinkConfig->type = pLinkContext->type;
  pLinkConfig->maxQueueSize = pChannel->maxQueueSize;

#if MBCNFG_SUPPORT_ASCII
  if(pLinkContext->type == MBLINK_TYPE_ASCII)
  {
    pLinkConfig->alink.delim1 = pLinkContext->delim1;
    pLinkConfig->alink.delim2 = pLinkContext->delim2;
  }
#endif

  tmwphys_getChannelConfig(pChannel, pPhysConfig);

  return(TMWDEFS_TRUE);
}


/* function: mbchnl_setChannelConfig  */
TMWTYPES_BOOL TMWDEFS_GLOBAL mbchnl_setChannelConfig(
  TMWCHNL             *pChannel,
  const MBLINK_CONFIG *pLinkConfig, 
  const TMWPHYS_CONFIG *pPhysConfig)
{
  MBLINK_CONTEXT *pLinkContext = (MBLINK_CONTEXT *)pChannel->pLinkContext;
  TMWPHYS_CONTEXT *pPhysContext = (TMWPHYS_CONTEXT *)pChannel->pPhysContext;

  /* cannot change type without closing and reopening
   * pLinkContext->type      = pLinkConfig->channelResponseTimeout;
   */
  pChannel->maxQueueSize  = pLinkConfig->maxQueueSize;
  
#if MBCNFG_SUPPORT_ASCII
  if(pLinkConfig->type == MBLINK_TYPE_ASCII)
  {
    pLinkContext->delim1 = pLinkConfig->alink.delim1;
    pLinkContext->delim2 = pLinkConfig->alink.delim2;
  }
#endif

  /* cannot be changed without closing and reopening channel
   * pPhysContext->rxBufferSize = pPhysConfig->rxBufferSize;
   */

  tmwphys_setChannelConfig(pChannel, pPhysConfig);

  return(TMWDEFS_TRUE);
}

/* function: mbchnl_modifyPhys */
TMWTYPES_BOOL TMWDEFS_GLOBAL mbchnl_modifyPhys(
  TMWCHNL *pChannel, 
  const TMWPHYS_CONFIG *pPhysConfig, 
  TMWTYPES_ULONG configMask)
{
  return(tmwphys_modifyChannel(pChannel, pPhysConfig, configMask));
}

/* function: mbchnl_modifyLink */
TMWTYPES_BOOL TMWDEFS_GLOBAL mbchnl_modifyLink(
  TMWCHNL *pChannel, 
  const MBLINK_CONFIG *pLinkConfig, 
  TMWTYPES_ULONG configMask)
{
  return(mblink_modifyChannel(pChannel, pLinkConfig, configMask));
}

/* function: mbchnl_closeChannel */
TMWTYPES_BOOL TMWDEFS_GLOBAL mbchnl_closeChannel(
  TMWCHNL *pChannel)
{
  TMWAPPL *pApplContext;

  /* Check for NULL since this would be a common error */
  if(pChannel == TMWDEFS_NULL)
  {
    return(TMWDEFS_FALSE);
  }

  pApplContext = pChannel->pApplContext;

  /* Lock application list of channels */
  TMWTARG_LOCK_SECTION(&pApplContext->lock);

  /* Lock channel */
  TMWTARG_LOCK_SECTION(&pChannel->lock);
  
  /* cancel the incremental timer in case it is running */
  tmwtimer_cancel(&pChannel->incrementalTimer);

  /* Close link layer */
  if(mblink_deleteChannel(pChannel) == TMWDEFS_FALSE)
  {
    /* Unlock channel */
    TMWTARG_UNLOCK_SECTION(&pChannel->lock);
    return(TMWDEFS_FALSE);
  }

  /* Remove channel from list of channels */
  tmwdlist_removeEntry(&pApplContext->channels, (TMWDLIST_MEMBER *)pChannel);

  /* Close physical layer */
  tmwphys_deleteChannel(pChannel);

  /* Unlock channel */
  TMWTARG_UNLOCK_SECTION(&pChannel->lock);
  TMWTARG_UNLOCK_SECTION(&pApplContext->lock);

  /* Free channel resources, including deleting lock */
  tmwchnl_deleteChannel(pChannel);

  /* Free memory */
  tmwmem_free(pChannel);

  return(TMWDEFS_TRUE);
}

/* function: mbchnl_sendMessage */
TMWTYPES_BOOL TMWDEFS_GLOBAL mbchnl_sendMessage(
  TMWSESN_TX_DATA *pTxData)
{
  MBCHNL_TX_DATA *pMBTxData = (MBCHNL_TX_DATA *)pTxData;
  TMWCHNL *pChannel = pTxData->pChannel;
  MBCHNL_TX_DATA *pRequest = TMWDEFS_NULL;

  /* Lock channel */
  TMWTARG_LOCK_SECTION(&pChannel->lock);

  /* Diagnostics */
  MBDIAG_INSERT_QUEUE(pTxData->pChannel, pTxData->pSession, pTxData->pMsgDescription);

  /* First check to make sure this does not exceed the maximum number of requests allowed */
  if((pChannel->maxQueueSize > 0) 
    &&(tmwdlist_size(&pChannel->messageQueue) >= pChannel->maxQueueSize))
  {
#if TMWCNFG_SUPPORT_DIAG
    MBDIAG_ERROR(pChannel, pTxData->pSession, "Too many requests, not inserted in transmit queue");
#endif

    /* Unlock channel */
    TMWTARG_UNLOCK_SECTION(&pChannel->lock);

    return(TMWDEFS_FALSE);
  } 
  
  /* First check to make sure this is not a duplicate request */
  while((pRequest = (MBCHNL_TX_DATA *)tmwdlist_getAfter(
    &pChannel->messageQueue, (TMWDLIST_MEMBER *)pRequest)) != TMWDEFS_NULL)
  {
    TMWTYPES_BOOL matched = TMWDEFS_TRUE;
    int i = 0;

    if(pRequest->tmw.pChannel != pTxData->pChannel)
      continue;

    if(pRequest->tmw.pSession != pTxData->pSession)
      continue;

    if(pRequest->tmw.msgLength != pTxData->msgLength)
      continue;

    if(pRequest->pUserCallback != ((MBCHNL_TX_DATA *)pTxData)->pUserCallback)
      continue;

    for(i = 0; i < pTxData->msgLength; i++)
    {
      if(pRequest->tmw.pMsgBuf[i] != pTxData->pMsgBuf[i])
      {
        matched = TMWDEFS_FALSE;
        break;
      }
    }

    if(!matched)
      continue;

    /* Allow duplicate request only if it is the same as the one has been sent
     */
    if(pRequest->sent)
      continue;

    /* Duplicate, remove the old one on the queue that matches 
     * keeping just the new one will guarantee the correct command
     * is done last.
     */
#if TMWCNFG_SUPPORT_DIAG
    MBDIAG_ERROR(pChannel, pTxData->pSession, "Duplicate request removed from message queue");
#endif

    /* Remove this request from queue */
    mbchnl_removeRequest((TMWSESN_TX_DATA *)pRequest);

    if(pRequest->pUserCallback != TMWDEFS_NULL)
    {
     /* Initialize user callback info */
      MBCHNL_RESPONSE_INFO response;
      response.pSession = pRequest->tmw.pSession;
      response.pTxData = (TMWSESN_TX_DATA *)pRequest;
      response.pRxData = TMWDEFS_NULL;
      response.status = MBCHNL_RESP_STATUS_CANCELED;
      response.responseTime = 0;

      pRequest->pUserCallback(pRequest->pUserCallbackParam, &response);
    }
    mbchnl_freeTxData((TMWSESN_TX_DATA *)pRequest);

    /* Only one duplicate that has not been sent is possible. */
    break;
  }

  /* Add entry to queue in priority order */
  pRequest = TMWDEFS_NULL;
  while((pRequest = (MBCHNL_TX_DATA *)tmwdlist_getAfter(
    &pChannel->messageQueue, (TMWDLIST_MEMBER *)pRequest)) != TMWDEFS_NULL)
  {
    if(pMBTxData->priority > pRequest->priority)
      break;
  }

  /* See if we are at the end of the queue */
  if(pRequest != TMWDEFS_NULL)
  {
    /* No, insert before next entry */
    tmwdlist_insertEntryBefore(&pChannel->messageQueue,
      (TMWDLIST_MEMBER *)pRequest, (TMWDLIST_MEMBER *)pTxData);
  }
  else
  {
    /* Yep, insert at end of queue */
    tmwdlist_addEntry(&pChannel->messageQueue,
      (TMWDLIST_MEMBER *)pTxData);
  }

  pTxData->timeSent = tmwtarg_getMSTime();

  /* Start response timer now */
  if(pTxData->responseTimeout != 0)
  {
    tmwtimer_start(&pTxData->responseTimer,
      pTxData->responseTimeout, pChannel, _responseTimeout, pTxData);
  }

  /* Send  frame to the link layer */
  _sendNextFrame(pChannel);

  /* Unlock channel */
  TMWTARG_UNLOCK_SECTION(&pChannel->lock);

  return(TMWDEFS_TRUE);
}

/* function: mbchnl_removeRequest */
void TMWDEFS_LOCAL mbchnl_removeRequest(
  TMWSESN_TX_DATA *pTxData)
{
  /* Delete request from pending message queue */
  tmwdlist_removeEntry(&pTxData->pChannel->messageQueue, (TMWDLIST_MEMBER *)pTxData);

  if(pTxData->pChannel->pCurrentMessage == pTxData)
  {
    tmwtimer_cancel(&pTxData->pChannel->incrementalTimer);
    pTxData->pChannel->pCurrentMessage = TMWDEFS_NULL;
  }
}

/* function: mbchnl_cleanupMessage */
TMWDEFS_GLOBAL void mbchnl_cleanupMessage(
  TMWSESN_TX_DATA *pTxData,
  TMWSESN_RX_DATA *pRxData,
  MBCHNL_RESP_STATUS status,
  TMWTYPES_UCHAR requestStatus)
{
  MBCHNL_TX_DATA *pChnlTxData = (MBCHNL_TX_DATA *)pTxData;
  TMWCHNL *pChannel = pTxData->pChannel;

  MBDIAG_REMOVE_MESSAGE(pTxData, status);

  if(status == MBCHNL_RESP_STATUS_TIMEOUT)
  {
    /* Mark the session offline */
    tmwsesn_setOnline(pTxData->pSession, TMWDEFS_FALSE);
  }

  mbchnl_removeRequest((TMWSESN_TX_DATA *)pChnlTxData);

  /* Remove this so physical layer won't try to access deallocated txData */
  if((pChannel->pPhysContext->pTxDescriptor != TMWDEFS_NULL) 
    && (pTxData == (TMWSESN_TX_DATA*)pChannel->pPhysContext->pTxDescriptor->pCallbackParam))
  {
    pChannel->pPhysContext->pTxDescriptor = TMWDEFS_NULL;
  }

  /* Call callbacks */
  if(pChnlTxData->pUserCallback != TMWDEFS_NULL)
  {
    MBCHNL_RESPONSE_INFO responseInfo;

    responseInfo.pSession = pChnlTxData->tmw.pSession;
    responseInfo.status = status;
    /*responseInfo.requestStatus = requestStatus;*/

    responseInfo.responseTime = tmwtarg_getMSTime() - pChnlTxData->tmw.timeSent;

    responseInfo.pTxData = pTxData;
    responseInfo.pRxData = pRxData;

    if(pChnlTxData->pUserCallback != TMWDEFS_NULL)
      pChnlTxData->pUserCallback(pChnlTxData->pUserCallbackParam, &responseInfo);
  }

  /* Free Trans Data Structure */
  mbchnl_freeTxData(pTxData);

  /* Send  frame to the link layer if there is one. */
  _sendNextFrame(pChannel);
}
  
/* function: mbchnl_deleteMessages */
TMWDEFS_GLOBAL void mbchnl_deleteMessages(
  TMWCHNL *pChannel,
  TMWSESN *pSession)
{
  TMWSESN_TX_DATA *pTxData = TMWDEFS_NULL;

  /* Search through pending requests for any for this session */
  while((pTxData = (TMWSESN_TX_DATA *)tmwdlist_getAfter(
    &pChannel->messageQueue, (TMWDLIST_MEMBER *)pTxData)) != TMWDEFS_NULL)
  {
    /* If sessions are the same we have a match */
    if(pTxData->pSession == pSession)
    {
      /* Delete request from pending message queue */ 
      mbchnl_removeRequest(pTxData);

      /* Free Trans Data Structure */
      mbchnl_freeTxData(pTxData);

      pTxData = TMWDEFS_NULL;
    }
  }
}

/* function: mbchnl_newTxData */
TMWSESN_TX_DATA * TMWDEFS_GLOBAL mbchnl_newTxData(
  TMWCHNL *pChannel,
  TMWSESN *pSession,
  TMWTYPES_USHORT bufLen)
{
  MBCHNL_TX_DATA *pTxData;

  if(bufLen > MBCHNL_TX_DATA_BUFFER_MAX)
  {
    TMWDIAG_SESN_ERROR(pSession, "mbchnl_newTxData bufLen too large");
    return(TMWDEFS_NULL);
  }
    
  /* If pSession is NULL this is a broadcast request, 
   * find first master DNP session to use 
   */
  if(pSession == TMWDEFS_NULL)
  {
    TMWDLIST *pList = pChannel->pLink->pLinkGetSessions(pChannel->pLinkContext);
    while((pSession = (TMWSESN *)tmwdlist_getAfter(pList, (TMWDLIST_MEMBER *)pSession)) != TMWDEFS_NULL)
    {
      if(pSession->type == TMWTYPES_SESSION_TYPE_MASTER)
        break;
    }
    /* If we could not find a master session just return failure */
    if(pSession == TMWDEFS_NULL)
    {
      return(TMWDEFS_NULL);
    }
  }
  
  pTxData = (MBCHNL_TX_DATA *)mbmem_alloc(MBMEM_CHNL_TX_DATA_TYPE);
  
  if(pTxData == TMWDEFS_NULL)
  {
    return(TMWDEFS_NULL);
  }

  tmwsesn_initTxData((TMWSESN_TX_DATA *)pTxData, pTxData->buffer, bufLen);

  pTxData->tmw.pChannel = pSession->pChannel;
  pTxData->tmw.pSession = pSession;

  pTxData->tmw.responseTimeout = 0;
  tmwtimer_init(&pTxData->tmw.responseTimer);

  pTxData->sent = TMWDEFS_FALSE;

  pTxData->priority = 128;

  pTxData->pInternalCallback = TMWDEFS_NULL;
  pTxData->pInternalCallbackParam = TMWDEFS_NULL;

  pTxData->pUserCallback = TMWDEFS_NULL;
  pTxData->pUserCallbackParam = TMWDEFS_NULL;

  return((TMWSESN_TX_DATA *)pTxData);
}

/* function: mbchnl_freeTxData */
void TMWDEFS_GLOBAL mbchnl_freeTxData(
  TMWSESN_TX_DATA *pTxData)
{ 
  /* Cancel application response timer */
  tmwtimer_cancel(&pTxData->responseTimer);

  pTxData->structureValid = 0x11;
  mbmem_free(pTxData);
}

/* function: mbchnl_discardInvLen */
void TMWDEFS_GLOBAL  mbchnl_discardInvLen(TMWSESN *pSession)
{
#if TMWCNFG_SUPPORT_DIAG
  MBDIAG_ERROR(pSession->pChannel, pSession, "Invalid length message discarded");
#endif
}
