/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: mmbsesn.h
 * description: Master Modbus Session Definitions
 */
#ifndef MMBSESN_DEFINED
#define MMBSESN_DEFINED

#include "tmwscl/utils/tmwchnl.h"
#include "tmwscl/utils/tmwsesn.h"

/* Master Modbus session configuration structure */
typedef struct MMBSessionConfigStruct {

  /* Address of modbus slave
   * For Modbus TCP this is the Unit Identifier which is typically 0xff (or 0) unless 
   * crossing a bridge to a Modbus slave 
   */
  TMWTYPES_USHORT slaveAddress;

  /* Default amount of time a request will wait for a response from a Modbus
   * slave 
   */
  TMWTYPES_MILLISECONDS defaultResponseTimeout;

  /* Determine whether the session is active or inactive. An inactive 
   * session will not transmit or receive frames.
   */
  TMWTYPES_BOOL active;

  /* User registered statistics callback function and parameter */
  TMWSESN_STAT_CALLBACK pStatCallback;
  void *pStatCallbackParam;
   
} MMBSESN_CONFIG;

/* DEPRECATED SHOULD USE mmbsesn_getSessionConfig and 
 *  mmbsesn_setSessionConfig
 */
#define MMBSESN_CONFIG_SLAVE        0x00000001L
#define MMBSESN_CONFIG_ACTIVE       0x00000002L
#define MMBSESN_CONFIG_RESP_TIMEOUT 0x00000004L

/* Include master Modbus 'private' structures and functions */
#include "tmwscl/modbus/mmbsesp.h"

#ifdef __cplusplus
extern "C" {
#endif

  /* function: mmbsesn_initConfig 
   * purpose: Initialize modbus master session configuration data structure.
   *  This routine should be called to initialize all the members of the
   *  data structure. Then the user should modify the data members they
   *  need in user code. Then pass the resulting structure to 
   *  mmbsesn_openSession.
   * arguments:
   *  pConfig - pointer to configuration data structure to initialize
   * returns:
   *  void
   */
  TMWDEFS_SCL_API void TMWDEFS_GLOBAL mmbsesn_initConfig(
    MMBSESN_CONFIG *pConfig);

  /* function: mmbsesn_openSession
   * purpose: Open a modbus master session
   * arguments:
   *  pChannel - channel to open session on
   *  pConfig - modbus master configuration data structure
   *  pUserHandle - handle passed to session database initialization routine
   * returns:
   *  Pointer to new session or TMWDEFS_NULL.
   */
  TMWDEFS_SCL_API TMWSESN * TMWDEFS_GLOBAL mmbsesn_openSession(
    TMWCHNL *pChannel,
    const MMBSESN_CONFIG *pConfig, 
    void *pUserHandle);
 
  /* function: mmbsesn_getSessionConfig  
   * purpose:  Get current configuration from an open session
   * arguments:
   *  pSession - session to get configuration from
   *  pConfig - modbus master configuration data structure to be filled in
   * returns:
   *  TMWDEFS_TRUE if successful
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsesn_getSessionConfig(
    TMWSESN *pSession,
    MMBSESN_CONFIG *pConfig);

  /* function: mmbsesn_setSessionConfig 
   * purpose: Modify a currently open session
   *  NOTE: normally mmbsesn_getSessionConfig() will be called
   *   to get the current config, some values will be changed 
   *   and this function will be called to set the values.
   * arguments:
   *  pSession - session to modify
   *  pConfig - modbus master configuration data structure
   * returns:
   *  TMWDEFS_TRUE if successful
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsesn_setSessionConfig(
    TMWSESN *pSession,
    const MMBSESN_CONFIG *pConfig);

  /* function: mmbsesn_modifySession  
   *  DEPRECATED FUNCTION, SHOULD USE mmbsesn_setSessionConfig()
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsesn_modifySession(
    TMWSESN *pSession,
    const MMBSESN_CONFIG *pConfig, 
    TMWTYPES_ULONG configMask);

  /* function: mmbsesn_closeSession  
   * purpose: Close a currently open session
   * arguments:
   *  pSession - session to close
   * returns:
   *  TMWDEFS_TRUE if successfull, else TMWDEFS_FALSE
   */
  TMWDEFS_SCL_API void TMWDEFS_GLOBAL mmbsesn_closeSession(
    TMWSESN *pSession);

#ifdef __cplusplus
}
#endif
#endif /* MMBSESN_DEFINED */
