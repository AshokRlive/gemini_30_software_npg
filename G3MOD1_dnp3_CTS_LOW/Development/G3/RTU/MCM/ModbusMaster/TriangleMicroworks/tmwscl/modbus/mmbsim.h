/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: mmbsim.h
 * description: Simulates a MB master database.
 *  This file is an example of a simulated MB master database interface.
 *  It should NOT be included in the final version of a MB master device.
 */
#ifndef MMBSIM_DEFINED
#define MMBSIM_DEFINED

#include "tmwscl/utils/tmwcnfg.h"

#if TMWCNFG_USE_SIMULATED_DB

#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/utils/tmwsim.h"
#include "tmwscl/modbus/mmbsesn.h"

/* Specify how many points of each data type to simulate inittialy */
#define MMBSIM_NUM_COILS               1000
#define MMBSIM_NUM_DISCRETE_INPUTS     1000
#define MMBSIM_NUM_INPUT_REGISTERS     1000
#define MMBSIM_NUM_HOLDING_REGISTERS   1000

/* Specify callback */
typedef void (*MMBSIM_CALLBACK_FUNC)(
  void *pCallbackParam, 
  TMWSIM_EVENT_TYPE type,
  TMWTYPES_UCHAR dataType, 
  TMWTYPES_USHORT address);

/* Define simulated database context */
typedef struct MMBSimDatabaseStruct {
  TMWSIM_TABLE_HEAD   coils;
  TMWSIM_TABLE_HEAD   discreteInputs;
  TMWSIM_TABLE_HEAD   inputRegisters;
  TMWSIM_TABLE_HEAD   holdingRegisters;

  TMWSIM_TABLE_HEAD   deviceIds;

  TMWTYPES_UCHAR      exceptionStatus;

  /* User callbacks */
  MMBSIM_CALLBACK_FUNC pUpdateCallback;
  void *pUpdateCallbackParam;

  /* Manged SCL database handle*/
  void *managedDBhandle;

} MMBSIM_DATABASE;

#ifdef __cplusplus
extern "C" {
#endif

  /* function: mmbsim_init
   * purpose:
   * arguments:
   * returns:
   */
  void * TMWDEFS_GLOBAL mmbsim_init(
    TMWSESN *pSession);

  /* function: mmbsim_close
   * purpose:
   * arguments:
   * returns:
   */
  void TMWDEFS_GLOBAL mmbsim_close(
    void *pHandle);

  /* Clear database */
  TMWDEFS_SCL_API void TMWDEFS_GLOBAL mmbsim_clear(
    void *pHandle);

  /* Set update callback and parameter */
  TMWDEFS_SCL_API void mmbsim_setCallback(
    void *pHandle,
    MMBSIM_CALLBACK_FUNC pUpdateCallback,
    void *pUpdateCallbackParam);

  /* function: mmbsim_getPointNumber
   * purpose:
   * arguments:
   * returns:
   */
  TMWDEFS_SCL_API TMWTYPES_USHORT TMWDEFS_GLOBAL mmbsim_getPointNumber(
    void *pPoint);

  /* Coils */

  /* function: mmbsim_addCoil
   * purpose: Add a simulated coil
   * arguments:
   * returns:
   */
  TMWDEFS_SCL_API TMWSIM_POINT * TMWDEFS_GLOBAL mmbsim_addCoil(
    void *pHandle,
    TMWTYPES_USHORT pointNumber);

  /* function: mmbsim_deleteCoil
   * purpose: Delete a simulated coil
   * arguments:
   * returns:
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_deleteCoil(
    void *pHandle,
    TMWTYPES_USHORT pointNumber);

  /* function: mmbsim_coilLookupPoint
   * purpose:
   * arguments:
   * returns:
   */
  TMWDEFS_SCL_API TMWSIM_POINT * TMWDEFS_GLOBAL mmbsim_coilLookupPoint(
    void *pHandle,
    TMWTYPES_USHORT pointNumber);

  /* function: mmbsim_CoilRead
   * purpose: Read the specified Coil.
   * arguments:
   *  pPoint - point to write
   *  value - pointer to array of values
   * returns:
   *  TMWDEFS_TRUE if successful, else TMWDEFS_FALSE
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_coilRead(
    void *pPoint, 
    TMWTYPES_UCHAR *Value);

  /* function: mmbsim_storeCoils
   * purpose:
   * arguments:
   * returns:
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_storeCoils(
    void *pHandle,
    TMWTYPES_USHORT startAddr,
    TMWTYPES_USHORT length,
    TMWTYPES_UCHAR *pValueArray);

  /* Discrete Inputs */

  /* function: mmbsim_addDiscreteInput
   * purpose: Add a simulated discrete input
   * arguments:
   * returns:
   */
  TMWDEFS_SCL_API TMWSIM_POINT *TMWDEFS_GLOBAL mmbsim_addDiscreteInput(
    void *pHandle,
    TMWTYPES_USHORT pointNumber);

  /* function: mmbsim_deleteDiscreteInput
   * purpose: Delete a simulated discrete input
   * arguments:
   * returns:
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_deleteDiscreteInput(
    void *pHandle,
    TMWTYPES_USHORT pointNumber);

  /* function: mmbsim_DiscreteInputLookupPoint
   * purpose: Get discrete input point
   * arguments:
   * returns:
   */
  TMWDEFS_SCL_API TMWSIM_POINT * TMWDEFS_GLOBAL mmbsim_discreteInputLookupPoint(
    void *pHandle,
    TMWTYPES_USHORT pointNumber);

  /* function: mmbsim_discreteInputRead
   * purpose: Read the specified discrete input.
   * arguments:
   *  pPoint - handle to database point
   *  pValue - pointer to value
   * returns:
   *  TMWDEFS_TRUE if successful, else TMWDEFS_FALSE
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_discreteInputRead(
    void *pPoint,
    TMWTYPES_UCHAR *pValue);

  /* function: mmbsim_storeDiscreteInputs
   * purpose:
   * arguments:
   * returns:
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_storeDiscreteInputs(
    void *pHandle,
    TMWTYPES_USHORT startAddr,
    TMWTYPES_USHORT length,
    TMWTYPES_UCHAR *pValueArray);

  /* Input Registers */

  /* function: mmbsim_addInputRegister
   * purpose: Add a simulated input register
   * arguments:
   * returns:
   */
  TMWDEFS_SCL_API TMWSIM_POINT * TMWDEFS_GLOBAL mmbsim_addInputRegister(
    void *pHandle,
    TMWTYPES_USHORT pointNumber);

  /* function: mmbsim_deleteInputRegister
   * purpose: Delete a simulated input register
   * arguments:
   * returns:
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_deleteInputRegister(
    void *pHandle,
    TMWTYPES_USHORT pointNumber);

  /* function: mmbsim_inputRegisterLookupPoint
   * purpose:
   * arguments:
   * returns:
   */
  TMWDEFS_SCL_API TMWSIM_POINT *TMWDEFS_GLOBAL mmbsim_inputRegisterLookupPoint(
    void *pHandle,
    TMWTYPES_USHORT pointNumber);

  /* function: mmbsim_inputRegisterRead
   * purpose: Read the specified input register
   * arguments:
   *  pPoint - handle to database point 
   *  pValue - pointer to value
   * returns:
   *  TMWDEFS_TRUE if successful, else TMWDEFS_FALSE
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_inputRegisterRead(
    void *pPoint,
    TMWTYPES_USHORT *pValue);

  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_storeInputRegisters(
    void *pHandle,
    TMWTYPES_USHORT startAddr,
    TMWTYPES_USHORT length,
    TMWTYPES_USHORT *pValueArray);

  /* Holding Registers */

  /* function: mmbsim_addHoldingRegister
   * purpose: Add a simulated holding register
   * arguments:
   * returns:
   */
  TMWDEFS_SCL_API TMWSIM_POINT * TMWDEFS_GLOBAL mmbsim_addHoldingRegister(
    void *pHandle,
    TMWTYPES_USHORT pointNumber);

  /* function: mmbsim_deleteHoldingRegister
   * purpose: Delete a simulated holding register
   * arguments:
   * returns:
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_deleteHoldingRegister(
    void *pHandle,
    TMWTYPES_USHORT pointNumber);

  /* function: mmbsim_holdingRegisterLookupPoint
   * purpose:
   * arguments:
   * returns:
   */
  TMWDEFS_SCL_API TMWSIM_POINT *TMWDEFS_GLOBAL mmbsim_holdingRegisterLookupPoint(
    void *pHandle,
    TMWTYPES_USHORT pointNumber);

  /* function: mmbsim_holdingRegisterRead
   * purpose: Read the specified holding registers.
   * arguments:
   *  pPoint - handle returned by mmbsim_holdingRegisterLookupPoint
   *  pValue - pointer to value
   * returns:
   *  TMWDEFS_TRUE if successful, else TMWDEFS_FALSE
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_holdingRegisterRead(
    void *pPoint,
    TMWTYPES_USHORT *pValue);

  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_storeHoldingRegisters(
    void *pHandle,
    TMWTYPES_USHORT startAddr,
    TMWTYPES_USHORT length,
    TMWTYPES_USHORT *pValueArray);
  
  /* function: mmbsim_storeExceptionStatus
   * purpose: Store exception status value
   * arguments:
   *  pHandle - database handle returned from mmbsim_init
   *  value - 8 bit exception status value
   * returns:
   *  TMWDEFS_TRUE if successful, else TMWDEFS_FALSE
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_storeExceptionStatus(
    void *pHandle,
    TMWTYPES_UCHAR value);

  /* function: mmbsim_addDeviceId
   * purpose: Add a simulated device identifier
   * arguments:
   * returns:
   */
  TMWSIM_POINT *TMWDEFS_GLOBAL mmbsim_addDeviceId(
    void *pHandle,
    TMWTYPES_UCHAR objectId);
 
  /* function: mmbsim_deleteDeviceId
   * purpose:  
   * arguments:
   * returns:
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_deleteDeviceId(
    void *pHandle,
    TMWTYPES_UCHAR objectId);

  /* function: mmbsim_deviceIdLookupPoint
   * purpose:  
   * arguments:
   * returns:
   */
  TMWSIM_POINT *TMWDEFS_GLOBAL mmbsim_deviceIdLookupPoint(
    void *pHandle,
    TMWTYPES_UCHAR objectId);

  /* function: mmbsim_deviceIdRead
   * purpose:  
   * arguments:
   *  pLength -When called this points to the max length allowed
   *           when returned it is the length of the data in the buffer.
   * returns:
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_deviceIdRead(
    void *pPoint,
    TMWTYPES_UCHAR *pLength,
    TMWTYPES_UCHAR *pBuf);

  /* function: mmbsim_storeDeviceId
   * purpose:  
   * arguments:
   * returns:
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_storeDeviceId(
    void *pHandle,
    TMWTYPES_UCHAR objectId,
    TMWTYPES_UCHAR length,
    TMWTYPES_UCHAR *pBuf);

  /* function: mmbsim_exceptionStatusRead 
   * purpose: Read exception status value from database
   * arguments:
   *  pHandle - database handle returned from mmbsim_init
   *  value - pointer to data to hold exception status value
   * returns:
   *  TMWDEFS_TRUE if successful, else TMWDEFS_FALSE
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL mmbsim_exceptionStatusRead(
    void *pHandle,
    TMWTYPES_UCHAR *pValue);

#if TMWCNFG_SUPPORT_DIAG
  TMWDEFS_SCL_API void TMWDEFS_GLOBAL mmbsim_showData(
    TMWSESN *pSession);
#endif

  /* function: mmbsim_saveDatabase
   * purpose: Save database state to XML
   * arguments:
   *  pSector - sector containing database to save
   * returns:
   *  XML string
   */
  TMWDEFS_SCL_API TMWTYPES_CHAR * TMWDEFS_GLOBAL mmbsim_saveDatabase(
    TMWSESN *pSession);

#ifdef __cplusplus
}
#endif
#endif /* TMWCNFG_USE_SIMULATED_DB */
#endif /* MMBSIM_DEFINED */
