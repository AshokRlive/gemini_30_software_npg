/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: mbdiag.h
 * description: Generic Modbus diagnostics.
 */
#include "tmwscl/modbus/mbdiag.h"

#include "tmwscl/utils/tmwtarg.h"
#include "tmwscl/utils/tmwcnfg.h"
#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/utils/tmwdiag.h"

#include "tmwscl/modbus/mbdefs.h"
#include "tmwscl/modbus/mblink.h"

#if TMWCNFG_SUPPORT_DIAG

/* Define constants used below */
#define MAX_ROW_LENGTH 16

/* Local Functions */

/* function: _fcString
 * purpose: return string representation of function code
 * arguments:
 *  fc - function code
 * returns:
 *  constant string describing function code
 */
static const char * TMWDEFS_LOCAL _fcString(
  TMWTYPES_UCHAR fc)
{
  switch(fc & 0x7f)
  {
  case MBDEFS_FC_READ_COILS:                        return("Read Coils");
  case MBDEFS_FC_READ_DISCRETE_INPUTS:              return("Read Discrete Inputs");
  case MBDEFS_FC_READ_HOLDING_REGISTERS:            return("Read Holding Registers");
  case MBDEFS_FC_READ_READ_INPUT_REGISTERS:         return("Read Input Registers");
  case MBDEFS_FC_WRITE_SINGLE_COIL:                 return("Write Single Coil");
  case MBDEFS_FC_WRITE_SINGLE_REGISTER:             return("Write Single Holding Register");
  case MBDEFS_FC_READ_EXCEPTION_STATUS:             return("Read Exception Status");
  case MBDEFS_FC_DIAGNOSTICS:                       return("Diagnostics");
  case MBDEFS_FC_WRITE_MULTIPLE_COILS:              return("Write Multiple Coils");
  case MBDEFS_FC_WRITE_MULTIPLE_HOLDING_REGISTERS:  return("Write Multiple Holding Registers");
  case MBDEFS_FC_READ_WRITE_MULTIPLE_REGISTERS:     return("Read/Write Multiple Holding Registers");
  case MBDEFS_FC_43_ENCAPSULATED:                   return("Read Encapsulated (DeviceId or Encapsulated Interface)");
  default:                                          return("Unknown");
  }
}

static char * TMWDEFS_LOCAL _getResponseErrorMsg(
  TMWTYPES_UCHAR exceptionCode)
{
  switch(exceptionCode)
  {
  case MBDEFS_MBE_ILLEGAL_FUNCTION:                        return("Illegal Function Code");
  case MBDEFS_MBE_ILLEGAL_DATA_ADDRESS:                    return("Invalid address range in request");
  case MBDEFS_MBE_ILLEGAL_DATA_VALUE:                      return("Invalid quantity in request");
  case MBDEFS_MBE_SLAVE_DEVICE_FAILURE:                    return("Slave device failure reading database");
  case MBDEFS_MBE_ACKNOWLEDGE:                             return("Acknowledge");
  case MBDEFS_MBE_SLAVE_DEVICE_BUSY:                       return("Slave device busy");
  case MBDEFS_MBE_NAK:                                     return("Negative Acknowledge");
  case MBDEFS_MBE_MEMORY_PARITY_ERROR:                     return("Memory parity error");
  case MBDEFS_MBE_GATEWAY_PATH_UNAVAILABLE:                return("Gateway path unavailable");
  case MBDEFS_MBE_GATEWAY_TARGET_DEVICE_FAILED_TO_RESPOND: return("Gateway target device failed to respond");
  default:                                                 return("Unknown Exception");
  }
}

/* function: _displayFCargs
 * purpose: display application layer frame function code arguments
 * arguments:
 *  prompt - prompt for text
 *  channelName - name of channel
 *  pBuf - header data
 *  numBytes - number of bytes
 * returns:
 *  void
 */
static void TMWDEFS_LOCAL _displayFCargs(
  TMWCHNL *pChannel,
  TMWSESN *pSession,
  const char *prompt,
  const TMWTYPES_UCHAR *pBuf,
  TMWTYPES_USHORT numBytes,
  TMWTYPES_UCHAR sendReceive,
  TMWDIAG_ID direction)
{
  char buf[256];
  TMWDIAG_ANLZ_ID anlzId;
  TMWTYPES_UCHAR fc;
  
  TMWTARG_UNUSED_PARAM(numBytes);
  TMWTARG_UNUSED_PARAM(prompt);

  if(tmwdiag_initId(&anlzId, pChannel, pSession, TMWDEFS_NULL, TMWDIAG_ID_APPL | direction) == TMWDEFS_FALSE)
  {
    return;
  }

  fc = pBuf[0];
  switch (fc)
  {
  case MBDEFS_FC_READ_COILS:
  case MBDEFS_FC_READ_DISCRETE_INPUTS:
  case MBDEFS_FC_READ_HOLDING_REGISTERS:
  case MBDEFS_FC_READ_READ_INPUT_REGISTERS:
    if (pSession && pSession->protocol == TMWTYPES_PROTOCOL_MB && pSession->type == TMWTYPES_SESSION_TYPE_MASTER && sendReceive == _MBDIAG_SEND_MSG)
    {
      TMWTYPES_USHORT startReg = MBDEFS_MAKEWORD(pBuf[1], pBuf[2]);
      TMWTYPES_USHORT length   = MBDEFS_MAKEWORD(pBuf[3], pBuf[4]);
      tmwtarg_snprintf(buf, sizeof(buf), "%16s Starting Register=%d, Quantity=%d\n", " ", startReg, length);
    }
    else if (pSession && pSession->protocol == TMWTYPES_PROTOCOL_MB && pSession->type == TMWTYPES_SESSION_TYPE_SLAVE && sendReceive == _MBDIAG_RECEIVE_MSG)
    {
      TMWTYPES_USHORT startReg = MBDEFS_MAKEWORD(pBuf[1], pBuf[2]);
      TMWTYPES_USHORT length   = MBDEFS_MAKEWORD(pBuf[3], pBuf[4]);
      tmwtarg_snprintf(buf, sizeof(buf), "%16s Starting Register=%d, Quantity=%d\n", " ", startReg, length);
     
#ifdef TMW_SUPPORT_MONITOR  
      /* If monitor mode, check to see if quantity makes sense */
      /* If not they may have opened the monitor channel to listen to a master instead of a slave device */
      if((pChannel->pPhysContext->monitorMode)
        &&(length > MBDEFS_SMB_MAX_IREG_READ))
      {
        if(((fc == MBDEFS_FC_READ_HOLDING_REGISTERS)
          ||(fc == MBDEFS_FC_READ_READ_INPUT_REGISTERS))
          ||((length > MBDEFS_SMB_MAX_DI_READ)
          && ((fc == MBDEFS_FC_READ_COILS)
            ||(fc == MBDEFS_FC_READ_DISCRETE_INPUTS))))
        { 
          tmwdiag_putLine(&anlzId, buf);
          tmwtarg_snprintf(buf, sizeof(buf), "%16s Received Invalid Quantity,\n      should this channel have been configured to monitor data from a SLAVE device?\n", " ");
          if(tmwdiag_initId(&anlzId, pChannel, pSession, TMWDEFS_NULL, TMWDIAG_ID_APPL|TMWDIAG_ID_ERROR|TMWDIAG_ID_RX) == TMWDEFS_FALSE)
          {
            return;
          }
        }
      }
#endif
    }
    else if (pSession && pSession->protocol == TMWTYPES_PROTOCOL_MB && pSession->type == TMWTYPES_SESSION_TYPE_SLAVE && sendReceive == _MBDIAG_SEND_MSG)
    {
      TMWTYPES_USHORT byteCount = pBuf[1];
      tmwtarg_snprintf(buf, sizeof(buf), "%16s Byte Count=%d\n", " ", byteCount);
    }
    else if (pSession && pSession->protocol == TMWTYPES_PROTOCOL_MB && pSession->type == TMWTYPES_SESSION_TYPE_MASTER && sendReceive == _MBDIAG_RECEIVE_MSG)
    {
      TMWTYPES_USHORT byteCount = pBuf[1];
      tmwtarg_snprintf(buf, sizeof(buf), "%16s Byte Count=%d\n", " ", byteCount);
    }
    else
    {
      return;
    }
    break;
  default:
    return;
  }

  tmwdiag_putLine(&anlzId, buf);
}

/* function: _displayBytes
 * purpose: display  
 * arguments:
 
 *  pBuf - header data
 *  numBytes - number of bytes
 * returns:
 *  void
 */
static void TMWDEFS_LOCAL _displayBytes(
  TMWDIAG_ANLZ_ID *pAnlzId,
  TMWTYPES_USHORT numBytes,
  const TMWTYPES_UCHAR *pBuf)
 {
  TMWTYPES_USHORT rowLength;
  TMWTYPES_USHORT index;
  int len;
  TMWTYPES_USHORT i;
  TMWTYPES_CHAR buf[256];

  /* Now display the actual bytes */
  index = 0;
  while(index < numBytes)
  {
    len = tmwtarg_snprintf(buf, sizeof(buf), "%16s", " ");

    rowLength = (TMWTYPES_USHORT)(numBytes - index);
    if(rowLength > MAX_ROW_LENGTH)
      rowLength = MAX_ROW_LENGTH;

    for(i = 0; i < rowLength; i++)
      len += tmwtarg_snprintf(buf + len, sizeof(buf) - len, "%02x ", pBuf[index++]);

    tmwtarg_snprintf(buf + len, sizeof(buf) - len, "\n");
    tmwdiag_putLine(pAnlzId, buf);
  }
}

/* function: _displayApplFrame
 * purpose: display application layer frame
 * arguments:
 *  prompt - prompt for text
 *  channelName - name of channel
 *  pBuf - header data
 *  numBytes - number of bytes
 * returns:
 *  void
 */
static void TMWDEFS_LOCAL _displayApplFrame(
  TMWCHNL *pChannel,
  TMWSESN *pSession,
  const char *prompt,
  const TMWTYPES_UCHAR *pBuf,
  TMWTYPES_USHORT numBytes,
  TMWTYPES_UCHAR sendReceive,
  TMWDIAG_ID direction)
{
  TMWDIAG_ANLZ_ID anlzId;
  TMWTYPES_UCHAR fc;
  char buf[256];

  if(tmwdiag_initId(&anlzId, pChannel, pSession, TMWDEFS_NULL, TMWDIAG_ID_APPL | direction) == TMWDEFS_FALSE)
  {
    return;
  }

  fc = pBuf[0];

  tmwtarg_snprintf(buf, sizeof(buf), "%s %-10s Application Header, %s\n",
  prompt, tmwsesn_getSessionName(pSession), _fcString(fc));
  
  tmwdiag_skipLine(&anlzId);
  tmwdiag_putLine(&anlzId, buf);
  _displayFCargs(pChannel, pSession, prompt, pBuf, numBytes, sendReceive, direction);

  _displayBytes(&anlzId, numBytes, pBuf);
}

/* function: _displayLINKargs
 * purpose: display application layer frame function code arguments
 * arguments:
 *  prompt - prompt for text
 *  channelName - name of channel
 *  pBuf - header data
 *  numBytes - number of bytes
 * returns:
 *  void
 */
static void _displayLINKargs(
  TMWCHNL *pChannel,
  TMWSESN *pSession,
  const char *prompt,
  const TMWTYPES_UCHAR *pBuf,
  TMWTYPES_USHORT numBytes,
  TMWTYPES_UCHAR sendReceive,
  TMWDIAG_ID direction)
{
  char buf[256];
  TMWDIAG_ANLZ_ID anlzId;
  MBLINK_CONTEXT *pLinkContext = (MBLINK_CONTEXT *)pChannel->pLinkContext;
  MBLINK_TYPE linkType = pLinkContext->type;

  TMWTARG_UNUSED_PARAM(sendReceive);
  TMWTARG_UNUSED_PARAM(numBytes);
  TMWTARG_UNUSED_PARAM(prompt);

  if(tmwdiag_initId(&anlzId, pChannel, pSession, TMWDEFS_NULL, TMWDIAG_ID_LINK | direction) == TMWDEFS_FALSE)
  {
    return;
  }

  switch (linkType)
  {
  case MBLINK_TYPE_TCP:
    {
      TMWTYPES_USHORT transID = MBDEFS_MAKEWORD(pBuf[0], pBuf[1]);
      TMWTYPES_USHORT protoID   = MBDEFS_MAKEWORD(pBuf[2], pBuf[3]);
      TMWTYPES_USHORT len   = MBDEFS_MAKEWORD(pBuf[4],pBuf[5]);
      TMWTYPES_USHORT dest   = pBuf[6];
      tmwtarg_snprintf(buf, sizeof(buf), "%16s TransID=%d, ProtoID=%d, DataLength=%d, Dest=%d\n", " ", transID, protoID, len, dest);
    }
    break;
  default:
    return;
  }

  tmwdiag_putLine(&anlzId, buf);
}

/* function: _displayLinkFrame
 * purpose: display link layer frame
 * arguments:
 *  prompt - prompt for text
 *  channelName - name of channel
 *  pBuf - header data
 *  numBytes - number of bytes
 * returns:
 *  void
 */
static void TMWDEFS_LOCAL _displayLinkFrame(
  TMWCHNL *pChannel,
  TMWSESN *pSession,
  const char *prompt,
  const TMWTYPES_UCHAR *pBuf,
  TMWTYPES_USHORT numBytes,
  TMWTYPES_UCHAR sendReceive,
  TMWDIAG_ID direction)
{
  TMWDIAG_ANLZ_ID anlzId;
  TMWTYPES_CHAR buf[256];
  char *pLinkTypeName;

  MBLINK_CONTEXT *pLinkContext = (MBLINK_CONTEXT *)pChannel->pLinkContext;
  MBLINK_TYPE linkType = pLinkContext->type;

  if(tmwdiag_initId(&anlzId, pChannel, pSession, TMWDEFS_NULL, TMWDIAG_ID_LINK | direction) == TMWDEFS_FALSE)
  {
    return;
  }

  switch (linkType)
  {
  case MBLINK_TYPE_TCP:
    {
      pLinkTypeName = "Modbus TCP";
    }
    break;
  case MBLINK_TYPE_ASCII:
    {
      pLinkTypeName = "Modbus ASCII";
    }
    break;
  case MBLINK_TYPE_RTU:
    {
      pLinkTypeName = "Modbus RTU";
    }
    break;
  case MBLINK_TYPE_PLUS:
    {
      pLinkTypeName = "Modbus PLUS";
    }
    break;
  default:
    pLinkTypeName = "";
    return;
  }

  /* Display header information */
  tmwtarg_snprintf(buf, sizeof(buf), "%s %-10s Link Header\n",
    prompt, tmwchnl_getChannelName(pChannel), pLinkTypeName);

  tmwdiag_skipLine(&anlzId);
  tmwdiag_putLine(&anlzId, buf);

  _displayLINKargs(pChannel, pSession, prompt, pBuf, numBytes, sendReceive, direction);

  /* Now display the actual bytes */
  _displayBytes(&anlzId, numBytes, pBuf);
}

/* Global Functions */

/* function: mbdiag_insertQueue */
void TMWDEFS_GLOBAL mbdiag_insertQueue(
  TMWCHNL *pChannel,
  TMWSESN *pSession,
  const char *description)
{
  TMWDIAG_ANLZ_ID id;
  char buf[128];

  if(tmwdiag_initId(&id, pChannel, pSession, TMWDEFS_NULL, TMWDIAG_ID_USER) == TMWDEFS_FALSE)
  {
    return;
  }

  tmwtarg_snprintf(buf, sizeof(buf), "<+++ %-10s Insert request in queue: %s\n",
    tmwsesn_getSessionName(pSession), description);

  tmwdiag_skipLine(&id);
  tmwdiag_putLine(&id, buf);
}

void TMWDEFS_GLOBAL mbdiag_removeMessage(
  TMWSESN_TX_DATA *pTxData, 
  MBCHNL_RESP_STATUS status)
{
  TMWDIAG_ANLZ_ID id;
  char buf[128];
  const char *pStatusString;

  switch(status)
  {
  case MBCHNL_RESP_STATUS_FAILURE:
    pStatusString = "Failure";
    break;
  case MBCHNL_RESP_STATUS_TIMEOUT:
    pStatusString = "Timeout";
    break;
  case MBCHNL_RESP_STATUS_CANCELED:
    pStatusString = "Canceled";
    break;
  default:
    return;
  }

  if(tmwdiag_initId(&id, pTxData->pChannel, pTxData->pSession, TMWDEFS_NULL, TMWDIAG_ID_USER) == TMWDEFS_FALSE)
  {
    return;
  }

  tmwtarg_snprintf(buf, sizeof(buf), "<+++ %-10s Remove request from queue:\n", 
    tmwsesn_getSessionName(pTxData->pSession));

  tmwdiag_skipLine(&id);
  tmwdiag_putLine(&id, buf);

  tmwtarg_snprintf(buf, sizeof(buf), "%16s%s, %s\n", 
    " ", pTxData->pMsgDescription, pStatusString);

  tmwdiag_putLine(&id, buf);
}

/* function: mbdiag_showCoil */
void TMWDEFS_GLOBAL mbdiag_showCoil(
  TMWSESN *pSession,
  TMWTYPES_USHORT point,
  TMWTYPES_USHORT value)
{
  TMWDIAG_ANLZ_ID anlzId;
  char buf[256];
  TMWDIAG_ID direction = 0;

  if(pSession->type == TMWTYPES_SESSION_TYPE_MASTER)
    direction = TMWDIAG_ID_RX;

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, pSession, TMWDEFS_NULL, TMWDIAG_ID_STATIC_DATA | direction) == TMWDEFS_FALSE)
  {
    return;
  }
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s Coil %01d = %d\n", " ", point, value);
  tmwdiag_putLine(&anlzId, buf);
}

/* function: mbdiag_showDiscreteInput */
void TMWDEFS_GLOBAL mbdiag_showDiscreteInput(
  TMWSESN *pSession,
  TMWTYPES_USHORT point,
  TMWTYPES_USHORT value)
{
  TMWDIAG_ANLZ_ID anlzId;
  char buf[256];
  TMWDIAG_ID direction = 0;

  if(pSession->type == TMWTYPES_SESSION_TYPE_MASTER)
    direction = TMWDIAG_ID_RX;

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, pSession, TMWDEFS_NULL, TMWDIAG_ID_STATIC_DATA | direction) == TMWDEFS_FALSE)
  {
    return;
  }
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s Discrete Input %01d = %d\n", " ", point, value);
  tmwdiag_putLine(&anlzId, buf);
}

/* function: mbdiag_showInputRegister */
void TMWDEFS_GLOBAL mbdiag_showInputRegister(
  TMWSESN *pSession,
  TMWTYPES_USHORT point,
  TMWTYPES_USHORT value)
{
  TMWDIAG_ANLZ_ID anlzId;
  char buf[256];
  TMWDIAG_ID direction = 0;

  if(pSession->type == TMWTYPES_SESSION_TYPE_MASTER)
    direction = TMWDIAG_ID_RX;

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, pSession, TMWDEFS_NULL, TMWDIAG_ID_STATIC_DATA | direction) == TMWDEFS_FALSE)
  {
    return;
  }

  tmwtarg_snprintf(buf, sizeof(buf), "%-21s Input Register %06d = %d (0x%x)\n", " ", point, value, value);
  tmwdiag_putLine(&anlzId, buf);
}

/* function: mbdiag_showHoldingRegister */
void TMWDEFS_GLOBAL mbdiag_showHoldingRegister(
  TMWSESN *pSession,
  TMWTYPES_USHORT point,
  TMWTYPES_USHORT value)
{
  TMWDIAG_ANLZ_ID anlzId;
  char buf[256];
  TMWDIAG_ID direction = 0;

  if(pSession->type == TMWTYPES_SESSION_TYPE_MASTER)
    direction = TMWDIAG_ID_RX;

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, pSession, TMWDEFS_NULL, TMWDIAG_ID_STATIC_DATA | direction) == TMWDEFS_FALSE)
  {
    return;
  }
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s Holding Register %06d = %d (0x%x)\n", " ", point, value, value);
  tmwdiag_putLine(&anlzId, buf);
}

/* function: mbdiag_showExceptionStatus */
void TMWDEFS_GLOBAL mbdiag_showExceptionStatus(
  TMWSESN *pSession,
  TMWTYPES_USHORT value)
{
  TMWDIAG_ANLZ_ID anlzId;
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, pSession, TMWDEFS_NULL, TMWDIAG_ID_STATIC_DATA | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s Exception Status = 0x%x\n", " ", value);
  tmwdiag_putLine(&anlzId, buf);
}

/* function: mbdiag_showDiagsResponse*/
void TMWDEFS_GLOBAL mbdiag_showDiagsResponse(
  TMWSESN *pSession,
  TMWTYPES_USHORT subFunction,
  TMWTYPES_UCHAR numBytes,
  const TMWTYPES_UCHAR *pBuf)
{
  TMWDIAG_ANLZ_ID anlzId;
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, pSession, TMWDEFS_NULL, TMWDIAG_ID_STATIC_DATA | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s Diagnostics Sub-function = 0x%x\n", " ", subFunction);
  tmwdiag_putLine(&anlzId, buf);
  _displayBytes(&anlzId, numBytes, pBuf);
}

static TMWTYPES_BOOL TMWDEFS_LOCAL _isPrintable(
  const TMWTYPES_UCHAR *pBuf,
  TMWTYPES_USHORT length)
{
  int i;
  for(i=0; i<length; i++)
  {
    if(((*(pBuf+i))<0x20) || ((*(pBuf+i))>0x7e))
    {
      return(TMWDEFS_FALSE);
    }
  }
  return(TMWDEFS_TRUE);
}

/* function: mbdiag_showDeviceId*/
void TMWDEFS_GLOBAL mbdiag_showDeviceId(
  TMWSESN *pSession,
  TMWTYPES_UCHAR objectId,
  TMWTYPES_UCHAR numBytes,
  const TMWTYPES_UCHAR *pBuf)
{
  TMWDIAG_ID direction;
  TMWDIAG_ANLZ_ID anlzId;
  char buf[300];

  direction = 0;
  if(pSession->type == TMWTYPES_SESSION_TYPE_MASTER)
    direction = TMWDIAG_ID_RX;

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, pSession, TMWDEFS_NULL, TMWDIAG_ID_STATIC_DATA | direction) == TMWDEFS_FALSE)
  {
    return;
  }
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s Device Identification Object Id = %d\n", " ", objectId);
  tmwdiag_putLine(&anlzId, buf);

  if(_isPrintable(pBuf, numBytes))
  {
    char tempBuf[256];
    memcpy(tempBuf, pBuf, numBytes);
    tempBuf[numBytes] = 0;
    tmwtarg_snprintf(buf, sizeof(buf), "%-21s  %s\n", " ", tempBuf);
    tmwdiag_putLine(&anlzId, buf);
  }
  else
    _displayBytes(&anlzId, numBytes, pBuf);
}

/* function: mbdiag_showDeviceIdMore*/
void TMWDEFS_GLOBAL mbdiag_showDeviceIdData(
  TMWSESN *pSession,
  TMWTYPES_UCHAR moreFollows,
  TMWTYPES_UCHAR nextObjectId,
  TMWTYPES_UCHAR conformityLevel)
{
  TMWDIAG_ID direction;
  TMWDIAG_ANLZ_ID anlzId;
  char buf[128];

  direction = 0;
  if(pSession->type == TMWTYPES_SESSION_TYPE_MASTER)
    direction = TMWDIAG_ID_RX;

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, pSession, TMWDEFS_NULL, TMWDIAG_ID_STATIC_DATA | direction) == TMWDEFS_FALSE)
  {
    return;
  }
  
  if(moreFollows == 0xff)
    tmwtarg_snprintf(buf, sizeof(buf), "%-21s Device Identification Conformity Level = 0x%x More Follows, Next Object Id = %d\n", " ", conformityLevel, nextObjectId);
  else
    tmwtarg_snprintf(buf, sizeof(buf), "%-21s Device Identification Conformity Level = 0x%x Next Object Id = %d\n", " ", conformityLevel, nextObjectId);
  tmwdiag_putLine(&anlzId, buf);
}


/* function: mbdiag_frameSent */
void TMWDEFS_GLOBAL mbdiag_frameSent(
  TMWCHNL *pChannel,
  TMWSESN *pSession,
  const TMWTYPES_UCHAR *pFrame,
  TMWTYPES_USHORT numBytes)
{
  _displayApplFrame(pChannel, pSession, "<===", pFrame, numBytes, _MBDIAG_SEND_MSG, 0);
}

/* function: mbdiag_frameReceived */
void TMWDEFS_GLOBAL mbdiag_frameReceived(
  TMWCHNL *pChannel,
  TMWSESN *pSession,
  const TMWTYPES_UCHAR *pFrame,
  TMWTYPES_USHORT numBytes)
{
  _displayApplFrame(pChannel, pSession, "===>", pFrame, numBytes, _MBDIAG_RECEIVE_MSG, TMWDIAG_ID_RX);
}

/* function: mbdiag_linkFrameSent */
void TMWDEFS_GLOBAL mbdiag_linkFrameSent(
  TMWCHNL *pChannel,
  TMWSESN *pSession,
  const TMWTYPES_UCHAR *pFrame,
  TMWTYPES_USHORT numBytes)
{
  _displayLinkFrame(pChannel, pSession, "<---", pFrame, numBytes, _MBDIAG_SEND_MSG, 0);
}

/* function: mbdiag_linkFrameReceived */
void TMWDEFS_GLOBAL mbdiag_linkFrameReceived(
  TMWCHNL *pChannel,
  TMWSESN *pSession,
  const TMWTYPES_UCHAR *pFrame,
  TMWTYPES_USHORT numBytes)
{
  _displayLinkFrame(pChannel, pSession, "--->", pFrame, numBytes, _MBDIAG_RECEIVE_MSG, TMWDIAG_ID_RX);
}

/* function: mbdiag_error */
void TMWDEFS_GLOBAL mbdiag_error(
  TMWCHNL *pChannel,
  TMWSESN *pSession,
  const char *errorMsg)
{
  TMWDIAG_ANLZ_ID anlzId;
  char buf[256];

  if(tmwdiag_initId(&anlzId, pChannel, pSession, TMWDEFS_NULL, TMWDIAG_ID_APPL | TMWDIAG_ID_ERROR) == TMWDEFS_FALSE)
  {
    return;
  }
 
  tmwtarg_snprintf(buf, sizeof(buf), "**** %s: %s ****\n", tmwsesn_getSessionName(pSession), errorMsg);
  
  tmwdiag_skipLine(&anlzId);
  tmwdiag_putLine(&anlzId, buf);
}

/* function: mbdiag_response_error */
void TMWDEFS_GLOBAL mbdiag_response_error(
  TMWSESN_RX_DATA *pRequest, 
  TMWTYPES_UCHAR exceptionCode)
{
  TMWDIAG_ANLZ_ID anlzId;
  char buf[256];
  char *pErrorMsg = _getResponseErrorMsg(exceptionCode);

  if(tmwdiag_initId(&anlzId, pRequest->pSession->pChannel, pRequest->pSession, TMWDEFS_NULL, TMWDIAG_ID_APPL | TMWDIAG_ID_ERROR) == TMWDEFS_FALSE)
  {
    return;
  }

  tmwtarg_snprintf(buf, sizeof(buf), "**** %s: %s MBFC %d ****\n", tmwsesn_getSessionName(pRequest->pSession), pErrorMsg,pRequest->pMsgBuf[0]);
 
  tmwdiag_skipLine(&anlzId);
  tmwdiag_putLine(&anlzId, buf);
}

#endif /* TMWCNFG_SUPPORT_DIAG */
