/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: mbdefs.h
 * description: Generic Modbus definitions
 */
#ifndef MBDEFS_DEFINED
#define MBDEFS_DEFINED

/* object types */
/* Holding(4x) and Input(3x) are correct below */
#define MBDEFS_OBJ_COIL                           0 /* R/W */
#define MBDEFS_OBJ_DISCRETE_INPUT_REGISTER        1 /* R   */
#define MBDEFS_OBJ_HOLDING_REGISTER               4 /* R/W */
#define MBDEFS_OBJ_INPUT_REGISTER                 3 /* R   */

#define MBDEFS_OBJ_STR_COIL                       "Coil"
#define MBDEFS_OBJ_STR_DISCRETE_INPUT_REGISTER    "Discrete Input Register"
#define MBDEFS_OBJ_STR_HOLDING_REGISTER           "Holding Register"
#define MBDEFS_OBJ_STR_INPUT_REGISTER             "Input Register"

/* This is not one of the standard objects but is used by the library for device identification objects */
#define MBDEFS_OBJ_DEVICE_ID                       43 /* R   */
#define MBDEFS_FC_STR_READ_DEVICE_ID              "Device Identification Object"


/* function codes */
#define MBDEFS_FC_READ_COILS                         1
#define MBDEFS_FC_READ_DISCRETE_INPUTS               2
#define MBDEFS_FC_READ_HOLDING_REGISTERS             3
#define MBDEFS_FC_READ_READ_INPUT_REGISTERS          4
#define MBDEFS_FC_WRITE_SINGLE_COIL                  5
#define MBDEFS_FC_WRITE_SINGLE_REGISTER              6
#define MBDEFS_FC_READ_EXCEPTION_STATUS              7
#define MBDEFS_FC_DIAGNOSTICS                        8
#define MBDEFS_FC_WRITE_MULTIPLE_COILS              15
#define MBDEFS_FC_WRITE_MULTIPLE_HOLDING_REGISTERS  16
#define MBDEFS_FC_READ_WRITE_MULTIPLE_REGISTERS     23
#define MBDEFS_FC_43_ENCAPSULATED                   43
#define MBDEFS_FC_READ_DEVICE_ID                    43


/* Sub-function codes for FC 8 Diagnostics. */
#define MBDEFS_DIAG_QUERY_DATA                       0
#define MBDEFS_DIAG_RESTART_COMM                     1
#define MBDEFS_DIAG_RETURN_DIAG_REGISTER             2
#define MBDEFS_DIAG_CHANGE_ASCII_DELIM               3
#define MBDEFS_DIAG_FORCE_LISTEN_ONLY                4
/* 5-9  Reserved */
#define MBDEFS_DIAG_CLEAR_BUS_COUNTERS               10
#define MBDEFS_DIAG_GET_BUS_MSG_COUNT                11
#define MBDEFS_DIAG_GET_BUS_CRC_ERR_COUNT            12 
#define MBDEFS_DIAG_GET_BUS_EXC_COUNT                13
#define MBDEFS_DIAG_GET_SLAVE_MSG_COUNT              14
#define MBDEFS_DIAG_GET_SLAVE_NR_COUNT               15 
#define MBDEFS_DIAG_GET_SLAVE_NAK_COUNT              16
#define MBDEFS_DIAG_GET_SLAVE_BUSY_COUNT             17
#define MBDEFS_DIAG_GET_BUS_OVERRUN_COUNT            18
/* 18  Reserved */
#define MBDEFS_DIAG_CLEAR_OVERRUN_COUNT              20

#define MBDEFS_DIAG_RESTART_CLEAR                0xff00
#define MBDEFS_DIAG_RESTART_NOCLEAR              0x0000


/* The following defines affect all slave MB sessions.
   These definitions specify the maximum number of elements allowed to read
   or write as specified by the Modbus protocol
*/
#define MBDEFS_SMB_MAX_DI_READ                    2000
#define MBDEFS_SMB_MAX_COIL_READ                  2000
#define MBDEFS_SMB_MAX_COIL_WRITE                 1968
#define MBDEFS_SMB_MAX_IREG_READ                   125
#define MBDEFS_SMB_MAX_HREG_READ                   125
#define MBDEFS_SMB_MAX_HREG_WRITE                  123
#define MBDEFS_SMB_MAX_HREG_READ_FC23              125
#define MBDEFS_SMB_MAX_HREG_WRITE_FC23             121

/* For the master */
#define MBDEFS_MMB_MAX_COIL_WRITE                 1968
#define MBDEFS_MMB_MAX_HREG_WRITE                  123
#define MBDEFS_MMB_MAX_HREG_READ_FC23              125
#define MBDEFS_MMB_MAX_HREG_WRITE_FC23             121

/* 
 * Modbus Utility Macros and Definitions
 */
#define MBDEFS_MAKEWORD(x,y) ((TMWTYPES_USHORT)(((TMWTYPES_USHORT)x << 8) + y))
#define MBDEFS_HIBYTE(x)     ((TMWTYPES_UCHAR)((TMWTYPES_USHORT)x >> 8))
#define MBDEFS_LOBYTE(x)     ((TMWTYPES_UCHAR)((TMWTYPES_USHORT)x & 0x00ff))
#define MBDEFS_LONIBBLE(a)	 (a & 0x0F)
#define MBDEFS_HINIBBLE(a)	 ((a>>4) & 0x0F)
#define MBDEFS_LOCHAR(a)		 (MBDEFS_LONIBBLE(a) <= 9) ? (MBDEFS_LONIBBLE(a) + '0') : (MBDEFS_LONIBBLE(a) + 'A' - 0x0A) 
#define MBDEFS_HICHAR(a)		 (MBDEFS_HINIBBLE(a) <= 9) ? (MBDEFS_HINIBBLE(a) + '0') : (MBDEFS_HINIBBLE(a) + 'A' - 0x0A) 


/* 
 * Modbus Exception codes 
 */
#define MBDEFS_MBE_ILLEGAL_FUNCTION                          0x01
#define MBDEFS_MBE_ILLEGAL_DATA_ADDRESS                      0x02
#define MBDEFS_MBE_ILLEGAL_DATA_VALUE                        0x03
#define MBDEFS_MBE_SLAVE_DEVICE_FAILURE                      0x04
#define MBDEFS_MBE_ACKNOWLEDGE                               0x05
#define MBDEFS_MBE_SLAVE_DEVICE_BUSY                         0x06
#define MBDEFS_MBE_NAK                                       0x07
#define MBDEFS_MBE_MEMORY_PARITY_ERROR                       0x08
#define MBDEFS_MBE_GATEWAY_PATH_UNAVAILABLE                  0x0A
#define MBDEFS_MBE_GATEWAY_TARGET_DEVICE_FAILED_TO_RESPOND   0x0B


/* For FC 43 MEI Type 15 Read Device Identification  
    Identification conformity level of the device and type of supported access
    0x01: basic identification (stream access only)
    0x02: regular identification (stream access only)
    0x03: extended identification (stream access only)
    0x81: basic identification (stream access and individual access)
    0x82: regular identification (stream access and individual access)
    0x83: extended identification(stream access and individual access)
  */
#define MBDEFS_DEVID_BASICSTREAM        0x01
#define MBDEFS_DEVID_REGULARSTREAM      0x02
#define MBDEFS_DEVID_EXTENDEDSTREAM     0x03
#define MBDEFS_DEVID_BASICSTREAMIND     0x81
#define MBDEFS_DEVID_REGULARSTREAMIND   0x82
#define MBDEFS_DEVID_EXTENDEDSTREAMIND  0x83

#endif
