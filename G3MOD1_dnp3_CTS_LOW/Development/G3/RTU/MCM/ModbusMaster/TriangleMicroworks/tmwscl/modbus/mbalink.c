/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: mbalink.h
 * description: Modbus ASCII Link Layer.
 */
#include "tmwscl/modbus/mbdiag.h"
#include "tmwscl/modbus/mbdefs.h"
#include "tmwscl/modbus/mbalink.h"
#include "tmwscl/modbus/mblink.h"
 
#include "tmwscl/utils/tmwtarg.h"

#if MBCNFG_SUPPORT_ASCII

/* function: mbalink_initConfig */
void TMWDEFS_GLOBAL mbalink_initConfig(
  MBALINK_CONFIG *pConfig)
{
  /*  Rarely used MBFC 8, diagnostic function can change these delimiters */ 
  pConfig->delim1 = '\r';
  pConfig->delim2 = '\n';
}

/* function: mbalink_getNeededBytes */
TMWTYPES_USHORT TMWDEFS_CALLBACK mbalink_getNeededBytes(
  void *pCallbackParam)
{
  TMWTARG_UNUSED_PARAM(pCallbackParam);
  
  /* Read as many bytes as many as you can */
  return(MBLINK_RX_FRAME_SIZE);
}

/* function: mbalink_parseBytes */
void TMWDEFS_CALLBACK mbalink_parseBytes(
  void *pCallbackParam,
  TMWTYPES_UCHAR *recvBuf,
  TMWTYPES_USHORT numBytes,
  TMWTYPES_MILLISECONDS firstByteTime)
{
  MBLINK_CONTEXT *pLinkContext = (MBLINK_CONTEXT *)pCallbackParam;
  TMWTYPES_USHORT offset = 0;
  TMWTYPES_USHORT newBytesRead;
  TMWTYPES_UCHAR sum;

#ifdef TMW_SUPPORT_MONITOR
  TMWTYPES_USHORT savedMonitorLength = 0;
#endif

  if(pLinkContext->rxOffset == 0)
  { 
    if(firstByteTime == 0)
    {
      pLinkContext->rxFrame.firstByteTime = tmwtarg_getMSTime();
    }
    else
    {
      pLinkContext->rxFrame.firstByteTime = firstByteTime;
    }

    while(recvBuf[offset] != ':')
    {
      offset += 1;
      if(offset >= numBytes)
        return;
    }
  }

  newBytesRead = numBytes - offset;

  /* Make sure received bytes won't overflow buffer */
  if((pLinkContext->rxOffset + newBytesRead) > MBLINK_RX_FRAME_SIZE)
  {
    /* Log Error, Never found delimiter */
    TMWDIAG_ERROR("Modbus ASCII - No delimiter");
    pLinkContext->rxOffset = 0;
    return;
  }

  memcpy(&pLinkContext->rxBuffer[pLinkContext->rxOffset], &recvBuf[offset], newBytesRead);
  pLinkContext->rxOffset += newBytesRead;

  if((pLinkContext->rxBuffer[pLinkContext->rxOffset - 2] == pLinkContext->delim1)
    && (pLinkContext->rxBuffer[pLinkContext->rxOffset - 1] == pLinkContext->delim2))
  {
    TMWTYPES_UCHAR *pRxBuf = pLinkContext->rxBuffer;
    TMWTYPES_UCHAR *pFrameBuf = pLinkContext->rxBinaryFrameBuffer;
    TMWTYPES_UCHAR addr;
    TMWSESN *pSession;
    TMWTYPES_UCHAR temp;
    int i, j;

    /* Parse Frame */

    /* get slave address */
    if ((pRxBuf[1]>='0') && (pRxBuf[1]<='9'))
      temp = (pRxBuf[1]-'0');
    else if ((pRxBuf[1]>='A') && (pRxBuf[1]<='F'))
      temp = (pRxBuf[1]-'A' + 10);
    else
    {
      /* Log Error, 
         char was not (0-F) */
      TMWDIAG_ERROR("Modbus ASCII - character was not (0-F)");
      pLinkContext->rxOffset = 0;
      return;
    }
    addr = temp << 4;
    if ((pRxBuf[2]>='0') && (pRxBuf[2]<='9'))
      temp = (pRxBuf[2]-'0');
    else if ((pRxBuf[2]>='A') && (pRxBuf[2]<='F'))
      temp = (pRxBuf[2]-'A' + 10);
    else
    {
      /* Log Error, 
         char was not (0-F) */
      TMWDIAG_ERROR("Modbus ASCII - character was not (0-F)");
      pLinkContext->rxOffset = 0;
      return;
    }
    addr |= temp;

    if(addr == MBLINK_BROADCAST_ADDRESS)
    {
      pLinkContext->rxFrame.isBroadcast = TMWDEFS_TRUE;
      pSession = TMWDEFS_NULL;
    }
    else
    {
#ifdef TMW_SUPPORT_MONITOR
      if(pLinkContext->tmw.pChannel->pPhysContext->monitorMode)
      {
        pSession = mblink_monitorModeFindSession((TMWLINK_CONTEXT *)pLinkContext, addr);
      }
      else
#endif
      pSession = mblink_findSession(pLinkContext, addr);
      if(pSession == TMWDEFS_NULL)
      {
        char buf[256];
        /* Log Error */
        /* Diagnostics */
        tmwtarg_snprintf(buf, sizeof(buf), "Modbus ASCII - session %d not found", addr);
        TMWDIAG_ERROR(buf);
        pLinkContext->rxOffset = 0;
        return;
      }
      pLinkContext->rxFrame.isBroadcast = TMWDEFS_FALSE;
    }

    sum = addr;
    for(i = 3, j = 0; i < pLinkContext->rxOffset - 2; i+=2, j++)
    {
      if ((pRxBuf[i]>='0') && (pRxBuf[i]<='9'))
        temp = (pRxBuf[i]-'0');
      else if ((pRxBuf[i]>='A') && (pRxBuf[i]<='F'))
        temp = (pRxBuf[i]-'A' + 10);

#ifdef TMW_SUPPORT_MONITOR
      /* In monitor mode it is possible to be receive multiple frames. 
       * Allow for that by calling this function recursively. 
       */
      else if((pRxBuf[i] == pLinkContext->delim1)
        && (pRxBuf[i+1] == pLinkContext->delim2)
        && (pRxBuf[i+2] == ':')) 
      {
        savedMonitorLength = i+2;
        break;
      }
#endif
      else
      {
        /* Log Error, 
           char was not (0-F) */
        TMWDIAG_ERROR("Modbus ASCII - character was not (0-F)");
        pLinkContext->rxOffset = 0;
        return;
      }
      pFrameBuf[j] = temp << 4;
      if ((pRxBuf[i+1]>='0') && (pRxBuf[i+1]<='9'))
        temp = (pRxBuf[i+1]-'0');
      else if ((pRxBuf[i+1]>='A') && (pRxBuf[i+1]<='F'))
        temp = (pRxBuf[i+1]-'A' + 10);
      else
      {
        /* Log Error, 
           char was not (0-F) */
        TMWDIAG_ERROR("Modbus ASCII - character was not (0-F)");
        pLinkContext->rxOffset = 0;
        return;
      }
      pFrameBuf[j] |= temp;
      sum += pFrameBuf[j];
    }
    
#ifdef TMW_SUPPORT_MONITOR
    if(savedMonitorLength >0)
    {
      MBDIAG_LINK_FRAME_RECEIVED(pLinkContext->tmw.pChannel, pSession, 
        pLinkContext->rxBuffer, savedMonitorLength);
    }
    else
#endif
      
    if(pSession != TMWDEFS_NULL)
      tmwsesn_setOnline(pSession, TMWDEFS_TRUE);

    MBDIAG_LINK_FRAME_RECEIVED(pLinkContext->tmw.pChannel, pSession, 
      pLinkContext->rxBuffer, pLinkContext->rxOffset);

    /* Update statistics */
    TMWCHNL_STAT_CALLBACK_FUNC(
      pLinkContext->tmw.pChannel,
      TMWCHNL_STAT_FRAME_RECEIVED, TMWDEFS_NULL);

    if (sum != 0)
    {
#ifdef TMWCNFG_SUPPORT_STATS
      TMWCHNL_STAT_ERROR_TYPE errorInfo;
      errorInfo.errorCode = TMWCHNL_ERROR_LINK_INVALID_CHECKSUM;
      errorInfo.pSession = pSession;
      TMWCHNL_STAT_CALLBACK_FUNC(pLinkContext->tmw.pChannel, TMWCHNL_STAT_ERROR, &errorInfo);
#endif 

      /* Log Error, cksum error */
      TMWDIAG_ERROR("Modbus ASCII - checksum error");

      pLinkContext->rxOffset = 0;
      return;
    }

    pLinkContext->rxFrame.msgLength = j - 1; /* don't include cksum */

    /* Send received frame to the transport layer and reset state */
    if(pLinkContext->tmw.pParseFunc)
    {
      if(pLinkContext->rxFrame.isBroadcast)
      {
        while((pSession = (TMWSESN *)tmwdlist_getAfter(
          &pLinkContext->tmw.sessions, (TMWDLIST_MEMBER *)pSession)) != TMWDEFS_NULL)
        { 
          if(pSession->active)
          {
            pLinkContext->rxFrame.pSession = pSession;
            pLinkContext->tmw.pParseFunc(pLinkContext->tmw.pCallbackParam,
              pSession, &pLinkContext->rxFrame);
          }
        }
      }
      else
      {
        pLinkContext->rxFrame.pSession = pSession;
        pLinkContext->tmw.pParseFunc(pLinkContext->tmw.pCallbackParam,
          pSession, &pLinkContext->rxFrame);
      }
    }

#ifdef TMW_SUPPORT_MONITOR
    if(savedMonitorLength >0)
    { 
      pLinkContext->rxOffset -= savedMonitorLength;
      memcpy(&pLinkContext->rxBuffer[0], &pLinkContext->rxBuffer[savedMonitorLength], pLinkContext->rxOffset);
      mbalink_parseBytes(pCallbackParam, recvBuf, 0, firstByteTime);
    }
#endif

    /* Start looking for a new frame */
    pLinkContext->rxOffset = 0;
  }
}

/* function: mbalink_transmitFrame */
void TMWDEFS_CALLBACK mbalink_transmitFrame(
  void *pContext,
  TMWSESN_TX_DATA *pTxDescriptor)
{
  MBLINK_CONTEXT *pLinkContext = (MBLINK_CONTEXT *)pContext;
  int offset = 0;
  int i;
  TMWTYPES_UCHAR sum = 0;
  TMWTYPES_UCHAR addr;

  if((pTxDescriptor->txFlags & TMWSESN_TXFLAGS_BROADCAST)==0)
  {
    /* If the session is not active simply return */
    if(!pTxDescriptor->pSession->active)
    {
#if TMWCNFG_SUPPORT_DIAG
      MBDIAG_ERROR(pTxDescriptor->pChannel,pTxDescriptor->pSession,
        "Attempt to send frame to inactive session");
#endif
      return;
    }
    addr = (TMWTYPES_UCHAR)pTxDescriptor->pSession->destAddress;
  }
  else
  {
    addr = MBLINK_BROADCAST_ADDRESS;
  }

  /* Header */
  pLinkContext->txFrameBuffer[offset++] = ':';
  pLinkContext->txFrameBuffer[offset++] = MBDEFS_HICHAR(addr);
  pLinkContext->txFrameBuffer[offset++] = MBDEFS_LOCHAR(addr);

  /* Convert to ASCII and calc cksum */
  sum = addr;
  for(i = 0; i < pTxDescriptor->msgLength; i++)
  {
    pLinkContext->txFrameBuffer[offset++] = MBDEFS_HICHAR(pTxDescriptor->pMsgBuf[i]);
    pLinkContext->txFrameBuffer[offset++] = MBDEFS_LOCHAR(pTxDescriptor->pMsgBuf[i]);
    sum += pTxDescriptor->pMsgBuf[i];
  }
  sum = (TMWTYPES_UCHAR) (-((char) sum));

  pLinkContext->txFrameBuffer[offset++] = MBDEFS_HICHAR(sum);
  pLinkContext->txFrameBuffer[offset++] = MBDEFS_LOCHAR(sum);

  pLinkContext->txFrameBuffer[offset++] = pLinkContext->delim1;
  pLinkContext->txFrameBuffer[offset++] = pLinkContext->delim2;

  pLinkContext->physTxDescriptor.pCallbackParam = pTxDescriptor;
  pLinkContext->physTxDescriptor.beforeTxCallback = TMWDEFS_NULL;
  pLinkContext->physTxDescriptor.afterTxCallback = mbchnl_afterTxCallback;
  pLinkContext->physTxDescriptor.failedTxCallback = TMWDEFS_NULL;
  pLinkContext->physTxDescriptor.numBytesToTx = offset;
  pLinkContext->physTxDescriptor.pTxBuffer = pLinkContext->txFrameBuffer;

  /* Update statistics */
  TMWCHNL_STAT_CALLBACK_FUNC(
    pLinkContext->tmw.pChannel,
    TMWCHNL_STAT_FRAME_SENT, TMWDEFS_NULL);

  MBDIAG_LINK_FRAME_SENT(pLinkContext->tmw.pChannel, pTxDescriptor->pSession, 
    pLinkContext->physTxDescriptor.pTxBuffer, 
    pLinkContext->physTxDescriptor.numBytesToTx);

  pTxDescriptor->pChannel->pPhys->pPhysTransmit(
    pTxDescriptor->pChannel->pPhysContext, &pLinkContext->physTxDescriptor);
}

#endif /* MBCNFG_SUPPORT_ASCII */
