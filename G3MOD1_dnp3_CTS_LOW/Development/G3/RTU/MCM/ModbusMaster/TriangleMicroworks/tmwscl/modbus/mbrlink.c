/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: mbrlink.h
 * description: Modbus RTU Link Layer.
 */
#include "tmwscl/modbus/mbdiag.h"
#include "tmwscl/modbus/mbdefs.h"
#include "tmwscl/modbus/mbrlink.h"
#include "tmwscl/modbus/mblink.h"
#include "tmwscl/utils/tmwtarg.h"

#if MBCNFG_SUPPORT_RTU
#if MBCNFG_PERFORM_RTU_CRC
#define MBRLINK_ADDR_AND_CRC_LEN 3

TMWTYPES_USHORT computeCRC16(TMWTYPES_UCHAR* pBuf, TMWTYPES_USHORT length)
{
static const TMWTYPES_UCHAR CRCHi[] = {
	0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,
	0x40,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,
	0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,
	0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,
	0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,
	0x40,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,
	0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,
	0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,
	0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,
	0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x01,0xC0,
	0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,
	0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,
	0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,
	0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x01,0xC0,
	0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,
	0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,
	0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,
	0x40
	} ;

static const TMWTYPES_UCHAR CRCLo[] = {
	0x00,0xC0,0xC1,0x01,0xC3,0x03,0x02,0xC2,0xC6,0x06,0x07,0xC7,0x05,0xC5,0xC4,
	0x04,0xCC,0x0C,0x0D,0xCD,0x0F,0xCF,0xCE,0x0E,0x0A,0xCA,0xCB,0x0B,0xC9,0x09,
	0x08,0xC8,0xD8,0x18,0x19,0xD9,0x1B,0xDB,0xDA,0x1A,0x1E,0xDE,0xDF,0x1F,0xDD,
	0x1D,0x1C,0xDC,0x14,0xD4,0xD5,0x15,0xD7,0x17,0x16,0xD6,0xD2,0x12,0x13,0xD3,
	0x11,0xD1,0xD0,0x10,0xF0,0x30,0x31,0xF1,0x33,0xF3,0xF2,0x32,0x36,0xF6,0xF7,
	0x37,0xF5,0x35,0x34,0xF4,0x3C,0xFC,0xFD,0x3D,0xFF,0x3F,0x3E,0xFE,0xFA,0x3A,
	0x3B,0xFB,0x39,0xF9,0xF8,0x38,0x28,0xE8,0xE9,0x29,0xEB,0x2B,0x2A,0xEA,0xEE,
	0x2E,0x2F,0xEF,0x2D,0xED,0xEC,0x2C,0xE4,0x24,0x25,0xE5,0x27,0xE7,0xE6,0x26,
	0x22,0xE2,0xE3,0x23,0xE1,0x21,0x20,0xE0,0xA0,0x60,0x61,0xA1,0x63,0xA3,0xA2,
	0x62,0x66,0xA6,0xA7,0x67,0xA5,0x65,0x64,0xA4,0x6C,0xAC,0xAD,0x6D,0xAF,0x6F,
	0x6E,0xAE,0xAA,0x6A,0x6B,0xAB,0x69,0xA9,0xA8,0x68,0x78,0xB8,0xB9,0x79,0xBB,
	0x7B,0x7A,0xBA,0xBE,0x7E,0x7F,0xBF,0x7D,0xBD,0xBC,0x7C,0xB4,0x74,0x75,0xB5,
	0x77,0xB7,0xB6,0x76,0x72,0xB2,0xB3,0x73,0xB1,0x71,0x70,0xB0,0x50,0x90,0x91,
	0x51,0x93,0x53,0x52,0x92,0x96,0x56,0x57,0x97,0x55,0x95,0x94,0x54,0x9C,0x5C,
	0x5D,0x9D,0x5F,0x9F,0x9E,0x5E,0x5A,0x9A,0x9B,0x5B,0x99,0x59,0x58,0x98,0x88,
	0x48,0x49,0x89,0x4B,0x8B,0x8A,0x4A,0x4E,0x8E,0x8F,0x4F,0x8D,0x4D,0x4C,0x8C,
	0x44,0x84,0x85,0x45,0x87,0x47,0x46,0x86,0x82,0x42,0x43,0x83,0x41,0x81,0x80,
	0x40
	} ;

	TMWTYPES_UCHAR uchCRCHi = 0xFF;
	TMWTYPES_UCHAR uchCRCLo = 0xFF;
	TMWTYPES_USHORT uIndex;

	while (length--)
	{
		uIndex = uchCRCHi ^ *pBuf++;
		uchCRCHi = uchCRCLo ^ CRCHi[uIndex];
		uchCRCLo = CRCLo[uIndex];
	}
	return (uchCRCLo<<8 | uchCRCHi);
}
#else
#define MBRLINK_ADDR_AND_CRC_LEN 1
#endif


/* function: mbrlink_initConfig */
void TMWDEFS_GLOBAL mbrlink_initConfig(
  MBRLINK_CONFIG *pConfig)
{
  pConfig->reserved = 1;
}

/* function: mbrlink_getNeededBytes */
TMWTYPES_USHORT TMWDEFS_CALLBACK mbrlink_getNeededBytes(
  void *pCallbackParam)
{
  TMWTARG_UNUSED_PARAM(pCallbackParam);

  /* Read as many bytes as many as you can */
  return(MBLINK_RX_FRAME_SIZE);
}

/* function: mbrlink_parseBytes */
/* NOTE: This should only be called when an entire message has been received  
 * Since there is no length in the message this code will assume the entire 
 * message has been received. If this is a partial message it will appear to 
 * be a CRC error.
 */
void TMWDEFS_CALLBACK mbrlink_parseBytes(
  void *pCallbackParam,
  TMWTYPES_UCHAR *recvBuf,
  TMWTYPES_USHORT numBytes,
  TMWTYPES_MILLISECONDS firstByteTime)
{
  MBLINK_CONTEXT *pLinkContext = (MBLINK_CONTEXT *)pCallbackParam;
  TMWTYPES_USHORT offset = 0;
  TMWTYPES_USHORT crc = 0;

  if(pLinkContext->rxOffset == 0)
  {
    if(firstByteTime == 0)
    {
      pLinkContext->rxFrame.firstByteTime = tmwtarg_getMSTime();
    }
    else
    {
      pLinkContext->rxFrame.firstByteTime = firstByteTime;
    }
  }

  /* Make sure received bytes won't overflow buffer */
  if((pLinkContext->rxOffset + numBytes) > MBLINK_RX_FRAME_SIZE)
  {
    TMWDIAG_ERROR("Modbus RTU - discarded receive chars");
    pLinkContext->rxOffset = 0;
    return;
  }

  memcpy(&pLinkContext->rxBuffer[pLinkContext->rxOffset], &recvBuf[offset], numBytes - offset);
  pLinkContext->rxOffset += numBytes - offset;

#if MBCNFG_PERFORM_RTU_CRC
  crc = computeCRC16(pLinkContext->rxBuffer, (TMWTYPES_USHORT)pLinkContext->rxOffset);
#endif
  if (crc == 0)
  {
    TMWTYPES_UCHAR *pRxBuf = pLinkContext->rxBuffer;
    TMWTYPES_UCHAR *pFrameBuf = pLinkContext->rxBinaryFrameBuffer;
    TMWTYPES_UCHAR addr;
    TMWSESN *pSession;

    /* Parse Frame */
    addr = pRxBuf[0];

    if(addr == MBLINK_BROADCAST_ADDRESS)
    {
      pLinkContext->rxFrame.isBroadcast = TMWDEFS_TRUE;
      pSession = TMWDEFS_NULL;
    }
    else
    {

#ifdef TMW_SUPPORT_MONITOR
      if(pLinkContext->tmw.pChannel->pPhysContext->monitorMode)
      {
        pSession = mblink_monitorModeFindSession((TMWLINK_CONTEXT *)pLinkContext, addr);
      }
      else
#endif
      pSession = mblink_findSession(pLinkContext, addr);
      if(pSession == TMWDEFS_NULL)
      {
        char buf[256];
        /* Log Error */
        /* Diagnostics */
        tmwtarg_snprintf(buf, sizeof(buf), "Modbus RTU - session %d not found", addr);
        TMWDIAG_ERROR(buf);
        pLinkContext->rxOffset = 0;
        return;
      }      
      pLinkContext->rxFrame.isBroadcast = TMWDEFS_FALSE;
    }
    
    if(pSession != TMWDEFS_NULL)
      tmwsesn_setOnline(pSession, TMWDEFS_TRUE);

    MBDIAG_LINK_FRAME_RECEIVED(pLinkContext->tmw.pChannel, pSession,
      pLinkContext->rxBuffer, pLinkContext->rxOffset);

    /* Update statistics */
    TMWCHNL_STAT_CALLBACK_FUNC(
      pLinkContext->tmw.pChannel,
      TMWCHNL_STAT_FRAME_RECEIVED, TMWDEFS_NULL);

    /* copy APDU into Frame Buf - don't include addr or CRC */
    memcpy(pFrameBuf, &pRxBuf[1], pLinkContext->rxOffset-MBRLINK_ADDR_AND_CRC_LEN);
    pLinkContext->rxFrame.msgLength = pLinkContext->rxOffset-MBRLINK_ADDR_AND_CRC_LEN;

    /* Send received frame to the transport layer and reset state */
    if(pLinkContext->tmw.pParseFunc)
    {
      if(pLinkContext->rxFrame.isBroadcast)
      {
        while((pSession = (TMWSESN *)tmwdlist_getAfter(
          &pLinkContext->tmw.sessions, (TMWDLIST_MEMBER *)pSession)) != TMWDEFS_NULL)
        { 
          if(pSession->active)
          {
            pLinkContext->rxFrame.pSession = pSession;
            pLinkContext->tmw.pParseFunc(pLinkContext->tmw.pCallbackParam,
              pSession, &pLinkContext->rxFrame);
          }
        }
      }
      else
      {
        pLinkContext->rxFrame.pSession = pSession;
        pLinkContext->tmw.pParseFunc(pLinkContext->tmw.pCallbackParam,
          pSession, &pLinkContext->rxFrame);
      }
    }

    /* Start looking for a new frame */
    pLinkContext->rxOffset = 0;
  }

#if MBCNFG_PERFORM_RTU_CRC
  /* CRC error */
  else
  {
#ifdef TMWCNFG_SUPPORT_STATS
    TMWCHNL_STAT_ERROR_TYPE errorInfo;
    errorInfo.errorCode = TMWCHNL_ERROR_LINK_INVALID_CHECKSUM;
    errorInfo.pSession = TMWDEFS_NULL;
    TMWCHNL_STAT_CALLBACK_FUNC(pLinkContext->tmw.pChannel, TMWCHNL_STAT_ERROR, &errorInfo);
#endif 
    TMWDIAG_ERROR("Modbus RTU - CRC error");

    pLinkContext->rxOffset = 0;
  }
#endif
}

/* function: mbrlink_transmitFrame */
void TMWDEFS_CALLBACK mbrlink_transmitFrame(
  void *pContext,
  TMWSESN_TX_DATA *pTxDescriptor)
{
  MBLINK_CONTEXT *pLinkContext = (MBLINK_CONTEXT *)pContext;
  TMWTYPES_USHORT crc = 0;
  TMWTYPES_USHORT offset = 0;
  TMWTYPES_USHORT i;
  TMWTYPES_UCHAR addr;

  if((pTxDescriptor->txFlags & TMWSESN_TXFLAGS_BROADCAST)==0)
  {
    /* If the session is not active simply return */
    if(!pTxDescriptor->pSession->active)
    {
#if TMWCNFG_SUPPORT_DIAG
      MBDIAG_ERROR(pTxDescriptor->pChannel, pTxDescriptor->pSession, "Attempt to send frame to inactive session");
#endif
      return;
    } 
    addr = (TMWTYPES_UCHAR)pTxDescriptor->pSession->destAddress;
  }
  else
  {
    addr = MBLINK_BROADCAST_ADDRESS;
  }

  /* Header */
  pLinkContext->txFrameBuffer[offset++] = addr;
  for(i = 0; i < pTxDescriptor->msgLength; i++)
  {
    pLinkContext->txFrameBuffer[offset++] = pTxDescriptor->pMsgBuf[i];
  }

#if MBCNFG_PERFORM_RTU_CRC
  crc = computeCRC16(pLinkContext->txFrameBuffer, offset);
  pLinkContext->txFrameBuffer[offset++] = MBDEFS_LOBYTE(crc);
  pLinkContext->txFrameBuffer[offset++] = MBDEFS_HIBYTE(crc);
#endif

  pLinkContext->physTxDescriptor.pCallbackParam   = pTxDescriptor;
  pLinkContext->physTxDescriptor.beforeTxCallback = TMWDEFS_NULL;
  pLinkContext->physTxDescriptor.afterTxCallback  = mbchnl_afterTxCallback;
  pLinkContext->physTxDescriptor.failedTxCallback = TMWDEFS_NULL;
  pLinkContext->physTxDescriptor.numBytesToTx = offset;
  pLinkContext->physTxDescriptor.pTxBuffer = pLinkContext->txFrameBuffer;

  /* Update statistics */
  TMWCHNL_STAT_CALLBACK_FUNC(
    pLinkContext->tmw.pChannel,
    TMWCHNL_STAT_FRAME_SENT, TMWDEFS_NULL);

  MBDIAG_LINK_FRAME_SENT(pLinkContext->tmw.pChannel, pTxDescriptor->pSession,
    pLinkContext->physTxDescriptor.pTxBuffer,
    pLinkContext->physTxDescriptor.numBytesToTx);

  pTxDescriptor->pChannel->pPhys->pPhysTransmit(
    pTxDescriptor->pChannel->pPhysContext, &pLinkContext->physTxDescriptor);
}

#endif /* MBCNFG_SUPPORT_RTU */
