/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                            MPP_COMMAND_PRODUCT                            */
/*                                                                           */
/* FILE NAME:    MBCNFG.h                                                    */
/* DESCRIPTION:  Master Modbus memory allocation routines                    */
/* PROPERTY OF:  Triangle MicroWorks, Inc. Raleigh, North Carolina USA       */
/*               www.TriangleMicroWorks.com  (919) 870-5101                  */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*****************************************************************************/
#ifndef MBCNFG_DEFINED
#define MBCNFG_DEFINED

#include "tmwscl/utils/tmwcnfg.h" 

/* Specify the number of link layer contexts to allocate. The TMW SCL needs
 * one per channel.
 */
#define MBCNFG_NUMALLOC_LINK_CONTEXTS        TMWCNFG_MAX_CHANNELS

/* Specify the number of transmit data structures to allocate. This is
 * dependent on how may application layer requests will be buffered per
 * session. 
 */
#define MBCNFG_NUMALLOC_CHNL_TX_DATAS        TMWCNFG_MAX_APPL_MSGS

/* If true automatic transaction id incrementing and checking will be 
 * supported for modbus TCP. 
 */
#define MBCNFG_SUPPORT_TRANSACTION_ID        TMWDEFS_TRUE
 
/* If true Modbus ASCII is included in the build
 */
#define MBCNFG_SUPPORT_ASCII                 TMWDEFS_TRUE
 
/* If true Modbus RTU is included in the build
 */
#define MBCNFG_SUPPORT_RTU                   TMWDEFS_TRUE
 
/* If true Modbus TCP is included in the build
 */
#define MBCNFG_SUPPORT_TCP                   TMWDEFS_TRUE
 
/* If true Modbus PLUS is included in the build
 */
#define MBCNFG_SUPPORT_MBP                   TMWDEFS_TRUE

/* If true SCL will generate a CRC when sending a message and validate CRC on a 
 * received message when using the modbus RTU protocol. 
 * If false target layer should should calculate and add the 2 byte CRC to end 
 * of a message being sent and validate the CRC before giving correctly received 
 * message to SCL (with 2 byte CRC removed) when using the modbus RTU protocol.
 * The SCL will always generate and check LRC for modbus ASCII no matter what  
 * this define is set to. (since LRC is done before converting to ASCII and  
 * checked after converting back to hex for received messages).
 */
#define MBCNFG_PERFORM_RTU_CRC               TMWDEFS_TRUE

#endif /* MBCNFG_DEFINED */

