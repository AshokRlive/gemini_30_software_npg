/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: mbsesn.c
 * description: Implementation of a Modbus session
 */
#include "tmwscl/modbus/mbdiag.h"
#include "tmwscl/utils/tmwtarg.h"
#include "tmwscl/utils/tmwdlist.h"

#include "tmwscl/modbus/mbdefs.h"
#include "tmwscl/modbus/mbsesn.h"
#include "tmwscl/modbus/mbsesn.h"

/* function: mbsesn_openSession */
TMWTYPES_BOOL TMWDEFS_GLOBAL mbsesn_openSession(
  TMWCHNL *pChannel,
  TMWSESN *pSession,
  TMWSESN_STAT_CALLBACK pCallback,
  void *pCallbackParam,
  TMWTYPES_PROTOCOL protocol,
  TMWTYPES_SESSION_TYPE type)
{

  if(pChannel == TMWDEFS_NULL)
      return(TMWDEFS_FALSE);
  
  /* Open TMW Session */
  tmwsesn_openSession(pChannel, pSession, pCallback, pCallbackParam, protocol, type);

  pSession->pChannel->pLink->pLinkOpenSession(
    pSession->pChannel->pLinkContext, (TMWSESN *)pSession, TMWDEFS_NULL);

  return(TMWDEFS_TRUE);
}

/* function: mbsesn_closeSession */
void TMWDEFS_GLOBAL mbsesn_closeSession(
  TMWSESN *pSession)
{
  pSession->pChannel->pLink->pLinkCloseSession(
    pSession->pChannel->pLinkContext, (TMWSESN *)pSession);
}

