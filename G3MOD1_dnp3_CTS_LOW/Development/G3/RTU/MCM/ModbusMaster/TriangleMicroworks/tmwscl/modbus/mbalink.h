/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: mbalink.h
 * description: Modbus ASCII Link Layer.
 */
#ifndef MBALINK_DEFINED
#define MBALINK_DEFINED

#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/utils/tmwchnl.h"

typedef struct MBALinkConfig
{
  /* Delimiters for Modbus Ascii */
  TMWTYPES_UCHAR delim1;
  TMWTYPES_UCHAR delim2;
} MBALINK_CONFIG;

#ifdef __cplusplus
extern "C" {
#endif

  /* function: mbalink_initConfig
   * purpose: Initialize a modbus ascii link configuration data structure
   * arguments:
   *  pConfig - pointer to modbus link configuration structure to be
   *   initialized.
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL mbalink_initConfig(
    MBALINK_CONFIG *pConfig);

  /* function: mbalink_getNeededBytes
   * purpose: return the number of bytes required to finish the
   *  current block
   * arguments:
   *  pCallbackParam - callback data, contains link layer context
   * returns
   *  number of characters to read
   */
  TMWTYPES_USHORT TMWDEFS_CALLBACK mbalink_getNeededBytes(
    void *pCallbackParam);

  /* function: mbalink_parseBytes
   * purpose: parse incoming data
   * arguments:
   *  pCallbackParam - callback data, contains link layer context
   *  recvBuf - received characters
   *  numBytes - number of bytes 
   *  firstByteTime - time that first byte was received. Could be zero
   *   if target layer does not fill this in. In that case it will be calculated
   *   in this function.
   * returns
   *  void
   */
  void TMWDEFS_CALLBACK mbalink_parseBytes(
    void *pCallbackParam,
    TMWTYPES_UCHAR *recvBuf,
    TMWTYPES_USHORT numBytes,
    TMWTYPES_MILLISECONDS firstByteTime);

  /* function: mbalink_transmitFrame   
   * purpose: Transmit a frame on a modbus ASCII link
   * arguments:
   *  pContext - pointer to a link context structure
   *  pTxDescriptor - pointer to a tx data structure
   * returns:
   *  void
   */ 
  void TMWDEFS_CALLBACK mbalink_transmitFrame(
    void *pContext,
    TMWSESN_TX_DATA *pTxDescriptor);

#ifdef __cplusplus
}
#endif
#endif /* MBALINK_DEFINED */
