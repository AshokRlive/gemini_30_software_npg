/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x!
 ***
 *** Name: G3DInput
 *** Description: Get the value of a DIGITAL G3 input
 *** Version: 
 ***     1.0: 2017-07-11/wang_p - Lucy Electric - 
 *************************************************************************/

#include "G3DInput.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "G3DInput_gen.cpp"
#endif

DEFINE_FIRMWARE_FB(FORTE_G3DInput, g_nStringIdG3DInput)

const CStringDictionary::TStringId FORTE_G3DInput::scm_anDataInputNames[] = {g_nStringIdQI, g_nStringIdNAME, g_nStringIdTYPE, g_nStringIdID};

const CStringDictionary::TStringId FORTE_G3DInput::scm_anDataInputTypeIds[] = {g_nStringIdBOOL, g_nStringIdWSTRING, g_nStringIdSTRING, g_nStringIdUINT};

const CStringDictionary::TStringId FORTE_G3DInput::scm_anDataOutputNames[] = {g_nStringIdQO, g_nStringIdSTATUS, g_nStringIdVALUE, g_nStringIdQUALITY};

const CStringDictionary::TStringId FORTE_G3DInput::scm_anDataOutputTypeIds[] = {g_nStringIdBOOL, g_nStringIdWSTRING, g_nStringIdDINT, g_nStringIdBOOL};

const TForteInt16 FORTE_G3DInput::scm_anEIWithIndexes[] = {0, 4};
const TDataIOID FORTE_G3DInput::scm_anEIWith[] = {0, 1, 2, 255, 0, 3, 255};
const CStringDictionary::TStringId FORTE_G3DInput::scm_anEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};

const TDataIOID FORTE_G3DInput::scm_anEOWith[] = {0, 1, 255, 0, 1, 2, 3, 255, 0, 1, 2, 3, 255};
const TForteInt16 FORTE_G3DInput::scm_anEOWithIndexes[] = {0, 3, 8, -1};
const CStringDictionary::TStringId FORTE_G3DInput::scm_anEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF, g_nStringIdIND};

const SFBInterfaceSpec FORTE_G3DInput::scm_stFBInterfaceSpec = {
  2,  scm_anEventInputNames,  scm_anEIWith,  scm_anEIWithIndexes,
  3,  scm_anEventOutputNames,  scm_anEOWith, scm_anEOWithIndexes,  4,  scm_anDataInputNames, scm_anDataInputTypeIds,
  4,  scm_anDataOutputNames, scm_anDataOutputTypeIds,
  0, 0
};


void FORTE_G3DInput::setInitialValues(){
  TYPE() = "Binary";
  VALUE() = 0;
  QUALITY() = false;
}


void FORTE_G3DInput::executeEvent(int pa_nEIID){
    QO() = QI();
    switch (pa_nEIID)
    {
        case scm_nEventINITID:
            if (true == QI())
            {
                QO() = CG3DBProcessInterface::initialise(true);
            }
            else
            {
                QO() = CG3DBProcessInterface::deinitialise();
            }

            sendOutputEvent(scm_nEventINITOID);
        break;
        case scm_nEventREQID:
            if (true == QI())
            {
                QO() = CG3DBProcessInterface::getInput(TYPE_DIGITAL, ID(), STATUS(), VALUE(), QUALITY());
            }
            sendOutputEvent(scm_nEventCNFID);
        break;
    }
}

lu_bool_t FORTE_G3DInput::handle(g3db::DBEvent& event)
{
    return CG3DBProcessInterface::handleInputEvent(event, TYPE_DIGITAL, ID(), STATUS(), VALUE(), QUALITY(),
                    QO(), scm_nEventINDID);
}

