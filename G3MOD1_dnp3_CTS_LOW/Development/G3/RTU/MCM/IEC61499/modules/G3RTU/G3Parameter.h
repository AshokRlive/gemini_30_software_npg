/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x!
 ***
 *** Name: G3Parameter
 *** Description: Get the value of a parameter
 *** Version: 
 ***     1.0: 2017-07-05/wang_p - Lucy Electric - 
 *************************************************************************/

#ifndef _G3PARAMETER_H_
#define _G3PARAMETER_H_

#include <funcbloc.h>
#include <forte_bool.h>
#include <forte_string.h>
#include <forte_dint.h>
#include <forte_uint.h>
#include <forte_wstring.h>
#include "g3dbprocint.h"
class FORTE_G3Parameter: public CG3DBProcessInterface{
  DECLARE_FIRMWARE_FB(FORTE_G3Parameter)

private:
  static const CStringDictionary::TStringId scm_anDataInputNames[];
  static const CStringDictionary::TStringId scm_anDataInputTypeIds[];
  CIEC_BOOL &QI() {
    return *static_cast<CIEC_BOOL*>(getDI(0));
  };

  CIEC_WSTRING &NAME() {
    return *static_cast<CIEC_WSTRING*>(getDI(1));
  };

  CIEC_STRING &TYPE() {
    return *static_cast<CIEC_STRING*>(getDI(2));
  };

  CIEC_UINT &ID() {
    return *static_cast<CIEC_UINT*>(getDI(3));
  };

  static const CStringDictionary::TStringId scm_anDataOutputNames[];
  static const CStringDictionary::TStringId scm_anDataOutputTypeIds[];
  CIEC_BOOL &QO() {
    return *static_cast<CIEC_BOOL*>(getDO(0));
  };

  CIEC_WSTRING &STATUS() {
    return *static_cast<CIEC_WSTRING*>(getDO(1));
  };

  CIEC_DINT &VALUE() {
    return *static_cast<CIEC_DINT*>(getDO(2));
  };

  static const TEventID scm_nEventINITID = 0;
  static const TEventID scm_nEventREQID = 1;
  static const TForteInt16 scm_anEIWithIndexes[];
  static const TDataIOID scm_anEIWith[];
  static const CStringDictionary::TStringId scm_anEventInputNames[];

  static const TEventID scm_nEventINITOID = 0;
  static const TEventID scm_nEventCNFID = 1;
  static const TForteInt16 scm_anEOWithIndexes[];
  static const TDataIOID scm_anEOWith[];
  static const CStringDictionary::TStringId scm_anEventOutputNames[];

  static const SFBInterfaceSpec scm_stFBInterfaceSpec;

   FORTE_FB_DATA_ARRAY(2, 4, 3, 0);

virtual void setInitialValues();

  void executeEvent(int pa_nEIID);

public:
  FUNCTION_BLOCK_CTOR_WITH_BASE_CLASS(FORTE_G3Parameter, CG3DBProcessInterface){
  };

  virtual ~FORTE_G3Parameter(){};

};

#endif //close the ifdef sequence from the beginning of the file

