/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x!
 ***
 *** Name: G3Control
 *** Description: Function block for sending a control command(e.g. switch operation).
 *** Version: 
 ***     0.0: 2017-07-11/4DIAC-IDE - 4DIAC-Consortium - 
 *************************************************************************/

#include "G3Control.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "G3Control_gen.cpp"
#endif

DEFINE_FIRMWARE_FB(FORTE_G3Control, g_nStringIdG3Control)

const CStringDictionary::TStringId FORTE_G3Control::scm_anDataInputNames[] = {g_nStringIdQI, g_nStringIdNAME, g_nStringIdID, g_nStringIdOPERATE};

const CStringDictionary::TStringId FORTE_G3Control::scm_anDataInputTypeIds[] = {g_nStringIdBOOL, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdBOOL};

const CStringDictionary::TStringId FORTE_G3Control::scm_anDataOutputNames[] = {g_nStringIdQO, g_nStringIdSTATUS};

const CStringDictionary::TStringId FORTE_G3Control::scm_anDataOutputTypeIds[] = {g_nStringIdBOOL, g_nStringIdWSTRING};

const TForteInt16 FORTE_G3Control::scm_anEIWithIndexes[] = {0, 3};
const TDataIOID FORTE_G3Control::scm_anEIWith[] = {0, 1, 255, 0, 2, 3, 255};
const CStringDictionary::TStringId FORTE_G3Control::scm_anEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};

const TDataIOID FORTE_G3Control::scm_anEOWith[] = {0, 1, 255, 0, 1, 255};
const TForteInt16 FORTE_G3Control::scm_anEOWithIndexes[] = {0, 3, -1};
const CStringDictionary::TStringId FORTE_G3Control::scm_anEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};

const SFBInterfaceSpec FORTE_G3Control::scm_stFBInterfaceSpec = {
  2,  scm_anEventInputNames,  scm_anEIWith,  scm_anEIWithIndexes,
  2,  scm_anEventOutputNames,  scm_anEOWith, scm_anEOWithIndexes,  4,  scm_anDataInputNames, scm_anDataInputTypeIds,
  2,  scm_anDataOutputNames, scm_anDataOutputTypeIds,
  0, 0
};


void FORTE_G3Control::executeEvent(int pa_nEIID){
    QO() = QI();
    switch(pa_nEIID){
      case scm_nEventINITID:
        if(true == QI()){
          QO() = CG3DBProcessInterface::initialise();
        }
        else{
          QO() = CG3DBProcessInterface::deinitialise();
        }
        sendOutputEvent(scm_nEventINITOID);
        break;

      case scm_nEventREQID:
        if(true == QI()){
          QO() = CG3DBProcessInterface::operateControl(
                          ID(),
                          OPERATE(),
                          STATUS());
        }
        sendOutputEvent(scm_nEventCNFID);
        break;
    }

}



