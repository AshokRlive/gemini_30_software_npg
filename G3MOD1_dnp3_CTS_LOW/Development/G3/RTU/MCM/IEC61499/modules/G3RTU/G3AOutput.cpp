/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x!
 ***
 *** Name: G3AOutput
 *** Description: Function block for updating the value of a ANALOGUE G3 output
 *** Version: 
 ***     1.0: 2017-07-13/wang_p - Lucy Electric - 
 *************************************************************************/

#include "G3AOutput.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "G3AOutput_gen.cpp"
#endif

DEFINE_FIRMWARE_FB(FORTE_G3AOutput, g_nStringIdG3AOutput)

const CStringDictionary::TStringId FORTE_G3AOutput::scm_anDataInputNames[] = {g_nStringIdQI, g_nStringIdNAME, g_nStringIdID, g_nStringIdVALUE};

const CStringDictionary::TStringId FORTE_G3AOutput::scm_anDataInputTypeIds[] = {g_nStringIdBOOL, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdREAL};

const CStringDictionary::TStringId FORTE_G3AOutput::scm_anDataOutputNames[] = {g_nStringIdQO, g_nStringIdSTATUS};

const CStringDictionary::TStringId FORTE_G3AOutput::scm_anDataOutputTypeIds[] = {g_nStringIdBOOL, g_nStringIdWSTRING};

const TForteInt16 FORTE_G3AOutput::scm_anEIWithIndexes[] = {0, 3};
const TDataIOID FORTE_G3AOutput::scm_anEIWith[] = {0, 1, 255, 0, 2, 3, 255};
const CStringDictionary::TStringId FORTE_G3AOutput::scm_anEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};

const TDataIOID FORTE_G3AOutput::scm_anEOWith[] = {0, 1, 255, 0, 1, 255};
const TForteInt16 FORTE_G3AOutput::scm_anEOWithIndexes[] = {0, 3, -1};
const CStringDictionary::TStringId FORTE_G3AOutput::scm_anEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};

const SFBInterfaceSpec FORTE_G3AOutput::scm_stFBInterfaceSpec = {
  2,  scm_anEventInputNames,  scm_anEIWith,  scm_anEIWithIndexes,
  2,  scm_anEventOutputNames,  scm_anEOWith, scm_anEOWithIndexes,  4,  scm_anDataInputNames, scm_anDataInputTypeIds,
  2,  scm_anDataOutputNames, scm_anDataOutputTypeIds,
  0, 0
};


void FORTE_G3AOutput::executeEvent(int pa_nEIID){
    QO() = QI();
    switch(pa_nEIID){
      case scm_nEventINITID:
        if(true == QI()){
          QO() = CG3DBProcessInterface::initialise();
        }
        else{
          QO() = CG3DBProcessInterface::deinitialise();
        }
        sendOutputEvent(scm_nEventINITOID);
        break;

      case scm_nEventREQID:
        if(true == QI()){
          QO() = CG3DBProcessInterface::setOutput(TYPE_ANALOGUE, ID(), VALUE(), LU_TRUE, STATUS());
        }
        sendOutputEvent(scm_nEventCNFID);
        break;
    }

}



