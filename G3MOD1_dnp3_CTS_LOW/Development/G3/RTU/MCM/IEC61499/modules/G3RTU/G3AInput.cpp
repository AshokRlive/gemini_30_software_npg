/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x!
 ***
 *** Name: G3AInput
 *** Description: Get the value of an ANALOGUE G3 input
 *** Version: 
 ***     1.0: 2017-07-11/wang_p - Lucy Electric - 
 *************************************************************************/

#include "G3AInput.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "G3AInput_gen.cpp"
#endif

DEFINE_FIRMWARE_FB(FORTE_G3AInput, g_nStringIdG3AInput)

const CStringDictionary::TStringId FORTE_G3AInput::scm_anDataInputNames[] = {g_nStringIdQI, g_nStringIdNAME, g_nStringIdID};

const CStringDictionary::TStringId FORTE_G3AInput::scm_anDataInputTypeIds[] = {g_nStringIdBOOL, g_nStringIdWSTRING, g_nStringIdUINT};

const CStringDictionary::TStringId FORTE_G3AInput::scm_anDataOutputNames[] = {g_nStringIdQO, g_nStringIdSTATUS, g_nStringIdVALUE, g_nStringIdQUALITY};

const CStringDictionary::TStringId FORTE_G3AInput::scm_anDataOutputTypeIds[] = {g_nStringIdBOOL, g_nStringIdWSTRING, g_nStringIdREAL, g_nStringIdBOOL};

const TForteInt16 FORTE_G3AInput::scm_anEIWithIndexes[] = {0, 3};
const TDataIOID FORTE_G3AInput::scm_anEIWith[] = {0, 1, 255, 0, 2, 255};
const CStringDictionary::TStringId FORTE_G3AInput::scm_anEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};

const TDataIOID FORTE_G3AInput::scm_anEOWith[] = {0, 1, 255, 0, 1, 2, 3, 255, 0, 1, 2, 3, 255};
const TForteInt16 FORTE_G3AInput::scm_anEOWithIndexes[] = {0, 3, 8, -1};
const CStringDictionary::TStringId FORTE_G3AInput::scm_anEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF, g_nStringIdIND};

const SFBInterfaceSpec FORTE_G3AInput::scm_stFBInterfaceSpec = {
  2,  scm_anEventInputNames,  scm_anEIWith,  scm_anEIWithIndexes,
  3,  scm_anEventOutputNames,  scm_anEOWith, scm_anEOWithIndexes,  3,  scm_anDataInputNames, scm_anDataInputTypeIds,
  4,  scm_anDataOutputNames, scm_anDataOutputTypeIds,
  0, 0
};


void FORTE_G3AInput::setInitialValues(){
  VALUE() = 0;
  QUALITY() = false;
}


void FORTE_G3AInput::executeEvent(int pa_nEIID){
    QO() = QI();
    switch (pa_nEIID)
    {
        case scm_nEventINITID:
            if (true == QI())
            {
                QO() = CG3DBProcessInterface::initialise(true);
            }
            else
            {
                QO() = CG3DBProcessInterface::deinitialise();
            }

            sendOutputEvent(scm_nEventINITOID);
        break;

        case scm_nEventREQID:
            if (true == QI())
            {
                QO() = CG3DBProcessInterface::getInput(TYPE_ANALOGUE, ID(), STATUS(), VALUE(), QUALITY());
            }
            sendOutputEvent(scm_nEventCNFID);
        break;
    }
}


lu_bool_t FORTE_G3AInput::handle(g3db::DBEvent& event)
{
    return CG3DBProcessInterface::handleInputEvent(event, TYPE_ANALOGUE, ID(), STATUS(), VALUE(), QUALITY(),
                    QO(), scm_nEventINDID);
}
