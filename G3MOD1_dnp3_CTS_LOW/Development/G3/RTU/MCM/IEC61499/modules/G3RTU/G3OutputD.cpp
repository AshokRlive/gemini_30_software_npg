/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x!
 ***
 *** Name: G3OutputD
 *** Description: Function block for updating the value of a DIGITAL G3 output
 *** Version: 
 ***     1.0: 2017-07-13/wang_p - Lucy Electric - 
 *************************************************************************/

#include "G3OutputD.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "G3OutputD_gen.cpp"
#endif

DEFINE_FIRMWARE_FB(FORTE_G3OutputD, g_nStringIdG3OutputD)

const CStringDictionary::TStringId FORTE_G3OutputD::scm_anDataInputNames[] = {g_nStringIdQI, g_nStringIdNAME, g_nStringIdTYPE, g_nStringIdID, g_nStringIdVALUE, g_nStringIdDEFAULT_INIT, g_nStringIdDEFAULT_VALUE};

const CStringDictionary::TStringId FORTE_G3OutputD::scm_anDataInputTypeIds[] = {g_nStringIdBOOL, g_nStringIdWSTRING, g_nStringIdSTRING, g_nStringIdUINT, g_nStringIdDINT, g_nStringIdBOOL, g_nStringIdDINT};

const CStringDictionary::TStringId FORTE_G3OutputD::scm_anDataOutputNames[] = {g_nStringIdQO, g_nStringIdSTATUS};

const CStringDictionary::TStringId FORTE_G3OutputD::scm_anDataOutputTypeIds[] = {g_nStringIdBOOL, g_nStringIdWSTRING};

const TForteInt16 FORTE_G3OutputD::scm_anEIWithIndexes[] = {0, 6};
const TDataIOID FORTE_G3OutputD::scm_anEIWith[] = {0, 1, 2, 5, 6, 255, 0, 3, 4, 255};
const CStringDictionary::TStringId FORTE_G3OutputD::scm_anEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};

const TDataIOID FORTE_G3OutputD::scm_anEOWith[] = {0, 1, 255, 0, 1, 255};
const TForteInt16 FORTE_G3OutputD::scm_anEOWithIndexes[] = {0, 3, -1};
const CStringDictionary::TStringId FORTE_G3OutputD::scm_anEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};

const SFBInterfaceSpec FORTE_G3OutputD::scm_stFBInterfaceSpec = {
  2,  scm_anEventInputNames,  scm_anEIWith,  scm_anEIWithIndexes,
  2,  scm_anEventOutputNames,  scm_anEOWith, scm_anEOWithIndexes,  7,  scm_anDataInputNames, scm_anDataInputTypeIds,
  2,  scm_anDataOutputNames, scm_anDataOutputTypeIds,
  0, 0
};


void FORTE_G3OutputD::setInitialValues(){
  TYPE() = "Binary";
}


void FORTE_G3OutputD::executeEvent(int pa_nEIID){
  QO() = QI();
    switch(pa_nEIID){
      case scm_nEventINITID:
        if(true == QI()){
          QO() = CG3DBProcessInterface::initialise();

          if(DEFAULT_INIT())
              QO() = CG3DBProcessInterface::setOutput(TYPE_DIGITAL, ID(), DEFAULT_VALUE(), LU_TRUE, STATUS());
        }
        else{
          QO() = CG3DBProcessInterface::deinitialise();
        }
        sendOutputEvent(scm_nEventINITOID);
        break;

      case scm_nEventREQID:
        if(true == QI()){
          QO() = CG3DBProcessInterface::setOutput(TYPE_DIGITAL, ID(), VALUE(), LU_TRUE, STATUS());
        }
        sendOutputEvent(scm_nEventCNFID);
        break;
    }
}



