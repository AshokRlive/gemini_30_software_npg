/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x!
 ***
 *** Name: G3ReadPoint
 *** Description: Read the value&status of a G3 virtual point.
 *** Version: 
 ***     0.0: 2017-07-11/wang_p - Lucy Electric - 
 *************************************************************************/

#include "G3ReadPoint.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "G3ReadPoint_gen.cpp"
#endif

DEFINE_FIRMWARE_FB(FORTE_G3ReadPoint, g_nStringIdG3ReadPoint)

const CStringDictionary::TStringId FORTE_G3ReadPoint::scm_anDataInputNames[] = {g_nStringIdQI, g_nStringIdID, g_nStringIdGROUP};

const CStringDictionary::TStringId FORTE_G3ReadPoint::scm_anDataInputTypeIds[] = {g_nStringIdBOOL, g_nStringIdUINT, g_nStringIdUINT};

const CStringDictionary::TStringId FORTE_G3ReadPoint::scm_anDataOutputNames[] = {g_nStringIdQO, g_nStringIdSTATUS, g_nStringIdVALUE, g_nStringIdQUALITY, g_nStringIdTYPE};

const CStringDictionary::TStringId FORTE_G3ReadPoint::scm_anDataOutputTypeIds[] = {g_nStringIdBOOL, g_nStringIdWSTRING, g_nStringIdANY_NUM, g_nStringIdBOOL, g_nStringIdWSTRING};

const TForteInt16 FORTE_G3ReadPoint::scm_anEIWithIndexes[] = {0, 3};
const TDataIOID FORTE_G3ReadPoint::scm_anEIWith[] = {0, 1, 255, 0, 255};
const CStringDictionary::TStringId FORTE_G3ReadPoint::scm_anEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};

const TDataIOID FORTE_G3ReadPoint::scm_anEOWith[] = {0, 1, 255, 0, 1, 2, 3, 4, 255};
const TForteInt16 FORTE_G3ReadPoint::scm_anEOWithIndexes[] = {0, 3, -1};
const CStringDictionary::TStringId FORTE_G3ReadPoint::scm_anEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};

const SFBInterfaceSpec FORTE_G3ReadPoint::scm_stFBInterfaceSpec = {
  2,  scm_anEventInputNames,  scm_anEIWith,  scm_anEIWithIndexes,
  2,  scm_anEventOutputNames,  scm_anEOWith, scm_anEOWithIndexes,  3,  scm_anDataInputNames, scm_anDataInputTypeIds,
  5,  scm_anDataOutputNames, scm_anDataOutputTypeIds,
  0, 0
};


void FORTE_G3ReadPoint::executeEvent(int pa_nEIID){
  switch(pa_nEIID){
    case scm_nEventINITID:
        if (true == QI())
        {
            QO() = CG3DBProcessInterface::initialise();
        }
        else
        {
            QO() = CG3DBProcessInterface::deinitialise();
        }

      sendOutputEvent(scm_nEventINITOID);
      break;

    case scm_nEventREQID:
       if(true == QI()){
        QO() = CG3DBProcessInterface::readPoint(GROUP(),ID(),STATUS(),VALUE(),QUALITY(),TYPE());
      }
      sendOutputEvent(scm_nEventCNFID);
      break;
  }
}



