#include <string>
#include <time.h>
#include "devexec.h"
#include "g3dbprocint.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define COPY_DB_NAME(query) \
    strncpy(query.databaseId, getResource().getInstanceName(), g3db::DB_ID_LENGHT)

#define LOG_SEND(msg) DEVLOG_INFO("G3DBClient ===> %s\n", msg)
#define LOG_RCV(msg)  DEVLOG_INFO("G3DBClient <=== %s\n", msg)
#define LOG_ERR(msg)  DEVLOG_ERROR("G3DBClient %s\n",    msg)

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static void gettime(timespec& time);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
const char * const CG3DBProcessInterface::scmOK = "OK";
const char * const CG3DBProcessInterface::scmNotInitialised = "Not initialised";
const char * const CG3DBProcessInterface::scmCommsError = "Comms Error";
const char * const CG3DBProcessInterface::scmCouldNotRead = "Could not read value";
const char * const CG3DBProcessInterface::scmCouldNotWrite = "Could not write value";
const char * const CG3DBProcessInterface::typeDigital = "DIGITAL";
const char * const CG3DBProcessInterface::typeAnalogue = "ANALOGUE";

g3db::G3DBClient CG3DBProcessInterface::g3dbclient;

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */


bool CG3DBProcessInterface::initialise(bool subscribeEvent)
{
    if(subscribeEvent)
    {
        subscribe();
        m_subscribed = subscribeEvent;
    }

    g3dbclient.connect();
    DEVLOG_INFO("%s initialised.\n", getInstanceName());
    return true;
}

bool CG3DBProcessInterface::deinitialise()
{
    g3dbclient.disconnect();

    if(m_subscribed)
    {
        unsubscribe();
        m_subscribed = false;
    }

    DEVLOG_INFO("%s de-initialised.\n", getInstanceName());
    return true;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

bool CG3DBProcessInterface::readPoint(
                const lu_uint16_t group,
                const lu_uint16_t id,
                CIEC_WSTRING& status,
                CIEC_ANY_NUM& value,
                CIEC_BOOL& quality,
                CIEC_WSTRING& type)
{
    g3db::DBQuery query;
    query.reqId = g3db::DB_REQUEST_ID_READ_POINT;
    query.ref.group = group;
    query.ref.id = id;
    LOG_SEND("READ POINT");

    bool qualifer = false;
    if (g3dbclient.sendRequest(query))
    {
        LOG_RCV("Read POINT");
        if (query.value.type == g3db::VALUE_TYPE_DIGITAL)
        {
            CIEC_DINT v(query.value.digital);
            value.saveAssign(v);
            type = typeDigital;
            status = scmOK;
            qualifer = true;
        }
        else if (query.value.type == g3db::VALUE_TYPE_ANALOGUE)
        {
            CIEC_REAL v(query.value.analogue);
            value.saveAssign(v);
            quality = query.quality.online;
            type = typeAnalogue;
            status = scmOK;
            qualifer = true;
        }
        else
        {
            status = scmCouldNotRead;
        }
    }
    else
    {
        status = scmCommsError;
    }

    if (qualifer)
       LOG_RCV("READ POINT SUCCESS");
    else
       LOG_ERR("READ POINT FAILED");
    return qualifer;
}

bool CG3DBProcessInterface::writePoint()
{
    return false; // NOT IMPLEMENTED
}

bool CG3DBProcessInterface::getInput(
                const TYPE type,
                const lu_uint16_t id,
                CIEC_WSTRING& status,
                CIEC_ANY_NUM& value,
                CIEC_BOOL& quality
                )
{
    g3db::DBQuery query;
    query.reqId = g3db::DB_REQUEST_ID_GET_INPUT;
    query.ref.id = id;
    query.value.type = (g3db::VALUE_TYPE)type;
    COPY_DB_NAME(query);
    LOG_SEND("READ INPUT");

    bool qualifer = false;

    if (g3dbclient.sendRequest(query))
    {
        LOG_RCV("Read INPUT");

        if (query.reqStatus == g3db::DB_REQUEST_STATUS_OK
            && query.value.type == (g3db::VALUE_TYPE)type)
        {
            if(type == TYPE_ANALOGUE )
                *((CIEC_REAL*)(&value)) = query.value.analogue;
            else
                *((CIEC_DINT*)(&value)) = query.value.digital;
            quality = query.quality.online;
            status = scmOK;
            qualifer = true;
        }
        else
        {
            status = scmCouldNotRead;
        }
    }
    else
    {
        status = scmCommsError;
    }

    if (qualifer)
        LOG_RCV("READ INPUT SUCCESS");
    else
        LOG_ERR("READ INPUT FAILED");
    return qualifer;
}


bool CG3DBProcessInterface::setOutput(
                const TYPE type,
                const lu_uint16_t id,
                const CIEC_ANY_NUM& value,
                const lu_bool_t online,
                CIEC_WSTRING& status)
{
    g3db::DBQuery query;
    COPY_DB_NAME(query);
    query.reqId = g3db::DB_REQUEST_ID_SET_OUTPUT;
    query.ref.id = id;
    query.quality.online = online == LU_TRUE ? 1 : 0;
    query.value.type =(g3db::VALUE_TYPE) type;
    gettime(query.timestamp.value);
    LOG_SEND("WRITE OUTPUT...");

    if(type == TYPE_DIGITAL)
        query.value.digital = *((CIEC_DINT*)(&value));
    else
        query.value.analogue = *((CIEC_REAL*)(&value));

    bool qualifer = false;
    if (g3dbclient.sendRequest(query))
    {
        if (query.reqStatus == g3db::DB_REQUEST_STATUS_OK)
        {
            status = scmOK;
            qualifer = true;
        }
        else
        {
            status = scmCouldNotWrite;
        }
    }
    else
    {
        status = scmCommsError;
    }

    if(qualifer)
        LOG_RCV("WRITE OUTPUT SUCCESS");
    else
        LOG_ERR("WRITE OUTPUT FAILED");
    return qualifer;
}

bool CG3DBProcessInterface::getParameter(const lu_uint16_t id,
                        CIEC_WSTRING& status,
                        CIEC_DINT& value)
{
    g3db::DBQuery query;
    query.reqId = g3db::DB_REQUEST_ID_GET_CONSTANT;
    query.ref.id = id;
    COPY_DB_NAME(query);
    LOG_SEND("READ PARAMETER...");

    bool qualifer = false;
    if (g3dbclient.sendRequest(query))
    {
        if (query.reqStatus == g3db::DB_REQUEST_STATUS_OK
            && query.value.type == g3db::VALUE_TYPE_DIGITAL)
        {
            value = query.value.digital;
            status = scmOK;
            qualifer = true;
        }
        else
        {
            status = scmCouldNotRead;
        }
    }
    else
    {
        status = scmCommsError;
    }

    if(qualifer)
        LOG_RCV("READ PARAMETER SUCCESS");
    else
        LOG_ERR("READ PARAMETER FAILED");
    return qualifer;

}

bool CG3DBProcessInterface::operateControl(const lu_uint16_t id,
                CIEC_BOOL& operate,
                CIEC_WSTRING& status)
{
    g3db::DBQuery query;
    COPY_DB_NAME(query);
    query.reqId = g3db::DB_REQUEST_ID_CONTROL;
    query.ref.id = id;
    query.value.digital = operate;
    gettime(query.timestamp.value);
    LOG_SEND("OPERATE");

    bool qualifer = false;
    if (g3dbclient.sendRequest(query))
    {
        if (query.reqStatus == g3db::DB_REQUEST_STATUS_OK)
        {
            status = scmOK;
            qualifer = true;
        }
        else
        {
            status = scmCouldNotWrite;
        }
    }
    else
    {
        status = scmCommsError;
    }

    if (qualifer)
        LOG_RCV("OPERATE SUCCESS");
    else
        LOG_ERR("OPERATE FAILED");
    return qualifer;

}
lu_bool_t CG3DBProcessInterface::handle(g3db::DBEvent& event)
{
    LU_UNUSED(event);
    return LU_FALSE;
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

void CG3DBProcessInterface::subscribe()
{
    g3dbclient.registerHandler(this);
}

void CG3DBProcessInterface::unsubscribe()
{
    g3dbclient.deregisterHandler(this);
}

lu_bool_t CG3DBProcessInterface::handleInputEvent(g3db::DBEvent& dbevent,
                const TYPE type, const lu_uint16_t id,
                CIEC_WSTRING& status, CIEC_ANY_NUM& value,
                CIEC_BOOL& quality,
                CIEC_BOOL& qualifier,
                const TEventID indication)
{
    if (dbevent.eventId != g3db::DB_EVENT_ID_INPUT_UPDATE)
        return LU_FALSE;

    if (strcmp(dbevent.databaseId, getResource().getInstanceName()) != 0)
        return LU_FALSE;

    g3db::InputEvent& evt = dbevent.inputEvent;
    if(evt.id != id || evt.value.type != (g3db::VALUE_TYPE)type)
        return LU_FALSE;


    if(type == TYPE_ANALOGUE)
        *((CIEC_REAL*)(&value)) = evt.value.analogue;
    else
        *((CIEC_DINT*)(&value)) = evt.value.digital;

    quality = evt.quality.online;
    status = scmOK;
    qualifier = true;
    sendOutputEvent(indication);
    startNewEventChain(this);

    return LU_TRUE;
}

static void gettime(timespec& time)
{
    clock_gettime(CLOCK_REALTIME, &time);
}


/*
 *********************** End of file ******************************************
 */
