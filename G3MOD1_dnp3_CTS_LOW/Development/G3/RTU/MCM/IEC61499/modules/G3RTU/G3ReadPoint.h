/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x!
 ***
 *** Name: G3ReadPoint
 *** Description: Read the value&status of a G3 virtual point.
 *** Version: 
 ***     0.0: 2017-07-11/wang_p - Lucy Electric - 
 *************************************************************************/

#ifndef _G3READPOINT_H_
#define _G3READPOINT_H_

#include <funcbloc.h>
#include <forte_any_num.h>


#include <forte_bool.h>
#include <forte_uint.h>
#include <forte_wstring.h>
#include "g3dbprocint.h"
class FORTE_G3ReadPoint: public CG3DBProcessInterface{
  DECLARE_FIRMWARE_FB(FORTE_G3ReadPoint)

private:
  static const CStringDictionary::TStringId scm_anDataInputNames[];
  static const CStringDictionary::TStringId scm_anDataInputTypeIds[];
  CIEC_BOOL &QI() {
    return *static_cast<CIEC_BOOL*>(getDI(0));
  };

  CIEC_UINT &ID() {
    return *static_cast<CIEC_UINT*>(getDI(1));
  };

  CIEC_UINT &GROUP() {
    return *static_cast<CIEC_UINT*>(getDI(2));
  };

  static const CStringDictionary::TStringId scm_anDataOutputNames[];
  static const CStringDictionary::TStringId scm_anDataOutputTypeIds[];
  CIEC_BOOL &QO() {
    return *static_cast<CIEC_BOOL*>(getDO(0));
  };

  CIEC_WSTRING &STATUS() {
    return *static_cast<CIEC_WSTRING*>(getDO(1));
  };

  CIEC_ANY_NUM &VALUE() {
    return *static_cast<CIEC_ANY_NUM*>(getDO(2));
  };

  CIEC_BOOL &QUALITY() {
    return *static_cast<CIEC_BOOL*>(getDO(3));
  };

  CIEC_WSTRING &TYPE() {
    return *static_cast<CIEC_WSTRING*>(getDO(4));
  };

  static const TEventID scm_nEventINITID = 0;
  static const TEventID scm_nEventREQID = 1;
  static const TForteInt16 scm_anEIWithIndexes[];
  static const TDataIOID scm_anEIWith[];
  static const CStringDictionary::TStringId scm_anEventInputNames[];

  static const TEventID scm_nEventINITOID = 0;
  static const TEventID scm_nEventCNFID = 1;
  static const TForteInt16 scm_anEOWithIndexes[];
  static const TDataIOID scm_anEOWith[];
  static const CStringDictionary::TStringId scm_anEventOutputNames[];

  static const SFBInterfaceSpec scm_stFBInterfaceSpec;

   FORTE_FB_DATA_ARRAY(2, 3, 5, 0);

  void executeEvent(int pa_nEIID);

public:
  FUNCTION_BLOCK_CTOR_WITH_BASE_CLASS(FORTE_G3ReadPoint, CG3DBProcessInterface){
  };

  virtual ~FORTE_G3ReadPoint(){};

};

#endif //close the ifdef sequence from the beginning of the file

