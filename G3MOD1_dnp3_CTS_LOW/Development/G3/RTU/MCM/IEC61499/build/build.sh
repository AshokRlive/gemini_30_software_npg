#!/bin/bash
APP="Debug/src/forte"
DEST="/exports/${USER}/gemini/application/bin/"

mkdir -p ${DEST}

if [ "$1" = "" ]; 
then
	 BUILD_TYPE="Debug"
else
	 BUILD_TYPE=$1
fi
echo "Build type:${BUILD_TYPE}"
 
cd ${BUILD_TYPE}
make
RESULT=$?
if [ $RESULT -eq 0 ]
then
    # successful make
    cd ..
    cp -v $APP $DEST
    echo copy done!
else
    exit $RESULT
fi
