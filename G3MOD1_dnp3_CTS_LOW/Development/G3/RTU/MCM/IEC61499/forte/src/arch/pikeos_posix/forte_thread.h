/*******************************************************************************
 * Copyright (c) 2006 - 2017, 2018 ACIN, fortiss GmbH, SYSGO AG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Alois Zoitl, Rene Smodic, Ingo Hegny, Martin Melik Merkiumians - initial API and implementation and/or initial documentation
 *  Alois Zoitl - extracted common functions to new base class CThreadBase
 *  Agostino Mascitti - Adaption to PikeOS 4.2
 *******************************************************************************/
#ifndef _PIKEOS_FORTE_THREAD_H_
#define _PIKEOS_FORTE_THREAD_H_

#include <p4types.h>
#include "../posix/forte_thread.h"

#endif /*_PIKEOS_FORTE_THREAD_H_*/

