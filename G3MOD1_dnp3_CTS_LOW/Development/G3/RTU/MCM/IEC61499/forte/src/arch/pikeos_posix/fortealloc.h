/*******************************************************************************
 * Copyright (c) 2010, 2018 ACIN, SYSGO AG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Alois Zoitl - initial API and implementation and/or initial documentation
 *  Agostino Mascitti - Adaption to PikeOS 4.2
 *******************************************************************************/
#ifndef PIKEOS_FORTEALLOC_H_
#define PIKEOS_FORTEALLOC_H_

//on posix environments we are typically happy with the generic alloc implementation based on malloc and free
#include "../genfortealloc.h"

#endif /* PIKEOS_FORTEALLOC_H_ */
