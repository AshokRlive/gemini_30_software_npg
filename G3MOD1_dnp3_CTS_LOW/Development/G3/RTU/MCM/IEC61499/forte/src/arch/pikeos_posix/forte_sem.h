/*******************************************************************************
 * Copyright (c) 2016, 2018 fortiss GmbH, SYSGO AG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Alois Zoitl - initial API and implementation and/or initial documentation
 *  Agostino Mascitti - Adaption to PikeOS 4.2
 *******************************************************************************/

#ifndef SRC_ARCH_PIKEOS_POSIX_SEMAPHORE_H_
#define SRC_ARCH_PIKEOS_POSIX_SEMAPHORE_H_

#include "../posix/forte_sem.h"

#endif /* SRC_ARCH_PIKEOS_POSIX_SEMAPHORE_H_ */
