/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:ArgParser.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   18 May 2017     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef RTU_MCM_GK2G3_SRC_GKSERVER_INCLUDE_ARGPARSER_H_
#define RTU_MCM_GK2G3_SRC_GKSERVER_INCLUDE_ARGPARSER_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define MAX_IPADDR_LEN 20

#define DEFAULT_GK_PORT      30502
#define DEFAULT_GK2G3_CONFIG "config/gk2g3.ini"

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
typedef struct
{
	lu_bool_t verbose;
	lu_bool_t debug_modbus;
	lu_bool_t debug_socket;
	lu_bool_t console;// Launch a console
	lu_char_t*  config_file;
}global_args_t;


typedef struct
{
    lu_bool_t   dc_active;
    lu_char_t   dc_ipaddr[MAX_IPADDR_LEN];
    lu_int32_t  dc_port;
    lu_uint32_t gkdev_number;
    lu_uint32_t gkdev_port;
} global_conf_t;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
extern global_args_t    g_args;
extern global_conf_t  g_conf;


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*
 * Parses arguments and stores into global_args_t structure.
 */
lu_int32_t parse_args(lu_int32_t argc, lu_char_t **argv, global_args_t *arg);

lu_int32_t parse_config(const lu_char_t* configFile, global_conf_t* conf);

#endif /* RTU_MCM_GK2G3_SRC_GKSERVER_INCLUDE_ARGPARSER_H_ */

/*
 *********************** End of file ******************************************
 */
