/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:Logger.c
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   2 May 2017     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdio.h>
#include <stdarg.h>
#include <memory.h>
#include <syslog.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "Logger.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */





/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
void logger_open(void)
{
    setlogmask (LOG_UPTO (LOG_INFO));
    openlog ("gk2g3", LOG_PID, LOG_LOCAL3);
}

void logger_close(void)
{
    closelog ();
}

void logerr(const lu_char_t* msg,  ...)
{

    va_list args;
    va_start(args, msg);

#if NDEBUG
    vsyslog (LOG_ERR, msg, args);
#else
    lu_char_t buffer[256];
    memset(buffer, 0, 256);
    vsprintf(buffer, msg, args);
    printf("[%-5s] %s\n", "ERR", buffer);
#endif

    va_end(args);
}

/**
 * Logs a info message. This function needs to be implemented by the invoker of the library.
 */
void loginfo(const lu_char_t* msg, ...)
{
    va_list args;
    va_start(args, msg);

#if NDEBUG
    vsyslog (LOG_NOTICE, msg, args);
#else
    lu_char_t buffer[256];
    memset(buffer, 0, 256);
    vsprintf(buffer, msg, args);
    printf("[%-5s] %s\n", "INFO", buffer);
#endif

    va_end(args);
}



/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
