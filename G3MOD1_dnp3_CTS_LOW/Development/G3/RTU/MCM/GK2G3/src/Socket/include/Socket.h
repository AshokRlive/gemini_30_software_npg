/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:hat_socket.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   8 Oct 2015     pwang     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef SRC_SOCKET_H_
#define SRC_SOCKET_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#ifdef WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#else
#include <netdb.h>
#include <fcntl.h>
#include <sys/select.h>
#endif

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define LOOPBACK_ADDR        "127.0.0.1"

#define MAX_CONNECTION_NUM   10

#define SOCK_RW_BUF_SIZE     4096

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
/**
 * A callback function to be called when receiving a request from client.
 */
typedef lu_int32_t (*socket_request_handler)(
                                    const lu_int32_t deviceIndex,
                                    const lu_uint8_t* requestPtr,
                                    const lu_int32_t requestSize,
                                    lu_uint8_t *replyPtr,
                                    lu_int32_t* replySize);

/**
 * A callback function to be called when the number of existing connections changes.
 */
typedef void (*socket_connchg_handler)(void* ctxt, lu_int32_t existingConnNum);

typedef struct socket_server_context_str
{
    /* The socket file descriptor for our "listening" socket*/
    lu_int32_t listenSock;

    /* Array of connected sockets so we know who we are talking to*/
    lu_int32_t connectlist[MAX_CONNECTION_NUM];

    /* The number of existing connections.*/
    lu_int32_t connectNum;

    /* Socket file descriptors we want to wake up for, using select()*/
    fd_set socks;

    /* Highest file descriptor, needed for select()*/
    lu_int32_t highsock;

    /* For handling request from client*/
    socket_request_handler handler;

    /* For handling connection changes.*/
    socket_connchg_handler connHandler;

    /* Reference to user object passed from init()*/
    void*  userContext;

    /* An array for storing ports thant have been open*/
    lu_int32_t portlist[MAX_CONNECTION_NUM];

} socket_server_context_t;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */
#ifdef WIN32
void socket_initWinsock();
#endif


lu_int32_t socket_make_socket(lu_int32_t port);

lu_int32_t socket_close(lu_int32_t sock);

lu_int32_t socket_send_data(lu_int32_t socket, lu_uint8_t* sendBuffer, lu_int32_t sendSize);

lu_int32_t socket_recv_data(lu_int32_t socket, lu_uint8_t* recvBuffer, lu_int32_t bufSize);

/* Initialise a server and returns a pointer to server context.*/
void* socket_server_init(
                void* userContext,
                lu_int32_t port,
                socket_request_handler reqhandler,
                socket_connchg_handler connchghandler,
                lu_uint32_t devNum);

/**
 * Accept the socket and call request handler to process request.
 */
void socket_server_process(void* context);

/**
 * Shutdown a server.
 */
void socket_server_shudown(void* context);

#endif /* SRC_SOCKET_H_ */

/*
 *********************** End of file ******************************************
 */
