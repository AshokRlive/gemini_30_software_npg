/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:Socket.c
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   8 Oct 2015     pwang     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <errno.h>
#include <stddef.h>
#include <stdio.h>
#include <memory.h>
#include <stdlib.h>

#ifndef WIN32
#include <unistd.h>     //Needed for POSIX's close()
#endif

#include <stdarg.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <netinet/in.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "Logger.h"
#include "Socket.h"
#include "GlobalArgs.h"
#include "Utils.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#ifdef WIN32
#define MSG_NOSIGNAL 0
#endif


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static void s_compose_error_reply(lu_uint8_t *replyPtr, lu_int32_t* replySize);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

#ifdef WIN32
 // Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")
#endif

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
lu_int32_t socket_make_socket(lu_int32_t port)
{
    lu_int32_t s;
    struct sockaddr_in si_me;

    //create a UDP socket
    if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {
        perror("socket");
        return -1;
    }

    // zero out the structure
    memset((char *) &si_me, 0, sizeof(si_me));

    si_me.sin_family = AF_INET;
    si_me.sin_port = htons(port);
    si_me.sin_addr.s_addr = htonl(INADDR_ANY);

    //bind socket to port
    if(bind(s , (struct sockaddr*)&si_me, sizeof(si_me) ) == -1)
    {
        perror("bind");
		socket_close(s);
		s = -1;
        logerr("Failed to open UDP socket at port:%d", port);
    }
    else
    {
    	loginfo("Open UDP socket[sockid: %d] for Gridkey device at port:%d", s, ntohs(si_me.sin_port));
    }

    return s;
}



lu_int32_t socket_close(lu_int32_t sock)
{
#ifdef WIN32
    return closesocket(sock);
#else
    return close(sock);
#endif
}


void* socket_server_init(
                void* userContext,
                lu_int32_t port,
                socket_request_handler reqhandler,
                socket_connchg_handler connchghandler,
                lu_uint32_t devNum)
{

    socket_server_context_t* ctxt = (socket_server_context_t*)malloc(sizeof(socket_server_context_t));
    ctxt->handler = reqhandler;
    ctxt->connHandler= connchghandler;
    ctxt->userContext = userContext;

    lu_uint32_t i;
    lu_int32_t  s;
    for (i = 0; i < devNum; ++i)
    {
    	ctxt->portlist[i] = port + i;
        s = socket_make_socket(ctxt->portlist[i]);
        ctxt->connectlist[i] = s;
        ctxt->highsock = s;
    }
    ctxt->connectNum = devNum;

    return (void*)ctxt;
}

void socket_server_process(void* socketContext)
{
    socket_server_context_t* ctxt = (socket_server_context_t*)socketContext;

    static lu_uint8_t buf[SOCK_RW_BUF_SIZE];
    struct sockaddr_in si_other;
    lu_int32_t slen = sizeof(si_other);
    lu_int32_t recv_len = 0;
    lu_int32_t send_len = 0;
    lu_int32_t retval;
    lu_int32_t i;

    while(1)
    {
        FD_ZERO(&(ctxt->socks));
        for (i = 0; i < ctxt->connectNum; ++i)
		{
			FD_SET(ctxt->connectlist[i], &(ctxt->socks));
		}

        loginfo("Socket >> waiting Gridkey connection ...");
        retval = select(ctxt->highsock + 1, &ctxt->socks, NULL, NULL, NULL);

        if (retval == -1)
        {
            perror ("select");
        }
        else if (retval == 0)
        {
           printf("timeout occurred!\n");
        }
        else
        {
            lu_int32_t i;
            for (i = 0; i < ctxt->connectNum; ++i)
            {
                if(FD_ISSET(ctxt->connectlist[i], &ctxt->socks))
                {
                	FD_CLR(ctxt->connectlist[i], &ctxt->socks);

                    loginfo("Socket >> selected socket:[%d]", ctxt->connectlist[i]);
                    // zero out the structure
                    memset((char *) &si_other, 0, sizeof(si_other));

                    //try to receive some data, this is a blocking call
                    if ((recv_len = recvfrom(ctxt->connectlist[i], buf, SOCK_RW_BUF_SIZE,0,(struct sockaddr *) &si_other, &slen)) == -1)
                    {
                        perror("recvfrom()");
                    }
                    else
                    {
                       // proceed the the data received from peer
                       loginfo("Socket >> received packet from %s:%d, size:%d", inet_ntoa(si_other.sin_addr), ntohs(si_other.sin_port), recv_len);
                       send_len = 0;
                       loginfo("Socket >> handling request from device: %d", i);
                       if ((*(ctxt->handler))(i, buf, recv_len, buf, &send_len) != 0)
                       {
                           s_compose_error_reply(buf, &send_len);
                       }

                       //now send reply to peer
                       if (sendto(ctxt->connectlist[i], buf, send_len, 0, (struct sockaddr*) &si_other, slen) == -1)
                       {
                           perror("sendto()");
                       }
                       else
                       {
                           loginfo("Socket >> sent reply, size:%d",send_len);
                       }
                    }
                }
            }
        }
    } // end of while()


}

void socket_server_shudown(void* context)
{
    lu_int32_t i;
    if(context != NULL)
    {
        socket_server_context_t* ctxt = (socket_server_context_t*)context;
        for(i =0; i < MAX_CONNECTION_NUM; i++)
        {
            socket_close(ctxt->connectlist[i]);
            ctxt->connectlist[i] = -1;
        }
        ctxt->connectNum = 0;
        ctxt->connHandler(ctxt, ctxt->connectNum);
        socket_close(ctxt->listenSock);

        free(ctxt);
    }
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */
static void s_compose_error_reply(lu_uint8_t *replyPtr, lu_int32_t* replySize)
{
    const lu_char_t* err = "internal error occurred";
    
    loginfo("Socket >> s_compose_error_reply() called");
    strcpy((lu_char_t*)replyPtr, err);
    *replySize = strlen(err);
}


/*
 *********************** End of file ******************************************
 */
