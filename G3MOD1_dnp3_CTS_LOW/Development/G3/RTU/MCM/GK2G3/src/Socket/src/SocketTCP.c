/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:Socket.c
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   8 Oct 2015     pwang     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <errno.h>
#include <stddef.h>
#include <stdio.h>
#include <memory.h>
#include <stdlib.h>

#ifndef WIN32
#include <unistd.h>     //Needed for POSIX's close()
#endif

#include <stdarg.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <netinet/in.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "Logger.h"
#include "Socket.h"
#include "GlobalArgs.h"
#include "Utils.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define BACKLOG 10  // how many pending connections queue will hold

#ifdef WIN32
#define MSG_NOSIGNAL 0
#endif


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static void s_compose_error_reply(lu_uint8_t *replyPtr, lu_int32_t* replySize);
static void s_build_select_list(socket_server_context_t* ctxt);
static void s_read_socks(socket_server_context_t* ctxt);
static void s_handle_new_connection(socket_server_context_t* ctxt);
static void s_deal_with_data(socket_server_context_t* ctxt, const lu_int32_t listnum);
static void s_socket_set_nonblock(lu_int32_t sockfd);
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

#ifdef WIN32
 // Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")
#endif

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

#ifdef WIN32
void socket_initWinsock()
{
    WSADATA wsaData;
    lu_int32_t iResult = 0;
    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0)
    {
       printf("WSAStartup failed with error: %d\n", iResult);
    }
}
#endif

void s_socket_set_nonblock(lu_int32_t sockfd)
{
#ifdef WIN32
    lu_int32_t NonBlock = 1;

    if (ioctlsocket(sockfd, FIONBIO, &NonBlock) == SOCKET_ERROR)
    {
        printf("ioctlsocket() failed with error %d\n", WSAGetLastError());
    }
    else
    {
        printf("ioctlsocket() is OK!\n");
    }
#else
    lu_int32_t flags;

    /* Set socket to non-blocking */

    if ((flags = fcntl(sockfd, F_GETFL, 0)) < 0)
    {
        logerr("Socket >> Failed to set nonblock");
    }


    if (fcntl(sockfd, F_SETFL, flags | O_NONBLOCK) < 0)
    {
        logerr("Socket >> Failed to set nonblock");
    }
#endif
}


lu_int32_t socket_make_socket(lu_int32_t port)
{
    struct addrinfo hints;
    struct addrinfo* servinfo;
    lu_int32_t rv;
    lu_int32_t listenSock = -1;
    lu_char_t portStr[20];
    lu_int32_t yes = 1;

    sprintf(portStr,"%i",port);// converts to port string.

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;// AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = AI_PASSIVE; // use my IP

    if ((rv = getaddrinfo(NULL, portStr, &hints, &servinfo)) != 0)
    {
        logerr("Socket >> getaddrinfo: %s", gai_strerror(rv));
    }
    else
    {
        // Create socket
        listenSock = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);
        if(listenSock < 0)
        {
            perror("failed to create socket");
        }
        else
        {
            // Set socket reusable
            if (setsockopt(listenSock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(lu_int32_t)) == -1)
            {
                socket_close(listenSock);
                listenSock = -1;
                perror("failed to setsockopt");
            }
            else
            {
                // Set socket to non-blocking
                s_socket_set_nonblock(listenSock);
                // Bind socket
                if(bind(listenSock, servinfo->ai_addr, servinfo->ai_addrlen) != 0)
                {
                    socket_close(listenSock);
                    listenSock = -1;
                    perror("failed to bind");
                }
            }
        }
        freeaddrinfo(servinfo); // all done with this structure

        // listen to socket
        if(listenSock >= 0)
        {
            if(listen(listenSock, BACKLOG) != 0)
            {
                socket_close(listenSock);
                listenSock = -1;
                perror("failed to listen");
            }
            else
            {
                loginfo("Listening TCP port:%d", port);
            }
        }
    }

    return listenSock;
}


lu_int32_t socket_recv_data(lu_int32_t socket, lu_uint8_t* recvBuffer, lu_int32_t bufSize)
{
    lu_int32_t result = 0;
    loginfo("receiving data from socket:%d ",socket);

    do
    {
        /* Receive a reply from the server*/
        result = recv(socket, (lu_char_t*)recvBuffer, bufSize, 0);
    }
    while( (result == -1) && (errno == EINTR) );


    if (result > 0 && g_args.debug_socket)
    {
        dump_data("received data", recvBuffer, result);
    }

    return result;
}


lu_int32_t socket_send_data(lu_int32_t socket, lu_uint8_t* sendBuffer, lu_int32_t sendSize)
{

    lu_int32_t result;      //Partial result
    lu_int32_t sent = 0;    //Final result of the operation

    do
    {
        result = send(socket, sendBuffer + sent, sendSize - sent, MSG_NOSIGNAL);
        if (result > 0)
        {
            sent += result;
        }
        else if( (result < 0) && (errno != EINTR) )
        {
            break;// failure
        }
    }
    while (sendSize > sent);


    loginfo("sent data[%d bytes]",sendSize);
    if(sendSize == sent && g_args.debug_socket)
    {
        dump_data("sent data", sendBuffer, sendSize);
    }

    return sent;
}


lu_int32_t socket_close(lu_int32_t sock)
{
#ifdef WIN32
    return closesocket(sock);
#else
    return close(sock);
#endif
}


void* socket_server_init(
                void* userContext,
                lu_int32_t port,
                socket_request_handler reqhandler,
                socket_connchg_handler connchghandler,
                lu_uint32_t devNum)
{

    socket_server_context_t* ctxt = (socket_server_context_t*)malloc(sizeof(socket_server_context_t));
    ctxt->handler = reqhandler;
    ctxt->connHandler= connchghandler;
    ctxt->userContext = userContext;

    lu_int32_t listenSock = socket_make_socket(port);
    lu_uint32_t i;

    /* Since we start with only one socket, the listening socket,
           it is the highest socket so far. */
    ctxt->highsock = listenSock;
    ctxt->listenSock = listenSock;
    //Init all connections on the list to an invalid socket value
    for (i = 0; i < MAX_CONNECTION_NUM; ++i)
    {
        ctxt->connectlist[i] = -1;
    }
    ctxt->connectNum = 0;

    return (void*)ctxt;
}

void socket_server_process(void* socketContext)
{

    lu_int32_t result;
    lu_uint16_t listnum;

    socket_server_context_t* ctxt = (socket_server_context_t*)socketContext;

//    struct timeval timeout;  /* Timeout for select */
//    timeout.tv_sec = 3;
//    timeout.tv_usec = 0;


    s_build_select_list(ctxt);

    loginfo("waiting for request....");


    result = select(ctxt->highsock+1, &(ctxt->socks), (fd_set *) 0, (fd_set *) 0, /*&timeout*/ NULL);

    if (result == 0)
    {
        /* Select timeout*/
        logerr("Socket >> select() timed out!");
    }
    else if( (result < 0) && (errno != EINTR) )
    {
        /* Select error*/
        logerr("Socket >> Error in select(): %s", strerror(errno));

        /* Close related sockets*/
        for (listnum = 0; listnum < MAX_CONNECTION_NUM; listnum++)
        {
            if(ctxt->connectlist[listnum] >= 0)
            {
                if (FD_ISSET(ctxt->connectlist[listnum], &(ctxt->socks)))
                {
                    loginfo("closed socket:%d", ctxt->connectlist[listnum]);
                    socket_close(ctxt->connectlist[listnum]);
                    ctxt->connectlist[listnum] = -1;
                    ctxt->connectNum --;
                    ctxt->connHandler(ctxt, ctxt->connectNum);
                }
            }
        }

    }
    else if (result > 0)
    {
        loginfo("Socket >> selected socket. num:%d", result);
        s_read_socks(ctxt);
    }
}

void socket_server_shudown(void* context)
{
    lu_int32_t i;
    if(context != NULL)
    {
        socket_server_context_t* ctxt = (socket_server_context_t*)context;
        for(i =0; i < MAX_CONNECTION_NUM; i++)
        {
            socket_close(ctxt->connectlist[i]);
            ctxt->connectlist[i] = -1;
        }
        ctxt->connectNum = 0;
        ctxt->connHandler(ctxt, ctxt->connectNum);
        socket_close(ctxt->listenSock);

        free(ctxt);
    }
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */
static void s_compose_error_reply(lu_uint8_t *replyPtr, lu_int32_t* replySize)
{
    const lu_char_t* err = "internal error occurred";
    
    loginfo("Socket >> s_compose_error_reply() called");
    strcpy((lu_char_t*)replyPtr, err);
    *replySize = strlen(err);
}


static void s_build_select_list(socket_server_context_t* ctxt)
{
    lu_int32_t listnum;         /* Current item in connectlist for for loops */

    FD_ZERO(&(ctxt->socks));
    FD_SET(ctxt->listenSock, &(ctxt->socks));

    /* Loops through all the possible connections and adds
        those sockets to the fd_set */
    for (listnum = 0; listnum < MAX_CONNECTION_NUM; listnum++)
    {
        if (ctxt->connectlist[listnum] >= 0)
        {
            //Add socket to the connection list
            FD_SET(ctxt->connectlist[listnum], &(ctxt->socks));
            if (ctxt->connectlist[listnum] > ctxt->highsock)
            {
                ctxt->highsock = ctxt->connectlist[listnum];
            }
        }
    }
}


static void s_handle_new_connection(socket_server_context_t* ctxt)
{
    lu_int32_t listnum;        /* Current item in connectlist for for loops */
    lu_int32_t connection;     /* Socket file descriptor for incoming connections */
    const lu_char_t* errTxt = "Sorry, this server is too busy. Try again later!\r\n";

    /* We have a new connection coming in!  We'll try to find a spot for it in connectlist. */
    connection = accept(ctxt->listenSock, NULL, NULL);
    if (connection < 0)
    {
        perror("failed to accept");
        return;
    }

    s_socket_set_nonblock(connection);

    /* Add new connection to the queue. */
    for (listnum = 0; (listnum < MAX_CONNECTION_NUM) && (connection != -1); listnum++)
    {
        if (ctxt->connectlist[listnum] < 0)
        {
            loginfo("Socket >> new connection accepted:   FD=%d; Slot=%d", connection, listnum);
            ctxt->connectlist[listnum] = connection;
            ctxt->connectNum ++;
            ctxt->connHandler(ctxt, ctxt->connectNum);
            connection = -1;    //Break loop
        }
    }

    /* Failed to add new connections to queue. */
    if (connection > 0)
    {
        //A connection was accepted but not added to the queue
        logerr("Socket >> no room left for new connection");
        socket_send_data(connection, (lu_uint8_t*)errTxt, strlen(errTxt) );
        socket_close(connection);
    }
}


static void s_deal_with_data(socket_server_context_t* ctxt, const lu_int32_t listnum)
{
    static lu_uint8_t readBuf [SOCK_RW_BUF_SIZE];    /* Buffer for socket reads */
    static lu_uint8_t writeBuf[SOCK_RW_BUF_SIZE];    /* Buffer for socket reads */
    lu_int32_t writeSize = 0;
    lu_int32_t readSize = 0;

    readSize = socket_recv_data(ctxt->connectlist[listnum],readBuf,SOCK_RW_BUF_SIZE);

    if (readSize < 0)
    {
        /* Connection closed, close this end and free up entry in connectlist */
        logerr("Socket >> connection lost: FD=%d;  Slot=%d", ctxt->connectlist[listnum],listnum);
        socket_close(ctxt->connectlist[listnum]);
        ctxt->connectlist[listnum] = -1;
        ctxt->connectNum --;
        ctxt->connHandler(ctxt, ctxt->connectNum);
    }
    else if(readSize > 0)
    {
        loginfo("Socket >> received data size: %d",readSize);
        loginfo("Socket >> processing data...");
        if ((*(ctxt->handler))(listnum, readBuf, readSize, writeBuf, &writeSize) != 0)
        {
            s_compose_error_reply(writeBuf, &writeSize);
        }
        loginfo("Socket >> sending reply...");
        socket_send_data(ctxt->connectlist[listnum], writeBuf, writeSize);
    }
    else
    {
        loginfo("peer is shutdown. closing connection index:%d.",listnum);
        socket_close(ctxt->connectlist[listnum]);
        ctxt->connectlist[listnum] = -1;
        ctxt->connectNum --;
        ctxt->connHandler(ctxt, ctxt->connectNum);
    }
}

static void s_read_socks(socket_server_context_t* ctxt)
{
    lu_int32_t listnum;         /* Current item in connectlist for for loops */

    /* Deal with listenSock */
    if(ctxt->listenSock >= 0)
    {
        if (FD_ISSET(ctxt->listenSock, &(ctxt->socks)))
        {
            if(g_args.verbose)
                loginfo("Socket >> handle new connection");
            s_handle_new_connection(ctxt);
        }
    }
    /* Deal with connection sockets */
    for (listnum = 0; listnum < MAX_CONNECTION_NUM; listnum++)
    {
        if(ctxt->connectlist[listnum] >= 0)
        {
            if (FD_ISSET(ctxt->connectlist[listnum], &(ctxt->socks)))
            {
                if(g_args.verbose)
                    loginfo("Socket >> s_deal_with_data");
                s_deal_with_data(ctxt, listnum);
            }
        }
    }
}



/*
 *********************** End of file ******************************************
 */
