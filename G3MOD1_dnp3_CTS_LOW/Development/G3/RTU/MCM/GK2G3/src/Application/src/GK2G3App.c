/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:Thread.c
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   2 May 2017     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <pthread.h>
#include <unistd.h> // sleep
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <modbus.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Logger.h"
#include "Utils.h"
#include "GKServer.h"
#include "GKData.h"
#include "GK2G3.h"
#include "MBSlave.h"
#include "GlobalArgs.h"
#include "GKDataCentre.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
 static void run_console(void)                     ;
 static void* mbslave_thread_routine(void* arg)    ;
 static void* gkserver_thread_routine(void* arg)   ;
// static void* gkdc_thread_routine(void* arg)       ;
 static void  print_console_help(void)             ;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */




/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

lu_int32_t launch_gk2g3_threads(void)
{
    /* this variable is our reference to the second thread */
    pthread_t thread_mmb;
    pthread_t thread_gkserver;
    //pthread_t thread_gkdc;
    lu_int32_t ret,i;

    const lu_int32_t mb_num = g_conf.gkdev_number;
    g_mb_ctxt = mbslave_init(DEFAULT_MB_SLAVE_PORT, mb_num);
    for (i = 0; i < mb_num; ++i)
    {
        /* Create a thread for each modbus slave session */
        ret = pthread_create(&thread_mmb, NULL, &mbslave_thread_routine, g_mb_ctxt[i]);
        if (ret != 0)
        {
            logerr("Error creating thread");
            return 1;
        }
    }

    /* Create a gridkey server thread for accepting connection from GK devices.*/
   ret = pthread_create(&thread_gkserver, NULL, &gkserver_thread_routine, 0);
   if (ret != 0)
   {
       logerr("Error creating thread");
       return 1;
   }

   if(g_args.console)
   {
       sleep(1);// Waiting other threads to be launched
       run_console();
   }
   else
   {
       pthread_join(thread_mmb, 0);
       pthread_join(thread_gkserver, 0);
   }
   return 0;
}



/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */
 static void* mbslave_thread_routine(void* arg)
 {
     return (void*)(intptr_t)mbslave_run((mbslave_context_t*)arg);
 }


 static void* gkserver_thread_routine(void* arg)
 {
     LU_UNUSED(arg);
     return (void*)(intptr_t)gkserver_run();
 }

 static void print_console_help(void)
 {
      printf(" ---> Console Menu <---\n");
      printf(" ?. Help\n");
      printf(" 0. Exit \n");
      printf(" 1. View GK Analogues\n");
      printf(" 2. View GK Binaries\n");
      printf(" 3. View Modbus Analogues\n");
      printf(" 4. View Modbus Binaries\n");
      printf(" 5. Enable/Disable Modbus Debug\n");
      printf(" 6. Enable/Disable Socket Debug\n");
      printf("Select an option:\n");
 }

 static void run_console(void)
 {
     lu_char_t opt = -1;
     print_console_help();

     while(1)
     {
           scanf("%c", &opt);
           switch( opt )
           {
              case '0':
                  loginfo("Exit");
                  exit(0);
                  break;

              case '1':
                  dump_data("GK Analogues", g_gk_data.analogue, ANALOGUE_COUNT * sizeof(analogue_t));
                  break;

              case '2':
                  dump_data("GK Binaries", g_gk_data.binary, BINARY_COUNT * sizeof(binary_t));
                  break;

              case '3':
                  printf("not implemented\n");
 //               dump_data("Modbus Analogues",
 //                       ((modbus_mapping_t*)gp_mb_mapping[0])->tab_input_registers,
 //                       ((modbus_mapping_t*)gp_mb_mapping[0])->nb_input_registers*2);
                  break;

              case '4':
                  printf("not implemented\n");
 //               dump_data("Modbus Binaries",
 //                       ((modbus_mapping_t*)gp_mb_mapping[0])->tab_input_bits,
 //                       ((modbus_mapping_t*)gp_mb_mapping[0])->nb_input_bits);
                  break;

              case '5':
                  g_args.debug_modbus = !g_args.debug_modbus;
                  break;

              case '6':
                  g_args.debug_socket = !g_args.debug_socket;
                  break;

              break;

              case '?':
                  print_console_help();
                  break;

              case '\n':
                  break;

              default:
                  printf("Invalid option:\"%c\" \n",opt);
                  break;
           }
       }
 }


/*
 *********************** End of file ******************************************
 */
