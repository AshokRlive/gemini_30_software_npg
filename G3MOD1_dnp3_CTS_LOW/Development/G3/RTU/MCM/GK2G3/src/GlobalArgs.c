/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:GlobalArgs.c
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   18 May 2017     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <getopt.h>
#include <ini.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "GlobalArgs.h"
#include "Logger.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static void display_usage(lu_char_t* appname);
static int config_parsing_handler(void* user, const char* section, const char* name,
                   const char* value);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static struct option long_options[] = {
            {"config",   required_argument, 0,  'c' },
            {"console",  no_argument,       0,  'o' },
            {"verbose",  no_argument,       0,  'v' },
            {"help",     no_argument,       0,  'h' },
            { NULL,      no_argument,       NULL, 0 }
        };

static lu_char_t* long_options_desc[] = {
            "specify the path of configuration file",
            "launch a console for diagnosing",
            "verbose mode",
            "display this message",
            ""
        };

static const char *optString = "c:ovh?";

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
lu_int32_t parse_args(lu_int32_t argc, lu_char_t **argv, global_args_t *args)
{

    int opt_val;
    int opt_index;

    while (1)
    {
        opt_index = 0;
        opt_val = getopt_long(argc, argv, optString, long_options, &opt_index);
        if (opt_val == -1)
            break;

        switch (opt_val)
        {
            break;

            case 'c':
            {
                args->config_file = optarg;
            }
            break;

            case 'v':
            {
                args->verbose = LU_TRUE;
            }
            break;

            case 'o':
            {
                args->console = LU_TRUE;
            }
            break;

            case 0: /* long option without a short arg */
            {
                printf("option %s", long_options[opt_index].name);
                if (optarg)
                    printf(" with arg %s", optarg);
                printf("\n");
            }
            break;

            case 'h': /* fall-through is intentional */
            case '?':
                display_usage(argv[0]);
                return -1;

            default:
                /* You won't actually get here. */
                printf("?? getopt returned character code 0%o ??\n", opt_val);
                return -1;
        }
    }

    if (optind < argc)
    {
        printf("non-option ARGV-elements: ");
        while (optind < argc)
            printf("%s ", argv[optind++]);
        printf("\n");
        return -1;
    }
    return 0;
}

lu_int32_t parse_config(const lu_char_t* configFile, global_conf_t* conf)
{
    lu_int32_t ret = 0;

    if (ini_parse(configFile, config_parsing_handler, conf) < 0)
    {
        loginfo("Can't load config file '%s', default configuration will be applied.",configFile);
    }
    else
    {
        loginfo("Loaded config from '%s':\n\t[%-15s]ipaddr=%s, port=%d, active=%d"
            "\n\t[%-15s]device num:%d port:%d", configFile,
            "Data Centre",conf->dc_ipaddr, conf->dc_port, conf->dc_active,
            "Grikey Devices", conf->gkdev_number, conf->gkdev_port);

        /* Validate configuration */
        if(conf->gkdev_number > 10)
        {
            logerr("Gridkey number must be equal or less than 10!");
            ret = -1;
        }

        if(conf->gkdev_port <=0 || conf->gkdev_port >= 65565)
        {
            logerr("Gridkey port must be within range (0,65565)!");
            ret = -1;
        }
    }

    return ret;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
static void display_usage(lu_char_t* appname)
{
    fprintf(stderr, "An application for receiving data messages from Gridkey devices "
                    "and converting them to modbus sessions(slave) which can be integrated with G3.\n");
    fprintf(stderr, "Usage: %s [-v] [-h] \n  options:\n", appname);

    lu_uint32_t i;
    for (i = 0; i < sizeof(long_options)/sizeof(struct option) -1; i++)
    {
        fprintf(stderr, "  --%s -%c %s\n\t%s\n",
                        long_options[i].name,
                        long_options[i].val,
                        long_options[i].has_arg == required_argument?"[Require Argument]":"",
                        long_options_desc[i]);
    }

}



static int config_parsing_handler(void* user, const char* section, const char* name,
                   const char* value)
{
    global_conf_t* pconfig = (global_conf_t*)user;

    #define MATCH(s, n) strcmp(section, s) == 0 && strcmp(name, n) == 0

    if(MATCH("Data Centre", "ipaddr"))
        strncpy(pconfig->dc_ipaddr, value, sizeof(pconfig->dc_ipaddr));
    else if(MATCH("Data Centre", "port"))
        pconfig->dc_port = atoi(value);
    else if(MATCH("Data Centre", "active"))
        pconfig->dc_active = (strcmp("true", value) == 0);
    else if(MATCH("Grikey Devices", "number"))
        pconfig->gkdev_number = atoi(value);
    else if(MATCH("Grikey Devices", "port"))
        pconfig->gkdev_port = atoi(value);
    else
        return 0; /* unknown section/name, error */

    return 1;
}
/*
 *********************** End of file ******************************************
 */
