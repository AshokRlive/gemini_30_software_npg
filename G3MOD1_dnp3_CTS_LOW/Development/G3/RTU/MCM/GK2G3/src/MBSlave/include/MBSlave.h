/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MBSlave.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   2 May 2017     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef RTU_MCM_GK2G3_SRC_MBSLAVE_INCLUDE_MBSLAVE_H_
#define RTU_MCM_GK2G3_SRC_MBSLAVE_INCLUDE_MBSLAVE_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <modbus.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define DEFAULT_MB_SLAVE_PORT 	502
#define DEFAULT_MB_SLAVE_ADDR 	"127.0.0.1"


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
typedef struct mbslave_context_struct
{
    modbus_mapping_t*       mapping;
    const lu_char_t*        ip;
    lu_int32_t              port;
} mbslave_context_t;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
extern mbslave_context_t** g_mb_ctxt;


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */
mbslave_context_t** mbslave_init(lu_int32_t port, lu_int32_t num);

lu_int32_t mbslave_destroy(mbslave_context_t** ctxt, lu_int32_t num);

lu_int32_t mbslave_run(mbslave_context_t* ctxt);


#endif /* RTU_MCM_GK2G3_SRC_MBSLAVE_INCLUDE_MBSLAVE_H_ */

/*
 *********************** End of file ******************************************
 */
