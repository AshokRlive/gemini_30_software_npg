/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MBSlave.c
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   2 May 2017     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <pthread.h>
#include <assert.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "MBSlave.h"
#include "Logger.h"
#include "GKData.h"
#include "Utils.h"
#include "GlobalArgs.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define COILS_BITS 				0
#define DISCRETE_INPUT_BITS 	(BINARY_COUNT * BINARY_SIZE_BYTES * 8)
#define INPUT_REG_WORDS 		(ANALOGUE_COUNT * ANALOGUE_SIZE_BYTES / 2)
#define HOLDING_REG_WORDS 		0
/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static modbus_mapping_t* mbslave_create_mapping(void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
mbslave_context_t**     g_mb_ctxt;
pthread_mutex_t         g_mb_mapping_lock;

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
mbslave_context_t** mbslave_init(lu_int32_t port, lu_int32_t num)
{
    lu_int32_t i;

    mbslave_context_t** pctxt = malloc(sizeof(mbslave_context_t*) * num);

    for (i = 0; i < num; ++i)
    {
        pctxt[i] = malloc(sizeof(mbslave_context_t));
        pctxt[i]->mapping = mbslave_create_mapping();
        pctxt[i]->ip = DEFAULT_MB_SLAVE_ADDR;
        pctxt[i]->port = port + i;
        loginfo("Initialised modbus slave[%s:%d]",
                        pctxt[i]->ip, pctxt[i]->port);
    }

    return pctxt;
}

lu_int32_t mbslave_destroy(mbslave_context_t** ctxt, lu_int32_t num)
{
    //TODO TO BE IMPLEMENTED
    //modbus_mapping_free(mb_mapping);
    LU_UNUSED(ctxt);
    LU_UNUSED(num);
    return -1;
}


lu_int32_t mbslave_run(mbslave_context_t* mbslave)
{
    //const lu_int32_t devNum = g_args.gkdevice_number;
    lu_int32_t socket = -1;
    modbus_t* ctxt;




    //TODO Create multiple ctxt and select them.
    while(1)
    {
        loginfo("Waiting Modbus TCP connection on [%s:%d]",
                        mbslave->ip, mbslave->port);
        ctxt = modbus_new_tcp(mbslave->ip, mbslave->port);

        modbus_set_debug(ctxt, g_args.debug_modbus);
        //modbus_set_slave(ctxt, 123);

        socket = modbus_tcp_listen(ctxt, 1);
        modbus_tcp_accept(ctxt, &socket);
        loginfo("Modbus TCP connected");
        lu_uint32_t count = 0;
        while (1)
        {
            uint8_t query[MODBUS_TCP_MAX_ADU_LENGTH];
            lu_int32_t rc;

            rc = modbus_receive(ctxt, query);
            if (rc != -1)
            {
                /* rc is the query size */
                pthread_mutex_lock(&g_mb_mapping_lock);
                modbus_reply(ctxt, query, rc, mbslave->mapping);
                pthread_mutex_unlock(&g_mb_mapping_lock);
                if (g_args.debug_modbus)
                    loginfo("Modbus >> replied [%d]", count++);
            }
            else
            {
                /* Connection closed by the client or error */
                loginfo("Modbus connection closed");
                break;
            }
        }

        close(socket);
        modbus_free(ctxt);
    }

    loginfo("Quit Modbus Slave: %s\n", modbus_strerror(errno));
    return 0;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */



/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
static modbus_mapping_t* mbslave_create_mapping(void)
{
    modbus_mapping_t * mapping = NULL;

    // Allocate modbus mapping
    mapping = modbus_mapping_new(
            COILS_BITS,
            DISCRETE_INPUT_BITS,
            HOLDING_REG_WORDS,
            INPUT_REG_WORDS);
    if (mapping == NULL)
    {
         logerr("Failed to allocate the modbus mapping: %s\n",
                 modbus_strerror(errno));
     }
    else
     {
        assert(mapping->nb_input_registers == ANALOGUE_COUNT*ANALOGUE_SIZE_BYTES/2);
        assert(mapping->nb_input_bits == BINARY_COUNT*BINARY_SIZE_BYTES * 8);

        loginfo("Allocated modbus mapping nb_bits:%d, nb_input_bits:%d, nb_input_registers:%d, nb_registers:%d",
                        mapping->nb_bits,
                        mapping->nb_input_bits,
                        mapping->nb_input_registers,
                        mapping->nb_registers);
     }

    return mapping;
}


/*
 *********************** End of file ******************************************
 */
