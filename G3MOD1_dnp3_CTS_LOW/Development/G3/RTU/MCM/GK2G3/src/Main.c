/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:Main.c
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11 May 2017     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <string.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "GK2G3.h"
#include "Logger.h"
#include "GlobalArgs.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
global_args_t   g_args;
global_conf_t   g_conf;


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */





/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */




/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/**
 * GK2G3 main entry point.
 */
int main(int argc, char **argv)
{
    logger_open();

    // Initialises default global arguments & config
    memset(&g_args, 0, sizeof(global_args_t));
    memset(&g_conf, 0, sizeof(global_conf_t));
    g_args.config_file = DEFAULT_GK2G3_CONFIG;
    g_conf.gkdev_number = 1;
    g_conf.gkdev_port = DEFAULT_GK_PORT;

    // Parse arguments
    if(parse_args(argc, argv, &g_args) != 0)
    {
        logerr("Application exit caused by invalid arguments!");
        return -1;
    }

    // Parse configuration
    if(parse_config(g_args.config_file, &g_conf) != 0)
    {
        logerr("Application exit caused by invalid configuration!");
        return -1;
    }

    launch_gk2g3_threads();

    logger_close();
    loginfo("Application exit!");
    return 0;
}


/*
 *********************** End of file ******************************************
 */





