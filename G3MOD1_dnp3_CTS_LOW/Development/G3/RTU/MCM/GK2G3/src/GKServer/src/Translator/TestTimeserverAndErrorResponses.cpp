#include "stdafx.h"
#include "CppUnitTest.h"
#include <ctime>

extern "C" {

#include "GK_Gk2G3Translator.h"

extern uint8_T PacketFromMcu [1024] ;
extern uint8_T PacketToMcu [32] ;
extern uint16_T PacketLength ;

extern uint16_T G3Binaries [sizeof_GK_enuBinaries] ;
extern real32_T G3Analogues [sizeof_GK_enuAnalogues] ;

}

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

void ChckCommonData (void)
{
}

namespace TestGk2G3Translator
{		
	TEST_CLASS(TestTimeServerResponse)
	{
	public:
		
    TEST_METHOD (ReturnTimestampFromEpochStart)
    {
      time_t RawTime ;
      time (&RawTime) ;
      struct tm* DateTime ;
      DateTime = localtime (&RawTime) ;

      Logger::WriteMessage ("Setting everything in the date/time to 1 (apart from tm_mon)") ;

      DateTime->tm_year = 101 ; // Needs to be after 1900+1970
      DateTime->tm_mon = 0 ;
      DateTime->tm_mday = 1 ;
      DateTime->tm_wday = 1 ;
      DateTime->tm_hour = 1 ;
      DateTime->tm_min = 1 ;
      DateTime->tm_sec = 1 ;
      DateTime->tm_isdst = 0 ;

      memcpy (PacketFromMcu, "TIM", 3) ;

      uint8_T DecodeStatus = 
        GK_TranslateGridkeyMessages (
          mktime (DateTime),
          PacketFromMcu,
          PacketToMcu,
          &PacketLength,
          G3Binaries,
          G3Analogues) ;

      Assert::AreEqual (
        float (GK_DECODE_WAS_SUCCESSFUL),
        float (DecodeStatus),
        float (0),
        L"Decode was not successful") ;

      char_T ExpectedResponse [] = "1 01/01/01,01:01:01" ;

      Assert::AreEqual (
        reinterpret_cast<const char*> (ExpectedResponse),
        reinterpret_cast<const char*> (PacketToMcu),
        L"Timestamp was not epoch start") ;

      Assert::AreEqual (
        double (sizeof (ExpectedResponse) - 1),
        double (PacketLength),
        L"Packet length didn't match") ;
    }

    TEST_METHOD (ReturnErrorFromInvalidTime)
    {
      Logger::WriteMessage ("Setting the system time to -1") ;

      memcpy (PacketFromMcu, "TIM", 3) ;

      uint8_T DecodeStatus =
        GK_TranslateGridkeyMessages (
          time_t (-1),
          PacketFromMcu,
          PacketToMcu,
          &PacketLength,
          G3Binaries,
          G3Analogues) ;

      Assert::AreEqual (
        float (GK_DECODE_INVALID_SYSTEM_TIME),
        float (DecodeStatus),
        float (0),
        L"Decode was not successful") ;

      char_T ExpectedResponse [] = "1 01/01/01,01:01:01" ;

      Assert::AreEqual (
        reinterpret_cast<const char*> (ExpectedResponse),
        reinterpret_cast<const char*> (PacketToMcu),
        L"Timestamp was not epoch start") ;

      Assert::AreEqual (
        double (sizeof (ExpectedResponse) - 1),
        double (PacketLength),
        L"Packet length didn't match") ;
    }

    TEST_METHOD (ReturnErrorFromUnsupportedMessageId)
    {
      Logger::WriteMessage ("Setting the message identity to 0xFFFFFFFFFFFFFFFF") ;

      memset (PacketFromMcu, 0xFF, 8) ;

      time_t RawTime ;
      time (&RawTime) ;

      uint8_T DecodeStatus =
        GK_TranslateGridkeyMessages (
        RawTime,
        PacketFromMcu,
        PacketToMcu,
        &PacketLength,
        G3Binaries,
        G3Analogues) ;

      Assert::AreEqual (
        float (GK_DECODE_UNKNOWN_MESSAGE),
        float (DecodeStatus),
        float (0),
        L"Message was recognised") ;
    }

  };
}