/*
 * =============================================================================
 *
 * Classification: Commercial-In-Confidence
 *
 * Name:           %PM%
 * Revision:       %PR%
 * Current Status: %PS%
 *
 * Project:        GK2G3
 * Title:          GK2G3 Interface (GK23000066)
 * Author(s):      Dave Everett
 * Modified By:    %AUTHOR%
 *
 * Copyright Notice
 * ----------------
 *
 * The information contained in this document is proprietary to Lucy Electric
 * unless stated otherwise and is made available in confidence; it must not be
 * used or disclosed without the express written permission Lucy Electric. This
 * document may not be copied in whole or in part in any form without the
 *
 * Public Access: Freedom Of Information Act 2000
 *
 * This document contains trade secrets and/or sensitive commercial information
 * as of the date provided to the original recipient by Lucy Electric and is
 * provided in confidence. Following a request for this information public
 * authorities are to consult with Lucy Electric regarding the current
 * releasability of the information prior to the decision to release all or
 * part of this document. Release of this information by a public authority may
 *
 * UK Origin
 *
 * Lucy Electric Limited
 * Christopher Martin Road Basildon Essex SS14 3EL England
 * Telephone: 01268 522822 Facsimile: 01268 883140
 *
 * =============================================================================
 * Description
 * -----------------------------------------------------------------------------
 *
 * This file contains the interface definitions to facilitate the conversion of
 * a GridKey statistical report into a Gemini 3 Modbus memory map.
 *
 *
 * =============================================================================
 * Version History
 * -----------------------------------------------------------------------------
 *
 * %PRT%
 * %PL%
 *
 * =============================================================================
 */

#ifndef GK_Gk2g3Interface_H
#define GK_Gk2g3Interface_H

/* =============================================================================
 * Include the project-wide headers and definitions
 * =============================================================================
 */

#include <rtwtypes.h>

/* =============================================================================
 * Set up the macros that allow this file to "own" the contents
 * =============================================================================
 */

#ifdef GK_EXTERN
#error GK_EXTERN Already Declared
#endif

#ifndef GK_Gk2g3Interface_C
#define GK_EXTERN extern
#define GK_ASSIGN(...)
#else
#define GK_EXTERN
#define GK_ASSIGN(...) = __VA_ARGS__
#endif

/* =============================================================================
 * Resolve external references for this file (if required)
 * =============================================================================
 */

/* =============================================================================
 * Declare exported macros (if required)
 * =============================================================================
 */

// Base modbus address for binary data
#define GK_BASE_ADDRESS_FOR_BINARIES 0x10010000
// Base modbus address for analogue data
#define GK_BASE_ADDRESS_FOR_ANALOGUES 0x10000000

// The translation was successful
#define GK_DECODE_WAS_SUCCESSFUL 0
// The message checksum didn't match the calculated checksum
#define GK_DECODE_CHECKSUM_ERROR 1
// The message identity was not recognised
#define GK_DECODE_UNKNOWN_MESSAGE 2
// The system time was invalid
#define GK_DECODE_INVALID_SYSTEM_TIME 3
// The alarm category/reference combination requested was invalid
#define GK_DECODE_INVALID_CAT_REF_COMBINATION 4
// Indicate that a feeder alarm was set
#define GK_DECODE_SET_FEEDER_ALARM 5
// Indicate that a busbar alarm was set
#define GK_DECODE_SET_BUSBAR_ALARM 6
// Indicate the a statistical report was generated
#define GK_DECODE_SET_STATISTICAL_DATA 7 

// Bit mask for Unit Alarm asserted
#define GK_BIN_UNIT_ALARM_ASSERTED (1 << 15)
// Bit mask for Unit Data centre comms active
#define GK_BIN_UNIT_DATA_CENTRE_COMMS_ACTIVE (1 << 6)
// Bit mask for Unit Feeder 5 online
#define GK_BIN_UNIT_FEEDER_5_ONLINE (1 << 5)
// Bit mask for Unit Feeder 4 online
#define GK_BIN_UNIT_FEEDER_4_ONLINE (1 << 4)
// Bit mask for Unit Feeder 3 online
#define GK_BIN_UNIT_FEEDER_3_ONLINE (1 << 3)
// Bit mask for Unit Feeder 2 online
#define GK_BIN_UNIT_FEEDER_2_ONLINE (1 << 2)
// Bit mask for Unit Feeder 1 online
#define GK_BIN_UNIT_FEEDER_1_ONLINE (1 << 1)
// Bit mask for Unit MCU520 online
#define GK_BIN_UNIT_MCU520_ONLINE (1 << 0)

// Bit mask for Busbar Voltage Alarm asserted
#define GK_BIN_BUSBAR_VOLTAGE_ALARM_ASSERTED (1 << 15)
// Bit mask for Busbar Voltage Phase 1 alarm
#define GK_BIN_BUSBAR_VOLTAGE_PHASE_1_ALARM (1 << 14)
// Bit mask for Busbar Voltage Phase 2 alarm
#define GK_BIN_BUSBAR_VOLTAGE_PHASE_2_ALARM (1 << 13)
// Bit mask for Busbar Voltage Phase 3 alarm
#define GK_BIN_BUSBAR_VOLTAGE_PHASE_3_ALARM (1 << 12)
// Bit mask for Busbar Voltage Phase 1 voltage high-high
#define GK_BIN_BUSBAR_VOLTAGE_PHASE_1_VOLTAGE_HIGH_HIGH (1 << 11)
// Bit mask for Busbar Voltage Phase 1 voltage high
#define GK_BIN_BUSBAR_VOLTAGE_PHASE_1_VOLTAGE_HIGH (1 << 10)
// Bit mask for Busbar Voltage Phase 1 voltage low
#define GK_BIN_BUSBAR_VOLTAGE_PHASE_1_VOLTAGE_LOW (1 << 9)
// Bit mask for Busbar Voltage Phase 1 voltage low-low
#define GK_BIN_BUSBAR_VOLTAGE_PHASE_1_VOLTAGE_LOW_LOW (1 << 8)
// Bit mask for Busbar Voltage Phase 2 voltage high-high
#define GK_BIN_BUSBAR_VOLTAGE_PHASE_2_VOLTAGE_HIGH_HIGH (1 << 7)
// Bit mask for Busbar Voltage Phase 2 voltage high
#define GK_BIN_BUSBAR_VOLTAGE_PHASE_2_VOLTAGE_HIGH (1 << 6)
// Bit mask for Busbar Voltage Phase 2 voltage low
#define GK_BIN_BUSBAR_VOLTAGE_PHASE_2_VOLTAGE_LOW (1 << 5)
// Bit mask for Busbar Voltage Phase 2 voltage low-low
#define GK_BIN_BUSBAR_VOLTAGE_PHASE_2_VOLTAGE_LOW_LOW (1 << 4)
// Bit mask for Busbar Voltage Phase 3 voltage high-high
#define GK_BIN_BUSBAR_VOLTAGE_PHASE_3_VOLTAGE_HIGH_HIGH (1 << 3)
// Bit mask for Busbar Voltage Phase 3 voltage high
#define GK_BIN_BUSBAR_VOLTAGE_PHASE_3_VOLTAGE_HIGH (1 << 2)
// Bit mask for Busbar Voltage Phase 3 voltage low
#define GK_BIN_BUSBAR_VOLTAGE_PHASE_3_VOLTAGE_LOW (1 << 1)
// Bit mask for Busbar Voltage Phase 3 voltage low-low
#define GK_BIN_BUSBAR_VOLTAGE_PHASE_3_VOLTAGE_LOW_LOW (1 << 0)

// Bit mask for Busbar Current Alarm asserted
#define GK_BIN_BUSBAR_CURRENT_ALARM_ASSERTED (1 << 15)
// Bit mask for Busbar Current Phase 1 alarm
#define GK_BIN_BUSBAR_CURRENT_PHASE_1_ALARM (1 << 14)
// Bit mask for Busbar Current Phase 2 alarm
#define GK_BIN_BUSBAR_CURRENT_PHASE_2_ALARM (1 << 13)
// Bit mask for Busbar Current Phase 3 alarm
#define GK_BIN_BUSBAR_CURRENT_PHASE_3_ALARM (1 << 12)
// Bit mask for Busbar Current Phase 1 Current High-High
#define GK_BIN_BUSBAR_CURRENT_PHASE_1_CURRENT_HIGH_HIGH (1 << 11)
// Bit mask for Busbar Current Phase 1 Current High
#define GK_BIN_BUSBAR_CURRENT_PHASE_1_CURRENT_HIGH (1 << 10)
// Bit mask for Busbar Current Phase 1 Current Low
#define GK_BIN_BUSBAR_CURRENT_PHASE_1_CURRENT_LOW (1 << 9)
// Bit mask for Busbar Current Phase 1 Current Negative
#define GK_BIN_BUSBAR_CURRENT_PHASE_1_CURRENT_NEGATIVE (1 << 8)
// Bit mask for Busbar Current Phase 2 Current High-High
#define GK_BIN_BUSBAR_CURRENT_PHASE_2_CURRENT_HIGH_HIGH (1 << 7)
// Bit mask for Busbar Current Phase 2 Current High
#define GK_BIN_BUSBAR_CURRENT_PHASE_2_CURRENT_HIGH (1 << 6)
// Bit mask for Busbar Current Phase 2 Current Low
#define GK_BIN_BUSBAR_CURRENT_PHASE_2_CURRENT_LOW (1 << 5)
// Bit mask for Busbar Current Phase 2 Current Negative
#define GK_BIN_BUSBAR_CURRENT_PHASE_2_CURRENT_NEGATIVE (1 << 4)
// Bit mask for Busbar Current Phase 3 Current High-High
#define GK_BIN_BUSBAR_CURRENT_PHASE_3_CURRENT_HIGH_HIGH (1 << 3)
// Bit mask for Busbar Current Phase 3 Current High
#define GK_BIN_BUSBAR_CURRENT_PHASE_3_CURRENT_HIGH (1 << 2)
// Bit mask for Busbar Current Phase 3 Current Low
#define GK_BIN_BUSBAR_CURRENT_PHASE_3_CURRENT_LOW (1 << 1)
// Bit mask for Busbar Current Phase 3 Current Negative
#define GK_BIN_BUSBAR_CURRENT_PHASE_3_CURRENT_NEGATIVE (1 << 0)

// Bit mask for Feeder 1 Alarm asserted
#define GK_BIN_FEEDER_1_ALARM_ASSERTED (1 << 15)
// Bit mask for Feeder 1 Phase 1 alarm
#define GK_BIN_FEEDER_1_PHASE_1_ALARM (1 << 14)
// Bit mask for Feeder 1 Phase 2 alarm
#define GK_BIN_FEEDER_1_PHASE_2_ALARM (1 << 13)
// Bit mask for Feeder 1 Phase 3 alarm
#define GK_BIN_FEEDER_1_PHASE_3_ALARM (1 << 12)
// Bit mask for Feeder 1 Phase 1 Current High-High
#define GK_BIN_FEEDER_1_PHASE_1_CURRENT_HIGH_HIGH (1 << 11)
// Bit mask for Feeder 1 Phase 1 Current High
#define GK_BIN_FEEDER_1_PHASE_1_CURRENT_HIGH (1 << 10)
// Bit mask for Feeder 1 Phase 1 Current Low
#define GK_BIN_FEEDER_1_PHASE_1_CURRENT_LOW (1 << 9)
// Bit mask for Feeder 1 Phase 1 Current Negative
#define GK_BIN_FEEDER_1_PHASE_1_CURRENT_NEGATIVE (1 << 8)
// Bit mask for Feeder 1 Phase 2 Current High-High
#define GK_BIN_FEEDER_1_PHASE_2_CURRENT_HIGH_HIGH (1 << 7)
// Bit mask for Feeder 1 Phase 2 Current High
#define GK_BIN_FEEDER_1_PHASE_2_CURRENT_HIGH (1 << 6)
// Bit mask for Feeder 1 Phase 2 Current Low
#define GK_BIN_FEEDER_1_PHASE_2_CURRENT_LOW (1 << 5)
// Bit mask for Feeder 1 Phase 2 Current Negative
#define GK_BIN_FEEDER_1_PHASE_2_CURRENT_NEGATIVE (1 << 4)
// Bit mask for Feeder 1 Phase 3 Current High-High
#define GK_BIN_FEEDER_1_PHASE_3_CURRENT_HIGH_HIGH (1 << 3)
// Bit mask for Feeder 1 Phase 3 Current High
#define GK_BIN_FEEDER_1_PHASE_3_CURRENT_HIGH (1 << 2)
// Bit mask for Feeder 1 Phase 3 Current Low
#define GK_BIN_FEEDER_1_PHASE_3_CURRENT_LOW (1 << 1)
// Bit mask for Feeder 1 Phase 3 Current Negative
#define GK_BIN_FEEDER_1_PHASE_3_CURRENT_NEGATIVE (1 << 0)

// Bit mask for Feeder 2 Alarm asserted
#define GK_BIN_FEEDER_2_ALARM_ASSERTED (1 << 15)
// Bit mask for Feeder 2 Phase 1 alarm
#define GK_BIN_FEEDER_2_PHASE_1_ALARM (1 << 14)
// Bit mask for Feeder 2 Phase 2 alarm
#define GK_BIN_FEEDER_2_PHASE_2_ALARM (1 << 13)
// Bit mask for Feeder 2 Phase 3 alarm
#define GK_BIN_FEEDER_2_PHASE_3_ALARM (1 << 12)
// Bit mask for Feeder 2 Phase 1 Current High-High
#define GK_BIN_FEEDER_2_PHASE_1_CURRENT_HIGH_HIGH (1 << 11)
// Bit mask for Feeder 2 Phase 1 Current High
#define GK_BIN_FEEDER_2_PHASE_1_CURRENT_HIGH (1 << 10)
// Bit mask for Feeder 2 Phase 1 Current Low
#define GK_BIN_FEEDER_2_PHASE_1_CURRENT_LOW (1 << 9)
// Bit mask for Feeder 2 Phase 1 Current Negative
#define GK_BIN_FEEDER_2_PHASE_1_CURRENT_NEGATIVE (1 << 8)
// Bit mask for Feeder 2 Phase 2 Current High-High
#define GK_BIN_FEEDER_2_PHASE_2_CURRENT_HIGH_HIGH (1 << 7)
// Bit mask for Feeder 2 Phase 2 Current High
#define GK_BIN_FEEDER_2_PHASE_2_CURRENT_HIGH (1 << 6)
// Bit mask for Feeder 2 Phase 2 Current Low
#define GK_BIN_FEEDER_2_PHASE_2_CURRENT_LOW (1 << 5)
// Bit mask for Feeder 2 Phase 2 Current Negative
#define GK_BIN_FEEDER_2_PHASE_2_CURRENT_NEGATIVE (1 << 4)
// Bit mask for Feeder 2 Phase 3 Current High-High
#define GK_BIN_FEEDER_2_PHASE_3_CURRENT_HIGH_HIGH (1 << 3)
// Bit mask for Feeder 2 Phase 3 Current High
#define GK_BIN_FEEDER_2_PHASE_3_CURRENT_HIGH (1 << 2)
// Bit mask for Feeder 2 Phase 3 Current Low
#define GK_BIN_FEEDER_2_PHASE_3_CURRENT_LOW (1 << 1)
// Bit mask for Feeder 2 Phase 3 Current Negative
#define GK_BIN_FEEDER_2_PHASE_3_CURRENT_NEGATIVE (1 << 0)

// Bit mask for Feeder 3 Alarm asserted
#define GK_BIN_FEEDER_3_ALARM_ASSERTED (1 << 15)
// Bit mask for Feeder 3 Phase 1 alarm
#define GK_BIN_FEEDER_3_PHASE_1_ALARM (1 << 14)
// Bit mask for Feeder 3 Phase 2 alarm
#define GK_BIN_FEEDER_3_PHASE_2_ALARM (1 << 13)
// Bit mask for Feeder 3 Phase 3 alarm
#define GK_BIN_FEEDER_3_PHASE_3_ALARM (1 << 12)
// Bit mask for Feeder 3 Phase 1 Current High-High
#define GK_BIN_FEEDER_3_PHASE_1_CURRENT_HIGH_HIGH (1 << 11)
// Bit mask for Feeder 3 Phase 1 Current High
#define GK_BIN_FEEDER_3_PHASE_1_CURRENT_HIGH (1 << 10)
// Bit mask for Feeder 3 Phase 1 Current Low
#define GK_BIN_FEEDER_3_PHASE_1_CURRENT_LOW (1 << 9)
// Bit mask for Feeder 3 Phase 1 Current Negative
#define GK_BIN_FEEDER_3_PHASE_1_CURRENT_NEGATIVE (1 << 8)
// Bit mask for Feeder 3 Phase 2 Current High-High
#define GK_BIN_FEEDER_3_PHASE_2_CURRENT_HIGH_HIGH (1 << 7)
// Bit mask for Feeder 3 Phase 2 Current High
#define GK_BIN_FEEDER_3_PHASE_2_CURRENT_HIGH (1 << 6)
// Bit mask for Feeder 3 Phase 2 Current Low
#define GK_BIN_FEEDER_3_PHASE_2_CURRENT_LOW (1 << 5)
// Bit mask for Feeder 3 Phase 2 Current Negative
#define GK_BIN_FEEDER_3_PHASE_2_CURRENT_NEGATIVE (1 << 4)
// Bit mask for Feeder 3 Phase 3 Current High-High
#define GK_BIN_FEEDER_3_PHASE_3_CURRENT_HIGH_HIGH (1 << 3)
// Bit mask for Feeder 3 Phase 3 Current High
#define GK_BIN_FEEDER_3_PHASE_3_CURRENT_HIGH (1 << 2)
// Bit mask for Feeder 3 Phase 3 Current Low
#define GK_BIN_FEEDER_3_PHASE_3_CURRENT_LOW (1 << 1)
// Bit mask for Feeder 3 Phase 3 Current Negative
#define GK_BIN_FEEDER_3_PHASE_3_CURRENT_NEGATIVE (1 << 0)

// Bit mask for Feeder 4 Alarm asserted
#define GK_BIN_FEEDER_4_ALARM_ASSERTED (1 << 15)
// Bit mask for Feeder 4 Phase 1 alarm
#define GK_BIN_FEEDER_4_PHASE_1_ALARM (1 << 14)
// Bit mask for Feeder 4 Phase 2 alarm
#define GK_BIN_FEEDER_4_PHASE_2_ALARM (1 << 13)
// Bit mask for Feeder 4 Phase 3 alarm
#define GK_BIN_FEEDER_4_PHASE_3_ALARM (1 << 12)
// Bit mask for Feeder 4 Phase 1 Current High-High
#define GK_BIN_FEEDER_4_PHASE_1_CURRENT_HIGH_HIGH (1 << 11)
// Bit mask for Feeder 4 Phase 1 Current High
#define GK_BIN_FEEDER_4_PHASE_1_CURRENT_HIGH (1 << 10)
// Bit mask for Feeder 4 Phase 1 Current Low
#define GK_BIN_FEEDER_4_PHASE_1_CURRENT_LOW (1 << 9)
// Bit mask for Feeder 4 Phase 1 Current Negative
#define GK_BIN_FEEDER_4_PHASE_1_CURRENT_NEGATIVE (1 << 8)
// Bit mask for Feeder 4 Phase 2 Current High-High
#define GK_BIN_FEEDER_4_PHASE_2_CURRENT_HIGH_HIGH (1 << 7)
// Bit mask for Feeder 4 Phase 2 Current High
#define GK_BIN_FEEDER_4_PHASE_2_CURRENT_HIGH (1 << 6)
// Bit mask for Feeder 4 Phase 2 Current Low
#define GK_BIN_FEEDER_4_PHASE_2_CURRENT_LOW (1 << 5)
// Bit mask for Feeder 4 Phase 2 Current Negative
#define GK_BIN_FEEDER_4_PHASE_2_CURRENT_NEGATIVE (1 << 4)
// Bit mask for Feeder 4 Phase 3 Current High-High
#define GK_BIN_FEEDER_4_PHASE_3_CURRENT_HIGH_HIGH (1 << 3)
// Bit mask for Feeder 4 Phase 3 Current High
#define GK_BIN_FEEDER_4_PHASE_3_CURRENT_HIGH (1 << 2)
// Bit mask for Feeder 4 Phase 3 Current Low
#define GK_BIN_FEEDER_4_PHASE_3_CURRENT_LOW (1 << 1)
// Bit mask for Feeder 4 Phase 3 Current Negative
#define GK_BIN_FEEDER_4_PHASE_3_CURRENT_NEGATIVE (1 << 0)

// Bit mask for Feeder 5 Alarm asserted
#define GK_BIN_FEEDER_5_ALARM_ASSERTED (1 << 15)
// Bit mask for Feeder 5 Phase 1 alarm
#define GK_BIN_FEEDER_5_PHASE_1_ALARM (1 << 14)
// Bit mask for Feeder 5 Phase 2 alarm
#define GK_BIN_FEEDER_5_PHASE_2_ALARM (1 << 13)
// Bit mask for Feeder 5 Phase 3 alarm
#define GK_BIN_FEEDER_5_PHASE_3_ALARM (1 << 12)
// Bit mask for Feeder 5 Phase 1 Current High-High
#define GK_BIN_FEEDER_5_PHASE_1_CURRENT_HIGH_HIGH (1 << 11)
// Bit mask for Feeder 5 Phase 1 Current High
#define GK_BIN_FEEDER_5_PHASE_1_CURRENT_HIGH (1 << 10)
// Bit mask for Feeder 5 Phase 1 Current Low
#define GK_BIN_FEEDER_5_PHASE_1_CURRENT_LOW (1 << 9)
// Bit mask for Feeder 5 Phase 1 Current Negative
#define GK_BIN_FEEDER_5_PHASE_1_CURRENT_NEGATIVE (1 << 8)
// Bit mask for Feeder 5 Phase 2 Current High-High
#define GK_BIN_FEEDER_5_PHASE_2_CURRENT_HIGH_HIGH (1 << 7)
// Bit mask for Feeder 5 Phase 2 Current High
#define GK_BIN_FEEDER_5_PHASE_2_CURRENT_HIGH (1 << 6)
// Bit mask for Feeder 5 Phase 2 Current Low
#define GK_BIN_FEEDER_5_PHASE_2_CURRENT_LOW (1 << 5)
// Bit mask for Feeder 5 Phase 2 Current Negative
#define GK_BIN_FEEDER_5_PHASE_2_CURRENT_NEGATIVE (1 << 4)
// Bit mask for Feeder 5 Phase 3 Current High-High
#define GK_BIN_FEEDER_5_PHASE_3_CURRENT_HIGH_HIGH (1 << 3)
// Bit mask for Feeder 5 Phase 3 Current High
#define GK_BIN_FEEDER_5_PHASE_3_CURRENT_HIGH (1 << 2)
// Bit mask for Feeder 5 Phase 3 Current Low
#define GK_BIN_FEEDER_5_PHASE_3_CURRENT_LOW (1 << 1)
// Bit mask for Feeder 5 Phase 3 Current Negative
#define GK_BIN_FEEDER_5_PHASE_3_CURRENT_NEGATIVE (1 << 0)

// Fixed-point scaling for temperature
#define GK_SCALING_FOR_TEMPERATURE 10
// Fixed-point scaling for voltage
#define GK_SCALING_FOR_VOLTAGE 64
// Fixed-point scaling for current
#define GK_SCALING_FOR_CURRENT 8
// Fixed-point scaling for phase angles
#define GK_SCALING_FOR_PHASE_ANGLE 16
// Fixed-point scaling for harmonic quantities
#define GK_SCALING_FOR_HARMONIC_QUANTITIES 327.68

/* =============================================================================
 * Declare exported enumerations (if required)
 * =============================================================================
 */

typedef enum _GK_enuBinaries
{
  // Flags for Unit binaries
  GK_binUnitFlags,
  // Flags for Busbar Voltage binaries
  GK_binBusbarVoltageFlags,
  // Flags for Busbar Current binaries
  GK_binBusbarCurrentFlags,
  // Flags for Feeder 1 binaries
  GK_binFeeder1Flags,
  // Flags for Feeder 2 binaries
  GK_binFeeder2Flags,
  // Flags for Feeder 3 binaries
  GK_binFeeder3Flags,
  // Flags for Feeder 4 binaries
  GK_binFeeder4Flags,
  // Flags for Feeder 5 binaries
  GK_binFeeder5Flags,
  // End of enumeration
  sizeof_GK_enuBinaries
}
// Binary item offsets used to index into the binary data provided by the function
GK_enuBinaries ;

typedef enum _GK_enuAnalogues
{
  // Flags for Unit Message index
  GK_anaUnitMessageIndex,
  // Flags for Unit Number of feeders in use
  GK_anaUnitNumberOfFeedersInUse,
  // Flags for Unit Periodic reporting period
  GK_anaUnitPeriodicReportingPeriod,
  // Flags for Unit Ambient temperature mean
  GK_anaUnitAmbientTemperatureMean,
  // Flags for Unit Ambient temperature maximum
  GK_anaUnitAmbientTemperatureMaximum,
  // Flags for Unit Ambient temperature minimum
  GK_anaUnitAmbientTemperatureMinimum,
  // Flags for Unit Transformer temperature mean
  GK_anaUnitTransformerTemperatureMean,
  // Flags for Unit Transformer temperature maximum
  GK_anaUnitTransformerTemperatureMaximum,
  // Flags for Unit Transformer temperature minimum
  GK_anaUnitTransformerTemperatureMinimum,
  // Flags for Unit Unit temperature mean
  GK_anaUnitUnitTemperatureMean,
  // Flags for Unit Unit temperature maximum
  GK_anaUnitUnitTemperatureMaximum,
  // Flags for Unit Unit temperature minimum
  GK_anaUnitUnitTemperatureMinimum,
  // Flags for Busbar Phase 1 voltage mean
  GK_anaBusbarPhase1VoltageMean,
  // Flags for Busbar Phase 1 voltage maximum
  GK_anaBusbarPhase1VoltageMaximum,
  // Flags for Busbar Phase 1 voltage minimum
  GK_anaBusbarPhase1VoltageMinimum,
  // Flags for Busbar Phase 1 current mean
  GK_anaBusbarPhase1CurrentMean,
  // Flags for Busbar Phase 1 current maximum
  GK_anaBusbarPhase1CurrentMaximum,
  // Flags for Busbar Phase 1 current minimum
  GK_anaBusbarPhase1CurrentMinimum,
  // Flags for Busbar Phase 1 active power mean
  GK_anaBusbarPhase1ActivePowerMean,
  // Flags for Busbar Phase 1 reactive power mean
  GK_anaBusbarPhase1ReactivePowerMean,
  // Flags for Busbar Phase 1 active energy mean
  GK_anaBusbarPhase1ActiveEnergyMean,
  // Flags for Busbar Phase 1 reactive energy mean
  GK_anaBusbarPhase1ReactiveEnergyMean,
  // Flags for Busbar Phase 2 voltage mean
  GK_anaBusbarPhase2VoltageMean,
  // Flags for Busbar Phase 2 voltage maximum
  GK_anaBusbarPhase2VoltageMaximum,
  // Flags for Busbar Phase 2 voltage minimum
  GK_anaBusbarPhase2VoltageMinimum,
  // Flags for Busbar Phase 2 current mean
  GK_anaBusbarPhase2CurrentMean,
  // Flags for Busbar Phase 2 current maximum
  GK_anaBusbarPhase2CurrentMaximum,
  // Flags for Busbar Phase 2 current minimum
  GK_anaBusbarPhase2CurrentMinimum,
  // Flags for Busbar Phase 2 active power mean
  GK_anaBusbarPhase2ActivePowerMean,
  // Flags for Busbar Phase 2 reactive power mean
  GK_anaBusbarPhase2ReactivePowerMean,
  // Flags for Busbar Phase 2 active energy mean
  GK_anaBusbarPhase2ActiveEnergyMean,
  // Flags for Busbar Phase 2 reactive energy mean
  GK_anaBusbarPhase2ReactiveEnergyMean,
  // Flags for Busbar Phase 3 voltage mean
  GK_anaBusbarPhase3VoltageMean,
  // Flags for Busbar Phase 3 voltage maximum
  GK_anaBusbarPhase3VoltageMaximum,
  // Flags for Busbar Phase 3 voltage minimum
  GK_anaBusbarPhase3VoltageMinimum,
  // Flags for Busbar Phase 3 current mean
  GK_anaBusbarPhase3CurrentMean,
  // Flags for Busbar Phase 3 current maximum
  GK_anaBusbarPhase3CurrentMaximum,
  // Flags for Busbar Phase 3 current minimum
  GK_anaBusbarPhase3CurrentMinimum,
  // Flags for Busbar Phase 3 active power mean
  GK_anaBusbarPhase3ActivePowerMean,
  // Flags for Busbar Phase 3 reactive power mean
  GK_anaBusbarPhase3ReactivePowerMean,
  // Flags for Busbar Phase 3 active energy mean
  GK_anaBusbarPhase3ActiveEnergyMean,
  // Flags for Busbar Phase 3 reactive energy mean
  GK_anaBusbarPhase3ReactiveEnergyMean,
  // Flags for Busbar Neutral current mean
  GK_anaBusbarNeutralCurrentMean,
  // Flags for Busbar Neutral current maximum
  GK_anaBusbarNeutralCurrentMaximum,
  // Flags for Busbar Neutral current minimum
  GK_anaBusbarNeutralCurrentMinimum,
  // Flags for Feeder 1 Phase 1 current mean
  GK_anaFeeder1Phase1CurrentMean,
  // Flags for Feeder 1 Phase 1 current maximum
  GK_anaFeeder1Phase1CurrentMaximum,
  // Flags for Feeder 1 Phase 1 current minimum
  GK_anaFeeder1Phase1CurrentMinimum,
  // Flags for Feeder 1 Phase 1 current phase angle
  GK_anaFeeder1Phase1CurrentPhaseAngle,
  // Flags for Feeder 1 Phase 1 active power mean
  GK_anaFeeder1Phase1ActivePowerMean,
  // Flags for Feeder 1 Phase 1 reactive power mean
  GK_anaFeeder1Phase1ReactivePowerMean,
  // Flags for Feeder 1 Phase 1 active energy mean
  GK_anaFeeder1Phase1ActiveEnergyMean,
  // Flags for Feeder 1 Phase 1 reactive energy mean
  GK_anaFeeder1Phase1ReactiveEnergyMean,
  // Flags for Feeder 1 Phase 1 active harmonic content
  GK_anaFeeder1Phase1ActiveHarmonicContent,
  // Flags for Feeder 1 Phase 1 reactive harmonic content
  GK_anaFeeder1Phase1ReactiveHarmonicContent,
  // Flags for Feeder 1 Phase 1 total harmonic distortion
  GK_anaFeeder1Phase1TotalHarmonicDistortion,
  // Flags for Feeder 1 Phase 2 current mean
  GK_anaFeeder1Phase2CurrentMean,
  // Flags for Feeder 1 Phase 2 current maximum
  GK_anaFeeder1Phase2CurrentMaximum,
  // Flags for Feeder 1 Phase 2 current minimum
  GK_anaFeeder1Phase2CurrentMinimum,
  // Flags for Feeder 1 Phase 2 current phase angle
  GK_anaFeeder1Phase2CurrentPhaseAngle,
  // Flags for Feeder 1 Phase 2 active power mean
  GK_anaFeeder1Phase2ActivePowerMean,
  // Flags for Feeder 1 Phase 2 reactive power mean
  GK_anaFeeder1Phase2ReactivePowerMean,
  // Flags for Feeder 1 Phase 2 active energy mean
  GK_anaFeeder1Phase2ActiveEnergyMean,
  // Flags for Feeder 1 Phase 2 reactive energy mean
  GK_anaFeeder1Phase2ReactiveEnergyMean,
  // Flags for Feeder 1 Phase 2 active harmonic content
  GK_anaFeeder1Phase2ActiveHarmonicContent,
  // Flags for Feeder 1 Phase 2 reactive harmonic content
  GK_anaFeeder1Phase2ReactiveHarmonicContent,
  // Flags for Feeder 1 Phase 2 total harmonic distortion
  GK_anaFeeder1Phase2TotalHarmonicDistortion,
  // Flags for Feeder 1 Phase 3 current mean
  GK_anaFeeder1Phase3CurrentMean,
  // Flags for Feeder 1 Phase 3 current maximum
  GK_anaFeeder1Phase3CurrentMaximum,
  // Flags for Feeder 1 Phase 3 current minimum
  GK_anaFeeder1Phase3CurrentMinimum,
  // Flags for Feeder 1 Phase 3 current phase angle
  GK_anaFeeder1Phase3CurrentPhaseAngle,
  // Flags for Feeder 1 Phase 3 active power mean
  GK_anaFeeder1Phase3ActivePowerMean,
  // Flags for Feeder 1 Phase 3 reactive power mean
  GK_anaFeeder1Phase3ReactivePowerMean,
  // Flags for Feeder 1 Phase 3 active energy mean
  GK_anaFeeder1Phase3ActiveEnergyMean,
  // Flags for Feeder 1 Phase 3 reactive energy mean
  GK_anaFeeder1Phase3ReactiveEnergyMean,
  // Flags for Feeder 1 Phase 3 active harmonic content
  GK_anaFeeder1Phase3ActiveHarmonicContent,
  // Flags for Feeder 1 Phase 3 reactive harmonic content
  GK_anaFeeder1Phase3ReactiveHarmonicContent,
  // Flags for Feeder 1 Phase 3 total harmonic distortion
  GK_anaFeeder1Phase3TotalHarmonicDistortion,
  // Flags for Feeder 1 Neutral current mean
  GK_anaFeeder1NeutralCurrentMean,
  // Flags for Feeder 1 Neutral current maximum
  GK_anaFeeder1NeutralCurrentMaximum,
  // Flags for Feeder 1 Neutral current minimum
  GK_anaFeeder1NeutralCurrentMinimum,
  // Flags for Feeder 2 Phase 1 current mean
  GK_anaFeeder2Phase1CurrentMean,
  // Flags for Feeder 2 Phase 1 current maximum
  GK_anaFeeder2Phase1CurrentMaximum,
  // Flags for Feeder 2 Phase 1 current minimum
  GK_anaFeeder2Phase1CurrentMinimum,
  // Flags for Feeder 2 Phase 1 current phase angle
  GK_anaFeeder2Phase1CurrentPhaseAngle,
  // Flags for Feeder 2 Phase 1 active power mean
  GK_anaFeeder2Phase1ActivePowerMean,
  // Flags for Feeder 2 Phase 1 reactive power mean
  GK_anaFeeder2Phase1ReactivePowerMean,
  // Flags for Feeder 2 Phase 1 active energy mean
  GK_anaFeeder2Phase1ActiveEnergyMean,
  // Flags for Feeder 2 Phase 1 reactive energy mean
  GK_anaFeeder2Phase1ReactiveEnergyMean,
  // Flags for Feeder 2 Phase 1 active harmonic content
  GK_anaFeeder2Phase1ActiveHarmonicContent,
  // Flags for Feeder 2 Phase 1 reactive harmonic content
  GK_anaFeeder2Phase1ReactiveHarmonicContent,
  // Flags for Feeder 2 Phase 1 total harmonic distortion
  GK_anaFeeder2Phase1TotalHarmonicDistortion,
  // Flags for Feeder 2 Phase 2 current mean
  GK_anaFeeder2Phase2CurrentMean,
  // Flags for Feeder 2 Phase 2 current maximum
  GK_anaFeeder2Phase2CurrentMaximum,
  // Flags for Feeder 2 Phase 2 current minimum
  GK_anaFeeder2Phase2CurrentMinimum,
  // Flags for Feeder 2 Phase 2 current phase angle
  GK_anaFeeder2Phase2CurrentPhaseAngle,
  // Flags for Feeder 2 Phase 2 active power mean
  GK_anaFeeder2Phase2ActivePowerMean,
  // Flags for Feeder 2 Phase 2 reactive power mean
  GK_anaFeeder2Phase2ReactivePowerMean,
  // Flags for Feeder 2 Phase 2 active energy mean
  GK_anaFeeder2Phase2ActiveEnergyMean,
  // Flags for Feeder 2 Phase 2 reactive energy mean
  GK_anaFeeder2Phase2ReactiveEnergyMean,
  // Flags for Feeder 2 Phase 2 active harmonic content
  GK_anaFeeder2Phase2ActiveHarmonicContent,
  // Flags for Feeder 2 Phase 2 reactive harmonic content
  GK_anaFeeder2Phase2ReactiveHarmonicContent,
  // Flags for Feeder 2 Phase 2 total harmonic distortion
  GK_anaFeeder2Phase2TotalHarmonicDistortion,
  // Flags for Feeder 2 Phase 3 current mean
  GK_anaFeeder2Phase3CurrentMean,
  // Flags for Feeder 2 Phase 3 current maximum
  GK_anaFeeder2Phase3CurrentMaximum,
  // Flags for Feeder 2 Phase 3 current minimum
  GK_anaFeeder2Phase3CurrentMinimum,
  // Flags for Feeder 2 Phase 3 current phase angle
  GK_anaFeeder2Phase3CurrentPhaseAngle,
  // Flags for Feeder 2 Phase 3 active power mean
  GK_anaFeeder2Phase3ActivePowerMean,
  // Flags for Feeder 2 Phase 3 reactive power mean
  GK_anaFeeder2Phase3ReactivePowerMean,
  // Flags for Feeder 2 Phase 3 active energy mean
  GK_anaFeeder2Phase3ActiveEnergyMean,
  // Flags for Feeder 2 Phase 3 reactive energy mean
  GK_anaFeeder2Phase3ReactiveEnergyMean,
  // Flags for Feeder 2 Phase 3 active harmonic content
  GK_anaFeeder2Phase3ActiveHarmonicContent,
  // Flags for Feeder 2 Phase 3 reactive harmonic content
  GK_anaFeeder2Phase3ReactiveHarmonicContent,
  // Flags for Feeder 2 Phase 3 total harmonic distortion
  GK_anaFeeder2Phase3TotalHarmonicDistortion,
  // Flags for Feeder 2 Neutral current mean
  GK_anaFeeder2NeutralCurrentMean,
  // Flags for Feeder 2 Neutral current maximum
  GK_anaFeeder2NeutralCurrentMaximum,
  // Flags for Feeder 2 Neutral current minimum
  GK_anaFeeder2NeutralCurrentMinimum,
  // Flags for Feeder 3 Phase 1 current mean
  GK_anaFeeder3Phase1CurrentMean,
  // Flags for Feeder 3 Phase 1 current maximum
  GK_anaFeeder3Phase1CurrentMaximum,
  // Flags for Feeder 3 Phase 1 current minimum
  GK_anaFeeder3Phase1CurrentMinimum,
  // Flags for Feeder 3 Phase 1 current phase angle
  GK_anaFeeder3Phase1CurrentPhaseAngle,
  // Flags for Feeder 3 Phase 1 active power mean
  GK_anaFeeder3Phase1ActivePowerMean,
  // Flags for Feeder 3 Phase 1 reactive power mean
  GK_anaFeeder3Phase1ReactivePowerMean,
  // Flags for Feeder 3 Phase 1 active energy mean
  GK_anaFeeder3Phase1ActiveEnergyMean,
  // Flags for Feeder 3 Phase 1 reactive energy mean
  GK_anaFeeder3Phase1ReactiveEnergyMean,
  // Flags for Feeder 3 Phase 1 active harmonic content
  GK_anaFeeder3Phase1ActiveHarmonicContent,
  // Flags for Feeder 3 Phase 1 reactive harmonic content
  GK_anaFeeder3Phase1ReactiveHarmonicContent,
  // Flags for Feeder 3 Phase 1 total harmonic distortion
  GK_anaFeeder3Phase1TotalHarmonicDistortion,
  // Flags for Feeder 3 Phase 2 current mean
  GK_anaFeeder3Phase2CurrentMean,
  // Flags for Feeder 3 Phase 2 current maximum
  GK_anaFeeder3Phase2CurrentMaximum,
  // Flags for Feeder 3 Phase 2 current minimum
  GK_anaFeeder3Phase2CurrentMinimum,
  // Flags for Feeder 3 Phase 2 current phase angle
  GK_anaFeeder3Phase2CurrentPhaseAngle,
  // Flags for Feeder 3 Phase 2 active power mean
  GK_anaFeeder3Phase2ActivePowerMean,
  // Flags for Feeder 3 Phase 2 reactive power mean
  GK_anaFeeder3Phase2ReactivePowerMean,
  // Flags for Feeder 3 Phase 2 active energy mean
  GK_anaFeeder3Phase2ActiveEnergyMean,
  // Flags for Feeder 3 Phase 2 reactive energy mean
  GK_anaFeeder3Phase2ReactiveEnergyMean,
  // Flags for Feeder 3 Phase 2 active harmonic content
  GK_anaFeeder3Phase2ActiveHarmonicContent,
  // Flags for Feeder 3 Phase 2 reactive harmonic content
  GK_anaFeeder3Phase2ReactiveHarmonicContent,
  // Flags for Feeder 3 Phase 2 total harmonic distortion
  GK_anaFeeder3Phase2TotalHarmonicDistortion,
  // Flags for Feeder 3 Phase 3 current mean
  GK_anaFeeder3Phase3CurrentMean,
  // Flags for Feeder 3 Phase 3 current maximum
  GK_anaFeeder3Phase3CurrentMaximum,
  // Flags for Feeder 3 Phase 3 current minimum
  GK_anaFeeder3Phase3CurrentMinimum,
  // Flags for Feeder 3 Phase 3 current phase angle
  GK_anaFeeder3Phase3CurrentPhaseAngle,
  // Flags for Feeder 3 Phase 3 active power mean
  GK_anaFeeder3Phase3ActivePowerMean,
  // Flags for Feeder 3 Phase 3 reactive power mean
  GK_anaFeeder3Phase3ReactivePowerMean,
  // Flags for Feeder 3 Phase 3 active energy mean
  GK_anaFeeder3Phase3ActiveEnergyMean,
  // Flags for Feeder 3 Phase 3 reactive energy mean
  GK_anaFeeder3Phase3ReactiveEnergyMean,
  // Flags for Feeder 3 Phase 3 active harmonic content
  GK_anaFeeder3Phase3ActiveHarmonicContent,
  // Flags for Feeder 3 Phase 3 reactive harmonic content
  GK_anaFeeder3Phase3ReactiveHarmonicContent,
  // Flags for Feeder 3 Phase 3 total harmonic distortion
  GK_anaFeeder3Phase3TotalHarmonicDistortion,
  // Flags for Feeder 3 Neutral current mean
  GK_anaFeeder3NeutralCurrentMean,
  // Flags for Feeder 3 Neutral current maximum
  GK_anaFeeder3NeutralCurrentMaximum,
  // Flags for Feeder 3 Neutral current minimum
  GK_anaFeeder3NeutralCurrentMinimum,
  // Flags for Feeder 4 Phase 1 current mean
  GK_anaFeeder4Phase1CurrentMean,
  // Flags for Feeder 4 Phase 1 current maximum
  GK_anaFeeder4Phase1CurrentMaximum,
  // Flags for Feeder 4 Phase 1 current minimum
  GK_anaFeeder4Phase1CurrentMinimum,
  // Flags for Feeder 4 Phase 1 current phase angle
  GK_anaFeeder4Phase1CurrentPhaseAngle,
  // Flags for Feeder 4 Phase 1 active power mean
  GK_anaFeeder4Phase1ActivePowerMean,
  // Flags for Feeder 4 Phase 1 reactive power mean
  GK_anaFeeder4Phase1ReactivePowerMean,
  // Flags for Feeder 4 Phase 1 active energy mean
  GK_anaFeeder4Phase1ActiveEnergyMean,
  // Flags for Feeder 4 Phase 1 reactive energy mean
  GK_anaFeeder4Phase1ReactiveEnergyMean,
  // Flags for Feeder 4 Phase 1 active harmonic content
  GK_anaFeeder4Phase1ActiveHarmonicContent,
  // Flags for Feeder 4 Phase 1 reactive harmonic content
  GK_anaFeeder4Phase1ReactiveHarmonicContent,
  // Flags for Feeder 4 Phase 1 total harmonic distortion
  GK_anaFeeder4Phase1TotalHarmonicDistortion,
  // Flags for Feeder 4 Phase 2 current mean
  GK_anaFeeder4Phase2CurrentMean,
  // Flags for Feeder 4 Phase 2 current maximum
  GK_anaFeeder4Phase2CurrentMaximum,
  // Flags for Feeder 4 Phase 2 current minimum
  GK_anaFeeder4Phase2CurrentMinimum,
  // Flags for Feeder 4 Phase 2 current phase angle
  GK_anaFeeder4Phase2CurrentPhaseAngle,
  // Flags for Feeder 4 Phase 2 active power mean
  GK_anaFeeder4Phase2ActivePowerMean,
  // Flags for Feeder 4 Phase 2 reactive power mean
  GK_anaFeeder4Phase2ReactivePowerMean,
  // Flags for Feeder 4 Phase 2 active energy mean
  GK_anaFeeder4Phase2ActiveEnergyMean,
  // Flags for Feeder 4 Phase 2 reactive energy mean
  GK_anaFeeder4Phase2ReactiveEnergyMean,
  // Flags for Feeder 4 Phase 2 active harmonic content
  GK_anaFeeder4Phase2ActiveHarmonicContent,
  // Flags for Feeder 4 Phase 2 reactive harmonic content
  GK_anaFeeder4Phase2ReactiveHarmonicContent,
  // Flags for Feeder 4 Phase 2 total harmonic distortion
  GK_anaFeeder4Phase2TotalHarmonicDistortion,
  // Flags for Feeder 4 Phase 3 current mean
  GK_anaFeeder4Phase3CurrentMean,
  // Flags for Feeder 4 Phase 3 current maximum
  GK_anaFeeder4Phase3CurrentMaximum,
  // Flags for Feeder 4 Phase 3 current minimum
  GK_anaFeeder4Phase3CurrentMinimum,
  // Flags for Feeder 4 Phase 3 current phase angle
  GK_anaFeeder4Phase3CurrentPhaseAngle,
  // Flags for Feeder 4 Phase 3 active power mean
  GK_anaFeeder4Phase3ActivePowerMean,
  // Flags for Feeder 4 Phase 3 reactive power mean
  GK_anaFeeder4Phase3ReactivePowerMean,
  // Flags for Feeder 4 Phase 3 active energy mean
  GK_anaFeeder4Phase3ActiveEnergyMean,
  // Flags for Feeder 4 Phase 3 reactive energy mean
  GK_anaFeeder4Phase3ReactiveEnergyMean,
  // Flags for Feeder 4 Phase 3 active harmonic content
  GK_anaFeeder4Phase3ActiveHarmonicContent,
  // Flags for Feeder 4 Phase 3 reactive harmonic content
  GK_anaFeeder4Phase3ReactiveHarmonicContent,
  // Flags for Feeder 4 Phase 3 total harmonic distortion
  GK_anaFeeder4Phase3TotalHarmonicDistortion,
  // Flags for Feeder 4 Neutral current mean
  GK_anaFeeder4NeutralCurrentMean,
  // Flags for Feeder 4 Neutral current maximum
  GK_anaFeeder4NeutralCurrentMaximum,
  // Flags for Feeder 4 Neutral current minimum
  GK_anaFeeder4NeutralCurrentMinimum,
  // Flags for Feeder 5 Phase 1 current mean
  GK_anaFeeder5Phase1CurrentMean,
  // Flags for Feeder 5 Phase 1 current maximum
  GK_anaFeeder5Phase1CurrentMaximum,
  // Flags for Feeder 5 Phase 1 current minimum
  GK_anaFeeder5Phase1CurrentMinimum,
  // Flags for Feeder 5 Phase 1 current phase angle
  GK_anaFeeder5Phase1CurrentPhaseAngle,
  // Flags for Feeder 5 Phase 1 active power mean
  GK_anaFeeder5Phase1ActivePowerMean,
  // Flags for Feeder 5 Phase 1 reactive power mean
  GK_anaFeeder5Phase1ReactivePowerMean,
  // Flags for Feeder 5 Phase 1 active energy mean
  GK_anaFeeder5Phase1ActiveEnergyMean,
  // Flags for Feeder 5 Phase 1 reactive energy mean
  GK_anaFeeder5Phase1ReactiveEnergyMean,
  // Flags for Feeder 5 Phase 1 active harmonic content
  GK_anaFeeder5Phase1ActiveHarmonicContent,
  // Flags for Feeder 5 Phase 1 reactive harmonic content
  GK_anaFeeder5Phase1ReactiveHarmonicContent,
  // Flags for Feeder 5 Phase 1 total harmonic distortion
  GK_anaFeeder5Phase1TotalHarmonicDistortion,
  // Flags for Feeder 5 Phase 2 current mean
  GK_anaFeeder5Phase2CurrentMean,
  // Flags for Feeder 5 Phase 2 current maximum
  GK_anaFeeder5Phase2CurrentMaximum,
  // Flags for Feeder 5 Phase 2 current minimum
  GK_anaFeeder5Phase2CurrentMinimum,
  // Flags for Feeder 5 Phase 2 current phase angle
  GK_anaFeeder5Phase2CurrentPhaseAngle,
  // Flags for Feeder 5 Phase 2 active power mean
  GK_anaFeeder5Phase2ActivePowerMean,
  // Flags for Feeder 5 Phase 2 reactive power mean
  GK_anaFeeder5Phase2ReactivePowerMean,
  // Flags for Feeder 5 Phase 2 active energy mean
  GK_anaFeeder5Phase2ActiveEnergyMean,
  // Flags for Feeder 5 Phase 2 reactive energy mean
  GK_anaFeeder5Phase2ReactiveEnergyMean,
  // Flags for Feeder 5 Phase 2 active harmonic content
  GK_anaFeeder5Phase2ActiveHarmonicContent,
  // Flags for Feeder 5 Phase 2 reactive harmonic content
  GK_anaFeeder5Phase2ReactiveHarmonicContent,
  // Flags for Feeder 5 Phase 2 total harmonic distortion
  GK_anaFeeder5Phase2TotalHarmonicDistortion,
  // Flags for Feeder 5 Phase 3 current mean
  GK_anaFeeder5Phase3CurrentMean,
  // Flags for Feeder 5 Phase 3 current maximum
  GK_anaFeeder5Phase3CurrentMaximum,
  // Flags for Feeder 5 Phase 3 current minimum
  GK_anaFeeder5Phase3CurrentMinimum,
  // Flags for Feeder 5 Phase 3 current phase angle
  GK_anaFeeder5Phase3CurrentPhaseAngle,
  // Flags for Feeder 5 Phase 3 active power mean
  GK_anaFeeder5Phase3ActivePowerMean,
  // Flags for Feeder 5 Phase 3 reactive power mean
  GK_anaFeeder5Phase3ReactivePowerMean,
  // Flags for Feeder 5 Phase 3 active energy mean
  GK_anaFeeder5Phase3ActiveEnergyMean,
  // Flags for Feeder 5 Phase 3 reactive energy mean
  GK_anaFeeder5Phase3ReactiveEnergyMean,
  // Flags for Feeder 5 Phase 3 active harmonic content
  GK_anaFeeder5Phase3ActiveHarmonicContent,
  // Flags for Feeder 5 Phase 3 reactive harmonic content
  GK_anaFeeder5Phase3ReactiveHarmonicContent,
  // Flags for Feeder 5 Phase 3 total harmonic distortion
  GK_anaFeeder5Phase3TotalHarmonicDistortion,
  // Flags for Feeder 5 Neutral current mean
  GK_anaFeeder5NeutralCurrentMean,
  // Flags for Feeder 5 Neutral current maximum
  GK_anaFeeder5NeutralCurrentMaximum,
  // Flags for Feeder 5 Neutral current minimum
  GK_anaFeeder5NeutralCurrentMinimum,
  // End of enumeration
  sizeof_GK_enuAnalogues
}
// Analogue item offsets used to index into the analogue data provided by the function
GK_enuAnalogues ;

/* =============================================================================
 * Declare exported structures (if required)
 * =============================================================================
 */

/* =============================================================================
 * Declare exported variables (if required)
 * =============================================================================
 */

/* =============================================================================
 * Declare exported functions (if required)
 * =============================================================================
 */

// Translate statistical, alert and timestamp messages received from the MCU520
GK_EXTERN boolean_T GK_TranslateGridkeyMessages (
  time_t SystemTime,
  uint8_T* PacketFromTheMcu,
  uint8_T* PacketToTheMcu,
  uint16_T* PacketLength,
  uint16_T* GridkeyBinaries,
  real32_T* GridkeyAnalogues) ;

#ifdef MCUSIMULATOR

GK_EXTERN uint8_T GK_SimulateMcu520 (
  boolean_T UseAddressOffsets,
  uint8_T AlarmCategory,
  boolean_T AssertL1VoltLowLow,
  boolean_T AssertL1VoltLow,
  boolean_T AssertL1VoltHigh,
  boolean_T AssertL1VoltHighHigh,
  boolean_T AssertL2VoltLowLow,
  boolean_T AssertL2VoltLow,
  boolean_T AssertL2VoltHigh,
  boolean_T AssertL2VoltHighHigh,
  boolean_T AssertL3VoltLowLow,
  boolean_T AssertL3VoltLow,
  boolean_T AssertL3VoltHigh,
  boolean_T AssertL3VoltHighHigh,
  boolean_T AssertL1CurrentHighHigh,
  boolean_T AssertL1CurrentHigh,
  boolean_T AssertL1CurrentLow,
  boolean_T AssertL1CurrentNegative,
  boolean_T AssertL2CurrentHighHigh,
  boolean_T AssertL2CurrentHigh,
  boolean_T AssertL2CurrentLow,
  boolean_T AssertL2CurrentNegative,
  boolean_T AssertL3CurrentHighHigh,
  boolean_T AssertL3CurrentHigh,
  boolean_T AssertL3CurrentLow,
  boolean_T AssertL3CurrentNegative,
  uint8_T* PacketFromTheMcu) ;

#endif

#endif // defined GK_Gk2g3Interface_H

/*
 * =============================================================================
 * End of %PM%
 * Classification: Commercial-In-Confidence
 * =============================================================================
 */

