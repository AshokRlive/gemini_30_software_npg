/*
 * =============================================================================
 *
 * Classification: Commercial-In-Confidence
 *
 * Name:           %PM%
 * Revision:       %PR%
 * Current Status: %PS%
 *
 * Project:        GK2G3
 * Title:          GK2G3 Interface (GK23000066)
 * Author(s):      Dave Everett
 * Modified By:    %AUTHOR%
 *
 * Copyright Notice
 * ----------------
 *
 * The information contained in this document is proprietary to Lucy Electric
 * unless stated otherwise and is made available in confidence; it must not be
 * used or disclosed without the express written permission Lucy Electric. This
 * document may not be copied in whole or in part in any form without the
 *
 * Public Access: Freedom Of Information Act 2000
 *
 * This document contains trade secrets and/or sensitive commercial information
 * as of the date provided to the original recipient by Lucy Electric and is
 * provided in confidence. Following a request for this information public
 * authorities are to consult with Lucy Electric regarding the current
 * releasability of the information prior to the decision to release all or
 * part of this document. Release of this information by a public authority may
 *
 * UK Origin
 *
 * Lucy Electric Limited
 * Christopher Martin Road Basildon Essex SS14 3EL England
 * Telephone: 01268 522822 Facsimile: 01268 883140
 *
 * =============================================================================
 * Description
 * -----------------------------------------------------------------------------
 *
 * This file contains the interface functions to facilitate the conversion of a
 * GridKey statistical report into a Gemini 3 Modbus memory map.
 *
 *
 * =============================================================================
 * Version History
 * -----------------------------------------------------------------------------
 *
 * %PRT%
 * %PL%
 *
 * =============================================================================
 */

#define GK_Gk2G3Interface_C

/* =============================================================================
 * Include the project-wide headers and definitions
 * =============================================================================
 */

#include <rtwtypes.h>

/* =============================================================================
 * Resolve external references for this file (if required)
 * =============================================================================
 */

#include <time.h>
#include <stdio.h>
#include <string.h>

#include "PeriodicReportBusGeneratedStructs.h"
#include "AlertReportBusGeneratedStructs.h"

#include "GK_Gk2G3Translator.h"

/* =============================================================================
 * Declare module-specific macros (if required)
 * =============================================================================
 */

// Model reference for AlarmCategoryFeeder1
#define ALARM_CAT_FEEDER_1 1
// Model reference for AlarmCategoryFeeder2
#define ALARM_CAT_FEEDER_2 2
// Model reference for AlarmCategoryFeeder3
#define ALARM_CAT_FEEDER_3 3
// Model reference for AlarmCategoryFeeder4
#define ALARM_CAT_FEEDER_4 4
// Model reference for AlarmCategoryFeeder5
#define ALARM_CAT_FEEDER_5 5
// Model reference for AlarmCategoryBusbar
#define ALARM_CAT_BUSBAR 6

// Model reference for BusbarL1LowVolt1 
#define ALARM_REF_BUSBAR_L1_VOLT_LOW 0
// Model reference for BusbarL1LowVolt2 
#define ALARM_REF_BUSBAR_L1_VOLT_LOW_LOW 1
// Model reference for BusbarL1HighVolt1 
#define ALARM_REF_BUSBAR_L1_VOLT_HIGH 2
// Model reference for BusbarL1HighVolt2 
#define ALARM_REF_BUSBAR_L1_VOLT_HIGH_HIGH 3
// Model reference for BusbarL1LowCurrent 
#define ALARM_REF_BUSBAR_L1_CURRENT_LOW 4
// Model reference for BusbarL1HighCurrent1 
#define ALARM_REF_BUSBAR_L1_CURRENT_HIGH 5
// Model reference for BusbarL1HighCurrent2 
#define ALARM_REF_BUSBAR_L1_CURRENT_HIGH_HIGH 6
// Model reference for BusbarL1NegCurrent 
#define ALARM_REF_BUSBAR_L1_CURRENT_NEGATIVE 7
// Model reference for BusbarL2LowVolt1 
#define ALARM_REF_BUSBAR_L2_VOLT_LOW 8
// Model reference for BusbarL2LowVolt2 
#define ALARM_REF_BUSBAR_L2_VOLT_LOW_LOW 9
// Model reference for BusbarL2HighVolt1 
#define ALARM_REF_BUSBAR_L2_VOLT_HIGH 10
// Model reference for BusbarL2HighVolt2 
#define ALARM_REF_BUSBAR_L2_VOLT_HIGH_HIGH 11
// Model reference for BusbarL2LowCurrent 
#define ALARM_REF_BUSBAR_L2_CURRENT_LOW 12
// Model reference for BusbarL2HighCurrent1 
#define ALARM_REF_BUSBAR_L2_CURRENT_HIGH 13
// Model reference for BusbarL2HighCurrent2 
#define ALARM_REF_BUSBAR_L2_CURRENT_HIGH_HIGH 14
// Model reference for BusbarL2NegCurrent 
#define ALARM_REF_BUSBAR_L2_CURRENT_NEGATIVE 15
// Model reference for BusbarL3LowVolt1 
#define ALARM_REF_BUSBAR_L3_VOLT_LOW 16
// Model reference for BusbarL3LowVolt2 
#define ALARM_REF_BUSBAR_L3_VOLT_LOW_LOW 17
// Model reference for BusbarL3HighVolt1 
#define ALARM_REF_BUSBAR_L3_VOLT_HIGH 18
// Model reference for BusbarL3HighVolt2 
#define ALARM_REF_BUSBAR_L3_VOLT_HIGH_HIGH 19
// Model reference for BusbarL3LowCurrent 
#define ALARM_REF_BUSBAR_L3_CURRENT_LOW 20
// Model reference for BusbarL3HighCurrent1 
#define ALARM_REF_BUSBAR_L3_CURRENT_HIGH 21
// Model reference for BusbarL3HighCurrent2 
#define ALARM_REF_BUSBAR_L3_CURRENT_HIGH_HIGH 22
// Model reference for BusbarL3NegCurrent 
#define ALARM_REF_BUSBAR_L3_CURRENT_NEGATIVE 23

// Model reference for FeederL1LowCurrent 
#define ALARM_REF_FEEDER_L1_CURRENT_LOW 0
// Model reference for FeederL1HighCurrent1 
#define ALARM_REF_FEEDER_L1_CURRENT_HIGH 1
// Model reference for FeederL1HighCurrent2 
#define ALARM_REF_FEEDER_L1_CURRENT_HIGH_HIGH 2
// Model reference for FeederL1NegCurrent 
#define ALARM_REF_FEEDER_L1_CURRENT_NEGATIVE 3
// Model reference for FeederL2LowCurrent 
#define ALARM_REF_FEEDER_L2_CURRENT_LOW 4
// Model reference for FeederL2HighCurrent1 
#define ALARM_REF_FEEDER_L2_CURRENT_HIGH 5
// Model reference for FeederL2HighCurrent2 
#define ALARM_REF_FEEDER_L2_CURRENT_HIGH_HIGH 6
// Model reference for FeederL2NegCurrent 
#define ALARM_REF_FEEDER_L2_CURRENT_NEGATIVE 7
// Model reference for FeederL3LowCurrent 
#define ALARM_REF_FEEDER_L3_CURRENT_LOW 8
// Model reference for FeederL3HighCurrent1 
#define ALARM_REF_FEEDER_L3_CURRENT_HIGH 9
// Model reference for FeederL3HighCurrent2 
#define ALARM_REF_FEEDER_L3_CURRENT_HIGH_HIGH 10
// Model reference for FeederL3NegCurrent 
#define ALARM_REF_FEEDER_L3_CURRENT_NEGATIVE 11
// Model reference for FeederNeutralMismatch 
#define cFeederNeutralMismatch 12

/* =============================================================================
 * Declare module-specific enumerations (if required)
 * =============================================================================
 */

/* =============================================================================
 * Declare module-specific structures (if required)
 * =============================================================================
 */

typedef struct _strUnitBinaries
{
  // Unit Alarm asserted flag: Indicates that an alarm is set from the busbar or feeder measurements
  uint16_T AlarmAsserted: 1 ;
  // Unused bit 14
  uint16_T Unused14: 1 ;
  // Unused bit 13
  uint16_T Unused13: 1 ;
  // Unused bit 12
  uint16_T Unused12: 1 ;
  // Unused bit 11
  uint16_T Unused11: 1 ;
  // Unused bit 10
  uint16_T Unused10: 1 ;
  // Unused bit 9
  uint16_T Unused9: 1 ;
  // Unused bit 8
  uint16_T Unused8: 1 ;
  // Unused bit 7
  uint16_T Unused7: 1 ;
  // Unit Data centre comms active flag: Indicates that the unit has a connection to the GridKey data centre
  uint16_T DataCentreCommsActive: 1 ;
  // Unit Feeder 5 online flag: Indicates that measurements are being generated for feeder 1
  uint16_T Feeder5Online: 1 ;
  // Unit Feeder 4 online flag: Indicates that measurements are being generated for feeder 2
  uint16_T Feeder4Online: 1 ;
  // Unit Feeder 3 online flag: Indicates that measurements are being generated for feeder 3
  uint16_T Feeder3Online: 1 ;
  // Unit Feeder 2 online flag: Indicates that measurements are being generated for feeder 4
  uint16_T Feeder2Online: 1 ;
  // Unit Feeder 1 online flag: Indicates that measurements are being generated for feeder 5
  uint16_T Feeder1Online: 1 ;
  // Unit MCU520 online flag: Indicates that the MCU520 is communicating with the Gemini-3
  uint16_T Mcu520Online: 1 ;
}
// Structure to encode/decode the Unit binary flags
strUnitBinaries ;

typedef struct _strBusbarVoltageBinaries
{
  // Busbar Voltage Alarm asserted flag: Indicates that an alarm is set from the busbar voltage voltage measurements
  uint16_T AlarmAsserted: 1 ;
  // Busbar Voltage Phase 1 alarm flag: Indicates that a busbar voltage phase 1 alarm is set
  uint16_T Phase1Alarm: 1 ;
  // Busbar Voltage Phase 2 alarm flag: Indicates that a busbar voltage phase 2 alarm is set
  uint16_T Phase2Alarm: 1 ;
  // Busbar Voltage Phase 3 alarm flag: Indicates that a busbar voltage phase 3 alarm is set
  uint16_T Phase3Alarm: 1 ;
  // Busbar Voltage Phase 1 voltage high-high flag: Indicates that a busbar voltage phase 1 voltage high-high has breached its highest alarm threshold point
  uint16_T Phase1VoltageHighHigh: 1 ;
  // Busbar Voltage Phase 1 voltage high flag: Indicates that a busbar voltage phase 1 voltage high has breached its high alarm threshold point
  uint16_T Phase1VoltageHigh: 1 ;
  // Busbar Voltage Phase 1 voltage low flag: Indicates that a busbar voltage phase 1 voltage low has breached its low alarm threshold point
  uint16_T Phase1VoltageLow: 1 ;
  // Busbar Voltage Phase 1 voltage low-low flag: Indicates that a busbar voltage phase 1 voltage low-low has breached its lowest alarm threshold point
  uint16_T Phase1VoltageLowLow: 1 ;
  // Busbar Voltage Phase 2 voltage high-high flag: Indicates that a busbar voltage phase 2 voltage high-high has breached its highest alarm threshold point
  uint16_T Phase2VoltageHighHigh: 1 ;
  // Busbar Voltage Phase 2 voltage high flag: Indicates that a busbar voltage phase 2 voltage high has breached its high alarm threshold point
  uint16_T Phase2VoltageHigh: 1 ;
  // Busbar Voltage Phase 2 voltage low flag: Indicates that a busbar voltage phase 2 voltage low has breached its low alarm threshold point
  uint16_T Phase2VoltageLow: 1 ;
  // Busbar Voltage Phase 2 voltage low-low flag: Indicates that a busbar voltage phase 2 voltage low-low has breached its lowest alarm threshold point
  uint16_T Phase2VoltageLowLow: 1 ;
  // Busbar Voltage Phase 3 voltage high-high flag: Indicates that a busbar voltage phase 3 voltage high-high has breached its highest alarm threshold point
  uint16_T Phase3VoltageHighHigh: 1 ;
  // Busbar Voltage Phase 3 voltage high flag: Indicates that a busbar voltage phase 3 voltage high has breached its high alarm threshold point
  uint16_T Phase3VoltageHigh: 1 ;
  // Busbar Voltage Phase 3 voltage low flag: Indicates that a busbar voltage phase 3 voltage low has breached its low alarm threshold point
  uint16_T Phase3VoltageLow: 1 ;
  // Busbar Voltage Phase 3 voltage low-low flag: Indicates that a busbar voltage phase 3 voltage low-low has breached its lowest alarm threshold point
  uint16_T Phase3VoltageLowLow: 1 ;
}
// Structure to encode/decode the Busbar Voltage binary flags
strBusbarVoltageBinaries ;

typedef struct _strBusbarCurrentBinaries
{
  // Busbar Current Alarm asserted flag: Indicates that an alarm is set from the busbar current current measurements
  uint16_T AlarmAsserted: 1 ;
  // Busbar Current Phase 1 alarm flag: Indicates that a busbar current phase 1 alarm is set
  uint16_T Phase1Alarm: 1 ;
  // Busbar Current Phase 2 alarm flag: Indicates that a busbar current phase 2 alarm is set
  uint16_T Phase2Alarm: 1 ;
  // Busbar Current Phase 3 alarm flag: Indicates that a busbar current phase 3 alarm is set
  uint16_T Phase3Alarm: 1 ;
  // Busbar Current Phase 1 Current High-High flag: Indicates that a busbar current phase 1 current high-high has breached its highest alarm threshold point
  uint16_T Phase1CurrentHighHigh: 1 ;
  // Busbar Current Phase 1 Current High flag: Indicates that a busbar current phase 1 current high has breached its high alarm threshold point
  uint16_T Phase1CurrentHigh: 1 ;
  // Busbar Current Phase 1 Current Low flag: Indicates that a busbar current phase 1 current low has breached its low alarm threshold point
  uint16_T Phase1CurrentLow: 1 ;
  // Busbar Current Phase 1 Current Negative flag: Indicates that a busbar current phase 1 current negative has breached its negative alarm threshold point
  uint16_T Phase1CurrentNegative: 1 ;
  // Busbar Current Phase 2 Current High-High flag: Indicates that a busbar current phase 2 current high-high has breached its highest alarm threshold point
  uint16_T Phase2CurrentHighHigh: 1 ;
  // Busbar Current Phase 2 Current High flag: Indicates that a busbar current phase 2 current high has breached its high alarm threshold point
  uint16_T Phase2CurrentHigh: 1 ;
  // Busbar Current Phase 2 Current Low flag: Indicates that a busbar current phase 2 current low has breached its low alarm threshold point
  uint16_T Phase2CurrentLow: 1 ;
  // Busbar Current Phase 2 Current Negative flag: Indicates that a busbar current phase 2 current negative has breached its negative alarm threshold point
  uint16_T Phase2CurrentNegative: 1 ;
  // Busbar Current Phase 3 Current High-High flag: Indicates that a busbar current phase 3 current high-high has breached its highest alarm threshold point
  uint16_T Phase3CurrentHighHigh: 1 ;
  // Busbar Current Phase 3 Current High flag: Indicates that a busbar current phase 3 current high has breached its high alarm threshold point
  uint16_T Phase3CurrentHigh: 1 ;
  // Busbar Current Phase 3 Current Low flag: Indicates that a busbar current phase 3 current low has breached its low alarm threshold point
  uint16_T Phase3CurrentLow: 1 ;
  // Busbar Current Phase 3 Current Negative flag: Indicates that a busbar current phase 3 current negative has breached its lowest alarm threshold point
  uint16_T Phase3CurrentNegative: 1 ;
}
// Structure to encode/decode the Busbar Current binary flags
strBusbarCurrentBinaries ;

typedef struct _strFeeder1Binaries
{
  // Feeder 1 Alarm asserted flag: Indicates that an alarm is set from the feeder 1 current measurements
  uint16_T AlarmAsserted: 1 ;
  // Feeder 1 Phase 1 alarm flag: Indicates that a feeder 1 phase 1 alarm is set
  uint16_T Phase1Alarm: 1 ;
  // Feeder 1 Phase 2 alarm flag: Indicates that a feeder 1 phase 2 alarm is set
  uint16_T Phase2Alarm: 1 ;
  // Feeder 1 Phase 3 alarm flag: Indicates that a feeder 1 phase 3 alarm is set
  uint16_T Phase3Alarm: 1 ;
  // Feeder 1 Phase 1 Current High-High flag: Indicates that a feeder 1 phase 1 current high-high has breached its highest alarm threshold point
  uint16_T Phase1CurrentHighHigh: 1 ;
  // Feeder 1 Phase 1 Current High flag: Indicates that a feeder 1 phase 1 current high has breached its high alarm threshold point
  uint16_T Phase1CurrentHigh: 1 ;
  // Feeder 1 Phase 1 Current Low flag: Indicates that a feeder 1 phase 1 current low has breached its low alarm threshold point
  uint16_T Phase1CurrentLow: 1 ;
  // Feeder 1 Phase 1 Current Negative flag: Indicates that a feeder 1 phase 1 current negative has breached its lowest alarm threshold point
  uint16_T Phase1CurrentNegative: 1 ;
  // Feeder 1 Phase 2 Current High-High flag: Indicates that a feeder 1 phase 2 current high-high has breached its highest alarm threshold point
  uint16_T Phase2CurrentHighHigh: 1 ;
  // Feeder 1 Phase 2 Current High flag: Indicates that a feeder 1 phase 2 current high has breached its high alarm threshold point
  uint16_T Phase2CurrentHigh: 1 ;
  // Feeder 1 Phase 2 Current Low flag: Indicates that a feeder 1 phase 2 current low has breached its low alarm threshold point
  uint16_T Phase2CurrentLow: 1 ;
  // Feeder 1 Phase 2 Current Negative flag: Indicates that a feeder 1 phase 2 current negative has breached its lowest alarm threshold point
  uint16_T Phase2CurrentNegative: 1 ;
  // Feeder 1 Phase 3 Current High-High flag: Indicates that a feeder 1 phase 3 current high-high has breached its highest alarm threshold point
  uint16_T Phase3CurrentHighHigh: 1 ;
  // Feeder 1 Phase 3 Current High flag: Indicates that a feeder 1 phase 3 current high has breached its high alarm threshold point
  uint16_T Phase3CurrentHigh: 1 ;
  // Feeder 1 Phase 3 Current Low flag: Indicates that a feeder 1 phase 3 current low has breached its low alarm threshold point
  uint16_T Phase3CurrentLow: 1 ;
  // Feeder 1 Phase 3 Current Negative flag: Indicates that a feeder 1 phase 3 current negative has breached its lowest alarm threshold point
  uint16_T Phase3CurrentNegative: 1 ;
}
// Structure to encode/decode the Feeder 1 binary flags
strFeeder1Binaries ;

typedef struct _strUnitAnalogues
{
  // Unit Message index flag: The message index of the last received dataset
  real32_T MessageIndex ;
  // Unit Number of feeders in use flag: The number of active feeders monitored by the MCU520
  real32_T NumberOfFeedersInUse ;
  // Unit Periodic reporting period flag: 1: 1 minute; 2: 10 minutes; 3: 15 minutes; 4: 20 minutes; 5: 30 minutes; 6: 1 hour; 7: 2 hours; 8: 3 hours; 9: 4 hours; 10: 6 hours; 11: 8 hours; 12: 12 hours; 13: 24 hours
  real32_T PeriodicReportingPeriod ;
  // Unit Ambient temperature mean flag: The ambient temperature mean in the substation
  real32_T AmbientTemperatureMean ;
  // Unit Ambient temperature maximum flag: The ambient temperature maximum in the substation
  real32_T AmbientTemperatureMaximum ;
  // Unit Ambient temperature minimum flag: The ambient temperature minimum in the substation
  real32_T AmbientTemperatureMinimum ;
  // Unit Transformer temperature mean flag: The transformer temperature mean in the substation
  real32_T TransformerTemperatureMean ;
  // Unit Transformer temperature maximum flag: The transformer temperature maximum in the substation
  real32_T TransformerTemperatureMaximum ;
  // Unit Transformer temperature minimum flag: The transformer temperature minimum in the substation
  real32_T TransformerTemperatureMinimum ;
  // Unit Unit temperature mean flag: The unit temperature mean in the substation
  real32_T UnitTemperatureMean ;
  // Unit Unit temperature maximum flag: The unit temperature maximum in the substation
  real32_T UnitTemperatureMaximum ;
  // Unit Unit temperature minimum flag: The unit temperature minimum in the substation
  real32_T UnitTemperatureMinimum ;
}
// Structure to encode/decode the Unit binary flags
strUnitAnalogues ;

typedef struct _strBusbarPhaseData
{
  // Busbar Phase 1 voltage mean flag: The mean phase 1 voltage at the busbar measured over the reporting period
  real32_T PhaseVoltageMean ;
  // Busbar Phase 1 voltage maximum flag: The maximum phase 1 voltage at the busbar measured over the reporting period
  real32_T PhaseVoltageMaximum ;
  // Busbar Phase 1 voltage minimum flag: The minimum phase 1 voltage at the busbar measured over the reporting period
  real32_T PhaseVoltageMinimum ;
  // Busbar Phase 1 current mean flag: The mean phase 1 current at the busbar measured over the reporting period
  real32_T PhaseCurrentMean ;
  // Busbar Phase 1 current maximum flag: The maximum phase 1 current at the busbar measured over the reporting period
  real32_T PhaseCurrentMaximum ;
  // Busbar Phase 1 current minimum flag: The minimum phase 1 current at the busbar measured over the reporting period
  real32_T PhaseCurrentMinimum ;
  // Busbar Phase 1 active power mean flag: The mean phase 1 active power at the busbar measured over the reporting period
  real32_T PhaseActivePowerMean ;
  // Busbar Phase 1 reactive power mean flag: The mean phase 1 reactive power at the busbar measured over the reporting period
  real32_T PhaseReactivePowerMean ;
  // Busbar Phase 1 active energy flag: The phase 1 active energy at the busbar measured over the reporting period
  real32_T PhaseActiveEnergy ;
  // Busbar Phase 1 reactive energy flag: The phase 1 reactive energy at the busbar measured over the reporting period
  real32_T PhaseReactiveEnergy ;
}
// Structure to encode/decode the Busbar binary flags
strBusbarPhaseData ;

typedef struct _strBusbarAnalogues
{
  // Phase-specific Busbar data
  strBusbarPhaseData BusbarPhase1Data ;
  // Phase-specific Busbar data
  strBusbarPhaseData BusbarPhase2Data ;
  // Phase-specific Busbar data
  strBusbarPhaseData BusbarPhase3Data ;
  // Busbar Neutral current mean flag: The mean neutral current at the busbar measured over the reporting period
  real32_T NeutralCurrentMean ;
  // Busbar Neutral current maximum flag: The maximum neutral current at the busbar measured over the reporting period
  real32_T NeutralCurrentMaximum ;
  // Busbar Neutral current minimum flag: The minimum neutral current at the busbar measured over the reporting period
  real32_T NeutralCurrentMinimum ;
}
// Structure to encode/decode the Busbar binary flags
strBusbarAnalogues ;

typedef struct _strFeederPhaseData
{
  // Feeder 1 Phase 1 current mean flag: The mean phase 1 current at feeder 1 measured over the reporting period
  real32_T PhaseCurrentMean ;
  // Feeder 1 Phase 1 current maximum flag: The maximum phase 1 current at feeder 1 measured over the reporting period
  real32_T PhaseCurrentMaximum ;
  // Feeder 1 Phase 1 current minimum flag: The minimum phase 1 current at feeder 1 measured over the reporting period
  real32_T PhaseCurrentMinimum ;
  // Feeder 1 Phase 1 current phase angle flag: The phase angle between phase 1 current and voltage at feeder 1 measured over the reporting period
  real32_T PhaseCurrentPhaseAngle ;
  // Feeder 1 Phase 1 active power mean flag: The mean phase 1 active power at feeder 1 measured over the reporting period
  real32_T PhaseActivePowerMean ;
  // Feeder 1 Phase 1 reactive power mean flag: The mean phase 1 reactive power at feeder 1 measured over the reporting period
  real32_T PhaseReactivePowerMean ;
  // Feeder 1 Phase 1 active energy flag: The phase 1 active energy at feeder 1 measured over the reporting period
  real32_T PhaseActiveEnergy ;
  // Feeder 1 Phase 1 reactive energy flag: The phase 1 reactive energy at feeder 1 measured over the reporting period
  real32_T PhaseReactiveEnergy ;
  // Feeder 1 Phase 1 active harmonic content flag: The phase 1 active harmonic content at feeder 1 measured over the reporting period
  real32_T PhaseActiveHarmonicContent ;
  // Feeder 1 Phase 1 reactive harmonic content flag: The phase 1 reactive harmonic content at feeder 1 measured over the reporting period
  real32_T PhaseReactiveHarmonicContent ;
  // Feeder 1 Phase 1 total harmonic distortion flag: The phase 1 total harmonic distortion at feeder 1 measured over the reporting period
  real32_T PhaseTotalHarmonicDistortion ;
}
// Structure to encode/decode the Feeder 1 binary flags
strFeederPhaseData ;

typedef struct _strFeeder1Analogues
{
  // Phase-specific Feeder 1 data
  strFeederPhaseData FeederPhase1Data ;
  // Phase-specific Feeder 1 data
  strFeederPhaseData FeederPhase2Data ;
  // Phase-specific Feeder 1 data
  strFeederPhaseData FeederPhase3Data ;
  // Feeder 1 Neutral current mean flag: The mean neutral current at feeder 1 measured over the reporting period
  real32_T NeutralCurrentMean ;
  // Feeder 1 Neutral current maximum flag: The maximum neutral current at feeder 1 measured over the reporting period
  real32_T NeutralCurrentMaximum ;
  // Feeder 1 Neutral current minimum flag: The minimum neutral current at feeder 1 measured over the reporting period
  real32_T NeutralCurrentMinimum ;
}
// Structure to encode/decode the Feeder 1 binary flags
strFeeder1Analogues ;

typedef struct _strAnalogues
{
  // Unit-specific analogues
  strUnitAnalogues UnitAnalogues ;
  // Busbar-specific analogues
  strBusbarAnalogues BusbarAnalogues ;
  // Feeder 1 analogues
  strFeeder1Analogues Feeder1Analogues ;
  // Feeder 2 analogues
  strFeeder1Analogues Feeder2Analogues ;
  // Feeder 3 analogues
  strFeeder1Analogues Feeder3Analogues ;
  // Feeder 4 analogues
  strFeeder1Analogues Feeder4Analogues ;
  // Feeder 5 analogues
  strFeeder1Analogues Feeder5Analogues ;
}
// Overlay for bodbus analogues
strAnalogues ;

/* =============================================================================
 * Declare module variables (if required)
 * =============================================================================
 */

// Local copies of busbar phase 1 voltage flags
static uint16_T BusbarPhase1VoltageFlags = 0 ;
// Local copies of busbar phase 2 voltage flags
static uint16_T BusbarPhase2VoltageFlags = 0 ;
// Local copies of busbar phase 3 voltage flags
static uint16_T BusbarPhase3VoltageFlags = 0 ;
// Local copies of busbar phase 1 current flags
static uint16_T BusbarPhase1CurrentFlags = 0 ;
// Local copies of busbar phase 2 current flags
static uint16_T BusbarPhase2CurrentFlags = 0 ;
// Local copies of busbar phase 3 current flags
static uint16_T BusbarPhase3CurrentFlags = 0 ;
// Local copies of feeder 1 phase 1 current flags
static uint16_T Feeder1Phase1CurrentFlags = 0 ;
// Local copies of feeder 1 phase 2 current flags
static uint16_T Feeder1Phase2CurrentFlags = 0 ;
// Local copies of feeder 1 phase 3 current flags
static uint16_T Feeder1Phase3CurrentFlags = 0 ;
// Local copies of feeder 2 phase 1 current flags
static uint16_T Feeder2Phase1CurrentFlags = 0 ;
// Local copies of feeder 2 phase 2 current flags
static uint16_T Feeder2Phase2CurrentFlags = 0 ;
// Local copies of feeder 2 phase 3 current flags
static uint16_T Feeder2Phase3CurrentFlags = 0 ;
// Local copies of feeder 3 phase 1 current flags
static uint16_T Feeder3Phase1CurrentFlags = 0 ;
// Local copies of feeder 3 phase 2 current flags
static uint16_T Feeder3Phase2CurrentFlags = 0 ;
// Local copies of feeder 3 phase 3 current flags
static uint16_T Feeder3Phase3CurrentFlags = 0 ;
// Local copies of feeder 4 phase 1 current flags
static uint16_T Feeder4Phase1CurrentFlags = 0 ;
// Local copies of feeder 4 phase 2 current flags
static uint16_T Feeder4Phase2CurrentFlags = 0 ;
// Local copies of feeder 4 phase 3 current flags
static uint16_T Feeder4Phase3CurrentFlags = 0 ;
// Local copies of feeder 5 phase 1 current flags
static uint16_T Feeder5Phase1CurrentFlags = 0 ;
// Local copies of feeder 5 phase 2 current flags
static uint16_T Feeder5Phase2CurrentFlags = 0 ;
// Local copies of feeder 5 phase 3 current flags
static uint16_T Feeder5Phase3CurrentFlags = 0 ;

/* =============================================================================
 * Declare module-specific functions (if required)
 * =============================================================================
 */

uint8_T CopyBusbarPhaseAnalogues (
  BusbarPhaseDataBus* GridkeyData,
  strBusbarPhaseData* ModbusData)
{
  uint8_T DecodeSuccess = GK_DECODE_WAS_SUCCESSFUL ;

  ModbusData->PhaseVoltageMean = (real32_T) GridkeyData->MeanVoltage / GK_SCALING_FOR_VOLTAGE ;
  ModbusData->PhaseVoltageMaximum = (real32_T) GridkeyData->MaxVoltage / GK_SCALING_FOR_VOLTAGE ;
  ModbusData->PhaseVoltageMinimum = (real32_T) GridkeyData->MinVoltage / GK_SCALING_FOR_VOLTAGE ;
  ModbusData->PhaseCurrentMean = (real32_T) GridkeyData->MeanCurrent / GK_SCALING_FOR_CURRENT ;
  ModbusData->PhaseCurrentMaximum = (real32_T) GridkeyData->MaxCurrent / GK_SCALING_FOR_CURRENT ;
  ModbusData->PhaseCurrentMinimum = (real32_T) GridkeyData->MinCurrent / GK_SCALING_FOR_CURRENT ;
  ModbusData->PhaseActivePowerMean = (real32_T) GridkeyData->MeanActivePower ;
  ModbusData->PhaseReactivePowerMean = (real32_T) GridkeyData->MeanReactivePower ;
  ModbusData->PhaseActiveEnergy = (real32_T) GridkeyData->ActiveEnergy ;
  ModbusData->PhaseReactiveEnergy = (real32_T) GridkeyData->ReactiveEnergy ;

  // Return the result of the decode operation

  return DecodeSuccess ;
}

uint8_T CopyFeederPhaseAnalogues (
  FeederPhaseDataBus* GridkeyData,
  strFeederPhaseData* ModbusData)
{
  uint8_T DecodeSuccess = GK_DECODE_WAS_SUCCESSFUL ;

  // Copy over the feeder phase data

  ModbusData->PhaseCurrentMean = (real32_T) GridkeyData->MeanCurrent / GK_SCALING_FOR_CURRENT ;
  ModbusData->PhaseCurrentMaximum = (real32_T) GridkeyData->MaxCurrent / GK_SCALING_FOR_CURRENT ;
  ModbusData->PhaseCurrentMinimum = (real32_T) GridkeyData->MinCurrent / GK_SCALING_FOR_CURRENT ;
  ModbusData->PhaseCurrentPhaseAngle = (real32_T) GridkeyData->PhaseAngle / GK_SCALING_FOR_PHASE_ANGLE ;
  ModbusData->PhaseActivePowerMean = (real32_T) GridkeyData->MeanActivePower / GK_SCALING_FOR_PHASE_ANGLE ;
  ModbusData->PhaseReactivePowerMean = (real32_T) GridkeyData->MeanReactivePower / GK_SCALING_FOR_PHASE_ANGLE ;
  ModbusData->PhaseActiveEnergy = (real32_T) GridkeyData->ActiveEnergy ;
  ModbusData->PhaseReactiveEnergy = (real32_T) GridkeyData->ReactiveEnergy ;
  ModbusData->PhaseActiveHarmonicContent = (real32_T) GridkeyData->TotalActiveHarmonicContent / (real32_T) GK_SCALING_FOR_HARMONIC_QUANTITIES ;
  ModbusData->PhaseReactiveHarmonicContent = (real32_T) GridkeyData->TotalReactiveHarmonicContent / (real32_T) GK_SCALING_FOR_HARMONIC_QUANTITIES ;
  ModbusData->PhaseTotalHarmonicDistortion = (real32_T) GridkeyData->TotalHarmonicDistortion / (real32_T) GK_SCALING_FOR_HARMONIC_QUANTITIES ;

  // Return the result of the decode operation

  return DecodeSuccess ;
}

uint8_T CopyFeederNeutralAnalogues (
  FeederDataBus* GridkeyData,
  strFeeder1Analogues* ModbusData)
{
  uint8_T DecodeSuccess = GK_DECODE_WAS_SUCCESSFUL ;

  // Copy over the neutral current data

  ModbusData->NeutralCurrentMaximum = (real32_T) GridkeyData->NeutralMaxMeasuredCurrent / GK_SCALING_FOR_CURRENT ;
  ModbusData->NeutralCurrentMinimum = (real32_T) GridkeyData->NeutralMinMeasuredCurrent / GK_SCALING_FOR_CURRENT ;
  ModbusData->NeutralCurrentMean = (real32_T) GridkeyData->NeutralMeanMeasuredCurrent / GK_SCALING_FOR_CURRENT ;

  // Return the result of the decode operation

  return DecodeSuccess ;
}

uint8_T CopyFeederData (
  uint16_T* UnitFlags,
  FeederDataBus* GridkeyData,
  strAnalogues* ModbusData)
{
  uint8_T DecodeSuccess = GK_DECODE_WAS_SUCCESSFUL ;

  // Work out which set of modbus feeder data we need to update

  strFeeder1Analogues* FeederAnalogues ;

  switch (GridkeyData->FeederRef)
  {
    case 1:
    {
      FeederAnalogues = &ModbusData->Feeder1Analogues ;
      *UnitFlags = *UnitFlags | GK_BIN_UNIT_FEEDER_1_ONLINE ;
    }
    break ;

    case 2:
    {
      FeederAnalogues = &ModbusData->Feeder2Analogues ;
      *UnitFlags = *UnitFlags | GK_BIN_UNIT_FEEDER_2_ONLINE ;
    }
    break ;

    case 3:
    {
      FeederAnalogues = &ModbusData->Feeder3Analogues ;
      *UnitFlags = *UnitFlags | GK_BIN_UNIT_FEEDER_3_ONLINE ;
    }
    break ;

    case 4:
    {
      FeederAnalogues = &ModbusData->Feeder4Analogues ;
      *UnitFlags = *UnitFlags | GK_BIN_UNIT_FEEDER_4_ONLINE ;
    }
    break ;

    case 5:
    {
      FeederAnalogues = &ModbusData->Feeder5Analogues ;
      *UnitFlags = *UnitFlags | GK_BIN_UNIT_FEEDER_5_ONLINE ;
    }
    break ;

    default: FeederAnalogues = NULL ;
  }

  // If we've got something to write to, update the feeder analogues

  if (FeederAnalogues != NULL)
  {
    CopyFeederPhaseAnalogues (
      &GridkeyData->FeederPhaseAData,
      &FeederAnalogues->FeederPhase1Data) ;

    CopyFeederPhaseAnalogues (
      &GridkeyData->FeederPhaseBData,
      &FeederAnalogues->FeederPhase2Data) ;

    CopyFeederPhaseAnalogues (
      &GridkeyData->FeederPhaseCData,
      &FeederAnalogues->FeederPhase3Data) ;

    CopyFeederNeutralAnalogues (
      GridkeyData,
      FeederAnalogues) ;
  }

  // Return the result of the decode operation

  return DecodeSuccess ;
}

uint8_T ProcessStatisticalReport (
  uint8_T* PacketFromTheMcu,
  uint16_T* UnitFlags,
  real32_T* GridkeyAnalogues)
{
  uint8_T DecodeSuccess = GK_DECODE_WAS_SUCCESSFUL ;

  // Map the statistical report structure onto the received message buffer

  PeriodicReportBus* PeriodicReport = (PeriodicReportBus*) PacketFromTheMcu ;

  // Set the MCU online flag in the unit flags

  *UnitFlags = *UnitFlags | GK_BIN_UNIT_MCU520_ONLINE ;

  // Set the DC online bit if the message index is changing (implies no retries)

  static real32_T LastMessageIndex = (real32_T) MAX_int32_T ;

  if (PeriodicReport->FileReference.FileReferenceNumber != LastMessageIndex) 
    *UnitFlags = *UnitFlags | GK_BIN_UNIT_DATA_CENTRE_COMMS_ACTIVE ;

  LastMessageIndex = PeriodicReport->FileReference.FileReferenceNumber ;

  // Map the output structure on the modbus memory map

  strAnalogues* Analogues = (strAnalogues*) GridkeyAnalogues ;

  // Process the busbar data, first the phase-specific measurements...

  CopyBusbarPhaseAnalogues (
    &PeriodicReport->BusbarData.BusbarPhaseAData,
    &Analogues->BusbarAnalogues.BusbarPhase1Data) ;

  CopyBusbarPhaseAnalogues (
    &PeriodicReport->BusbarData.BusbarPhaseBData,
    &Analogues->BusbarAnalogues.BusbarPhase2Data) ;

  CopyBusbarPhaseAnalogues (
    &PeriodicReport->BusbarData.BusbarPhaseCData,
    &Analogues->BusbarAnalogues.BusbarPhase3Data) ;

  // ...then the neutral current measurements

  Analogues->BusbarAnalogues.NeutralCurrentMaximum = (real32_T) PeriodicReport->BusbarData.NeutralMaxMeasuredCurrent / GK_SCALING_FOR_CURRENT ;
  Analogues->BusbarAnalogues.NeutralCurrentMinimum = (real32_T) PeriodicReport->BusbarData.NeutralMinMeasuredCurrent / GK_SCALING_FOR_CURRENT ;
  Analogues->BusbarAnalogues.NeutralCurrentMean = (real32_T) PeriodicReport->BusbarData.NeutralMeanMeasuredCurrent / GK_SCALING_FOR_CURRENT ;

  // Feeder data needs to be processed. Remember that we won't necessarily have feeder 1 measurements in the first set of feeder information

  if (PeriodicReport->Feeder1Data.FeederRef != 0) CopyFeederData (
    UnitFlags,
    &PeriodicReport->Feeder1Data,
    Analogues) ;

  if (PeriodicReport->Feeder2Data.FeederRef != 0) CopyFeederData (
    UnitFlags,
    &PeriodicReport->Feeder2Data,
    Analogues) ;

  if (PeriodicReport->Feeder3Data.FeederRef != 0) CopyFeederData (
    UnitFlags,
    &PeriodicReport->Feeder3Data,
    Analogues) ;

  if (PeriodicReport->Feeder4Data.FeederRef != 0) CopyFeederData (
    UnitFlags,
    &PeriodicReport->Feeder4Data,
    Analogues) ;

  if (PeriodicReport->Feeder5Data.FeederRef != 0) CopyFeederData (
    UnitFlags,
    &PeriodicReport->Feeder5Data,
    Analogues) ;

  // Copy the unit-level data

  Analogues->UnitAnalogues.MessageIndex = (real32_T) PeriodicReport->FileReference.FileReferenceNumber ;
  Analogues->UnitAnalogues.NumberOfFeedersInUse = (real32_T) PeriodicReport->DataPeriodData.NumberOfFeedersInUse ;
  Analogues->UnitAnalogues.PeriodicReportingPeriod = (real32_T) PeriodicReport->DataPeriodData.PeriodicReportingPeriod ;
  Analogues->UnitAnalogues.AmbientTemperatureMean = (real32_T) PeriodicReport->TemperatureData.MeanBusbarTemperature / GK_SCALING_FOR_TEMPERATURE ;
  Analogues->UnitAnalogues.AmbientTemperatureMaximum = (real32_T) PeriodicReport->TemperatureData.MaxBusbarTemperature / GK_SCALING_FOR_TEMPERATURE ;
  Analogues->UnitAnalogues.AmbientTemperatureMinimum = (real32_T) PeriodicReport->TemperatureData.MinBusbarTemperature / GK_SCALING_FOR_TEMPERATURE ;
  Analogues->UnitAnalogues.TransformerTemperatureMean = (real32_T) PeriodicReport->TemperatureData.MeanTransformerTemperature / GK_SCALING_FOR_TEMPERATURE ;
  Analogues->UnitAnalogues.TransformerTemperatureMaximum = (real32_T) PeriodicReport->TemperatureData.MaxTransaformerTemperature / GK_SCALING_FOR_TEMPERATURE ;
  Analogues->UnitAnalogues.TransformerTemperatureMinimum = (real32_T) PeriodicReport->TemperatureData.MinTransformerTemperature / GK_SCALING_FOR_TEMPERATURE ;
  Analogues->UnitAnalogues.UnitTemperatureMean = (real32_T) PeriodicReport->TemperatureData.MeanMcuTemperature / GK_SCALING_FOR_TEMPERATURE ;
  Analogues->UnitAnalogues.UnitTemperatureMaximum = (real32_T) PeriodicReport->TemperatureData.MaxMcuTemperature / GK_SCALING_FOR_TEMPERATURE ;
  Analogues->UnitAnalogues.UnitTemperatureMinimum = (real32_T) PeriodicReport->TemperatureData.MinMcuTemperature / GK_SCALING_FOR_TEMPERATURE ;

  // Return the result of the decode operation

  return DecodeSuccess ;
}

void SetOrClear (
  uint8_T Key,
  uint16_T *Flags,
  uint16_T BitMask)
{
  if (Key == 4)
  {
    *Flags |= BitMask ;
  }
  else
  {
    *Flags &= ~BitMask ;
  }
}

void DeriveFeederBinariesMask (
  uint16_T* GridkeyBinaries,
  ThresholdDataBus* ThresholdData,
  GK_enuBinaries BinaryFlagSet,
  uint16_T *Phase1Flags,
  uint16_T *Phase2Flags,
  uint16_T *Phase3Flags)
{
  // Use the reference to determine the mask to apply

  switch (ThresholdData->AlarmReference)
  {
    case ALARM_REF_FEEDER_L1_CURRENT_HIGH_HIGH: // Phase 1 current high-high
    {
      SetOrClear (
        ThresholdData->DataKey,
        Phase1Flags,
        GK_BIN_BUSBAR_CURRENT_PHASE_1_CURRENT_HIGH_HIGH) ;
    }
    break ;

    case ALARM_REF_FEEDER_L1_CURRENT_HIGH: // Phase 1 current high
    {
      SetOrClear (
        ThresholdData->DataKey,
        Phase1Flags,
        GK_BIN_BUSBAR_CURRENT_PHASE_1_CURRENT_HIGH) ;
    }
    break ;

    case ALARM_REF_FEEDER_L1_CURRENT_LOW: // Phase 1 current low
    {
      SetOrClear (
        ThresholdData->DataKey,
        Phase1Flags,
        GK_BIN_BUSBAR_CURRENT_PHASE_1_CURRENT_LOW) ;
    }
    break ;

    case ALARM_REF_FEEDER_L1_CURRENT_NEGATIVE: // Phase 1 current negative
    {
      SetOrClear (
        ThresholdData->DataKey,
        Phase1Flags,
        GK_BIN_BUSBAR_CURRENT_PHASE_1_CURRENT_NEGATIVE) ;
    }
    break ;

    case ALARM_REF_FEEDER_L2_CURRENT_HIGH_HIGH: // Phase 2 current high-high
    {
      SetOrClear (
        ThresholdData->DataKey,
        Phase2Flags,
        GK_BIN_BUSBAR_CURRENT_PHASE_2_CURRENT_HIGH_HIGH) ;
    }
    break ;

    case ALARM_REF_FEEDER_L2_CURRENT_HIGH: // Phase 2 current high
    {
      SetOrClear (
        ThresholdData->DataKey,
        Phase2Flags,
        GK_BIN_BUSBAR_CURRENT_PHASE_2_CURRENT_HIGH) ;
    }
    break ;

    case ALARM_REF_FEEDER_L2_CURRENT_LOW: // Phase 2 currnet low
    {
      SetOrClear (
        ThresholdData->DataKey,
        Phase2Flags,
        GK_BIN_BUSBAR_CURRENT_PHASE_2_CURRENT_LOW) ;
    }
    break ;

    case ALARM_REF_FEEDER_L2_CURRENT_NEGATIVE: // Phase 2 current negative
    {
      SetOrClear (
        ThresholdData->DataKey,
        Phase2Flags,
        GK_BIN_BUSBAR_CURRENT_PHASE_2_CURRENT_NEGATIVE) ;
    }
    break ;

    case ALARM_REF_FEEDER_L3_CURRENT_HIGH_HIGH: // Phase 3 current high-high
    {
      SetOrClear (
        ThresholdData->DataKey,
        Phase3Flags,
        GK_BIN_BUSBAR_CURRENT_PHASE_3_CURRENT_HIGH_HIGH) ;
    }
    break ;

    case ALARM_REF_FEEDER_L3_CURRENT_HIGH: // Phase 3 current high
    {
      SetOrClear (
        ThresholdData->DataKey,
        Phase3Flags,
        GK_BIN_BUSBAR_CURRENT_PHASE_3_CURRENT_HIGH) ;
    }
    break ;

    case ALARM_REF_FEEDER_L3_CURRENT_LOW: // Phase 3 current low
    {
      SetOrClear (
        ThresholdData->DataKey,
        Phase3Flags,
        GK_BIN_BUSBAR_CURRENT_PHASE_3_CURRENT_LOW) ;
    }
    break ;

    case ALARM_REF_FEEDER_L3_CURRENT_NEGATIVE: // Phase 3 current negative
    {
      SetOrClear (
        ThresholdData->DataKey,
        Phase3Flags,
        GK_BIN_BUSBAR_CURRENT_PHASE_3_CURRENT_NEGATIVE) ;
    }
    break ;

    default: {} ;
  }

  // Set the phase fault present flag for each phase

  GridkeyBinaries [BinaryFlagSet] = *Phase1Flags | *Phase2Flags | *Phase3Flags ;

  if (*Phase1Flags != 0) GridkeyBinaries [BinaryFlagSet] |= GK_BIN_FEEDER_1_PHASE_1_ALARM ;
  if (*Phase2Flags != 0) GridkeyBinaries [BinaryFlagSet] |= GK_BIN_FEEDER_1_PHASE_2_ALARM ;
  if (*Phase3Flags != 0) GridkeyBinaries [BinaryFlagSet] |= GK_BIN_FEEDER_1_PHASE_3_ALARM ;

  // Set the overall alarm asserted flags

  if (GridkeyBinaries [BinaryFlagSet] != 0) GridkeyBinaries [BinaryFlagSet] |= GK_BIN_FEEDER_1_ALARM_ASSERTED ;
}

uint8_T ProcessAlertReport (
  uint8_T* PacketFromTheMcu,
  uint16_T* UnitFlags,
  uint16_T* GridkeyBinaries)
{
  uint8_T DecodeSuccess = GK_DECODE_WAS_SUCCESSFUL ;

  // Map the alert report structure onto the received message buffer

  AlertReportBus* AlertReport = (AlertReportBus*) PacketFromTheMcu ;

  // Use the category to determine which set of flags needs to be 
  // modified. We won't set any unit flags from the alert so we'll 
  // use it to indicate that we can't translate the report

  switch (AlertReport->ThresholdData.AlarmCatagory)
  {
    case ALARM_CAT_BUSBAR:
    {
      switch (AlertReport->ThresholdData.AlarmReference)
      {
        case ALARM_REF_BUSBAR_L1_VOLT_HIGH_HIGH: // Phase 1 voltage high-high
        {
          SetOrClear (
            AlertReport->ThresholdData.DataKey,
            &BusbarPhase1VoltageFlags,
            GK_BIN_BUSBAR_VOLTAGE_PHASE_1_VOLTAGE_HIGH_HIGH) ;
        }
        break ;

        case ALARM_REF_BUSBAR_L1_VOLT_HIGH: // Phase 1 voltage high
        {
          SetOrClear (
            AlertReport->ThresholdData.DataKey,
            &BusbarPhase1VoltageFlags,
            GK_BIN_BUSBAR_VOLTAGE_PHASE_1_VOLTAGE_HIGH) ;
        }
        break ;

        case ALARM_REF_BUSBAR_L1_VOLT_LOW_LOW: // Phase 1 voltage low-low
        {
          SetOrClear (
            AlertReport->ThresholdData.DataKey,
            &BusbarPhase1VoltageFlags,
            GK_BIN_BUSBAR_VOLTAGE_PHASE_1_VOLTAGE_LOW_LOW) ;
        }
        break ;

        case ALARM_REF_BUSBAR_L1_VOLT_LOW: // Phase 1 voltage low
        {
          SetOrClear (
            AlertReport->ThresholdData.DataKey,
            &BusbarPhase1VoltageFlags,
            GK_BIN_BUSBAR_VOLTAGE_PHASE_1_VOLTAGE_LOW) ;
        }
        break ;

        case ALARM_REF_BUSBAR_L2_VOLT_HIGH_HIGH: // Phase 2 voltage high-high
        {
          SetOrClear (
            AlertReport->ThresholdData.DataKey,
            &BusbarPhase2VoltageFlags,
            GK_BIN_BUSBAR_VOLTAGE_PHASE_2_VOLTAGE_HIGH_HIGH) ;
        }
        break ;

        case ALARM_REF_BUSBAR_L2_VOLT_HIGH: // Phase 2 voltage high
        {
          SetOrClear (
            AlertReport->ThresholdData.DataKey,
            &BusbarPhase2VoltageFlags,
            GK_BIN_BUSBAR_VOLTAGE_PHASE_2_VOLTAGE_HIGH) ;
        }
        break ;

        case ALARM_REF_BUSBAR_L2_VOLT_LOW_LOW: // Phase 2 voltage low-low
        {
          SetOrClear (
            AlertReport->ThresholdData.DataKey,
            &BusbarPhase2VoltageFlags,
            GK_BIN_BUSBAR_VOLTAGE_PHASE_2_VOLTAGE_LOW_LOW) ;
        }
        break ;

        case ALARM_REF_BUSBAR_L2_VOLT_LOW: // Phase 2 voltage low
        {
          SetOrClear (
            AlertReport->ThresholdData.DataKey,
            &BusbarPhase2VoltageFlags,
            GK_BIN_BUSBAR_VOLTAGE_PHASE_2_VOLTAGE_LOW) ;
        }
        break ;

        case ALARM_REF_BUSBAR_L3_VOLT_HIGH_HIGH: // Phase 3 voltage high-high
        {
          SetOrClear (
            AlertReport->ThresholdData.DataKey,
            &BusbarPhase3VoltageFlags,
            GK_BIN_BUSBAR_VOLTAGE_PHASE_3_VOLTAGE_HIGH_HIGH) ;
        }
        break ;

        case ALARM_REF_BUSBAR_L3_VOLT_HIGH: // Phase 3 voltage high
        {
          SetOrClear (
            AlertReport->ThresholdData.DataKey,
            &BusbarPhase3VoltageFlags,
            GK_BIN_BUSBAR_VOLTAGE_PHASE_3_VOLTAGE_HIGH) ;
        }
        break ;

        case ALARM_REF_BUSBAR_L3_VOLT_LOW_LOW: // Phase 3 voltage low-low
        {
          SetOrClear (
            AlertReport->ThresholdData.DataKey,
            &BusbarPhase3VoltageFlags,
            GK_BIN_BUSBAR_VOLTAGE_PHASE_3_VOLTAGE_LOW_LOW) ;
        }
        break ;

        case ALARM_REF_BUSBAR_L3_VOLT_LOW: // Phase 3 voltage low
        {
          SetOrClear (
            AlertReport->ThresholdData.DataKey,
            &BusbarPhase3VoltageFlags,
            GK_BIN_BUSBAR_VOLTAGE_PHASE_3_VOLTAGE_LOW) ;
        }
        break ;

        case ALARM_REF_BUSBAR_L1_CURRENT_HIGH_HIGH: // Phase 1 current high-high
        {
          SetOrClear (
            AlertReport->ThresholdData.DataKey,
            &BusbarPhase1CurrentFlags,
            GK_BIN_BUSBAR_CURRENT_PHASE_1_CURRENT_HIGH_HIGH) ;
        }
        break ;

        case ALARM_REF_BUSBAR_L1_CURRENT_HIGH: // Phase 1 current high
        {
          SetOrClear (
            AlertReport->ThresholdData.DataKey,
            &BusbarPhase1CurrentFlags,
            GK_BIN_BUSBAR_CURRENT_PHASE_1_CURRENT_HIGH) ;
        }
        break ;

        case ALARM_REF_BUSBAR_L1_CURRENT_LOW: // Phase 1 current low
        {
          SetOrClear (
            AlertReport->ThresholdData.DataKey,
            &BusbarPhase1CurrentFlags,
            GK_BIN_BUSBAR_CURRENT_PHASE_1_CURRENT_LOW) ;
        }
        break ;

        case ALARM_REF_BUSBAR_L1_CURRENT_NEGATIVE: // Phase 1 current negative
        {
          SetOrClear (
            AlertReport->ThresholdData.DataKey,
            &BusbarPhase1CurrentFlags,
            GK_BIN_BUSBAR_CURRENT_PHASE_1_CURRENT_NEGATIVE) ;
        }
        break ;

        case ALARM_REF_BUSBAR_L2_CURRENT_HIGH_HIGH: // Phase 2 current high-high
        {
          SetOrClear (
            AlertReport->ThresholdData.DataKey,
            &BusbarPhase2CurrentFlags,
            GK_BIN_BUSBAR_CURRENT_PHASE_2_CURRENT_HIGH_HIGH) ;
        }
        break ;

        case ALARM_REF_BUSBAR_L2_CURRENT_HIGH: // Phase 2 current high
        {
          SetOrClear (
            AlertReport->ThresholdData.DataKey,
            &BusbarPhase2CurrentFlags,
            GK_BIN_BUSBAR_CURRENT_PHASE_2_CURRENT_HIGH) ;
        }
        break ;

        case ALARM_REF_BUSBAR_L2_CURRENT_LOW: // Phase 2 current low
        {
          SetOrClear (
            AlertReport->ThresholdData.DataKey,
            &BusbarPhase2CurrentFlags,
            GK_BIN_BUSBAR_CURRENT_PHASE_2_CURRENT_LOW) ;
        }
        break ;

        case ALARM_REF_BUSBAR_L2_CURRENT_NEGATIVE: // Phase 2 current negative
        {
          SetOrClear (
            AlertReport->ThresholdData.DataKey,
            &BusbarPhase2CurrentFlags,
            GK_BIN_BUSBAR_CURRENT_PHASE_2_CURRENT_NEGATIVE) ;
        }
        break ;

        case ALARM_REF_BUSBAR_L3_CURRENT_HIGH_HIGH: // Phase 3 current high-high
        {
          SetOrClear (
            AlertReport->ThresholdData.DataKey,
            &BusbarPhase3CurrentFlags,
            GK_BIN_BUSBAR_CURRENT_PHASE_3_CURRENT_HIGH_HIGH) ;
        }
        break ;

        case ALARM_REF_BUSBAR_L3_CURRENT_HIGH: // Phase 3 current high
        {
          SetOrClear (
            AlertReport->ThresholdData.DataKey,
            &BusbarPhase3CurrentFlags,
            GK_BIN_BUSBAR_CURRENT_PHASE_3_CURRENT_HIGH) ;
        }
        break ;

        case ALARM_REF_BUSBAR_L3_CURRENT_LOW: // Phase 3 current low
        {
          SetOrClear (
            AlertReport->ThresholdData.DataKey,
            &BusbarPhase3CurrentFlags,
            GK_BIN_BUSBAR_CURRENT_PHASE_3_CURRENT_LOW) ;
        }
        break ;

        case ALARM_REF_BUSBAR_L3_CURRENT_NEGATIVE: // Phase 3 current negative
        {
          SetOrClear (
            AlertReport->ThresholdData.DataKey,
            &BusbarPhase3CurrentFlags,
            GK_BIN_BUSBAR_CURRENT_PHASE_3_CURRENT_NEGATIVE) ;
        }
        break ;

        default: {} ;
      }

      // Set the phase fault present flag for each phase

      GridkeyBinaries [GK_binBusbarVoltageFlags] = BusbarPhase1VoltageFlags | BusbarPhase2VoltageFlags | BusbarPhase2VoltageFlags ;
      GridkeyBinaries [GK_binBusbarCurrentFlags] = BusbarPhase1CurrentFlags | BusbarPhase2CurrentFlags | BusbarPhase2CurrentFlags ;

      if (BusbarPhase1VoltageFlags != 0) GridkeyBinaries [GK_binBusbarVoltageFlags] |= GK_BIN_BUSBAR_VOLTAGE_PHASE_1_ALARM ;
      if (BusbarPhase2VoltageFlags != 0) GridkeyBinaries [GK_binBusbarVoltageFlags] |= GK_BIN_BUSBAR_VOLTAGE_PHASE_2_ALARM ;
      if (BusbarPhase3VoltageFlags != 0) GridkeyBinaries [GK_binBusbarVoltageFlags] |= GK_BIN_BUSBAR_VOLTAGE_PHASE_3_ALARM ;

      if (BusbarPhase1CurrentFlags != 0) GridkeyBinaries [GK_binBusbarCurrentFlags] |= GK_BIN_BUSBAR_CURRENT_PHASE_1_ALARM ;
      if (BusbarPhase2CurrentFlags != 0) GridkeyBinaries [GK_binBusbarCurrentFlags] |= GK_BIN_BUSBAR_CURRENT_PHASE_2_ALARM ;
      if (BusbarPhase3CurrentFlags != 0) GridkeyBinaries [GK_binBusbarCurrentFlags] |= GK_BIN_BUSBAR_CURRENT_PHASE_3_ALARM ;

      // Set the overall alarm asserted flags

      if (GridkeyBinaries [GK_binBusbarVoltageFlags] != 0) GridkeyBinaries [GK_binBusbarVoltageFlags] |= GK_BIN_BUSBAR_VOLTAGE_ALARM_ASSERTED ;
      if (GridkeyBinaries [GK_binBusbarCurrentFlags] != 0) GridkeyBinaries [GK_binBusbarCurrentFlags] |= GK_BIN_BUSBAR_CURRENT_ALARM_ASSERTED ;
    }
    break ;

    case ALARM_CAT_FEEDER_1:
    {
      DeriveFeederBinariesMask (
        GridkeyBinaries,
        &AlertReport->ThresholdData,
        GK_binFeeder1Flags,
        &Feeder1Phase1CurrentFlags,
        &Feeder1Phase2CurrentFlags,
        &Feeder1Phase3CurrentFlags) ;
    }
    break ;

    case ALARM_CAT_FEEDER_2:
    {
      DeriveFeederBinariesMask (
        GridkeyBinaries,
        &AlertReport->ThresholdData,
        GK_binFeeder2Flags,
        &Feeder2Phase1CurrentFlags,
        &Feeder2Phase2CurrentFlags,
        &Feeder2Phase3CurrentFlags) ;
    }
    break ;

    case ALARM_CAT_FEEDER_3:
    {
      DeriveFeederBinariesMask (
        GridkeyBinaries,
        &AlertReport->ThresholdData,
        GK_binFeeder3Flags,
        &Feeder3Phase1CurrentFlags,
        &Feeder3Phase2CurrentFlags,
        &Feeder3Phase3CurrentFlags) ;
    }
    break ;

    case ALARM_CAT_FEEDER_4:
    {
      DeriveFeederBinariesMask (
        GridkeyBinaries,
        &AlertReport->ThresholdData,
        GK_binFeeder4Flags,
        &Feeder4Phase1CurrentFlags,
        &Feeder4Phase2CurrentFlags,
        &Feeder4Phase3CurrentFlags) ;
    }
    break ;

    case ALARM_CAT_FEEDER_5:
    {
      DeriveFeederBinariesMask (
        GridkeyBinaries,
        &AlertReport->ThresholdData,
        GK_binFeeder5Flags,
        &Feeder5Phase1CurrentFlags,
        &Feeder5Phase2CurrentFlags,
        &Feeder5Phase3CurrentFlags) ;
    }
    break ;

    default: {} ;
  }

  // Set the overall alarm asserted flag in the unit flags

  if ((GridkeyBinaries [GK_binBusbarVoltageFlags] != 0) ||
      (GridkeyBinaries [GK_binBusbarCurrentFlags] != 0) ||
      (GridkeyBinaries [GK_binFeeder1Flags] != 0) ||
      (GridkeyBinaries [GK_binFeeder2Flags] != 0) ||
      (GridkeyBinaries [GK_binFeeder3Flags] != 0) ||
      (GridkeyBinaries [GK_binFeeder4Flags] != 0) ||
      (GridkeyBinaries [GK_binFeeder5Flags] != 0))
  {
    *UnitFlags = *UnitFlags | GK_BIN_UNIT_ALARM_ASSERTED ;
  }
  else
  {
    *UnitFlags = *UnitFlags & ~GK_BIN_UNIT_ALARM_ASSERTED ;
  }

  // Return the result of the decode operation

  return DecodeSuccess ;
}

/*
 * =============================================================================
 *
 * Function Calculate KLV Checksum
 * -------------------------------
 *
 * This function Calculates the KLV checksum for a message based on the length
 * field in the message header.
 *
 * =============================================================================
 * Inputs
 * -----------------------------------------------------------------------------
 *
 * Message Buffer
 *   The buffer that houses the KLV-headed message.
 *
 * =============================================================================
 * Outputs
 * -----------------------------------------------------------------------------
 *
 * None
 *
 * =============================================================================
 * Returns
 * -----------------------------------------------------------------------------
 *
 * KLV checksum for the message
 *
 * =============================================================================
 */

uint16_T CalculateKlvChecksum (
  uint8_T* MessageBuffer)
{
  // Map the KLV header onto the message buffer

  CommonDataBus* KlvHeader = (CommonDataBus*) MessageBuffer ;

  // Move through the message calculating the checksum as we go

  uint16_T CalculatedChecksum = 0 ;

  uint16_T BytePos ;

  for (BytePos = 14 ; BytePos < KlvHeader->MessageLength ; BytePos++)
    CalculatedChecksum += MessageBuffer [BytePos] << (8 * ((BytePos + 1) % 2)) ;

  // Return the calculated checksum

  return CalculatedChecksum ;
}

/*
 * =============================================================================
 *
 * Function Translate GridKey Messages
 * -----------------------------------
 *
 * This function takes in a packet received from the MCU520 and translates the
 * message content into analogues for statistical reports, binaries for alert
 * reports and timestamps for timestamp requests. The return packet and
 * associated length is supplied for transmission back to the MCU520.
 *
 * =============================================================================
 * Inputs
 * -----------------------------------------------------------------------------
 *
 * System time
 *   The system time, used to generate the tmiestamp response to the MCU
 *
 * Packet from the MCU
 *   The packet received over the TCP server from the MCU
 *
 * =============================================================================
 * Outputs
 * -----------------------------------------------------------------------------
 *
 * Packet to the MCU
 *   The packet to be transmitted from the TCP server to the MCU
 *
 * Packet length
 *   The number of bytes in the packet to be transmitted
 *
 * GridKey binaries
 *   Binary alarm data provided for mapping onto the modbus address map
 *
 * GridKey analogues
 *   analogue measurement data provided for mapping onto the modbus address map
 *
 * =============================================================================
 * Returns
 * -----------------------------------------------------------------------------
 *
 * Zero if the packet was decoded without error
 *
 * =============================================================================
 */
#include "Logger.h"
uint8_T GK_TranslateGridkeyMessages (
  time_t SystemTime,
  uint8_T* PacketFromTheMcu,
  uint8_T* PacketToTheMcu,
  uint16_T* PacketLength,
  uint16_T* GridkeyBinaries,
  real32_T* GridkeyAnalogues)
{
  uint8_T DecodeSuccess = GK_DECODE_WAS_SUCCESSFUL ;

  // Firstly, check to see whether we've received a request for a timestamp

  const char_T cTimeRequest [] = "TIM" ;

  if ((PacketFromTheMcu [0] == cTimeRequest [0]) &&
      (PacketFromTheMcu [1] == cTimeRequest [1]) &&
      (PacketFromTheMcu [2] == cTimeRequest [2]))
  {
    // Make sure we've got a valid time

    if (SystemTime > 0)
    {
      // Use the system time to build a timestamp response. Year needs 100 
      // taken off and month needs to be changed from zero-based to one-based

      struct tm* DecodedTime = gmtime (&SystemTime) ;

      *PacketLength = sprintf ((char_T*) PacketToTheMcu, "%d %02d/%02d/%02d,%02d:%02d:%02d %d",
        DecodedTime->tm_wday, DecodedTime->tm_year - 100, DecodedTime->tm_mon + 1, DecodedTime->tm_mday,
        DecodedTime->tm_hour, DecodedTime->tm_min, DecodedTime->tm_sec, DecodedTime->tm_wday) ;
    }
    else
    {
      DecodeSuccess = GK_DECODE_INVALID_SYSTEM_TIME ;
    }
  }
  else
  {
    // We're decoding a KLV message. Map the KLV header structure onto the received message buffer

    CommonDataBus* KlvHeader = (CommonDataBus*) PacketFromTheMcu ;

    // Check message integrity using the KLV checksum

    if (KlvHeader->Checksum.u16Checksum == CalculateKlvChecksum (PacketFromTheMcu))
    {
      const uint8_T cStatisticalReport [] = { 0xE1, 0xD2, 0xC3, 0xB4, 0x80, 0x01, 0x00, 0x00 } ;
      const uint8_T cAlertReport [] = { 0xE1, 0xD2, 0xC3, 0xB4, 0x80, 0x02, 0x00, 0x00 } ;

      // We need to see what type of message was received

      if ((KlvHeader->MessageIdentifier [4] == cStatisticalReport [4]) &&
          (KlvHeader->MessageIdentifier [5] == cStatisticalReport [5]) &&
          (KlvHeader->MessageIdentifier [6] == cStatisticalReport [6]) &&
          (KlvHeader->MessageIdentifier [7] == cStatisticalReport [7]))
      {
        loginfo ("Decoder >> (Statistical Report)\n") ;

        // Decode the statistical report

        DecodeSuccess = ProcessStatisticalReport (
          PacketFromTheMcu,
          &GridkeyBinaries [GK_binUnitFlags],
          GridkeyAnalogues) ;
      }
      else
      {
        if ((KlvHeader->MessageIdentifier [4] == cAlertReport [4]) &&
            (KlvHeader->MessageIdentifier [5] == cAlertReport [5]) &&
            (KlvHeader->MessageIdentifier [6] == cAlertReport [6]) &&
            (KlvHeader->MessageIdentifier [7] == cAlertReport [7]))
        {
          loginfo ("Decoder >> (Alert Report)\n") ;

          // Decode the alert report

          DecodeSuccess = ProcessAlertReport (
            PacketFromTheMcu,
            &GridkeyBinaries [GK_binUnitFlags],
            GridkeyBinaries) ;
        }
        else
        {
          // Return unknown message error

          DecodeSuccess = GK_DECODE_UNKNOWN_MESSAGE ;

          const uint8_T cStatusReport [] = { 0xE1, 0xD2, 0xC3, 0xB4, 0x80, 0x11, 0x00, 0x00 } ;
          const uint8_T cFactoryConfigs [] = { 0xE1, 0xD2, 0xC3, 0xB4, 0x80, 0x05, 0x00, 0x00 } ;
          const uint8_T cUserConfigs [] = { 0xE1, 0xD2, 0xC3, 0xB4, 0x80, 0x05, 0x01, 0x00 } ;
          const uint8_T cCalibrationData [] = { 0xE1, 0xD2, 0xC3, 0xB4, 0x80, 0x05, 0x03, 0x00 } ;

          if ((KlvHeader->MessageIdentifier [4] == cStatusReport [4]) &&
            (KlvHeader->MessageIdentifier [5] == cStatusReport [5]) &&
            (KlvHeader->MessageIdentifier [6] == cStatusReport [6]) &&
            (KlvHeader->MessageIdentifier [7] == cStatusReport [7]))
          {
            loginfo ("Decoder >> (Status Report)\n") ;
          }
          else if ((KlvHeader->MessageIdentifier [4] == cFactoryConfigs [4]) &&
                   (KlvHeader->MessageIdentifier [5] == cFactoryConfigs [5]) &&
                   (KlvHeader->MessageIdentifier [6] == cFactoryConfigs [6]) &&
                   (KlvHeader->MessageIdentifier [7] == cFactoryConfigs [7]))
          {
            loginfo ("Decoder >> (Factory Configs)\n") ;

            // ReportingPeriod = PacketFromTheMcu [0x44] ;
          }
          else if ((KlvHeader->MessageIdentifier [4] == cUserConfigs [4]) &&
                   (KlvHeader->MessageIdentifier [5] == cUserConfigs [5]) &&
                   (KlvHeader->MessageIdentifier [6] == cUserConfigs [6]) &&
                   (KlvHeader->MessageIdentifier [7] == cUserConfigs [7]))
          {
              loginfo ("Decoder >> (User Configs)\n") ;
          }
          else if ((KlvHeader->MessageIdentifier [4] == cCalibrationData [4]) &&
                   (KlvHeader->MessageIdentifier [5] == cCalibrationData [5]) &&
                   (KlvHeader->MessageIdentifier [6] == cCalibrationData [6]) &&
                   (KlvHeader->MessageIdentifier [7] == cCalibrationData [7]))
          {
              loginfo ("Decoder >> (Calibration Data)\n") ;
          }
          else
          {
              loginfo ("Decoder >> (Unknown)\n") ;
          }
        }
      }
    }
    else
    {
      // Return a bad checksum error

      DecodeSuccess = GK_DECODE_CHECKSUM_ERROR ;
    }

    // Return "VALID" regardless of whether the data was translated correctly

    *PacketLength = 5 ;
    memcpy (PacketToTheMcu, "VALID", *PacketLength) ;
  }

  // Return the result of the decode operation

  return DecodeSuccess ;
}

uint8_T GK_SimulateMcu520 (
  boolean_T UseAddressOffsets,
  uint8_T AlarmCategory,
  boolean_T AssertL1VoltLowLow,
  boolean_T AssertL1VoltLow,
  boolean_T AssertL1VoltHigh,
  boolean_T AssertL1VoltHighHigh,
  boolean_T AssertL2VoltLowLow,
  boolean_T AssertL2VoltLow,
  boolean_T AssertL2VoltHigh,
  boolean_T AssertL2VoltHighHigh,
  boolean_T AssertL3VoltLowLow,
  boolean_T AssertL3VoltLow,
  boolean_T AssertL3VoltHigh,
  boolean_T AssertL3VoltHighHigh,
  boolean_T AssertL1CurrentHighHigh,
  boolean_T AssertL1CurrentHigh,
  boolean_T AssertL1CurrentLow,
  boolean_T AssertL1CurrentNegative,
  boolean_T AssertL2CurrentHighHigh,
  boolean_T AssertL2CurrentHigh,
  boolean_T AssertL2CurrentLow,
  boolean_T AssertL2CurrentNegative,
  boolean_T AssertL3CurrentHighHigh,
  boolean_T AssertL3CurrentHigh,
  boolean_T AssertL3CurrentLow,
  boolean_T AssertL3CurrentNegative,
  uint8_T* PacketFromTheMcu)
{
  uint8_T DecodeStatus = GK_DECODE_WAS_SUCCESSFUL ;

  // If the alarm category is set, we're generating an alert

  boolean_T AlarmSet = false ;

  if (AlarmCategory != 0)
  {
    AlertReportBus* AlertReport = (AlertReportBus*) PacketFromTheMcu ;

    // Set a failure, will be set to good if all's well

    DecodeStatus = GK_DECODE_INVALID_CAT_REF_COMBINATION ;

    // Alarm cat/ref is a bit convoluted

    const uint8_T cAlertReport [] = { 0xE1, 0xD2, 0xC3, 0xB4, 0x80, 0x02, 0x00, 0x00 } ;
    memcpy (AlertReport->CommonData.MessageIdentifier, cAlertReport, sizeof (cAlertReport)) ;

    AlertReport->CommonData.MessageLength = sizeof (AlertReportBus) ;

    AlertReport->ThresholdData.AlarmCatagory = AlarmCategory ;
    AlertReport->ThresholdData.AlarmReference = 0xFF ;

    if (AlarmCategory == ALARM_CAT_BUSBAR)
    {
      if (AssertL1VoltLowLow)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_BUSBAR_L1_VOLT_LOW_LOW ;
      else if (AssertL1VoltLow)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_BUSBAR_L1_VOLT_LOW ;
      else if (AssertL1VoltHigh)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_BUSBAR_L1_VOLT_HIGH ;
      else if (AssertL1VoltHighHigh)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_BUSBAR_L1_VOLT_HIGH_HIGH ;
      else if (AssertL2VoltLowLow)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_BUSBAR_L2_VOLT_LOW_LOW ;
      else if (AssertL2VoltLow)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_BUSBAR_L2_VOLT_LOW ;
      else if (AssertL2VoltHigh)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_BUSBAR_L2_VOLT_HIGH ;
      else if (AssertL2VoltHighHigh)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_BUSBAR_L2_VOLT_HIGH_HIGH ;
      else if (AssertL3VoltLowLow)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_BUSBAR_L3_VOLT_LOW_LOW ;
      else if (AssertL3VoltLow)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_BUSBAR_L3_VOLT_LOW ;
      else if (AssertL3VoltHigh)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_BUSBAR_L3_VOLT_HIGH ;
      else if (AssertL3VoltHighHigh)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_BUSBAR_L3_VOLT_HIGH_HIGH ;
      else if (AssertL1CurrentHighHigh)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_BUSBAR_L1_CURRENT_HIGH_HIGH ;
      else if (AssertL1CurrentHigh)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_BUSBAR_L1_CURRENT_HIGH ;
      else if (AssertL1CurrentLow)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_BUSBAR_L1_CURRENT_LOW ;
      else if (AssertL1CurrentNegative)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_BUSBAR_L1_CURRENT_NEGATIVE ;
      else if (AssertL2CurrentHighHigh)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_BUSBAR_L2_CURRENT_HIGH_HIGH ;
      else if (AssertL2CurrentHigh)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_BUSBAR_L2_CURRENT_HIGH ;
      else if (AssertL2CurrentLow)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_BUSBAR_L2_CURRENT_LOW ;
      else if (AssertL2CurrentNegative)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_BUSBAR_L2_CURRENT_NEGATIVE ;
      else if (AssertL3CurrentHighHigh)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_BUSBAR_L3_CURRENT_HIGH_HIGH ;
      else if (AssertL3CurrentHigh)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_BUSBAR_L3_CURRENT_HIGH ;
      else if (AssertL3CurrentLow)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_BUSBAR_L3_CURRENT_LOW ;
      else if (AssertL3CurrentNegative)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_BUSBAR_L3_CURRENT_NEGATIVE ;
    }
    else if (AlarmCategory < ALARM_CAT_BUSBAR)
    {
      if (AssertL1CurrentHighHigh)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_FEEDER_L1_CURRENT_HIGH_HIGH ;
      else if (AssertL1CurrentHigh)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_FEEDER_L1_CURRENT_HIGH ;
      else if (AssertL1CurrentLow)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_FEEDER_L1_CURRENT_LOW ;
      else if (AssertL1CurrentNegative)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_FEEDER_L1_CURRENT_NEGATIVE ;
      else if (AssertL2CurrentHighHigh)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_FEEDER_L2_CURRENT_HIGH_HIGH ;
      else if (AssertL2CurrentHigh)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_FEEDER_L2_CURRENT_HIGH ;
      else if (AssertL2CurrentLow)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_FEEDER_L2_CURRENT_LOW ;
      else if (AssertL2CurrentNegative)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_FEEDER_L2_CURRENT_NEGATIVE ;
      else if (AssertL3CurrentHighHigh)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_FEEDER_L3_CURRENT_HIGH_HIGH ;
      else if (AssertL3CurrentHigh)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_FEEDER_L3_CURRENT_HIGH ;
      else if (AssertL3CurrentLow)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_FEEDER_L3_CURRENT_LOW ;
      else if (AssertL3CurrentNegative)
        AlertReport->ThresholdData.AlarmReference = ALARM_REF_FEEDER_L3_CURRENT_NEGATIVE ;
    }

    // Set the decoded status if a reference was set

    if (AlertReport->ThresholdData.AlarmReference != 0xFF)
    {
      DecodeStatus = GK_DECODE_WAS_SUCCESSFUL ;
      AlarmSet = true ;
    }
  }

  // If an alert report wasn't generated, populate statistical data ;

  if (!AlarmSet)
  {
    PeriodicReportBus* PeriodicReport = (PeriodicReportBus*) PacketFromTheMcu ;

    const uint8_T cStatisticalReport [] = { 0xE1, 0xD2, 0xC3, 0xB4, 0x80, 0x01, 0x00, 0x00 } ;
    memcpy (PeriodicReport->CommonData.MessageIdentifier, cStatisticalReport, sizeof (cStatisticalReport)) ;

    PeriodicReport->CommonData.MessageLength = sizeof (PeriodicReportBus) ;

    // If we're using address offsets, we set constants

    if (UseAddressOffsets)
    {
      // Make sure the feeder numbers are mapped

      PeriodicReport->Feeder1Data.FeederRef = 1 ;
      PeriodicReport->Feeder2Data.FeederRef = 2 ;
      PeriodicReport->Feeder3Data.FeederRef = 3 ;
      PeriodicReport->Feeder4Data.FeederRef = 4 ;
      PeriodicReport->Feeder5Data.FeederRef = 5 ;

      // Mappings of data from addresses

      PeriodicReport->FileReference.FileReferenceNumber = 0x1000 ;
      PeriodicReport->DataPeriodData.NumberOfFeedersInUse = 5 ; // Set this explicitly or the translator won't work
      PeriodicReport->DataPeriodData.PeriodicReportingPeriod = 1 ;
      PeriodicReport->TemperatureData.MeanBusbarTemperature = 0x100C ;
      PeriodicReport->TemperatureData.MaxBusbarTemperature = 0x1010 ;
      PeriodicReport->TemperatureData.MinBusbarTemperature = 0x1014 ;
      PeriodicReport->TemperatureData.MeanTransformerTemperature = 0x1018 ;
      PeriodicReport->TemperatureData.MaxTransaformerTemperature = 0x101C ;
      PeriodicReport->TemperatureData.MinTransformerTemperature = 0x1020 ;
      PeriodicReport->TemperatureData.MeanMcuTemperature = 0x1024 ;
      PeriodicReport->TemperatureData.MaxMcuTemperature = 0x1028 ;
      PeriodicReport->TemperatureData.MinMcuTemperature = 0x102C ;

      PeriodicReport->BusbarData.BusbarPhaseAData.MeanVoltage = 0x1030 ;
      PeriodicReport->BusbarData.BusbarPhaseAData.MaxVoltage = 0x1034 ;
      PeriodicReport->BusbarData.BusbarPhaseAData.MinVoltage = 0x1038 ;
      PeriodicReport->BusbarData.BusbarPhaseAData.MeanCurrent = 0x103C ;
      PeriodicReport->BusbarData.BusbarPhaseAData.MaxCurrent = 0x1040 ;
      PeriodicReport->BusbarData.BusbarPhaseAData.MinCurrent = 0x1044 ;
      PeriodicReport->BusbarData.BusbarPhaseAData.MeanActivePower = 0x1048 ;
      PeriodicReport->BusbarData.BusbarPhaseAData.MeanReactivePower = 0x104C ;
      PeriodicReport->BusbarData.BusbarPhaseAData.ActiveEnergy = 0x1050 ;
      PeriodicReport->BusbarData.BusbarPhaseAData.ReactiveEnergy = 0x1054 ;
      PeriodicReport->BusbarData.BusbarPhaseBData.MeanVoltage = 0x1058 ;
      PeriodicReport->BusbarData.BusbarPhaseBData.MaxVoltage = 0x105C ;
      PeriodicReport->BusbarData.BusbarPhaseBData.MinVoltage = 0x1060 ;
      PeriodicReport->BusbarData.BusbarPhaseBData.MeanCurrent = 0x1064 ;
      PeriodicReport->BusbarData.BusbarPhaseBData.MaxCurrent = 0x1068 ;
      PeriodicReport->BusbarData.BusbarPhaseBData.MinCurrent = 0x106C ;
      PeriodicReport->BusbarData.BusbarPhaseBData.MeanActivePower = 0x1070 ;
      PeriodicReport->BusbarData.BusbarPhaseBData.MeanReactivePower = 0x1074 ;
      PeriodicReport->BusbarData.BusbarPhaseBData.ActiveEnergy = 0x1078 ;
      PeriodicReport->BusbarData.BusbarPhaseBData.ReactiveEnergy = 0x107C ;
      PeriodicReport->BusbarData.BusbarPhaseCData.MeanVoltage = 0x1080 ;
      PeriodicReport->BusbarData.BusbarPhaseCData.MaxVoltage = 0x1084 ;
      PeriodicReport->BusbarData.BusbarPhaseCData.MinVoltage = 0x1088 ;
      PeriodicReport->BusbarData.BusbarPhaseCData.MeanCurrent = 0x108C ;
      PeriodicReport->BusbarData.BusbarPhaseCData.MaxCurrent = 0x1090 ;
      PeriodicReport->BusbarData.BusbarPhaseCData.MinCurrent = 0x1094 ;
      PeriodicReport->BusbarData.BusbarPhaseCData.MeanActivePower = 0x1098 ;
      PeriodicReport->BusbarData.BusbarPhaseCData.MeanReactivePower = 0x109C ;
      PeriodicReport->BusbarData.BusbarPhaseCData.ActiveEnergy = 0x10A0 ;
      PeriodicReport->BusbarData.BusbarPhaseCData.ReactiveEnergy = 0x10A4 ;
      PeriodicReport->BusbarData.NeutralMeanMeasuredCurrent = 0x10A8 ;
      PeriodicReport->BusbarData.NeutralMaxMeasuredCurrent = 0x10AC ;
      PeriodicReport->BusbarData.NeutralMinMeasuredCurrent = 0x10B0 ;

      PeriodicReport->Feeder1Data.FeederPhaseAData.MeanCurrent = 0x10B4 ;
      PeriodicReport->Feeder1Data.FeederPhaseAData.MaxCurrent = 0x10B8 ;
      PeriodicReport->Feeder1Data.FeederPhaseAData.MinCurrent = 0x10BC ;
      PeriodicReport->Feeder1Data.FeederPhaseAData.PhaseAngle = 0x10C0 ;
      PeriodicReport->Feeder1Data.FeederPhaseAData.MeanActivePower = 0x10C4 ;
      PeriodicReport->Feeder1Data.FeederPhaseAData.MeanReactivePower = 0x10C8 ;
      PeriodicReport->Feeder1Data.FeederPhaseAData.ActiveEnergy = 0x10CC ;
      PeriodicReport->Feeder1Data.FeederPhaseAData.ReactiveEnergy = 0x10D0 ;
      PeriodicReport->Feeder1Data.FeederPhaseAData.TotalActiveHarmonicContent = 0x10D4 ;
      PeriodicReport->Feeder1Data.FeederPhaseAData.TotalReactiveHarmonicContent = 0x10D8 ;
      PeriodicReport->Feeder1Data.FeederPhaseAData.TotalHarmonicDistortion = 0x10DC ;
      PeriodicReport->Feeder1Data.FeederPhaseBData.MeanCurrent = 0x10E0 ;
      PeriodicReport->Feeder1Data.FeederPhaseBData.MaxCurrent = 0x10E4 ;
      PeriodicReport->Feeder1Data.FeederPhaseBData.MinCurrent = 0x10E8 ;
      PeriodicReport->Feeder1Data.FeederPhaseBData.PhaseAngle = 0x10EC ;
      PeriodicReport->Feeder1Data.FeederPhaseBData.MeanActivePower = 0x10F0 ;
      PeriodicReport->Feeder1Data.FeederPhaseBData.MeanReactivePower = 0x10F4 ;
      PeriodicReport->Feeder1Data.FeederPhaseBData.ActiveEnergy = 0x10F8 ;
      PeriodicReport->Feeder1Data.FeederPhaseBData.ReactiveEnergy = 0x10FC ;
      PeriodicReport->Feeder1Data.FeederPhaseBData.TotalActiveHarmonicContent = 0x1100 ;
      PeriodicReport->Feeder1Data.FeederPhaseBData.TotalReactiveHarmonicContent = 0x1104 ;
      PeriodicReport->Feeder1Data.FeederPhaseBData.TotalHarmonicDistortion = 0x1108 ;
      PeriodicReport->Feeder1Data.FeederPhaseCData.MeanCurrent = 0x110C ;
      PeriodicReport->Feeder1Data.FeederPhaseCData.MaxCurrent = 0x1110 ;
      PeriodicReport->Feeder1Data.FeederPhaseCData.MinCurrent = 0x1114 ;
      PeriodicReport->Feeder1Data.FeederPhaseCData.PhaseAngle = 0x1118 ;
      PeriodicReport->Feeder1Data.FeederPhaseCData.MeanActivePower = 0x111C ;
      PeriodicReport->Feeder1Data.FeederPhaseCData.MeanReactivePower = 0x1120 ;
      PeriodicReport->Feeder1Data.FeederPhaseCData.ActiveEnergy = 0x1124 ;
      PeriodicReport->Feeder1Data.FeederPhaseCData.ReactiveEnergy = 0x1128 ;
      PeriodicReport->Feeder1Data.FeederPhaseCData.TotalActiveHarmonicContent = 0x112C ;
      PeriodicReport->Feeder1Data.FeederPhaseCData.TotalReactiveHarmonicContent = 0x1130 ;
      PeriodicReport->Feeder1Data.FeederPhaseCData.TotalHarmonicDistortion = 0x1134 ;
      PeriodicReport->Feeder1Data.NeutralMeanMeasuredCurrent = 0x1138 ;
      PeriodicReport->Feeder1Data.NeutralMaxMeasuredCurrent = 0x113C ;
      PeriodicReport->Feeder1Data.NeutralMinMeasuredCurrent = 0x1140 ;

      PeriodicReport->Feeder2Data.FeederPhaseAData.MeanCurrent = 0x1144 ;
      PeriodicReport->Feeder2Data.FeederPhaseAData.MaxCurrent = 0x1148 ;
      PeriodicReport->Feeder2Data.FeederPhaseAData.MinCurrent = 0x114C ;
      PeriodicReport->Feeder2Data.FeederPhaseAData.PhaseAngle = 0x1150 ;
      PeriodicReport->Feeder2Data.FeederPhaseAData.MeanActivePower = 0x1154 ;
      PeriodicReport->Feeder2Data.FeederPhaseAData.MeanReactivePower = 0x1158 ;
      PeriodicReport->Feeder2Data.FeederPhaseAData.ActiveEnergy = 0x115C ;
      PeriodicReport->Feeder2Data.FeederPhaseAData.ReactiveEnergy = 0x1160 ;
      PeriodicReport->Feeder2Data.FeederPhaseAData.TotalActiveHarmonicContent = 0x1164 ;
      PeriodicReport->Feeder2Data.FeederPhaseAData.TotalReactiveHarmonicContent = 0x1168 ;
      PeriodicReport->Feeder2Data.FeederPhaseAData.TotalHarmonicDistortion = 0x116C ;
      PeriodicReport->Feeder2Data.FeederPhaseBData.MeanCurrent = 0x1170 ;
      PeriodicReport->Feeder2Data.FeederPhaseBData.MaxCurrent = 0x1174 ;
      PeriodicReport->Feeder2Data.FeederPhaseBData.MinCurrent = 0x1178 ;
      PeriodicReport->Feeder2Data.FeederPhaseBData.PhaseAngle = 0x117C ;
      PeriodicReport->Feeder2Data.FeederPhaseBData.MeanActivePower = 0x1180 ;
      PeriodicReport->Feeder2Data.FeederPhaseBData.MeanReactivePower = 0x1184 ;
      PeriodicReport->Feeder2Data.FeederPhaseBData.ActiveEnergy = 0x1188 ;
      PeriodicReport->Feeder2Data.FeederPhaseBData.ReactiveEnergy = 0x118C ;
      PeriodicReport->Feeder2Data.FeederPhaseBData.TotalActiveHarmonicContent = 0x1190 ;
      PeriodicReport->Feeder2Data.FeederPhaseBData.TotalReactiveHarmonicContent = 0x1194 ;
      PeriodicReport->Feeder2Data.FeederPhaseBData.TotalHarmonicDistortion = 0x1198 ;
      PeriodicReport->Feeder2Data.FeederPhaseCData.MeanCurrent = 0x119C ;
      PeriodicReport->Feeder2Data.FeederPhaseCData.MaxCurrent = 0x11A0 ;
      PeriodicReport->Feeder2Data.FeederPhaseCData.MinCurrent = 0x11A4 ;
      PeriodicReport->Feeder2Data.FeederPhaseCData.PhaseAngle = 0x11A8 ;
      PeriodicReport->Feeder2Data.FeederPhaseCData.MeanActivePower = 0x11AC ;
      PeriodicReport->Feeder2Data.FeederPhaseCData.MeanReactivePower = 0x11B0 ;
      PeriodicReport->Feeder2Data.FeederPhaseCData.ActiveEnergy = 0x11B4 ;
      PeriodicReport->Feeder2Data.FeederPhaseCData.ReactiveEnergy = 0x11B8 ;
      PeriodicReport->Feeder2Data.FeederPhaseCData.TotalActiveHarmonicContent = 0x11BC ;
      PeriodicReport->Feeder2Data.FeederPhaseCData.TotalReactiveHarmonicContent = 0x11C0 ;
      PeriodicReport->Feeder2Data.FeederPhaseCData.TotalHarmonicDistortion = 0x11C4 ;
      PeriodicReport->Feeder2Data.NeutralMeanMeasuredCurrent = 0x11C8 ;
      PeriodicReport->Feeder2Data.NeutralMaxMeasuredCurrent = 0x11CC ;
      PeriodicReport->Feeder2Data.NeutralMinMeasuredCurrent = 0x11D0 ;

      PeriodicReport->Feeder3Data.FeederPhaseAData.MeanCurrent = 0x11D4 ;
      PeriodicReport->Feeder3Data.FeederPhaseAData.MaxCurrent = 0x11D8 ;
      PeriodicReport->Feeder3Data.FeederPhaseAData.MinCurrent = 0x11DC ;
      PeriodicReport->Feeder3Data.FeederPhaseAData.PhaseAngle = 0x11E0 ;
      PeriodicReport->Feeder3Data.FeederPhaseAData.MeanActivePower = 0x11E4 ;
      PeriodicReport->Feeder3Data.FeederPhaseAData.MeanReactivePower = 0x11E8 ;
      PeriodicReport->Feeder3Data.FeederPhaseAData.ActiveEnergy = 0x11EC ;
      PeriodicReport->Feeder3Data.FeederPhaseAData.ReactiveEnergy = 0x11F0 ;
      PeriodicReport->Feeder3Data.FeederPhaseAData.TotalActiveHarmonicContent = 0x11F4 ;
      PeriodicReport->Feeder3Data.FeederPhaseAData.TotalReactiveHarmonicContent = 0x11F8 ;
      PeriodicReport->Feeder3Data.FeederPhaseAData.TotalHarmonicDistortion = 0x11FC ;
      PeriodicReport->Feeder3Data.FeederPhaseBData.MeanCurrent = 0x1200 ;
      PeriodicReport->Feeder3Data.FeederPhaseBData.MaxCurrent = 0x1204 ;
      PeriodicReport->Feeder3Data.FeederPhaseBData.MinCurrent = 0x1208 ;
      PeriodicReport->Feeder3Data.FeederPhaseBData.PhaseAngle = 0x120C ;
      PeriodicReport->Feeder3Data.FeederPhaseBData.MeanActivePower = 0x1210 ;
      PeriodicReport->Feeder3Data.FeederPhaseBData.MeanReactivePower = 0x1214 ;
      PeriodicReport->Feeder3Data.FeederPhaseBData.ActiveEnergy = 0x1218 ;
      PeriodicReport->Feeder3Data.FeederPhaseBData.ReactiveEnergy = 0x121C ;
      PeriodicReport->Feeder3Data.FeederPhaseBData.TotalActiveHarmonicContent = 0x1220 ;
      PeriodicReport->Feeder3Data.FeederPhaseBData.TotalReactiveHarmonicContent = 0x1224 ;
      PeriodicReport->Feeder3Data.FeederPhaseBData.TotalHarmonicDistortion = 0x1228 ;
      PeriodicReport->Feeder3Data.FeederPhaseCData.MeanCurrent = 0x122C ;
      PeriodicReport->Feeder3Data.FeederPhaseCData.MaxCurrent = 0x1230 ;
      PeriodicReport->Feeder3Data.FeederPhaseCData.MinCurrent = 0x1234 ;
      PeriodicReport->Feeder3Data.FeederPhaseCData.PhaseAngle = 0x1238 ;
      PeriodicReport->Feeder3Data.FeederPhaseCData.MeanActivePower = 0x123C ;
      PeriodicReport->Feeder3Data.FeederPhaseCData.MeanReactivePower = 0x1240 ;
      PeriodicReport->Feeder3Data.FeederPhaseCData.ActiveEnergy = 0x1244 ;
      PeriodicReport->Feeder3Data.FeederPhaseCData.ReactiveEnergy = 0x1248 ;
      PeriodicReport->Feeder3Data.FeederPhaseCData.TotalActiveHarmonicContent = 0x124C ;
      PeriodicReport->Feeder3Data.FeederPhaseCData.TotalReactiveHarmonicContent = 0x1250 ;
      PeriodicReport->Feeder3Data.FeederPhaseCData.TotalHarmonicDistortion = 0x1254 ;
      PeriodicReport->Feeder3Data.NeutralMeanMeasuredCurrent = 0x1258 ;
      PeriodicReport->Feeder3Data.NeutralMaxMeasuredCurrent = 0x125C ;
      PeriodicReport->Feeder3Data.NeutralMinMeasuredCurrent = 0x1260 ;

      PeriodicReport->Feeder4Data.FeederPhaseAData.MeanCurrent = 0x1264 ;
      PeriodicReport->Feeder4Data.FeederPhaseAData.MaxCurrent = 0x1268 ;
      PeriodicReport->Feeder4Data.FeederPhaseAData.MinCurrent = 0x126C ;
      PeriodicReport->Feeder4Data.FeederPhaseAData.PhaseAngle = 0x1270 ;
      PeriodicReport->Feeder4Data.FeederPhaseAData.MeanActivePower = 0x1274 ;
      PeriodicReport->Feeder4Data.FeederPhaseAData.MeanReactivePower = 0x1278 ;
      PeriodicReport->Feeder4Data.FeederPhaseAData.ActiveEnergy = 0x127C ;
      PeriodicReport->Feeder4Data.FeederPhaseAData.ReactiveEnergy = 0x1280 ;
      PeriodicReport->Feeder4Data.FeederPhaseAData.TotalActiveHarmonicContent = 0x1284 ;
      PeriodicReport->Feeder4Data.FeederPhaseAData.TotalReactiveHarmonicContent = 0x1288 ;
      PeriodicReport->Feeder4Data.FeederPhaseAData.TotalHarmonicDistortion = 0x128C ;
      PeriodicReport->Feeder4Data.FeederPhaseBData.MeanCurrent = 0x1290 ;
      PeriodicReport->Feeder4Data.FeederPhaseBData.MaxCurrent = 0x1294 ;
      PeriodicReport->Feeder4Data.FeederPhaseBData.MinCurrent = 0x1298 ;
      PeriodicReport->Feeder4Data.FeederPhaseBData.PhaseAngle = 0x129C ;
      PeriodicReport->Feeder4Data.FeederPhaseBData.MeanActivePower = 0x12A0 ;
      PeriodicReport->Feeder4Data.FeederPhaseBData.MeanReactivePower = 0x12A4 ;
      PeriodicReport->Feeder4Data.FeederPhaseBData.ActiveEnergy = 0x12A8 ;
      PeriodicReport->Feeder4Data.FeederPhaseBData.ReactiveEnergy = 0x12AC ;
      PeriodicReport->Feeder4Data.FeederPhaseBData.TotalActiveHarmonicContent = 0x12B0 ;
      PeriodicReport->Feeder4Data.FeederPhaseBData.TotalReactiveHarmonicContent = 0x12B4 ;
      PeriodicReport->Feeder4Data.FeederPhaseBData.TotalHarmonicDistortion = 0x12B8 ;
      PeriodicReport->Feeder4Data.FeederPhaseCData.MeanCurrent = 0x12BC ;
      PeriodicReport->Feeder4Data.FeederPhaseCData.MaxCurrent = 0x12C0 ;
      PeriodicReport->Feeder4Data.FeederPhaseCData.MinCurrent = 0x12C4 ;
      PeriodicReport->Feeder4Data.FeederPhaseCData.PhaseAngle = 0x12C8 ;
      PeriodicReport->Feeder4Data.FeederPhaseCData.MeanActivePower = 0x12CC ;
      PeriodicReport->Feeder4Data.FeederPhaseCData.MeanReactivePower = 0x12D0 ;
      PeriodicReport->Feeder4Data.FeederPhaseCData.ActiveEnergy = 0x12D4 ;
      PeriodicReport->Feeder4Data.FeederPhaseCData.ReactiveEnergy = 0x12D8 ;
      PeriodicReport->Feeder4Data.FeederPhaseCData.TotalActiveHarmonicContent = 0x12DC ;
      PeriodicReport->Feeder4Data.FeederPhaseCData.TotalReactiveHarmonicContent = 0x12E0 ;
      PeriodicReport->Feeder4Data.FeederPhaseCData.TotalHarmonicDistortion = 0x12E4 ;
      PeriodicReport->Feeder4Data.NeutralMeanMeasuredCurrent = 0x12E8 ;
      PeriodicReport->Feeder4Data.NeutralMaxMeasuredCurrent = 0x12EC ;
      PeriodicReport->Feeder4Data.NeutralMinMeasuredCurrent = 0x12F0 ;

      PeriodicReport->Feeder5Data.FeederPhaseAData.MeanCurrent = 0x12F4 ;
      PeriodicReport->Feeder5Data.FeederPhaseAData.MaxCurrent = 0x12F8 ;
      PeriodicReport->Feeder5Data.FeederPhaseAData.MinCurrent = 0x12FC ;
      PeriodicReport->Feeder5Data.FeederPhaseAData.PhaseAngle = 0x1300 ;
      PeriodicReport->Feeder5Data.FeederPhaseAData.MeanActivePower = 0x1304 ;
      PeriodicReport->Feeder5Data.FeederPhaseAData.MeanReactivePower = 0x1308 ;
      PeriodicReport->Feeder5Data.FeederPhaseAData.ActiveEnergy = 0x130C ;
      PeriodicReport->Feeder5Data.FeederPhaseAData.ReactiveEnergy = 0x1310 ;
      PeriodicReport->Feeder5Data.FeederPhaseAData.TotalActiveHarmonicContent = 0x1314 ;
      PeriodicReport->Feeder5Data.FeederPhaseAData.TotalReactiveHarmonicContent = 0x1318 ;
      PeriodicReport->Feeder5Data.FeederPhaseAData.TotalHarmonicDistortion = 0x131C ;
      PeriodicReport->Feeder5Data.FeederPhaseBData.MeanCurrent = 0x1320 ;
      PeriodicReport->Feeder5Data.FeederPhaseBData.MaxCurrent = 0x1324 ;
      PeriodicReport->Feeder5Data.FeederPhaseBData.MinCurrent = 0x1328 ;
      PeriodicReport->Feeder5Data.FeederPhaseBData.PhaseAngle = 0x132C ;
      PeriodicReport->Feeder5Data.FeederPhaseBData.MeanActivePower = 0x1330 ;
      PeriodicReport->Feeder5Data.FeederPhaseBData.MeanReactivePower = 0x1334 ;
      PeriodicReport->Feeder5Data.FeederPhaseBData.ActiveEnergy = 0x1338 ;
      PeriodicReport->Feeder5Data.FeederPhaseBData.ReactiveEnergy = 0x133C ;
      PeriodicReport->Feeder5Data.FeederPhaseBData.TotalActiveHarmonicContent = 0x1340 ;
      PeriodicReport->Feeder5Data.FeederPhaseBData.TotalReactiveHarmonicContent = 0x1344 ;
      PeriodicReport->Feeder5Data.FeederPhaseBData.TotalHarmonicDistortion = 0x1348 ;
      PeriodicReport->Feeder5Data.FeederPhaseCData.MeanCurrent = 0x134C ;
      PeriodicReport->Feeder5Data.FeederPhaseCData.MaxCurrent = 0x1350 ;
      PeriodicReport->Feeder5Data.FeederPhaseCData.MinCurrent = 0x1354 ;
      PeriodicReport->Feeder5Data.FeederPhaseCData.PhaseAngle = 0x1358 ;
      PeriodicReport->Feeder5Data.FeederPhaseCData.MeanActivePower = 0x135C ;
      PeriodicReport->Feeder5Data.FeederPhaseCData.MeanReactivePower = 0x1360 ;
      PeriodicReport->Feeder5Data.FeederPhaseCData.ActiveEnergy = 0x1364 ;
      PeriodicReport->Feeder5Data.FeederPhaseCData.ReactiveEnergy = 0x1368 ;
      PeriodicReport->Feeder5Data.FeederPhaseCData.TotalActiveHarmonicContent = 0x136C ;
      PeriodicReport->Feeder5Data.FeederPhaseCData.TotalReactiveHarmonicContent = 0x1370 ;
      PeriodicReport->Feeder5Data.FeederPhaseCData.TotalHarmonicDistortion = 0x1374 ;
      PeriodicReport->Feeder5Data.NeutralMeanMeasuredCurrent = 0x1378 ;
      PeriodicReport->Feeder5Data.NeutralMaxMeasuredCurrent = 0x137C ;
      PeriodicReport->Feeder5Data.NeutralMinMeasuredCurrent = 0x1380 ;
    }
    else
    {

    }
  }

  // Calculate the checksum of the message before it's sent back

  CommonDataBus* KlvHeader = (CommonDataBus*) PacketFromTheMcu ;
  KlvHeader->Checksum.u16Checksum = CalculateKlvChecksum (PacketFromTheMcu) ;

  return DecodeStatus ;
}

/*
 * =============================================================================
 * End of %PM%
 * Classification: Commercial-In-Confidence
 * =============================================================================
 */

