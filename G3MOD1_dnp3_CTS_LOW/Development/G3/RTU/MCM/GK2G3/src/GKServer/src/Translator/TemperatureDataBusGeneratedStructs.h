/*
 * ===========================================================================
 *
 * A U T O G E N E R A T E D   S O U R C E
 * ---------------------------------------
 *
 * Source "CheckForCorruptionRefMdl"
 *
 * Generated @ Wed May 03 09:52:09 2017
 *
 * ===========================================================================
 *
 * Classification: UNCLASSIFIED
 *
 * Name:           %PM%
 * Revision:       %PR%
 * Current Status: %PS%
 *
 * Project            GridKey
 * Title:             CheckForCorruptionRefMdl Autocode
 * Author(s):
 * Modified By:       %AUTHOR%
 * Related documents:
 *
 * Copyright Notice
 * ----------------
 *
 * The information contained in this document is proprietary to Lucy ES unless
 * stated otherwise and is made available in confidence; it must not be used or
 * disclosed without the express written permission Lucy ES. This document may
 * not be copied in whole or in part in any form without the express written
 * consent of Lucy Electric.
 *
 * Public Access: Freedom Of Information Act 2000
 *
 * This document contains trade secrets and/or sensitive commercial information
 * as of the date provided to the original recipient by Lucy Electric and is
 * provided in confidence. Following a request for this information public
 * authorities are to consult with Lucy ES regarding the current releasability
 * of the information prior to the decision to release all or part of this
 * document.Release of this information by a public authority may constitute an
 * actionable breach of confidence.
 *
 * UK Origin
 *
 * Lucy Electric Limited
 * Argent Court Basildon Essex SS15 6TH England
 * Telephone: 01268 850000
 *
 * ===========================================================================
 * Description
 * ---------------------------------------------------------------------------
 *
 * This file was automatically generated from the autocode generation model
 * for CheckForCorruptionRefMdl.
 *
 * Generated Filename:    TemperatureDataBusGeneratedStructs.h
 * Generated On:          Wed May 03 09:52:09 2017
 *
 * Model Version:         1.205
 * RTW File Version:      8.0 (R2011a) 09-Mar-2011
 * RTW File generated on: Wed May 03 09:52:08 2017
 * TLC Version:           8.0 (Feb  3 2011)
 *
 * ===========================================================================
 * Version History
 * ---------------------------------------------------------------------------
 *
 * %PRT%
 * %PL%
 *
 * ===========================================================================
 */

#ifndef RTW_HEADER_TemperatureDataBusGeneratedStructs_h_
#define RTW_HEADER_TemperatureDataBusGeneratedStructs_h_
#include "rtwtypes.h"

/* TemperatureDataBus */
typedef struct {
  uint8_T DataKey;
  uint8_T DataLength;
  int16_T MeanBusbarTemperature;
  int16_T MaxBusbarTemperature;
  int16_T MinBusbarTemperature;
  int16_T MeanTransformerTemperature;
  int16_T MaxTransaformerTemperature;
  int16_T MinTransformerTemperature;
  int16_T MeanMcuTemperature;
  int16_T MaxMcuTemperature;
  int16_T MinMcuTemperature;
} TemperatureDataBus;

#endif                                 /* RTW_HEADER_TemperatureDataBusGeneratedStructs_h_ */

/*
 * ===========================================================================
 * End of TemperatureDataBusGeneratedStructs.h
 * Classification: UNCLASSIFIED
 * ===========================================================================
 */
