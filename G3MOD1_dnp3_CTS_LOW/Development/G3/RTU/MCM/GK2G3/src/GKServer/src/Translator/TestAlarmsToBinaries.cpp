#include "stdafx.h"
#include "CppUnitTest.h"

extern "C" {

#include "GK_Gk2G3Translator.c"

  uint8_T PacketFromMcu [1024] ;
  uint8_T PacketToMcu [32] ;
  uint16_T PacketLength ;

  uint16_T G3Binaries [sizeof_GK_enuBinaries] ;
  real32_T G3Analogues [sizeof_GK_enuAnalogues] ;
}

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

#define ASSERT_L1VoltLowLow 0
#define ASSERT_L1VoltLow 1
#define ASSERT_L1VoltHigh 2
#define ASSERT_L1VoltHighHigh 3
#define ASSERT_L2VoltLowLow 4
#define ASSERT_L2VoltLow 5
#define ASSERT_L2VoltHigh 6
#define ASSERT_L2VoltHighHigh 7
#define ASSERT_L3VoltLowLow 8
#define ASSERT_L3VoltLow 9
#define ASSERT_L3VoltHigh 10
#define ASSERT_L3VoltHighHigh 11
#define ASSERT_L1CurrentHighHigh 12
#define ASSERT_L1CurrentHigh 13
#define ASSERT_L1CurrentLow 14
#define ASSERT_L1CurrentNegative 15
#define ASSERT_L2CurrentHighHigh 16
#define ASSERT_L2CurrentHigh 17
#define ASSERT_L2CurrentLow 18
#define ASSERT_L2CurrentNegative 19
#define ASSERT_L3CurrentHighHigh 20
#define ASSERT_L3CurrentHigh 21
#define ASSERT_L3CurrentLow 22
#define ASSERT_L3CurrentNegative 23
#define ASSERT_no_alarm 24

boolean_T AlarmAssertions [25] = { false } ;

uint8_T CallSimulateMcu520 (
  boolean_T UseAddressOffsets,
  uint8_T AlarmCategory,
  uint8_T AlarmToAssert,
  uint8_T* PacketFromTheMcu)
{
  memset (AlarmAssertions, 0, sizeof (AlarmAssertions)) ; 
  if (AlarmToAssert < ASSERT_no_alarm) AlarmAssertions [AlarmToAssert] = true ;

  return GK_SimulateMcu520 (
    UseAddressOffsets,
    AlarmCategory,
    AlarmAssertions [ASSERT_L1VoltLowLow],
    AlarmAssertions [ASSERT_L1VoltLow],
    AlarmAssertions [ASSERT_L1VoltHigh],
    AlarmAssertions [ASSERT_L1VoltHighHigh],
    AlarmAssertions [ASSERT_L2VoltLowLow],
    AlarmAssertions [ASSERT_L2VoltLow],
    AlarmAssertions [ASSERT_L2VoltHigh],
    AlarmAssertions [ASSERT_L2VoltHighHigh],
    AlarmAssertions [ASSERT_L3VoltLowLow],
    AlarmAssertions [ASSERT_L3VoltLow],
    AlarmAssertions [ASSERT_L3VoltHigh],
    AlarmAssertions [ASSERT_L3VoltHighHigh],
    AlarmAssertions [ASSERT_L1CurrentHighHigh],
    AlarmAssertions [ASSERT_L1CurrentHigh],
    AlarmAssertions [ASSERT_L1CurrentLow],
    AlarmAssertions [ASSERT_L1CurrentNegative],
    AlarmAssertions [ASSERT_L2CurrentHighHigh],
    AlarmAssertions [ASSERT_L2CurrentHigh],
    AlarmAssertions [ASSERT_L2CurrentLow],
    AlarmAssertions [ASSERT_L2CurrentNegative],
    AlarmAssertions [ASSERT_L3CurrentHighHigh],
    AlarmAssertions [ASSERT_L3CurrentHigh],
    AlarmAssertions [ASSERT_L3CurrentLow],
    AlarmAssertions [ASSERT_L3CurrentNegative],
    PacketFromTheMcu) ;
}

namespace TestGk2G3Translator
{
  TEST_CLASS(TestAlarmsToBinaries)
  {
  public:
    
    TEST_METHOD (TestInvalidFeederAlarmRefCatRejection)
    {
      Logger::WriteMessage ("Make sure only valid feeder alarms are encoded") ;

      uint16_T AlarmRef = 0 ;

      while (AlarmRef < ASSERT_L1CurrentHighHigh)
      {
        uint8_T DecodeStatus =
          CallSimulateMcu520 (
          false,
          ALARM_CAT_FEEDER_1,
          uint8_T (AlarmRef),
          PacketFromMcu) ;

        Assert::AreEqual (
          float (GK_DECODE_INVALID_CAT_REF_COMBINATION),
          float (DecodeStatus),
          float (0),
          L"Should have been a bad combination") ;

        AlarmRef++ ;
      }

      while (AlarmRef < ASSERT_no_alarm)
      {
        uint8_T DecodeStatus =
          CallSimulateMcu520 (
          false,
          ALARM_CAT_FEEDER_1,
          uint8_T (AlarmRef),
          PacketFromMcu) ;

        Assert::AreEqual (
          float (GK_DECODE_SET_FEEDER_ALARM),
          float (DecodeStatus),
          float (0),
          L"Should have been a good combination") ;

        AlarmRef++ ;
      }

      while (AlarmRef <= 255)
      {
        uint8_T DecodeStatus =
          CallSimulateMcu520 (
          false,
          ALARM_CAT_FEEDER_1,
          uint8_T (AlarmRef),
          PacketFromMcu) ;

        Assert::AreEqual (
          float (GK_DECODE_INVALID_CAT_REF_COMBINATION),
          float (DecodeStatus),
          float (0),
          L"Should have been an invalid alarm") ;

        AlarmRef++ ;
      }
    }

    TEST_METHOD (TestInvalidBusbarAlarmRefCatRejection)
    {
      Logger::WriteMessage ("Make sure only valid busbar alarms are encoded") ;

      uint16_T AlarmRef = 0 ;

      while (AlarmRef < ASSERT_no_alarm)
      {
        uint8_T DecodeStatus =
          CallSimulateMcu520 (
          false,
          ALARM_CAT_BUSBAR,
          uint8_T (AlarmRef),
          PacketFromMcu) ;

        Assert::AreEqual (
          float (GK_DECODE_SET_BUSBAR_ALARM),
          float (DecodeStatus),
          float (0),
          L"Should have been a good combination") ;

        AlarmRef++ ;
      }

      while (AlarmRef <= 255)
      {
        uint8_T DecodeStatus =
          CallSimulateMcu520 (
          false,
          ALARM_CAT_FEEDER_1,
          uint8_T (AlarmRef),
          PacketFromMcu) ;

        Assert::AreEqual (
          float (GK_DECODE_INVALID_CAT_REF_COMBINATION),
          float (DecodeStatus),
          float (0),
          L"Should have been an invalid alarm") ;

        AlarmRef++ ;
      }
    }

    TEST_METHOD (TestNoAlarmsSet)
    {
      Logger::WriteMessage ("Checking unit offline flag") ;

      time_t RawTime ;
      time (&RawTime) ;

      ((PeriodicReportBus*) PacketFromMcu)->FileReference.FileReferenceNumber = 123 ;
      
      uint8_T DecodeStatus =
        GK_TranslateGridkeyMessages (
          RawTime,
          PacketFromMcu,
          PacketToMcu,
          &PacketLength,
          G3Binaries,
          G3Analogues) ;

      Assert::AreEqual (
        float (true),
        float ((G3Binaries [GK_binUnitFlags] & GK_BIN_UNIT_DATA_CENTRE_COMMS_ACTIVE) != 0),
        float (0),
        L"Expected DC comms active as message index is different") ;

      DecodeStatus =
        GK_TranslateGridkeyMessages (
          RawTime,
          PacketFromMcu,
          PacketToMcu,
          &PacketLength,
          G3Binaries,
          G3Analogues) ;

      Assert::AreEqual (
        float (false),
        float ((G3Binaries [GK_binUnitFlags] & GK_BIN_UNIT_DATA_CENTRE_COMMS_ACTIVE) != 0),
        float (0),
        L"Expected DC comms inactive as message index hasn't changed") ;
    }

  };
}