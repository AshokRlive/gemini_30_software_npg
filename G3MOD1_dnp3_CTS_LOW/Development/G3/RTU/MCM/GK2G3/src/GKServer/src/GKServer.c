/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:GKServer.c
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   2 May 2017     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include <modbus.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "Logger.h"
#include "GlobalArgs.h"
#include "GKData.h"
#include "GKServer.h"
#include "GKDataCentre.h"
#include "GK_Gk2G3Translator.h"
#include "MBSlave.h"
#include "Socket.h"
#include "Utils.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static lu_int32_t s_gk_request_handler(
									const lu_int32_t deviceIndex,
                                    const lu_uint8_t* requestPtr,
                                    const lu_int32_t requestSize,
                                    lu_uint8_t *replyPtr,
                                    lu_int32_t* replySize);

static void s_gk_connchg_handler(void* userContext, lu_int32_t existingConnNum);
#if PROTOTYPE_DEV_TEST
static void s_get_time(gk520_time_t* gktime);
#endif
static lu_int32_t s_copy_gkdata_to_mbmapping(gk520_data_t* gkdata, modbus_mapping_t *mbmapping);


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
extern pthread_mutex_t  g_mb_mapping_lock;


gk520_data_t            g_gk_data;

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static time_t cur_time; // Current time of RTU


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
lu_int32_t gkserver_run(void)
{
    void* context;
    context = socket_server_init(NULL,
                    g_conf.gkdev_port,
                    &s_gk_request_handler,
                    &s_gk_connchg_handler,
                    g_conf.gkdev_number);

    // Init gk data
    memset(&g_gk_data, 0, sizeof(g_gk_data));

    // Handle socket request
    while(1)
    {
        socket_server_process(context);
    }

    socket_server_shudown(context);
}



/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */
static lu_int32_t s_gk_request_handler(
									const lu_int32_t deviceIndex,
                                    const lu_uint8_t* requestPtr,
                                    const lu_int32_t requestSize,
                                    lu_uint8_t *replyPtr,
                                    lu_int32_t* replySize)
{
    uint16_T tempLen;
    uint8_T err;

    // Get system time
    time(&cur_time);

    // Forward request to data centre.
    if(g_conf.dc_active)
    {
        gkdc_send_data(g_conf.dc_ipaddr,g_conf.dc_port, requestPtr, requestSize, NULL, 0);
    }

    // Decode request message to gk data.
    err = GK_TranslateGridkeyMessages(
                    cur_time,
                    (uint8_T*)requestPtr,
                    (uint8_T*)replyPtr,
                    &tempLen,
                    (uint16_T*)(g_gk_data.binary),
                    (real32_T*)(g_gk_data.analogue)
            );
    *replySize = tempLen;

    if(err == 0)
    {
        // Copy gk data to MB slave mapping.
        mbslave_context_t* mb_ctxt = g_mb_ctxt[deviceIndex];
        s_copy_gkdata_to_mbmapping(&g_gk_data, mb_ctxt->mapping);
    }

    return 0;
}

static void s_gk_connchg_handler(void* userContext, lu_int32_t existingConnNum)
{
    LU_UNUSED(userContext);
    loginfo("GKServer >> connection num:%d", existingConnNum);
}

static lu_int32_t s_copy_gkdata_to_mbmapping(gk520_data_t* gkdata, modbus_mapping_t* mbmapping)
{
    lu_int32_t i,j;
    if(mbmapping == NULL)
    {
       logerr("modbus mapping not allocated!");
       return -1;
    }

    pthread_mutex_lock(&g_mb_mapping_lock);

    // Copying analogue data
    uint32_t* inputreg = (uint32_t*)(mbmapping->tab_input_registers);
    for (i = 0; i < ANALOGUE_COUNT; ++i)
    {
        *inputreg = gkdata->analogue[i];
        inputreg++;
    }

    // Copying binary data
    uint8_t* inputbits = (uint8_t*)(mbmapping->tab_input_bits);
    for (i = 0; i < BINARY_COUNT; ++i)
    {
    	for (j = 0; j <16; j++)
    	{
    		*inputbits = (gkdata->binary[i] >> j) & 0x01;
    		//printf("%d",*inputbits);
    		inputbits++;
    	}
    	//printf("\n");
    }

    if(g_args.verbose)
    {
        dump_data("Binaries", mbmapping->tab_input_bits, mbmapping->nb_input_bits);
        dump_data("Analogue", mbmapping->tab_input_registers, mbmapping->nb_input_registers*2);
    }

    pthread_mutex_unlock(&g_mb_mapping_lock);

    return 0;
}



/*
 *********************** End of file ******************************************
 */
