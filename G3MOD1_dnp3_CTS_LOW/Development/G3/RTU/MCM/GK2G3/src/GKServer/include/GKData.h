/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:GKData.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   9 May 2017     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef RTU_MCM_GK2G3_SRC_GKSERVER_INCLUDE_GKDATA_H_
#define RTU_MCM_GK2G3_SRC_GKSERVER_INCLUDE_GKDATA_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "../src/Translator/GK_Gk2G3Translator.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define ANALOGUE_COUNT sizeof_GK_enuAnalogues
#define BINARY_COUNT sizeof_GK_enuBinaries

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
typedef lu_uint32_t analogue_t;
typedef lu_uint16_t binary_t;

#define ANALOGUE_SIZE_BYTES sizeof(analogue_t)
#define BINARY_SIZE_BYTES   sizeof(binary_t)

typedef struct {
    lu_uint32_t epoch;
}gk520_time_t;

typedef struct
{
    gk520_time_t    lastUpdateTime;
    analogue_t      analogue[ANALOGUE_COUNT];
    binary_t        binary[BINARY_COUNT];
}gk520_data_t;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
extern gk520_data_t g_gk_data;

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */



#endif /* RTU_MCM_GK2G3_SRC_GKSERVER_INCLUDE_GKDATA_H_ */

/*
 *********************** End of file ******************************************
 */
