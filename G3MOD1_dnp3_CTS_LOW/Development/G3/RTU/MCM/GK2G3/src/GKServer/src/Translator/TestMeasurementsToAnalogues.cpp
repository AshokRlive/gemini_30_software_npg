#include "stdafx.h"
#include "CppUnitTest.h"
#include <ctime>

extern "C" {

#include "GK_Gk2G3Translator.h"

  extern uint8_T PacketFromMcu [1024] ;
  extern uint8_T PacketToMcu [32] ;
  extern uint16_T PacketLength ;

  extern uint16_T G3Binaries [sizeof_GK_enuBinaries] ;
  extern real32_T G3Analogues [sizeof_GK_enuAnalogues] ;

}

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

extern uint8_T CallSimulateMcu520 (
  boolean_T UseAddressOffsets,
  uint8_T AlarmCategory,
  uint8_T AlarmToAssert,
  uint8_T* PacketFromTheMcu) ;

namespace TestGk2G3Translator
{
  TEST_CLASS(TestMeasurementsToAnalogues)
  {
  public:
    
    TEST_METHOD(TestMeasurementsAddressOffsets)
    {
      Logger::WriteMessage ("Check that analogues are set based on address offset") ;

      uint8_T DecodeSuccess =
        CallSimulateMcu520 (
        true,
        0,
        0xFF,
        PacketFromMcu) ;

      Assert::AreEqual (
        float (GK_DECODE_WAS_SUCCESSFUL),
        float (DecodeSuccess),
        float (0),
        L"Expected successful encoding") ;

      time_t RawTime ;
      time (&RawTime) ;

      uint8_T DecodeStatus =
        GK_TranslateGridkeyMessages (
        RawTime,
        PacketFromMcu,
        PacketToMcu,
        &PacketLength,
        G3Binaries,
        G3Analogues) ;

      Assert::AreEqual (
        float (GK_DECODE_WAS_SUCCESSFUL),
        float (DecodeSuccess),
        float (0),
        L"Expected successful encoding") ;

      uint16_T AddressOffset ;

      for (AddressOffset = 0 ; AddressOffset < sizeof_GK_enuAnalogues ; AddressOffset++)
      {
        char_T ErrorMsg [] = "Checking entry 0000" ;
        sprintf (ErrorMsg, "Checking entry %d", AddressOffset) ;
        Logger::WriteMessage (ErrorMsg) ;

        Assert::AreNotEqual (
          float (0),
          float (G3Analogues [AddressOffset]),
          float (0),
          L"Data set to zero") ;

        // Special cases for number of feeders and reporting period

        if (AddressOffset == 1)
        {
          Assert::AreEqual (
            float (5),
            float (G3Analogues [AddressOffset]),
            float (0),
            L"Expect 5 feeders active") ;
        }
        else if (AddressOffset == 2)
        {
          Assert::AreEqual (
            float (1),
            float (G3Analogues [AddressOffset]),
            float (0),
            L"Expect reporting period enum to be 1") ;
        }
        else
        {
          Assert::AreEqual (
            float (AddressOffset * 4 + 0x1000),
            float (G3Analogues [AddressOffset]),
            float (0),
            L"Data differs from expected (address-based)") ;
        }
      }
    }

  };
}