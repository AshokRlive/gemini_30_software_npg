/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:GKDataCentre.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   9 Oct 2017     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef SRC_GKDATACENTRE_INCLUDE_GKDATACENTRE_H_
#define SRC_GKDATACENTRE_INCLUDE_GKDATACENTRE_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/**
 * Sends data to Data Centre.
 * Return the size of the received data. -1 if there is an error.
 */
lu_int32_t gkdc_send_data(
                const lu_char_t* ipaddr,
                const lu_int32_t port,
                const lu_uint8_t* sendbuf,
                const lu_int32_t sendsize,
                lu_uint8_t* recvbuf,
                const lu_int32_t recvsize);


#endif /* SRC_GKDATACENTRE_INCLUDE_GKDATACENTRE_H_ */

/*
 *********************** End of file ******************************************
 */
