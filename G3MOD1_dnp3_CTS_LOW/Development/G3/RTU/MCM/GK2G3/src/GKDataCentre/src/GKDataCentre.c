/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:GKDataCentre.c
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   9 Oct 2017     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h>

// inet_aton include
#include <sys/select.h>
#include <netinet/in.h>
#include <arpa/inet.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "GKDataCentre.h"
#include "Logger.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */





/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

lu_int32_t gkdc_send_data(
                const lu_char_t* ipaddr,
                const lu_int32_t port,
                const lu_uint8_t* sendbuf,
                const lu_int32_t sendsize,
                lu_uint8_t* recvbuf,
                const lu_int32_t recvsize)
{
    static lu_int32_t sockid = -1;
    static struct sockaddr_in si_target;
    static lu_int32_t slen = sizeof(si_target);

    lu_int32_t sent = 0; // sent size
    lu_int32_t recv = 0; // received size

    loginfo("GKDataCentre >>> forwarding data to Data Centre <<< ");

    /* Create socket*/
    if(sockid <= 0)
    {
        if ((sockid = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
        {
            perror("socket");
            return -1;
        }

        /* Socket created successfully, now prepare the socket address*/
         memset((char *) &si_target, 0, sizeof(si_target));
         si_target.sin_family = AF_INET;
         si_target.sin_port = htons(port);
         if (inet_aton(ipaddr, &si_target.sin_addr) == 0)
         {
             logerr("GKDataCentre >> failed to resolve IP address:%s", ipaddr);
         }
    }


    /* Send the message*/
    loginfo("GKDataCentre >> sending data...");
    sent = sendto(sockid, sendbuf, sendsize, 0, (struct sockaddr *) &si_target, slen);
    if (sent != sendsize)
    {
        logerr("GKDataCentre >> invalid sent size:%d, expected:%d", sent, sendsize);
        return -1;
    }
    loginfo("GKDataCentre >> sent size:%d ", sent);

    if( recvbuf != NULL)
    {
        /* Receive the reply*/
        struct timeval tv; // timeout
        tv.tv_sec = 1;
        tv.tv_usec = 0;

        loginfo("GKDataCentre >> awaiting reply...");
        fd_set socks;
        FD_ZERO(&socks);
        FD_SET(sockid, &socks);

        if (select(sockid + 1, &socks, NULL, NULL, &tv))
        {
            memset(recvbuf, 0, recvsize);
            recv = recvfrom(sockid, recvbuf, recvsize, 0, NULL, NULL);
            if (recv > 0)
            {
                loginfo("GKDataCentre >> received size:%d",recv);
                loginfo("GKDataCentre >> server replied content:%s ",recvbuf);
            }
            else
           {
               logerr("GKDataCentre >> no reply");
               return -1;
           }
        }
        else
        {
            logerr("GKDataCentre >> reply timeout!");
            return -1;
        }
    }

    loginfo("GKDataCentre >>> end of forwarding data to Data Centre [sockid:%d] <<< ",sockid);
    return recv;
}



/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */



/*
 *********************** End of file ******************************************
 */
