#!/bin/bash
if [ "$#" -ne 1 ]; then
echo "Add arguemnt'-DCROSS_COMPILE=ON' to enable/disable cross compiling".
fi

# ** DEBUG **
# check Debug directory and clean it
if [ -d Debug ]; then
    # Remove Debug contents
    rm -rf Debug/*
else
    # Remove Debug directory
    rm -rf Debug
    # Create Debug directory
    mkdir Debug
fi
# Run cmake and create Debug Makefiles
cd Debug
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ../../src/ -DCMAKE_INSTALL_PREFIX=./install $@

