/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:Main.c
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   2 May 2017     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <ctype.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MCU520Sim.h"
#include "Logger.h"
#include "rtwtypes.h"
#include "Socket.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static lu_int32_t   parse_args(lu_int32_t argc, lu_char_t **argv);
static void         run_console();

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
mcu520sim_args_t g_sim_args;

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static struct option long_options[] = {
            {"target",   required_argument, 0,  't' },
            {"port",     required_argument, 0,  'p' },
            {"verbose",  no_argument,       0,  'v' },
            {"help",     no_argument, 0,  'h' },
            { NULL,      no_argument, NULL, 0 }
        };

static lu_char_t* long_options_desc[] = {
            "target address to be connected(used by mcu520 simulator only)",
            "target port to be connected(used by mcu520 simulator only)",
            "verbose mode",
            "display this message",
            ""
        };

static const char *optString = "t:p:vh?";


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

lu_int32_t main(lu_int32_t argc, lu_char_t **argv) {
    if(parse_args(argc, argv) != 0)
        return -1;

    loginfo("Target:%s:%d",g_sim_args.target_addr,g_sim_args.target_port);

    run_console();
}



/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */
static void display_console_help(void)
{
     printf("\n ---> Menu <---\n");
     printf(" ?. Help\n");
     printf(" 0. Exit \n");
     printf(" 1. Connect\n");
     printf(" 2. Disconnect\n");
     printf(" 3. Request Time\n");
     printf(" 4. Report Alert\n");
     printf("Select an option:");
}


static void run_console()
{
    lu_char_t opt = -1;
    display_console_help();

    while(1)
    {
        scanf("%c", &opt);
        switch( opt )
        {
           case '0':
               loginfo("Exit");
               exit(0);
               break;

           case '1':
               mcu520sim_connect();
               break;

           case '2':
               mcu520sim_disconnect();
               break;

           case '3':
               mcu520sim_send_time_request();
               break;

           case '4':
           {
              uint32_T AlarmCategory = 0xff;
              uint32_T AlarmIndex = 0xff;

              printf("\nAlarmCategory[1-6]:");
              scanf("%u", &AlarmCategory);

              printf("\nAlarmIndex[0-24]:");
              scanf("%u", &AlarmIndex);

              printf("\nYour input alarm category:%u, index:%u\n",AlarmCategory,AlarmIndex);
              mcu520sim_send_alert_report(AlarmCategory, AlarmIndex);
           }
           break;

           case '?':
               display_console_help();
               break;

           default:
               break;
        }
    }
}

static void display_usage(lu_char_t* appname)
{
    fprintf(stderr, "Gridkey device simulator for testing GK2G3 application.\n");
    fprintf(stderr, "Usage: %s [-v] [-t] IP Address [-h] \n  options:\n", appname);

    int i;
    for (i = 0; i < sizeof(long_options)/sizeof(struct option) -1; i++)
    {
        fprintf(stderr, "  --%s -%c %s\n\t%s\n",
                        long_options[i].name,
                        long_options[i].val,
                        long_options[i].has_arg == required_argument?"[Require Argument]":"",
                        long_options_desc[i]);
    }

}


static lu_int32_t parse_args(lu_int32_t argc, lu_char_t **argv)
{

        int c;
        int digit_optind = 0;

        // set default global argument value
           memset(&g_sim_args, 0, sizeof(mcu520sim_args_t));
           g_sim_args.target_addr = LOOPBACK_ADDR;
           g_sim_args.target_port = DEFAULT_GK_PORT;

       while (1) {
            int this_option_optind = optind ? optind : 1;
            int option_index = 0;
           c = getopt_long(argc, argv, optString,
                     long_options, &option_index);
            if (c == -1)
                break;

        switch (c)
        {
            case 'v':
            {
                g_sim_args.verbose = LU_TRUE;
            }
            break;

            case 't':
            {
                static lu_char_t target[30];
                strncpy(target, optarg, 30);
                g_sim_args.target_addr = target;
            }
            break;

            case 'p':
            {
                g_sim_args.target_port = atoi(optarg);
            }
            break;

            case 0: /* long option without a short arg */
            {
                printf("option %s", long_options[option_index].name);
                if (optarg)
                    printf(" with arg %s", optarg);
                printf("\n");
            }
            break;

           case 'h': /* fall-through is intentional */
           case '?':
               display_usage(argv[0]);
               return -1;

           default:
               /* You won't actually get here. */
               printf("?? getopt returned character code 0%o ??\n", c);
               return -1;
            }
        }

       if (optind < argc) {
            printf("non-option ARGV-elements: ");
            while (optind < argc)
                printf("%s ", argv[optind++]);
            printf("\n");
            return -1;
        }
    return 0;
}

/*
 *********************** End of file ******************************************
 */
