/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MCU520Sim.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   9 May 2017     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef RTU_MCM_GK2G3_TEST_SRC_MCU520SIM_INCLUDE_MCU520SIM_H_
#define RTU_MCM_GK2G3_TEST_SRC_MCU520SIM_INCLUDE_MCU520SIM_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "rtwtypes.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define ASSERT_L1VoltLowLow 0
#define ASSERT_L1VoltLow 1
#define ASSERT_L1VoltHigh 2
#define ASSERT_L1VoltHighHigh 3
#define ASSERT_L2VoltLowLow 4
#define ASSERT_L2VoltLow 5
#define ASSERT_L2VoltHigh 6
#define ASSERT_L2VoltHighHigh 7
#define ASSERT_L3VoltLowLow 8
#define ASSERT_L3VoltLow 9
#define ASSERT_L3VoltHigh 10
#define ASSERT_L3VoltHighHigh 11
#define ASSERT_L1CurrentHighHigh 12
#define ASSERT_L1CurrentHigh 13
#define ASSERT_L1CurrentLow 14
#define ASSERT_L1CurrentNegative 15
#define ASSERT_L2CurrentHighHigh 16
#define ASSERT_L2CurrentHigh 17
#define ASSERT_L2CurrentLow 18
#define ASSERT_L2CurrentNegative 19
#define ASSERT_L3CurrentHighHigh 20
#define ASSERT_L3CurrentHigh 21
#define ASSERT_L3CurrentLow 22
#define ASSERT_L3CurrentNegative 23
#define ASSERT_no_alarm 24

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef struct
{
    lu_bool_t verbose;
    lu_bool_t debug_modbus;
    lu_bool_t debug_socket;
    lu_bool_t console;
    lu_char_t *target_addr;
    lu_int32_t target_port;
}mcu520sim_args_t;
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
extern mcu520sim_args_t g_sim_args;

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */
lu_int32_t mcu520sim_connect(void);
lu_int32_t mcu520sim_disconnect(void);
lu_int32_t mcu520sim_send_time_request(void);
lu_int32_t mcu520sim_send_alert_report(uint8_T AlarmCategory, uint8_T AlarmToAssert);



#endif /* RTU_MCM_GK2G3_TEST_SRC_MCU520SIM_INCLUDE_MCU520SIM_H_ */

/*
 *********************** End of file ******************************************
 */
