/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:hat_socket.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   8 Oct 2015     pwang     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef SRC_SOCKET_H_
#define SRC_SOCKET_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#ifdef WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#else
#include <netdb.h>
#include <fcntl.h>
#include <sys/select.h>
#endif

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define DEFAULT_GK_PORT      30502

#define LOOPBACK_ADDR        "127.0.0.1"

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */
#ifdef WIN32
void socket_initWinsock();
#endif


lu_int32_t socket_make_socket(lu_char_t* serverIPAddr, lu_int32_t port);

lu_int32_t socket_close(lu_int32_t sock);

lu_int32_t socket_send_data(lu_int32_t socket, lu_uint8_t* sendBuffer, lu_int32_t sendSize);

lu_int32_t socket_recv_data(lu_int32_t socket, lu_uint8_t* recvBuffer, lu_int32_t bufSize);



#endif /* SRC_SOCKET_H_ */

/*
 *********************** End of file ******************************************
 */
