/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MCU520Sim.c
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   9 May 2017     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <time.h>
#include <string.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MCU520Sim.h"
#include "Socket.h"
#include "lu_types.h"
#include "Logger.h"
#include "GK_Gk2G3Translator.h"
#include "Utils.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define MSG_BUF_SIZE        1024
/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static lu_uint8_t sendbuf[MSG_BUF_SIZE];
static lu_int8_t  rcvBuf[MSG_BUF_SIZE];
static lu_int32_t sockid = 0;

static boolean_T AlarmAssertions[25] = {false};

static struct sockaddr_in si_other;


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

lu_int32_t mcu520sim_connect(void)
{
    if(sockid <= 0)
    {
        loginfo("Opening socket \"%s:%d\"",g_sim_args.target_addr, g_sim_args.target_port);
        sockid = socket_make_socket(g_sim_args.target_addr, g_sim_args.target_port);
        if( sockid > 0)
        {
            loginfo("Connected");
        }
        else
        {
            logerr("Failed to open socket!");
        }
    }

    return sockid > 0 ? 0:-1;
}

lu_int32_t mcu520sim_disconnect(void)
{
    if(sockid > 0)
    {
        socket_close(sockid);
        sockid = 0;
        loginfo("Disconnected!");
    }
    return 0;
}

static lu_int32_t send_buf()
{
    if(socket_send_data(sockid, sendbuf, MSG_BUF_SIZE) > 0)
        socket_recv_data(sockid, rcvBuf, MSG_BUF_SIZE);
   return 0;
}

lu_int32_t mcu520sim_send_time_request(void)
{
    if (sockid <= 0)
    {
        logerr("Not connected!");
        return -1;
    }

    sendbuf[0] = 'T';
    sendbuf[1] = 'I';
    sendbuf[2] = 'M';
    sendbuf[3] = '\0';

    return send_buf();
}


lu_int32_t mcu520sim_send_alert_report(uint8_T AlarmCategory, uint8_T AlarmToAssert)
{
    if (sockid <= 0)
    {
        logerr("Not connected!");
        return -1;
    }

    boolean_T UseAddressOffsets = true;

    memset(sendbuf, 0, MSG_BUF_SIZE);
    memset(AlarmAssertions, 0, sizeof(AlarmAssertions));
    if (AlarmToAssert < ASSERT_no_alarm)
        AlarmAssertions[AlarmToAssert] = true;

    GK_SimulateMcu520(UseAddressOffsets, AlarmCategory,
                    AlarmAssertions[ASSERT_L1VoltLowLow],
                    AlarmAssertions[ASSERT_L1VoltLow],
                    AlarmAssertions[ASSERT_L1VoltHigh],
                    AlarmAssertions[ASSERT_L1VoltHighHigh],
                    AlarmAssertions[ASSERT_L2VoltLowLow],
                    AlarmAssertions[ASSERT_L2VoltLow],
                    AlarmAssertions[ASSERT_L2VoltHigh],
                    AlarmAssertions[ASSERT_L2VoltHighHigh],
                    AlarmAssertions[ASSERT_L3VoltLowLow],
                    AlarmAssertions[ASSERT_L3VoltLow],
                    AlarmAssertions[ASSERT_L3VoltHigh],
                    AlarmAssertions[ASSERT_L3VoltHighHigh],
                    AlarmAssertions[ASSERT_L1CurrentHighHigh],
                    AlarmAssertions[ASSERT_L1CurrentHigh],
                    AlarmAssertions[ASSERT_L1CurrentLow],
                    AlarmAssertions[ASSERT_L1CurrentNegative],
                    AlarmAssertions[ASSERT_L2CurrentHighHigh],
                    AlarmAssertions[ASSERT_L2CurrentHigh],
                    AlarmAssertions[ASSERT_L2CurrentLow],
                    AlarmAssertions[ASSERT_L2CurrentNegative],
                    AlarmAssertions[ASSERT_L3CurrentHighHigh],
                    AlarmAssertions[ASSERT_L3CurrentHigh],
                    AlarmAssertions[ASSERT_L3CurrentLow],
                    AlarmAssertions[ASSERT_L3CurrentNegative],
                    sendbuf);

    return send_buf();
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
