/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:Socket.c
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   8 Oct 2015     pwang     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <errno.h>
#include <stddef.h>
#include <stdio.h>
#include <memory.h>
#include <stdlib.h>

#ifndef WIN32
#include <unistd.h>     //Needed for POSIX's close()
#endif

#include <stdarg.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <netinet/in.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "Logger.h"
#include "Socket.h"
#include "Utils.h"
#include "MCU520Sim.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define SOCK_RW_BUF_SIZE 1024
#define BACKLOG 10  // how many pending connections queue will hold

#ifdef WIN32
#define MSG_NOSIGNAL 0
#endif


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */


lu_int32_t socket_make_socket(lu_char_t* serverIPAddr, lu_int32_t port)
{
    lu_int32_t sockfd = -1;
    struct addrinfo hints;
    struct addrinfo* res = NULL;
    lu_char_t portStr[20];

    loginfo("trying to connect to server \"%s:%d\"",serverIPAddr, port);

    // converts to port string.
    sprintf(portStr,"%i",port);

    // Load up address structs with getaddrinfo()
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    if(getaddrinfo((lu_char_t*)serverIPAddr, portStr, &hints, &res) != 0)
    {
        perror("failed to get address info");
    }
    else
    {
        // Make a socket
        loginfo("opening socket...");
        sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
        if (sockfd < 0)
        {
            perror("failed to create socket");
        }
        else
        {
            // Connect socket!
            loginfo("connecting socket...");
            if (connect(sockfd, res->ai_addr, res->ai_addrlen) < 0)
            {
                perror("failed to connect");
                socket_close(sockfd);
                sockfd = -1;
            }
        }
    }

    loginfo("socket init: %s", (sockfd < 0)? "failed!" : "completed!");

    freeaddrinfo(res);

    loginfo("TCP socket opened");
    return sockfd;
}


lu_int32_t socket_recv_data(lu_int32_t socket, lu_uint8_t* recvBuffer, lu_int32_t bufSize)
{
    lu_int32_t result = 0;
    loginfo("receiving data from socket:%d ",socket);

    do
    {
        /* Receive a reply from the server*/
        result = recv(socket, (lu_char_t*)recvBuffer, bufSize, 0);
    }
    while( (result == -1) && (errno == EINTR) );


    if (result > 0 && g_sim_args.debug_socket)
    {
        dump_data("received data", recvBuffer, result);
    }

    return result;
}


lu_int32_t socket_send_data(lu_int32_t socket, lu_uint8_t* sendBuffer, lu_int32_t sendSize)
{

    lu_int32_t result;      //Partial result
    lu_int32_t sent = 0;    //Final result of the operation

    do
    {
        result = send(socket, sendBuffer + sent, sendSize - sent, MSG_NOSIGNAL);
        if (result > 0)
        {
            sent += result;
        }
        else if( (result < 0) && (errno != EINTR) )
        {
            break;// failure
        }
    }
    while (sendSize > sent);


    loginfo("sent data[%d bytes]",sendSize);
    if(sendSize == sent && g_sim_args.debug_socket)
    {
        dump_data("sent data", sendBuffer, sendSize);
    }

    return sent;
}


lu_int32_t socket_close(lu_int32_t sock)
{
#ifdef WIN32
    return closesocket(sock);
#else
    return close(sock);
#endif
}




/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
