/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:Main.c
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   2 May 2017     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include <time.h>
#include <memory.h>

#include "rtwtypes.h"
#include "Utils.h"
#include "GK_Gk2G3Translator.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */





/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

#define ASSERT_L1VoltLowLow 0
#define ASSERT_L1VoltLow 1
#define ASSERT_L1VoltHigh 2
#define ASSERT_L1VoltHighHigh 3
#define ASSERT_L2VoltLowLow 4
#define ASSERT_L2VoltLow 5
#define ASSERT_L2VoltHigh 6
#define ASSERT_L2VoltHighHigh 7
#define ASSERT_L3VoltLowLow 8
#define ASSERT_L3VoltLow 9
#define ASSERT_L3VoltHigh 10
#define ASSERT_L3VoltHighHigh 11
#define ASSERT_L1CurrentHighHigh 12
#define ASSERT_L1CurrentHigh 13
#define ASSERT_L1CurrentLow 14
#define ASSERT_L1CurrentNegative 15
#define ASSERT_L2CurrentHighHigh 16
#define ASSERT_L2CurrentHigh 17
#define ASSERT_L2CurrentLow 18
#define ASSERT_L2CurrentNegative 19
#define ASSERT_L3CurrentHighHigh 20
#define ASSERT_L3CurrentHigh 21
#define ASSERT_L3CurrentLow 22
#define ASSERT_L3CurrentNegative 23
#define ASSERT_no_alarm 24


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */
boolean_T AlarmAssertions [25] = { false } ;

uint8_T CallSimulateMcu520 (
  boolean_T UseAddressOffsets,
  uint8_T AlarmCategory,
  uint8_T AlarmToAssert,
  uint8_T* PacketFromTheMcu)
{
  memset (AlarmAssertions, 0, sizeof (AlarmAssertions)) ;
  if (AlarmToAssert < ASSERT_no_alarm) AlarmAssertions [AlarmToAssert] = true ;

  return GK_SimulateMcu520 (
    UseAddressOffsets,
    AlarmCategory,
    AlarmAssertions [ASSERT_L1VoltLowLow],
    AlarmAssertions [ASSERT_L1VoltLow],
    AlarmAssertions [ASSERT_L1VoltHigh],
    AlarmAssertions [ASSERT_L1VoltHighHigh],
    AlarmAssertions [ASSERT_L2VoltLowLow],
    AlarmAssertions [ASSERT_L2VoltLow],
    AlarmAssertions [ASSERT_L2VoltHigh],
    AlarmAssertions [ASSERT_L2VoltHighHigh],
    AlarmAssertions [ASSERT_L3VoltLowLow],
    AlarmAssertions [ASSERT_L3VoltLow],
    AlarmAssertions [ASSERT_L3VoltHigh],
    AlarmAssertions [ASSERT_L3VoltHighHigh],
    AlarmAssertions [ASSERT_L1CurrentHighHigh],
    AlarmAssertions [ASSERT_L1CurrentHigh],
    AlarmAssertions [ASSERT_L1CurrentLow],
    AlarmAssertions [ASSERT_L1CurrentNegative],
    AlarmAssertions [ASSERT_L2CurrentHighHigh],
    AlarmAssertions [ASSERT_L2CurrentHigh],
    AlarmAssertions [ASSERT_L2CurrentLow],
    AlarmAssertions [ASSERT_L2CurrentNegative],
    AlarmAssertions [ASSERT_L3CurrentHighHigh],
    AlarmAssertions [ASSERT_L3CurrentHigh],
    AlarmAssertions [ASSERT_L3CurrentLow],
    AlarmAssertions [ASSERT_L3CurrentNegative],
    PacketFromTheMcu) ;
}

#define MSG_SIZE 1024

int main(int argc, char **argv) {

    uint8_T AlarmCategory = 6;
    uint8_T AlarmToAssert = ASSERT_L1CurrentLow;
    uint16_T PacketLength =0;

    uint8_T PacketFromTheMcu[MSG_SIZE];
    uint8_T PacketToTheMcu[MSG_SIZE];

    uint16_T binaries[sizeof_GK_enuBinaries];
    int32_T  analgoues[sizeof_GK_enuAnalogues];


    memset(PacketFromTheMcu, 0, sizeof(PacketFromTheMcu));
    memset(PacketToTheMcu, 0, sizeof(PacketToTheMcu));
    memset(binaries, 0, sizeof(binaries));
    memset(analgoues, 0, sizeof(analgoues));

    CallSimulateMcu520(
      1,
      AlarmCategory,
      AlarmToAssert,
      PacketFromTheMcu
    );

    dump_data("[Sim]PacketFromTheMcu", PacketFromTheMcu, sizeof(PacketFromTheMcu));
    dump_data("[Sim]PacketToTheMcu", PacketToTheMcu, sizeof(PacketToTheMcu));
    dump_data("[Sim]binaries", binaries, sizeof(binaries));
    dump_data("[Sim]analgoues", analgoues, sizeof(analgoues));

    GK_TranslateGridkeyMessages(
                      1,
                      PacketFromTheMcu,
                      PacketToTheMcu,
                      &PacketLength,
                      binaries,
                      analgoues);

    dump_data("[Codec]PacketFromTheMcu", PacketFromTheMcu, sizeof(PacketFromTheMcu));
    dump_data("[Codec]PacketToTheMcu", PacketToTheMcu, sizeof(PacketToTheMcu));
    dump_data("[Codec]binaries", binaries, sizeof(binaries));
    dump_data("[Codec]analgoues", analgoues, sizeof(analgoues));

}


/*
 *********************** End of file ******************************************
 */
