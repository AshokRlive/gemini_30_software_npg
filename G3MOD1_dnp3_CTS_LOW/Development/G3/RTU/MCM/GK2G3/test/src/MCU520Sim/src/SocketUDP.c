/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:Socket.c
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   8 Oct 2015     pwang     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <errno.h>
#include <stddef.h>
#include <stdio.h>
#include <memory.h>
#include <stdlib.h>

#ifndef WIN32
#include <unistd.h>     //Needed for POSIX's close()
#endif

#include <stdarg.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <netinet/in.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "Logger.h"
#include "Socket.h"
#include "Utils.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define SOCK_RW_BUF_SIZE 1024
#define BACKLOG 10  // how many pending connections queue will hold

#ifdef WIN32
#define MSG_NOSIGNAL 0
#endif


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

#ifdef WIN32
 // Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")
#endif

static struct sockaddr_in si_other;
static int slen = sizeof(si_other);
/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

lu_int32_t socket_make_socket(lu_char_t* serverIPAddr, lu_int32_t port)
{
    lu_int32_t sockfd = -1;

    if ((sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {
        perror("socket");
    }

    // Prepare sockaddr
   memset((char *) &si_other, 0, sizeof(si_other));
   si_other.sin_family = AF_INET;
   si_other.sin_port = htons(port);
   if (inet_aton(serverIPAddr, &si_other.sin_addr) == 0)
   {
       fprintf(stderr, "inet_aton() failed\n");
   }
   loginfo("UDP socket opened");
    return sockfd;
}

lu_int32_t socket_recv_data(lu_int32_t socket, lu_uint8_t* recvBuffer, lu_int32_t bufSize)
{
    lu_int32_t recv;

    // Set timeout
	struct timeval tv;
	tv.tv_sec = 1;
	tv.tv_usec = 0;

    loginfo("Socket >> awaiting reply...");
	fd_set socks;
	FD_ZERO(&socks);
	FD_SET(socket, &socks);
	if (select(socket + 1, &socks, NULL, NULL, &tv))
	{
		memset(recvBuffer, 0, bufSize);
		recv = recvfrom(socket, recvBuffer, bufSize, 0, (struct sockaddr *) &si_other, &slen);
		if (recv > 0)
		{
			loginfo("Socket >> received size:%d",recv);
			loginfo("Socket >> Server replied content:%s ",recvBuffer);
			return recv;
		}
		else
	   {
		   logerr("No reply");
		   return -1;
	   }
	}
	else
	{
		logerr("Reply timeout!");
		return -1;
	}
}


lu_int32_t socket_send_data(lu_int32_t sockid, lu_uint8_t* sendbuf, lu_int32_t sendSize)
{
    lu_int32_t sent;

    //send the message
    loginfo("Socket >> sending data...");
    sent = sendto(sockid, sendbuf, sendSize, 0, (struct sockaddr *) &si_other, slen);
    if (sent != sendSize)
    {
        logerr("Socket >> invalid sent size:%d, expected:%d", sent, sendSize);
        return -1;
    }
    loginfo("Socket >> sent size:%d ", sent);
    return sent;
}


lu_int32_t socket_close(lu_int32_t sock)
{
#ifdef WIN32
    return closesocket(sock);
#else
    return close(sock);
#endif
}



/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
