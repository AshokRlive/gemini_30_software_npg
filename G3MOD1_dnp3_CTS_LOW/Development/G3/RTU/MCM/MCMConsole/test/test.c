#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/un.h>      // AF_UNIX
#include <netinet/in.h>  // AF_INET


int main(void)
{
    lu_int32_t sockFd;
    lu_int32_t ret;

    int i, j;

    AppMsgStr outMsg;
    char buf[100];
    lu_int32_t optVal;
    struct sockaddr_un servAddr;

    unlink(CONSOLESOCKETNAME);
    sockFd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (sockFd < 0)
    {
        perror("Error %i creating socket (%s)", errno, strerror(errno));
    }
    memset(&servAddr, 0, sizeof(struct sockaddr_un));
    servAddr.sun_family = AF_UNIX;
    strncpy(servAddr.sun_path, CONSOLESOCKETNAME, sizeof(servAddr.sun_path) - 1);

    // Reuse address
    setsockopt(sockFd, SOL_SOCKET, SO_REUSEADDR, &optVal, sizeof(optVal));

    // Set to non-blocking mode
    lu_int32_t sockFlags = fcntl(sockFd, F_GETFL);
    fcntl(sockFd, F_SETFL, sockFlags | O_NONBLOCK);

    if (bind(sockFd, (struct sockaddr*)&servAddr, sizeof(servAddr)) < 0)
    {
        close(sockFd);
        perror("Error %i binding to the Console socket (%s).\n", errno, strerror(errno));
    }

    if( listen(sockFd, 0) < 0 )
    {
        close(sockFd);
        perror("Error %i setting the Console socket in listening mode (%s).\n",
                    errno, strerror(errno)
                    );
    }






//    if (connect(sockFd, (struct sockaddr *) &servAddr, sizeof(servAddr)) != 0)
//    {
//        perror("Error connecting!");
//    }
//    else
//    {
//        printf("CONNECTED!\n");
//
//        while (1)
//        {
//            if (sendExtPacket(sockFd, &outMsg) > 0)
//            {
//                printf("Sent packet\n");
//            }
//
//            else
//            {
//                perror("Send FAILED errno!");
//            }
//#if 1
//            printf("Receiving...\n");
//            if ((ret = recvExtPacket(sockFd, buf, sizeof(buf), 100)) > 0)
//            {
//                printf("RX:[%d] bytes\n", ret);
//#if 0
//                for (j = 0; j < ret; j++)
//                {
//                    printf("%02X ", (unsigned char) buf[j]);
//                }
//                printf("\n");
//#endif
//            }
//#endif
//            usleep(50000);
//        }
//    }

    close(sockFd);

    return 0;
}
