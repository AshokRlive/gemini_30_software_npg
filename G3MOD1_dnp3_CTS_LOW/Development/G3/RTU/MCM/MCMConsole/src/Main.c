/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCMConsole
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       The main of MCMMonitor
 *
 *    CURRENT REVISION
 *
 *               $Revision::               $: (Revision of last commit)
 *               $Author::                 $: (Author of last commit)
 *       \date   $Date::                   $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24/02/14      pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#define _GNU_SOURCE //use of getline()

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include <fcntl.h>
#include <sys/time.h>       //timeval definition
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/un.h>      // AF_UNIX
#include <sys/wait.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "GlobalDefinitions.h"
#include "timeOperations.h"
#include "svnRevision.h"
#include "versions.h"

#include "Main.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/* Use CLOCK_MONOTONIC_RAW when available */
#ifdef CLOCK_MONOTONIC_RAW
#define MCMCLOCKELAPSED CLOCK_MONOTONIC_RAW
#else
#define MCMCLOCKELAPSED CLOCK_MONOTONIC
#endif

#define INPUTBUFMAXSIZE       300   //Input char buffer for incoming data

static const struct timeval INPUT_TIMEOUT =    //default user input timeout for input polling
{
    0, 500000    //500 ms
};

#define ALIVE_POLL_TIME_MS  1000    //Alive message polling time, in milliseconds

/* Console-specific commands */
#define CMD_EXIT    "exit"      //Leave console
#define CMD_ID      "identity\n"//Request identity of the remote host
#define CMD_ALIVE   "\x05\n"    //Alive polling command
#define CMD_REPEAT  "\x1b\x5b"  //Repeat last command

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
typedef enum
{
    STATUS_INIT,        //Initialising connection
    STATUS_TRYING,      //Trying to connect
    STATUS_CONNECTING,  //Connected, but internal operations pending
    STATUS_CONNECTED    //Connected and responding
} STATUS;

/* Simple C string */
typedef struct CStringDef
{
    size_t size;        //String size (number of chars)
    lu_char_t* text;    //String content
} CString;

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

static sigset_t initSignalMasks(void);
static void commandLineArgs(int argc, char *argv[]);
static lu_int32_t initSocket(lu_int32_t sockFd, struct sockaddr_un* clientAddr);
static lu_bool_t checkCommand(const lu_char_t *command, const lu_char_t *reference);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

/* ============= MAIN =================== */
int main(int argc, char *argv[])
{
    sigset_t oldSignalMask;
    STATUS status = STATUS_INIT;
    lu_bool_t stopCLI = LU_FALSE;
    struct sockaddr_un clientAddr;
    lu_int32_t sockFd = -1;
    lu_bool_t tried = LU_FALSE; //Already tried to connect once (only 1 message needed)
    struct timeval tout;        //timeout copy to be overwritten by select()
    lu_int32_t retSel;  //select() result
    lu_char_t outLine[INPUTBUFMAXSIZE];
    ssize_t nRead = 0;
    CString command = {0, NULL};    //Latest command string
    CString lastCommand = {0, NULL};//Previous command string
    lu_int32_t sentBytes;
    struct timespec currentTime;
    struct timespec lastTime;

    //file descriptor sets
    fd_set inputSet;
    lu_int32_t fdIn = fileno(stdin);       //convert from input file stream to file descriptor
    lu_int32_t maxFD;

    const lu_uint32_t aliveLen = strlen(CMD_ALIVE) - 1;   //Remove newline

    /* Handle signals */
    oldSignalMask = initSignalMasks();

    /* Handle Command Line Arguments */
    commandLineArgs(argc, argv);

    clock_gettime(MCMCLOCKELAPSED, &lastTime);

    fprintf(stdout, "== MCM Console system - Type '%s' to leave console access ==\n", CMD_EXIT);
    fflush(stdout);

    while (stopCLI == LU_FALSE)
    {
        //Each time we need to set values to be then overridden by select()
        tout = INPUT_TIMEOUT;
        FD_ZERO(&inputSet);

        switch(status)
        {
            case STATUS_INIT:
                /* Connect to the MCM running App */
                sockFd = initSocket(sockFd, &clientAddr);
                if(tried == LU_FALSE)
                {
                    tried = LU_TRUE;
                    fprintf(stdout, "== Trying to connect to the MCM running Application... ==\n");
                    fflush(stdout);
                }
                status = STATUS_TRYING;
                break;

            case STATUS_TRYING:
                /* check user input for exit command */
                FD_SET(fdIn, &inputSet);

                if( (retSel = connect(sockFd, (struct sockaddr*)&clientAddr, sizeof(clientAddr))) >= 0 )
                {
                    //Successful connection
                    lucy_usleep(500000); //delay a little to allow connection refusal (when applies)
                    /* Send ID request message */
                    sentBytes = send(sockFd, CMD_ID, strlen(CMD_ID)+1, 0);
                    if(sentBytes <= 0)
                    {
                        //Connection refused: retry without repeating message
                        status = STATUS_INIT;
                    }
                    else
                    {
                        fprintf(stdout, "== Connected to the MCM console ==\n");
                        fflush(stdout);

                        tried = LU_FALSE;   //reset flag for next time
                        status = STATUS_CONNECTED;
                    }
                }
                break;

            case STATUS_CONNECTED:
                //Check user and Console server input
                FD_SET(sockFd, &inputSet);
                FD_SET(fdIn, &inputSet);

                /* Send Alive  message in order to keep connection status */
                clock_gettime(MCMCLOCKELAPSED, &currentTime);
                if(timespec_elapsed_ms(&lastTime, &currentTime) > ALIVE_POLL_TIME_MS)
                {
                    sentBytes = send(sockFd, CMD_ALIVE, aliveLen+2, 0); //Send with newline + EOL
                    if(sentBytes <= 0)
                    {
                        if(errno == EPIPE)
                        {
                            fprintf(stdout, "== Connection lost with MCM console ==\n");
                            fflush(stdout);
                            status = STATUS_INIT;
                        }
                    }
                    lastTime = currentTime;
                }
                break;

            default:
                /* Invalid state */
                stopCLI = LU_TRUE;
                fprintf(stdout, "== Leaving MCM console ==\n");
                fflush(stdout);
                break;
        }//endswitch

        /* Check any input from user and/or MCM console server */
        maxFD = (sockFd > fdIn)? sockFd : fdIn;
        ++maxFD;
        retSel = select(maxFD, &inputSet, NULL, NULL, &tout);  //Wait for incoming App data
        if(stopCLI == LU_TRUE)
        {
            continue; //re-evaluate exit condition after waiting
        }
        if(retSel > 0)
        {
            /* Read MCM console server */
            if(FD_ISSET(sockFd, &inputSet))
            {
                //available data from App: read it
                nRead = recv(sockFd, outLine, INPUTBUFMAXSIZE, 0);
                if(nRead > 0)
                {
                    outLine[nRead] = 0; //force Cstring terminator
                    if(strncmp(outLine, CMD_ALIVE, aliveLen) == 0 )
                    {
                        /* Console-specific message */
                        //nothing to do here for Alive message reply... just ignore
                    }
                    else
                    {
                        /* Write App output to the user console */
                        fprintf(stdout, "%s", outLine);
                        fflush(stdout);
                    }
                }
            }
            /* Read user input */
            if(FD_ISSET(fdIn, &inputSet))
            {
                /* Note that getline() re-allocates memory as needed */
                nRead = getline(&command.text, &command.size, stdin);
                if(nRead > 0)
                {
                    if(checkCommand(command.text, CMD_EXIT) == LU_TRUE)
                    {
                        //User exit command
                        stopCLI = LU_TRUE;
                        fprintf(stdout, "== Leaving MCM console ==\n");
                        fflush(stdout);
                        continue;
                    }

                    if(checkCommand(command.text, CMD_REPEAT) == LU_TRUE)
                    {
                        if(lastCommand.size != 0)
                        {
                            //Repeat last command
                            lu_char_t* lText = (lu_char_t*)malloc(lastCommand.size + 1);
                            strncpy(lText, lastCommand.text, lastCommand.size);
                            lText[lastCommand.size - 1] = '\0'; //remove newline
                            fprintf(stdout, "== Repeating last command: '%s' ==\n", lText);
                            fflush(stdout);
                            free(lText);

                            strncpy(command.text, lastCommand.text, lastCommand.size);
                            command.size = lastCommand.size;
                        }
                    }
                    command.size = strlen(command.text);
                    sentBytes = send(sockFd, command.text, command.size + 1, 0);
                    if(sentBytes <= 0)
                    {
                        if(errno == EPIPE)
                        {
                            fprintf(stdout, "== Connection lost with MCM console ==\n");
                            fflush(stdout);
                            status = STATUS_INIT;
                        }
                        else
                        {
                            fprintf(stdout, "== Unable to send message to MCM console err=%i (%s) ==\n",
                                            errno, strerror(errno));
                            fflush(stdout);
                        }
                    }
                    //Store last command
                    lastCommand.text = (lu_char_t*)realloc(lastCommand.text, command.size + 1);
                    strncpy(lastCommand.text, command.text, command.size);
                    lastCommand.size = command.size;

                }
                else
                {
                    fprintf(stdout, "== Connection lost with MCM console ==\n");
                    fflush(stdout);
                    status = STATUS_INIT;
                }
            }
        }
    } //endwhile
    /* Free used resources */
    if(command.text != NULL)
    {
        free(command.text);
    }
    if(lastCommand.text != NULL)
    {
        free(lastCommand.text);
    }
    if(sockFd >= 0)
    {
        close(sockFd);
    }

    return 0;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */
static sigset_t initSignalMasks(void)
{
    sigset_t newSignalMask;
    sigset_t oldSignalMask;

    // Mask most signals for us and child threads
    sigemptyset(&newSignalMask);
    sigaddset(&newSignalMask, SIGPIPE);

    if (pthread_sigmask(SIG_BLOCK, &newSignalMask, &oldSignalMask) != 0)
    {
        fprintf(stdout, "== Unable to Handle signals! ==\n");
        fflush(stdout);
        exit(EXIT_FAILURE);
    }
    return oldSignalMask;
}


static void commandLineArgs(int argc, char *argv[])
{
    lu_int32_t arg;

    if (argc > 1)
    {
        fprintf(stdout, "\nMCMConsole - (c) Lucy Switchgear\n");
    }

    // Handle command line arguments
    while ((arg = getopt(argc, argv, "hv")) != -1)
    {
        switch (arg)
        {
            case 'v':
                fprintf(stdout, "MCM Console Version %c-%u.%u.%u svn:%ld\n",
                                *VERSION_TYPE_ToSTRING(MCM_CONSOLE_SOFTWARE_VERSION_RELTYPE), //get 1st char only
                                MCM_CONSOLE_SOFTWARE_VERSION_MAJOR,
                                MCM_CONSOLE_SOFTWARE_VERSION_MINOR,
                                MCM_CONSOLE_SOFTWARE_VERSION_PATCH,
                                _SVN_REVISION);
                exit(0);
            break;

            /* TODO: pueyos_a - CONSOLE add option to specify the socket name */

            default:
                fprintf(stdout,
                        "Options:\n"
                        "-h  \tDisplay this help\n"
                        "-v  \tDisplay version\n");
                exit(0);
        }
    }
}


static lu_int32_t initSocket(lu_int32_t sockFd, struct sockaddr_un* clientAddr)
{
    /* Init socket */
    if(sockFd >= 0)
    {
        close(sockFd);
    }
    if( (sockFd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0)
    {
        perror("== Unable to create connection to the MCM console ==");
        exit(1);
    }
    memset(clientAddr, 0, sizeof(struct sockaddr_un));
    clientAddr->sun_family = AF_UNIX;
    strncpy(clientAddr->sun_path, CONSOLESOCKETNAME, sizeof(clientAddr->sun_path)-1);
    // Set to non-blocking mode
    lu_int32_t sockFlags = fcntl(sockFd, F_GETFL);
    fcntl(sockFd, F_SETFL, sockFlags | O_NONBLOCK);

    return sockFd;
}


static lu_bool_t checkCommand(const lu_char_t *command, const lu_char_t *reference)
{
    lu_int32_t cmdSize = strlen(reference);
    return ( strncmp(command, reference, cmdSize) == 0 )? LU_TRUE : LU_FALSE;

}




/*
 *********************** End of file ******************************************
 */
