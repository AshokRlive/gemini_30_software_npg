cmake_minimum_required(VERSION 2.8)


#--------------------------- REQUIRED VARIABLES --------------------------------
# Set the root path of G3 
set(CMAKE_G3_PATH_ROOT "${CMAKE_CURRENT_SOURCE_DIR}/../../../../")

# Set the path of the header files generated from XMLs, svn ,etc
set(CMAKE_G3_GENERATED_HEADER_BUILD_PATH "${CMAKE_CURRENT_SOURCE_DIR}/../includeGenerated/")


#---------------------COMPILER & GLOBAL ENVIRONMENT-----------------------------
# Set target board
include("${CMAKE_G3_PATH_ROOT}/RTU/MCM/cmake/target_board.cmake")

# Set cross compiler
if(DEFINED MCM_TOOLCHAIN_FILE)
SET(CMAKE_TOOLCHAIN_FILE "${MCM_TOOLCHAIN_FILE}")
endif()

# Set the name of the project. The project name will be used as the name of
# the final executable
project (MCMConsole)

# Set GCC default flags
include("${MCM_GCC_FLAGS_FILE}")

# Include generated header
include("${CMAKE_G3_PATH_ROOT}/RTU/MCM/cmake/generated_header.cmake")

# Import global variables
include("${CMAKE_G3_PATH_ROOT}/RTU/MCM/cmake/global_variables.cmake")

#-------------------------------INCLUDING---------------------------------------

# GLOBAL include 
set(GLOBAL_INCLUDES
    ${CMAKE_G3_PATH_COMMON_INCLUDE}
    ${CMAKE_G3_PATH_RTU_COMMON_INCLUDE}
    ${CMAKE_G3_PATH_COMMON_LIBRARY}/util/include/
    )
include_directories(${GLOBAL_INCLUDES})

# LOCAL include
set(LOCAL_INCLUDES
	../include/
	../includeGenerated/
	)
include_directories(${LOCAL_INCLUDES})


#-----------------------------BUILD DIRECTORIES---------------------------------
# Include all the library used by the project. The first argument is the
# library relative path. The second argument is where the Makefiles and
# binaries will be saved. This path is relative to the directory where the
# cmake is called. This allows a full out-of-source build.

# LIBRARY directories to be built

# LOCAL directories to be built
#...


#------------------------------EXECUTABLE --------------------------------------
add_executable(${EXE_NAME} Main.c)


#------------------------------LINKING------------------------------------------
# Link LOCAL components 
#...

# Link LIBRARY components
target_link_libraries(${EXE_NAME} ${CMAKE_G3_PATH_MCM_COMMONLIB_LINK})
target_link_libraries (${EXE_NAME} rt)
target_link_libraries (${EXE_NAME} pthread)
#target_link_libraries (${EXE_NAME} ${CMAKE_THREAD_LIBS_INIT})


#---------------------------DEPENDENCIES----------------------------------------
add_dependencies(${EXE_NAME} svnRevision)
add_dependencies(${EXE_NAME} globalHeaders)

#------------------------------OTHER--------------------------------------------
# Generate binary file from executable file
#generate_bin()

# Generate Doxygen documentation
# gen_doxygen("MCMConsole" "")


include("${CMAKE_G3_PATH_ROOT}/RTU/MCM/cmake/install.cmake")