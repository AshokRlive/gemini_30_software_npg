#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <stdio.h>

//#include <pthread.h>

//#include "SysLogManagerPrintf.h"
#include "CommandLineInterpreter.h"



void fOne()
{
    printf("Function one.\n");
}

void fTwo()
{
    printf("Function TWO.\n");
}

void fThree()
{
    printf("Function Three.\n");
}

void fExit()
{
    printf("Exit requested.\n");
//    if(refCLI != NULL)
//    {
//        refCLI->stop();
//    }
}



/*
class CTwo : public CommandCLI
{
public:
    virtual void call()  { printf("Obj TWO.\n"); fTwo(); }
};
*/

class Console
{
public:
    Console() : cli(NULL) {};
    void setCLI(CommandLineInterpreter* newCLI)
    {
        cli = newCLI;
    }

    class COne : public CommandCLI
    {
    public:
        COne(Console& consoleRef) :
            CommandCLI("one", "", "Function One", LU_FALSE),
            console(consoleRef)
        {}

        void call()
        {
            lu_uint32_t local;
            console.cli->message("\tlocal(1..200): ");
            if(console.cli->askAndCheck(&local, 1, 200) == LU_TRUE)
            {
                printf("Weee! local=%i\n", local);
                return;
            }
            else
            {
                printf("invalid param\n");
            }
        }
    private:
        Console& console;
    };

    class CThree : public CommandCLI
    {
    public:
        CThree(Console& consoleRef) :
            CommandCLI("three", "3", "Function Three", LU_FALSE),
            console(consoleRef)
        {}

        typedef enum {
            SWITCH_OPERATION_CLOSE,
            SWITCH_OPERATION_OPEN=68000,
            SWITCH_OPERATION_LAST
        }SWITCH_OPERATION;

        void call()
        {
            lu_uint32_t clogic, operation, local;
            SWITCH_OPERATION so;
            printf("Obj 3.\n");
            console.cli->message("\tOperation(0 open - 1 close): ");
            if(console.cli->askAndCheck(&operation, 0, 1) == LU_TRUE)
            {
                clogic = 5;
                console.cli->message("\tlocal(1..68000): ");
                if(console.cli->askAndCheck(&local, 1, SWITCH_OPERATION_OPEN) == LU_TRUE)
                {
                    printf("Weee! op=%i, local=%i\n", operation, local);
                    return;
                }
            }
            console.cli->message("Invalid parameters. Operation NOT started.\n");
        }
    private:
        Console& console;
    };

    class CFour : public CommandCLI
    {
    public:
        CFour(Console& consoleRef) :
            CommandCLI("four", "4", "Function Four Asks you 5 chars", LU_FALSE),
            console(consoleRef)
        {}
        void call()
        {
            lu_char_t buff[50];
            if( console.cli->ask("What do you think in 5 chars? ", buff, 5) == LU_TRUE)
            {
                printf("OK");
            }
            else
            {
                printf("Bah");
            }
            printf(", so you think '%s'!!\n", buff);
        }
    private:
        Console& console;
    };

    class CExit : public CommandCLI
    {
    public:
        CExit(Console& consoleRef)
        :
            CommandCLI("exit", "x", "End the test"),
            console(consoleRef)
        {}

        void call()
        {
            CommandLineInterpreter::YESNORESULT res = console.cli->waitYesNo("Do you want to exit?");
            printf("%s\n", (res == CommandLineInterpreter::YESNORESULT_YES)? "Exiting" :
                           "Cancelled.");
            if(res == CommandLineInterpreter::YESNORESULT_YES)
            {
                console.cli->stop();
            }
        }
    private:
        Console& console;
    };

protected:
    CommandLineInterpreter* cli;
};
int main()
{
    /* Create CLI */
    CommandLineInterpreter *refCLI = NULL; //reference to the CLI
    Console cons;

    CommandCLI* cmdList[] =
    {
        new Console::COne(cons),
        new Console::CThree(cons),
        new Console::CFour(cons),
        new Console::CExit(cons)
    };
    printf("sending list of %i elements (%i / %i)\n", (sizeof(cmdList) / sizeof(CommandCLI*)), sizeof(cmdList), sizeof(CommandCLI*));
    refCLI = new CommandLineInterpreter(cmdList, (sizeof(cmdList) / sizeof(CommandCLI*)));
    if(refCLI != NULL)
    {
        /* Start CLI: main will relinquish the control! */
        cons.setCLI(refCLI);
        refCLI->start();
    }

    printf("CLI Test finished.\n");
}
