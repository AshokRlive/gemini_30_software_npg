/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: CommandLineInterpreter.cpp 01-Aug-2013 15:12:55 pueyos_a $
 *               $HeadURL: \\10.11.0.7\pueyos_a\workspace\app_refactor\G3\RTU\MCM\MCMApp\src\CommandLineInterpreter\src\CommandLineInterpreter.cpp $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Command Line Interpreter header module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 01-Aug-2013 15:12:55	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   01-Aug-2013 15:12:55  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_E8735D32_789C_4707_9F27_639767E22EB4__INCLUDED_)
#define EA_E8735D32_789C_4707_9F27_639767E22EB4__INCLUDED_



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <string>
#include <stdio.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Thread.h"
#include "Logger.h"

//forward declarations
class CommandLineInterpreter;

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define CLI_OUTPUT_MAX_SIZE 255     //Max size of line to output to the console

/**
 * Define a class for a command
 *
 * Use it to define a Class for a command in a shorter way.
 *
 * Usage:
 *      COMMANDCLASS(CLICInfo, "info", "", "RTU general information");
 */
#define COMMANDCLASS(ClassName, commandMain, commandAlt, commandIsHidden, commandDescription)   \
    class ClassName : public CommandCLI                                         \
    {                                                                           \
    public:                                                                     \
        ClassName(Console& c):                                                  \
                CommandCLI(commandMain, commandAlt, commandDescription, commandIsHidden),       \
                console(c)                                                      \
        {}                                                                      \
        void call(CommandLineInterpreter& cli);                                 \
    private:                                                                    \
        Console& console;                                                       \
    }

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 *  \brief CLI Command
 *
 * This object defines a command to be used by the Command Line Interpreter.
 * Parameter commandName must cannot be and empty string, meanwhile
 * commandNameAlt can.
 *
 */
class CommandCLI
{
public:
    /**
     * \brief Custom constructor
     *
     * \param cliRef Reference to the CommandLineInterpreter
     * \param commandName Command string (Not empty, please)
     * \param commandNameAlt Alternate command string
     * \param description Description that goes in the help message (keep it short!)
     * \param accessLevel Attribute stating the user level needed to execute this command
     * \param hidden Attribute stating this command is hidden and not shown by the help command.
     */
    CommandCLI( std::string commandName,
                std::string commandNameAlt,
                std::string description,
                //lu_uint8_t accessLevel = 0,   /* TODO: AP - add user level control into the CLI in the future */
                lu_bool_t hidden = LU_FALSE
                ) :
                    commandName(commandName),
                    commandNameAlt(commandNameAlt),
                    description(description),
                    hidden(hidden)
    {};
    virtual ~CommandCLI() {};

    /**
     * \brief Action to do when this command is executed
     *
     * This method must be implemented in each command
     */
    virtual void call(CommandLineInterpreter& cli) = 0;

public:
    const std::string commandName;    //Command string
    const std::string commandNameAlt; //Alternate command string. May be NULL
    const std::string description;    //Description that goes in the help message (keep it short!)
    /* TODO: AP - add user level control into the CLI in the future */
    //const lu_uint8_t userLevel;           //Attribute: user level access needed for this command
    const lu_bool_t hidden;               //Attribute: this command is hidden and will not appear in the help
};


/**
 * \brief class to implement help display (list of available commands)
 */
class CommandHelp : public CommandCLI
{
public:
    CommandHelp(CommandCLI* commandsList[], lu_uint32_t commandsListSize):
            CommandCLI("help", "h", "This help list screen", LU_FALSE),
            commandList(commandsList),
            listSize(commandsListSize)
    {}
    void call(CommandLineInterpreter& cli);
private:
    CommandCLI** commandList;
    lu_uint32_t listSize;           //Size of the Commands list
};

/**
 * \brief This class creates and maintains a Command Line Interpreter
 *
 * To configure the CLI, create the object by giving an array of commandCLI
 * objects, implementing its related functionality.
 * Do not add the help function since it is already built-in.
 * After providing the array, simply initiate the CLI by calling start().
 * This will use the caller thread and stay doing an infinite loop until stop()
 * is called.
 * The CLI will check for user input and execute the command if it's present in
 * the array.
 * Do not destroy the array during the execution of the CLI.
 * The CLI is not automatically destroyed after execution.
 *
 * All user inputs are made with select() instead of using directly fscanf() in
 * order to allow the CLI to end gracefully.
 */
class CommandLineInterpreter : public Thread
{
public:
    /**
     * \brief Possible results of the waitYesNo() operation
     */
    typedef enum
    {
        YESNORESULT_CANCEL, //Operation cancelled
        YESNORESULT_NO,     //User answered no
        YESNORESULT_YES,    //User answered yes
        YESNORESULT_LAST
    }YESNORESULT;

public:
    /**
     * \brief Custom constructor
     *
     * \param commandsList Command list to be used by the CLI.
     * \param listSize Amount of commands present in the commands list
     * \param inConsole Input stream.
     * \param outConsole Output stream.
     */
	CommandLineInterpreter( CommandCLI* commandsList[],
                            lu_uint32_t listSize,
                            lu_int32_t inConsole = fileno(stdin),
                            lu_int32_t outConsole = fileno(stdout)
                            );
	virtual ~CommandLineInterpreter();

    /**
     * \brief Get the file descriptor of the input stream
     *
     * \return file descriptor
     */
    lu_int32_t getInConsole() const
    {
        return inCons;
    }

    /**
     * \brief Get the file descriptor of the output stream
     *
     * \return file descriptor
     */
    lu_int32_t getOutConsole() const
    {
        return outCons;
    }

    /**
     * \brief Check the CLI to see if there is any input
     */
    void check();

    /**
     * \brief Sends a given message to the console.
     *
     * \param message Message to be sent to the console, in C++ string or printf-like style.
     */
    void message(const lu_char_t* message, ...);
    void message(const std::string message);

    /**
     * \brief Wait for numeric user input and check correct boundaries.
     *
     * Note that only integer values are supported. Different datatypes in the
     * parameters are intended to allow to use enums and other constant values.
     *
     * \param data Pointer to where to store the value
     * \param minValue Minimum value acceptable
     * \param maxValue Maximum value acceptable
     *
     * \return Operation result, being LU_TRUE when the value is correct and inside boundaries.
     */
    template <typename T, typename T1, typename T2>
    lu_bool_t askAndCheck(T* data, const T1 minValue, const T2 maxValue)
    {
        if(data == NULL)
        {
            return LU_FALSE;
        }
        lu_int32_t result;
        std::string buf;
        if(input(buf) <= 0)
        {
            return LU_FALSE;
        }
        if( sscanf(buf.c_str(), "%i", &result) > 0)
        {
            if( (static_cast<lu_int64_t>(result) > static_cast<lu_int64_t>(maxValue)) ||
                (static_cast<lu_int64_t>(result) < static_cast<lu_int64_t>(minValue))
                )
            {
                return LU_FALSE;
            }
            *data = static_cast<T>(result);
            return LU_TRUE;
        }
        return LU_FALSE;
    }


	/**
	 * \brief Waits for the user to enter data from the console
	 */
    /**
     * \brief Shows a message to the console and waits for user response
     *
     * \param question Message to be shown in the console. When not NULL, a " (y/n)?" message will be added at the end.
     * \param result Where to store the user input
     * \param maxlen Maximum char length of the input (the remaining chars are dropped)
     *
     * \return result of the operation. LU_TRUE when successful.
     */
	lu_bool_t ask(const lu_char_t *question, lu_char_t *result, const lu_uint32_t maxlen);
	lu_bool_t ask(std::string question, std::string &result);

	/**
     * \brief Get for user input
     *
     * Use it instead fscanf(). It is more secure and allows the CLI to exit at
     * any time.
     *
     * \param result Where to store the user input string
     * \param maxlen Maximum char length of the input (the remaining chars are dropped)
     *
     * \return Number of characters read. Negative number for error (exit the CLI).
     */
    lu_int32_t input(std::string &result, lu_bool_t usePending = LU_TRUE);

	/**
	 * \brief Wait input for being yes or no only.
	 *
	 * \param title Title to show to the user. An ending string " (y/n)?" will
	 * be appended at the end of it automatically.
	 * Note that this function does not take any previous entry from the
	 * command line.
	 *
	 * \return result of the user's input, being only Yes, No, or Cancel.
	 */
	CommandLineInterpreter::YESNORESULT waitYesNo(const lu_char_t* title);
	CommandLineInterpreter::YESNORESULT waitYesNo(const std::string title);

    void performGettingLogLevel();

    lu_bool_t performSettingLogLevel();

protected:
    void threadBody();

private:
    /**
     * \brief Check if a command line matches with the main or alternate command
     *
     * It takes the input from inpLine and updates inputPos to point after it.
     *
     * \param command Main reference command to be compared with.
     * \param commandAlt Alternate reference command to be compared with.
     *
     * \return Result of comparison, LU_TRUE if it matches
     */
    lu_bool_t checkCommand(const std::string command, const std::string commandAlt);

	/**
	 * \brief Show the help text explaining the commands
	 */
	void help();

	/**
	 * \brief Get input line from input stream
	 *
	 * Please call this function after checking input stream has available
	 * chars ready.
	 *
	 * \return string containing the input line. Empty string when error.
	 */
	std::string getInput();


private:
    CommandHelp helpCommand;        //Internal help command
    lu_int32_t inCons;  //input stream
    lu_int32_t outCons; //output stream

    CommandCLI** commandList;
    lu_uint32_t listSize;           //Size of the Commands list

    std::string inpLine;
    std::size_t inputPos;
    std::string prevLine;   //last line input from user
    lu_bool_t hdrPrint;     //header print control to show header only after a command
};


#endif // !defined(EA_E8735D32_789C_4707_9F27_639767E22EB4__INCLUDED_)

/*
 *********************** End of file ******************************************
 */

