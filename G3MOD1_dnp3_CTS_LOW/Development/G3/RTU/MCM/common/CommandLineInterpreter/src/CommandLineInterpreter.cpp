/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: CommandLineInterpreter.cpp 01-Aug-2013 15:12:55 pueyos_a $
 *               $HeadURL: \\10.11.0.7\pueyos_a\workspace\app_refactor\G3\RTU\MCM\MCMApp\src\CommandLineInterpreter\src\CommandLineInterpreter.cpp $
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Command Line Interpreter module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 01-Aug-2013 15:12:55	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   01-Aug-2013 15:12:55  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdio.h>
#include <string.h>
#include <algorithm>    //use of std::min()
#include <sys/time.h>
#include <unistd.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "CommandLineInterpreter.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

static const timeval INPUT_TIMEOUT =    //default user input timeout for input polling
{
    0, 500000    //500 ms
};

/* Console-specific commands */
#define CMD_ID      "identity"  //Request identity of the remote host
#define CMD_ALIVE   '\x05'      //Alive polling command

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

CommandLineInterpreter::CommandLineInterpreter(CommandCLI* commandsList[],
                                               lu_uint32_t commandsListSize,
                                               lu_int32_t inConsole,
                                               lu_int32_t outConsole) :
                            Thread(SCHED_TYPE_FIFO, Thread::PRIO_MIN + 25, LU_TRUE, "CLI"),
                            helpCommand(commandsList, commandsListSize),
                            inCons(inConsole),
                            outCons(outConsole),
                            commandList(commandsList),    //Build table with the given list
                            listSize(commandsListSize),
                            inpLine(""),
                            inputPos(0),
                            hdrPrint(LU_TRUE)
{}


CommandLineInterpreter::~CommandLineInterpreter()
{
    stop();
}


void CommandLineInterpreter::check()
{
    timeval tout;       //timeout copy to be overwritten by select()
    lu_int32_t retSel;  //select() result
    lu_bool_t commandFound;         //states whether the command entered is matched or not
    bool inputEmpty = true;
    /* file descriptor sets */
    fd_set inFd;

    do
    {
        inputPos = 0;

        /* Print header if needed */
        if(hdrPrint == LU_TRUE)
        {
            message("\nWaiting for command...\n");
            hdrPrint = LU_FALSE;  //print only after a command has finished
        }

        //Each time we need to set values to be then overridden by select()
        tout = INPUT_TIMEOUT;
        FD_ZERO(&inFd);
        FD_SET(inCons, &inFd);

        /* Wait for user input */
        retSel = select(inCons+1, &inFd, NULL, NULL, &tout);
        if(isInterrupting() == LU_TRUE)
        {
            return; //re-evaluate exit condition after waiting
        }
        if(retSel > 0)
        {
            if(FD_ISSET(inCons, &inFd))
            {
                //available data at input stream: read it
                inpLine = getInput();
                if(inpLine.size() == 0)
                    break;
                hdrPrint = LU_TRUE; //mark for printing the header again
                inputEmpty = true;
            }
        }
        else
        {
            return;
        }
        if( (inpLine.find_first_not_of(" \n") == std::string::npos) || inpLine.empty() )
        {
            //no useful chars found
            hdrPrint = LU_FALSE;    //don't print header this time
            return; //nothing read, so no need to evaluate
        }

        /* Check escape sequence: up arrow for command repeat */
        if( (inpLine[0] == 0x1b) && (inpLine[1] == 0x5b) && (inpLine.size() <= 4) )
        {
            //repeat last command if possible
            if(prevLine.empty())
            {
                return;   //no last command available
            }
            else
            {
                message("Repeating last command: '%s'\n", prevLine.substr(0, prevLine.size() - 1).c_str());
                inpLine = prevLine;     //copy last command onto the present one
            }
        }
        else
        {
            prevLine = inpLine; //backup present command onto the previous buffer
        }


        /* === Decode command === */
        /* Commandlist commands */
        if(checkCommand(helpCommand.commandName, helpCommand.commandNameAlt) == LU_TRUE)
        {
            //help command
            helpCommand.call(*this);
            return;   //skip checking the rest of the commands
        }
        commandFound = LU_FALSE;
        lu_uint32_t index;
        for (index = 0; index < listSize; ++index)
        {
            if(commandList[index] == NULL)
            {
                break;
            }
            //check each command in table
            if(checkCommand(commandList[index]->commandName, commandList[index]->commandNameAlt) == LU_TRUE)
            {
                //match found
                commandList[index]->call(*this); //execute action for this command
                commandFound = LU_TRUE;
                break;
            }
        }//endfor
        if(commandFound != LU_TRUE)
        {
            message("Unsupported command: %s\n", inpLine.c_str());
        }
    }
    while(inputEmpty);

	return;   //exit the CLI
}


void CommandLineInterpreter::message(const std::string message)
{
    write(outCons, message.c_str(), message.size());
}

void CommandLineInterpreter::message(const lu_char_t* message, ...)
{
    FILE* logStream;    //stream in memory for string conversion
    lu_char_t* lineBuffer;
    size_t bufSize;
    va_list args;

    logStream = open_memstream(&lineBuffer, &bufSize);
    va_start(args, message);
    vfprintf(logStream, message, args);
    va_end(args);
    fflush(logStream);
    rewind(logStream);
    lu_char_t* lineBuf = new lu_char_t[bufSize];
    fread(lineBuf, 1, bufSize, logStream);
    write(outCons, lineBuf, bufSize);
    fclose(logStream);
    delete [] lineBuf;
}


lu_bool_t CommandLineInterpreter::ask(std::string question, std::string &result)
{
    lu_int32_t res;

    message(question);  //Show question
    res = input(result);
    result.erase(result.end()-1,result.end()); //Remove new line symbol at the end
    return (res>=0)? LU_TRUE : LU_FALSE;
}

lu_bool_t CommandLineInterpreter::ask(const lu_char_t *question, lu_char_t *result, const lu_uint32_t maxlen)
{
    lu_int32_t res;

    if(result == NULL)
    {
        return LU_FALSE;
    }
    if(question != NULL)
    {
        message(question);       //show question
    }
    std::string inputLine;
    res = input(inputLine);
    strncpy(result, inputLine.c_str(), std::min((lu_uint32_t)(inputLine.size()-1), maxlen));
    result[std::min((lu_uint32_t)(inputLine.size()-1), maxlen)] = 0;   //add C string terminator
    return (res>=0)? LU_TRUE : LU_FALSE;
}


CommandLineInterpreter::YESNORESULT CommandLineInterpreter::waitYesNo(const std::string title)
{
    return waitYesNo(title.c_str());
}

CommandLineInterpreter::YESNORESULT CommandLineInterpreter::waitYesNo(const lu_char_t* title)
{
    std::string reply;
    do
    {
        //show message and wait for user input
        if(title != NULL)
        {
            message("%s (y/n)? ", title);
        }
        do
        {
            if(input(reply, LU_FALSE) < 0) //do not use previous entry from user
            {
                return YESNORESULT_CANCEL;  //cancelled entry
            }
        } while(reply[0] < 0x33); //ignore control chars or escape sequences
    } while(reply.substr(0, 1).find_first_of("yYnN") == std::string::npos);
    return( (reply.find_first_of("yY") != std::string::npos)? YESNORESULT_YES : YESNORESULT_NO );
}


void CommandLineInterpreter::performGettingLogLevel()
{
   LOG_LEVEL logLevel;

   message("Subsystem log levels:\n");
   for(lu_uint32_t i = 0; i < SUBSYSTEM_ID_LAST; ++i)
   {
       logLevel = Logger::getLogLevel(static_cast<SUBSYSTEM_ID>(i));

       message("\t%-22s: %s\n",
               Logger::getSubSystemName(static_cast<SUBSYSTEM_ID>(i)),
               Logger::getLogLevelName(logLevel)
              );
   }
}

lu_bool_t CommandLineInterpreter::performSettingLogLevel()
{
    SUBSYSTEM_ID subsystem;
    LOG_LEVEL logLevel = LOG_LEVEL_LAST;
    lu_bool_t result = LU_FALSE;

    message("Select subsystem:\n");
    /*Display all sub systems*/
    for(lu_uint32_t i = 0; i < SUBSYSTEM_ID_LAST; ++i)
    {
        subsystem = static_cast<SUBSYSTEM_ID>(i);
        message("\t%i: [%5s] %s\n",
                i,
                Logger::getLogLevelName(Logger::getLogLevel(subsystem)),
                Logger::getSubSystemName(subsystem)
                );
    }
    /*Display "All subsystem" option */
    message("\t%i: %s\n", SUBSYSTEM_ID_LAST, "Set ALL the subsystems");

    /*Display "Cancel" option*/
    message("\t%i: %s\n", SUBSYSTEM_ID_LAST + 1, "-Cancel-");

    /* Get input subsystem*/
    if( askAndCheck(&subsystem, 0, SUBSYSTEM_ID_LAST) == LU_TRUE )
    {
        //Get its current log level
        logLevel = Logger::getLogLevel(subsystem);

        /*Display all log levels*/
        message("Select log level:\n");
        for(lu_uint32_t i = 0; i < LOG_LEVEL_LAST; ++i)
        {
            message("\t%i: %s%s\n",
                    i,
                    (static_cast<LOG_LEVEL>(i) == logLevel)? "> " : "  ",
                    Logger::getLogLevelName(static_cast<LOG_LEVEL>(i))
                   );
        }

        /*Get input log level*/
        if (askAndCheck(&logLevel, 0, LOG_LEVEL_LAST) == LU_TRUE)
        {
            LOG_ERROR ret;
            if(subsystem == SUBSYSTEM_ID_LAST)
                ret = Logger::setAllLogLevel(logLevel);
            else
                ret = Logger::getLogger(subsystem).setLogLevel(logLevel);
            switch(ret)
            {
                case LOG_ERROR_NONE:
                    message("Set %s subsystem to log level %s done.\n",
                                (subsystem == SUBSYSTEM_ID_LAST)? "all" :
                                Logger::getSubSystemName(subsystem),
                                Logger::getLogLevelName(logLevel)
                            );
                    result = LU_TRUE;
                    break;
                case LOG_ERROR_INVALID_SUBSYSTEM:
                    message("Error: invalid subsystem %i specified.\n", subsystem);
                    break;
                case LOG_ERROR_INVALID_LOGLEVEL:
                    message("Error: invalid log level %i specified.\n", logLevel);
                    break;
                default:
                    message("Error: unable to set log level.\n");
                    break;
            }
        }
    }
    if(result == LU_FALSE)
    {
        message("Cancelled.\n");
    }

    return result;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void CommandLineInterpreter::threadBody()
{
    /* send/receive data until exit request */
    while(isRunning() == LU_TRUE)
    {
        check();   //Make CLI to check input
    }
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
lu_bool_t CommandLineInterpreter::checkCommand(const std::string command, const std::string commandAlt)
{
    std::string token;

    inputPos = inpLine.find_first_of(" \n");
    token = inpLine.substr(0, inputPos);
    if( (inputPos == std::string::npos) || token.empty() )
    {
        return LU_FALSE;
    }
    return ( (token.compare(command) == 0) || (token.compare(commandAlt) == 0) )? LU_TRUE : LU_FALSE;
}


void CommandHelp::call(CommandLineInterpreter& cli)
{
    cli.message("\nCommands available:\n");

    /* print help-specific line */
    cli.message("\t%s (%s): %s\n", commandName.c_str(), commandNameAlt.c_str(), description.c_str());
    /* print all the rest of command's lines */
    for (lu_uint32_t index = 0; index < listSize; ++index)
    {
        if(commandList[index] == NULL)
        {
            break;
        }

        if(commandList[index]->hidden != LU_TRUE)  //print only the ones that are not hidden
        {
            cli.message("\t%s%s%s%s: %s\n",
                    (commandList[index]->commandName.c_str()),
                    (commandList[index]->commandNameAlt.empty())? "" : " (",
                    (commandList[index]->commandNameAlt.empty())? "" : commandList[index]->commandNameAlt.c_str(),
                    (commandList[index]->commandNameAlt.empty())? "" : ")",
                    (commandList[index]->description.c_str())
                    );
        }
    }
}


std::string CommandLineInterpreter::getInput()
{
    lu_char_t lineBuf[CLI_OUTPUT_MAX_SIZE];
    ssize_t nRead = 0;

    /* Read incoming data */
    nRead = read(inCons, lineBuf, CLI_OUTPUT_MAX_SIZE);
    if(nRead <= 0)
    {
        stop(); //connection was closed!
        return std::string();
    }

    std::string inputLine(lineBuf, nRead);  //compose input with what was read

    /* IMPORTANT NOTE: erase-remove idiom, std::find_if and other string-related
     *                  algorithms seems to do NOT be working properly on this
     *                  version of the host's glibc library.
     */
    lu_uint32_t pos = 0;
    for (lu_uint32_t i = 0; i < (lu_uint32_t)nRead; ++i)
    {
        if(lineBuf[i] == CMD_ALIVE)
        {
            //remove all "alive" message entries:
            pos = i + 2;    //skip \n as well
        }
        if(isalnum(inputLine[i]) != 0)
        {
            //First valid char is considered the start of the valid input
            return inputLine.substr(pos);   ///String contains alphanumeric chars
        }
    }
    return "";  //No alphanumeric chars
}


lu_int32_t CommandLineInterpreter::input(std::string &result, lu_bool_t usePending)
{
    std::string inputLine;  //Line read;
    lu_int32_t retSel;  //select() result
    timeval tout;       //timeout copy to be overwritten by select()
    /* file descriptor sets */
    fd_set inFd;

    result.clear();

    /* Check if there was parameters pending to process */
    if( (usePending == LU_TRUE) &&
        !((inpLine.find_first_not_of(" \n", inputPos) == std::string::npos) ||
            (inpLine.empty()) ||
            (inputPos == 0) || (inputPos == std::string::npos)
        ) )
    {
        std::string token;
        {
            inputPos = inpLine.find_first_not_of(" ", inputPos);  //skip spaces before token
        }
        std::size_t prevPos = inputPos;
        inputPos = inpLine.find_first_of(" \n", inputPos);  //advance to the next space / end
        if(prevPos < inputPos)
        {
            //token = inpLine.substr(prevPos, inputPos - prevPos);    //Gets only the word (for future use)
            token = inpLine.substr(prevPos);
            result = token;
            return token.size();
        }
    }

    //Nothing pending, so go and get it from the input file stream

    /* -- User input loop -- */
    while(isRunning() == LU_TRUE)
    {
        result.clear();     //empty storage buffer before reading

        //Each time we need to set values to be then overridden by select()
        tout = INPUT_TIMEOUT;
        FD_ZERO(&inFd);
        FD_SET(inCons, &inFd);

        /* Wait for user input */
        retSel = select(inCons+1, &inFd, NULL, NULL, &tout);

        if(isInterrupting() == LU_TRUE)
        {
            continue; //re-evaluate exit condition after waiting
        }
        if(retSel > 0)
        {
            if(FD_ISSET(inCons, &inFd))
            {
                //available data at input stream: read it
                result = getInput();
            }

            /* Check empty */
            if(result.size() == 0)
            {
                continue; //nothing read, so no need to evaluate
            }

            /* Check escape chars */
            if( (result.size() > 1) && (result.size() < 5) )
            {
                if( (result[0] == 0x1b) && (result[1] == 0x0a) )
                {
                    //escape char followed by enter: escape sequence to exit
                    return -2;
                }
            }

            /* Check content */
            if( result.find_first_not_of(" \n") == std::string::npos )
            {
                continue; //nothing read, so no need to evaluate
            }
            else
            {
                return result.size();   //input OK
            }
        }
    }
    return -1;
}



/*
 *********************** End of file ******************************************
 */

