/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Serial link layer for Configuration Tool handler
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   09/07/12      pueyos_a    Initial very basic version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <cstdlib>
#include <string>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/select.h>
#include <termios.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "SerialCTHLinkLayer.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define BAUDRATE B115200

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

SerialCTHLinkLayer::SerialCTHLinkLayer( lu_uint32_t maxPayload  ,
                                        SCHED_TYPE schedType    ,
                                        lu_uint32_t priority    ,
                                        lu_char_t *serialPortID
                                      ) :
                  Thread(schedType, priority, LU_FALSE, serialPortID),
                  ICTHLinkLayer(maxPayload),
                  log(Logger::getLogger(SUBSYSTEM_ID_CT_HANDLER)),
                  serialPort(serialPortID),
                  serialfd(-1),
                  observer(NULL)
{}


SerialCTHLinkLayer::~SerialCTHLinkLayer()
{
    stopServer();
}


CTH_ERROR SerialCTHLinkLayer::startServer()
{
    const lu_char_t *FTITLE = "SerialCTHLinkLayer::startServer:";
    CTH_ERROR ret = CTH_ERROR_NONE;

    if(isRunning() == LU_TRUE)
    {
        return CTH_ERROR_RUNNING;
    }

    log.info("%s initialising serial port.", FTITLE);
    /* init serial port */
    serialfd = open(serialPort, O_RDWR | O_NOCTTY);
    if(serialfd < 0)
    {
        log.error("%s initialise serial port error %i", FTITLE, serialfd);
        ret = CTH_ERROR_CGI;
    }

    if(ret == CTH_ERROR_NONE)
    {
        /* Serial port initialised correctly. Start thread */
        if (Thread::start() != THREAD_ERR_NONE)
        {
            ret = CTH_ERROR_THREAD;
        }
    }

    if(ret != CTH_ERROR_NONE)
    {
        if(serialfd >= 0 )
        {
            /* Close port */
            close(serialfd);
        }
    }
    else
    {
        log.info("%s serial Configuration Tool up and running", FTITLE);
    }

    return ret;
}


CTH_ERROR SerialCTHLinkLayer::stopServer(lu_bool_t killThread)
{
    if(isRunning() != LU_TRUE)
    {
        return CTH_ERROR_THREAD;
    }

    /* Signal the thread to close */
    Thread::stop();

    if(serialfd >= 0 )
    {
        /* Close port */
        close(serialfd);
    }

    /* Wait thread termination */
    join();

    return CTH_ERROR_NONE;
}


CTH_ERROR SerialCTHLinkLayer::attach(ICTHLinkLayerObserver *observer)
{
    /* Check input parameters */
    if(observer == NULL)
    {
        return CTH_ERROR_PARAM;
    }

    /* If no observer already registered register the new one */
    if(this->observer == NULL)
    {
        this->observer = observer;
        return CTH_ERROR_NONE;
    }
    else
    {
        /* Observer already registered. Generate an error */
        return CTH_ERROR_OREGISTERED;
    }
}


CTH_ERROR SerialCTHLinkLayer::detach(ICTHLinkLayerObserver *observer)
{
    /* Check input parameters */
    if(observer == NULL)
    {
        return CTH_ERROR_PARAM;
    }

    /* Check if the observer we are deregistering is
     * the same is currently registered
     */
    if(this->observer == observer)
    {
       /* Deregister observer */
        this->observer = NULL;
        return CTH_ERROR_NONE;
    }
    else
    {
        return CTH_ERROR_OREGISTERED;
    }
}


CTH_ERROR SerialCTHLinkLayer::send(CTHMessage &message)
{
    const lu_char_t *FTITLE = "SerialCTHLinkLayer::send:";
    CTMessageRaw header;
    CTMessageRaw payload;
    lu_int32_t commRet;
    CTH_ERROR ret = CTH_ERROR_NONE;

    message.getRawHeader(header);
    message.getRawPayaload(payload);

    /* Send message header */
    if(ret == CTH_ERROR_NONE)
    {
        commRet = write(serialfd,
                        reinterpret_cast<lu_char_t*>(header.payloadPtr),
                        header.payloadLen
                        );
        if(commRet != static_cast<lu_int32_t>(header.payloadLen))
        {
            ret = CTH_ERROR_WRITE;
        }
    }

    /* Send message payload */
    if(ret == CTH_ERROR_NONE)
    {
        commRet = write(serialfd,
                        reinterpret_cast<lu_char_t*>(payload.payloadPtr),
                        payload.payloadLen
                        );
        if(commRet != static_cast<lu_int32_t>(payload.payloadLen))
        {
            ret = CTH_ERROR_WRITE;
        }
    }

    if(ret != CTH_ERROR_NONE)
    {
        log.error("%s serial write error (%d)", FTITLE, commRet);
    }

    return ret;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void SerialCTHLinkLayer::threadBody()
{
    const lu_char_t *FTITLE = "SerialCTHLinkLayer::threadBody:";
    lu_char_t *streamBuffer;
    CTMessageRaw payload;
    lu_int32_t result;
    lu_int32_t cLength;
    struct termios cfgserial;

    /* Allocate local stream buffer */
    streamBuffer = new lu_char_t[maxPayload];

    /* Set the payload using the streamBuffer */
    payload.payloadLen = 0;
    payload.payloadPtr = reinterpret_cast<lu_uint8_t*>(streamBuffer);

    cfmakeraw(&cfgserial);
    cfsetispeed(&cfgserial, BAUDRATE);  //input baudrate
    cfsetospeed(&cfgserial, BAUDRATE);  //output baudrate
    tcsetattr(serialfd, TCSANOW, &cfgserial);

    fd_set readfs, errorfs;
    struct timeval timeOut;
    timeOut.tv_sec = 5;
    timeOut.tv_usec = 0;

    while(isRunning() == LU_TRUE)
    {
        /* initialise select's sets */
        FD_ZERO(&readfs);
        FD_ZERO(&errorfs);
        FD_SET(serialfd, &readfs);
        FD_SET(serialfd, &errorfs);

        /* Wait for an incoming request */
        result = select(serialfd+1, &readfs, NULL, &errorfs, &timeOut);

        if(isInterrupting() == LU_TRUE)
            continue;

        if(result < 0)
        {
            log.error("%s select() error %i", FTITLE, result);
            //TODO: AP - empty buffer
            payload.payloadLen = 0;
            continue;
        }
        else if(result == 0)
        {
            //timeout
            //TODO: AP - empty buffer
            payload.payloadLen = 0;
        }
        else
        {
            //serial data received
            if(FD_ISSET(serialfd, &readfs))
            {
                /* store data chunk */
                cLength = read(serialfd, streamBuffer, maxPayload);

                std::string dataRcv = "";
                lu_char_t dataVal[10];
                for (int idx = 0; idx < cLength; ++idx)
                {
                    sprintf(dataVal, "0x%02X ", streamBuffer[idx]);
                    dataRcv += dataVal;
                }
                /* Log received data */
                log.debug("%s Data received. Length: %s, Data: %s",
                                FTITLE,
                                cLength,
                                dataRcv.c_str()
                              );

                /* Check Content length */
                if(cLength > 0)
                {
                    /* Log received message */
                    log.info("%s Request received. Length: %s", FTITLE, cLength);
                    /* Buffer correctly read. Forward the buffer */
                    if(observer != NULL)
                    {
                        /* Set payload length */
                        payload.payloadLen = cLength;
                        observer->newData(payload);

                        /* Reset payload length */
                        payload.payloadLen = 0;
                    }
                }
            }
        }
    }

    /* Close serial port */
    if(serialfd >= 0 )
    {
        close(serialfd);
    }

    /* free memory */
    if(streamBuffer != NULL)
    {
        delete[] streamBuffer;
    }
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
