/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM Common[AbstractCTHandler]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   16/08/13      wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <string.h>
#include <iostream>
#include <fstream>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "AbstractCTHandler.h"
#include "Logger.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
namespace CTH{

/**
 * \brief Get the internal payload of the Configuration Tool protocol message
 *
 * \param input reference to the input RAW buffer
 * \param output reference to the buffer where the internal payload is saved
 */
inline static void getPayload(const CTMessageRaw &input, CTMessagePayload &output);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
ReplyID replyID; // Global replyID instance

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
CTMsg_R_AackNack* createAckNack(const CTMessageHeader &header,
                                 const CTH_ACKNACKCODE ackNack)
{
	return new CTMsg_R_AackNack(header.messageType,
                                header.requestID,
                                replyID.newID(),
                                ackNack);
}

AbstractCTHandler::AbstractCTHandler(ICTHLinkLayer &linkLayer):
                                                  fileTransfer(),
                                                  linkLayer(linkLayer),
                                                  log(Logger::getLogger(SUBSYSTEM_ID_CT_HANDLER))
{
    /* Start the link layer server */
    linkLayer.attach(this);
}


AbstractCTHandler::~AbstractCTHandler(){
    linkLayer.stopServer();
}

void AbstractCTHandler::handleRequest(const CTMessageRaw& request)
{
    CTMessageHeader header;
    header.messageID = -1;
    header.messageType = -1;
    header.replyID = -1;
    header.requestID = -1;


    if(decodeHeader(request,header) == LU_TRUE)
    {
        /*Debug*/
        if(log.isDebugEnabled())
        {

            /* TODO: cause memory leaking
             * log.debug( "Start handling CTH message.%s[PL size:%i]",
                        header.toString().c_str(),
                        request.dataLen - sizeof(CTMessageHeader)
                     );*/
        }

    	/* Create a reply message by decoding the request*/
    	CTHMessage* reply = decodePayload(header,request);

    	/*Request handled. Send reply message over the link layer*/
    	if(reply != NULL)
    	{
    	    /*Debug*/
            if(log.isDebugEnabled())
            {
                // TODO: cause memory leaking
                //log.debug( "Composed replied CTH message.%s",reply->toString().c_str());
            }

    	   /*Sending */
    	   lu_uint32_t ret = linkLayer.send(*reply);

    	   if(ret!= CTH_ERROR_NONE)
           {
    	       log.error("Failed to send CTH message over link layer[Err:%i]",ret);
           }

    	   delete reply;
    	}

    	/*Request not handled*/
    	else
    	{
    	    log.warn("Unsupported message. %s[PL size:%i]",
                       header.toString().c_str(),
                       request.dataLen - sizeof(CTMessageHeader)
                     );
    	}

    }

    /*Header not decoded. Discard request.*/
    else
    {
        log.warn("Discarded unrecognised message.");
    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
lu_bool_t AbstractCTHandler::decodeHeader(const CTMessageRaw &request,
                                                CTMessageHeader &header)
{
    const lu_char_t *FTITLE = "AbstractCTHandler::decodeHeader:";

    /* Validate header length*/
    if(request.dataLen < sizeof(CTMessageHeader))
    {
        log.error("%s Cannot decode header.Invalid size:%i. ", FTITLE, request.dataLen);

        return LU_FALSE;
    }

    /*Decode the content of header*/
    const CTMessageHeader *headerPtr = reinterpret_cast<CTMessageHeader*>(request.dataPtr);
    header.messageID = headerPtr->messageID;
    header.messageType = headerPtr->messageType;
    header.replyID = headerPtr->replyID;
    header.requestID = headerPtr->requestID;

    return LU_TRUE;
}

CTHMessage* AbstractCTHandler::decodePayload(const CTMessageHeader& header,
                                             const CTMessageRaw& request)
{
	CTHMessage* ret = NULL;

	/* Get the payload in request message*/
    CTMessagePayload payload;
	getPayload(request, payload);

	/* Decode Message Type */
	switch(header.messageType)
	{
		case CTMsgType_FileWrite:
		case CTMsgType_FileRead:
			ret = fileTransfer.decodeFileTransferMessage(header, payload);
		break;

		default:
			ret = decodePayload(header,payload);
		break;
	}//endswitch(header.messageType)

	return ret;
}


CTHMessage* AbstractCTHandler::decodeGetSDPVersion(const CTMessageHeader &header,
                    const CTMessagePayload &payload)
{
    LU_UNUSED(payload);

    CTMsg_R_SDPVersion* reply = new CTMsg_R_SDPVersion(header.requestID, replyID.newID());
    CTMsgPL_R_SDPVersion* rPayload = reinterpret_cast<CTMsgPL_R_SDPVersion*>(reply->firstEntry());

    std::string sdpversion;
    std::ifstream sdpfile (SDP_VERSION_FILENAME);
    std::string line;

    if (sdpfile.is_open())
    {
      while ( std::getline (sdpfile,line) )
      {
          sdpversion += line;
          sdpversion += "\r\n";
      }
      sdpfile.close();

      log.info("Read SDP version:%s", sdpversion.c_str());
      const char* sdpversionPtr = sdpversion.c_str();
      strncpy(rPayload->sdpVersion, sdpversionPtr, rPayload->SDPVersionlength);
      reply->setEntries(1);
    }
    else
    {
        log.error("Unable to open SDP file:%s",SDP_VERSION_FILENAME);
    }

    return reply;
}


inline void getPayload(const CTMessageRaw &input, CTMessagePayload &output)
{
    /* Check if there is a payload */
    if(input.dataLen > sizeof(CTMessageHeader))
    {
        output.payloadLen = input.dataLen - sizeof(CTMessageHeader);
        output.payloadPtr = input.dataPtr + sizeof(CTMessageHeader);
    }
    else
    {
        output.payloadLen = 0;

        output.payloadPtr = NULL;
    }
}

}//End namespace CTH

/*
 *********************** End of file ******************************************
 */
