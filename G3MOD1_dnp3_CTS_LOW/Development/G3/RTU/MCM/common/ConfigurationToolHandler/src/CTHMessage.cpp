/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/02/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "CTHMessage.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

/*
 * Implementation of CTHMessage
 */
CTHMessage::CTHMessage( lu_uint16_t messageType,
                        lu_uint16_t messageID,
                        lu_uint16_t requestID,
                        lu_uint16_t replyID
                      )
{
    header.messageType = messageType;
    header.messageID   = messageID  ;
    header.requestID   = requestID  ;
    header.replyID     = replyID    ;
}

CTHMessage::~CTHMessage()
{ }

CTH_ERROR CTHMessage::getRawHeader(CTMessageRaw &headerStream)
{
    /* Set Payload */
    headerStream.dataPtr = reinterpret_cast<lu_uint8_t*>(&header);
    headerStream.dataLen = sizeof(header);

    return CTH_ERROR_NONE;
}

/*
 * Implementation of CTMsg_R_AackNack
 */
CTH_ERROR CTMsg_R_AackNack::getRawPayaload(CTMessageRaw &messageRaw)
{
    messageRaw.dataLen = sizeof(CTMsgPL_R_AckNack);
    messageRaw.dataPtr = reinterpret_cast<lu_uint8_t*>(&payload);

    return CTH_ERROR_NONE;
}

std::string CTMsg_R_AackNack::toString()
{
    std::stringstream ss;
    ss << header.toString();
    ss << " [PL size:" << getRawPayloadSize() << "]";
    ss << payload.toString();
    return ss.str();
}

lu_uint32_t CTMsg_R_AackNack::getRawPayloadSize()
{
   return PAYLOAD_SIZE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
