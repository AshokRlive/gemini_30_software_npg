/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       System & Event log manager implementation for the Configuration Tool
 *       Handler.
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   20/09/12      pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <string>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "dbg.h"
#include "LogReqHandler.h"
#include "AbstractCTHandler.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

namespace CTH
{

void getFileListStr(const lu_char_t* directory, std::string& result);

LogReqHandler::LogReqHandler() :
                            log(Logger::getLogger(SUBSYSTEM_ID_CT_HANDLER)),
                            bufferLogger(Logger::getBufferLogger())

{
}

LogReqHandler::~LogReqHandler() {}


CTHMessage* LogReqHandler::decodeMessageLog(const CTMessageHeader &header,
                                       const CTMessagePayload &payload)
{
    log.info("%s \"%s\" message received.", __AT__, CTMsgID_Log_ToSTRING(header.messageID));

    /* Decode message ID */
    switch(header.messageID)
    {
        case CTMsgID_C_GetLastLog:
            /* Get latest log entries message */
            return decodeGetLogListMessage(header, payload);
        break;

        case CTMsgID_C_GetLogLevel:
            /* Get loglevel message */
            return decodeGetLogLevelMessage(header, payload);
        break;

        case CTMsgID_C_SetLogLevel:
            /* Set loglevel message*/
            return decodeSetLogLevelMessage(header, payload);
        break;

         case CTMsgID_C_GetLogFileName:
            /* Get Log file name*/
            return decodeGetLogFileName(header, payload);
         break;

        default:
            /* Unsupported message type */
            return createAckNack(header,CTH_NACK_UNSUPPORTED);
    }//endswitch(header.messageID)
}


CTHMessage* LogReqHandler::decodeGetLogListMessage(const CTMessageHeader &header,
                                                   const CTMessagePayload &payload
                                                  )
{
    CTH_ACKNACKCODE ret = CTH_ACK;

    if(payload.payloadLen != 0)
    {
        /* invalid payload */
        ret = CTH_NACK_PAYLOAD;
        return createAckNack(header,ret);
    }

    CTMsg_R_LogList* reply = new CTMsg_R_LogList(header.requestID, replyID.newID());

    lu_char_t *logPtr = reinterpret_cast<lu_char_t *>(&(*reply)[0]);
    lu_uint32_t paySize;
    if(bufferLogger.getLogMessage(logPtr, CTMsg_MaxPayloadSize, &paySize) == 0)
    {
        reply->setEntries(paySize);//TODO AP - replace with appendBytes()
        return reply;
    }
    else
    {
        delete reply;
        ret = CTH_NACK_LOGLISTEMPTY;
        return createAckNack(header,ret);
    }
}

CTHMessage* LogReqHandler::decodeGetLogLevelMessage( const CTMessageHeader &header,
                                                    const CTMessagePayload &payload
                                                   )
{
    CTH_ACKNACKCODE ret = CTH_ACK;
    std::ostringstream ssDebugMsg;  //only for debug messages
    CTMsgPL_R_LogLevels *loglevelPtr; //pointer to list element inside message

    if(payload.payloadLen != 0)
    {
        /* invalid payload */
        ret = CTH_NACK_PAYLOAD;
        return createAckNack(header,ret);
    }

    CTMsg_R_LogLevels* reply = new CTMsg_R_LogLevels(header.requestID, replyID.newID());

    for(lu_uint8_t subSys = 0; subSys < SUBSYSTEM_ID_LAST; subSys++)
    {
        /*compose message */
        loglevelPtr = &(*reply)[(SUBSYSTEM_ID)subSys];   //convert to message list format
        loglevelPtr->subsystem = (SUBSYSTEM_ID)subSys;
        loglevelPtr->level = Logger::getLogger((SUBSYSTEM_ID)subSys).getLogLevel();
        //compose debug message
        if(log.isDebugEnabled() == LU_TRUE)
        {
            ssDebugMsg << log.getSubSystemName((SUBSYSTEM_ID)subSys) <<
                        "=" <<
                        log.getLogLevelName((LOG_LEVEL)loglevelPtr->level) <<
                        ";";
        }
    }
    reply->setEntries(SUBSYSTEM_ID_LAST);  // Set the number of valid entries

    // TODO cause memory leaking
    //log.debug("%s Get Log levels: %s", FTITLE, ssDebugMsg.str().c_str());
    return reply;
}

CTHMessage* LogReqHandler::decodeSetLogLevelMessage(const CTMessageHeader &header,
                                                const CTMessagePayload &payload)
{
    const lu_char_t *FTITLE = "LogHandler::decodeSetLogLevelMessage:";
    CTH_ACKNACKCODE ret = CTH_NACK_GENERIC;
    LOG_LEVEL level;
    SUBSYSTEM_ID subSys;
    CTMsgPL_C_LogLevels *loglevelPtr; //pointer to list element inside message

    if(payload.payloadLen != sizeof(CTMsgPL_C_LogLevels))
    {
        /* invalid payload */
        ret = CTH_NACK_PAYLOAD;
        return createAckNack(header,ret);
    }

    /* Decode valid payload */
    loglevelPtr = reinterpret_cast<CTMsgPL_C_LogLevels *>(payload.payloadPtr);
    level = (LOG_LEVEL)loglevelPtr->level;
    if(loglevelPtr->subsystem == CTMsgPL_C_LogLevels::SUBSYSTEM_ID_ALL)
    {
        subSys = SUBSYSTEM_ID_LAST; //set ALL the subsystems to this loglevel
    }
    else
    {
        if(loglevelPtr->subsystem >= SUBSYSTEM_ID_LAST)
        {
            ret = CTH_NACK_BADSUBSYSTEM;
            return createAckNack(header,ret);
        }
        else
        {
            subSys = (SUBSYSTEM_ID)(loglevelPtr->subsystem);
        }
    }

    if(subSys == SUBSYSTEM_ID_LAST)
    {
        /* Set ALL subsystems */
        if(log.setAllLogLevel(level) != LOG_ERROR_NONE)
        {
            log.error("%s Error setting log level %i for all subsystems.", FTITLE,
                                level
                                );
            ret = CTH_NACK_BADLOGLEVEL;
        }
        else
        {
            ret = CTH_ACK;
            log.info("%s Set log level %s for ALL subsystems.", FTITLE,
                        log.getLogLevelName(level)
                        );
        }
    }
    else
    {
        if(Logger::getLogger(subSys).setLogLevel(level) != LOG_ERROR_NONE)
        {
            log.error("%s Error setting log level %i for %s subsystem.", FTITLE,
                                level, log.getSubSystemName(subSys)
                                );
            ret = CTH_NACK_BADLOGLEVEL;
        }
        else
        {
            log.info("%s set subsystem %s to log level %s", FTITLE,
                        log.getSubSystemName((SUBSYSTEM_ID)loglevelPtr->subsystem),
                        log.getLogLevelName((LOG_LEVEL)loglevelPtr->level)
                        );
            ret = CTH_ACK;
        }
    }

    return createAckNack(header,ret);
}

CTHMessage* LogReqHandler::decodeGetLogFileName(const CTMessageHeader &header,
                                    const CTMessagePayload &payload)
{
    std::string   logFileList;
    LU_UNUSED(payload);

    CTMsg_R_LogFileName* reply = new CTMsg_R_LogFileName(header.requestID,
                                        replyID.newID());

    reply->setEntries(0);

    getFileListStr(G3_LOG_FOLDER,logFileList);
    DBG_INFO("Current Log file list:%s",logFileList.c_str());

    reply->appendBytes((lu_uint8_t*)(logFileList.c_str()), logFileList.length());

    return reply;
}

}//end namespace CTH
/*
 *********************** End of file ******************************************
 */
