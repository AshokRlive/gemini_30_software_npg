/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <cstdlib>
#include <cstring>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "ZMQLinkLayer.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
#define ZMQ_TCP_PORT "tcp://*:5555"

//#define DEBUG_ZMQ

inline void debug_print(const lu_char_t *fmt, ...)
{
#ifdef DEBUG_ZMQ
     va_list args;
     va_start (args, fmt);
     fprintf(stdout, fmt, args);
     va_end (args);
#else
     LU_UNUSED(fmt);
#endif
}


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
ZMQLinkLayer::ZMQLinkLayer( lu_uint32_t maxPayload  ,
                                    SCHED_TYPE schedType    ,
                                    lu_uint32_t priority    ,
                                    const lu_char_t *cgiSocketName
                                  )
    : Thread(schedType, priority, LU_TRUE, cgiSocketName),
      ICTHLinkLayer(maxPayload),
      context(NULL),
      responder(NULL),
      cgiSocketName(cgiSocketName),
      observer(NULL),
      streamBuffer(NULL),
      log(Logger::getLogger(SUBSYSTEM_ID_CT_HANDLER))
{
     debug_print("ZMQ -> ZMQLinkLayer created\n");
}


ZMQLinkLayer::~ZMQLinkLayer()
{
    stopServer();

    delete[] streamBuffer;
    observer = NULL;

    debug_print("ZMQ -> ZMQLinkLayer destructed\n");
}


CTH_ERROR ZMQLinkLayer::startServer()
{
    CTH_ERROR ret = CTH_ERROR_NONE;

    if(isRunning() == LU_TRUE)
    {
        return CTH_ERROR_RUNNING;
    }

    /* +++ Prepare socket connection +++ */
    log.info("ZMQLinkLayer::startServer initialising ZMQ.");

    context = zmq_ctx_new ();
    responder = zmq_socket (context, ZMQ_REP);
    int rc = zmq_bind (responder, ZMQ_TCP_PORT);
    assert (rc == 0);
    if(rc != 0)
    {
        log.error( "ZMQLinkLayer::startServer initialise ZMQ socket error %i",rc);
        debug_print("ZMQ -> ZMQ socket binding error %i\n",rc);

        ret = CTH_ERROR_CGI;
    }

    if(ret == CTH_ERROR_NONE)
    {
        /* ZMQ initialised correctly. Start thread */
        if (Thread::start() != THREAD_ERR_NONE)
        {
            ret = CTH_ERROR_THREAD;
        }
    }

    return ret;
}


CTH_ERROR ZMQLinkLayer::stopServer(lu_bool_t killThread)
{
    /* Signal the thread to close */
    return (Thread::stop() != THREAD_ERR_NONE)? CTH_ERROR_THREAD : CTH_ERROR_NONE;
}


CTH_ERROR ZMQLinkLayer::attach(ICTHLinkLayerObserver *observer)
{
    /* Check input parameters */
    if(observer == NULL)
    {
        return CTH_ERROR_PARAM;
    }

    /* If no observer already registered register the new one */
    if(this->observer == NULL)
    {
        this->observer = observer;
        return CTH_ERROR_NONE;
    }
    else
    {
        /* Observer already registered. Generate an error */
        return CTH_ERROR_OREGISTERED;
    }
}


CTH_ERROR ZMQLinkLayer::detach(ICTHLinkLayerObserver *observer)
{
    /* Check input parameters */
    if(observer == NULL)
    {
        return CTH_ERROR_PARAM;
    }

    /* Detach observers */
    if(this->observer == observer)
    {
        this->observer = NULL;
        return CTH_ERROR_NONE;
    }
    else
    {
        return CTH_ERROR_OREGISTERED;
    }
}


CTH_ERROR ZMQLinkLayer::send(CTHMessage &message)
{
    CTMessageRaw header; // CT message header
    CTMessageRaw payload;// CT message payload
    CTH_ERROR ret = CTH_ERROR_NONE;

    message.getRawHeader(header);
    message.getRawPayaload(payload);

    /* Set content type */
    lu_int32_t nbytes0 = zmq_send (responder, header.dataPtr, header.dataLen, ZMQ_SNDMORE);
    lu_int32_t nbytes1 = zmq_send (responder, payload.dataPtr, payload.dataLen, 0);

    if(nbytes0 < 0 || nbytes1 <0)
    {
        perror("The following error occurred");
        ret = CTH_ERROR_WRITE;
    }else
    {
        debug_print("ZMQ -> Send reply. Bytes: %d\n",nbytes0 + nbytes1);
    }

    if(ret != CTH_ERROR_NONE)
    {
        log.error("ZMQLinkLayer::send send message error");
    }

    return ret;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void ZMQLinkLayer::threadBody()
{
    CTMessageRaw payload;
    lu_int32_t nbytes;

    /* Allocate local stream buffer */
    streamBuffer = new lu_char_t[maxPayload];

    /* Set the payload using the streamBuffer */
    payload.dataLen = 0;
    payload.dataPtr = reinterpret_cast<lu_uint8_t*>(streamBuffer);


    while (1)
    {
        nbytes = zmq_recv(responder, streamBuffer, maxPayload, 0);
        if (nbytes < 0)
        {
            closeContext();
            break;
        }

        //TODO validate request if it is malformed or invalid size?

        debug_print("ZMQ -> Received Request. Bytes: %d\n",nbytes);

        /* Buffer correctly read. Forward the buffer */
        if (observer != NULL)
        {
            /* Set payload length */
            payload.dataLen = nbytes;

            /*Handle request*/
            observer->handleRequest(payload);

            /* Reset payload length */
            payload.dataLen = 0;
        }
    }

}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void ZMQLinkLayer::closeContext()
{
   if (context != NULL)
   {
       zmq_ctx_destroy(context);
       context = NULL;

       debug_print( "ZMQ -> ZMQ context is destroyed");
   }
}


/*
 *********************** End of file ******************************************
 */
