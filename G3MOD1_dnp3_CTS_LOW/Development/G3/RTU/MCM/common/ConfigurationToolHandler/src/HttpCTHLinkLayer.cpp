/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <cstdlib>
#include <cstring>
#include <sys/stat.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "HttpCTHLinkLayer.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/* Webserver socket listen queue depth */
const static lu_int32_t listenQueueDepth = 5;

const static lu_int32_t LISTENSOCK_FLAGS = FCGI_FAIL_ACCEPT_ON_INTR;

const static lu_char_t contentTypeHeader[] = "Content-Type: message/g3\r\n\r\n";
const static lu_uint32_t contentTypeHeaderLen = sizeof(contentTypeHeader) - 1;

const static lu_char_t contentType[] = "message/g3";
const static lu_uint32_t contentTypeLen = sizeof(contentType) - 1;

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

HttpCTHLinkLayer::HttpCTHLinkLayer( lu_uint32_t maxPayload  ,
                                    SCHED_TYPE schedType    ,
                                    lu_uint32_t priority    ,
                                    const lu_char_t *cgiSocketName
                                  )
    : Thread(schedType, priority, LU_TRUE, cgiSocketName),
      ICTHLinkLayer(maxPayload),
      cgiSocketName(cgiSocketName),
      observer(NULL),
      streamBuffer(NULL),
      log(Logger::getLogger(SUBSYSTEM_ID_CT_HANDLER)),
      stopping(LU_FALSE)
{}


HttpCTHLinkLayer::~HttpCTHLinkLayer()
{
    stopServer();
    /* Close connection and socket */
    FCGX_Free(&cgi, LU_TRUE);
    /* Remove used elements */
    delete[] streamBuffer;
    observer = NULL;
}


CTH_ERROR HttpCTHLinkLayer::startServer()
{
    CTH_ERROR ret = CTH_ERROR_NONE;
    lu_int32_t socketfd = -1;
    lu_int32_t cgiErr;

    if(isRunning() == LU_TRUE)
    {
        return CTH_ERROR_RUNNING;
    }

    /* +++ Prepare socket connection +++ */
    log.info("HttpCTHLinkLayer::startServer initialising fast CGI.");

    /* call before Accept in multithreaded apps */
    cgiErr = FCGX_Init();
    if(cgiErr != 0)
    {
        log.error( "HttpCTHLinkLayer::startServer initialise CGI error %i",
                        cgiErr
                      );
        ret = CTH_ERROR_CGI;
    }

    if(ret == CTH_ERROR_NONE)
    {
        /* Open unix socket to communicate with the webserver. Set
         * Set the permission mask to 777 (lighttp runs as user lighttp)
         */
        __mode_t oldMask = umask(0);
        socketfd = FCGX_OpenSocket(cgiSocketName, listenQueueDepth);

        /* Restore previous global permission mask */
        umask(oldMask);

        if(socketfd < 0)
        {
            log.error("HttpCTHLinkLayer::startServer open CGI socket (%s) error.",
                            cgiSocketName, cgiErr
                          );
            ret = CTH_ERROR_CGI;
        }
    }

    if(ret == CTH_ERROR_NONE)
    {
        cgiErr = FCGX_InitRequest(&cgi, socketfd, LISTENSOCK_FLAGS);
        if(cgiErr < 0)
        {
            log.error("HttpCTHLinkLayer::startServer FCGX_InitRequest error %i", cgiErr);
            ret = CTH_ERROR_CGI;
        }
    }

    if(ret == CTH_ERROR_NONE)
    {
        /* fast CGI initialised correctly. Start thread */
        if (Thread::start() != THREAD_ERR_NONE)
        {
            ret = CTH_ERROR_THREAD;
        }
    }
    if(ret != CTH_ERROR_NONE)
    {
        if(socketfd < 0 )
        {
            /* Close socket */
            FCGX_Free(&cgi, LU_TRUE);
        }

    }
    else
    {
        log.info("HttpCTHLinkLayer::startServer fast CGI up and running.");
    }

    return ret;
}


CTH_ERROR HttpCTHLinkLayer::stopServer(lu_bool_t killThread)
{
    /* Don't accept anymore connections */
    FCGX_ShutdownPending();

    stopping = LU_TRUE;

    /* Signal the thread to close */
    if(killThread == LU_TRUE)
        return (Thread::stop() != THREAD_ERR_NONE)? CTH_ERROR_THREAD : CTH_ERROR_NONE;
    else
        return CTH_ERROR_NONE;
}


CTH_ERROR HttpCTHLinkLayer::attach(ICTHLinkLayerObserver *observer)
{
    /* Check input parameters */
    if(observer == NULL)
    {
        return CTH_ERROR_PARAM;
    }

    /* If no observer already registered register the new one */
    if(this->observer == NULL)
    {
        this->observer = observer;
        return CTH_ERROR_NONE;
    }
    else
    {
        /* Observer already registered. Generate an error */
        return CTH_ERROR_OREGISTERED;
    }
}


CTH_ERROR HttpCTHLinkLayer::detach(ICTHLinkLayerObserver *observer)
{
    /* Check input parameters */
    if(observer == NULL)
    {
        return CTH_ERROR_PARAM;
    }

    /* Check if the observer we are deregistering is
     * the same is currently registered
     */
    if(this->observer == observer)
    {
       /* Deregister observer */
        this->observer = NULL;
        return CTH_ERROR_NONE;
    }
    else
    {
        return CTH_ERROR_OREGISTERED;
    }
}


CTH_ERROR HttpCTHLinkLayer::send(CTHMessage &message)
{
    CTMessageRaw header; // CT message header
    CTMessageRaw payload;// CT message payload
    lu_int32_t cgiRet;
    CTH_ERROR ret = CTH_ERROR_NONE;

    message.getRawHeader(header);
    message.getRawPayaload(payload);

    /*Debug*/
    /* TODO: cause memory leaking
    if(log.isDebugEnabled())
    {
        std::string rawDataStr = header.toHexString();
        rawDataStr += payload.toHexString();
        log.debug("HttpCTHLinkLayer::Sending Data[Size:%i] %s",
                    header.dataLen + payload.dataLen,
                    rawDataStr.c_str());
    }*/

    /* Set content type */
    cgiRet = FCGX_PutStr(contentTypeHeader, contentTypeHeaderLen, cgi.out);
    if(cgiRet != static_cast<lu_int32_t>(contentTypeHeaderLen))
    {
        ret = CTH_ERROR_WRITE;
    }

    /* Send message header */
    if(ret == CTH_ERROR_NONE)
    {
        cgiRet = FCGX_PutStr( reinterpret_cast<lu_char_t*>(header.dataPtr),
                              header.dataLen,
                              cgi.out
                            );
        if(cgiRet != static_cast<lu_int32_t>(header.dataLen))
        {
            ret = CTH_ERROR_WRITE;
        }
    }

    /* Send message payload */
    if(ret == CTH_ERROR_NONE)
    {
        cgiRet = FCGX_PutStr( reinterpret_cast<lu_char_t*>(payload.dataPtr),
                              payload.dataLen,
                              cgi.out
                            );
        if(cgiRet != static_cast<lu_int32_t>(payload.dataLen))
        {
            ret = CTH_ERROR_WRITE;
        }
    }

    if(ret != CTH_ERROR_NONE)
    {
        log.error("HttpCTHLinkLayer::send write error");
    }

    return ret;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void HttpCTHLinkLayer::threadBody()
{
    lu_int32_t cgiErr;
    CTMessageRaw payload;
    lu_char_t *cTypePtr, *cLengthPtr;
    lu_int32_t cLength;
    lu_int32_t readByte;

    /*** Handle Incoming messages ***/

    /* Allocate local stream buffer */
    streamBuffer = new lu_char_t[maxPayload];

    /* Set the payload using the streamBuffer */
    payload.dataLen = 0;
    payload.dataPtr = reinterpret_cast<lu_uint8_t*>(streamBuffer);

    while(isRunning() == LU_TRUE && stopping == LU_FALSE)
    {
        /* Wait for an incoming request */
        cgiErr = FCGX_Accept_r(&cgi);

        if(isInterrupting() == LU_TRUE)
            continue;   //exit thread

        if(cgiErr != 0)
        {
            log.error("HttpCTHLinkLayer::threadBody FCGX_Accept_r error %i",cgiErr);

            continue;//MG ?????? or break ????
        }

        /* Get request environment parameters */
        cTypePtr = FCGX_GetParam("CONTENT_TYPE", cgi.envp);
        cLengthPtr = FCGX_GetParam("CONTENT_LENGTH", cgi.envp);

        /* Log received message */
        log.debug( "HttpCTHLinkLayer Request Received. Content-Type: %s - Content-Length: %s",
                   cTypePtr,
                   cLengthPtr
                  );

        /* Validate request */
        if( ( cTypePtr != NULL) && (cLengthPtr != NULL) )
        {
            /* Convert length to integer */
            cLength = atoi(cLengthPtr);

            /* Check Content length and Content type */
            if( (cLength <= static_cast<lu_int32_t>(maxPayload))      &&
                (strncmp(cTypePtr, contentType, contentTypeLen) == 0)
              )
            {
                /* Valid Stream - Read It */
                readByte = FCGX_GetStr(streamBuffer, cLength, cgi.in);
                if(readByte == cLength)
                {
                    /* Buffer correctly read. Forward the buffer */
                    if(observer != NULL)
                    {
                        /* Set payload length */
                        payload.dataLen = cLength;

                        /*Debug*/
                        // TODO cause memory leaking
                        /*if(log.isDebugEnabled())
                        {
                            std::string rawDataStr = payload.toHexString();
                            log.debug( "HttpCTHLinkLayer::Received Data[Size:%i]%s",
                                         payload.dataLen,
                                        rawDataStr.c_str());
                        }*/

                        /*Hander request*/
                        observer->handleRequest(payload);

                        /* Reset payload length */
                        payload.dataLen = 0;
                    }
                }
            }
            else
            {
                log.error("HttpCTHLinkLayer Request refused: payload too big or malformed request.");
            }
        }

        FCGX_Finish_r(&cgi);
    }
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
