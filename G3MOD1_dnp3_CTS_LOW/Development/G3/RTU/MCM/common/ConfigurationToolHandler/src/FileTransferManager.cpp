/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Configuration Tool File transferring implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25/04/12      pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>
#include <iostream>
#include <sys/stat.h> //stat
#include <dirent.h> //opendir
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "FileTransferManager.h"
#include "AbstractCTHandler.h"
#include "Utilities.h"

#include "dbg.h"

namespace CTH{
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define TIMEOUT     10 // File transfer timeout (seconds)

#define FILETEMPEXT ".tmp"  //temporal transferred file
#define FILEOLDEXT ".old"   //backup of the previously existent file
#define FILEBACKUP ".bkp"   //Backup of the configuration
#define FILEDEACTIVATED ".incoming" //Not yet activated file

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static void createDefaultFolders();
/**
 * \brief Backups the configuration file
 *
 * This copies the configuration file on a backup file. Previously existing
 * backup file will be destroyed.
 * If no configuration file is present, nothing is done, and CTH_ACK is returned.
 *
 * \return CTH_ACK on success.
 */
static CTH_ACKNACKCODE backupCfg();

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
void getFileListStr(const char* directory, std::string& result);

FileTransferManager::FileTransferManager():
                                        inFileWriting(LU_FALSE),
                                        inFileReading(LU_FALSE),
                                        log(Logger::getLogger(SUBSYSTEM_ID_CT_HANDLER)),
                                        fwTimer(NULL),
                                        frTimer(NULL)
{
    createDefaultFolders();

    /* Set initial state */
    initFileWriting();
    initFileReading();

}


FileTransferManager::~FileTransferManager()
{
    /* remove timers */
    delete fwTimer;
    delete frTimer;
}


CTHMessage* FileTransferManager::decodeFileTransferMessage(const CTMessageHeader &header,
                                                           const CTMessagePayload &payload)
{
    /* Decode file writing message */
    if(header.messageType == CTMsgType_FileWrite)
    {
        /* Decode message ID */
        switch (header.messageID)
        {
            /* Start File Writing*/
            case CTMsgID_C_FWriteStart:
                return fileWriteStartMessage(header, payload);

                /* Write File Fragment*/
            case CTMsgID_C_FWriteFragment:
                return fileWriteFragmentMessage(header, payload);

                /* Finish/Cancel File Writing*/
            case CTMsgID_C_FWriteFinish:
            {
                CTHMessage* reply = fileWriteFinishMessage(header, payload);
                //            if(fileTransfer.isConfigRecve()){
                //                configRecvei(fileTransfer.getConfigCRC());
                //            }
                return reply;
            }
            default:
                /*Unsupported message ID*/
                return NULL;
        }    //endswitch(header.messageID)
    }

    /* Decode file reading message */
    else if(header.messageType == CTMsgType_FileRead)
    {
        /* Decode message ID */
        switch(header.messageID)
        {
            /* Start File Reading */
            case CTMsgID_C_FReadStart:
                return fileReadStartMessage(header,payload);

            /* Read File Fragment */
            case CTMsgID_C_FReadFragment:
                return fileReadFragmentMessage(header,payload);

            /* Cancel File Reading */
            case CTMsgID_C_FReadCancel:
                return  fileReadCancelMessage(header,payload);

            case CTMsgID_C_FReadFileList:
            	return decodeGetFileList(header,payload);

            /*Unsupported message ID*/
            default:
                return NULL;
        }//endswitch(header.messageID)
    }
    return NULL;    //Unsupported message type
}


CTHMessage* FileTransferManager::decodeGetFileList(const CTMessageHeader &header,
                                    const CTMessagePayload &payload)
{
    std::string   fileList;
    lu_char_t     dir[payload.payloadLen + 1];

    memcpy(dir, payload.payloadPtr, payload.payloadLen);
    dir[payload.payloadLen] = '\0';

    getFileListStr(dir,fileList);


    CTMsg_R_FileList* reply = new CTMsg_R_FileList(header.requestID, replyID.newID());
    reply->setEntries(0);
    reply->appendBytes((lu_uint8_t*)(fileList.c_str()), fileList.length());

    DBG_INFO("File list at dir:%s \n:%s", dir, fileList.c_str());


    return reply;
}


CTHMessage* FileTransferManager::fileWriteStartMessage(const CTMessageHeader &header,
                                                       const CTMessagePayload &payload)
{
    /*Check state*/
    if(inFileWriting == LU_TRUE)
    {
    	/* Transferring is in progress*/
        log.warn("Cannot start file writing which is already in progress!");
        return createAckNack(header,CTH_NACK_TRANSFER);
    }

    /*Check payload size*/
    if(payload.payloadLen < sizeof(CTMsgPL_C_FileWriteStart))
    {
    	/* Invalid payload size*/
        log.warn("Invalid payload length for start file writing. Expected:%i, Actual:%i",
                        sizeof(CTMsgPL_C_FileWriteStart),
                        payload.payloadLen
                 );
        return createAckNack(header,CTH_NACK_PAYLOAD);
    }

    /*Set initial state */
    initFileWriting();

    /* Decode payload content */
    CTH_ACKNACKCODE ret = getPayloadContent(
                reinterpret_cast<CTMsgPL_C_FileWriteStart*>(payload.payloadPtr),
                &fwrite);

    /*Fail to decode payload content*/
    if(ret != CTH_ACK)
    {
        log.error("Fail to decode payload content!");
        return createAckNack(header,ret);
    }

    /* Create directories as necessary */
    if(makeDirs(fwrite.fileName) != CTH_ACK)
    {
        //Unable to create/access these directories
        log.error("Path not accessible for file %s", fwrite.fileName.c_str());
        return createAckNack(header, CTH_NACK_INVALIDPATH);
    }

    /* Open temporary file for writing */
    std::string tempFileName = fwrite.fileName + FILETEMPEXT;
    lu_int32_t fd = open(tempFileName.c_str(),
							O_CREAT | O_RDWR, (mode_t)fwrite.permissions);
	/* Open failed */
    if(fd < 0)
	{
        log.error("Cannot create file %s", fwrite.fileName.c_str());
		return createAckNack(header,CTH_NACK_OPEN);
	}

	/* Check available space in the file system */
	if (checkSpaceAvail(tempFileName.c_str(), fwrite.fileSize) == LU_FALSE)
	{
		/* No available space */
	    log.error("No available space for creating file %s", fwrite.fileName.c_str());
	    close(fd);
	    unlink(tempFileName.c_str());   //remove created file
		return createAckNack(header,CTH_NACK_NOSPACE);
	}

	/* Start writing succeeded */
	fwrite.tempFile = fd;
	fwrite.tempFileName = tempFileName;

    /* Go into writing state */
    log.debug("%s File Writing started.");
	inFileWriting = LU_TRUE;

	/* Start timeout timer */
	delete fwTimer;     //stop and remove any previous timer
    fwTimer = new Timer(TIMEOUT, 0, this, Timer::TIMER_TYPE_ONESHOT, "FileWriteTmr");
    if(fwTimer->start() < 0)
    {
        log.error("Unable to create Timer for file writing operation");
    }

    return createAckNack(header,CTH_ACK);
}


CTHMessage* FileTransferManager::fileWriteFragmentMessage(const CTMessageHeader &header,
                                                          const CTMessagePayload &payload)
{
    /* Check state */
    if(inFileWriting != LU_TRUE)
    {
    	/* Transferring is NOT in progress*/
        log.warn("File writing not started!");
        return createAckNack(header,CTH_NACK_NOTRANSFER);
    }

    /* Check payload size*/
    if(payload.payloadLen < sizeof(CTMsgPL_C_WriteFileFrag))
    {
        /*Invalid payload size*/
        log.warn("Invalid payload length. Expected: %i Actual:%i %s",
                        sizeof(CTMsgPL_C_WriteFileFrag),
                        payload.payloadLen,
                        __AT__);
        return createAckNack(header,CTH_NACK_PAYLOAD);
    }

    /* Decode payload content*/
    CTMsgPL_C_WriteFileFrag *fileTransferPtr
            = reinterpret_cast<CTMsgPL_C_WriteFileFrag*>(payload.payloadPtr);

    /* Debug*/
    if(log.isDebugEnabled())
    log.debug(  "%s, (%d bytes): Received %d out of %d bytes at offset %08X to copy.",
                fwrite.fileName.c_str(), fwrite.fileSize,
                payload.payloadLen - sizeof(CTMsgPL_C_WriteFileFrag),
                fileTransferPtr->size,
                fileTransferPtr->offset
                );

    /* Check chunk size*/
    if( fileTransferPtr->size != (payload.payloadLen - sizeof(CTMsgPL_C_WriteFileFrag)) )
    {
        return createAckNack(header,CTH_NACK_INCOMPLETEMSG);
    }

    /* Check writing offset  */
    if((fileTransferPtr->offset + fileTransferPtr->size) > fwrite.fileSize
    		|| lseek(fwrite.tempFile, fileTransferPtr->offset, SEEK_SET) < 0)
    {
        return createAckNack(header,CTH_NACK_OVERFLOW);
    }
    else
    {
        /* Write chunk */
        lu_uint8_t *chunkPtr;
        chunkPtr = (lu_uint8_t *)(fileTransferPtr) + sizeof(CTMsgPL_C_WriteFileFrag);
        if(write(fwrite.tempFile, chunkPtr, fileTransferPtr->size)
        		== (ssize_t)fileTransferPtr->size )
        {
            fsync(fwrite.tempFile);

            /* update states */
            fwrite.rcvSize += fileTransferPtr->size;
            fwrite.rcvParts ++;
        }
        else
        {
            return createAckNack(header,CTH_NACK_WRITE);
        }
    }

    if(fwTimer != NULL)
    {
        fwTimer->reset();
    }
    return createAckNack(header,CTH_ACK);
}


CTHMessage*  FileTransferManager::fileWriteFinishMessage(const CTMessageHeader &header,
                                                         const CTMessagePayload &payload)
{
    /* Check state */
    if (inFileWriting != LU_TRUE)
    {
      /* Transferring is NOT in progress */
      log.warn("File writing not started!");
      return createAckNack(header, CTH_NACK_NOTRANSFER);
    }

    /*Check payload size*/
    if (payload.payloadLen < sizeof(CTMsgPL_C_WriteFinish))
    {
        /*Invalid payload size*/
        log.warn("Invalid payload length. Expected: %i Actual:%i %s",
                                sizeof(CTMsgPL_C_WriteFinish),
                                payload.payloadLen,
                                __AT__);
        return createAckNack(header, CTH_NACK_PAYLOAD);
    }

    /* Decode valid payload */
    CTMsgPL_C_WriteFinish *fileInfoPtr
            = reinterpret_cast<CTMsgPL_C_WriteFinish*>(payload.payloadPtr);
    log.debug( "%s, (%d bytes): Received finish %i order.",
                fwrite.fileName.c_str(),
                fwrite.fileSize,
                fileInfoPtr->cancel
              );

    /* Finish writing */
    CTH_ACKNACKCODE ret = finishFileWriting(fileInfoPtr->cancel);

    // Succeeded, return file info
    if(ret == CTH_ACK)
    {
        CTMsg_R_FileWriteEndInfo* reply = new CTMsg_R_FileWriteEndInfo(header.requestID, replyID.newID());
        CTMsgPL_R_FileWriteEndInfo* fileStatusPtr
                = reinterpret_cast<CTMsgPL_R_FileWriteEndInfo*>(&(*reply)[0]);
        fileStatusPtr->partsCount = fwrite.rcvParts;
        fileStatusPtr->fileSize = fwrite.actualFileSize;
        fileStatusPtr->dataSize = fwrite.rcvSize;
        reply->setEntries(1); /* Set the number of valid entries */

        log.info(
                "Replies with file recap: R=%d, D=%d, #=%d",
                fileStatusPtr->fileSize,
                fileStatusPtr->dataSize,
                fileStatusPtr->partsCount
              );


        return reply;

    }
    // Failed, return nack.
    else{
       return createAckNack(header,ret);
    }
}


CTHMessage* FileTransferManager::fileReadStartMessage(const CTMessageHeader &header,
											 		  const CTMessagePayload &payload)
{
    /* Reading is in progress, cancel the previous one */
    if (inFileReading == LU_TRUE)
    {
        /* Transferring is in progress*/
        log.warn("Cannot start file reading which is already in progress!");
        return createAckNack(header,CTH_NACK_TRANSFER);
    }
    /* Check payload size*/
    if (payload.payloadLen == 0)
    {
        log.warn("Invalid payload length for start file reading. Expected:%i, Actual:%i",
                    sizeof(CTMsgPL_C_FileReadStart),
                    payload.payloadLen
                );
        return createAckNack(header, CTH_NACK_PAYLOAD);
    }

    /*Set initial state */
    initFileReading();


    /* Decode payload content*/
    CTH_ACKNACKCODE ret = getPayloadContent(
                    reinterpret_cast<CTMsgPL_C_FileReadStart *> (payload.payloadPtr),
                    &fread);
    /*Fail to decode payload content*/
    if (ret != CTH_ACK)
    {
        return createAckNack(header, ret);
    }
    if (checkFileExistence(fread.fileName) == LU_FALSE)
    {
        /* File does NOT exist */
        log.warn("File not found:%s",fread.fileName.c_str());
        return fread.type == CTH_TRANSFERTYPE_G3CONFIG?
                        createAckNack(header, CTH_NACK_NOCONFIG)
                      : createAckNack(header, CTH_NACK_FILENAME) ;
    }

    /* Calculate file CRC to be reported in the reply */
    lu_uint32_t fileCRC = 0;
    lu_int32_t calcRes = calcFileCRC(fread.fileName.c_str(), &fileCRC);
    if(calcRes != 0)
    {
        log.warn("Error %i - Unable to calculate CRC of file %s.",
                    calcRes, fread.fileName.c_str());
    }

    /* Compose reply message */
    CTMsg_R_FileReadStart* reply
                = new CTMsg_R_FileReadStart(header.requestID,replyID.newID());
    CTMsgPL_R_FileReadStart *fileInfoPtr
                = reinterpret_cast<CTMsgPL_R_FileReadStart *> (&(*reply)[0]);
    fread.fileSize = getFileSize(fread.fileName);
    fileInfoPtr->fileSize = fread.fileSize;
    fileInfoPtr->crc32 = fileCRC;

    reply->setEntries(1); // Set the number of valid entries

    /* Open file */
    fread.fileDescriptor = open(fread.fileName.c_str(), O_RDONLY);

    /* Go into reading state */
    inFileReading = LU_TRUE;
    log.debug("File read started.");

    /* Start timeout timer */
    delete frTimer;     //stop and remove any previous timer
    frTimer = new Timer(TIMEOUT, 0, this, Timer::TIMER_TYPE_ONESHOT, "FileReadTmr");
    if(frTimer->start() < 0)
    {
        log.error("Unable to create Timer for file reading operation");
    }

    return reply;
}


CTHMessage* FileTransferManager::fileReadFragmentMessage(const CTMessageHeader &header,
                                                         const CTMessagePayload &payload)
{
    /* Check transfer state */
    if(inFileReading != LU_TRUE)
    {
        log.warn("File reading not started!");
        return createAckNack(header,CTH_NACK_NOTRANSFER);
    }

    /*Check payload size*/
    if(payload.payloadLen != 0)
    {
        /* Invalid payload size*/
        log.warn("Invalid payload length:%i %s",payload.payloadLen,__AT__);
        return createAckNack(header,CTH_NACK_PAYLOAD);
    }

    log.debug("Read file fragment at offset 0x%04X.", fread.offset);

    if( fread.fileDescriptor < 0)
    {
        return createAckNack(header,CTH_NACK_OPEN);
    }

    /*Create reply message*/
    CTMsg_R_FileReadFrag* reply = new CTMsg_R_FileReadFrag(header.requestID,replyID.newID());
    CTMsgPL_R_ReadFileFrag *fileDataPtr
                = reinterpret_cast<CTMsgPL_R_ReadFileFrag *>(&(*reply)[0]);

    /*Invalid offset*/
    if(lseek(fread.fileDescriptor, fread.offset, SEEK_SET) < 0)
    {
        fileDataPtr->offset = fread.offset;
        fileDataPtr->size = 0;
    }
    /*Extract file fragment into reply message*/
    else
    {
        /* Default return when failing to open/read file */
        lu_int64_t readSize;   //file data read result (can be -1)
        lu_uint32_t maxSize;    //max file data can be stored on the payload
        lu_uint8_t *dataGetPtr; //pointer to file data stored on the payload

        dataGetPtr = (lu_uint8_t *)(fileDataPtr) + sizeof(CTMsgPL_C_WriteFileFrag);
        maxSize =  reply->getMaxEntries() - sizeof(CTMsgPL_R_ReadFileFrag);
        readSize = read(fread.fileDescriptor, dataGetPtr, maxSize);
        if(readSize >= 0)
        {
            fileDataPtr->offset = fread.offset;
            fileDataPtr->size = readSize;
            fread.offset += readSize;  //set offset for next message
        }
        else
        {
            fileDataPtr->offset = fread.offset;
            fileDataPtr->size = 0;
        }

        log.debug("Replies with file part offset %04X (%d bytes).",
                   fileDataPtr->offset,
                   fileDataPtr->size
                 );

    }

    /* No data has been read, finish reading*/
    if(fileDataPtr->size == 0)
    {
       finishFileReading(LU_FALSE);
    }
    else
    {
        if(frTimer != NULL)
        {
            frTimer->reset();
        }
    }

    /* Set the number of valid entries */
    reply->setEntries(fileDataPtr->size + sizeof(CTMsgPL_R_ReadFileFrag));
    return reply;
}


CTHMessage* FileTransferManager::fileReadCancelMessage(const CTMessageHeader &header,
                                                       const CTMessagePayload &payload)
{
    /* Check transfer state */
    if(inFileReading != LU_TRUE)
    {
        log.warn("File reading not started!");
        return createAckNack(header,CTH_NACK_NOTRANSFER);
    }

    /* Check payload size*/
    if(payload.payloadLen != 0)
    {
        /* Invalid payload size*/
        log.warn("Invalid payload length:%i %s",payload.payloadLen,__AT__);
        return createAckNack(header,CTH_NACK_PAYLOAD);
    }

    log.info("File reading cancelled.");

    finishFileReading(LU_TRUE);

    return createAckNack(header,CTH_ACK);
}


void FileTransferManager::handleAlarmEvent(Timer &source)
{
    if((&source == fwTimer) && (inFileWriting == LU_TRUE))
    {
        log.error("File writing timeout!");
        closeFileWriting(LU_TRUE);
    }


    if((&source == frTimer) && (inFileReading == LU_TRUE))
    {
        log.error("File reading timeout!");
        closeFileReading(LU_TRUE);
    }
}


CTHMessage* FileTransferManager::eraseConfig(const CTMessageHeader &header,
                                             const CTMessagePayload &payload)
{
    if(payload.payloadLen != 0)
    {
        /* Invalid payload */
        log.warn("Invalid payload length:%i %s",payload.payloadLen,__AT__);
        return createAckNack(header,CTH_NACK_PAYLOAD);
    }

    /*Check state*/
    if(inFileWriting == LU_TRUE || inFileReading == LU_TRUE)
    {
        /* Transferring is in progress*/
        log.warn("Cannot erase Configuration file since file transfer is in progress.");
        return createAckNack(header,CTH_NACK_TRANSFER);
    }

    eraseConfigFile();
    log.info("Erased Configuration file.");

    return createAckNack(header,CTH_ACK);
}

CTHMessage* FileTransferManager::eraseCommissioningCache(const CTMessageHeader &header,
                                             const CTMessagePayload &payload)
{
    if(payload.payloadLen != 0)
    {
        /* Invalid payload */
        log.warn("Invalid payload length:%i %s",payload.payloadLen,__AT__);
        return createAckNack(header,CTH_NACK_PAYLOAD);
    }

    /*Check state*/
    if(inFileWriting == LU_TRUE || inFileReading == LU_TRUE)
    {
        /* Transferring is in progress*/
        log.warn("Cannot erase Configuration file since file transfer is in progress.");
        return createAckNack(header,CTH_NACK_TRANSFER);
    }

    unlink(G3_COMMISSIONING_CACHE);
    log.info("Erased Configuration file.");

    return createAckNack(header,CTH_ACK);
}


bool FileTransferManager::updateConfigCRC(const std::string currentCfg, lu_uint32_t* crc)
{
    bool ret = true;
    std::string current;    //Current configuration file
    if(currentCfg.empty())
    {
        current = G3_CONFIGFILE_ZIP;
    }
    else
    {
        current = currentCfg;
    }

    /* Check CRC */
    lu_uint32_t fileCRC = 0;
    lu_int32_t calcRes = calcFileCRC(current.c_str(), &fileCRC);
    if(calcRes != 0)
    {
        log.warn("Error %i - Unable to calculate CRC of file %s.", calcRes, current.c_str());
        ret = false;
    }
    //Notify CRC value given at the transfer start
    updateObservers(&fileCRC);

    //Return crc if provided
    if(crc != NULL)
    {
        *crc = fileCRC;
    }
    return ret;
}


CTH_ACKNACKCODE FileTransferManager::activateConfigFile()
{
    static const std::string incoming = std::string(G3_CONFIGFILE_ZIP) + FILEDEACTIVATED;

    /* Check file exists */
    struct stat fileSysStatus;
    lu_int32_t result = stat(incoming.c_str(), &fileSysStatus);

    if (result != 0)
    {
        /* File not exists */
        return CTH_NACK_NOCONFIG;
    }

    bool failure = true;
    static const std::string current = G3_CONFIGFILE_ZIP;
    static const std::string backup = std::string(G3_CONFIGFILE_ZIP) + FILEBACKUP;

    /* Backup existing file */
    if(backupCfg() != CTH_ACK)
    {
        log.error("Failed to back up current configuration file!");
    }

    /* Activate now the config file */
    unlink(current.c_str()); //remove file
    if(link(incoming.c_str(), current.c_str()) != 0)  //copy over
    {
        log.error("Failed to Activate configuration file! - Trying to restore backup...");
        if(link(backup.c_str(), current.c_str()) != 0)  //copy over
        {
            log.error("Failed to restore backup! no configuration file will be available");
        }
    }
    else
    {
        //link() successful: file copied
        if(updateConfigCRC(current))
        {
            failure = false;
            unlink(incoming.c_str());
            log.info("New configuration file activated successfully");
        }
    }
    return (failure)? CTH_NACK_NOCONFIG : CTH_ACK;
}


CTH_ACKNACKCODE FileTransferManager::restoreConfigFile()
{
    /* Check file exists */
    struct stat fileSysStatus;
    lu_int32_t result;
    static const std::string current = G3_CONFIGFILE_ZIP;
    static const std::string backup = std::string(G3_CONFIGFILE_ZIP) + FILEBACKUP;
    static const std::string temp = std::string(G3_CONFIGFILE_ZIP) + FILEBACKUP + FILETEMPEXT;

    result = stat(backup.c_str(), &fileSysStatus);
    if (result != 0)
    {
        /* No backup available */
        return CTH_NACK_FILENOTEXIST;
    }

    result = stat(current.c_str(), &fileSysStatus);
    if (result != 0)
    {
        /* No current file: put backup as current */
        if(link(backup.c_str(), current.c_str()) == 0)  //copy over
        {
            unlink(backup.c_str()); //remove backup
            return CTH_ACK;
        }
        return CTH_NACK_WRITE;
    }

    /* Swap backup and current file */
    bool failure = true;
    unlink(temp.c_str());
    if(link(backup.c_str(), temp.c_str()) == 0)  //copy over
    {
        unlink(backup.c_str());
        if(link(current.c_str(), backup.c_str()) == 0)  //copy over
        {
            unlink(current.c_str());
            if(link(temp.c_str(), current.c_str()) == 0)  //copy over
            {
                if(updateConfigCRC(current))
                {
                    failure = false; //all successful: file copied
                    log.info("Configuration file restored successfully");
                }
            }
        }
    }
    unlink(temp.c_str());   //Remove temporal swap file
    return (failure)? CTH_NACK_WRITE : CTH_ACK;
}


void eraseConfigFile()
{
    /* Try to backup Config File first */
    backupCfg();
    /* Try to delete current configuration file */
    unlink(G3_CONFIGFILE);
    unlink(G3_CONFIGFILE_ZIP);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void FileTransferManager::initFileWriting(void)
{
    fwrite.fileName.clear();
    fwrite.fileSize = 0;
    fwrite.permissions = 0;
    fwrite.overwrite = LU_FALSE;
    fwrite.type = CTH_TRANSFERTYPE_LAST;

    fwrite.tempFile = -1;
    fwrite.tempFileName.clear();

    fwrite.actualFileSize = 0;
    fwrite.rcvSize = 0;
    fwrite.rcvParts = 0;
}


void FileTransferManager::initFileReading(void)
{
    fread.fileName.clear();
    fread.fileDescriptor = -1;
    fread.offset = 0;
    fread.type = CTH_TRANSFERTYPE_LAST;
}


CTH_ACKNACKCODE FileTransferManager::getPayloadContent(
                                        const CTMsgPL_C_FileWriteStart *input,
                                              FileWriteInfo *output)
{
    /*Validate file size*/
    if(input->fileSize == 0)
        return CTH_NACK_FILENAME; // Invalid file size

    /*Validate transfer type*/
    if(input->transferType >= CTH_TRANSFERTYPE_LAST)
        return CTH_NACK_UNSUPPORTED; // Unsupported transfer type

    /*Decode file name*/
    std::string fname = (char *) (input) + sizeof(CTMsgPL_C_FileWriteStart);
    if(fname.empty())
    {
        return CTH_NACK_FILENAME;
    }


    /*Validation passed, copy content*/
    output->permissions    = input->filePermissions;
    output->fileSize       = input->fileSize;
    output->overwrite      = input->fileCopy ? LU_TRUE : LU_FALSE;
    output->type           = static_cast<CTH_TRANSFERTYPE>(input->transferType);
    output->fileName       = getFilePath(output->type, fname, true);
    output->crc            = input->crc32;

    log.debug( "getPayloadContent[Write]: file %s, (%d bytes) [%d] Type=%d, Copy=%d.",
                fname.c_str(), output->fileSize,
                output->permissions,
                output->type ,
                output->overwrite
              );

    return CTH_ACK;
}


CTH_ACKNACKCODE FileTransferManager::getPayloadContent(
                               const CTMsgPL_C_FileReadStart *input,
                                     FileReadInfo *output)
{

    /*Validate transfer type*/
   if(input->transferType >= CTH_TRANSFERTYPE_LAST)
       return CTH_NACK_UNSUPPORTED; // Unsupported transfer type

   /*Get file name*/
   std::string fname = (char *) (input) + sizeof(CTMsgPL_C_FileReadStart);
   if(fname.empty())
   {
      return CTH_NACK_FILENAME;
   }

   /*Validation passed, copy content*/
   output->type        = static_cast<CTH_TRANSFERTYPE>(input->transferType);
   output->fileName    = getFilePath(output->type, fname);


   log.debug( "getPayloadContent[Read] Read file of type %d called %s",
               output->type,
               output->fileName.c_str()
               );
   return CTH_ACK;
}


CTH_ACKNACKCODE FileTransferManager::finishFileWriting(lu_bool_t cancel)
{
    if(inFileWriting == LU_FALSE)
        return CTH_ACK;

    /*Stop timer*/
    if(fwTimer != NULL)
    {
        delete fwTimer; //stop and remove timer
        fwTimer = NULL;
    }
    return closeFileWriting(cancel);
}


CTH_ACKNACKCODE FileTransferManager::closeFileWriting(const lu_bool_t cancel)
{
    if(inFileWriting == LU_FALSE)
        return CTH_ACK;

    inFileWriting = LU_FALSE;

    if(fwrite.tempFile < 0 || fwrite.tempFileName.empty())
    {
       return CTH_NACK_FILENOTEXIST;
    }

    if (cancel)
    {
        /*Cancelled, discard temp file*/
        close(fwrite.tempFile);
        unlink(fwrite.tempFileName.c_str()); //Delete temp file
        return CTH_ACK_CANCEL;
    }

    else
    {
        /*Write temp file to its destination */
        if (close(fwrite.tempFile) != 0)
        {
            return CTH_NACK_WRITE; //failed to close
        }

        /* Check CRC */
        lu_uint32_t fileCRC = 0;
        lu_int32_t calcRes = calcFileCRC(fwrite.tempFileName.c_str(), &fileCRC);
        if(calcRes != 0)
        {
            log.warn("Error %i - Unable to calculate CRC of file %s.",
                        calcRes, fwrite.fileName.c_str());

            //Notify CRC value given at the transfer start
            if(fwrite.type == CTH_TRANSFERTYPE_G3CONFIG)
            {
                fileCRC = fwrite.crc;
                updateObservers(&fileCRC);
            }
        }
        else
        {
            if(fileCRC != fwrite.crc)
            {
                /* CRC value does not match: discard file! */
                log.warn("CRC mismatch error receiving file %s (%lu != %lu).",
                            fwrite.fileName.c_str(), calcRes, fwrite.crc);
                unlink(fwrite.tempFileName.c_str()); //Delete temp file
                return CTH_NACK_BADCRC;
            }
            else
            {
                //Match: notify CRC value of the new Configuration file
                if(fwrite.type == CTH_TRANSFERTYPE_G3CONFIG)
                {
                    updateObservers(&fileCRC);
                }
            }
        }

        /* Get final file size */
        struct stat fileStat;
        if (stat(fwrite.tempFileName.c_str(), &fileStat) == 0)
        {
            fwrite.actualFileSize = fileStat.st_size;
        }
        else
        {
            return CTH_NACK_WRITE; //stat failed!
        }

        /* Copy temp file over original one */
        unlink(fwrite.fileName.c_str()); //eliminate previous if already exist
        if (link(fwrite.tempFileName.c_str(), fwrite.fileName.c_str()) == 0) //copy over
        {
            unlink(fwrite.tempFileName.c_str()); //try to delete temporal file
            log.info("File writing finished:%s",fwrite.fileName.c_str());
            return CTH_ACK; //link() successful: file copied
        }
        else
        {
            log.warn("Fail to copy temporary file to :%s",fwrite.fileName.c_str());
            return CTH_NACK_WRITE;
        }
    }
}


CTH_ACKNACKCODE FileTransferManager::finishFileReading(const lu_bool_t cancel)
{
    /* Stop timer */
    if(frTimer != NULL)
    {
      delete frTimer;   //stop and remove timer
      frTimer = NULL;
    }
    return closeFileReading(cancel);
}


CTH_ACKNACKCODE FileTransferManager::closeFileReading(const lu_bool_t cancel)
{
    inFileReading = LU_FALSE;

    /* Close file */
    if(fread.fileDescriptor >= 0)
        close(fread.fileDescriptor);
    initFileReading();  //reset file reading
    log.info((cancel == LU_TRUE)? "File reading cancelled." : "File reading finished.");
    return CTH_ACK;
}


const std::string FileTransferManager::getFilePath(const CTH_TRANSFERTYPE type, std::string fname, bool write)
{
    switch(type)
    {
        case CTH_TRANSFERTYPE_G3CONFIG: //G3 Configuration file
        {
            //When writing, it returns the file with a "deactivated" extension
            std::string ret = G3_CONFIGFILE_ZIP;
            if(write)
            {
                ret.append(FILEDEACTIVATED);
            }
            return ret;
        }
        break;

        case CTH_TRANSFERTYPE_MCM:      //G3 MCM files: sw, Configuration Tool...
            return G3_FW_MCM;

        case CTH_TRANSFERTYPE_SLAVE:    //G3 Slave firmware
            return G3_FW_SLAVE;

        case CTH_TRANSFERTYPE_LOG:
            return G3_LOG_FOLDER + fname;

        case CTH_TRANSFERTYPE_AUTOLIB:
            return G3_FW_AUTOLIB_FOLDER + fname;

        case CTH_TRANSFERTYPE_GENERIC:  //Generic file
        default:
            return G3_UPLOAD_FOLDER + fname;
    }

}


CTH_ACKNACKCODE makeDirs(const std::string& filename)
{
    /*Get the written file status*/
    struct stat fileSysStatus;
    lu_int32_t result = stat(filename.c_str(), &fileSysStatus);

    /*File not exists*/
    if (result != 0)
    {
        /* create necessary directories then */
        struct stat dirStatus;
        std::string pathName;
        std::string dirName;
        size_t toChar;
        size_t fromChar;
        size_t endChar;
        endChar = filename.find_last_of("/\\");
        if (endChar >= filename.size())
        {
            //not found a dir!
            return CTH_ACK;
        }

        pathName = filename.substr(0, endChar + 1);
        dirName = "";
        fromChar = 0;

        while (fromChar < endChar)
        {
            toChar = pathName.find_first_of("/\\", fromChar);
            if (toChar >= endChar)
            {
                break; //end loop
            }
            dirName = pathName.substr(0, toChar);

            if (stat(dirName.c_str(), &dirStatus) != 0)
            {
                //not found, create
                if (mkdir(dirName.c_str(), S_IRUSR | S_IWUSR | S_IXUSR
                                | S_IRGRP | S_IXGRP ) < 0)
                {
                    return CTH_NACK_INVALIDPATH;
                }
            }
            else
            {
                /* check if it is a dir */
                if (!S_ISDIR(dirStatus.st_mode))
                {
                    //not a dir! error
                    return CTH_NACK_INVALIDPATH;
                }
            }
            fromChar = toChar + 1; //next
        }
        return CTH_ACK; //done
    }
    return CTH_ACK; //already exists: done
}


static void createDefaultFolders()
{
    std::string syscmd = "mkdir -p ";
    syscmd += G3_FW_FOLDER;
    system(syscmd.c_str());

    syscmd = "mkdir -p ";
    syscmd += G3_UPLOAD_FOLDER;
    system(syscmd.c_str());
}


static CTH_ACKNACKCODE backupCfg()
{
    static const std::string current = G3_CONFIGFILE_ZIP;
    static const std::string backup = std::string(G3_CONFIGFILE_ZIP) + FILEBACKUP;

    /* Check Config File exists first */
    struct stat fileSysStatus;
    lu_int32_t result = stat(current.c_str(), &fileSysStatus);
    if(result == 0)
    {
        //Config file exists: back it up
        unlink(backup.c_str()); //remove backup (either if already exists or not)
        if(link(current.c_str(), backup.c_str()) != 0)  //copy over
        {
            return CTH_NACK_FILENOTEXIST;
        }
    }
    return CTH_ACK;
}


void getFileListStr(const char* directory, std::string& result)
{
    DIR*            dir;
    struct dirent*  entry;
    struct stat     fileStats;
    std::string     filePath;
    std::string     fileName;


   /* Composing file list */
   if ((dir = opendir(directory)) != NULL)
   {
       /* print all the files and directories within directory */
       while ((entry = readdir(dir)) != NULL)
       {
           fileName.clear();
           filePath.clear();

           filePath.append(directory);
           filePath.append(entry->d_name);
           fileName.append(entry->d_name);

           // Get the stats for the file
           if (stat((const char *)(filePath.c_str()), &fileStats) == 0)
           {
               // Do not include directories and hidden files
               if ((fileStats.st_mode & S_IFDIR) == 0
                               && fileName.substr(0,1).compare(".") != 0)
               {
                   result.append(fileName);
                   result.append("\n");
               }
           }
       }
       closedir(dir);
   }
}


} //end namespace CTH


/*
 *********************** End of file ******************************************
 */
