/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Serial link layer for Configuration Tool handler
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   09/07/12      pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(SERIAL_CTH__INCLUDED_)
#define SERIAL_CTH__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Thread.h"
#include "CTHandlerCommon.h"
#include "ICTHLinkLayer.h"
#include "Logger.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Configuration Tool Handler serial link layer.
 *
 * Any reply should be sent in the observer context
 */
class SerialCTHLinkLayer : private Thread, public ICTHLinkLayer
{

public:
    /**
     * \brief Default constructor
     *
     * \param maxPayload Maximum allowed payload
     * \param schedType Scheduler type
     * \param priority Thread priority
     * \param serialPort Serial port to be used
     */
    SerialCTHLinkLayer( lu_uint32_t maxPayload  ,
                          SCHED_TYPE schedType    ,
                          lu_uint32_t priority    ,
                          lu_char_t *serialPort
                        );

    virtual ~SerialCTHLinkLayer();

    /**
     * \brief Start link layer server
     *
     * \return Error code
     */
    virtual CTH_ERROR startServer();

    /**
     * \brief Stop link layer server
     *
     * This function is automatically called by the destructor
     *
     * \return Error code
     */
    virtual CTH_ERROR stopServer(lu_bool_t killThread = LU_TRUE);

    /**
     * \brief Register an observer in the link layer
     *
     *  Only one observer is supported. If an observer is already registered
     *  an error is returned.
     *
     * \param observer Pointer to the observer to register
     *
     * \return Error Code
     */
    virtual CTH_ERROR attach(ICTHLinkLayerObserver *observer);

    /**
     * \brief Deregister an observer in the link layer
     *
     * \param observer Pointer to the observer to deregister
     *
     * \return Error Code
     */
    virtual CTH_ERROR detach(ICTHLinkLayerObserver *observer);

    /**
     * \brief Send some data over the link layer
     *
     * \param message Message to send
     *
     * \return Error Code
     */
    virtual CTH_ERROR send(CTHMessage &message);

protected:
    /**
     * \brief Thread main loop
     *
     * This function is called automatically when a thread is started
     */
    virtual void threadBody();

private:
    Logger& log;
    lu_char_t *serialPort;  //serial device name
    lu_int32_t serialfd;    //serial device (file) descriptor

    ICTHLinkLayerObserver *observer;
};



#endif // !defined(SERIAL_CTH__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
