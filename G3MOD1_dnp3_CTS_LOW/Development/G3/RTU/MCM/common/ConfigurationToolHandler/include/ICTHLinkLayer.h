/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Configuration Tool Handler protocol link layer
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/02/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_7F85C940_420F_4fb4_B997_DF6D54A2CB4D__INCLUDED_)
#define EA_7F85C940_420F_4fb4_B997_DF6D54A2CB4D__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "CTHandlerCommon.h"
#include "CTHMessage.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Configuration Tool Handler Link Layer Observer
 *
 * This class is used by the link layer to notify the upper level layer that
 * new data is available. If the observer has to write some data the data
 * should be sent to the link layer directly from the observer
 */
class ICTHLinkLayerObserver
{

public:
    ICTHLinkLayerObserver() {}

    virtual ~ICTHLinkLayerObserver() {}

    /**
     * \brief Notify the availability of new data
     *
     * \param request Reference to the tnew data available
     *
     * \return none
     */
    virtual void handleRequest(const CTMessageRaw &request) = 0;
};

/**
 * \brief Abstract Link Layer for the Configuration Tool Handler Protocol
 *
 * The observer pattern is used to asynchronously notify the Configuration
 * Tool Handler when new data is available. The Link Layer should run in its
 * own context. The Configuration Tool Handler runs in the link layer context
 */
class ICTHLinkLayer
{

public:
    /**
     * \brief Custom constructor
     *
     * \param maxPayload Maximum payload allowed
     *
     * \return none
     */
    ICTHLinkLayer(lu_uint32_t maxPayload) : maxPayload(maxPayload) {}

    virtual ~ICTHLinkLayer() {}

    /**
     * \brief Start link layer server
     *
     * \return Error code
     */
    virtual CTH_ERROR startServer() = 0;

    /**
     * \brief Stop link layer server
     *
     * This function is automatically called by the destructor
     *
     * \return Error code
     */
    virtual CTH_ERROR stopServer(lu_bool_t killThread = LU_TRUE) = 0;

    /**
     * \brief Register an observer in the link layer
     *
     * \param observer Pointer to the observer to register
     *
     * \return Error Code
     */
    virtual CTH_ERROR attach(ICTHLinkLayerObserver *observer) = 0;

    /**
     * \brief Deregister an observer in the link layer
     *
     * \param observer Pointer to the observer to deregister
     *
     * \return Error Code
     */
    virtual CTH_ERROR detach(ICTHLinkLayerObserver *observer) = 0;

    /**
     * \brief Send some data over the link layer
     *
     * \param message Message to send
     *
     * \return Error Code
     */
    virtual CTH_ERROR send(CTHMessage &message) = 0;

protected:
    lu_uint32_t maxPayload;
};


#endif // !defined(EA_7F85C940_420F_4fb4_B997_DF6D54A2CB4D__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
