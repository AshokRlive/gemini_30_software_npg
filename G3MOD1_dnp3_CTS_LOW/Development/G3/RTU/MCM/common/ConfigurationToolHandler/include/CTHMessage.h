/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Class wrapper for Configuration Tool Protocol
 *       low level messages
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/02/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_F4F63B30_D2CB_448a_A0B9_2680EB6495C8__INCLUDED_)
#define EA_F4F63B30_D2CB_448a_A0B9_2680EB6495C8__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <cstddef>
#include <string.h>
#include <arpa/inet.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "CTHandlerCommon.h"
#include "StringUtil.h"
#include "Table.h"
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define ARRAY_SIZE(array) (sizeof((array))/sizeof((array[0])))

/**
 * Help method to convert structure to a string.
 */
inline static std::string versionToString(CTVersionStr v);
inline static std::string versionToString(CTBasicVersionStr v);
inline static std::string IPToString(lu_int32_t ip);
inline static std::string MACToString(lu_uint8_t mac[6]);

/**
 * Help method to append a pair of value and its name to the given string stream.
 */
inline static void putStr(std::stringstream &ss, const char *name, unsigned char value);
inline static void putStr(std::stringstream &ss, const char *name, char value);
template<typename T>
inline static void putStr(std::stringstream &ss, const char *name, T value);


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/**
 * \brief Virtual class for defining CTH message used by link layer.
 */
class CTHMessage
{

public:
    /**
     * \brief Custom Constructor
     *
     * \param messageType Message Type
     * \param messageID Message ID
     * \param requestID Request ID
     * \param replyID Reply ID
     *
     * \return None
     */
    CTHMessage( lu_uint16_t messageType,
                lu_uint16_t messageID,
                lu_uint16_t requestID,
                lu_uint16_t replyID
              );
    virtual ~CTHMessage();

    /**
     * \brief Provide a RAW stream reference for the message header
     *
     * \param headerStream Buffer where the the header stream reference is saved
     *
     *\return Error Code
     */
    CTH_ERROR getRawHeader(CTMessageRaw &headerStream);

    /**
     * \brief Provide a RAW stream reference for the message payload
     *
     * \param CTMessageRaw Buffer where the the payload stream reference is saved
     *
     *\return Error Code
     */
    virtual CTH_ERROR getRawPayaload(CTMessageRaw &payloadStream) = 0;

    virtual lu_uint32_t getRawPayloadSize() = 0;

    virtual std::string toString() = 0;


protected:
    CTMessageHeader header;
};


/**
 * \brief Template class use to build variable payload length messages
 *
 *                  CT Message Structure
 *------------------------------------------------------
 *| Message Type | Message ID | Request ID | Reply ID  |   Header[8bytes]
 *-----------------------------------------------------------------
 *| Entries Section(Size: EntryTypeSize * EntryNumber) |
 *------------------------------------------------------   Payload
 *| Appended Bytes Section (Size:1 * AppendByteSize)   |
 *------------------------------------------------------
 *
 * \param EntryType Configuration Tool Protocol Message we will use as an entry
 *          of the variable length payalod
 * \param  MsgType Message Type of the message
 * \param  MsgID Message ID of the message
 */
template<lu_uint16_t MsgType, lu_uint16_t MsgID, typename EntryType>
class CTMsg_Template : public CTHMessage
{

public:
    /**
     * \brief Custom Constructor
     *
     * \param requestID Request ID
     * \param replyID Reply ID
     * \param pl RAW buffer to use to store the variable length protocol
     *
     * \return None
     */
    CTMsg_Template( lu_uint16_t requestID,
                        lu_uint16_t replyID
                      ) : CTHMessage(MsgType, MsgID, requestID, replyID),
                      maxEntries(0),
                      entries(0),
                      localBufferSize(CTMsg_MaxPayloadSize)
    {
        localBuffer = new lu_uint8_t[localBufferSize];

        appendBytesSize = 0;

        /* Set the maximum number of entries
         * we can store in the provided buffer
         */
        maxEntries = (localBufferSize*sizeof(lu_uint8_t) / sizeof(EntryType));
    }

    virtual ~CTMsg_Template()
    {
        delete[]localBuffer;
    };

    /**
     * \brief Provide a RAW stream reference for the message payload
     *
     * \param payload Buffer where the the payload stream reference is saved
     *
     *\return Error Code
     */
    virtual CTH_ERROR getRawPayaload(CTMessageRaw &payload)
    {
        /* Save a reference to the payload */
        payload.dataPtr = localBuffer;//this->payload.payloadPtr;

        /* Use the number of entries set and NOT the maximum number of entries
         * to calculate the size of the buffer. We don't want to return the
         * entire RAW buffer but just the portion that contains valid data
         */
        payload.dataLen = getRawPayloadSize();/*entries * sizeof(T);*/

        return CTH_ERROR_NONE;
    }

    virtual lu_uint32_t getRawPayloadSize()
    {
        lu_uint32_t size = entries * sizeof(EntryType) + appendBytesSize;

        if (size > localBufferSize)
            size = localBufferSize;

        return size;
    }

    virtual std::string toString()
    {
        EntryType* entryType = ((reinterpret_cast<EntryType*> (localBuffer)));
        std::stringstream ss;
        ss << header.toString();
        ss <<" [PL size:"<< getRawPayloadSize()<<"]";

        ss << "Entries Num:";
        ss << entries;
        ss << " ";
        for (lu_uint32_t i = 0; i < entries; i++)
        {
            ss << "[";
            ss << i;
            ss << "]";
            ss << entryType->toString();
            entryType++;
        }

        return ss.str();
    }

    /**
     * \brief Get a pointer to the first entry.
     *
     * This effectively gives the buffer to the local buffer to make it easy to
     * fill it out in some cases.
     *
     * \return Pointer to the first entry
     */
    EntryType* firstEntry()
    {
        return (reinterpret_cast<EntryType*>(localBuffer));
    }

    /**
     * \brief Overload [] operator
     *
     * This can used to easily set an entry in the payload. If we try to
     * access an entry outside the registered buffer area the last entry is
     * returned. This is done to prevent any type of buffer overflow
     *
     * \return Reference to the requested entry
     */
    EntryType& operator[](std::size_t n)
    {
        /* If n is greater than the buffer use the last entry */
        if(n >= maxEntries)
        {
            n = maxEntries-1;
        }

        /* Get the position of the requested entry */
        return ((reinterpret_cast<EntryType*>(localBuffer))[n]);
    }

    /* \brief Return the maximum number of entries that can be stored
     * in the registered buffer
     *
     * \return Number of entries
     */
    lu_uint32_t getMaxEntries()
    {
        return maxEntries;
    }

    /**
     * \brief Get the number of valid entries saved in the buffer
     *
     * \return Number of entries
     */
    lu_uint32_t getEntries()
    {
        return entries;
    }

    /**
     * \brief Set the number of valid entries saved in the buffer
     *
     * \param entries Number of valid entries
     *
     * \return Error code
     */
    CTH_ERROR setEntries(lu_uint32_t entries)
    {
        /* Invalid number of entries */
        if(entries > maxEntries)
        {
            return CTH_ERROR_PARAM;
        }

        this->entries = entries;
        return CTH_ERROR_NONE;
    }

    /**
     * Append raw bytes after payload entries.No effect if the buffer overflow.
     *
     * NOTE This method can only be called after the entries have been set
     * to avoid the appended bytes being overwritten by setEntries().
     */
    CTH_ERROR appendBytes(const lu_uint8_t *bytesPtr, const lu_uint32_t size)
    {
        lu_uint32_t curSize = getRawPayloadSize();

        /*Check available space*/
        if(size > localBufferSize - curSize)
        {
            return CTH_ERROR_OVERFLOW;
        }

        memcpy(localBuffer+curSize, bytesPtr, size);

        /*update appendBytesSize*/
        appendBytesSize += size;

        return CTH_ERROR_NONE;
    }

private:

    /** Maximum number of entries the buffer can hold */
    lu_uint32_t maxEntries;
    /** Number of valid entries saved in the buffer */
    lu_uint32_t entries;

    lu_uint8_t* localBuffer;
    const lu_uint32_t localBufferSize;

    lu_uint32_t appendBytesSize; // The size of bytes appended after entries.
};



/* ------------------------ CT Message Payload(PL) Definitions----------------*/
/* Force 1-byte alignment */
#pragma pack(1)

struct CTMsgPL_R_ModuleInfo
{
public:
    lu_uint8_t type;            //module type
    lu_uint8_t id;              //module ID
    lu_uint32_t serial;         //serial number
    CTVersionStr software;      //software version
    CTBasicVersionStr feature;  //feature revision
    CTBasicVersionStr sysAPI;   //system API version
    CTBasicVersionStr bootloaderAPI;    //Bootloader API version
    CTVersionStr bootloaderSw;  //Bootloader software version
    lu_uint8_t moduleStatus;    //internal status (bit field)
    lu_uint8_t moduleErrors;    //severity error codes (bit field)
    /* Status bit field */
    lu_uint8_t ok : 1;          //value 1 when the module is running OK
    lu_uint8_t registered : 1;  //value 1 when is in the registration database
    lu_uint8_t configured : 1;  //value 1 when configured
    lu_uint8_t present : 1;     //value 1 when connected and present on the bus
    lu_uint8_t detected : 1;    //value 1 when the module has ever been detected on the bus
    lu_uint8_t disabled : 1;    //value 1 when module disabled due to tampering
    lu_uint8_t configXML : 1;   //deprecated. value 1 when the module is present in the XML configuration file
    lu_uint8_t unused : 1;
    lu_uint8_t fsm;             //the current state of finite state machine
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss,"type",type);
        putStr(ss,"id",id);
        putStr(ss,"serial",serial);
        putStr(ss,"OK",ok);
        putStr(ss,"FSM",fsm);
        return ss.str();
    }
};

struct CTMsgPL_R_ModuleDetail
{
public:
    lu_uint32_t svnRevisionBoot;//BootLoader SVN revision
    lu_uint32_t svnRevisionApp; //Application SVN revision
    lu_uint32_t architecture;   //Module's system architecture
    lu_uint32_t uptime;         //Current module running time (in seconds, relative to module startup)
    lu_uint8_t reserved[32];    //reserved

public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss,"BL_SVN v.",svnRevisionBoot);
        putStr(ss,"APP_SVN v.",svnRevisionApp);
        putStr(ss,"Arch.",architecture);
        putStr(ss,"Uptime(secs)",uptime);
        return ss.str();
    }
};

struct CTMsgPL_R_ModuleCANStats
{
public:
    /* Validation bit field: specifies valid value for the rest of the fields */
    lu_uint16_t validCANError : 1;
    lu_uint16_t validCANBusError : 1;
    lu_uint16_t validCANArbitrationError : 1;
    lu_uint16_t validCANDataOverrun : 1;
    lu_uint16_t validCANBusOff : 1;
    lu_uint16_t validCANErrorPassive : 1;
    lu_uint16_t validCANTxCount : 1;
    lu_uint16_t validCANRxCount : 1;
    lu_uint16_t validCANRxLost : 1;
    lu_uint16_t unused : 7;
    lu_uint32_t canError;
    lu_uint32_t canBusError;
    lu_uint32_t canArbitrationError;
    lu_uint32_t canDataOverrun;
    lu_uint32_t canBusOff;
    lu_uint32_t canErrorPassive;
    lu_uint32_t canTxCount; // TX Total messages
    lu_uint32_t canRxCount; // RXMessageTotal
    lu_uint32_t canRxLost;  // RXMessageLost
public:
    std::string toString()
    {
        std::stringstream ss;
        putValidValue(ss, "Error",           validCANError,          canError);
        putValidValue(ss, "BusError",        validCANBusError,       canBusError);
        putValidValue(ss, "ArbitError",      validCANArbitrationError, canArbitrationError);
        putValidValue(ss, "DataOverrun",     validCANDataOverrun,    canDataOverrun);
        putValidValue(ss, "BusOff",          validCANBusOff,         canBusOff      );
        putValidValue(ss, "ErrorPassive",    validCANErrorPassive,   canErrorPassive);
        putValidValue(ss, "TxCount",         validCANTxCount,        canTxCount     );
        putValidValue(ss, "RxCount",         validCANRxCount,        canRxCount     );
        putValidValue(ss, "RxLost",          validCANRxLost,         canRxLost      );
        return ss.str();
    }
private:
    template<typename T> static void putValidValue( std::stringstream &ss,
                                                    const char *name,
                                                    const lu_uint16_t valid,
                                                    T value
                                                    )
    {
        if(valid)
            putStr(ss, name, value);
        else
            putStr(ss, name, "--");
    }
};

struct CTMsgPL_R_ModuleAlarms
{
public:
    lu_uint8_t      state;     /* Bit fields decode with SYS_ALARM_STATE */
    lu_uint8_t      subSystem; /* SYS_ALARM_SUBSYSTEM_* */
    lu_uint32_t     alarmCode; /* Subsystem specific alarm codes e.g. SYSALC_SYSTEM_* */
    lu_uint8_t      severity;  /* Bit field of alarm severity: SYS_ALARM_SEVERITY_* */
    lu_uint16_t     parameter; /* additional debug info */
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss,"State",      state);
        putStr(ss,"alarmCode",  alarmCode);
        putStr(ss,"subSystem",  subSystem);
        putStr(ss,"parameter",  parameter);
        return ss.str();
    }
};

struct CTMsgPL_C_RTChannelData
{
public:
    lu_uint8_t      moduleType;
    lu_uint8_t      moduleID;
    lu_uint8_t      channelType;
    lu_uint8_t      channelID;
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss,"moduleType",     moduleType);
        putStr(ss,"moduleID",       moduleID);
        putStr(ss,"channelType",    channelType);
        putStr(ss,"channelID",      channelID);
        return ss.str();
    }
};

struct CTMsgPL_R_RTChannelData
{
public:
    lu_uint8_t      moduleType;
    lu_uint8_t      moduleID;
    lu_uint8_t      channelType;
    lu_uint8_t      channelID;
    lu_uint8_t      valid : 1;      //The value offered is valid.
    lu_uint8_t      online : 1;     //Channel is online.
    lu_uint8_t      alarm : 1;      //The channel is in alarm.
    lu_uint8_t      restart : 1;    //Value is initial and never really updated.
    lu_uint8_t      outRange : 1;   //Overflow/underflow.
    lu_uint8_t      filtered : 1;   //Analogue value has been filtered.
    lu_uint8_t      simulated : 1;  //Value is simulated and replaces the real value.
    lu_uint8_t      unused : 1;
    union
    {
    lu_uint32_t     intvalue;
    lu_float32_t    floatvalue;
    };

public:
    CTMsgPL_R_RTChannelData() :
                                moduleType(0),
                                moduleID(0),
                                channelType(0),
                                channelID(0),
                                valid(0),
                                online(0),
                                alarm(0),
                                restart(1),
                                outRange(0),
                                filtered(0),
                                simulated(0),
                                unused(0)
    {}
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss,"moduleType",     moduleType);
        putStr(ss,"moduleID",       moduleID);
        putStr(ss,"channelType",    channelType);
        putStr(ss,"channelID",      channelID);
        putStr(ss,"intvalue",       intvalue);
        putStr(ss,"valid",          valid);

        return ss.str();
    }
};

struct CTMsgPL_C_PointID
{
public:
    lu_uint16_t group;
    lu_uint16_t ID;
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss,"group",       group);
        putStr(ss,"ID",          ID);
        return ss.str();
    }
};

struct CTMsgPL_R_RTPointData_Digital
{

public:
    CTTimestamp timestamp;
    CTMsgPL_C_PointID point;
    lu_uint8_t valid : 1;
    lu_uint8_t online :1 ;
    lu_uint8_t chatter : 1;
    lu_uint8_t value : 2;
    lu_uint8_t unused : 3;
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss,"point",          point.toString());
        putStr(ss,"valid",          valid);
        putStr(ss,"online",         online);
        putStr(ss,"chatter",        chatter);
        putStr(ss,"value",          value);
        return ss.str();
    }
};

struct CTMsgPL_R_RTPointData_Analog
{

public:
    CTTimestamp timestamp;
    CTMsgPL_C_PointID point;
    lu_float32_t value;
    lu_uint8_t valid : 1;
    lu_uint8_t online : 1;
    lu_uint8_t filterLimit : 1;
    lu_uint8_t overflow : 1;
    lu_uint8_t filterStatus : 3;
    lu_uint8_t unused : 1;
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss,"point",          point.toString());
        putStr(ss,"valid",          valid);
        putStr(ss,"online",         online);
        putStr(ss,"filterLimit",    filterLimit);
        putStr(ss,"value",          value);
        putStr(ss,"filterStatus",   filterStatus);
        putStr(ss,"overflow",       overflow);
        return ss.str();
    }
};

struct CTMsgPL_R_RTPointData_Counter
{
public:
    CTTimestamp timestamp;
    CTMsgPL_C_PointID point;
    lu_uint32_t value;
    lu_uint32_t frozenValue;
    CTTimestamp frozenTimestamp;
    /* Status */
    lu_uint8_t valid : 1;
    lu_uint8_t online : 1;
    lu_uint8_t overflow : 1;
    lu_uint8_t unused : 5;
    /* Frozen Status */
    lu_uint8_t frzValid : 1;
    lu_uint8_t frzOnline : 1;
    lu_uint8_t frzOverflow : 1;
    lu_uint8_t frzUnused : 5;

public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss,"point",          point.toString());
        putStr(ss,"value",          value);
        putStr(ss,"valid",          valid);
        putStr(ss,"online",         online);
        putStr(ss,"overflow",       overflow);
        putStr(ss,"frzValue",       value);
        putStr(ss,"frzValid",       valid);
        putStr(ss,"frzOnline",      online);
        putStr(ss,"frzOvflow",      overflow);
        return ss.str();
    }
};

struct CTMsgPL_C_FileWriteStart
{
public:
    lu_uint32_t fileSize;       //size of file in Bytes (Max 4GBytes)
    lu_uint32_t filePermissions;//Unix int fs_flags filesystem permissions
    lu_uint32_t crc32;          //CRC of file
    lu_uint8_t fileCopy : 1;    //File to be copied flag
    lu_uint8_t unused : 7;
    lu_uint8_t transferType;    //File type (uses CTH_TRANSFERTYPE values).
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss,"fileSize",       fileSize);
        putStr(ss,"filePermissions",filePermissions);
        putStr(ss,"crc32",          crc32);
        putStr(ss,"fileCopy",       fileCopy);
        putStr(ss,"transferType",   transferType);
        return ss.str();
    }
};

struct CTMsgPL_C_WriteFileFrag
{
public:
    lu_uint32_t offset;     //Absolute position on file
    lu_uint32_t size;       //File data fragment size
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss,"offset",   offset);
        putStr(ss,"size",     size);
        return ss.str();
    }
};

typedef CTMsgPL_C_WriteFileFrag CTMsgPL_R_ReadFileFrag;

struct CTMsgPL_C_WriteFinish
{
public:
    lu_uint8_t cancel : 1;  //Cancel operation request
    lu_uint8_t unused : 7;
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss,"cancel",     cancel);
        return ss.str();
    }
};

struct CTMsgPL_R_FileWriteEndInfo
{
public:
    lu_uint32_t fileSize;   //Written File size (in Bytes)
    lu_uint32_t dataSize;   //Actual data received (in Bytes)
    lu_uint32_t partsCount; //Number of parts received
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss,"fileSize",     fileSize);
        putStr(ss,"dataSize",     dataSize);
        putStr(ss,"partsCount",   partsCount);
        return ss.str();
    }
};

struct CTMsgPL_C_FileReadStart
{
public:
    lu_uint8_t transferType;    //File type (uses CTH_TRANSFERTYPE values).
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss,"transferType",   transferType);
        return ss.str();
    }
};

struct CTMsgPL_R_FileReadStart
{
public:
    lu_uint32_t fileSize;   //File size (in Bytes)
    lu_uint32_t crc32;          //CRC of file
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss,"fileSize",   fileSize);
        putStr(ss,"crc32",      crc32);
        return ss.str();
    }
};

struct CTMsgPL_R_DateTime
{
public:
    lu_int32_t tm_sec;  //seconds [0..61]
    lu_int32_t tm_min;  //minutes [0..59]
    lu_int32_t tm_hour; //hours [0..23]
    lu_int32_t tm_mday; //day [1..31]
    lu_int32_t tm_mon;  //month [0..11]
    lu_int32_t tm_year; //years since 1900
    lu_int32_t tm_wday; //days since Sunday [0..6]
    lu_int32_t tm_yday; //days since January 1 [0..365]
    /* date attributes bit field */
    lu_uint8_t isDST : 2; //Daylight Saving Time. Valid values DATE_DST
    lu_uint8_t sync : 1;  //time synchronisation
    lu_uint8_t unused : 5;
    lu_uint8_t syncSource;  //source of the last synchronisation
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss,"tm_sec",     tm_sec);
        putStr(ss,"tm_min",     tm_min);
        putStr(ss,"tm_hour",    tm_hour);
        putStr(ss,"tm_mday",    tm_mday);
        putStr(ss,"tm_mon",     tm_mon);
        putStr(ss,"tm_year",    tm_year);
        putStr(ss,"tm_wday",    tm_wday);
        putStr(ss,"tm_yday",    tm_yday);
        putStr(ss,"isDST",      isDST);
        putStr(ss,"sync",       sync);
        return ss.str();
    }
};

typedef CTMsgPL_R_DateTime CTMsgPL_C_DateTime;

struct CTMsgPL_C_SetServiceMode
{
public:
    lu_int8_t reason; //Reason for putting the RTU Out of Service. Valid values OUTSERVICEREASON
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss,"reason",       reason);
        return ss.str();
    }
};

struct CTMsgPL_R_ServiceStatus
{
public:
    lu_int8_t serviceStatus;    //RTU Service Status. Valid values SERVICEMODE
    lu_int8_t reason;           //Reason for putting the RTU Out of Service. Valid values OUTSERVICEREASON
    lu_int8_t autoRunning   : 1;
    lu_int8_t autoFailed    : 1;
    lu_int8_t autoInhibited : 1;
    lu_int8_t autoDebugging : 1;
    lu_int8_t autoReserved  : 4;
    lu_int8_t reserved;
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss,"serviceStatus",   serviceStatus);
        putStr(ss,"reason",          reason);
        return ss.str();
    }
};

struct CTMsgPL_R_DebugMode
{
public:
    lu_int8_t rtuDebugEnabled;
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss,"debugMode",   rtuDebugEnabled);
        return ss.str();
    }
};

struct CTMsgPL_R_EmptyEntry
{
public:
    std::string toString()
    {
        return std::string("");
    }
};


struct CTMsgPL_R_AliveInfo
{
public:
    lu_int8_t runningApp;   //ID of the App running. Valid values are CTH_RUNNINGAPP type
    lu_uint32_t sysUptime;  //Time in seconds reported by the Operating System to be up and running.
    lu_uint32_t appUptime;  //Time in seconds for the App to be up and running.
    lu_uint8_t doorOpen : 1;//Door open status bit. Value 1 when door is open
    lu_uint8_t unused : 7;
    lu_uint8_t reserved1;   //Reseved for future use
    lu_uint8_t reserved2;   //Reseved for future use
public:
   std::string toString()
    {
        std::stringstream ss;
        putStr(ss, "runningApp",runningApp);
        putStr(ss, "sysUptime",sysUptime);
        putStr(ss, "appUptime",appUptime);
        putStr(ss, "doorStatus",doorOpen);
    return ss.str();
    }
};

struct CTMsgPL_C_UserLogin
{
public:
    static const lu_uint32_t HASH_BUF_SIZE = 128;

public:
    CTMsgPL_C_UserLogin()
    {
        memset(passwHash, 0, HASH_BUF_SIZE);
        passwLen = 0;
        memset(ip, 0, 4);
        memset(mac, 0, 6);
    }
    lu_int8_t passwHash[HASH_BUF_SIZE];
    lu_uint32_t passwLen;       //Password length
    lu_uint8_t ip[4];           //IP address
    lu_uint8_t mac[6];          //MAC address
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss, "passwHash", to_hex_string(passwHash,16));
        putStr(ss, "ip",        to_hex_string(ip,4));
        putStr(ss, "mac",       to_hex_string(mac,6));
        return ss.str();
    }
};

struct CTMsgPL_R_UserInfo
{
public:
    lu_int8_t userLevel;        //User group level. Valid values are USER_LEVEL
    lu_int16_t sessionID;       //Unique session ID
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss, "userLevel", userLevel);
        putStr(ss, "sessionID", sessionID);
        return ss.str();
    }
};

struct CTMsgPL_R_MDAlgorithm
{
public:
    static const lu_uint8_t ALGORITHM_LENGTH = 20;

public:
    lu_char_t mdAlgorithm[ALGORITHM_LENGTH];
    std::string toString()
       {
           std::stringstream ss;
           putStr(ss, "MDAlgorithm", std::string(mdAlgorithm));
           return ss.str();
       }
};

struct CTMsgPL_R_UserList
{
public:
    lu_uint8_t ip[4];           //Connection IP address
    lu_uint8_t mac[6];          //Connection MAC address
    lu_char_t user[20];         //user name
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss, "ip", to_hex_string(ip,4));
        putStr(ss, "mac", to_hex_string(mac,6));
        putStr(ss, "user", std::string(user));
        return ss.str();
    }
};


struct CTMsgPL_R_BatteryCfg
{
public:
    lu_uint8_t type;            //TODO: AP - Define type field in Battery Configuration
    lu_uint8_t memChunk[256];   //Memory chunk with the battery configuration
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss, "type", type);
        putStr(ss, "memChunk", to_hex_string(memChunk,256));
        return ss.str();
    }
};

struct CTMsgPL_R_LogLevels
{
public:
    //selects all subsystems at once to set Log Level
    static const lu_uint8_t SUBSYSTEM_ID_ALL = 0xFF;
public:
    lu_uint8_t subsystem;   //subsystem (SUBSYSTEM_ID_ALL for setting ALL subsystems)
    lu_uint8_t level;       //log level of the subsystem
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss, "subsystem", subsystem);
        putStr(ss, "level",     level);
        return ss.str();
    }
};

typedef CTMsgPL_R_LogLevels CTMsgPL_C_LogLevels;

/**
 * CAUTION: Try to never change it in order to keep compatibility checking
 */
struct CTMsgPL_R_SystemAPI
{
public:
    typedef struct VersionInfoDef
    {
        CTBasicVersionStr systemAPI;
    }VersionInfoStr;
public:
    VersionInfoStr configG3;
    CTBasicVersionStr schema;
    VersionInfoStr MCMModule;
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss, "configG3",    versionToString(configG3.systemAPI));
        putStr(ss, "schema",      versionToString(schema));
        putStr(ss, "MCMModule",   versionToString(MCMModule.systemAPI));
        return ss.str();
    }
};

/**
 * \brief Configuration Tool RTU/MCM Info payload
 */
struct CTMsgPL_R_RTUInfo
{
public:
    static const lu_uint8_t revMCMlength = 200;
    static const lu_uint8_t revKernellength = 200;
    static const lu_uint8_t rootFSlength = 200;
    static const lu_uint8_t cfgTstamplength = 30;
public:
    lu_uint64_t serialCPU;          //CPU serial number
    lu_char_t revisionMCM[revMCMlength];        //MCM firmware revision string
    lu_char_t revisionKernel[revKernellength];  //Kernel revision string
    lu_char_t revisionRootFS[rootFSlength];     //root file system version
    lu_uint8_t lastReset;   //Last system reset cause. Valid values CTH_LASTRESET
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss, "revisionMCM",       std::string(revisionMCM));
        putStr(ss, "revisionKernel",    std::string(revisionKernel));
        putStr(ss, "revisionRootFS",    std::string(revisionRootFS));
        putStr(ss, "serialCPU",         to_hex_string(serialCPU));
        putStr(ss, "lastReset",         lastReset);
        return ss.str();
    }
};

struct CTMsgPL_R_SDPVersion
{
    static const lu_uint8_t SDPVersionlength = 200;

    lu_char_t sdpVersion[SDPVersionlength];

public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss, "sdpVersion",  std::string(sdpVersion));
        return ss.str();
    }
};

/**
 * \brief Configuration Tool Configuration Info payload
 */
struct CTMsgPL_R_ConfigInfo
{
public:
    static const lu_uint8_t cfgTstamplength = 30;
    static const lu_uint8_t cfgTdesclength = 200;
public:
    lu_char_t cfgFileTimestamp[cfgTstamplength]; //Date of last modification of the Config file
    CTBasicVersionStr cfgFileRev;   //Configuration file version in use as set by user
    lu_uint32_t cfgFileCRC;         //CRC32 of the configuration file
    lu_uint8_t cfgFilePresent : 1;  //Configuration File presence
    lu_uint8_t cfgFileCRCValid : 1; //Configuration File CRC validity
    lu_uint8_t unused : 6;
    lu_uint8_t reserved[21];        //reserved
    lu_char_t  cfgFileDesc[cfgTdesclength];        //reserved
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss, "CfgFileTimestamp",  std::string(cfgFileTimestamp));
        putStr(ss, "CfgFileRevMajor",   cfgFileRev.major);
        putStr(ss, "CfgFileRevMinor",   cfgFileRev.minor);
        putStr(ss, "CfgFileCRC",        to_hex_string(cfgFileCRC));
        putStr(ss, "cfgFilePresent",    cfgFilePresent);
        putStr(ss, "cfgFileCRCValid",   cfgFileCRCValid);
        return ss.str();
    }
};

/**
 * \brief Configuration Tool net Info payload
 */
struct CTMsgPL_R_NetInfo
{
public:
    static const lu_uint8_t devNamelength = 100;
public:
    CTconfIP confIP;    //IP address data (IP, net mask, gateway...)
    lu_uint8_t mac[6];  //MAC address configured for ethernet device 0
    lu_uint8_t ethDevice;   //Ethernet device ID; uses ETHERNET_DEVICE enum
    /* Device configuration bit field */
    lu_uint8_t enable : 1;  //this Ethernet device is enabled
    lu_uint8_t dhcp : 1;    //The Ethernet device is configured as DHCP
    lu_uint8_t unused : 6;
    lu_char_t devName[devNamelength];  //MCM firmware revision string
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss,"confIP",     IPToString(confIP.ip));
        putStr(ss,"mask",       IPToString(confIP.mask));
        putStr(ss,"gateway",    IPToString(confIP.gateway));
        putStr(ss,"bcast",      IPToString(confIP.broadcast));
        putStr(ss,"ethDevice",  ethDevice);
        putStr(ss,"enable",     enable);
        putStr(ss,"dhcp",       dhcp);
        putStr(ss,"MAC",        MACToString(mac));
        putStr(ss,"devName",    std::string(devName));
        return ss.str();
    }
};

struct CTMsgPL_C_ModuleID
{
public:
    lu_uint8_t moduleType;  //module type
    lu_uint8_t moduleID;    //module ID
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss,"moduleType", moduleType);
        putStr(ss,"moduleID",   moduleID);
        return ss.str();
    }
};

struct CTMsgPL_C_ModuleRef
{
public:
    lu_uint32_t serial;     //serial number
    lu_uint8_t moduleType;  //module type
    lu_uint8_t moduleID;    //module ID
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss,"serial",     serial);
        putStr(ss,"moduleType", moduleType);
        putStr(ss,"moduleID",   moduleID);
        return ss.str();
    }
};

typedef CTMsgPL_C_ModuleRef CTMsgPL_C_EraseFirmware;
typedef CTMsgPL_C_ModuleRef CTMsgPL_R_EraseFirmware;

/**
 * \brief Configuration Tool Upgrade Status payload
 */
struct CTMsgPL_R_UpgradeState
{
public:
    lu_uint8_t state;   //Upgrade state code. Valid values UPGRADE_STATE
    lu_uint8_t value;   //Associated value: 0..100
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss,"state",   state);
        putStr(ss,"value",   value);
        return ss.str();
    }
};

/**
 * \brief Configuration Tool Off/Local/Remote State payload
 */
struct CTMsgPL_R_OLRState
{
public:
    lu_uint8_t stateOLR; //Off/Local/Remote Status code. Valid values OLR_State
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss,"stateOLR",   stateOLR);
        return ss.str();
    }
};

typedef CTMsgPL_R_OLRState CTMsgPL_C_OLRState;

/**
 * \brief Configuration Tool Switch Test payload
 */
struct CTMsgPL_C_OperateSwitch
{
public:
    lu_uint16_t clogic;         //Control logic block
    lu_uint8_t operation : 1;   //switch operation. Valid values SWITCH_OPERATION
    lu_uint8_t local : 1;       //local or remote operation.
    lu_uint8_t unused : 6;
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss,"clogic",         clogic);
        putStr(ss,"operation",      operation);
        putStr(ss,"local",          local);
        return ss.str();
    }
};

/**
 * \brief Configuration Tool Control Logic Operation payload
 */
struct CTMsgPL_C_OperateCLogic
{
public:
    lu_uint16_t clogic;     //Control logic block
    lu_uint16_t duration;   //duration of test (in seconds or minutes). Set to 0 for default value.
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss,"clogic",          clogic);
        putStr(ss,"duration",        duration);
        return ss.str();
    }
};

struct CTMsgPL_R_AckNack
{
public:
    lu_uint8_t ackNackCode;
public:
    std::string toString()
    {
        std::stringstream ss;
        putStr(ss,"AckNackCode", ackNackCode);
        return ss.str();
    }
};

#pragma pack()


/* ----------------------------- CT Message Definitions-----------------------*/

typedef CTMsg_Template< CTMsgType_PointData,
                        CTMsgID_R_PollPointDigital,
                        CTMsgPL_R_RTPointData_Digital   /*Multiple Entries*/
                      > CTMsg_R_RTPointData_Digital;

typedef CTMsg_Template< CTMsgType_PointData,
                        CTMsgID_R_PollPointAnalogue,
                        CTMsgPL_R_RTPointData_Analog    /*Multiple Entries*/
                      > CTMsg_R_RTPointData_Analog;

typedef CTMsg_Template< CTMsgType_PointData,
                        CTMsgID_R_PollPointCounter,
                        CTMsgPL_R_RTPointData_Counter    /*Multiple Entries*/
                      > CTMsg_R_RTPointData_Counter;

typedef CTMsg_Template< CTMsgType_Module,
                        CTMsgID_R_ModuleInfo,
                        CTMsgPL_R_ModuleInfo
                      > CTMsg_R_ModuleInfo;

typedef CTMsg_Template< CTMsgType_Module,
                        CTMsgID_R_ModuleAlarm,
                        CTMsgPL_R_ModuleAlarms
                      > CTMsg_R_ModuleAlarms;

typedef CTMsg_Template< CTMsgType_Module,
                        CTMsgID_R_ModuleDetail,
                        CTMsgPL_R_ModuleDetail
                      > CTMsg_R_ModuleDetail;

typedef CTMsg_Template< CTMsgType_Module,
                        CTMsgID_R_ModuleCANStats,
                        CTMsgPL_R_ModuleCANStats
                      > CTMsg_R_ModuleCANStats;

typedef CTMsg_Template< CTMsgType_FileWrite,
                        CTMsgID_R_FWriteFinish,
                        CTMsgPL_R_FileWriteEndInfo
                      > CTMsg_R_FileWriteEndInfo;

typedef CTMsg_Template< CTMsgType_FileRead,
                        CTMsgID_R_FReadStart,
                        CTMsgPL_R_FileReadStart
                      > CTMsg_R_FileReadStart;

typedef CTMsg_Template< CTMsgType_FileRead,
                        CTMsgID_R_FReadFragment,
                        CTMsgPL_R_EmptyEntry
                        /*Append fragments byte*/
                      > CTMsg_R_FileReadFrag;

typedef CTMsg_Template< CTMsgType_Date,
                        CTMsgID_R_GetDate,
                        CTMsgPL_R_DateTime
                      > CTMsg_R_DateTime;

typedef CTMsg_Template< CTMsgType_RTUControl,
                        CTMsgID_R_GetServiceMode,
                        CTMsgPL_R_ServiceStatus
                      > CTMsg_R_ServiceStatus;

typedef CTMsg_Template< CTMsgType_RTUControl,
                        CTMsgID_R_GetDebugMode,
                        CTMsgPL_R_DebugMode
                      > CTMsg_R_DebugMode;

typedef CTMsg_Template< CTMsgType_Log,
                        CTMsgID_R_GetLastLog,
                        CTMsgPL_R_EmptyEntry    /*Multiple Entries*/
                      > CTMsg_R_LogList;

typedef CTMsg_Template< CTMsgType_Log,
                        CTMsgID_R_GetLogLevel,
                        CTMsgPL_R_LogLevels     /*Multiple Entries*/
                      > CTMsg_R_LogLevels;

typedef CTMsg_Template< CTMsgType_Log,
                        CTMsgID_R_GetLogFileName,
                        CTMsgPL_R_EmptyEntry
                      > CTMsg_R_LogFileName;

typedef CTMsg_Template< CTMsgType_FileRead,
						CTMsgID_R_FReadFileList,
                        CTMsgPL_R_EmptyEntry
                      > CTMsg_R_FileList;

typedef CTMsg_Template< CTMsgType_Log,
                        CTMsgID_R_GetEventLog,
                        CTMsgPL_R_EmptyEntry    /*Multiple Entries*/
                      > CTMsg_R_EventList;

typedef CTMsg_Template< CTMsgType_RTUInfo,
                        CTMsgID_R_GetSysAPI,
                        CTMsgPL_R_SystemAPI
                      > CTMsg_R_SystemAPI;

typedef CTMsg_Template< CTMsgType_RTUInfo,
                        CTMsgID_R_GetRTUInfo,
                        CTMsgPL_R_RTUInfo
                        /* Appended Site Name*/
                      > CTMsg_R_RTUInfo;

typedef CTMsg_Template< CTMsgType_RTUInfo,
                        CTMsgID_R_GetSDPVersion,
                        CTMsgPL_R_SDPVersion
                      > CTMsg_R_SDPVersion;

#ifdef CONFIGCRC
/* TODO: pueyos_a - Enable Config CRC info */
typedef CTMsg_Template< CTMsgType_RTUInfo,
                        CTMsgID_R_GetConfigInfo,
                        CTMsgPL_R_ConfigInfo
                        /* Appended Site Name*/
                      > CTMsg_R_ConfigInfo;
#endif

typedef CTMsg_Template< CTMsgType_RTUInfo,
                        CTMsgID_R_GetNetInfo,
                        CTMsgPL_R_NetInfo   /*Multiple Entries*/
                      > CTMsg_R_NetInfo;

typedef CTMsg_Template< CTMsgType_RTUInfo,
                        CTMsgID_R_GetBatteryCfg,
                        CTMsgPL_R_BatteryCfg
                      > CTMsg_R_BatteryCfg;

typedef CTMsg_Template< CTMsgType_Upgrade,
                        CTMsgID_R_GetUpgradeState,
                        CTMsgPL_R_UpgradeState
                      > CTMsg_R_UpgradeState;

typedef CTMsg_Template< CTMsgType_Upgrade,
                        CTMsgID_R_EraseFirmware,
                        CTMsgPL_R_EraseFirmware
                      > CTMsg_R_EraseFirmware;

typedef CTMsg_Template< CTMsgType_Testing,
                        CTMsgID_R_GetOLR,
                        CTMsgPL_R_OLRState
                      > CTMsg_R_OLRState;

typedef CTMsg_Template< CTMsgType_Login,
                        CTMsgID_R_CheckAlive,
                        CTMsgPL_R_AliveInfo
                      > CTMsg_R_AliveInfo;

typedef CTMsg_Template< CTMsgType_Login,
                        CTMsgID_R_UserLogin,
                        CTMsgPL_R_UserInfo
                      > CTMsg_R_UserInfo;

typedef CTMsg_Template< CTMsgType_Login,
                        CTMsgID_R_MDAlgorithm,
                        CTMsgPL_R_MDAlgorithm
                      > CTMsg_R_MDAlgorithm;

//typedef CTMsg_Template< CTMsgType_Login,
//                        CTMsgID_R_UsersConnected,
//                        CTMsgPL_R_UserList
//                      > CTMsg_R_UserList;

typedef CTMsg_Template< CTMsgType_Diagnostic,
                        CTMsgID_R_GetChannel,
                        CTMsgPL_R_RTChannelData
                      > CTMsg_R_RTChannelData;

typedef CTMsg_Template< CTMsgType_Diagnostic,
                        CTMsgID_R_GetPointDigital,
                        CTMsgPL_R_RTPointData_Digital   /*Multiple Entries*/
                      > CTMsg_R_RTGetPointData_Digital;

typedef CTMsg_Template< CTMsgType_Diagnostic,
                        CTMsgID_R_GetPointAnalogue,
                        CTMsgPL_R_RTPointData_Analog    /*Multiple Entries*/
                      > CTMsg_R_RTGetPointData_Analog;

typedef CTMsg_Template< CTMsgType_Diagnostic,
                        CTMsgID_R_GetPointCounter,
                        CTMsgPL_R_RTPointData_Counter    /*Multiple Entries*/
                      > CTMsg_R_RTGetPointData_Counter;


typedef Table<CTMsgPL_C_PointID> CTPointListStrTable;


/**
 * Configuration Tool Protocol Ack/Nack message
 */
class CTMsg_R_AackNack : public CTHMessage
{

public:
    CTMsg_R_AackNack( lu_uint16_t messageType,
                       lu_uint16_t requestID,
                       lu_uint16_t replyID,
                       lu_uint8_t ackNackCode
                     ) : CTHMessage(messageType, CTMsgID_R_AckNack, requestID, replyID)
    {
        payload.ackNackCode = ackNackCode;
    }

    ~CTMsg_R_AackNack() {};

    /**
     * \brief Provide a RAW stream reference for the message payload
     *
     * \param payload Buffer where the the payload stream reference is saved
     *
     *\return Error Code
     */
    virtual CTH_ERROR getRawPayaload(CTMessageRaw &messageRaw);

    virtual lu_uint32_t getRawPayloadSize();

    virtual std::string toString();

private:
    CTMsgPL_R_AckNack payload;// The payload of this AckNack message

    static const lu_uint32_t PAYLOAD_SIZE
                = sizeof(CTMsgPL_R_AckNack); // The size of this message's payload
};

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

inline static std::string versionToString(CTVersionStr v)
{
    std::stringstream ss;
    ss << static_cast<lu_uint32_t>(v.version.major) << "."
       << static_cast<lu_uint32_t>(v.version.minor) << "."
       << static_cast<lu_uint32_t>(v.patch);
    return ss.str();
}

inline static std::string versionToString(CTBasicVersionStr v)
{
    std::stringstream ss;
    ss << static_cast<lu_uint32_t>(v.major) << "."
       << static_cast<lu_uint32_t>(v.minor);
    return ss.str();
}

inline static std::string IPToString(lu_int32_t ip)
{
    lu_char_t addrTmp[INET_ADDRSTRLEN]; //return string
    struct in_addr address;
    address.s_addr = ip;
    inet_ntop(AF_INET, &address, addrTmp, INET_ADDRSTRLEN);
    return std::string(addrTmp);
}

inline static std::string MACToString(lu_uint8_t mac[6])
{
    std::stringstream ss;
    for (lu_uint32_t i = 0; i < 6; ++i)
    {
        ss << std::setfill ('0') << std::setw(sizeof(lu_uint8_t)*2) << std::hex << static_cast<lu_uint32_t>(mac[i]);
        if(i<5)
        {
            ss << ":";
        }
    }
    return ss.str();
}


inline static void putStr(std::stringstream &ss, const char *name, unsigned char value)
{
    putStr(ss, name, static_cast<lu_uint32_t>(value));
}

inline static void putStr(std::stringstream &ss, const char *name, char value)
{
    putStr(ss, name, static_cast<lu_uint32_t>(value));
}

template<typename T>
inline static void putStr(std::stringstream &ss, const char *name, T value)
{
        ss << " " << name << ":" << value;
}


#endif // !defined(EA_F4F63B30_D2CB_448a_A0B9_2680EB6495C8__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
