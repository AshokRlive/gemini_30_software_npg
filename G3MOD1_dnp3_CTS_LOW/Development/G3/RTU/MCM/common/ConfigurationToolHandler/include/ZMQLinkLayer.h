/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Http link layer (FastCGI)
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   16/02/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(ZMQLINKLAYER_INCLUDE_)
#define ZMQLINKLAYER_INCLUDE_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "CTHandlerCommon.h"
#include "ICTHLinkLayer.h"
#include "Thread.h"
#include "Logger.h"

#include "zmq.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */


/**
 * \brief Configuration Tool Handler http link layer.
 *
 * The Fast CGI protocol is used to communicate with the Webserver.
 * Any reply should be sent iin the observer context
 */
class ZMQLinkLayer : private Thread, public ICTHLinkLayer
{

public:
    /**
     * \brief Default constructor
     *
     * \param maxPayload Maximum allowed payload
     * \param schedType Scheduler type
     * \param priority Thread priority
     * \param cgiSocketName Fast CGI socket name
     * \param logger logger reference
     */
    ZMQLinkLayer( lu_uint32_t maxPayload  ,
                      SCHED_TYPE schedType    ,
                      lu_uint32_t priority    ,
                      const lu_char_t *cgiSocketName
                    );

    virtual ~ZMQLinkLayer();

    /**
     * \brief Start link layer server
     *
     * \return Error code
     */
    virtual CTH_ERROR startServer();

    /**
     * \brief Stop link layer server
     *
     * This function is automatically called by the destructor
     *
     * \return Error code
     */
    virtual CTH_ERROR stopServer(lu_bool_t killThread = LU_TRUE);

    /**
     * \brief Register an observer in the link layer
     *
     *  Only one observer is supported. If an observer is already registered
     *  an error is returned.
     *
     * \param observer Pointer to the observer to register
     *
     * \return Error Code
     */
    virtual CTH_ERROR attach(ICTHLinkLayerObserver *observer);

    /**
     * \brief Deregister an observer in the link layer
     *
     * \param observer Pointer to the observer to deregister
     *
     * \return Error Code
     */
    virtual CTH_ERROR detach(ICTHLinkLayerObserver *observer);

    /**
     * \brief Send some data over the link layer
     *
     * \param message Message to send
     *
     * \return Error Code
     */
    virtual CTH_ERROR send(CTHMessage &message);


protected:
    /**
     * \brief Thread main loop
     *
     * This function is called automatically when a thread is started
     */
    virtual void threadBody();

private:
    void closeContext();

private:
    void *context;
    void *responder;

    const lu_char_t* cgiSocketName;
    ICTHLinkLayerObserver* observer;
    lu_char_t* streamBuffer;        //Buffer for incoming messages
    Logger& log;
};

#endif // !defined(ZMQLINKLAYER_INCLUDE_)

/*
 *********************** End of file ******************************************
 */
