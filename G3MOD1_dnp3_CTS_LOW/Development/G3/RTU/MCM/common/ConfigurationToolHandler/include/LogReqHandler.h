/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       System & Event log manager for the Configuration Tool Handler.
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   20/09/12      pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#if !defined(_MCM_COMMON_LOG_REQUEST_HANDLER)
#define _MCM_COMMON_LOG_REQUEST_HANDLER

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "CTHandlerCommon.h"
#include "CTHMessage.h"
#include "BufferLogger.h"
#include "Logger.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

namespace CTH
{

/**
 * \brief ConfigTool log handler which decodes messages of type CTMsgType_Log
 *  and replies with the latest logs, log levels, etc.
 *
 *  Note the event log messages are not supported cause UPDATER doesn't manage
 *  any events.
 */
class LogReqHandler
{

public:
    /**
     * \brief Custom constructor
     *
     * \param moduleMgr Reference to the module manager object
     *
     * \return none
     */
	LogReqHandler();
	virtual ~LogReqHandler();

    /**
    * \brief Decode an "Event & Control Log" type message
    *
    * \param header Reference to the message header
    * \param payload Reference to the message payload
    *
    * \return reply message
    */
    CTHMessage* decodeMessageLog(const CTMessageHeader &header,
                                   const CTMessagePayload &payload);


private:
    /**
     * \brief Process Get latest Log entries message.
     *
     * Returns latest log entries available.
     *
     * \param payload Reference to the message payload.
     * \param message Reference to the message to be returned.
     *
     * \return Error code
     */
	CTHMessage* decodeGetLogListMessage(const CTMessageHeader &header,
                                        const CTMessagePayload &payload);

    /**
     * \brief Process Get LogLevel message.
     *
     * Returns list of subsystems and log levels.
     *
     * \param payload Reference to the message payload.
     * \param message Reference to the message to be returned.
     *
     * \return Error code
     */
	CTHMessage* decodeGetLogLevelMessage(const CTMessageHeader &header,
                                        const CTMessagePayload &payload);

    /**
     * \brief Process Set LogLevel message.
     *
     * It checks message and sets the log level for a subsystem.
     *
     * \param payload Reference to the message payload
     *
     * \return Error code
     */
	CTHMessage* decodeSetLogLevelMessage(const CTMessageHeader &header,
	                                    const CTMessagePayload &payload);

	/**
	 * \brief Process GetLogFileName message.
	 */
	CTHMessage* decodeGetLogFileName(const CTMessageHeader &header,
	                                    const CTMessagePayload &payload);


private:
    Logger& log;
    BufferLogger& bufferLogger;

};

}//end namespace CTH


#endif // !defined(_MCM_COMMON_LOG_REQUEST_HANDLER)

/*
 *********************** End of file ******************************************
 */
