/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Abstract Configuration Tool Handler public interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/08/13      wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(_ABSTRACT_CONFIGURATION_TOOL_HANDLER)
#define _ABSTRACT_CONFIGURATION_TOOL_HANDLER

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "CTHMessage.h"
#include "CTHandlerCommon.h"
#include "FileTransferManager.h"
#include "ICTHLinkLayer.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
 
/* The name of file that stores SDP version number*/ 
#define SDP_VERSION_FILENAME "SDPVersion" 

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
namespace CTH
{

/**
 * \brief This class is used to track the reply ID
 *
 * The reply ID is a 16 bit number. the 0xFFFF value is reserved. It's used
 * to identify an invalid reply ID field
 */
class ReplyID
{
public:
    ReplyID() : replyID(0) {};
    ~ReplyID() {};

    /**
     * \brief Get a new "unique" replyID
     *
     * The invalid ID is never returned
     * \return replyID
     */
    inline lu_uint16_t newID()
    {
        lu_uint16_t tmp = replyID;

        /* skip invalid id */
        if(++replyID == CTMessageHeader::INVALID_ID) replyID++;

        return tmp;
    }
private:
    lu_uint16_t replyID;
};

/**
 * \brief Abstract Configuration Tool Handler serves as a common library.
 * It receives and handles requests from link layer,then reply with corresponding
 *  messages defined in G3ConfigProtocol.
 *
 * The basic features(e.g file transferring) in G3ConfigProtocol have been
 * implemented in this class. To support more features, virtual function
 * decodePayload need to be implemented to handle more type of requests.
 *
 * The registered link layer reference is used to write data. An observer is
 * also registered into the link layer to receive notifications when new data
 * is available. If the data is valid a reply is generated in the
 * "observer context". This class doesn't need a thread. It's event driven and
 * runs in the link layer context. The class can be easily expanded to support
 * more than one link layer at the same time
 */
class AbstractCTHandler : public ICTHLinkLayerObserver
{

public:
    /**
     * \brief Custom constructor
     *
     * \param linkLayer Reference to the transport link layer
     */
	AbstractCTHandler(ICTHLinkLayer &linkLayer);
    virtual ~AbstractCTHandler();

    /**
     * \brief Observer method used to handle new request
     *
     * If the payload is valid a reply is generated and sent in the caller
     * context
     *
     * \param request RAW request
     *
     * \return None
     */
    virtual void handleRequest(const CTMessageRaw &request);

protected:
    FileTransferManager fileTransfer;
    ICTHLinkLayer &linkLayer;
    Logger &log;

protected:
    CTHMessage* decodeGetSDPVersion(const CTMessageHeader &header,
                    const CTMessagePayload &payload);

private:
    /**
     * \brief Configuration Tool Communication protocol header decoder
     *
     * The content of header will be deocded and copied to CTMessageHeader
     *  if the request is valid.
     *
     * \param request Reference to the raw message
     * \param header Reference to the message header
     *
     * \return true if decoding succeeded, false otherwise.
     */
    lu_bool_t decodeHeader(const CTMessageRaw &request,
                                CTMessageHeader &header);

    /**
	 * \brief Decode the payload of a raw CT message and return a reply message
	 *
     * \param header Reference to the message header
     * \param request Reference to the message raw
	 *
	 * \return reply message. NULL if the request is not handled.
	 */
    CTHMessage* decodePayload(const CTMessageHeader& header,
                              const CTMessageRaw& request);

    /*
     * \brief Decode the payload of a CTMessage and return a reply message.
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return reply message. NULL if the request is not handled.
     */
    virtual CTHMessage* decodePayload(const CTMessageHeader &header,
                                      const CTMessagePayload &payload)= 0;

};

    extern ReplyID replyID;

    /*
     * \brief Create a ackNack message with a ackNack code.
     */
    CTMsg_R_AackNack* createAckNack(const CTMessageHeader &requestHeader,
                                        const CTH_ACKNACKCODE ackNack);

}
#endif // !defined(_ABSTRACT_CONFIGURATION_TOOL_HANDLER)
/*
 *********************** End of file ******************************************
 */
