/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Configuration Tool Handler Common header file (C)
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11/10/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_2594145F_824A_4ed2_964C_604DA4DDE0EF__INCLUDED_)
#define EA_2594145F_824A_4ed2_964C_604DA4DDE0EF__INCLUDED_


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "versions.h"
#include "Table.h"
#include "G3ConfigProtocol.h"
#include "StringUtil.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/* string messages for use with CTH logs */
#define CTH_ACKMESSAGE "ACK"
#define CTH_NACKMESSAGE "NACK"

/* Default paths and files */
#define G3_CONFIGFILE       "./G3Config.xml"
#define G3_CONFIGFILE_ZIP   "./G3Config.xml.gz"
#define G3_FW_FOLDER        "../update/"
#define G3_FW_MCM           G3_FW_FOLDER"MCM.zip"
#define G3_FW_SLAVE         G3_FW_FOLDER"Slave.fw"
#define G3_FW_AUTOLIB_FOLDER  "./lib/automation/"
#define G3_UPLOAD_FOLDER     "../upload/"
#define G3_LOG_FOLDER          "/usr/local/gemini/log/"
#define G3_COMMISSIONING_CACHE  G3_UPLOAD_FOLDER"commissioning.cache"
/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
/* Force 1-byte alignment */
#pragma pack(1)

typedef BasicVersionDef CTBasicVersionStr;  //make an alias
typedef GenericVersionDef CTVersionStr;     //make an alias

/**
 * \brief Overall IP configuration format
 */
struct CTconfIP
{
    lu_uint32_t ip;
    lu_uint32_t mask;
    lu_uint32_t gateway;
    lu_uint32_t broadcast;
    lu_uint32_t dns1;
    lu_uint32_t dns2;
};

/**
 * \brief Overall timestamp
 */
struct CTTimestamp
{
public:
    lu_uint32_t sec;
    lu_uint32_t nsec;
};

struct CTMessageRaw
{

public:
    lu_uint8_t* dataPtr;
    lu_uint32_t dataLen;

    std::string toHexString()
    {
        return to_hex_string(dataPtr,dataLen);
    }
};

/**
 * \brief Configuration Tool Protocol Message Header
 */
struct CTMessageHeader
{
public:
    lu_uint16_t messageType;
    lu_uint16_t messageID;
    lu_uint16_t requestID;
    lu_uint16_t replyID; //Reply ID is used as session ID in request message
    static const lu_uint16_t INVALID_ID = 0xFFFF;

    std::string toString()
    {
        std::ostringstream ss;
        ss <<" Type:"<< to_hex_string(messageType);
        ss <<" ID:"<< to_hex_string(messageID);
        ss <<" RequestID:"<< to_hex_string(requestID);
        ss <<" ReplyID:"<< to_hex_string(replyID);
       return ss.str();
    };
};

struct CTMessagePayload
{

public:
    lu_uint8_t* payloadPtr;
    lu_uint32_t payloadLen;

};
#pragma pack()


/* TODO: pueyos_a - Define CTH error messages in XML in order to generate description strings and use it in log report */
/**
 * \brief Configuration Tool Handler error codes
 */
enum CTH_ERROR
{
    CTH_ERROR_NONE = 0,
    CTH_ERROR_PARAM,
    CTH_ERROR_CGI,
    CTH_ERROR_THREAD,
    CTH_ERROR_OREGISTERED,
    CTH_ERROR_RUNNING,
    CTH_ERROR_MODULES,
    CTH_ERROR_DB_READ,
    CTH_ERROR_LIST,
    CTH_ERROR_INVALID_MSG,
    CTH_ERROR_NOT_SUPPORTED,
    CTH_ERROR_WRITE,
    CTH_ERROR_OVERFLOW,
    CTH_ERROR_LAST
};


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

#endif // !defined(EA_2594145F_824A_4ed2_964C_604DA4DDE0EF__INCLUDED_)


/*
 *********************** End of file ******************************************
 */
