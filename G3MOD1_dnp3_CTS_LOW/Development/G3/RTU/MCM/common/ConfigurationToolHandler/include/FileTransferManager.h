/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       System Information module public interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25/04/12      pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#if !defined(EA_9C7E3BEF_FD0F_4272_8374_2A8248376CC3__INCLUDED_)
#define EA_9C7E3BEF_FD0F_4272_8374_2A8248376CC3__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <string>
#include <stdio.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "CTHandlerCommon.h"
#include "CTHMessage.h"
#include "Logger.h"
#include "Timer.h"
#include "ObserveableObj.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

namespace CTH
{

/**
 * \brief A class to notify when a new Configuration File's CRC has been calculated.
 */
class FileCRCObserver : public ObserverObj<lu_uint32_t>
{};

/**
 * \brief A CTH component for handling file transfer request from ConfigTool.
 *
 */
class FileTransferManager : private ITimerHandler, public SubjectObj<lu_uint32_t>
{
public:
    FileTransferManager();
    virtual ~FileTransferManager();

    /**
      * \brief Decode a File Transfer type message
      *
      * \param header Reference to the message header
      * \param payload Reference to the message payload
      *
      * \return reply message.
      */
      CTHMessage* decodeFileTransferMessage(const CTMessageHeader &header,
                                          const CTMessagePayload &payload);

      /**
       *  \brief Activate the previously uploaded configuration file.
       *
       *  \return error code.
       */
      CTH_ACKNACKCODE activateConfigFile();

      /**
       *  \brief Restore the cofig file from backup.
       *
       *  It effectively swaps the current config file with the backup.
       *
       *  \return error code.
       */
      CTH_ACKNACKCODE restoreConfigFile();

      /**
       *  \brief Update stored Config file's CRC with the one of the given file.
       *
       *  \param currentCfg Path and name of the configuration file. Use empty for default.
       *  \param crc Where to store the resulting file CRC.
       *
       *  \return True when successful
       */
      bool updateConfigCRC(const std::string currentCfg = "", lu_uint32_t* crc = NULL);

      /**
          * \brief Decode Erase RTU configuration message.
          *
          * \param payload Reference to the message payload
          *
          * \return reply message. NULL if decoding failed.
          */
        CTHMessage* eraseConfig(const CTMessageHeader &header,
                                const CTMessagePayload &payload);

        CTHMessage* eraseCommissioningCache(const CTMessageHeader &header,
                                const CTMessagePayload &payload);

private:
      //Forward declarations
      struct FileReadInfo;
      struct FileWriteInfo;

private:
    /**
     * \brief Decode a File Write Start message using the given file
     * information.
     *
     * It extracts the initial information of the file that is about to be
     * received.
     *
     * \param payload Reference to the message payload
     *
     * \return reply message. NULL if decoding failed.
     */
     CTHMessage* fileWriteStartMessage(const CTMessageHeader &header,
                                      const CTMessagePayload &payload);

    /**
     * \brief Decode a File Write Fragmwent message obtaining a file chunk to
     * add - ifoperation was started.
     *
     * It gets file data and adds it to the temporal file copy as it is
     * received.
     *
     * \param payload Reference to the message payload
     *
      * \return reply message. NULL if decoding failed.
     */
     CTHMessage* fileWriteFragmentMessage(const CTMessageHeader &header,
                                         const CTMessagePayload &payload);

    /**
      * \brief Decode a File Write Finish message to finish the file reading
      * operation.
      *
      * Depending on the given command, it finish the file writing or cancels
      * the file writing process.
      *
      * \param payload Reference to the message payload
      *
      * \return reply message. NULL if decoding failed.
      */
    CTHMessage* fileWriteFinishMessage(const CTMessageHeader &header,
                                    const CTMessagePayload &payload);

    /**
     * \brief Decode a Read File Start message with the given file name.
     *
     * It checks if the given file name exists and start the Read File process.
     *
     * \param payload Reference to the message payload
     * \param message Reference to the message to be returned.
     *
     * \return reply message. NULL if decoding failed.
     */
    CTHMessage* fileReadStartMessage(const CTMessageHeader &header,
                                    const CTMessagePayload &payload);

    /**
      * \brief Decode a File Read Fragment message to give the next chunk of
      * the file - if operation was started.
      *
      * Sends next file chunk available. It will set Size=0 for the final part.
      *
      * \param payload Reference to the message payload
      * \param message Reference to the message to be returned.
      *
      * \return reply message. NULL if decoding failed.
      */
    CTHMessage* fileReadFragmentMessage(const CTMessageHeader &header,
                                        const CTMessagePayload &payload);

    /**
      * \brief Decode a File Read Cancel message to finish the file reading
      * operation.
      *
      * Cancel the file reading operation.
      *
      * \param payload Reference to the message payload
      *
      * \return reply message. NULL if decoding failed.
      */
    CTHMessage* fileReadCancelMessage(const CTMessageHeader &header,
                                      const CTMessagePayload &payload);

    /*
     * \brief Get the full file name based on the given transfer type and file name.
     *
     * \param type Type of transfer
     * \param fname name of the file, if applies to the type
     * \param write Set to true when a file is being written
     *
     * \return Path and filename to be used
     */
    static const std::string getFilePath(const CTH_TRANSFERTYPE type,
                                        const std::string fname,
                                        const bool write = false);

    CTHMessage* decodeGetFileList(const CTMessageHeader &header,
                                        const CTMessagePayload &payload);

    /*
     *\Inherit
     */
    virtual void handleAlarmEvent(Timer &source);

    /**
     * \brief initialise file writing
     */
    void initFileWriting(void);

    /**
     * \brief initialise file reading
     */
    void initFileReading(void);

    /**
     * \brief finish file writing and exit file writing state, cancelling timers.
     *
     * Do nothing if the file writing is not initiated.
     *
     * \param cancel If it is cancelled, the written file will be discarded.
     *
     * \return CTH_ACK if file writing is finished correctly.
     *         CTH_NACK if there is error to finish writing.
     */
    CTH_ACKNACKCODE finishFileWriting(const lu_bool_t cancel);

    /**
     * \brief finish file writing and exit file writing state.
     *
     * Do nothing if the file writing is not initiated.
     *
     * \param cancel If it is cancelled, the written file will be discarded.
     *
     * \return CTH_ACK if file writing is finished correctly.
     *         CTH_NACK if there is error to finish writing.
     */
    CTH_ACKNACKCODE closeFileWriting(const lu_bool_t cancel);

    /**
     * \brief finish file reading and exit file reading state, cancelling timers.
     */
    CTH_ACKNACKCODE finishFileReading(const lu_bool_t cancel);

    /**
     * \brief finish file reading and exit file reading state.
     */
    CTH_ACKNACKCODE closeFileReading(const lu_bool_t cancel);

    /**
     *  \brief Validate an input payload content, if succeeds,
     *  copy it to output and return ACK, otherwise return NACK.
     */
    CTH_ACKNACKCODE getPayloadContent(const CTMsgPL_C_FileWriteStart *input,
                                        FileWriteInfo *output);

    /**
     *  \brief Validate and decode payload.
     */
    CTH_ACKNACKCODE getPayloadContent(const CTMsgPL_C_FileReadStart *input,
                                        FileReadInfo *output);

private:
    /*
     * Data structure for storing file writing session information
     */
    struct FileWriteInfo
    {
        std::string fileName;       // complete path + file name
        lu_uint32_t fileSize;       // the size to be written as expected
        lu_uint32_t permissions;    // Unix fs_flags filesystem permissions (octal)
        lu_bool_t overwrite;        // Indicates if the file must be copied before writing starts
        CTH_TRANSFERTYPE type;      // File type
        lu_uint32_t crc;            // 32-bit CRC of the file

        lu_int32_t tempFile;        // Temporary file descriptor for writing
        std::string tempFileName;   // Temporary file name for writing

        lu_uint32_t actualFileSize; // Final size of the written file (in bytes)
        lu_uint32_t rcvSize;        // Total amount of arrived data (in bytes)
        lu_uint32_t rcvParts;       // Total amount file parts received
    };

    /*
     * Data structure for storing file reading session information
     */
    struct FileReadInfo
    {
        std::string fileName;       //Complete path + file name
        CTH_TRANSFERTYPE type;      //File type
        lu_uint32_t fileSize;

        lu_int32_t fileDescriptor;
        lu_uint32_t offset;     //Current reading position
    };

private:
    lu_bool_t inFileWriting;// Indicates if the file writing is in progress
    lu_bool_t inFileReading;// Indicates if the file reading is in progress

    FileWriteInfo fwrite;   // Writing session information
    FileReadInfo fread;     // Reading session information

    Logger& log;

    Timer* fwTimer;// File writing timeout timer
    Timer* frTimer;// File transferring timeout timer
};


/**
* \brief Create necessary directories for a file path.
*/
CTH_ACKNACKCODE makeDirs(const std::string& filepath);


/**
* \brief Erases the RTU configuration file.
*
* Any (zipped or unzipped) Configuration file is deleted, effectively
* making the MCM to do not have any configuration when it's restarted.
*/
void eraseConfigFile();



}//end namespace CTH


#endif // !defined(EA_9C7E3BEF_FD0F_4272_8374_2A8248376CC3__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
