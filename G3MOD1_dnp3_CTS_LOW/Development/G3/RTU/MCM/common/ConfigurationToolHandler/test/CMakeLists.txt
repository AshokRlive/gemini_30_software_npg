cmake_minimum_required(VERSION 2.8)

# Set ROOT path of G3 (Do NOT remove!!!)   
set(CMAKE_G3_PATH_ROOT "${CMAKE_CURRENT_SOURCE_DIR}/../../../../../")


#---------------------COMPILER & GLOBAL ENVIRONMENT-----------------------------
# Use CodeSourcery GCC as cross-compiler
set(CMAKE_TOOLCHAIN_FILE "${CMAKE_G3_PATH_ROOT}/RTU/MCM/cmake/GCC_TX25_toolchain.cmake")

# Set the name of the project. The project name will be used as the name of
# the final executable 
project (CTHTest)

# Set GCC default flags
include("${CMAKE_G3_PATH_ROOT}/RTU/MCM/cmake/GCC_TX25_app_flags.cmake")

# Import global variables
include("${CMAKE_G3_PATH_ROOT}/RTU/MCM/cmake/global_variables.cmake")

#-------------------------------INCLUDING---------------------------------------
# Set GLOBAL including paths 
set(GLOBAL_INCLUDES
    ${CMAKE_G3_PATH_COMMON_INCLUDE}
	${CMAKE_G3_PATH_MCM_THIRDPARTY}/include/
	${CMAKE_G3_PATH_MCM_COMMONLIB}/BaseReuse/include/
#    ${CMAKE_G3_PATH_MCM_COMMONLIB}/CommandLineInterpreter/include/
    ${CMAKE_G3_PATH_MCM_COMMONLIB}/ConfigurationToolHandler/include/
#    ${CMAKE_G3_PATH_MCM_COMMONLIB}/ModuleProtocol/include/
#    ${CMAKE_G3_PATH_MCM_COMMONLIB}/CANFraming/include/
    ${CMAKE_G3_PATH_MCM_COMMONLIB}/Logger/include/
    )
include_directories(${GLOBAL_INCLUDES})

# Set LOCAL including paths
set(LOCAL_INCLUDES
#	../include/
#	../src/ConfigurationToolHandler/include
#	../src/Console/include
#	../src/ModuleManager/include
	)
include_directories(${LOCAL_INCLUDES})


#-----------------------------BUILD DIRECTORIES---------------------------------
# LIBRARY directories to be built
add_subdirectory(${CMAKE_G3_PATH_MCM_COMMONLIB}/BaseReuse/src BaseReuse)
#add_subdirectory(${CMAKE_G3_PATH_MCM_COMMONLIB}/CommandLineInterpreter/src CommandLineInterpreter)
add_subdirectory(${CMAKE_G3_PATH_MCM_COMMONLIB}/ConfigurationToolHandler/src AbstractCTHandler)
#add_subdirectory(${CMAKE_G3_PATH_MCM_COMMONLIB}/CANFraming/src CANFraming)
add_subdirectory(${CMAKE_G3_PATH_MCM_COMMONLIB}/Logger/src Logger)
add_subdirectory(${CMAKE_G3_PATH_RTU_MCM_LIBRARY}/LogMessage/src/ common/LogMessage/)

# LOCAL directories to be built
#add_subdirectory(../src/ConfigurationToolHandler/src ConfigurationToolHandler)
#add_subdirectory(../src/Console/src Console)
#add_subdirectory(../src/ModuleManager/src ModuleManager)


#------------------------------EXECUTABLE --------------------------------------
# Build executable
#add_executable(${EXE_NAME} HttpCTHLinkLayerTest.cpp)
add_executable(HttpCTHLinkLayerTest.axf HttpCTHLinkLayerTest.cpp)

set (EXE_LIST
     #${EXE_NAME}
     HttpCTHLinkLayerTest.axf
     #...
    )
    
#------------------------------LINKING------------------------------------------
# STANDARD libraries to be linked
foreach(var ${EXE_LIST})
    target_link_libraries (${var}  AbstractCTHandler)
    target_link_libraries (${var}  Logger)
    target_link_libraries (${var}  LogMessage)
    target_link_libraries (${var}  BaseReuse)
    target_link_libraries (${var}  ${fcgi})
    target_link_libraries (${var}  rt       ) # pthread support
endforeach(var ${EXE_LIST})	

#------------------------------OTHER--------------------------------------------

# Generate binary file from executable file
#generate_bin()

# Generate Doxygen documentation
# gen_doxygen("MonitorProtocolHandlerTest" "")

