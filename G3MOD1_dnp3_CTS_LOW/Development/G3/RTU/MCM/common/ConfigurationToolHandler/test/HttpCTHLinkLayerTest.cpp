/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:HttpCTHLinkLayerTest.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24 Sep 2013     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <iostream>
#include <signal.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "HttpCTHLinkLayer.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define CGI_SOCKET_NAME "/var/lib/lighttpd/sockets/g3.comm.socket"

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
using namespace std;

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

void testStop()
{
    HttpCTHLinkLayer link(CTMsg_MaxSize,
                            SCHED_TYPE_FIFO,
                            Thread::PRIO_MIN,
                            CGI_SOCKET_NAME);
    cout <<"start server..."<<endl;
    link.startServer();

    cout <<"stop server..."<<endl;
    link.stopServer();

    cout <<"waiting server termination..."<<endl;
    link.join();

    cout <<"Server stopped"<<endl;

}

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
int main(char* arg[])
{
    {
         sigset_t signalMask;
         sigfillset(&signalMask);
         pthread_sigmask(SIG_BLOCK, &signalMask, NULL);


         /* Unblock some useful signals */
         sigemptyset(&signalMask);
         sigaddset(&signalMask, SIGINT);
         sigaddset(&signalMask, SIGTERM);
         pthread_sigmask(SIG_UNBLOCK, &signalMask, NULL);
     }


    testStop();
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
