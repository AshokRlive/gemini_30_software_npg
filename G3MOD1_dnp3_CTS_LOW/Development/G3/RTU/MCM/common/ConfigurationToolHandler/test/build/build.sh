#!/bin/bash
# Build and copy unstripped axf to export path, for sending to the MCM itself
# parameters are the ones used by make, such as "all" or "clean"
APP="Debug/*.axf"
DEST="/home/opt/exports/${USER}/gemini/application/"

cd Debug
make $1

if [ $? -eq 0 ]
then
    # successful make
    cd ..
    cp -v $APP $DEST
    echo copy done!
fi
