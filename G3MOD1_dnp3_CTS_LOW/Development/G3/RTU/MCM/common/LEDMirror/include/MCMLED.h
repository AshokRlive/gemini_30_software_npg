/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: MCMLED.cpp 09-Aug-2013 12:04:33 pueyos_a $
 *               $HeadURL: \\10.11.0.7\pueyos_a\workspace\app_refactor\G3\RTU\MCM\MCMApp\src\..\..\common\LEDMirror\src\MCMLED.cpp $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MCMLED header module
 *
 *    CURRENT REVISION
 *
 *               $Revision:     $: (Revision of last commit)
 *               $Author: pueyos_a  $: (Author of last commit)
 *       \date   $Date: 09-Aug-2013 12:04:33    $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   09-Aug-2013 12:04:33  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_58ED82D0_9543_43f1_9DFC_50FC3CF50BBF__INCLUDED_)
#define EA_58ED82D0_9543_43f1_9DFC_50FC3CF50BBF__INCLUDED_



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Timer.h"
#include "stdio.h"      //FILE* for GPIO handling
#include "LockingMutex.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define PIN_NUM 2

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

// Forward declarations
class BlinkLEDTimer;

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief LED control and mirroring
 *
 * This class is in charge of keeping the state of a front LED and light it up
 * or not depending of is state. This may include the power saving state.
 *
 * This class is a singleton with common (static) methods acting as embedded
 * factory procedures.
 */
class MCMLED
{
public:
    /**
     * \brief LED entity ID
     *
     * LED_IDs identifies abstract entities that may be composed by several
     * physical LEDs but has a common behaviour.
     */
    typedef enum
    {
        LED_ID_NONE,        //Fake LED - does not control any physical LED
        LED_ID_EXTINGUISH,  //Power LED (only to set to off or back to on)
        LED_ID_CAN,         //Front panel's CAN activity indicator
        LED_ID_DUMMYSW,     //Front panel's Dummy switch status indicator
        LED_ID_AUTO,        //Front panel's Automation status indicator
        LED_ID_LAST
    } LED_ID;

    /**
     * \brief Status LED setting
     */
    typedef enum
    {
        LED_STATUS_OFF,             //Off (all colours)
        LED_STATUS_SOLID_1,         //Solid colour 1
        LED_STATUS_SLOWFLASH_1,     //Slow flashing colour 1
        LED_STATUS_FASTFLASH_1,     //Fast flashing colour 1
        LED_STATUS_SOLID_2,         //Solid colour 2
        LED_STATUS_SLOWFLASH_2,     //Slow flashing colour 2
        LED_STATUS_FASTFLASH_2,     //Fast flashing colour 2
        LED_STATUS_SOLID_1_2,       //Solid both colour 1 & colour 2
        LED_STATUS_SLOWFLASH_1_2,   //Slow flashing both colour 1 & colour 2
        LED_STATUS_FASTFLASH_1_2,   //Fast flashing both colour 1 & colour 2
        LED_STATUS_ALTERNATE,       //Fast colour 1/colour 2 alternating flashing
        LED_STATUS_LAST
    } LED_STATUS;

    /**
     * \brief Default values for LED blink speed setting
     *
     * Values are in ms, and it means time on and time off;
     * as in "200" is 200ms on, 200 ms off
     */
    typedef enum
    {
        LED_DEF_FLASHING_SOLID = 0,     //no flashing
        LED_DEF_FLASHING_SLOW = 1000,   //Slow flashing
        LED_DEF_FLASHING_FAST = 250,    //Fast flashing
        LED_DEF_FLASHING_ALTERNATE = 200,  //Faster flashing
        LED_DEF_FLASHING_LAST
    } LED_DEF_FLASHING;

public:
    /**
      * \brief Factory method: return the appropriate MCM's LED object.
      *
      * \param idLED ID of the desired LED object.
      *
      * \return Pointer to a MCM's LED
      */
    static MCMLED& getMCMLED(const LED_ID idLED);

    /**
     * \brief Set the status of the Power Saving mode for ALL the involved LEDs
     *
     * \param mode Set to LU_TRUE to enable Power Saving mode.
     */
    static void setPowerSaveMode(const lu_bool_t mode);

    /**
     * \brief Configure the flashing periods for this LED
     *
     * \param slowFlash_ms Half-period amount of time (in ms) for slow flashing
     * \param fastFlash_ms Half-period amount of time (in ms) for fast flashing
     * \param alternateFlash_ms Half-period amount of time (in ms) for alternate flashing
     */
    void configure( const lu_uint32_t slowFlash_ms,
                    const lu_uint32_t fastFlash_ms,
                    const lu_uint32_t alternateFlash_ms
                    );

    /**
     * \brief Set the value of a front panel's LED in the mirror
     *
     * Changes take effect in memory, but physical LEDs are not updated except
     * when power saving mode is disabled.
     *
     * \param status Status for the LED to be set
     * \param force Force status setting even if the Status is already this one.
     *
     * \return Result value: 0 for OK, <0 for error
     */
    lu_int32_t setStatus(LED_STATUS status, lu_bool_t force = LU_FALSE);

protected:
    friend class BlinkLEDTimer;

    /**
     * Internal storage of physical LED configuration and status
     */
    struct LEDLight
    {
        lu_uint8_t gpio;    //GPIO that drives the physical LED
        FILE* fileValGPIO;  //Associated GPIO value file
        lu_bool_t value;    //Mirror value for this physical LED
    };

    /**
     * \brief Updates a LED display by writing to the related physical LED
     *
     * \param valueLED LED configuration
     * \param newValue Value to set
     */
    void updateLED(MCMLED::LEDLight& valueLED, lu_bool_t newValue);

public:
    void setLED(lu_int32_t pinId, lu_bool_t newValue)
    {
        if (pinId >= 0 && pinId < PIN_NUM)
            updateLED(lightLED[pinId], newValue);
    }

private:
    /**
     * Internal flash rate index for flash timing
     */
    enum
    {
        FLASH_RATE_SLOW,        //Slow-paced flashing
        FLASH_RATE_FAST,        //Fast-paced flashing
        FLASH_RATE_ALTERNATE,   //Alternate colours flashing rate
        FLASH_RATE_LAST
    } FLASH_RATE;

    LEDLight lightLED[PIN_NUM]; //Store the value and reference of LEDs
    LED_STATUS status;          //status (in mirror) of the LED
    lu_bool_t powerSavingMode;  //power saving mode (LU_TRUE for power saving enabled)
    Mutex accessLED;            //Mutex to avoid LED changing concurrently
    BlinkLEDTimer *blinkLEDTimer;   //Timer in charge of blinking
    lu_uint32_t flashTime_ms[FLASH_RATE_LAST];  //Flash timing

private:
    /**
     * Custom Constructor
     *
     * \param gpio_1 GPIO ID of physical LED for action 1 (as in setting to Red)
     * \param gpio_2 GPIO ID of physical LED for action 2 (as in setting to Green)
     * \param name Name of the LED Handler, used on the related blink thread.
     */
    MCMLED(const lu_uint8_t gpio_1 = 0, const lu_uint8_t gpio_2 = 0);
    /**
     * Copy Constructor
     */
    MCMLED(const MCMLED &mcmLED) { LU_UNUSED(mcmLED); };
    virtual ~MCMLED();

    /**
     * Object initialisation method
     */
    static void init();

    /**
     * \brief Update the output of the LED into the physical LEDs.
     */
    void updateStatus();

    /**
     * \brief Stop flashing (and stop the thread)
     */
    void stopFlashing();

    /**
     * \brief Set Power Mode
     *
     * This function shuts down/awake the controlled LED.
     *
     * \param powerMode Power Saving mode. Set to LU_TRUE for shut down LEDs
     */
    void setPowerMode(const lu_bool_t powerMode);
};





/**
 * \brief LED blinking timer
 *
 * This timer allows to blink a LED in a defined interval pattern and colour.
 * Note that the blinking ignores any Power Saving mode, so in that case the
 * blinking should be stopped.
 * As a Thread cannot be resumed, the object is intended to be destroyed to
 * stop blinking, and create a new one to resume blinking.
 */
class BlinkLEDTimer : private ITimerHandler
{
public:
    /**
     * Custom constructor
     *
     * \param mcmLED Reference to the MCMLED object
     * \param led0 channel_1 Channel reference of the physical LED for action 1 (as in OPEN)
     * \param channel_2 Channel reference of the physical LED for action 2 (as in CLOSE)
     * \param threadName Thread name
     * \param priority Thread priority to use
     */
    BlinkLEDTimer(MCMLED& mcmLED,
                    const MCMLED::LEDLight& led0,
                    const MCMLED::LEDLight& led1
                    );
    ~BlinkLEDTimer();

    /**
     * \brief (Re)start LED blinker thread
     *
     * \param onBlinkTimeMs Amount of time in ms to set the action 1 physical LED on
     * \param offBlinkTimeMs Amount of time in ms to set the action 2 physical LED on
     *
     * \return Result of the operation. LU_TRUE when successful.
     */
    lu_bool_t start(lu_uint32_t onBlinkTimeMs = 0, lu_uint32_t offBlinkTimeMs = 0);

    /**
     * \brief Stop LED blinker thread
     *
     * This stops the blinking and kills the related thread
     */
    void stop();

private:
    MCMLED& mcmLED;
    MCMLED::LEDLight cfgLED0;   //LED configuration (action 1)
    MCMLED::LEDLight cfgLED1;   //LED configuration (action 2)
    lu_bool_t blinkOn;          //blinking status (colour to alternate On/Off)
    lu_uint32_t onBlinkTime;    //Blinking time (in milliseconds) when ON state
    lu_uint32_t offBlinkTime;   //Blinking time (in milliseconds) when OFF state
    Timer* blinkTimer;          //Timer reference for blinking period

private:
    void handleAlarmEvent(Timer &source);

};


#endif // !defined(EA_58ED82D0_9543_43f1_9DFC_50FC3CF50BBF__INCLUDED_)

/*
 *********************** End of file ******************************************
 */

