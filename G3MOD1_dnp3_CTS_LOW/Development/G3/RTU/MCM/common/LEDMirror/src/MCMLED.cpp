/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: MCMLED.cpp 09-Aug-2013 12:04:33 pueyos_a $
 *               $HeadURL: \\10.11.0.7\pueyos_a\workspace\app_refactor\G3\RTU\MCM\MCMApp\src\..\..\common\LEDMirror\src\MCMLED.cpp $
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MCMLED module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 09-Aug-2013 12:04:33	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   09-Aug-2013 12:04:33  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <unistd.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MCMLED.h"
#include "MCMIOMap.h"
#include "Timer.h"
#include "timeOperations.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static MCMLED* mcmLED[MCMLED::LED_ID_LAST]; //list of instances of the handlers
static lu_bool_t initialised = LU_FALSE;    //initialisation status of the factory

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

MCMLED& MCMLED::getMCMLED(const LED_ID idLED)
{
    init();
    if(idLED >= LED_ID_LAST)
    {
        return *(mcmLED[LED_ID_NONE]);  //Return dummy (fake) LED
    }
    return *(mcmLED[idLED]);
}


void MCMLED::setPowerSaveMode(const lu_bool_t mode)
{
    init();

    //Note: Each call to setPowerMode is protected from concurrent access

    /* Note: LED_EXTINGUISH is a special case since it is inverted and really
     * shows the Power Saving mode, so we must not set its power mode but its
     * value.
     */
    mcmLED[LED_ID_EXTINGUISH]->setStatus((mode == LU_TRUE)? MCMLED::LED_STATUS_SOLID_1 :
                                                            MCMLED::LED_STATUS_OFF);
    /* Notify to the rest of LEDs */
    for (lu_uint32_t index = 0; index < LED_ID_LAST; ++index)
    {
        if( (mcmLED[index] != NULL) && (index != LED_ID_EXTINGUISH) )
        {
            mcmLED[index]->setPowerMode(mode);
        }
    }
}

void MCMLED::configure( const lu_uint32_t slowFlash_ms,
                        const lu_uint32_t fastFlash_ms,
                        const lu_uint32_t alternateFlash_ms
                        )
{
    flashTime_ms[0] = slowFlash_ms;
    flashTime_ms[1] = fastFlash_ms;
    flashTime_ms[2] = alternateFlash_ms;
}


lu_int32_t MCMLED::setStatus(LED_STATUS newStatus, lu_bool_t force)
{
    LockingMutex lMutex(accessLED);     //Protect from concurrent access

    lu_bool_t blink = LU_TRUE;  //states if LED should blink
    lu_uint32_t flash[2] = {0, 0};  //flash rate if blinking
    lu_bool_t newValue[2] = {LU_FALSE, LU_FALSE};   //new value to set (solid colours)

    //Update only when changing value
    if( (status == newStatus) && (force == LU_FALSE) )
    {
        return 0;
    }
    status = newStatus; //store new status
    switch(newStatus)
    {
        case LED_STATUS_SOLID_1:         //Solid colour 1
            newValue[0] = LU_TRUE;
            newValue[1] = LU_FALSE;
            blink = LU_FALSE;
            break;
        case LED_STATUS_SOLID_2:         //Solid colour 2
            newValue[0] = LU_FALSE;
            newValue[1] = LU_TRUE;
            blink = LU_FALSE;
            break;
        case LED_STATUS_SOLID_1_2:       //Solid both colour 1 & colour 2
            newValue[0] = LU_TRUE;
            newValue[1] = LU_TRUE;
            blink = LU_FALSE;
            break;

        case LED_STATUS_SLOWFLASH_1:     //Slow flashing colour 1
            flash[0] = flashTime_ms[FLASH_RATE_SLOW];
            break;
        case LED_STATUS_FASTFLASH_1:     //Fast flashing colour 1
            flash[0] = flashTime_ms[FLASH_RATE_FAST];
            break;
        case LED_STATUS_SLOWFLASH_2:     //Slow flashing colour 2
            flash[1] = flashTime_ms[FLASH_RATE_SLOW];
            break;
        case LED_STATUS_FASTFLASH_2:     //Fast flashing colour 2
            flash[1] = flashTime_ms[FLASH_RATE_FAST];
            break;
        case LED_STATUS_SLOWFLASH_1_2:   //Slow flashing both colour 1 & colour 2
            flash[0] = flashTime_ms[FLASH_RATE_SLOW];
            flash[1] = flashTime_ms[FLASH_RATE_SLOW];
            break;
        case LED_STATUS_FASTFLASH_1_2:   //Fast flashing both colour 1 & colour 2
            flash[0] = flashTime_ms[FLASH_RATE_FAST];
            flash[1] = flashTime_ms[FLASH_RATE_FAST];
            break;
        case LED_STATUS_ALTERNATE:       //Fast colour 1/colour 2 alternating flashing
            flash[0] = flashTime_ms[FLASH_RATE_ALTERNATE];
            flash[1] = flashTime_ms[FLASH_RATE_ALTERNATE];
            break;
        case LED_STATUS_OFF:
        default:
            newValue[0] = LU_FALSE;
            newValue[1] = LU_FALSE;
            blink = LU_FALSE;
    }


    stopFlashing();   //if already blinking, stop thread in any case
    if(blink == LU_TRUE)
    {
        //create blink timer
        blinkLEDTimer = new BlinkLEDTimer( *this, lightLED[0], lightLED[1]);
        if(blinkLEDTimer == NULL)
        {
            return -1;  //memory error, nothing to do!
        }
        if(powerSavingMode == LU_FALSE)
        {
            //Start blinking now
            blinkLEDTimer->start(flash[0], flash[1]);
        }
    }
    else
    {
        //not blinking

        /* update to new value in memory */
        lightLED[0].value = newValue[0];
        lightLED[1].value = newValue[1];

        /* Set the value to the physical LED */
        updateStatus();
    }
    return 0;
}




BlinkLEDTimer::BlinkLEDTimer(MCMLED& mcmLEDRef,
                            const MCMLED::LEDLight& led0,
                            const MCMLED::LEDLight& led1) :
                mcmLED(mcmLEDRef),
                cfgLED0(led0),
                cfgLED1(led1),
                blinkOn(LU_TRUE),
                onBlinkTime(0),
                offBlinkTime(0),
                blinkTimer(NULL)
{
}

BlinkLEDTimer::~BlinkLEDTimer()
{
    this->stop();     //stop blink and switch off LEDs
    delete blinkTimer;
    blinkTimer = NULL;
}

lu_bool_t BlinkLEDTimer::start(lu_uint32_t onBlinkTimeMs, lu_uint32_t offBlinkTimeMs)
{
    onBlinkTime = onBlinkTimeMs;
    offBlinkTime = offBlinkTimeMs;

    lu_uint32_t blinkTime = (onBlinkTime != 0)? onBlinkTime : offBlinkTime;
    struct timeval flashTime;       //initial on time
    flashTime = ms_to_timeval(blinkTime);
    if(blinkTimer == NULL)
    {
        //Create timer
        blinkTimer = new Timer(flashTime.tv_sec, flashTime.tv_usec,
                                this, Timer::TIMER_TYPE_ONESHOT, "LEDBlinkTmr");
    }
    if(blinkTimer == NULL)
    {
        return LU_FALSE;    //failed to create timer
    }
    blinkTimer->stop(); //stop the timer before starting just in case
    if(blinkTimer->start() < 0)
    {
        return LU_FALSE;    //failed to create timer
    }
    return LU_TRUE;
}

void BlinkLEDTimer::stop()
{
    if(blinkTimer != NULL)
    {
        //stop the timer
        blinkTimer->stop();
    }
    //shut off both LEDs
    mcmLED.updateLED(cfgLED0, LU_FALSE);
    mcmLED.updateLED(cfgLED1, LU_FALSE);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void MCMLED::updateLED(MCMLED::LEDLight& valueLED, lu_bool_t newValue)
{
    if( (valueLED.gpio != 0) && (valueLED.fileValGPIO != NULL) )
    {
        writeGPIO(valueLED.fileValGPIO, newValue);
    }
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

MCMLED::MCMLED(const lu_uint8_t gpio0, const lu_uint8_t gpio1) :
                    status(LED_STATUS_OFF),
                    powerSavingMode(LU_FALSE),
                    blinkLEDTimer(NULL)    //No blinking enabled at first
{
    flashTime_ms[0] = LED_DEF_FLASHING_SLOW;
    flashTime_ms[1] = LED_DEF_FLASHING_FAST;
    flashTime_ms[2] = LED_DEF_FLASHING_ALTERNATE;
    lightLED[0].gpio = gpio0;
    lightLED[1].gpio = gpio1;
    lightLED[0].fileValGPIO = NULL;
    lightLED[1].fileValGPIO = NULL;
    status = LED_STATUS_OFF;
    powerSavingMode = LU_FALSE;

    if(lightLED[0].gpio != 0)
    {
        lightLED[0].gpio = (initGPIO(lightLED[0].gpio,
                                    GPIOLIB_DIRECTION_OUTPUT,
                                    &(lightLED[0].fileValGPIO)) == 0)?
                            lightLED[0].gpio :
                            0;
    }
    if(lightLED[1].gpio != 0)
    {
        lightLED[1].gpio = (initGPIO(lightLED[1].gpio,
                                    GPIOLIB_DIRECTION_OUTPUT,
                                    &(lightLED[1].fileValGPIO)) == 0)?
                            lightLED[1].gpio :
                            0;
    }
    setStatus(LED_STATUS_OFF, LU_TRUE); //force setting of OFF status
    setPowerMode(LU_FALSE);             //Not in power saving mode
}


MCMLED::~MCMLED()
{
    stopFlashing();     //stop and kill blinking timer if any
}


void MCMLED::init()
{
    if(initialised == LU_TRUE)
        return;

    //create instances of objects
    mcmLED[LED_ID_NONE] =         new MCMLED();
    mcmLED[LED_ID_EXTINGUISH] =   new MCMLED(MCMOutputGPIO_LED_EXTINGUISH,
                                            0
                                            );
    mcmLED[LED_ID_CAN] =          new MCMLED(MCMOutputGPIO_LED_CAN_RED,
                                            MCMOutputGPIO_LED_CAN_GREEN
                                            );
    mcmLED[LED_ID_DUMMYSW] =      new MCMLED(MCMOutputGPIO_LED_DUMMYSW_RED,
                                            MCMOutputGPIO_LED_DUMMYSW_GREEN
                                            );

    mcmLED[LED_ID_AUTO]   =      new MCMLED(MCMOutputGPIO_LED_AUTO_RED,
                                            MCMOutputGPIO_LED_AUTO_GREEN
                                            );

    initialised = LU_TRUE;
}


void MCMLED::updateStatus()
{
    for (lu_uint32_t index = 0; index < 2; ++index)
    {
        updateLED(lightLED[index], (powerSavingMode == LU_FALSE)?
                                            lightLED[index].value :
                                            LU_FALSE
                );
    }
}


void MCMLED::stopFlashing()
{
    delete blinkLEDTimer;
    blinkLEDTimer = NULL;
}


void MCMLED::setPowerMode(const lu_bool_t powerMode)
{
    LockingMutex lMutex(accessLED);     //Protect from concurrent access

    //Report Event only when changing state
    if(powerMode != powerSavingMode)
    {
        powerSavingMode = powerMode;
        if(powerMode == LU_TRUE)
        {
            /* shut down LED */
            stopFlashing();
            for (lu_uint32_t index = 0; index < 2; ++index)
            {
                updateLED(lightLED[index], LU_FALSE);   //force LED value to OFF
            }
        }
        else
        {
            /* awake LED */
            //copy status from mirror to LEDs
            setStatus(status, LU_TRUE);  //force setting of this status
        }
    }
}


void BlinkLEDTimer::handleAlarmEvent(Timer &source)
{
    lu_uint32_t blinkTime;          //Time of blinking in ms

    if(onBlinkTime != 0)
    {
        mcmLED.updateLED(cfgLED0, blinkOn);
    }
    if(offBlinkTime != 0)
    {
        mcmLED.updateLED(cfgLED1, !blinkOn);
    }
    blinkTime = (blinkOn == LU_TRUE)?
                    ( (onBlinkTime != 0)? onBlinkTime : offBlinkTime ) :
                    ( (offBlinkTime != 0)? offBlinkTime : onBlinkTime );
    blinkOn = !blinkOn; //invert output for next LED state change

    /* Wait again for time out */
    source.start(ms_to_timespec(blinkTime));
}




/*
 *********************** End of file ******************************************
 */
