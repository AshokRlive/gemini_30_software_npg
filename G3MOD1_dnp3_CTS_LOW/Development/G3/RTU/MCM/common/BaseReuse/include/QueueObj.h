/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: QueueObj.h 1466 2012-06-20 20:42:54Z fryers_j $
 *               $HeadURL: http://svn-general-svc.wlucynet.com:18080/svn/gemini_30_software/branches/NPG/G3MOD1_dnp3_CTS_LOW/Development/G3/RTU/MCM/common/BaseReuse/include/QueueObj.h $
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_D9D18F24_EF14_4d3f_A2FD_D98B19444048__INCLUDED_)
#define EA_D9D18F24_EF14_4d3f_A2FD_D98B19444048__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "QueueMgr.h"
#include "ListMgr.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * QueueMgr must be defined as a friend
 */
template<class queueType> class QueueObj
{

public:
    QueueObj() : nextInstance(NULL) {};

    virtual ~QueueObj() {};

private:
    queueType* nextInstance;

    /**
     * /brief Set a reference to the next object in the queue
     *
     * \param Pointer to the next object
     */
    void setReference(queueType* referencePtr) {nextInstance = referencePtr;};

    /**
     * \brief Return a reference to the next object
     *
     * \return Pointer to the next object. NULL if there are no more objects.
     */
    queueType* getReference(void) {return nextInstance;};

    friend class QueueMgr<queueType>;
    friend class ListMgr<queueType>;
};


#endif // !defined(EA_D9D18F24_EF14_4d3f_A2FD_D98B19444048__INCLUDED_)

/*
 *********************** End of file ******************************************
 */





