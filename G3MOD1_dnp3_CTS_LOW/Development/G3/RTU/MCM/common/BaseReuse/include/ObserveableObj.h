/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:ObserveableObj.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   22 Aug 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef OBSERVEABLEOBJ_H_
#define OBSERVEABLEOBJ_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <vector>
#include <algorithm>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "LockingMutex.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Class to provide a generic Subject role in Observer Pattern setup.
 *
 * This class provides basic Subject capabilities for attaching and detaching
 * observers.
 *
 * The object type provided should be the type of observers accepted.
 */
template <class Observer>
class ObserveableObj
{
public:
    typedef std::vector<Observer*> ObserverList;

public:
    ObserveableObj(){}
    virtual ~ObserveableObj(){}

    virtual bool attach(Observer* observer)
    {
        MasterLockingMutex mMutex(vectMutex, LU_TRUE);

        if(observer != NULL && containObserver(observer) == false)
        {
            obsVect.push_back(observer);
            return true;
        }
        return false;
    }


    virtual void detach(Observer* observer)
    {
        MasterLockingMutex mMutex(vectMutex, LU_TRUE);

        if(observer != NULL)
        {
            obsVect.erase(std::find(obsVect.begin(),obsVect.end(),observer), obsVect.end());
        }
    }


    void detachAll()
    {
        obsVect.clear();
    }

    virtual ObserverList getAllObservers()
    {
        MasterLockingMutex mMutex(vectMutex);
        return obsVect;
    }

private:
    bool containObserver(Observer *observer)
    {
        MasterLockingMutex mMutex(vectMutex);
        return (std::find(obsVect.begin(),obsVect.end(),observer)!= obsVect.end());
    }

private:
    ObserverList  obsVect;
    MasterMutex     vectMutex;   //Mutex to protect Observer vector
};



/*
 * \brief Class to provide Observer role in Observer Pattern setup.
 *
 * This is intended as a Decorator Pattern class that supplies other classes
 * with Observer-related functionality in the Observer Pattern.
 *
 * Usage example:
 *  1) For 1 parameter (for example, lu_uint8_t):
 *      1.1) Create a Class that extends this one, implementing observerUpdate method:
 *          class MyObserver : public ObserverObj<lu_uint8_t>
 *          {
 *              void observerUpdate(lu_uint8_t* p)
 *              {
 *                  do(whatever) with (p)
 *              }
 *          };
 *
 *  2) For >1 parameter:
 *      2.1) Define a structure that contains all the parameters:
 *          struct NotifParams
 *          {
 *              lu_int8_t x;
 *              lu_uint32_t y;
 *          };
 *      2.2) Create a Class that extends this one, implementing observerUpdate method:
 *          class MyObserver : public ObserverObj<NotifParams>
 *          {
 *              void observerUpdate(NotifParams* p)
 *              {
 *                  do(whatever) with (p.x or p.y)
 *              }
 *          };
 *
 *  3) Ensure that the observer is attached to the subject:
 *      MyObserver obs;
 *      subject.attach(&obs);
 */
template <class Params>
class ObserverObj
{
public:
    ObserverObj() {};
    virtual ~ObserverObj() {};
    /* \brief Subject notification reception
     *
     * Method to be implemented in the children object.
     */
    virtual void observerUpdate(Params* params = NULL) = 0;
};

/* \brief ObserverObj template specialization for notifications without parameters
 *
 * Example:
 *      class MyObserver : public ObserverObj<void>
 *      {
 *          void observerUpdate()
 *          {
 *              do(whatever)
 *          }
 *      };
 */
template <>
class ObserverObj<void>
{
public:
    ObserverObj() {};
    virtual ~ObserverObj() {};
    /* \brief Subject notification reception
     *
     * Method to be implemented in the children object.
     */
    virtual void observerUpdate() = 0;
};

/**
 * \brief Class to provide Subject role in Observer Pattern setup.
 *
 * This is intended as a Decorator Pattern class that supplies other classes
 * with Subject-related Observer Pattern capabilities.
 *
 * Usage example:
 *      1) Create an appropiate Observer object, with intended parameters
 *          class MyObserver : public <MyObserver_Params>
 *          {};
 *      2) Create a subject class that inherits from this template:
 *          class MySubject : public SubjectObj<MyObserver>
 *          {};
 *      3) Ensure updateObservers() is called with the proper parameters:
 *          MyObserver_Params p;
 *          p.param1 = value;
 *          updateObservers(&p);    //Note that we pass a pointer to the params
 */
template <class Params>
class SubjectObj : public ObserveableObj< ObserverObj<Params> >
{
private:
    typedef ObserverObj<Params> ObserverType;

public:
    SubjectObj() {};
    virtual ~SubjectObj() {};

    /**
     * \brief Update all the observers with the given parameters
     */
    virtual void updateObservers(Params* params = NULL)
    {
        //Update observers using a copy of the observer list
        typename ObserveableObj<ObserverType>::ObserverList obsList = ObserveableObj<ObserverType>::getAllObservers();
        for(typename ObserveableObj<ObserverType>::ObserverList::iterator it = obsList.begin(); it != obsList.end(); ++it)
        {
            (*it)->observerUpdate(params);
        }
    }

protected:
    /**
     * \brief Attach an observer and immediately update it with given params
     *
     * This is an optional method intended for Subject implementations that wants
     * to update as soon as the observer is attached. It requires to override
     * the original attach() method.
     *
     * Usage example:
     *      bool attach(Observer* observer)             //Overrides attach method
     *      {
     *          Param params = this->getValue();        //Value to be used for update
     *          return attachUpdate(observer, &params); //Attach + update
     *      }
     */
    virtual bool attachUpdate(ObserverType* observer, Params* params = NULL)
    {
        bool ret = ObserveableObj<ObserverType>::attach(observer);
        if(ret)
        {
            observer->observerUpdate(params);
        }
        return ret;
    }
};

/* \brief SubjectObj template specialization for notifications without parameters
 *
 * Usage example:
 *      class MySubject : public SubjectObj<void>
 */
template <>
class SubjectObj<void> : public ObserveableObj< ObserverObj<void> >
{
private:
    typedef ObserverObj<void> ObserverType;

public:
    SubjectObj() {};
    virtual ~SubjectObj() {};

    /**
     * \brief Update all the observers with the given parameters
     */
    virtual void updateObservers()
    {
        //Update observers using a copy of the observer list
        typename ObserveableObj<ObserverType>::ObserverList obsList = ObserveableObj<ObserverType>::getAllObservers();
        for(typename ObserveableObj<ObserverType>::ObserverList::iterator it = obsList.begin(); it != obsList.end(); ++it)
        {
            (*it)->observerUpdate();
        }
    };

protected:
    /**
     * \brief Attach an observer and immediately update it with given params
     *
     * This is an optional method intended for Subject implementations that wants
     * to update as soon as the observer is attached. It requires to override
     * the original attach() method.
     *
     * Usage example:
     *      bool attach(Observer* observer)         //Overrides attach method
     *      {
     *          return attachUpdate(observer);      //Attach + update
     *      }
     */
    virtual bool attachUpdate(ObserverType* observer)
    {
        bool ret = ObserveableObj<ObserverType>::attach(observer);
        if(ret)
        {
            observer->observerUpdate();
        }
        return ret;
    }
};


#endif /* OBSERVEABLEOBJ_H_ */

/*
 *********************** End of file ******************************************
 */
