/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:Index.hpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   7 Nov 2017     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef INDEX_HPP_
#define INDEX_HPP_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <map>
#include <stdexcept>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "LockingMutex.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Helper class for creating Associative Arrays or similar
 */
template <class UID, class Position>
class Index
{
private:
    typedef std::map<UID, Position> Indexer;

public:
    Index() {};
    virtual ~Index() {};

    virtual void add(const UID& id, const Position& position)
    {
        LockingMutex lMutex(locker);
        m_indexer.insert(std::pair<UID,Position>(id, position));
    };

    virtual Position get(const UID& id) throw (std::exception)
    {
        LockingMutex lMutex(locker);
        typename Indexer::const_iterator it = m_indexer.find(id);
        if(it == m_indexer.end())
        {
            throw std::out_of_range("Not found");   //Not found
        }
        return (it->second);    //Found
    };

    virtual void remove(const UID& id)
    {
        LockingMutex lMutex(locker);
        m_indexer.erase(id);
    };

    virtual void clear()
    {
        LockingMutex lMutex(locker);
        m_indexer.clear();
    };

    virtual std::string toString()
    {
        std::ostringstream ss;
        LockingMutex lMutex(locker);
        for(typename Indexer::const_iterator it=m_indexer.begin(); it != m_indexer.end(); ++it)
        {
            ss << "[" << (*it).first << "]=" << (*it).second << ";";
        }
        return ss.str();
    }

private:
    Indexer m_indexer;  //Index map
    Mutex locker;       //Protects concurrent access to functionality
};


#endif /* INDEX_HPP_ */

/*
 *********************** End of file ******************************************
 */
