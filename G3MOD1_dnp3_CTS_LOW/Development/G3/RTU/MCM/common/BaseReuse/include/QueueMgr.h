/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: QueueMgr.h 4157 2013-11-20 12:59:22Z pueyos_a $
 *               $HeadURL: http://svn-general-svc.wlucynet.com:18080/svn/gemini_30_software/branches/NPG/G3MOD1_dnp3_CTS_LOW/Development/G3/RTU/MCM/common/BaseReuse/include/QueueMgr.h $
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_129B4E18_8D89_4a01_B7B2_C4E64A6647FA__INCLUDED_)
#define EA_129B4E18_8D89_4a01_B7B2_C4E64A6647FA__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <errno.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Mutex.h"
#include "LockingMutex.h"
#include "Semaphore.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

enum QUEUE_ERROR
{
    QUEUE_ERROR_NONE = 0,
    QUEUE_ERROR_PARAM   ,
    QUEUE_ERROR_TIMEOUT ,
    QUEUE_ERROR_SEMA    ,

    QUEUE_ERROR_LAST
};

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */



template<class queueType> class QueueMgr
{

public:
    QueueMgr() : queueMutex(LU_TRUE),
                 queueHead(NULL)    ,
                 queueTail(NULL)
    {
        return;
    };

    virtual ~QueueMgr() {};

    /**
     * \brief Add an object to the tail of the queue (FIFO)
     *
     * \param objPtr Pointer to the object to add
     *
     * \return LU_TRUE if the object is added successfully
     */
    lu_bool_t enqueue(queueType* objPtr)
    {
        /* Entering Critical Area */
        LockingMutex lMutex(queueMutex);

        /* Consistency Check */
        if ( ( (queueHead == NULL) && (queueTail != NULL) ) ||
             ( (queueHead != NULL) && (queueTail == NULL) )
           )
        {
            /* Reset queue pointer */
            queueHead = queueTail = NULL;

            /* reset queue counter */
            queueSema.rstValue();

            return (LU_FALSE);
        }

        /* Everything OK. Queue the message in the queue tail */
        if ( (queueHead == NULL) && (queueTail == NULL) )
        {
            queueHead = queueTail = objPtr;
            objPtr->setReference(NULL);
        }
        else
        {
            queueTail->setReference(objPtr);
            queueTail = objPtr;
            objPtr->setReference(NULL);
        }

        /* Signal new message available */
        queueSema.post();

        return (LU_TRUE);
    }

    /**
     * /brief Remove the first object (the oldest) from the queue.
     *
     * If the queue is empty it blocks waiting for a object to be added.
     *
     * \return The first object in the queue
     */
    queueType* dequeue()
    {
        /* Wait for new messages */
        queueSema.wait();

        /* A message is available in queue */
        return pop();
    }


    /* XXX: pueyos_a - dequeue(timeout) DEPRECATED. Absolute Timeout for sem_timedwait() is dangerous
     * if user gives a wrong value (such as any time value 1 or more nanoseconds less than the current
     * time) (TO BE REMOVED) */
    /**
     * /brief Remove the first object (the oldest) from the queue.
     *
     * If the queue is empty it blocks waiting for a object to be added or
     * until the timeout expires
     *
     * \param obj Buffer where the object reference is saved
     * \param absTimeout Reference to the absolute timeout
     *
     * \return Error code.
     */
    QUEUE_ERROR dequeue(queueType** obj, struct timespec *absTimeout)
    {
        if( (obj        == NULL) ||
            (absTimeout == NULL)
          )
        {
            return QUEUE_ERROR_PARAM;
        }

        /* Wait for new messages */
        if (queueSema.wait(absTimeout) == 0)
        {
            /* A message is available in queue */
            *obj = pop();

            return QUEUE_ERROR_NONE;
        }
        else
        {
            /* Get the error */
            lu_int32_t errTmp = errno;

            return (errTmp == ETIMEDOUT) ? QUEUE_ERROR_TIMEOUT : QUEUE_ERROR_SEMA;
        }
    }

    /**
     * /brief Remove the specified object from the queue.
     *
     * \param objPtr Object to remove
     *
     * \return None
     */
    void dequeue(queueType *objPtr)
    {
        queueType* curObjPtr;
        queueType* prevObjPtr;

        /* Entering Critical Area */
        LockingMutex lMutex(queueMutex);

        /* Consistency Check */
        if ( ( (queueHead == NULL) && (queueTail != NULL) ) ||
             ( (queueHead != NULL) && (queueTail == NULL) )
           )
        {
            /* Reset queue pointer */
            queueHead = queueTail = NULL;
            /* reset job counter */
            queueSema.rstValue();

            return;
        }

        /* Everything OK. Find the message */
        curObjPtr = queueHead;
        prevObjPtr = curObjPtr;
        while(curObjPtr != NULL)
        {
            /* Object found ? */
            if(curObjPtr == objPtr)
            {
                /* Yes - remove Object from the queue */
                if(prevObjPtr == curObjPtr)
                {
                    /* First object. Just update queue head */
                    queueHead = curObjPtr->getReference();
                }
                else
                {
                    prevObjPtr->setReference(curObjPtr->getReference());
                }

                /* Last Object */
                if(curObjPtr->getReference() == NULL)
                {
                    /* Update tail */
                	if(prevObjPtr != objPtr)
                		queueTail = prevObjPtr;
                	else
                		queueTail = NULL;

                }

                return;
            }
            else
            {
                /* No - Get next object in the queue */
                prevObjPtr = curObjPtr;
                curObjPtr = curObjPtr->getReference();
            }
        }

        return;
    }



    /**
     * \brief Empty Queue
     *
     * \return None
     */
    void empty()
    {
        queueHead = queueTail = NULL;
        queueSema.rstValue();
    }

    /**
     * \brief check if the queue is empty.
     *
     * \return LU_TRUE if the queue is empty
     */
    lu_bool_t isEmpty()
    {
        lu_int32_t val;

        queueSema.getValue(&val);

        return ( (val == 0) ? true : false);
    }

    /**
     * \brief Return the first object (the head) of the queue
     *
     * The object is NOT removed
     *
     * \return Pointer to the first object. NULL if the queue is empty
     */
    queueType* getFirst()
    {

        LockingMutex lMutex(queueMutex);

        return queueHead;
    }

    /**
     * /brief Return the next object in the queue relatively to the object passed as
     * parameter.
     *
     * The object is NOT removed.
     *
     * \param objPtr Current object
     *
     * \return Next object in the queue. NULL if there are no more objects
     */
    queueType* getNext(queueType* objPtr)
    {
        if(objPtr != NULL)
        {
            LockingMutex lMutex(queueMutex);

            return objPtr->getReference();
        }

        return NULL;
    }

private:
    /**
     * /brief Remove the first object (the oldest) from the queue.
     *
     * If the queue is empty the behaviour is unspecified
     *
     * \return The first object in the queue
     */
    queueType* pop()
    {
        queueType* retObjPtr;
        queueType* nxtObjPtr;

        /* Entering Critical Area */
        LockingMutex lMutex(queueMutex);

        /* Consistency Check */
        if ( ( (queueHead == NULL) && (queueTail != NULL) ) ||
             ( (queueHead != NULL) && (queueTail == NULL) )
           )
        {
            /* Reset queue pointer */
            queueHead = queueTail = NULL;
            /* reset job counter */
            queueSema.rstValue();

            return (NULL);
        }

        /* Everything OK. Dequeue a message */
        retObjPtr = queueHead;
        nxtObjPtr = retObjPtr->getReference();

        if (nxtObjPtr == NULL)
        {
            /* Pool Finished */
            queueHead = queueTail = NULL;
        }
        else
        {
            queueHead = nxtObjPtr;
        }

        return (retObjPtr);
    }


private:
    Mutex      queueMutex;
    Semaphore  queueSema ;
    queueType* queueHead ;
    queueType* queueTail ;

};


#endif // !defined(EA_129B4E18_8D89_4a01_B7B2_C4E64A6647FA__INCLUDED_)

/*
 *********************** End of file ******************************************
 */





