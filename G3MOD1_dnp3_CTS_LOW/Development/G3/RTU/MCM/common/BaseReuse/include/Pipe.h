/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: Pipe.h 8324 2015-02-16 13:09:46Z pueyos_a $
 *               $HeadURL: http://svn-general-svc.wlucynet.com:18080/svn/gemini_30_software/branches/NPG/G3MOD1_dnp3_CTS_LOW/Development/G3/RTU/MCM/common/BaseReuse/include/Pipe.h $
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_589C1549_0934_4a9d_A734_1F68CA5C6956__INCLUDED_)
#define EA_589C1549_0934_4a9d_A734_1F68CA5C6956__INCLUDED_


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <unistd.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef enum
{
    PIPE_FD_READ = 0,
    PIPE_FD_WRITE   ,

    PIPE_FD_LAST
}PIPE_FD;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class Pipe
{
public:
    /**
     * \brief Types of Pipe available.
     */
    typedef enum
    {
        PIPE_TYPE_NONBLOCKING,  //Does not block when reading or writing
        PIPE_TYPE_BLOCKING,     //Does block when reading or writing
        PIPE_TYPE_BLOCKREAD,    //Does block when reading only
        PIPE_TYPE_BLOCKWRITE    //Does block when writing only
    } PIPE_TYPE;

public:
    /**
     * \brief Create a new Posix pipe
     *
     * \param pipeType Type of blocking pipe to be created
     */
    Pipe(Pipe::PIPE_TYPE pipeType);

    virtual ~Pipe();

    /**
     * \brief Get a pipe file descriptor
     *
     * \param type File descriptor type
     *
     * \return File descriptor
     */
    lu_int32_t getFD(PIPE_FD type);

    /**
     * \brief Write to the pipe
     *
     * \param buffer Buffer to write
     * \param bufferSize Buffer size
     *
     * \return Number of bytes written. -1 when error. Check errno for detailed error report
     */
    lu_int32_t writeP(lu_uint8_t *buffer, lu_uint32_t bufferSize);

    /**
     * \brief Read from the pipe
     *
     * It will BLOCK only when the Pipe is configured to do so.
     *
     * \param buffer Buffer where the data is saved
     * \param bufferSize Buffer size
     *
     * \return Number of bytes read. -1 when error. Check errno for detailed error report
     */
    lu_int32_t readP(lu_uint8_t *buffer, lu_uint32_t bufferSize);

    /**
     * \brief Read from the pipe AND BLOCKS with timeout
     *
     * It blocks waiting to have any available data in the pipe, EVEN if the Pipe
     * was NOT configured TO BLOCK when reading. If do not want to block, use
     * other readP() or set the timeout to a low value.
     *
     * \param buffer Buffer where the data is saved
     * \param bufferSize Buffer size
     * \param timeout Time to wait for an entry to be available. May be overwritten.
     *
     * \return error code. 0 when OK, 1 when timeout, <0 when error.
     */
    lu_int32_t readP(lu_uint8_t *buffer,
                    lu_uint32_t bufferSize,
                    const struct timeval timeout
                    );

    /**
     * \brief Read from the pipe AND BLOCKS with timeout
     *
     * It blocks waiting to have any available data in the pipe, EVEN if the Pipe
     * was NOT configured TO BLOCK when reading. If do not want to block, use
     * other readP() or set the timeout to a low value.
     *
     * Any remaining time in the timeout will be written back to the timeout pointer
     *
     * \param buffer     Buffer where the data is saved
     * \param bufferSize Buffer size
     * \param timeoutPtr Time to wait for an entry to be available. May be overwritten.
     *                   Any time remaining in the timeout will be written back to the pointer
     *
     * \return error code. 0 when OK, 1 when timeout, <0 when error.
     */
    lu_int32_t readP(lu_uint8_t *buffer,
                    lu_uint32_t bufferSize,
                    struct timeval* timeoutPtr
                    );

private:
    lu_int32_t fildes[PIPE_FD_LAST];

};


#endif // !defined(EA_589C1549_0934_4a9d_A734_1F68CA5C6956__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
