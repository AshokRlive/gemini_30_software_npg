/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:TCPSocket.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   21 Jul 2014     andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef TCPSOCKET_H_
#define TCPSOCKET_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <errno.h>
#include <unistd.h>     // close

#include <arpa/inet.h>  // htons
#include <sys/types.h>
#include <sys/socket.h> // socket/bind/connect/listen/accept
#include <fcntl.h>      // fcntl
#include <signal.h>
#include <string.h>     // memset
#include <netinet/tcp.h>
#include <netdb.h>      // getaddrinfo

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"



/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
class TCPSocket
{
public:
    typedef enum
    {
        SOCK_ERROR_NONE = 0,
        SOCK_ERROR_CONFIG,
        SOCK_ERROR_CONNECTION,
        SOCK_ERROR_STATE,
        SOCK_ERROR_LAST
    } SOCK_ERROR;

    typedef enum
    {
        SOCK_STATE_UNKNOWN,
        SOCK_STATE_ERROR,
        SOCK_STATE_CLOSED,
        SOCK_STATE_INIT,
        SOCK_STATE_LISTENING,
        SOCK_STATE_CONNECTING,
        SOCK_STATE_CONNECTED,
        SOCK_STATE_LAST

    } SOCK_STATE;

public:
    /**
     * \brief Create a new Posix socket
     *
     * \return none
     */
    TCPSocket();

    virtual ~TCPSocket();

    /*
     * \brief Create socket and set sig masks
     *
     * \return Error Code
     */
    SOCK_ERROR init();

    /*
     * \brief Set socket to reuse address
     *
     * \param sockFd - Socket Descriptor
     *
     * \return Error Code
     */
    SOCK_ERROR setReuseAddr(lu_int32_t sockFd);

    /*
     * \brief Set socket to reuse address
     *
     * \return Error Code
     */
    SOCK_ERROR setReuseAddr();

    /*
     * \brief Set socket to non-blocking
     *
     * \param sockFd - Socket Descriptor
     *
     * \return Error Code
     */
    SOCK_ERROR setNonBlocking(lu_int32_t sockFd);

    /*
     * \brief Set socket to non-blocking
     *
     * \return Error Code
     */
    SOCK_ERROR setNonBlocking();

    /*
     * \brief Set socket to no delay
     *
     * \param sockFd - Socket Descriptor
     *
     * \return Error Code
     */
    SOCK_ERROR setNoDelay(lu_int32_t sockFd);

    /*
     * \brief Set socket to no delay
     *
     * \return Error Code
     */
    SOCK_ERROR setNoDelay();

    /*
     * \brief Listen on a given ipAddress (e.g. 127.0.0.1 for loopback)
     *
     * \param ipAddress - ip address to listen on (bind)
     * \param port      - port to listen on
     *
     * \return Error Code
     */
    SOCK_ERROR listenSocket(const lu_char_t *ipAddress, lu_int32_t port);

    /*
     * \brief Listen on supplied port (all interfaces)
     *
     * \param port - port to listen on
     *
     * \return Error Code
     */
    SOCK_ERROR listenSocket(lu_int32_t port);

    /* TODO: pueyos_a - Improve acceptSocket by returning an already reallocated pointer to the IP address */
    /*
     * \brief Accept a socket connection
     *
     * \warning: ipAdress must have enough space reserved beforehand
     *
     * \param socket       Socket Object
     * \param ipAddress    If not NULL, the ipAddress of the connecting device is returned
     *
     * \return Error Code
     */
    SOCK_ERROR acceptSocket(TCPSocket *socket, lu_char_t *ipAddress);

    /*
     * \brief Connect to supplied ip and port
     *
     * \param ipAddress   - IpAddress to connect to
     * \param port        - port to connect to
     * \param nonBlocking - Non blocking connect
     *
     * \return Error Code
     */
    SOCK_ERROR connectSocket(const lu_char_t *ipAddress, lu_int32_t port, lu_bool_t nonBlocking);

    /*
     * \brief Check if a connecting socket is connected yet
     *
     * \param timeout Timeout value for the select()
     *
     * \return Error Code
     */
    SOCK_ERROR updateConnectedState(struct timeval timeout);

    /*
     * \brief Close the socket
     *
     * \return Error Code
     */
    SOCK_ERROR closeSocket();

    /**
     * \brief Get a socket file descriptor
     *
     * \return File descriptor
     */
    inline lu_int32_t getFD() { return socketFd; };

    /**
     * \brief Get state of the socket
     *
     * \return state
     */
    inline lu_int32_t getState() { return state; };

    /**
     * \brief See if the socket state is CONNECTED
     *
     * \return LU_TRUE or LU_FALSE
     */
    inline lu_bool_t isConnected() { return (state == SOCK_STATE_CONNECTED) ? LU_TRUE : LU_FALSE; };

    /**
     * \brief See if the socket state is CONNECTING
     *
     * \return LU_TRUE or LU_FALSE
     */
    inline lu_bool_t isConnecting() { return (state == SOCK_STATE_CONNECTING) ? LU_TRUE : LU_FALSE; };

    /**
     * \brief Write to the socket
     *
     * \param buffer     Buffer to write
     * \param bufferSize Buffer size
     * \param flags      Flags for send()
     *
     * \return POSIX error code
     */
    SOCK_ERROR writeSocket(lu_uint8_t *buffer, lu_uint32_t bufferSize, lu_int32_t flags);

    /**
     * \brief Write to the socket
     *
     * \param buffer     Buffer to write
     * \param bufferSize Buffer size
     *
     * \return POSIX error code
     */
    SOCK_ERROR writeSocket(lu_uint8_t *buffer, lu_uint32_t bufferSize);

    /**
     * \brief Read from the socket
     *
     * \param buffer        Buffer where the data is saved
     * \param bufferSize    Buffer size
     * \param timeoutPtr    Pointer to timeout
     * \param flags         Flags for recv()
     *
     * \return #bytes read, 0 for a tiemout (no data) and -1 for error
     */
    lu_int32_t readSocket(lu_uint8_t *buffer, lu_uint32_t bufferSize, struct timeval *timeoutPtr, lu_int32_t flags);

    /**
     * \brief Read from the socket
     *
     * \param buffer        Buffer where the data is saved
     * \param bufferSize    Buffer size
     * \param timeoutPtr    Pointer to timeout
     *
     * \return #bytes read or -1 for error
     */
    lu_int32_t readSocket(lu_uint8_t *buffer, lu_uint32_t bufferSize, struct timeval *timeoutPtr);

    /**
     * \brief Read from the socket
     *
     * \param buffer        Buffer where the data is saved
     * \param bufferSize    Buffer size
     * \param timeoutMs     timeout in milliseconds
     *
     * \return #bytes read or -1 for error
     */
    lu_int32_t readSocketMs(lu_uint8_t *buffer, lu_uint32_t bufferSize, lu_uint64_t timeoutMs);

private:
    lu_int32_t socketFd;

    SOCK_STATE state;
};



#endif /* TCPSOCKET_H_ */

/*
 *********************** End of file ******************************************
 */
