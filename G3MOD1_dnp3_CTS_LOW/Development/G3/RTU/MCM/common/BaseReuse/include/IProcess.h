/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: IProcess.h 4135 2013-11-14 15:04:08Z pueyos_a $
 *               $HeadURL: http://svn-general-svc.wlucynet.com:18080/svn/gemini_30_software/branches/NPG/G3MOD1_dnp3_CTS_LOW/Development/G3/RTU/MCM/common/BaseReuse/include/IProcess.h $
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_BD65FF06_A999_419c_892F_FECDE70FFF98__INCLUDED_)
#define EA_BD65FF06_A999_419c_892F_FECDE70FFF98__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <pthread.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef enum
{
    SCHED_TYPE_NORMAL = SCHED_OTHER,
    SCHED_TYPE_FIFO =   SCHED_FIFO,
    SCHED_TYPE_RR  =    SCHED_RR,
    SCHED_TYPE_LAST
}SCHED_TYPE;

typedef enum
{
    THREAD_ERR_NONE      = 0,
    THREAD_ERR_INVALID_STATE,
    THREAD_ERR_CREATE_FAILED,
    THREAD_ERR_CANCEL_FAILED,
    THREAD_ERR_JOIN_FAILED,
    THREAD_ERR_THROW_EXCEPTION
}THREAD_ERR;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class IProcess
{

public:
    IProcess() {};

    virtual ~IProcess() {};

#ifndef PRIO_MIN
    static const lu_uint32_t PRIO_MIN = 1;
#endif

    /**
     * Highest priority
     */
#ifndef PRIO_MAX
    const static lu_uint32_t PRIO_MAX = 50;
#endif

    /**
     * \brief Start a process
     *
     * \return LU_TRUE if the process started correctly
     */
    virtual THREAD_ERR start() = 0;

    /**
     * \brief Stop (cancel) a detached thread
     *
     * A joinable thread cannot be cancelled
     *
     * \return LU_TRUE if the process stopped correctly
     */
    virtual THREAD_ERR stop() = 0;

    /**
     * \brief Wait for a joinable thread termination
     *
     * A detached thread cannot be joined

     * \return 0 when OK, < 0 in case of errors
     */
    virtual THREAD_ERR join() = 0;

    /**
     * \brief Return the scheduler in use
     *
     * \return Scheduler type
     */
    virtual SCHED_TYPE getSchedType() = 0;

    /**
     * \brief Return the current priority
     *
     * \return Priority
     */
    virtual lu_uint32_t getPriority() = 0;
};


#endif // !defined(EA_BD65FF06_A999_419c_892F_FECDE70FFF98__INCLUDED_)

/*
 *********************** End of file ******************************************
 */

