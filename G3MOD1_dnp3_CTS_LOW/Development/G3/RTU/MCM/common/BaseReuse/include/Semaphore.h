/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: Semaphore.h 4026 2013-10-21 13:07:19Z pueyos_a $
 *               $HeadURL: http://svn-general-svc.wlucynet.com:18080/svn/gemini_30_software/branches/NPG/G3MOD1_dnp3_CTS_LOW/Development/G3/RTU/MCM/common/BaseReuse/include/Semaphore.h $
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_C71CB5EB_2536_45e3_B7EA_95FE2651AE7C__INCLUDED_)
#define EA_C71CB5EB_2536_45e3_B7EA_95FE2651AE7C__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <semaphore.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class Semaphore
{

public:
	/**
	 * \brief Create a new Semaphore
	 * 
     * \return None
     */
    inline Semaphore() {sem_init(&sema, 0, 0);};

    /**
     * \brief Create a Semaphore (advanced)
     *
     * \param shared True if this is a shared(across different processes) semaphore
     * \param value Initial value of the semaphore
     *
     * \return Posix error code
     */
    inline  Semaphore(lu_bool_t shared, lu_uint32_t value)
    {
        sem_init(&sema, shared, value);
    };

    inline virtual ~Semaphore() {sem_destroy(&sema);};

    /**
     * \brief wait on a Semaphore. Block if the Semaphore value is zero
     *
     * \return Posix error code
     */
    inline lu_int32_t wait() {return sem_wait(&sema);};

    /* XXX: pueyos_a - DEPRECATED (to be removed) */
    /**
     * \brief wait on a Semaphore. Block if the Semaphore value is zero until
     * the timeout
     *
     * WARNING!: ANY wrong value on absTimeout (any value in the past, even 1 ns)
     *          could affect nanosleep() and make the system to stop/delay forever
     *          running threads, as well as preventing new threads from starting.
     *
     *\param absTimeout Pointer to the absolute timeout
     *
     * \return Posix error code
     */
    inline lu_int32_t wait(struct timespec *absTimeout) {return sem_timedwait(&sema, absTimeout);};

    /**
     * \brief Increase the semaphore value
     *
     * \return Posix error code
     */
    inline lu_int32_t post() {return sem_post(&sema);};

    /**
     * \brief Get the current semaphore internal counter
     *
     * \param val Pointer to the buffer where the value is saved
     *
     * \return Posix error code
     */
    inline lu_int32_t getValue(lu_int32_t* val) {return sem_getvalue(&sema, val);};

    /**
     * \brief Reset internal semaphore counter
     *
     * \return Posix error code
     */
    inline lu_int32_t rstValue() {return sem_init(&sema, 0, 0);};

private:
    /**
     * Posix semaphore ID
     */
    sem_t sema;

};


#endif // !defined(EA_C71CB5EB_2536_45e3_B7EA_95FE2651AE7C__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
