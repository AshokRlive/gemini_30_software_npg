/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ListMgr.h 4046 2013-10-24 15:07:18Z pueyos_a $
 *               $HeadURL: http://svn-general-svc.wlucynet.com:18080/svn/gemini_30_software/branches/NPG/G3MOD1_dnp3_CTS_LOW/Development/G3/RTU/MCM/common/BaseReuse/include/ListMgr.h $
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_990C9240_3B87_4b35_B3C5_17DA6D68F1CF__INCLUDED_)
#define EA_990C9240_3B87_4b35_B3C5_17DA6D68F1CF__INCLUDED_


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stddef.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * Implement a simple linked list. For performance reason the list doesn't have
 * any internal mutex. The caller MUST ensure the operations are atomic.
 * "listType" MUSt inherit from QueueObj.
 */
template<class listType>
class ListMgr
{

public:
    ListMgr() : listHead(NULL),
                listTail(NULL),
                entryNum(0)
    {};

    virtual ~ListMgr() {};

    /**
     * \brief add an object to the list
     *
     * \param obj Object to add
     *
     * \return LU_TRUE if the obj has been correctly added
     */
    lu_bool_t add(listType* objPtr)
    {
        if(objPtr == NULL)
        {
            return LU_FALSE;
        }

        /* Consistency Check */
        if ( ( (listHead == NULL) && (listTail != NULL) ) ||
             ( (listHead != NULL) && (listTail == NULL) )
           )
        {
            /* Reset queue pointer */
            listHead = listTail = NULL;
            entryNum = 0;

            return (LU_FALSE);
        }

        /* Everything OK. Queue the message in the queue tail */
        if ( (listHead == NULL) && (listTail == NULL) )
        {
            listHead = listTail = objPtr;
            objPtr->setReference(NULL);
        }
        else
        {
            listTail->setReference(objPtr);
            listTail = objPtr;
            objPtr->setReference(NULL);
        }

        /* Object added correctly */
        entryNum++;

        return (LU_TRUE);
    }

    /**
     * \brief Remove an object from the list
     *
     * \param obj Object to remove
     *
     * \return LU_TRUE if the obj has been correctly removed
     */
    lu_bool_t remove(listType* objPtr)
    {
        listType* curObjPtr;
        listType* prevObjPtr;

        if(objPtr == NULL)
        {
            return LU_TRUE; //is "correctly removed" anyway
        }

        /* Consistency Check */
        if ( ( (listHead == NULL) && (listTail != NULL) ) ||
             ( (listHead != NULL) && (listTail == NULL) )
           )
        {
            /* Reset queue pointer */
            listHead = listTail = NULL;
            entryNum = 0;

            return LU_FALSE;
        }

        /* Everything OK. Find the message */
        curObjPtr = listHead;
        prevObjPtr = curObjPtr;

        while(curObjPtr != NULL)
        {
            /* Object found ? */
            if(curObjPtr == objPtr)
            {
                /* Yes - remove Object from the queue */
                if(prevObjPtr == curObjPtr)
                {
                    /* First object. Just update queue head */
                    listHead = curObjPtr->getReference();
                }
                else
                {
                    prevObjPtr->setReference(curObjPtr->getReference());
                }

                /* Last Object */
                if(curObjPtr->getReference() == NULL)
                {
                    /* Update tail */
                    if(prevObjPtr == objPtr)
                    {
                        listTail = NULL;
                    }
                    else
                    {
                        listTail = prevObjPtr;
                        listTail->setReference(NULL);
                    }
                }

                /* Object removed correctly */
                if(entryNum > 0)
                {
                    entryNum--;
                }

                return LU_TRUE;
            }
            else
            {
                /* No - Get next object in the queue */
                prevObjPtr = curObjPtr;
                curObjPtr = curObjPtr->getReference();
            }
        }

        return LU_FALSE;
    }

    /**
     * \brief Remove all objects from the list
     *
     * \param None
     *
     * \return LU_TRUE if the list has been correctly emptied
     */
    lu_bool_t removeAll()
    {
        entryNum = 0;
        listHead = listTail = NULL;

        return LU_TRUE;
    }

    /**
     * \brief Get the first object in the list
     *
     * \return Pointer to the first object in the list
     */
    listType* getFirst()
    {
        return listHead;
    }

    /**
     * Get object in position idx
     *
     * \param Position of the object to retrieve
     *
     * \return Pointer to the object
     */
    listType* getIdx(lu_uint32_t idx)
    {
        listType *listTypeTmp;

        /* Check index */
        if(idx > entryNum)
        {
            return NULL;
        }

        /* Search for the entry */
        for( listTypeTmp = listHead;
             (idx > 0) && (listTypeTmp != NULL);
             idx--, listTypeTmp = listTypeTmp->getReference()
           );

        /* Return the entry */
        return listTypeTmp;
    }

    /**
     * \brief Get the next object in the list
     *
     * \param objPtr Get the next object
     *
     * \return Pointer to the next object. NULL if there are no more objects
     */
    listType* getNext(listType* objPtr)
    {
        if(objPtr != NULL)
        {
            return objPtr->getReference();
        }

        return NULL;
    }

    /**
     * \brief Return the number of entries
     *
     * \return Number of entries
     */
    lu_uint32_t getEntries() {return entryNum;};
private:
    listType *listHead;
    listType *listTail;
    lu_uint32_t entryNum;

};
#endif // !defined(EA_990C9240_3B87_4b35_B3C5_17DA6D68F1CF__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
