/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: RAWBuffer.h 1466 2012-06-20 20:42:54Z fryers_j $
 *               $HeadURL: http://svn-general-svc.wlucynet.com:18080/svn/gemini_30_software/branches/NPG/G3MOD1_dnp3_CTS_LOW/Development/G3/RTU/MCM/common/BaseReuse/include/RAWBuffer.h $
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       RAW buffer container
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/02/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_5EB0198A_C649_43fc_8863_6C90E060D4A3__INCLUDED_)
#define EA_5EB0198A_C649_43fc_8863_6C90E060D4A3__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

struct PayloadRAW
{

public:
    lu_uint8_t* payloadPtr;
    lu_uint32_t payloadLen;

};

struct StringRAW
{

public:
    lu_char_t*  payloadPtr;
    lu_uint32_t payloadLen;

};

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */




#endif // !defined(EA_5EB0198A_C649_43fc_8863_6C90E060D4A3__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
