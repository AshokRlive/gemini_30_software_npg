/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id: Mutex.h 4105 2013-11-07 09:46:58Z pueyos_a $
 *               $HeadURL: http://svn-general-svc.wlucynet.com:18080/svn/gemini_30_software/branches/NPG/G3MOD1_dnp3_CTS_LOW/Development/G3/RTU/MCM/common/BaseReuse/include/Mutex.h $
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief Basic components public header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   16/06/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#if !defined(EA_6D49A5D1_CAE2_4bbb_BF81_C35F81FD0A7B__INCLUDED_)
#define EA_6D49A5D1_CAE2_4bbb_BF81_C35F81FD0A7B__INCLUDED_


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <pthread.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */



/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class Mutex
{

public:
    /**
     * \brief Create a new mutex
     *
     * \param prioInherit If true the priority inheritance feature is enabled
     */
    Mutex(lu_bool_t prioInherit = LU_FALSE);
    virtual ~Mutex();

    /**
     * \brief Acquire exclusive lock of the mutex. If the mutex is already locked by
     * another thread the caller is blocked.
     *
     * \return Posix error code
     */
    inline lu_int32_t lock() {return pthread_mutex_lock(&mutex);}

    /**
     * \brief Release the exclusive lock previously acquired
     *
     * \return Posix error code
     */
    inline lu_int32_t unlock() {return pthread_mutex_unlock(&mutex);}

    /**
     * \brief Acquire exclusive lock of the mutex. If the mutex is already locked the
     * function returns immediately with an error code.
     *
     * \return Posix error code
     */
    inline lu_int32_t tryLock() {return pthread_mutex_trylock(&mutex);}

private:
    pthread_mutex_t mutex;

};


/**
 * \brief Master preference Lock class
 *
 * This class implements a read/write lock (mutex) that give priority to writers
 * ("Masters") in order to avoid Master to be starved by a faster reader thread.
 * The Master role is intended for very few methods.
 * An example is a close() method that needs to prevent a processing thread
 * to acquire the mutex all the time, thus giving the Master a kind of
 * "priority" independently of the priorities of the two threads.
 */
class MasterMutex
{

public:
    /**
     * \brief Create a new R/W Lock mutex
     *
     * \param shared When set to LU_FALSE, only threads within the process can access this mutex.
     */
    MasterMutex(lu_bool_t shared = LU_FALSE);
    ~MasterMutex();

    /**
     * \brief Acquire exclusive lock of the mutex from a reader.
     * If the mutex is already locked by another thread the caller is blocked.
     * Note that this method is intended for the majority of the concurrent
     * processes.
     *
     * \return POSIX error code
     */
    inline lu_int32_t rlock() {return pthread_rwlock_rdlock(&rwMutex);}

    /**
     * \brief Acquire exclusive lock of the mutex from a writer.
     * The writer will supposedly have the preference as soon as the mutex is
     * available.
     * If the mutex is already locked by another thread the caller is blocked.
     * Note that this method is intended for a few of the concurrent processes
     * only.
     *
     * \return POSIX error code
     */
    inline lu_int32_t wlock() {return pthread_rwlock_wrlock(&rwMutex);}

    /**
     * \brief Release the exclusive lock previously acquired
     *
     * \return POSIX error code
     */
    inline lu_int32_t unlock() {return pthread_rwlock_unlock(&rwMutex);}

    /**
     * \brief Try to acquire exclusive lock of the mutex as reader.
     * If the mutex is already locked, the method returns immediately with a
     * POSIX error code.
     *
     * \return POSIX error code
     */
    inline lu_int32_t tryLock() {return pthread_rwlock_tryrdlock(&rwMutex);}

private:
    pthread_rwlock_t rwMutex;
};


#endif // !defined(EA_6D49A5D1_CAE2_4bbb_BF81_C35F81FD0A7B__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
