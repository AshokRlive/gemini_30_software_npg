/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: Thread.h 6175 2014-09-24 15:20:26Z wang_p $
 *               $HeadURL: http://svn-general-svc.wlucynet.com:18080/svn/gemini_30_software/branches/NPG/G3MOD1_dnp3_CTS_LOW/Development/G3/RTU/MCM/common/BaseReuse/include/Thread.h $
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_BFC109D1_875A_4c25_A609_9ACD51B0EA26__INCLUDED_)
#define EA_BFC109D1_875A_4c25_A609_9ACD51B0EA26__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <pthread.h>
#include <string>
#include <signal.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "IProcess.h"
#include "LockingMutex.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
typedef enum
{
    THREAD_STATE_NEW,   // Thread object created and wait to be started
    THREAD_STATE_READY, //Ready to run thread body
    THREAD_STATE_RUNNING,
    THREAD_STATE_INTERRUPTING,
    THREAD_STATE_FINISHED
}THREAD_STATE;

/* Use the C prctl() function to name the Thread
   Not available in the C++ libraries
*/
extern "C"
{
    #include <sys/prctl.h>

    #define SETTHREADNAME(threadName)       prctl(PR_SET_NAME, threadName, 0, 0, 0)
}


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */



class Thread : public IProcess
{
public:
    enum
    {
        TYPE_DETACHED = LU_TRUE,
        TYPE_JOINABLE = LU_FALSE
    } TYPE;

public:
    /**
     * \brief Create a new thread
     *
     * If we create a thread in a joinable state all the resources of the thread
     * will be released ONLY after we call the join. A detached thread can NOT
     * be joined
     *
     * \param scheduleType Thread Scheduler type
     * \param priority Thread priority
     * \param detached Set to Thread::TYPE_DETACHED for a detached thread, Thread::TYPE_JOINABLE for a joinable thread
     * \param name Name of the thread for debugging purposes
     *
     * \return None
     */
    Thread(SCHED_TYPE scheduleType, lu_uint32_t priority, lu_bool_t detached, const std::string name = "NoNameThread");

    virtual ~Thread()
    {
        if(isRunning())
        {
            stop();

            /* Joinable thread must be joined to release resources.*/
            if(isJoinable())
                join();
        }
    };


    virtual THREAD_ERR start();

    virtual THREAD_ERR stop();

    virtual THREAD_ERR join();

    virtual SCHED_TYPE getSchedType();

    virtual lu_uint32_t getPriority();

    virtual lu_bool_t isInterrupting();

    virtual lu_bool_t isRunning();

    virtual lu_bool_t isFinished();

    virtual THREAD_STATE getState();

    virtual lu_bool_t isJoinable();

    pthread_t getTID(){return threadID;}

protected:
    /**
     * \brief Thread main loop
     *
     * This function is called automatically when a thread is started.
     *
     * Please note that:
     *      -a Joinable thread should finish by itself
     *      -a Detached thread will be interrupted when a cancellation point
     *          is reached (use pthread_testcancel() if needed)
     *      -keep in mind when and where the thread could be interrupted,
     *          taking care of which resources will be in need to be freed.
     */
    virtual void threadBody() = 0;

private:
    /**
     * \brief Internal routine for thread body running.
     *
     * \param pth Pointer to the thread object
     *
     * \return NULL
     */
    static void* start_routine(void* pth);

private:
    /**
     * \brief Type of thread to be created
     */
    typedef enum
    {
        THREADTYPE_DETACHED = PTHREAD_CREATE_DETACHED,    //Detached: can be stopped at cancellation points
        THREADTYPE_JOINABLE = PTHREAD_CREATE_JOINABLE,    //Joinable: Should finish thread body
        THREADTYPE_LAST
    }THREADTYPE;

    /**
     * \brief Thread stack size in bytes
     */
    static const lu_uint32_t stackSize = 300 * 1024;

    THREAD_STATE state;
    pthread_t threadID;     //POSIX Thread identifier
    SCHED_TYPE schedType;   //App scheduler type - related to POSIX scheduler type
    lu_uint32_t priority;   //Thread priority
    const THREADTYPE threadType;  //Thread type: detached or joinable
    const std::string threadName;
    Mutex locker;           //Protects concurrent access to Thread functionality

};



inline void SIG_Block(int sig){

    sigset_t signalMask;
    /*Unblock */
    sigemptyset(&signalMask);
    sigaddset(&signalMask, sig);
    sigprocmask(SIG_BLOCK, &signalMask, NULL );
}

inline void SIG_Unblock(int sig){

    sigset_t signalMask;
    /*Unblock */
    sigemptyset(&signalMask);
    sigaddset(&signalMask, sig);
    sigprocmask(SIG_UNBLOCK, &signalMask, NULL );
}

inline void SIG_BlockAll()
{
    /*Block all signal*/
    sigset_t signalMask;
    sigfillset(&signalMask);
    pthread_sigmask(SIG_BLOCK, &signalMask, NULL);
}

/**
 * Block all signals except termination and interrupt.
 */
inline void SIG_BlockAllExceptTerm()
{
    /*Block all signal*/
    sigset_t signalMask;
    sigfillset(&signalMask);
    pthread_sigmask(SIG_BLOCK, &signalMask, NULL);

    SIG_Unblock(SIGTERM);
    SIG_Unblock(SIGINT);
}

#endif // !defined(EA_BFC109D1_875A_4c25_A609_9ACD51B0EA26__INCLUDED_)

/*
 *********************** End of file ******************************************
 */





