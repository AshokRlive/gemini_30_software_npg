/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: Timer.h 7040 2014-11-27 12:26:15Z pueyos_a $
 *               $HeadURL: http://svn-general-svc.wlucynet.com:18080/svn/gemini_30_software/branches/NPG/G3MOD1_dnp3_CTS_LOW/Development/G3/RTU/MCM/common/BaseReuse/include/Timer.h $
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_5D578F21_731C_45b2_878C_AECEB5B13090__INCLUDED_)
#define EA_5D578F21_731C_45b2_878C_AECEB5B13090__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <signal.h>
#include <time.h>
#include <string>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Thread.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
class Timer;
class TimerThread;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief This class interfaces a handler timer event.
 */
class ITimerHandler
{
public:
    virtual ~ITimerHandler(){};

    /**
     * \brief This method will be called when time expires, ringing the alarm.
     *
     * \caution Do NOT stop or delete the Timer from within this method.
     *          Nevertheless, it is possible to re-program/restart the timer.
     *
     * \param Timer The timer calling this method. Use it when called from different timers
     */
    virtual void handleAlarmEvent(Timer &source) = 0;
};


/**
 * \brief Implements a system timer.
 *
 * This class uses system timer that could be waited over or could execute a
 * given handler code.
 * The timer could be created without handler, leaving to the caller to have to
 * wait over the timer signal by using Timer::wait(); or it could be created by
 * providing an ITimerHandler handler that will be executed when the timer
 * expires (unless the timer is stopped before expiring).
 *
 * IMPORTANT NOTE about Timers with handlers:
 *    Given that POSIX timers have its own handlers, but the content is limited
 *    to async-signal-safe instructions, we opted instead for handling the timer
 *    activity in an associate thread. This gives the caller freedom about the
 *    code in the handler, and the thread will be idle most of the time, except
 *    when it is executing the handler code.
 *    This implementation behaves almost the same as having a POSIX timer
 *    handler to grant a semaphore for running the code in a separate thread.
 *    In oder to avoid undesirable thread-killing side effects, the thread is
 *    kept alive alongside of the timer, and re-used every time the Timer is
 *    re-started.
 */
class Timer
{
public:
    static const lu_int32_t INVALID_TIMER = -2;

    /**
     * Types of timer
     */
    typedef enum
    {
        TIMER_TYPE_ONESHOT,     //non-periodic
        TIMER_TYPE_PERIODIC,    //periodic
        TIMER_TYPE_ONMINUTE     //periodic, every minute on the minute - Time adjustments affect it
    } TIMER_TYPE;

public:
    /**
     * \brief Create a new timer
     *
     * In order to use this timer, use the provided wait() method.
     *
     * \param sec Timer expiration time (second)
     * \param usec Timer expiration time (usecond)
     * \param timerType Timer type
     * \param name Timer name for the associated thread, if any (not used here)
     *
     * \return Posix error code
     */
    Timer(lu_uint32_t sec,
          lu_uint32_t usec,
          Timer::TIMER_TYPE periodic,
          const lu_char_t* name = NULL
         );

    /**
     * \brief Create a new timer with a signal handler
     *
     * The signal will be managed by the handler provided. The wait() method
     * doesn't block.
     *
     * \param sec Timer expiration time (second)
     * \param usec Timer expiration time (usecond)
     * \param handler Handler called when the timer expires
     * \param timerType Timer type
     * \param name Timer name for the associated thread. Set to less than 15 chars for readability
     *
     * \return Posix error code
     */
    Timer(lu_uint32_t sec,
          lu_uint32_t usec,
          ITimerHandler* handler,
          Timer::TIMER_TYPE timerType,
          const lu_char_t* name = NULL
        );

    virtual ~Timer();

    /**
     * \brief Start the timer
     *
     * \return POSIX error code, plus INVALID_TIMER if there are no timers available
     */
    lu_int32_t start();

    /**
     * \brief Re-Start the timer with given time instead of the configured one
     *
     * \param New time setting
     *
     * \return POSIX error code, plus INVALID_TIMER if there are no timers available
     */
    lu_int32_t start(const struct timespec setTime);

    /**
     * \brief Wait for the timer to expire.
     * Note it will not wait if a handler is set because wait cannot be called
     *  in multi thread(caller thread and timer thread), otherwise, the handler
     *  may never be called.
     *
     * \return POSIX error code: check errno when -1; and INVALID_TIMER if there are no timers available.
     */
    lu_int32_t wait();

    /**
     * \brief Stop a running timer
     *
     * \return POSIX error code
     */
    lu_int32_t stop();

    /**
     * \brief Reset a running timer
     *
     * \return POSIX error code
     */
    lu_int32_t reset();

    /**
     * For unit test usage only
     */
    lu_int32_t getSignalId() { return signalId; }

    /* USING POSIX TIMER HANDLERS */
    /* We leave this code here until timer signals with handlers are better
     * understood and proven to be trusted.
    static void alarmFunction(lu_int32_t signo, siginfo_t *info, void *context);
    */

private:
    lu_int32_t _wait();

    void init(lu_uint32_t sec,
                lu_uint32_t usec,
                ITimerHandler* handler,
                Timer::TIMER_TYPE timerType,
                const lu_char_t* name
              );

    /* USING POSIX TIMER HANDLERS */
    /* We leave this code here until timer signals with handlers are better
     * understood and proven to be trusted.
    void memberAlarmFunction();
    */

private:
    timer_t timerId;            //ID of this timer once is created
    lu_int32_t signalId;        //Signal used for this timer
    Timer::TIMER_TYPE type;     //Timer type
    ITimerHandler* tHandler;    //Handler to be executed when time is out
    TimerThread* tThread;       //Associated thread for timer wait/handler execution
    std::string name;           //Timer name, used for the associated thread, if any
    struct sigevent timerEvent; //Timer configuration structure
    /* USING POSIX TIMER HANDLERS */
    /* We leave this code here until timer signals with handlers are better
     * understood and proven to be trusted.
    struct sigaction timerOldAction;
    */
    struct itimerspec timerValue;   //Time intervals (first and periodic)
    sigset_t signalSet;             //Signal configuration

    friend class TimerThread;
};


/**
 * \brief Class for keeping the associated thread in charge of the handler
 *
 * This class creates a thread specifically to wait over a timer and then call
 * its handler function. It is only meant for timers with associated handlers.
 */
class TimerThread : public Thread
{
public:

    /**
     *
     * \brief Custom constructor
     *
     * \param timerOwner Timer associated to this thread
     * \param timerHandler Function/Member to call when the timer expires
     * \param signal Signal fired when the timer expires
     * \param name Name to be used for the thread
     */
    TimerThread(Timer& timerOwner,
                ITimerHandler &timerHandler,
                lu_int32_t signal,
                std::string name
                );
    ~TimerThread();

protected:
    void threadBody();

private:
    Timer& timer;
    ITimerHandler& tmrHandler;
    lu_int32_t signalID;        //timer signal ID
};


#endif // !defined(EA_5D578F21_731C_45b2_878C_AECEB5B13090__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
