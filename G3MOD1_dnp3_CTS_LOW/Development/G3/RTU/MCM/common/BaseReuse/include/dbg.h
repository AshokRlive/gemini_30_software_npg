/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:Logger.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   12 May 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef __dbg_h__
#define __dbg_h__

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdio.h>
#include <errno.h>
#include <string.h>

#ifdef __cplusplus
#include <string>
#include <typeinfo>
#endif
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#ifndef SUBSYSTEM_NAME
#define SUBSYSTEM_NAME ""
#define DBG_SEPARATOR  ""
#else
#define DBG_SEPARATOR " >> "
#endif



/* Current source name without path*/
#define __FNAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#define STRINGIZE(x) STRINGIZE2(x)
#define STRINGIZE2(x) #x
#define LINE_STRING STRINGIZE(__LINE__)

/* Current source line plus file name */
#ifdef __AT__
#undef __AT__
#endif
#ifdef _cplusplus
#define __AT__ ((std::string(__FNAME__)+" line:"+LINE_STRING)).c_str()
#else
#define __AT__ __FNAME__
#endif

/* Error number string*/
#define clean_errno() (errno == 0 ? "None" : strerror(errno))

/* Removing definitions to prevent "warning: DBG_xxx redefined" compiler messages */
#ifdef DBG_ERR
#undef DBG_ERR
#undef DBG_EMPTY
#undef DBG_INFO
#undef DBG_WARN
#undef DBG_TRACK
#undef DBG_ERRNO
#undef DBG_CHECK
#undef DBG_CTOR
#undef DBG_DTOR
#undef DBG_THREAD
#undef DBG_RAW
#endif

#ifdef NDEBUG

/* Empty command. Use it instead of leaving empty macros.
 * This should be optimized by most compilers by removing it automatically, while
 * keeping the "statement feel" of the macro.
 * In addition it removes tons of "ISO C99 requires rest arguments to be used"
 * compiler warnings.
 *
 * \warning Macros defined as empty are dangerous when used as a statement.
 *      For example:
 *          #define DBG_ERR()
 *          ...
 *          f1(x);
 *          if(c)
 *              f2(x)
 *          else
 *              DBG_ERR("Error");
 *          f3(x);
 *          f4(x);
 * Note that in the example, it will work as intended when DBG_ERR is defined:
 *  - c==true then order is f1(), f2(), f3().
 *  - c==false then order is f1(), DBG_ERR(), f3(), f4().
 * However, when it is left undefined, the code execution becomes:
 *  - c==true then order is f1(), f2(), f4() -- Note that f3 is NOT executed!!!
 *  - c==false then order is f1(), f3(), f4().
 *
 * Please take note that the statement is left without the semicolon at the end.
 */
#define DBG_EMPTY   do {} while(0)
#define DBG_TRACK(M, ...)       DBG_EMPTY
#define DBG_ERR(M, ...)         DBG_EMPTY
#define DBG_ERRNO(M, ...)       DBG_EMPTY
#define DBG_WARN(M, ...)        DBG_EMPTY
#define DBG_INFO(M, ...)        DBG_EMPTY
#define DBG_CHECK(A, M, ...)    DBG_EMPTY
#define DBG_CTOR()              DBG_EMPTY
#define DBG_DTOR()              DBG_EMPTY
#define DBG_THREAD(msg)         DBG_EMPTY
#define DBG_RAW(M, ...)         DBG_EMPTY

#else

#define DBG_TRACK(M, ...)   fprintf(stdout, "[TRACK ]%s%s" M "\n", SUBSYSTEM_NAME, DBG_SEPARATOR, ##__VA_ARGS__)

#define DBG_INFO(M, ...)    fprintf(stdout, "[INFO  ]%s%s" M "\n", SUBSYSTEM_NAME, DBG_SEPARATOR, ##__VA_ARGS__)

#define DBG_ERR(M, ...)     fprintf(stderr, "[ERROR ]%s%s" M " (%s)\n",SUBSYSTEM_NAME, DBG_SEPARATOR, ##__VA_ARGS__, __AT__)

#define DBG_ERRNO(M, ...)   fprintf(stderr, "[ERROR ]%s%s" M "(%s errno: %s)\n",SUBSYSTEM_NAME, DBG_SEPARATOR, ##__VA_ARGS__,  __AT__,  clean_errno())

#define DBG_WARN(M, ...)    fprintf(stderr, "[WARN  ]%s%s" M "\n", SUBSYSTEM_NAME, DBG_SEPARATOR, ##__VA_ARGS__ )

#define DBG_CHECK(A, M, ...)    if(!(A)) { DBG_ERR(M, ##__VA_ARGS__);}

#define DBG_RAW(M, ...)     fprintf(stdout, M, ##__VA_ARGS__)

/**
 * Output a message for constructor.
 */
#define DBG_CTOR()          fprintf(stdout, "[Ctor  ]%s%s \"%s\" @ %p\n",SUBSYSTEM_NAME, DBG_SEPARATOR, typeid(*this).name(), (void*)this)

/**
 * Output a message for destructor.
 */
#define DBG_DTOR()          fprintf(stdout, "[Dtor  ]%s%s \"%s\" @ %p\n",SUBSYSTEM_NAME, DBG_SEPARATOR, typeid(*this).name(), (void*)this)

/**
 * Output a message including current thread id and local time.
 */
#define DBG_THREAD(msg)\
                {\
                char s[1000];\
                time_t t = time(NULL);\
                struct tm * p = localtime(&t);\
                strftime(s, 1000, "%T", p);\
                cout<<"["<<s<<"][tid:0x"<<hex<<pthread_self()<<"] "<<msg<<endl;\
                }

#endif /* End of NDEBUG*/




/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

#endif /* dbg.h */

/*
 *********************** End of file ******************************************
 */
