/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: Table.h 4519 2014-03-10 16:46:32Z pueyos_a $
 *               $HeadURL: http://svn-general-svc.wlucynet.com:18080/svn/gemini_30_software/branches/NPG/G3MOD1_dnp3_CTS_LOW/Development/G3/RTU/MCM/common/BaseReuse/include/Table.h $
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_6517F8D3_C0BF_4817_A62D_705E8BE7BE94__INCLUDED_)
#define EA_6517F8D3_C0BF_4817_A62D_705E8BE7BE94__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stddef.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

template<class T> class Table
{

public:
    /**
     * Custom constructor: creates the table and initialises it like setTable()
     *
     * \param table Pointer to the table
     * \param size Table entries
     */
    Table(T* table, lu_uint32_t size) : tablePtr(table), entries(size) {};

    Table() : tablePtr(NULL), entries(0) {};

    virtual ~Table(){};

    /**
     * /brief Initialize the table
     *
     * Set the table pointer and the table size
     *
     * \param table Pointer to the table
     * \param size Table entries
     */
    inline void setTable(T* table, lu_uint32_t size)
    {
        tablePtr = table;
        entries  = size ;
    };

    /**
     * \brief Get a pointer to the table
     *
     * \return Table pointer
     */
    inline T* getTable() {return  tablePtr;};

    /**
     * \brief Get the number of entries of the table
     *
     * \return Table entries
     */
    inline lu_uint32_t getEntries(){return entries;};

    /**
     * \brief Get the size of the table
     *
     * ("entry size" *" number of entries")
     *
     * \return Table size
     */
    inline lu_uint32_t getSize(){return (entries * sizeof(T));};

    /**
     * \brief Overload square bracket operator
     */
    T operator[](lu_uint32_t n)
    {
        if(n < entries)
        {
            return tablePtr[n];
        }
        else
        {
            return NULL;
        }
    };

private:
    /**
     * Pointer to the table
     */
    T* tablePtr;

    /**
     * Number of entries
     */
    lu_uint32_t entries;
};


#endif // !defined(EA_6517F8D3_C0BF_4817_A62D_705E8BE7BE94__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
