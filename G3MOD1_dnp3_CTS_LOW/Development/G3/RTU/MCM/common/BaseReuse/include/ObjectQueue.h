/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ObjectQueue.h 14 Oct 2013 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/common/BaseReuse/include/ObjectQueue.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Thread-safe FIFO queue, good for processing event or job queues.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 14 Oct 2013 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14 Oct 2013   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef OBJECTQUEUE_H__INCLUDED
#define OBJECTQUEUE_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <memory>   //smart pointers
#include <string.h>
#include <errno.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Pipe.h"
#include "LockingMutex.h"
#include "Semaphore.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/**
 * \brief Codes for operation result.
 */
enum OBJECTQUEUERESULT
{
    OBJECTQUEUERESULT_OK = 0,       //successful operation
    OBJECTQUEUERESULT_TIMEOUT = 1,  //Operation timed out
    OBJECTQUEUERESULT_ERROR = -1    //Error (generic error)
};

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

template<class ObjectType> class ObjectQueue
{
public:
    ObjectQueue() :
//                    semOperation(LU_TRUE, 0),   //shared semaphore, acquired by default
                    accessMutex(LU_FALSE),
                    pipeFIFO(Pipe::PIPE_TYPE_BLOCKREAD)
    {
        /* Keep the object reference's size as it will be stored in the FIFO */
        objectRefSize = sizeof(ObjectType*);
//        semOperation.post();    //release semaphore to allow read/write operations
    };

    ~ObjectQueue()
    {
        //Acquire mutex as master to inhibit any read/write operation
        MasterLockingMutex wMutex(accessMutex, LU_TRUE);
        //empty pipe if needed
        ObjectRef objectRef;
        struct timeval timeout= {0, 250};
        while(pipeFIFO.readP(objectRef.address, objectRefSize, timeout) == 0)
        {
            ObjectType* ev = objectRef.object;
            delete ev;
        }
    }

    /**
     * \brief Add an object to the tail of the queue (FIFO)
     *
     * \param objPtr Pointer to the object to add. The ObjectQueue will take ownership until popped
     *
     * \return LU_TRUE if the object is added successfully to the queue
     */
    lu_bool_t push(std::auto_ptr<ObjectType> objectPtr)
    {
        MasterLockingMutex lMutex(accessMutex);    /* Entering Critical Area */
        ObjectRef objectRef;
        objectRef.object = objectPtr.release();
        return (pipeFIFO.writeP(objectRef.address, objectRefSize) == 0)? LU_TRUE : LU_FALSE;
    }

    /**
     * \brief Remove the first object (the oldest) from the queue.
     *
     * If the queue is empty it blocks waiting for an object to be added or
     * until the timeout expires
     *
     * \param timeout Time to wait for an object to be present in the queue.
     * \param result Stores here the result of the operation: success, timeout, or error when <0.
     *
     * \return objectPtr Retrieved object. NULL when no available object, or error.
     */
    std::auto_ptr<ObjectType> popLock(const struct timeval timeout, lu_int32_t &result)
    {
        ObjectRef objectRef;

        /* block the thread waiting for new data */
        result = pipeFIFO.readP(objectRef.address, objectRefSize, timeout);
        if(result == 0)
        {
            MasterLockingMutex lMutex(accessMutex);    /* Entering Critical Area */

            ObjectType* ev = objectRef.object;
            return std::auto_ptr<ObjectType>(ev);
        }
        return std::auto_ptr<ObjectType>(NULL);    //timed out or error returns NULL
    }

private:
    /**
     * \brief Structure to convert between object address and object reference.
     *
     * We wanted to store the object address in the Pipe, and then retrieve it
     * as an object.
     */
    union ObjectRef
    {
        ObjectType* object;                      //object reference
        lu_uint8_t address[sizeof(ObjectType*)]; //object address (to be stored)
    };

private:
    MasterMutex accessMutex;        //Mutex to lock update of the objects concurrently.
    Pipe pipeFIFO;              //FIFO where we are storing object references.
    lu_uint32_t objectRefSize;  //Size of the reference of the object to be stored.

};


#endif /* OBJECTQUEUE_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
