#include <embUnit/embUnit.h>
#include <stdio.h>
#include <stdlib.h>    // abs
#include <errno.h>
#include "Timer.h"
#include <string.h>    // strerror


static lu_int64_t elapse_time_us(timeval start, timeval end);
static void checkNoPendingSig(char*title,lu_uint32_t signalId);


#define SEC_2_USEC(a) (a * 1000000)

#define TIMER01_SEC    1
#define TIMER01_USEC   0
#define TIMER01_DURATION_US SEC_2_USEC(TIMER01_SEC) + TIMER01_USEC

#define TIMER02_SEC    3
#define TIMER02_USEC   0
#define TIMER02_DURATION_US SEC_2_USEC(TIMER02_SEC) + TIMER02_USEC

#define TIMER_MAX_ERROR_US 2000

#define TIMER_TESTS_NUM 5

static struct timeval startime;


class MyHandler:public ITimerHandler
{
public:
    MyHandler(lu_bool_t elapsetimecheck = LU_TRUE):elapsetimecheck(elapsetimecheck)
    {
        eventCount = 0;
    };

    virtual ~MyHandler(){};

    virtual void handleAlarmEvent(Timer &source)
    {
        checkNoPendingSig("timerHandler",source.getSignalId());

        eventCount ++;

        if(elapsetimecheck == LU_TRUE)
        {
            checkElapseTime();
        }
    };

    void checkElapseTime()
    {
        struct timeval endtime;
        lu_int64_t elapsetime;
        lu_int64_t diff;

        gettimeofday(&endtime, NULL);

        elapsetime = (SEC_2_USEC(endtime.tv_sec) + endtime.tv_usec) -
                   (SEC_2_USEC(startime.tv_sec) + startime.tv_usec);
        printf("Handler called. elapse time: %lli us \n",elapsetime);

        diff = abs(elapsetime - TIMER01_DURATION_US);
        if(diff > TIMER_MAX_ERROR_US)
            printf("Diff:%lli > %i\n",diff,TIMER_MAX_ERROR_US);

        TEST_ASSERT(diff <= TIMER_MAX_ERROR_US);
    }

    int getEventCount()
    {
        return eventCount;
    };

private:
    lu_int16_t eventCount;
    lu_bool_t elapsetimecheck;
};


static void setUp(void)
{
    startime.tv_sec = 0;
    startime.tv_usec = 0;
}

static void tearDown(void)
{
}


static void testElapsedTime(void)
{
    struct timeval tv1, tv2;
    lu_int64_t elapsetime;

    for(lu_uint32_t i = 0; i < TIMER_TESTS_NUM; ++i)
    {
        Timer timer01(TIMER01_SEC, TIMER01_USEC,NULL, Timer::TIMER_TYPE_ONESHOT);

        gettimeofday(&tv1, NULL);
        timer01.start();
        timer01.wait();
        gettimeofday(&tv2, NULL);
        timer01.stop();

        elapsetime = elapse_time_us(tv1,tv2);
        lu_int64_t diff = abs(elapsetime - TIMER01_DURATION_US);

        printf("Expected Diff:%lli <= %i\n",diff,TIMER_MAX_ERROR_US);
        TEST_ASSERT(diff <= TIMER_MAX_ERROR_US);
    }

}


static void testTimerHandler(void)
{
    PRINT_SEPERATOR;

    MyHandler handler;
    Timer timer(TIMER01_SEC, TIMER01_USEC, &handler, Timer::TIMER_TYPE_ONESHOT);

    TEST_ASSERT_EQUAL_INT(0,handler.getEventCount());

    timer.start();
    gettimeofday(&startime, NULL);

    sleep(3);//wait timer to go off.

    TEST_ASSERT_EQUAL_INT(1,handler.getEventCount());
    checkNoPendingSig("After testTimerHandler", timer.getSignalId());
}

static void testStop()
{
    PRINT_SEPERATOR;

    MyHandler handler;

    Timer timer(TIMER01_SEC, TIMER01_USEC, &handler, Timer::TIMER_TYPE_ONESHOT);
    timer.start();
    timer.stop();

    sleep(TIMER01_SEC + 2);

    // No event expected cause timer is stopped
    TEST_ASSERT_EQUAL_INT(0, handler.getEventCount());
}

static void testReset()
{
    PRINT_SEPERATOR;

    MyHandler handler;

    Timer timer(TIMER01_SEC, TIMER01_USEC, &handler, Timer::TIMER_TYPE_ONESHOT);
    timer.start();
    timer.reset();
    timer.reset();
    timer.reset();

    gettimeofday(&startime, NULL);

    sleep(3);
    // Only 1 event expected cause timer is reset before expires.
    TEST_ASSERT_EQUAL_INT(1, handler.getEventCount());
}

static void testRestart(void)
{
    PRINT_SEPERATOR;

    timeval tv1,tv2;
    lu_int64_t elapsetime;
    lu_int64_t diff;

    Timer timer(TIMER01_SEC, TIMER01_USEC, NULL, Timer::TIMER_TYPE_ONESHOT);



    /* Start timer using TIMER01*/
    timer.start();
    gettimeofday(&tv1, NULL);

    timer.wait();
    gettimeofday(&tv2, NULL);

    // Calc elapsed time
    elapsetime = elapse_time_us(tv1,tv2);
    printf("Elapsed timeA: %lli us\n",elapsetime);

    // Check diff
    diff = abs(elapsetime - TIMER01_DURATION_US);
    printf("Expected Diff:%lli <= %i\n",diff,TIMER_MAX_ERROR_US);
    TEST_ASSERT(diff <= TIMER_MAX_ERROR_US);




    /* Restart timer using TIMER02*/
    timespec ts02;
    ts02.tv_sec = TIMER02_SEC;
    ts02.tv_nsec = TIMER02_USEC;
    timer.start(ts02);
    gettimeofday(&tv1, NULL);

    timer.wait();
    gettimeofday(&tv2, NULL);

    // Calc elapsed time
    elapsetime = elapse_time_us(tv1,tv2);
    printf("Elapsed timeB: %lli us\n",elapsetime);

    // Check diff
    diff = abs(elapsetime - TIMER02_DURATION_US);
    printf("Expected Diff:%lli <= %i\n",diff,TIMER_MAX_ERROR_US);
    TEST_ASSERT(diff <= TIMER_MAX_ERROR_US);
}

static void testRestartBeforeExpire(void)
{
    PRINT_SEPERATOR;

    timeval tv1,tv2;
    lu_int64_t elapsetime;
    lu_int64_t diff;

    timespec ts02;
    ts02.tv_sec = TIMER02_SEC;
    ts02.tv_nsec = TIMER02_USEC;


    Timer timer(TIMER01_SEC, TIMER01_USEC, NULL, Timer::TIMER_TYPE_ONESHOT);

    // Start timer
    timer.start(ts02);
    gettimeofday(&tv1, NULL);

    // Restart time before timer goes off
    sleep(1);
    timer.start(ts02);
    gettimeofday(&tv1, NULL);

    timer.wait();
    gettimeofday(&tv2, NULL);

    // Calc elapsed time
    elapsetime = elapse_time_us(tv1,tv2);
    printf("Elapsed timeB: %lli us\n",elapsetime);

    // Check diff
    diff = abs(elapsetime - TIMER02_DURATION_US);
    printf("Expected Diff:%lli <= %i\n",diff,TIMER_MAX_ERROR_US);
    TEST_ASSERT(diff <= TIMER_MAX_ERROR_US);
}

static void testPeriodicTimer(void)
{
    PRINT_SEPERATOR;

    MyHandler handler(LU_FALSE);//Don't check elapse time since it is periodic
    Timer timer(TIMER01_SEC, TIMER01_USEC, &handler, Timer::TIMER_TYPE_PERIODIC);
    timer.start();

    const int periods = 5;
    sleep(TIMER01_SEC * periods);
    usleep(10000);

    //5 events expected since it is non periodic.
    TEST_ASSERT_EQUAL_INT(periods, handler.getEventCount());
}

static void testNonPeriodciTimer(void)
{
    PRINT_SEPERATOR;

    MyHandler handler(LU_FALSE);
    Timer timer(TIMER01_SEC, TIMER01_USEC, &handler, Timer::TIMER_TYPE_ONESHOT);
    timer.start();

    const int periods = 5;
    sleep(TIMER01_SEC * periods);
    //Only 1 event expected since it is non periodic.
    TEST_ASSERT_EQUAL_INT(1, handler.getEventCount());
}


/* Test Suite*/
TestRef TimerTest_tests(void)
{
	EMB_UNIT_TESTFIXTURES(fixtures) {
		new_TestFixture("testElapsedTime",testElapsedTime),
		new_TestFixture("testTimerHandler",testTimerHandler),
		new_TestFixture("testStop",testStop),
		new_TestFixture("testReset",testReset),
		new_TestFixture("testRestart",testRestart),
		new_TestFixture("testRestartBeforeExpire",testRestartBeforeExpire),
		new_TestFixture("testPeriodicTimer",testPeriodicTimer),
		new_TestFixture("testNonPeriodciTimer",testNonPeriodciTimer),
	};
	EMB_UNIT_TESTCALLER(TimerTest,"TimerTest",setUp,tearDown,fixtures);

	return (TestRef)&TimerTest;
}






/************************************/
/*	Help Methods                   */
/************************************/

static lu_int64_t elapse_time_us(timeval start, timeval end)
{
    return (SEC_2_USEC(end.tv_sec) + end.tv_usec) -
           (SEC_2_USEC(start.tv_sec) + start.tv_usec);
}

static void checkNoPendingSig(char*title,lu_uint32_t signalId)
{
   sigset_t sset;
   int x = sigpending(&sset);
   if( x< 0)
   {
       printf("[tid:0x%8x sig%i] fail to get pending signal! errno=%i (%s)\n",
                       pthread_self(),
                       signalId,
                       errno,
                       strerror(errno)
              );
       return;
   }

   if(sigismember(&sset, signalId) > 0)
   {
       printf("[tid:0x%8x sig%i] pending signal detected at %s\n",pthread_self(), signalId,title);
       TEST_FAIL("Unexpected signal detected!");
   }
}
