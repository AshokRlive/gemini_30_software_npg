/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 * 				$Id: c_template.c 1469 2012-06-20 20:51:28Z fryers_j $
 *              $HeadURL: http://10.11.0.6:81/svn/gemini_30_software/trunk/Development/G3/common/template/c_template.c $
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <iostream>
#include <signal.h>
#include <embUnit/embUnit.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Thread.h"
#include "dbg.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
using namespace std;

class TestThread: public Thread
{
public:
    TestThread(SCHED_TYPE schedType, lu_bool_t isJoinable)
                                : Thread(schedType, Thread::PRIO_MIN, isJoinable)
    {};
    TestThread() : Thread(SCHED_TYPE_FIFO, Thread::PRIO_MIN, LU_FALSE)
    {};

protected:
    virtual void threadBody()
    {
        cout << ("Enter thread body")<<endl;
        sleep(3);
        cout << ("Exit thread body")<<endl;
    }
};


static void setUp(void)
{

}

static void tearDown(void)
{
}

static void testThreadJoin(void)
{
    PRINT_SEPERATOR;

    THREAD_ERR ret ;

    TestThread test01;
    /*Joinable state is true*/
    if(test01.isJoinable() == LU_FALSE)
    {
        TEST_FAIL("is NOT Joinable ");
    }

    /*A thread cannot be joined before starting*/
    ret = test01.join();
    if(ret == THREAD_ERR_NONE)
    {
      TEST_FAIL("A thread cannot be joined before starting ");
    }

    /*Start*/
    ret = test01.start();
    if(ret != THREAD_ERR_NONE)
    {
        TEST_FAIL("Start failed ");
    }

    /*Join after start*/
    ret = test01.join();
    if(ret != THREAD_ERR_NONE)
    {
        TEST_FAIL("Join err");
    }
}

// Detached thread cannot be joined
static void testThreadJoin2(void)
{
    PRINT_SEPERATOR;

    TestThread test01(SCHED_TYPE_FIFO,LU_TRUE);//Detached thread

    if(test01.isJoinable() == LU_TRUE)
    {
        TEST_FAIL( "isJoinable should be false");
    }

    THREAD_ERR ret = test01.start();
    if(ret != THREAD_ERR_NONE)
    {
        TEST_FAIL("Start err");
    }

    ret = test01.join();
    if(ret == THREAD_ERR_NONE)
    {
        TEST_FAIL("Join err");
    }

}

static void testStopSelf(void)
{
    PRINT_SEPERATOR;

    class SelStopThread: public Thread
    {
    public:
        SelStopThread() : Thread(SCHED_TYPE_NORMAL, Thread::PRIO_MIN, LU_TRUE)
        {};

    protected:
        virtual void threadBody()
        {
            DBG_INFO("Enter thread body");
            delete this;
            sleep(1);//Cancellation point for terminate thread.
            TEST_FAIL("should not reach here since detached "
                "thread should be stopped immediately when it is deleted");
        }
    };


    SelStopThread* thread = new SelStopThread();
    thread->start();

    sleep(5);

}


/* Test Suite*/
TestRef ThreadTest_tests(void)
{
    EMB_UNIT_TESTFIXTURES(fixtures) {
        new_TestFixture("testThreadJoin",testThreadJoin),
        new_TestFixture("testThreadJoin2",testThreadJoin2),
        new_TestFixture("testStopSelf",testStopSelf),
    };
    EMB_UNIT_TESTCALLER(ThreadTest,"ThreadTest",setUp,tearDown,fixtures);

    return (TestRef)&ThreadTest;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
