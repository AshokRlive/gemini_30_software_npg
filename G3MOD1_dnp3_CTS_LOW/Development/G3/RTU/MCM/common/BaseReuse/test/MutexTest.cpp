/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 * 				$Id: c_template.c 1469 2012-06-20 20:51:28Z fryers_j $
 *              $HeadURL: http://10.11.0.6:81/svn/gemini_30_software/trunk/Development/G3/common/template/c_template.c $
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <iostream>
#include <signal.h>
#include <embUnit/embUnit.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Thread.h"
#include "dbg.h"
#include "LockingMutex.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
using namespace std;


static MasterMutex fixture;

static void setUp(void)
{

}

static void tearDown(void)
{
}

static void lockMutex()
{
    DBG_INFO("trying lock mutex again");
    MasterLockingMutex mMutex2(fixture, LU_TRUE);
    DBG_INFO("mutex locked again");
}

static void testReentrant(void)
{
    PRINT_SEPERATOR;

    DBG_INFO("trying lock mutex ");
    MasterLockingMutex mMutex(fixture,LU_TRUE);
    DBG_INFO("mutex locked");

    lockMutex();

}




/* Test Suite*/
TestRef MutexTest_tests(void)
{
    EMB_UNIT_TESTFIXTURES(fixtures) {
        new_TestFixture("testReentrant",testReentrant),
    };
    EMB_UNIT_TESTCALLER(MutexTest,"MutexTest",setUp,tearDown,fixtures);

    return (TestRef)&MutexTest;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
