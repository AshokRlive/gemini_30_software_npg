
#include <cstdio>
#include <unistd.h>
#include <sys/time.h>


#include <signal.h>

extern "C"{
#include <embUnit/embUnit.h>
#include <textui/TextOutputter.h>
#include <textui/TextUIRunner.h>
}


TestRef TimerTest_tests(void);
TestRef QueueTest_tests(void);
TestRef ThreadTest_tests(void);
TestRef ObjectQueueTest_tests(void);
TestRef MutexTest_tests(void);

int main()
{
#if 1
    /* Work-around for signal handling in linux implementation of POSIX
     * thread: block all signals
     */
    sigset_t signalMask;
    sigfillset (&signalMask);
    pthread_sigmask (SIG_BLOCK, &signalMask, NULL);

    // Unblock signal
    sigemptyset(&signalMask);
    sigaddset(&signalMask, SIGTERM);
    sigaddset(&signalMask, SIGINT);
    sigprocmask(SIG_UNBLOCK, &signalMask, NULL );


    TextUIRunner_setOutputter(TextOutputter_outputter());
    TextUIRunner_start();
        TextUIRunner_runTest(TimerTest_tests());
        TextUIRunner_runTest(QueueTest_tests());
        TextUIRunner_runTest(ThreadTest_tests());
        TextUIRunner_runTest(ObjectQueueTest_tests());
        TextUIRunner_runTest(MutexTest_tests());
    TextUIRunner_end();
#endif
    //test2();

	return 0;
}



