#include <embUnit/embUnit.h>
#include <stdio.h>

#include "Thread.h"
#include "QueueMgr.h"
#include "QueueObj.h"
#include "Table.h"

class testQ;
typedef QueueMgr<testQ> testQueue;
typedef QueueObj<testQ> testQueueObj;

class testQ : public testQueueObj
{
public:
    lu_uint32_t a;
};

/* Queue manager */
testQueue queue;

/* Queue objects */
#define TEST_Q_SET_SIZE 100
testQ testQSet1[TEST_Q_SET_SIZE];
testQ testQSet2[TEST_Q_SET_SIZE];


class addQueue: public Thread
{
public:
    addQueue(testQ* obj, lu_uint32_t burstLen, lu_uint32_t sleep) :
        Thread(SCHED_TYPE_NORMAL, 1, LU_FALSE),
        obj(obj),
        burstLen(burstLen),
        sleep(sleep){};

    ~addQueue() {};

protected:
    virtual void threadBody()
    {
        for(lu_uint32_t i = 0; i < TEST_Q_SET_SIZE; ++i)
        {
            //printf("Enqueue: %i\n", obj[i].a);
            queue.enqueue(&obj[i]);

            if ( (i %  burstLen) == 0)
            {
                usleep(sleep);
            }
        }

        return;
    }

private:
    testQ *obj;
    lu_uint32_t burstLen;
    lu_uint32_t sleep;
};



static void setUp(void)
{
}

static void tearDown(void)
{
}


/*Test add/remove objects from different threads*/
static void testAddRemove(void)
{
    addQueue addQueue01(testQSet1, 10, 1000);
    addQueue addQueue02(testQSet2, 20, 3000);
    testQ *testqPtr;

    /* Initialize test queue objects */
    for(lu_uint32_t i = 0; i < TEST_Q_SET_SIZE; ++i)
    {
        testQSet1[i].a = i;
        testQSet2[i].a = i + TEST_Q_SET_SIZE;
    }


    /* Start adding object to the queue manager */
    addQueue01.start();
    addQueue02.start();

    /* Dequeue objects */
    for(lu_uint32_t i = 0; i < TEST_Q_SET_SIZE*2; ++i)
    {
        //printf("Dequeue: wait\n");
        testqPtr = queue.dequeue();
        //printf("Dequeue: %i\n", testqPtr->a);
    }

    /* Wait for thread termination */
    addQueue01.join();
    addQueue02.join();

    /* The queue should be empty */
    TEST_ASSERT_EQUAL_INT(LU_TRUE, queue.isEmpty());

}

static void testFollowLinkedList(void)
{
    lu_uint32_t i;
    testQ *testqPtr;

    /* Initialize test queue objects and add to tthe queue*/
    for(i = 0; i < TEST_Q_SET_SIZE; ++i)
    {
        testQSet1[i].a = i;
        queue.enqueue(&testQSet1[i]);
    }

    /* Follow the link list */
    testqPtr = queue.getFirst();
    for(i = 0; i < TEST_Q_SET_SIZE; ++i)
    {
        if (testqPtr == NULL)
        {
            printf("List broken. NULL pointer\n");
            break;
        }

        if (testqPtr->a != i)
        {
            printf("List broken. Expected: %i - Found: %i\n", i, testqPtr->a);
            break;
        }

        testqPtr = queue.getNext(testqPtr);
    }

    /* The queue should be empty */
    TEST_ASSERT_EQUAL_INT(i, TEST_Q_SET_SIZE);

}

static void testRemoveObject(void)
{
    lu_uint32_t i;
    testQ *testqPtr;
    lu_uint32_t counter = 0;

    /* Initialize test queue objects and add to the queue*/
    for(i = 0; i < TEST_Q_SET_SIZE; ++i)
    {
        testQSet1[i].a = i;
        queue.enqueue(&testQSet1[i]);
    }

    /* Remove first obj */
    queue.dequeue(&testQSet1[0]);

    /* Remove last obj */
    queue.dequeue(&testQSet1[TEST_Q_SET_SIZE-1]);

    /* Remove middle obj */
    queue.dequeue(&testQSet1[50]);

    /* Remove obj not in the queue */
    queue.dequeue(&testQSet1[0]);

    printf("Queue val: ");
    testqPtr = queue.getFirst();
    while(testqPtr != NULL)
    {
        if( (testqPtr->a == 0) ||
            (testqPtr->a == TEST_Q_SET_SIZE-1) ||
            (testqPtr->a == 50)
          )
        {
            printf("\nError: unexpected value: %i\n", testqPtr->a);
            break;
        }

        printf("%i ", testqPtr->a);

        counter++;
        testqPtr = queue.getNext(testqPtr);
    }
    printf("\n");

    /* The queue should be empty */
    TEST_ASSERT_EQUAL_INT(counter, TEST_Q_SET_SIZE-3);

}


TestRef QueueTest_tests(void)
{
    EMB_UNIT_TESTFIXTURES(fixtures) {
        new_TestFixture("testAddRemove",testAddRemove),
        new_TestFixture("testFollowLinkedList",testFollowLinkedList),
        new_TestFixture("testRemoveObject",testRemoveObject),
    };
    EMB_UNIT_TESTCALLER(QueueTest,"QueueTest",setUp,tearDown,fixtures);

    return (TestRef)&QueueTest;
}
