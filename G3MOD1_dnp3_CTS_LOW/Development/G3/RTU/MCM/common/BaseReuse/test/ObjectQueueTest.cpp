/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ObjectQueueTest.cpp 14 Oct 2013 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/common/BaseReuse/test/ObjectQueueTest.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 14 Oct 2013 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14 Oct 2013   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <signal.h>
#include <iostream>
#include <string>
#include <embUnit/embUnit.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "Thread.h"
#include "ObjectQueue.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

using namespace std;

class TestEvent
{
public:
    TestEvent() {};
    virtual ~TestEvent() {};

    /**
     * \brief Method executed by the consumer
     */
    virtual void run() = 0;
};

class TestEvA : public TestEvent
{
private:
    std::string message;
    int id;

public:
    TestEvA(int objectID, std::string messageToPrint) :
        message(messageToPrint),
        id(objectID)
    {
        cout << "Created Test Event A" << id << endl;
    }
    virtual ~TestEvA() {};
    void run()
    {
        cout << "-->Test Event A" << id << ": " << message << endl;
        sleep(1);
        cout << "-->Test Event A" << id << " finished." << endl;
    }
};


class TestEvB : public TestEvent
{
private:
    int id;
    int value;

public:
    TestEvB(int objectID) :
        id(objectID),
        value(0)
    {
        cout << "Created Test Event B" << id << endl;
    }
    virtual ~TestEvB() {};
    void run()
    {
        cout << "-->Test Event B" << id << ": " << ++value << endl;
    }
};


class EventScheduler: public Thread
{
private:
    ObjectQueue<TestEvent> eventQ;

public:
    EventScheduler() : Thread(SCHED_TYPE_FIFO, Thread::PRIO_MIN, LU_TRUE, "EventScheduler")
    {};
    ~EventScheduler()
    {};

    lu_bool_t addEvent(TestEvent* eventPtr)
    {
        if(eventPtr == NULL)
        {
            cout << "Error adding: event is NULL!" << endl;
        }
        cout << "Adding event " << eventPtr << "|" << endl;
        std::auto_ptr<TestEvent> p(eventPtr);
        return eventQ.push(p);
    }

    void threadBody()
    {
        struct timeval timeout = {1, 0};

        cout << "Event scheduler running..." << endl;

        while(isRunning() == LU_TRUE)
        {
            lu_int32_t result;
            std::auto_ptr<TestEvent> p = eventQ.popLock(timeout, result);
            switch(result)
            {
                case 0:
                    if(p.get() != NULL)
                    {
                        cout << "Running dispatcher of event " << p.get() << "|" <<endl;
                        p.get()->run();
                        p.release();
                    }
                    else
                    {
                        cout << "event Scheduler timed out (or error)" << endl;
                    }
                    break;
                case 1:
                    cout << "thr(timed out)" << endl;
                    break;
                default:
                    cout << "event Scheduler error " << result << endl;
                    break;
            }
        }
        cout << "Event scheduler finished!" << endl;

    }
};

static void setUp(void)
{

}

static void tearDown(void)
{
}

static void testObjQueue(void)
{
    TestEvA* pA;
    TestEvB* pB;
    TestEvB* pBB;
    lu_bool_t ret;
    sigset_t signalMask;

    /* Work-around for signal handling in Linux implementation of POSIX
     * thread: block all signals
     */
    sigfillset (&signalMask);
    pthread_sigmask (SIG_BLOCK, &signalMask, NULL);

    /* Unblock some useful signals */
    sigemptyset(&signalMask);
    sigaddset(&signalMask, SIGINT);
    sigaddset(&signalMask, SIGTERM);
    pthread_sigmask (SIG_UNBLOCK, &signalMask, NULL);

    cout << "-----------------------" << endl;
    EventScheduler es;
    es.start();

    pA = new TestEvA(1, "Run this!");
    ret = es.addEvent(pA);
    cout << ((ret)? "OK" : "Fail") << endl;

    pB = new TestEvB(2);
    ret = es.addEvent(pB);
    cout << ((ret)? "OK" : "Fail") << endl;

    pBB = new TestEvB(3);
    ret = es.addEvent(pBB);
    cout << ((ret)? "OK" : "Fail") << endl;

    cout << "............" << endl;
    sleep(1);

    pB = new TestEvB(4);
    ret = es.addEvent(pB);
    cout << ((ret)? "OK" : "Fail") << endl;

    cout << "::::::::::::" << endl;
    sleep(2);

    pB = new TestEvB(5);
    ret = es.addEvent(pB);
    cout << ((ret)? "OK" : "Fail") << endl;

    pA = new TestEvA(6, "or else!");
    ret = es.addEvent(pA);
    cout << ((ret)? "OK" : "Fail") << endl;

    pB = new TestEvB(7);
    ret = es.addEvent(pB);
    cout << ((ret)? "OK" : "Fail") << endl;
    cout << endl;

    sleep(5);
    es.stop();

//    /*FORCE DELETION*/
//    pA = new TestEvA(6, "or else!");
//    ret = es.addEvent(pA);
//    cout << ((ret)? "OK" : "Fail") << endl;

    cout << "-----------------------" << endl;
    cout << "Thread Test finished!" << endl;
}


TestRef ObjectQueueTest_tests(void)
{
    EMB_UNIT_TESTFIXTURES(fixtures) {
        new_TestFixture("testObjQueue",testObjQueue),
    };
    EMB_UNIT_TESTCALLER(ObjectQueueTest,"ObjectQueueTest",setUp,tearDown,fixtures);

    return (TestRef)&ObjectQueueTest;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
