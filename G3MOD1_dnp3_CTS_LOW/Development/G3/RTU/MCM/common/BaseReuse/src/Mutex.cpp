/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief Basic components public implementation file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   31/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdio.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "Mutex.h"
#include "dbg.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */


Mutex::Mutex(lu_bool_t prioInherit)
{
    pthread_mutexattr_t mta;
    lu_int32_t ret;
    lu_int32_t protocol;

    pthread_mutexattr_init(&mta);

    (prioInherit == LU_TRUE) ? protocol = PTHREAD_PRIO_INHERIT:
                               protocol = PTHREAD_PRIO_NONE   ;
    ret = pthread_mutexattr_setprotocol(&mta, protocol);
    if (ret != 0)
    {
        //MG Error
    }

    /* Create a recursive mutex */
    pthread_mutexattr_settype(&mta, PTHREAD_MUTEX_RECURSIVE);

    if(pthread_mutex_init(&mutex, &mta) != 0)
    {
        DBG_ERRNO("Mutex creation error");
    }

    pthread_mutexattr_destroy(&mta);    //attributes are no longer required
}


Mutex::~Mutex()
{
    pthread_mutex_destroy(&mutex);
}


MasterMutex::MasterMutex(lu_bool_t shared)
{
    /* Set R/W Lock attributes */
    pthread_rwlockattr_t attr;
    pthread_rwlockattr_init(&attr);
    pthread_rwlockattr_setpshared(&attr, (shared == LU_TRUE)?
                                PTHREAD_PROCESS_SHARED :
                                PTHREAD_PROCESS_PRIVATE
                                );
    //When possible, give *real* preference for writers over readers:
    /* Note that the PTHREAD_RWLOCK_PREFER_WRITER_NP could "release" a reader's
     * lock when a writer is pending and the reader lock tries to re-acquire it,
     * so making it non-recursive allows the reader to keep the lock until
     * finished. The "PREFER_WRITER" will favour writers afterwards.
     * The recursion is done externally by ignoring the rwlock acquisition when
     * it returns EDEADLK -- if the thread already acquired the lock, there is
     * no need to re-acquire it.
     */
    pthread_rwlockattr_setkind_np(&attr, PTHREAD_RWLOCK_PREFER_WRITER_NONRECURSIVE_NP);

    //create R/W mutex lock: if it fails, rwMutex is invalid
    if(pthread_rwlock_init(&rwMutex, &attr) != 0)
    {
        DBG_ERRNO("MasterMutex creation error");
    }
    pthread_rwlockattr_destroy(&attr);    //attributes are no longer required
}


MasterMutex::~MasterMutex()
{
    pthread_rwlock_destroy(&rwMutex);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
