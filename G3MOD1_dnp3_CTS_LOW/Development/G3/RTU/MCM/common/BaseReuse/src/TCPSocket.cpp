/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:TCPSocket.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   21 Jul 2014     andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "TCPSocket.h"
#include "timeOperations.h"
#include "dbg.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
TCPSocket::TCPSocket() : socketFd(-1),
                         state(SOCK_STATE_UNKNOWN)
{
}

TCPSocket::~TCPSocket()
{

}

TCPSocket::SOCK_ERROR TCPSocket::init()
{
    struct sigaction sa;


    if (socketFd >= 0)
    {
        closeSocket();
    }

    if ((socketFd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    {
        state = SOCK_STATE_ERROR;
        return SOCK_ERROR_CONNECTION;
    }

    // Ignore SIGPIPE when writing on broken socket
    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = SIG_IGN;
    sigaction(SIGPIPE, &sa, (struct sigaction *) NULL);

    state = SOCK_STATE_INIT;
    return SOCK_ERROR_NONE;
}

TCPSocket::SOCK_ERROR TCPSocket::connectSocket(const lu_char_t *ipAddress, lu_int32_t port, lu_bool_t nonBlocking)
{
    struct addrinfo     *pAddrInfo = NULL;
    struct addrinfo     hints;
    lu_char_t           portName[10];
    struct sockaddr_in  localAddr;
    lu_int32_t          result;

    if ((ipAddress == NULL)  || (port <= 0)|| (port > 65535))
    {
        return SOCK_ERROR_CONFIG;
    }

    // Ensure the socket is in the correct state
    switch (state)
    {
        case SOCK_STATE_UNKNOWN:
        case SOCK_STATE_ERROR:
        case SOCK_STATE_CLOSED:
            if (init() != SOCK_ERROR_NONE)
            {
                return SOCK_ERROR_CONNECTION;
            }
            break;
        case SOCK_STATE_LISTENING :
        case SOCK_STATE_CONNECTING :
        case SOCK_STATE_CONNECTED  :
            return SOCK_ERROR_STATE;
        default:
            break;
    }

    memset(&hints, 0, sizeof(hints));
    hints.ai_flags = 0;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_family = AF_INET;
    hints.ai_addrlen = 0;
    hints.ai_addr = NULL;
    hints.ai_canonname = NULL;
    hints.ai_next = NULL;

    sprintf(portName, "%d", port);
    if (getaddrinfo(ipAddress, portName, &hints, &pAddrInfo) != 0)
    {
        if(pAddrInfo != NULL)
        {
            freeaddrinfo(pAddrInfo);
        }
        closeSocket();
        return SOCK_ERROR_CONFIG;
    }

    memset(&localAddr, 0, sizeof(localAddr));
    localAddr.sin_family = AF_INET;
    localAddr.sin_port = htons(0);
    localAddr.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(socketFd, (struct sockaddr *) &localAddr, sizeof(localAddr)) != 0)
    {
        freeaddrinfo(pAddrInfo);
        closeSocket();
        return SOCK_ERROR_CONNECTION;
    }

    // Set the non-blocking attributes of the socket
    if (nonBlocking)
    {
        setNonBlocking();
        setNoDelay();
    }

    if ((result = connect(socketFd, (struct sockaddr *) pAddrInfo->ai_addr, pAddrInfo->ai_addrlen)) != 0)
    {
        if ((errno != EWOULDBLOCK) && (errno != EINPROGRESS))
        {
            freeaddrinfo(pAddrInfo);
            closeSocket();
            state = SOCK_STATE_ERROR;
            return SOCK_ERROR_CONNECTION;
        }

        // The socket is non blocking and the connection is in progress
        freeaddrinfo(pAddrInfo);
        state = SOCK_STATE_CONNECTING;
        return SOCK_ERROR_NONE;
    }

    state = SOCK_STATE_CONNECTED;

    freeaddrinfo(pAddrInfo);
    return SOCK_ERROR_NONE;
}


TCPSocket::SOCK_ERROR TCPSocket::updateConnectedState(struct timeval timeout)
{
    fd_set              sockFds;
    lu_int32_t          result;
    lu_int32_t          error;
    socklen_t           length;


    if ((state != SOCK_STATE_CONNECTING) && (state != SOCK_STATE_CONNECTED))
    {
        return SOCK_ERROR_STATE;
    }

    /* Wait for connection to come up */
    FD_ZERO(&sockFds);
    FD_SET(socketFd, &sockFds);

    result = select(socketFd + 1, NULL, &sockFds, NULL, &timeout);
    switch (result)
    {
        case -1:
            closeSocket();
            return SOCK_ERROR_CONNECTION;
            break;
        case 0:
            // timeout
            return SOCK_ERROR_NONE;
            break;
    }

    // Check if our fd is writable (meaning it's probably connected)
    if (FD_ISSET(socketFd, &sockFds))
    {
        length = sizeof(error);
        result = getsockopt(socketFd, SOL_SOCKET, SO_ERROR, (void*) &error, &length);
        if (result != 0)
        {
            closeSocket();
            return SOCK_ERROR_CONNECTION;
        }

        switch(error)
        {
            case 0:
                state = SOCK_STATE_CONNECTED;
                break;

            case EHOSTUNREACH:
            case ECONNRESET:
                closeSocket();
                return SOCK_ERROR_CONNECTION;
                break;

            default:
                closeSocket();
                return SOCK_ERROR_CONNECTION;
                break;
        }
    }

    return SOCK_ERROR_NONE;
}

TCPSocket::SOCK_ERROR TCPSocket::listenSocket(const lu_char_t *ipAddress, lu_int32_t port)
{
    struct sockaddr_in  localAddr;
    SOCK_ERROR          ret;


    if ((ipAddress == NULL) || (port <= 0) || (port > 65535))
    {
        return SOCK_ERROR_CONFIG;
    }

    memset(&localAddr, 0, sizeof(localAddr));
    localAddr.sin_family = AF_INET;
    localAddr.sin_port   = htons(port);

    if (strcmp(ipAddress, "*.*.*.*") == 0)
    {
        localAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    }
    else if (strcmp(ipAddress, "127.0.0.1") == 0)
    {
        localAddr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    }
    else
    {
        localAddr.sin_addr.s_addr = inet_addr(ipAddress);
    }

    // Ensure the socket is in the correct state
    switch (state)
    {
        case SOCK_STATE_UNKNOWN:
        case SOCK_STATE_ERROR:
        case SOCK_STATE_CLOSED:
            if (init() != SOCK_ERROR_NONE)
            {
                return SOCK_ERROR_CONNECTION;
            }
            break;

        case SOCK_STATE_LISTENING :
        case SOCK_STATE_CONNECTING :
        case SOCK_STATE_CONNECTED  :
            return SOCK_ERROR_STATE;
        default:
            break;
    }

    if (bind(socketFd, (struct sockaddr *) &localAddr, sizeof(localAddr)) != 0)
    {
        closeSocket();
        return SOCK_ERROR_CONNECTION;
    }

    if (listen(socketFd, 1) != 0) // Single pending connection
    {
        closeSocket();
        return SOCK_ERROR_CONNECTION;
    }

    state = SOCK_STATE_LISTENING;

    ret = setNonBlocking();

    return ret;
}

TCPSocket::SOCK_ERROR TCPSocket::listenSocket(lu_int32_t port)
{
    return listenSocket("*.*.*.*", port);
}

TCPSocket::SOCK_ERROR TCPSocket::acceptSocket(TCPSocket *socket, lu_char_t *ipAddress)
{
    struct sockaddr_in  remoteAddr;
    socklen_t           remoteAddrLen;


    if (socket == NULL)
    {
        return SOCK_ERROR_CONFIG;
    }

    if (state != SOCK_STATE_LISTENING)
    {
        return SOCK_ERROR_STATE;
    }

    remoteAddrLen = sizeof(remoteAddr);

    socket->socketFd = accept(socketFd,(struct sockaddr *) &remoteAddr, &remoteAddrLen);
    if (socket->socketFd == -1)
    {
        if ((errno != EAGAIN) && (errno != EWOULDBLOCK))
        {
            socket->state = SOCK_STATE_ERROR;
            return SOCK_ERROR_CONNECTION;
        }

        // There's no pending connection
        return SOCK_ERROR_NONE;
    }

    if (ipAddress != NULL)
    {
        lu_char_t   a, b, c, d;
        lu_uint32_t temp = remoteAddr.sin_addr.s_addr;

        a = (temp >> 24);
        temp = temp << 8;
        b = (temp >> 24);
        temp = temp << 8;
        c = (temp >> 24);
        temp = temp << 8;
        d = (temp >> 24);

        // Return the ip address in the supplied buffer
        sprintf(ipAddress, "%d.%d.%d.%d", d, c, b, a);
    }

    if (setNonBlocking(socket->socketFd) != SOCK_ERROR_NONE)
    {
        socket->state = SOCK_STATE_ERROR;
        return SOCK_ERROR_CONNECTION;
    }

    if (setNoDelay(socket->socketFd) != SOCK_ERROR_NONE)
    {
        socket->state = SOCK_STATE_ERROR;
        return SOCK_ERROR_CONNECTION;
    }

    socket->state = SOCK_STATE_CONNECTED;

    return SOCK_ERROR_NONE;
}

TCPSocket::SOCK_ERROR TCPSocket::setReuseAddr(lu_int32_t sockFd)
{
    lu_int32_t argVal1 = 1;


    if (setsockopt(sockFd, SOL_SOCKET, SO_REUSEADDR, (lu_int32_t *) &argVal1, sizeof(argVal1)) != 0)
    {
        close(sockFd);
        return SOCK_ERROR_CONNECTION;
    }

    return SOCK_ERROR_NONE;
}

TCPSocket::SOCK_ERROR TCPSocket::setReuseAddr()
{
    SOCK_ERROR ret;

    if ((ret = setReuseAddr(socketFd)) != SOCK_ERROR_NONE)
    {
        closeSocket();
    }

    return ret;
}

TCPSocket::SOCK_ERROR TCPSocket::setNonBlocking(lu_int32_t sockFd)
{
    lu_int32_t flags;

    /* We have a new connection, set up socket parameters for TCP */
    flags = fcntl(sockFd, F_GETFL, 0);

    if (fcntl(sockFd, F_SETFL, flags | O_NONBLOCK) == -1)
    {
        close(sockFd);
        return SOCK_ERROR_CONNECTION;
    }

    return SOCK_ERROR_NONE;
}

TCPSocket::SOCK_ERROR TCPSocket::setNonBlocking()
{
    SOCK_ERROR ret;

    if ((ret = setNonBlocking(socketFd)) != SOCK_ERROR_NONE)
    {
        closeSocket();
    }

    return ret;
}

TCPSocket::SOCK_ERROR TCPSocket::setNoDelay(lu_int32_t sockFd)
{
    lu_int32_t cmd_arg = 1;


    if (setsockopt(sockFd, SOL_TCP, TCP_NODELAY, (char *) &cmd_arg, sizeof(cmd_arg)) != 0)
    {
        close(sockFd);
        return SOCK_ERROR_CONNECTION;
    }

    return SOCK_ERROR_NONE;
}

TCPSocket::SOCK_ERROR TCPSocket::setNoDelay()
{
    SOCK_ERROR ret;

    if ((ret = setNoDelay(socketFd)) != SOCK_ERROR_NONE)
    {
        closeSocket();
    }

    return ret;
}

TCPSocket::SOCK_ERROR TCPSocket::closeSocket()
{
    if (socketFd >= 0)
    {
        close(socketFd);
    }
    socketFd = -1;
    state = SOCK_STATE_CLOSED;

    return SOCK_ERROR_NONE;
}

TCPSocket::SOCK_ERROR TCPSocket::writeSocket(lu_uint8_t *buffer, lu_uint32_t bufferSize, lu_int32_t flags)
{
    if (buffer == NULL)
    {
        return SOCK_ERROR_CONFIG;
    }

    if (state != SOCK_STATE_CONNECTED)
    {
        return SOCK_ERROR_STATE;
    }

    if (socketFd != -1)
    {
        if (send(socketFd, buffer, bufferSize, flags) > 0)
        {
            return SOCK_ERROR_NONE;
        }
    }

    closeSocket();
    return SOCK_ERROR_CONNECTION;

}

TCPSocket::SOCK_ERROR TCPSocket::writeSocket(lu_uint8_t *buffer, lu_uint32_t bufferSize)
{
    return writeSocket(buffer, bufferSize, 0);
}

lu_int32_t TCPSocket::readSocket(lu_uint8_t *buffer, lu_uint32_t bufferSize, struct timeval *timeout, lu_int32_t flags)
{
    lu_int32_t ret = -1;
    lu_int32_t retSel;  //select() result
    struct timeval selTimeout;
    struct timeval prevTime;
    struct timeval endTime;


    if ((socketFd == -1) || (buffer == NULL) || (timeout == NULL))
    {
        return -1;
    }

    /* file descriptor sets */
    fd_set inFd;

    FD_ZERO(&inFd);
    FD_SET(socketFd, &inFd);

    /* select may change the value of timeout. Use a local copy */
    selTimeout = *timeout;

    clock_gettimeval(&prevTime);

    /* Wait for user input */
    retSel = select(socketFd + 1, &inFd, NULL, NULL, &selTimeout);
    if (retSel > 0)
    {
        if(FD_ISSET(socketFd, &inFd))
        {
            // Read the data on the socket
            ret = recv(socketFd, buffer, bufferSize, flags);

            // If the recv() gets 0 bytes the other end of the socket has gone
            // This should be treated as an error
            if (ret == 0)
            {
                ret = -1;
            }
        }
    }
    else
    {
        ret = 0;

        // On a non-blocking socket we may get EAGAIN from a select
        if ((errno != EAGAIN) && (errno != EWOULDBLOCK))
        {
            ret = retSel;
        }
    }

    clock_gettimeval(&endTime);

    // Store the length of time the select() took in selTimeout
    timeval_elapsed(&prevTime, &endTime, &selTimeout);

    // Reduce timeout by the length of time the select() took
    prevTime = *timeout;
    timeval_elapsed(&selTimeout, &prevTime, timeout);

    // If there's an error we should close the socket
    if (ret < 0)
    {
        closeSocket();
    }

    return ret;
}

lu_int32_t TCPSocket::readSocket(lu_uint8_t *buffer, lu_uint32_t bufferSize, struct timeval *timeout)
{
    return readSocket(buffer, bufferSize, timeout, 0);
}

lu_int32_t TCPSocket::readSocketMs(lu_uint8_t *buffer, lu_uint32_t bufferSize, lu_uint64_t timeoutMs)
{
    struct timeval tmval_timeout;

    tmval_timeout = ms_to_timeval(timeoutMs);

    return readSocket(buffer, bufferSize, &tmval_timeout, 0);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
