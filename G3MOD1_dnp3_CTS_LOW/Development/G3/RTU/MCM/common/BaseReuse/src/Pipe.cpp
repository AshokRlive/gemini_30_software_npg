/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "timeOperations.h"
#include "Pipe.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
Pipe::Pipe(Pipe::PIPE_TYPE pipeType)
{
    /* Create a pipe */
    if( pipe(fildes) < 0)
    {
        /* Error creating the pipe.
         * Set the FD to -1
         */
        fildes[PIPE_FD_READ] = fildes[PIPE_FD_WRITE] = -1;
    }
    else
    {
        switch(pipeType)
        {
            case PIPE_TYPE_NONBLOCKING:
                fcntl(fildes[PIPE_FD_READ], F_SETFL, O_NONBLOCK);
                fcntl(fildes[PIPE_FD_WRITE], F_SETFL, O_NONBLOCK);
                break;
            case PIPE_TYPE_BLOCKREAD:
                fcntl(fildes[PIPE_FD_WRITE], F_SETFL, O_NONBLOCK);
                break;
            case PIPE_TYPE_BLOCKWRITE:
                fcntl(fildes[PIPE_FD_READ], F_SETFL, O_NONBLOCK);
                break;
            case PIPE_TYPE_BLOCKING:
                //pipe is created as blocking, so nothing to do here.
                break;
            default:
                //invalid: do as if it was an error
                fildes[PIPE_FD_READ] = fildes[PIPE_FD_WRITE] = -1;
                break;
        }
    }
}


Pipe::~Pipe()
{
    if (fildes[PIPE_FD_READ] != -1)
    {
        close(fildes[PIPE_FD_READ]);
    }

    if (fildes[PIPE_FD_WRITE] != -1)
    {
        close(fildes[PIPE_FD_WRITE]);
    }
}



lu_int32_t Pipe::getFD(PIPE_FD type){

    if (type < PIPE_FD_LAST)
    {
        return  fildes[type];
    }

    return -1;
}


lu_int32_t Pipe::writeP(lu_uint8_t *buffer, lu_uint32_t bufferSize)
{
    lu_int32_t ret = -1;

    if (fildes[PIPE_FD_WRITE] != -1)
    {
        ret = write(fildes[PIPE_FD_WRITE], buffer, bufferSize);
    }
    if(ret > 0)
    {
        ret = 0;
    }

    return ret;
}


lu_int32_t Pipe::readP(lu_uint8_t *buffer, lu_uint32_t bufferSize)
{
    lu_int32_t ret = -1;

    if (fildes[PIPE_FD_READ] != -1)
    {
        ret = read(fildes[PIPE_FD_READ], buffer, bufferSize);
    }
    if(ret > 0)
    {
        ret = 0;
    }

    return ret;
}


lu_int32_t Pipe::readP(lu_uint8_t *buffer,
                        lu_uint32_t bufferSize,
                        const struct timeval timeout
                        )
{
    struct timeval timeoutCopy = timeout;

    return readP(buffer, bufferSize, &timeoutCopy);
}

lu_int32_t Pipe::readP(lu_uint8_t *buffer,
                        lu_uint32_t bufferSize,
                        struct timeval *timeout
                        )
{
    lu_int32_t ret = -1;
    lu_int32_t retSel;  //select() result
    struct timeval selTimeout;
    struct timeval prevTime;
    struct timeval endTime;


    if ((fildes[PIPE_FD_READ] == -1) || (buffer == NULL) || (timeout == NULL))
    {
        return -1;
    }

    /* file descriptor sets */
    fd_set inFd;

    FD_ZERO(&inFd);
    FD_SET(fildes[PIPE_FD_READ], &inFd);

    /* select may change the value of timeout. Use a local copy */
    selTimeout = *timeout;

    clock_gettimeval(&prevTime);

    /* Wait for user input */
    retSel = select(fildes[PIPE_FD_READ] + 1, &inFd, NULL, NULL, &selTimeout);
    if(retSel > 0)
    {
        if(FD_ISSET(fildes[PIPE_FD_READ], &inFd))
        {
            //available data at input Pipe: read it
            ret = read(fildes[PIPE_FD_READ], buffer, bufferSize);
            if(ret > 0)
            {
                ret = 0;    //read OK
            }
        }
    }
    else if(retSel == 0)
    {
        //timeout
        ret = 1;
    }
    else
    {
        //Error
        ret = retSel;
    }
    clock_gettimeval(&endTime);

    // Store the length of time the select() took in selTimeout
    timeval_elapsed(&prevTime, &endTime, &selTimeout);

    // Reduce timeout by the length of time the select() took
    prevTime = *timeout;
    timeval_elapsed(&selTimeout, &prevTime, timeout);

    return ret;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Functions
 ******************************************************************************
 */



/*
 *********************** End of file ******************************************
 */
