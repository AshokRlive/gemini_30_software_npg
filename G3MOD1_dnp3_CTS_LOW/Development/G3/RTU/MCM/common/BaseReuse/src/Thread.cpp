/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdio.h>
#include <cstring>
#include <sched.h>
#include <signal.h>
#include <cstddef>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "Thread.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

Thread::Thread(SCHED_TYPE scheduleType,
                lu_uint32_t thrPriority,
                lu_bool_t detached,
                const std::string name) :
                                 state(THREAD_STATE_NEW),
                                 threadID(0),
                                 schedType(scheduleType),
                                 priority(thrPriority),
                                 threadType((detached==LU_TRUE)? THREADTYPE_DETACHED : THREADTYPE_JOINABLE),
                                 threadName(name),
                                 locker()
{
    if (schedType >= SCHED_TYPE_LAST)
    {
        schedType = SCHED_TYPE_FIFO; //Default type
    }

    if ((priority > IProcess::PRIO_MAX) || (priority < IProcess::PRIO_MIN))
    {
        priority =  IProcess::PRIO_MIN; // Default prior
    }

}


THREAD_ERR Thread::start()
{
    LockingMutex lMutex(locker);

    /* FIXME: pueyos_a - [#3044] Review Thread behaviour */
	if(state != THREAD_STATE_NEW)
//    if(state != THREAD_STATE_NEW && state != THREAD_STATE_FINISHED)
    {
        return THREAD_ERR_INVALID_STATE;
    }

    pthread_attr_t     attr;
    struct sched_param schp;
    lu_int32_t PMax        ;
    lu_int32_t PMin        ;
    lu_int32_t posixPrio   ;
    THREAD_ERR ret = THREAD_ERR_NONE;
    int attrCreated = -1;       //result of attribute creation

    try
    {
        /* Initialise attribute */
        attrCreated = pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr, threadType);

        /* Set stack size */
        pthread_attr_setstacksize(&attr, stackSize);
#ifdef TARGET_BOARD_G3_SYSTEMPROC
        /*Cannot use PTHREAD_EXPLICIT_SCHED since it requires sudo permission or enough ulimit values*/
        pthread_attr_setinheritsched(&attr, PTHREAD_INHERIT_SCHED);
#else
        pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
#endif
        memset(&schp, 0, sizeof(schp));

        /* Find the POSIX priority */
        PMax = sched_get_priority_max(schedType);
        PMin = sched_get_priority_min(schedType);
        posixPrio = (lu_uint32_t)((float)(priority*(PMax-PMin))/IProcess::PRIO_MAX) + PMin;
        schp.sched_priority = posixPrio;

        /* Set scheduling policy */
        pthread_attr_setschedpolicy(&attr, schedType);
        pthread_attr_setschedparam(&attr, &schp);

        state = THREAD_STATE_READY;
        if(pthread_create( &threadID, &attr, &(Thread::start_routine), this) != 0)
        {
            ret = THREAD_ERR_CREATE_FAILED;
        }
    }
    catch(...)
    {
        fprintf(stderr, "Thread::start: [%s] error trying to create pthread\n", threadName.c_str());
        ret = THREAD_ERR_THROW_EXCEPTION;
    }

    if (ret != THREAD_ERR_NONE)
    {
        state = THREAD_STATE_FINISHED;
        threadID = 0;
    }

    if(attrCreated == 0)
    {
        pthread_attr_destroy(&attr);
    }

    return ret;

}

THREAD_ERR Thread::stop()
{
    THREAD_ERR ret = THREAD_ERR_NONE;
    LockingMutex lMutex(locker);

    /* FIXME: pueyos_a - [#3044] Review Thread behaviour */
    state = THREAD_STATE_INTERRUPTING;
//    state = (threadID != 0 )
//                    ? THREAD_STATE_INTERRUPTING
//                    : THREAD_STATE_FINISHED;

    if ((threadType == THREADTYPE_DETACHED) && (threadID != 0))
    {

        if(pthread_cancel(threadID) != 0)
            ret = THREAD_ERR_CANCEL_FAILED;
        state = THREAD_STATE_FINISHED;
        threadID = 0;
    }

    return ret;
}


THREAD_ERR Thread::join()
{
    LockingMutex lMutex(locker);

    if (state == THREAD_STATE_FINISHED || state == THREAD_STATE_NEW)
    {
        return THREAD_ERR_INVALID_STATE;
    }

    if (threadType != THREADTYPE_JOINABLE)
    {
        return THREAD_ERR_INVALID_STATE;    //A detached thread cannot be joined
    }

    if(threadID <= 0)
    {
        return THREAD_ERR_INVALID_STATE;
    }

    lu_int32_t status = 0;
    THREAD_ERR ret = THREAD_ERR_NONE;

    /* TODO: pueyos_a - Migrate to pthread_timedjoin_np() instead */
    if(pthread_join(threadID, (void**)&status) != 0)
    {
        ret = THREAD_ERR_JOIN_FAILED;
    }

    state = THREAD_STATE_FINISHED;
    threadID = 0;

    return ret;
}


SCHED_TYPE Thread::getSchedType()
{
    return schedType;
}


lu_uint32_t Thread::getPriority()
{
    return priority;
}


lu_bool_t      Thread::isInterrupting()
{
   return (state == THREAD_STATE_INTERRUPTING) ? LU_TRUE : LU_FALSE;
}

lu_bool_t      Thread::isRunning()
{
    return (state == THREAD_STATE_RUNNING) ? LU_TRUE : LU_FALSE;
}

lu_bool_t      Thread::isFinished()
{
    return (state == THREAD_STATE_FINISHED) ? LU_TRUE : LU_FALSE;
}

THREAD_STATE   Thread::getState()
{
    return state;

}

lu_bool_t Thread::isJoinable()
{
    return  (threadType == THREADTYPE_JOINABLE) ? LU_TRUE : LU_FALSE;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/**
 * Function executed by the Posix thread
 */
void* Thread::start_routine(void* pth)
{
    Thread* threadPtr = static_cast<Thread*>(pth);

    if (threadPtr != NULL)
    {
        sigset_t signalMask;


        /* Work-around for signal handling in linux implementation of POSIX
         * thread: block all signals
         */
        try
        {
            sigfillset (&signalMask);
            pthread_sigmask (SIG_BLOCK, &signalMask, NULL);
            pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED,NULL);
        }
        catch(...)
        {
            fprintf(stderr, "Thread::start_routine: [%s]Error trying to initialise thread\n",
                            threadPtr->threadName.c_str());

            threadPtr->state = THREAD_STATE_FINISHED;
            threadPtr->threadID = 0;    //thread is not running
            return NULL;
        }

        // Set the name of the Thread (view with ps -T)
        SETTHREADNAME((const char *)threadPtr->threadName.c_str());

        threadPtr->state = THREAD_STATE_RUNNING;
        threadPtr->threadBody();

        threadPtr->state = THREAD_STATE_FINISHED;
        threadPtr->threadID = 0;    //thread is not running
    }

    return NULL;
}



/*
 *********************** End of file ******************************************
 */





