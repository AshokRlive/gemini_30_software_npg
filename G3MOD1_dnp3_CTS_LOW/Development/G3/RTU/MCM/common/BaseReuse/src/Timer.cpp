/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <string.h>
#include <errno.h>
#include <stdio.h>          //use of perror()
#include <sys/resource.h>   //use of getrlimit()
#include <sstream>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Timer.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/* Convert uSec to nSec */
#define USEC_TO_NSEC(usec) (usec * 1000)

/* Use CLOCK_MONOTONIC_RAW when available */
#ifdef TARGET_BOARD_G3_SYSTEMPROC
#define CK_MONOTONIC CLOCK_MONOTONIC
#else
#ifdef CLOCK_MONOTONIC_RAW
#define CK_MONOTONIC CLOCK_MONOTONIC_RAW
#else
#define CK_MONOTONIC CLOCK_MONOTONIC
#endif
#endif

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
 struct signalMark
    {
    public:
        lu_int32_t signalID;        //Signal used for this timer
        lu_bool_t inUse;
    };

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/* static Timer Signal Pool implementation */
#define POOLSIZE 50
#define SIG_BEGIN SIGRTMIN
static Mutex siglock;           //
static lu_char_t sigInUse[POOLSIZE] = {};   //Initialised to 0 (not in use)

/*
 * \brief Get an available signal from the pool
 *
 * \return Signal number to use. < 0 when no signal available
 */
static lu_int32_t allocateSignal()
{
    LockingMutex mmutex(siglock);

    lu_int32_t maxsig = SIGRTMAX - SIG_BEGIN;

    for (lu_int32_t i = 0; (i < POOLSIZE) && (i < maxsig); ++i)
    {
        if(sigInUse[i] == 0)
        {
            sigInUse[i] = 1;
            return SIG_BEGIN + i;
        }
    }
    return -1;  //not found
}

/*
 * \brief Release a used signal to the pool
 *
 * \param Signal to release. Next time, this signal will be available.
 */
static void releaseSignal(lu_int32_t signalId)
{
    if( (signalId >= SIG_BEGIN)
        && (signalId < (SIG_BEGIN + POOLSIZE))
        && (signalId < SIGRTMAX)
        )
    {
        LockingMutex mmutex(siglock);
        sigInUse[signalId - SIG_BEGIN] = 0;
    }
}

/* Compose a Timer name from the ID provided */
static std::string getThreadName(lu_int32_t v)
{
    std::stringstream ss;
    ss << "TimerThread" << v;
    return ss.str();
}

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

static lu_uint32_t createdTimers = 0;   //Counts the amount of created Timers
                                        //Warning: It does not count timers created outside this object!

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
Timer::Timer(lu_uint32_t sec,
      lu_uint32_t usec,
      ITimerHandler* handler,
      Timer::TIMER_TYPE timerType,
      const lu_char_t* name
    )
{
    init(sec, usec, handler, timerType, name);
}

Timer::Timer(lu_uint32_t sec,
         lu_uint32_t usec,
         Timer::TIMER_TYPE timerType,
         const lu_char_t* name
        )
{
    init(sec, usec, NULL, timerType, name);
}


Timer::~Timer()
{
    if(signalId != -1)
    {
        timer_delete(timerId);  //Stop timer
        releaseSignal(signalId);
    }
    delete tThread;         //Stop and remove associated thread, if any
    tThread = NULL;
    --createdTimers;        //Decrease amount of created Timers


    /* USING POSIX TIMER HANDLERS */
    /* We leave this code here until timer signals with handlers are better
     * understood and proven to be trusted.
    // Uninstall handler
    if (tHandler != NULL)
    {
        //Unblock signal
        pthread_sigmask(SIG_BLOCK, &signalSet, NULL);
        sigaction(signalId, &timerOldAction, NULL);
    }
    */
}


lu_int32_t Timer::start()
{
    return start(timerValue.it_value);
}

lu_int32_t Timer::start(const struct timespec setTime)
{
    if(signalId < 0)
    {
        return INVALID_TIMER;
    }

    /* Start Timer with given time.
     * Note That this method does not stop/kill the associated thread, only
     * re-programs the Timer itself.
     */
    lu_int32_t flags = 0;
    struct itimerspec newTimerValue;
    newTimerValue.it_value = setTime;

    switch(type)
    {
        case TIMER_TYPE_ONESHOT:
            newTimerValue.it_interval.tv_sec = 0;
            newTimerValue.it_interval.tv_nsec = 0;
            break;
        case TIMER_TYPE_PERIODIC:
            newTimerValue.it_interval = setTime;
            break;
        case TIMER_TYPE_ONMINUTE:
        {
            lu_uint32_t secround = (60 - (setTime.tv_sec % 60)) % 60;
            newTimerValue.it_value.tv_sec = setTime.tv_sec + secround;
            newTimerValue.it_value.tv_nsec = 0;
            newTimerValue.it_interval.tv_sec = 60;
            newTimerValue.it_interval.tv_nsec = 0;
            flags = TIMER_ABSTIME;
            break;
        }
        default:
            return INVALID_TIMER;
    }

    return timer_settime( timerId, flags, &newTimerValue, NULL );
}


lu_int32_t Timer::wait()
{
    if(tHandler == NULL)
    {
        return _wait();
    }
    return 0;
}


lu_int32_t Timer::_wait()
{
    /* Wait on timer */
    siginfo_t siginfo;
    lu_int32_t ret = 0;

    if(signalId < 0)
    {
        return INVALID_TIMER;
    }

    /* When another signal interrupts the waiting procedure, it sets errno
     * to EINTR. Being not the wanted signal, we should wait again.
     * If the signal is missing (not pending), sigwaitinfo() will exit
     * with errno set to EAGAIN.
     */
    do
    {
        ret = sigwaitinfo(&signalSet, &siginfo);
    }
    while( (ret < 0) && (errno == EINTR) );

    return (ret > 0)? 0 : ret;
}


lu_int32_t Timer::stop()
{
    if(signalId < 0)
    {
        return INVALID_TIMER;
    }
    struct itimerspec stopTimer;

    stopTimer.it_interval.tv_sec  = 0;
    stopTimer.it_value.tv_sec     = 0;
    stopTimer.it_interval.tv_nsec = 0;
    stopTimer.it_value.tv_nsec    = 0;

    /* Stop Timer */
    return timer_settime( timerId , 0 , &stopTimer , NULL );
}


lu_int32_t Timer::reset()
{
    return start();
}


/**
 * The signal handler function with extended signature
 */
/* USING POSIX TIMER HANDLERS */
/* We leave this code here until timer signals with handlers are better
 * understood and proven to be trusted.
void Timer::alarmFunction(int sigNumb, siginfo_t *si, void *uc)
{
    LU_UNUSED(sigNumb);
    LU_UNUSED(uc);

    // get the pointer out of the siginfo structure and assign it to a new pointer variable
    Timer * ptrTimerClass =
                    reinterpret_cast<Timer *>(si->si_value.sival_ptr);
    // call the member function
    if(ptrTimerClass != NULL)
    {
        ptrTimerClass->memberAlarmFunction();
    }
}
*/


TimerThread::TimerThread(Timer& timerOwner,
                        ITimerHandler &timerHandler,
                        lu_int32_t signal,
                        std::string name) :
                Thread(SCHED_TYPE_FIFO, 25, LU_TRUE, name),
                timer(timerOwner),
                tmrHandler(timerHandler),
                signalID(signal)
{
}

TimerThread::~TimerThread()
{
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
void TimerThread::threadBody()
{
    /* Run at least once for any timer, repeat if it is a periodic timer */
    do
    {
        if(timer._wait() == 0)//Cancellation point
        {
            if(this->isInterrupting() == LU_TRUE)
            {
                break;
            }
            tmrHandler.handleAlarmEvent(timer);
        }
    }
    while(1);
}


/*
 ******************************************************************************
 * Private Functions
 ******************************************************************************
 */
void Timer::init(lu_uint32_t sec,
                lu_uint32_t usec,
                ITimerHandler* handler,
                Timer::TIMER_TYPE timerType,
                const lu_char_t* timerName
               )
{
    lu_int32_t ret;

    this->tHandler = handler;
    this->signalId = allocateSignal();
    this->tThread = NULL;
    this->type = timerType;
    this->name = (timerName == NULL)? getThreadName(signalId) : timerName;

    /* check limit of timers available */
    struct rlimit resourceLim;
    if (signalId == -1)
    {
        //No signal available: invalid timer
        perror("No timer signals available");
        return;
    }
    if(getrlimit(RLIMIT_SIGPENDING, &resourceLim) == 0)
    {
        if(createdTimers > resourceLim.rlim_cur)
        {
            //No timers available: invalid timer
            signalId = -1;
            perror("Maximum amount of timers reached");
            return;
        }
    }
    /* Initialise time event struct */
    timerEvent.sigev_notify          = SIGEV_SIGNAL  ;
    timerEvent.sigev_signo           = signalId      ;
    timerEvent.sigev_value.sival_ptr = (void*) this;

    /* Set up timer interval */
    timerValue.it_value.tv_sec     = sec               ;
    timerValue.it_value.tv_nsec    = USEC_TO_NSEC(usec);
    if(type == TIMER_TYPE_ONESHOT)
    {
        timerValue.it_interval.tv_sec  = 0      ;
        timerValue.it_interval.tv_nsec = 0;
    }
    else
    {
        timerValue.it_interval.tv_sec  = sec      ;
        timerValue.it_interval.tv_nsec = USEC_TO_NSEC(usec);
    }

    /* Initialise wait mask */
    sigemptyset(&signalSet)        ;
    sigaddset(&signalSet, signalId);

    /* Set up signal handler */

        /* USING POSIX TIMER HANDLERS */
        /* CAUTION: POSIX Timers have an issue when using handlers. Anytime a
         * handler is called, other functions (such as select, read, write, or
         * nanosleep) may be interrupted as well.
         * This is a problem in the current implementation since a read will
         * abort before finishing, thus reading, say 8 bytes instead of 10.
         * This non-deterministic behaviour is not convenient now, so the
         * implementation has changed to avoid this.
         * We leave this code here until timer signals with handlers are better
         * understood and proven to be trusted.
        if(handler != NULL)
        {
            struct sigaction timerAction;
            memset(&timerAction, 0, sizeof(timerAction));
            timerAction.sa_sigaction = Timer::alarmFunction;
            timerAction.sa_flags = SA_SIGINFO | SA_RESTART;
            ret = sigaction(signalId, &timerAction, &timerOldAction);

            // Unblock signal
            pthread_sigmask(SIG_UNBLOCK, &signalSet, NULL);
        }
     */

    if(handler != NULL)
    {
         /* create thread to wait over the timer */
         tThread = new TimerThread(*this, *tHandler, signalId, name);
         THREAD_ERR result;
         result = tThread->start();
         if (result != THREAD_ERR_NONE)
         {
             //No thread: invalid timer
             signalId = -1;
             perror("Fail to create timer thread");
             return;
         }
    }

    /* Create timer */
    clockid_t clockType = (type == TIMER_TYPE_ONMINUTE)? CLOCK_REALTIME : CK_MONOTONIC;
    ret = timer_create(clockType, &timerEvent, &timerId);
    if( ret < 0 )
    {
        signalId = -1;
        perror("Fail to create timer");
    }
    else
    {
        ++createdTimers;    //Increase amount of created Timers
    }
}


/* USING POSIX TIMER HANDLERS */
/* We leave this code here until timer signals with handlers are better
 * understood and proven to be trusted.
void Timer::memberAlarmFunction()
{
    if(tHandler != NULL)
    {
        tHandler->handleAlarmEvent(*this);
    }
}
*/

/*
 *********************** End of file ******************************************
 */
