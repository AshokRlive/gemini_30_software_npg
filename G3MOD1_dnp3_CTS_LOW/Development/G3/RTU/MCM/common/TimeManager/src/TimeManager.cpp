/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: TimeManager.cpp 14-Aug-2013 13:12:34 pueyos_a $
 *               $HeadURL: \\10.11.0.7\pueyos_a\workspace\app_refactor\G3\RTU\MCM\MCMApp\src\..\..\common\TimeManager\src\TimeManager.cpp $
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       TimeManager module implements the manager for handling clock time.
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 14-Aug-2013 13:12:34	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   14-Aug-2013 13:12:34  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <string>
#include <sstream>
#include <iomanip>
#include <stdio.h>      //use of perror()
#include <Logger.h>     //For Test subsystem only
#include <stdlib.h>     //use of system() for NTP & HW clock save

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "TimeManager.h"
#include "timeOperations.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#ifndef TIME_MANAGER_SIGNAL
#define TIME_MANAGER_SIGNAL         (SIGRTMIN + 16)
#endif

/* \brief Default synchronisation time out in milliseconds */
#define SYNCHTIMEOUT_DEFAULT     SEC_TO_MSEC(5 * 60 * 60)   //5*60*60 = 5 minutes

/* NTP script for synchronisation check */
#define NTP_CHECK_SCRIPT "scripts/ntp_test.sh "

/* Day and Month printing Macros */
#define DAYPRINT(d) std::setfill ('0') << std::setw(2) << d
#define MONTHPRINT(m) std::setfill ('0') << std::setw(2) << m

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static void printDate(std::stringstream& stream, TimeManager::TimeStr::DATEFORMAT const dateFormat, const lu_uint32_t day, const lu_uint32_t mon, const lu_uint32_t year)
{
    switch (dateFormat)
    {
        case TimeManager::TimeStr::DATEFORMAT_MIDDLEENDIAN:
            stream << MONTHPRINT(mon) << "/" <<
                      DAYPRINT(day)  << "/" <<
                      year;
            break;
        case TimeManager::TimeStr::DATEFORMAT_BIGENDIAN:
            stream << year <<
                      MONTHPRINT(mon) << "/" <<
                      DAYPRINT(day)  << "/";
            break;
        case TimeManager::TimeStr::DATEFORMAT_LITTLEENDIAN:
        default:
            stream << DAYPRINT(day)  << "/" <<
                      MONTHPRINT(mon) << "/" <<
                      year;
            break;
    }
}

static void printTime(std::stringstream& stream, const lu_uint32_t hour, const lu_uint32_t min, const lu_uint32_t sec)
{
    stream << std::setfill ('0') << std::setw(2) << hour << ":" <<
              std::setfill ('0') << std::setw(2) << min  << ":" <<
              std::setfill ('0') << std::setw(2) << sec;
}

static void printTime_ms(std::stringstream& stream, const lu_uint32_t hour, const lu_uint32_t min, const lu_uint32_t sec, const lu_uint32_t msec)
{
    printTime(stream, hour, min, sec);
    stream << "." << std::setw(3) << msec;  //add milliseconds
}

static void printSync(std::stringstream& stream, const lu_bool_t isSynch)
{
    stream << " (" << ((isSynch == LU_TRUE)? "Sync" : "NotSync") << ")";   //add synch indication
}

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
TimeManager::TimeStr::TimeStr() : //Set default values
                      time(TIMESPEC_ZERO),
                      relatime(TIMESPEC_ZERO),
                      synchronised(false),
                      isUTC(true),
                      neverSynchronised(true),
                      badTime(true)
{}


bool TimeManager::TimeStr::operator==(const TimeStr a)
{
    return( (timespec_compare(&(this->time), &(a.time)) == LU_TRUE) &&
            (timespec_compare(&(this->relatime), &(a.relatime)) == LU_TRUE) &&
            (this->synchronised == a.synchronised)
          );
}


bool TimeManager::TimeStr::operator!=(const TimeStr a)
{
    return !(*this == a);
}


lu_uint64_t TimeManager::TimeStr::elapsed_ms(const TimeStr endTime)
{
    return timespec_elapsed_ms(&(this->relatime), &(endTime.relatime));
}


std::string TimeManager::TimeStr::toString(const bool withSync,
                                           const DATEFORMAT dateFormat) const
{
    struct tm tempTm;
    std::stringstream result;

    /* Convert seconds to a broken-down like-UTC time  */
    gmtime_r(&(time.tv_sec), &tempTm);

    /* Convert the broken-down like-UTC time plus milliseconds to an ASCII string */
    printDate(result, dateFormat, tempTm.tm_mday, tempTm.tm_mon+1, tempTm.tm_year + 1900);
    result << " ";
    printTime_ms(result, tempTm.tm_hour, tempTm.tm_min, tempTm.tm_sec, NSEC_TO_MSEC(time.tv_nsec));
    if(withSync)
    {
        printSync(result, synchronised);
    }
    return result.str();
}


std::string TimeManager::TimeStr::dateString(const bool withSync,
                                             const DATEFORMAT dateFormat) const
{
    struct tm tempTm;
    std::stringstream result;

    /* Convert seconds to a broken-down like-UTC time  */
    gmtime_r(&(time.tv_sec), &tempTm);
    printDate(result, dateFormat, tempTm.tm_mday, tempTm.tm_mon+1, tempTm.tm_year + 1900);
    if(withSync)
    {
        printSync(result, synchronised);
    }
    return result.str();
}


std::string TimeManager::TimeStr::timeString(const bool withSync) const
{
    struct tm tempTm;
    std::stringstream result;

    /* Convert seconds to a broken-down like-UTC time  */
    gmtime_r(&(time.tv_sec), &tempTm);
    printTime(result, tempTm.tm_hour, tempTm.tm_min, tempTm.tm_sec);
    if(withSync)
    {
        printSync(result, synchronised);
    }
    return result.str();
}


std::string TimeManager::TimeStr::timeString_ms(const bool withSync) const
{
    struct tm tempTm;
    std::stringstream result;

    /* Convert seconds to a broken-down like-UTC time  */
    gmtime_r(&(time.tv_sec), &tempTm);
    printTime_ms(result, tempTm.tm_hour, tempTm.tm_min, tempTm.tm_sec, NSEC_TO_MSEC(time.tv_nsec));
    if(withSync)
    {
        printSync(result, synchronised);
    }
    return result.str();
}


void TimeManager::configure(const lu_uint64_t syncTimeout_ms)
{
    m_synchTimeout = syncTimeout_ms;
}


TimeManager::TERROR TimeManager::setTime(struct tm datetime,
                                        const lu_uint32_t& mseconds,
                                        const lu_bool_t& synch,
                                        const SYNC_SOURCE& source,
                                        const lu_bool_t& asUTC)
{
    struct timespec daytime;
    daytime.tv_nsec = MSEC_TO_NSEC(mseconds);
    bool useUTC = (asUTC == LU_TRUE);
    if(!useUTC)
    {
        useUTC = m_timezoneUTC;
    }
    if(useUTC)
    {
        //Note that timegm() is GNU-only but mktime() assumes tm in local time and not in UTC
        daytime.tv_sec = timegm(&datetime); //Assume given time is in UTC
    }
    else
    {
        daytime.tv_sec = mktime(&datetime); //Assume given time is in local time
    }
    if(daytime.tv_sec == -1)
    {
        return TERROR_SYNC; //timegm() or mktime() error
    }
    return setTime(daytime, synch, source);
}

TimeManager::TERROR TimeManager::setTime(const struct timespec& datetime,
                                        const lu_bool_t& synch,
                                        const SYNC_SOURCE& source)
{
    TERROR ret = TERROR_SYNC;   //Time NOT set yet

    /* Check validity of parameters */
    if(datetime.tv_sec < 0)
    {
        Logger::getLogger(SUBSYSTEM_ID_MAIN).error("Time Manager: Tried to set invalid time");
        return TERROR_OTHER;
    }

    /* Check for priority of the time synch source */
    if(canSetTime(source))
    {
        int res;
        m_onMinuteTimer->stop();
        {//LockingMutex environment
            MasterLockingMutex lMutex(m_clockMutex, LU_TRUE); //master mutex acquisition
            res = clock_settime(CLOCK_REALTIME, &datetime);       //set System date
        } //Note: mutex is destroyed here, so it's "safe" to Log or other longer operations

        if(res < 0)
        {
            int resError = errno;
            std::ostringstream ss;
            ss << "Unable to set system clock! [" << resError << "]: " << strerror(resError);
            perror(ss.str().c_str());
            Logger::getLogger(SUBSYSTEM_ID_TEST).error(ss.str().c_str());
            return TERROR_OTHER;
        }
        /* remember data of last synchronisation */
        storeSynchTime(source);

        ret = setSync(synch==LU_TRUE);
        
        /* Re-enable on-the-minute synchronisation */
        if(m_onMinuteTimer->start(datetime) < 0)
        {
            perror("Error: Unable to use timer for on-the-minute notifications");
            ret = TERROR_TIMER;
        }

        if( (Logger::getLogger(SUBSYSTEM_ID_TEST).getLogLevel() == LOG_LEVEL_INFO) ||
            (Logger::getLogger(SUBSYSTEM_ID_TEST).getLogLevel() == LOG_LEVEL_DEBUG) )
        {
            Logger::getLogger(SUBSYSTEM_ID_TEST).info("RTU Time set at time mark %lu.%09lu "
                                            "to %s "
                                            "from %s",
                                            m_synchTime.relatime.tv_sec, m_synchTime.relatime.tv_nsec,
                                            now().toString(true).c_str(),
                                            SYNC_SOURCE_ToSTRING(source));
        }
    }
    /* Note: Do not report date change here, use an observer to be informed by inSynchTimer handler */
    return ret;
}


void TimeManager::getTime(TimeManager::TimeStr& datetime, const bool inUTC)
{
    /* WARNING! The absolute and relative times aren't guaranteed to be
     * equivalent to the exact same time stamp, since any thread could be
     * running its time slice between the two consecutive clock_gettime()
     * calls and add a delay. However, this may not be much in practice.
     * Since this is being used in a non-RT OS, there is no way to ensure it is
     * atomically executed.
     * A semaphore/mutex/lock will not do the trick since it does not ensure the
     * atomicity of this code against other processes such as the OS itself.
     */
    MasterLockingMutex lMutex(m_clockMutex);  //non-master mutex acquisition
    /* Get system time from RTC clock */
    clock_gettime(CLOCK_REALTIME, &(datetime.time));        //System UTC time
    clock_gettime(MCMCLOCKELAPSED, &(datetime.relatime));   //relative time
    if(m_timezoneUTC || inUTC)
    {
        datetime.isUTC = true;
    }
    else
    {
        //RTU uses local time: Convert System UTC to local time
        datetime = TimeToLocal(datetime);
        datetime.isUTC = false;
    }
    datetime.synchronised = m_synchTime.synchronised;
    datetime.neverSynchronised = m_neverSynch;
    datetime.badTime = (datetime.time.tv_sec < TimeManager::TimeStr::GOODTIMES);
}

void TimeManager::getTime(TimeManager::TimeStr& datetime, SYNC_SOURCE& source, const bool inUTC)
{
    getTime(datetime, inUTC);
    source = m_lastSource;
}

void TimeManager::getTime(struct timespec *datetime,
                            lu_bool_t *synch,
                            SYNC_SOURCE *source,
                            const bool inUTC)
{
    TimeManager::TimeStr dateTimeRel;
    getTime(dateTimeRel, inUTC);
    if(datetime != NULL)
    {
        *datetime = dateTimeRel.time;
    }
    if(synch != NULL)
    {
        *synch = m_synchTime.synchronised;
    }
    if(source != NULL)
    {
        *source = m_lastSource;
    }
}

void TimeManager::getTime(struct tm* datetime,
                            lu_bool_t* synch,
                            SYNC_SOURCE* source,
                            const bool inUTC)
{
    struct timespec timestamp;
    getTime(&timestamp, synch, source, inUTC);
    TimeToStructtm(timestamp, datetime);
}


const TimeManager::TimeStr TimeManager::now()
{
    TimeManager::TimeStr timeNow;
    getTime(timeNow);
    return timeNow;
}


const TimeManager::TimeStr& TimeManager::getInitialTime()
{
    return m_initialTime;
}


void TimeManager::setTimezoneUTC(bool timezoneUTC)
{
    m_timezoneUTC = timezoneUTC;
}

bool TimeManager::isTimezoneUTC() const
{
    return m_timezoneUTC;
}


void TimeManager::setTimezoneName(const std::string localFileName)
{
    m_POSIXtzFileName = localFileName;
    std::size_t pos;
    //Remove "Etc/" from POSIX name
    pos = m_POSIXtzFileName.find("Etc/");
    if(pos != std::string::npos)
    {
        m_timezoneName = m_POSIXtzFileName.substr(pos+4);
    }
    else
    {
        m_timezoneName = m_POSIXtzFileName;
    }
    /* Convert timezone naming POSIX convention "GMT-4" to ISO-8601 "GMT+4" and
     * GMT+8 to GMT-8 */
    pos = m_timezoneName.find("-");
    if(pos != std::string::npos)
    {
        m_timezoneName.replace(pos, 1, "+");
    }
    else
    {
        //Char '-' not found, try '+'
        pos = m_timezoneName.find("+");
        if(pos != std::string::npos)
        {
            m_timezoneName.replace(pos, 1, "-");
        }
    }
}

const char* TimeManager::getTimezoneName()
{
    if(m_POSIXtzFileName.empty())
    {
        return ( (m_timezoneUTC)? "UTC" : "local" );
    }
    return m_timezoneName.c_str();
}

const char* TimeManager::getPOSIXTimezoneName()
{
    if(m_POSIXtzFileName.empty())
    {
        return ( (m_timezoneUTC)? "UTC" : "local" );
    }
    return m_POSIXtzFileName.c_str();
}


void TimeManager::setSynchTimeoutTime(const struct timespec timeoutSynch)
{
    m_synchTimeout = timespec_to_ms(&timeoutSynch);
}

void TimeManager::setSynchTimeoutTime(const lu_uint64_t timeoutSynch)
{
    m_synchTimeout = timeoutSynch;
}


lu_bool_t TimeManager::getSyncStatus()
{
    return m_synchTime.synchronised;
}

lu_bool_t TimeManager::getSyncStatus(TimeManager::TimeStr& synchdatetime)
{
    synchdatetime = m_synchTime;
    return m_synchTime.synchronised;
}

lu_bool_t TimeManager::getSyncStatus(SYNC_SOURCE *source)
{
    if(source != NULL)
    {
        *source = m_lastSource;
    }
    return m_synchTime.synchronised;
}

lu_bool_t TimeManager::getSyncStatus(SYNC_SOURCE *source, struct timespec *syncTime)
{
    if(source != NULL)
    {
        *source = m_lastSource;
    }
    if(syncTime != NULL)
    {
        *syncTime = m_synchTime.time;
    }
    return m_synchTime.synchronised;
}

lu_bool_t TimeManager::getSyncStatus(SYNC_SOURCE *source, lu_uint64_t *syncTime)
{
    struct timespec synchroTime;
    getSyncStatus(source, &synchroTime);
    if(syncTime != NULL)
    {
        TimeStr currentTime;
        getTime(currentTime);
        *syncTime =  timespec_elapsed_ms(&(m_synchTime.relatime), &(currentTime.relatime));
    }
    return m_synchTime.synchronised;
}


lu_bool_t TimeManager::TimeToStructtm(const time_t datetime_sec, struct tm* result)
{
    /* Get UTC time */
    return (gmtime_r(&(datetime_sec), result) == NULL) ? LU_FALSE : LU_TRUE;
}

lu_bool_t TimeManager::TimeToStructtm(const struct timespec& datetime, struct tm* result)
{
    /* Get UTC time */
    return (gmtime_r(&(datetime.tv_sec), result) == NULL) ? LU_FALSE : LU_TRUE;
}


struct timespec TimeManager::structtmtoTimespec(struct tm& datetime_tm, const long int nsecs)
{
    struct timespec result;
    result.tv_nsec = nsecs;
    if(m_timezoneUTC)
    {
        result.tv_sec = timegm(&datetime_tm);   //Note that timegm() is GNU-only
    }
    else
    {
        result.tv_sec = mktime(&datetime_tm);   //mktime() assumes tm in local time and not in UTC
    }
    return result;
}


const TimeManager::TimeStr TimeManager::localUTC(const TimeManager::TimeStr& datetime, const bool toUTC)
{
    struct tm tempTime;
    TimeManager::TimeStr result = datetime;
    if(toUTC)
    {
        gmtime_r(&(datetime.time.tv_sec), &tempTime);       //Get UTC time
    }
    else
    {
        localtime_r(&(datetime.time.tv_sec), &tempTime);    //Get local time
    }
    //temptime is now in UTC/localtime: use plain conversion to get it back
    result.time.tv_sec = timegm(&tempTime);
    return result;
}

const TimeManager::TimeStr TimeManager::TimeToLocal(const TimeManager::TimeStr& datetime)
{
    return localUTC(datetime, false);
}

const TimeManager::TimeStr TimeManager::TimeToUTC(const TimeManager::TimeStr& datetime)
{
    return localUTC(datetime, true);
}


void TimeManager::setAppStartTime(const struct timespec startTime)
{
    //override application start time
    appStartTime = startTime;
}


struct timespec TimeManager::getAppRunningTime()
{
    struct timespec result;
    struct timespec appCurrentTime;
    clock_gettime(MCMCLOCKELAPSED, &appCurrentTime);
    timespec_elapsed(&appStartTime, &appCurrentTime, &result);
    return result;
}


lu_uint64_t TimeManager::getAppRunningTime_ms()
{
    struct timespec appCurrentTime;
    clock_gettime(MCMCLOCKELAPSED, &appCurrentTime);
    return timespec_elapsed_ms(&appStartTime, &appCurrentTime);
}


lu_int32_t TimeManager::attachSynch(TimeObserver* observer)
{
    if(observer == NULL)
    {
        return -1;
    }

    // Protect the observer queue
    MasterLockingMutex mMutex(m_synchQueueMutex, LU_TRUE);

    /* Add the observer to the observer list */
    if(m_synchObservers.enqueue(observer) == LU_TRUE)
    {
        /* After attaching an observer, update it */
        TimeStr timestamp;
        getTime(timestamp);

        /* TODO: pueyos_a - lock clockMutex here? Or use another lock? */
        // Protect the data to ensure all observers update with same data
//        LockingMutex lMutex(mutex);
        //MasterLockingMutex lMutex(clockMutex);  //non-master mutex acquisition
        observer->updateTime(timestamp);
        return 0;
    }
    return -1;
}


void TimeManager::detachSynch(TimeObserver* observer)
{
    if(observer == NULL)
    {
        return;
    }

    // Protect the observer queue
    MasterLockingMutex mMutex(m_synchQueueMutex, LU_TRUE);

    /* Remove observer */
    m_synchObservers.dequeue(observer);
}


lu_int32_t TimeManager::attachMinute(TimeObserver* observer)
{
    if(observer == NULL)
    {
        return -1;
    }

    // Protect the observer queue
    MasterLockingMutex mMutex(m_minuteQueueMutex, LU_TRUE);

    /* Add the observer to the observer list */
    if(m_minuteObservers.enqueue(observer) == LU_TRUE)
    {
        /* After attaching an observer, update it */
        TimeStr timestamp;
        getTime(timestamp);

        /* TODO: pueyos_a - lock clockMutex here? Or use another lock? */
        // Protect the data to ensure all observers update with same data
//        LockingMutex lMutex(mutex);
        //MasterLockingMutex lMutex(clockMutex);  //non-master mutex acquisition
        observer->updateTime(timestamp);
        return 0;
    }
    return -1;
}


void TimeManager::detachMinute(TimeObserver* observer)
{
    if(observer == NULL)
    {
        return;
    }

    // Protect the observer queue
    MasterLockingMutex mMutex(m_minuteQueueMutex, LU_TRUE);

    /* Remove observer */
    m_minuteObservers.dequeue(observer);
}


lu_int32_t TimeManager::attachFirstSynch(TimeObserver* observer)
{
    if(observer == NULL)
    {
        return -1;
    }

    // Protect the observer queue
    MasterLockingMutex mMutex(m_1stSynchQueueMutex, LU_TRUE);

    /* Add the observer to the observer list */
    if(m_1stSynchObservers.enqueue(observer) == LU_TRUE)
    {
        /* After attaching an observer, update it if time was already synchronised */
        if(!m_neverSynch)
        {
            TimeStr timestamp;
            getTime(timestamp);
            observer->updateTime(timestamp);
        }
        return 0;
    }
    return -1;
}


void TimeManager::detachFirstSynch(TimeObserver* observer)
{
    if(observer == NULL)
    {
        return;
    }

    // Protect the observer queue
    MasterLockingMutex mMutex(m_1stSynchQueueMutex, LU_TRUE);

    /* Remove observer */
    m_1stSynchObservers.dequeue(observer);
}


void TimeManager::addSyncSource(const SYNC_SOURCE source)
{
    if(source == SYNC_SOURCE_NTP)
    {
        synchFromNTP(); //Force 1st update from NTP
        /* Start NTP synchronisation check timer */
        if(m_NTPSynchTimer->start() < 0)
        {
            perror("Error: Unable to start timer for NTP synchronisation check");
        }
    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
TimeManager::TimeManager() :
                m_clockMutex(LU_TRUE),
                m_timezoneUTC(false),   //In local time zone by default
                m_synchTimeout(SYNCHTIMEOUT_DEFAULT),
                m_lastSource(SYNC_SOURCE_NONE),
                m_neverSynch(true),
                m_1stSynchReported(false),
                m_inSynchTimer(NULL),
                m_outSynchTimer(NULL),
                m_onMinuteTimer(NULL),
                m_1stSynchTimer(NULL),
                m_NTPSynchTimer(NULL)
{
    m_synchTime.synchronised = false;

    //register application start time
    clock_gettime(MCMCLOCKELAPSED, &appStartTime);
    getTime(m_initialTime);
    m_synchTime = m_initialTime;
}


void TimeManager::start()
{
    static bool started = false;
    if(started)
    {
        return;
    }

    //Init synchronisation timers
    m_inSynchTimer = new Timer(0, 10, this, Timer::TIMER_TYPE_ONESHOT, "InSynchTmr");
    m_outSynchTimer = new Timer(1, 0, this, Timer::TIMER_TYPE_ONESHOT, "OutSynchTmr");

    //Init FirstSynch timer (to be used once!)
    m_1stSynchTimer = new Timer(0, 20, this, Timer::TIMER_TYPE_ONESHOT, "FirstSyncTmr");

    //Init minute-on-the-minute timer
    m_onMinuteTimer = new Timer(m_initialTime.time.tv_sec, 0, this, Timer::TIMER_TYPE_ONMINUTE, "OnMinuteTmr");
    if(m_onMinuteTimer->start() < 0)
    {
        perror("Error: Unable to start timer for on-the-minute notification");
    }

    //Init NTP refresh timer
    m_NTPSynchTimer = new Timer(60, 0, this, Timer::TIMER_TYPE_PERIODIC, "NTPSynchTmr");

    started = true;
}


TimeManager::~TimeManager()
{
    stopObservers();
    /* Stop and remove timers */
    delete m_inSynchTimer;
    m_inSynchTimer = NULL;
    delete m_outSynchTimer;
    m_outSynchTimer = NULL;
    delete m_onMinuteTimer;
    m_onMinuteTimer = NULL;
    delete m_1stSynchTimer;
    m_1stSynchTimer = NULL;
    delete m_NTPSynchTimer;
    m_NTPSynchTimer = NULL;
}


void TimeManager::updateSynchObservers()
{
    TimeObserver *observerPtr;

    // Protect the observer queue
    MasterLockingMutex mMutex(m_synchQueueMutex);

    //Get the data once to ensure all observers update with same data
    TimeStr timestamp;
    getTime(timestamp);

    /* Notify observer. Get first observer */
    observerPtr = m_synchObservers.getFirst();
    while(observerPtr != NULL)
    {
        observerPtr->updateTime(timestamp);

        /* Get next observer */
        observerPtr = m_synchObservers.getNext(observerPtr);
    }
}


void TimeManager::updateMinuteObservers(TimeStr& timestamp)
{
    TimeObserver *observerPtr;

    //Round timestamp to nearest minute
    TimeStr roundTime = timestamp;
    roundTime.time.tv_nsec = 0;
    roundTime.time.tv_sec -= (roundTime.time.tv_sec % 60);

    // Protect the observer queue
    MasterLockingMutex mMutex(m_minuteQueueMutex);

    /* Notify observer. Get first observer */
    observerPtr = m_minuteObservers.getFirst();
    while(observerPtr != NULL)
    {
        observerPtr->updateTime(roundTime);

        /* Get next observer */
        observerPtr = m_minuteObservers.getNext(observerPtr);
    }
}


void TimeManager::updateFirstSynchObservers(TimeStr& timestamp)
{
    TimeObserver *observerPtr;

    // Protect the observer queue
    MasterLockingMutex mMutex(m_1stSynchQueueMutex);

    /* Notify observer. Get first observer */
    observerPtr = m_1stSynchObservers.getFirst();
    while(observerPtr != NULL)
    {
        observerPtr->updateTime(timestamp);

        /* Get next observer */
        observerPtr = m_1stSynchObservers.getNext(observerPtr);
    }
}


void TimeManager::stopObservers()
{
    {//Lock scope
        // Protect the observer queue
        MasterLockingMutex mMutex(m_synchQueueMutex, LU_TRUE);

        /* Delete all observers */
        m_synchObservers.empty();
    }
    {//Lock scope
        // Protect the observer queue
        MasterLockingMutex mMutex(m_minuteQueueMutex, LU_TRUE);

        /* Delete all observers */
        m_minuteObservers.empty();
    }
    {//Lock scope
        // Protect the observer queue
        MasterLockingMutex mMutex(m_1stSynchQueueMutex, LU_TRUE);

        /* Delete all observers */
        m_1stSynchObservers.empty();
    }
}


void TimeManager::handleAlarmEvent(Timer &source)
{
    //Get the data once to ensure all observers update with same data
    TimeStr timestamp;
    getTime(timestamp);

    if(&source == m_inSynchTimer)
    {
        //Timer back on synch: used for report only
        m_synchTime.synchronised = true;
        updateSynchObservers();
    }
    else if(&source == m_outSynchTimer)
    {
        //Timer expired: we are now out of synchronisation
        m_synchTime.synchronised = false;
        updateSynchObservers();
    }
    else if(&source == m_onMinuteTimer)
    {
        //On-the-minute tick
        updateMinuteObservers(timestamp);

        /* Take the opportunity to clear the firstSynchTimer if it was already finished */
        if(m_1stSynchReported)
        {
            delete m_1stSynchTimer;
            m_1stSynchTimer = NULL;
        }
        /* Store time to HW every minute (it does not wear out) */
        saveTime();
    }
    else if(&source == m_1stSynchTimer)
    {
        //first synch received
        updateFirstSynchObservers(timestamp);
        m_1stSynchReported = true;
    }
    else if(&source == m_NTPSynchTimer)
    {
        synchFromNTP();
    }
    else
    {
        return; //not a valid timer
    }
}

void TimeManager::synchFromNTP()
{
    if(canSetTime(SYNC_SOURCE_NTP))
    {
        //NTP enabled: check NTP status
        std::stringstream commandNTP;
        commandNTP << NTP_CHECK_SCRIPT
                   << static_cast<lu_uint32_t>(MSEC_TO_SEC(m_synchTimeout));
        if(system(commandNTP.str().c_str()) == 0)
        {
            //NTP update within sync time frame: keep sync
            //printf("NTP in sync @ %s\n", now().toString(true).c_str());
            storeSynchTime(SYNC_SOURCE_NTP);
            setSync(true);
        }
        else
        {
            //NTP update out of sync: do not update
            //printf("NTP OUT sync @ %s\n", now().toString(true).c_str());

            /*Note: the script already checks for the time out passed, so at this
             * point the NTP synchronisation is already out of sync.
             */
            if(m_lastSource == SYNC_SOURCE_NTP)
            {
                setSync(false);
            }
        }
    }
}

bool TimeManager::canSetTime(const SYNC_SOURCE source)
{
    //Allow to set the system time when:
    return(
        (m_synchTime.synchronised == false) ||  //Was not synchronised, accept any source
        (source == SYNC_SOURCE_CONFIGTOOL) ||   //Config Tool can force set time
        (m_lastSource == SYNC_SOURCE_CONFIGTOOL) || //Overriding Config Tool
        (source >= m_lastSource)                //Same source, or source has more priority than last
    );
}


TimeManager::TERROR TimeManager::setSync(const bool syncStatus)
{
    TERROR ret = TERROR_NONE;
    bool oldSynch = m_synchTime.synchronised;
    m_synchTime.synchronised = syncStatus;
    if(syncStatus)
    {
        //Synchronisation accepted
        lu_int32_t res;
        /* (Re-)start timer for "End of synchronisation" notification */
        res = m_outSynchTimer->start(ms_to_timespec(m_synchTimeout)); //restart expiration timer
        if(res < 0)
        {
            ret = TERROR_TIMER;
        }

        //Check first synchronisation
        if(m_neverSynch)
        {
            m_neverSynch = false;    //has been synchronised (good or bad) from some source
            if(m_1stSynchTimer->start() < 0)
            {
                perror("Error: Unable to use timer for first synchronisation");
                ret = TERROR_TIMER;
            }
        }

        //Check Back to be synchronised
        if(oldSynch != syncStatus)
        {
            //synch 0->1: Back to be synchronised
            res = m_inSynchTimer->start();    //start timer to update observers
            if(res < 0)
            {
                ret = TERROR_TIMER;
            }
        }
    }
    else
    {
        if(oldSynch != syncStatus)
        {
            //synch 1->0: De-synchronisation
            m_inSynchTimer->stop();
            m_outSynchTimer->stop();
            handleAlarmEvent(*m_outSynchTimer); //call expiration procedure
        }
    }
    return ret;
}

void TimeManager::saveTime()
{
    /* FIXME: pueyos_a - Update of hardware clock to be made non-HW dependant */
    system("hwclock -w");
}

void TimeManager::storeSynchTime(const SYNC_SOURCE source)
{
    m_synchTime = now();
    m_lastSource = source;
}


/*
 *********************** End of file ******************************************
 */

