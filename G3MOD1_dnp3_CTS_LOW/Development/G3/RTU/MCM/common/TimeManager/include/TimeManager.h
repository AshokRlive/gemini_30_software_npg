/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: TimeManager.cpp 14-Aug-2013 13:12:34 pueyos_a $
 *               $HeadURL: \\10.11.0.7\pueyos_a\workspace\app_refactor\G3\RTU\MCM\MCMApp\src\..\..\common\TimeManager\src\TimeManager.cpp $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       TimeManager header module defines the manager for handling clock time.
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 14-Aug-2013 13:12:34	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   14-Aug-2013 13:12:34  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_9FD1EFF1_3DAC_4671_A4D9_DA86DCAFB10B__INCLUDED_)
#define EA_9FD1EFF1_3DAC_4671_A4D9_DA86DCAFB10B__INCLUDED_



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <sys/time.h>
#include <string>
#include <assert.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "LockingMutex.h"
#include "Timer.h"
#include "QueueMgr.h"
#include "QueueObj.h"
#include "MainAppEnum.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/* Use CLOCK_MONOTONIC_RAW when available */
#ifdef CLOCK_MONOTONIC_RAW
#define MCMCLOCKELAPSED CLOCK_MONOTONIC_RAW
#else
#define MCMCLOCKELAPSED CLOCK_MONOTONIC
#endif

/* Forward declaration */
class TimeObserver;

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/**
 * TimeManager observer queue manager
 */
typedef QueueMgr<TimeObserver> TimeObserverQueue;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * Class TimeManager
 *
 * This Singleton class keeps track of the synchronisation so when the application
 * is not set to a date/time for a long time, the date/time becomes not secure due
 * to possible internal time drift. That means the system is not synchronised.
 */
class TimeManager : private ITimerHandler
{
public:
     /**
      * \brief Time stamp with synchronisation flag
      */
     struct TimeStr
     {
     public:
         /**
          * \brief Date format to use
          */
         typedef enum
         {
             DATEFORMAT_LITTLEENDIAN,   //"dd/mm/yyyy" EU format
             DATEFORMAT_MIDDLEENDIAN,   //"mm/dd/yyyy" USA format
             DATEFORMAT_BIGENDIAN,      //"yyyy/mm/dd" Japanese format
             DATEFORMAT_LAST
         } DATEFORMAT;

     public:
         static const __time_t GOODTIMES = 1420070400L; //Basic "good" time to set the badTime flag
                                                        //1420070400 = 1/1/2015
     public:
         /* WARNING! The absolute and relative times aren't guaranteed to be
          * equivalent to the exact same time stamp.
          */
         struct timespec time;      //Absolute time (as in system time: CLOCK_REALTIME)
         struct timespec relatime;  //Relative time (as in elapsed time: CLOCK_MONOTONIC)
         bool synchronised;         //True when time is synchronised
         bool isUTC;                //The Absolute time is UTC or time zone is not used
         bool neverSynchronised;    //True when never synchronised from startup
         bool badTime;              //True when time is set before TimeStr::GOODTIMES

     public:
         TimeStr(); //Constructor
         bool operator==(const TimeStr a);  //Comparison operator
         bool operator!=(const TimeStr a);  //Comparison operator

         /**
          * \brief Calculate elapsed milliseconds between two Time Stamps
          *
          * Please note that this function always returns a positive value, so
          * if the finish time is earlier than the start time, it returns 0.
          * The nanosecond precision is dropped;that means if a difference
          * is 42.9 ms, it returns 42 since a complete ms is not reached.
          * Relative time is used so it is not affected by time adjustments.
          *
          * \param endTime The other Time Stamp to compare to
          *
          * \return Elapsed milliseconds
          */
         lu_uint64_t elapsed_ms(const TimeStr endTime);

         /**
          * \brief Convert absolute time field to specific string
          *
          * Default format is "DD/MM/YY HH:MM:SS.MSS"
          *
          * \param WithSync Add the Synchronisation status to the string
          * \param dateFormat Format to use for the date part of the string
          *
          * \return Output string in the specified format
          */
         std::string toString(const bool withSync = false,
                              const DATEFORMAT dateFormat = DATEFORMAT_LITTLEENDIAN
                              ) const;

         /**
          * \brief Convert absolute time field date to specific string
          *
          * \param WithSync Add the Synchronisation status to the string
          * \param dateFormat Format to use for the date part of the string
          *
          * \return Output in the format "DD/MM/YY"
          */
         std::string dateString(const bool withSync = false,
                                const DATEFORMAT dateFormat = DATEFORMAT_LITTLEENDIAN
                                ) const;

         /**
          * \brief Convert absolute time field to specific string without the days
          *
          * \param WithSync Add the Synchronisation status to the string
          *
          * \return Output in the format "HH:MM:SS"
          */
         std::string timeString(const bool withSync = false) const;

         /**
          * \brief Convert absolute time field to specific string without the days
          *
          * \param WithSync Add the Synchronisation status to the string
          *
          * \return Output in the format "HH:MM:SS.MSS"
          */
         std::string timeString_ms(const bool withSync = false) const;
    };

    /**
     * \brief Daylight saving time values
     */
    typedef enum
    {
        DATE_DST_NOTUSED = -1,  //Daylight Saving Time not available
        DATE_DST_OFF = 0,       //Daylight Saving Time is not in effect
        DATE_DST_ON = 1,        //Daylight Saving Time is in effect
        DATE_DST_LAST
    } DATE_DST;

    /**
     * \brief Time Manager result error codes
     */
    typedef enum
    {
        TERROR_NONE,          //No error
        TERROR_SYNC,          //Synchronisation not applied
        TERROR_TIMER,         //System timer unavailable
        TERROR_OTHER,         //Other error
        TERROR_LAST
    } TERROR;

public:
    /**
     * \brief Get a Time Manager instance.
     *
     * Usage example:
     *  TimeManager& timeMgr = TimeManager::getInstance();
     *
     * \return Reference to this singleton
     */
    static TimeManager& getInstance()
    {
        static TimeManager instance;
        return instance;
    };

    /**
     * \brief Starts the Time Manager internal timers and counters
     */
    void start();

    /**
     * \brief Configure the Time Manager.
     *
     * \param syncTimeout_ms Time for considering out of synchronisation, in milliseconds.
     */
    void configure(const lu_uint64_t syncTimeout_ms);

    /**
	 * \brief Set the system time (as UTC or local) for all the application
	 *
	 * Note: All the application should set the time using TimeManager::setTime().
	 *
     * Note that parameters are by reference for performance reasons
     *
	 * \param datetime Date and time specification to set (NOT reference since it is modified)
     * \param mseconds Extra time resolution in milliseconds
	 * \param sync Synchronisation flag. Set to LU_TRUE if the source of synchronisation is reliable.
	 * \param asUTC Incoming datetime is directly in UTC, not local time
	 * \param source Source of the synchronisation.
	 *
	 * \return Error code: TERROR_NONE when given time has been set
	 */
	TERROR setTime(struct tm datetime,
	                const lu_uint32_t& mseconds = 0,
                    const lu_bool_t& sync = LU_FALSE,
	                const SYNC_SOURCE& source = SYNC_SOURCE_NONE,
	                const lu_bool_t& asUTC = LU_FALSE
	                );

    /**
     * \brief Set the system time (as UTC only) for all the application
     *
     * Note: All the application should set the time using TimeManager::setTime().
     *
     * Note that parameters are by reference for performance reasons
     *
     * \param datetime Date and time specification to set
     * \param sync Synchronisation flag. Set to LU_TRUE if the source of synchronisation is reliable.
     * \param source Source of the synchronisation.
     *
     * \return Error code: TERROR_NONE when given time has been set
     */
	TERROR setTime(const struct timespec& datetime,
	                const lu_bool_t& sync = LU_FALSE,
	                const SYNC_SOURCE& source = SYNC_SOURCE_NONE
	                );

	/**
     * \brief Tells the Time Manager to add a special synchronisation source
     *
     * This is a way to indicate the Time Manager that it has to do some special
     * processing specially for the specified source. For example, for NTP source
     * it starts a timed job to check the NTP status.
     *
     * \param source New source of sync.
     */
	void addSyncSource(const SYNC_SOURCE source);

	/**
	 * \brief Get the current system time
	 *
	 * By default, the given time is in local time except specified otherwise.
     *
     * WARNING! In the TimeManager::TimeStr structure, the absolute and relative
     * times aren't guaranteed to be equivalent to the exact same time stamp.
     *
	 * \param datetime Where to store the current time
	 * \param sync Return here the current synchronisation status
	 * \param source Return here the source of the last time synchronisation
	 * \param inUTC Get the system time in UTC instead of local time
	 */
    void getTime(TimeManager::TimeStr& datetime, const bool inUTC = false);
    void getTime(TimeManager::TimeStr& datetime,
                    SYNC_SOURCE& source,
                    const bool inUTC = false);
    void getTime(struct timespec* datetime,
                    lu_bool_t* sync = NULL,
                    SYNC_SOURCE* source = NULL,
                    const bool inUTC = false);
	void getTime(struct tm* datetime,
	                lu_bool_t* sync = NULL,
	                SYNC_SOURCE* source = NULL,
	                const bool inUTC = false);

    /**
     * \brief Return the current UTC system time
     *
     * WARNING! In the TimeManager::TimeStr structure, the absolute and relative
     * times aren't guaranteed to be equivalent to the exact same time stamp.
     *
     * return Current time
     */
	const TimeManager::TimeStr now();

	/**
	 * \brief Get the initial time stamp when app was started
	 * 
	 * Used for initial value on time stamps
	 * 
	 * \return Registered time when app was started.
	 */
	const TimeManager::TimeStr& getInitialTime();

	/**
     * \brief Set whether the time is set as local or UTC
     *
     * \param timezoneUTC Time zone set as UTC; set to false for local time
     */
    void setTimezoneUTC(bool timezoneUTC);

	/**
     * \brief Get if the time is set as local or UTC
     *
     * \return True when is set as UTC
     */
    bool isTimezoneUTC() const;

    /**
     * \brief Set the name of the time zone in use
     *
     * Usualy is the name of the time zone file, such as "UTC", "Etc/GMT+1", and so on
     *
     * \param localFileName Name of the time zone in use
     */
    void setTimezoneName(const std::string localFileName);

    /**
     * \brief Get the time zone name as ISO 8601
     *
     * \return Time zone name C string
     */
    const char* getTimezoneName();

    /**
     * \brief Get the time zone name as POSIX file name
     *
     * \return Time zone name C string
     */
    const char* getPOSIXTimezoneName();

    /**
     * \brief Set synchronisation timeout time.
     *
     * This sets the parameter that determines how much time has to pass without
     * synchronisation to declare the time not synchronised.
     *
     * \param timeoutSynch Amount of time to consider the time as synchronised.
     */
    void setSynchTimeoutTime(const struct timespec timeoutSynch);
    void setSynchTimeoutTime(const lu_uint64_t timeoutSynch_ms);

    /**
     * \brief Gets the status of the internal synchronisation
     *
     * \param source Returns here the source of the last time synchronisation.
     * \param syncTime Returns here the time WHEN last synchronisation happened.
     * \param syncTime_ms Returns here the time SINCE the last time synchronisation.
     *
     * \return Synchronisation status. LU_TRUE means the system is still synchronised.
     */
    lu_bool_t getSyncStatus();
    lu_bool_t getSyncStatus(TimeManager::TimeStr& synchdatetime);
    lu_bool_t getSyncStatus(SYNC_SOURCE *source);
    lu_bool_t getSyncStatus(SYNC_SOURCE *source, struct timespec *syncTime);
    lu_bool_t getSyncStatus(SYNC_SOURCE *source, lu_uint64_t *syncTime_ms);

    /**
     * \brief Convert from time value to struct tm
     *
     * \param datetime Time to be converted
     * \param result Where to store the resulting conversion
     *
     * \return LU_FALSE when conversion error happened
     */
    static lu_bool_t TimeToStructtm(const time_t datetime_sec, struct tm* result);
    static lu_bool_t TimeToStructtm(const struct timespec& datetime, struct tm* result);

    /**
     * \brief Convert from C struct tm time data to struct timespec
     *
     * Since the struct tm does not have capability to store values under the
     * second resolution, this function allows to add it separately.
     *
     * \param datetime_tm Time to be converted
     * \param nsecs Number of nano seconds to add, if available
     *
     * \return Resulting time
     */
    struct timespec structtmtoTimespec(struct tm& datetime_tm, const long int nsecs = 0);

    /**
     * \brief Convert from time structure to local or UTC time
     *
     * This assumes the input is in already in the source time zone, and makes
     * the conversion to the other time zone.
     *
     * \param datetime Time to be converted
     * \param toUTC Converts local time to UTC; otherwise converts UTC to local time
     *
     * \return Converted time
     */
    static const TimeManager::TimeStr localUTC(const TimeManager::TimeStr& datetime, const bool toUTC=true);
    static const TimeManager::TimeStr TimeToLocal(const TimeManager::TimeStr& datetime);
    static const TimeManager::TimeStr TimeToUTC(const TimeManager::TimeStr& datetime);

    /**
     * \brief Set the initial application time if different from object creation time
     *
     * Use this to set the start time if it was before the creation of this object.
     * This value will be used as reference to calculate the elapsed time since
     * the application started running.
     * The correct way to obtain this time is by using:
     *      struct timespec appStartTime;
     *      clock_gettimespec(&appStartTime);
     * at the beginning of the application, an then call this method with the
     * value obtained.
     *
     * \param startTime Application start time
     */
    void setAppStartTime(const struct timespec startTime);

    /**
     * \brief Get the application running time
     *
     * \return Application running time in sec & usec OR msec
     */
	struct timespec getAppRunningTime();
	lu_uint64_t getAppRunningTime_ms();

    /**
     * \brief Attach an observer for in/out of synchronisation state change
     *
     * When the RTU goes out of synchronisation, all the observers are automatically called
     *
     * \param observer New observer
     *
     * \return Error code
     */
    virtual lu_int32_t attachSynch(TimeObserver* observer);

    /**
     * \brief Remove an observer from the Synchronisation state observer queue
     *
     * \param observer Observer to remove
     *
     * \return error code
     */
    virtual void detachSynch(TimeObserver* observer);

    /**
     * \brief Attach an observer for on-the-minute notification
     *
     * When the RTU time reaches 00 seconds (minute change), all the observers
     * are automatically called
     *
     * \param observer New observer
     *
     * \return Error code
     */
    virtual lu_int32_t attachMinute(TimeObserver* observer);

    /**
     * \brief Remove an observer from the on-the-minute notification observer queue
     *
     * \param observer Observer to remove
     *
     * \return error code
     */
    virtual void detachMinute(TimeObserver* observer);

    /**
     * \brief Attach an observer for first synch notification
     *
     * When the RTU is synchronised for the first time, all the observers
     * are automatically notified.
     *
     * \param observer New observer
     *
     * \return Error code
     */
    virtual lu_int32_t attachFirstSynch(TimeObserver* observer);

    /**
     * \brief Remove an observer from the first synch notification observer queue
     *
     * \param observer Observer to remove
     *
     * \return error code
     */
    virtual void detachFirstSynch(TimeObserver* observer);
    
private:
    /**
     * \brief Singleton: Private constructor & destructor
     */
	TimeManager();
	virtual ~TimeManager();

	/*
     * \brief Singleton: Private copy constructor
     */
	TimeManager(const TimeManager &timeMgr)
    {
        LU_UNUSED(timeMgr);
        assert(false);  //Do not use copy constructor on a Singleton
    };

	/**
	 * \brief Update the synch status by checking time
	 *
	 * \param syncTime Returns here the time elapsed since the last time synchronisation.
	 * \return Synch status
	 */
	lu_bool_t checkSyncStatus(struct timespec *syncTime);

	/**
	 * \brief Updates all the attached observers if the synchronisation status changes
	 */
	void updateSynchObservers();

    /**
     * \brief Updates all the attached observers if the current hour reaches next minute
     */
    void updateMinuteObservers(TimeStr& timestamp);

    /**
     * \brief Updates all the attached observers when a first synch is reached
     */
    void updateFirstSynchObservers(TimeStr& timestamp);

    /**
     * \brief Prevents any new update of any observer in all the queues
     */
    void stopObservers();

    //Inherited from ITimerHandler
    virtual void handleAlarmEvent(Timer &source);

    /**
     * \brief Checks the synchronisation status on the NTP client
     *
     * This checks the NTP client status on the system and updates the NTP
     * synchronisation status accordingly.
     */
    void synchFromNTP();

    /**
     * \brief Tells whether the Time Manager can set the time from the given source
     *
     * \param source Source of synchronisation
     *
     * \return true when the time can be set
     */
    bool canSetTime(const SYNC_SOURCE source);

    /**
     * \brief Set the sync status, operating related timers
     *
     * Note that this method does NOT check the source itself!
     * When set to sync:
     *  - It Refreshes the sync, restarting the outSynchTimer
     *  - if it was not sync:
     *      - Starts the inSyncTimer
     *      - Returns true
     * When set to not sync:
     *  - cancels related timers
     *
     * \param source Source of synchronisation
     *
     * \return Error code
     */
    TERROR setSync(const bool syncStatus);

    /**
     * \brief Stores the wall clock time in the hardware chip
     *
     * Note that the chip does allow any amount of writings: since it is not an
     * NVRAM it could handle it.
     */
    void saveTime();

    /**
     * \brief Stores synchronisation time and last source
     *
     * Stores the current time as the synchronisation time, updating the last
     * source of synchronisation
     *
     * \param source Last source of synchronisation
     */
    inline void storeSynchTime(const SYNC_SOURCE source);

private:
	MasterMutex m_clockMutex;       //Mutex for exclusion between get/set time
    bool m_timezoneUTC;             //States that UTC is in use. Otherwise, use local time
	lu_uint64_t m_synchTimeout;     //Time out (in ms) for considering out of synchronisation
    TimeStr m_synchTime;            //Internal status of the last synchronisation
    TimeStr m_initialTime;          //Initialisation time stamp (fixed)
    SYNC_SOURCE m_lastSource;       //Source of the last synchronisation
    struct timespec appStartTime;   //time stamp of the app start (modifiable) - used for App elapsed time
    bool m_neverSynch;              //True when never synchronised from startup
    bool m_1stSynchReported;        //First synchronisation has been completely reported to all observers
    std::string m_POSIXtzFileName;  //Local Time Zone name, given as local file name: e.g. "Etc/GMT+1"
    std::string m_timezoneName;     //Local Time Zone name, given as "GMT+/-n" (+/- are inverted respect to POSIX)

    /* Timers */
    Timer* m_inSynchTimer;    //One-shot timer to run code when system is synchronised
    Timer* m_outSynchTimer;   //One-shot timer to notify when the system goes out of synchronisation
    Timer* m_onMinuteTimer;   //Timer to notify when the system time reaches the minute
    Timer* m_1stSynchTimer;   //One-shot timer for when the system time has been synch for the first time
    Timer* m_NTPSynchTimer;   //Timer for checking the NTP synchronisation status
    TimeObserverQueue m_synchObservers;     //enlisted synchronisation notification observers
    TimeObserverQueue m_minuteObservers;    //enlisted on-the-minute notification observers
    TimeObserverQueue m_1stSynchObservers;  //enlisted first synch notification observers
    MasterMutex m_synchQueueMutex;      //mutex to protect Synch observer queue
    MasterMutex m_minuteQueueMutex;     //mutex to protect on-the-minute observer queue
    MasterMutex m_1stSynchQueueMutex;   //mutex to protect first synch observer queue
};


/**
 * \brief Class for notifications of any time status change
 *
 * This class must be used to create an observer to take action when there is
 * any time status change, such as synchronisation (time quality), or on the
 * minute time.
 */
class TimeObserver : public QueueObj<TimeObserver>
{
public:
     TimeObserver() {};
     virtual ~TimeObserver() {};

     /**
      * \brief notify that the time status has changed
      *
      * \param currentTime Current time, with synchronisation flag embedded.
      */
     virtual void updateTime(TimeManager::TimeStr& currentTime) = 0;
};

#endif // !defined(EA_9FD1EFF1_3DAC_4671_A4D9_DA86DCAFB10B__INCLUDED_)

/*
 *********************** End of file ******************************************
 */

