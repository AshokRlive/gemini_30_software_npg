/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:G3DBClient.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   5 Jul 2017     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef COMMON_G3DATABASE_SRC_G3DBCLIENT_H_
#define COMMON_G3DATABASE_SRC_G3DBCLIENT_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <pthread.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "../include/G3DBProtocol.h"

namespace g3db {
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */


/**
 * Interface of event handler.
 */
class IEventHandler
{
public:
    virtual ~IEventHandler(){}
    virtual lu_bool_t handle(DBEvent& event) = 0;
};

class G3DBClient
{
    typedef std::vector<IEventHandler*> HandlerList;

public:
    G3DBClient();
    virtual ~G3DBClient();

    void        setRequestTimeout(lu_uint32_t timeoutMs);
    void        connect();
    void        disconnect();
    lu_bool_t   sendRequest(DBQuery& query);
    void        recvSubscribe();
    void        registerHandler(IEventHandler* handler);
    void        deregisterHandler(IEventHandler* handler);

private:
    void            startSubscribeThread();
    static void*    subscribeThreadRoutine(void*);


private:
    pthread_t       m_thread; // a thread for receiving subscribed messages.
    pthread_mutex_t m_lock;		
    HandlerList     m_handlers; // list of handler for handling subscribed messages.
    lu_uint32_t     m_requestTimeoutMs;
    lu_bool_t       m_connected;	

};

} // end of namespace g3db

#endif /* COMMON_G3DATABASE_SRC_G3DBCLIENT_H_ */

/*
 *********************** End of file ******************************************
 */
