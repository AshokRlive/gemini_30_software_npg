/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:G3DBClient.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   5 Jul 2017     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdio.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "zhelpers.hpp"
#include "zmq.hpp"
#include "G3DBClient.h"
#include "G3DBProtocol.h"
#include "timeOperations.h"
#include "../../BaseReuse/include/dbg.h"

namespace g3db {
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
/*
 * If defined, a Lazy Pirate will be applied when sending request.
 * */
#define RELIABLE_REQUEST_REPLY  0
#if RELIABLE_REQUEST_REPLY
#define REQUEST_RETRIES 3     //  Before we abandon
#define REQUEST_TIMEOUT 2500  //  msecs, (> 1000!)
#endif

#define TITLE "G3DBClient >>"

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static zmq::socket_t* s_createSubSocket(zmq::context_t& ctxt);
static zmq::socket_t* s_createReqSocket(zmq::context_t& ctxt, lu_uint32_t timeout);
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static zmq::context_t* mp_ctxt = NULL;
static zmq::socket_t*  mp_requester;
static zmq::socket_t*  mp_subscriber;

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
G3DBClient::G3DBClient():
                m_thread(0),
                m_lock(),
                m_handlers(),
                m_requestTimeoutMs(1000),
                m_connected(LU_FALSE)
{
    /* Initializes mutex to be reentrant*/
    pthread_mutexattr_t Attr;
    pthread_mutexattr_init(&Attr);
    pthread_mutexattr_settype(&Attr, PTHREAD_MUTEX_RECURSIVE);
    pthread_mutex_init(&m_lock, &Attr);
}

G3DBClient::~G3DBClient()
{
    disconnect();
}

void G3DBClient::registerHandler(IEventHandler* handler)
{
    if(handler != NULL)
    {
    	/*Add to list if not exists*/
        if (std::find(m_handlers.begin(), m_handlers.end(), handler) == m_handlers.end())
            m_handlers.push_back(handler);
    }

}
void G3DBClient::deregisterHandler(IEventHandler* handler)
{
    m_handlers.erase(std::remove(m_handlers.begin(), m_handlers.end(), handler), m_handlers.end());
}

void  G3DBClient::setRequestTimeout(lu_uint32_t timeoutMs)
{
    m_requestTimeoutMs = timeoutMs;
}

void G3DBClient::connect()
{
    pthread_mutex_lock(&m_lock);

    if(m_connected == LU_TRUE)
    {
        pthread_mutex_unlock(&m_lock);
        return;// Already connected
    }

    if(mp_ctxt == NULL)
        mp_ctxt = new zmq::context_t(1);

    if(mp_requester == NULL)
        mp_requester = s_createReqSocket(*mp_ctxt, m_requestTimeoutMs);

    if(mp_subscriber == NULL)
        mp_subscriber = s_createSubSocket(*mp_ctxt);

    if(m_thread == 0)
    {
       startSubscribeThread();
    }

    m_connected = LU_TRUE;
    DBG_INFO("G3DBClient connected");
    pthread_mutex_unlock(&m_lock);
}

void G3DBClient::disconnect()
{
    pthread_mutex_lock(&m_lock);

    if(m_connected == LU_FALSE)
    {
        pthread_mutex_unlock(&m_lock);
        return;// Already disconnected
    }

    if(mp_requester != NULL)
    {
        delete mp_requester;
        mp_requester = NULL;
        DBG_INFO("G3DBClient disconnected");
    }

    if(mp_subscriber != NULL)
    {
        delete mp_subscriber;
        mp_subscriber = NULL;
    }

    if(m_thread > 0 )
    {

        if(pthread_cancel(m_thread) == 0)
            m_thread = 0;
        else
        {
            // TODO add error to syslog
            DBG_ERR("Failed to cancel thread\n");
        }
    }

    if(mp_ctxt != NULL)
    {
       delete mp_ctxt;
       mp_ctxt = NULL;
    }

    m_connected = LU_FALSE;

    pthread_mutex_unlock(&m_lock);
}

lu_bool_t G3DBClient::sendRequest(DBQuery& query)
{
    lu_bool_t success = LU_FALSE;
#if !RELIABLE_REQUEST_REPLY
    if(mp_requester == NULL)
    {
        return LU_FALSE;
    }

    pthread_mutex_lock(&m_lock);
    try
    {
        zmq::message_t request(sizeof(query));
        memcpy(request.data(), &query, sizeof(query));

        DBG_INFO("%s sending request", TITLE);
        if (mp_requester->send(request, ZMQ_NOBLOCK))
        {
            //  Get the reply.
            zmq::message_t reply;
            DBG_INFO("request sent. receiving response...");

            if (mp_requester->recv(&reply))
            {
                DBG_INFO("%s response received",TITLE);
                if(sizeof(query) == reply.size())
                {
                    memcpy(&query, reply.data(),reply.size());
                    success = LU_TRUE;
                }
                else
                {
                    DBG_ERR("%s malformed response!!",TITLE);
                }

            }
            else
            {
                DBG_ERR("%s response NOT received", TITLE);
            }
        }
        else
        {
            DBG_ERR("%s request NOT sent",TITLE);
        }
    }
    catch (zmq::error_t& e)
    {
        DBG_ERR("%s [SendRequest]exception caught:%s", TITLE, e.what());

    }

    if (success == LU_FALSE)
    {
        // Reopen socket
        DBG_INFO("%s re-opening socket...",TITLE);
        delete mp_requester;
        mp_requester = s_createReqSocket(*mp_ctxt, m_requestTimeoutMs);
    }

    pthread_mutex_unlock(&m_lock);
    return success;

#else
    int retries_left = REQUEST_RETRIES;

        while (retries_left) {
            s_send (*mp_requester, msgStr);

            bool expect_reply = true;
            while (expect_reply) {
                //  Poll socket for a reply, with timeout
                zmq::pollitem_t items[] = { {*mp_requester, 0, ZMQ_POLLIN, 0 } };
                zmq::poll (&items[0], 1, REQUEST_TIMEOUT);

                //  If we got a reply, process it
                if (items[0].revents & ZMQ_POLLIN) {
                    //  We got a reply from the server, must match sequence
                    std::string reply = s_recv (*mp_requester);
                    std::cout << "I: server replied OK (" << reply << ")" << std::endl;
                    retries_left = REQUEST_RETRIES;
                    expect_reply = false;
                }
                else if (--retries_left == 0) {
                    std::cout << "E: server seems to be offline, abandoning" << std::endl;
                    expect_reply = false;
                    break;
                }
                else {
                    std::cout << "W: no response from server, retrying..." << std::endl;
                    //  Old socket will be confused; close it and open a new one
                    delete mp_requester;
                    mp_requester = s_request_socket (context); // TODO
                    //  Send request again, on new socket
                    s_send (*mp_requester, msgStr);
                }
            }
        }

#endif

}

void G3DBClient::recvSubscribe()
{
    if(mp_subscriber == NULL)
        return;

    try
    {
        DBEvent event;
        zmq::message_t message(sizeof(event));
        if(mp_subscriber->recv(&message, ZMQ_NOBLOCK))
        {
            if(message.size() == sizeof(event))
            {
                memcpy(&event,message.data(), sizeof(event));
                for(std::vector<IEventHandler*>::iterator it = m_handlers.begin(); it != m_handlers.end(); ++it)
                {
                    (*it)->handle(event);
                }
                //printf("%s Received published event. eventId:%d\n", TITLE, event.eventId);
            }
            else
            {
                DBG_ERR("%s received malformed message", TITLE);
            }
        }
    }
    catch (std::exception& e)
    {
        DBG_ERR("%s [SUB]exception caught:%s\n", TITLE, e.what());

        // Reopen socket
        delete mp_subscriber;
        mp_subscriber = s_createSubSocket(*mp_ctxt);
    }
}
/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods*+-6
 ******************************************************************************
 */


void G3DBClient::startSubscribeThread()
{
    int rc;
   pthread_attr_t attr; // thread attribute
   // set thread detachstate attribute to DETACHED
   pthread_attr_init(&attr);
   pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

   /* Run debug server in a separate thread*/
   rc = pthread_create(&m_thread, &attr, &(G3DBClient::subscribeThreadRoutine), this);
   if (rc)
   {
       DBG_ERR("return code from pthread_create() is %d", rc);
   }
}

void* G3DBClient::subscribeThreadRoutine(void* owner)
{
    G3DBClient* client = (G3DBClient*)owner;
    DBG_INFO("%s subscribe thread is running",TITLE);

    while(true)
    {
        client->recvSubscribe();
        lucy_usleep(10000); // In microsecs
    }
    return NULL;
}


static zmq::socket_t* s_createReqSocket(zmq::context_t& ctxt, lu_uint32_t timeout)
{
    //  Configure socket to not wait at close time
      lu_int32_t linger = 0;
      lu_uint32_t timout = timeout; //ms
      zmq::socket_t* req;
      req = new zmq::socket_t(ctxt,ZMQ_REQ);
      req->setsockopt (ZMQ_LINGER, &linger, sizeof (linger));
      req->setsockopt (ZMQ_RCVTIMEO, &timout, sizeof (timout));
      req->connect (G3DB_SERVER_REQ);

      return req;
}

static zmq::socket_t* s_createSubSocket(zmq::context_t& ctxt)
{
    //  Configure socket to not wait at close time
      lu_int32_t linger = 0;
      lu_uint32_t timout = 3000; //ms
      zmq::socket_t* sub;
      sub = new zmq::socket_t(ctxt,ZMQ_SUB);
      sub->setsockopt (ZMQ_LINGER, &linger, sizeof (linger));
      sub->setsockopt (ZMQ_RCVTIMEO, &timout, sizeof (timout));
      sub->connect (G3DB_SERVER_SUB);

      const char *filter = ""; // empty filter to allow receive anything
      sub->setsockopt(ZMQ_SUBSCRIBE, filter, strlen (filter));

      return sub;
}

} // end of namespace g3db
/*
 *********************** End of file ******************************************
 */
