#!/bin/bash
# copy axf of MCM board to export path, for sending to the MCM itself
APP="Debug/*.axf"
DEST="/exports/${USER}/gemini/application"


# ** RELEASE ** 
# check Release directory and clean it
if [ -d Release ]; then
    # Remove Release contents
    rm -rf Release/*
else
    # Remove Release directory
    rm -rf Release
    # Create Release directory
    mkdir Release
fi
# Run cmake and create Release Makefiles
cd Release
cmake -G "Unix Makefiles" ../  -DCMAKE_BUILD_TYPE=Release -DCROSS_COMPILE=ON -DCMAKE_INSTALL_PREFIX=~/lib/

make install/strip

RESULT=$?
if [ $RESULT -eq 0 ]
then
    # successful make
    cp -v **/*.so $DEST
    cd ..
else
    exit $RESULT
fi
