/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:Main.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   28 Apr 2016     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdio.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "zhelpers.hpp"
#include "G3DBServer.h"
#include "G3DBProtocol.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

using namespace g3db;
/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
class Handler : public IRequestHandler
{
public:
    virtual lu_bool_t handle(DBQuery& query)
    {
        getTimestamp(query.timestamp);
        query.value.digital = 1000 + query.reqId;
        query.value.type = VALUE_TYPE_DIGITAL;
        query.reqStatus = DB_REQUEST_STATUS_OK;

        printf("Processed request [DB:%s]\n",query.databaseId);
        return LU_TRUE;
    }
};

int main(int argc, char **argv) {
    s_version ();

    zmq::context_t context(1);
    G3DBServer server(context);
    server.registerHandler(new Handler());
    server.start();
    while(true)
    {
        DBEvent event;
        event.eventId = DB_EVENT_ID_VPOINT_UPDATE;
        getTimestamp(event.pointEvent.timestamp);
        server.publish(event);
        sleep(1);
    }

    return 0;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
