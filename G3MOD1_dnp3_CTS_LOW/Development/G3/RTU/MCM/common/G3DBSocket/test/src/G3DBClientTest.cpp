/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:Main.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   28 Apr 2016     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdio.h>
#include <unistd.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "G3DBClient.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
#define TEST_REQ 0
#define TEST_SUB 1

class EventHandler: public g3db::IEventHandler
{
    virtual lu_bool_t handle(g3db::DBEvent& event);
};

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

using namespace g3db;

#define print_cpp_version() \
if( __cplusplus == 201103L ) std::cout << "C++11\n" ; \
else if( __cplusplus == 199711L ) std::cout << "C++98\n" ;\
else std::cout << "pre-standard C++\n" ; \
/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
static void init(DBQuery& query);

                            // Plus 1 non-exist
#define POINT_NUM           (4)
#define INPUT_NUM           (6)
#define OUTPUT_NUM          (6)
#define CONTROL_NUM         (2)
#define CONSTANT_NUM        (2)

void print_results(const char* name, DBQuery* queries, int size)
{
    printf("\n=== %s ==== \n",name);
     for(int i = 0; i < size; i++){
         printf("%s value: %-20s online:%-5s (status:%d)\n",
                        queries->ref.toString().c_str(),
                        queries->value.toString().c_str(),
                        queries->quality.online?"true":"false",
                        queries->reqStatus);
         queries++;
     }

}

int main(int argc, char **argv) {
    print_cpp_version();

    G3DBClient client;
    client.setRequestTimeout(5000);
    client.connect();

#if TEST_REQ
{
    DBQuery points[POINT_NUM];
    for(int i = 0; i < POINT_NUM; i++){
        init(points[i]);
        points[i].reqId = DB_REQUEST_ID_READ_POINT;
        points[i].ref.id = i;
        points[i].ref.group = 0;
        if(!client.sendRequest(points[i]))
            std::cerr <<"POINT failed"<<std::endl;
    }
    print_results("POINTS", points, POINT_NUM);
}


{
    DBQuery inputs[INPUT_NUM];
    for(int i = 0; i < INPUT_NUM; i++){
        init(inputs[i]);
        inputs[i].reqId = DB_REQUEST_ID_GET_INPUT;
        inputs[i].ref.id = i;
        if(i == 4 || i == 5)
            inputs[i].value.type = VALUE_TYPE_ANALOGUE;
        else
            inputs[i].value.type = VALUE_TYPE_DIGITAL;
        if(!client.sendRequest(inputs[i]))
            std::cerr <<"INPUT failed at:"<< i <<std::endl;
    }
    print_results("INPUTS", inputs, INPUT_NUM);
}


{
    DBQuery consts[CONSTANT_NUM];
    for(int i = 0; i < CONSTANT_NUM; i++){
        init(consts[i]);
        consts[i].reqId = DB_REQUEST_ID_GET_CONSTANT;
        consts[i].ref.id = i;
        if(!client.sendRequest(consts[i]))
            std::cerr <<"CONSTANT failed"<<std::endl;
    }
    print_results("CONSTATNS", consts, CONSTANT_NUM);
}


{
    DBQuery outputs[OUTPUT_NUM];
    for(int i = 0; i < OUTPUT_NUM; i++){
        init(outputs[i]);
        outputs[i].reqId = DB_REQUEST_ID_SET_OUTPUT;
        outputs[i].ref.id = i;

        if(i < 4)
        {
            outputs[i].value.type =  g3db::VALUE_TYPE_DIGITAL;
            outputs[i].value.digital = 0;
        }
        else
        {
            outputs[i].value.type =  g3db::VALUE_TYPE_ANALOGUE;
            outputs[i].value.analogue = 123.123 + i;
        }
        outputs[i].quality.online = 1;
    if(!client.sendRequest(outputs[i]))
        std::cerr <<"OUTPUT failed"<<std::endl;
    }
    print_results("OUTPUTS", outputs, OUTPUT_NUM);
}

{
    DBQuery controls[CONTROL_NUM];
    for(int i = 0; i < CONTROL_NUM; i++){
        init(controls[i]);
        controls[i].reqId = DB_REQUEST_ID_CONTROL;
        controls[i].ref.id = i;
        controls[i].value.type =  g3db::VALUE_TYPE_DIGITAL;
        controls[i].value.digital=  0;
        controls[i].quality.online = 1;
    if(!client.sendRequest(controls[i]))
        std::cerr <<"CONTROL failed"<<std::endl;
    }
    print_results("CONTROLS", controls, CONTROL_NUM);

}
#endif

#if TEST_SUB
    client.registerHandler(new EventHandler());
    sleep(1000);
#endif


    client.disconnect();
    return 0;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
lu_bool_t EventHandler::handle(g3db::DBEvent& event)
{
    printf("received eventId:%d \n", event.eventId);

    return 0;
}

static void init(DBQuery& query)
{
    memset(&query, 0, sizeof(DBQuery));
    strcpy(query.databaseId, "Sectionaliser");
}
/*
 *********************** End of file ******************************************
 */
