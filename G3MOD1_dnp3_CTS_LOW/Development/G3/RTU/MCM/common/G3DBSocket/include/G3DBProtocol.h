/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MBMConfigProto.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   12 May 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef G3DATABASEMESSAGE_H_
#define G3DATABASEMESSAGE_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <time.h>       // timespec, localtime
#include <sstream>      // std::ostringstream
#include <iostream>     // std::cout, std::endl
#include <iomanip>      // std::setw
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "../../../../../common/include/lu_types.h"
#include "vector"

/* G3 database name space*/
namespace g3db {
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define G3DB_SERVER_REQ "ipc:///tmp/g3db_req.ipc"
#define G3DB_SERVER_SUB  "ipc:///tmp/g3db_sub.ipc"

const lu_uint16_t DB_ID_LENGHT = 30;

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
/* G3 database point type*/
typedef enum
{
    VALUE_TYPE_INVALID     = 0 ,
    VALUE_TYPE_DIGITAL         ,
    VALUE_TYPE_ANALOGUE
}VALUE_TYPE;

typedef enum
{
    DB_REQUEST_ID_INVALID           = 0x00,

    DB_REQUEST_ID_READ_POINT        ,
    DB_REQUEST_ID_WRITE_POINT       ,

    DB_REQUEST_ID_GET_INPUT         ,
    DB_REQUEST_ID_SET_OUTPUT        ,
    DB_REQUEST_ID_GET_CONSTANT      ,
    DB_REQUEST_ID_CONTROL
}DB_REQUEST_ID;

typedef enum
{
    DB_REQUEST_STATUS_NOT_PROC      = 0x00,  /* request not processed*/
    DB_REQUEST_STATUS_OK            ,
    DB_REQUEST_STATUS_INVALID_REQUEST,
    DB_REQUEST_STATUS_DB_ERR,               /* internal error in DB*/
    DB_REQUEST_STATUS_UNSUPPORT
}DB_REQUEST_STATUS;


typedef enum
{
    DB_EVENT_ID_INVALID            = 0x00,
    DB_EVENT_ID_VPOINT_UPDATE,
    DB_EVENT_ID_INPUT_UPDATE
}DB_EVENT_ID;

struct Value;
struct Timestamp ;
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */




/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
const lu_char_t*    getRequestName(DB_REQUEST_ID id);
const lu_char_t*    getEventName  (DB_EVENT_ID   id);
const lu_char_t*    getPointTypeName  (VALUE_TYPE   type);
const std::string   pointValueToStr (Value& value, VALUE_TYPE type);
void getTimestamp(Timestamp& ts);

template <class T>
static inline std::string to_string(const T &t)
{
    std::stringstream ss;
    ss << t;
    return ss.str();
}


/* Database entries reference. */
struct Reference
{
    lu_uint16_t     group;
    lu_uint16_t     id;
public:
    std::string toString()
    {
        std::ostringstream ss;
        ss << "[" << group << "," << id <<"]";
        return ss.str();
    }
};

struct Timestamp
{
    timespec value;

public:
    std::string toString()
    {
        struct tm t;
        lu_char_t buf[40];

        localtime_r(&value.tv_sec, &t);
        strftime(buf,30, "%F %H:%M:%S", &t);
        snprintf(&buf[strlen(buf)], 10, ".%06ld", value.tv_nsec/1000000);
        return buf;
    }
};

struct Quality
{
    lu_bool_t online:1;      /* Online flag*/
    lu_bool_t unused:7;
};
struct Value
{
   VALUE_TYPE      type;
   union
   {
       lu_float32_t analogue  ;
       lu_int32_t   digital ;
   };

    std::string toString()
    {
        std::ostringstream ss;

        switch (type)
        {
            case VALUE_TYPE_ANALOGUE:
                ss << to_string(analogue) <<"(analogue)";
            break;
            case VALUE_TYPE_DIGITAL:
                ss << to_string(digital) <<"(digital)";
            break;
            default:
                ss << to_string("?");
        }
        return ss.str();
    }
};

struct PointEvent
{
    Reference       ref;
    Quality         quality;
    Value           value;
    Timestamp       timestamp;
};

struct InputEvent
{
    lu_uint16_t     id;
    Quality         quality;
    Value           value;
    Timestamp       timestamp;
};


struct DBEvent
{
    DB_EVENT_ID     eventId;
    lu_char_t       databaseId[DB_ID_LENGHT + 1]; /*Database identifier*/
    union
    {
        PointEvent  pointEvent;
        InputEvent  inputEvent;
    };

public:
    DBEvent()
    {
        memset(this,0, sizeof(*this));
    }

public:
    std::string toString()
    {
        std::ostringstream ss;
        ss <<"=== DBEvent Content==="
           << std::endl<< std::setw(10) << "Event:"       << getEventName(eventId)
           << std::endl<< std::setw(10) << "Point:"       << pointEvent.ref.toString().c_str()
           << std::endl<< std::setw(10) << "Value:"       << pointEvent.value.toString()
           << std::endl<< std::setw(10) << "Online:"      << (pointEvent.quality.online ?"true":"false")
           << std::endl<< std::setw(10) << "timestamp:"   << pointEvent.timestamp.toString()
           << std::endl;

        return ss.str();
    }

};

struct DBQuery
{

    lu_char_t           databaseId[DB_ID_LENGHT + 1]; /*Database identifier*/
    DB_REQUEST_ID       reqId;
    DB_REQUEST_STATUS   reqStatus;
    Reference           ref;
    Quality             quality;
    Value               value;
    Timestamp           timestamp;

    DBQuery()
    {
        memset(this,0, sizeof(*this));
    }

private:

public:
    std::string toString()
    {
        std::ostringstream ss;
        if(reqStatus == DB_REQUEST_STATUS_OK)
        {
        ss <<"=== DBQuery Content==="
           << std::endl<< std::setw(10) << "Request:"     << getRequestName(reqId)
           << std::endl<< std::setw(10) << "Point:"       << ref.toString().c_str()
           << std::endl<< std::setw(10) << "Value:"       << value.toString()
           << std::endl<< std::setw(10) << "Online:"      << (quality.online ?"true":"false")
           << std::endl<< std::setw(10) << "timestamp:"   << timestamp.toString()
           << std::endl;
        }
        else
        {
            ss << "Invalid Request[Status:" <<  reqStatus << "]" << std::endl;
        }


        return ss.str();
    }

};

inline const lu_char_t* getRequestName(DB_REQUEST_ID id)
{
    switch(id)
    {
        case DB_REQUEST_ID_INVALID    : return "Invalid Request";
        case DB_REQUEST_ID_READ_POINT : return "Read Point";
        case DB_REQUEST_ID_WRITE_POINT: return "Write Point";
        case DB_REQUEST_ID_CONTROL    : return "Control";
        case DB_REQUEST_ID_GET_CONSTANT: return "Constant";
        case DB_REQUEST_ID_GET_INPUT: return "Input";
        case DB_REQUEST_ID_SET_OUTPUT: return "Output";
        default                       : return "Unknown Request Name";
    }

}

inline const lu_char_t* getEventName  (DB_EVENT_ID   id)
{
    switch(id)
    {
        case DB_EVENT_ID_INVALID      : return "Invalid Event";
        case DB_EVENT_ID_VPOINT_UPDATE : return "Point Update Event";
        default                       : return "Unsupported Event";
    }
}

inline const lu_char_t* getPointTypeName  (VALUE_TYPE   type)
{
    switch(type)
    {
        case VALUE_TYPE_ANALOGUE : return "Analogue";
        case VALUE_TYPE_DIGITAL: return "Digital";
        default : return "Unknown";
    }
}


inline void getTimestamp(Timestamp& ts)
{
    clock_gettime(CLOCK_REALTIME, &ts.value);
}


} /** End of name space "g3db" */

#endif /* G3DATABASEMESSAGE_H_ */

/*
 *********************** End of file ******************************************
 */
