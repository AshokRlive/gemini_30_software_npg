/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:G3DBServer.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   6 Jul 2017     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef COMMON_G3DBSERVER_SRC_G3DBSERVER_H_
#define COMMON_G3DBSERVER_SRC_G3DBSERVER_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <pthread.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "zmq.hpp"
#include "lu_types.h"
#include "../include/G3DBProtocol.h"

namespace g3db
{

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * Interface of request handler.
 */
class IRequestHandler
{
public:
    virtual ~IRequestHandler(){}

    /**
     *  Handles a query.
     *  Returns true if the query has been processed.
     */
    virtual lu_bool_t handle(DBQuery& query) = 0;
};

class G3DBServer
{
public:
    G3DBServer(zmq::context_t&  ctxt);
    virtual ~G3DBServer();

    void start();
    void stop();
    void publish(DBEvent& event);
    void receiveRequest();
    void registerHandler(IRequestHandler* handler);
    void deregisterHandler(IRequestHandler* handler);

private:
    void startRequestHandlingThread();
    static void* requestHandlingThreadRoutine(void*);


    pthread_mutex_t     m_lock;
    pthread_t           m_thread;
    zmq::context_t&     m_ctxt;
    zmq::socket_t*      mp_listener;
    zmq::socket_t*      mp_publisher;
    std::vector<IRequestHandler*> m_handlers;
    };


} // end of namespace g3db

#endif /* COMMON_G3DBSERVER_SRC_G3DBSERVER_H_ */

/*
 *********************** End of file ******************************************
 */
