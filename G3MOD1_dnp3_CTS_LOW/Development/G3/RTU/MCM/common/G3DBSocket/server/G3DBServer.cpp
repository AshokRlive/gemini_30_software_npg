/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:G3DBServer.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   6 Jul 2017     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <zmq.hpp>
#include "zhelpers.hpp"

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "G3DBServer.h"
#include "timeOperations.h"
#include "LockingMutex.h"
#include "G3DBProtocol.h"
#include "../../BaseReuse/include/dbg.h"

namespace g3db
{
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define TITLE "G3DBServer >>"
/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static zmq::socket_t* s_createPubSocket(zmq::context_t& ctxt);
static zmq::socket_t* s_createListenerSocket(zmq::context_t& ctxt);
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
G3DBServer::G3DBServer(zmq::context_t&  ctxt):
                m_lock(),
                m_thread(0),
                m_ctxt(ctxt),
                mp_listener(NULL),
                mp_publisher(NULL)

{
    /* Initializes mutex to be reentrant*/
    pthread_mutexattr_t Attr;
    pthread_mutexattr_init(&Attr);
    pthread_mutexattr_settype(&Attr, PTHREAD_MUTEX_RECURSIVE);
    pthread_mutex_init(&m_lock, &Attr);
}

G3DBServer::~G3DBServer()
{
    m_handlers.clear();
    stop();
    delete mp_listener;
    delete mp_publisher;
}

void G3DBServer::registerHandler(IRequestHandler* handler)
{
    m_handlers.push_back(handler);
}

void G3DBServer::deregisterHandler(IRequestHandler* handler)
{
    m_handlers.erase(std::remove(m_handlers.begin(), m_handlers.end(), handler), m_handlers.end());
}

void G3DBServer::start()
{
    pthread_mutex_lock(&m_lock);
    if(mp_listener == NULL)
        mp_listener = s_createListenerSocket(m_ctxt);

    if(mp_publisher == NULL)
        mp_publisher = s_createPubSocket(m_ctxt);

    if(m_thread == 0)
    {
        startRequestHandlingThread();
    }

    pthread_mutex_unlock(&m_lock);
}

void G3DBServer::stop()
{
    pthread_mutex_lock(&m_lock);

    if(m_thread > 0 )
    {

        if(pthread_cancel(m_thread) == 0)
            m_thread = 0;
        else
           DBG_ERR("%s failed to cancel thread",TITLE);

        delete mp_listener;
        delete mp_publisher;
        mp_publisher = NULL;
        mp_listener = NULL;
    }

    pthread_mutex_unlock(&m_lock);
}

void G3DBServer::publish(DBEvent& event)
{
    pthread_mutex_lock(&m_lock);
    try
    {
        if (mp_publisher != NULL)
        {
            zmq::message_t message(sizeof(event));
            memcpy(message.data(), &event, sizeof(event));
            mp_publisher->send(message);
            //printf("%s Published event. eventId:%d\n", TITLE, event.eventId);

        }

    }
    catch (std::exception& e)
    {
        DBG_ERR("%s [Publish]exception caught:%s", TITLE, e.what());
    }
    pthread_mutex_unlock(&m_lock);
}

void G3DBServer::receiveRequest()
{
    try
    {
        DBQuery query;
        zmq::message_t message(sizeof(query));

        //  Wait for next request from client
        if(mp_listener->recv(&message, ZMQ_NOBLOCK)) {

            lu_bool_t processed = LU_FALSE;
            if(sizeof(query) == message.size())
            {
                // Get query from message
                memcpy(&query, message.data(), sizeof(query));
                DBG_INFO("%s received DB request:%s \n%s",TITLE, g3db::getRequestName(query.reqId),query.toString().c_str());

                for(std::vector<IRequestHandler*>::iterator it = m_handlers.begin(); it != m_handlers.end(); ++it)
                {
                    if((*it)->handle(query))
                    {
                        processed = LU_TRUE;
                        break;
                    }
                }

                if(processed == LU_FALSE)
                {
                    query.reqStatus = DB_REQUEST_STATUS_NOT_PROC;
                    DBG_ERR("%s request(%s) not processed!",TITLE, g3db::getRequestName(query.reqId));
                }
            }
            else
            {
                DBG_ERR("%s malformed DB request!!", TITLE);
                query.reqStatus = DB_REQUEST_STATUS_INVALID_REQUEST;
            }

            // Copy reply to message
            memcpy(message.data(), &query, sizeof(query));

            //Send reply back to client
            if(mp_listener->send(message))
                DBG_INFO("%s response sent \n%s", TITLE,query.toString().c_str());
            else
                DBG_ERR("%s response NOT sent",TITLE);
        }

    }
    catch (std::exception& e)
    {
        DBG_ERR("%s [RecvRequest]exception caught:%s\n", TITLE, e.what());
        delete mp_listener;
        mp_listener = s_createListenerSocket(m_ctxt);
    }
}
/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
static zmq::socket_t* s_createListenerSocket(zmq::context_t& ctxt)
{
   //  Configure socket to not wait at close time
   lu_int32_t linger = 0;
   lu_uint32_t timout = 100; //ms
   zmq::socket_t* rep;
   rep = new zmq::socket_t(ctxt,ZMQ_REP);
   rep->setsockopt (ZMQ_LINGER, &linger, sizeof (linger));
   rep->setsockopt (ZMQ_RCVTIMEO, &timout, sizeof (timout));
   rep->bind (G3DB_SERVER_REQ);
   return rep;
}

static zmq::socket_t* s_createPubSocket(zmq::context_t& ctxt)
{
   //  Configure socket to not wait at close time
   lu_int32_t linger = 0;
   lu_uint32_t timout = 3000; //ms
   zmq::socket_t* pub;

   pub = new zmq::socket_t(ctxt,ZMQ_PUB);
   pub->setsockopt (ZMQ_LINGER, &linger, sizeof (linger));
   pub->setsockopt (ZMQ_RCVTIMEO, &timout, sizeof (timout));
   pub->bind (G3DB_SERVER_SUB);
   return pub;
}


void G3DBServer::startRequestHandlingThread()
{
    int rc;
    pthread_attr_t attr; // thread attribute
    // set thread detachstate attribute to DETACHED
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

    /* Run debug server in a separate thread*/
    rc = pthread_create(&m_thread, &attr, &(G3DBServer::requestHandlingThreadRoutine), this);
    if (rc)
    {
        DBG_ERR("return code from pthread_create() is %d", rc);
    }
}

void* G3DBServer::requestHandlingThreadRoutine(void* owner)
{
    G3DBServer* server = (G3DBServer*)owner;
    DBG_INFO("%s G3DBServer thread is running", TITLE);

    while(true)
    {
        server->receiveRequest();
        lucy_usleep(10000); //In microsecs -- delay as thread cancellation point.
    }

    return NULL;
}


} // end of namespace g3db
/*
 *********************** End of file ******************************************
 */
