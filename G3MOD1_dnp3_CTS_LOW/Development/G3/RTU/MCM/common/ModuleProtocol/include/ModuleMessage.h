/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_47DD2598_6691_4a81_B22D_DB3D42EE67E1__INCLUDED_)
#define EA_47DD2598_6691_4a81_B22D_DB3D42EE67E1__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <sys/time.h>
#include <iostream>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "RAWBuffer.h"
#include "ModuleProtocol.h"
#include "StringUtil.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

struct ModuleMessageHeader
{
public:
    MODULE_MSG_TYPE messageType;
    MODULE_MSG_ID 	messageID;
    MODULE 			destination;
    MODULE_ID 		destinationID;
    MODULE			source;
    MODULE_ID 		sourceID;
    struct timeval  timestamp;

    std::string toString()
    {
        std::stringstream ss;
        ss << "msgType:"<< to_string(messageType)
           << " msgID:"<< to_string(messageID)
           << " src:" << to_string(source)
           << " srcID:" << to_string(sourceID)
           << " dest:" << to_string(destination)
           << " destID:" << to_string(destinationID);

        return ss.str();
    }
};

typedef enum
{
    MODULE_ERROR_NONE = 0,
    MODULE_ERROR_PARAM   ,

    MODULE_ERROR_LAST
}MODULE_ERROR;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class ModuleMessage
{

public:
    ModuleMessage() {};
    virtual ~ModuleMessage() {};

    virtual MODULE_ERROR getPayload(PayloadRAW *payload) = 0;

    ModuleMessageHeader header;
};


#endif // !defined(EA_47DD2598_6691_4a81_B22D_DB3D42EE67E1__INCLUDED_)

/*
 *********************** End of file ******************************************
 */








