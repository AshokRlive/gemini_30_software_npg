/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       RAW Module Message interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   22/06/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_DB165774_EA48_48ee_87ED_6A056695BEC3__INCLUDED_)
#define EA_DB165774_EA48_48ee_87ED_6A056695BEC3__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stddef.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "ModuleMessage.h"
#include "CANProtocol.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */


/**
 * Use this message when the message type in unknown. It is typically used in the
 * CANFraming module.
 */
class RAWSmallModuleMessage : public ModuleMessage
{

public:
    RAWSmallModuleMessage(lu_uint32_t size = CAN_MAX_PAYLOAD)
    {
        if(size < CAN_MAX_PAYLOAD)
        {
            payloadLen = size;
        }
        else
        {
            payloadLen = CAN_MAX_PAYLOAD;
        }
    };

    virtual ~RAWSmallModuleMessage() {};

    virtual MODULE_ERROR getPayload(PayloadRAW *payload)
    {
        if(payload == NULL)
        {
            return MODULE_ERROR_PARAM;
        }

        payload->payloadPtr = this->payload;
        payload->payloadLen = payloadLen;

        return MODULE_ERROR_NONE;
    }

    /**
     * RAW Payload.
     */
    lu_uint8_t payload[CAN_MAX_PAYLOAD];
    /**
     * Payload length
     */
    lu_uint32_t payloadLen;

};


/**
 * Use this message when the message type in unknown. It is typically used in the
 * CANFraming module.
 */
class RAWLargeModuleMessage : public ModuleMessage
{

public:
    RAWLargeModuleMessage(lu_uint32_t size = MODULE_MESSAGE_LENGTH)
    {
        if(size < MODULE_MESSAGE_LENGTH)
        {
            payloadLen = size;
        }
        else
        {
            payloadLen = MODULE_MESSAGE_LENGTH;
        }
    };

    virtual ~RAWLargeModuleMessage() {};

    virtual MODULE_ERROR getPayload(PayloadRAW *payload)
    {
        if(payload == NULL)
        {
            return MODULE_ERROR_PARAM;
        }

        payload->payloadPtr = this->payload;
        payload->payloadLen = payloadLen;

        return MODULE_ERROR_NONE;
    }

    /**
     * RAW Payload.
     */
    lu_uint8_t payload[MODULE_MESSAGE_LENGTH];
    /**
     * Payload length
     */
    lu_uint32_t payloadLen;

};

/**
 * Wrapper for a PayloadRAW structure
 */
class RAWModuleMessage : public ModuleMessage
{
public:
    RAWModuleMessage(PayloadRAW *payload) : payloadPtr(payload) {};

    virtual ~RAWModuleMessage() {};

    virtual MODULE_ERROR getPayload(PayloadRAW *payload)
    {
        if(payload == NULL)
        {
            payload->payloadPtr = NULL;
            payload->payloadLen = 0;
        }
        else
        {
            payload->payloadPtr = payloadPtr->payloadPtr;
            payload->payloadLen = payloadPtr->payloadLen;
        }

        return MODULE_ERROR_NONE;
    }

    /**
     * Pointer to RAW payload
     */
    PayloadRAW *payloadPtr;
};

#endif // !defined(EA_DB165774_EA48_48ee_87ED_6A056695BEC3__INCLUDED_)

/*
 *********************** End of file ******************************************
 */




