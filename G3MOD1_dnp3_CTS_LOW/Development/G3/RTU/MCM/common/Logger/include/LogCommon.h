/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: LogCommon.cpp 21-Nov-2012 11:41:21 pueyos_a $
 *               $HeadURL: Y:\workspace\G3Trunk\G3\RTU\MCM\src\Syslog\src\LogCommon.cpp $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Common definitions for Logging actions.
 *
 *    CURRENT REVISION
 *
 *               $Revision:     $: (Revision of last commit)
 *               $Author: pueyos_a  $: (Author of last commit)
 *       \date   $Date: 21-Nov-2012 11:41:21    $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   21-Nov-2012 11:41:21  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_DB310C48_AC8F_48a5_8EFD_1FE353CD4B84__INCLUDED_)
#define EA_DB310C48_AC8F_48a5_8EFD_1FE353CD4B84__INCLUDED_



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/* --- Time reference --- */

struct TimeMilisStr
{
    lu_uint32_t sec;     //seconds from the Epoch (1-1-1970)
    lu_uint32_t msec;    //milliseconds
};

struct TimeSpecStr
{
    lu_uint32_t sec;     //seconds from the Epoch (1-1-1970)
    lu_uint32_t nsec;    //nanoseconds
};

/**
 * Special "Dirty keeper" Log List position control class.
 *
 * This class keeps index positions for the log as an usual list handler,
 * but keeps the older entry just for restarting the reading of the list;
 * this makes the extract index extractPos more of a reading index since
 * extracted entries are not deleted.
 * It does not add any mutex control to allow a more flexible control,
 * specially between writing/reading elements and increasing insert/extract
 * positions. User should add it later if needed.
 * Older position control is usually at position 0 at first pass (log not full),
 * or at the insert position (log full).
 *
 * Usage:
 *  - initialise with constructor, like LogPosCtrlStr(MYMAXENTRIES)
 *  - write any content in the insert position, then call incInsertPos() to
 *      increase insert position and update the rest accordingly.
 *  - read any value from the extract position, then call incExtractPos() to
 *      increase extract position and update the rest accordingly.
 *  - If needed, reset the extract position to the older entry available by
 *      calling beginPos(). Extract position and log size will be updated.
 *  Please take care of not doing inadvertently:
 *  - extraction operations between writing and incInsertPos().
 *  - insertion operations between reading incExtractPos().
 */
class LogPosCtrlStr
{
public:
    /* Most members are for reading only. They are public to allow direct setting
     * for block operations (as in reading blocks of entries from files).
     */
    lu_uint32_t insertPos;  //insert position
    lu_uint32_t extractPos; //extract position
    lu_uint32_t olderPos;   //older entry at the buffer
    lu_uint32_t logSize;    //amount of entries available for read (from extractPos to insertPos)
    lu_uint32_t maxEntries; //maximum number of entries
    lu_uint32_t allLogSize; //total amount of all entries (from olderPos to insertPos)

public:
    /**
     * \brief Custom constructor
     *
     * \param Max number of entries allowed in the list.
     *
     * \return none
     */
    LogPosCtrlStr(lu_uint32_t maxLogEntriesNum)
    {
        maxEntries = maxLogEntriesNum;
        clearPos();
    }

    virtual ~LogPosCtrlStr() {};

    /**
     * \brief Set the indexes back to the initial state (clear list)
     *
     * Clears a list by setting indexes at the initial states.
     *
     * \return error code
     */
    virtual void clearPos()
    {
        logSize = 0;
        olderPos = 0;
        insertPos = 0;
        extractPos = 0;
        allLogSize = 0;
    }

    /**
     * \brief Read the list back from the oldest entry
     *
     * Sets the extract index to the older entry index.
     *
     * \return error code
     */
    virtual void beginPos()
    {
        extractPos = olderPos;
        //adjust new log size (from olderPos to insertPos)
        logSize = allLogSize;
    }

    /**
     * \brief Increase insert index
     *
     * Increases insert index, caring to be circular and displacing the
     * extract index if needed; in that case, it is effectively discarding
     * the older entry. The older index is updated as well.
     *
     * \return error code
     */
    virtual lu_int32_t incInsertPos()
    {
        lu_uint32_t oldinsertPos = insertPos;   //keep older position for comparison
        insertPos = (insertPos + 1) % maxEntries;

        if( (oldinsertPos == extractPos) && (logSize == maxEntries) )
        {
            //overlapping: increase extract index
            extractPos = insertPos; //discard one entry
        }
        ++logSize; //increase amount of elements available for read
        logSize = (logSize > maxEntries)? maxEntries : logSize;
        if( (oldinsertPos == olderPos) && (allLogSize == maxEntries) )
        {
            //overlapping: increase older entry index
            olderPos = insertPos; //discard one entry
        }
        else
        {
            ++allLogSize; //increase amount of total elements
            allLogSize = (allLogSize > maxEntries)? maxEntries : allLogSize;
        }
        return 0;
    }

    /**
     * \brief Increase extract index
     *
     * Increases extract index, caring to be circular and displacing the
     * extract index if needed.
     *
     * \return error code
     */
    virtual lu_int32_t incExtractPos()
    {
        if (logSize > 0)
        {
            extractPos = (extractPos + 1) % maxEntries;  //move to next
            --logSize; //extracted, then decrease amount of entries in the list
            return 0;
        }
        return 1;   //empty list
    }
};


#endif // !defined(EA_DB310C48_AC8F_48a5_8EFD_1FE353CD4B84__INCLUDED_)

/*
 *********************** End of file ******************************************
 */

