/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: Logger.h 18 Sep 2013 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/common/Logger/include/Logger.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 18 Sep 2013 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   18 Sep 2013   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef LOGGER_H__INCLUDED
#define LOGGER_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdarg.h>
#include <string.h> //use of strrchr()
#include <string>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "LogLevelDef.h"
#include "Mutex.h"
#include "QueueMgr.h"
#include "QueueObj.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
typedef enum
{
    LOG_ERROR_NONE = 0,
    LOG_ERROR_GENERIC, //generic error
    LOG_ERROR_INIT, //generic error
    LOG_ERROR_PARAM  ,
    LOG_ERROR_INVALID_SUBSYSTEM, //incorrect subsystem number specified
    LOG_ERROR_INVALID_LOGLEVEL, //incorrect log level number specified
    LOG_ERROR_LAST
}LOG_ERROR;


#ifndef __AT__
#define __AT__ ""
#endif

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class BufferLogger;

/**
 * \brief Observer for new Log entries
 */
class LoggerObserver : public QueueObj<LoggerObserver>
{
public:
    LoggerObserver() {};
    virtual ~LoggerObserver() {};

    /**
     * \brief notify that a new log entry has arrived
     *
     * Note that the observer must handle this quickly as it could delay several
     * other threads if locked.
     *
     * \param subSystem subsystem generating the log message
     * \param level Log level of the message
     * \param msg Format specifier for the new log entry.
     * \param args Argument's list for the new log entry.
     */
    virtual void update(const SUBSYSTEM_ID subsystem,
                        const LOG_LEVEL level,
                        const lu_char_t *msg,
                        va_list args) = 0;
};


/**
 * \brief Observer for Log status
 */
class LoggerStatusObserver : public QueueObj<LoggerStatusObserver>
{
public:
    LoggerStatusObserver() {};
    virtual ~LoggerStatusObserver() {};

    /**
     * \brief notify that any log status has changed (log level changed)
     *
     * Note that the observer must handle this quickly.
     */
    virtual void update() = 0;
};


/*!
 *   \brief Maintains and logs messages to the Application Log
 *
 *   Logs are stored in the designed facility.
 *
 * This class is a singleton.
 */
class Logger
{
public:
    /**
      * \brief Factory method: return the appropriate Logger.
      *
      * \param subSystem The associated subsystem for the desired Logger
      *
      * \return Reference of the specified Logger. SUBSYSTEM_ID_MAIN Logger by default
      */
    static Logger& getLogger(const SUBSYSTEM_ID subSystem, const lu_char_t* identity = NULL);

    /**
      * \brief Factory method: return the associated Buffer Logger.
      *
      * Buffer Logger start working only after this method is called.
      * If Buffer Logger functionality is not needed, just do not call this.
      *
      * \return Reference of the associated Buffer Logger.
      */
    static BufferLogger& getBufferLogger();

     /**
      * \brief Factory method: Set the same log level for ALL the subsystems
      *
      * \param loglevel Log level to set
      *
      * \return Error code
      */
    static LOG_ERROR setAllLogLevel(const LOG_LEVEL loglevel);

     /**
      * \brief Factory method: Get the log level by subsystem ID.
      *
      * \param subSystem The associated subsystem of the desired Logger
      *
      * \return Error code
      */
    static LOG_LEVEL getLogLevel(const SUBSYSTEM_ID subSystem);

    /**
     * \brief Factory method: Get the name associated to a log level.
     *
     * \param level The log level ID.
     *
     * \return Pointer to a C string containing the name.
     */
    static const lu_char_t* getLogLevelName(LOG_LEVEL level);

    /**
     * \brief Factory method: Get the name associated to a subsystem ID.
     *
     * \param subSystem The subsystem ID.
     *
     * \return Pointer to a C string containing the name.
     */
    static const lu_char_t* getSubSystemName(SUBSYSTEM_ID subsystem);

    /**
     * \brief Configure the log level of this logger
     *
     * \param loglevel Log level to set in this subsystem (this logger).
     */
    LOG_ERROR setLogLevel(LOG_LEVEL loglevel);

    /**
     * \brief Get the configured log level of this logger
     *
     * \return Log level of this subsystem (this logger).
     */
    LOG_LEVEL getLogLevel();

    /**
     * \brief Register/de-register observers
     * \return LU_TRUE if the object is added successfully
     */
    static lu_bool_t attach(LoggerObserver& observer);
    static void detach(LoggerObserver& observer);
    static lu_bool_t attach(LoggerStatusObserver& observer);
    static void detach(LoggerStatusObserver& observer);


    /**
     * \brief Check if log level is set to debug
     *
     * This could help to avoid some code execution made only for debugging
     * purposes.
     *
     * \return LU_TRUE when log level is set to debug
     */
    lu_bool_t isDebugEnabled();

    /**
     * Log a message
     *
     * Use it like printf(), and choose the one which fits with the desired
     * log level.
     */
    virtual void fatal(const lu_char_t *msg, ...);
    virtual void error(const lu_char_t *msg, ...);
    virtual void warn(const lu_char_t *msg, ...);
    virtual void info(const lu_char_t *msg, ...);
    virtual void debug(const lu_char_t *msg, ...);

private:
    static void init(const lu_char_t* identity = NULL);

    /**
     * Custom constructor
     *
     * \param facility Storage facility descriptor
     * \param subsystem Log Subsystem related to this Logger
     */
    Logger();
    Logger(const lu_int32_t facility,
            const SUBSYSTEM_ID subsystem,
            const lu_char_t* identity,
            const LOG_LEVEL level = LOG_LEVEL_WARNING
            );
    virtual ~Logger();

    /*
     * Private copy constructor
     */
    Logger(const Logger &logg) {LU_UNUSED(logg);};


    /**
     * \brief Logs a message from the given parameter list
     *
     * \param level Log level
     * \param msg Format specifier
     * \param args Argument's list
     */
    void vlogMsg(LOG_LEVEL level, const lu_char_t *msg, va_list args);

    /**
     * \brief Update data for the attached observers
     *
     * \param subSystem subsystem generating the log message
     * \param level Log level of the message
     * \param msg Format specifier
     * \param args Argument's list
     */
    void updateObservers(const SUBSYSTEM_ID subSystem,
                        const LOG_LEVEL level,
                        const lu_char_t *fmtPtr,
                        va_list argsList);

    /**
     * \brief Update data for the attached status observers
     */
    void updateObservers();

private:
    lu_int32_t facility;    //Storage facility descriptor
    SUBSYSTEM_ID subSystem; //Log Subsystem related to this Logger
    LOG_LEVEL logLevel;     //Log level setting. Messages with less preference will not be logged.
    lu_char_t* logID;       //Logger identity string

public:
    static Logger g_logger;
};


#endif /* LOGGER_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
