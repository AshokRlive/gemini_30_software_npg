/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/RTU/MCM/Syslog/include/SysLogFormat.h $
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Log System storage format for Log & Event entries.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: $: (Author of last commit)
 *       \date   $Date: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19/07/12      pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef SYSLOG_FORMAT_QEC190712_INCLUDED_
#define SYSLOG_FORMAT_QEC190712_INCLUDED_


#pragma pack(1) /* Force 1-byte alignment */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "LogCommon.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define LOGMAXENTRIES 200

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/**
 * \brief Log entry format
 * Log entries format to be returned by getLogMessage()
 */
typedef struct SLMLogEntryDef
{
    TimeMilisStr timestamp;  //time stamp of this entry
    lu_int8_t subsystem;    //subsystem of this entry
    lu_uint8_t level;       //log level of this entry
    lu_uint16_t size;       //text length (without '\0')
} SLLogEntryStr;


#pragma pack()

#endif /* SYSLOG_FORMAT_QEC190712_INCLUDED_ */

/*
 *********************** End of file ******************************************
 */
