/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: BufferLogger.h 19 Sep 2013 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/common/Logger/include/BufferLogger.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Buffering of Logs entries to memory
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 19 Sep 2013 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19 Sep 2013   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MEMORYLOGGER_H__INCLUDED
#define MEMORYLOGGER_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdarg.h>
#include <string>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Mutex.h"
#include "SysLogFormat.h"
#include "LogLevelDef.h"
#include "Logger.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
class LoggerObserverBuffer : public LoggerObserver
{
public:
    virtual void update(const SUBSYSTEM_ID subsystem,
                            const LOG_LEVEL level,
                            const lu_char_t *msg,
                            va_list args);
};

/*!
 *   \brief Buffers of Logs entries to RAM
 *
 *   This class stores a predefined number of entries in a buffer to allow
 *   real-time consultation of the latest log
 */
class BufferLogger : public LoggerObserver
{
private:
    //Private constructor/destructor to avoid bad usage
    BufferLogger();
    virtual ~BufferLogger();

public:
    /**
     * \brief Log a message (RAW format)
     *
     * Variable length parameters list (printf like)
     *
     * \param subsystem Source
     * \param level Log level
     * \param msg Message format -- as in printf("%i", )
     * \param args Message parameters -- as in printf( , value)
     *
     * \return none
     */
    virtual void update(const SUBSYSTEM_ID subsystem,
                            const LOG_LEVEL level,
                            const lu_char_t *msg,
                            va_list args);
    /**
     * \brief Get log entries from the internal buffer
     *
     * Write a bunch of log entries to the given buffer, up to the maximum
     * given size. If last entry does not fit, is not added.
     *
     * \param retMessage Where to copy log entries.
     * \param maxSize maximum size of the buffer.
     * \param finalSize final size written on the buffer (in bytes).
     *
     * \return Error code
     */
    virtual lu_int32_t getLogMessage(lu_char_t *retMessage,
                                        lu_uint32_t maxSize,
                                        lu_uint32_t *finalSize
                                        );

private:
    LogPosCtrlStr logPos;

    /* Log List */
    std::string logEntries[LOGMAXENTRIES];
    SLLogEntryStr logList[LOGMAXENTRIES];

    /* memory stream parameters */
    char *logBuffer;
    size_t logBufSize;
    FILE *logStream;  //stream containing the log text

    MasterMutex logInsertLock;  //mutex for inserting-related operations
    MasterMutex logExtractLock; //mutex for extracting-related operations

private:
    friend class Logger;

private:
    /**
     * Adds a log entry to the circular buffer from the stream
     *
     * Entry is added to insert position. Insertion Mutex should be set to
     * reserve access beforehand.
     */
    void addlogMessage();
};

#endif /* MEMORYLOGGER_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
