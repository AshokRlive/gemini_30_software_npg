/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: BufferLogger.cpp 19 Sep 2013 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/common/Logger/src/BufferLogger.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Buffering of Logs entries to memory
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 19 Sep 2013 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19 Sep 2013   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>  // free()
#include <time.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "BufferLogger.h"
#include "LockingMutex.h"
#include "TimeManager.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

BufferLogger::BufferLogger(): logPos(LOGMAXENTRIES),
                                logBuffer(NULL),
                                logBufSize(0)
{}


BufferLogger::~BufferLogger()
{}

void BufferLogger::update(const SUBSYSTEM_ID subsystem,
                            const LOG_LEVEL level,
                            const lu_char_t *msg,
                            va_list args)
{
    struct timespec currentTime;    //Current time assigned as time stamp
    TimeManager::getInstance().getTime(&currentTime);   //Get current time

    /* Enter critical region */
    MasterLockingMutex lMutex(logInsertLock, LU_TRUE);

    /* Prepare header */
    logList[logPos.insertPos].timestamp.sec = currentTime.tv_sec;
    logList[logPos.insertPos].timestamp.msec = currentTime.tv_nsec / 1000000;
    logList[logPos.insertPos].level = level;
    logList[logPos.insertPos].subsystem = subsystem;

    /* get message for log storing */
    logStream = open_memstream(&logBuffer, &logBufSize);
    vfprintf(logStream, msg, args); //convert format+args to string stream
    fflush(logStream);
    addlogMessage();    //store
    fclose(logStream);

    // Free the memstream
    if (logBuffer != NULL)
    {
        free(logBuffer);
        logBuffer = NULL;
    }
}

lu_int32_t BufferLogger::getLogMessage(lu_char_t *retMessage,
                                                lu_uint32_t maxSize,
                                                lu_uint32_t *finalSize
                                                )
{
    *finalSize = 0;
    if(logPos.logSize == 0)
    {
        return -1;  //message list is empty
    }

    /* extract from log buffer */
    lu_char_t *posPtr = retMessage; //points to text of the extracted log entry
    SLLogEntryStr *entryPtr;       //points to the extracted log entry
    lu_uint32_t const headSize = sizeof(SLLogEntryStr);
    lu_uint32_t rest = maxSize;     //control the space available on the buffer
    lu_uint32_t oldExtractPos;       //to check for a message bigger than the buffer
    entryPtr = reinterpret_cast<SLLogEntryStr *>(retMessage);
    /* Enter critical region for extracting */
    MasterLockingMutex lMutex(logExtractLock);
    oldExtractPos = logPos.extractPos;
    while( (logPos.logSize > 0) && (rest > (logList[logPos.extractPos].size + headSize)) )
    {
        //extract 1 entry from the list and store it in the given buffer
        *entryPtr = logList[logPos.extractPos];
        posPtr = (lu_char_t *)(entryPtr) + headSize;
        memcpy(posPtr, logEntries[logPos.extractPos].c_str(), logList[logPos.extractPos].size);
        posPtr += logList[logPos.extractPos].size;
        entryPtr = reinterpret_cast<SLLogEntryStr *>(posPtr);
        rest -= headSize + logList[logPos.extractPos].size;
        logPos.incExtractPos();
    }
    *finalSize = maxSize - rest;
    if(oldExtractPos == logPos.extractPos)
    {
        if(logList[logPos.extractPos].size > maxSize)
        {
            //Entry too long for the buffer: skip it!!!
            logPos.incExtractPos();
        }
    }
    return 0;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void BufferLogger::addlogMessage()
{
    const lu_char_t* LOGERRMESSAGE = "MemoryLogger::addlogMessage Error processing entry. A log entry has been lost.";
    lu_char_t *entry = 0;       //log entry message buffer (resized when needed)
    lu_uint32_t result;
    lu_bool_t isDeleted = LU_FALSE;

    /* extract the already formatted string */
    rewind(logStream);
    entry = new lu_char_t[logBufSize + 1]; //reserve space for entry & its terminator
    if(entry == NULL)
    {
        return;
    }
    result = fread(entry, 1, logBufSize, logStream);
    entry[logBufSize] = 0;    //add string terminator

    if(result != logBufSize)
    {
        //error happened adding a log entry: add a new log entry for that!
        delete[] entry;
        entry = (lu_char_t *)LOGERRMESSAGE;
        isDeleted = LU_TRUE;
    }


    /* insert on log buffer */
    logEntries[logPos.insertPos] = entry;
    if(logEntries[logPos.insertPos].size() > 0xFFFF)
    {
        logEntries[logPos.insertPos].resize(0xFFFF);  //truncate if too long
    }
    //store entry size
    logList[logPos.insertPos].size = logEntries[logPos.insertPos].size();
    // Enter critical region for blocking extraction
    MasterLockingMutex lMutex(logExtractLock, LU_TRUE);
    logPos.incInsertPos();
    if (isDeleted == LU_FALSE)
    {
        delete[] entry;
    }
}

/*
 *********************** End of file ******************************************
 */
