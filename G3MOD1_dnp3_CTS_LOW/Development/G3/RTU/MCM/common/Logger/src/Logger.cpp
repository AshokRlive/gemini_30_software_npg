/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id: Logger.cpp 18 Sep 2013 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/common/Logger/src/Logger.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 18 Sep 2013 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   18 Sep 2013 pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "Logger.h"
#include "LogMessage.h"
#include "GlobalDefinitions.h"
#include "LockingMutex.h"
#include "BufferLogger.h"
#include "dbg.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
//Global logger
Logger Logger::g_logger(LOG_FACILITY, SUBSYSTEM_ID_MAIN, NULL, LOG_LEVEL_INFO);

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static Logger* logger[SUBSYSTEM_ID_LAST];   //list of instances of the handlers
static BufferLogger* bufferLogger = NULL;   //Instance of the associated buffer

/* Variables shared by all Loggers */
static lu_bool_t initialised = LU_FALSE;    //Initial status of the factory
static Mutex mutexInit; //Controls concurrent access to the log at initialisation time
static MasterMutex queueMutex;       //Mutex to protect observers Queue
static MasterMutex queueStatusMutex; //Mutex to protect statusObservers Queue
static QueueMgr<LoggerObserver> observers; //Observers Queue for new log entries
static QueueMgr<LoggerStatusObserver> statusObservers; //Observers Queue for Logger status
/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

Logger& Logger::getLogger(const SUBSYSTEM_ID subSystem, const lu_char_t* identity)
{
    init(identity);
    /*Invalid subsystem or log level*/
    if(subSystem >= SUBSYSTEM_ID_LAST)
    {
        return *(logger[SUBSYSTEM_ID_MAIN]);   //by default return the basic one
    }
    return *(logger[subSystem]);
}


BufferLogger& Logger::getBufferLogger()
{
    init();
    if(bufferLogger == NULL)
    {
        /* Create and register associated BufferLogger */
        bufferLogger = new BufferLogger();
        attach(*bufferLogger);
    }
    return *bufferLogger;
}


LOG_LEVEL Logger::getLogLevel(const SUBSYSTEM_ID subSystem)
{
    return Logger::getLogger(subSystem).getLogLevel();
}

const lu_char_t* Logger::getLogLevelName(LOG_LEVEL level)
{
    const lu_char_t* name = LOG_LEVEL_ToSTRING(level);

    return (name == NULL) ? "NA" : name;
}

const lu_char_t* Logger::getSubSystemName(SUBSYSTEM_ID subsystem)
{
    const lu_char_t* name = SUBSYSTEM_ID_ToSTRING(subsystem);

    return (name == NULL) ? "NA" : name;
}


LOG_ERROR Logger::setAllLogLevel(const LOG_LEVEL loglevel)
{
    if(loglevel >= LOG_LEVEL_LAST)
    {
       return LOG_ERROR_INVALID_LOGLEVEL;
    }

    init();

    LOG_ERROR err = LOG_ERROR_NONE;
    for (lu_uint32_t idx = 0; idx < SUBSYSTEM_ID_LAST; ++idx)
    {

        LOG_ERROR ret = logger[idx]->setLogLevel(loglevel);
        if(ret != LOG_ERROR_NONE)
            err = ret;
    }
    return err;
}

LOG_ERROR Logger::setLogLevel(LOG_LEVEL loglevel)
{
    if(loglevel >= LOG_LEVEL_LAST)
    {
        return LOG_ERROR_INVALID_LOGLEVEL;
    }
    this->logLevel = loglevel;
    updateObservers();

    return LOG_ERROR_NONE;
}


LOG_LEVEL Logger::getLogLevel()
{
    return logLevel;
}


lu_bool_t Logger::attach(LoggerObserver& observer)
{
    // Protect the observer queue
    MasterLockingMutex mMutex(queueMutex, LU_TRUE);

    /* Add the observer to the observer list */
    return observers.enqueue(&observer);
}

void Logger::detach(LoggerObserver& observer)
{
    // Protect the observer queue
    MasterLockingMutex mMutex(queueMutex, LU_TRUE);

    /* Remove the observer to the observer list */
    observers.dequeue(&observer);
}


lu_bool_t Logger::attach(LoggerStatusObserver& observer)
{
    // Protect the observer queue
    MasterLockingMutex mMutex(queueStatusMutex, LU_TRUE);

    /* Add the observer to the observer list */
    return statusObservers.enqueue(&observer);
}

void Logger::detach(LoggerStatusObserver& observer)
{
    // Protect the observer queue
    MasterLockingMutex mMutex(queueStatusMutex, LU_TRUE);

    /* Remove the observer to the observer list */
    statusObservers.dequeue(&observer);
}


lu_bool_t Logger::isDebugEnabled()
{
    return (logLevel == LOG_LEVEL_DEBUG)? LU_TRUE : LU_FALSE;
}

#include <stdio.h>
void Logger::fatal(const lu_char_t *msg, ...)
{
    va_list args;
    va_start (args, msg);
    vlogMsg(LOG_LEVEL_ERR_FATAL, msg, args);
    va_end (args);
}

void Logger::error(const lu_char_t *msg, ...)
{
    va_list args;
    va_start (args, msg);
    vlogMsg(LOG_LEVEL_ERR, msg, args);
    va_end (args);
}

void Logger::warn(const lu_char_t *msg, ...)
{
    va_list args;
    va_start (args, msg);
    vlogMsg(LOG_LEVEL_WARNING, msg, args);
    va_end (args);
}

void Logger::info(const lu_char_t *msg, ...)
{
    va_list args;
    va_start (args, msg);
    vlogMsg(LOG_LEVEL_INFO, msg, args);
    va_end (args);
}

void Logger::debug(const lu_char_t *msg, ...)
{
    va_list args;
    va_start (args, msg);
    vlogMsg(LOG_LEVEL_DEBUG, msg, args);
    va_end (args);
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

Logger::Logger(const lu_int32_t gFacility,
               const SUBSYSTEM_ID gSubsystem,
               const lu_char_t* identity,
               const LOG_LEVEL level
               ) :
                            facility(gFacility),
                            subSystem(gSubsystem),
                            logLevel(level)
{
    //Note: console echo does not work as expected: do not use.
    logMessageInit(LOG_PID, facility, identity);
}


Logger::~Logger()
{
    /* Delete all observers (including the Buffer Logger) */
    {
        MasterLockingMutex mMutex(queueMutex, LU_TRUE);
        observers.empty();
    }
    {
        MasterLockingMutex sMutex(queueStatusMutex, LU_TRUE);
        statusObservers.empty();
    }

    /* Note: We do not need to delete the loggers since this destructor will be
     *      called only when closing the application -- and memory is freed
     *      automatically.
     */
}


void Logger::init(const lu_char_t* identity)
{
    LockingMutex lock(mutexInit);
    /* Create Log entities (Logger singleton instances) */
    if(initialised == LU_TRUE)
        return;

    for (lu_uint32_t idx = 0; idx < SUBSYSTEM_ID_LAST; ++idx)
    {
        logger[idx] = new Logger(LOG_FACILITY, static_cast<SUBSYSTEM_ID>(idx), identity);
    }
    initialised = LU_TRUE;
}


void Logger::vlogMsg(LOG_LEVEL level, const lu_char_t* msg, va_list args)
{
    if (level <= logLevel)
    {
        va_list copiedArgs;
        va_copy(copiedArgs, args);

        //Note: this function is already thread-safe
        vlogMessage(subSystem, level,(lu_char_t*)msg, args);
        updateObservers(subSystem, level,(lu_char_t*)msg, copiedArgs);
        va_end(copiedArgs);
    }
}


void Logger::updateObservers(const SUBSYSTEM_ID subSystem,
                            const LOG_LEVEL level,
                            const lu_char_t *message,
                            va_list argsList)
{
    LoggerObserver* observerPtr;

    //Protect the observer queue
    MasterLockingMutex mMutex(queueMutex);

    /* Notify observer list from first observer */
    observerPtr = observers.getFirst();
    while(observerPtr != NULL)
    {
        observerPtr->update(subSystem, level, message, argsList);

        /* Get next observer */
        observerPtr = observers.getNext(observerPtr);
    }
}


void Logger::updateObservers()
{
    LoggerStatusObserver* observerPtr;

    //Protect the observer queue
    MasterLockingMutex mMutex(queueStatusMutex);

    /* Notify observer list from first observer */
    observerPtr = statusObservers.getFirst();
    while(observerPtr != NULL)
    {
        observerPtr->update();

        /* Get next observer */
        observerPtr = statusObservers.getNext(observerPtr);
    }
}


/*
 *********************** End of file ******************************************
 */
