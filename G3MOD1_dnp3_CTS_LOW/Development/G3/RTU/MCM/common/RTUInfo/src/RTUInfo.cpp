/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: RTUInfo.cpp 14-Aug-2013 13:12:34 pueyos_a $
 *               $HeadURL: \\10.11.0.7\pueyos_a\workspace\app_refactor\G3\RTU\MCM\MCMApp\src\..\..\common\RTUInfo\src\RTUInfo.cpp $
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       RTUInfo module implements methods to get common basic information about the RTU.
 *
 *    CURRENT REVISION
 *
 *               $Revision:     $: (Revision of last commit)
 *               $Author: pueyos_a  $: (Author of last commit)
 *       \date   $Date: 29-Oct-2013 10:39:23    $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   29-Oct-2013 10:39:23  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <fstream>
#include <stdlib.h>
#include <sys/utsname.h>
#include <sys/sysinfo.h>

/* Net info */
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <netpacket/packet.h>   //for sockaddr_ll

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "RTUInfo.h"
#include "versions.h"
#include "StringUtil.h"
#include "svnRevision.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define SDP_VERSION_FILENAME "SDPVersion"       //File that stores SDP version number
#define CPUINFO_FILENAME "/proc/cpuinfo"        //File that contains CPU information
#define ROOTFS_FILENAME "/etc/lucy-mcm-release" //File that contains Root File System version

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static lu_uint32_t GetGatewayForInterface(const char* interface);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static std::string MCMAppVersionString; //Stores version to calculate it once
static std::string MCMUpdVersionString; //Stores version to calculate it once
static std::string OSVersionString;     //Stores version to calculate it once
static std::string RootFSVersionString; //Stores version to calculate it once
static std::string SDPVersionString;    //Stores version to calculate it once
static lu_bool_t haveCPUSerial = LU_FALSE;  //check when serial num has been calculated
static lu_uint64_t CPUSerialNum = 0;    //Stores serial to calculate it once
/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

uptimeDaysStr::uptimeDaysStr()
{
    seconds = 0;
    minutes = 0;
    hours   = 0;
    days    = 0;
}


void uptimeDaysStr::convertSeconds(const lu_uint64_t secsUptime)
{
    lu_uint64_t uptime = secsUptime;
    seconds = uptime % 60;
    uptime /= 60;
    minutes = uptime % 60;
    uptime /= 60;
    hours   = uptime % 24;
    uptime /= 24;
    days    = uptime;
}


std::string uptimeDaysStr::toString()
{
    std::ostringstream ss;
    ss << days  << " days, " <<
        std::setfill ('0') << std::setw(2) << hours   << " hours, " <<
        std::setfill ('0') << std::setw(2) << minutes << " minutes, " <<
        std::setfill ('0') << std::setw(2) << seconds << " seconds";
    return ss.str();
}

std::string uptimeDaysStr::toShortString()
{
    std::ostringstream ss;
    ss << days  << "d " <<
        std::setfill ('0') << std::setw(2) << hours   << ":" <<
        std::setfill ('0') << std::setw(2) << minutes << ":" <<
        std::setfill ('0') << std::setw(2) << seconds ;
    return ss.str();
}


std::string netConfigStr::toString(lu_uint32_t IPaddress)
{
    lu_char_t ip4[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &IPaddress, ip4, INET_ADDRSTRLEN);
    std::string result = ip4;
    return result;
}


std::string netConfigStr::toString(lu_uint8_t MACaddress[6])
{
    std::ostringstream ss;
    for (lu_uint32_t i = 0; i < 6; ++i)
    {
        ss << to_hex_string_value(MACaddress[i]);
        if(i < 5)
        {
            ss << ":";
        }
    }
    return ss.str();
}


lu_char_t getReleaseTypeSymbol(const VERSION_TYPE releaseType)
{
    //Get first char only
    lu_char_t relType = *VERSION_TYPE_ToSTRING(releaseType);
    relType=(relType=='U')? 'X' : relType;    //convert Unknown to 'X'
    return relType;
}

lu_char_t getReleaseTypeSymbol(const lu_uint8_t releaseType)
{
    return getReleaseTypeSymbol(static_cast<VERSION_TYPE>(releaseType));
}


std::string composeVersionString(const GenericVersionDef firmwareVersion)
{
    lu_char_t type; //char that represents the type of release
    type = getReleaseTypeSymbol((VERSION_TYPE)firmwareVersion.relType);
    std::ostringstream sstr;
    sstr << type << "-" <<
            static_cast<lu_uint32_t>(firmwareVersion.version.major) << "." <<
            static_cast<lu_uint32_t>(firmwareVersion.version.minor) << "." <<
            static_cast<lu_uint32_t>(firmwareVersion.patch);
    return sstr.str();
}


std::string getMCMAppVersionString()
{
    if(MCMAppVersionString.empty())
    {
        GenericVersionStr appFw;
        appFw.relType = MCM_SOFTWARE_VERSION_RELTYPE;
        appFw.version.major = MCM_SOFTWARE_VERSION_MAJOR;
        appFw.version.minor = MCM_SOFTWARE_VERSION_MINOR;
        appFw.patch = MCM_SOFTWARE_VERSION_PATCH;
        std::ostringstream sstr;
        sstr << "MCM Application firmware " <<
                composeVersionString(appFw) <<
                " svn " << static_cast<lu_uint32_t>(_SVN_REVISION) <<
                " - Lucy Electric";
        MCMAppVersionString = sstr.str();
    }
    return MCMAppVersionString;
}


std::string getMCMUpdaterVersionString()
{
    if(MCMUpdVersionString.empty())
    {
        GenericVersionStr appFw;
        appFw.relType = MCM_UPDATER_SOFTWARE_VERSION_RELTYPE;
        appFw.version.major = MCM_UPDATER_SOFTWARE_VERSION_MAJOR;
        appFw.version.minor = MCM_UPDATER_SOFTWARE_VERSION_MINOR;
        appFw.patch = MCM_UPDATER_SOFTWARE_VERSION_PATCH;
        std::ostringstream sstr;
        sstr << "MCM Updater V. " <<
                composeVersionString(appFw) <<
                " svn " << static_cast<lu_uint32_t>(_SVN_REVISION) <<
                " - Lucy Electric";
        MCMUpdVersionString = sstr.str();
    }
    return MCMUpdVersionString;
}


std::string getOSVersion()
{
    if(OSVersionString.empty())
    {
        struct utsname kernelVer;

        /* Get OS version */
        if(uname(&kernelVer) < 0)
        {
            OSVersionString = "No OS version available";    //defaul value
        }
        else
        {
            /* Compose OS version string */
            std::ostringstream sstr;
            sstr << kernelVer.sysname << " " <<
                    kernelVer.version << " Rel. " <<
                    kernelVer.release;
            OSVersionString = sstr.str();
        }
    }
    return OSVersionString;
}


std::string getRootFSVersion()
{
    if(RootFSVersionString.empty())
    {
        /* get RootFS version string */
        std::ifstream fRootFS(ROOTFS_FILENAME, std::ifstream::in);
        std::string line;

        if (fRootFS.is_open())
        {
            if( std::getline(fRootFS, line) )
            {
                RootFSVersionString = line;
            }
            else
            {
                RootFSVersionString = "No RootFS version available";    //default value
            }
            fRootFS.close();
        }
    }
    return RootFSVersionString;
}


lu_uint64_t getCPUSerialNum()
{
    if(haveCPUSerial == LU_FALSE)
    {
        /* CPU serial num */
        CPUSerialNum = 0; //by default 0 if not successful operation
        std::ifstream fCPUInfo(CPUINFO_FILENAME, std::ifstream::in);
        std::string line;

        if (fCPUInfo.is_open())
        {
            while ( std::getline(fCPUInfo, line) )
            {
                //Check line is the one we look for
                size_t pos = line.find_first_of("Serial");
                if(pos != std::string::npos)
                {
                    //Found:
                    //Note that we use length() as a max value: substr will take less chars if needed
                	lu_uint32_t start = line.find_first_not_of(" :\t", pos + 6);
                	if(start < line.length())
                	{
						std::string serial = line.substr(start, line.length());
						//convert from hex to uint64: if it fails, returns 0
						CPUSerialNum = strtoull(serial.c_str(), NULL, 16);
						break;
                	}
                }
            }
            fCPUInfo.close();
        }
    }
    return CPUSerialNum;
}


lu_uint64_t getSystemUptime()
{
    /* Get System uptime */
    struct sysinfo info;
    if(sysinfo(&info) == 0)
    {
        return info.uptime;
    }
    return 0;

}


netConfigVector getNetConfig()
{
    netConfigStr netCfg;
    std::vector <netConfigStr> nets;

    /* Get configuration of each network */
    struct ifaddrs *ifAddrStruct = NULL;    //list of net devices
    struct ifaddrs *ifa = NULL;             //IP net device iterator
    struct ifaddrs *ifb = NULL;             //MAC net device iterator
    lu_int32_t i;

    // Now get net list and search for IPv4 and MAC addresses
    if(getifaddrs(&ifAddrStruct) < 0)
        return nets;    //error

    for (ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next)
    {
        if (ifa->ifa_addr)
        {
            if(ifa->ifa_addr->sa_family == AF_INET) // check it is IP4
            {
                /* Extract net info */
                struct sockaddr_in *addrPtr = (sockaddr_in *)(ifa->ifa_addr);
                if(htonl(*((lu_uint32_t *)&(addrPtr->sin_addr))) == INADDR_LOOPBACK)
                {
                    continue;   //skip loopback
                }
                struct sockaddr_in *maskPtr = (sockaddr_in *)(ifa->ifa_netmask);
                struct sockaddr_in *broadPtr = (sockaddr_in *)(ifa->ifa_ifu.ifu_broadaddr);
                netCfg.ip = *((lu_uint32_t *)&(addrPtr->sin_addr));
                netCfg.mask = *((lu_uint32_t *)&(maskPtr->sin_addr));
                netCfg.broadcast = *((lu_uint32_t *)&(broadPtr->sin_addr));
                netCfg.gateway = GetGatewayForInterface(ifa->ifa_name);
                netCfg.devName = ifa->ifa_name;

                /* search list again for MAC address */
                for (ifb = ifAddrStruct; ifb != NULL; ifb = ifb->ifa_next)
                {
                    if (ifb->ifa_addr)
                    {
                        if(ifb->ifa_addr->sa_family == AF_PACKET) // check ethernet
                        {
                            if( netCfg.devName.compare(ifb->ifa_name) == 0)
                            {
                                struct sockaddr_ll *macPtr = (struct sockaddr_ll *)(ifb->ifa_addr);
                                for(i=0; i<6; ++i)
                                {
                                    netCfg.mac[i] = macPtr->sll_addr[i];
                                }
                            }
                        }
                    }
                }
                nets.push_back(netCfg); //Add to the list
            }
        }
    }
    if (ifAddrStruct != NULL)
    {
        freeifaddrs(ifAddrStruct);  //free memory used
    }
    return nets;
}


std::string getSDPVersion(const bool force)
{
    if(SDPVersionString.empty() || force)
    {
        std::string sdpversion;
        std::ifstream sdpfile(SDP_VERSION_FILENAME, std::ifstream::in);
        std::string line;

        if (sdpfile.is_open())
        {
          while ( std::getline(sdpfile, line) )
          {
              sdpversion += line;
              sdpversion += "\r\n";
          }
          sdpfile.close();
        }
        else
        {
            sdpversion = "Unavailable";
        }
        SDPVersionString = sdpversion;
    }
    return SDPVersionString;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
static lu_uint32_t GetGatewayForInterface(const char* interface)
{
    lu_uint32_t ret = 0;
    char cmd [1000] = {0x0};
    char line[256]={0x0};
    sprintf(cmd,"route -n | grep %s | awk '{print $2}'", interface);
    FILE* fp = popen(cmd, "r");
    if(fp == NULL)
    {
        return 0;
    }
    struct in_addr gateway;
    if(fgets(line, sizeof(line), fp) != NULL)
    {
        inet_aton(line, &gateway);
        ret = *((lu_uint32_t *)&(gateway.s_addr));
    }
    pclose(fp);
    return ret;
}


/*
 *********************** End of file ******************************************
 */

