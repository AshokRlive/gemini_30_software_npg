/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: RTUInfo.h 14-Aug-2013 13:12:34 pueyos_a $
 *               $HeadURL: \\10.11.0.7\pueyos_a\workspace\app_refactor\G3\RTU\MCM\MCMApp\src\..\..\common\RTUInfo\include\RTUInfo.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       RTUInfo header module gets common basic information about the RTU.
 *
 *    CURRENT REVISION
 *
 *               $Revision:     $: (Revision of last commit)
 *               $Author: pueyos_a  $: (Author of last commit)
 *       \date   $Date: 29-Oct-2013 10:39:23    $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   29-Oct-2013 10:39:23  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(RTUINFO__INCLUDED_)
#define RTUINFO__INCLUDED_



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <string>
#include <vector>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "versions.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/**
 * \brief Uptime values broken down to hours only.
 *
 * To offer Uptime values, is more accurate to show up to hours only, since
 * months and years calculation must take into account calendar adjustments.
 */
struct uptimeDaysStr
{
    lu_uint64_t days;
    lu_uint32_t hours;
    lu_uint32_t minutes;
    lu_uint64_t seconds;

    uptimeDaysStr();

    /*
     * Fills this structure with the given uptime (in seconds) broken down to
     * days/hours/mis/secs.
     *
     * \param amount of seconds to convert into time units.
     */
    void convertSeconds(const lu_uint64_t secsUptime);

    /**
     * \brief Get the struct values in a formatted string.
     * It follows the format "D..DD days, HH hours, MM minutes, SS seconds",
     * keeping hours, minutes and seconds in two-char format.
     * Days could be a long number.
     */
    std::string toString();

    /**
     * \brief Get the struct values in a formatted string.
     * It follows the format "D..DDd HH:MM:SS",
     * keeping hours, minutes and seconds in two-char format.
     * Days could be a long number.
     */
    std::string toShortString();

};

/**
 * \brief Overall network configuration format
 */
struct netConfigStr
{
    lu_uint32_t ip;
    lu_uint32_t mask;
    lu_uint32_t gateway;
    lu_uint32_t broadcast;
    lu_uint32_t dns1;
    lu_uint32_t dns2;
    lu_uint8_t mac[6];      //MAC address
    std::string devName;    //Device name

    /**
     * \brief Get the given values in a formatted string.
     * For IP addresses it follows the format "XXX.XXX.XXX.XXX", and for
     * MAC addresses it follows the format "MM:MM:MM:MM:MM:MM", being MM the
     * values in hexadecimal.
     */
    static std::string toString(lu_uint32_t IPaddress);
    static std::string toString(lu_uint8_t MACaddress[6]);

};

typedef std::vector<netConfigStr> netConfigVector;
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Gives the character that represents the version type from its code.
 *
 * The character is always the first letter of the version type name (as 'D' in
 * "Debug"), except in the "Unknown" case.
 *
 * \param releaseType Release type code.
 *
 * \return character that represents the release type.
 */
lu_char_t getReleaseTypeSymbol(const VERSION_TYPE releaseType);

/**
 * \brief Overloaded version of getReleaseTypeSymbol(VERSION_TYPE) to support int values.
 *
 * \param releaseType Release type code.
 *
 * \return character that represents the release type.
 */
lu_char_t getReleaseTypeSymbol(const lu_uint8_t releaseType);

/**
 * \brief Get the version string from the given version structure.
 *
 * It follows the format "X-M.m.p", being:
 *      X - release type char
 *      M - major version number
 *      m - minor version number
 *      p - patch version number
 *
 * \param kernelString It returns here the string.
 */
std::string composeVersionString(const GenericVersionDef firmwareVersion);

/**
 * \brief Get the version string for the MCM Application.
 *
 * \return Version string.
 */
std::string getMCMAppVersionString();

/**
 * \brief Get the version string for the MCM Updater.
 *
 * \return Version string.
 */
std::string getMCMUpdaterVersionString();

/**
 * \brief Get the version string from data reported from the Operating System.
 *
 * \return Version string.
 */
std::string getOSVersion();

/**
 * \brief Get the Lucy File System version.
 *
 * \param kernelString It returns here the string.
 */
std::string getRootFSVersion();

/**
 * \brief Get the CPU serial number.
 *
 * \return CPU serial as a 64-bit number -- Value 0 when error
 */
lu_uint64_t getCPUSerialNum();

/**
 * \brief Get the Operating System uptime.
 *
 * \return Amount of seconds the system is up and running.
 */
lu_uint64_t getSystemUptime();

/**
 * \brief Get the RTU Network configuration.
 *
 * \return STL vector with the array of configurations.
 */
netConfigVector getNetConfig();

/**
 * \brief Get the SDP (software Distribution Package) version string.
 *
 * \return String containing the version.
 */
std::string getSDPVersion(const bool force = false);



#endif // !defined(RTUINFO__INCLUDED_)

/*
 *********************** End of file ******************************************
 */

