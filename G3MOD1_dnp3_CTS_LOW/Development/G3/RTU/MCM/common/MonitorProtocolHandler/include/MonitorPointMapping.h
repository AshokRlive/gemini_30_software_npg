/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MonitorPointMapping.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       To map MCM Monitor Channel addresses
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   23 Dec 2013     andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef MONITORPOINTMAPPING_H_
#define MONITORPOINTMAPPING_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "MCMIOMap.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
// Channel numbers for the Monitor AI values
typedef enum
{
    MCM_MON_AI_VLOGIC_ADDR = 0,
    MCM_MON_AI_V5_0_ADDR,
    MCM_MON_AI_V3_3_ADDR,
    MCM_MON_AI_VLOCAL,
    MCM_MON_AI_VREMOTE,
    MCM_MON_AI_TEMP_ADDR,

    MCM_MON_AI_CAN0_TX_BYTES,
    MCM_MON_AI_CAN0_RX_BYTES,
    MCM_MON_AI_CAN0_USAGE,          // Virtual based on TX & RX_BYTES
    MCM_MON_AI_CAN0_TX_ERRS,
    MCM_MON_AI_CAN0_RX_ERRS,
    MCM_MON_AI_CAN0_ERRS,           // Total of TX & RX_ERRS
    MCM_MON_AI_CAN0_TX_PACKETS,
    MCM_MON_AI_CAN0_RX_PACKETS,
    MCM_MON_AI_CAN0_RXDROP,
    MCM_MON_AI_CAN0_OVERERR,

    MCM_MON_AI_CAN1_TX_BYTES,
    MCM_MON_AI_CAN1_RX_BYTES,
    MCM_MON_AI_CAN1_USAGE,          // Virtual based on TX & RX_BYTES
    MCM_MON_AI_CAN1_TX_ERRS,
    MCM_MON_AI_CAN1_RX_ERRS,
    MCM_MON_AI_CAN1_ERRS,           // Total of TX & RX_ERRS
    MCM_MON_AI_CAN1_TX_PACKETS,
    MCM_MON_AI_CAN1_RX_PACKETS,
    MCM_MON_AI_CAN1_RXDROP,
    MCM_MON_AI_CAN1_OVERERR,

    MCM_MON_AI_LAST

} MCM_MON_AI;

// Channel numbers for the Monitor DI values
typedef enum
{
    MCM_MON_DI_GPIO_LOCAL          =  0,
    MCM_MON_DI_GPIO_REMOTE,
    MCM_MON_DI_GPIO_SYS_HEALTHY,
    MCM_MON_DI_GPIO_WDOG_HANDSHAKE,
    MCM_MON_DI_GPIO_FACTORY,
    MCM_MON_DI_WDOG_FAILED,
    MCM_MON_DI_LAST
} MCM_MON_DI;

// Channel numbers for the Monitor DO values
typedef enum
{
    MCM_MON_DO_GPIO_LOCAL       =  0,
    MCM_MON_DO_GPIO_REMOTE      =  1,
    MCM_MON_DO_LAST
} MCM_MON_DO;

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


#endif /* MONITORPOINTMAPPING_H_ */

/*
 *********************** End of file ******************************************
 */
