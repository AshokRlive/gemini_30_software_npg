/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: MonitorLinkLayer.cpp 23-Aug-2013 11:38:06 andrews_s $
 *               $HeadURL: C:\sw_dev\gemini_30_software\Development\G3\RTU\MCM\Common\MonitorLinkLayer\include\MonitorLinkLayer.cpp $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MonitorLinkLayer header module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: andrews_s	$: (Author of last commit)
 *       \date   $Date: 23-Aug-2013 11:38:06	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   23-Aug-2013 11:38:06  andrews_s    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_E01959A8_5DFA_43c2_B9AE_48B6FE70B000__INCLUDED_)
#define EA_E01959A8_5DFA_43c2_B9AE_48B6FE70B000__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Thread.h"
#include "Timer.h"
#include "MonitorProtocol.h"
#include "AbstractMonitorProtocolLayer.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define SOCKET_NAME_MAX  108    //Usual UNIX_PATH_MAX is 108
#define RX_BUFF_LEN (XMSG_CONFIG_MAX_MSG_LEN * 2)    /* Maximum buffer size for storing messages */
#define TX_BUFF_LEN (XMSG_CONFIG_MAX_MSG_LEN)        /* Maximum buffer size for storing messages */

#define PING_INTERVAL_MS 1000
#define PING_TIMEOUT_MS  5000

class AbstractMonitorProtocolLayer;

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class MonitorLinkLayer : private Thread
{
public:
    /** \brief Protocol action error codes */
    typedef enum
    {
        MONLINK_ERROR_NONE         =  0,
        MONLINK_ERROR_CONNECTION   = -1,   // Wrong connection state
        MONLINK_ERROR_PROTOCOL     = -2,   // Protocol Object Error
        MONLINK_ERROR_FORMAT       = -3    // Format error in message
    } MONLINK_ERROR;

public:
    /**
     * Custom constructor
     *
     * \param schedType Scheduler type to use for the thread
     * \param priority Thread priority
     * \param sockName Socket name
     */
    MonitorLinkLayer( SCHED_TYPE           schedType,
                        lu_uint32_t        priority,
                        const lu_char_t*   sockName);

    virtual ~MonitorLinkLayer();

    /**
     * \brief Configure the related Protocol Layer
     *
     * \param monitorProtocolLayerPtr Pointer to the implementation of the Protocol Layer
     */
    void setProtocolLayer(AbstractMonitorProtocolLayer* monitorProtocolLayerPtr);


    /**
     * \brief start the Protocol Layer server
     *
     * \return operation result. LU_TRUE for successful operation
     */
    THREAD_ERR start();

    /**
     * \brief stop the Protocol Layer server
     *
     * \return operation result. LU_TRUE for successful operation
     */
    THREAD_ERR stop();

    /**
     * \brief Send a message packet on the socket
     *
     * \param message Message to be sent
     *
     * \return > 0 if bytes are sent.
     *         < 0 Error
     */
    lu_int32_t sendMessage(const AppMsgStr& message);

    /**
     * \brief Virtual function to process an incoming message
     *
     * This method could be overridden to decode the received message <msg>
     *
     * \param message Message received
     *
     * \return Error Code
     */
    virtual lu_int32_t processMessage(AppMsgStr& message);

protected:
    /**
     * \brief Thread main loop
     *
     * Main thread manages the connection to the socket and receives
     * incoming messages which are passed on to receiveMessage
     */
    void threadBody();

private:
    /**
     * \brief Connect to listening domain socket <sockName>
     *
     * \return Error code
     */
    lu_int32_t makeConnection();   // Client

    /**
     * \brief Close a socket connection
     *
     * \return None
     */
    void closeConnection();

    /**
     * \brief Sets the Connection Online Status
     *
     * If the status changes it will call notify in the attached protocol layer
     *
     * \param status Online status
     *
     * \return Error Code
     */
    void setConnectionOnlineStatus(lu_bool_t status);

    /**
     * \brief Waits for a complete packet to be received on the socket
     *
     * This method returns 0 when it has populated <rxMsg> with a valid packet.
     *
     * \param timeout Max time to wait for incoming messages.
     *
     * \return Error Code
     */
    lu_int32_t receiveMessage(lu_int32_t timeout);

    /**
     * \brief Waits for a complete chunk to be received on the socket
     *
     * This method returns 0 when it has populated <rxMsg> with a valid packet.
     *
     * \param timeout Max time to wait for incoming messages.
     *
     * \return Error Code
     */
    lu_int32_t receiveChunk(lu_int32_t chunkSize, lu_int32_t timeout);

    /**
     * \brief Set internal connection status
     *
     * \param newStatus New status to set: LU_TRUE to set to connected
     */
    void setConnection(lu_bool_t newStatus);

    /**
     * \brief Get internal connection status
     *
     * \return Connection status
     */
    lu_bool_t getConnection();

private:
    AbstractMonitorProtocolLayer* protocolLayerPtr; //Reference to the related Protocol Layer

    lu_char_t   socketName[SOCKET_NAME_MAX];
    lu_int32_t  socketHandle;

    // Used by rxMsg
    AppMsgStr   rxMsg;          //Received message
    lu_int32_t  rxBufPos;
    lu_uint8_t  rxBuffer[RX_BUFF_LEN * 2];

    Mutex       statusMutex;    //status change protected by a mutex
    lu_bool_t   connected;      //Socket status
    lu_bool_t   connectionOnlineStatus;

    Mutex       sendMutex;     // sending is done in two parts - protect this with a mutex
// TODO - SKA - receiveMutex Required?
    Mutex       receiveMutex;  // receiving is done in two parts - protect this with a mutex


};


#endif // !defined(EA_E01959A8_5DFA_43c2_B9AE_48B6FE70B000__INCLUDED_)

/*
 *********************** End of file ******************************************
 */

