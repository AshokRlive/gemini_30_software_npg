

#ifndef _MonitorProtocol_INCLUDED
#define _MonitorProtocol_INCLUDED


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdio.h>
#include <sysexits.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "versions.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions
 ******************************************************************************
 */

/* Message format used between the Monitor and the MCM Application/MCM Updater */
#define XMSG_FLAGS_REPLY          0x100    /* OR in to REPLY to a message */
#define XMSG_FLAGS_REPLY_REQUIRED 0x200    /* OR in to the REQ if a reply is expected */
#define XMSG_FLAGS_NACK           0x400    /* OR in to NACK a message */

/* Message ID used between the Monitor and the MCM Application/MCM Updater */
#define XMSG_CMD_REQ_ALIVE     (0x01 | XMSG_FLAGS_REPLY_REQUIRED)
#define XMSG_REP_REQ_ALIVE     (0x01 | XMSG_FLAGS_REPLY)
#define XMSG_CMD_REQ_EXIT      (0x02)
#define XMSG_CMD_HTTP_RESTART  (0x03)
#define XMSG_CMD_NOTIFY_EXIT   (0x04)

#define XMSG_CMD_GET_DI_VAL    (0x06 | XMSG_FLAGS_REPLY_REQUIRED)
#define XMSG_REP_GET_DI_VAL    (0x06 | XMSG_FLAGS_REPLY)
#define XMSG_CMD_GET_AI_VAL    (0x07 | XMSG_FLAGS_REPLY_REQUIRED)
#define XMSG_REP_GET_AI_VAL    (0x07 | XMSG_FLAGS_REPLY)
#define XMSG_CMD_SET_OLR_VAL   (0x08)
#define XMSG_CMD_SET_IP        (0x0B | XMSG_FLAGS_REPLY_REQUIRED)
#define XMSG_REP_SET_IP        (0x0B | XMSG_FLAGS_REPLY)
#define XMSG_CMD_SET_PPP       (0x0C | XMSG_FLAGS_REPLY_REQUIRED)
#define XMSG_REP_SET_PPP       (0x0C | XMSG_FLAGS_REPLY)
#define XMSG_CMD_SET_NTP       (0x0D | XMSG_FLAGS_REPLY_REQUIRED)
#define XMSG_REP_SET_NTP       (0x0D | XMSG_FLAGS_REPLY)
#define XMSG_CMD_GET_APP_VER   (0x0E | XMSG_FLAGS_REPLY_REQUIRED)
#define XMSG_REP_GET_APP_VER   (0x0E | XMSG_FLAGS_REPLY)
#define XMSG_CMD_SET_OK_LED    (0x0F | XMSG_FLAGS_REPLY_REQUIRED)
#define XMSG_REP_SET_OK_LED    (0x0F | XMSG_FLAGS_REPLY)
#define XMSG_CMD_SET_KICK_WDOG (0x10 | XMSG_FLAGS_REPLY_REQUIRED)
#define XMSG_REP_SET_KICK_WDOG (0x10 | XMSG_FLAGS_REPLY)

#define XMSG_STX              0x02

#define XMSG_DATA_SOCK_NAME    "/tmp/data"

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Enums
 ******************************************************************************
 */
// Thread State - Controls OK LED Flash State
typedef enum
{
    OK_LEDSTATE_HEALTHY      = 0,
    OK_LEDSTATE_SPECIAL1     = 1,  // Used as a special blink style
    OK_LEDSTATE_SPECIAL2     = 2,  // Used as a special blink style
    OK_LEDSTATE_INITIALISING = 3,
    OK_LEDSTATE_WARNING      = 4,
    OK_LEDSTATE_ALARM        = 5,
    OK_LEDSTATE_CRITICAL     = 6,
    OK_LEDSTATE_LAST
} OK_LEDSTATE;

/**
 * \brief ID of the application that is communicating
 */
typedef enum
{
    APP_ID_MCMMONITOR = 0,    /* MCM Monitor */
    APP_ID_MCMAPP     = 1,    /* MCM Application */
    APP_ID_MCMUPDATER = 2,    /* MCM Updater */
    APP_ID_MCMPLUGIN1 = 3,    /* Software Plug-in for the MCM */
    APP_ID_MCMPLUGIN2 = 4,    /* Software Plug-in for the MCM */
    APP_ID_LAST
} APP_ID;

/**
 * \brief MCM Application Exit Code Definitions
 *        These are used by the Monitor to decide what to do when a
 *        monitored application exits
 *        These codes have been selected to be outside the 'standard' range of exit codes
 *        as used in sysexits.h
 */
typedef enum
{
    APP_EXIT_CODE_RESTART               = EX_OK,          /* Restart the application that exited */
    APP_EXIT_CODE_REBOOT_RTU            = EX__MAX + 1,    /* Reboot the RTU */
    APP_EXIT_CODE_SHUTDOWN_RTU          = EX__MAX + 2,    /* Shutdown the RTU */
    APP_EXIT_CODE_START_MCM_APPLICATION = EX__MAX + 3,    /* Start the MCM Application */
    APP_EXIT_CODE_START_MCM_UPDATER     = EX__MAX + 4,    /* Start the MCM Updater */
    APP_EXIT_CODE_EXIT_MCM_MONITOR      = EX__MAX + 5,    /* Exit the Monitor (typically relaunched from /etc/inittab)*/
    APP_EXIT_CODE_START_MCM_UPDATER_HAT = EX__MAX + 6,    /* Start the MCM Updater in HAT/factory mode */
    APP_EXIT_CODE_LAST
} APP_EXIT_CODE;

/**
 * \brief Action codes for exiting/rebooting
 */
typedef enum
{
    XMSG_EXIT_ACTION_SHUTDOWN = 0,    /* Shutdown the RTU (allow engineer to remove unit) */
    XMSG_EXIT_ACTION_REBOOT   = 1,    /* Reboot the RTU */
    XMSG_EXIT_ACTION_RESTART  = 2,    /* MCM Application termination/restart */
    XMSG_EXIT_ACTION_LAST
} XMSG_EXIT_ACTION;

/**
 * \brief Reason codes for exiting/rebooting
 */
typedef enum
{
    XMSG_EXIT_CODE_OVERTEMP  = 0,      /* Temperature is getting too high */
    XMSG_EXIT_CODE_VOLTAGE_RANGE = 1,  /* Voltage out of range */
    XMSG_EXIT_CODE_WDOG_FAIL = 2,      /* Hardware watchdog stopped responding */
    XMSG_EXIT_CODE_APP_FAIL  = 3,      /* Application failure */
    XMSG_EXIT_CODE_LAST
} XMSG_EXIT_CODE;

/**
 * \brief Flags used for point status (AI & DI)
 */
typedef enum
{
    XMSG_VALUE_FLAG_ALARM        = 0x1,    /* In Alarm */
    XMSG_VALUE_FLAG_OFF_LINE     = 0x2,    /* Point off-line / not readable */
    XMSG_VALUE_FLAG_OUT_OF_RANGE = 0x4,    /* Value out of range */
    XMSG_VALUE_FLAG_FILTERED     = 0x8,    /* Value filtered */
    XMSG_VALUE_FLAG_LAST
} XMSG_VALUE_FLAG;

/**
 * \brief Values for OLR State
 */
typedef enum
{
    XMSG_VALUE_OLR_CODE_OFF    = 0,
    XMSG_VALUE_OLR_CODE_LOCAL  = 1,
    XMSG_VALUE_OLR_CODE_REMOTE = 2,
    XMSG_VALUE_OLR_CODE_ERROR  = 3,
    XMSG_VALUE_OLR_CODE_LAST
} XMSG_VALUE_OLR_CODE;


/*
 ******************************************************************************
 * EXPORTED - Structures
 ******************************************************************************
 */

#pragma pack(1)

/**
 * \brief App Protocol Message header
 */
typedef struct AppMsgHeaderDef
{
    lu_uint8_t stx;               /* STX character */
    lu_uint8_t appId;             /* Source application ID (Values are APP_ID-type) */    
    lu_uint16_t cmd;              /* Message ID (Values are XMSG_-type) */    
    BasicVersionStr apiVersion;   /* Protocol API Version */
    lu_uint32_t crc32;            /* CRC32 of payLoadLen + payLoad */    
    lu_uint16_t payLoadLen;       /* Size (in bytes) of the payload */    
} AppMsgHeaderStr;

/**
 * \brief Application Protocol Message Structure
 */
typedef struct AppMsgDef
{
    AppMsgHeaderStr  header;    /* Message common header */
    void            *payload;   /* Message specific payload */
} AppMsgStr;

/**
 * \brief Application Version Structure
 */
typedef GenericVersionStr PLAppVerStr;

/**
 * \brief Exit command requires the Application to exit, as for restart/reboot/shutdown reasons
 */
typedef struct PLExitDef
{
    lu_int8_t action;    /* Exit command (Values are XMSG_EXIT_ACTION-type) */    
    lu_int8_t reason;    /* Exit reason (Values are XMSG_EXIT_CODE-type) */    
} PLExitStr;

/**
 * \brief Client notifies Monitor its intention to exit with supplied ExitCode
 *        timeout allows Monitor to kill client if it has not exited
 */
typedef struct PLNotifyExitDef
{
    lu_int32_t    exitCode;    /* Expected Exit Code (Values are XMSG_EXIT_CODE-type) */
    lu_uint32_t   timeoutMs;   /* Timeout for Monitor to kill application if no exit occurs */
} PLNotifyExitStr;

/**
 * \brief Get Digital Input value and status
 */
typedef struct PLGetDIValDef
{
    lu_uint8_t channel;    /* Channel ID (DI address) */
    lu_uint8_t value;      /* Value */
    lu_uint8_t flags;      /* Status flags (Values are XMSG_VALUE_FLAG-type) */
} PLGetDIValStr;

/**
 * \brief Get Analogue Input value and status
 */
typedef struct PLGetAIValDef
{
    lu_uint8_t   channel;  /* Channel ID (AI address) */
    lu_float32_t value;    /* Value in IEEE 32-bit float format */    
    lu_uint8_t   flags;    /* Status flags (Values are XMSG_VALUE_FLAG-type) */
} PLGetAIValStr;

/**
 * \brief Set New IP configuration for an ethernet device
 */
typedef struct PLSetIPDef
{
    lu_uint32_t ipAddr;    /* IP address */    
    lu_uint32_t mask;      /* Net Mask */    
    lu_uint32_t gwAddr;    /* Gateway IP address */    
    lu_char_t   device[8]; /* device ID (as in 'eth0') */
} PLSetIPStr;

/**
 * \brief Set New NTP IP configuration
 */
typedef struct PLSetNTPDef
{
    lu_uint32_t serverIPAddr;    /* IP address of NTP Server*/
} PLSetNTStr;

/**
 * \brief Set the OLR State
 */
typedef struct PLOLRDef
{
    lu_uint8_t OLRState; /* OLR State (Values are XMSG_VALUE_OLR_CODE-type) */
} PLSetOLRStr;

/**
 * \brief Set the OK LED State
 */
typedef struct PLSetOKLEDDef
{
    lu_int32_t ledState;  /* Alarm status (Values are OK_LEDSTATE-type) */
} PLSetOKLEDStr;


/**
 * \brief Control the WDog kicking
 */
typedef struct PLSetKickWdogDef
{
    lu_bool_t kick;  /* True to KICK or False to STOP */
} PLSetKickWdogStr;


#pragma pack()

/* Configuration constants for the Application Protocol */
#define XMSG_CONFIG_MAX_PAYLOAD_LEN 40    /* Maximum payload size */
#define XMSG_CONFIG_MIN_MSG_LEN sizeof(AppMsgHeaderStr)    /* Minimum message size */
#define XMSG_CONFIG_HEADER_LEN  sizeof(AppMsgHeaderStr)    /* Header Length */
#define XMSG_CONFIG_MAX_MSG_LEN (XMSG_CONFIG_MIN_MSG_LEN + XMSG_CONFIG_MAX_PAYLOAD_LEN)    /* Maximum message size */


/*
 ******************************************************************************
 * EXPORTED - Typedefs
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Convert 32-bit uint IP address to char array
 *
 *   \param ip          IP Address as 32-bit uint
 *          ipPtr       Pointer to a char array
 *          ipLen       Length of the supplied char array (>=16)
 *
 *   \return IP address as a 32 nit uint
 *
 ******************************************************************************
 */
static inline lu_int32_t ipToString(lu_uint32_t ip, lu_int8_t *ipPtr, lu_uint32_t ipLen)
{
    lu_uint8_t first;
    lu_uint8_t second;
    lu_uint8_t third;
    lu_uint8_t fourth;

    if ((ipPtr == NULL) || (ipLen < 16))
    {
        return -1;
    }

    first  = (ip & 0xff000000) >> 24;
    second = (ip & 0x00ff0000) >> 16;
    third  = (ip & 0x0000ff00) >> 8;
    fourth = (ip & 0x000000ff);

    snprintf(ipPtr, (lu_uint32_t) ipLen, "%d.%d.%d.%d", first, second, third, fourth);
    ipPtr[ipLen-1] = '\0';

    return 0;
}

#endif /* _G3ConfigProtocol_INCLUDED */

/*
 *********************** End of file ******************************************
 */
