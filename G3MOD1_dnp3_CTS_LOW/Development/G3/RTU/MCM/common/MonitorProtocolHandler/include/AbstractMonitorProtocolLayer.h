/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: MonitorProtocolLayer.cpp 23-Aug-2013 11:38:15 andrews_s $
 *               $HeadURL: C:\sw_dev\gemini_30_software\Development\G3\RTU\MCM\Common\MonitorProtocol\include\AbstractMonitorProtocolLayer.cpp $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       AbstractMonitorProtocolLayer header module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: andrews_s	$: (Author of last commit)
 *       \date   $Date: 23-Aug-2013 11:38:15	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   23-Aug-2013 11:38:15  andrews_s    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_5CCAA3CD_9B33_4d0e_B77A_ABF2390B38F0__INCLUDED_)
#define EA_5CCAA3CD_9B33_4d0e_B77A_ABF2390B38F0__INCLUDED_



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Thread.h"
#include "MonitorProtocol.h"
#include "MonitorLinkLayer.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
class MonitorLinkLayer;
/**
 * Class AbstractMonitorProtocolLayer
 */
class AbstractMonitorProtocolLayer
{
public:
    /** \brief Protocol action error codes */
    typedef enum
    {
        MONPROTOCOL_ERROR_NONE   =  0,
        MONPROTOCOL_ERROR_INIT   = -1,   //Socket not initialised
        MONPROTOCOL_ERROR_SEND   = -2,   //Error sending a message
        MONPROTOCOL_ERROR_FORMAT = -3    //Format error in message
    } MONPROTOCOL_ERROR;

public:
    AbstractMonitorProtocolLayer( APP_ID appID,
                                  SCHED_TYPE   schedType,
                                  lu_uint32_t  dataSocketPriority    = Thread::PRIO_MIN + 10);

    virtual ~AbstractMonitorProtocolLayer();

	/**
	 * \brief Returns the Application's ID
	 *
	 * \return application ID
	 */
	APP_ID getAppID() { return applicationID; };

    /**
     * \brief Builds and sends a request for ai values msg
     *
     * Sends the built message down the link layer
     *
     * \param channel Channel ID to get data from
     *
     * \return Error Code
     */
    MONPROTOCOL_ERROR send_req_ai_msg(lu_uint16_t channel);

    /**
     * \brief Builds and sends a request for the application version
     *
     * Sends the built message down the link layer
     *
     * \return Error Code
     */
    MONPROTOCOL_ERROR send_req_app_ver_msg();

    /**
     * \brief Builds and sends a request for di values msg
     *
     * Sends the built message down the link layer
     *
     * \param channel Channel ID to get data from
     *
     * \return  Error Code
     */
	MONPROTOCOL_ERROR send_req_di_msg(lu_uint16_t channel);

    /**
     * \brief Builds and sends ip address configuration
     *
     * Sends the built message down the link layer
     *
     * \param   device      Interface device name (e.g. 'eth0')
     *          ipAddress   4 byte encoded ip address
     *          mask        4 byte encoded mask address
     *          gwAddress   4 byte encoded gateway address
     *
     * \return  Error Code
     */
	MONPROTOCOL_ERROR send_set_ip_msg(lu_char_t *device, lu_uint32_t ipAddress, lu_uint32_t mask, lu_uint32_t gwAddress);

	/**
     * \brief Builds and sends a ping msg
     *
     * Sends the built message down the link layer
     *
     * \param   none
     *
     * \return  Error Code
     */
    MONPROTOCOL_ERROR send_ping_msg();

    /**
     * \brief Builds and sends a request exit msg
     *
     * Sends the built message down the link layer
     *
     * \param   action  Define what type of exit (shutdown, restart etc)
     *          reason  The reason for the exit request
     *
     * \return  Error Code
     */
	MONPROTOCOL_ERROR send_req_exit(XMSG_EXIT_ACTION action, XMSG_EXIT_CODE reason);

    /**
     * \brief Builds and sends a notify exit msg
     *
     * Sends the built message down the link layer
     *
     * \param   exitCode  exit code being notified
     *          timoutMs  timeout for sending the exit code
     *
     * \return  Error Code
     */
	MONPROTOCOL_ERROR send_notify_exit(APP_EXIT_CODE exitCode, lu_uint32_t timoutMs);

    /**
     * \brief Builds and sends an HTTP Server Restart Request
     *
     * Sends the built message down the link layer
     *
     * \param   none
     *
     * \return  Error Code
     */
	MONPROTOCOL_ERROR send_http_restart();

    /**
     * \brief Builds and sends an OK LED State Request
     *
     * Note that the OK LED will reflect the worst state of
     * all contributors to its state.  e.g. We may send an OK state to
     * the LED but if it's set to WARNING elsewhere the OK LED will show
     * WARNING.
     *
     * Sends the built message down the link layer
     *
     * \param   ledState  desired OK LED State
     *
     * \return  Error Code
     */
	MONPROTOCOL_ERROR send_OK_LED_state(OK_LEDSTATE ledState);

    /**
     * \brief Builds and sends a ping reply
     *
     * Sends the built msg down the link layer
     *
     * \return  Error Code
     */
	MONPROTOCOL_ERROR send_ping_reply_msg();

    /**
     * \brief Builds and sends Off/Local/Remote state request msg
     *
     * Sends the built message down the link layer
     *
     * \param stateOLR Off/Local/Remote state to be set
     *
     * \return  Error Code
     */
    MONPROTOCOL_ERROR send_req_offLocalRemote_msg(XMSG_VALUE_OLR_CODE stateOLR);

    /**
     * \brief Builds and sends Wdog Kick control request msg
     *
     * Sends the built message down the link layer
     *
     * \param start - LU_TRUE for start, else LU_FALSE
     *
     * \return  Error Code
     */
    MONPROTOCOL_ERROR send_req_setWdogKick_msg(lu_bool_t start);

protected:
    friend class MonitorLinkLayer;

protected:
    /**
     * \brief Decodes protocol messages
     *
     * Calls the appropriate decode methods based on the msg's command code
     *
     * \return Error Code
     */
    lu_int32_t decode(AppMsgStr *buffer);

    /**
     * \brief Notifies application of new ai values
     *
     * Override in application to handle new ai values arriving
     *
     * \return None
     */
    virtual void notify_rep_ai_val(lu_uint8_t channel, lu_float32_t value, lu_uint8_t flags)
    {
        LU_UNUSED(channel);
        LU_UNUSED(value);
        LU_UNUSED(flags);
    };

    /**
     * \brief Notifies application of new di values
     *
     * Override in application to handle new di values arriving
     *
     * \return None
     */
    virtual void notify_rep_di_val(lu_uint8_t channel, lu_bool_t value, lu_uint8_t flags)
    {
        LU_UNUSED(channel);
        LU_UNUSED(value);
        LU_UNUSED(flags);
    };

    /**
     * \brief Notifies application of application version
     *
     * Override in application to use application version information
     *
     * \return None
     */
    virtual void notify_rep_app_ver(lu_uint8_t relType, BasicVersionStr version, lu_uint8_t patch)
    {
        LU_UNUSED(relType);
        LU_UNUSED(version);
        LU_UNUSED(patch);
    };

    /**
     * \brief Notifies application of a ping request
     *
     * Override if necessary; default method will usually suffice
     *
     * \return None
     */
    virtual void notify_req_ping() { send_ping_reply_msg(); };

    /**
     * \brief Notifies application of a ping reply
     *
     * Override as necessary
     *
     * \return None
     */
    virtual void notify_rep_ping() { };

    /**
     * \brief Notifies application of a connection status change
     *
     * Override as necessary
     *
     * \return None
     */
    virtual void notify_connection_status(lu_bool_t status)
    {
        LU_UNUSED(status);
    };

    /**
     * \brief Notifies application of a kick wdog reply
     *
     * Override as necessary
     *
     * \return None
     */
    void notify_rep_kick_wdog(lu_bool_t start)
    {
        LU_UNUSED(start);
    };

    /**
     * \brief Notifies application of a set ip reply
     *
     * Override as necessary
     *
     * \return None
     */
    virtual void notify_rep_set_ip(lu_bool_t success, lu_char_t *device, lu_uint32_t ipAddr, lu_uint32_t mask, lu_uint32_t gwAddr)
    {
        LU_UNUSED(success);
        LU_UNUSED(device);
        LU_UNUSED(ipAddr);
        LU_UNUSED(mask);
        LU_UNUSED(gwAddr);
    };

    /**
     * \brief Notifies application of a set ppp reply
     *
     * Override as necessary
     *
     * \return None
     */
    virtual void notify_rep_set_ppp(lu_bool_t success)
    {
        LU_UNUSED(success);
    };


    /**
     * \brief Notifies application of a request to exit
     *
     * Override as necessary
     *
     * \return None
     */
    virtual void notify_req_exit(lu_int8_t action, lu_int8_t reason)
    {
        LU_UNUSED(action);
        LU_UNUSED(reason);
    };



private:

    /**
     * \brief Inserts CRC to a completed packet
     *
     */
    MONPROTOCOL_ERROR insertCRC(AppMsgStr *msg);

    /**
     * \brief Returns cmd from msg
     *
     */
    lu_int32_t  getCmdId(AppMsgStr *msg);

    /**
     * \brief Returns pointer to the payload of a msg
     *
     */
    void       *getPayload(AppMsgStr *msg);

    /**
     * \brief Checks the message's STX & CRC
     *
     */
    MONPROTOCOL_ERROR  checkValid(AppMsgStr *message);

    /**
     * \brief Decodes incoming ai values msgs
     *
     * Forwards the decoded information to notify_rep_ai_val()
     *
     * \return Error Code
     */
    lu_int32_t decode_rep_ai_msg(AppMsgStr *msg);

    /**
     * \brief Decodes incoming di values msgs
     *
     * Forwards the decoded information to notify_rep_di_val()
     *
     * \return Error Code
     */
    lu_int32_t decode_rep_di_msg(AppMsgStr *msg);

    /**
     * \brief Decodes incoming application version msgs
     *
     * Forwards the decoded information to notify_rep_app_ver()
     *
     * \return Error Code
     */
    lu_int32_t decode_rep_app_ver_msg(AppMsgStr *msg);

    /**
     * \brief Decodes incoming ping request msgs
     *
     * Forwards the decoded information to notify_req_ping()
     *
     * \return Error Code
     */
    lu_int32_t decode_req_ping_msg();

    /**
     * \brief Decodes incoming ping reply msgs
     *
     * Forwards the decoded information to notify_rep_ping()
     *
     * \return Error Code
     */
    lu_int32_t decode_rep_ping_msg();

    /**
     * \brief Decodes incoming set ip reply msgs
     *
     * Forwards the decoded information to notify_rep_set_ip()
     *
     * \return Error Code
     */
    lu_int32_t decode_rep_set_ip(AppMsgStr *msg);

    /**
     * \brief Decodes incoming set ppp reply msgs
     *
     * Forwards the decoded information to notify_rep_set_ppp()
     *
     * \return Error Code
     */
    lu_int32_t decode_rep_set_ppp(AppMsgStr *msg);

    /**
     * \brief Decodes incoming request exit msgs
     *
     * Forwards the decoded information to notify_rep_exit()
     *
     * \return Error Code
     */
    lu_int32_t decode_req_exit(AppMsgStr *msg);

    /**
     * \brief Decodes incoming request to kick_wdog
     *
     * Forwards the decoded information to notify_rep_exit()
     *
     * \return Error Code
     */
    lu_int32_t  decode_rep_set_kick_wdog(AppMsgStr *msg);

    /**
     * \brief Decodes incoming request for power save mode
     *
     * Forwards the decoded information to notify_req_powersave()
     *
     * \return Error Code
     */
    lu_int32_t decode_rep_offLocalRemote(AppMsgStr *msg);

    /**
     * \brief Create a stub msg header
     *
     * Populates the header provided using the command and payloadLen
     *
     * \return Error Code
     */
	lu_int32_t build_header(AppMsgHeaderStr *header, lu_uint16_t command, lu_uint16_t payloadLen);

private:
    APP_ID applicationID;   //Related Application ID

    /* References to MonitorLinkLayer objects */
    MonitorLinkLayer* DataLink;

};


#endif // !defined(EA_5CCAA3CD_9B33_4d0e_B77A_ABF2390B38F0__INCLUDED_)

/*
 *********************** End of file ******************************************
 */

