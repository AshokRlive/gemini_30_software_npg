/*
 * TestMonitolLinkLayer.cpp
 *
 *  Created on: 28 Aug 2013
 *      Author: andrews_s
 */
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include "test.h"

//// ---------- DERIVED LINK LAYER ----------
//TestMonitorlLinkLayer::TestMonitorlLinkLayer( SCHED_TYPE   schedType,
//                                              lu_uint32_t  priority,
//                                              lu_char_t   *sockName) :
//                                                          MonitorLinkLayer(schedType,
//                                                                                   priority,
//                                                                                   sockName)
//{
//}
//
//TestMonitorlLinkLayer::~TestMonitorlLinkLayer()
//{
//}
//
//lu_int32_t TestMonitorlLinkLayer::processMessage(AppMsgStr& msg)
//{
//    protPtr->decode(msg);
//}

// ---------- DERIVED PROTOCOL LAYER ----------
TestMonitorProtocolLayer::TestMonitorProtocolLayer(APP_ID appID,
                                                   SCHED_TYPE  schedType) :
                        AbstractMonitorProtocolLayer(appID, schedType)
{
//    DataLink->setProtocolLayer(this);
//    ControlLink->setProtocolLayer(this);
}

TestMonitorProtocolLayer::~TestMonitorProtocolLayer()
{
}


void TestMonitorProtocolLayer::notify_rep_ai_val(lu_uint8_t channel, lu_float32_t value, lu_uint8_t flags)
{
    printf("NOTIFY AI!!!! [%d]=[%f] ", channel, value);

    if (flags & XMSG_VALUE_FLAG_ALARM)
    {
        printf("ALARM ");
    }

    if (flags & XMSG_VALUE_FLAG_OFF_LINE)
    {
        printf("OFFLINE ");
    }

    if (flags & XMSG_VALUE_FLAG_OUT_OF_RANGE)
    {
        printf("OUTOFRANGE ");
    }
    printf("\n");
}

void TestMonitorProtocolLayer::notify_rep_di_val(lu_uint8_t channel, lu_bool_t value, lu_uint8_t flags)
{
    printf("NOTIFY DI!!!! [%d]=[%d] ", channel, value);

    if (flags & XMSG_VALUE_FLAG_ALARM)
    {
        printf("ALARM ");
    }

    if (flags & XMSG_VALUE_FLAG_OFF_LINE)
    {
        printf("OFFLINE ");
    }

    if (flags & XMSG_VALUE_FLAG_OUT_OF_RANGE)
    {
        printf("OUTOFRANGE ");
    }
    printf("\n");
}

void TestMonitorProtocolLayer::notify_rep_ping()
{
    printf("NOTIFY PING REPLY!!!!\n");
}

void TestMonitorProtocolLayer::notify_req_exit(lu_int8_t action, lu_int8_t reason)
{
    printf("NOTIFY EXIT REQUEST!!!!\n");
}

void TestMonitorProtocolLayer::notify_rep_app_ver(lu_uint8_t relType, BasicVersionStr version, lu_uint8_t patch)
{
    printf("NOTIFY APP VER  V%d.%d.%d (%d)!!!!\n", version.major, version.minor, patch, relType);
}

void TestMonitorProtocolLayer::notify_connection_status(lu_bool_t status)
{
    printf("NOTIFY CONNECTION STATUS  [%s]!!!!\n", status ? "CONNECTED" : "DISCONNECTED");
}


#if 0
void TestMonitorProtocolLayer::notify_req_ping()
{
    printf("Not sending ping reply!!\n");
}
#endif


// Main Application
int main()
{
    {
        sigset_t signalMask;

        /* Work-around for signal handling in Linux implementation of POSIX
         * thread: block all signals
         */
        sigfillset (&signalMask);
        pthread_sigmask (SIG_BLOCK, &signalMask, NULL);

        /* Unblock some useful signals */
        sigemptyset(&signalMask);
        sigaddset(&signalMask, SIGINT);
        sigaddset(&signalMask, SIGTERM);
        pthread_sigmask (SIG_UNBLOCK, &signalMask, NULL);
    }


    // This call starts the communication threads on the data and control sockets
    TestMonitorProtocolLayer proto(APP_ID_MCMAPP, SCHED_TYPE_FIFO);

    lu_bool_t powerSave = LU_TRUE;

#if 1 // REPEAT TESTS
    while(1)
#endif
    {
#if 1
        // Loop around the ai points, requesting values
        for (int i = 0; i <= 5; i++)
        {
            printf("send req ai [%d]...\n", i);
            proto.send_req_ai_msg(i);
        }
        usleep(500000);
#endif

#if TEST_PING
        printf("send ping\n");
        proto.send_ping_msg();
        usleep(50000);
#endif

#if TEST_APP_VER
        printf("request app ver\n");
        proto.send_req_app_ver_msg();
        usleep(50000);
#endif

#if TEST_NOTIFY_EXIT
        printf("send notify exit [APP_EXIT_CODE_START_MCM_APPLICATION], 5 seconds\n");
        proto.send_notify_exit(APP_EXIT_CODE_START_MCM_APPLICATION, 5000);
        sleep(1);
#endif

#if TEST_REBOOT
        printf("Sending reboot request\n");
        proto.send_req_exit(XMSG_EXIT_ACTION_REBOOT, XMSG_EXIT_CODE_VOLTAGE_RANGE);
        break;
#endif

#if TEST_SHUTDOWN
        printf("Sending shutdown request\n");
        proto.send_req_exit(XMSG_EXIT_ACTION_SHUTDOWN, XMSG_EXIT_CODE_VOLTAGE_RANGE);
        break;
#endif

#if TEST_SET_IP
        printf("Setting IP address details");
        //                                       10.11.3.32  255.255.255.0 10.11.0.254
        proto.send_set_ip_msg((lu_char_t *)"eth0", 0x0a0b0320, 0xffffff00, 0x0a0b00fe);
        break;
#endif

#if TEST_HTTP_RESTART // TEST_HTTP_RESTART
        printf("Restarting HTTP service");
        proto.send_http_restart();
        break;
#endif

#if TEST_SET_OK_LED

        for (int i = 0; i < OK_LEDSTATE_LAST; i++)
        {
                printf("Setting OK LED - [%d]...\n", i);
                proto.send_OK_LED_state((OK_LEDSTATE) 1);
                sleep(4);
        }
#endif

#if 0
        printf("Setting OLR to OFF\n");
        proto.send_req_offLocalRemote_msg(XMSG_VALUE_OLR_CODE_OFF);
        sleep(1);

        printf("Setting OLR to LOCAL\n");
        proto.send_req_offLocalRemote_msg(XMSG_VALUE_OLR_CODE_LOCAL);
        sleep(1);

        printf("Setting OLR to REMOTE\n");
        proto.send_req_offLocalRemote_msg(XMSG_VALUE_OLR_CODE_REMOTE);
        sleep(1);
#endif

#if TEST_SET_SET_DO
#endif
    }

    printf("Done!\n");

    return 0;
}
