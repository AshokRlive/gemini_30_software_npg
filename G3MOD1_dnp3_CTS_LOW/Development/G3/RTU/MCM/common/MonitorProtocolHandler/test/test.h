/*
 * TestMonitorlLinkLayer.h
 *
 *  Created on: 28 Aug 2013
 *      Author: andrews_s
 */

#ifndef TESTMONITORLLINKLAYER_H_
#define TESTMONITORLLINKLAYER_H_

#include "lu_types.h"
//#include "MonitorLinkLayer.h"
#include "AbstractMonitorProtocolLayer.h"
//#include "Thread.h"

// Forward declaration
class TestMonitorlLinkLayer;


class TestMonitorProtocolLayer: public AbstractMonitorProtocolLayer
{
public:
    TestMonitorProtocolLayer(APP_ID appID, SCHED_TYPE  schedType);


    virtual ~TestMonitorProtocolLayer();

    void notify_rep_ai_val(lu_uint8_t address, lu_float32_t value, lu_uint8_t flags);
    void notify_rep_di_val(lu_uint8_t address, lu_bool_t value, lu_uint8_t flags);
    void notify_rep_ping();
    void notify_req_exit(lu_int8_t action, lu_int8_t reason);
    void notify_rep_app_ver(lu_uint8_t relType, BasicVersionStr version, lu_uint8_t patch);
    void notify_connection_status(lu_bool_t status);

#if 0
    void notify_req_ping();
#endif
};


//class TestMonitorlLinkLayer: public MonitorLinkLayer
//{
//public:
//    TestMonitorlLinkLayer( SCHED_TYPE   schedType,
//                           lu_uint32_t  priority,
//                           lu_int8_t   *sockName);
//
//    virtual ~TestMonitorlLinkLayer();
//
//    lu_int32_t processMessage(AppMsgStr msg);
//
//private:
//    TestMonitorProtocolLayer *protPtr;
//};


#endif /* TESTMONITORLLINKLAYER_H_ */
