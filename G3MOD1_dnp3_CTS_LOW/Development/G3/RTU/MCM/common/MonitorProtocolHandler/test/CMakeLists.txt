cmake_minimum_required(VERSION 2.8)

# Set ROOT path of G3 (Do NOT remove!!!)   
set(CMAKE_G3_PATH_ROOT "${CMAKE_CURRENT_SOURCE_DIR}/../../../../..")
set(CMAKE_G3_GENERATED_HEADER_BUILD_PATH "${CMAKE_CURRENT_SOURCE_DIR}/../../../MCMApp/includeGenerated/")


#---------------------COMPILER & GLOBAL ENVIRONMENT-----------------------------
# Use CodeSourcery GCC as cross-compiler
set(CMAKE_TOOLCHAIN_FILE "${CMAKE_G3_PATH_ROOT}/RTU/MCM/cmake/GCC_TX25_DI_toolchain.cmake")

# Set the name of the project. The project name will be used as the name of
# the final executable 
project (MonitorProtocolHandlerTest)

# Set GCC default flags
include("${CMAKE_G3_PATH_ROOT}/RTU/MCM/cmake/GCC_TX25_app_flags.cmake")

# Import global variables
include("${CMAKE_G3_PATH_ROOT}/RTU/MCM/cmake/global_variables.cmake")

#-------------------------------INCLUDING---------------------------------------
# Set GLOBAL including paths 
set(GLOBAL_INCLUDES
    ${CMAKE_G3_PATH_COMMON_INCLUDE}
    )
include_directories(${GLOBAL_INCLUDES})

# Set LOCAL including paths
set(LOCAL_INCLUDES
    "../include"
	)
include_directories(${LOCAL_INCLUDES})

include_directories(../../include/)
include_directories(${CMAKE_G3_PATH_COMMON_INCLUDE})
include_directories(${CMAKE_G3_PATH_MCM_COMMONLIB}/BaseReuse/include)

# Include generated header
include("${CMAKE_G3_PATH_ROOT}/RTU/MCM/cmake/generated_header.cmake")


# Include all the library used by the project. The first argument is the
# library relative path. The second argument is where the Makefiles and
# binaries will be saved. This path is relative to the directory where the
# cmake is called. This allows a full out-of-source build.
add_subdirectory(${CMAKE_G3_PATH_MCM_COMMONLIB}/BaseReuse/src BaseReuse)
add_subdirectory(${CMAKE_G3_PATH_COMMON_LIBRARY}/crc32/src crc32)
add_subdirectory(${CMAKE_G3_PATH_RTU_MCM_LIBRARY}/LogMessage/src LogMessage)
add_subdirectory(../src MonitorCommunication)


# List all the include search path needed by the executable (relative path)

# common includes
include_directories(${CMAKE_G3_PATH_MCM_COMMONLIB}/BaseReuse/include)
include_directories(${CMAKE_G3_PATH_MCM_COMMONLIB}/Logger/include)
include_directories(${CMAKE_G3_PATH_COMMON_LIBRARY}/crc32/include)
include_directories(${CMAKE_G3_PATH_RTU_MCM_LIBRARY}/LogMessage/include)

# List all the source files needed to build the executable
add_executable(${EXE_NAME} test.cpp)

# List all the library needed by the linker
target_link_libraries (${EXE_NAME} MonitorCommunication)
target_link_libraries (${EXE_NAME} BaseReuse)
target_link_libraries (${EXE_NAME} Crc32)
target_link_libraries (${EXE_NAME} LogMessage)
target_link_libraries (${EXE_NAME} rt)
target_link_libraries (${EXE_NAME} pthread)

# Generate binary file from executable file
#generate_bin()

# Generate Doxygen documentation
# gen_doxygen("MonitorProtocolHandlerTest" "")

