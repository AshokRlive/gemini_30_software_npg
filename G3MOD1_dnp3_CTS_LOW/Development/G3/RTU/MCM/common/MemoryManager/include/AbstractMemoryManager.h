/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: AbstractMemoryManager.h 12 Jan 2015 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/currentTrunk/G3/RTU/MCM/common/MemoryManager/include/AbstractMemoryManager.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Abstract Memory Manager to implement an specific Memory Manager
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 12 Jan 2015 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   12 Jan 2015   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef ABSTRACTMEMORYMANAGER_H__INCLUDED
#define ABSTRACTMEMORYMANAGER_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <limits>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IMemoryManager.h"
#include "MemoryManagerFactory.h"
#include "IMemService.h"

#include "NVRAMDefID.h"
#include "NVRAMDefApp.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/* MEMORY DEVICES USED */
#define MEMDEVICE_IDROM    "/sys/bus/i2c/devices/0-0050/eeprom"  //RTU factory ID storage device
#define MEMDEVICE_IDAPPRAM "/sys/bus/i2c/devices/0-0054/eeprom"  //RTU factory ID backup storage device
#define MEMDEVICE_APPRAM   "/sys/bus/i2c/devices/0-0054/eeprom"  //Application storage device
#define MEMDEVICE_FILE     "storage.mem" //File device storage

#ifdef TARGET_BOARD_G3_SYSTEMPROC
#define MEMDEVICE_FILE_EVENT     "event.mem"
#define MEMDEVICE_FILE_FRAM    	 "fram.mem"
#define MEMDEVICE_FILE_NVRAM     "nvram.mem"
#define MEMDEVICE_FILE_NVRAMID   "nvramid.mem"
#endif

/* Event FRAM device storage */
//#define MEMDEVICE_EVENTFRAM "/usr/local/log/events.log"  /* XXX: SKA /usr/local/log is in SPI Flash */
#define MEMDEVICE_EVENTFRAM "/sys/bus/spi/devices/spi0.2/eeprom"

/* FRAM device storage */
//#define MEMDEVICE_FRAM0 "fram.mem"  /* XXX: AP - simulation of FRAM in a file (to be removed) */
#define MEMDEVICE_FRAM0 "/sys/bus/i2c/devices/2-0050/eeprom"

//FRAM size (in bytes)
#define MEMDEVICE_FRAM_MAXSIZE 16384
                                //16384 = 128 Kbits, 862 Events (19 bytes/event)
                                //32767 = 256 Kbits, 1724 Events (19 bytes/event)
                                //250000 = 2 Mbits, 13797 (not 13157) Events (19 bytes/event)
                                //750000 = 6 Mbits, 39472 Events (19 bytes/event)


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 * Memory offset address from where blocks starts counting.
 */
typedef enum
{
    MEM_OFFSET_NVRAMID    = NVRAM_ID_BLK_INFO_OFFSET,
    MEM_OFFSET_NVRAM      = NVRAM_APP_BLK_USER_A_OFFSET,
    MEM_OFFSET_FRAM       = 0,
    MEM_OFFSET_FILE       = 0,
    MEM_OFFSET_LAST
} MEM_OFFSET;

/*
 * Memory block size (in bytes) for writing operations on memory access (minimum cell size).
 * Set to 0 when not used.
 */
typedef enum
{
    MEM_BLOCKSIZE_NVRAMID    = NVRAM_ID_BLK_INFO_SIZE,
    MEM_BLOCKSIZE_NVRAM      = NVRAM_APP_BLK_USER_A_SIZE,
    MEM_BLOCKSIZE_FRAM       = 1000,
    MEM_BLOCKSIZE_FILE       = 0,
    MEM_BLOCKSIZE_LAST
} MEM_BLOCKSIZE;

/*
 * Memory setting that states if use block writing or not.
 */
enum
{
    MEM_USEOFFSET_NVRAMID    = LU_TRUE,
    MEM_USEOFFSET_NVRAM      = LU_TRUE,
    MEM_USEOFFSET_FRAM       = LU_TRUE,
    MEM_USEOFFSET_FILE       = LU_FALSE,
    MEM_USEOFFSET_LAST
};

/*
 * Total Memory size (in bytes).
 * Set to 0 when not used (such as files).
 */
typedef enum
{
    MEMDEVICE_SIZE_NVRAMID    = NVRAM_ID_BLK_INFO_SIZE,
    MEMDEVICE_SIZE_NVRAM      = NVRAM_APP_BLK_USER_A_SIZE,
    MEMDEVICE_SIZE_FRAM       = MEMDEVICE_FRAM_MAXSIZE,
    MEMDEVICE_SIZE_FILE       = 0,
    MEMDEVICE_SIZE_LAST
} MEMDEVICE_SIZE;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Skeleton of a basic Memory Manager implementation.
 *
 * The memory Manager provides interaction with different memory devices, doing
 * it transparently by providing the IMemoryManager interface. The user doesn't
 * need to know where and how it is stored, but what is stored (through
 * IMemoryManager::STORAGE_VAR names).
 *
 * Note that the expected life cycle of a Memory Manager is the following:
 *  1) Create Memory Manager Object.
 *  2) Configure content size by using setSize(), each time for different memory
 *      type/service provider.
 *  3) Start up the Memory Manager by calling init() (or initialise separately)
 *  4) Use the operations for accessing the memory, such as read/write/flush...
 *  5) Destroy the object when not in use anymore. This closes the memory services.
 */
class AbstractMemoryManager: public IMemoryManager
{
public:
    AbstractMemoryManager();
    virtual ~AbstractMemoryManager();

    /* ==Inherited from IMemoryManager== */
    virtual void setSize(IMemoryManager::STORAGE_VAR variable, size_t size);
    virtual IMemoryManager::MEMERROR init(IMemoryManager::MEM_TYPE memService);
    virtual void init();

    virtual lu_bool_t read(IMemoryManager::STORAGE_VAR variable, lu_uint8_t *value, size_t* sizeRead = NULL);
    virtual lu_bool_t write(IMemoryManager::STORAGE_VAR variable, lu_uint8_t* value);

    virtual lu_bool_t invalidate(IMemoryManager::MEM_TYPE memService);

    virtual void flush();
    virtual void close();

protected:
    /**
     * \brief Memory type-specific service provider creation.
     *
     * Please override it with the specific for the implementation.
     *
     * \param type Type of memory service.
     *
     * \return Pointer to a newly-created service provider.
     */
    virtual IMemService* createService(IMemoryManager::MEM_TYPE type) = 0;

protected:
    IMemService* m_service[IMemoryManager::MEM_TYPE_LAST];    //Keep track of already-created service providers

private:
    void internalInit();

private:
    bool initialised;
};


#endif /* ABSTRACTMEMORYMANAGER_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
