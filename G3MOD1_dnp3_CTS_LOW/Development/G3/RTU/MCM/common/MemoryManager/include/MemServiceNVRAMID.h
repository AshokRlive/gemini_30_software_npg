/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: MemServiceNVRAMID.h 28 Mar 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/MemoryManager/include/MemServiceNVRAMID.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 28 Mar 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   28 Mar 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MEMSERVICENVRAMID_H__INCLUDED
#define MEMSERVICENVRAMID_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "Table.h"
#include "MemServiceNVRAMAbstract.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Class to support ID NVRAM access
 *
 * This class implements accessing to the NVRAM ID, both the one in the
 * read-only NVRAM device and the backup copy kept in the APP NVRAM.
 */
class MemServiceNVRAMID : public MemServiceNVRAMAbstract
{
public:
    MemServiceNVRAMID(const lu_char_t* MEMDEVICE_ID_PATH,
                      const lu_char_t* MEMDEVICE_APP_PATH);
    virtual ~MemServiceNVRAMID();

    //Inherited from IMemService
    virtual void setSize(IMemoryManager::STORAGE_VAR variable, size_t size);
    virtual IMemService::MEMORYERROR init();
    virtual lu_bool_t read(IMemoryManager::STORAGE_VAR variable, lu_uint8_t* value, size_t* sizeRead);
    virtual lu_bool_t write(IMemoryManager::STORAGE_VAR variable, lu_uint8_t* value);
    virtual IMemService::MEMORYERROR invalidate();
    virtual void flush();
    virtual void close();

private:
    const lu_char_t* MemDevice_ID;      //ID memory path
    const lu_char_t* MemDevice_AppID;   //APP ID backup memory path
    lu_uint8_t* bufferID;   //buffer to store what was read from memory.
    Table <lu_uint8_t*> bufNVRAM;   //Table to keep buffer access
    lu_bool_t valid;        //validity of the buffer
};


#endif /* MEMSERVICENVRAMID_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
