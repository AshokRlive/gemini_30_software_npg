/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: MemServiceNVRAMAbstract.h 2 Apr 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/MemoryManager/include/MemServiceNVRAMAbstract.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 2 Apr 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   2 Apr 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MEMSERVICENVRAMABSTRACT__INCLUDED
#define MEMSERVICENVRAMABSTRACT__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IMemService.h"
#include "Mutex.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Abstract class to implement common interaction with NVRAM
 */
class MemServiceNVRAMAbstract : public IMemService
{
public:
    MemServiceNVRAMAbstract();
    virtual ~MemServiceNVRAMAbstract();

    //Inherited from IMemService
    virtual void setSize(IMemoryManager::STORAGE_VAR variable, size_t size) = 0;
    virtual IMemService::MEMORYERROR init() = 0;
    virtual lu_bool_t read(IMemoryManager::STORAGE_VAR variable, lu_uint8_t* value, size_t* sizeRead) = 0;
    virtual lu_bool_t write(IMemoryManager::STORAGE_VAR variable, lu_uint8_t* value) = 0;
    virtual IMemService::MEMORYERROR invalidate() = 0;
    virtual void flush() = 0;
    virtual void close() = 0;

protected:
    /**
     * \brief Enable or disable NVRAM's WEN signal
     *
     * Enables or disables the WEN (Write Enable) signal that allows to write to
     * the APP NVRAM.
     * Note that the ID NVRAM has its WEN signal controlled externally from the
     * backplane, thus not needing to call this function.
     *
     * \param enable Write enabling or disabling.
     *
     * \return Operation result: LU_TRUE when successful.
     */
    virtual lu_bool_t enableWrite(lu_bool_t enable = LU_TRUE);

    /**
     * \brief Update CRC of NVRAM scratch data.
     *
     * This function does not have any concurrency checking.
     * Caution: please prevent any change in the NVRAM scratch data during CRC
     * calculation and before using this value since CRC and content could
     * mismatch.
     *
     * \param buffer Buffer where the read data will be stored.
     * \param bufSize Total buffer size/size of data to be recovered.
     */
    void updateCRC( lu_uint8_t* buffer, const lu_uint32_t bufSize);

    /**
     * \brief (Initial) Read of the contents of the specified memory device.
     *
     * This method opens the memory device, reads it, and close it afterwards.
     * It doesn't lock any semaphore to access to memory.
     * The CRC is verified and if it fails, the read fails.
     * Note that when providing an offset, it is absolute and ignores the
     * memConfig settings. When not providing it, uses memConfig.offset, or 0
     * when memConfig.useOffset is not true.
     *
     * \param device Memory device to read from.
     * \param buffer Buffer where the resulting read data will be stored.
     * \param bufSize Total buffer size / size of data to be recovered.
     * \param offset Absolute reading address. If not provided, uses memConfig-defined.
     *
     * \return result of the reading operation.
     */
    virtual IMemService::MEMORYERROR readMemory( const lu_char_t* devicePath,
                                                    lu_uint8_t* buffer,
                                                    const lu_uint32_t bufSize
                                                    );
    virtual IMemService::MEMORYERROR readMemory( const lu_char_t* devicePath,
                                                    lu_uint8_t* buffer,
                                                    const lu_uint32_t bufSize,
                                                    const lu_uint32_t offset
                                                    );

    /**
     * \brief Write (flush) a buffer block into memory, adding the CRC integrity checksum.
     *
     * This method opens the memory device, writes to it, and close it afterwards.
     * It doesn't lock any semaphore to access to memory.
     * If the data to write hasn't changed, it does not write that part.
     * NOTE: memConfig offset settings are always used.
     *
     * \param device Memory device to read from.
     * \param offset Address where the data can be found.
     * \param buffer Buffer where the data to write is presently stored.
     * \param bufSize Total buffer size / size of data to be stored.
     *
     * \return result of the operation.
     */
    virtual IMemService::MEMORYERROR writeMemory(const lu_char_t* devicePath,
                                                    const lu_uint8_t* buffer,
                                                    const lu_uint32_t bufSize
                                                    );

protected:
    MasterMutex memAccess;  //Common Mutex for concurrent access to memory

private:
    static lu_bool_t initialisedGPIO;    //Initialises GPIO only once
    static FILE* wenGPIO;           //GPIO file for enable/disable write operation (one for all instances)
};


#endif /* MEMSERVICENVRAMABSTRACT__INCLUDED */

/*
 *********************** End of file ******************************************
 */
