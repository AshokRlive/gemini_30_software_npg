/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: IMemoryManager.cpp 04-Jan-2013 08:25:12 pueyos_a $
 *               $HeadURL: U:\G3\RTU\MCM\src\MemoryManager\src\IMemoryManager.cpp $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       IMemoryManager - header module to interface memory storage access from
 *       the rest of the application.
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 04-Jan-2013 08:25:12	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   04-Jan-2013 08:25:12  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_14FB4DA0_2EBB_4641_B55C_6E52D6C3E0FC__INCLUDED_)
#define EA_14FB4DA0_2EBB_4641_B55C_6E52D6C3E0FC__INCLUDED_



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stddef.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Common class to interface memory storage access.
 */
class IMemoryManager
{
public:
    /*
     * Types of memory for the Service Providers
     */
    typedef enum
    {
        MEM_TYPE_NONE,    //Not stored
        MEM_TYPE_EVENT,   //Event-specific storage (this event Service Provider should be the first)
        MEM_TYPE_NVRAMID, //NVRAM ID storage
        MEM_TYPE_NVRAM,   //NVRAM APP
        MEM_TYPE_FRAM,    //FRAM
        MEM_TYPE_FILE,    //File storage (default)
        //MEM_TYPE_FILE_OTHER,    //File storage (other for storing in a different file/mem)
        MEM_TYPE_LAST
    } MEM_TYPE;

    /*
     * Data to store
     */
    typedef enum
    {
        STORAGE_VAR_ID,             //Complete factory data, such as serial number, board ID...
        STORAGE_VAR_MODULEUID,      //Factory data's Unique module ID (serial) number
        STORAGE_VAR_FEATUREREV,     //Factory data's module feature revision
        STORAGE_VAR_EVENT,          //Event entry
        STORAGE_VAR_EVENTLOG,       //Amount of event stored
        STORAGE_VAR_EVENTMEMSIZE,   //Amount of memory available for storing events (in bytes)
        STORAGE_VAR_OLR,            //Off/local/Remote
        STORAGE_VAR_CFGFILECRC,     //Configuration File CRC
        STORAGE_VAR_SERVICE,        //Out of service setting
        STORAGE_VAR_SERVICEREASON,  //Reason for being Out of service
        STORAGE_VAR_DUMMYSW,        //Dummy switch position
        STORAGE_VAR_LOGLEVEL,       //Log level settings
        STORAGE_VAR_LAST
    } STORAGE_VAR;

    /*
     * Error codes
     */
    typedef enum
    {
        MEMERROR_NONE,
        MEMERROR_THREAD,                     //Internal thread creation error
        MEMERROR_CONFIG,                     //Error in memory configuration
        MEMERROR_ACCESS,                     //Error accessing memory
        MEMERROR_ACCESSBKP,                  //Error accessing secondary/backup memory
        MEMERROR_LAST
    } MEMERROR;

public:
    IMemoryManager() {};
    virtual ~IMemoryManager() {};

    /**
     * \brief Set the size (in bytes) of a variable.
     *
     * Initialise a variable registration by giving its size. This size is
     * stored in the list of variables to be stored in non-volatile memory.
     *
     * NOTE: size of all the variables should have been set before any read/write operation.
     *
     * \param variable Type (enum) of the variable in the storage list.
     * \param size Size in bytes of the aforementioned variable.
     */
    virtual void setSize(IMemoryManager::STORAGE_VAR variable, size_t size) = 0;

    /**
     * \brief Initialise a specific memory Service Provider.
     *
     * A memory Service Provider is committed to check/read its memory blocks
     * and report any problem.
     * NOTE: size of all the variables related with this memory Service Provider
     *      must have been set before calling this method.
     *
     * \param memService Memory Service Provider ID to be initialised.
     */
    virtual IMemoryManager::MEMERROR init(IMemoryManager::MEM_TYPE memService) = 0;

    /**
     * \brief Initialise all the memory Service Providers.
     *
     * All the memory Service Providers that weren't already initialised are
     * committed to check/read their memory blocks and report any problem.
     * NOTE: size of all the variables must have been set before calling this
     * method.
     */
    virtual void init() = 0;

    /**
     * \brief Read a value from non-volatile memory.
     *
     * NOTE: always write the default value to non-volatile memory when first read failed.
     * NOTE: size of this variable should have been set before any read/write operation.
     *
     * \param variable Type (enum) of the variable to read.
     * \param value Pointer to memory area where to store the value recovered from non-volatile memory.
     * \param sizeRead Returns here the size of the data stored in the value parameter.
     *
     * \return Operation result. LU_FALSE when error.
     */
    virtual lu_bool_t read(IMemoryManager::STORAGE_VAR variable, lu_uint8_t* value, size_t* sizeRead = NULL) = 0;

    /**
     * \brief Write a value to non-volatile memory.
     *
     * NOTE: size of this variable should have been set before any read/write operation.
     *
     * \param variable Type (enum) of the variable to write
     * \param value Pointer to memory area where is the value to be stored in non-volatile memory
     *
     * \return Operation result. LU_FALSE when error.
     */
    virtual lu_bool_t write(IMemoryManager::STORAGE_VAR variable, lu_uint8_t* value) = 0;

    /**
     * \brief Invalidate the contents of a non-volatile memory.
     *
     * Tells the designed memory service provider to invalidate the contents of
     * its memory.
     * This is usually done by writing a wrong data check code (e.g. CRC32).
     *
     * \param memService Memory service provider.
     *
     * \return operation result. LU_FALSE when error.
     */
    virtual lu_bool_t invalidate(IMemoryManager::MEM_TYPE memService) = 0;

    /**
     * \brief Update the values to the non-volatile memory.
     *
     * Copies all the variable values cached to the non-volatile memory. The
     * effective writing is done only in there is any data change.
     */
    virtual void flush() = 0;

    /**
     * \brief Close access to a non-volatile memory.
     *
     * The non-volatile memory is updated (flushed) and then closed, finishing
     * storage operations. This should be called when the application is about
     * to close.
     */
    virtual void close() = 0;
};


#endif // !defined(EA_14FB4DA0_2EBB_4641_B55C_6E52D6C3E0FC__INCLUDED_)

/*
 *********************** End of file ******************************************
 */

