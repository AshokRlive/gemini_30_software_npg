/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: IMemService.cpp 04-Jan-2013 08:24:52 pueyos_a $
 *               $HeadURL: U:\G3\RTU\MCM\src\MemoryManager\src\IMemService.cpp $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       IMemService - header module that gives a base interface for Memory
 *       Service Providers.
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 04-Jan-2013 08:24:52	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   04-Jan-2013 08:24:52  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_3EAB2F92_3ECC_486f_9640_D56427882105__INCLUDED_)
#define EA_3EAB2F92_3ECC_486f_9640_D56427882105__INCLUDED_



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdio.h>  //FILE operations

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IMemoryManager.h"
#include "Logger.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Base class to interface memory Service Providers.
 */
class IMemService
{
public:
    typedef enum
    {
        MEMORYERROR_NONE,
        MEMORYERROR_ACCESS,                     //Error accessing memory
        MEMORYERROR_CRC,                        //CRC error in memory
        MEMORYERROR_THREAD,                     //Internal thread creation error
        MEMORYERROR_RAM,                        //Not enough memory for buffering operations
        MEMORYERROR_BOUNDARIES,                 //Data to be written is bigger than block boundaries
        MEMORYERROR_WARNMAIN,                   //Main data in error, using backup data
        MEMORYERROR_WARNBACKUP,                 //Backup data in error, but using main data

        /* Service Provider Type-specific */
        /* XXX: pueyos_a - Mem Service Provider-specific error codes might be deprecated (to be removed) */
//        MEMORYERROR_NVRAMID_ACCESS,             //Error accessing NVRAM ID device
//        MEMORYERROR_NVRAMID_CRC,                //Incorrect CRC in NVRAM ID
        MEMORYERROR_NVRAMAPP_ACCESS,            //Error accessing NVRAM APP device
        MEMORYERROR_NVRAMAPP_IDBLOCK_CRC,       //Incorrect CRC in NVRAM APP - ID block
        MEMORYERROR_NVRAMAPP_UBLOCK_CRC,        //Incorrect CRC in NVRAM APP - user block
        MEMORYERROR_FRAM_ACCESS,                //Error accessing FRAM
        MEMORYERROR_FRAM_CRC,                   //Incorrect CRC in FRAM
        MEMORYERROR_FILE_ACCESS,                //Error accessing a File
        MEMORYERROR_FILE_CRC,                   //Incorrect CRC in File

        MEMORYERROR_LAST
    } MEMORYERROR;

    struct memConfigStr
    {
        lu_uint32_t memorySize; //Total or partial memory size
        lu_uint32_t blockSize;  //Memory block size for writing operations,
        lu_uint32_t offset;     //Initial offset from where block starts (usually 0)
        lu_bool_t useOffset;    //use offset or not
    };

public:
    IMemService();
    virtual ~IMemService() {};

    /**
     * \brief set the parameters needed by the memory
     *
     * Configures a memory giving several different parameters.
     * Each Memory Service Provider may or may not use these values.
     * This is intended to be called before any other operations.
     *
     * \param memoryConfig Memory configuration parameters.
     */
    virtual void setMemoryConfig(IMemService::memConfigStr memoryConfig);

    /**
     * \brief Set the size (in bytes) of a variable.
     *
     * Initialise a variable registration by giving its size. This size is
     * stored in the list of variables to be stored in non-volatile memory.
     *
     * NOTE: size of all the variables should have been set before any
     * read/write operation.
     *
     * \param variable Type (enum) of the variable in the storage list.
     * \param size Size in bytes of the aforementioned variable.
     */
    virtual void setSize(IMemoryManager::STORAGE_VAR variable, size_t size) = 0;

    /**
     * \brief Initialise the memory manager.
     *
     * The memory Service Provider will check/read its memory blocks and report
     * any problem.
     * NOTES:
     *      -Size of all the variables used by the memory Service Provider must
     *      have been set before calling this method.
     *      -The memory Service Provider should have a way to handle several
     *      calls to this method as needed (e.g.: ignore calls after first one,
     *      re-adjust parameters, etc.).
     */
    virtual IMemService::MEMORYERROR init() = 0;

    /**
     * \brief Read a value from non-volatile memory.
     *
     * NOTE: always write the default value to non-volatile memory when first read failed.
     * NOTE: size of this variable should have been set before any read/write operation.
     *
     * \param variable Type (enum) of the variable to read
     * \param value Pointer to memory area where to store the value recovered from non-volatile memory
     * \param sizeRead Where to store the size of the value recovered
     *
     * \return Operation result. LU_FALSE when error.
     */
    virtual lu_bool_t read(IMemoryManager::STORAGE_VAR variable, lu_uint8_t* value, size_t* sizeRead = NULL) = 0;

    /**
     * \brief Write a value to non-volatile memory.
     *
     * NOTE: always write the default value to memory when first read failed.
     * NOTE: size of this variable should have been set before any read/write operation.
     *
     * \param variable Type (enum) of the variable to write
     * \param value Pointer to memory area where is the value to be stored in non-volatile memory
     *
     * \return Operation result. LU_FALSE when error.
     */
    virtual lu_bool_t write(IMemoryManager::STORAGE_VAR variable, lu_uint8_t* value) = 0;

    /**
     * \brief Invalidate the contents of the non-volatile memory.
     *
     * The memory Service Provider invalidates the contents of its associated
     * memory.
     * This is usually done by writing a wrong data check code (e.g. CRC32).
     *
     * \return operation result. LU_FALSE when error.
     */
    virtual IMemService::MEMORYERROR invalidate() = 0;

    /**
     * \brief Update the values to the non-volatile memory.
     *
     * Copies all the variable values cached to the non-volatile memory. The
     * effective writing is done only in there is any data change.
     */
    virtual void flush() = 0;

    /**
     * \brief Close access to a non-volatile memory.
     *
     * The non-volatile memory is updated (flushed) and then closed, finishing
     * storage operations.
     */
    virtual void close() = 0;

    /**
     * \brief Default memory writing implementation that writes to block memory devices
     *
     * The default implementation writes only the blocks that are different to avoid
     * rewriting blocks that doesn't need that, thus reducing memory wearing.
     * This function should be overridden if the procedure differs to the one needed
     * by the memory device.
     * It should be noted that the specified offset counts FROM the offset given
     * by memConfig.offset, being 0 when memConfig.useOffset is False. Examples:
     * memConfig.useOffset  memConfig.offset    offset param    writing address
     * ------------------------------------------------------------------------
     * LU_FALSE             100                 0               0
     * LU_FALSE             100                 20              0 + 20
     * LU_TRUE              100                 0               100
     * LU_TRUE              100                 20              100 + 20
     *
     * \param device Memory device
     * \param bufferRead Buffer already stored in memory (for comparison purposes)
     * \param bufferWrite Buffer to write in memory (only the parts that changed)
     * \param bufferSize Size of bufferWrite
     * \param offset Writing offset: counting from memConfig.offset, or from 0 if offset not in use
     * \param force When set to LU_TRUE, write all data without comparing blocks
     *
     * \return Successful operation when LU_TRUE
     */
    virtual lu_bool_t writeToMemory(FILE* device,
                                    const lu_uint8_t* bufferRead,
                                    const lu_uint8_t* bufferWrite,
                                    const size_t bufferSize,
                                    const size_t offset = 0,
                                    const lu_bool_t force = LU_FALSE
                                    );

    /**
     * \brief Notify the timer tick is expired
     *
     * This function will be periodically called in case the Service Provider
     * needs delayed writing or a different thread for some operations.
     *
     * \param dTime Time elapsed from the previous tick (in ms)
     */
    virtual void tickEvent(lu_uint32_t dTime);

protected:
    Logger& log;

    memConfigStr memConfig; //Memory common configuration parameters
    lu_bool_t initialised;  //Flag for initial state. LU_FALSE when device was not yet configured.

};


#endif // !defined(EA_3EAB2F92_3ECC_486f_9640_D56427882105__INCLUDED_)

/*
 *********************** End of file ******************************************
 */

