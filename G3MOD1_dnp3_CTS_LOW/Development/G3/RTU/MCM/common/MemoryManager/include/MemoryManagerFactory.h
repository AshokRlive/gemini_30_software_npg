/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: MemoryManagerFactory.cpp 20-Dec-2012 16:03:34 pueyos_a $
 *               $HeadURL: U:\G3\RTU\MCM\src\MemoryManager\src\MemoryManagerFactory.cpp $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MemoryManagerFactory header module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 20-Dec-2012 16:03:34	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   20-Dec-2012 16:03:34  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_DF699585_F3CA_48aa_A8A9_4EEBB8A0F19C__INCLUDED_)
#define EA_DF699585_F3CA_48aa_A8A9_4EEBB8A0F19C__INCLUDED_



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IMemService.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * Class MemoryManagerFactory
 */
/**
 * This class creates Memory Service Providers as solicited.
 * The class uses a function template pointer.
 */
class MemoryManagerFactory
{

public:
	MemoryManagerFactory() {};
	virtual ~MemoryManagerFactory() {};

	/**
	 * \brief Creates a new Memory Service Provider of the given type.
	 *
	 * \return pointer to the newly created Service Provider object.
	 */
	template <typename MemMgrType>
	static IMemService* create() {return new MemMgrType();}

   /**
     * \brief Creates a new Memory Service Provider of the given type with 1 parameter.
     *
     * \return pointer to the newly created Service Provider object.
     */
    template <typename MemMgrType, typename ParamType>
    static IMemService* create(ParamType x) {return new MemMgrType(x);}

    /**
      * \brief Creates a new Memory Service Provider of the given type with 1 parameter.
      *
      * \return pointer to the newly created Service Provider object.
      */
     template <typename MemMgrType, typename ParamType1, typename ParamType2>
     static IMemService* create(ParamType1 x1, ParamType2 x2)
     {
         return new MemMgrType(x1, x2);
     }

    /* Note: variable parameter templates could be achieved by using:
     *  -C++11 Variadic Templates (we are not using C++11 at the moment);
     *  -An arbitrary number of parameters with default types;
     *  -Template recursion;
     *  -Typelists, chains, template chains;
     * At the moment these solutions aren't adopted since they add
     * complexity and we don't need that much flexibility.
     * For more info, see:
     *  en.wikipedia.org/wiki/Variadic_Templates
     *  stackoverflow.com/questions/1511532
     *  www.drdobbs.com/using-chains-to-free-library-code/184401982
     */
};


#endif // !defined(EA_DF699585_F3CA_48aa_A8A9_4EEBB8A0F19C__INCLUDED_)

/*
 *********************** End of file ******************************************
 */

