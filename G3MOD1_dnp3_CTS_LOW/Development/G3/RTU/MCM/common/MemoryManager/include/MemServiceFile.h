/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: MemServiceFile.cpp 20-Dec-2012 15:49:59 pueyos_a $
 *               $HeadURL: U:\G3\RTU\MCM\src\MemoryManager\src\MemServiceFile.cpp $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Header module for handling memory storage to file.
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 20-Dec-2012 15:49:59	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   20-Dec-2012 15:49:59  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_6F2534AB_C42A_4036_BEC9_DBD8D30B1DEB__INCLUDED_)
#define EA_6F2534AB_C42A_4036_BEC9_DBD8D30B1DEB__INCLUDED_



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <fcntl.h>
#include <stdio.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IMemService.h"
#include "LockingMutex.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * Class MemServiceFile
 *
 * This class makes the assumption that is first initialised using setSize() for
 * all the variables to be stored/retrieved, and then at the first read() or
 * write() operation, opens the file to store/retrieve values.
 * Values aren't forced to dump to disk until any call to close() or flush()
 */
class MemServiceFile : public IMemService
{
public:
	MemServiceFile(const lu_char_t *fileName);
	virtual ~MemServiceFile();

	virtual void setSize(IMemoryManager::STORAGE_VAR variable, size_t size);
	virtual IMemService::MEMORYERROR init();

	virtual lu_bool_t read(IMemoryManager::STORAGE_VAR variable, lu_uint8_t* value, size_t* sizeRead);
	virtual lu_bool_t write(IMemoryManager::STORAGE_VAR variable, lu_uint8_t* value);

	virtual IMemService::MEMORYERROR invalidate();
    virtual void flush();
    virtual void close();

private:
    /**
     * \brief Check if memory contents have changed.
     *
     * Compares the scratch data with the data stored in non-volatile memory.
     *
     * @return comparison result; LU_TRUE when data differs.
     */
    lu_bool_t checkMemChange();

    /**
     * \brief Update the contents of the non-volatile memory.
     *
     * \param invalidate Command to invalidate the contents of the non-volatile memory [optional].
     *
     * \return error code
     */
    IMemService::MEMORYERROR update(lu_bool_t invalidate = LU_FALSE);

    /**
     * \brief Write to non-volatile memory only the parts of a buffer that has changed
     *
     * It compares blocks of buffers and writes them if they changed.
     *
     * @param device Memory device to be accessed.
     * @param bufferRead Buffer of data already stored in non-volatile memory.
     * @param bufferWrite Buffer of data to be stored in non-volatile memory.
     * @param bufferSize Size of both buffers.
     * @param devBlockSize Block size to be used to write to this device.
     *
     * @return operation result LU_FALSE when error accessing/writing the device.
     */
    lu_bool_t writeToMem(FILE* device,
                        const lu_uint8_t* bufferRead,
                        const lu_uint8_t* bufferWrite,
                        const size_t bufferSize,
                        const size_t devBlockSize
                        );

private:
    struct MemFileElemStr
    {
        IMemoryManager::STORAGE_VAR varEnum;    //Type of variable
        size_t varSize;         //size of variable in bytes
        lu_uint32_t filePos;    //position at file
        lu_bool_t invalid;      //flag stating if value is not valid yet (fail read/not written).
    };

private:
    const lu_char_t *FileName;  //file name to be used
    FILE *fileMem;              //file pointer for memory storage.
    Mutex fileAccess;           //Mutex to avoid file accesses concurrently.
    lu_bool_t updPending;       //flag stating if any value has change but not written to storage.
    lu_uint8_t *memReadFileBuffer;  //buffer to store what was read from memory.
    lu_uint8_t *memWriteFileBuffer; //buffer to store what should be written to memory.
    lu_uint32_t bufSize;        //size of that buffer (including header).
    lu_uint32_t dataSize;       //size of data stored in the buffer (excluding header).

    MemFileElemStr memFileIndex[IMemoryManager::STORAGE_VAR_LAST];  //memory storage table
};


#endif // !defined(EA_6F2534AB_C42A_4036_BEC9_DBD8D30B1DEB__INCLUDED_)

/*
 *********************** End of file ******************************************
 */

