/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: MemServiceFRAM.cpp 07-Jan-2013 15:29:15 pueyos_a $
 *               $HeadURL: U:\G3\RTU\MCM\src\MemoryManager\src\MemServiceFRAM.cpp $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Header module for handling FRAM storage.
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 07-Jan-2013 15:29:15	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   07-Jan-2013 15:29:15  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_59F10088_0801_4660_AB28_FF40AFDA57BE__INCLUDED_)
#define EA_59F10088_0801_4660_AB28_FF40AFDA57BE__INCLUDED_



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IMemService.h"
#include "LockingMutex.h"
#include "NVRAMDef.h"   //for use of memory block header NVRAMBlkHeadStr

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Object for accessing the FRAM contents
 */
class MemServiceFRAM : public IMemService
{

public:
	MemServiceFRAM(const lu_char_t *memoryDeviceName);
	virtual ~MemServiceFRAM();

	virtual void setSize(IMemoryManager::STORAGE_VAR variable, size_t size);
    virtual IMemService::MEMORYERROR init();

    virtual lu_bool_t read(IMemoryManager::STORAGE_VAR variable, lu_uint8_t* value, size_t* sizeRead);
    virtual lu_bool_t write(IMemoryManager::STORAGE_VAR variable, lu_uint8_t* value);

    virtual IMemService::MEMORYERROR invalidate();
    virtual void flush();
    virtual void close();

private:
    typedef enum
    {
        MEMH_ERROR_NONE,
        MEMH_ERROR_DEVICE,   //device access error
        MEMH_ERROR_READ,     //failure at reading
        MEMH_ERROR_WRITE,    //failure at writing
        MEMH_ERROR_CRC,      //CRC check failed
        MEMH_ERROR_LAST      //failure at reading
    } MEMH_ERROR;

    /* structure for tracking of variables stored in memory */
    struct MemFRAMElemStr
    {
        IMemoryManager::STORAGE_VAR varEnum;    //Type of variable
        size_t varSize;         //size of variable in bytes
        lu_uint32_t address;    //memory address at the non-volatile memory device
        lu_bool_t invalid;      //flag stating if value is not valid yet (fail read/not written).
    };

private:
    /**
     * \brief Update the contents of the non-volatile memory.
     *
     * \param invalidate Command to invalidate the contents of the non-volatile memory [optional].
     *
     * \return error code
     */
    IMemService::MEMORYERROR update(lu_bool_t invalidate = LU_FALSE);

    /**
     * \brief (Initial) Read of the contents of the specified memory device.
     *
     * This method assumes that the memory device is already opened, and in
     * turn doesn't lock any semaphore to access to memory.
     * Buffer-related variables and data validity flags are updated according
     * to the result of the operation.
     * The CRC is verified and if it fails, the read fails.
     * When failing to access the device, the device parameter is set to NULL.
     *
     * \param device Memory device to read from. If device fails, is set to NULL.
     * \param offset Address where the data should be stored.
     *
     * \return result of the reading operation; LU_TRUE when data has been read.
     */
    lu_bool_t readMemory(FILE *device, lu_uint32_t offset);

    /**
     * \brief Write (flush) a buffer block into memory, adding the CRC integrity checksum.
     *
     * This method assumes that the memory device is already opened, and in
     * turn doesn't lock any semaphore to access to memory.
     *
     * \param device Memory device to be written
     * \param offset Address where the data should be written
     * \param buffer Data to write into memory.
     * \param bufferSize Data buffer size.
     * \param invalidate Set to LU_TRUE to invalidate CRC [optional].
     *
     * \return Data update pending. LU_FALSE when data is written to memory so is not pending anymore.
     */
    MemServiceFRAM::MEMH_ERROR writeMemory(FILE *device,
                                            lu_uint32_t offset,
                                            lu_uint8_t *buffer,
                                            lu_uint32_t bufferSize,
                                            lu_bool_t invalidate = LU_FALSE
                                            );

    /**
     * \brief Check if memory contents have changed.
     *
     * Compares the scratch data with the data stored in non-volatile memory.
     *
     * @return comparison result; LU_TRUE when data differs.
     */
    lu_bool_t checkMemChange();

private:
    const lu_char_t *MemoryDevice;    //Device access: device location name/specification
    FILE *memDevice;            //Memory device
    Mutex memAccess;            //Mutex to avoid memory accesses concurrently.
    lu_bool_t updPending;       //flag stating if any value has change but not written to storage.
    lu_uint32_t baseAddress;    //Base address of the memory block

    lu_uint8_t *memReadBuffer;  //buffer to store what was read from memory.
    lu_uint8_t *memWriteBuffer; //buffer to store what should be written to memory.
    lu_uint32_t bufSize;        //size of both buffers (including header).
    lu_uint32_t dataSize;       //size of data stored in the buffer (excluding header).

    MemFRAMElemStr memIndex[IMemoryManager::STORAGE_VAR_LAST];  //memory storage index table

};





#endif // !defined(EA_59F10088_0801_4660_AB28_FF40AFDA57BE__INCLUDED_)

/*
 *********************** End of file ******************************************
 */

