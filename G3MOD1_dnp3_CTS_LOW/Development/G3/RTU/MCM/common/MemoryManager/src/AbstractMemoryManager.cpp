/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: AbstractMemoryManager.cpp 12 Jan 2015 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/currentTrunk/G3/RTU/MCM/common/MemoryManager/src/AbstractMemoryManager.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 12 Jan 2015 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   12 Jan 2015   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "AbstractMemoryManager.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
/**
 * Destination specification
 */
struct memDestinationStr
{
    IMemoryManager::STORAGE_VAR varType;    //variable to store
    IMemoryManager::MEM_TYPE memType;       //memory Service Provider in charge to store this variable
    IMemService* memService;             //pointer to that memory Service Provider
};

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/* Lookup table to manage what data to store and which memory Service Provider:
 *                  variable type,
 *                  memory type,
 *                  related memory Service Provider (to be filled automatically)
 */
static memDestinationStr memDestination[] =
{
    {IMemoryManager::STORAGE_VAR_EVENT          , IMemoryManager::MEM_TYPE_EVENT,       NULL},
    {IMemoryManager::STORAGE_VAR_EVENTLOG       , IMemoryManager::MEM_TYPE_EVENT,       NULL},
    {IMemoryManager::STORAGE_VAR_EVENTMEMSIZE   , IMemoryManager::MEM_TYPE_EVENT,       NULL},
    {IMemoryManager::STORAGE_VAR_ID             , IMemoryManager::MEM_TYPE_NVRAMID,     NULL},
    {IMemoryManager::STORAGE_VAR_MODULEUID      , IMemoryManager::MEM_TYPE_NVRAMID,     NULL},
    {IMemoryManager::STORAGE_VAR_FEATUREREV     , IMemoryManager::MEM_TYPE_NVRAMID,     NULL},
    {IMemoryManager::STORAGE_VAR_OLR            , IMemoryManager::MEM_TYPE_NVRAM,       NULL},
    {IMemoryManager::STORAGE_VAR_CFGFILECRC     , IMemoryManager::MEM_TYPE_NVRAM,       NULL},
    {IMemoryManager::STORAGE_VAR_SERVICE        , IMemoryManager::MEM_TYPE_NVRAM,       NULL},
    {IMemoryManager::STORAGE_VAR_SERVICEREASON  , IMemoryManager::MEM_TYPE_NVRAM,       NULL},
    {IMemoryManager::STORAGE_VAR_DUMMYSW        , IMemoryManager::MEM_TYPE_NVRAM,       NULL},
    {IMemoryManager::STORAGE_VAR_LOGLEVEL       , IMemoryManager::MEM_TYPE_NVRAM,       NULL},
    {IMemoryManager::STORAGE_VAR_LAST           , IMemoryManager::MEM_TYPE_NONE,        NULL}
};
/* Memory configuration parameters: common memory configuration set:
 *      -Memory device size in Bytes (to be used by the memory Service Provider)
 *      -Memory block size (in Bytes) for writing operations,
 *      -Initial offset from where block starts (usually 0),
 *      -Apply offset or not
 */
static IMemService::memConfigStr memServiceCfg[IMemoryManager::MEM_TYPE_LAST] = {
    {0, 0, 0, 0}, //MEM_TYPE_NONE
#ifdef TARGET_BOARD_G3_SYSTEMPROC
	{MEMDEVICE_SIZE_FRAM,   0, 0 ,0},  //Event mem Service: default value if size cannot be obtained
	{MEMDEVICE_SIZE_NVRAMID,0, 0 ,0},
	{MEMDEVICE_SIZE_NVRAM,  0, 0 ,0},
	{MEMDEVICE_SIZE_FRAM,   0, 0 ,0},
#else
    {MEMDEVICE_SIZE_FRAM, 0, 0 ,0},  //Event mem Service: default value if size cannot be obtained
    {MEMDEVICE_SIZE_NVRAMID,MEM_BLOCKSIZE_NVRAMID,  MEM_OFFSET_NVRAMID   ,MEM_USEOFFSET_NVRAMID},
    {MEMDEVICE_SIZE_NVRAM,  MEM_BLOCKSIZE_NVRAM,    MEM_OFFSET_NVRAM     ,MEM_USEOFFSET_NVRAM  },
    {MEMDEVICE_SIZE_FRAM,   MEM_BLOCKSIZE_FRAM,     MEM_OFFSET_FRAM      ,MEM_USEOFFSET_FRAM   },
#endif
	{0,                     MEM_BLOCKSIZE_FILE,     MEM_OFFSET_FILE      ,MEM_USEOFFSET_FILE   }
};


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

AbstractMemoryManager::AbstractMemoryManager() : initialised(false)
{
    /* Initialise service providers list as empty */
    for(lu_uint32_t i = 0; i < IMemoryManager::MEM_TYPE_LAST; i++)
    {
        m_service[i] = NULL;
    }
}

AbstractMemoryManager::~AbstractMemoryManager()
{
    /* Remove service providers */
    for(lu_uint32_t i = 0; i < IMemoryManager::MEM_TYPE_LAST; i++)
    {
        delete m_service[i];
        m_service[i] = NULL;
    }
}


void AbstractMemoryManager::setSize(IMemoryManager::STORAGE_VAR variable,
                                    size_t size)
{
    internalInit(); //Initialise services if it wasn't already

    for(lu_uint32_t i = 0; i < IMemoryManager::STORAGE_VAR_LAST; i++)
    {
        if(memDestination[i].varType == variable)
        {
            //found:
            if(memDestination[i].memService != NULL)
            {
                //valid:
                memDestination[i].memService->setSize(variable, size);
            }
            return; //search finished
        }
    }
}


IMemoryManager::MEMERROR AbstractMemoryManager::init(IMemoryManager::MEM_TYPE memService)
{
    internalInit(); //Initialise services if it wasn't already

    /* Init an specific memory Service Provider */
    IMemoryManager::MEMERROR res = IMemoryManager::MEMERROR_ACCESS;
    if(m_service[memService] != NULL)
    {
        IMemService::MEMORYERROR result = m_service[memService]->init();

        /* TODO: AP - report every Memory manager Service Provider error */

        switch(memService)
        {
        case IMemoryManager::MEM_TYPE_NVRAMID:
            switch(result)
            {
            case IMemService::MEMORYERROR_NONE:
                //No error
                res = IMemoryManager::MEMERROR_NONE;
                break;
            case IMemService::MEMORYERROR_WARNMAIN:
                //NVRAM ID failed but backup OK
                res = IMemoryManager::MEMERROR_ACCESS;
                break;
            case IMemService::MEMORYERROR_WARNBACKUP:
                //NVRAM APP - ID block (backup) failed but main OK
                res = IMemoryManager::MEMERROR_ACCESSBKP;
                break;
            default:
                //Both main and backup failed
                res = IMemoryManager::MEMERROR_CONFIG;
                break;
            }
            break;
        case IMemoryManager::MEM_TYPE_NVRAM:
            switch(result)
            {
            case IMemService::MEMORYERROR_NONE:
                //No error
                res = IMemoryManager::MEMERROR_NONE;
                break;
            case IMemService::MEMORYERROR_NVRAMAPP_ACCESS:
            case IMemService::MEMORYERROR_NVRAMAPP_UBLOCK_CRC:
                //NVRAM APP failed
                res = IMemoryManager::MEMERROR_ACCESS;
                break;
            case IMemService::MEMORYERROR_RAM:
            case IMemService::MEMORYERROR_BOUNDARIES:
                //Memory error
                res = IMemoryManager::MEMERROR_CONFIG;
                break;
            default:
                break;
            }
            break;
        default:
            //Other Service Providers
            res = (result == IMemService::MEMORYERROR_NONE)?
                            IMemoryManager::MEMERROR_NONE :
                            IMemoryManager::MEMERROR_ACCESS;
            break;
        }
    }
    return res;
}


void AbstractMemoryManager::init()
{
    internalInit(); //Initialise services if it wasn't already

    /* Silent initialisation of all the configured memory Service Providers */
    for(lu_uint32_t i=0; i < IMemoryManager::MEM_TYPE_LAST; i++)
    {
        this->init( static_cast<IMemoryManager::MEM_TYPE>(i) );
    }
}


lu_bool_t AbstractMemoryManager::read(IMemoryManager::STORAGE_VAR variable,
                                      lu_uint8_t* value,
                                      size_t* sizeRead
                                      )
{
    /* Pass the read operation to the proper Memory Service Provider */
    for(lu_uint32_t i=0; i<IMemoryManager::STORAGE_VAR_LAST; i++)
    {
        if( (variable == memDestination[i].varType) &&              //is this variable
            (memDestination[i].memService != NULL) &&               //has a Service Provider
            (memDestination[i].memType != IMemoryManager::MEM_TYPE_NONE)   //has a type assigned
            )
        {
            return memDestination[i].memService->read(variable, value, sizeRead);
        }
    }
    return LU_FALSE;    //not found
}


lu_bool_t AbstractMemoryManager::write(IMemoryManager::STORAGE_VAR variable,
                                       lu_uint8_t* value
                                       )
{
    /* Pass the write operation to the proper Memory Service Provider */
    for(lu_uint32_t i=0; i<IMemoryManager::STORAGE_VAR_LAST; i++)
    {
        if( (variable == memDestination[i].varType) &&              //is this variable
            (memDestination[i].memService != NULL) &&               //has a Service Provider
            (memDestination[i].memType != IMemoryManager::MEM_TYPE_NONE)   //has a type assigned
            )
        {
            return memDestination[i].memService->write(variable, value);
        }
    }
    return LU_FALSE;    //not found
}


lu_bool_t AbstractMemoryManager::invalidate(IMemoryManager::MEM_TYPE memService)
{
    /* Pass the write operation to the proper Memory Service Provider */
    lu_bool_t ret = LU_FALSE;
    //notify to registered memory Service Providers
    if(m_service[memService] != NULL)
    {
        ret = (m_service[memService]->invalidate() == IMemService::MEMORYERROR_NONE)?
                        LU_TRUE : LU_FALSE;
    }
    return ret;
}


void AbstractMemoryManager::flush()
{
    /* Execute the flush operation in all the registered Memory Service Providers */
    for(lu_uint32_t i = 0; i < IMemoryManager::MEM_TYPE_LAST; i++)
    {
        if(m_service[i] != NULL)
        {
            m_service[i]->flush();
        }
    }
}


void AbstractMemoryManager::close()
{
    /* Execute the close operation in all the registered Memory Service Providers */
    for(lu_uint32_t i = 0; i < IMemoryManager::MEM_TYPE_LAST; i++)
    {
        if(m_service[i] != NULL)
        {
            m_service[i]->close();
        }
    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void AbstractMemoryManager::internalInit()
{
    if(initialised)
        return;

    /* Instantiate managers as needed */

    //Fill memDestination's memService field by creating Memory Service Providers through a factory
    for(lu_uint32_t i = 0; i < IMemoryManager::STORAGE_VAR_LAST; i++)
    {
        if(m_service[memDestination[i].memType] == NULL)
        {
            //Not already created. Create it
            m_service[memDestination[i].memType] = createService(memDestination[i].memType);
        }
        //keep track of created Service Provider
        memDestination[i].memService = m_service[memDestination[i].memType];
    }

    /* Configure memory Service Providers */
    for(lu_uint32_t i = 0; i < IMemoryManager::MEM_TYPE_LAST; i++)
    {
        if(m_service[i] != NULL)
        {
            //Service Provider present: configure it
            m_service[i]->setMemoryConfig(memServiceCfg[i]);
        }
    }

    initialised = true; //List is initialised
}


/*
 *********************** End of file ******************************************
 */
