/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: MemServiceFile.cpp 20-Dec-2012 15:49:59 pueyos_a $
 *               $HeadURL: U:\G3\RTU\MCM\src\MemoryManager\src\MemServiceFile.cpp $
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Module for handling storage in a file like it was a memory device.
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 20-Dec-2012 15:49:59	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   20-Dec-2012 15:49:59  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <cstring>
#include <cstdlib>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "MemServiceFile.h"
#include "crc32.h"
#include "NVRAMDef.h"   //use of the header for CRC handling purposes

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
const lu_uint32_t MEMBLOCKSIZE = 10000;

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
MemServiceFile::MemServiceFile(const lu_char_t *fileName) :
                                FileName(fileName),
                                fileMem(NULL),
                                fileAccess(LU_TRUE),
                                updPending(LU_FALSE),
                                memReadFileBuffer(NULL),
                                memWriteFileBuffer(NULL),
                                bufSize(sizeof(NVRAMBlkHeadStr)),
                                dataSize(0)
{
    /* initialise internal table for memory storage */
    for(lu_uint32_t i=0; i < IMemoryManager::STORAGE_VAR_LAST; i++)
    {
        memFileIndex[i].varEnum = IMemoryManager::STORAGE_VAR_LAST; //none yet
        memFileIndex[i].varSize = 0;
        memFileIndex[i].filePos = 0;
        memFileIndex[i].invalid = LU_TRUE;
    }
}

MemServiceFile::~MemServiceFile()
{
    this->close();
    delete [] memReadFileBuffer;
    delete [] memWriteFileBuffer;
}


void MemServiceFile::setSize(IMemoryManager::STORAGE_VAR variable, size_t size)
{
    //update internal table
    for(lu_uint32_t i=0; i < IMemoryManager::STORAGE_VAR_LAST; i++)
    {
        if(memFileIndex[i].varEnum == variable)
        {
            //already present: update
            memFileIndex[i].varSize = size;
            return;
        }
        if(memFileIndex[i].varEnum == IMemoryManager::STORAGE_VAR_LAST)
        {
            //first empty entry: fill it
            memFileIndex[i].varEnum = variable;
            memFileIndex[i].varSize = size;
            return;
        }
    }
}


IMemService::MEMORYERROR MemServiceFile::init()
{
    const lu_char_t *FTITLE = "MemFileService::init:";
    IMemService::MEMORYERROR ret = IMemService::MEMORYERROR_FILE_ACCESS;

    if(initialised == LU_TRUE)
    {
        return IMemService::MEMORYERROR_NONE;     //already initialised
    }

    //calculate space for memory storage buffer
    for(lu_uint32_t i=0;
        ( (i < IMemoryManager::STORAGE_VAR_LAST) &&
            (memFileIndex[i].varEnum != IMemoryManager::STORAGE_VAR_LAST) );
        ++i
        )
    {
        memFileIndex[i].filePos = bufSize;
        bufSize += memFileIndex[i].varSize;
    }
    //reserve memory for that (initialised to 0)
//    memReadFileBuffer = (lu_uint8_t *)calloc(1, bufSize);
//    memWriteFileBuffer = (lu_uint8_t *)calloc(1, bufSize);
    memReadFileBuffer = new lu_uint8_t[bufSize]();
    memWriteFileBuffer = new lu_uint8_t[bufSize]();
    dataSize = bufSize - sizeof(NVRAMBlkHeadStr);

    if( (memReadFileBuffer == NULL) || (memWriteFileBuffer == NULL) )
    {
        delete [] memReadFileBuffer;
        memReadFileBuffer = NULL;
        delete [] memWriteFileBuffer;
        memWriteFileBuffer = NULL;
        log.error("%s Error reserving memory for non-volatile memory file.", FTITLE);
        return IMemService::MEMORYERROR_RAM;
    }
    if(bufSize > MEMBLOCKSIZE)
    {
        log.error("%s Data to be written will be bigger than block boundaries.", FTITLE);
        return IMemService::MEMORYERROR_BOUNDARIES;
    }

    //Try to read memory file
    ret = IMemService::MEMORYERROR_FILE_ACCESS;
    do
    {
        fileMem = fopen(FileName, "r+");
        if(fileMem == NULL)
        {
            break;
        }
        NVRAMBlkHeadStr* headerPtr = reinterpret_cast<NVRAMBlkHeadStr *>(memReadFileBuffer);
        if( fread(memReadFileBuffer, 1, bufSize, fileMem) != bufSize)
        {
            break;
        }
        /* Check memory file integrity */
        lu_uint32_t crc32n = 0;
        crc32_calc32(memReadFileBuffer + sizeof(NVRAMBlkHeadStr), dataSize, &crc32n);
        if( (headerPtr->dataCrc32 != crc32n) || (headerPtr->dataSize != dataSize) )
        {
            log.error("%s CRC error in non-volatile memory file.", FTITLE);
            ret = IMemService::MEMORYERROR_FILE_CRC;
            break;
        }
        //Copy buffer to scratch writing one
        memcpy(memWriteFileBuffer, memReadFileBuffer, bufSize);

        //Mark values read as valid
        for(lu_uint32_t i=0; i < IMemoryManager::STORAGE_VAR_LAST; i++)
        {
            memFileIndex[i].invalid = LU_FALSE;
        }
        initialised = LU_TRUE; //file already initialised
        log.info("%s data acquired correctly from File memory device %s.", FTITLE, FileName);
        ret = IMemService::MEMORYERROR_NONE;
    } while(0);

    if( (fileMem != NULL) && (ret != MEMORYERROR_NONE) )
    {
        fclose(fileMem);
        fileMem = NULL;
    }
    if(fileMem == NULL)
    {
        //report error reading file
        log.error("%s Error reading file for non-volatile memory. "
                    "The file will be created again with default values.",
                    FTITLE
                  );

        //file doesn't exist or is corrupted: (re-)create it
        fileMem = fopen(FileName, "w");
        if(fileMem == NULL)
        {
            log.error("%s Unable to create a file. Related values will not be stored.", FTITLE);
            ret = IMemService::MEMORYERROR_FILE_ACCESS;
        }
        /*
         * NOTE: if(fileMem != NULL), then is OK as long as memFileIndex[i].invalid
         *      is LU_TRUE, since file is open but values aren't valid.
         */
    }
    if(fileMem != NULL)
    {
        initialised = LU_TRUE; //file already initialised
    }
    return ret;
}


lu_bool_t MemServiceFile::read(IMemoryManager::STORAGE_VAR variable, lu_uint8_t* value, size_t* sizeRead)
{
    if(initialised == LU_FALSE)
    {
        //tried to read before initialising
        if(init() != IMemService::MEMORYERROR_NONE)
        {
            return LU_FALSE;
        }
    }
    if(memWriteFileBuffer == NULL)
    {
        return LU_FALSE;
    }
    for(lu_uint32_t i=0; memFileIndex[i].varEnum != IMemoryManager::STORAGE_VAR_LAST; i++)
    {
        if(memFileIndex[i].varEnum == variable)
        {
            if(memFileIndex[i].invalid == LU_TRUE)
            {
                return LU_FALSE;    //datum is not valid yet
            }
            //variable found: return stored value
            memcpy(value, &memWriteFileBuffer[memFileIndex[i].filePos], memFileIndex[i].varSize);
            if(sizeRead != NULL)
            {
                *sizeRead = memFileIndex[i].varSize;
            }
            return LU_TRUE; //recovered successfully
        }
    }
    return LU_FALSE;    //error: variable not found
}


lu_bool_t MemServiceFile::write(IMemoryManager::STORAGE_VAR variable, lu_uint8_t* value)
{
    lu_bool_t updateit = LU_FALSE;

    if(initialised == LU_FALSE)
    {
        //tried to write before initialising
        IMemService::MEMORYERROR result = init();
        switch(result)
        {
        case IMemService::MEMORYERROR_NONE:
        case IMemService::MEMORYERROR_FILE_CRC:
            break;
        default:
            return LU_FALSE;
        }
    }
    if(memReadFileBuffer == NULL)
    {
        return LU_FALSE;
    }
    for(lu_uint32_t i=0; memFileIndex[i].varEnum != IMemoryManager::STORAGE_VAR_LAST; i++)
    {
        if(memFileIndex[i].varEnum == variable)
        {
            //variable found
            if(memFileIndex[i].invalid == LU_FALSE)
            {
                //valid: check value change
                if( memcmp(&memWriteFileBuffer[memFileIndex[i].filePos],
                                value,
                                memFileIndex[i].varSize) != 0
                            )
                {
                    //update value
                    updateit = LU_TRUE;
                }
            }
            else
            {
                updateit = LU_TRUE; //value is invalid: update
            }
            if(updateit == LU_TRUE)
            {
                memcpy(&memWriteFileBuffer[memFileIndex[i].filePos], value, memFileIndex[i].varSize);
                memFileIndex[i].invalid = LU_FALSE; //mark value as no longer invalid
                updPending = checkMemChange();  //mark to write only when differs from what was stored
            }
            return LU_TRUE; //update writing successfully (or value was already that)
        }
    }
    return LU_FALSE;    //error: variable not found
}

IMemService::MEMORYERROR MemServiceFile::invalidate()
{
    return update(LU_TRUE); //write an invalid CRC to memory
}

void MemServiceFile::flush()
{
    if( (updPending == LU_TRUE) && (fileMem != NULL) && (initialised == LU_TRUE) )
    {
        //write only when data has changed and is initialised
        updPending = (update() == IMemService::MEMORYERROR_NONE)? LU_FALSE : LU_TRUE;
    }
}

void MemServiceFile::close()
{
    this->flush();
    LockingMutex lMutex(fileAccess);
    if(fileMem != NULL)
    {
        fclose(fileMem);
    }
    fileMem = NULL;
    updPending = LU_FALSE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

lu_bool_t MemServiceFile::checkMemChange()
{
    if( (memReadFileBuffer == NULL) || (memWriteFileBuffer == NULL) ||
        (fileMem == NULL) || (initialised == LU_FALSE)
        )
    {
        return LU_FALSE;    //device not initialised
    }
    //return LU_TRUE when they are different (ignoring the header)
    return (memcmp(memReadFileBuffer + sizeof(NVRAMBlkHeadStr),
                    memWriteFileBuffer + sizeof(NVRAMBlkHeadStr),
                    dataSize) == 0)? LU_FALSE : LU_TRUE;
}


IMemService::MEMORYERROR MemServiceFile::update(lu_bool_t invalidate)
{
    IMemService::MEMORYERROR result = IMemService::MEMORYERROR_NONE;

    LockingMutex lMutex(fileAccess);
    if(fileMem == NULL)
    {
        return IMemService::MEMORYERROR_FILE_ACCESS;
    }
    log.debug("%s: flushing memory to %s device.", __AT__, FileName);

    //try to write memory
    do
    {
        NVRAMBlkHeadStr* headerPtr = reinterpret_cast<NVRAMBlkHeadStr *>(memWriteFileBuffer);


        /* calculate CRC32 */
        lu_uint32_t crc32n = 0;
        crc32_calc32(memWriteFileBuffer + sizeof(NVRAMBlkHeadStr), dataSize, &crc32n);
        headerPtr->dataCrc32 = crc32n;
        headerPtr->dataSize = dataSize;

        if(invalidate == LU_TRUE)
        {
            /* To invalidate, assign an invalid CRC */
            crc32n ^= 0xFFFFFFFF;
            while(crc32n == headerPtr->dataCrc32)  //enforce that it is invalid
            {
                crc32n++;
            }
            headerPtr->dataCrc32 = crc32n;
            /* Write header only */
            if(writeToMemory(fileMem, NULL, memWriteFileBuffer, bufSize, 0, LU_TRUE) != LU_TRUE)
            {
                result = IMemService::MEMORYERROR_FILE_ACCESS;
            }
        }
        else
        {
            /* write header+data */
            if(writeToMemory(fileMem, memReadFileBuffer, memWriteFileBuffer, bufSize) != LU_TRUE)
            {
                result = IMemService::MEMORYERROR_FILE_ACCESS;
                break;
            }
            memcpy(memReadFileBuffer, memWriteFileBuffer, bufSize);
        }
        //force writing to the device by flush+close and then re-open it
        fileMem = freopen(FileName, "r+", fileMem);
        if(fileMem == NULL)
        {
            result = IMemService::MEMORYERROR_FILE_ACCESS;
            break;
        }
    } while(LU_FALSE);
    switch(result)
    {
        case IMemService::MEMORYERROR_NONE:
            return result;  //successful operation
            break;
        default:
            //device fault
            fclose(fileMem);
            fileMem = NULL;
    }
    return result;    //unsuccessful operation
}


/*
 *********************** End of file ******************************************
 */

