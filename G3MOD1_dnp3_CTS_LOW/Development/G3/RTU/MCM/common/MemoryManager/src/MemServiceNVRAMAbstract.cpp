/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: MemServiceNVRAMAbstract.cpp 2 Apr 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/MemoryManager/src/MemServiceNVRAMAbstract.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 2 Apr 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   2 Apr 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <cstdlib>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "MemServiceNVRAMAbstract.h"
#include "crc32.h"
#include "NVRAMDef.h"
#include "MCMIOMap.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
lu_bool_t MemServiceNVRAMAbstract::initialisedGPIO = LU_FALSE;
FILE* MemServiceNVRAMAbstract::wenGPIO = NULL;

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

MemServiceNVRAMAbstract::MemServiceNVRAMAbstract()
{
    if(initialisedGPIO == LU_FALSE)
    {
        initialisedGPIO = LU_TRUE;  //try once only
        /* Initialise the APPRAM Write Enable GPIO */
        if (initGPIO(MCMOutputGPIO_APPRAM_WEN, GPIOLIB_DIRECTION_OUTPUT, &wenGPIO) != 0)
        {
            log.error("%s error initialising APPRAM_WEN GPIO [%d]",
                        __AT__, MCMOutputGPIO_APPRAM_WEN);
        }
    }
}

MemServiceNVRAMAbstract::~MemServiceNVRAMAbstract()
{
    if(wenGPIO != NULL)
    {
        closeGPIO(wenGPIO); //not really needed to close GPIO - it will be closed at program's termination
        wenGPIO = NULL;
    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
lu_bool_t MemServiceNVRAMAbstract::enableWrite(lu_bool_t enable)
{
    //writes a 0 for enable, and a 1 for disable writing
    return ( writeGPIO(wenGPIO, (enable==LU_TRUE)? LU_FALSE : LU_TRUE) > 0)?
                    LU_TRUE : LU_FALSE;
}


void MemServiceNVRAMAbstract::updateCRC(lu_uint8_t* buffer, const lu_uint32_t bufSize)
{
    if(buffer == NULL)
        return; //nothing to do

    /* add header (size+CRC) to data block */
    NVRAMBlkHeadStr *headerPtr = reinterpret_cast<NVRAMBlkHeadStr *>(buffer);
    memset(headerPtr, 0, sizeof(NVRAMBlkHeadStr));
    lu_uint32_t crc32;
    lu_uint16_t dataSize = bufSize - sizeof(NVRAMBlkHeadStr);
    crc32_calc32(buffer + sizeof(NVRAMBlkHeadStr), dataSize, &crc32);
    headerPtr->dataCrc32 = crc32;
    headerPtr->dataSize = dataSize;
}


IMemService::MEMORYERROR MemServiceNVRAMAbstract::readMemory(const lu_char_t* devicePath,
                                                                lu_uint8_t* buffer,
                                                                const lu_uint32_t bufSize
                                                                )
{
    lu_uint32_t offset = (memConfig.useOffset == LU_TRUE)? memConfig.offset : 0;
    return readMemory(devicePath, buffer, bufSize, offset);
}

IMemService::MEMORYERROR MemServiceNVRAMAbstract::readMemory(const lu_char_t* devicePath,
                                                                lu_uint8_t* buffer,
                                                                const lu_uint32_t bufSize,
                                                                const lu_uint32_t offset
                                                                )
{
    IMemService::MEMORYERROR ret = IMemService::MEMORYERROR_ACCESS;
    FILE* device;

    if( (devicePath == NULL) || (buffer == NULL) || (bufSize == 0) )
    {
        return ret; //bad params
    }

    device = fopen(devicePath, "r");
    if(device == NULL)
    {
        return ret;  //error
    }
    //try to read memory
    do
    {
        if(fseek(device, offset, SEEK_SET) != 0)
        {
            break;  //error
        }
        if(fread(buffer, 1, bufSize, device) != bufSize)
        {
            break;  //error
        }

        /* Check memory integrity / checksum */
        lu_uint32_t crc32;
        NVRAMBlkHeadStr *headerPtr = reinterpret_cast<NVRAMBlkHeadStr *>(buffer);
        crc32_calc32(buffer + sizeof(NVRAMBlkHeadStr), bufSize - sizeof(NVRAMBlkHeadStr), &crc32);
        if(crc32 != headerPtr->dataCrc32)
        {
            ret = IMemService::MEMORYERROR_CRC;
            break;
        }
        ret = IMemService::MEMORYERROR_NONE;
    } while(0); //run once

    fclose(device);
    return ret;
}


IMemService::MEMORYERROR MemServiceNVRAMAbstract::writeMemory(const lu_char_t* devicePath,
                                                                 const lu_uint8_t* buffer,
                                                                 const lu_uint32_t bufSize
                                                                 )
{
    IMemService::MEMORYERROR ret = IMemService::MEMORYERROR_ACCESS;
    lu_uint8_t* bufferRead = NULL;
    FILE* device;

    if( (devicePath == NULL) || (buffer == NULL) || (bufSize == 0) )
    {
        return ret; //bad params
    }

    if(enableWrite(LU_TRUE) != LU_TRUE)     //enable writing
    {
        return ret;  //error
    }
    device = fopen(devicePath, "w+");
    if(device == NULL)
    {
        enableWrite(LU_FALSE);
        return ret;  //error
    }
    //try to write memory
    do
    {
        lu_uint32_t offset = (memConfig.useOffset == LU_TRUE)?memConfig.offset : 0;
        if(fseek(device, offset, SEEK_SET) != 0)
        {
            break;  //error
        }
        /* Read current memory content for comparison */
        bufferRead = new lu_uint8_t[bufSize]();
        if(bufferRead == NULL)
        {
            ret = IMemService::MEMORYERROR_RAM;
            break;  //error
        }
        if(fread(bufferRead, 1, bufSize, device) != bufSize)
        {
            break;  //error
        }
        /* Write data stored into buffer */
        if(writeToMemory(device, bufferRead, buffer, bufSize) == bufSize )
        {
            ret = IMemService::MEMORYERROR_NONE;
        }
        ret = IMemService::MEMORYERROR_NONE;
    } while(0); //run once

    fclose(device);
    if(enableWrite(LU_FALSE) == LU_FALSE)     //disable writing
    {
        //failed to disable writing: warning only
        log.warn("%s Unable to restore permission after writing on device %s, ID block",
                 __AT__, devicePath
                 );
    }

    if(ret == IMemService::MEMORYERROR_NONE)
    {
        /* Read back to check if writing really successful */
        ret = readMemory(devicePath, bufferRead, bufSize);
        if(ret == IMemService::MEMORYERROR_NONE)
        {
            if(memcmp(buffer, bufferRead, bufSize) != 0)
            {
                ret = IMemService::MEMORYERROR_CRC;  //error writing intended data!
            }
        }
    }

    if(bufferRead != NULL)
    {
        delete [] bufferRead;
    }
    return ret;

}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
