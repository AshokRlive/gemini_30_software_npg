/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: MemServiceFRAM.cpp 07-Jan-2013 15:29:15 pueyos_a $
 *               $HeadURL: U:\G3\RTU\MCM\src\MemoryManager\src\MemServiceFRAM.cpp $
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Module for handling FRAM storage.
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 07-Jan-2013 15:29:15	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   07-Jan-2013 15:29:15  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <cstring>
#include <cstdlib>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "MemServiceFRAM.h"
#include "crc32.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Classes
 ******************************************************************************
 */
MemServiceFRAM::MemServiceFRAM(const lu_char_t *memoryDeviceName) :
                                    MemoryDevice(memoryDeviceName),
                                    memDevice(NULL),
                                    memAccess(LU_TRUE),
                                    updPending(LU_FALSE),
                                    baseAddress(0),
                                    memReadBuffer(NULL),
                                    memWriteBuffer(NULL),
                                    dataSize(0)
{
    bufSize = NVRAM_APP_BLK_USER_A_SIZE;       //block size

    /* -- initialise internal table for memory storage -- */
    for(lu_uint32_t i=0; i < IMemoryManager::STORAGE_VAR_LAST; i++)
    {
        memIndex[i].varEnum = IMemoryManager::STORAGE_VAR_LAST; //none yet
        memIndex[i].varSize = 0;
        memIndex[i].address = 0;
        memIndex[i].invalid = LU_TRUE;
    }

    /* Open device for memory access */
    memDevice = fopen(MemoryDevice, "r+");
    if(memDevice == NULL)
    {
        log.error("%s error accessing FRAM device", __AT__);
    }
}


MemServiceFRAM::~MemServiceFRAM()
{
    this->close();
    delete [] memReadBuffer;
    delete [] memWriteBuffer;
}


void MemServiceFRAM::setSize(IMemoryManager::STORAGE_VAR variable, size_t size)
{
    //update internal table
    for(lu_uint32_t i=0; i < IMemoryManager::STORAGE_VAR_LAST; i++)
    {
        if(memIndex[i].varEnum == variable)
        {
            //already present: update
            memIndex[i].varSize = size;
            return;
        }
        if(memIndex[i].varEnum == IMemoryManager::STORAGE_VAR_LAST)
        {
            //first empty entry: fill it
            memIndex[i].varEnum = variable;
            memIndex[i].varSize = size;
            return;
        }
    }
}


IMemService::MEMORYERROR MemServiceFRAM::init()
{
    const lu_char_t *FTITLE = "MemFRAMService::init:";
    IMemService::MEMORYERROR ret = IMemService::MEMORYERROR_FRAM_ACCESS;
    lu_uint32_t bufPos = 0;

    if(initialised == LU_TRUE)
    {
        return IMemService::MEMORYERROR_NONE;     //already initialised
    }
    if(memDevice == NULL)
    {
        return IMemService::MEMORYERROR_FRAM_ACCESS; //file was not open: unable to initialise
    }

    //calculate space for memory storage buffer
    for(lu_uint32_t i=0; ( (i < IMemoryManager::STORAGE_VAR_LAST) &&
                            (memIndex[i].varEnum != IMemoryManager::STORAGE_VAR_LAST) );
        ++i)
    {
        memIndex[i].address = bufPos;
        bufPos += memIndex[i].varSize;
    }

    //reserve memory for that (initialised to 0)
//    memReadBuffer = (lu_uint8_t *)calloc(1, bufSize);
//    memWriteBuffer = (lu_uint8_t *)calloc(1, bufSize);
    memReadBuffer = new lu_uint8_t[bufSize]();
    memWriteBuffer = new lu_uint8_t[bufSize]();
    dataSize = bufPos;    //set the stored data size

    if( (memReadBuffer == NULL) || (memWriteBuffer == NULL) )
    {
        //memory error
        delete [] memReadBuffer;
        memReadBuffer = NULL;
        delete [] memWriteBuffer;
        memWriteBuffer = NULL;
        log.error("%s error reserving buffer memory: not enough memory.", FTITLE);
        return IMemService::MEMORYERROR_RAM;
    }

    LockingMutex lMutex(memAccess);

    if(memDevice != NULL)
    {
        if(readMemory(memDevice, baseAddress) == LU_FALSE)
        {
            //Memory APP device failed reading or CRC
            if(memDevice != NULL)
            {
                //incorrect CRC
                ret = IMemService::MEMORYERROR_FRAM_CRC;
            }
        }
        else
        {
            //memory read OK
            initialised = LU_TRUE; //file already initialised
            ret = IMemService::MEMORYERROR_NONE;
        }
    }
    switch(ret)
    {
        case IMemService::MEMORYERROR_FRAM_ACCESS:
            log.error("%s error accessing FRAM memory device.", FTITLE);
            break;
        case IMemService::MEMORYERROR_FRAM_CRC:
            log.error("%s CRC error in FRAM memory device.", FTITLE);
            break;
        case IMemService::MEMORYERROR_NONE:
            log.info("%s data acquired correctly from FRAM memory device.", FTITLE);
            break;
        default:
            break;
    }
    return ret;
}



lu_bool_t MemServiceFRAM::read(IMemoryManager::STORAGE_VAR variable, lu_uint8_t *value, size_t* sizeRead)
{
    if(initialised == LU_FALSE)
    {
        //tried to read before initialising
        if(init() != IMemService::MEMORYERROR_NONE)
        {
            return LU_FALSE;
        }
    }
    for(lu_uint32_t i=0; memIndex[i].varEnum != IMemoryManager::STORAGE_VAR_LAST; i++)
    {
        if(memIndex[i].varEnum == variable)
        {
            if(memIndex[i].invalid == LU_TRUE)
            {
                return LU_FALSE;    //datum is not valid yet
            }
            //variable found: return stored value
            memcpy(value, &memWriteBuffer[memIndex[i].address], memIndex[i].varSize);
            if(sizeRead != NULL)
            {
                *sizeRead = memIndex[i].varSize;
            }
            return LU_TRUE; //recovered successfully
        }
    }
    return LU_FALSE;    //error: variable not found
}


lu_bool_t MemServiceFRAM::write(IMemoryManager::STORAGE_VAR variable, lu_uint8_t *value)
{
    lu_bool_t updateit = LU_FALSE;

    if(initialised == LU_FALSE)
    {
        //tried to write before initialising
        if(init() != IMemService::MEMORYERROR_NONE)
        {
            return LU_FALSE;
        }
    }
    for(lu_uint32_t i=0; memIndex[i].varEnum != IMemoryManager::STORAGE_VAR_LAST; i++)
    {
        if(memIndex[i].varEnum == variable)
        {
            //variable found
            if(memIndex[i].invalid == LU_FALSE)
            {
                //valid: check value change
                if( memcmp(&memWriteBuffer[memIndex[i].address],
                                value,
                                memIndex[i].varSize) != 0
                            )
                {
                    //update value
                    updateit = LU_TRUE;
                }
            }
            else
            {
                updateit = LU_TRUE; //value is invalid: update
            }
            if(updateit == LU_TRUE)
            {
                memcpy(&memWriteBuffer[memIndex[i].address], value, memIndex[i].varSize);
                memIndex[i].invalid = LU_FALSE; //mark value as no longer invalid
                updPending = checkMemChange();  //mark to write only when differs from what was stored
            }
            return LU_TRUE; //update writing successfully (or value was already that)
        }
    }
    return LU_FALSE;    //error: variable not found
}


IMemService::MEMORYERROR MemServiceFRAM::invalidate()
{
    return update(LU_TRUE);    //write an invalid CRC to memory
}


void MemServiceFRAM::flush()
{
    if( (updPending == LU_TRUE) && (initialised == LU_TRUE) )
    {
        //write only when data has changed and is initialised
        switch(update())
        {
        case IMemService::MEMORYERROR_NONE:
        case IMemService::MEMORYERROR_FRAM_ACCESS:
            updPending = LU_FALSE;
            break;
        default:
            updPending = LU_TRUE;
            break;
        }
    }
}


void MemServiceFRAM::close()
{
    this->flush();  //write any pending data
    LockingMutex lMutex(memAccess);

    //close memory device
    if(memDevice != NULL)
    {
        fclose(memDevice);
    }
    memDevice = NULL;
    updPending = LU_FALSE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

lu_bool_t MemServiceFRAM::checkMemChange()
{
    if( (memReadBuffer == NULL) || (memWriteBuffer == NULL) ||
        (memDevice == NULL) || (initialised == LU_FALSE)
        )
    {
        return LU_FALSE;    //device not initialised
    }
    //return LU_TRUE when they are different (ignoring the header)
    return (memcmp(memReadBuffer + sizeof(NVRAMBlkHeadStr),
                    memWriteBuffer + sizeof(NVRAMBlkHeadStr),
                    dataSize
                    ) == 0)? LU_FALSE : LU_TRUE;

}


lu_bool_t MemServiceFRAM::readMemory(FILE *device, lu_uint32_t offset)
{
    MemServiceFRAM::MEMH_ERROR result = MemServiceFRAM::MEMH_ERROR_NONE;

    if(device == NULL)
    {
        return LU_FALSE;
    }
    //try to read memory
    do
    {
        NVRAMBlkHeadStr header;
        if(fseek(device, offset, SEEK_SET) != 0)
        {
            //error
            result = MemServiceFRAM::MEMH_ERROR_READ;
            break;
        }
        //read header
        if( fread(&header, sizeof(NVRAMBlkHeadStr), 1, device) != 1 )
        {
            //error
            result = MemServiceFRAM::MEMH_ERROR_READ;
            break;
        }
        bufSize = header.dataSize;

        if( fread(memReadBuffer, 1, bufSize, device) != bufSize )
        {
            //error
            result = MemServiceFRAM::MEMH_ERROR_READ;
            break;
        }

        /* Check memory integrity/checksum */
        lu_uint32_t crc32n = 0;
        crc32_calc32(memReadBuffer, dataSize, &crc32n);
        if(header.dataCrc32 != crc32n)
        {
            //error
            result = MemServiceFRAM::MEMH_ERROR_READ;
            break;
        }

        //Copy to scratch writing buffer
        memcpy(memWriteBuffer, memReadBuffer, bufSize);

        //Mark values read as valid
        for(lu_uint32_t i=0; i < IMemoryManager::STORAGE_VAR_LAST; i++)
        {
            memIndex[i].invalid = LU_FALSE;
        }
    } while(LU_FALSE);
    switch(result)
    {
        case MemServiceFRAM::MEMH_ERROR_NONE:
            return LU_TRUE;  //successful operation
        case MemServiceFRAM::MEMH_ERROR_CRC:
            //CRC fail
            break;
        default:
            //device fault
            fclose(device);
            device = NULL;
    }
    return LU_FALSE;    //unsuccessful operation
}


MemServiceFRAM::MEMH_ERROR MemServiceFRAM::writeMemory(FILE *device, lu_uint32_t offset, lu_uint8_t *buffer, lu_uint32_t bufferSize, lu_bool_t invalidate)
{
    LU_UNUSED(offset);
    MemServiceFRAM::MEMH_ERROR result = MemServiceFRAM::MEMH_ERROR_NONE;

    if(device == NULL)
    {
        return MemServiceFRAM::MEMH_ERROR_DEVICE;
    }

    LockingMutex lMutex(memAccess);
    //try to write memory
    do
    {
        NVRAMBlkHeadStr header;
        header.dataSize = bufferSize;

        crc32_calc32(buffer, bufferSize, &header.dataCrc32);

        if(invalidate == LU_TRUE)
        {
            /* To invalidate, assign an invalid CRC */
            lu_uint32_t crc32;
            crc32 = header.dataCrc32;
            crc32 ^= 0xFFFFFFFF;
            while(crc32 == header.dataCrc32)  //enforce that it is invalid
            {
                crc32++;
            }
            header.dataCrc32 = crc32;
        }
        /* Write header */
        if( fwrite(buffer, sizeof(NVRAMBlkHeadStr), 1, device) != 1 )
        {
            result = MemServiceFRAM::MEMH_ERROR_WRITE;
        }
        break;

        if(invalidate != LU_TRUE)
        {
            /* write data too */
            if( fwrite(buffer, 1, bufferSize, device) != bufferSize)
            {
                result = MemServiceFRAM::MEMH_ERROR_WRITE;
                break;
            }
            memcpy(memReadBuffer, buffer, bufferSize);
            fflush(device);
        }
    } while(LU_FALSE);
    switch(result)
    {
        case MemServiceFRAM::MEMH_ERROR_NONE:
            return result;  //successful operation
            break;
        default:
            //device fault
            fclose(device);
            device = NULL;
    }
    return result;    //unsuccessful operation

}


IMemService::MEMORYERROR MemServiceFRAM::update(lu_bool_t invalidate)
{
    IMemService::MEMORYERROR result = IMemService::MEMORYERROR_FRAM_ACCESS;

    log.debug("%s: flushing memory.", __AT__);

    LockingMutex lMutex(memAccess);
    if(memDevice != NULL)
    {
        if( writeMemory(memDevice, baseAddress, memWriteBuffer,
                        bufSize, invalidate) == MEMH_ERROR_NONE )
        {
            result = IMemService::MEMORYERROR_NONE;
        }
    }

    return result;
}


/*
 *********************** End of file ******************************************
 */

