/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: MemServiceNVRAMID.cpp 28 Mar 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/MemoryManager/src/MemServiceNVRAMID.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 28 Mar 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   28 Mar 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "MemServiceNVRAMID.h"
#include "NVRAMDef.h"
#include "versions.h"   //BasicVersionStr definition
#include "ModuleProtocol.h" //ModuleUIDStr definition

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/* Default offset values for ID memory and its backup */
#define MEMNVRAMIDH_ID_OFFSET       NVRAM_ID_BLK_INFO_OFFSET    //defined in NVRAMDefID.h

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
MemServiceNVRAMID::MemServiceNVRAMID(const lu_char_t* MEMDEVICE_ID_PATH,
                                     const lu_char_t* MEMDEVICE_APP_PATH) :
                                        MemDevice_ID(MEMDEVICE_ID_PATH),
                                        MemDevice_AppID(MEMDEVICE_APP_PATH),
                                        bufferID(NULL),
                                        bufNVRAM(&bufferID, sizeof(NVRAMBlkHeadStr)+ sizeof(NVRAMInfoStr)),
                                        valid(LU_FALSE)
{
    /* Set default values for memory configuration - might be overridden by setMemoryConfig()
     * Note that this is used only for backup storage since it is the only writable in this case.
     */
//    memConfig.memorySize = MEMNVRAMIDH_APPID_SIZE;
//    memConfig.blockSize = MEMNVRAMIDH_APPID_SIZE;
//    memConfig.offset = MEMNVRAMIDH_APPID_OFFSET;
//    memConfig.useOffset = LU_TRUE;
}

MemServiceNVRAMID::~MemServiceNVRAMID()
{
    /* TODO: pueyos_a - use bufNVRAM instead of bufferID */
    if(bufferID != NULL)
    {
        delete [] bufferID;
    }
}


void MemServiceNVRAMID::setSize(IMemoryManager::STORAGE_VAR variable, size_t size)
{
    LU_UNUSED(variable);
    LU_UNUSED(size);
    return; //No size to set for NVRAM ID
}


IMemService::MEMORYERROR MemServiceNVRAMID::init()
{
    const lu_char_t *FTITLE = "MemNVRAMIDService::init:";

    MasterLockingMutex iMutex(memAccess, LU_TRUE);

    if(initialised == LU_TRUE)
    {
        return IMemService::MEMORYERROR_NONE;     //already initialised
    }

    IMemService::MEMORYERROR ret = IMemService::MEMORYERROR_WARNMAIN;
    IMemService::MEMORYERROR retApp = IMemService::MEMORYERROR_WARNBACKUP;

    //reserve memory for data (initialised to 0)
    const lu_uint32_t DataSize = bufNVRAM.getEntries(); //set the stored data size
    bufferID = new lu_uint8_t[DataSize]();

    /* Check/Read ID memory device first */
    ret = readMemory(MemDevice_ID, bufferID, DataSize, MEMNVRAMIDH_ID_OFFSET);


    /* XXX: pueyos_a - debug (to be removed) */
//    printf("NVRAMID h+data (%d size)\n", DataSize);
//    for (lu_uint32_t c = 0; c < DataSize; ++c)
//    {
//        printf("0x%02x ", bufferID[c]);
//    }
//    printf("\n");
//    for (lu_uint32_t c = 0; c < DataSize; ++c)
//    {
//        if(bufferID[c] < 32)
//            printf("%c ", '.');
//        else
//            printf("%c ", bufferID[c]);
//    }
//    printf("\n");
    /* XXX: pueyos_a - debug (to be removed) */
    NVRAMInfoStr* infoNVRAM = (NVRAMInfoStr *)(bufferID + sizeof(NVRAMBlkHeadStr));
//    printf("NVVV F.M= 0x%04x, F.m= 0x%04x, S/N= 0x%04x\n",
//            infoNVRAM->moduleFeatureMajor, infoNVRAM->moduleFeatureMinor, infoNVRAM->serialNumber);
    std::string batchNumber((char *)infoNVRAM->batchNumber, NVRAMINFO_BATCH_MAX_SIZE);
    std::string supplierId((char *)infoNVRAM->supplierId, NVRAMINFO_SUPPLY_ID_MAX_SIZE);
    std::string assemblyNo ((char *)infoNVRAM->assemblyNo , NVRAMINFO_ASSY_NO_MAX_SIZE);
    std::string assemblyRev((char *)infoNVRAM->assemblyRev, NVRAMINFO_ASSY_REV_MAX_SIZE);
    std::string subAssemblyNo ((char *)infoNVRAM->subAssemblyNo , NVRAMINFO_ASSY_NO_MAX_SIZE);
    std::string subAssemblyRev((char *)infoNVRAM->subAssemblyRev, NVRAMINFO_ASSY_REV_MAX_SIZE);
    std::string subAssemblyBatchNumber((char *)infoNVRAM->subAssemblyBatchNumber, NVRAMINFO_BATCH_MAX_SIZE);
    printf("Major version             : %u\n", infoNVRAM->InfoBlockVersionMajor  );
    printf("Minor version             : %u\n", infoNVRAM->InfoBlockVersionMinor  );
    printf("Module type               : %u\n", infoNVRAM->moduleType             );
    printf("Module Feature Major      : %u (0x%08x)\n", infoNVRAM->moduleFeatureMajor, infoNVRAM->moduleFeatureMajor    );
    printf("Module Feature Minor      : %u (0x%08x)\n", infoNVRAM->moduleFeatureMinor, infoNVRAM->moduleFeatureMinor    );
    printf("Batch number              : %s\n", batchNumber.c_str());    //infoNVRAM->batchNumber);
    printf("Assembly number           : %s\n", assemblyNo .c_str());    //infoNVRAM->assemblyNo );
    printf("Assembly revision         : %s\n", assemblyRev.c_str());    //infoNVRAM->assemblyRev);
    printf("Supplier ID               : %s\n", supplierId .c_str());    //infoNVRAM->supplierId );
    printf("Module serial number      : %06u (0x%08x)\n", infoNVRAM->serialNumber, infoNVRAM->serialNumber          );
    printf("sub Assembly number       : %s\n", subAssemblyNo .c_str()); //infoNVRAM->subAssemblyNo );
    printf("sub Assembly revision     : %s\n", subAssemblyRev.c_str()); //infoNVRAM->subAssemblyRev);
    printf("Sub assembly serial number: %u (0x%08x)\n", infoNVRAM->subAssemblySerialNumber, infoNVRAM->subAssemblySerialNumber);
    printf("Sub assembly Batch number : %s\n", subAssemblyBatchNumber.c_str());  //infoNVRAM->subAssemblyBatchNumber);
    printf("Manufacturing build Day   : %u\n", infoNVRAM->buildDay               );
    printf("Manufacturing build Month : %u\n", infoNVRAM->buildMonth             );
    printf("Manufacturing build Year  : %u (0x%04x)\n", infoNVRAM->buildYear, infoNVRAM->buildYear             );






    /* print error accessing device */
    if(ret == IMemService::MEMORYERROR_NONE)
    {
        valid = LU_TRUE;    //data from primary NVRAM ID is valid
        log.info("%s data acquired correctly from NVRAM ID memory device %s.",
                    FTITLE, MemDevice_ID
                  );
    }
    else
    {
        log.warn("%s %s NVRAM ID memory device %s. Trying secondary storage.",
                FTITLE,
                (ret == IMemService::MEMORYERROR_CRC)? "CRC error in" :
                                                          "Error accessing",
                MemDevice_ID
              );
    }

    /* Check/Read/Write APP device */
    if(ret == IMemService::MEMORYERROR_NONE)
    {
        //Backup ID onto APPID memory if ID reading was successful.
        //  Note that this checks APP ID NVRAM as well, and does not write when both matches.
        retApp = writeMemory(MemDevice_AppID, bufferID, DataSize);
    }
    else
    {
        //Read from backup
        retApp = readMemory(MemDevice_AppID, bufferID, DataSize);
    }

    /* Deal with APP ID error code */
    if(ret != IMemService::MEMORYERROR_NONE)
    {
        if(retApp == IMemService::MEMORYERROR_NONE)
        {
            valid = LU_TRUE;    //data from secondary NVRAM ID is valid
            log.info("%s backup data acquired correctly from NVRAM ID memory device %s.",
                        FTITLE, MemDevice_ID
                      );
        }
        else
        {
            log.error("%s Error: no NVRAM ID data! %s secondary storage NVRAM ID memory device %s.",
                    FTITLE,
                    (retApp == IMemService::MEMORYERROR_CRC)? "CRC error in" :
                                                                 "error accessing",
                    MemDevice_AppID
                  );
        }
    }
    else if(retApp != IMemService::MEMORYERROR_NONE)
    {
        log.warn("%s %s secondary storage NVRAM ID memory device %s.",
                FTITLE,
                (retApp == IMemService::MEMORYERROR_CRC)? "CRC error in" :
                                                             "error accessing",
                MemDevice_AppID
              );
    }

    initialised = LU_TRUE; //already initialised (good or bad)
    /* Return error code depending on ID, APPID, or both error:
     *      ID  APP Result
     *      ---------------
     *      OK  OK  MEMORYERROR_NONE
     *      OK  ERR MEMORYERROR_WARNBACKUP
     *      ERR OK  MEMORYERROR_WARNMAIN
     *      ERR ERR error code given by Main ID memory (CRC, ACCESS...)
     */
    return ( (ret == IMemService::MEMORYERROR_NONE)?
                    (retApp == IMemService::MEMORYERROR_NONE)? ret :
                                    IMemService::MEMORYERROR_WARNBACKUP
                    :
                    (retApp != IMemService::MEMORYERROR_NONE)? ret :
                                    IMemService::MEMORYERROR_WARNMAIN
            );
}


lu_bool_t MemServiceNVRAMID::read(IMemoryManager::STORAGE_VAR variable, lu_uint8_t* value, size_t* sizeRead)
{
    if(initialised == LU_FALSE)
    {
        init();        //tried to read before initialising
    }

    if(value == NULL)
    {
        return LU_FALSE;    //bad params
    }

    if(valid == LU_FALSE)
    {
        return LU_FALSE;    //nothing to read
    }

    /* Get the NVRAM block and read it */
    NVRAMInfoStr* mcmID;
    mcmID = reinterpret_cast<NVRAMInfoStr*>(*(bufNVRAM.getTable()) + sizeof(NVRAMBlkHeadStr));
    switch(variable)
    {
        case IMemoryManager::STORAGE_VAR_ID:
            //Get complete ID block
            memcpy(value, mcmID, sizeof(NVRAMInfoStr));
            if(sizeRead != NULL)
            {
                *sizeRead = sizeof(NVRAMInfoStr);
            }
            break;
        case IMemoryManager::STORAGE_VAR_MODULEUID:
        {
            /* Get Supplier ID & serial number */
            ModuleUIDStr moduleUID;
            moduleUID.serialNumber = mcmID->serialNumber;
            //Get the supplier ID from the 2 first chars of the NVRAM field
            lu_uint32_t temp = 0;   //If numbers aren't recognised, return 0
            char supID[3];
            memcpy(supID, mcmID->supplierId, 2);
            supID[2] = '\0';
            if(sscanf(supID, "%u", &temp) < 1)
            {
                moduleUID.supplierId = 0    ;
            }
            else
            {
                moduleUID.supplierId = temp;
            }
            memcpy(value, &moduleUID, sizeof(ModuleUIDStr));
            if(sizeRead != NULL)
            {
                *sizeRead = sizeof(ModuleUIDStr);
            }
        }
            break;
        case IMemoryManager::STORAGE_VAR_FEATUREREV:
            //feature revision
            BasicVersionStr featRev;
            featRev.major = mcmID->moduleFeatureMajor;
            featRev.minor = mcmID->moduleFeatureMinor;
            memcpy(value, &featRev, sizeof(featRev));
            if(sizeRead != NULL)
            {
                *sizeRead = sizeof(featRev);
            }
            break;
        default:
            return LU_FALSE;    //invalid variable
            break;
    }
    return LU_TRUE; //recovered successfully
}


lu_bool_t MemServiceNVRAMID::write(IMemoryManager::STORAGE_VAR variable, lu_uint8_t* value)
{
    LU_UNUSED(variable);
    LU_UNUSED(value);
    return LU_FALSE;    //external write unsupported for this memory
}


IMemService::MEMORYERROR MemServiceNVRAMID::invalidate()
{
    return IMemService::MEMORYERROR_ACCESS; //external write unsupported for this memory
}


void MemServiceNVRAMID::flush()
{
    return;    //flush unsupported for this memory
}


void MemServiceNVRAMID::close()
{
    return;    //closing unnecessary for this memory
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
