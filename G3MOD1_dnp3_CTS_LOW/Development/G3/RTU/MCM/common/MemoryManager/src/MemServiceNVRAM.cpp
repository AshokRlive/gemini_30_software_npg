/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: MemServiceNVRAM.cpp 04-Jan-2013 09:41:06 pueyos_a $
 *               $HeadURL: U:\G3\RTU\MCM\src\MemoryManager\src\MemServiceNVRAM.cpp $
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Service Provider for handling user NVRAM storage.
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 04-Jan-2013 09:41:06	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   04-Jan-2013 09:41:06  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "NVRAMBlock.h"
#include "MemServiceNVRAM.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
MemServiceNVRAM::MemServiceNVRAM(const lu_char_t* MEMDEVICE_APP_PATH) :
                        MemoryDevice(MEMDEVICE_APP_PATH),
                        initAccess(LU_TRUE),
                        updPending(LU_FALSE),
                        scratchBuffer(NULL),
                        bufSize(0), //default size - recalculated at init
                        dataSize(0),
                        valid(LU_FALSE),
                        initTry(3)
{
//    /* Set default values for memory configuration - might be overridden by setMemoryConfig() */
//    memConfig.memorySize = MEMNVRAMH_APP_SIZE;
//    memConfig.blockSize = MEMNVRAMH_APP_SIZE;
//    memConfig.offset = MEMNVRAMH_APP_OFFSET;
//    memConfig.useOffset = LU_TRUE;

    /* -- initialise internal table for memory storage -- */
    for(lu_uint32_t i=0; i < IMemoryManager::STORAGE_VAR_LAST; i++)
    {
        memIndex[i].varEnum = IMemoryManager::STORAGE_VAR_LAST; //none yet
        memIndex[i].varSize = 0;
        memIndex[i].address = 0;
        memIndex[i].invalid = LU_TRUE;
    }
}


MemServiceNVRAM::~MemServiceNVRAM()
{
    this->close();

    if(scratchBuffer != NULL)
    {
        delete [] scratchBuffer;
    }
}


void MemServiceNVRAM::setSize(IMemoryManager::STORAGE_VAR variable, size_t size)
{
    MasterLockingMutex iMutex(initAccess, LU_TRUE);
    if(initialised == LU_FALSE)
    {
        //update internal table
        for(lu_uint32_t i=0; i < IMemoryManager::STORAGE_VAR_LAST; i++)
        {
            if(memIndex[i].varEnum == variable)
            {
                //already present: update
                memIndex[i].varSize = size;
                return;
            }
            if(memIndex[i].varEnum == IMemoryManager::STORAGE_VAR_LAST)
            {
                //first empty entry: fill it
                memIndex[i].varEnum = variable;
                memIndex[i].varSize = size;
                return;
            }
        }
    }
}


IMemService::MEMORYERROR MemServiceNVRAM::init()
{
    const lu_char_t *FTITLE = "MemNVRAMService::init:";
    IMemService::MEMORYERROR ret = IMemService::MEMORYERROR_ACCESS;

    MasterLockingMutex iMutex(initAccess);

    if( (initialised == LU_TRUE) || (initTry == 0) )
    {
        return IMemService::MEMORYERROR_NONE;     //already initialised
    }

    lu_uint32_t bufPos = sizeof(NVRAMBlkHeadStr);   //buffer pos, initially after header

    //calculate space for memory storage buffer
    for(lu_uint32_t i = 0;
        ( (i < IMemoryManager::STORAGE_VAR_LAST) &&
            (memIndex[i].varEnum != IMemoryManager::STORAGE_VAR_LAST)
            );
        ++i)
    {
        memIndex[i].address = bufPos;
        bufPos += memIndex[i].varSize;
    }

    if(bufPos > memConfig.memorySize)
    {
        log.fatal("%s NVRAM memory space needed for application (%i bytes) "
                "exceeds maximum size of available NVRAM memory (%i bytes).",
                FTITLE, bufPos, memConfig.memorySize);
        initTry = 0; //do not try to initialise again
        return IMemService::MEMORYERROR_RAM;
    }

    //reserve memory for that (initialised to 0)
    bufSize = bufPos;
    scratchBuffer = new lu_uint8_t[bufPos]();
    dataSize = bufPos - sizeof(NVRAMBlkHeadStr);    //set the stored data size

    if(scratchBuffer == NULL)
    {
        log.error("%s error reserving buffer memory: not enough memory.", FTITLE);
        --initTry;
        return IMemService::MEMORYERROR_RAM;
    }

    MasterLockingMutex lMutex(memAccess, LU_TRUE);

    /* Open APP device for first reading from memory */
    ret = readMemory(MemoryDevice, scratchBuffer, bufSize);
    if(ret == IMemService::MEMORYERROR_NONE)
    {
        valid = LU_TRUE;
        //Mark values read as valid
        for(lu_uint32_t i=0; i < IMemoryManager::STORAGE_VAR_LAST; i++)
        {
            memIndex[i].invalid = LU_FALSE;
        }

        log.info("%s data acquired correctly from NVRAM memory device %s.",
                    FTITLE, MemoryDevice
                  );
    }
    else
    {
        log.error("%s %s NVRAM memory device %s.",
                FTITLE,
                (ret == IMemService::MEMORYERROR_CRC)? "CRC error in" :
                                                          "error accessing",
                MemoryDevice
              );
    }

    initialised = LU_TRUE;  //initialisation completed
    return ret;
}


lu_bool_t MemServiceNVRAM::read(IMemoryManager::STORAGE_VAR variable, lu_uint8_t* value, size_t* sizeRead)
{
    if(init() != IMemService::MEMORYERROR_NONE) //tried to read before initialising
    {
        return LU_FALSE;    //nothing to read
    }
    if( (value == NULL) || (valid == LU_FALSE) )
    {
        return LU_FALSE;    //nothing to read
    }

    for(lu_uint32_t i=0; memIndex[i].varEnum != IMemoryManager::STORAGE_VAR_LAST; i++)
    {
        if(memIndex[i].varEnum == variable)
        {
            if(memIndex[i].invalid == LU_TRUE)
            {
                return LU_FALSE;    //datum is not valid yet
            }
            //variable found: return latest stored value
            memcpy(value, &scratchBuffer[memIndex[i].address], memIndex[i].varSize);
            if(sizeRead != NULL)
            {
                *sizeRead = memIndex[i].varSize;
            }
            return LU_TRUE; //recovered successfully
        }
    }
    return LU_FALSE;    //error: variable not found
}


lu_bool_t MemServiceNVRAM::write(IMemoryManager::STORAGE_VAR variable, lu_uint8_t* value)
{
    lu_bool_t updateit = LU_FALSE;

    init(); //in case it was tried to write before initialising
    if(initialised == LU_FALSE)
    {
        return LU_FALSE;    //unable to write when initialising failed
    }

    MasterLockingMutex lMutex(memAccess);

    for(lu_uint32_t i=0; memIndex[i].varEnum != IMemoryManager::STORAGE_VAR_LAST; i++)
    {
        if(memIndex[i].varEnum == variable)
        {
            //variable found
            MasterLockingMutex lMutex(memAccess);
            if(memIndex[i].invalid == LU_FALSE)
            {
                //when already valid, check value change
                if( memcmp(&scratchBuffer[memIndex[i].address],
                                value,
                                memIndex[i].varSize) != 0
                            )
                {
                    //update value
                    updateit = LU_TRUE;
                }
            }
            else
            {
                updateit = LU_TRUE; //value is invalid: update
            }
            if(updateit == LU_TRUE)
            {
                /* Write value to scratch buffer */
                memcpy(&scratchBuffer[memIndex[i].address], value, memIndex[i].varSize);
                memIndex[i].invalid = LU_FALSE; //mark value as no longer invalid if it was
                updPending = LU_TRUE;  //May be a change
            }
            return LU_TRUE; //update writing successfully (or value was already that)
        }
    }
    return LU_FALSE;    //error: variable not found
}


IMemService::MEMORYERROR MemServiceNVRAM::invalidate()
{
    init(); //in case it was tried to write before initialising
    if(initialised == LU_FALSE)
    {
        return IMemService::MEMORYERROR_ACCESS; //unable to write when initialising failed
    }

    MasterLockingMutex lMutex(memAccess, LU_TRUE);
    updateCRC(scratchBuffer, bufSize);
    NVRAMBlkHeadStr *headerPtr = reinterpret_cast<NVRAMBlkHeadStr *>(scratchBuffer);
    headerPtr->dataCrc32++; //invalidate CRC
    return writeMemory(MemoryDevice, scratchBuffer, bufSize);
}


void MemServiceNVRAM::flush()
{
    init(); //in case it was tried to write before initialising
    if(initialised == LU_FALSE)
    {
        return;    //unable to write when initialising failed
    }

    MasterLockingMutex lMutex(memAccess, LU_TRUE);

    if( (updPending == LU_TRUE) && (initialised == LU_TRUE) )
    {
        //Note that it will write only data that has changed
        updateCRC(scratchBuffer, bufSize);
        switch(writeMemory(MemoryDevice, scratchBuffer, bufSize))
        {
        case IMemService::MEMORYERROR_NONE:
        case IMemService::MEMORYERROR_ACCESS: //do not retry if major failure
            updPending = LU_FALSE;
            break;
        default:
            updPending = LU_TRUE;
            break;
        }
    }
}


void MemServiceNVRAM::close()
{
    MasterLockingMutex lMutex(memAccess, LU_TRUE);
    this->flush();  //write any pending data
    updPending = LU_FALSE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */

