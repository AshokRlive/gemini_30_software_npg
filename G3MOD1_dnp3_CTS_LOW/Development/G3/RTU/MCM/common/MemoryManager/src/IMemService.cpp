/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id:  $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//G3Trunk/G3/RTU/MCM/src/MemoryManager/src/IMemService.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Memory Service Provider common interface - default implementation.
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 9 Jan 2013$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <cstring>  //memcmp() usage

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "IMemService.h"
//#include "LogLevelDef.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

IMemService::IMemService() :
                                log(Logger::getLogger(SUBSYSTEM_ID_MEMORYMAN)),
                                initialised(LU_FALSE)
{
    memConfig.memorySize = 0;
    memConfig.blockSize = 0;
    memConfig.offset = 0;
    memConfig.useOffset = LU_FALSE;
}


void IMemService::setMemoryConfig(IMemService::memConfigStr memoryConfig)
{
    /* TODO: pueyos_a - protect access to "initialised" member (getter/setter with init mutex) */
    if(initialised == LU_FALSE)
    {
        //allow to set memory config only before initialising
        memConfig = memoryConfig;
    }
}


lu_bool_t IMemService::writeToMemory(FILE* device,
                                    const lu_uint8_t* bufferRead,
                                    const lu_uint8_t* bufferWrite,
                                    const size_t bufferSize,
                                    const size_t offset,
                                    const lu_bool_t force
                                    )
{
    lu_uint32_t prevBlocks;     //amount of blocks before the first block to write
    lu_uint32_t totalBlocks;    //amount of blocks to write
    lu_uint32_t memOffset;      //Address where the memory blocks start to count
    lu_uint32_t initBlock;      //Address of first block to write
    lu_uint32_t lastPos;        //Address of last position of the buffer
    lu_uint32_t memBlockSz;     //Minimum block size to write in memory
    size_t comparingSize;       //Comparison sub-block size (related to buffer)
    lu_uint32_t pos = 0;        //Position for comparing buffers
    lu_uint32_t curPos = 0;     //Current block address
    lu_uint32_t nextPos = 0;    //Next block address
    lu_uint32_t writePos = 0;  //Address where write the buffer data
    lu_bool_t doWrite = LU_FALSE;

    if( (device == NULL) || (bufferSize == 0) ||
        ( (bufferRead == NULL) && (force == LU_FALSE) ) ||  //when forcing, bufferRead isn't needed
        (bufferWrite == NULL)
        )
    {
        return LU_FALSE;
    }

    memBlockSz = (memConfig.blockSize == 0)? bufferSize : memConfig.blockSize;
    memOffset = (memConfig.useOffset == LU_TRUE)? memConfig.offset : 0;
    memOffset += offset;
    writePos = memOffset;   //initial write position is the calculated offset
    prevBlocks = (offset - memOffset) / memBlockSz;
    initBlock = memOffset + (memBlockSz * prevBlocks);
    lastPos = offset + bufferSize;
    totalBlocks = 1 + ((lastPos - initBlock) / memBlockSz); //equivalent to ceil(lastpos/memBlockSz)

    //Write only the blocks that changed
    for (lu_uint32_t i = 0; i < totalBlocks; ++i)
    {
        curPos = initBlock + (i * memBlockSz);
        nextPos = curPos + memBlockSz;
        comparingSize = LU_MIN(lastPos, nextPos) - LU_MAX(offset, curPos);
        if(force == LU_TRUE)
        {
            doWrite = LU_TRUE;  //always write
        }
        else
        {
            if(memcmp(bufferRead + pos, bufferWrite + pos, comparingSize) != 0 )
            {
                doWrite = LU_TRUE;  //sub-block changed: write partial block
            }
        }
        if(doWrite == LU_TRUE)
        {
            if( fseek(device, writePos, SEEK_SET) != 0 )
            {
                return LU_FALSE;
            }
            if( fwrite(bufferWrite + pos, comparingSize, 1, device) != 1)
            {
                return LU_FALSE;
            }

        }
        pos += comparingSize;
        writePos += comparingSize;
    }
    return LU_TRUE;
}


void IMemService::tickEvent(lu_uint32_t dTime)
{
    LU_UNUSED(dTime);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
