/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id: MCMIOMap.h 1 Oct 2013 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/common/include/MCMIOMap.h $
 *
 *    FILE TYPE: C header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MCM's I/O Map
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 1 Oct 2013 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   1 Oct 2013    pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef MCMIOMAP_H__INCLUDED
#define MCMIOMAP_H__INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdio.h>
#include <limits.h>  // For LONG_MAX

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define GPIO_STRING_LEN         50
#define KLED_STRING_LEN         GPIO_STRING_LEN

/* Path to each digital IOs GPIOlib sysfs entry */
#define GPIOLIB_PATH            "/sys/class/gpio/"
#define GPIOLIB_BANK_SIZE       32


/**
 * \brief GPIO calculation macro
 *
 * \param bank GPIO hardware bank. Values [1,3]
 * \param id GPIO hardware ID.
 * \return OS's GPIO ID
 */
#define GPIO(bank, id)      ( ( ((bank)-1) * GPIOLIB_BANK_SIZE) + (id) )


/* ============ MCM I/O Map ============== */

/*
 * GPIOs are defined here. They are defined as OS's GPIOs, that follows this
 * convention: GPIOx[yy] gives OS's GPIO = ((x-1) * 32) + y
 *
 * GPIO values are usually lu_uint8_t values.
 */

/**
 * MCM GPIO Input IDs (OS GPIOs)
 */
enum MCMInputGPIO
{
    MCMInputGPIO_LOCAL          = GPIO(1,27),   //27    /* Active low */
    MCMInputGPIO_REMOTE         = GPIO(1,28),   //28    /* Active low */
#if (MCM_FEATURE_VERSION_MAJOR == 0)
    MCMInputGPIO_DOOR_SWITCH    = GPIO(3,19),   //83    /* Active low */
#else
    MCMInputGPIO_DOOR_SWITCH    = GPIO(3,4),    //68    /* Active low */
#endif
    MCMInputGPIO_HMI_PWR_GOOD   = GPIO(3,15),   //79
    MCMInputGPIO_HMI_DETECT     = GPIO(1,29),   //29
    MCMInputGPIO_OLR_BUTTON     = GPIO(3, 3),   //67    /* Active low */
    MCMInputGPIO_CFG_IN         = GPIO(1,30),   //30
    MCMInputGPIO_FACTORY        = GPIO(2, 5),   //37
    MCMInputGPIO_WDOG_HANDSHAKE = GPIO(1,19),   //19
    MCMInputGPIO_SYS_HEALTHY    = GPIO(1,20)    //20
};

/**
 * MCM GPIO Output IDs (OS GPIOs)
 */
enum MCMOutputGPIO
{
    MCMOutputGPIO_LOCAL             = GPIO(2,14), //46
    MCMOutputGPIO_REMOTE            = GPIO(2,16), //48
    MCMOutputGPIO_APPRAM_WEN        = GPIO(1, 6), //6
    MCMOutputGPIO_HMI_PWR_ENABLE    = GPIO(3,16), //80
    MCMOutputGPIO_CONFIG_DTR        = GPIO(1, 4), //4
    MCMOutputGPIO_CONTROL_DTR       = GPIO(1, 5), //5
    MCMOutputGPIO_LED_CAN_RED       = GPIO(2, 8), //40
    MCMOutputGPIO_LED_CAN_GREEN     = GPIO(2, 9), //41
    MCMOutputGPIO_LED_DUMMYSW_RED   = GPIO(2,10), //42
    MCMOutputGPIO_LED_DUMMYSW_GREEN = GPIO(2,11), //43
    MCMOutputGPIO_LED_EXTINGUISH    = GPIO(2,12), //44
    MCMOutputGPIO_CFG_OUT           = GPIO(1,31), //31
    MCMOutputGPIO_USB_PWR_ENABLE    = GPIO(4,11),  //59
    MCMOutputGPIO_LED_AUTO_GREEN    = GPIO(1,30), //30
    MCMOutputGPIO_LED_AUTO_RED      = GPIO(1,31)  //31
    /* In use elsewhere (here for reference)
     * MCMOutputGPIO_WDOG_1HZ_KICK  = GPIO(3,25) : 89
     * MCMOutputGPIO_LED_OK_RED     = GPIO(2, 6) : 38
     * MCMOutputGPIO_LED_OK_GREEN   = GPIO(2, 7) : 39
     */
};


/**
 * MCM Kernel LED names
 */
#define KLED_WDOG_1HZ_KICK_NAME "HWDOG_KICK"
#define KLED_OK_GREEN_NAME      "OK-LED-GREEN"
#define KLED_OK_RED_NAME        "OK-LED-RED"


/**
 *  Analogue Input definitions
 * */
#define MCMANALOGUEINPUT_DEVICE_NAME_LEN  20
#define MCMANALOGUEINPUT_DEVICE_PATH_LEN  MCMANALOGUEINPUT_DEVICE_NAME_LEN + 100

#ifdef PRE_KERNEL_2_6_39
/*
 * Path to each analogue IOs GPIOlib entry
 * e.g. /sys/class/hwmon/hwmon1/device/v5V0
  */
#define MCMANALOGUEINPUT_PATH             "/sys/class/hwmon/"
#define MCMANALOGUEINPUT_DEVICE           "hwmon"
#define MCMANALOGUEINPUT_DEVICE0          0
#define MCMANALOGUEINPUT_DEVICE1          1
#endif

/* From Kernel 2.6.39.4
 * Path to each analogue IOs GPIOlib entry
 * e.g. /sys/devices/platform/imx-i2c.2/i2c-2/2-0049/temp1_input
 */
#define MCMANALOGUEINPUT_PATH             "/sys/devices/platform/"
#define MCMANALOGUEINPUT_DEVICE0          "imx-i2c.2/i2c-2/2-0049/"
#define MCMANALOGUEINPUT_DEVICE1          "mx25-tscadc/"

/**
 * MCM Analogue Input Device names
 */
#define MCMANALOGUEINPUT_VLOGIC     "vLogic"
#define MCMANALOGUEINPUT_5V         "v5V0"
#define MCMANALOGUEINPUT_3V3        "v3V3"
#define MCMANALOGUEINPUT_VLOCAL     "vLocal"
#define MCMANALOGUEINPUT_VREMOTE    "vRemote"
#define MCMANALOGUEINPUT_CORETEMP   "temp1_input"

/* Configuration of AI Channels (Type MCMMonAIConfigStr)                                    -Scan-        --Raw--         --Scaled--     --Limits--        --Alarm func--          --Update func--*/
/* Scan rate of 0 will make the Channel an "update on request" point with no scanning */
#define MCMMONAI_VLOGIC_CONFIG        { MCMANALOGUEINPUT_DEVICE1, MCMANALOGUEINPUT_VLOGIC,   10000,       0,   4095,       0.0,  47.5,   20.0,  45.0, analogueInputAlarmOutLimits,  NULL }
#define MCMMONAI_5V_CONFIG            { MCMANALOGUEINPUT_DEVICE1, MCMANALOGUEINPUT_5V,       10000,      0,   4095,       0.0,   7.5,    4.5,  6.5,  analogueInputAlarmOutLimits,  NULL }
#define MCMMONAI_3V3_CONFIG           { MCMANALOGUEINPUT_DEVICE1, MCMANALOGUEINPUT_3V3,      10000,      0,   4095,       0.0,   5.0,    2.8,  4.8,  analogueInputAlarmOutLimits,  NULL }
#define MCMMONAI_VLOCAL_CONFIG        { MCMANALOGUEINPUT_DEVICE1, MCMANALOGUEINPUT_VLOCAL,   1000,       0,   4095,       0.0,  47.5,   10.0,  90.0, ioManAlarmFuncPercOfvLogic,   NULL }
#define MCMMONAI_VREMOTE_CONFIG       { MCMANALOGUEINPUT_DEVICE1, MCMANALOGUEINPUT_VREMOTE,  1000,       0,   4095,       0.0,  47.5,   10.0,  90.0, ioManAlarmFuncPercOfvLogic,   NULL }
#define MCMMONAI_CORETEMP_CONFIG      { MCMANALOGUEINPUT_DEVICE0, MCMANALOGUEINPUT_CORETEMP, 10000, -40000, 150000,     -40.0, 150.0,  -39.0,  80.0, analogueInputAlarmOutLimits,  NULL }

#define MCMMONAI_CAN0TXBYTES_CONFIG   { "",                       "",                        10000,      0,    100,       0.0,   0.0,    0.0,   0.0, NULL,                         CAN0UpdateAITxBytes }
#define MCMMONAI_CAN0RXBYTES_CONFIG   { "",                       "",                        10000,      0,    100,       0.0,   0.0,    0.0,   0.0, NULL,                         CAN0UpdateAIRxBytes }
#define MCMMONAI_CAN0USAGE_CONFIG     { "",                       "",                        10000,      0,    LONG_MAX,  0.0,   0.0,    0.0,   0.0, NULL,                         CAN0UpdateAIUsage   }
#define MCMMONAI_CAN0TXERRS_CONFIG    { "",                       "",                        0,          0,    LONG_MAX,  0.0,   0.0,    0.0,   0.0, NULL,                         CAN0UpdateAITxErrs  }
#define MCMMONAI_CAN0RXERRS_CONFIG    { "",                       "",                        0,          0,    LONG_MAX,  0.0,   0.0,    0.0,   0.0, NULL,                         CAN0UpdateAIRxErrs  }
#define MCMMONAI_CAN0ERRS_CONFIG      { "",                       "",                        0,          0,    LONG_MAX,  0.0,   0.0,    0.0,   0.0, NULL,                         CAN0UpdateAIErrs    }
#define MCMMONAI_CAN0TXPACKETS_CONFIG { "",                       "",                        0,          0,    LONG_MAX,  0.0,   0.0,    0.0,   0.0, NULL,                         CAN0UpdateAITxPack    }
#define MCMMONAI_CAN0RXPACKETS_CONFIG { "",                       "",                        0,          0,    LONG_MAX,  0.0,   0.0,    0.0,   0.0, NULL,                         CAN0UpdateAIRxPack    }
#define MCMMONAI_CAN0RXDROPPED_CONFIG { "",                       "",                        0,          0,    LONG_MAX,  0.0,   0.0,    0.0,   0.0, NULL,                         CAN0UpdateAIRxDropped }
#define MCMMONAI_CAN0RXOVERERR_CONFIG { "",                       "",                        0,          0,    LONG_MAX,  0.0,   0.0,    0.0,   0.0, NULL,                         CAN0UpdateAIRxOverErr }

#define MCMMONAI_CAN1TXBYTES_CONFIG   { "",                       "",                        10000,      0,    100,       0.0,   0.0,    0.0,   0.0, NULL,                         CAN1UpdateAITxBytes }
#define MCMMONAI_CAN1RXBYTES_CONFIG   { "",                       "",                        10000,      0,    100,       0.0,   0.0,    0.0,   0.0, NULL,                         CAN1UpdateAIRxBytes }
#define MCMMONAI_CAN1USAGE_CONFIG     { "",                       "",                        10000,      0,    LONG_MAX,  0.0,   0.0,    0.0,   0.0, NULL,                         CAN1UpdateAIUsage   }
#define MCMMONAI_CAN1TXERRS_CONFIG    { "",                       "",                        0,          0,    LONG_MAX,  0.0,   0.0,    0.0,   0.0, NULL,                         CAN1UpdateAITxErrs  }
#define MCMMONAI_CAN1RXERRS_CONFIG    { "",                       "",                        0,          0,    LONG_MAX,  0.0,   0.0,    0.0,   0.0, NULL,                         CAN1UpdateAIRxErrs  }
#define MCMMONAI_CAN1ERRS_CONFIG      { "",                       "",                        0,          0,    LONG_MAX,  0.0,   0.0,    0.0,   0.0, NULL,                         CAN1UpdateAIErrs    }
#define MCMMONAI_CAN1TXPACKETS_CONFIG { "",                       "",                        0,          0,    LONG_MAX,  0.0,   0.0,    0.0,   0.0, NULL,                         CAN1UpdateAITxPack    }
#define MCMMONAI_CAN1RXPACKETS_CONFIG { "",                       "",                        0,          0,    LONG_MAX,  0.0,   0.0,    0.0,   0.0, NULL,                         CAN1UpdateAIRxPack    }
#define MCMMONAI_CAN1RXDROPPED_CONFIG { "",                       "",                        0,          0,    LONG_MAX,  0.0,   0.0,    0.0,   0.0, NULL,                         CAN1UpdateAIRxDropped }
#define MCMMONAI_CAN1RXOVERERR_CONFIG { "",                       "",                        0,          0,    LONG_MAX,  0.0,   0.0,    0.0,   0.0, NULL,                         CAN1UpdateAIRxOverErr }

/* NOTE: The vLocal & vRemote lines have their scan times adjusted during a requested change in OLR state,
 * thus a slow normal scan rate is appropriate */


/* Configuration of DI Channels (Type MCMMonDIConfigStr)
                                        GPIO                       Scan   Invert    Notify Change  Update Func */
#define MCMMONDI_LOCAL_CONFIG      { 0 /* VIRTUAL */,              1000,  LU_FALSE, LU_TRUE,       ioManUpdateVirtualDigitalInput }
#define MCMMONDI_REMOTE_CONFIG     { 0 /* VIRTUAL */,              1000,  LU_FALSE, LU_TRUE,       ioManUpdateVirtualDigitalInput }
#define MCMMONDI_SYSHEALTHY_CONFIG { MCMInputGPIO_SYS_HEALTHY,     0,     LU_FALSE, LU_TRUE,       NULL                           }
#define MCMMONDI_WDOG_HANDSHAKE    { MCMInputGPIO_WDOG_HANDSHAKE,  0,     LU_FALSE, LU_FALSE,      NULL                           }  // Used internally by WDOGThread Only
#define MCMMONDI_FACTORY           { MCMInputGPIO_FACTORY,         0,     LU_FALSE, LU_FALSE,      NULL                           }  // Used internally by WDOGThread Only
#define MCMMONDI_WDOG_FAILED       { 0 /* VIRTUAL */,              0,     LU_FALSE, LU_TRUE,       ioManUpdateVirtualDigitalInput }

/* Configuration of DO Channels (Type MCMMonDOConfigStr)
                                     GPIO  */
#define MCMMONDO_LOCAL_CONFIG      { MCMOutputGPIO_LOCAL  }
#define MCMMONDO_REMOTE_CONFIG     { MCMOutputGPIO_REMOTE }

/**
 * Path to each Kernel's LED driver entry.
 * The Kernel's LED driver is used to interface with the WDOG KICK
 */
#define KLED_LIB_PATH            "/sys/class/leds/"

/* Sysfs GPIOlib GPIO file names and control values */
#define GPIOLIB_VALUENAME        "value"
#define GPIOLIB_DIRECTIONNAME    "direction"
#define GPIOLIB_DIRECTIONINPUT   "in"            //value for setting as input
#define GPIOLIB_DIRECTIONOUTPUT  "out"           //value for setting as output

/* Sysfs GPIOlib Kernel LED control values */
#define GPIOLIB_KLEDVALUENAME    "brightness"    //on/off state
#define GPIOLIB_KLEDTRIGGERNAME  "trigger"       //manual/auto control
#define GPIOLIB_KLEDDELAYONNAME  "delay-on"      //delay-on time (milliseconds
#define GPIOLIB_KLEDDELAYOFFNAME "delay-off"     //delay-off time (milliseconds

#define GPIOLIB_KLEDMANUALSTATE  "none"          //value for trigger
#define GPIOLIB_KLEDTIMERSTATE   "timer"         //value for trigger



/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef enum
{
    GPIOLIB_DIRECTION_INPUT,
    GPIOLIB_DIRECTION_OUTPUT
} GPIOLIB_DIRECTION;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/* Sysfs GPIOlib export file */
static const lu_char_t gpioExport[] = GPIOLIB_PATH"export";

/* Sysfs GPIOlib un-export file */
static const lu_char_t gpioUnexport[] = GPIOLIB_PATH"unexport";

/* Sysfs GPIOlib exported file base path */
static const lu_char_t gpioExported[] = GPIOLIB_PATH"gpio";

/*
 ******************************************************************************
 * EXPORTED - Prototypes Of Functions
 ******************************************************************************
 */

/**
 * \brief Initialises the GPIO
 *
 * This function configures the GPIO (export + set direction) and keeps open the
 * associated value file.
 * Note that when there is an error, no file is left open and fdValues is NULL.
 *
 * \param gpio GPIO Number.
 * \param direction Output setting. Set to LU_FALSE to set GPIO as input, LU_TRUE as output.
 * \param fdValue GPIO value file is returned here.  (set to NULL if not required)
 *
 * \return Error Code
 */
lu_int32_t initGPIO(const lu_uint8_t gpio, const GPIOLIB_DIRECTION direction, FILE** fdValue);

/**
 * \brief Close a GPIO value file
 *
 * \param fdValue GPIO value file is returned here.
 *
 * \return Error Code
 */
lu_int32_t closeGPIO(FILE *fdValue);

/**
 * \brief Reads a digital value from the given GPIO
 *
 * \param fd GPIO value file.
 * \param val Where to store the received value.
 *
 * \return Error Code
 */
lu_int32_t readGPIO(FILE *fd, lu_bool_t *val);

/**
 * \brief Writes a value to the given fd
 *
 * \param fd GPIO Export file.
 * \param val Value to write.
 *
 * \return Error Code
 */
lu_int32_t exportGPIO(FILE *fd, lu_uint8_t val);

/**
 * \brief Writes a digital value to the given GPIO
 *
 * \param fd GPIO value file.
 * \param val Value to write.
 *
 * \return Error Code
 */
lu_int32_t writeGPIO(FILE *fd, lu_bool_t val);

/**
 * \brief Initialises the Kernel LED
 *
 * This function configures the Kernel LED associated with the given name, and
 * keeps open the associated value file.
 *
 * \param ledName Kernel LED name.
 * \param fdValue Value file is returned here.
 *
 * \return Error Code
 */
lu_int32_t initKLED(const char *ledName, FILE **fdValuePtrPtr);

/**
 * \brief Set a Kernel LED's trigger to Manual mode
 *
 * Use the [brightness] file to operate the LED
 *
 * \param ledName Kernel LED name.
 *
 * \return Error Code
 */
lu_int32_t setKLEDManual(const char *ledName);

/**
 * \brief Set a Kernel LED's trigger to Timer mode
 *
 * Use the [delay-on] and [delay-off] files to control the flash behaviour
 *
 * \param   ledName - Kernel LED name.
 *          delayOn - On delay time (ms)
 *          delayOf - Off delay time (ms)
 *
 * \return Error Code
 */
lu_int32_t setKLEDTimer(const char *ledName, lu_uint32_t delayOn, lu_uint32_t delayOff);

/**
 * \brief Reads a digital value from the given Kernel LED
 *
 * \param fd Kernel LED value file.
 * \param val Where to store the received value.
 *
 * \return Error Code
 */
lu_int32_t readKLED(FILE *fdValue, lu_bool_t *val);

/**
 * \brief Writes a digital value to the given Kernel LED
 *
 * \param fd Kernel LED value file.
 * \param val Value to write.
 *
 * \return Error Code
 */
lu_int32_t writeKLED(FILE *fdValue, lu_bool_t val);

/**
 * \brief Initialises the Analogue Input
 *
 * This function keeps open the associated value file.
 * Note that when there is an error, no file is left open and fdValue is NULL.
 *
 * \param aiName        Name of the AI
 * \param hwMonPtr      The relative path from [/sys/platform/] to the device folder
 * \param fdValuePtrPtr AI value file is returned here.
 *
 * \return Error Code
 */
lu_int32_t initAI(const char *aiNamePtr, const char *hwMonPtr, FILE **fdValuePtrPtr);

/**
 * \brief Read Analogue Input Value
 *
 * This function requires the Analogue Input file to be open by calling
 * initAI() first.
 *
 * \param fd  AI value file.
 * \param val Where to store the received value.
 *
 * \return Error Code
 */
lu_int32_t readAIInt(FILE *fd, lu_int32_t *val);

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

#ifdef __cplusplus
}
#endif

#endif /* MCMIOMAP_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
