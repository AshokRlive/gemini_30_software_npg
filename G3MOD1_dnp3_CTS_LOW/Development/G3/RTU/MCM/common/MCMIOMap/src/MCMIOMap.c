/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id: MCMIOMap.c 2 Oct 2013 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/common/MCMIOMap/src/MCMIOMap.c $
 *
 *    FILE TYPE: C source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 2 Oct 2013 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   2 Oct 2013    pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <errno.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MCMIOMap.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

lu_int32_t initGPIO(const lu_uint8_t gpio, const GPIOLIB_DIRECTION direction, FILE **fdValue)
{
    //LU_UNUSED(direction);  // Maintain compatibility and remove warning
    lu_char_t gpioString[GPIO_STRING_LEN];

    /* Export GPIO first */
    FILE* fdExport = fopen(gpioExport, "w");
    if (fdExport != NULL)
    {
        //Note that we ignore return error codes
        fprintf(fdExport, "%i\n", gpio);
        fclose(fdExport);
    }

    /* Set GPIO Direction */
    snprintf( gpioString, GPIO_STRING_LEN, "%s%i/%s",
              gpioExported, gpio, GPIOLIB_DIRECTIONNAME
            );
    gpioString[GPIO_STRING_LEN-1] = '\0';   //NULL terminate the string

    /* Try to open the value file for the exported GPIO */
    FILE* fdDir = fopen(gpioString, "w+");
    if (fdDir != NULL)
    {
        //Note that we ignore return error codes
        fprintf(fdDir, "%s\n", (direction==GPIOLIB_DIRECTION_INPUT)? GPIOLIB_DIRECTIONINPUT : GPIOLIB_DIRECTIONOUTPUT);
        fclose(fdDir);
    }


    // Only if a valid pointer is given do we try to open the gpio value file
    if (fdValue != NULL)
    {
        /* Create GPIO value file full path */
        snprintf( gpioString, GPIO_STRING_LEN, "%s%i/%s",
                  gpioExported, gpio, GPIOLIB_VALUENAME
                );
        gpioString[GPIO_STRING_LEN-1] = '\0';   //NULL terminate the string

        /* Try to open the value file for the exported GPIO */
        *fdValue = fopen(gpioString, "r+");
        if (*fdValue == NULL)
        {
            return errno;
        }
    }
    return 0;   //success
}


lu_int32_t closeGPIO(FILE *fdValue)
{
    if (fdValue != NULL)
    {
        return fclose(fdValue);    //close associated GPIO value file
    }

    return -1;
}


lu_int32_t initKLED(const char *ledName, FILE **fdValuePtrPtr)
{
  lu_int8_t ledString[KLED_STRING_LEN];

  *fdValuePtrPtr = NULL;

  /* Create led value file full path */
  snprintf(ledString, KLED_STRING_LEN, "%s%s/%s", KLED_LIB_PATH, ledName, GPIOLIB_KLEDVALUENAME);
  ledString[KLED_STRING_LEN-1] = '\0';   //NULL terminate the string

  /* Try to open the value file for the exported gpio */
  if ((*fdValuePtrPtr = fopen(ledString, "r+")) == NULL)
  {
      return errno;
  }

  return 0;
}


lu_int32_t setKLEDManual(const char *ledName)
{
    FILE *fdLed;
    lu_int32_t errTmp;
    lu_int8_t   ledString[KLED_STRING_LEN];

    /* Create led trigger file full path */
    snprintf(ledString, KLED_STRING_LEN, "%s%s/%s", KLED_LIB_PATH, ledName, GPIOLIB_KLEDTRIGGERNAME);
    ledString[KLED_STRING_LEN-1] = '\0';   //NULL terminate the string


    if ((fdLed = fopen((const char *)ledString, "w")) == NULL)
    {
        return errno;
    }
    // Make sure the LED GPIOs are created
    errTmp = fprintf(fdLed, "%s\n", GPIOLIB_KLEDMANUALSTATE);
    fclose(fdLed);  //close file anyway
    if(errTmp <= 0)
    {
        return -1;
    }
    return 0;
}


lu_int32_t setKLEDTimer(const char *ledName, lu_uint32_t delayOn, lu_uint32_t delayOff )
{
    FILE *fdLed;
    lu_int32_t errTmp;
    lu_int8_t   ledString[KLED_STRING_LEN];

    /* Create led trigger file full path */
    snprintf(ledString, KLED_STRING_LEN, "%s%s/%s", KLED_LIB_PATH, ledName, GPIOLIB_KLEDTRIGGERNAME);
    ledString[KLED_STRING_LEN-1] = '\0';   //NULL terminate the string

    if ((fdLed = fopen((const char *)ledString, "w")) == NULL)
    {
        return errno;
    }
    // Make sure the LED GPIOs are created
    errTmp = fprintf(fdLed, "%s\n", GPIOLIB_KLEDTIMERSTATE);
    fclose(fdLed);  //close file anyway
    if(errTmp < 0)
    {
        return errTmp;
    }

    /* Create led [delay-on] file full path */
    snprintf(ledString, KLED_STRING_LEN, "%s%s/%s", KLED_LIB_PATH, ledName, GPIOLIB_KLEDDELAYONNAME);
    ledString[KLED_STRING_LEN-1] = '\0';   //NULL terminate the string

    if ((fdLed = fopen((const char *)ledString, "w")) == NULL)
    {
        return errno;
    }
    // Write the on time
    errTmp = fprintf(fdLed, "%i\n", delayOn);
    fclose(fdLed);  //close file anyway
    if(errTmp < 0)
    {
        return errTmp;
    }

    /* Create led [delay-off] file full path */
    snprintf(ledString, KLED_STRING_LEN, "%s%s/%s", KLED_LIB_PATH, ledName, GPIOLIB_KLEDDELAYOFFNAME);
    ledString[KLED_STRING_LEN-1] = '\0';   //NULL terminate the string

    if ((fdLed = fopen((const char *)ledString, "w")) == NULL)
    {
        return errno;
    }
    // Write the off time
    errTmp = fprintf(fdLed, "%i\n", delayOff);
    fclose(fdLed);  //close file anyway
    if(errTmp < 0)
    {
        return errTmp;
    }
    return 0;
}


inline lu_int32_t readGPIO(FILE *fd, lu_bool_t *val)
{
    lu_int32_t ret;
    lu_int32_t value;   //value read

    if( (fd == NULL) || (val == NULL) )
    {
        return -1;
    }
    /* Go to the beginning of the FD */
    fseek(fd, 0, SEEK_SET);
    /* Read */
    ret = fscanf(fd, "%i\n", &value);
    if ((ret > 0) && (ret != EOF))
    {
        *val = (value==0)? LU_FALSE : LU_TRUE;
    }
    return ret;
}


inline lu_int32_t exportGPIO(FILE *fd, lu_uint8_t val)
{
    lu_int32_t ret = -1;
    if (fd == NULL)
    {
        return -1;
    }
    ret = fprintf(fd, "%i\n", val);
    if(ret > 0)
    {
        fflush(fd);
    }
    return ret;
}


inline lu_int32_t writeGPIO(FILE *fd, lu_bool_t val)
{
    return exportGPIO(fd, (lu_uint8_t) val);
}


inline lu_int32_t readKLED(FILE *fdValue, lu_bool_t *val)
{
    return readGPIO(fdValue, val);
}


inline lu_int32_t writeKLED(FILE *fdValue, lu_bool_t val)
{
    return writeGPIO(fdValue, val);
}


lu_int32_t initAI(const char *aiNamePtr, const char *hwMonPtr, FILE **fdValuePtrPtr)
{
    lu_int8_t aiString[MCMANALOGUEINPUT_DEVICE_PATH_LEN];

    *fdValuePtrPtr = NULL;

    /* Create ai value file full path */
    snprintf(aiString, MCMANALOGUEINPUT_DEVICE_PATH_LEN, "%s%s%s",
                    MCMANALOGUEINPUT_PATH, hwMonPtr,
                    aiNamePtr);

    aiString[MCMANALOGUEINPUT_DEVICE_PATH_LEN - 1] = '\0'; //NULL terminate the string

    /* Try to open the value file for the AI (read only) */
    if ((*fdValuePtrPtr = fopen(aiString, "r")) == NULL )
    {
        return -1;
    }

    return 0;
}


inline lu_int32_t readAIInt(FILE *fd, lu_int32_t *val)
{
    lu_int32_t ret;
    lu_int32_t value;   //value read

    if( (fd == NULL) || (val == NULL) )
    {
        return -1;
    }
    /* Go to the beginning of the FD */
    fseek(fd, 0, SEEK_SET);
    /* Read */
    ret = fscanf(fd, "%i\n", &value);
    if ((ret > 0) && (ret != EOF))
    {
        *val = value;
    }

    return ret;
}


/*
 *********************** End of file ******************************************
 */
