/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: CANInterface.cpp 11 Nov 2013 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/common/CANFraming/src/CANInterface.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 11 Nov 2013 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11 Nov 2013   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <unistd.h>
#include <cstring>
#include <errno.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include <linux/can.h>
#include <linux/can/raw.h>
#include <linux/can/error.h>
//#include <linux/can/netlink.h> // !! conflict with "libsocketcan.h"

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "CANInterface.h"
#include "LockingMutex.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#ifndef CAN_FRAMING_SIGNAL
#define CAN_FRAMING_SIGNAL  (SIGRTMIN + 10)
#endif

#define CANIFACE_POLLSTATUSTIME_MS 200 //Time in ms to poll the interface status

#define CANIFACE_MAXRETRIES     10  //Number of times to try to recover the interface when in error state
#define CANIFACE_WAITAGAIN_MS   500 //Time in ms to wait after an unsuccessful interface recovering try
                                    //Note: always > CANIFACE_POLLSTATUSTIME_MS
#define CANIFACE_WAITBRINGUP_MS 100 //Time in ms to wait after bringing DOWN the interface
                                    //Note: always < CANIFACE_POLLSTATUSTIME_MS

/* Conversions to time structures */
static const struct timeval CANIFACE_POLLSTATUS = ms_to_timeval(CANIFACE_POLLSTATUSTIME_MS);

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/* Variables shared by all Loggers */
static CANInterface* canIface[COMM_INTERFACE_LAST]; //list of instances of the handlers
static lu_bool_t initialised = LU_FALSE;            //Initial status of the objects pool
static Mutex mutexInit;             //Controls concurrent access at initialisation time

#if POLLING_CAN_INTERFACE_STATE
static Timer* timerStatus;          //Timer for checking the interface status regularly
static CANStatusTimer* statusHandler;   //Handler of the timer to attend the event
#endif

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

CANInterface& CANInterface::getInstance(const COMM_INTERFACE iface)
{
    initObjectsPool();

    if(iface >= COMM_INTERFACE_LAST)
        return *(canIface[COMM_INTERFACE_UNKNOWN]);

    return *(canIface[iface]);
}


const lu_char_t* CANInterface::getName()
{
    return iName;
}


const struct sockaddr_can& CANInterface::getSocketAddress()
{
    return address;
}

#if POLLING_CAN_INTERFACE_STATE
lu_bool_t CANInterface::attach(CANIfaceObserver& observer)
{
    // Protect the observer queue
    MasterLockingMutex mMutex(queueMutex, LU_TRUE);

    /* Add the observer to the observer list */
    return observers.enqueue(&observer);
}


void CANInterface::detach(CANIfaceObserver& observer)
{
    // Protect the observer queue
    MasterLockingMutex mMutex(queueMutex, LU_TRUE);

    /* Add the observer to the observer list */
    observers.dequeue(&observer);
}


CANInterface::CANIFACE_ERROR CANInterface::up()
{
    const lu_char_t* FTITLE = "CANInterface::up:";

    /* Prevent other operations */
    MasterLockingMutex aMutex(accessMutex);

    if(convertCANState(canState) == IFSTATUS_UP)
    {
        log.info("%s Interface %s already Up.", FTITLE, iName);
        return CANInterface::CANIFACE_ERROR_NONE;  //already set
    }

    /* Bring the interface up */
    if(bringIface(IFSTATUS_UP) != CANInterface::CANIFACE_ERROR_NONE)
    {
        log.error("%s Unable to bring up interface %s.", FTITLE, iName);
        return CANInterface::CANIFACE_ERROR_INIT;
    }

    log.warn("%s interface %s is set to UP.", FTITLE, iName);

    updateStatus();
    return CANInterface::CANIFACE_ERROR_NONE;
}


CANInterface::CANIFACE_ERROR CANInterface::down()
{

    const lu_char_t* FTITLE = "CANInterface::down:";


    /* Prevent other operations */
    MasterLockingMutex aMutex(accessMutex, LU_TRUE);

    if(canState == CAN_STATE_STOPPED)
    {
        log.info("%s Interface %s already Down.", FTITLE, iName);
        return CANInterface::CANIFACE_ERROR_NONE;  //already set
    }

    /* Bring the interface down */
    if(bringIface(IFSTATUS_DOWN) != CANInterface::CANIFACE_ERROR_NONE)
    {
        log.error("%s Unable to bring down interface %s.", FTITLE, iName);
        return CANInterface::CANIFACE_ERROR_INIT;
    }

    log.warn("%s interface %s is set to DOWN.", FTITLE, iName);

    updateStatus();
    return CANInterface::CANIFACE_ERROR_NONE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void CANInterface::updateStatus()
{
    const lu_char_t* FTITLE = "CANInterface::updateStatus:";
    lu_int32_t newStateCAN = -1;
    lu_int32_t result;

    if(ifaceID == COMM_INTERFACE_UNKNOWN)
        return; //invalid interface

    result = can_get_state(iName, &newStateCAN);
    if(result < 0)
    {
        //error getting CAN state
        log.error("%s error %i getting %s state", FTITLE, result, this->iName);
        newStateCAN = CAN_STATE_BUS_OFF;    //Set as Disabled/error
    }
    const can_state stateCAN = static_cast<can_state>(newStateCAN);

    if(canState != stateCAN)
    {
        //Status changed

        /* Update status */
        canState = stateCAN;
        logCANState(FTITLE, canState);

        status = convertCANState(canState);
        updateObservers(status);

        /* Detect error status */
        if(/* (stateCAN == CAN_STATE_BUS_OFF) ||*/ (stateCAN == CAN_STATE_ERROR_PASSIVE) )
        {
            //Entered error status: start counting
            log.warn("%s %s interface seems to be in error state: recovering...", FTITLE, iName);
            clock_gettimespec(&timeLastError);  //get timestamp of the last error
        }
        else
        {
            //Not an error status: cancel counting
            timeLastError = TIMESPEC_ZERO;
            fatalErrorCount = 0;
        }
    }
    else
    {
        //Status did not change: check if it is still in error
        if( ( (stateCAN == CAN_STATE_BUS_OFF) || (stateCAN == CAN_STATE_ERROR_PASSIVE) ) &&
                (fatalErrorCount <= CANIFACE_MAXRETRIES)    //do not retry anymore after fatal
                )
        {
            //It keeps being in error state: check timeout of last error */
            if(timespec_compare(&timeLastError, &TIMESPEC_ZERO) != LU_TRUE)
            {
                struct timespec timeNow;
                clock_gettimespec(&timeNow);
                if(timespec_elapsed_ms(&timeLastError, &timeNow) >= CANIFACE_WAITAGAIN_MS)
                {
                    if(fatalErrorCount++ < CANIFACE_MAXRETRIES)
                    {
                        //Time is up and it isn't recovering: restart interface */
                        log.warn("%s %s interface seems to do NOT recover: restarting it (try %i of %i).",
                                  FTITLE, iName, fatalErrorCount, CANIFACE_MAXRETRIES
                                  );
                        can_do_stop(iName);         //bring interface down
                        timeNow = ms_to_timespec(CANIFACE_WAITBRINGUP_MS);
                        lucy_nanosleep(&timeNow);   //wait for recovering
                        can_do_start(iName);        //bring interface up

                        clock_gettimespec(&timeLastError);  //restart counting for retrying
                    }
                    else
                    {
                        /* Retries did not have any success: give up */
                        log.fatal("%s Unable to recover %s interface.", FTITLE, iName);

                        /* Report unrecoverable error */
                        updateObservers(IFSTATUS_ERROR_FATAL);
                    }
                }
            }
        }
    }
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

void CANStatusTimer::handleAlarmEvent(Timer &source)
{
    LU_UNUSED(source);

    /* Update status of all of the Interface objects present in the factory */
    for (lu_uint32_t idx = 0; idx < COMM_INTERFACE_LAST; ++idx)
    {
        canIface[idx]->updateStatus();
    }
}
#endif

CANInterface::CANInterface(const COMM_INTERFACE interface) :
                                canState(CAN_STATE_MAX),
                                ifaceID(interface),
#if POLLING_CAN_INTERFACE_STATE
                                status(IFSTATUS_DOWN),
                                accessMutex(LU_FALSE),
                                queueMutex(LU_FALSE),
                                timeLastError(TIMESPEC_ZERO),
                                fatalErrorCount(0),
#endif
                                log(Logger::getLogger(SUBSYSTEM_ID_CANFRAMING)),
                                enabled(LU_FALSE)
{
    const lu_char_t* FTITLE = "CANInterface:";
    const lu_char_t *ifaceStr;

    address.can_family = AF_CAN;
    address.can_ifindex = 0;    //Set broadcast enabled by default
    iName[0] = '\0';            //Set initial name empty

    /* Find interface name */
    switch(ifaceID)
    {
    case COMM_INTERFACE_CAN0:
        ifaceStr = CAN0;
        break;
    case COMM_INTERFACE_CAN1:
        ifaceStr = CAN1;
        break;
    case COMM_INTERFACE_UNKNOWN:
        ifaceStr = CAN_UNKNOWN;
        break;
    default:
        /* Unsupported interface */
        log.fatal("%s Unsupported interface: %i", FTITLE, ifaceID);
        return;
    }

    /* Save interface name */
    strncpy(iName, ifaceStr, IFNAMSIZ);
    iName[IFNAMSIZ - 1] = '\0';     //cut if too big

    /* Initialise interface address and related values */
    if(ifaceID != COMM_INTERFACE_UNKNOWN)
        initSocket();

#if POLLING_CAN_INTERFACE_STATE
    updateStatus();
#endif
}


CANInterface::~CANInterface()
{
#if POLLING_CAN_INTERFACE_STATE
    /* Discard observers queue */
    MasterLockingMutex mlock(queueMutex, LU_TRUE);
    observers.empty();
    /* Stop timer and handler */
    timerStatus->stop();    //Stop timer
    delete timerStatus;
    delete statusHandler;   //Remove handler
    /* Bring interface down */
    down();
#endif
}


void CANInterface::initObjectsPool()
{
    LockingMutex lock(mutexInit);
    if(initialised == LU_TRUE)
        return; //Already initialised

    /* Create instances */
    for (lu_uint32_t idx = 0; idx < COMM_INTERFACE_LAST; ++idx)
    {
        /* Create all interface objects */
        canIface[idx] = new CANInterface(static_cast<COMM_INTERFACE>(idx));
    }

#if POLLING_CAN_INTERFACE_STATE
    /* Initialise timer for updating CAN status of all objects in the pool */
    statusHandler = new CANStatusTimer();
    timerStatus = new Timer(CANIFACE_POLLSTATUS.tv_sec, CANIFACE_POLLSTATUS.tv_usec,
                    CAN_FRAMING_SIGNAL, statusHandler, LU_TRUE);
    timerStatus->start();
#endif

    initialised = LU_TRUE;  //Initialisation finished
}


CANInterface::CANIFACE_ERROR CANInterface::initSocket()
{
    const lu_char_t* FTITLE = "CANInterface::initSocket:";
    lu_int32_t socketFd;    //associated socket FD for getting address info
    struct ifreq ifr;       //Structure for getting address info

    /* Open RAW CAN socket for association with the Interface */
    socketFd = socket(PF_CAN, SOCK_RAW, CAN_RAW);
    if (socketFd < 0)
    {
        log.fatal("%s socket creation error %i (%s) - iface: %s",
                    FTITLE, errno, strerror(errno), iName
                  );
        return CANInterface::CANIFACE_ERROR_INIT;
    }

    /* Get interface address */
    strcpy(ifr.ifr_name, iName);
    if (ioctl(socketFd, SIOCGIFINDEX, &ifr) < 0)
    {
        log.fatal("%s SIOCGIFINDEX error %i (%s) - iface: %s",
                    FTITLE, errno, strerror(errno), iName
                  );
        close(socketFd);
        return CANInterface::CANIFACE_ERROR_INIT;
    }

    /* Store CAN address structure */
    address.can_family = AF_CAN;
    address.can_ifindex = ifr.ifr_ifindex;

    close(socketFd);
    log.debug("%s created socket %i for interface %s", FTITLE, socketFd, iName);

    return CANInterface::CANIFACE_ERROR_NONE;
}

#if POLLING_CAN_INTERFACE_STATE
void CANInterface::updateObservers(IFSTATUS ifstatus)
{
    MasterLockingMutex mMutex(queueMutex);

    /*Notify observer list from first observer*/
    CANIfaceObserver* obPtr = observers.getFirst();
    while(obPtr != NULL)
    {
        obPtr->update(ifstatus);

        //Get next observer
        obPtr = observers.getNext(obPtr);
    }
}


CANInterface::CANIFACE_ERROR CANInterface::bringIface(IFSTATUS status)
{
    const lu_char_t* FTITLE = "CANInterface::bringIface:";

    switch (status)
    {
        case IFSTATUS_UP:
            if(can_do_start(iName) < 0)
            {
                log.error("%s interface %s is NOT brought up.", FTITLE, iName);
                return CANInterface::CANIFACE_ERROR_UP;
            }
            break;
        case IFSTATUS_DOWN:
        default:
            if(can_do_stop(iName) < 0)
            {
                log.error("%s interface %s is NOT brought down.", FTITLE, iName);
                return CANInterface::CANIFACE_ERROR_DOWN;
            }
            break;
    }

    //Note that the state will be checked on the next timer iteration
    return CANInterface::CANIFACE_ERROR_NONE;
}

CANInterface::IFSTATUS CANInterface::convertCANState(can_state state)
{
    switch (state)
    {
        case CAN_STATE_ERROR_ACTIVE:
        case CAN_STATE_ERROR_WARNING:
            return CANInterface::IFSTATUS_UP;
        default:
            break;
    }
    return CANInterface::IFSTATUS_DOWN;
}


void CANInterface::logCANState(const lu_char_t* title, const can_state stateCAN)
{
    //state changed: log it
    lu_bool_t errorType = LU_FALSE; //Type of log: LU_TRUE==Error, LU_FALSE==Warning
    std::string logString;
    switch(stateCAN)
    {
        case CAN_STATE_BUS_OFF:
            logString = "BUS-OFF";
            errorType = LU_TRUE;
            break;
        case CAN_STATE_ERROR_ACTIVE:
            logString = "ERR-ACT";
            break;
        case CAN_STATE_ERROR_PASSIVE:
            logString = "ERR-PAS";
            errorType = LU_TRUE;
            break;
        case CAN_STATE_ERROR_WARNING:
            logString = "WARN";
            break;
        case CAN_STATE_SLEEPING:
            logString = "SLEEP";
            break;
        case CAN_STATE_STOPPED:
            logString = "STOP";
            break;
        case CAN_STATE_MAX:
            logString = "Invalid";
            errorType = LU_TRUE;
            break;
        default:
            logString = "Other";
            break;
    }
    if(errorType == LU_TRUE)
    {
        log.error("%s %s state changed to %s (%i)",
                    title, this->iName, logString.c_str(), stateCAN);
    }
    else
    {
        log.warn("%s %s state changed to %s (%i)",
                    title, this->iName, logString.c_str(), stateCAN);
    }
}
#endif


/*
 *********************** End of file ******************************************
 */
