/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       The CAN Framing is in charge of keeping the CAN communication and
 *       messaging that involves communication opening, status, and closing,
 *       as well as reading or writing operations, even if it implies message
 *       fragmentation handling.
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <cstring>
#include <sstream>
#include <errno.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include <linux/can/raw.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "timeOperations.h"
#include "CANProtocol.h"
#include "ModuleProtocol.h"
#include "CANInterface.h"
#include "CANFraming.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/* CAN fragmentation management values */
#define CAN_FRAGMENT_PAYLOAD        (7)
#define CAN_FRAGMENT_ID_MASK        (0x3F) /* 6 bits */

#define INCREMENT_AND_WRAP(a, mask) ((a) = (((a)+ 1) & mask))
#define CAN_FRAGMENT_NEXT_ID(a) (INCREMENT_AND_WRAP(a, CAN_FRAGMENT_ID_MASK))
#define CAN_FRAGMENT_HEADER_SIZE    (sizeof(CANFragmentHeaderStr))

#define CAN_FAIL_MAXRETRIES         (40)

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
#define SEND_ERR_THRESHOLD 10

/**
 * \brief Get the name of some of the most important CAN messages
 *
 * Used for CAN protocol debug messages. If the CAN message is not "important",
 * it does not give any name, but provides its code anyway.
 *
 * \param msgType CAN message type
 * \param msgID CAN message ID
 *
 * \return Constant char string with the intended name. Empty string if not listed.
 */
static const lu_char_t* getCANMessageName(const MODULE_MSG_TYPE msgType,
                                            const MODULE_MSG_ID msgID)
{
    switch(msgType)
    {
    case MODULE_MSG_TYPE_HPRIO:
        return (msgID == MODULE_MSG_ID_HPRIO_TSYNCH)? "TSYNCH_C" : "";
    case MODULE_MSG_TYPE_LPRIO:
        switch(msgID)
        {
        case MODULE_MSG_ID_LPRIO_HBEAT_M:
            return "HBEAT_M";
        case MODULE_MSG_ID_LPRIO_HBEAT_S:
            return "HBEAT_S";
        case MODULE_MSG_ID_LPRIO_MINFO_C:
            return "MINFO_C";
        case MODULE_MSG_ID_LPRIO_MINFO_R:
            return "MINFO_R";
        default:
            break;
        }
        break;
    case MODULE_MSG_TYPE_EVENT:
        switch(msgID)
        {
        case MODULE_MSG_ID_EVENT_FPI:
            return "EVENT FPI";
        case MODULE_MSG_ID_EVENT_DIGITAL:
            return "EVENT DIGITAL";
        case MODULE_MSG_ID_EVENT_ANALOGUE:
            return "EVENT ANALOG";
        default:
            break;
        }
        break;
    case MODULE_MSG_TYPE_CMD:
        switch(msgID)
        {
        case MODULE_MSG_ID_CMD_SWC_CH_SELECT_C:
            return "SW SELECT_C";
        case MODULE_MSG_ID_CMD_SWC_CH_SELECT_R:
            return "SW SELECT_R";
        case MODULE_MSG_ID_CMD_SWC_CH_OPERATE_C:
            return "SW OPERATE_C";
        case MODULE_MSG_ID_CMD_SWC_CH_OPERATE_R:
            return "SW OPERATE_R";
        case MODULE_MSG_ID_CMD_SWC_CH_CANCEL_C:
            return "SW CANCEL_C";
        case MODULE_MSG_ID_CMD_SWC_CH_CANCEL_R:
            return "SW CANCEL_R";
        default:
            return "CMD";
            break;
        }
    case MODULE_MSG_TYPE_MD_CMD:
        switch(msgID)
        {
        case MODULE_MSG_ID_MD_CMD_START_MODULE_C:
            return "STARTMODULE_C";
        case MODULE_MSG_ID_MD_CMD_START_MODULE_R:
            return "STARTMODULE_R";
        case MODULE_MSG_ID_MD_CMD_RESTART:
            return "MODULE RESTART";
        default:
            break;
        }
        break;
    case MODULE_MSG_TYPE_BL:
        switch(msgID)
        {
        case MODULE_MSG_ID_BL_START_APP_C:
            return "BL_STARTAPP_C";
        case MODULE_MSG_ID_BL_START_APP_R:
            return "BL_STARTAPP_R";
        default:
            break;
        }
        break;
    default:
        break;
    }
    return "";
}


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

const static lu_int32_t CanRXBufferSize = (256 * 1024);

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

CANFragmentDecoder::CANFragmentDecoder() : status(DECODER_STATUS_IDLE),
                                           message(NULL),
                                           firstFragmentID(0),
                                           expectedHeader(0),
                                           maxFragmentOffset(0),
                                           receivedFragments(0),
                                           expectedFragments(0),
                                           log(Logger::getLogger(SUBSYSTEM_ID_CANFRAMING))
{
}


CAN_ERROR CANFragmentDecoder::addFragment( struct can_frame *frame,
                                           ModuleMessage **msg
                                         )
{
    CANHeaderStr *CANHeaderPtr;
    CANFragmentHeaderStr *CANFragmentHeaderPtr;
    lu_uint32_t payloadOffset;
    lu_uint32_t fragmentOffset;
    lu_uint32_t msgLen;
    CAN_ERROR ret = CAN_ERROR_NONE;

    if( (frame == NULL)  || (msg == NULL) )
    {
        return CAN_ERROR_PARAM;
    }

    /* Decode header */
    CANHeaderPtr = (CANHeaderStr*)(&frame->can_id);
    if(CANHeaderPtr->fragment == LU_FALSE)
    {
        return CAN_ERROR_INVALID_FRAME;
    }

    /* Decode fragmentation header */
    CANFragmentHeaderPtr = (CANFragmentHeaderStr*)(&frame->data[0]);

    /* Save message length */
    msgLen = frame->can_dlc - CAN_FRAGMENT_HEADER_SIZE;

    /* Check start/stop bits */
    if ( (CANFragmentHeaderPtr->start == 1) &&
         (CANFragmentHeaderPtr->stop  == 0)
       )
    {
        /* Valid first fragment: Initialise control data */
        this->reset();  //clear first

        /* Allocate RAW message */
        message = new RAWLargeModuleMessage();
        if(message == NULL)
        {
            ret = CAN_ERROR_MEM;
            return ret;
        }
        status = DECODER_STATUS_RUNNING;
        expectedFragments = 0;
        receivedFragments = 1;
        firstFragmentID = CANFragmentHeaderPtr->id;
        maxFragmentOffset = 0;
        expectedHeader = frame->can_id;

        /* Save buffer from padded to packed (non-aligned) message */
        for(lu_uint32_t i = 0; i < msgLen; i++)
        {
            message->payload[i] = frame->data[i+1];
        }

        /* Save length */
        message->payloadLen = msgLen;

        ret = CAN_ERROR_INCOMPLETE;
    }
    else if(status==DECODER_STATUS_RUNNING)
    {
        /* Check start/stop bits */
        if (CANFragmentHeaderPtr->stop == 1)
        {
            /* Valid header (continuation packet)
             * Check msg header
             */
            if ( (expectedHeader == frame->can_id)                       &&
                 (message->payloadLen + msgLen <= MODULE_MESSAGE_LENGTH )
               )
            {
                /* Another fragment received */
                receivedFragments++;

                /* Increase message length */
                message->payloadLen += msgLen;

                /* Get payload offset */
                payloadOffset = getPayloadOffset(CANFragmentHeaderPtr->id);

                /* Get fragment offset */
                fragmentOffset = getFragmentPos(CANFragmentHeaderPtr->id) + 1;
                maxFragmentOffset = LU_MAX(maxFragmentOffset, fragmentOffset);

                /* Save buffer from padded to packed (non-aligned) message */
                for(lu_uint32_t i = 0; i < msgLen; i++)
                {
                    message->payload[payloadOffset+i] = frame->data[i+1];
                }

                /* Last fragment ? */
                if(CANFragmentHeaderPtr->start == 0)
                {
                    /* Yes - Update the expected fragments */
                    expectedFragments = fragmentOffset;
                }

                /*  */
                if( (expectedFragments > 0)                 &&
                    (expectedFragments == receivedFragments)&&
                    (maxFragmentOffset == expectedFragments)
                  )
                {
                    /* Full package received */
                    message->header.messageType   = static_cast<MODULE_MSG_TYPE>(CANHeaderPtr->messageType);
                    message->header.messageID     = static_cast<MODULE_MSG_ID>(CANHeaderPtr->messageID);
                    message->header.destination   = static_cast<MODULE>(CANHeaderPtr->deviceDst);
                    message->header.destinationID = static_cast<MODULE_ID>(CANHeaderPtr->deviceIDDst);
                    message->header.source        = static_cast<MODULE>(CANHeaderPtr->deviceSrc);
                    message->header.sourceID      = static_cast<MODULE_ID>(CANHeaderPtr->deviceIDSrc);

                    /* Change state machine */
                    status = DECODER_STATUS_IDLE;

                    /* Return RAW buffer */
                    *msg = message;

                    /* Set local message as NULL.
                     * The caller will delete the buffer
                     */
                    message = NULL;

                    ret = CAN_ERROR_NONE;
                }
                else
                {
                    ret = CAN_ERROR_INCOMPLETE;
                }
            }
            else
            {
                /* Wrong canid/length - Reset State Machine */
                if(expectedHeader != frame->can_id)
                {
                    log.error("CAN Message fragment has wrong CAN Frame ID. "
                                "Expected: 0x%x (err 0x%x) - Received: 0x%x (err 0x%x)",
                                expectedHeader,
                                expectedHeader & CAN_EFF_FLAG,
                                frame->can_id,
                                frame->can_id & CAN_EFF_FLAG
                              );
                }
                else
                {
                    log.error("CAN Message fragment has unexpected length. "
                                "Received: %u Bytes, more than the %u max allowed.",
                                message->payloadLen + msgLen,
                                MODULE_MESSAGE_LENGTH
                              );
                }
                /* Release message buffer */
                this->reset();  //set status to DECODER_STATUS_IDLE
                ret = CAN_ERROR_INVALID_FRAME;
            }
        }
        else
        {
            /* Wrong header - Reset State Machine */
            log.error("Wrong fragmentation header from CAN device %i:%i "
                            "in message 0x%02x-0x%02x; "
                            "Expected continuation packet - Received: "
                            "start: %i - stop: %i - id: 0x%02x. ",
                            CANHeaderPtr->deviceSrc,
                            CANHeaderPtr->deviceIDSrc,
                            CANHeaderPtr->messageType,
                            CANHeaderPtr->messageID,
                            CANFragmentHeaderPtr->start,
                            CANFragmentHeaderPtr->stop,
                            CANFragmentHeaderPtr->id
                          );
            /* Release message buffer */
            this->reset();  //set status to DECODER_STATUS_IDLE
            ret = CAN_ERROR_INVALID_FRAME;
        }
    }
    return ret;
}


void CANFragmentDecoder::reset()
{
    if(message != NULL)
    {
        delete message;
        message = NULL;
    }

    status = DECODER_STATUS_IDLE;
}




CANFraming::CANFraming(COMM_INTERFACE ifaceID, lu_bool_t enableLoopBack) :
                                 socketInit(LU_FALSE),
                                 iface(CANInterface::getInstance(ifaceID)),
                                 socketFd(-1),
                                 loopBack(enableLoopBack),
                                 readMutex(LU_TRUE),
                                 writeMutex(LU_TRUE),
                                 log(Logger::getLogger(SUBSYSTEM_ID_CANFRAMING))


{
    iName = iface.getName();    //Save interface name

    if(initSocket() == CAN_ERROR_NONE)
    {
        socketInit = LU_TRUE;
    }

#if POLLING_CAN_INTERFACE_STATE
    //Register this as an observer to get notified of any change in the Interface status
    iface.attach(*this);
#endif
}

CANFraming::~CANFraming()
{
#if POLLING_CAN_INTERFACE_STATE
    iface.detach(*this);
#endif
    closeSocket();
}


CAN_ERROR CANFraming::setFilter(CanFilterTable &filter)
{
    /* Force setting of the EFF flag */
    /* When this flag is not set, the filter is acknowledged as a non-extended
     * CAN message, thus ignoring the lower part of the can_id and not filtering
     * as expected. This issue is more visible when having 2 fragmented incoming
     * messages at the same time (interleaved).
     */
    struct can_filter* filterCANTable = filter.getTable();
    for (lu_uint32_t i = 0; i < filter.getEntries(); ++i)
    {
        filterCANTable->can_id |= CAN_EFF_FLAG; //Force EFF flag on the filter setting
        filterCANTable->can_mask |= CAN_EFF_FLAG; //Force EFF flag on the filter mask
    }

    /* Clear previous table */
    if(filterTable.getEntries() != 0)
    {
        if(filterTable.getTable() != NULL)
        {
            delete [] filterTable.getTable();
            filterTable.setTable(NULL, 0);
        }
    }
    /* Copy the new filter locally */
    if (filter.getEntries() != 0)
    {
        struct can_filter* filterCAN = new can_filter[filter.getEntries()];
        memcpy(filterCAN, filter.getTable(), filter.getSize());
        filterTable.setTable(filterCAN, filter.getEntries());
    }

    if (socketInit == LU_TRUE)
    {
        /* Apply filter */
        if(setsockopt(socketFd, SOL_CAN_RAW, CAN_RAW_FILTER,
                    filterTable.getTable(), filterTable.getSize()) == 0)
        {
            return CAN_ERROR_NONE;
        }
    }
    return CAN_ERROR_INIT;
}


CAN_ERROR CANFraming::read(ModuleMessage **message, struct timeval *timeout)
{
    const lu_char_t* FTITLE = "CANFraming::read:";
    CAN_ERROR ret = CAN_ERROR_NONE;
    CANHeaderStr *CANHeaderPtr;
    struct can_frame frame;

    if (message == NULL)
        return CAN_ERROR_PARAM;

    if (socketInit == LU_FALSE || socketFd < 0)
        return CAN_ERROR_INIT;

#if POLLING_CAN_INTERFACE_STATE
    if (iface.getStatus() != CANInterface::IFSTATUS_UP)
        return CAN_ERROR_DOWN;
#endif

    LockingMutex lMutex(readMutex);

    /* Continuously read until we find a valid packet or an error/timeout occur */
    while(LU_TRUE)
    {
        /* Read message */
        ret = receive(FTITLE, frame, timeout);
        if(ret != CAN_ERROR_NONE)
        {
            return ret; //error happened: we can't keep trying to read
        }

        /* check Frame type */
        if(frame.can_id & CAN_EFF_FLAG)
        {
            /* Extended frame. Decode header */
            CANHeaderPtr = (CANHeaderStr*)(&frame.can_id);

            if(CANHeaderPtr->fragment == LU_FALSE)
            {
                /* Standard payload. Allocate a small message */
                RAWSmallModuleMessage *messagePtr = new RAWSmallModuleMessage(frame.can_dlc);
                if (messagePtr == NULL)
                {
                    log.fatal("%s Memory error getting CAN payload - iface: %s.", FTITLE, iName);
                    ret = CAN_ERROR_MEM;
                }
                else
                {
                    /* Save decoded header*/
                    messagePtr->header.messageType   = static_cast<MODULE_MSG_TYPE>(CANHeaderPtr->messageType);
                    messagePtr->header.messageID     = static_cast<MODULE_MSG_ID>(CANHeaderPtr->messageID);
                    messagePtr->header.destination   = static_cast<MODULE>(CANHeaderPtr->deviceDst);
                    messagePtr->header.destinationID = static_cast<MODULE_ID>(CANHeaderPtr->deviceIDDst);
                    messagePtr->header.source        = static_cast<MODULE>(CANHeaderPtr->deviceSrc);
                    messagePtr->header.sourceID      = static_cast<MODULE_ID>(CANHeaderPtr->deviceIDSrc);

                    /* save payload: optimized */
                    *(lu_uint32_t*)&messagePtr->payload[0] = *(lu_uint32_t*)&frame.data[0];
                    *(lu_uint32_t*)&messagePtr->payload[4] = *(lu_uint32_t*)&frame.data[4];

                    /* Save the message buffer */
                    *message = messagePtr;
                }

                /* Valid message found or error. Exit from loop */
                break;
            }
            else
            {
                /* Fragmented payload */
                ret = fragmentDecoder.addFragment(&frame, message);
                if(ret == CAN_ERROR_NONE)
                {
                    /* Valid message found. Exit from loop */
                    break;
                }
            }
        }
        else if(frame.can_id & CAN_ERR_FLAG)
        {
            /* Error frame */
            log.warn("%s Error CAN frame: %0X - "
                            "0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x",
                            FTITLE,
                            frame.can_id,
                            frame.data[0],
                            frame.data[1],
                            frame.data[2],
                            frame.data[3],
                            frame.data[4],
                            frame.data[5],
                            frame.data[6],
                            frame.data[7]);
            ret = CAN_ERROR_ERR_FRAME;
            break;
        }
        else
        {
            log.warn("%s Unsupported CAN Frame: %0X", FTITLE, frame.can_id);
            ret = CAN_ERROR_INVALID_FRAME;
            break;
        }
    }

    return ret;
}

CAN_ERROR CANFraming::write(ModuleMessage *message, const lu_uint16_t retries)
{
    const lu_char_t* FTITLE = "CANFraming::write:";
    CAN_ERROR ret = CAN_ERROR_NONE;
    PayloadRAW payload;

    if(message == NULL)
        return CAN_ERROR_PARAM;

    if (socketInit == LU_FALSE || socketFd < 0)
        return CAN_ERROR_INIT;

#if POLLING_CAN_INTERFACE_STATE
    if (iface.getStatus() != CANInterface::IFSTATUS_UP)
        return CAN_ERROR_DOWN;
#endif
    if (iface.isEnabled() == LU_FALSE)
        return CAN_ERROR_DOWN;

    LockingMutex lMutex(writeMutex);

    /* Get "serialised" payload */
    message->getPayload(&payload);

    lu_int32_t i = retries;
    do
    {
        /* Check payload length */
        if (payload.payloadLen <= CAN_MAX_PAYLOAD)
        {
           ret = sendMessage(message);
        }
        else if (payload.payloadLen <= MODULE_MESSAGE_LENGTH)
        {
            ret = sendFragmentedMessage(message);
        }
        else
        {
            log.error("%s Message too long (%i)",FTITLE, payload.payloadLen);
            ret = CAN_ERROR_TOO_LONG;
            break;
        }

        if(ret == CAN_ERROR_NONE)
        {
            // send success
            break;
        }
        else
        {
            //send failed, continue to retry
            i --;
        }
    }while(i > 0);

    return ret;
}


lu_int32_t CANFraming::getFD()
{
    if(isActive() == LU_TRUE)
    {
        return socketFd;    //Give socket FD only when status is valid
    }
    else
    {
        return -1;
    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
#if POLLING_CAN_INTERFACE_STATE
void CANFraming::update(CANInterface::IFSTATUS status)
{
    if(socketFd < 0 && status == CANInterface::IFSTATUS_UP)
    {
        /* No socket present: create */
        if(initSocket() == CAN_ERROR_NONE)
            socketInit = LU_TRUE;

    }
}
#endif

inline lu_uint8_t CANFragmentDecoder::getFragmentPos(lu_uint8_t currentID)
{
    if(currentID > firstFragmentID)
    {
        return (currentID - firstFragmentID);
    }
    else
    {
        return ((CAN_MAX_FRAGMENTS + currentID - firstFragmentID) + 1);
    }

}


inline lu_uint32_t CANFragmentDecoder::getPayloadOffset(lu_uint8_t currentID)
{

    return (getFragmentPos(currentID) * CAN_FRAGMENT_PAYLOAD);

}


CAN_ERROR CANFraming::initSocket()
{
    const lu_char_t* FTITLE = "CANFraming::initSocket:";
    struct ifreq ifr;
    lu_int32_t curr_rcvbuf_size;
    socklen_t curr_rcvbuf_size_len;

    /* Open RAW CAN socket */
    socketFd = socket(PF_CAN, SOCK_RAW, CAN_RAW);
    if (socketFd < 0)
    {
        log.fatal("%s socket creation error (%i) - iface: %s.", FTITLE, errno, iName);
        return CAN_ERROR_INIT;
    }
    log.info("%s created socket %i for interface %s", FTITLE, socketFd, iName);

    /* Get interface number */
    strcpy(ifr.ifr_name, iName);
    if (ioctl(socketFd, SIOCGIFINDEX, &ifr) < 0)
    {
        log.fatal("%s SIOCGIFINDEX error (%i) - iface: %s.", FTITLE, errno, iName);
        return CAN_ERROR_INIT;
    }

    /* Initialise CAN address structure */
    address = iface.getSocketAddress();

    /* Disable default receive filter */
    if(setsockopt(socketFd, SOL_CAN_RAW, CAN_RAW_FILTER, NULL, 0) < 0)
    {
        log.warn("%s setsockopt error (%i) - iface: %s.", FTITLE, errno, iName);
    }

    /* Disable loopback */
    if(loopBack == LU_FALSE)
    {
        lu_uint32_t loopbackVal = 0;
        setsockopt( socketFd,
                    SOL_CAN_RAW,
                    CAN_RAW_LOOPBACK,
                    &loopbackVal,
                    sizeof(loopbackVal)
                  );
    }

    /* Bind the interface to the local socket */
    if (bind(socketFd, (struct sockaddr *)&address, sizeof(address)) < 0)
    {
        log.warn("%s bind error (%i) - iface: %s.", FTITLE, errno, iName);
        return CAN_ERROR_INIT;
    }

    /* Set receive buffer size */
    if( setsockopt( socketFd,
                    SOL_SOCKET,
                    SO_RCVBUF,
                    &CanRXBufferSize,
                    sizeof(CanRXBufferSize)
                  ) < 0
      )
    {
        log.warn("%s Set receive buffer size error (%i) - iface: %s", FTITLE, errno, iName);
    }

    /* Get receive buffer size */
    curr_rcvbuf_size_len = sizeof(curr_rcvbuf_size);
    if( getsockopt( socketFd,
                    SOL_SOCKET,
                    SO_RCVBUF,
                    &curr_rcvbuf_size,
                    &curr_rcvbuf_size_len
                  ) < 0
      )
    {
        log.warn("%s Get receive buffer size error (%i) - iface: %s", FTITLE, errno, iName);
    }
    else
    {
        log.info("%s Buffer size: set: %i Get: %i - iface: %s",
                    FTITLE, CanRXBufferSize, curr_rcvbuf_size, iName
                  );
    }

    /* Apply filter */
    if( filterTable.getEntries() != 0)
    {
        if( setsockopt(socketFd, SOL_CAN_RAW, CAN_RAW_FILTER,
                        filterTable.getTable(), filterTable.getSize()) )
        {
            log.warn("%s Cannot set CAN filter: error %i (%s) - iface: %s",
                                FTITLE, errno, strerror(errno), iName
                              );
        }
    }
    return CAN_ERROR_NONE;
}


void CANFraming::closeSocket()
{
    /* close the socket */
    if (socketFd >= 0)
    {
        close(socketFd);
        socketFd = -1;
    }
}


CAN_ERROR CANFraming::sendMessage(ModuleMessage *message)
{
    const lu_char_t* FTITLE = "CANFraming::sendMessage:";
    CANHeaderStr *CANHeaderPtr;
    PayloadRAW payload;
    struct can_frame frame;

    /* Get "serialised" payload */
    message->getPayload(&payload);

    /* Encode message header */
    frame.can_id = CAN_EFF_FLAG;//Extended frame
    CANHeaderPtr = (CANHeaderStr*)(&frame.can_id);
    CANHeaderPtr->messageType = message->header.messageType;
    CANHeaderPtr->messageID   = message->header.messageID;
    CANHeaderPtr->deviceDst   = message->header.destination;
    CANHeaderPtr->deviceIDDst = message->header.destinationID;
    CANHeaderPtr->deviceSrc   = message->header.source;
    CANHeaderPtr->deviceIDSrc = message->header.sourceID;
    CANHeaderPtr->fragment    = LU_FALSE;

    /* Save buffer from padded to packed (non-aligned) message */
    frame.can_dlc = payload.payloadLen;
    for(lu_uint32_t i = 0; i < payload.payloadLen; ++i)
    {
        frame.data[i] = payload.payloadPtr[i];
    }

    /* Send complete message */
    return sending(FTITLE, frame);
}


CAN_ERROR CANFraming::sendFragmentedMessage(ModuleMessage *message)
{
    const lu_char_t* FTITLE = "CANFraming::sendFragmentedMessage:";
    CANHeaderStr *CANHeaderPtr;         //message header
    PayloadRAW payload;
    CANFragmentHeaderStr *CANFragmentHeaderPtr;
    struct can_frame frame;
    lu_uint32_t fragmentCounterTX = 0;  //Fragmented message counter
    lu_bool_t firstFragment = LU_TRUE;
    lu_uint32_t sentData = 0;           //amount of data sent in a frame
    lu_uint32_t bytes = 0;              //frame size calculation


    if(socketInit == LU_FALSE)
       return CAN_ERROR_SEND;

    /* Check socket status */
    if(socketFd < 0)
        return CAN_ERROR_SEND;

    /* Get "serialised" payload */
    message->getPayload(&payload);

    /* Encode message header */
    frame.can_id = CAN_EFF_FLAG;//Extended frame
    CANHeaderPtr = (CANHeaderStr*)(&frame.can_id);
    CANHeaderPtr->messageType = message->header.messageType;
    CANHeaderPtr->messageID   = message->header.messageID;
    CANHeaderPtr->deviceDst   = message->header.destination;
    CANHeaderPtr->deviceIDDst = message->header.destinationID;
    CANHeaderPtr->deviceSrc   = message->header.source;
    CANHeaderPtr->deviceIDSrc = message->header.sourceID;
    CANHeaderPtr->fragment    = LU_TRUE;

    /* Get a reference for the fragmentation header */
    CANFragmentHeaderPtr = (CANFragmentHeaderStr *)&frame.data[0];

    while(payload.payloadLen > 0)
    {
        /* Initialise fragmentation header */
        if(firstFragment == LU_TRUE)
        {
            CANFragmentHeaderPtr->start = 1;
            CANFragmentHeaderPtr->stop  = 0;

            firstFragment = LU_FALSE;

            fragmentCounterTX = 0;
        }
        else
        {
            if (payload.payloadLen > CAN_FRAGMENT_PAYLOAD)
            {
                CANFragmentHeaderPtr->start = 1;
                CANFragmentHeaderPtr->stop  = 1;
            }
            else
            {
                /* Last fragment */
                CANFragmentHeaderPtr->start = 0;
                CANFragmentHeaderPtr->stop  = 1;
            }
        }

        /* Update fragment ID */
        CANFragmentHeaderPtr->id = fragmentCounterTX;
        CAN_FRAGMENT_NEXT_ID(fragmentCounterTX);

        /* Set frame size */
        bytes = LU_MIN(payload.payloadLen, CAN_FRAGMENT_PAYLOAD);

        /* Copy payload */
        for(lu_uint32_t i = 0; i < bytes; i++)
        {
            frame.data[i + 1] = payload.payloadPtr[sentData + i];
        }

        /* Set payload length */
        frame.can_dlc = bytes + CAN_FRAGMENT_HEADER_SIZE;

        /* Check socket status */
        if(socketFd < 0)
            return CAN_ERROR_SEND;

        /* Send data */
        CAN_ERROR ret = sending(FTITLE, frame);
        if(ret != CAN_ERROR_NONE)
        {
            /* failed to write! */
            return ret;
        }

        /* Update Counters */
        sentData           += bytes;
        payload.payloadLen -= bytes;

        /* Little delay between fragments to free some bandwidth */
        lucy_usleep(50);//MG
    }
    return CAN_ERROR_NONE;  //successful write of all the message fragments
}



CAN_ERROR CANFraming::receive(const char* title, struct can_frame& frame, struct timeval *timeout)
{
    lu_int32_t nbytes;

    /* Try to read a message */
    if (timeout != NULL)
    {
        /* Check if there is something to read first */
        fd_set rfds;
        struct timeval timeoutLocal;
        lu_int32_t retval;

        FD_ZERO(&rfds);

        /* Check socket status */
        if(socketFd < 0)
            return CAN_ERROR_RECV;

        FD_SET(socketFd, &rfds);

        /* select may change the value of timeout. Use a local copy */
        timeoutLocal = *timeout;
        retval = select(socketFd+1, &rfds, NULL, NULL, &timeoutLocal);

        /* check select error */
        if (retval == -1)
        {
            log.error("%s select() error %i on interface %s.", title, errno, iName);
            return CAN_ERROR_RECV;
        }
        else if (retval == 0)
        {
            return CAN_ERROR_TIMEOUT;   /* Timeout */
        }
    }

    nbytes = recv(socketFd, &frame, sizeof(struct can_frame), 0);
    if (nbytes < 0)
    {
        /* Error receiving frame */
        if(errno != ENETDOWN)
        {
            log.warn("%s recv error %i (%s) in %s interface, socket %i.",
                        title, errno, strerror(errno), iName, socketFd
                        );
        }
        else
        {
            //When interface is down, it does not produce any error message
            return CAN_ERROR_DOWN;
        }
    }
    else
    {
        LogRAWMessage(LU_FALSE, frame);

        /* Check received frame validity */
        if (nbytes < static_cast<lu_int32_t>(sizeof(struct can_frame)))
        {
            log.error("%s Error in %s interface - received incomplete CAN frame (%i of %u).",
                            title, iName, nbytes, sizeof(struct can_frame)
                            );
            return CAN_ERROR_INVALID_FRAME;
        }

        return CAN_ERROR_NONE;  //Complete: received OK
    }
    return CAN_ERROR_RECV;  //recv failed all the retries
}


CAN_ERROR CANFraming::sending(const char* title, struct can_frame& frame)
{
    lu_int32_t nbytes;

    /* Try to write a message */
    nbytes = send(socketFd, &frame, sizeof(struct can_frame), 0);
    if(nbytes > 0)
    {
        /* successful write */
        LogRAWMessage(LU_TRUE, frame);  //log when successful write

        sendErrCounter = 0;
        return CAN_ERROR_NONE;
    }
    else
    {
        // Error changed, reset error counter.
        if(errno != sendErr)
        {
            sendErrCounter = 0;
            sendErr = errno;
        }

        // Logging
        if(sendErrCounter < SEND_ERR_THRESHOLD)
        {
            log.error("%s send error %i (%s) in %s (socket %i).",
                    title, errno, strerror(errno), iName, socketFd);

            sendErrCounter++;

            if(sendErrCounter == SEND_ERR_THRESHOLD)
            {
                log.error("%s send error reached threshold: %i.",
                                title, SEND_ERR_THRESHOLD);
            }
        }

        return errno == ENETDOWN ? CAN_ERROR_DOWN: CAN_ERROR_SEND;
    }
}


void CANFraming::LogRAWMessage(const lu_bool_t Tx, struct can_frame& frame)
{
    /* Log RAW message */
    if(log.isDebugEnabled() == LU_TRUE)
    {
        CANHeaderStr* CANHeader = (CANHeaderStr*)(&frame.can_id);
        std::ostringstream ss;
        if ((frame.can_id & CAN_EFF_FLAG) && (CANHeader->fragment == LU_TRUE))
        {
            /* Fragmented message */
            CANFragmentHeaderStr* CANFragHeader = (CANFragmentHeaderStr*)(&frame.data[0]);

            ss << " --- Frag=" << std::setw(2) << std::setfill('0') << static_cast<lu_uint32_t>(CANFragHeader->id)
                << " s=" << static_cast<lu_uint32_t>(CANFragHeader->start)
                << " e=" << static_cast<lu_uint32_t>(CANFragHeader->stop);
        }
        MODULE_MSG_TYPE msgType = static_cast<MODULE_MSG_TYPE>(CANHeader->messageType);
        MODULE_MSG_ID msgID = static_cast<MODULE_MSG_ID>(CANHeader->messageID);
        if(log.isDebugEnabled() == LU_TRUE)
        {
            log.debug("%s %s|%s:%i->%s:%i "
                        "%s %s %s Msg: 0x%02x:0x%02x %s%s",
                        iName,
                        (Tx == LU_TRUE)? "Tx" : "Rx",
                        MODULE_ToSTRING(CANHeader->deviceSrc), CANHeader->deviceIDSrc,
                        MODULE_ToSTRING(CANHeader->deviceDst), CANHeader->deviceIDDst,
                        to_hex_string(frame.can_id & CAN_EFF_MASK).c_str(),
                        to_hex_string(frame.can_dlc).c_str(),
                        to_hex_string(frame.data, frame.can_dlc).c_str(),
                        msgType, msgID,
                        getCANMessageName(msgType, msgID),
                        ss.str().c_str()
                        );
        }
    }
}


/*
 *********************** End of file ******************************************
 */
