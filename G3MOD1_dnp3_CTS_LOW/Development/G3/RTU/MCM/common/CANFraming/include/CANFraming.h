/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_363726BC_D066_4e5d_AB4D_CA14AD902F61__INCLUDED_)
#define EA_363726BC_D066_4e5d_AB4D_CA14AD902F61__INCLUDED_


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <net/if.h>
#include <linux/can.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Mutex.h"
#include "Logger.h"
#include "ModuleMessage.h"
#include "RAWModuleMessage.h"
#include "Table.h"
#include "CANInterface.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef Table<struct can_filter> CanFilterTable;    //Filter configuration

typedef enum
{
    CAN_ERROR_NONE      = 0,
    CAN_ERROR_INIT         ,
    CAN_ERROR_PARAM        ,
    CAN_ERROR_TOO_LONG     ,
    CAN_ERROR_SEND         ,
    CAN_ERROR_RECV         ,
    CAN_ERROR_INVALID_FRAME,
    CAN_ERROR_ERR_FRAME    ,
    CAN_ERROR_DECODER      ,
    CAN_ERROR_MEM          ,
    CAN_ERROR_TIMEOUT      ,
    CAN_ERROR_INCOMPLETE   ,
    CAN_ERROR_DOWN         ,    //Interface is down
    CAN_ERROR_LAST
}CAN_ERROR;



/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class CANFragmentDecoder
{
public:
    CANFragmentDecoder();
    ~CANFragmentDecoder() {};

    /**
     * \brief Add a new fragment to the decoder
     *
     * \param frame New fragment
     * \param msg Final message
     *
     * \return Error code
     */
    CAN_ERROR addFragment(struct can_frame *frame, ModuleMessage **msg);

    /**
     *\brief Reset decoder
     */
    void reset();

private:
    /**
     * \brief Decoder internal status
     */
    typedef enum
    {
        DECODER_STATUS_IDLE    = 0,
        DECODER_STATUS_RUNNING    ,
        DECODER_STATUS_LAST
    }DECODER_STATUS;

private:
    DECODER_STATUS status;          //internal status of the CAN Decoder
    RAWLargeModuleMessage *message;
    lu_uint8_t firstFragmentID;
    lu_uint32_t expectedHeader;
    lu_uint32_t maxFragmentOffset;
    lu_uint8_t receivedFragments;
    lu_uint8_t expectedFragments;
    Logger& log;

private:
    /**
     * \brief Return the relative (to the first frame) fragment position
     *
     * \param currentID The frameID
     *
     * \return Relative fragment position
     */
    lu_uint8_t getFragmentPos(lu_uint8_t currentID);

    /**
     * \brief Return the fragment offset within the final message payload
     *
     * \param currentID The frameID
     *
     * \return Fragment offset
     */
    lu_uint32_t getPayloadOffset(lu_uint8_t currentID);
};


/**
 * \brief Object Oriented wrapper for the socketCAN interface
 * 
 * The G3 "low level" framing is also handled:
 * - Addressing (through the extended ID)
 * - Fragmentation
 */
class CANFraming
#if POLLING_CAN_INTERFACE_STATE
                : private CANIfaceObserver
#endif
{
public:
    /**
     * \brief Create a new socket connected to the specified can interface.
     *
     * The receive filter is reset: no messages are received. In order to receive
     * messages set a valid filter using the setFilter function
     *
     * \param ifaceID Interface type
     * \param enableLoopBack If LU_TRUE the loopBack for the socket is enabled
     *
     * \return None
     */
    CANFraming(COMM_INTERFACE ifaceID, lu_bool_t enableLoopBack);

    virtual ~CANFraming();

    /**
     * /brief Load a set of filters
     *
     * If the filter table is NULL the socket filter table is reset
     *
     * \param filter Filter table
     *
     * \return Error code
     */
    CAN_ERROR setFilter(CanFilterTable &filter);

    /**
     * \brief Read a message form the can interface.
     *
     * Only messages that conforms to the filters are returned.
     *
     * \param message Pointer to a message to be filled out by the incoming one.
     * \param timeout Read timeout; if NULL, it waits indefinitely for a message.
     *
     * \return Pointer to a message. NULL if there are no full messages available
     */
    CAN_ERROR read(ModuleMessage **message, struct timeval *timeout);

    /**
     * \brief Send a CAN message
     *
     * \param msgPtr Pointer to the message to write
     * \param retries the times of retrying if writing message fails.
     *
     * \return Error code
     */
    CAN_ERROR write(ModuleMessage *msgPtr, const lu_uint16_t retries = 0);

    /**
     * \brief Return the socket FD
     *
     * \return Socket FD. Value of -1 if not active (not created/interface is down).
     */
    lu_int32_t getFD();

    /**
     * \brief Return socket interface
     *
     * \return Socket interface
     */
    COMM_INTERFACE getIface() {return iface.getID();}

    const lu_char_t* getName() {return iface.getName();}

    lu_bool_t isActive()
    {
#if POLLING_CAN_INTERFACE_STATE
       return ((socketInit == LU_TRUE)
                && (iface.getStatus() == CANInterface::IFSTATUS_UP)
                && (iface.isEnabled() == LU_TRUE)
               ) ? LU_TRUE : LU_FALSE;
#else
       return ((socketInit == LU_TRUE)
                   && (iface.isEnabled() == LU_TRUE)
                  ) ? LU_TRUE : LU_FALSE;
#endif
    }

private:

#if POLLING_CAN_INTERFACE_STATE
    /**
     * Inherited from CANIfaceObserver
     */
    virtual void update(CANInterface::IFSTATUS status);
#endif

    /**
     * \brief Initialise CAN Framing socket
     *
     * This creates a new socket, configures it, and binds it to the interface
     *
     * \return Error code
     */
    CAN_ERROR initSocket();

    /**
     * \brief Close CAN Framing socket
     *
     * After closing the socket, the File Descriptor will be invalidated (-1)
     */
    void closeSocket();

    /**
     * \brief Send a message using socket CAN
     *
     * \param msgPtr Pointer to the message to send
     *
     * \return Error code
     */
    CAN_ERROR sendMessage(ModuleMessage *msgPtr);

    /**
     * \brief Send a fragmented message using socket CAN
     *
     * \param msgPtr Pointer to the message to send
     *
     * \return Error code
     */
    CAN_ERROR sendFragmentedMessage(ModuleMessage *msgPtr);

    /**
     * \brief Receive a message from socket CAN, retrying when needed
     *
     * \param title Title to show in log messages
     * \param buffer Pointer to the message to receive
     * \param timeout Read timeout; if NULL, it waits indefinitely for a message
     *
     * \return Error code
     */
    CAN_ERROR receive(const char* title, struct can_frame& frame, struct timeval *timeout);

    /**
     * \brief Send a message to socket CAN, retrying when needed
     *
     * \param title Title to show in log messages
     * \param buffer Pointer to the message to send
     *
     * \return Error code
     */
    CAN_ERROR sending(const char* title, struct can_frame& frame);

    /**
     * \brief Log a CAN RAW message, with some content details
     *
     * \param Tx Direction of the message: set to true for outgoing messages
     * \param frame CAN frame content to print out
     *
     * \return Associated Error code
     */
    void LogRAWMessage(const lu_bool_t Tx, struct can_frame& frame);

private:
    lu_bool_t socketInit;       //status of the socket
    COMM_INTERFACE ifaceID;     //Associated interface ID
    CANInterface& iface;    //Reference to the associated CAN interface
    const lu_char_t* iName;     //Interface Name (can0, can1, ...)
    lu_int32_t socketFd;        //Socket file descriptor
    lu_bool_t loopBack;         //Socket loopback configuration
    struct sockaddr_can address;    //socket configuration
    CanFilterTable filterTable;     //socket filter configuration
    CANFragmentDecoder fragmentDecoder; //Fragmented message handler

    Mutex readMutex;    //Protects read operation and prevents use of same fragmentDecoder
    Mutex writeMutex;   //Protects write operation and its fellows fragmentCounterTX and errCounter
    Logger& log;

    lu_uint32_t sendErr;
    lu_uint8_t  sendErrCounter;
};


#endif // !defined(EA_363726BC_D066_4e5d_AB4D_CA14AD902F61__INCLUDED_)

/*
 *********************** End of file ******************************************
 */




