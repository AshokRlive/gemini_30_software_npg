/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: CANInterface.h 11 Nov 2013 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/common/CANFraming/include/CANInterface.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 11 Nov 2013 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11 Nov 2013   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef CANINTERFACE_H__INCLUDED
#define CANINTERFACE_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <libsocketcan.h>
#include <net/if.h>
#include <linux/can.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "timeOperations.h"
#include "Mutex.h"
#include "Timer.h"
#include "Logger.h"
#include "CANProtocol.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define POLLING_CAN_INTERFACE_STATE 0


/* Define AF_CAN and PF_CAN if not yet defined in libc */
#ifndef AF_CAN
#define AF_CAN 29
#endif

#ifndef PF_CAN
#define PF_CAN AF_CAN
#endif

/* Device names for the interfaces */
#define CAN0 "can0"
#define CAN1 "can1"
#define CAN_UNKNOWN "unknown"

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

#if POLLING_CAN_INTERFACE_STATE
/* Forward declarations */
class CANIfaceObserver;
class CANStatusTimer;
#endif

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Keeps the status of CAN Interfaces (devices)
 *
 * This is a factory of singletons class that keeps the status of each interface
 * (one singleton per interface to keep track of the status).
 */
class CANInterface
{
public:
    /**
     * Error codes for return
     */
    typedef enum
    {
        CANIFACE_ERROR_NONE      = 0,
        CANIFACE_ERROR_INIT       ,   //Interface has failed to initialise
        CANIFACE_ERROR_UP         ,   //Interface has failed to be brought up
        CANIFACE_ERROR_DOWN       ,   //Interface has failed to be brought down
        CANIFACE_ERROR_LAST
    }CANIFACE_ERROR;

#if POLLING_CAN_INTERFACE_STATE
    /**
     * Interface status codes
     */
    typedef enum
    {
        IFSTATUS_UP,
        IFSTATUS_DOWN,
        IFSTATUS_ERROR_FATAL,   //Unrecoverable error: we gave up trying to recover
        IFSTATUS_ERROR_INIT     //Error setting interface
    } IFSTATUS;
#endif


    /**
      * \brief Factory method: return the appropriate Interface.
      *
      * \param subSystem The associated subsystem for the desired Logger
      *
      * \return Pointer to a Logger
      */
    static CANInterface& getInstance(const COMM_INTERFACE iface);

    /**
      * \brief return the Interface name.
      *
      * \return Pointer to a string containing the name of the interface
      */
    const lu_char_t* getName();

    COMM_INTERFACE getID(){ return ifaceID;}

    void setEnabled(lu_bool_t enabled)
    {
        this->enabled = enabled;
    }

    lu_bool_t isEnabled()
    {
        return enabled;
    }


    /**
     * \brief Get the reference of the interface definition
     *
     * Use this method for associate (bind) a socket with this interface.
     * Example:
     *      struct sockaddr_can& canAddr = canInterface.get();
     *      bind(mysocket, &canAddr, sizeof(canAddr));
     *
     * \return Socket structure with the interface definition
     */
    const struct sockaddr_can& getSocketAddress();

#if POLLING_CAN_INTERFACE_STATE
    /**
     * \brief Register (attach) an observer over an specific interface
     *
     * \param observer Reference of the observer to register
     *
     * \return Success or error
     */
    lu_bool_t attach(CANIfaceObserver& observer);

    /**
     * \brief Unregister (detach) an observer over an specific interface
     *
     * \param observer Reference of the observer to register
     */
    void detach(CANIfaceObserver& observer);

    /**
     * \brief Bring up the CAN interface
     *
     * \return Error code
     */
    CANIFACE_ERROR up();

    /**
     * \brief Bring down the CAN interface
     *
     * \return Error code
     */
    CANIFACE_ERROR down();

    /*
     * Get the current status of this interface
     */
    CANInterface::IFSTATUS getStatus(){ return status;}

    friend class CANStatusTimer;


protected:
    /**
     * \brief Update the interface status.
     */
    void updateStatus();
#endif


private:
    /**
     * \brief private default constructor.
     */
    CANInterface();

    /**
     * \brief private custom constructor.
     *
     * \param interface Interface type
     */

    CANInterface(const COMM_INTERFACE interface);

    /*
     * \brief private copy constructor to prevent copy of this object.
     */
    CANInterface(const CANInterface &cani) :
                                canState(CAN_STATE_MAX),
                                ifaceID(ifaceID),
#if POLLING_CAN_INTERFACE_STATE
                                status(IFSTATUS_DOWN),
                                accessMutex(),
                                queueMutex(LU_FALSE),
                                timeLastError(TIMESPEC_ZERO),
                                fatalErrorCount(0),
#endif
                                log(Logger::getLogger(SUBSYSTEM_ID_CANFRAMING))
    { LU_UNUSED(cani); }

    virtual ~CANInterface();

    /**
     * \brief Creates and binds the associated CAN Framing socket
     *
     * \return Error code
     */
    CANInterface::CANIFACE_ERROR initSocket();


#if POLLING_CAN_INTERFACE_STATE
    /**
     * \brief Update the registered observers with a new interface status
     *
     * \param interfaceStatus Status of the interface to be reported
     */
    void updateObservers(IFSTATUS interfaceStatus);


    /**
     * \brief Brings the interface up/down
     *
     * \param status Where to bring the interface (up/down).
     *
     * \return Error code
     */
    CANInterface::CANIFACE_ERROR bringIface(IFSTATUS status);

    /**
     * \brief log a change in the interface status
     *
     *  It does not log a message if the interface is already down or the state
     *  did not change.
     *
     * \param stateCAN New state reported by the CAN interface.
     *
     * \return Error code
     */
    void logCANState(const lu_char_t* title, const can_state stateCAN);

    /**
     * \brief Factory method: convert can_state to IFSTATUS enum, simplifying states
     *
     * \param state CAN state of the interface
     *
     * \return Converted state to Up/Down/Error
     */
    static CANInterface::IFSTATUS convertCANState(can_state state);
#endif


    /**
    * \brief Instantiate all CANInterface objects in a pool.
    */
    static void initObjectsPool();

    can_state canState;             //Current CAN state
    const COMM_INTERFACE ifaceID;   //Interface ID
    lu_char_t iName[IFNAMSIZ];      //Interface Name (can0, can1, ...)
    struct sockaddr_can address;    //associated socket configuration

#if POLLING_CAN_INTERFACE_STATE
    IFSTATUS status;                //Current status of this interface
    MasterMutex accessMutex;        //Prevents other operations while operating the interface
    QueueMgr<CANIfaceObserver> observers; //Observers Queue
    MasterMutex queueMutex;         //Mutex to protect Observers Queue
    struct timespec timeLastError;  //Time stamp of the last time we checked for interface status unsuccessfully
    lu_uint32_t fatalErrorCount;    //Amount of fatal errors with no recovering
#endif

    Logger& log;
    lu_bool_t enabled; //Interface enabled
};

#if POLLING_CAN_INTERFACE_STATE
/**
 * Class to keep a list of observers for each interface
 */
class CANIfaceObserver: public QueueObj<CANIfaceObserver>
{
public:
    CANIfaceObserver() {};
    virtual ~CANIfaceObserver() {};
    virtual void update(CANInterface::IFSTATUS status) = 0;
};


/**
 * Class in charge of update the status of all the registered interfaces
 */
class CANStatusTimer : public ITimerHandler
{
    /*
     * Handle timeout event.
     */
    void handleAlarmEvent(Timer &source);
};
#endif

#endif /* CANINTERFACE_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
