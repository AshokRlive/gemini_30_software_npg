////////////////////////////////////////////////////////////////////////
// Rollover function
////////////////////////////////////////////////////////////////////////

function Rollover(nameimg, namedom)
{

	document.images[namedom].src = nameimg;
	return true;	
	
}

////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
// Dropdown function
////////////////////////////////////////////////////////////////////////

function ExpandDiv(theDivName)
{
	InitializeGlobalData();

	if (null == theDivName || typeof(theDivName) == "undefined") return; var theDiv = allDivsInPage[theDivName]; if (null == theDiv || typeof(theDiv) == "undefined") return;
	theDiv.style.display = "block";

	var thePic = allImagesInPage[theDivName + "_img"];
	if (null != thePic && typeof(thePic) != "undefined")
		{
		thePic.src = thePic.src.replace("blueup_CLV.gif", "bluedrop_CLV.gif");
		}
}

function CollapseDiv(theDivName)
{
	InitializeGlobalData();

	if (null == theDivName || typeof(theDivName) == "undefined") return; var theDiv = allDivsInPage[theDivName]; if (null == theDiv || typeof(theDiv) == "undefined") return;
	theDiv.style.display = "none";

	var thePic = allImagesInPage[theDivName + "_img"];
	if (null != thePic && typeof(thePic) != "undefined")
		{
		//earlier it was like this...
		//thePic.src = "plus" + strIsRtl + "";	
		thePic.src = thePic.src.replace("bluedrop_CLV.gif", "blueup_CLV.gif");
		}
}

function ToggleDiv(theDivName)
{
	InitializeGlobalData();

	if (null == theDivName || typeof(theDivName) == "undefined") return; var theDiv = allDivsInPage[theDivName]; if (null == theDiv || typeof(theDiv) == "undefined") return;

	if (theDiv.style.display.toUpperCase() != "BLOCK")
		ExpandDiv(theDivName);
	else
		CollapseDiv(theDivName);
}

function AlterAllDivs(displayStyle)
{
	InitializeGlobalData();

	if (null == allDivsInPage || typeof(allDivsInPage) == "undefined")
		return;

	if (typeof(allDivsInPage["divShowAll"]) != "undefined" &&
		typeof(allDivsInPage["divHideAll"]) != "undefined")
		{
		if (displayStyle == "block")
			{
			allDivsInPage["divShowAll"].style.display = "none";
			allDivsInPage["divHideAll"].style.display = "block";
			}
		else
			{
			allDivsInPage["divShowAll"].style.display = "block";
			allDivsInPage["divHideAll"].style.display = "none";
			}
		}

	AlterAllDivsSpans(document.body.getElementsByTagName("DIV"), displayStyle);
	AlterAllDivsSpans(document.body.getElementsByTagName("SPAN"), displayStyle);
}

function AlterAllDivsSpans(allDivsSpans, displayStyle)
{
	if (typeof(allDivsSpans) == "undefined" ||
		null == allDivsSpans)
		return;

	for (i=0; i < allDivsSpans.length; i++)
		if (typeof(allDivsSpans[i]) != "undefined" &&
			null != allDivsSpans[i] &&
			typeof(allDivsSpans[i].id) != "undefined" &&
			null != allDivsSpans[i].id &&
			allDivsSpans[i].id.length > 0)
			{
			if (0 == allDivsSpans[i].id.indexOf("divExpCollAsst_")) 
				{
				var thePic = allImagesInPage[allDivsSpans[i].id + "_img"];

				if (displayStyle == "block")
					{
					allDivsSpans[i].style.display = "block";

					if (typeof(thePic) != "undefined" && null != thePic)
						{
						thePic.src = thePic.src.replace("blueup_CLV.gif", "bluedrop_CLV.gif");
						}
					}
				else
					{
					allDivsSpans[i].style.display = "none";

					if (typeof(thePic) != "undefined" && null != thePic)
						{
						//thePic.src = "plus" + strIsRtl + ".gif";
						thePic.src = thePic.src.replace("blueup_CLV.gif", "bluedrop_CLV.gif");
						}
					}
				}

			if (0 == allDivsSpans[i].id.indexOf("divInlineDef_")) 
				if (displayStyle == "block")
					allDivsSpans[i].style.display = "inline";
				else
					allDivsSpans[i].style.display = "none";
			}
}

function ToggleAllDivs()
{
	InitializeGlobalData();

	if (fExpandedAssistance)
		AlterAllDivs("none");
	else
		AlterAllDivs("block");

	fExpandedAssistance = !fExpandedAssistance;
}

function InitializeGlobalData()
{
	if ('undefined' != typeof(strRtl))
		strIsRtl = strRtl;

	var divs = document.body.getElementsByTagName("DIV");
	var spans = document.body.getElementsByTagName("SPAN");

	var countDiv = 0;
	var countSpan = 0;
	if (typeof(divs) != "undefined" && null != divs)
		countDiv = divs.length;

	if (typeof(spans) != "undefined" && null != spans)
		countSpan = spans.length;

	allDivsInPage = new Array();
	for (i=0; i < countDiv; i++)
		if (typeof(divs[i].id) != "undefined" &&
			null != divs[i].id &&
			divs[i].id.length > 0)
			allDivsInPage[divs[i].id] = divs[i];

	for (i=0; i < countSpan; i++)
		if (typeof(spans[i].id) != "undefined" &&
			null != spans[i].id &&
			spans[i].id.length > 0)
			allDivsInPage[spans[i].id] = spans[i];

	allImagesInPage = document.body.getElementsByTagName("IMG");
}
