#include <embUnit/embUnit.h>

static void setUp(void)
{
}

static void tearDown(void)
{
}

static void testInit(void)
{
}

static void test1(void)
{
	TEST_ASSERT_EQUAL_INT(1, 1);
}

TestRef CANIOModuleTest_tests(void)
{
	EMB_UNIT_TESTFIXTURES(fixtures) {
		new_TestFixture("testInit",testInit),
		new_TestFixture("test1",test1),
	};
	EMB_UNIT_TESTCALLER(CANIOModuleTest,"CANIOModuleTest",setUp,tearDown,fixtures);

	return (TestRef)&CANIOModuleTest;
}
