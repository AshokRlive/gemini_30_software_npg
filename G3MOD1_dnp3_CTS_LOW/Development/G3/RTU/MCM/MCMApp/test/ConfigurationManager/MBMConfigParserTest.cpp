#include <embUnit/embUnit.h>
#include "XMLModbusConfigParser.h"
#include "MBDebug.h"

#define CONFIG_FILE "MBMConfigParserTest.xml"

// The device number in your XML
#define XML_DEVICE_NUM 2
#define CHANNEL_TYPE_NUM 3


static int channelNums[XML_DEVICE_NUM][CHANNEL_TYPE_NUM] =
{
    /*Number of AI, DI, DO*/
    {3,2,1},
    {1,1,1}
};

static CHANNEL_TYPE channelTypes[CHANNEL_TYPE_NUM] =
{
    CHANNEL_TYPE_AINPUT,
    CHANNEL_TYPE_DINPUT,
    CHANNEL_TYPE_DOUTPUT,
};



g3schema::Cg3schema* configuration = NULL;
XMLModbusConfigParser* fixture;
static PortManager portMgr;

static void setUp(void)
{
    try
       {
           /* Initialise XML xercesc parser */
           xercesc::XMLPlatformUtils::Initialize();

           /* Load the configuration file */
           configuration = new Cg3schema(Cg3schema::LoadFromFile(CONFIG_FILE));
       }
       catch (...)
       {
           DBG_ERR("Fail to open configuration file:%s",CONFIG_FILE);
           TEST_FAIL("Failed");
       }

       fixture = new XMLModbusConfigParser(*configuration, portMgr);
}

static void tearDown(void)
{
    delete configuration;
    delete fixture;
    configuration = NULL;
}

static void testInit(void)
{
    PRINT_SEPERATOR;

    TEST_ASSERT_NOT_NULL(configuration);
    TEST_ASSERT_NOT_NULL(fixture);

}

static void testConfParser(void)
{
    PRINT_SEPERATOR;

    lu_uint16_t devNum;
    devNum = fixture->getDeviceNum();

    // Check device num
    TEST_ASSERT_EQUAL_INT(XML_DEVICE_NUM, devNum);

    // Check channel num
    for (int i = 0; i < XML_DEVICE_NUM; ++i) {
        for (int j = 0; j < CHANNEL_TYPE_NUM; ++j) {
            lu_int32_t chlNum = fixture->getChannelNum(i, channelTypes[j]);
            TEST_ASSERT_EQUAL_INT(channelNums[i][j], chlNum);
        }
    }


    // Print device conf
    PRINT_SEPERATOR;
    mbm::DeviceConf devconf;
    for (int i = 0; i < devNum; ++i) {
        fixture->getDeviceConf(i, &devconf);
        DBG_INFO("%s",devconf.toString().c_str());
    }

    // Print channel conf
    PRINT_SEPERATOR;

    mbm::ChannelConf chconf;
    mbm::ChannelRef chref;
    for (int i = 0; i < XML_DEVICE_NUM; ++i) {
        for (int j = 0; j < CHANNEL_TYPE_NUM; ++j) {
            lu_int32_t chlNum = fixture->getChannelNum(i, channelTypes[j]);
            for (int k = 0; k < chlNum; ++k) {
                chref.channelId = k;
                chref.channelType = channelTypes[i];
                chref.device.deviceId = i;
                fixture->getChannelConf(chref, &chconf);
                DBG_INFO("%s",chconf.toString().c_str());
            }
        }
    }
}


#if 0
static void testGetDeviceConf(void)
{
    PRINT_SEPERATOR;

    mbm::DeviceConf devConf;


    fixture->getDeviceConf(0, &devConf);
    TEST_ASSERT_EQUAL_INT(1,    devConf.ref.deviceType); // MBDevice Type
    TEST_ASSERT_EQUAL_INT(0,    devConf.ref.deviceId);   // MBDevice Type
    TEST_ASSERT_EQUAL_STRING("channel-tcp",    devConf.conf.tcpDevConf.tcpconf.chnlName);

    fixture->getDeviceConf(1, &devConf);
    TEST_ASSERT_EQUAL_INT(0,    devConf.ref.deviceType); // MBDevice Type
    TEST_ASSERT_EQUAL_INT(1,    devConf.ref.deviceId);   // MBDevice Type
    TEST_ASSERT_EQUAL_STRING("channel-serial", devConf.conf.serialDevConf.serialconf.chnlName);

}

static void testGetChannelNum(void)
{

    PRINT_SEPERATOR;

    lu_uint16_t chNum;


    chNum = fixture->getChannelNum(0, CHANNEL_TYPE_AINPUT);
    TEST_ASSERT_EQUAL_INT(DEV0_AI_NUM, chNum);

    chNum = fixture->getChannelNum(0, CHANNEL_TYPE_DINPUT);
    TEST_ASSERT_EQUAL_INT(DEV0_DI_NUM, chNum);

    chNum = fixture->getChannelNum(1, CHANNEL_TYPE_AINPUT);
    TEST_ASSERT_EQUAL_INT(DEV1_AI_NUM, chNum);

    chNum = fixture->getChannelNum(1, CHANNEL_TYPE_DINPUT);
    TEST_ASSERT_EQUAL_INT(DEV1_DI_NUM, chNum);
}

static void testGetChanelConf(void)
{
    PRINT_SEPERATOR;

    mbm::ChannelConf chconf;
    mbm::ChannelRef chref;
    chref.channelId = 0;
    chref.channelType = CHANNEL_TYPE_AINPUT;
    chref.device.deviceId = 1;
    chref.device.deviceType = mbm::DEVICE_TYPE_SERIAL;

    fixture->getChannelConf(chref, &chconf);


    // <AIChs channelID="0" dataType="0" description="Holding Register 9000"
    // pollRate="600" regAddress="9000" regType="3" valueMask="0"/>

    TEST_ASSERT_EQUAL_INT(chref.channelId,          chconf.channel.channelId);
    TEST_ASSERT_EQUAL_INT(chref.channelType,        chconf.channel.channelType);
    TEST_ASSERT_EQUAL_INT(chref.device.deviceId,    chconf.channel.device.deviceId);
    TEST_ASSERT_EQUAL_INT(chref.device.deviceType,  chconf.channel.device.deviceType);

//    TEST_ASSERT_EQUAL_INT(0,    chconf.datatype);
//    TEST_ASSERT_EQUAL_INT(600,  chconf.pollingRate);
//    TEST_ASSERT_EQUAL_INT(9000, chconf.reg);
//    TEST_ASSERT_EQUAL_INT(3,    chconf.regType);
    DBG_INFO("%s",chconf.toString().c_str());
}

#endif

/* Test Suite*/
TestRef MBMConfigParserTest_tests(void)
{
    EMB_UNIT_TESTFIXTURES(fixtures) {
        /* Test Cases*/
        new_TestFixture("testInit",         testInit),
        new_TestFixture("testConfParser",   testConfParser),
//        new_TestFixture("testGetDeviceConf", testGetDeviceConf),
//        new_TestFixture("testGetChannelNum", testGetChannelNum),
//        new_TestFixture("testGetChanelConf", testGetChanelConf),
    };
    EMB_UNIT_TESTCALLER(MBMConfigParserTest_tests,"MBMConfigParserTest_tests",setUp,tearDown,fixtures);

    return (TestRef)&MBMConfigParserTest_tests;;
}
