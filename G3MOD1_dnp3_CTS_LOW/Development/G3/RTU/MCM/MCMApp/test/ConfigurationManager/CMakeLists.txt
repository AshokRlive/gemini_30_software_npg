cmake_minimum_required(VERSION 2.8)

#project (MemoryManagerTest)


add_library( ConfigurationManagerTest STATIC
			MBMConfigParserTest.cpp
           ) 

# List all the source files needed to build the executable
#add_executable(${EXE_NAME} MemoryManagerTest.cpp) 

# List all the library needed by the linker
#target_link_libraries (MemoryManagerTest embunit)
#target_link_libraries (MemoryManagerTest MemoryManager)
#target_link_libraries (MemoryManagerTest BaseReuse)
#target_link_libraries (MemoryManagerTest Syslog)



# Generate binary file from executable file
#generate_bin()

# Generate Doxygen documentation
# gen_doxygen("main" "")

