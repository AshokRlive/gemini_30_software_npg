#include <embUnit/embUnit.h>
#include "MBDevice.h"
#include "MBDebug.h"

static mbm::MBDevice* fixture;

static void setUp(void)
{
    fixture = new mbm::MBDevice(MODULE_ID_1);
}

static void tearDown(void)
{
    delete fixture;
}

/* Test Case*/
static void testSetConfig(void)
{
    mbm::DeviceConf conf;
    conf.ref.deviceId = 123;
    conf.ref.deviceType = 2;
    fixture->setConfig(conf);

    mbm::DeviceConf* m_conf = fixture->getConfig();

    TEST_ASSERT_NOT_NULL(m_conf);
    TEST_ASSERT_EQUAL_INT(123, m_conf->ref.deviceId);
    TEST_ASSERT_EQUAL_INT(2, m_conf->ref.deviceType);
}

static void testGetChannel(void)
{
    const int ainum = 10;
    const int dinum = 15;


    /*Init channels*/
    mbm::MBChannel** achs = new mbm::MBChannel* [ainum];
    for (int i = 0; i < ainum; ++i) {
        achs[i] = new mbm::MBChannel(CHANNEL_TYPE_AINPUT,i);
    }
    mbm::MBChannel** dchs = new mbm::MBChannel* [dinum];
    for (int i = 0; i < dinum; ++i) {
        dchs[i] = new mbm::MBChannel(CHANNEL_TYPE_DINPUT,i);
    }
    fixture->setChannels(CHANNEL_TYPE_AINPUT, achs,ainum);
    fixture->setChannels(CHANNEL_TYPE_DINPUT,dchs,dinum);


    TEST_ASSERT_EQUAL_INT(ainum, fixture->getChannelNum(CHANNEL_TYPE_AINPUT));
    TEST_ASSERT_EQUAL_INT(dinum, fixture->getChannelNum(CHANNEL_TYPE_DINPUT));


    IChannel* ch;
    for (int i = 0; i < ainum; ++i) {
        ch = fixture->getChannel(CHANNEL_TYPE_AINPUT,i);
        TEST_ASSERT_NOT_NULL(ch);
        TEST_ASSERT_EQUAL_INT(i, ch->getID().id);
        TEST_ASSERT_EQUAL_INT(CHANNEL_TYPE_AINPUT, ch->getID().type);
    }

    for (int i = 0; i < dinum; ++i) {
        ch = fixture->getChannel(CHANNEL_TYPE_DINPUT,i);
        TEST_ASSERT_NOT_NULL(ch);
        TEST_ASSERT_EQUAL_INT(i, ch->getID().id);
        TEST_ASSERT_EQUAL_INT(CHANNEL_TYPE_DINPUT, ch->getID().type);
    }

    TEST_ASSERT_NULL(fixture->getChannel(CHANNEL_TYPE_AINPUT,ainum));
    TEST_ASSERT_NULL(fixture->getChannel(CHANNEL_TYPE_DINPUT,dinum));

}

static void testHandleAEvent(void)
{
    // Handle event
    mbm::ChannelEvent aevent;
    aevent.channel.channelId = 0;
    aevent.channel.device.deviceId = 123;
    aevent.channel.device.deviceType = 2;
    fixture->handleEvent(aevent);
}


static void testDataConversion()
{
    lu_int32_t rawdata;

    rawdata = -1;

    // Convert to unsigned 32

    lu_float32_t udata;
    udata = *((lu_uint32_t*)&rawdata);
    DBG_INFO("unsigned: %f raw:%i",udata,rawdata);

}

/* Test Suite*/
TestRef MBDeviceTest_tests(void)
{
    EMB_UNIT_TESTFIXTURES(fixtures) {
        /* Test Cases*/
        new_TestFixture("testSetConfig",testSetConfig),
        new_TestFixture("testGetChannel",testGetChannel),
        new_TestFixture("testHandleAEvent",testHandleAEvent),
        new_TestFixture("testDataConversion",testDataConversion),
        // ... more test cases
    };
    EMB_UNIT_TESTCALLER(MBDeviceTest_tests,"MBDeviceTest_tests",setUp,tearDown,fixtures);

    return (TestRef)&MBDeviceTest_tests;;
}
