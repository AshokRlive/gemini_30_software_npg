extern "C"
{
#include <embUnit/embUnit.h>
#include <textui/TextOutputter.h>
#include <textui/TextUIRunner.h>
}

#include "Logger.h"
#include "mbm_helpers.h"

TestRef CANIOModuleTest_tests(void);
TestRef MemoryManagerTest_tests(void);
TestRef ModbusDeviceManagerTest_tests(void);
TestRef MBDeviceTest_tests(void);
TestRef MBMConfigParserTest_tests(void);
TestRef CommsDeviceManagerTest_tests(void);
TestRef DialupModemTest_tests(void);

int main(int argc, const char* argv[])
{
    blockAllExceptTerm(); //Block all sigs

    TextUIRunner_setOutputter (TextOutputter_outputter());
    TextUIRunner_start();
//    TextUIRunner_runTest(CANIOModuleTest_tests());
//	TextUIRunner_runTest(ModbusDeviceManagerTest_tests());
//    TextUIRunner_runTest(MBDeviceTest_tests());
//    TextUIRunner_runTest(DialupModemTest_tests());
    TextUIRunner_runTest(CommsDeviceManagerTest_tests());

    // This test requires Config file present
//    TextUIRunner_runTest(MBMConfigParserTest_tests());
    TextUIRunner_end();
    return 0;
}
