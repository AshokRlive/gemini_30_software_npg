#include <embUnit/embUnit.h>
#include "MemoryManager.h"
#include "IStatusManager.h"

// Stub
class StatusManager : public IStatusManager
{
public:
    StatusManager(lu_bool_t mcmDevBoard, lu_bool_t waitPSM){}
    ~StatusManager(){}
    void reboot()                                                                           {}
    void restart()                                                                          {}
    void shutdown()                                                                         {}
    MCM_ERROR commissionModules()                                                           {}
    MCM_ERROR factoryReset()                                                                {}
    void setServiceMode(SERVICEMODE serviceMode, OUTSERVICEREASON reason)                   {}
    void getServiceMode(SERVICEMODE &serviceMode, OUTSERVICEREASON &reason)                 {}
    void setUpgradeState(CTH_UPGRADESTATE upgState)                                         {}
    CTH_UPGRADESTATE getUpgradeState()                                                      {}
     void setLED(LEDS nameLED, LED value)                                                   {}
     void setAlarm(STATMAN_ALARM alarmCode)                                                 {}
     void setPowerMode(lu_bool_t powerMode)                                                 {}
     void doorEvent(lu_bool_t isClosed)                                                     {}
     void restartSlaveModules()                                                             {}
     void setSiteNameRTU(std::string siteName)                                              {}
     std::string getSiteNameRTU()		                                                    {}
     void setBackplaneSwitchCfg(lu_bool_t setUseBackplaneSwitch)                            {}
     lu_bool_t getBackplaneSwitchCfg()                                                      {}
     void updateOLRSwitchPosition(lu_uint32_t channelID, lu_uint8_t value)                  {}
     void setOLRSwitchPosition()                                                            {}
     OLR_STATE getOLRSwitchPosition()                                                       {}
     UserList::USERLIST_ERROR createUserList(lu_uint32_t listSize)                          {}
     UserList::USERLIST_ERROR userAdd(lu_uint32_t userIndex, UserLoginData user)            {}
     lu_int32_t userLogin(UserLoginData &user)                                              {}
     UserList::USERLIST_ERROR userFind(UserLoginData userData, lu_uint32_t &userIndex)      {}

    lu_uint32_t getAppUptime() {}
     std::string getConfigFileTimestamp() {}
     lu_bool_t getConfigFileVersion(BasicVersionStr *version) {}
     void setConfigFileTimestamp(std::string cfgFileTimestamp) {}
     void setConfigFileVersion(BasicVersionStr version) {}



};

MemoryManager *manager;
StatusManager *statusMgr;

static void setUp(void)
{
	statusMgr = new StatusManager(0,0);
	manager = new MemoryManager(statusMgr,
									SCHED_TYPE_FIFO,
										MEMORY_MANAGER_THREAD,
										MEMORY_MANAGER_SIGNAL);
}

static void tearDown(void)
{
	delete manager;
	delete statusMgr;
}

static void testInit(void)
{
	manager->init(IMemoryManager::MEM_TYPE_EVENT);
	printf("testInit done!\n");
}

static void testRead(void)
{
	manager->setSize(MemoryManager::STORAGE_VAR_EVENTLOG, 123);
//	TEST_ASSERT_EQUAL_INT(TEST_ASSERT_EQUAL_INT, manager->);
	 lu_uint32_t eventLogMaxEntries = 100;
	manager->read(IMemoryManager::STORAGE_VAR_EVENTLOG,
			(lu_uint8_t *)&eventLogMaxEntries,
						NULL);
	printf("Read entries: %d\n",eventLogMaxEntries);
	printf("testRead done!\n");
}

static void testWrite(void)
{
	SLEventEntryStr event;
	manager->write(IMemoryManager::STORAGE_VAR_EVENT, (lu_uint8_t *)&event);
	printf("testWrite done!\n");
}

TestRef MemoryManagerTest_tests(void)
{
	EMB_UNIT_TESTFIXTURES(fixtures) {
		new_TestFixture("testInit",testInit),
		new_TestFixture("testRead",testRead),
		new_TestFixture("testWrite",testWrite),
	};
	EMB_UNIT_TESTCALLER(MemoryManagerTest,"MemoryManagerTest",setUp,tearDown,fixtures);


	return (TestRef)&MemoryManagerTest;
}


//int main (int argc, const char* argv[])
//{
//	return 0;
//}



