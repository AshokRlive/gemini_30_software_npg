#include <embUnit/embUnit.h>
#include "DialupModem.h"
#include "Port.h"
#include "Debug.h"


class ModemObserver:public ICommsDeviceObserver
{
public:
    ModemObserver(){}
    ~ModemObserver(){}
    virtual void notifyConnectionState(bool connected, lu_int32_t commDevFD)
    {
        if(connected)
            DBG_INFO("Dialup Modem is now CONNECTED");
        else
            DBG_INFO("Dialup Modem is now CONNECTED");
    }
};

static DialupModem* fixture;
static ModemObserver observer;
static Port port;

static void setUp(void)
{
    DialupModem::DialupModemConfig config(port);
    fixture = new DialupModem(config);
    fixture->attach(&observer);
}

static void tearDown(void)
{
    delete fixture;
}

/* Test Case*/
static void testTickEvent()
{
    for (int i = 0; i < 10; ++i) {

        fixture->tickEvent(i);
        sleep(1);
    }
}


/* Test Suite*/
TestRef DialupModemTest_tests(void)
{
    EMB_UNIT_TESTFIXTURES(fixtures) {
        /* Test Cases*/
        new_TestFixture("testTickEvent",testTickEvent),
    };
    EMB_UNIT_TESTCALLER(DialupModemTest,"DialupModemTest",setUp,tearDown,fixtures);

    return (TestRef)&DialupModemTest;
}
