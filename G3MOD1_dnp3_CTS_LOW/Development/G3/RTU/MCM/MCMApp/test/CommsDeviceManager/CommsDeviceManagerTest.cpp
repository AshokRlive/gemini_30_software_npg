/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:Main.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   8 May 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdio.h>
#include <stdlib.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "CommsDeviceManager.h"
#include "CommsDeviceDebug.h"
#include <string.h>
#include <embUnit/embUnit.h>
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define MB_DUMP_EVENT 0

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

static CommsDeviceManager* fixture;

static void setUp(void)
{
    fixture = new CommsDeviceManager();
}

static void tearDown(void)
{
    delete fixture;
}

static void testAddCommsDevice()
{
    //TODO to be implemented
}

static void testStart()
{
    DBG_INFO("start device manager");

    fixture->startDeviceManger();

    sleep(5);
}


TestRef CommsDeviceManagerTest_tests(void)
{
    EMB_UNIT_TESTFIXTURES(fixtures) {
        new_TestFixture("testAddCommsDevice",testAddCommsDevice),
        //new_TestFixture("testStart",         testStart),
    };
    EMB_UNIT_TESTCALLER(CommsDeviceManagerTest,"CommsDeviceManagerTest",setUp,tearDown,fixtures);

    return (TestRef)&CommsDeviceManagerTest;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */



/*
 *********************** End of file ******************************************
 */
