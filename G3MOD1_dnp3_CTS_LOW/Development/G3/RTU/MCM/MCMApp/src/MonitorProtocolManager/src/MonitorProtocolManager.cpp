/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: c++_template.c 1469 2012-06-20 20:51:28Z fryers_j $
 *               $HeadURL: http://10.11.0.6:81/svn/gemini_30_software/trunk/Development/G3/common/template/c++_template.c $
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "GlobalDefs.h"
#include "MonitorProtocol.h"
#include "AbstractMonitorProtocolLayer.h"
#include "MonitorProtocolManager.h"
#include "IOModuleCommon.h"
#include "Logger.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
MonitorProtocolManager::MonitorProtocolManager(IMCMApplication& applicationMCM) :
                   AbstractMonitorProtocolLayer(APP_ID_MCMAPP, SCHED_TYPE_FIFO, MONITOR_PROTOCOL_THREAD),
                   mcmApplication(applicationMCM)
{
}

MonitorProtocolManager::~MonitorProtocolManager()
{
}


lu_int32_t MonitorProtocolManager::attach(MonitorObserver* observer)
{
    if(observer == NULL)
    {
        return -1;
    }

    // Protect the observer queue
    MasterLockingMutex mMutex(queueMutex, LU_TRUE);

    /* Add the observer to the observer list */
    if(observers.enqueue(observer) == LU_TRUE)
    {
        return 0;
    }
    else
    {
        return -2;
    }
}

void MonitorProtocolManager::detach(MonitorObserver* observer)
{
    if(observer == NULL)
    {
        return;
    }

    // Protect the observer queue
    MasterLockingMutex mMutex(queueMutex, LU_TRUE);

    /* Remove observer */
    observers.dequeue(observer);
}


void MonitorProtocolManager::stop()
{
    // Protect the observer queue
    MasterLockingMutex mMutex(queueMutex, LU_TRUE);

    /* Delete all observers */
    observers.empty();
}


void MonitorProtocolManager::notify_rep_ai_val(lu_uint8_t channel, lu_float32_t value, lu_uint8_t flags)
{
    /* Update observers */
    updateAIObservers(channel, value, flags);
}

void MonitorProtocolManager::notify_rep_di_val(lu_uint8_t channel, lu_bool_t value, lu_uint8_t flags)
{
    /* Update observers */
    updateDIObservers(channel, value, flags);
}

void MonitorProtocolManager::notify_connection_status(lu_bool_t status)
{
    updateOnlineStatus(status);
}

void MonitorProtocolManager::notify_req_exit(lu_int8_t action, lu_int8_t reason)
{
    const lu_char_t* reasonTxt;
    const lu_char_t* actionTxt;
    switch (action)
    {
        case APP_EXIT_CODE_RESTART:
        case APP_EXIT_CODE_START_MCM_APPLICATION:
            actionTxt = "restart";
            break;
        case APP_EXIT_CODE_REBOOT_RTU:
            actionTxt = "reboot the RTU";
            break;
        case APP_EXIT_CODE_SHUTDOWN_RTU:
            actionTxt = "shutdown the RTU";
            break;
        case APP_EXIT_CODE_START_MCM_UPDATER:
            actionTxt = "start the MCM Updater";
            break;
        default:
            actionTxt = "exit";
            break;
    }
    switch (reason)
    {
        case XMSG_EXIT_CODE_OVERTEMP:
            reasonTxt = "Temperature is getting too high";
            break;
        case XMSG_EXIT_CODE_VOLTAGE_RANGE:
            reasonTxt = "Voltage out of range";
            break;
        case XMSG_EXIT_CODE_WDOG_FAIL:
            reasonTxt = "Hardware watchdog stopped responding";
            break;
        case XMSG_EXIT_CODE_APP_FAIL:
            reasonTxt = "Application failure";
            break;
        default:
            reasonTxt = "Unknown";
            break;
    }
    Logger::getLogger(SUBSYSTEM_ID_MAIN).warn("MCM Application is about to %s (code %i); reason (%i): %s",
                    actionTxt, action, reason, reasonTxt);
    //Note that restart from Monitor notification is always forced
    mcmApplication.restart(IMCMApplication::RESTART_TYPE_FORCED, static_cast<APP_EXIT_CODE>(action));
}


void MonitorProtocolManager::notify_rep_ping()
{
    // Nothing to do
}

void MonitorProtocolManager::notify_rep_app_ver(lu_uint8_t relType, BasicVersionStr version, lu_uint8_t patch)
{
    // Nothing to do
    LU_UNUSED(relType);
    LU_UNUSED(version);
    LU_UNUSED(patch);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

void MonitorProtocolManager::updateAIObservers( lu_uint8_t channel,
                                                lu_float32_t value,
                                                lu_uint8_t flags
                                                )
{
    MonitorObserver* observerPtr;

    // Protect the observer queue
    MasterLockingMutex mMutex(queueMutex);

    // Protect the data to ensure all observers update with same data
    MasterLockingMutex lMutex(mutex);

    /* Notify observer. Get first observer */
    observerPtr = observers.getFirst();
    while(observerPtr != NULL)
    {
        if( (observerPtr->getType() == MON_CHANNEL_TYPE_AI) &&
            (observerPtr->getChannel() == channel)
            )
        {
            //Update only for the given channel
            observerPtr->update(value, flags);
        }

        /* Get next observer */
        observerPtr = observers.getNext(observerPtr);
    }
}


void MonitorProtocolManager::updateDIObservers( lu_uint8_t channel,
                                                lu_bool_t value,
                                                lu_uint8_t flags
                                                )
{
    MonitorObserver* observerPtr;

    // Protect the observer queue
    MasterLockingMutex mMutex(queueMutex);

    // Protect the data to ensure all observers update with same data
    MasterLockingMutex lMutex(mutex);

    /* Notify observer. Get first observer */
    observerPtr = observers.getFirst();
    while(observerPtr != NULL)
    {
        if( (observerPtr->getType() == MON_CHANNEL_TYPE_DI) &&
            (observerPtr->getChannel() == channel)
            )
        {
            //Update only for the given channel
            observerPtr->update((lu_float32_t)value, flags);
        }

        /* Get next observer */
        observerPtr = observers.getNext(observerPtr);
    }
}


void MonitorProtocolManager::updateOnlineStatus(lu_bool_t online)
{
    MonitorObserver* observerPtr;

    // Protect the observer queue
    MasterLockingMutex mMutex(queueMutex);

    // Protect the data to ensure all observers update with same data
    MasterLockingMutex lMutex(mutex);

    /* Notify ALL observers. */
    observerPtr = observers.getFirst();
    while(observerPtr != NULL)
    {
        observerPtr->updateOnline(online);  //Value 0 since is disconnected

        /* Get next observer */
        observerPtr = observers.getNext(observerPtr);
    }
}

/*
 *********************** End of file ******************************************
 */


