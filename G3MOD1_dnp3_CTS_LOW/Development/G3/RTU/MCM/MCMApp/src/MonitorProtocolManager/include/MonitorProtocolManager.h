/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id:  $
 *               $HeadURL:  $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MonitorProtocolHandler - Protocol interface to the Monitor Application
 *
 *    CURRENT REVISION
 *
 *               $Revision:     $: (Revision of last commit)
 *               $Author:       $: (Author of last commit)
 *       \date   $Date:         $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   22/10/2013           andrews_s    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MONITORPROTOCOLHANDLER_H_
#define MONITORPROTOCOLHANDLER_H_


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "AbstractMonitorProtocolLayer.h"
#include "QueueMgr.h"
#include "QueueObj.h"
#include "IMCMApplication.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/* Forward declaration */
class MonitorObserver;

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/**
 * observer queue manager
 */
typedef QueueMgr<MonitorObserver> MonitorObserverQueue;

typedef enum
{
    MON_CHANNEL_TYPE_DI,
    MON_CHANNEL_TYPE_AI,
    MON_CHANNEL_TYPE_LAST
} MON_CHANNEL_TYPE;
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class MonitorObserver : public QueueObj<MonitorObserver>
{
public:
    MonitorObserver(MON_CHANNEL_TYPE pointType, lu_uint8_t monChannel) :
                                                    type(pointType),
                                                    channel(monChannel)
    {};
    virtual ~MonitorObserver() {};

    /**
     * \brief get type of monitor channel
     *
     * \return Type of channel
     */
    MON_CHANNEL_TYPE getType() {return type;}

    /**
     * \brief get monitor channel ID
     *
     * \return Monitor channel ID (Not MCM channel ID)
     */
    lu_uint8_t getChannel() {return channel;}

    /**
     * \brief notify that a channel has been updated
     *
     * The subject is passed as a parameter: the observer can monitor multiple
     * channels at the same time
     *
     * \param value The new value of the channel.
     * \param flags The (new) flag status of the channel.
     *
     * \return None
     */
    virtual void update(lu_float32_t value, lu_uint8_t flags) = 0;
    /**
     * \brief Update only the status of the channel
     *
     * \param online LU_TRUE to set the status of the channel as online
     */
    virtual void updateOnline(lu_bool_t online) = 0;

private:
    MON_CHANNEL_TYPE type;  //Monitor point type
    lu_uint8_t channel;     //Monitor point channel

};


/**
 * \brief Class that manages Monitor protocol communications
 */
class MonitorProtocolManager: public AbstractMonitorProtocolLayer
{
public:
    MonitorProtocolManager(IMCMApplication& mcmApplication);
    virtual ~MonitorProtocolManager();

    /**
     * \brief Attach an observer to the protocol
     *
     * When a message reply arrives, the related observers are automatically called
     *
     * \param observer New observer
     *
     * \return Error code
     */
    virtual lu_int32_t attach(MonitorObserver* observer);

    /**
     * \brief Remove an observer
     *
     * \param observer Observer to remove
     *
     * \return error code
     */
    virtual void detach(MonitorObserver* observer);

    /* Overridden methods */
    void notify_rep_ai_val(lu_uint8_t address, lu_float32_t value, lu_uint8_t flags);
    void notify_rep_di_val(lu_uint8_t address, lu_bool_t value, lu_uint8_t flags);
    void notify_rep_ping();
    void notify_req_exit(lu_int8_t action, lu_int8_t reason);
    void notify_rep_app_ver(lu_uint8_t relType, BasicVersionStr version, lu_uint8_t patch);
    void notify_connection_status(lu_bool_t status);
    void notify_rep_kick_wdog(lu_bool_t start);

    void stop();

private:
    IMCMApplication& mcmApplication;//Reference to the main application
    MasterMutex mutex;              //mutex for concurrent read/write
    MasterMutex queueMutex;         //Mutex to protect Observer Queue
    MonitorObserverQueue observers; //enlisted observers

private:
    void updateDIObservers( lu_uint8_t channel,
                            lu_uint8_t value,
                            lu_uint8_t flags
                            );
    void updateAIObservers( lu_uint8_t channel,
                            lu_float32_t value,
                            lu_uint8_t flags
                            );
    void updateOnlineStatus(lu_bool_t online);
};
#endif

/*
 *********************** End of file ******************************************
 */





