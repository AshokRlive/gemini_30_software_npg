/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:S104EventPublisher.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief Protocol point eventing for IEC-101
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14 Oct 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef S104EVENTPROCESSOR_H_
#define S104EVENTPROCESSOR_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "LockingMutex.h"
#include "Pipe.h"

#include "SCADAProtocolCommon.h"
#include "S104InputPoint.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define S14_LOCAL_EVENT_SUPPORT 1   //Storage to Local Event Log

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * This component is for caching and publishing Slave IEC104 events to SCADA.
 */
class S104EventPublisher
{
private:
    enum EventType
    {
        MSP_EVENT,
        MDP_EVENT,
        MMENA_EVENT,
        MMENB_EVENT,
        MMENC_EVENT,
        MIT_EVENT
    };

    /**
     * \brief Internal storage of 104 events
     */
    struct TMWEvent
    {
        lu_uint8_t      eventType; /*! EventType*/
        TMWSCTR*        pSector;
        TMWTYPES_ULONG  ioa;
        TMWDTIME        timestamp;
        TMWTYPES_UCHAR  flags;
        union {
            TMWTYPES_UCHAR siq; // single point value and flags
            TMWTYPES_UCHAR diq; //double point value and flags
            TMWTYPES_ANALOG_VALUE NVC;
            TMWTYPES_LONG  bcr;
        }InformationElement;
    };

public:
    S104EventPublisher();
    virtual ~S104EventPublisher();

    /**
     * Generate an event and add it to event queue for a MSP point.
     */
    SCADAP_ERROR addMSPEvent(S104MSP& evtSrc, PointData& evtData);

    /**
     * Generate an event and add it to event queue for a MDP point.
     */
    SCADAP_ERROR addMDPEvent(S104MDP& evtSrc, PointData& evtData);

    /**
     * Generate an event and add it to event queue for a MDP point.
     */
    SCADAP_ERROR addAnalogEvent(S104Analog& evtSrc, PointData& evtData);

    /**
     * Generate an event and add it to event queue for a MDP point.
     */
    SCADAP_ERROR addMITEvent(S104MIT& evtSrc, PointData& evtData);

    /**
     * Pop events from event queue and publish it to SCADA until it is empty.
     */
    void publishEvents();

private:
    /**
     * \brief Add an event to the internal pipe to be processed
     *
     * \param event internal description of the event
     * \param
     *
     * \return
     */
    SCADAP_ERROR addEvent(TMWEvent& event);

private:
    Pipe        m_eventPipe;
    Mutex       m_eventPipeLock;
};


extern S104EventPublisher g_s14EventProc; // Global instance


#endif /* S104EVENTPROCESSOR_H_ */

/*
 *********************** End of file ******************************************
 */
