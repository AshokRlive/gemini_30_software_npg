/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s4cseta.c
 * description: IEC 60870-5-104 slave CSETA (Normalized Set Point Command
 *  With Time) functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s4cseta.h"
#include "tmwscl/i870/s14csena.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14diag.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/s104sctr.h"
#include "tmwscl/i870/s4util.h"
#include "tmwscl/utils/tmwtarg.h"

#if (S14DATA_SUPPORT_CSE_A && S14DATA_SUPPORT_TIMETAG_COMMANDS)

#if S14DATA_SUPPORT_MULTICMDS
  
/* function: s4cseta_processRequest */
void TMWDEFS_CALLBACK s4cseta_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  S14SCTR *p14Sector;
  S14SCTR_CMD *pContext;
  TMWTYPES_BOOL timerIsActive;
  TMWTYPES_ULONG ioa;
  TMWTYPES_ULONG oldIOA;
  TMWTYPES_SHORT oldNVA;
  TMWTYPES_UCHAR oldQOS;
  
  pContext = s14util_processMultiRequest(pSector, pMsg, I14DEF_TYPE_CSETA1, &ioa, &timerIsActive);
  if(pContext == TMWDEFS_NULL)
    return;

  pContext->pStatus = s14data_csenaStatus;
  
  /* Save old values in case this is an execute and we need to compare with select values.
   * All of the error paths require the context fields to be filled in with rcvd values.
   */
  oldIOA = pContext->ioa;
  oldNVA = pContext->value.sval; 
  oldQOS = pContext->qualifier;  
  
  /* If only one context per typeId is allowed, this might be cancelling and replacing the previous ioa */
  pContext->ioa = ioa;

  /* Parse NVA */
  tmwtarg_get16(&pMsg->pRxData->pMsgBuf[pMsg->offset], (TMWTYPES_USHORT *)&pContext->value.sval);
  pContext->valueType = TMWTYPES_ANALOG_TYPE_SHORT;
  pContext->useValue = TMWDEFS_TRUE;
  pMsg->offset += 2;

  /* Parse QOS */
  pContext->qualifier = pMsg->pRxData->pMsgBuf[pMsg->offset++];
  pContext->useQualifier = TMWDEFS_TRUE;

  /* Check command age */
  pContext->useDateTime = TMWDEFS_TRUE;
  i870util_read56BitTime(pMsg, &pContext->requestTime);

  /* Parse received cause of transmission */
  if(pMsg->cot == I14DEF_COT_ACTIVATION)
  {
    pContext->cot = I14DEF_COT_ACTCON;
  }
  else if(pMsg->cot == I14DEF_COT_DEACTIVATION)
  {
    /* Any deactivate will cause the select to be cancelled, even an incorrect one 
     * There is currently not a s14data function to be called indicating deactivate.
     * 
     * The 101 and 601 specs are unclear about whether a DEACTCON positive or negative 
     * should be sent on error. Since even an incorrect DEACT results in no select being
     * active, a positive reply will be sent (except if ioa is invalid below).
     */
    pContext->cot = I14DEF_COT_DEACTCON;
  }
  else
  {
    pContext->cot = I14DEF_COT_UNKNOWN_COT | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* Lookup data point using information object address */
  p14Sector = (S14SCTR *)pSector;
  pContext->pPoint = s14data_csenaLookupPoint(p14Sector->i870.pDbHandle, ioa);
  if(pContext->pPoint == TMWDEFS_NULL)
  {
    pContext->cot = I14DEF_COT_UNKNOWN_IOA | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* if deactivate, context->cot was set to DEACTCON above. */
  if(pMsg->cot == I14DEF_COT_DEACTIVATION)
  {
    return;
  }
  
  if(s4util_checkCommandAge((S104SCTR *)pSector, &pContext->requestTime) == TMWDEFS_FALSE)
  {
    /* 104 Edition 2 says there should be no response sent */
    s14util_freeCmdCtxt(p14Sector, pContext);
    S14DIAG_ERROR(p14Sector->i870.tmw.pSession, pSector, S14DIAG_CMD_DELAYED);
    return;
  }

  /* Perform select or execute and set operation status */
  if((pContext->qualifier & I14DEF_QOC_SE_SELECT) != 0)
  { 
    pContext->state = TMWDEFS_CMD_STATE_SELECTING;
    pContext->status = s14data_csenaSelect(pContext->pPoint, 
      pMsg->cot, pContext->value.sval, pContext->qualifier);
  }
  else
  {
    /* If a select is required for this point make sure a valid select
     * operation is pending and compare the execute parameters against
     * the select parameters.
     */
    if(s14data_csenaSelectRequired(pContext->pPoint))
    {
      /* Make sure select is active */
      /* Compare information object address */
      /* Compare qualifier, ignore the select bit */
      if(!timerIsActive
         ||(pContext->ioa != oldIOA)
         ||(pContext->value.sval != oldNVA)
         ||(pContext->qualifier != (oldQOS & ~I14DEF_QOC_SE_MASK)))
      {
        pContext->cot |= I14DEF_COT_NEGATIVE_CONFIRM;
        return;
      }
    }

    pContext->state = TMWDEFS_CMD_STATE_EXECUTING;
    pContext->status = s14data_csenaExecute(pContext->pPoint, 
      pMsg->cot, pContext->value.sval, pContext->qualifier);
  }
}

#else /* !S14DATA_SUPPORT_MULTICMDS */

/* function: _checkStatus */
static TMWDEFS_COMMAND_STATUS TMWDEFS_CALLBACK _checkStatus(
  TMWSCTR *pSector)
{
  return (s14csena_checkStatus(pSector));
}

/* function: s4cseta_processRequest */
void TMWDEFS_CALLBACK s4cseta_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  S104SCTR *p104Sector;
  S14SCTR *p14Sector;
  TMWTYPES_BOOL timerIsActive;
  TMWTYPES_ULONG oldIOA;
  TMWTYPES_SHORT oldNVA;
  TMWTYPES_UCHAR oldQOS;

  p104Sector = (S104SCTR *)pSector;
  p14Sector = (S14SCTR *)pSector;

  /* Any new or failed command cancels the select timer 
   * Save the timer state in case this is a valid execute command
   */
  timerIsActive = tmwtimer_isActive(&p14Sector->csenaSelectTimer);
  if(timerIsActive)
  {
    tmwtimer_cancel(&p14Sector->csenaSelectTimer);
  }

  /* If there is a previous command in progress, (response still to be sent)
   * queue a negative ACT CON to be sent. 
   * Don't overwrite the information from the previous command.
   */
  if((pMsg->cot == I14DEF_COT_ACTIVATION)
    && ((p14Sector->csetaCOT != 0) || (p14Sector->csenaCOT != 0)))
  {
    s14sesn_sendNegActCon(pSector, pMsg, I14DEF_COT_ACTCON); 
    return;
  }

  /* Store originator address */
  p14Sector->csenaOriginator = pMsg->origAddress;

  /* Save old values in case this is an execute and we need to compare with select values.
   * All of the error paths require the p104Sector fields to be filled in with rcvd values.
   */
  oldIOA = p14Sector->csenaIOA;
  oldNVA = p14Sector->csenaNVA; 
  oldQOS = p14Sector->csenaQOS;

  /* Parse Information Object Address */
  i870util_readInfoObjAddr(pSector->pSession, pMsg, &p14Sector->csenaIOA);

  /* Parse NVA */
  tmwtarg_get16(&pMsg->pRxData->pMsgBuf[pMsg->offset], (TMWTYPES_USHORT *)&p14Sector->csenaNVA);
  pMsg->offset += 2;

  /* Parse QOS */
  p14Sector->csenaQOS = pMsg->pRxData->pMsgBuf[pMsg->offset++];

  /* Check command age */
  i870util_read56BitTime(pMsg, &p104Sector->csetaRequestTime);

  /* Parse received cause of transmission */
  if(pMsg->cot == I14DEF_COT_ACTIVATION)
  {
    p14Sector->csetaCOT = I14DEF_COT_ACTCON;
  }
  else if(pMsg->cot == I14DEF_COT_DEACTIVATION)
  {
    /* Any deactivate will cause the select to be cancelled, even an incorrect one 
     * There is currently not a s14data function to be called indicating deactivate.
     * 
     * The 101 and 601 specs are unclear about whether a DEACTCON positive or negative 
     * should be sent on error. Since even an incorrect DEACT results in no select being
     * active, a positive reply will be sent (except if ioa is bad below).
     */
    p14Sector->csetaCOT = I14DEF_COT_DEACTCON;
  }
  else
  {
    p14Sector->csetaCOT = I14DEF_COT_UNKNOWN_COT | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }
 
  /* Lookup data point using information object address */
  p14Sector->pCSENAPoint = s14data_csenaLookupPoint(p14Sector->i870.pDbHandle, p14Sector->csenaIOA);
  if(p14Sector->pCSENAPoint == TMWDEFS_NULL)
  {
    p14Sector->csetaCOT = I14DEF_COT_UNKNOWN_IOA | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* if deactivate, command is finished */
  if(pMsg->cot == I14DEF_COT_DEACTIVATION)
  {
    return;
  }

  if(s4util_checkCommandAge(p104Sector, &p104Sector->csetaRequestTime) == TMWDEFS_FALSE)
  {
    /* 104 Edition 2 says there should be no response sent */
    p14Sector->csetaCOT = 0;
    S14DIAG_ERROR(p104Sector->s14.i870.tmw.pSession, (TMWSCTR *)p104Sector, S14DIAG_CMD_DELAYED);
    return;
  }

  /* Perform select or execute and set operation status */
  if((p14Sector->csenaQOS & I14DEF_QOC_SE_SELECT) != 0)
  { 
    p14Sector->csenaState = TMWDEFS_CMD_STATE_SELECTING;
    p14Sector->csenaStatus = s14data_csenaSelect(p14Sector->pCSENAPoint, 
      pMsg->cot, p14Sector->csenaNVA, p14Sector->csenaQOS);
  }
  else
  {
    /* If a select is required for this point make sure a valid select
     * operation is pending and compare the execute parameters against
     * the select parameters.
     */
    if(s14data_csenaSelectRequired(p14Sector->pCSENAPoint))
    {
      /* Make sure select is active */
      /* Compare information object address */
      /* Compare qualifier, ignore the select bit */
      if(!timerIsActive
         ||(p14Sector->csenaIOA != oldIOA)
         ||(p14Sector->csenaNVA != oldNVA)
         ||(p14Sector->csenaQOS != (oldQOS & ~I14DEF_QOC_SE_MASK)))
      {
        p14Sector->csetaCOT |= I14DEF_COT_NEGATIVE_CONFIRM;
        return;
      }
    }

    p14Sector->csenaState = TMWDEFS_CMD_STATE_EXECUTING;
    p14Sector->csenaStatus = s14data_csenaExecute(p14Sector->pCSENAPoint, 
      pMsg->cot, p14Sector->csenaNVA, p14Sector->csenaQOS);
  }
}

/* function: s4cseta_buildResponse */
TMWTYPES_BOOL TMWDEFS_CALLBACK s4cseta_buildResponse(
  TMWSCTR *pSector, 
  TMWTYPES_BOOL buildResponse)
{
  S104SCTR *p104Sector = (S104SCTR *)pSector;
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  return(s14util_buildCommandResponse(pSector, buildResponse, 
    I14DEF_TYPE_CSETA1, &p14Sector->csetaCOT, p14Sector->csenaOriginator,
    p14Sector->csenaIOA, p14Sector->csenaQOS, TMWDEFS_TRUE, &p14Sector->csenaNVA, 2, 
    &p104Sector->csetaRequestTime, _checkStatus));
}

#endif /* !S14DATA_SUPPORT_MULTICMDS */
#endif /* S14DATA_SUPPORT_CSE_A && S14DATA_SUPPORT_TIMETAG_COMMANDS */
