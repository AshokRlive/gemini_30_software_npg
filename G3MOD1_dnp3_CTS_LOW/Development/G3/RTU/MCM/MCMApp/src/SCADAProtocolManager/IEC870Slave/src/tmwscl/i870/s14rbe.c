/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14rbe.c
 * description: IEC 60870-5-101/104 Slave Report By Exception support.
 */

/* Misc Header Files */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/utils/tmwchnl.h"
#include "tmwscl/utils/tmwtimer.h"
#include "tmwscl/utils/tmwtarg.h"

#include "tmwscl/i870/s14rbe.h"
#include "tmwscl/i870/i870util.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/i14diag.h"
#include "tmwscl/i870/i870chnl.h"

/* ASDU Specific Processing */
#include "tmwscl/i870/s14msp.h"
#include "tmwscl/i870/s14mdp.h"
#include "tmwscl/i870/s14mst.h"
#include "tmwscl/i870/s14mbo.h"
#include "tmwscl/i870/s14mmena.h"
#include "tmwscl/i870/s14mmenb.h"
#include "tmwscl/i870/s14mmenc.h"
#include "tmwscl/i870/s14mit.h"
#include "tmwscl/i870/s14mitc.h"
#include "tmwscl/i870/s14mepta.h"
#include "tmwscl/i870/s14meptb.h"
#include "tmwscl/i870/s14meptc.h"
#include "tmwscl/i870/s14mpsna.h"
#include "tmwscl/i870/s14mmend.h"
#include "tmwscl/i870/s14fdrta.h"

/* function: _sendSpontaneousCCSNA */
static TMWTYPES_BOOL _sendSpontaneousCCSNA(
  TMWSCTR *pSector, 
  TMWDTIME *pEventTime)
{
  TMWSESN_TX_DATA *pTxData;

  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  S14SESN *pS14Session = (S14SESN *)pSector->pSession;

  if((pTxData = i870chnl_newTxData(pSector->pSession->pChannel, 
    pSector->pSession, pSector, pS14Session->maxASDUSize)) == TMWDEFS_NULL)
  {
    return(TMWDEFS_FALSE);
  }

#if TMWCNFG_SUPPORT_DIAG
  pTxData->pMsgDescription = "Clock Sync Event";
#endif
  pTxData->txFlags = TMWSESN_TXFLAGS_NO_RESPONSE;
  pTxData->responseTimeout = p14Sector->defaultResponseTimeout;

  /* Send clock sync event */
  i870util_buildMessageHeader(pSector->pSession, 
    pTxData, I14DEF_TYPE_CCSNA1, I14DEF_COT_SPONTANEOUS, 
    0, p14Sector->i870.asduAddress);

  /* Store Information Object Address, always 0 */
  i870util_storeInfoObjAddr(pSector->pSession, pTxData, 0);

  /* Store event time */
  i870util_write56BitTime(pTxData, pEventTime, S14DATA_SUPPORT_GENUINE_TIME);

  /* Send message */
  i870chnl_sendMessage(pTxData);

  /* Update last clock sync time */
  p14Sector->lastClockSyncTime = *pEventTime;

  return(TMWDEFS_TRUE);
} 

/* function: _scanTimeout */
static void TMWDEFS_CALLBACK _scanTimeout(void *pParam)
{
  TMWSCTR *pSector = (TMWSCTR *)pParam;
  TMWSESN *pSession = pSector->pSession;
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  /* Scan all monitored data types */
  if(s14rbe_scanForChanges(pSector))
  {
    /* If any events were generated tell link layer 
     * we have data 
     */
    pSession->pChannel->pLink->pLinkDataReady(
      pSession->pChannel->pLinkContext, pSession);
  }

  /* Restart scan period */
  if(p14Sector->rbeScanPeriod != 0)
  {
    tmwtimer_start(&p14Sector->rbeScanTimer, 
      p14Sector->rbeScanPeriod, pSession->pChannel,
      _scanTimeout, pSector);
  }
}

/* function: s14rbe_initSector */
void TMWDEFS_GLOBAL s14rbe_initSector(
  TMWSCTR *pSector)
{
  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;
 
#if S14DATA_SUPPORT_MSP
  s14msp_init(pSector); 
#endif
#if S14DATA_SUPPORT_MDP
  s14mdp_init(pSector); 
#endif
#if S14DATA_SUPPORT_MST  
  s14mst_init(pSector);
#endif
#if S14DATA_SUPPORT_MBO 
  s14mbo_init(pSector);
#endif
#if S14DATA_SUPPORT_MME_A
  s14mmena_init(pSector);
#endif
#if S14DATA_SUPPORT_MME_B
  s14mmenb_init(pSector);
#endif
#if S14DATA_SUPPORT_MME_C
  s14mmenc_init(pSector);
#endif
#if S14DATA_SUPPORT_MIT
  s14mit_init(pSector);
#endif
#if S14DATA_SUPPORT_MITC
  s14mitc_init(pSector);
#endif
#if S14DATA_SUPPORT_MEPTA
  s14mepta_init(pSector);
#endif
#if S14DATA_SUPPORT_MEPTB
  s14meptb_init(pSector);
#endif
#if S14DATA_SUPPORT_MEPTC 
  s14meptc_init(pSector);
#endif
#if S14DATA_SUPPORT_MPS
  s14mpsna_init(pSector);
#endif
#if S14DATA_SUPPORT_MMEND
  s14mmend_init(pSector);
#endif
#if S14DATA_SUPPORT_FILE
  s14fdrta_init(pSector);
#endif 

  tmwtimer_init(&p14Sector->rbeScanTimer);

  if(p14Sector->rbeScanPeriod != 0)
  {
    tmwtimer_start(&p14Sector->rbeScanTimer, 
      p14Sector->rbeScanPeriod, pSector->pSession->pChannel,
      _scanTimeout, pSector);
  }
}

/* function: s14rbe_closeSector */
void TMWDEFS_GLOBAL s14rbe_closeSector(
  TMWSCTR *pSector)
{
  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  /* Cancel scan timer */
  tmwtimer_cancel(&p14Sector->rbeScanTimer);

#if S14DATA_SUPPORT_MSP
  s14msp_close(pSector); 
#endif
#if S14DATA_SUPPORT_MDP
  s14mdp_close(pSector); 
#endif
#if S14DATA_SUPPORT_MST  
  s14mst_close(pSector);
#endif
#if S14DATA_SUPPORT_MBO 
  s14mbo_close(pSector);
#endif
#if S14DATA_SUPPORT_MME_A
  s14mmena_close(pSector);
#endif
#if S14DATA_SUPPORT_MME_B
  s14mmenb_close(pSector);
#endif
#if S14DATA_SUPPORT_MME_C
  s14mmenc_close(pSector);
#endif
#if S14DATA_SUPPORT_MIT
  s14mit_close(pSector);
#endif
#if S14DATA_SUPPORT_MITC
  s14mitc_close(pSector);
#endif
#if S14DATA_SUPPORT_MEPTA
  s14mepta_close(pSector);
#endif
#if S14DATA_SUPPORT_MEPTB
  s14meptb_close(pSector);
#endif
#if S14DATA_SUPPORT_MEPTC 
  s14meptc_close(pSector);
#endif
#if S14DATA_SUPPORT_MPS
  s14mpsna_close(pSector);
#endif
#if S14DATA_SUPPORT_MMEND
  s14mmend_close(pSector);
#endif
#if S14DATA_SUPPORT_FILE
  s14fdrta_close(pSector);
#endif
}

/* function: s14rbe_updateRBEScanPeriod */
void TMWDEFS_GLOBAL s14rbe_updateRBEScanPeriod(
  TMWSCTR *pSector, 
  TMWTYPES_MILLISECONDS period)
{
  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  if(p14Sector->rbeScanPeriod != period)
  {
    /* Cancel the old timer */
    tmwtimer_cancel(&p14Sector->rbeScanTimer);

    /* Set period to new value */
    p14Sector->rbeScanPeriod = period;

    /* Restart timer if required */
    if(p14Sector->rbeScanPeriod != 0)
    {
      tmwtimer_start(&p14Sector->rbeScanTimer, 
        p14Sector->rbeScanPeriod, pSector->pSession->pChannel,
        _scanTimeout, pSector);
    }
  }
}

/* function: s14rbe_scanForChanges */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14rbe_scanForChanges(
  TMWSCTR *pSector)
{
  TMWTYPES_BOOL eventsAdded = TMWDEFS_FALSE;

#if S14DATA_SUPPORT_MSP
  eventsAdded |= s14msp_scanForChanges(pSector);
#endif
#if S14DATA_SUPPORT_MDP
  eventsAdded |= s14mdp_scanForChanges(pSector);
#endif
#if S14DATA_SUPPORT_MST  
  eventsAdded |= s14mst_scanForChanges(pSector);
#endif
#if S14DATA_SUPPORT_MBO 
  eventsAdded |= s14mbo_scanForChanges(pSector);
#endif
#if S14DATA_SUPPORT_MME_A
  eventsAdded |= s14mmena_scanForChanges(pSector);
#endif
#if S14DATA_SUPPORT_MME_B
  eventsAdded |= s14mmenb_scanForChanges(pSector);
#endif
#if S14DATA_SUPPORT_MME_C
  eventsAdded |= s14mmenc_scanForChanges(pSector);
#endif
#if S14DATA_SUPPORT_MEPTA
  eventsAdded |= s14mepta_scanForChanges(pSector);
#endif
#if S14DATA_SUPPORT_MEPTB
  eventsAdded |= s14meptb_scanForChanges(pSector);
#endif
#if S14DATA_SUPPORT_MEPTC 
  eventsAdded |= s14meptc_scanForChanges(pSector);
#endif
#if S14DATA_SUPPORT_MPS
  eventsAdded |= s14mpsna_scanForChanges(pSector);
#endif
#if S14DATA_SUPPORT_MMEND
  eventsAdded |= s14mmend_scanForChanges(pSector);
#endif
#if S14DATA_SUPPORT_FILE
  eventsAdded |= s14fdrta_scanForChanges(pSector);
#endif

  /* Scanning for MIT changes is not supported. 
   * User should call addEvent when a freeze is performed
   */

  return(eventsAdded);
}

/* function: s14rbe_countEvents */
TMWTYPES_USHORT TMWDEFS_GLOBAL s14rbe_countEvents(
  TMWSCTR *pSector,
  TMWTYPES_BOOL countAll)
{
  TMWTYPES_USHORT numEvents = 0;

#if S14DATA_SUPPORT_MSP
  numEvents += s14msp_countEvents(pSector); 
  if(!countAll && (numEvents > 0))
    return(numEvents);
#endif
#if S14DATA_SUPPORT_MDP
  numEvents += s14mdp_countEvents(pSector);
  if(!countAll && (numEvents > 0))
    return(numEvents);
#endif
#if S14DATA_SUPPORT_MST  
  numEvents += s14mst_countEvents(pSector);
  if(!countAll && (numEvents > 0))
    return(numEvents);
#endif
#if S14DATA_SUPPORT_MBO 
  numEvents += s14mbo_countEvents(pSector);
  if(!countAll && (numEvents > 0))
    return(numEvents);
#endif
#if S14DATA_SUPPORT_MME_A
  numEvents += s14mmena_countEvents(pSector);
  if(!countAll && (numEvents > 0))
    return(numEvents);
#endif
#if S14DATA_SUPPORT_MME_B
  numEvents += s14mmenb_countEvents(pSector);
  if(!countAll && (numEvents > 0))
    return(numEvents);
#endif
#if S14DATA_SUPPORT_MME_C
  numEvents += s14mmenc_countEvents(pSector);
  if(!countAll && (numEvents > 0))
    return(numEvents);
#endif
#if S14DATA_SUPPORT_MIT
  numEvents += s14mit_countEvents(pSector);
  if(!countAll && (numEvents > 0))
    return(numEvents);
#endif
#if S14DATA_SUPPORT_MITC
  numEvents += s14mitc_countEvents(pSector);
  if(!countAll && (numEvents > 0))
    return(numEvents);
#endif
#if S14DATA_SUPPORT_MEPTA
  numEvents += s14mepta_countEvents(pSector);
  if(!countAll && (numEvents > 0))
    return(numEvents);
#endif
#if S14DATA_SUPPORT_MEPTB
  numEvents += s14meptb_countEvents(pSector);
  if(!countAll && (numEvents > 0))
    return(numEvents);
#endif
#if S14DATA_SUPPORT_MEPTC 
  numEvents += s14meptc_countEvents(pSector);
  if(!countAll && (numEvents > 0))
    return(numEvents);
#endif
#if S14DATA_SUPPORT_MPS
  numEvents += s14mpsna_countEvents(pSector);
  if(!countAll && (numEvents > 0))
    return(numEvents);
#endif
#if S14DATA_SUPPORT_MMEND
  numEvents += s14mmend_countEvents(pSector);
#endif
  
  /* FDRTA events will not be put in event queue. 
   * They will just set fileFdrtaCOT which will cause 
   * directory FDRTA ASDU to be sent
   */

  return((TMWTYPES_USHORT)numEvents);
}

/* function: s14rbe_processEvents */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14rbe_processEvents(
  TMWSCTR *pSector, 
  TMWTYPES_BOOL buildResponse)
{
  TMWTYPES_BOOL spontaneousCCSNARequired;
  TMWDTIME eventTime;
  TMWDTIME firstTime;
  S14EVNT_STATUS eventStatus = S14EVNT_NOT_SENT;

  if(buildResponse == TMWDEFS_FALSE)
  {
    if(s14rbe_countEvents(pSector, TMWDEFS_FALSE))
    {
      return(TMWDEFS_TRUE);
    }
  }

  else /* buildResponse == FALSE */
  {
    spontaneousCCSNARequired = TMWDEFS_FALSE;
 
#if S14DATA_SUPPORT_DOUBLE_TRANS
#if S14DATA_SUPPORT_MSP
    if(s14msp_countEvents(pSector))
    {
      eventStatus = s14msp_processDblTransEvents(pSector);
      if(eventStatus == S14EVNT_SENT)
        return(TMWDEFS_TRUE);
    }
#endif
#if S14DATA_SUPPORT_MDP 
    if(s14mdp_countEvents(pSector))
    {
      eventStatus = s14mdp_processDblTransEvents(pSector);
      if(eventStatus == S14EVNT_SENT)
        return(TMWDEFS_TRUE);
    }
#endif
#if S14DATA_SUPPORT_MST 
    if(s14mst_countEvents(pSector))
    {
      eventStatus = s14mst_processDblTransEvents(pSector);
      if(eventStatus == S14EVNT_SENT)
        return(TMWDEFS_TRUE);
    }
#endif
#if S14DATA_SUPPORT_MBO
    if(s14mbo_countEvents(pSector))
    {
      eventStatus = s14mbo_processDblTransEvents(pSector);
      if(eventStatus == S14EVNT_SENT)
        return(TMWDEFS_TRUE);
    }
#endif
#if S14DATA_SUPPORT_MMENA
    if(s14mmena_countEvents(pSector))
    {
      eventStatus = s14mmena_processDblTransEvents(pSector);
      if(eventStatus == S14EVNT_SENT)
        return(TMWDEFS_TRUE);
    } 
#endif
#if S14DATA_SUPPORT_MMENB
    if(s14mmenb_countEvents(pSector))
    {
      eventStatus = s14mmenb_processDblTransEvents(pSector);
      if(eventStatus == S14EVNT_SENT)
        return(TMWDEFS_TRUE);
    } 
#endif
#if S14DATA_SUPPORT_MMENC
    if(s14mmenc_countEvents(pSector))
    {
      eventStatus = s14mmenc_processDblTransEvents(pSector);
      if(eventStatus == S14EVNT_SENT)
        return(TMWDEFS_TRUE);
    } 
#endif
#if S14DATA_SUPPORT_MIT
    if(s14mit_countEvents(pSector))
    {
      eventStatus = s14mit_processDblTransEvents(pSector);
      if(eventStatus == S14EVNT_SENT)
        return(TMWDEFS_TRUE);
    }  
#endif
#if S14DATA_SUPPORT_MITC
    if(s14mitc_countEvents(pSector))
    {
      eventStatus = s14mitc_processDblTransEvents(pSector);
      if(eventStatus == S14EVNT_SENT)
        return(TMWDEFS_TRUE);
    }  
#endif
#endif

#if S14DATA_SUPPORT_MSP
    if(s14msp_countEvents(pSector))
    {
      eventStatus = s14msp_processEvents(pSector, &eventTime, 0);
      if(eventStatus == S14EVNT_SENT)
        return(TMWDEFS_TRUE);

      if(eventStatus == S14EVNT_CCSNA_SPONT_REQUIRED)
      {
        spontaneousCCSNARequired = TMWDEFS_TRUE;
        firstTime = eventTime;
      }
    }
#endif

#if S14DATA_SUPPORT_MDP 
    if(s14mdp_countEvents(pSector))
    {
      eventStatus = s14mdp_processEvents(pSector, &eventTime, 0);
      if(eventStatus == S14EVNT_SENT)
        return(TMWDEFS_TRUE);

      if(eventStatus == S14EVNT_CCSNA_SPONT_REQUIRED)
      {
        /* Keep track of earliest time in case we have to send another clock sync */
        if(!spontaneousCCSNARequired || !tmwdtime_checkTimeOrder(&firstTime, &eventTime))
        {
          spontaneousCCSNARequired = TMWDEFS_TRUE;
          firstTime = eventTime;
        }
      }
    }
#endif

#if S14DATA_SUPPORT_MST  
    if(s14mst_countEvents(pSector))
    {
      eventStatus = s14mst_processEvents(pSector, &eventTime, 0);
      if(eventStatus == S14EVNT_SENT)
        return(TMWDEFS_TRUE);

      if(eventStatus == S14EVNT_CCSNA_SPONT_REQUIRED)
      {
        /* Keep track of earliest time in case we have to send another clock sync */
        if(!spontaneousCCSNARequired || !tmwdtime_checkTimeOrder(&firstTime, &eventTime))
        {
          spontaneousCCSNARequired = TMWDEFS_TRUE;
          firstTime = eventTime;
        }
      }
    }
#endif

#if S14DATA_SUPPORT_MBO 
    if(s14mbo_countEvents(pSector))
    {
      eventStatus = s14mbo_processEvents(pSector, &eventTime);
      if(eventStatus == S14EVNT_SENT)
        return(TMWDEFS_TRUE);

      if(eventStatus == S14EVNT_CCSNA_SPONT_REQUIRED)
      {
        /* Keep track of earliest time in case we have to send another clock sync */
        if(!spontaneousCCSNARequired || !tmwdtime_checkTimeOrder(&firstTime, &eventTime))
        {
          spontaneousCCSNARequired = TMWDEFS_TRUE;
          firstTime = eventTime;
        }
      }
    }
#endif

#if S14DATA_SUPPORT_MME_A 
    if(s14mmena_countEvents(pSector))
    {
     eventStatus = s14mmena_processEvents(pSector, &eventTime);
     if(eventStatus == S14EVNT_SENT)
        return(TMWDEFS_TRUE);

      if(eventStatus == S14EVNT_CCSNA_SPONT_REQUIRED)
      {
        /* Keep track of earliest time in case we have to send another clock sync */
        if(!spontaneousCCSNARequired || !tmwdtime_checkTimeOrder(&firstTime, &eventTime))
        {
          spontaneousCCSNARequired = TMWDEFS_TRUE;
          firstTime = eventTime;
        }
      }
    }
#endif

#if S14DATA_SUPPORT_MME_B 
    if(s14mmenb_countEvents(pSector))
    {
      eventStatus = s14mmenb_processEvents(pSector, &eventTime);
      if(eventStatus == S14EVNT_SENT)
        return(TMWDEFS_TRUE);

      if(eventStatus == S14EVNT_CCSNA_SPONT_REQUIRED)
      {
        /* Keep track of earliest time in case we have to send another clock sync */
        if(!spontaneousCCSNARequired || !tmwdtime_checkTimeOrder(&firstTime, &eventTime))
        {
          spontaneousCCSNARequired = TMWDEFS_TRUE;
          firstTime = eventTime;
        }
      }
    }
#endif

#if S14DATA_SUPPORT_MME_C 
    if(s14mmenc_countEvents(pSector))
    {
      eventStatus = s14mmenc_processEvents(pSector, &eventTime);
      if(eventStatus == S14EVNT_SENT)
        return(TMWDEFS_TRUE);

      if(eventStatus == S14EVNT_CCSNA_SPONT_REQUIRED)
      {
        /* Keep track of earliest time in case we have to send another clock sync */
        if(!spontaneousCCSNARequired || !tmwdtime_checkTimeOrder(&firstTime, &eventTime))
        {
          spontaneousCCSNARequired = TMWDEFS_TRUE;
          firstTime = eventTime;
        }
      }
    }
#endif

#if S14DATA_SUPPORT_MIT
    if(s14mit_countEvents(pSector))
    {
      eventStatus = s14mit_processEvents(pSector, &eventTime);
      if(eventStatus == S14EVNT_SENT)
        return(TMWDEFS_TRUE);

      if(eventStatus == S14EVNT_CCSNA_SPONT_REQUIRED)
      {
        /* Keep track of earliest time in case we have to send another clock sync */
        if(!spontaneousCCSNARequired || !tmwdtime_checkTimeOrder(&firstTime, &eventTime))
        {
          spontaneousCCSNARequired = TMWDEFS_TRUE;
          firstTime = eventTime;
        }
      }    
    }
#endif

#if S14DATA_SUPPORT_MITC
    if(s14mitc_countEvents(pSector))
    {
      eventStatus = s14mitc_processEvents(pSector, &eventTime);
      if(eventStatus == S14EVNT_SENT)
        return(TMWDEFS_TRUE);

      if(eventStatus == S14EVNT_CCSNA_SPONT_REQUIRED)
      {
        /* Keep track of earliest time in case we have to send another clock sync */
        if(!spontaneousCCSNARequired || !tmwdtime_checkTimeOrder(&firstTime, &eventTime))
        {
          spontaneousCCSNARequired = TMWDEFS_TRUE;
          firstTime = eventTime;
        }
      }    
    }
#endif
#if S14DATA_SUPPORT_MEPTA
      if(s14mepta_countEvents(pSector))
      {
        eventStatus = s14mepta_processEvents(pSector, &eventTime);
        if(eventStatus == S14EVNT_SENT)
          return(TMWDEFS_TRUE);

      if(eventStatus == S14EVNT_CCSNA_SPONT_REQUIRED)
      {
        /* Keep track of earliest time in case we have to send another clock sync */
        if(!spontaneousCCSNARequired || !tmwdtime_checkTimeOrder(&firstTime, &eventTime))
        {
          spontaneousCCSNARequired = TMWDEFS_TRUE;
          firstTime = eventTime;
        }
      }
      }
#endif
#if S14DATA_SUPPORT_MEPTB
    if(s14meptb_countEvents(pSector))
    {
      eventStatus = s14meptb_processEvents(pSector, &eventTime);
      if(eventStatus == S14EVNT_SENT)
        return(TMWDEFS_TRUE);

      if(eventStatus == S14EVNT_CCSNA_SPONT_REQUIRED)
      {
        /* Keep track of earliest time in case we have to send another clock sync */
        if(!spontaneousCCSNARequired || !tmwdtime_checkTimeOrder(&firstTime, &eventTime))
        {
          spontaneousCCSNARequired = TMWDEFS_TRUE;
          firstTime = eventTime;
        }
      }
    }
#endif
#if S14DATA_SUPPORT_MEPTC 
    if(s14meptc_countEvents(pSector))
    {
      eventStatus = s14meptc_processEvents(pSector, &eventTime);
      if(eventStatus == S14EVNT_SENT)
        return(TMWDEFS_TRUE);

      if(eventStatus == S14EVNT_CCSNA_SPONT_REQUIRED)
      {
        /* Keep track of earliest time in case we have to send another clock sync */
        if(!spontaneousCCSNARequired || !tmwdtime_checkTimeOrder(&firstTime, &eventTime))
        {
          spontaneousCCSNARequired = TMWDEFS_TRUE;
          firstTime = eventTime;
        }
      }
    }
#endif

#if S14DATA_SUPPORT_MPS
    if(s14mpsna_countEvents(pSector))
    {
      eventStatus = s14mpsna_processEvents(pSector, &eventTime);
      if(eventStatus == S14EVNT_SENT)
        return(TMWDEFS_TRUE);

      if(eventStatus == S14EVNT_CCSNA_SPONT_REQUIRED)
      {
        /* Keep track of earliest time in case we have to send another clock sync */
        if(!spontaneousCCSNARequired || !tmwdtime_checkTimeOrder(&firstTime, &eventTime))
        {
          spontaneousCCSNARequired = TMWDEFS_TRUE;
          firstTime = eventTime;
        }
      }
    }
#endif
#if S14DATA_SUPPORT_MMEND
    if(s14mmend_countEvents(pSector))
    {
      eventStatus = s14mmend_processEvents(pSector, &eventTime);
      if(eventStatus == S14EVNT_SENT)
        return(TMWDEFS_TRUE);

      if(eventStatus == S14EVNT_CCSNA_SPONT_REQUIRED)
      {
        /* Keep track of earliest time in case we have to send another clock sync */
        if(!spontaneousCCSNARequired || !tmwdtime_checkTimeOrder(&firstTime, &eventTime))
        {
          spontaneousCCSNARequired = TMWDEFS_TRUE;
          firstTime = eventTime;
        }
      }

      /* FDRTA events will not be put in event queue. 
       * They will just set fileFdrtaCOT which will cause 
       * directory FDRTA ASDU to be sent
       */
    }
#endif

    /* If a clock sync is required because time of next to send
     * did not fall in same hour as last spontaneous CCSNA, send
     * another spontaneous CCSNA first.
     */
    if(spontaneousCCSNARequired)
    {
      return(_sendSpontaneousCCSNA(pSector, &eventTime)); 
    }
  } /* end of buildResponse == FALSE */

  return(TMWDEFS_FALSE);
} 

/* function: s14rbe_processMonitorEvents */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14rbe_processMonitorEvents(
  TMWSCTR *pSector,
  TMWTYPES_UCHAR asduType)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  S14EVNT_STATUS status;
  TMWDTIME eventTime;

  status = S14EVNT_NOT_SENT;

#if S14DATA_SUPPORT_MSP
  if(asduType == I14DEF_TYPE_CSCNA1)
  {
    status = s14msp_processEvents(pSector, &eventTime, S14UTIL_MODE_TO_REMLOC_COT(p14Sector->localMode));
  }
#endif

#if S14DATA_SUPPORT_MDP
  if(asduType == I14DEF_TYPE_CDCNA1)
  {
    status = s14mdp_processEvents(pSector, &eventTime, S14UTIL_MODE_TO_REMLOC_COT(p14Sector->localMode));
  }
#endif

#if S14DATA_SUPPORT_MST
  if(asduType == I14DEF_TYPE_CRCNA1)
  {
    status = s14mst_processEvents(pSector, &eventTime, S14UTIL_MODE_TO_REMLOC_COT(p14Sector->localMode));
  }
#endif

  if(status == S14EVNT_CCSNA_SPONT_REQUIRED)
  {
    return(_sendSpontaneousCCSNA(pSector, &eventTime)); 
  }
  else if(status == S14EVNT_SENT)
  {
    return TMWDEFS_TRUE;
  }

  return TMWDEFS_FALSE;
}
