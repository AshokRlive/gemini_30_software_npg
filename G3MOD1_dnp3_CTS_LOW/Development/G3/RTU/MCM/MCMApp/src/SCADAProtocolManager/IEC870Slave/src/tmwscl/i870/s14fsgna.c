/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/


/* file: s14fsgna.c
 * description: IEC 60870-5-101 slave FSGNA (File Transfer - Segment)
 *  functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14fsgna.h"
#include "tmwscl/i870/s14file.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/i14diag.h"
#include "tmwscl/i870/s14diag.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/utils/tmwtarg.h"

#if S14DATA_SUPPORT_FILE

/* function: s14fsgna_processRequest */
void TMWDEFS_CALLBACK s14fsgna_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  TMWTYPES_ULONG  ioa;
  TMWTYPES_USHORT fileName;
  TMWTYPES_UCHAR  sectionName;
  TMWTYPES_UCHAR  segmentLength;

  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  I870SESN *pI870Session = (I870SESN *)pSector->pSession;

  /* Store originator address */
  p14Sector->fileOriginator = pMsg->origAddress;

  /* Diagnostics */
  I14DIAG_SHOW_STORE_TYPE(pSector, pMsg);
   
  if(pMsg->cot != I14DEF_COT_FILE_XFER)
  { 
    /* Build and send a Negative Activation Confirmation */
    s14sesn_sendNegActCon(pSector, pMsg, I14DEF_COT_UNKNOWN_COT);
    return;
  }
  
  /* Protect against badly formatted message  
   * verify there are enough bytes for this variable length message 
   */
  if(!i870util_validateMinLen(pSector, pMsg, (pI870Session->infoObjAddrSize + 4)))  
    return;
  
  /* Parse Information Object Address */
  i870util_readInfoObjAddr(pSector->pSession, pMsg, &ioa);
  
  /* Read name of file */
  tmwtarg_get16(&pMsg->pRxData->pMsgBuf[pMsg->offset], &fileName);
  pMsg->offset += 2;

  /* Read name of section */
  sectionName = pMsg->pRxData->pMsgBuf[pMsg->offset++];

  /* Read length of segment */
  segmentLength = pMsg->pRxData->pMsgBuf[pMsg->offset++];

  /* Protect against badly formatted message */
  if(!i870util_validateMinLen(pSector, pMsg, segmentLength)) 
    return;

  p14Sector->fileSectionChecksum = s14file_calculateChecksum(p14Sector->fileSectionChecksum,
    &pMsg->pRxData->pMsgBuf[pMsg->offset], segmentLength);

  /* Get Segment data directly from ASDU */
  if(s14data_fileSegmentStore(p14Sector->i870.pDbHandle, ioa, 
    fileName, sectionName, segmentLength, &pMsg->pRxData->pMsgBuf[pMsg->offset]) == TMWDEFS_FALSE)
  {
    /* Corrupt checksum so error will be sent when FLSNA is received */  
    p14Sector->fileSectionChecksum++;
  }
    
  /* Diagnostics */
  I14DIAG_SHOW_FSGNA(pSector, ioa, fileName, sectionName, segmentLength, &pMsg->pRxData->pMsgBuf[pMsg->offset]);

  p14Sector->fileState = S14FILE_IDLE_STATE;
}

#endif /* S14DATA_SUPPORT_FILE */
