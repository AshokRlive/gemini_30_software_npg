/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i870util.h
 * description: This file is intended for internal SCL use only.
 *  IEC 60870-5 utilities
 */
#ifndef I870UTIL_DEFINED
#define I870UTIL_DEFINED

#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/utils/tmwdtime.h"
#include "tmwscl/utils/tmwlink.h"

/* Define structure to hold details of the current message */
typedef struct I870Message {
  TMWSESN_RX_DATA *pRxData;
  TMWTYPES_USHORT offset;              /* Current offset into message */
  TMWTYPES_UCHAR typeId;               /* ASDU Type Id of message */
  TMWTYPES_BOOL indexed;               /* Is message indexed or sequential */
  TMWTYPES_UCHAR quantity;             /* Number of information objects in message */
  TMWTYPES_UCHAR cot;                  /* Cause of transmission */
  TMWTYPES_UCHAR origAddress;          /* Originator address */
  TMWTYPES_USHORT asduAddress;         /* Command address of ASDU */
  TMWTYPES_ULONG nextIOA;              /* Next information object address, 
                                         used for indexed addressing */
  TMWTYPES_UCHAR recordAddress;        /* Record Address - for 102 */
} I870UTIL_MESSAGE;

#ifdef __cplusplus
extern "C" {
#endif

  /* function: i870util_messageSize
   * purpose: Calculate the expected size of a message based on the number 
   *  of objects and the size of each object.
   * arguments:
   *  pSession - session this message is for
   *  indexed - use indexed or sequential addressing
   *  numObjects - number of information objects
   *  dataSize - size of data portion of information object
   * returns:
   *  size of message in bytes
   */
  TMWTYPES_USHORT TMWDEFS_GLOBAL i870util_messageSize(
    TMWSESN *pSession, 
    TMWTYPES_BOOL indexed, 
    TMWTYPES_UCHAR numObjects, 
    TMWTYPES_USHORT dataSize);

  /* function: i870util_validateMessageSize 
   * purpose: Validate that the length of the received message is long enouogh
   *  for the claimed number of objects and the size of each object.
   * arguments:
   *  pSession - session this message is for 
   *  pMsg - pointer to structure containing information about the received message
   *  dataSize - size of data portion of information object
   * returns:
   *  size of message in bytes
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL i870util_validateMessageSize(
    TMWSESN *pSession,
    I870UTIL_MESSAGE *pMsg,
    TMWTYPES_SHORT dataSize);

  /* function: i870util_validateMinLen
   * purpose: Validate that there are enough bytes remaining in the received 
   *  message to keep parsing.
   * arguments:
   *  pSession - session this message is for 
   *  pMsg - pointer to structure containing information about the received message
   *  length - minimum required bytes that must be available to be parsed.
   * returns:
   *  size of message in bytes
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL i870util_validateMinLen(
    TMWSCTR *pSector, 
    I870UTIL_MESSAGE *pMsg, 
    TMWTYPES_USHORT length);

  /* function: i870util_buildMessageHeader */
  void TMWDEFS_GLOBAL i870util_buildMessageHeader(
    TMWSESN *pSession,
    TMWSESN_TX_DATA *pTxData, 
    TMWTYPES_UCHAR typeId, 
    TMWTYPES_UCHAR cot, 
    TMWTYPES_UCHAR originatorAddress,
    TMWTYPES_USHORT sectorAddress);

  /* function: i870util_readMessageHeader */
  TMWTYPES_BOOL TMWDEFS_GLOBAL i870util_readMessageHeader(
    TMWSESN *pSession,
    I870UTIL_MESSAGE *pMsgHeader, 
    TMWSESN_RX_DATA *pRxData);

  /* function: i870util_storeInfoObjAddr */
  void TMWDEFS_GLOBAL i870util_storeInfoObjAddr(
    TMWSESN *pSession,
    TMWSESN_TX_DATA *pTxData, 
    TMWTYPES_ULONG ioa);

  /* function: i870util_readInfoObjAddr */
  void TMWDEFS_GLOBAL i870util_readInfoObjAddr(
    TMWSESN *pSession,
    I870UTIL_MESSAGE *pMsgHeader, 
    TMWTYPES_ULONG *pIOA);

  /* function: i870util_storeHourDateInMessage */
  void TMWDEFS_GLOBAL i870util_storeHourDateInMessage(
    TMWTYPES_UCHAR *pBuf,
    TMWDTIME *pDateTime,
    TMWTYPES_BOOL useDayOfWeek);

  /* function: i870util_writeHourDateInMessage */
  void TMWDEFS_GLOBAL i870util_writeHourDateInMessage(
    TMWSESN_TX_DATA *pTxData, 
    TMWDTIME *pDateTime);

  /* function: i870util_store24BitTime */
  void TMWDEFS_GLOBAL i870util_store24BitTime(
    TMWTYPES_UCHAR *pBuf, 
    TMWDTIME *pDateTime,
    TMWTYPES_BOOL res1BitSupport);

  /* function: i870util_write24BitTime */
  void TMWDEFS_GLOBAL i870util_write24BitTime(
    TMWSESN_TX_DATA *pTxData, 
    TMWDTIME *pDateTime,
    TMWTYPES_BOOL res1BitSupport);

  /* function: i870util_store56BitTime */
  void TMWDEFS_GLOBAL i870util_store56BitTime(
    TMWTYPES_UCHAR *pBuf, 
    TMWDTIME *pDateTime,
    TMWTYPES_BOOL res1BitSupport,
    TMWTYPES_BOOL useDayOfWeek);

  /* function: i870util_write56BitTime */
  void TMWDEFS_GLOBAL i870util_write56BitTime(
    TMWSESN_TX_DATA *pTxData, 
    TMWDTIME *pDateTime,
    TMWTYPES_BOOL res1BitSupport);

  /* function: i870util_read24BitTime */
  TMWDEFS_SCL_API void TMWDEFS_GLOBAL i870util_read24BitTime(
    I870UTIL_MESSAGE *pMsgHeader, 
    TMWDTIME *pDateTime);

  /* function: i870util_read56BitTime */
  TMWDEFS_SCL_API void TMWDEFS_GLOBAL i870util_read56BitTime(
    I870UTIL_MESSAGE *pMsgHeader, 
    TMWDTIME *pDateTime);

  /* The following functions are used by 103 only */

  /* function: i870util_writeInfoObjectId */ 
  void TMWDEFS_GLOBAL i870util_writeInfoObjectId(
    TMWSESN_TX_DATA *pTxData, 
    TMWTYPES_UCHAR functionType, 
    TMWTYPES_UCHAR infoNumber);
 
  /* function: i870util_readInfoObjectId */
  void TMWDEFS_GLOBAL i870util_readInfoObjectId(
    I870UTIL_MESSAGE *pMsg, 
    TMWTYPES_UCHAR *pFunctionType, 
    TMWTYPES_UCHAR *pInfoNumber);
 
  /* function: i870util_store32BitTime */
  void TMWDEFS_GLOBAL i870util_store32BitTime(
    TMWTYPES_UCHAR *pBuf, 
    TMWDTIME *pDateTime);

  /* function: i870util_write32BitTime */
  void TMWDEFS_GLOBAL i870util_write32BitTime(
    TMWSESN_TX_DATA *pTxData, 
    TMWDTIME *pDateTime);

  /* function: i870util_read32BitTime */
  TMWDEFS_SCL_API void TMWDEFS_GLOBAL i870util_read32BitTime(
    I870UTIL_MESSAGE *pMsgHeader, 
    TMWDTIME *pDateTime);

#ifdef __cplusplus
}
#endif
#endif /* I870UTIL_DEFINED */
