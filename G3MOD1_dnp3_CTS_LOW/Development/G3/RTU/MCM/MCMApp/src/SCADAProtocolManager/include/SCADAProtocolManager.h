/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       SCADA Protocol Manager public interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   06/09/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_51F393E3_5327_4996_8638_4FE550922919__INCLUDED_)
#define EA_51F393E3_5327_4996_8638_4FE550922919__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include <vector>

#include "lu_types.h"
#include "GeminiDatabase.h"
#include "EventLogManager.h"
#include "Logger.h"
#include "ISlaveProtocol.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class SCADAProtocolManager
{
public:
    // Default thread scheduling and priority
    static const SCHED_TYPE  SCADAThreadSched = SCHED_TYPE_FIFO;
    static const lu_uint32_t SCADAThreadPrio  = SCADA_PMANAGER_THREAD;

public:
    SCADAProtocolManager();
    virtual ~SCADAProtocolManager();

    /**
     * \brief Start all the protocols
     *
     * \return Error code
     */
    SCADAP_ERROR startProtocolStack();

    /**
     * \brief Stop all the protocols
     *
     * \return Error code
     */
    SCADAP_ERROR stopProtocolStack();

    /**
     * \brief Add a new SCADA Protocol
     *
     * \return Error code
     */
    SCADAP_ERROR addProtocolStack(ISlaveProtocol &SlaveProtocol);

private:
    typedef std::vector<ISlaveProtocol*> SPVector;

public:
    SPVector slaveProtocols;  // Vector of Slave Protocol Pointers

private:

    Logger& log;
    EventLogManager* eventLog;
};

#endif // !defined(EA_51F393E3_5327_4996_8638_4FE550922919__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
