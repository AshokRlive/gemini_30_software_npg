/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14dbas.h
 * description: This file is intended for internal SCL use only.
 *  IEC 60870-5-101/104 slave database interface
 */
#ifndef S14DBAS_DEFINED
#define S14DBAS_DEFINED

#include "tmwscl/i870/s14sesn.h"
#include "tmwscl/i870/s14sctr.h"

typedef enum S14GroupStatus {
  S14DBAS_GROUP_STATUS_NO_DATA,
  S14DBAS_GROUP_STATUS_MORE_DATA,
  S14DBAS_GROUP_STATUS_COMPLETE
} S14DBAS_GROUP_STATUS;

#ifdef __cplusplus
extern "C" {
#endif

  /* function: s14dbas_readPoint */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14dbas_readPoint(
    TMWSESN_TX_DATA *pTxData,
    TMWSCTR *pSector, 
    TMWTYPES_ULONG ioa);
    
  /* function: s14dbas_readGroup */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14dbas_readGroup(
    TMWSESN_TX_DATA *pTxData,
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot, 
    TMWDEFS_GROUP_MASK groupMask,
    TMWTYPES_UCHAR *pGroupIndex, 
    TMWTYPES_USHORT *pPointIndex);

  /* function: s14dbas_readCounterGroup */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14dbas_readCounterGroup(
    TMWSESN_TX_DATA *pTxData,
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot, 
    TMWDEFS_GROUP_MASK groupMask,
    TMWTYPES_UCHAR *pGroupIndex, 
    TMWTYPES_USHORT *pPointIndex);

#ifdef __cplusplus
}
#endif
#endif /* S14DBAS_DEFINED */
