/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14mem.h
 * description: IEC 60870-5-101/104 slave memory functions
 */
#ifndef S14MEM_DEFINED
#define S14MEM_DEFINED

#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/utils/tmwmem.h"

#include "tmwscl/i870/s14cnfg.h"
#include "tmwscl/i870/s14data.h"

/* Memory allocation defines */
typedef enum S14memAllocType {
#if S14DATA_SUPPORT_MULTICMDS
  S14MEM_SCTR_CMD_TYPE,
#endif
  S14MEM_MBO_EVENT_TYPE,
  S14MEM_MDP_EVENT_TYPE,
  S14MEM_MEPTA_EVENT_TYPE,
  S14MEM_MEPTB_EVENT_TYPE,
  S14MEM_MEPTC_EVENT_TYPE,
  S14MEM_MIT_EVENT_TYPE,
#if S14DATA_SUPPORT_MITC
  S14MEM_MITC_EVENT_TYPE,
#endif
  S14MEM_MMENA_EVENT_TYPE,
  S14MEM_MMENB_EVENT_TYPE,
#if S14DATA_SUPPORT_MME_C
  S14MEM_MMENC_EVENT_TYPE,
#endif
  S14MEM_MMEND_EVENT_TYPE,
  S14MEM_MPSNA_EVENT_TYPE,
  S14MEM_MSP_EVENT_TYPE,
  S14MEM_MST_EVENT_TYPE,

#if TMWCNFG_USE_SIMULATED_DB
#if S14DATA_SUPPORT_FILE
  S14MEM_SIM_FILE_TYPE,
  S14MEM_SIM_SECTION_TYPE,
#endif
  S14MEM_SIM_DATABASE_TYPE,
#endif

  S14MEM_ALLOC_TYPE_MAX
} S14MEM_ALLOC_TYPE;

typedef struct {
  /* For each event type the number of events that can be queued at once.
   * This number specifies the maximum number of events per data type for 
   * all 101/104 slaves in the system so it should generally be set to
   * the maximum number per sector times the number of sectors.
   */
  TMWTYPES_UINT numSctrCmds;
  TMWTYPES_UINT numMBOEvents;
  TMWTYPES_UINT numMDPEvents;
  TMWTYPES_UINT numMEPTAEvents;
  TMWTYPES_UINT numMEPTBEvents;
  TMWTYPES_UINT numMEPTCEvents;
  TMWTYPES_UINT numMITEvents;
  TMWTYPES_UINT numMITCEvents;
  TMWTYPES_UINT numMMENAEvents;
  TMWTYPES_UINT numMMENBEvents;
  TMWTYPES_UINT numMMENCEvents;
  TMWTYPES_UINT numMMENDEvents;
  TMWTYPES_UINT numMPSNAEvents;
  TMWTYPES_UINT numMSPEvents;
  TMWTYPES_UINT numMSTEvents;

#if TMWCNFG_USE_SIMULATED_DB
  /* Specify the max number of slave 101/104 simulated databases. The TMW SCL
   * will allocate a new simulated database for each slave 101/104 sector.
   * These are not be needed once an actual database is implemented.
   */
  TMWTYPES_UINT numSimDbases;

  /* Number of files and sections that can be stored in the simulated
   * databases. These two are only relevant if simulated database AND file transfer
   * are supported
   */
  TMWTYPES_UINT numSimFiles;
  TMWTYPES_UINT numSimSections;
#endif
} S14MEM_CONFIG;
 

#ifdef __cplusplus
extern "C" {
#endif

  /* routine: s14mem_initConfig
   * purpose:  INTERNAL FUNCTION to initialize the memory configuration 
   *  structure indicating how many of each structure type to put in
   *  the memory pool. These will be initialized according 
   *  to the compile time defines.
   *  NOTE: user should call s101mem_initConfig() or s104mem_initConfig()
   *  to modify the number of buffers allowed for each type.
   * arguments:
   *  pConfig - pointer to memory configuration structure to be filled in
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL s14mem_initConfig(
    S14MEM_CONFIG   *pConfig);

  /* routine: s14mem_init
   * purpose: INTERNAL memory management init function.
   *  NOTE: user should call s101mem_initMemory() or s104mem_initMemory() 
   *  to modify the number of buffers allowed for each type.
   * arguments:
   *  pConfig - pointer to memory configuration structure to be used
   * returns:
   *  TMWDEFS_TRUE if successful
   *  TMWDEFS_FALSE otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14mem_init(
    S14MEM_CONFIG *pConfig);

  /* function: s14mem_alloc
   * purpose:  Allocate memory  
   * arguments: 
   *  type - enum value indicating what structure to allocate
   * returns:
   *   TMWDEFS_NULL if allocation failed
   *   void * pointer to allocated memory if successful
   */
  void * TMWDEFS_GLOBAL s14mem_alloc(
    S14MEM_ALLOC_TYPE type);
 
  /* function: s14mem_free
   * purpose:  Deallocate memory
   * arguments: 
   *  pBuf - pointer to buffer to be deallocated
   * returns:    
   *   void  
   */
  void TMWDEFS_CALLBACK s14mem_free(
    void *pBuf);

  /* function: s14mem_getUsage
   * purpose:  Determine memory usage for each type of memory
   *    managed by this file.
   * arguments: 
   *  index: index of pool, starting with 0 caller can call
   *    this function repeatedly, while incrementing index. When
   *     index is larger than number of pools, this function
   *     will return TMWDEFS_FALSE
   *  pName: pointer to a char pointer to be filled in
   *  pStruct: pointer to structure to be filled in.
   * returns:    
   *  TMWDEFS_TRUE  if successfully returned usage statistics.
   *  TMWDEFS_FALSE if failure because index is too large.
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL s14mem_getUsage(
    TMWTYPES_UCHAR index,
    const TMWTYPES_CHAR **pName,
    TMWMEM_POOL_STRUCT *pStruct);

#ifdef __cplusplus
}
#endif
#endif /* S14MEM_DEFINED */
