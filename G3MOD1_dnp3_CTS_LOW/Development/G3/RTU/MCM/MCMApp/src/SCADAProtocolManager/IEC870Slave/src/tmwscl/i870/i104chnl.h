/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i104chnl.h
 * description: Implement IEC 60870-5-104 Master and Slave channel.
 */
#ifndef I4CHNL_DEFINED
#define I4CHNL_DEFINED

#include "tmwscl/utils/tmwphys.h"
#include "tmwscl/utils/tmwchnl.h"
#include "tmwscl/i870/i870lnk4.h"
#include "tmwscl/i870/i870chnl.h"
#ifdef __cplusplus
extern "C" {
#endif

  /* function: i104chnl_initConfig 
   * purpose:  Initialize I870-5-104 channel configuration data structures. The   
   *  user should call this routine to initialize these data structures
   *  and then modify the desired fields before calling i104chnl_openChannel
   *  to actually open the desired channel.
   * arguments: 
   *  pChnlConfig - pointer to channel configuration structure
   *  pLinkConfig - pointer to link configuration structure
   *  pPhysConfig - pointer to physical configuration structure
   * returns:
   *   void
   */
  TMWDEFS_SCL_API void TMWDEFS_GLOBAL i104chnl_initConfig(
    I870CHNL_CONFIG *pChnlConfig,
    I870LNK4_CONFIG *pLinkConfig, 
    TMWPHYS_CONFIG  *pPhysConfig);

  /* function: i104chnl_openChannel 
   * purpose: Open an I870-5-104 channel. 
   * arguments:
   *  pApplContext - application context to add channel to
   *  pChnlConfig - pointer to channel configuration
   *  pLinkConfig - pointer to link layer configuration
   *  pPhysConfig - pointer to physical layer configuration
   *  pIOConfig - pointer to target I/O configuration data structure
   *   which is passed directly to the target routines implemented
   *   in tmwtarg.h/c
   *  pTmwTargConfig - TMW specific IO configuration information 
   *   which will be passed to low level IO routines in tmwtarg.h.
   * returns:
   *  pointer to new channel, this pointer is used to reference this
   *  channel in other calls to the SCL.
   */
  TMWDEFS_SCL_API TMWCHNL * TMWDEFS_GLOBAL i104chnl_openChannel(
    TMWAPPL               *pApplContext,
    const I870CHNL_CONFIG *pChnlConfig,
    const I870LNK4_CONFIG *pLinkConfig, 
    const TMWPHYS_CONFIG  *pPhysConfig, 
    const void            *pIOConfig,
    struct TMWTargConfigStruct *pTmwTargConfig);
 
  /* function: i104chnl_modifyPhys 
   * purpose: Modify physical layer parameters on an open channel
   * arguments:
   *  pChannel - channel containing physical context to modify
   *   returned by i104chnl_openChannel.
   *  pPhysConfig - pointer to physical layer configuration data
   *  configMask - mask telling SCL which fields in pPhysConfig to
   *   modify
   * returns:
   *   TMWDEFS_TRUE if successful, else TMWDEFS_FALSE
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL i104chnl_modifyPhys(
    TMWCHNL *pChannel, 
    const TMWPHYS_CONFIG *pPhysConfig, 
    TMWTYPES_ULONG configMask);

  /* function: i104chnl_modifyLink 
   * purpose: Modify link layer parameters on an open channel
   * arguments:
   *  pChannel - channel containing link context to modify
   *   returned by i104chnl_openChannel.
   *  pLinkConfig - pointer to link layer configuration
   *  configMask - mask telling SCL which fields in pLinkConfig to
   *   modify
   * returns:
   *   TMWDEFS_TRUE if successful, else TMWDEFS_FALSE
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL i104chnl_modifyLink(
    TMWCHNL               *pChannel, 
    const I870LNK4_CONFIG *pLinkConfig, 
    TMWTYPES_ULONG          configMask);

  /* function: i104chnl_closeChannel  
   * purpose: Close a previously opened channel
   * arguments:
   *  pChannel - channel to close
   *   returned by i104chnl_openChannel.
   * returns:
   *  TMWDEFS_TRUE if successful, else TMWDEFS_FALSE
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL i104chnl_closeChannel(
    TMWCHNL *pChannel);

#ifdef __cplusplus
}
#endif
#endif /* I4CHNL_DEFINED */
