/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:S101Channel.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief Protocol Physical Channel
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19 Jul 2016     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "S101Debug.h"
#include "S101TMWIncludes.h"
#include "S101Channel.h"
#include "S101Session.h"
#include "ISlaveProtocolChannel.h"
#include "ISCADAConnectionManager.h"



/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static  Logger& log(Logger::getLogger(SUBSYSTEM_ID_SCADA_HANDLER));

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
S101Channel::S101Channel(GeminiDatabase&  g3database, S101Protocol* protocol):
                        mp_protocol(protocol),
                        m_g3db(g3database),
                        mp_tmwchnl(NULL),
                        mp_tmwchnlCnfg(NULL),
                        m_sessions()

{}

S101Channel::~S101Channel()
{
    /* Delete all sessions*/
    for (S101SessionVect::iterator it = m_sessions.begin();
                    it != m_sessions.end(); ++it)
    {
        delete (*it);
    }
    m_sessions.clear();

    closeChannel();


    /* Delete config */
    if(mp_tmwchnlCnfg != NULL)
    {
        // Delete channel config
        delete mp_tmwchnlCnfg;
        mp_tmwchnlCnfg = NULL;
    }

}

SCADAP_ERROR S101Channel::openChannel(TMWAPPL* tmwappl)
{
    DBG_INFO("%s opening channel",TITLE);

    checkNotNull(tmwappl,          "IEC101 application context is null");
    checkNotNull(mp_tmwchnlCnfg,   "IEC101 channel not configured");

    /*Open channel*/
    mp_tmwchnl = ft12chnl_openChannel(tmwappl,
                                        &(mp_tmwchnlCnfg->pChnlConfig),
                                        &(mp_tmwchnlCnfg->pLinkConfig),
                                        &(mp_tmwchnlCnfg->pPhysConfig),
                                        &(mp_tmwchnlCnfg->pIOConfig  ),
                                        &(mp_tmwchnlCnfg->pTMWTargConfig)
                                        );

    checkNotNull(mp_tmwchnl,   "IEC101 channel open failed");


    /* Open all Sessions*/
    for (S101SessionVect::iterator itSession = m_sessions.begin();
                    itSession != m_sessions.end(); ++itSession)
    {
        (*itSession)->openSession(mp_tmwchnl);
    }

    return  SCADAP_ERROR_NONE;
}


void S101Channel::closeChannel()
{
    /* Close all channels*/
    for (S101SessionVect::iterator it = m_sessions.begin();
                    it != m_sessions.end(); ++it)
    {
        DBG_INFO("%s closing session...",TITLE);
        (*it)->closeSession();
    }

    if(mp_tmwchnl != NULL)
    {
        i870chnl_closeChannel(mp_tmwchnl);
    }
    mp_tmwchnl = NULL;
}

void S101Channel::configure(const S101ChannelConfig& channelConfig)
{
    if(mp_tmwchnlCnfg == NULL)
    {
        mp_tmwchnlCnfg = new S101ChannelConfig();
    }
    memcpy(mp_tmwchnlCnfg, &channelConfig, sizeof(channelConfig));

    /** TODO: apply configuration to existing channel. */
}



void S101Channel::addSession(S101Session* sessionPtr)
{
    if(sessionPtr != NULL)
    {
        m_sessions.push_back(sessionPtr);
    }
}




/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
