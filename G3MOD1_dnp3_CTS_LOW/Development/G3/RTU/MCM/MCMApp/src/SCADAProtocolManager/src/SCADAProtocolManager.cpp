/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       SCADA Protocol Manager Module Implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   06/09/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "SCADAProtocolManager.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

SCADAProtocolManager::SCADAProtocolManager() : log(Logger::getLogger(SUBSYSTEM_ID_SCADA_HANDLER))
{
    eventLog = EventLogManager::getInstance();
}

SCADAProtocolManager::~SCADAProtocolManager()
{
    stopProtocolStack();

    // TODO - SKA - Loop over the slaveProtocols, deleting them
}

SCADAP_ERROR SCADAProtocolManager::startProtocolStack()
{
    const lu_char_t* FTITLE = "SCADAProtocolManager::startProtocolStack:";
    SCADAP_ERROR ret;
    lu_uint32_t i = 0;
    for (SPVector::iterator itSlaveProtocol = slaveProtocols.begin();
                           itSlaveProtocol != slaveProtocols.end();
                           ++itSlaveProtocol)
    {
        log.info("%s Starting protocol stack", FTITLE);

        ret = (*itSlaveProtocol)->startProtocol();
        if (ret != SCADAP_ERROR_NONE)
        {
            log.error("%s Error(%i) starting protocol %u", FTITLE, ret, i++);
            CTEventCommStr event;
            event.device = EVENTCOMM_DEVICE_SCADA;
            event.action = EVENTCOMM_ACTION_ERROR_CONNECT; //fail
            eventLog->logEvent(event);
        }

    }

    return ret;
}

SCADAP_ERROR SCADAProtocolManager::stopProtocolStack()
{
    const lu_char_t* FTITLE = "SCADAProtocolManager::stopProtocolStack:";
    SCADAP_ERROR ret;

    // Loop over the slaveProtocols, stopping them
    for (SPVector::iterator itSlaveProtocol = slaveProtocols.begin();
                           itSlaveProtocol != slaveProtocols.end();
                           ++itSlaveProtocol)
    {
        ret = (*itSlaveProtocol)->stopProtocol();
        if (ret != SCADAP_ERROR_NONE)
        {
            log.error("%s: Error(%i) stopping protocol", FTITLE, ret);
        }
    }

    //Report Event
    CTEventCommStr event;
    event.device = EVENTCOMM_DEVICE_SCADA;
    event.action = (ret == SCADAP_ERROR_NONE)? EVENTCOMM_ACTION_DISCONNECT : EVENTCOMM_ACTION_ERROR_DISCONNECT; //connect/fail
    eventLog->logEvent(event);

    return ret;
}

SCADAP_ERROR SCADAProtocolManager::addProtocolStack(ISlaveProtocol& SlaveProtocol)
{
    // Add protocol to the vector of Slave Protocols
    slaveProtocols.push_back(&SlaveProtocol);

    return SCADAP_ERROR_NONE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
