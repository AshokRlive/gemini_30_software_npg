/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14sesn.h
 * description: This file is intended for internal SCL use only.
 *  Slave IEC 60870-5-101/104 session
 */
#ifndef S14SESN_DEFINED
#define S14SESN_DEFINED

#include "tmwscl/utils/tmwscl.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/i870sesn.h"
#include "tmwscl/i870/i870util.h"

/* Define prototype for user provided callback function used to process requests
 * from master devices for ASDU typeIds not supported by SCL
 */
typedef TMWTYPES_BOOL (*S14SESN_PROCESS_REQ_CALLBACK)(
  void *pProcessRequestParam,
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsgHeader);

/* Define prototype for user provided callback function used to build responses
 * to master devices for ASDU typeIds not supported by SCL
 */
typedef TMWTYPES_BOOL (*S14SESN_BUILD_RESP_CALLBACK)(
  void *pBuildResponseParam,
  TMWSCTR *pSector, 
  TMWTYPES_BOOL buildResponse);

/* S14 Session Context */
typedef struct S14SessionStruct { 
  
  /* Generic IEC 60870 Session info, must be first entry */
  I870SESN i870;

  /* Configuration */

  /* Maximum ASDU size */
  TMWTYPES_UCHAR                  maxASDUSize;

  /* User registered process request function for ASDU typeIds
   * not supported by the SCL
   */ 
  S14SESN_PROCESS_REQ_CALLBACK   pProcessRequestCallback;
  void                          *pProcessRequestParam;

  /* User registered build response function for ASDU typeIds
   * not supported by the SCL
   */ 
  S14SESN_BUILD_RESP_CALLBACK    pBuildResponseCallback;
  void                          *pBuildResponseParam;

  /* Last sector polled */
  TMWSCTR *pLastSectorPolled;
  
#if S14DATA_SUPPORT_CICNAWAIT 
  /* Prevent the slave from sending cyclic data before
   * the first CICNA is complete. Not in spec. Only required
   * for some masters.
   */
  TMWTYPES_BOOL cyclicWaitCICNAComplete;
#endif

} S14SESN;

typedef void (*S14SESN_PROCESS_REQUEST_FUNC)(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsgHeader);

typedef TMWTYPES_BOOL (*S14SESN_BUILD_RESPONSE_FUNC)(
  TMWSCTR *pSector, 
  TMWTYPES_BOOL buildResponse);

typedef struct S14FuncEntry {
  TMWTYPES_UCHAR typeId;
  TMWTYPES_SHORT dataSize;
  S14SESN_PROCESS_REQUEST_FUNC pProcessRequestFunc;
  S14SESN_BUILD_RESPONSE_FUNC pBuildResponseFunc;
} S14SESN_FUNC_ENTRY;

#ifdef __cplusplus
extern "C" {
#endif

  void TMWDEFS_GLOBAL s14sesn_openSession(
    TMWSESN *pSession);

  TMWTYPES_BOOL TMWDEFS_GLOBAL s14sesn_closeSession(
    TMWSESN *pSession);

  void TMWDEFS_GLOBAL s14sesn_infoCallback( 
    TMWSESN *pSession, 
    TMWSCL_INFO sesnInfo);

  /* function: s14sesn_sendNegActCon 
   * purpose: Send Negative Activation Confirmation back to master
   * arguments:
   *  pSector
   *  pRxFrame - message that was received, will be echoed back
   *  cot - cause of transmission, I14DEF_COT_NEGATIVE_CONFIRM will be ORed in.
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL s14sesn_sendNegActCon(
    TMWSCTR *pSector, 
    I870UTIL_MESSAGE *pMsg,
    TMWTYPES_UCHAR cot);

  TMWTYPES_BOOL TMWDEFS_GLOBAL s14sesn_parseFrameCallback(
    TMWSESN *pSession, 
    TMWSESN_RX_DATA *pRxFrame,
    const void *pFuncTable);

  TMWTYPES_BOOL TMWDEFS_GLOBAL s14sesn_checkClassCallback( 
    TMWSESN *pSession,
    TMWDEFS_CLASS_MASK classMask, 
    TMWTYPES_BOOL buildResponse,
    const void *pFuncTable);
  
  /* function s14sesn_prepareMessage */
  void TMWDEFS_GLOBAL s14sesn_prepareMessage(
    TMWSESN_TX_DATA *pTxData);
  
  /* function: s14sesn_processRequest */
  /* Empty routine to avoid null pointer in function table */
  void TMWDEFS_CALLBACK s14sesn_processRequest(
    TMWSCTR *pSector, 
    I870UTIL_MESSAGE *pMsg);


#ifdef __cplusplus
}
#endif

#endif /* S14SESN_DEFINED */
