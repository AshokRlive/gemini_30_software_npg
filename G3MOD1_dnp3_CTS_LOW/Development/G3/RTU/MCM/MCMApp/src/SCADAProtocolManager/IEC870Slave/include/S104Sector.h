/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:S104Sector.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief Protocol Sector - TMW artifact for handling part of a session
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   6 Oct 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef IEC104SLAVEPROTOCOLSECTOR_H_
#define IEC104SLAVEPROTOCOLSECTOR_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "S104TMWIncludes.h"
#include "S104Database.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class   S104Session;    /* Forward declaration*/


class S104Sector
{
public:
    S104Sector(S104Session& session);

    virtual ~S104Sector();

    S104Database& getS104Database();

    SCADAP_ERROR openSector(TMWSESN* tmwsesn);

    void closeSector();

    void configure(const S104SCTR_CONFIG& cnfg);

    SCADAP_ERROR getSectorHandle(TMWSCTR** ppSectorHdle)
    {
        *ppSectorHdle = mp_tmwsector;
        return (mp_tmwsector != NULL)? SCADAP_ERROR_NONE : SCADAP_ERROR_NULL_POINTER;
    }

    S104Session& getSession() {return m_session;}

private:
    S104Session&  m_session;      // Owner session reference

    TMWSCTR*            mp_tmwsector;
    S104SCTR_CONFIG*    mp_tmwsectorCnfg;

    S104Database        m_s104db;       // Sector database
    I870DBHandleStr     m_twmdbhandle;  // Database handle passed to TMW lib.
};


#endif /* IEC104SLAVEPROTOCOLSECTOR_H_ */

/*
 *********************** End of file ******************************************
 */
