/*
 * TMWSDNPDatabase.h
 *
 *  Created on: 7 Sep 2011
 *      Author: galli_m
 */

#ifndef TMWSDNPDATABASE_H_
#define TMWSDNPDATABASE_H_

#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/utils/tmwtypes.h"
#include "tmwscl/utils/lucycrypto.h"
#include "../dnpcnfg.h"

#include "lucy_common_defs.h"

typedef struct VPointIdDef
{
    lu_uint16_t group;
    lu_uint16_t ID;
} VPointIdStr;

/*
 * Supported CROB Types
 */
#define SDNPDATA_CROB_CTRL_SUPPORTED ( DNPDEFS_CROB_CTRL_PAIRED_CLOSE |\
                                       DNPDEFS_CROB_CTRL_PAIRED_TRIP  |\
                                       DNPDEFS_CROB_CTRL_LATCH_ON     |\
                                       DNPDEFS_CROB_CTRL_LATCH_OFF     \
                                     )

typedef void* ProtocolManagerInst;

/*
 * Forward Declarations
 */
typedef struct TMWSDNPDatabaseDef TMWSDNPDatabaseStr;
typedef struct TMWSDNPBOutputDef  TMWSDNPBOutputStr;
typedef struct TMWSDNPAOutputDef  TMWSDNPAOutputStr;
typedef struct TMWSDNPCInputDef   TMWSDNPCInputStr;

/*
 * Callback Signatures
 *
 */

///**
// * \brief Signature for the restart callback
// *
// * \param coldRestart TRUE for Cold restart and FALSE for warm restart
// *
// * \return N/A
// */
//typedef void (*CALLBACK_restartSessionDef)(TMWTYPES_BOOL coldRestart);


/**
 * \brief Signature for the Set Date and Time callback
 *
 * \param pNewTime Date & Time in TMW format
 *
 * \return N/A
 */
typedef void (*CALLBACK_setDateTimeDef)(TMWDTIME *pNewTime);

/**
 * \brief Signature for the Get Binary Input callback
 *
 * \param dbPtr         Pointer to the DB Structure
 * \param pointNum      Point number requested
 *
 * \return Binary Input Point Pointer
 */
typedef void* (*CALLBACK_binInGetPointDef)(TMWSDNPDatabaseStr *dbPtr, TMWTYPES_USHORT pointNum);

/**
 * \brief Signature for the Get Binary Output callback
 *
 * \param dbPtr         Pointer to the DB Structure
 * \param pointNum      Point number requested
 *
 * \return Binary Output Point Pointer
 */
typedef void* (*CALLBACK_binOutGetPointDef)(TMWSDNPDatabaseStr *dbPtr, TMWTYPES_USHORT pointNum);

/**
 * \brief Signature for the Get Binary Output Status callback
 *
 * \param dbPtr         Pointer to the DB Structure
 * \param pointNum      Point number requested
 *
 * \return Binary Output Point Status: TMWDEFS_TRUE when online
 */
typedef TMWTYPES_BOOL (*CALLBACK_binOutStatusDef)(TMWSDNPBOutputStr *pointPtr);

/**
 * \brief Signature for the Select Binary Output callback
 *
 * \param pointPtr      Pointer to the Binary Output Point
 * \param controlCode   Type of control
 * \param count         Operation Count
 *
 * \return Error Code
 */
typedef DNPDEFS_CROB_ST (*CALLBACK_binOutSelectDef)(TMWSDNPBOutputStr *pointPtr, TMWTYPES_UCHAR controlCode, TMWTYPES_UCHAR count);

/**
 * \brief Signature for the Operate Binary Output callback
 *
 * \param pointPtr      Pointer to the Binary Output Point
 * \param controlCode   Type of control
 * \param count         Operation Count
 *
 * \return Error Code
 */
typedef DNPDEFS_CROB_ST (*CALLBACK_binOutOperateDef)(TMWSDNPBOutputStr *pointPtr, TMWTYPES_UCHAR controlCode, TMWTYPES_UCHAR count);

typedef void* (*CALLBACK_anlgOutGetPointDef)(TMWSDNPDatabaseStr *dbPtr, TMWTYPES_USHORT pointNum);

typedef DNPDEFS_CROB_ST (*CALLBACK_anlgOutSelectDef)(TMWSDNPAOutputStr *pointPtr, TMWTYPES_ANALOG_VALUE *pValue);

typedef DNPDEFS_CTLSTAT (*CALLBACK_anlgOutOperateDef)(TMWSDNPAOutputStr *pointPtr, TMWTYPES_ANALOG_VALUE *pValue);

/**
 * \brief Signature for the operate switch callback
 *
 * \param handler Protocol Manager handler
 * \param id Switch internal (geminiDB) ID
 * \param open true to open, false to close
 *
 * \return Error Code
 */
//typedef DNPDEFS_CROB_ST (*CALLBACK_operateSwitchDef)(CBHandler handler, TMWTYPES_USHORT id, TMWTYPES_BOOL open);

/**
 * \brief Signature for the Get Analogue Input callback
 *
 * \param dbPtr         Pointer to the DB Structure
 * \param pointNum      Point number requested
 *
 * \return Analogue Input Point Pointer
 */
typedef void* (*CALLBACK_anlgInGetPointDef)(TMWSDNPDatabaseStr *dbPtr, TMWTYPES_USHORT pointNum);

/**
 * \brief Signature for the Get Double Binary Input callback
 *
 * \param dbPtr         Pointer to the DB Structure
 * \param pointNum      Point number requested
 *
 * \return Double Binary Input Point Pointer
 */
typedef void* (*CALLBACK_dblInGetPointDef)(TMWSDNPDatabaseStr *dbPtr, TMWTYPES_USHORT pointNum);

/**
 * \brief Signature for the Get Counter Input callback
 *
 * \param dbPtr         Pointer to the DB Structure
 * \param pointNum      Point number requested
 *
 * \return Counter Input Point Pointer
 */
typedef void* (*CALLBACK_ctrInGetPointDef)(TMWSDNPDatabaseStr* dbPtr, TMWTYPES_USHORT pointNum);

/**
 * \brief Signature for the Get Counter Input callback
 *
 * \param pointNum          Point number requested
 * \param freezeAndClear    Clear after freeze.
 *
 * \return operation result. TMWDEFS_TRUE when successful.
 */
typedef TMWTYPES_BOOL (*CALLBACK_ctrInFreezeDef)(TMWSDNPCInputStr* pointPtr, TMWTYPES_BOOL freezeAndClear);


/**
 * Callback structure.
 *
 * These callbacks are used to forward an operation from the protocol stack
 * to the gemini database
 *
 * The Protocol manager can register its own callbacks here.
 */
typedef struct TMWSDNPCallbacksDef
{
    /** Protocol manager instance */
    ProtocolManagerInst             protocolManager;

    /* TMW-related Callbacks */
    CALLBACK_restartSessionDef      restartSession;
    CALLBACK_setDateTimeDef         setDateTime;
    CALLBACK_binInGetPointDef       binInGetPoint;
    CALLBACK_binOutGetPointDef      binOutGetPoint;
    CALLBACK_binOutStatusDef        binOutGetStatus;
    CALLBACK_binOutSelectDef        binOutSelect;
    CALLBACK_binOutOperateDef       binOutOperate;
    CALLBACK_anlgOutGetPointDef     anlgOutGetPoint;
    CALLBACK_anlgOutSelectDef       anlgOutSelect;
    CALLBACK_anlgOutOperateDef      anlgOutOperate;
    CALLBACK_anlgInGetPointDef      anlgInGetPoint;
    CALLBACK_dblInGetPointDef       dblInGetPoint;
    CALLBACK_ctrInGetPointDef       ctrInGetPoint;
    CALLBACK_ctrInFreezeDef         ctrInFreeze;
    CALLBACK_getLocalState          getLocalState;

    CALLBACK_deleteEventIDDef       deleteEventID;
    CALLBACK_getTimeSyncStatusDef   getTimeSyncStatus;

}TMWSDNPCallbacksStr;

typedef struct TMWSDNPBInputDef
{
    /* variation for object 1 (used only if the global default variation is 0) */
    TMWTYPES_BOOL enabled;
    TMWTYPES_USHORT defaultVariation;
    /* variation for object 2 (used only if the global default variation is 0) */
    TMWTYPES_USHORT eventDefaultVariation;
    TMWDEFS_CLASS_MASK eventClass;
    /* TRUE if part of the class0 message */
    TMWTYPES_BOOL enableClass0;
    /* Only generate events when session is marked as online */
    TMWTYPES_BOOL eventOnlyWhenOnline;
    /* Mirror events to the RTU Event log */
    TMWTYPES_BOOL eventLogStore;


    /*   The following values (or OR'd combinations) are valid for this type:
     *      DNPDEFS_DBAS_FLAG_OFF_LINE - the point is off-line, and the returned
     *        state of this point may not be correct
     *      DNPDEFS_DBAS_FLAG_ON_LINE - the binary input point has been read
     *        successfully
     *      DNPDEFS_DBAS_FLAG_RESTART - the field device that originated the
     *        data object has been restarted. This device may be the deviced
     *        reporting this data object.
     *      DNPDEFS_DBAS_FLAG_COMM_LOST - the device reporting this data object
     *        has lost communication with the originator of the data object
     *      DNPDEFS_DBAS_FLAG_REMOTE_FORCED - the state of the binary object
     *        has been forced to its current state at the originating device
     *      DNPDEFS_DBAS_FLAG_LOCAL_FORCED - the state of the binary object
     *        has been forced to its current state at the device reporting
     *        this data object
     *      DNPDEFS_DBAS_FLAG_CHATTER - the binary input point has been filtered
     *        in order to remove unneeded transitions in the state of the input
     *      DNPDEFS_DBAS_FLAG_BINARY_ON  - the current state of the input (On)
     *      DNPDEFS_DBAS_FLAG_BINARY_OFF - the current state of the input (Off)
     */
    TMWTYPES_UCHAR value;
}TMWSDNPBInputStr;

typedef struct TMWSDNPDBInputDef
{
    TMWTYPES_BOOL enabled;
    /* variation for object 3 (used only if the global default variation is 0) */
    TMWTYPES_USHORT defaultVariation;
    /* variation for object 4 (used only if the global default variation is 0) */
    TMWTYPES_USHORT eventDefaultVariation;
    TMWDEFS_CLASS_MASK eventClass;
    /* TRUE if part of the class0 message */
    TMWTYPES_BOOL enableClass0;
    /* Only generate events when session is marked as online */
    TMWTYPES_BOOL eventOnlyWhenOnline;
    /* Mirror events to the RTU Event log */
    TMWTYPES_BOOL eventLogStore;


    /*   The following values (or OR'd combinations) are valid for this type:
     *      DNPDEFS_DBAS_FLAG_OFF_LINE - the point is off-line, and the returned
     *        state of this point may not be correct
     *      DNPDEFS_DBAS_FLAG_ON_LINE - the binary input point has been read
     *        successfully
     *      DNPDEFS_DBAS_FLAG_RESTART - the field device that originated the
     *        data object has been restarted. This device may be the deviced
     *        reporting this data object.
     *      DNPDEFS_DBAS_FLAG_COMM_LOST - the device reporting this data object
     *        has lost communication with the originator of the data object
     *      DNPDEFS_DBAS_FLAG_REMOTE_FORCED - the state of the binary object
     *        has been forced to its current state at the originating device
     *      DNPDEFS_DBAS_FLAG_LOCAL_FORCED - the state of the binary object
     *        has been forced to its current state at the device reporting
     *        this data object
     *      DNPDEFS_DBAS_FLAG_CHATTER - the binary input point has been filtered
     *        in order to remove unneeded transitions in the state of the input
     *      DNPDEFS_DBAS_FLAG_BINARY_ON  - the current state of the input (On)
     *      DNPDEFS_DBAS_FLAG_BINARY_OFF - the current state of the input (Off)
     */
    TMWTYPES_UCHAR value;   /*Note: contains value AND flags */
}TMWSDNPDBInputStr;

typedef struct TMWSDNPAInputDef
{
    TMWTYPES_BOOL enabled;
    /* variation for object 30 (used only if the global default variation is 0) */
    TMWTYPES_USHORT defaultVariation;
    /* variation for object 32 (used only if the global default variation is 0) */
    TMWTYPES_USHORT eventDefaultVariation;
    TMWDEFS_CLASS_MASK eventClass;
    /* TRUE if part of the class0 message */
    TMWTYPES_BOOL enableClass0;
    /* Only generate events when session is marked as online */
    TMWTYPES_BOOL eventOnlyWhenOnline;
    /* Mirror events to the RTU Event log */
    TMWTYPES_BOOL eventLogStore;

    TMWTYPES_ANALOG_VALUE value;
    TMWTYPES_UCHAR flags;
}TMWSDNPAInputStr;

typedef struct TMWSDNPCInputDef
{
    TMWTYPES_BOOL enabled;
    VPointIdStr vPointRef;   //Reference to the Virtual Point

    /* variation for object 20 (used only if the global default variation is 0) */
    TMWTYPES_USHORT defaultVariation;
    /* variation for object 22 (used only if the global default variation is 0) */
    TMWTYPES_USHORT eventDefaultVariation;
    /* Event class 0/1/2/3 */
    TMWDEFS_CLASS_MASK eventClass;
    /* TRUE if part of the class0 message */
    TMWTYPES_BOOL enableClass0;
    /* Only generate events when session is marked as online */
    TMWTYPES_BOOL eventOnlyWhenOnline;
    /* Mirror events to the RTU Event log */
    TMWTYPES_BOOL eventLogStore;

    /* variation for object 21 (used only if the global default variation is 0) */
    TMWTYPES_USHORT frzDefaultVariation;
    /* variation for object 23 (used only if the global default variation is 0) */
    TMWTYPES_USHORT frzEventDefaultVariation;
    /* Event class 0/1/2/3 */
    TMWDEFS_CLASS_MASK frzEventClass;
    /* TRUE if part of the class0 message */
    TMWTYPES_BOOL frzEnableClass0;
    /* Only generate events when session is marked as online */
    TMWTYPES_BOOL frzEventOnlyWhenOnline;

    /*   The following values (or OR'd combinations) are valid for this type:
     *      DNPDEFS_DBAS_FLAG_OFF_LINE - the point is off-line, and the returned
     *        state of this point may not be correct
     *      DNPDEFS_DBAS_FLAG_ON_LINE - the binary input point has been read
     *        successfully
     *      DNPDEFS_DBAS_FLAG_RESTART - the field device that originated the
     *        data object has been restarted. This device may be the deviced
     *        reporting this data object.
     *      DNPDEFS_DBAS_FLAG_COMM_LOST - the device reporting this data object
     *        has lost communication with the originator of the data object
     *      DNPDEFS_DBAS_FLAG_REMOTE_FORCED - the state of the binary object
     *        has been forced to its current state at the originating device
     *      DNPDEFS_DBAS_FLAG_LOCAL_FORCED - the state of the binary object
     *        has been forced to its current state at the device reporting
     *        this data object
     *      DNPDEFS_DBAS_FLAG_CNTR_ROLLOVER - the counter has been rolled over.
     *        This flag is deprecated in the latest versions of the DNP specs.
     *      DNPDEFS_DBAS_FLAG_DISCONTINUITY  - Discontinuity indication.
     *        Indicates that a counter value cannot be referenced to a previous
     *        counter value. This means the count value has become invalid for
     *        any reason, such as offline or invalid source, in the time from
     *        previous measurement to the current one.
     *      DNPDEFS_DBAS_FLAG_OVER_RANGE - Gemini 2.5 uses this flag as rollover
     *        indication. Here for backwards compatibility.
     */
    TMWTYPES_ULONG value;           //Counter normal value  *Non-config item*
    TMWTYPES_ULONG frzValue;        //Frozen value          *Non-config item*
    TMWTYPES_UCHAR flags;           //Point flags           *Non-config item*

    TMWTYPES_BOOL frozenFlag;       //Type of counter. Used to specify DNP Group 20 or 21
    TMWTYPES_BOOL useRolloverFlag;  //Configure to use deprecated ROLLOVER flag

    /* Pointer to the callback structure */
    TMWSDNPCallbacksStr* callbacks;

    /* Pointer to the Session Pointer */
    TMWSESN **sclSessionPtrPtr;

}TMWSDNPCInputStr;

typedef struct TMWSDNPBOutputDef
{
    /* variation for object 10 (used only if the global default variation is 0) */
    TMWTYPES_USHORT defaultVariation;

    TMWTYPES_BOOL enabled;

    /* TRUE if part of the class0 message */
    TMWTYPES_BOOL enableClass0;

    /* Types of commands accepted. Possible values:
     *  + Select Before Operate (SBO)
     *  + Direct Operate (DO)
     *  + Both
     */
    TMWTYPES_BOOL acceptSBO;
    TMWTYPES_BOOL acceptDO;

    /*   The following values (or OR'd combinations) are valid for this type:
     *      DNPDEFS_DBAS_FLAG_OFF_LINE - the point is off-line, and the returned
     *        state of this point may not be correct
     *      DNPDEFS_DBAS_FLAG_ON_LINE - the binary input point has been read
     *        successfully
     *      DNPDEFS_DBAS_FLAG_RESTART - the field device that originated the
     *        data object has been restarted. This device may be the deviced
     *        reporting this data object.
     *      DNPDEFS_DBAS_FLAG_COMM_LOST - the device reporting this data object
     *        has lost communication with the originator of the data object
     *      DNPDEFS_DBAS_FLAG_REMOTE_FORCED - the state of the binary object
     *        has been forced to its current state at the originating device
     *      DNPDEFS_DBAS_FLAG_LOCAL_FORCED - the state of the binary object
     *        has been forced to its current state at the device reporting
     *        this data object
     *      DNPDEFS_DBAS_FLAG_CHATTER - the binary input point has been filtered
     *        in order to remove unneeded transitions in the state of the input
     *      DNPDEFS_DBAS_FLAG_BINARY_ON  - the current state of the input (On)
     *      DNPDEFS_DBAS_FLAG_BINARY_OFF - the current state of the input (Off)
     */
    TMWTYPES_UCHAR value;

    /* Internal ID of the switch associated to the protocol point */
    TMWTYPES_UCHAR switchID;

    /* Pointer to the callback structure */
    TMWSDNPCallbacksStr *callbacks;

    /* Pointer to the Session Pointer */
    TMWSESN **sclSessionPtrPtr;

}TMWSDNPBOutputStr;

typedef struct TMWSDNPAOutputDef
{
    TMWTYPES_BOOL enabled;

    /* variation for object 40 (used only if the global default variation is 0) */
    TMWTYPES_USHORT defaultVariation;
    /* variation for object 42 (used only if the global default variation is 0) */
    TMWTYPES_USHORT eventDefaultVariation;
    /* Event class 0/1/2/3 */
    TMWDEFS_CLASS_MASK eventClass;
    /* TRUE if part of the class0 message */
    TMWTYPES_BOOL enableClass0;
    /* Only generate events when session is marked as online */
    TMWTYPES_BOOL eventOnlyWhenOnline;
    /* Mirror events to the RTU Event log */
    TMWTYPES_BOOL eventLogStore;

    TMWTYPES_ANALOG_VALUE value;    //Analogue value    *Non-config item*
    TMWTYPES_UCHAR flags;           //point flags       *Non-config item*

    /* Internal ID of the logic associated to the protocol point */
    TMWTYPES_UCHAR logicGroup;

    /* Protocol DNP3 ID */
    TMWTYPES_UINT dnp3Id;

    /* Pointer to the callback structure */
    TMWSDNPCallbacksStr *callbacks;

    /* Pointer to the Session Pointer */
    TMWSESN **sclSessionPtrPtr;

}TMWSDNPAOutputStr;


typedef struct TMWSDNPBInputListDef
{
    TMWTYPES_USHORT size;

    TMWSDNPBInputStr *list;
}TMWSDNPBInputListStr;

typedef struct TMWSDNPDBInputListDef
{
    TMWTYPES_USHORT size;

    TMWSDNPDBInputStr *list;
}TMWSDNPDBInputListStr;

typedef struct TMWSDNPAInputListDef
{
    TMWTYPES_USHORT size;

    TMWSDNPAInputStr *list;
}TMWSDNPAInputListStr;

typedef struct TMWSDNPCInputListDef
{
    TMWTYPES_USHORT size;

    TMWSDNPCInputStr *list;
}TMWSDNPCInputListStr;

typedef struct TMWSDNPBOutputListDef
{
    TMWTYPES_USHORT size;

    TMWSDNPBOutputStr *list;
}TMWSDNPBOutputListStr;

typedef struct TMWSDNPAOutputListDef
{
    TMWTYPES_USHORT size;

    TMWSDNPAOutputStr *list;
}TMWSDNPAOutputListStr;


typedef struct TMWSDNPDatabaseDef
{
    void    *DNP3ProtocolSessionPtr;

    TMWSESN *sclSessionPtr;

    /** The following bits should be set by the user if appropriate:
      *  DNPDEFS_IIN_TROUBLE     - should be set when an abnormal condition exists,
      *                            such as hardware problems. Only set this if
      *                            another IIN bit does not indicate this condition.
      *  DNPDEFS_IIN_LOCAL       - should be set if any output point is in the local
      *                            operation mode
      *  DNPDEFS_IIN_BAD_CONFIG  - should be set when a corrupt configuration is
      *                            detected. Setting this bit is optional.
      */
    //TMWTYPES_USHORT IIN;

    /**
     * Enabled unsolicited class mask
     */
//    TMWDEFS_CLASS_MASK unsolEventMask;

    /** Indicate that a read of events and static data is in progress */
//    TMWTYPES_BOOL inProgress;

    /* Lucy Session Setting to enable/disable Controls (Select/Op./Dir.Op.) using the Broadcast Address */
    TMWTYPES_BOOL allowControlOnBrdCstAddr;

    TMWSDNPBInputListStr binaryInput;
    TMWSDNPDBInputListStr dbinaryInput;
    TMWSDNPAInputListStr analogueInput;
    TMWSDNPCInputListStr counterInput;
    TMWSDNPBOutputListStr binaryOutput;
    TMWSDNPAOutputListStr analogueOutput;

    TMWSDNPCallbacksStr callbacks;

#if DNPCNFG_SUPPORT_AUTHENTICATION
    /* Secure Authentication */
    LUCYCRYPTO_USER_DB   *pUserDB;
#endif /* DNPCNFG_SUPPORT_AUTHENTICATION */

}TMWSDNPDatabaseStr;


#endif /* TMWSDNPDATABASE_H_ */
