/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:S101Channel.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief Protocol Physical Channel
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19 Jul 2016     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef S101CHANNEL_H_
#define S101CHANNEL_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "S101TMWIncludes.h"
#include "S101Session.h"
#include "AbstractSlaveProtocolChannel.h"
#include "GeminiDatabase.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
class S101Protocol; // Forward declaration


class S101Channel : public AbstractSlaveProtocolChannel
{
public:
    struct S101ChannelConfig
    {
        I870CHNL_CONFIG    pChnlConfig;
        I870LNK1_CONFIG    pLinkConfig;
        TMWPHYS_CONFIG     pPhysConfig;
        LINIO_CONFIG       pIOConfig  ;
        TMWTARG_CONFIG     pTMWTargConfig;
    };

public:
    S101Channel(GeminiDatabase& g3database, S101Protocol* protocol);
    virtual ~S101Channel();

    SCADAP_ERROR openChannel(TMWAPPL* tmwappl);

    void closeChannel();

    void configure(const S101ChannelConfig& channelConfig);

    void addSession(S101Session* sessionPtr);


    /**Override*/
    virtual bool         connectChannel()
    {
        //TODO to be implemented
        return false;
    }

    /**Override*/
    virtual lu_uint32_t  getConnectionTimeout()
    {
        //TODO to be implemented
        return 0;
    }

    GeminiDatabase&     getG3Database(){return m_g3db;}

    const S101Protocol* const getProtocol() {return mp_protocol;}

private:
    /**Override*/
    virtual void         tickEvent()
    {
        //TODO to be implemented
    };

    /**Override*/
    virtual lu_uint32_t  countSessionEvents(lu_uint8_t classMask, bool countAll)
    {
        //TODO to be implemented
        return 0;
    };


private:
    typedef std::vector<S101Session*> S101SessionVect;
    typedef std::vector<TMWCHNL*>     RdcyChnlVect;

    const S101Protocol* const   mp_protocol;
    GeminiDatabase&     m_g3db;
    TMWCHNL*            mp_tmwchnl;
    S101ChannelConfig*  mp_tmwchnlCnfg;
    S101SessionVect     m_sessions;

};


#endif /* S101CHANNEL_H_ */

/*
 *********************** End of file ******************************************
 */
