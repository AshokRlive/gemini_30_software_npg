/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: lnk4rdcy.c
 * description: IEC 60870-5-104 profile Link Layer Redundancy Implementation
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/utils/tmwtarg.h"
#include "tmwscl/utils/tmwscl.h"

#include "tmwscl/i870/lnk4rdcy.h"
#include "tmwscl/i870/i870chnl.h"
#include "tmwscl/i870/i104chnl.h"
#include "tmwscl/i870/i870dia4.h"
#include "tmwscl/i870/i870mem.h"
#include "tmwscl/i870/i870mem4.h"

/* Local function declarations */

/* function _beforeTxCallback */
static void TMWDEFS_CALLBACK _beforeTxCallback(
  TMWSESN_TX_DATA *pTxData)
{
  i870chnl_beforeTxCallback(pTxData);
}

/* function _afterTxCallback */
static void TMWDEFS_CALLBACK _afterTxCallback(
  TMWSESN_TX_DATA *pTxData)
{
  i870chnl_afterTxCallback(pTxData);
}

/* function _failedTxCallback */
static void TMWDEFS_CALLBACK _failedTxCallback(
  TMWSESN_TX_DATA *pTxData)
{
  i870chnl_failedTxCallback(pTxData);
}

/* function: _parseFrameCallback */
static void TMWDEFS_CALLBACK _parseFrameCallback(
  void *pParam,
  TMWSESN *pSession,
  TMWSESN_RX_DATA *pRxFrame)
{
  i870chnl_parseFrameCallback(pParam, pSession, pRxFrame);
}

/* function: _checkDataAvailable */
static TMWTYPES_BOOL TMWDEFS_CALLBACK _checkDataAvailable(
  void *pParam,
  TMWSESN *pSession,
  TMWDEFS_CLASS_MASK classMask,
  TMWTYPES_BOOL buildMessage)
{
  return (i870chnl_checkDataAvailable(pParam, pSession, classMask, buildMessage));
}

/* function: _getSessions
 * purpose: return list of sessions on this channel
 * arguments:
 *  pContext - link layer context
 * returns:
 *  pointer to list of sessions
 */
static TMWDLIST * TMWDEFS_CALLBACK _getSessions(
  TMWLINK_CONTEXT *pContext)
{
  return(tmwlink_getSessions(pContext));
}

/* To turn on verbose redundancy messages */
/*#define VERBOSE 1*/

/* function: _diagInfo
 * purpose: output diagnostic information
 * arguments:
 *   *pBuf - pointer to string
 * returns
 *  void
 */
static void TMWDEFS_LOCAL _diagInfo(
  LNK4RDCY_CONNECT_CONTEXT *pRdntConnect,
  const char *pBuf)
{
#if TMWCNFG_SUPPORT_DIAG && VERBOSE
  TMWDIAG_ANLZ_ID id;
  const char *name;
  char buf[256];

  if(tmwdiag_initId(&id, pRdntConnect->pLnk4Channel, TMWDEFS_NULL, TMWDEFS_NULL, TMWDIAG_ID_LINK | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }
  
  name = tmwchnl_getChannelName(pRdntConnect->pLnk4Channel);
  tmwtarg_snprintf(buf, sizeof(buf), "%s: %s\n", name, pBuf);

  tmwdiag_putLine(&id, buf);
  tmwdiag_putLine(&id, "\n");
#else
TMWTARG_UNUSED_PARAM(pRdntConnect);
TMWTARG_UNUSED_PARAM(pBuf);
#endif
}

/* function: _addRedundantChannel
 * purpose: Add an open channel to this redundancy group
 * arguments:
 *  pRdcyGroupChannel - pointer to redundancy group group channel
 *  pRdntChannel - pointer to redundant channel
 * returns
 *  pointer to redundant connect context LNK4RDCY_CONNECT_CONTEXT
 */
static LNK4RDCY_CONNECT_CONTEXT * TMWDEFS_LOCAL _addRedundantChannel(
  TMWCHNL *pRdcyGroupChannel,
  TMWCHNL *pRdntChannel)
{
  LNK4RDCY_GROUP_CONTEXT *pRdcyGroup = (LNK4RDCY_GROUP_CONTEXT *)pRdcyGroupChannel;
  LNK4RDCY_CONNECT_CONTEXT *pRdntConnect = (LNK4RDCY_CONNECT_CONTEXT *)
    i870mem4_alloc(I870MEM4_RDCY_CONNECT_TYPE);

  if(pRdntConnect != TMWDEFS_NULL)
  {
    i870lnk4_enableRedundancy(pRdntChannel->pLinkContext);
    
    pRdntConnect->state = LNK4RDCY_STATE_OFFLINE;
    pRdntConnect->pLnk4Channel = pRdntChannel; 
   
    tmwdlist_addEntry(&pRdcyGroup->rdntChannels, (TMWDLIST_MEMBER *)pRdntConnect); 
    
    if(pRdcyGroup->pCurrentLink == TMWDEFS_NULL)
    {
      pRdcyGroup->pCurrentLink = pRdntConnect;
      /* pLink was set up to point to local version of functions */
      pRdcyGroup->tmw.pLinkContext = pRdntConnect->pLnk4Channel->pLinkContext;
      pRdcyGroup->tmw.pPhys = pRdntConnect->pLnk4Channel->pPhys;
      pRdcyGroup->tmw.pPhysContext = pRdntConnect->pLnk4Channel->pPhysContext;
    }
    else
    {
      /* if session is already open, call opensession for new channel */ 
      if(pRdcyGroup->pSession != TMWDEFS_NULL) 
      {
        if(pRdntChannel->pLink->pLinkOpenSession(
          (TMWLINK_CONTEXT *)(pRdntChannel->pLinkContext), pRdcyGroup->pSession, TMWDEFS_NULL) != TMWDEFS_TRUE)
        {
          i870mem4_free(pRdntConnect);
          return(TMWDEFS_NULL);
        }
      }
    }
    return(pRdntConnect);
  }
  return(TMWDEFS_NULL);
}

/* function: _openSession
 * purpose: opens a session on this redundancy group
 *          Passed on to link layer channels below this layer
 * arguments:
 *  pContext - link layer context returned from lnk4rdcy_initChannel
 *  pSession - pointer to session to open
 * returns
 *  TMWDEFS_TRUE if successful, else TMWDEFS_FALSE
 */
static TMWTYPES_BOOL TMWDEFS_LOCAL  _openSession(
  TMWLINK_CONTEXT *pContext,
  TMWSESN *pSession,
  void *pConfig)
{
  LNK4RDCY_CONNECT_CONTEXT *pConnectContext = TMWDEFS_NULL;
  LNK4RDCY_GROUP_CONTEXT *pRdcyContext = (LNK4RDCY_GROUP_CONTEXT *)pSession->pChannel;
  TMWTYPES_BOOL success = TMWDEFS_TRUE;

  /* Get link contexts from list of redundant channels */
  TMWTARG_UNUSED_PARAM(pContext);

  /* If there are no channels open don't allow session to open */
  if(pRdcyContext->pCurrentLink == TMWDEFS_NULL)
    return(TMWDEFS_FALSE);

  /* If a session has already been opened on this redundancy group 
   * return failure 
   */
  if(pRdcyContext->pSession != TMWDEFS_NULL)
    return(TMWDEFS_FALSE);

  /* Save pointer to session and configuration */
  pRdcyContext->pSession = pSession;
  
  /* send openSession to all of the links */
  while((pConnectContext = (LNK4RDCY_CONNECT_CONTEXT *)tmwdlist_getAfter(
    &pRdcyContext->rdntChannels, (TMWDLIST_MEMBER *)pConnectContext)) != TMWDEFS_NULL)
  {
    if(pConnectContext->pLnk4Channel->pLink->pLinkOpenSession(
      (TMWLINK_CONTEXT *)(pConnectContext->pLnk4Channel->pLinkContext), pSession, pConfig) != TMWDEFS_TRUE)
    {
      /* If any fail, return failure */
      success = TMWDEFS_FALSE;
      pRdcyContext->pSession = TMWDEFS_NULL;
      break;
    }
  }

  return(success);
}

/* function: _closeSession
 * purpose: close the specified session,
 *          This close is passed on to all of the link layer contexts below 
 *          this redundancy group
 * arguments:
 *  pContext - link layer context returned from lnk4rdcy_initChannel
 *  pSession - pointer to session to close
 * returns
 *  TMWDEFS_TRUE if successful, else TMWDEFS_FALSE
 */
static TMWTYPES_BOOL TMWDEFS_LOCAL  _closeSession(
  TMWLINK_CONTEXT *pContext,
  TMWSESN *pSession)
{
  LNK4RDCY_CONNECT_CONTEXT *pConnectContext = TMWDEFS_NULL;
  LNK4RDCY_GROUP_CONTEXT *pRdcyContext = (LNK4RDCY_GROUP_CONTEXT *)pSession->pChannel;

  /* Get link contexts from list of redundant channels */
  TMWTARG_UNUSED_PARAM(pContext);

  /* Send closeSession to all of the link contexts */
  while((pConnectContext = (LNK4RDCY_CONNECT_CONTEXT *)tmwdlist_getAfter(
    &pRdcyContext->rdntChannels, (TMWDLIST_MEMBER *)pConnectContext)) != TMWDEFS_NULL)
  {
    pConnectContext->pLnk4Channel->pLink->pLinkCloseSession((TMWLINK_CONTEXT *)pConnectContext->pLnk4Channel->pLinkContext, pSession);
  }

  pRdcyContext->pSession = TMWDEFS_NULL;

  return(TMWDEFS_TRUE);
}

/* function: _getOnlineLink
 * purpose:  Search the connect contexts for this redundancy group
 *           to see if any link channels are ONLINE
 * arguments:
 *   pRdcyGroup - pointer to context returned by lnk4rdcy_initChannel
 * returns
 *  void
 */
static LNK4RDCY_CONNECT_CONTEXT * TMWDEFS_LOCAL _getOnlineLink(
  LNK4RDCY_GROUP_CONTEXT *pRdcyGroup)
{
  LNK4RDCY_CONNECT_CONTEXT *pRdntConnect = TMWDEFS_NULL;

  while((pRdntConnect = (LNK4RDCY_CONNECT_CONTEXT *)tmwdlist_getAfter(
    &pRdcyGroup->rdntChannels, (TMWDLIST_MEMBER *)pRdntConnect)) != TMWDEFS_NULL)
  {
    if(pRdntConnect->state == LNK4RDCY_STATE_INACTIVE)
    {
      return pRdntConnect;
    }
  }

  return TMWDEFS_NULL;
}

/* function: _deActivateLink
 * purpose:  Deactivate the currently active link
 *           and tell it to cancel all of the outstanding messages 
 * arguments:
 *   pRdcyGroup - pointer to context returned by lnk4rdcy_initChannel
 * returns
 *  void
 */
static void TMWDEFS_LOCAL _deActivateLink(
  LNK4RDCY_GROUP_CONTEXT *pRdcyGroup)
{
  I870CHNL_TX_DATA *pTxData = TMWDEFS_NULL;

  /* Tell link layer to cancel all of the messages it has outstanding 
   * We will leave these in rdcy group queue so they can be resent on an active channel
   */
  i870lnk4_cancelRedundantData((I870LNK4_CONTEXT *)pRdcyGroup->tmw.pLinkContext);

  while((pTxData = (I870CHNL_TX_DATA *)tmwdlist_getAfter(
    &pRdcyGroup->tmw.messageQueue, (TMWDLIST_MEMBER *)pTxData)) != TMWDEFS_NULL)
  {
    /* Mark it not sent, so it may be sent again on new active link */
    pTxData->state = I870CHNL_NOT_SENT;
  }
  /* Controlling side must send STOP DT */
  if(pRdcyGroup->isControlling == TMWDEFS_TRUE)
  {
    /* Only send STOPDT if connected, otherwise STOPDT will timeout */
    if(pRdcyGroup->pCurrentLink->state != LNK4RDCY_STATE_OFFLINE)
    {
      /* Make the current active link inactive */
      i870lnk4_sendStopDt(pRdcyGroup->tmw.pLinkContext);
    }
  }
  else
  { 
    /* spec says if a start dt is received on a slave and there is another started channel (has 
     * not received a stopdt), close that redundant channel that was started.
     * Setting it to stopped is not sufficient.
     */
    if(((I870LNK4_CONTEXT *)pRdcyGroup->tmw.pLinkContext)->linkActiveState != I870LNK4_ACTIVE_STOPPED)
    {
      i870lnk4_restartRedundantLink((I870LNK4_CONTEXT *)pRdcyGroup->tmw.pLinkContext);
    }
  }
}

/* Send START DT on specified channel */
static void TMWDEFS_CALLBACK _sendStartDtFunc(void *pCallbackParam)
{ 
  i870lnk4_sendStartDt((TMWLINK_CONTEXT *)pCallbackParam);
}

/* function: _switchOver
 * purpose:  Deactivate the currently active link
 *           and make the specified connect context active
 * arguments:
 *   pRdcyGroup - pointer to context returned by lnk4rdcy_initChannel
 *   pNewConnect - pointer to redundant connect context to make active
 * returns
 *  void
 */
static void TMWDEFS_LOCAL _switchOver(
  LNK4RDCY_GROUP_CONTEXT *pRdcyGroup,
  LNK4RDCY_CONNECT_CONTEXT *pNewConnect,
  TMWTYPES_BOOL manual)
{ 
  _deActivateLink(pRdcyGroup);

  /* Update the link and physical pointers but don't overwrite the 
   * pLink functions, these specify local _openSession and _closeSession 
   */
  pRdcyGroup->pCurrentLink = pNewConnect; 
  pRdcyGroup->tmw.pLinkContext = pNewConnect->pLnk4Channel->pLinkContext;
  pRdcyGroup->tmw.pPhys = pNewConnect->pLnk4Channel->pPhys;
  pRdcyGroup->tmw.pPhysContext = pNewConnect->pLnk4Channel->pPhysContext;

  /* Controlling side must send START DT */
  if(pRdcyGroup->isControlling == TMWDEFS_TRUE)
  { 
    if(manual)
    {
      /* Delay this to make sure other end sees the STOPDT on the other connection first */ 
      tmwtimer_start(&pRdcyGroup->delayTimer,
        100, (TMWCHNL*)pRdcyGroup, _sendStartDtFunc, pNewConnect->pLnk4Channel->pLinkContext); 
    }
    else
    {
      i870lnk4_sendStartDt(pNewConnect->pLnk4Channel->pLinkContext);
    }
  }

  /* Tell user this is the active connection channel */
  TMWCHNL_STAT_CALLBACK_FUNC(pNewConnect->pLnk4Channel,
    TMWCHNL_STAT_ACTIVE, TMWDEFS_NULL);
}

/* function: _processRedundancyInfo
 * purpose: process session info (events) from the link layer.
 *          Decide what action to take, ie. whether to switch
 *          to a redundant link/channel and then pass the information
 *          on up to the application if appropriate
 * arguments:
 *  pCallbackParam   - pointer to LNK4RDCY_CONNECT_CONTEXT specifying which
 *    connection/link layer instance this information is from.
 *  pSession - pointer to session
 *  sesnInfo - event such as ONLINE, OFFLINE, STARTDT, STOPDT
 * returns
 *  void
 */
static void TMWDEFS_LOCAL _processRdcyInfoCallback(
  void        *pCallbackParam,
  TMWSESN     *pSession,
  TMWSCL_INFO  sesnInfo)
{
  LNK4RDCY_CONNECT_CONTEXT *pRdntConnect = (LNK4RDCY_CONNECT_CONTEXT *)pCallbackParam;  
  LNK4RDCY_GROUP_CONTEXT *pRdcyGroup = (LNK4RDCY_GROUP_CONTEXT *)pSession->pChannel;

  switch(sesnInfo)
  {
    case(TMWSCL_INFO_OFFLINE):
    {
      _diagInfo(pRdntConnect, "REDUNDANT OFFLINE received ");

      /* Mark this link offline */
      pRdntConnect->state = LNK4RDCY_STATE_OFFLINE;
      
      /* If this was not the current link for this redundancy group
       * just return             
       */
      if(pRdcyGroup->pCurrentLink != pRdntConnect)
      {
        return;
      }

      /* On the controlling side, if any other link in this redundancy  
       * group is online, switch to it.                            
       */
      if(pRdcyGroup->isControlling == TMWDEFS_TRUE)
      {
        LNK4RDCY_CONNECT_CONTEXT  *pNewLink = _getOnlineLink(pRdcyGroup);
        if(pNewLink != TMWDEFS_NULL)
        {   
          _diagInfo(pRdntConnect,"REDUNDANT automatic switch");
          _switchOver(pRdcyGroup, pNewLink, TMWDEFS_FALSE);
          return;
        }
      }
       
      /* Since all links are now offline, if we were previously not offline
       * notify the application                                             
       */
      if(pRdcyGroup->state != LNK4RDCY_STATE_OFFLINE)
      {
        I870SESN *pI870Session = (I870SESN *)pSession;
        if(pI870Session->pProcessInfoFunc != TMWDEFS_NULL)
          pI870Session->pProcessInfoFunc(pSession, TMWSCL_INFO_OFFLINE);
        
        tmwsesn_setOnline(pSession, TMWDEFS_FALSE);
      }
 
      pRdcyGroup->state  = LNK4RDCY_STATE_OFFLINE;

      break;
    }

    case(TMWSCL_INFO_ONLINE):
    {
      _diagInfo(pRdntConnect,"REDUNDANT ONLINE received ");

      /* If this connection was offline process ONLINE event. */
      if(pRdntConnect->state == LNK4RDCY_STATE_OFFLINE)
      {
        pRdntConnect->state = LNK4RDCY_STATE_INACTIVE;

        /* If redundancy group was offline, make it inactive and tell application
         * it is online. 
         */
        if(pRdcyGroup->state == LNK4RDCY_STATE_OFFLINE)
        {
          I870SESN *pI870Session = (I870SESN *)pSession;
          pRdcyGroup->state = LNK4RDCY_STATE_INACTIVE;
        
          if(pI870Session->pProcessInfoFunc != TMWDEFS_NULL)
            pI870Session->pProcessInfoFunc(pSession, TMWSCL_INFO_ONLINE);

          tmwsesn_setOnline(pSession, TMWDEFS_TRUE);

          /* If controlling, switch to this link and send STARTDT to cause switch 
           * on remote controlled system.                                         
           */
          if(pRdcyGroup->isControlling == TMWDEFS_TRUE)
          {
            if(pRdcyGroup->pCurrentLink != pRdntConnect)
            {
              _switchOver(pRdcyGroup, pRdntConnect, TMWDEFS_FALSE);
            }
            else
            {           
              i870lnk4_sendStartDt(pRdntConnect->pLnk4Channel->pLinkContext);
                          
              /* Tell user this is the active connection channel */
              TMWCHNL_STAT_CALLBACK_FUNC(pRdntConnect->pLnk4Channel,
                TMWCHNL_STAT_ACTIVE, TMWDEFS_NULL);
            }
          }
        }
      }
      break;
    }

    case(TMWSCL_INFO_STARTDT):
    {
      _diagInfo(pRdntConnect,"REDUNDANT STARTDT received ");

      /* Mark this connection Active */
      pRdntConnect->state = LNK4RDCY_STATE_ACTIVE;

      /* if the new link was not the current link then switch to it */
      if(pRdntConnect != pRdcyGroup->pCurrentLink)
      {
        _switchOver(pRdcyGroup, pRdntConnect, TMWDEFS_FALSE);
      }
      else 
      {
        /* Tell user this is the active connection channel */
        TMWCHNL_STAT_CALLBACK_FUNC(pRdntConnect->pLnk4Channel,
          TMWCHNL_STAT_ACTIVE, TMWDEFS_NULL);
      }

      pRdcyGroup->state = LNK4RDCY_STATE_ACTIVE;

      /* Application does not currently care about STARTDT */
      break;
    }

    case(TMWSCL_INFO_STOPDT):
    {
      _diagInfo(pRdntConnect,"REDUNDANT STOPDT received ");

      /* Mark this link inactive */
      if(pRdntConnect->state == LNK4RDCY_STATE_ACTIVE)
        pRdntConnect->state = LNK4RDCY_STATE_INACTIVE;

      /* Application does not currently care about STOPDT */
      break;
    }

    default:
      break;
  }
} 

/* function: lnk4rdcy_initConfig */
void TMWDEFS_GLOBAL lnk4rdcy_initConfig(
    LNK4RDCY_CONFIG *pConfig)
{
  pConfig->incrementalTimeout = TMWDEFS_SECONDS(30);
  pConfig->isControlling = TMWDEFS_TRUE;
  pConfig->pStatCallback = TMWDEFS_NULL;
  pConfig->pStatCallbackParam = TMWDEFS_NULL;
}

/* function: lnk4rdcy_initRdcyGroup */
TMWCHNL * TMWDEFS_GLOBAL lnk4rdcy_initRdcyGroup(
  const LNK4RDCY_CONFIG *pRdcyConfig)
{ 
  static const TMWLINK_INTERFACE _lnk4rdcyInterface = {
    (TMWLINK_OPEN_SESSION_FUNC)_openSession,
    (TMWLINK_CLOSE_SESSION_FUNC)_closeSession,
    (TMWLINK_GET_SESSIONS_FUNC)_getSessions,
    (TMWLINK_DATA_READY_FUNC)i870lnk4_checkDataReady,
    (TMWLINK_SET_CALLBACKS_FUNC)tmwlink_setCallbacks,
    (TMWLINK_TRANSMIT_FUNC)i870lnk4_transmitFrame,
    (TMWLINK_UPDATE_MSG_FUNC)i870lnk4_updateMsg,
    (TMWLINK_CANCEL_FUNC)i870lnk4_cancelFrame
  };

  LNK4RDCY_GROUP_CONTEXT *pRdcyGroup;
  
  if(!tmwappl_getInitialized(TMWAPPL_INIT_I870))
  {
    if(!i870mem_init(TMWDEFS_NULL))
      return(TMWDEFS_NULL);

#if TMWCNFG_SUPPORT_DIAG
    i870diag_init();
#endif
    tmwappl_setInitialized(TMWAPPL_INIT_I870);
  } 
  
  if(!tmwappl_getInitialized(TMWAPPL_INIT_104))
  {
    if(!i870mem4_init(TMWDEFS_NULL))
      return(TMWDEFS_NULL);

#if TMWCNFG_SUPPORT_DIAG
    i870dia4_init();
#endif   
    tmwappl_setInitialized(TMWAPPL_INIT_104);
  }

  pRdcyGroup = (LNK4RDCY_GROUP_CONTEXT *)
    i870mem4_alloc(I870MEM4_RDCY_GROUP_TYPE);

  if(pRdcyGroup != TMWDEFS_NULL)
  {
    I870CHNL_CONFIG chnlConfig;
    chnlConfig.incrementalTimeout = pRdcyConfig->incrementalTimeout;

    i870chnl_openChannel((TMWCHNL *)pRdcyGroup, &chnlConfig);
    tmwchnl_initChannel(TMWDEFS_NULL, (TMWCHNL *)pRdcyGroup, 
      pRdcyConfig->pStatCallback, pRdcyConfig->pStatCallbackParam,
      TMWDEFS_NULL, TMWDEFS_NULL, TMWDEFS_NULL, TMWDEFS_NULL, TMWDEFS_TRUE);
    
    tmwdlist_initialize(&pRdcyGroup->rdntChannels);
  
    pRdcyGroup->state         = LNK4RDCY_STATE_OFFLINE;
    pRdcyGroup->isControlling = pRdcyConfig->isControlling;
    pRdcyGroup->pSession      = TMWDEFS_NULL;
    pRdcyGroup->pCurrentLink  = TMWDEFS_NULL;

    pRdcyGroup->tmw.pLink = &_lnk4rdcyInterface;
    pRdcyGroup->isControlling = pRdcyConfig->isControlling;

    tmwtimer_init(&pRdcyGroup->delayTimer);

 
    return((TMWCHNL *)pRdcyGroup);
  }

  return(TMWDEFS_NULL);
}

/* function: lnk4rdcy_deleteRdcyGroup */
TMWTYPES_BOOL TMWDEFS_GLOBAL lnk4rdcy_deleteRdcyGroup(
  TMWCHNL *pRdcyGroupChannel)
{
  I870CHNL_TX_DATA *pTxData;
  LNK4RDCY_GROUP_CONTEXT *pRdcyGroup = (LNK4RDCY_GROUP_CONTEXT *)pRdcyGroupChannel;

  /* If the session is still open, or there are redundant channels still attached 
   * return failure
   */
  if((pRdcyGroup->pSession != TMWDEFS_NULL)
    ||(tmwdlist_size(&pRdcyGroup->rdntChannels) >0))
  {
    return(TMWDEFS_FALSE);
  }

  /* Cancel timer */
  tmwtimer_cancel(&pRdcyGroupChannel->incrementalTimer);
  tmwtimer_cancel(&pRdcyGroup->delayTimer);

  /* Remove any Tx Datas left on the queue, cancelling the responseTimers */
  while((pTxData = (I870CHNL_TX_DATA *)tmwdlist_getFirst(&pRdcyGroupChannel->messageQueue)) != TMWDEFS_NULL)
  {
    tmwtimer_cancel(&pTxData->tmw.responseTimer);
    tmwdlist_removeEntry(&pRdcyGroupChannel->messageQueue, (TMWDLIST_MEMBER *)pTxData);
    i870chnl_freeTxData((TMWSESN_TX_DATA *)pTxData);
  }

  /* Cleanup channel structure, including deleting the lock */
  tmwchnl_deleteChannel((TMWCHNL*)pRdcyGroup);

  i870mem4_free(pRdcyGroup);
  return(TMWDEFS_TRUE);
}

/* function: lnk4rdcy_openRedundantChannel */
TMWCHNL * TMWDEFS_GLOBAL lnk4rdcy_openRedundantChannel(
  TMWAPPL               *pApplContext,
  TMWCHNL               *pRdcyGroup,
  const I870CHNL_CONFIG *pChnlConfig,
  I870LNK4_CONFIG       *pLinkConfig,
  const TMWPHYS_CONFIG  *pPhysConfig,
  const void            *pIOConfig,
  TMWTARG_CONFIG        *pTmwTargConfig)
{
  LNK4RDCY_CONNECT_CONTEXT *pRdntConnect;
  TMWCHNL *pChannel;

  pLinkConfig->isRedundant = TMWDEFS_TRUE;
  pChannel = i104chnl_openChannel(pApplContext,
    pChnlConfig, pLinkConfig, pPhysConfig, pIOConfig, pTmwTargConfig);

  TMWTARG_LOCK_SHARE(&pRdcyGroup->lock, &pChannel->lock);

  if(pChannel != TMWDEFS_NULL)
  {
    if((pRdntConnect =_addRedundantChannel(pRdcyGroup, pChannel)) != TMWDEFS_NULL)
    {
      /* Replace the standard link callbacks with the ones used for redundancy */
      pChannel->pLink->pLinkSetCallbacks(pChannel->pLinkContext,
       pRdntConnect, _processRdcyInfoCallback, _parseFrameCallback, _checkDataAvailable,
       _beforeTxCallback, _afterTxCallback, _failedTxCallback);

      return(pChannel);
    }
    i104chnl_closeChannel(pChannel);
  }
  return(TMWDEFS_NULL);
}

/* function: lnk4rdcy_closeRedundantChannel */
TMWTYPES_BOOL TMWDEFS_GLOBAL lnk4rdcy_closeRedundantChannel(
  TMWCHNL *pRdcyGroupChannel,
  TMWCHNL *pRdntChannel)
{ 
  LNK4RDCY_GROUP_CONTEXT *pRdcyGroup = (LNK4RDCY_GROUP_CONTEXT *)pRdcyGroupChannel;
  LNK4RDCY_CONNECT_CONTEXT *pRdntConnect = TMWDEFS_NULL;

  if((tmwdlist_size(&pRdcyGroup->rdntChannels) == 1) 
    && (pRdcyGroup->pSession != TMWDEFS_NULL))
  {
    /* Don't let last channel be removed, if session is still open */
    return(TMWDEFS_FALSE);
  }

  /* Find this channel on the list */
  while((pRdntConnect = (LNK4RDCY_CONNECT_CONTEXT *)tmwdlist_getAfter(
    &pRdcyGroup->rdntChannels, (TMWDLIST_MEMBER *)pRdntConnect)) != TMWDEFS_NULL)
  {

    if(pRdntConnect->pLnk4Channel == pRdntChannel)
    {
      /* Found this redundant channel, remove it from list */
      tmwdlist_removeEntry(&pRdcyGroup->rdntChannels, (TMWDLIST_MEMBER *)pRdntConnect);

      /* If the link being deleted is the one currently being used, 
       * deactivate the old one and replace it with another redundant link 
       */
      if(pRdcyGroup->pSession != TMWDEFS_NULL)
      {
        /* Close the session on this link */  
        pRdntConnect->pLnk4Channel->pLink->pLinkCloseSession(
          (TMWLINK_CONTEXT *)pRdntConnect->pLnk4Channel->pLinkContext, pRdcyGroup->pSession);
  
        if(pRdcyGroup->pCurrentLink == pRdntConnect)
        {
          LNK4RDCY_CONNECT_CONTEXT *pNewConnect;

          if((pNewConnect = _getOnlineLink(pRdcyGroup)) == TMWDEFS_NULL)
          {
            /* If no online links, just pick the first one in the list */      
            pNewConnect = (LNK4RDCY_CONNECT_CONTEXT *)
              tmwdlist_getFirst(&pRdcyGroup->rdntChannels);
          }

          /* We checked to make sure there was a second link above, so pNewLink
           * will not be NULL 
           */
          _diagInfo(pNewConnect,"REDUNDANT manual switch");
          _switchOver(pRdcyGroup, pNewConnect, TMWDEFS_FALSE);
        }
      }
      
      /* Close the 104 channel */
      i104chnl_closeChannel(pRdntChannel);
      
      /* deallocate the memory. */
      i870mem4_free(pRdntConnect);
 
      return(TMWDEFS_TRUE);
    }
  }
  return(TMWDEFS_FALSE);
} 

/* function: lnk4rdcy_manualSwitchOver */
TMWTYPES_BOOL TMWDEFS_GLOBAL lnk4rdcy_manualSwitchOver(
  TMWCHNL *pRdcyGroupChannel,
  TMWCHNL *pRdntChannel)
{  
  LNK4RDCY_GROUP_CONTEXT *pRdcyGroup = (LNK4RDCY_GROUP_CONTEXT *)pRdcyGroupChannel;
  LNK4RDCY_CONNECT_CONTEXT *pRdntConnect = TMWDEFS_NULL;

  /* First verify that the requested link is one of the redundant links */
  while((pRdntConnect = (LNK4RDCY_CONNECT_CONTEXT *)tmwdlist_getAfter(
    &pRdcyGroup->rdntChannels, (TMWDLIST_MEMBER *)pRdntConnect)) != TMWDEFS_NULL)
  {
    if(pRdntConnect->pLnk4Channel == pRdntChannel)
    {
      _diagInfo(pRdntConnect,"REDUNDANT manual switch");

      _switchOver(pRdcyGroup, pRdntConnect, TMWDEFS_TRUE);
   
      return(TMWDEFS_TRUE);
    }
  }

  return(TMWDEFS_FALSE);
} 
