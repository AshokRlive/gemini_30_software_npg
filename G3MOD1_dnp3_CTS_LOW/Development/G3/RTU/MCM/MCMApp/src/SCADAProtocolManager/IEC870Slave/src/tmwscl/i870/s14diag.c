/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14diag.h
 * description: IEC 60870-5-101/104 Slave Diagnostics
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/utils/tmwtarg.h"
#include "tmwscl/utils/tmwcnfg.h"
#include "tmwscl/utils/tmwdefs.h"

#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/i14diag.h"
#include "tmwscl/i870/s14diag.h"
#include "tmwscl/i870/s14sim.h"

#if TMWCNFG_SUPPORT_DIAG

/* Error messages used by slave 101/104 */
const I870DIAG_ERROR_ENTRY s14diagErrorMsg[] = {
  { S14DIAG_ALLOC_CYCLIC     ,  "Can't allocate txData for Cyclic Response" },
  { S14DIAG_ALLOC_BACKGROUND ,  "Can't allocate txData for Background Response" },
  { S14DIAG_INVALID_SIZE     ,  "Invalid message size received" },
  { S14DIAG_UNKNOWN_ASDU     ,  "Received request for unknown ASDU type" },
  { S14DIAG_ALLOC_UNKNOWN    ,  "Can't allocate txData for response to unknown" },
  { S14DIAG_UNKNOWN_SECTOR   ,  "Received request for unknown sector" },
  { S14DIAG_ALLOC_EVENT      ,  "Error allocating space for event" },
  { S14DIAG_CMD_DELAYED      ,  "Command has exceeded max delay or time in future, no response will be sent" },

#ifdef TMW_SUPPORT_MONITOR
  { S14DIAG_MON_UNKNOWN_ASDU ,  "Received request for unknown ASDU type,\n      Should this channel have been configured to monitor data from a SLAVE device?" },
#endif

  { S14DIAG_ERROR_ENUM_MAX   ,  ""}
};

/* Array to determine if specific error messages are disabled.
 */
static TMWTYPES_UCHAR _errorMsgDisabled[(S14DIAG_ERROR_ENUM_MAX/8)+1];

/* routine: s14diag_init */
void TMWDEFS_GLOBAL s14diag_init()
{
  /* No error messages are disabled by default. */
  memset(_errorMsgDisabled, 0, (S14DIAG_ERROR_ENUM_MAX/8)+1);
}

/* routine: s14diag_validateErrorTable */
TMWTYPES_BOOL s14diag_validateErrorTable(void)
{
  int i;
  for(i=0; i<S14DIAG_ERROR_ENUM_MAX;i++)
  {
    if(s14diagErrorMsg[i].errorNumber != i)
      return(TMWDEFS_FALSE);
  }

  return(TMWDEFS_TRUE);
}

/* function: s14diag_errorMsgEnable */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14diag_errorMsgEnable(
  S14DIAG_ERROR_ENUM errorNumber,
  TMWTYPES_BOOL value)
{
  if(errorNumber > S14DIAG_ERROR_ENUM_MAX)
    return(TMWDEFS_FALSE);

  if(value)
    _errorMsgDisabled[errorNumber/8] &=  ~(1 <<(errorNumber%8));
  else
    _errorMsgDisabled[errorNumber/8] |=  (1 <<(errorNumber%8));

  return(TMWDEFS_TRUE);
}

/* function: _errorMsgEnabled */
static TMWTYPES_BOOL TMWDEFS_LOCAL _errorMsgEnabled(
   S14DIAG_ERROR_ENUM errorNumber)
{
  int value;

  if(errorNumber > S14DIAG_ERROR_ENUM_MAX)
    return(TMWDEFS_FALSE);

  value = _errorMsgDisabled[errorNumber/8] & (1 <<(errorNumber%8));
  if(value == 0)
    return(TMWDEFS_TRUE);
  else
    return(TMWDEFS_FALSE);
}

/* function: s14diag_error */
void TMWDEFS_GLOBAL s14diag_error(
  TMWSESN *pSession, 
  TMWSCTR *pSector,
  S14DIAG_ERROR_ENUM errorNumber)
{
  TMWDIAG_ANLZ_ID anlzId;
  char buf[256];

#ifdef TMW_SUPPORT_MONITOR
  if(pSession->pChannel->pPhysContext->monitorMode)
  {
    /* give a more helpful error message */
    if(errorNumber == S14DIAG_UNKNOWN_ASDU)   
      errorNumber = S14DIAG_MON_UNKNOWN_ASDU;
  }
#endif

  if(_errorMsgEnabled(errorNumber))
  {
    const char *pName;

    if(tmwdiag_initId(&anlzId, pSession->pChannel, pSession, pSector, TMWDIAG_ID_APPL | TMWDIAG_ID_ERROR) == TMWDEFS_FALSE)
    {
      return;
    }

    if(pSector == TMWDEFS_NULL)
    {
      pName = tmwsesn_getSessionName(pSession);
    }
    else
    {
      pName = tmwsctr_getSectorName(pSector);
    }

    if(pName == (const char *)0)
      pName = " ";
  
    (void)tmwtarg_snprintf(buf, sizeof(buf), "**** %s   %s ****\n", pName, s14diagErrorMsg[errorNumber].pErrorMsg);

    tmwdiag_skipLine(&anlzId);
    tmwdiag_putLine(&anlzId, buf);
  }
}

#endif /* TMWCNFG_SUPPORT_DIAG */
