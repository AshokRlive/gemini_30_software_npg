/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14sctr.h
 * description: This file is intended for internal SCL use only.
 *  IEC 60870-5-101/104 Slave sector
 */
#ifndef S14SCTR_DEFINED
#define S14SCTR_DEFINED

#include "tmwscl/utils/tmwdtime.h"
#include "tmwscl/utils/tmwtimer.h"
#include "tmwscl/i870/i870sctr.h"
#include "tmwscl/i870/s14data.h"

/* Bits to limit some optional functionality to interoperate with masters
 * that do not support this behavior.
 */ 

/* Even though 60870-5-101 shows many typeDefs can be sent with multiple points
 * per response with SQ=0 PMENA/B/C are not shown that way. By default the S101/4
 * library allows multiple points to be sent in a response. To limit responses to 
 * a single point for masters that do not support set this bit in strictAdherence 
 */
#define S14SCTR_STRICT_PMENABC   0x0001


#define GROUP_INDEX_IDLE 255
 
/* S14 Sector Context */
typedef struct S14SectorStruct {

  /* Generic IEC 60870 Sector info, must be first entry */
  I870SCTR i870;

  /* IEC 60870-5-101/104 Configuration */
  TMWTYPES_BOOL cseUseActTerm;
  TMWTYPES_BOOL cmdUseActTerm;
  
  /* Bit mask to limit some optional functionality to interoperate with masters
   * that do not support this behavior.
   * currently only S14SCTR_STRICT_PMENABC is defined
   */ 
  TMWTYPES_ULONG strictAdherence;
   
  /* Whether or not this sector is in local mode. Sector is NOT normally in local mode.
   * Currently the only behavior changes associated with this mode are as follows.
   * When a command is received an event may be automatically generated for the 
   * monitored point if one is present. 
   * TMWDEFS_FALSE, the default value would send COT 11 I14DEF_COT_RETURN_REMOTE
   * TMWDEFS_TRUE,  this will send COT 12 I14DEF_COT_RETURN_LOCAL instead
   */
  TMWTYPES_BOOL localMode;

  TMWTYPES_MILLISECONDS selectTimeout;
  TMWTYPES_MILLISECONDS clockValidPeriod;
  
  TMWTYPES_MILLISECONDS defaultResponseTimeout;

  /* Misc */
  TMWTIMER clockValidTimer;
  TMWTYPES_USHORT propagationDelay;

  /* Cyclic data processing */
  TMWTIMER cyclicTimer;
  TMWTYPES_MILLISECONDS cyclicPeriod;
  TMWTYPES_MILLISECONDS cyclicFirstPeriod;
  TMWTYPES_UCHAR cyclicGroupIndex;
  TMWTYPES_USHORT cyclicPointIndex;

  /* Background scan data processing */
  TMWTIMER backgroundTimer;
  TMWTYPES_MILLISECONDS backgroundPeriod;
  TMWTYPES_UCHAR backgroundGroupIndex;
  TMWTYPES_USHORT backgroundPointIndex;

  /* Report By Exception processing */
  TMWTIMER rbeScanTimer;
  TMWDTIME lastClockSyncTime;
  TMWTYPES_BOOL sendClockSyncEvents;
  TMWTYPES_BOOL deleteOldestEvent;
  TMWTYPES_MILLISECONDS rbeScanPeriod;

  /* Event configuration */
#if S14DATA_SUPPORT_MSP
  TMWTYPES_BOOL mspScanEnabled;
  TMWTYPES_USHORT mspMaxEvents;
  TMWDEFS_EVENT_MODE mspEventMode;
  TMWDEFS_TIME_FORMAT mspTimeFormat;
#endif

#if S14DATA_SUPPORT_MDP
  TMWTYPES_BOOL mdpScanEnabled;
  TMWTYPES_USHORT mdpMaxEvents;
  TMWDEFS_EVENT_MODE mdpEventMode;
  TMWDEFS_TIME_FORMAT mdpTimeFormat;
#endif

#if S14DATA_SUPPORT_MST
  TMWTYPES_BOOL mstScanEnabled;
  TMWTYPES_USHORT mstMaxEvents;
  TMWDEFS_EVENT_MODE mstEventMode;
  TMWDEFS_TIME_FORMAT mstTimeFormat;
#endif

#if S14DATA_SUPPORT_MBO
  TMWTYPES_BOOL mboScanEnabled;
  TMWTYPES_USHORT mboMaxEvents;
  TMWDEFS_EVENT_MODE mboEventMode;
  TMWDEFS_TIME_FORMAT mboTimeFormat;
#endif

#if S14DATA_SUPPORT_MME_A
  TMWTYPES_BOOL mmenaScanEnabled;
  TMWTYPES_USHORT mmenaMaxEvents;
  TMWDEFS_EVENT_MODE mmenaEventMode;
  TMWDEFS_TIME_FORMAT mmenaTimeFormat;
#endif

#if S14DATA_SUPPORT_MME_B
  TMWTYPES_BOOL mmenbScanEnabled;
  TMWTYPES_USHORT mmenbMaxEvents;
  TMWDEFS_EVENT_MODE mmenbEventMode;
  TMWDEFS_TIME_FORMAT mmenbTimeFormat;
#endif

#if S14DATA_SUPPORT_MME_C
  TMWTYPES_BOOL mmencScanEnabled;
  TMWTYPES_USHORT mmencMaxEvents;
  TMWDEFS_EVENT_MODE mmencEventMode;
  TMWDEFS_TIME_FORMAT mmencTimeFormat;
#endif

#if S14DATA_SUPPORT_MIT
  /* no scanning for integrated totals changes */
  TMWTYPES_USHORT mitMaxEvents;
  TMWDEFS_EVENT_MODE mitEventMode;
  TMWDEFS_TIME_FORMAT mitTimeFormat;
#endif

#if S14DATA_SUPPORT_MITC
  /* no scanning for integrated BCD changes */
  TMWTYPES_USHORT mitcMaxEvents;
  TMWDEFS_EVENT_MODE mitcEventMode;
  TMWDEFS_TIME_FORMAT mitcTimeFormat;
#endif

#if S14DATA_SUPPORT_MEPTA
  TMWTYPES_BOOL meptaScanEnabled;
  TMWTYPES_USHORT meptaMaxEvents;
  TMWDEFS_EVENT_MODE meptaEventMode;
  TMWDEFS_TIME_FORMAT meptaTimeFormat;
#endif

#if S14DATA_SUPPORT_MEPTB
  TMWTYPES_BOOL meptbScanEnabled;
  TMWTYPES_USHORT meptbMaxEvents;
  TMWDEFS_EVENT_MODE meptbEventMode;
  TMWDEFS_TIME_FORMAT meptbTimeFormat;
#endif

#if S14DATA_SUPPORT_MEPTC
  TMWTYPES_BOOL meptcScanEnabled;
  TMWTYPES_USHORT meptcMaxEvents;
  TMWDEFS_EVENT_MODE meptcEventMode;
  TMWDEFS_TIME_FORMAT meptcTimeFormat;
#endif

#if S14DATA_SUPPORT_MPS
  TMWTYPES_BOOL mpsnaScanEnabled;
  TMWTYPES_USHORT mpsnaMaxEvents;
  TMWDEFS_EVENT_MODE mpsnaEventMode;
#endif

#if S14DATA_SUPPORT_MMEND
  TMWTYPES_BOOL mmendScanEnabled;
  TMWTYPES_USHORT mmendMaxEvents;
  TMWDEFS_EVENT_MODE mmendEventMode;
#endif

#if S14DATA_SUPPORT_FILE
  /* File Transfer Directory */
  TMWTYPES_BOOL fdrtaScanEnabled;
#endif

  /* Time format for response to read CRDNA for all data types except measurands  */
  /* This is also used as the time format for responses to CCINA counter requests */
  TMWDEFS_TIME_FORMAT readTimeFormat;
  
  /* Time format for response to read CRDNA for measurands */
  TMWDEFS_TIME_FORMAT readMsrndTimeFormat;

  /* The spec says not to send ASDU types with timeStamps in response to GI. 
   * However, some customers require this capability. 
   * For example setting this to TMWDEFS_TIME_FORMAT_56 will cause:
   *      Type I14DEF_TYPE_MSPTB1 to be sent instead of I14DEF_TYPE_MSPNA1,
   *      Type I14DEF_TYPE_MDPTB1 to be sent instead of I14DEF_TYPE_MDPNA1,
   *      Type I14DEF_TYPE_MBOTB1 to be sent instead of I14DEF_TYPE_MBONA1,
   *  and Type I14DEF_TYPE_MMETF1 to be sent instead of I14DEF_TYPE_MMENC1.
   *  Normally this should be set to TMWDEFS_TIME_FORMAT_NONE
   */
  TMWDEFS_TIME_FORMAT cicnaTimeFormat;

  /* Lists to hold events for each data type */
#if  S14DATA_SUPPORT_MSP
  TMWDLIST mspEvents;
#endif
#if  S14DATA_SUPPORT_MDP
  TMWDLIST mdpEvents;
#endif
#if  S14DATA_SUPPORT_MST
  TMWDLIST mstEvents;
#endif
#if  S14DATA_SUPPORT_MBO
  TMWDLIST mboEvents;
#endif
#if  S14DATA_SUPPORT_MIT
  TMWDLIST mitEvents;
#endif
#if  S14DATA_SUPPORT_MITC
  TMWDLIST mitcEvents;
#endif
#if  S14DATA_SUPPORT_MME_A
  TMWDLIST mmenaEvents;
#endif
#if  S14DATA_SUPPORT_MME_B
  TMWDLIST mmenbEvents;
#endif
#if  S14DATA_SUPPORT_MME_C
  TMWDLIST mmencEvents;
#endif
#if  S14DATA_SUPPORT_MEPTA
  TMWDLIST meptaEvents;
#endif
#if  S14DATA_SUPPORT_MEPTB
  TMWDLIST meptbEvents;
#endif
#if  S14DATA_SUPPORT_MEPTC
  TMWDLIST meptcEvents;
#endif
#if  S14DATA_SUPPORT_MPS
  TMWDLIST mpsnaEvents;
#endif
#if  S14DATA_SUPPORT_MMEND
  TMWDLIST mmendEvents;
#endif
  
#if S14DATA_SUPPORT_DOUBLE_TRANS 
  /* 
   * S14DATA_TRANSMISSION_SINGLE - Normal Single spontaneous message will be sent
   * S14DATA_TRANSMISSION_DOUBLE - Double transmission
   *   A single call to s14mxx_addEvent will cause the SCL to send
   *   a high priority spontaneous message without time, followed by a lower 
   *   priority spontaneous message with time. 
   * S14DATA_TRANSMISSION_PERPOINT - s14data_mxxx_getTransmissionMode() will be called
   */
#if  S14DATA_SUPPORT_MSP
  S14DATA_TRANSMISSION_MODE mspTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MDP
  S14DATA_TRANSMISSION_MODE mdpTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MST
  S14DATA_TRANSMISSION_MODE mstTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MBO
  S14DATA_TRANSMISSION_MODE mboTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MIT 
  S14DATA_TRANSMISSION_MODE mitTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MITC 
  S14DATA_TRANSMISSION_MODE mitcTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MME_A 
  S14DATA_TRANSMISSION_MODE mmenaTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MME_B
  S14DATA_TRANSMISSION_MODE mmenbTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MME_C 
  S14DATA_TRANSMISSION_MODE mmencTransmissionMode;
#endif
#endif

  /* Flags to indicate if event queues have overflowed */
#if  S14DATA_SUPPORT_MSP
  TMWTYPES_BOOL mspEventsOverflowed;
#endif
#if  S14DATA_SUPPORT_MDP
  TMWTYPES_BOOL mdpEventsOverflowed;
#endif
#if  S14DATA_SUPPORT_MST
  TMWTYPES_BOOL mstEventsOverflowed;
#endif
#if  S14DATA_SUPPORT_MBO
  TMWTYPES_BOOL mboEventsOverflowed;
#endif
#if  S14DATA_SUPPORT_MIT
  TMWTYPES_BOOL mitEventsOverflowed;
#endif
#if  S14DATA_SUPPORT_MITC
  TMWTYPES_BOOL mitcEventsOverflowed;
#endif
#if  S14DATA_SUPPORT_MME_A
  TMWTYPES_BOOL mmenaEventsOverflowed;
#endif
#if  S14DATA_SUPPORT_MME_B
  TMWTYPES_BOOL mmenbEventsOverflowed;
#endif
#if  S14DATA_SUPPORT_MME_C
  TMWTYPES_BOOL mmencEventsOverflowed;
#endif
#if  S14DATA_SUPPORT_MEPTA
  TMWTYPES_BOOL meptaEventsOverflowed;
#endif
#if  S14DATA_SUPPORT_MEPTB
  TMWTYPES_BOOL meptbEventsOverflowed;
#endif
#if  S14DATA_SUPPORT_MEPTC
  TMWTYPES_BOOL meptcEventsOverflowed;
#endif
#if  S14DATA_SUPPORT_MPS
  TMWTYPES_BOOL mpsnaEventsOverflowed;
#endif
#if  S14DATA_SUPPORT_MMEND
  TMWTYPES_BOOL mmendEventsOverflowed;
#endif

  /* Response to unknown  ASDU type id, asdu address, COT or IOA */
  TMWSESN_TX_DATA *pUnknownResponse;

#if S14DATA_SUPPORT_CICNA  
  /* General Interrogation */
  TMWTYPES_UCHAR cicnaCOT;
  TMWTYPES_BOOL  cicnaDataReady;
  TMWTYPES_BOOL  cicnaIsBroadcast;
  TMWTYPES_UCHAR cicnaQualifier;
  TMWTYPES_UCHAR cicnaGroupIndex;
  TMWTYPES_USHORT cicnaPointIndex;
  TMWTYPES_UCHAR cicnaOriginator;
  TMWTYPES_ULONG cicnaIOA;
  TMWTYPES_ULONG groupMask;
#endif

#if  S14DATA_SUPPORT_CCINA
  /* Counter Interrogation Command */
  TMWTYPES_UCHAR ccinaCOT;
  TMWTYPES_BOOL  ccinaDataReady;
  TMWTYPES_UCHAR ccinaQCC;
  TMWTYPES_UCHAR ccinaGroupIndex;
  TMWTYPES_USHORT ccinaPointIndex;
  TMWTYPES_UCHAR ccinaOriginator;
  TMWTYPES_ULONG ccinaIOA;
#endif

#if S14DATA_SUPPORT_CRDNA  
  /* Read Command */
  void *pCRDNAPoint;
  TMWTYPES_UCHAR crdnaCOT;
  TMWTYPES_BOOL  crdnaDataReady;
  TMWTYPES_ULONG crdnaIOA;
  TMWTYPES_UCHAR crdnaOriginator;
#endif

#if S14DATA_SUPPORT_CCSNA
  /* Clock Synchronization Command */
  TMWTYPES_UCHAR ccsnaCOT;
  TMWTYPES_UCHAR ccsnaOriginator;  
  TMWDTIME ccsnaTime;
  TMWTYPES_ULONG ccsnaIOA;
  
  /* Internal clock sync response sequence number to help determine whether
   * a spontaneous event should be sent before the clock sync response. 
   * This will not be exchanged with the master, 
   * it is for internal SCL processing only.
   */
  TMWTYPES_UCHAR ccsnaRespNumber;
#endif

#if S14DATA_SUPPORT_CRPNA
  /* Reset Process Command */
  TMWTYPES_UCHAR crpnaCOT;
  TMWTYPES_UCHAR crpnaQRP;
  TMWTYPES_UCHAR crpnaOriginator;
  TMWTYPES_ULONG crpnaIOA;
#endif
  
#if S14DATA_SUPPORT_MULTICMDS 
  TMWTYPES_UCHAR  commandsPerType;
  TMWTYPES_USHORT commandsPerSector;
  TMWDLIST commandList;
#else
#if S14DATA_SUPPORT_CSC 
  /* Single Command */
  void *pCSCPoint;
  TMWTYPES_UCHAR cscnaCOT;
  TMWTYPES_UCHAR csctaCOT;
  TMWTYPES_ULONG cscnaIOA;
  TMWTYPES_UCHAR cscnaSCO;
  TMWTIMER cscnaSelectTimer;
  TMWDEFS_COMMAND_STATE cscnaState;
  TMWDEFS_COMMAND_STATUS cscnaStatus;
  TMWTYPES_UCHAR cscnaOriginator;
#endif

#if S14DATA_SUPPORT_CDC
  /* Double Command */
  void *pCDCPoint;
  TMWTYPES_UCHAR cdcnaCOT;
  TMWTYPES_UCHAR cdctaCOT; 
  TMWTYPES_ULONG cdcnaIOA;
  TMWTYPES_UCHAR cdcnaDCO;
  TMWTIMER cdcnaSelectTimer;
  TMWDEFS_COMMAND_STATE cdcnaState;
  TMWDEFS_COMMAND_STATUS cdcnaStatus;
  TMWTYPES_UCHAR cdcnaOriginator;
#endif

#if S14DATA_SUPPORT_CRC
  /* Regulating Step Command */
  void *pCRCPoint;
  TMWTYPES_UCHAR crcnaCOT;
  TMWTYPES_UCHAR crctaCOT; 
  TMWTYPES_ULONG crcnaIOA;
  TMWTYPES_UCHAR crcnaRCO;
  TMWTIMER crcnaSelectTimer;
  TMWDEFS_COMMAND_STATE crcnaState;
  TMWDEFS_COMMAND_STATUS crcnaStatus;
  TMWTYPES_UCHAR crcnaOriginator;
#endif

#if S14DATA_SUPPORT_CBO 
  /* Bitstring Command */
  void *pCBOPoint;
  TMWTYPES_UCHAR cbonaCOT;
  TMWTYPES_UCHAR cbotaCOT; 
  TMWTYPES_ULONG cbonaIOA;
  TMWTYPES_ULONG cbonaBSI;
  TMWDEFS_COMMAND_STATUS cbonaStatus;
  TMWTYPES_UCHAR cbonaOriginator;
#endif

#if S14DATA_SUPPORT_CSE_A  
  /* Normalized Set Point Command */
  void *pCSENAPoint;
  TMWTYPES_UCHAR csenaCOT;
  TMWTYPES_UCHAR csetaCOT;
  TMWTYPES_ULONG csenaIOA;
  TMWTYPES_SHORT csenaNVA;
  TMWTYPES_UCHAR csenaQOS;
  TMWTIMER csenaSelectTimer;
  TMWDEFS_COMMAND_STATE csenaState;
  TMWDEFS_COMMAND_STATUS csenaStatus;
  TMWTYPES_UCHAR csenaOriginator;
#endif

#if S14DATA_SUPPORT_CSE_B
  /* Scaled Set Point Command */
  void *pCSENBPoint;
  TMWTYPES_UCHAR csenbCOT;
  TMWTYPES_UCHAR csetbCOT;
  TMWTYPES_ULONG csenbIOA;
  TMWTYPES_SHORT csenbSVA;
  TMWTYPES_UCHAR csenbQOS;
  TMWTIMER csenbSelectTimer;
  TMWDEFS_COMMAND_STATE csenbState;
  TMWDEFS_COMMAND_STATUS csenbStatus;
  TMWTYPES_UCHAR csenbOriginator;
#endif

#if S14DATA_SUPPORT_CSE_C
  /* Float Set Point Command */
  void *pCSENCPoint;
  TMWTYPES_UCHAR csencCOT;
  TMWTYPES_UCHAR csetcCOT;
  TMWTYPES_ULONG csencIOA;
  TMWTYPES_SFLOAT csencFVA;
  TMWTYPES_UCHAR csencQOS;
  TMWTIMER csencSelectTimer;
  TMWDEFS_COMMAND_STATE csencState;
  TMWDEFS_COMMAND_STATUS csencStatus;
  TMWTYPES_UCHAR csencOriginator;
#endif

#if S14DATA_SUPPORT_CSE_Z
  /* Set Integrated Totals BCD Command */
  void *pCSENZPoint;
  TMWTYPES_UCHAR csenzCOT;
  TMWTYPES_ULONG csenzIOA;
  TMWTYPES_UCHAR csenzBCD[6];
  TMWTYPES_UCHAR csenzQOS;
  TMWTIMER csenzSelectTimer;
  TMWDEFS_COMMAND_STATE csenzState;
  TMWDEFS_COMMAND_STATUS csenzStatus;
  TMWTYPES_UCHAR csenzOriginator;
#endif
#endif /* !S14DATA_SUPPORT_MULTICMDS */

#if S14DATA_SUPPORT_CCTNA
  /* Set Configuration Table Command */
  TMWTYPES_UCHAR cctnaCOT;
  TMWTYPES_ULONG cctnaIOA;
  TMWTYPES_UCHAR cctnaOriginator;
#endif

#if S14DATA_SUPPORT_PMENA
  /* Parameter of Measured Value, Normalized Value */
  TMWTYPES_UCHAR pmenaCOT;
  TMWTYPES_ULONG pmenaIOA;
  TMWTYPES_SHORT pmenaNVA;
  TMWTYPES_UCHAR pmenaQPM;
  TMWTYPES_UCHAR pmenaOriginator;
#endif

#if S14DATA_SUPPORT_PMENB
  /* Parameter of Measured Value, Scaled Value */
  TMWTYPES_UCHAR pmenbCOT;
  TMWTYPES_ULONG pmenbIOA;
  TMWTYPES_SHORT pmenbSVA;
  TMWTYPES_UCHAR pmenbQPM;
  TMWTYPES_UCHAR pmenbOriginator;
#endif

#if S14DATA_SUPPORT_PMENC
  /* Parameter of Measured Value, Short Floating Point Value */
  TMWTYPES_UCHAR pmencCOT;
  TMWTYPES_ULONG pmencIOA;
  TMWTYPES_SFLOAT pmencFVA;
  TMWTYPES_UCHAR pmencQPM;
  TMWTYPES_UCHAR pmencOriginator;
#endif

#if S14DATA_SUPPORT_PACNA
  /* Parameter Activation */
  TMWTYPES_UCHAR pacnaCOT;
  TMWTYPES_ULONG pacnaIOA;
  TMWTYPES_UCHAR pacnaQPA;
  TMWTYPES_UCHAR pacnaOriginator;
#endif

#if S14DATA_SUPPORT_FILE   
  /* File Transfer */
  TMWTYPES_UCHAR  fileState;
  TMWTYPES_ULONG  fileIOA;
  TMWTYPES_USHORT fileName;
  TMWTYPES_ULONG  fileLength;
  TMWTYPES_UCHAR  fileSectionName;
  TMWTYPES_ULONG  fileSectionLength;
  TMWTYPES_UCHAR  fileSectionChecksum;
  TMWTYPES_UCHAR  fileChecksum;
  TMWTYPES_UCHAR  fileFRQ;
  TMWTYPES_UCHAR  fileSRQ;
  TMWTYPES_UCHAR  fileAFQ;
  TMWTYPES_UCHAR  fileLSQ;
  TMWTYPES_UCHAR  fileCHS;
  TMWTYPES_UCHAR  fileOriginator;
  TMWTYPES_UCHAR  fileFdrtaCOT;
  TMWTYPES_UCHAR  fileDirIndex;
#if S14DATA_SUPPORT_FSCNB   
  TMWDTIME        fileRangeStartTime; 
  TMWDTIME        fileRangeStopTime;
#endif
#endif
   
#if S14DATA_SUPPORT_CICNAWAIT
  TMWTYPES_BOOL CICNAComplete;
#endif

} S14SCTR;

#if S14DATA_SUPPORT_MULTICMDS 
/* There is no TMWTYPES_xxx_TYPE for 6 byte bcd */
#define S14SCTR_TYPE_BCD   255
 
typedef TMWDEFS_COMMAND_STATUS (*S14SCTR_MULTISTATUS_FUNC)(void *pPoint);
typedef void (*S14SCTR_MONITORPOINT_FUNC)(TMWSCTR *pSector, struct s14sctr_command *pContext);

typedef struct s14sctr_command{
  /* List Member, must be first entry */
  TMWDLIST_MEMBER listMember;

  S14SCTR *pS14Sector;
  void    *pPoint;

  TMWTYPES_ULONG ioa;
  TMWTYPES_UCHAR typeId;
  TMWTYPES_UCHAR cot;
  TMWTYPES_UCHAR originatorAddress;
  TMWTYPES_UCHAR qualifier; 
  TMWTYPES_BOOL useValue;
  TMWTYPES_BOOL useQualifier;

#if S14DATA_SUPPORT_TIMETAG_COMMANDS
  TMWTYPES_BOOL useDateTime;
  TMWDTIME requestTime;
#endif

  TMWDEFS_COMMAND_STATE state;
  TMWDEFS_COMMAND_STATUS status;
  TMWTIMER selectTimer;
  
  S14SCTR_MULTISTATUS_FUNC pStatus;
  S14SCTR_MONITORPOINT_FUNC pCheckMonitoredPoint;

  TMWTYPES_ANALOG_TYPE valueType;
  union _valueUnion
  {
    TMWTYPES_SHORT  sval;
    TMWTYPES_ULONG  ulval;
#if TMWCNFG_SUPPORT_FLOAT 
    TMWTYPES_SFLOAT fval;
#endif
#if S14DATA_SUPPORT_CSE_Z 
    TMWTYPES_UCHAR  bcd[6];
#endif
  } value;

} S14SCTR_CMD;

#endif
#ifdef __cplusplus
extern "C" {
#endif

  /* function: s14sctr_openSector */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14sctr_openSector(
    TMWSCTR *pSector, 
    void *pUserHandle);

  /* function: s14sctr_updateCyclicPeriod */
  void TMWDEFS_GLOBAL s14sctr_updateCyclicPeriod(
    TMWSCTR *pSector, 
    TMWTYPES_MILLISECONDS period);

  /* function: s14sctr_updateBackgroundPeriod */
  void TMWDEFS_GLOBAL s14sctr_updateBackgroundPeriod(
    TMWSCTR *pSector, 
    TMWTYPES_MILLISECONDS period);

  /* function: s14sctr_closeSector */
  void TMWDEFS_GLOBAL s14sctr_closeSector(
    TMWSCTR *pSector);

  /* function: s14sctr_parseFrameCallback */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14sctr_parseFrameCallback(
    TMWSCTR *pSector, 
    TMWSESN_RX_DATA *pRxFrame,
    void *pFuncTable);

  /* function: s14sctr_checkClassCallback */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14sctr_checkClassCallback( 
    TMWSCTR *pSector,
    TMWDEFS_CLASS_MASK classMask, 
    TMWTYPES_BOOL buildResponse,
    void *pFuncTable);

  /* function: s14sctr_cyclicTimeout */
  void TMWDEFS_GLOBAL s14sctr_cyclicTimeout(
    void *pParam);

  /* function: s14sctr_sendCyclic 
   * purpose: Start sending cylic data now.
   *   This internal interface which is called by s101/s104sctr_sendCyclic
   *   can be used when S101/104SCTR_CONFIG cyclic is set to 0, which disables 
   *   the SCL timer that causes cyclic data to be sent periodically. 
   * arguments:
   *  pSector - pointer to sector returned by s14sctr_openSector.
   *   Indicates sector on which to send cyclic data.
   * returns:
   *  void
   */
  TMWDEFS_SCL_API void TMWDEFS_GLOBAL s14sctr_sendCyclic(
    TMWSCTR *pSector);

  /* function: s14sctr_restartClockValidTime 
   * purpose: Restart the clock valid time. An application could call this if it
   *   had set the time using an external mechanism and wanted the SCL to consider
   *   time valid for the configured clockValidPeriod.
   * arguments:
   *  pSector - pointer to sector returned by s101/4sctr_openSector 
   * returns:
   *  void
   */
  TMWDEFS_SCL_API void TMWDEFS_GLOBAL s14sctr_restartClockValidTime(
    TMWSCTR *pSector);

#ifdef __cplusplus
}
#endif
#endif /* S14SCTR_DEFINED */
