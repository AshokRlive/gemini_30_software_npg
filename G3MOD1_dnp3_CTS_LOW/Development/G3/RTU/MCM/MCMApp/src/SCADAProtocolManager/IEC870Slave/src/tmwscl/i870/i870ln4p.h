/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i870ln4p.h
 * description: This file is intended for internal SCL use only.
 *  IEC 60870-5-104 profile Link Layer Implementation
 */
#ifndef I870LN4P_DEFINED
#define I870LN4P_DEFINED

#include "tmwscl/utils/tmwlink.h"
#include "tmwscl/utils/tmwchnl.h"
#include "tmwscl/i870/i870lnk4.h"
 
#define I870DEF4_DEFAULT_T0_TIME_PERIOD   30000
#define I870DEF4_DEFAULT_T1_TIME_PERIOD   15000
#define I870DEF4_DEFAULT_T2_TIME_PERIOD   10000
#define I870DEF4_DEFAULT_T3_TIME_PERIOD   20000

#define I870DEF4_DEFAULT_W_VALUE          8
#define I870DEF4_DEFAULT_K_VALUE          12

/* Indices of fields within the IEC 60870-5-104 APDU frame */
#define I870DEF4_APDU_INDEX_SYNC           0
#define I870DEF4_APDU_INDEX_LENGTH         1
#define I870DEF4_APDU_INDEX_CONTROL_1      2
#define I870DEF4_APDU_INDEX_CONTROL_2      3
#define I870DEF4_APDU_INDEX_CONTROL_3      4
#define I870DEF4_APDU_INDEX_CONTROL_4      5
#define I870DEF4_APDU_INDEX_ASDU           6

/* protocol field lengths */
#define I870DEF4_APDU_MINIMUM_SIZE         6

#define I870DEF4_APCI_LENGTH               6

#define I870DEF4_CONTROL_LENGTH            4

#define I870DEF4_ASDU_MAX_SIZE          (I870LNK4_MAX_TX_BUFFER_LENGTH-I870DEF4_APDU_MINIMUM_SIZE)

/* frame synchronization octet */
#define I870DEF4_APDU_START_CHARACTER   0x68

/* APDU format types */
#define I870DEF4_APDU_FORMAT_MASK       0x03
#define I870DEF4_APDU_FORMAT_SUPER      0x01
#define I870DEF4_APDU_FORMAT_UNNUM      0x03

/* APDU control (U format) codes */
#define I870DEF4_APDU_CTRL_STARTDT_ACT  0x07
#define I870DEF4_APDU_CTRL_STARTDT_CON  0x0B
#define I870DEF4_APDU_CTRL_STOPDT_ACT   0x13
#define I870DEF4_APDU_CTRL_STOPDT_CON   0x23
#define I870DEF4_APDU_CTRL_TESTFR_ACT   0x43
#define I870DEF4_APDU_CTRL_TESTFR_CON   0x83

/* Bit masks indicating what APDU needs to be transmitted */
#define  I870DEF4_XMT_STARTDT_ACT       0x01
#define  I870DEF4_XMT_STOPDT_ACT        0x02
#define  I870DEF4_XMT_TESTFR_ACT        0x04
#define  I870DEF4_XMT_STARTDT_CON       0x08
#define  I870DEF4_XMT_STOPDT_CON        0x10
#define  I870DEF4_XMT_TESTFR_CON        0x20
#define  I870DEF4_XMT_SFORMAT           0x40	 
#define  I870DEF4_XMT_IFORMAT           0x80	 
    
typedef enum {
  I870LNK4_STATE_OFFLINE,
  I870LNK4_STATE_CONNECTED,
  I870LNK4_STATE_ONLINE
} I870LNK4_STATE;

typedef enum {
  I870LNK4_ACTIVE_STARTED,
  I870LNK4_ACTIVE_WAIT_STOP_CNFM,
  I870LNK4_ACTIVE_WAIT_SEND_CNFM,
  I870LNK4_ACTIVE_STOPPED
} I870LNK4_ACTIVE_STATE;

/* 104 link layer transmit descriptor        
 * One for each possible "K value" information format APDU
 */
typedef struct {

  /* timer for receiving ack to transmitted ADPU.  */
  TMWTIMER              t1AckTimer;  
  
  /* sequence number that would be used in the     */
  /* next APDU transmitted after this one          */
  TMWTYPES_USHORT        afterTxSequence;    
  
  /* Pointer to application provided SESN TX DATA structure */
  TMWSESN_TX_DATA       *pTxData;           

} I870LNK4_APDU_TX_DESCRIPTOR;
  
/* Number of structures for transmit array         
 * Maximum number of outstanding unacked transmits  
 */
#define I870LNK4_MAX_NUMBER_TX_DESCRS I870LNK4_MAX_K_VALUE

/* Link layer 104 context structure */
typedef struct i870lnk4Context {
  /* Generic link layer info, must be first entry   */
  TMWLINK_CONTEXT tmw;

  I870LNK4_STATE  state;

  /* Configuration */
  TMWTYPES_USHORT       rxFrameSize;
   
  TMWTYPES_MILLISECONDS t1AckPeriod;   
  TMWTYPES_MILLISECONDS t2SFramePeriod;
  TMWTYPES_MILLISECONDS t3TestPeriod;

  TMWTYPES_USHORT       kValue;
  TMWTYPES_USHORT       wValue;

  /* If TRUE this side sends STARTDT etc. */
  TMWTYPES_BOOL         isControlling;      
  
  TMWTYPES_BOOL         redundancyEnabled;

  /* Setting this to TMWDEFS_TRUE on a slave will cause unacked responses 
   * (Information Frames) to be discarded when the TCP connection is broken.
   * If a slave has sent responses, but has not yet received a link layer ack,
   * and the master is restarted and reconnects, the "old" unacked responses
   * will be sent again. This is required behavior when redundancy is enabled.
   * However, with redundancy not enabled, some masters and some customers get
   * bothered by these "old" responses being received by the master.   
   *
   * DO NOT SET THIS TO TRUE WHEN REDUNDANCY IS ENABLED!
   */
  TMWTYPES_BOOL         discardIFramesOnDisconnect;

  /* Receive */ 
  TMWSESN_RX_DATA       rxFrame;
  TMWTYPES_UCHAR        rxFrameBuffer[I870LNK4_MAX_RX_BUFFER_LENGTH];
  TMWTYPES_UCHAR        rxCurrentLength;
  TMWTYPES_UCHAR        rxExpectedLength;

  /* Transmit */   
  TMWPHYS_TX_DESCRIPTOR physTxDescriptor;
  TMWTYPES_UCHAR        txFrameBuffer[I870LNK4_MAX_TX_BUFFER_LENGTH];

  /* OK to send to Physical Layer              */
  TMWTYPES_BOOL  okToTransmit;			  

  /* physTransmit() has not yet returned       */
  TMWTYPES_BOOL  transmitInProgress;	

  /* was not OK to transmit before, still need */
	/* transmit                                  */
  TMWTYPES_UCHAR transmitNeeded;		  

  /* Indicates whether STARTDT or STOPDT has been sent or received */
  I870LNK4_ACTIVE_STATE  linkActiveState;      

  /* Timer to send S-format APDU "acks" if we have  
	 * no I-format APDUs to send                      
   */
  TMWTIMER               t2SFrameTimer;      

  /* Timer to send periodic test              
   * frame U-format APDUs                     
   */
  TMWTIMER               t3TestTimer;        

  /* Timers to ensure an ack to a control function U Format APDUs */
  TMWTIMER               startDtCnfmTimer; 
  TMWTIMER               stopDtCnfmTimer; 
  TMWTIMER               testFrCnfmTimer; 

  /* Timer to ask application again if it has anything to send  
   * since there is no polling from the master, nothing will cause 
   * this to try again.
   */
  TMWTIMER balancedRetryTimer;

  /* expected sequence number of next received APDU */
  TMWTYPES_USHORT         nextExpectedSequence; 

  /* sequence number of last APDU acknowledged by   */
  /* remote device            */
  TMWTYPES_USHORT         lastAckedSequence; 

  /* counts number of unacknowledged received APDUs */  
  TMWTYPES_USHORT         numUnackedRcvdAPDUs;                  


  /* transmit data (I-FORMAT APDUs)                 */

  /* Array of descriptors holding                   */
	/* information about each transmit APDU           */
  I870LNK4_APDU_TX_DESCRIPTOR txDescriptors[I870LNK4_MAX_NUMBER_TX_DESCRS];  
                                      
  /* oldest tx descriptor in the queue that has not been acknowledged.     */
  /* this is basically the start of the current queue                      */
  int oldestApduTxDescriptorIndex;
  
  /* index to the next tx descriptor to use. The end of the current queue  */
  int nextApduTxDescriptorIndex;   
  
  /* Index to tx desc/APDU that should be transmitted next.                */
  int sendApduTxDescriptorIndex;      
                               
  /* TRUE if txDescr array full                     */
  TMWTYPES_BOOL			 txDescrListFull;         

  /* TRUE when all APDUs in txDescriptors list have been sent  
   * FALSE if there are any APDUs in list that have not yet been sent
   */
  TMWTYPES_BOOL       allApdusSent; 

  /* TRUE when no APDUs are waiting for ack         */
  TMWTYPES_BOOL       allApdusAcknowledged; 
  
  /* sequence number of next transmitted APDU       */
  TMWTYPES_USHORT     nextTxSequence;        

  /* User callback function to be called when message is given to link layer
   * Most implementations will not need to provide this callback function. This 
   * allows the message to be modified before it is transmitted for test purposes,
   * for example to modify some bytes or introduce errors.
   */
  I870LNK4_TX_CALLBACK_FUNC pUserTxCallback;
  void *pCallbackParam;

} I870LNK4_CONTEXT;

#endif /* I870LN4P_DEFINED */
