/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i870mem.c
 * description: IEC 60870-5 memory functions  
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/i870mem.h"
#include "tmwscl/i870/i870chnl.h"

typedef struct{
  TMWMEM_HEADER           header;
  I870CHNL_TX_DATA        data;
} I870MEM_TX_DATA;

static const TMWTYPES_CHAR *_nameTable[I870MEM_ALLOC_TYPE_MAX] = {
  "I870CHNL_TX_DATA"
};

#if !TMWCNFG_USE_DYNAMIC_MEMORY
/* Use static allocated memory instead of dynamic memory */
static I870MEM_TX_DATA i870mem_txDatas[I870MEM_NUMALLOC_SESN_TX_DATAS];
#endif

static TMWMEM_POOL_STRUCT _i870AllocTable[I870MEM_ALLOC_TYPE_MAX]; 

#if TMWCNFG_USE_DYNAMIC_MEMORY
void TMWDEFS_GLOBAL i870mem_initConfig(
  I870MEM_CONFIG   *pConfig)
{
  pConfig->numTxDatas = I870MEM_NUMALLOC_SESN_TX_DATAS;
}
#endif

/* routine: i870mem_init */
TMWTYPES_BOOL TMWDEFS_GLOBAL i870mem_init(
  I870MEM_CONFIG   *pConfig)
{  
#if TMWCNFG_USE_DYNAMIC_MEMORY
  /* dynamic memory allocation supported */
  
  I870MEM_CONFIG  config; 

  /* If caller has not specified memory pool configuration, use the
   * default compile time values 
   */
  if(pConfig == TMWDEFS_NULL)
  {
    pConfig = &config;
    i870mem_initConfig(pConfig);
  }
  
  if(!tmwmem_lowInit(_i870AllocTable, I870MEM_CHNL_TX_DATA_TYPE,  pConfig->numTxDatas, sizeof(I870MEM_TX_DATA), TMWDEFS_NULL))
    return TMWDEFS_FALSE;
#else

  /* static memory allocation supported */
  TMWTARG_UNUSED_PARAM(pConfig);

  if(!tmwmem_lowInit(_i870AllocTable, I870MEM_CHNL_TX_DATA_TYPE,  I870MEM_NUMALLOC_SESN_TX_DATAS, sizeof(I870MEM_TX_DATA), (TMWTYPES_UCHAR*)i870mem_txDatas))
    return TMWDEFS_FALSE;
#endif
  return TMWDEFS_TRUE;
}

/* function: i870mem_alloc */
 void * TMWDEFS_GLOBAL i870mem_alloc(
  I870MEM_ALLOC_TYPE type)
{
  if(type >= I870MEM_ALLOC_TYPE_MAX)
  {
    return(TMWDEFS_NULL);
  }

  return(tmwmem_lowAlloc(&_i870AllocTable[type]));
}

/* function: i870mem_free */
 void TMWDEFS_GLOBAL i870mem_free(
  void *pBuf)
{    
  TMWMEM_HEADER *pHeader = TMWMEM_GETHEADER(pBuf);
  TMWTYPES_UCHAR   type = TMWMEM_GETTYPE(pHeader);

  if(type >= I870MEM_ALLOC_TYPE_MAX)
  {
    return;
  }

  tmwmem_lowFree(&_i870AllocTable[type], pHeader);
}

 TMWTYPES_BOOL TMWDEFS_GLOBAL i870mem_getUsage(
  TMWTYPES_UCHAR index,
  const TMWTYPES_CHAR **name,
  TMWMEM_POOL_STRUCT *pStruct)
{
  if(index >= I870MEM_ALLOC_TYPE_MAX)
  {
    return(TMWDEFS_FALSE);
  }

  *name = _nameTable[index];
  *pStruct = _i870AllocTable[index];
  return(TMWDEFS_TRUE);
}



 
