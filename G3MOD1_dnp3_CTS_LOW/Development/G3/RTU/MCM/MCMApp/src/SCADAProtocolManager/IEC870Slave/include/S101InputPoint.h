/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:S101InputPoint.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       Contains all the functionality(classes) with regards to updating/getting
 *       values from the different Slave101 input points: single point(MSP),
 *       double point(MDP), floating analog(scaled,normalized and floating),
 *       and integrated totals.
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   18 Jul 2016     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef S101INPUTPOINT_H_
#define S101INPUTPOINT_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "SCADAProtocolCommon.h"
#include "S101TMWIncludes.h"
#include "IPointObserver.h"
#include "IPoint.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
class S101Database;// Forward declaration


/**
 * Abstract S101 Input point.
 */
class S101InputPoint: public IPointObserver
{
public:
    /**
     * \brief S101 point types
     */
    typedef enum {
        MSP, //single point
        MDP, //double point
        MMENA, // normalized analog
        MMENB, // scaled analog
        MMENC, // floating analog
        MIT,   // Integrated totals
    } ProtPointType;

    /**
     * \brief common input point configuration
     */
    struct Config
    {
        PointIdStr      vpointID;
        I870CommonElements pointAttributes;
    };

public:
    S101InputPoint(ProtPointType type, PointIdStr& vpointID):
                    m_vpointID(vpointID),
                    mp_db(NULL),
                    m_type(type)
    {}
    virtual ~S101InputPoint() {};

    /**
     * /brief to get the group and ID associated with the input point
     *
     * /returns Point ID
     *
     */
    virtual PointIdStr getPointID()
    {
        return m_vpointID;
    }

    /**
     * /brief Gets the pointer of this point to be used by TMW library.
     *
     * /returns returns pointer to particular type of input point
     *
     */
    virtual void* getTMWPointPtr() = 0;

    /**
     * /brief Gets the type of this input point.
     *
     * /returns Protocol Point Type
     *
     */
    virtual ProtPointType getType() {return m_type;}

    /**
     * /brief sets the database pointer member of this input point.
     *
     * /param pointer to instance of database
     */
    virtual void setDatabase(S101Database* db);

    /**
     * /brief get the pointer to the sector
     *
     * /param pointer to 101 sector
     *
     * /returns SCADAP_ERROR_NONE or SCADAP_ERROR_NULL_POINTER
     */
    virtual SCADAP_ERROR getSectorHandle(TMWSCTR** ppSectorHandle);

protected:
    PointIdStr          m_vpointID;
    S101Database*       mp_db;
    ProtPointType       m_type;
};


/**
 * S101 monitored single point.
 */
class S101MSP : public S101InputPoint
{
public:
    S101MSP(S101InputPoint::Config& conf);

    virtual ~S101MSP();

    virtual void update(PointIdStr pointID, PointData *pointDataPtr);

    virtual void* getTMWPointPtr(){return (void*)(&m_data);}

    virtual const I870DataMSP& getData(){return m_data;}

private:
    I870DataMSP         m_data;
};



/**
 * S101 monitored double point.
 */
class S101MDP : public S101InputPoint
{
public:
    S101MDP(S101InputPoint::Config& conf);
    virtual ~S101MDP();

    virtual void update(PointIdStr pointID, PointData *pointDataPtr);

    virtual void* getTMWPointPtr(){return (void*)(&m_data);}

    virtual const I870DataMDP& getData(){return m_data;}

private:
    I870DataMDP         m_data;
};


/**
 * S101 monitored Analog.
 */
class S101Analog : public S101InputPoint
{
public:

    S101Analog(ProtPointType type, S101InputPoint::Config& conf);
    virtual ~S101Analog();

    virtual void update(PointIdStr pointID, PointData *pointDataPtr);

    virtual void* getTMWPointPtr(){return (void*)(&m_data);}

    virtual const I870DataAnalog& getData(){return m_data;}

    static const lu_uint16_t NORMALIZE_SCALING_FACTOR = 32768;

private:

    I870DataAnalog         m_data;
};


/**
 * S101 monitored Integrated totals
 */
class S101MIT : public S101InputPoint
{
public:
    typedef enum
    {
        COUNTER_TYPE_CURRENT,
        COUNTER_TYPE_FROZEN
    } COUNTER_TYPE;

public:
    S101MIT(S101InputPoint::Config& conf);
    virtual ~S101MIT();

    virtual void update(PointIdStr pointID, PointData *pointDataPtr);

    virtual void* getTMWPointPtr(){return (void*)(&m_data);}

    virtual const I870DataMIT& getData(){return m_data;}

    virtual void setType(const COUNTER_TYPE type) {m_type = type;}

private:
    I870DataMIT         m_data;
    COUNTER_TYPE m_type;    //Type of value to report to master
    lu_uint32_t m_seqNum;   //Counter sequence number (BCR SQ 5 bits, from 33 to 37)
};


#endif /* S101INPUTPOINT_H_ */

/*
 *********************** End of file ******************************************
 */
