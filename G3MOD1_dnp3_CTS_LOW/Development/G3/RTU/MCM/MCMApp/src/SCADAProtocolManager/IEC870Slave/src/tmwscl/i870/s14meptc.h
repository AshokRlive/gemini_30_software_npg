/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14meptc.h
 * description: This file is intended for internal SCL use only.
 *  IEC 60870-5-101/104 slave MEPTC/MEPTD support
 *
 * This file supports ASDU types 
 *  19/40 Packed output curcuit information of protection equipment 
 *        with 24/56 bit time tag 
 */
#ifndef S14MEPTC_DEFINED
#define S14MEPTC_DEFINED

#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/utils/tmwdtime.h"
#include "tmwscl/utils/tmwsesn.h"
#include "tmwscl/i870/s14sesn.h"
#include "tmwscl/i870/s14sctr.h"
#include "tmwscl/i870/s14dbas.h"
#include "tmwscl/i870/s14evnt.h"

/* Structure used to store single point events */
typedef struct S14MEPTCEventStruct {
  S14EVNT s14Event;
  TMWTYPES_UCHAR oci;
  TMWTYPES_USHORT relayOperatingTime;
} S14MEPTC_EVENT;

#ifdef __cplusplus
extern "C" {
#endif

  /* function: s14meptc_init
   * purpose: Initialize MEPTC events
   * arguments:
   *  pSector - identifies sector
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL s14meptc_init(
    TMWSCTR *pSector);

  /* function: s14meptc_close
   * purpose: Close MEPTC events
   * arguments:
   *  pSector - identifies sector
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL s14meptc_close(
    TMWSCTR *pSector);

  /* function: s14meptc_addEvent
   * purpose: Create and queue an MEPTC event
   * arguments:
   *  pSector - identifies sector
   *  ioa - information object address
   *  oci - Output curcuit command of protection equipment, 
   *        defined in 7.2.6.12
   *  qdp - quality descriptor of protection equipment, defined in 7.2.6.4
   *  relayOperatingTime - two octet relay operating time
   *  pTimeStamp - time of event
   *   To set RES1 bit to indicate Substituted time, 
   *    set pTimeStamp->genuineTime to TMWEFS_FALSE and make sure
   *    S14DATA_SUPPORT_GENUINE_TIME is defined in s14data.h
   *   pTimeStamp == TMWDEFS_NULL is not allowed, this cannot be sent without time
   * returns:
   *  void * - non-NULL value indicates success; TMWDEFS_NULL indicates failure
   */
  TMWDEFS_SCL_API void * TMWDEFS_GLOBAL s14meptc_addEvent(
    TMWSCTR *pSector, 
    TMWTYPES_ULONG ioa, 
    TMWTYPES_UCHAR oci,
    TMWTYPES_UCHAR qdp,
    TMWTYPES_USHORT relayOperatingTime,
    TMWDTIME *pTimeStamp);

  /* function: s14meptc_countEvents
   * purpose: count MEPTC events
   * arguments:
   *  pSector - identifies sector
   * returns:
   *  number of events in queue
   */
  TMWTYPES_USHORT TMWDEFS_GLOBAL s14meptc_countEvents(
    TMWSCTR *pSector);

  /* function: s14meptc_scanForChanges
   * purpose: Scans slave database for changes to MEPTC data points
   * arguments:
   *  pSector - identifies sector
   * returns:
   *  TMWDEFS_TRUE if successful, else TMWDEFS_FALSE
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14meptc_scanForChanges(
    TMWSCTR *pSector);

  /* function: s14meptc_processevents
   * purpose: Process MEPTC events into a message
   * arguments:
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   *  pEventTime - returns time of earliest event if a clock sync is required
   * returns:
   *  S14EVNT_SENT                - ASDU was sent
   *  S14EVNT_CLOCK_SYNC_REQUIRED - Spontaneous CCSNA is required
   *  S14EVNT_NOT_SENT            - No ASDU was sent
   */
  S14EVNT_STATUS TMWDEFS_GLOBAL s14meptc_processEvents(
    TMWSCTR *pSector,
    TMWDTIME *pEventTime);

#ifdef __cplusplus
}
#endif
#endif /* S14MEP_DEFINED */
