/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:S101Sector.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief Protocol Sector - TMW artifact for handling part of a session
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19 Jul 2016     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "S101Sector.h"
#include "S101Protocol.h"
#include "S101Debug.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
//static  Logger& log(Logger::getLogger(SUBSYSTEM_ID_SCADA_HANDLER));

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
S101Sector::S101Sector(S101Session& session):
                       m_session(session),
                       mp_tmwsector(NULL),
                       mp_tmwsectorCnfg(NULL),
                       m_s101db(*this)
{
    /** Initialise handle which will be passed to TMW*/
    memset(&m_twmdbhandle,0,sizeof(I870DBHandleStr));
    m_twmdbhandle.pIEC870DB        = &m_s101db;

    /** Register callback functions */
    m_twmdbhandle.ccsnaSetTime           = S101Database::ccsnaSetTime          ;
    m_twmdbhandle.ccsnaGetTime           = S101Database::ccsnaGetTime          ;
    m_twmdbhandle.mspGetPoint            = S101Database::mspGetPoint          ;
    m_twmdbhandle.mdpGetPoint            = S101Database::mdpGetPoint          ;
    m_twmdbhandle.mmenaGetPoint          = S101Database::mmenaGetPoint        ;
    m_twmdbhandle.mmenbGetPoint          = S101Database::mmenbGetPoint        ;
    m_twmdbhandle.mmencGetPoint          = S101Database::mmencGetPoint        ;
    m_twmdbhandle.mitGetPoint            = S101Database::mitGetPoint          ;
    m_twmdbhandle.cscLookupPoint         = S101Database::cscLookupPoint       ;
    m_twmdbhandle.cdcLookupPoint         = S101Database::cdcLookupPoint       ;
    m_twmdbhandle.hasSectorReset         = S101Database::hasSectorReset       ;
    m_twmdbhandle.crpnaExecuteRTUReset   = S101Database::crpnaExecuteRTUReset;
//    m_twmdbhandle.mitFreeze              = S101Database::mitFreeze            ;
//    m_twmdbhandle.cscSelect              = S101Database::cscSelect            ;
//    m_twmdbhandle.cscExecute             = S101Database::cscExecute           ;
//    m_twmdbhandle.cdcSelect              = S101Database::cdcSelect            ;
//    m_twmdbhandle.cdcExecute             = S101Database::cdcExecute           ;
    m_twmdbhandle.deleteEventID          = S101Database::deleteEventID        ;
}

S101Sector::~S101Sector()
{
    closeSector();

    delete mp_tmwsectorCnfg;
    mp_tmwsectorCnfg = NULL;
}


SCADAP_ERROR S101Sector::openSector(TMWSESN* tmwsesn)
{
    checkNotNull(mp_tmwsectorCnfg, "Sector not configured");

    /*Open sector and register callback functions*/
    mp_tmwsector = s101sctr_openSector(tmwsesn, mp_tmwsectorCnfg, &m_twmdbhandle);
    checkNotNull(mp_tmwsector,"S101 Sector open failed");

    tmwsctr_setUserDataPtr(mp_tmwsector,&m_twmdbhandle);

    /* Update sector handle*/
    m_twmdbhandle.pTMWSector = mp_tmwsector;

    DBG_INFO("%s open sector: %p",TITLE, (void*)mp_tmwsector);
    return SCADAP_ERROR_NONE;
}

void S101Sector::closeSector()
{
    if(mp_tmwsector != NULL)
    {
        s101sctr_closeSector(mp_tmwsector);
        mp_tmwsector = NULL;
    }
}

void S101Sector::configure(const S101SCTR_CONFIG& sectorConfig)
{
    if(mp_tmwsectorCnfg == NULL)
    {
        mp_tmwsectorCnfg = new S101SCTR_CONFIG();
    }

    memcpy(mp_tmwsectorCnfg, &sectorConfig, sizeof(sectorConfig));

    /* Apply configuration to existing sector*/
    if(mp_tmwsector != NULL)
    {
        s101sctr_setSectorConfig(mp_tmwsector, mp_tmwsectorCnfg);
    }
}

S101Database& S101Sector::getS101Database()
{
    return m_s101db;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
