/******************************************************************************
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:SCADAConnectionManager.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   18 Jul 2014     andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "SCADAConnectionManager.h"

#include "GeminiDatabase.h"
#include "PointData.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

SCADAConnectionManager::SCADAConnectionManager(ISlaveProtocolChannel& channel) :
               m_log(Logger::getLogger(SUBSYSTEM_ID_SCADA_HANDLER)),
               m_slaveProtoCh(channel),
               m_initialised(false),
               m_connectionInProgress(false),
               m_connectionDelayInOperation(false),
               m_connectionTriesLeft(0),
               m_connectivityCheckTriesLeft(0),
               m_connectivityCheckIndex(0),
               mp_inhibit(NULL)
{
    // SCADA Connectivity Checking disabled by default
    m_config.m_connectivityCheckConfig.enabled = false;
}

SCADAConnectionManager::~SCADAConnectionManager()
{
    delete m_scadaListener.connectorConfigPtr;
    delete m_scadaListener.listenerConfigPtr;
    delete m_scadaListener.acceptedPtr;
    delete m_scadaListener.connectorPtr;
    delete m_scadaListener.listenerPtr;

    delete m_scadaConnector.connectorConfigPtr;
    delete m_scadaConnector.listenerConfigPtr;
    delete m_scadaConnector.acceptedPtr;
    delete m_scadaConnector.connectorPtr;
    delete m_scadaConnector.listenerPtr;

    if(mp_inhibit != NULL)
    {
        GeminiDatabase::getInstance()->attach(mp_inhibit->getPointID(), mp_inhibit);
        delete mp_inhibit;
    }
}

void SCADAConnectionManager::init()
{
    if (m_initialised == true)
    {
        return;
    }

    // Initialise the Connection Manager Sockets
    // and start the listeners
    if (m_scadaConnector.listenerConfigPtr != NULL)
    {
        m_scadaConnector.listenerPtr  = new TCPSocket();
        m_scadaConnector.listenerPtr->init();

        m_scadaConnector.acceptedPtr = new TCPSocket();
        m_scadaConnector.acceptedPtr->init();

        m_scadaConnector.connectorPtr = new TCPSocket();
        m_scadaConnector.connectorPtr->init();

        // Listen for connection requests from protocol channel
        m_scadaConnector.listenerPtr->listenSocket(m_scadaConnector.listenerConfigPtr->ipAddress.c_str(),
                                                   m_scadaConnector.listenerConfigPtr->port);
    }

    if (m_scadaListener.listenerConfigPtr != NULL)
    {
        m_scadaListener.listenerPtr   = new TCPSocket();
        m_scadaListener.listenerPtr->init();

        m_scadaListener.acceptedPtr   = new TCPSocket();
        m_scadaListener.acceptedPtr->init();

        m_scadaListener.connectorPtr  = new TCPSocket();
        m_scadaListener.connectorPtr->init();

        // Listen for incoming SCADA connections
        m_scadaListener.listenerPtr->listenSocket(m_scadaListener.listenerConfigPtr->ipAddress.c_str(),
                                                  m_scadaListener.listenerConfigPtr->port);
    }

    // Initialise Timeouts
    m_timeouts[TIMER_COMS_INACTIVITY]  = m_config.m_connectionSettingsConfig.inactivityTimeSecs * 1000;
    m_timeouts[TIMER_CONNECT]          = m_slaveProtoCh.getConnectionTimeout();
    m_timeouts[TIMER_CONNECTION_DELAY] = m_config.m_connectionSettingsConfig.connectionRetryDelaySecs * 1000;

    resetConnectionGroupRetries();

    // Initialise Timers
    stopTimer(TIMER_COMS_INACTIVITY);
    stopTimer(TIMER_CONNECT);
    stopTimer(TIMER_CONN_CHECK);
    stopTimer(TIMER_CONN_CHECK_CONNECT);
    stopTimer(TIMER_CONNECTION_DELAY);
    stopTimer(TIMER_NO_INCOMING_CALL);

    if (m_config.m_incomingConConfig.enabled == LU_TRUE)
    {
        m_timeouts[TIMER_NO_INCOMING_CALL] = m_config.m_incomingConConfig.notReceivedHrs * 3600000;
        startTimer(TIMER_NO_INCOMING_CALL);
    }

    if (m_config.m_connectivityCheckConfig.enabled == LU_TRUE)
    {
        resetConnectivityRetries();

        m_timeouts[TIMER_CONN_CHECK] = m_config.m_connectivityCheckConfig.periodMin * 60000;
        startTimer(TIMER_CONN_CHECK);

        // Connectivity check connection timer
        m_timeouts[TIMER_CONN_CHECK_CONNECT] = m_slaveProtoCh.getConnectionTimeout();
    }


    // Init power cycle inhibit
    PointIdStr& inhibitPoint = m_config.m_commsPowerSupplyConfig.powerCycleInhibit;
    if(inhibitPoint.isValid())
    {
        mp_inhibit = new InhibitObserver(inhibitPoint);
        GeminiDatabase::getInstance()->attach(inhibitPoint, mp_inhibit);
    }

    m_initialised = true;
}

void SCADAConnectionManager::tickEvent()
{
    bool makeConnection = false;
    bool resetCommms    = false;
    TimeManager::TimeStr g3Time;

    TimeMan.getTime(g3Time);

    if (m_initialised == false)
    {
        return;
    }

    if ((m_connectionInProgress == false) &&
        (m_connectionDelayInOperation == false) &&
        (m_scadaListener.active == false))
    {
        /* If there are qualifying events in the channel's sessions,
         * tell the channel to connect (this actually makes
         * the channel connect to the SCADAConnectionManager!)
         */
        if ((m_slaveProtoCh.countSessionEvents(m_config.m_connectionSettingsConfig.connectOnEventClassMask, false) > 0) ||
            (g3Time.badTime))
        {
            if (m_slaveProtoCh.connectChannel() == true)
            {
                // TMW thinks it's connected!
                makeConnection = true;
            }
        }

        /* Detect if Channel has made a connection Connection Manager,
         * if it has we must connect to SCADA (this could be caused
         * by the above call to connectChannel())
         *
         */
        if ((checkListener(m_scadaConnector) == true) || (makeConnection))
        {
            // Start connection to SCADA!!
            connectToSCADA();
        }
    }

    checkSCADAConnectionProgress();
    checkConnectivityConnectionProgress();

    /* Check for incoming SCADA connections
     *
     */
    if ((m_connectionInProgress == false) && (m_scadaConnector.active == false))
    {
        if (checkListener(m_scadaListener) == true)
        {
            // Start the socket inactivity here so we correctly timeout
            // on sockets where no data is exchanged
            startTimer(TIMER_COMS_INACTIVITY);

            sniffIncomingSCADAData();
        }
    }

    /* Transfer data between our socket pairs
     *
     */
    if (transferSocketData(m_scadaConnector) != CONNMAN_ERROR_NONE)
    {
        resetCommms = true;
        // If no data passed over the SCADA connector socket
        // then SCADA may have rejected our connection.  To prevent us from hammering the SCADA
        // port we will treat this as a failed connection

        // TODO - SKA - If the RTU has generated an event that caused a connection attempt, but
        // SCADA did not download the events, the RTU will immediately attempt to reconnect.  We
        // need a way to detect this situation - and what do we do if we do detect it?
        // 1. Treat the call as a failure if no alarms are transferred
        // 2. Treat the call as failed and go through the retry logic
        // 3. Only reconnect if a new event is generated
        if ((m_scadaConnector.numBytesIn == 0) && (m_scadaConnector.numBytesOut == 0))
        {
            connectToSCADAFailed();
        }
    }

    if (transferSocketData(m_scadaListener) != CONNMAN_ERROR_NONE)
    {
        resetCommms = true;
    }

    if (resetCommms)
    {
        DBG_INFO("SCADA socket closed. Closing connection.");
        m_log.info("SCADA socket closed. Closing connection.");
        resetSockets();

        stopTimer(TIMER_COMS_INACTIVITY);
    }

    /* If the ScadaListener is active, hold off the no
     * incoming call timer as this is an active incoming call
     *
     */
    if (m_scadaListener.active == true)
    {
        resetTimer(TIMER_NO_INCOMING_CALL);
    }

    /* Check our timers
     *
     */
    checkTimers();
}


SCADAConnectionManager::SocketConfig *SCADAConnectionManager::getSCADAConnectionIPConfig()
{
    return m_scadaConnector.listenerConfigPtr;
}

SCADAConnectionManager::SocketConfig *SCADAConnectionManager::getSCADAListenerIPConfig()
{
    return m_scadaListener.connectorConfigPtr;
}

void SCADAConnectionManager::setSCADAConnectionIPConfig(std::string ipAddress, lu_int32_t port)
{
    if (m_scadaConnector.connectorConfigPtr == NULL)
    {
        SocketConfig *configPtr = new SocketConfig();
        m_scadaConnector.connectorConfigPtr = configPtr;
    }

    // Only allocate local port the first time this is called
    if (m_scadaConnector.listenerConfigPtr == NULL)
    {
        SocketConfig *configPtr = new SocketConfig();
        m_scadaConnector.listenerConfigPtr = configPtr;

        // Details used to listen for Channel incoming connections
        m_scadaConnector.listenerConfigPtr->ipAddress = "127.0.0.1";
        m_scadaConnector.listenerConfigPtr->port      = getLocalPort();
    }

    // Update the details used to connect to SCADA
    m_scadaConnector.connectorConfigPtr->ipAddress = ipAddress;
    m_scadaConnector.connectorConfigPtr->port      = port;


    DBG_INFO("Connect to SCADA on %s:%d", m_scadaConnector.connectorConfigPtr->ipAddress.c_str(),
                                          m_scadaConnector.connectorConfigPtr->port);

    DBG_INFO("TMW Channel connects to ConMan on %s:%d", m_scadaConnector.listenerConfigPtr->ipAddress.c_str(),
                                                        m_scadaConnector.listenerConfigPtr->port);
}

void SCADAConnectionManager::setSCADAListenerIPConfig(std::string ipAddress, lu_int32_t port)
{
    if (m_scadaListener.listenerConfigPtr == NULL)
    {
        SocketConfig *configPtr = new SocketConfig();
        m_scadaListener.listenerConfigPtr = configPtr;
    }

    if (m_scadaListener.connectorConfigPtr == NULL)
    {
        SocketConfig *configPtr = new SocketConfig();
        m_scadaListener.connectorConfigPtr = configPtr;

        // Details used for the Channel to listen for incoming SCADA connections on
        m_scadaListener.connectorConfigPtr->ipAddress = "127.0.0.1";
        m_scadaListener.connectorConfigPtr->port = getLocalPort();
    }

    // Details used to listen for incoming SCADA connections
    m_scadaListener.listenerConfigPtr->ipAddress = ipAddress;
    m_scadaListener.listenerConfigPtr->port      = port;

    DBG_INFO("Listen for SCADA on %s:%d", m_scadaListener.listenerConfigPtr->ipAddress.c_str(),
                                          m_scadaListener.listenerConfigPtr->port);

    DBG_INFO("Connect to TMW Channel on %s:%d", m_scadaListener.connectorConfigPtr->ipAddress.c_str(),
                                                        m_scadaListener.connectorConfigPtr->port);
}

void SCADAConnectionManager::setSCADAConnManConfig(SCADAConnManConfig config)
{
    m_config = config;
}

lu_int32_t SCADAConnectionManager::getLocalPort()
{
    for (lu_int32_t i = 0; (i < LOCALPORTSPOOL); ++i)
    {
        // Check to see if this port is available
        if (localPorts[i] == 0)
        {
            localPorts[i] = LOCALPORTSTART + i;
            return localPorts[i];
        }
    }

    return -1;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


lu_int32_t SCADAConnectionManager::getUint16FromBuff(lu_uint8_t *buf, lu_uint16_t *val)
{
    lu_uint8_t *a, *b;

    if ((buf == NULL) || (buf+1 == NULL) || (val == NULL))
    {
        return -1;
    }

    a = buf;
    b = buf+1;

    *val = *b;
    *val = (*val << 8) | *a;

    return 0;
}

lu_int32_t SCADAConnectionManager::parseDNP3Packet(lu_uint8_t *buf, lu_int32_t bufLen, lu_uint16_t *srcAddr, lu_uint16_t *destAddr)
{
    lu_uint16_t header;


    if ((buf == NULL) && (bufLen > 8))
    {
        return -1;
    }

    if (getUint16FromBuff(buf, &header) != 0)
    {
        return -1;
    }

    if (header != Dnp3Header)
    {
        m_log.warn("parseDNP3Packet: header [%d] expected [%d]!\n", header, Dnp3Header);
        return -1;
    }

    if (getUint16FromBuff(buf+4, destAddr) != 0)
    {
        return -1;
    }

    if (getUint16FromBuff(buf+6, srcAddr) != 0)
    {
        return -1;
    }

    return 0;
}

bool SCADAConnectionManager::checkListener(SocketPair &socketPair)
{
    // Only check SCADA Listener if we are in Server or Dual-Endpoint mode
    if (socketPair.listenerPtr == NULL)
    {
        return false;
    }

    if (socketPair.active == true)
    {
        // Already connected - no new connection required
        return false;
    }

    if (socketPair.listenerPtr->getState() == TCPSocket::SOCK_STATE_LISTENING)
    {
        // Listen for incoming SCADA Connections
        if ((!socketPair.acceptedPtr->isConnecting()) &&
            (!socketPair.acceptedPtr->isConnected()))
        {
            char connectingIpAddress[20];

            if (socketPair.listenerPtr->acceptSocket(socketPair.acceptedPtr, connectingIpAddress) == TCPSocket::SOCK_ERROR_NONE)
            {
                if (socketPair.acceptedPtr->isConnected() == true)
                {
                    bool ipOK = true;

                    // If the connectingIpAddress is the loopback it's OK
                    if (strncmp(connectingIpAddress, "127.0.0.1", 20) == 0)
                    {
                        return true;
                    }

                    // If we must validate incoming IP Addresses we shall check against the failover list
                    if (m_config.m_validateMasterIP == true)
                    {
                        if ((ipOK = m_slaveProtoCh.findSCADAIPAddress(connectingIpAddress)) == false)
                        {
                            DBG_INFO("SCADAConnectionMan: Connection from [%s]- IP not in group", connectingIpAddress);
                            m_log.warn("SCADAConnectionMan: Connection from [%s]- IP not in group", connectingIpAddress);
                            socketPair.acceptedPtr->closeSocket();
                        }
                    }

                    m_log.info("SCADAConnectionMan: Connection from [%s]- Accepted", connectingIpAddress);

                    return ipOK;
                }
            }
            else
            {
                m_log.error("SCADAConnectionMan: Failed to accept incoming call - reset sockets");
                deactivateSocketPair(socketPair);
                socketPair.listenerPtr->listenSocket(socketPair.listenerConfigPtr->port);
            }
        }
    }
    else
    {
        /* The listenerPtr should ALWAYS be listening.
         * If not, reinitialise it and get it listening again
         */
        socketPair.listenerPtr->init();
        socketPair.listenerPtr->listenSocket(socketPair.listenerConfigPtr->ipAddress.c_str(),
                                             socketPair.listenerConfigPtr->port);
    }

    return false;
}


bool SCADAConnectionManager::connectivityCheck()
{
    if (m_config.m_connectivityCheckConfig.enabled == true)
    {
        ISlaveProtocolChannel::ScadaAddress address;

        if (m_slaveProtoCh.getSCADAAddressIndex(m_connectivityCheckIndex, address) == false)
        {
            m_log.error("SCADAConnectionManager: Unable to get current SCADA Address");
            return false;
        }

        m_log.info("SCADAConnectionMan: Connectivity check to [%s:%d] Tries left [%d]",
                        address.ip.c_str(),
                        m_config.m_connectivityCheckConfig.checkIPPort,
                        m_connectivityCheckTriesLeft);


        m_connectivityCheckSock.closeSocket();

        if (m_connectivityCheckSock.connectSocket(address.ip.c_str(),
                                       m_config.m_connectivityCheckConfig.checkIPPort,
                                       LU_TRUE) == TCPSocket::SOCK_ERROR_NONE)
        {
            // If the connection was started, start the Connect timer
            startTimer(TIMER_CONN_CHECK_CONNECT);
        }
        else
        {
            m_log.warn("SCADAConnectionMan: Connectivity check to [%s:%d] failed",
                            address.ip.c_str(),
                            m_config.m_connectivityCheckConfig.checkIPPort);

            connectivityCheckFailed();

            return false;
        }
    }

    return true;
}


void SCADAConnectionManager::connectivityCheckFailed()
{
    // Stop the Connect Timer
    stopTimer(TIMER_CONN_CHECK_CONNECT);

    // Close any sockets
    m_connectivityCheckSock.closeSocket();

    // Alter retries
    setNextConnectivityCheck();
}


void SCADAConnectionManager::checkConnectivityConnectionProgress()
{
    if (m_timers[TIMER_CONN_CHECK_CONNECT].isStarted())
    {
        struct timeval timeout = { 0, 50000 };
        m_connectivityCheckSock.updateConnectedState(timeout);

        if (m_connectivityCheckSock.isConnected())
        {
            DBG_INFO("Connectivity Check Connected");
            m_log.info("SCADAConnectionMan: Connectivity check success");

            resetConnectivityRetries();
            m_connectivityCheckSock.closeSocket();
            stopTimer(TIMER_CONN_CHECK_CONNECT);
        }
    }
}

void SCADAConnectionManager::setNextConnectivityCheck()
{
    // Attempt the next connectivity check on the next SCADA
    m_connectivityCheckIndex++;
    if (m_connectivityCheckIndex >= m_slaveProtoCh.getNumSCADAAddresses())
    {
        m_connectivityCheckIndex = 0;
        m_connectivityCheckTriesLeft--;
    }
    else
    {
        // Immediately try the next SCADA
        connectivityCheck();
    }

    if (m_connectivityCheckTriesLeft == 0)
    {
        // If all the tries left are zero we cycle the power
        cycleCommsPower();

        // After cycling the CommsPower, reset all the retries
        resetConnectivityRetries();
    }
}

void SCADAConnectionManager::resetConnectivityRetries()
{
    m_connectivityCheckTriesLeft = m_config.m_connectivityCheckConfig.retries;

    // Restart the connectivity check timer
    resetTimer(TIMER_CONN_CHECK);
}

void SCADAConnectionManager::resetConnectionGroupRetries()
{
    // The m_outgoingConConfig.retries setting is the number of retries over the whole failover group.
    // Multiplying this by the number of SCADAAddresses gives us the actual number of SCADA connection
    // tries
    m_connectionTriesLeft = m_config.m_outgoingConConfig.retries * m_slaveProtoCh.getNumSCADAAddresses();
}


CONNMAN_ERROR SCADAConnectionManager::transferSocketData(SocketPair &sockPair)
{
    lu_int32_t bytesRead;
    CONNMAN_ERROR err = CONNMAN_ERROR_NONE;

    if ((sockPair.connectorPtr == NULL) || (sockPair.listenerPtr == NULL) ||
        (sockPair.active == false))
    {
        return CONNMAN_ERROR_NONE;
    }

    if (!sockPair.connectorPtr->isConnected() || !sockPair.acceptedPtr->isConnected())
    {
        return CONNMAN_ERROR_NONE;
    }

    if ((bytesRead = sockPair.connectorPtr->readSocketMs(mp_buf, BUFFER_SIZE, 0)) > 0)
    {
        if (sockPair.acceptedPtr->writeSocket(mp_buf, bytesRead) == TCPSocket::SOCK_ERROR_NONE)
        {
            DBG_INFO("TX [%d] bytes to SCADA from Channel", bytesRead);
            resetTimer(TIMER_COMS_INACTIVITY);
            sockPair.numBytesOut += bytesRead;
        }
    }
    if (bytesRead < 0)
    {
        m_log.debug("ConnectionManager: Failed to read from socket");
        err = CONNMAN_ERROR_SOCKET;
    }

    if ((bytesRead = sockPair.acceptedPtr->readSocketMs(mp_buf, BUFFER_SIZE, 0)) > 0)
    {
        if (sockPair.connectorPtr->writeSocket(mp_buf, bytesRead) == TCPSocket::SOCK_ERROR_NONE)
        {
            DBG_INFO("TX [%d] bytes to Channel from SCADA", bytesRead);
            resetTimer(TIMER_COMS_INACTIVITY);
            sockPair.numBytesIn += bytesRead;
        }
    }
    if (bytesRead < 0)
    {
        m_log.debug("ConnectionManager: Failed to read from socket");
        err = CONNMAN_ERROR_SOCKET;
    }

    return err;
}

void SCADAConnectionManager::deactivateSocketPair(SocketPair &sockPair)
{
    if (sockPair.connectorPtr != NULL)
    {
        sockPair.connectorPtr->init();
    }

    if (sockPair.acceptedPtr != NULL)
    {
        // Simply close the accepted socket - do no init()
        sockPair.acceptedPtr->closeSocket();
    }

    sockPair.active      = false;
    sockPair.numBytesIn  = 0;
    sockPair.numBytesOut = 0;
}

lu_int32_t SCADAConnectionManager::sniffIncomingPacket(TCPSocket& sock, lu_uint16_t *address, bool *validMaster)
{
    // TODO - Extract from Data Link Receive Timeout!!
    struct timeval timeout = { 5, 0 }; // 5 Seconds
    lu_int32_t     bytesRead = -1;

    if (validMaster == NULL)
    {
        return -1;
    }

    *validMaster = false;

    if ((bytesRead = sock.readSocket(mp_buf, BUFFER_SIZE, &timeout, MSG_PEEK)) > 0)
    {
        lu_uint16_t masterAddr;
        lu_uint16_t slaveAddr;

        if (parseDNP3Packet(mp_buf, bytesRead, &masterAddr, &slaveAddr) == 0)
        {
            m_log.info("SCADAConnectionMan: Incoming packet src[%d] dest[%d]", masterAddr, slaveAddr);
            // Attempt to set the Current Master details in the channel.
            // If the masterAddr is not in the failover list the connection should be
            // rejected
            *validMaster = m_slaveProtoCh.setMasterAddress(masterAddr);
            *address = masterAddr;
        }
    }

    return bytesRead;
}

void SCADAConnectionManager::sniffIncomingSCADAData()
{
    bool validIncomingMaster = false;

    if ((m_scadaListener.acceptedPtr == NULL) || (m_scadaListener.connectorPtr == NULL))
    {
        return;
    }

    // If we have a connection to SCADA but have not determined which SCADA Master we're
    // connected to we keep sniffing data until we do
    if ((m_scadaListener.acceptedPtr->isConnected()) && (m_scadaListener.active == false))
    {
        lu_uint16_t masterAddr;

        if (sniffIncomingPacket(*(m_scadaListener.acceptedPtr), &masterAddr, &validIncomingMaster) > 0)
        {
            if (validIncomingMaster == false)
            {
                DBG_INFO("SCADAConnectionMan: Incoming SCADA Connection - invalid Master Address [%d]", masterAddr);
                m_log.warn("SCADAConnectionMan: Incoming SCADA Connection - invalid Master Address [%d]", masterAddr);

                stopTimer(TIMER_COMS_INACTIVITY);
                resetSockets();

                return;
            }

            if ((!m_scadaListener.connectorPtr->isConnecting()) &&
                (!m_scadaListener.connectorPtr->isConnected()))
            {
                DBG_INFO("Incoming connection from a valid Master Address [%d]", masterAddr);

                // Blocking connect() call to the local socket to join up with the incoming SCADA socket
                if (m_scadaListener.connectorPtr->connectSocket((lu_char_t *)"127.0.0.1",
                                                                m_scadaListener.connectorConfigPtr->port,
                                                                LU_FALSE
                                                               ) == TCPSocket::SOCK_ERROR_NONE)
                {
                    m_scadaListener.active = true;
                    m_slaveProtoCh.setSessionsOnline(true);
//                    startTimer(TIMER_COMS_INACTIVITY);
                    resetTimer(TIMER_NO_INCOMING_CALL);

                    m_log.info("SCADAConnectionMan: Incoming SCADA Connection OK from known Master [%d]", masterAddr);
                }
            }
        }
    }
}

void SCADAConnectionManager::connectToSCADA()
{
    if ((m_connectionInProgress == true) ||
        (m_scadaConnector.active == true) ||
        (m_scadaConnector.connectorPtr == NULL))
    {
        return;
    }

    if (m_scadaConnector.connectorPtr->isConnecting() ||
        m_scadaConnector.connectorPtr->isConnected())
    {
        return;
    }

    // If there's a connectionDelay in action we need to wait until it's expired.
    if (m_connectionDelayInOperation)
    {
        return;
    }

    ISlaveProtocolChannel::ScadaAddress currentSCADA;

    // Start the connection timer
    startTimer(TIMER_CONNECT);
    m_connectionInProgress = true;

    // Get connection details from the current failover channel
    if (m_slaveProtoCh.getCurrentSCADAAddress(currentSCADA) == false)
    {
        m_log.error("SCADAConnectionManager: Unable to get current SCADA Address");
        return;
    }

    // Set the scadaConnector details to the current SCADA
    setSCADAConnectionIPConfig(currentSCADA.ip, currentSCADA.port);

    // Set all the sessions' Master Addresses to the current Master
    m_slaveProtoCh.setMasterAddress(currentSCADA.masterAddress);

    // Non-blocking connect() call to SCADA
    if (m_scadaConnector.connectorPtr->connectSocket(m_scadaConnector.connectorConfigPtr->ipAddress.c_str(),
                                                    m_scadaConnector.connectorConfigPtr->port,
                                                    LU_TRUE
                                                   ) == TCPSocket::SOCK_ERROR_NONE)
    {
        m_log.info("Connecting to SCADA [%s:%d] Master:%d Tries Left:%d", currentSCADA.ip.c_str(),
                                                currentSCADA.port,
                                                currentSCADA.masterAddress, m_connectionTriesLeft);

        // Ignore return code - the connection will timeout if there's a problem
    }
    else
    {
        m_log.error("Error connecting to SCADA [%s:%d] Master:%d", currentSCADA.ip.c_str(),
                                                                   currentSCADA.port,
                                                                   currentSCADA.masterAddress);

        connectToSCADAFailed();
    }
}


void SCADAConnectionManager::checkSCADAConnectionProgress()
{
    if (m_connectionInProgress == false)
    {
        return;
    }

    // Update the socket status
    if (m_scadaConnector.connectorPtr->isConnecting() == LU_TRUE)
    {
        struct timeval timeout = { 0, 50000 };
        m_scadaConnector.connectorPtr->updateConnectedState(timeout);
    }

    if (m_scadaConnector.connectorPtr->isConnected() == LU_TRUE)
    {
        DBG_INFO("Connected to SCADA [%s]", m_scadaConnector.connectorConfigPtr->ipAddress.c_str());
        m_log.info("SCADAConnectionMan: Connected to SCADA [%s]", m_scadaConnector.connectorConfigPtr->ipAddress.c_str());

        stopTimer(TIMER_CONNECT);
        m_connectionInProgress = false;
        m_scadaConnector.active = true;
        m_slaveProtoCh.setSessionsOnline(true);

        // Reset Connection Retries
        resetConnectionGroupRetries();

        // Reset the connection delay
        stopTimer(TIMER_CONNECTION_DELAY);
        m_connectionDelayInOperation = false;

        // Start the Communicating Timer
        startTimer(TIMER_COMS_INACTIVITY);
    }
}

void SCADAConnectionManager::connectToSCADAFailed()
{
    resetSockets();

    // Connect to the Next SCADA in the failover group next time
    m_slaveProtoCh.setNextSCADAAddress();
    m_connectionInProgress = false;

    if (m_connectionTriesLeft == 0)
    {
        resetConnectionGroupRetries();
        if (m_config.m_outgoingConConfig.enabled == LU_TRUE)
        {
            // Exhausted the number of failover group retries, reset the retries and power cycle the comms device
            cycleCommsPower();

            // Delay any pending connections by the Initialisation Time of the comms device
            m_timeouts[TIMER_CONNECTION_DELAY] = m_config.m_commsPowerSupplyConfig.initTimeSecs * 1000;

            // Start the connection retry delay
            startTimer(TIMER_CONNECTION_DELAY);
            m_connectionDelayInOperation = true;
            m_log.info("SCADAConnectionManager: Power cycle initialisation delay of [%d mS]", m_timeouts[TIMER_CONNECTION_DELAY]);

            return;
        }
    }
    else
    {
        // Only report this error the first time after a reset
        if (m_connectionTriesLeft == m_config.m_outgoingConConfig.retries * m_slaveProtoCh.getNumSCADAAddresses())
        {
            m_log.info("Connection to SCADA timed out");
        }
        m_connectionTriesLeft--;
    }

    // If the next SCADA is the top of the failover group, apply the connection delay
    if (m_slaveProtoCh.getCurrentSCADAIndex() == 0)
    {
        // Delay any pending connections by failover group Retry Delay
        m_timeouts[TIMER_CONNECTION_DELAY] = m_config.m_connectionSettingsConfig.connectionRetryDelaySecs * 1000;

        // Start the connection retry delay
        startTimer(TIMER_CONNECTION_DELAY);
        m_connectionDelayInOperation = true;
        m_log.info("SCADAConnectionManager: Connection retry delay of [%d mS]", m_timeouts[TIMER_CONNECTION_DELAY]);
    }
}

void SCADAConnectionManager::resetSockets()
{
    // Reset all channel sockets and set sessions offline
    deactivateSocketPair(m_scadaConnector);
    deactivateSocketPair(m_scadaListener);
    m_slaveProtoCh.setSessionsOnline(false);

    DBG_INFO("Resetting communication sockets");
    m_log.debug("Resetting communication sockets");
}

IOM_ERROR SCADAConnectionManager::cycleCommsPower()
{
    PowerSupplyCANChannel *PowerSupplyChan;
    IOM_ERROR              ret = IOM_ERROR_NONE;

    // Reset the sockets
    resetSockets();

    // Cycle the Comms Power
    if (m_config.m_commsPowerSupplyConfig.powerSupplyChanPtr != NULL)
    {
        // Check inhibit
        if(mp_inhibit != NULL
            && mp_inhibit->getValue() == LU_TRUE)
        {
            m_log.warn("SCADAConnectionMan: Cycling power inhibited");
            return IOM_ERROR_NONE;
        }

        PowerSupplyChan = (PowerSupplyCANChannel*) m_config.m_commsPowerSupplyConfig.powerSupplyChanPtr;
        m_log.info("SCADAConnectionMan: Cycling power to Comms Device");

        //Send command to the channel
        ret = PowerSupplyChan->selOperate(1,          //Ignored by the PSM
                                          LU_FALSE,   //Ignored by the PSM
                                          m_config.m_commsPowerSupplyConfig.durationSecs //duration
                                         );
        if (ret != IOM_ERROR_NONE)
        {
            m_log.error("SCADAConnectionMan: Error cycling power to Comms Device");
            ret = PowerSupplyChan->cancel();
        }
    }

    return ret;
}

void SCADAConnectionManager::checkTimers()
{
    for (lu_uint32_t timer = 0; timer < ISCADAConnectionManager::TIMER_LAST; timer++)
    {
        if (m_timers[timer].isTimeout() == true)
        {
            switch (timer)
            {
                case TIMER_COMS_INACTIVITY:
                    DBG_INFO("TIMER_COMS_INACTIVITY Expired! Closing sockets");
                    m_log.info("SCADAConnectionMan: Communications inactivity - closing socket");

                    resetSockets();

                    stopTimer(TIMER_COMS_INACTIVITY);
                    break;

                case TIMER_CONNECT:
                    DBG_INFO("TIMER_CONNECT Expired! Closing sockets and setting next SCADA");

                    connectToSCADAFailed();

                    // Stop this timer, only restart if a new connection is attempted
                    stopTimer(TIMER_CONNECT);
                    break;

                case TIMER_CONN_CHECK:
                    DBG_INFO("TIMER_CONN_CHECK Expired!");

                    /* Connect to the current SCADA Channel's Connectivity Check Port
                     * but only if we're not already online
                     */
                    if ((m_scadaConnector.active == false) && (m_scadaListener.active == false))
                    {
                        connectivityCheck();
                    }
                    resetTimer(TIMER_CONN_CHECK);
                    break;

                case TIMER_CONN_CHECK_CONNECT:
                    DBG_INFO("TIMER_CONN_CHECK_CONNECT Expired!");
                    m_log.warn("SCADAConnectionMan: Connectivity check connect timed out");

                    connectivityCheckFailed();
                    break;

                case TIMER_CONNECTION_DELAY:
                    m_connectionDelayInOperation = false;
                    stopTimer(TIMER_CONNECTION_DELAY);
                    break;

                case TIMER_NO_INCOMING_CALL:
                    DBG_INFO("TIMER_NO_INCOMING_CALL Expired!");
                    m_log.warn("SCADAConnectionMan: No incoming call for %i hours, cycling comms power", m_config.m_incomingConConfig.notReceivedHrs);
                    cycleCommsPower();
                    resetTimer(TIMER_NO_INCOMING_CALL);
                    break;
            }
        }
    }
}

void SCADAConnectionManager::startTimer(TIMER timer)
{
    if (timer < TIMER_LAST)
    {
        // Don't start timers that have a 0 timeout
        if (m_timeouts[timer] > 0)
        {
            m_timers[timer].start(m_timeouts[timer]);
        }
    }
}

void SCADAConnectionManager::resetTimer(TIMER timer)
{
    if (timer < TIMER_LAST)
    {
        m_timers[timer].reset();
    }
}

void SCADAConnectionManager::stopTimer(TIMER timer)
{
    if (timer < TIMER_LAST)
    {
        m_timers[timer].stop();
    }
}

SCADAConnectionManager::InhibitObserver::InhibitObserver(PointIdStr& pid):m_pid(pid)
{}

void SCADAConnectionManager::InhibitObserver::update(PointIdStr pointID, PointData *pointDataPtr)
{
    if(pointID == m_pid)
        m_value = (lu_uint8_t)(*pointDataPtr);
}

PointIdStr SCADAConnectionManager::InhibitObserver::getPointID()
{
return m_pid;
}

/*
 *********************** End of file ******************************************
 */
