/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14sctr.h
 * description: IEC 60870-5-101/104 Slave sector
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/utils/tmwtarg.h"
#include "tmwscl/utils/tmwchnl.h"

#include "tmwscl/i870/s14sctr.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/i870chnl.h"
#include "tmwscl/i870/s14sesn.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/s14rbe.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14mem.h"

/* function: _backgroundTimeout */
static void TMWDEFS_CALLBACK _backgroundTimeout(void *pParam)
{
  TMWSCTR *pSector = (TMWSCTR *)pParam;
  TMWSESN *pSession = pSector->pSession;
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  if(pSession->online == TMWSESN_STAT_ONLINE)
  {
    p14Sector->backgroundGroupIndex = 0;
    p14Sector->backgroundPointIndex = 0;

    /* Tell link layer we have something to transmit */
    pSession->pChannel->pLink->pLinkDataReady(
      pSession->pChannel->pLinkContext, pSession);
  }

  if(p14Sector->backgroundPeriod != 0)
  {
    tmwtimer_start(&p14Sector->backgroundTimer, 
      p14Sector->backgroundPeriod, pSession->pChannel,
      _backgroundTimeout, pSector);
  }
}

/* function: _infoCallback */
static void TMWDEFS_CALLBACK _infoCallback(
  TMWSCTR *pSector, 
  TMWSCL_INFO sesnInfo)
{
#if S14DATA_SUPPORT_CICNAWAIT
  S14SESN *p14Session = (S14SESN *)pSector->pSession;
#endif

  if(sesnInfo == TMWSCL_INFO_RESET_USER)
  {
#if S14DATA_SUPPORT_CRPNA
    s14data_crpnaExecute(pSector, I14DEF_QRP_RESET_GENERAL);
#endif
    return;
  }
#if S14DATA_SUPPORT_CICNAWAIT
  if(p14Session->cyclicWaitCICNAComplete)
  {
    S14SCTR *p14Sector = (S14SCTR *)pSector;
    
    /* For 104 use STARTDT as the indicator
     * For 101 use ONLINE as the indicator
     */
    if((sesnInfo == TMWSCL_INFO_STARTDT)
      || ((pSector->pSession->protocol == TMWTYPES_PROTOCOL_101) && (sesnInfo == TMWSCL_INFO_ONLINE)))
    {
      p14Sector->CICNAComplete = TMWDEFS_FALSE;

      if(p14Sector->cyclicPeriod != 0)
      { 
        p14Sector->i870.cyclicPending = TMWDEFS_FALSE;
        p14Sector->cyclicGroupIndex = GROUP_INDEX_IDLE;
        tmwtimer_start(&p14Sector->cyclicTimer,
          p14Sector->cyclicFirstPeriod, pSector->pSession->pChannel,
          s14sctr_cyclicTimeout, pSector);
      }
      if(p14Sector->backgroundPeriod != 0)
      { 
        p14Sector->backgroundGroupIndex = GROUP_INDEX_IDLE;
        tmwtimer_start(&p14Sector->backgroundTimer,
          p14Sector->backgroundPeriod, pSector->pSession->pChannel,
          _backgroundTimeout, pSector);
      }
    } 
    else if(sesnInfo == TMWSCL_INFO_OFFLINE)
    {
      p14Sector->CICNAComplete = TMWDEFS_FALSE;

      p14Sector->i870.cyclicPending = TMWDEFS_FALSE;
      p14Sector->cyclicGroupIndex = GROUP_INDEX_IDLE;
      tmwtimer_cancel(&p14Sector->cyclicTimer);

      p14Sector->backgroundGroupIndex = GROUP_INDEX_IDLE;
      tmwtimer_cancel(&p14Sector->backgroundTimer);
    }
  }
#endif
}

/* function: s14sctr_openSector */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14sctr_openSector( 
  TMWSCTR *pSector, 
  void *pUserHandle)
{
  TMWTYPES_BOOL status = TMWDEFS_TRUE;
  S14SCTR *p14Sector = (S14SCTR *)pSector;
#if S14DATA_SUPPORT_CICNAWAIT
  S14SESN *p14Session = (S14SESN *)pSector->pSession;
#endif

  /* Misc */
  p14Sector->propagationDelay = 0;
  tmwtimer_init(&p14Sector->clockValidTimer);
  
  p14Sector->i870.pProcessInfoFunc = _infoCallback;

  /* Response to unknown  ASDU type id, asdu address, COT or IOA */
  p14Sector->pUnknownResponse = TMWDEFS_NULL;
 
#if S14DATA_SUPPORT_CICNA
  /* General Interrogation Command */
  p14Sector->cicnaCOT = 0;
  p14Sector->cicnaIOA = 0;
  p14Sector->cicnaDataReady = TMWDEFS_FALSE;  
  p14Sector->cicnaIsBroadcast = TMWDEFS_FALSE;
  p14Sector->cicnaQualifier = 0;
  p14Sector->cicnaGroupIndex = GROUP_INDEX_IDLE;
  p14Sector->cicnaPointIndex = 0;
  p14Sector->cicnaOriginator = 0;
  p14Sector->groupMask = 0;
#endif

#if S14DATA_SUPPORT_CCINA
  /* Counter Interrogation Command */
  p14Sector->ccinaCOT = 0;
  p14Sector->ccinaIOA = 0;
  p14Sector->ccinaDataReady = TMWDEFS_FALSE;
  p14Sector->ccinaQCC = 0;
  p14Sector->ccinaGroupIndex = GROUP_INDEX_IDLE;
  p14Sector->ccinaPointIndex = 0;
  p14Sector->ccinaOriginator = 0;
#endif

#if S14DATA_SUPPORT_CRDNA
  /* Read Command */
  p14Sector->crdnaCOT = 0;
  p14Sector->crdnaIOA = 0;
  p14Sector->crdnaDataReady = TMWDEFS_FALSE;
  p14Sector->crdnaOriginator = 0;
#endif

#if S14DATA_SUPPORT_CCSNA
  /* Clock Synchronization Command */
  p14Sector->ccsnaCOT = 0;
  p14Sector->ccsnaIOA = 0;
  p14Sector->ccsnaOriginator = 0;  
  p14Sector->ccsnaRespNumber = 0;
#endif

#if S14DATA_SUPPORT_CRPNA
  /* Reset Process Command */
  p14Sector->crpnaCOT = 0;
  p14Sector->crpnaIOA = 0;
  p14Sector->crpnaQRP = 0;
  p14Sector->crpnaOriginator = 0;
#endif

#if S14DATA_SUPPORT_MULTICMDS
  tmwdlist_initialize(&p14Sector->commandList);
#else
#if S14DATA_SUPPORT_CSC
  /* Single Command */
  p14Sector->cscnaCOT = 0;
  p14Sector->csctaCOT = 0;
  p14Sector->cscnaIOA = 0;
  p14Sector->cscnaSCO = 0;
  p14Sector->cscnaOriginator = 0;
  p14Sector->pCSCPoint = TMWDEFS_NULL;
  p14Sector->cscnaState = TMWDEFS_CMD_STATE_IDLE;
  p14Sector->cscnaStatus = TMWDEFS_CMD_STAT_SUCCESS;
  tmwtimer_init(&p14Sector->cscnaSelectTimer);
#endif

#if S14DATA_SUPPORT_CDC
  /* Double Command */
  p14Sector->cdcnaCOT = 0;
  p14Sector->cdctaCOT = 0;
  p14Sector->cdcnaIOA = 0;
  p14Sector->cdcnaDCO = 0;
  p14Sector->cdcnaOriginator = 0;
  p14Sector->pCDCPoint = TMWDEFS_NULL;
  p14Sector->cdcnaState = TMWDEFS_CMD_STATE_IDLE;
  p14Sector->cdcnaStatus = TMWDEFS_CMD_STAT_SUCCESS;
  tmwtimer_init(&p14Sector->cdcnaSelectTimer);
#endif

#if S14DATA_SUPPORT_CRC
  /* Regulating Step Command */
  p14Sector->crcnaCOT = 0;
  p14Sector->crctaCOT = 0;
  p14Sector->crcnaIOA = 0;
  p14Sector->crcnaRCO = 0;
  p14Sector->crcnaOriginator = 0;
  p14Sector->pCRCPoint = TMWDEFS_NULL;
  p14Sector->crcnaState = TMWDEFS_CMD_STATE_IDLE;
  p14Sector->crcnaStatus = TMWDEFS_CMD_STAT_SUCCESS;
  tmwtimer_init(&p14Sector->crcnaSelectTimer);
#endif

#if S14DATA_SUPPORT_CBO
  /* Bitstring Command */
  p14Sector->cbonaCOT = 0;
  p14Sector->cbotaCOT = 0;
  p14Sector->cbonaIOA = 0;
  p14Sector->cbonaBSI = 0;
  p14Sector->cbonaOriginator = 0;
  p14Sector->pCBOPoint = TMWDEFS_NULL;
  p14Sector->cbonaStatus = TMWDEFS_CMD_STAT_SUCCESS;
#endif

#if S14DATA_SUPPORT_CSE_A
  /* Normalized Set Point Command */
  p14Sector->csenaCOT = 0;
  p14Sector->csetaCOT = 0;
  p14Sector->csenaIOA = 0;
  p14Sector->csenaNVA = 0;
  p14Sector->csenaQOS = 0;
  p14Sector->csenaOriginator = 0;
  p14Sector->pCSENAPoint = TMWDEFS_NULL;
  p14Sector->csenaState = TMWDEFS_CMD_STATE_IDLE;
  p14Sector->csenaStatus = TMWDEFS_CMD_STAT_SUCCESS;
  tmwtimer_init(&p14Sector->csenaSelectTimer);
#endif

#if S14DATA_SUPPORT_CSE_B
  /* Scaled Set Point Command */
  p14Sector->csenbCOT = 0;
  p14Sector->csetbCOT = 0;
  p14Sector->csenbIOA = 0;
  p14Sector->csenbSVA = 0;
  p14Sector->csenbQOS = 0;
  p14Sector->csenbOriginator = 0;
  p14Sector->pCSENBPoint = TMWDEFS_NULL;
  p14Sector->csenbState = TMWDEFS_CMD_STATE_IDLE;
  p14Sector->csenbStatus = TMWDEFS_CMD_STAT_SUCCESS;
  tmwtimer_init(&p14Sector->csenbSelectTimer);
#endif
  

#if S14DATA_SUPPORT_CSE_C
  /* Float Set Point Commands */
  p14Sector->csencCOT = 0;
  p14Sector->csetcCOT = 0;
  p14Sector->csencIOA = 0;
  p14Sector->csencFVA = 0.0;
  p14Sector->csencQOS = 0;
  p14Sector->csencOriginator = 0;
  p14Sector->pCSENCPoint = TMWDEFS_NULL;
  p14Sector->csencState = TMWDEFS_CMD_STATE_IDLE;
  p14Sector->csencStatus = TMWDEFS_CMD_STAT_SUCCESS;
  tmwtimer_init(&p14Sector->csencSelectTimer);
#endif

#if S14DATA_SUPPORT_CSE_Z
  /* Set Integrated Totals BCD Command */
  p14Sector->csenzCOT = 0;
  p14Sector->csenzIOA = 0;
  /* p14Sector->csenzBCD */
  p14Sector->csenzQOS = 0;
  p14Sector->csenzOriginator = 0;
  p14Sector->pCSENZPoint = TMWDEFS_NULL;
  p14Sector->csenzState = TMWDEFS_CMD_STATE_IDLE;
  p14Sector->csenzStatus = TMWDEFS_CMD_STAT_SUCCESS;
  tmwtimer_init(&p14Sector->csenzSelectTimer);
#endif
#endif /* !S14DATA_SUPPORT_MULTICMDS */

#if S14DATA_SUPPORT_CCTNA
  /* Set Configuration Table Command */
  p14Sector->cctnaCOT = 0;
  p14Sector->cctnaIOA = 0;
  p14Sector->cctnaOriginator = 0;
#endif

#if S14DATA_SUPPORT_PMENA
  /* Parameter of Measured Value, Normalized Value */
  p14Sector->pmenaCOT = 0;
  p14Sector->pmenaIOA = 0;
  p14Sector->pmenaNVA = 0;
  p14Sector->pmenaQPM = 0;
  p14Sector->pmenaOriginator = 0;
#endif

#if S14DATA_SUPPORT_PMENB
  /* Parameter of Measured Value, Scaled Value */
  p14Sector->pmenbCOT = 0;
  p14Sector->pmenbIOA = 0;
  p14Sector->pmenbSVA = 0;
  p14Sector->pmenbQPM = 0;
  p14Sector->pmenbOriginator = 0;
#endif

#if S14DATA_SUPPORT_PMENC
  /* Parameter of Measured Value, Short Floating Point Value */
  p14Sector->pmencCOT = 0;
  p14Sector->pmencIOA = 0;
  p14Sector->pmencFVA = 0.0;
  p14Sector->pmencQPM = 0;
  p14Sector->pmencOriginator = 0;
#endif

#if S14DATA_SUPPORT_PACNA
  /* Parameter Activation */
  p14Sector->pacnaCOT = 0;
  p14Sector->pacnaIOA = 0;
  p14Sector->pacnaQPA = 0;
  p14Sector->pacnaOriginator = 0;
#endif

#if S14DATA_SUPPORT_FILE
  /* File Transfer */
  p14Sector->fileState         = 0;
  p14Sector->fileIOA           = 0;
  p14Sector->fileName          = 0;
  p14Sector->fileLength        = 0;
  p14Sector->fileSectionName   = 0;
  p14Sector->fileSectionLength = 0;
  p14Sector->fileFRQ           = 0;
  p14Sector->fileSRQ           = 0;
  p14Sector->fileAFQ           = 0;
  p14Sector->fileLSQ           = 0;
  p14Sector->fileCHS           = 0;
  p14Sector->fileFdrtaCOT      = 0;
  p14Sector->fileDirIndex      = 0;
#endif

  /* Setup Report By Exception processing */
  s14rbe_initSector(pSector);
  memset(&p14Sector->lastClockSyncTime, 0, sizeof(TMWDTIME));
  p14Sector->lastClockSyncTime.invalid = TMWDEFS_TRUE;

  /* Setup for cyclic data processing */
  p14Sector->cyclicGroupIndex = GROUP_INDEX_IDLE;
  p14Sector->cyclicPointIndex = 0; 
  p14Sector->i870.cyclicPending = TMWDEFS_FALSE;
  tmwtimer_init(&p14Sector->cyclicTimer);
   
  /* Setup for background scan processing */
  p14Sector->backgroundGroupIndex = GROUP_INDEX_IDLE;
  p14Sector->backgroundPointIndex = 0;
  tmwtimer_init(&p14Sector->backgroundTimer);

#if S14DATA_SUPPORT_CICNAWAIT
  if((S14SESN*)p14Session->cyclicWaitCICNAComplete)
  {
    /* If cyclic data is configured to wait for CICNA complete, 
     * do not start the cyclic and background timers now.
     */
    p14Sector->CICNAComplete = TMWDEFS_FALSE;
  }
  else
#endif
  {
    if(p14Sector->cyclicPeriod != 0)
    { 
      tmwtimer_start(&p14Sector->cyclicTimer, 
        p14Sector->cyclicFirstPeriod, pSector->pSession->pChannel,
        s14sctr_cyclicTimeout, pSector);
    }
    if(p14Sector->backgroundPeriod != 0)
    { 
      tmwtimer_start(&p14Sector->backgroundTimer, 
        p14Sector->backgroundPeriod, pSector->pSession->pChannel,
        _backgroundTimeout, pSector);
    }
  }

  /* Initialize slave database */
  p14Sector->i870.pDbHandle = s14data_init(pSector, pUserHandle);
  if(p14Sector->i870.pDbHandle == TMWDEFS_NULL)
  {
    /* Log error */ 
    
    tmwtimer_cancel(&p14Sector->cyclicTimer);
    tmwtimer_cancel(&p14Sector->backgroundTimer);
    status = TMWDEFS_FALSE;
  }

  return(status);
}

/* function: s14sctr_closeSector */
void TMWDEFS_GLOBAL s14sctr_closeSector(
  TMWSCTR *pSector)
{
  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  S14SESN *pS14Session = (S14SESN *)pSector->pSession;

  /* Lock channel */
  TMWTARG_LOCK_SECTION(&pSector->pSession->pChannel->lock);

  /* If this was the last sector polled start at the head of the
   * list next time through.
   */
  if(pS14Session->pLastSectorPolled == pSector)
    pS14Session->pLastSectorPolled = TMWDEFS_NULL;

#if S14DATA_SUPPORT_MULTICMDS 
  {
    /* Remove any command contexts that are queued */
    S14SCTR_CMD *pContext = TMWDEFS_NULL;
    while((pContext = (S14SCTR_CMD *)tmwdlist_getFirst(&p14Sector->commandList)) != TMWDEFS_NULL)
    { 
      s14util_freeCmdCtxt(p14Sector, pContext);
    }
  }
#else
  /* Cancel all command timers */
#if S14DATA_SUPPORT_CSC
  tmwtimer_cancel(&p14Sector->cscnaSelectTimer);
#endif
#if S14DATA_SUPPORT_CDC
  tmwtimer_cancel(&p14Sector->cdcnaSelectTimer);
#endif
#if S14DATA_SUPPORT_CRC
  tmwtimer_cancel(&p14Sector->crcnaSelectTimer);
#endif
#if S14DATA_SUPPORT_CSE_A
  tmwtimer_cancel(&p14Sector->csenaSelectTimer);
#endif
#if S14DATA_SUPPORT_CSE_B
  tmwtimer_cancel(&p14Sector->csenbSelectTimer);
#endif
#if S14DATA_SUPPORT_CSE_C
  tmwtimer_cancel(&p14Sector->csencSelectTimer);
#endif
#if S14DATA_SUPPORT_CSE_Z
  tmwtimer_cancel(&p14Sector->csenzSelectTimer);
#endif
#endif /* !S14DATA_SUPPORT_MULTICMDS */

  /* Stop scanning for events */
  s14rbe_closeSector(pSector);

  /* Cancel cyclic timer */
  tmwtimer_cancel(&p14Sector->cyclicTimer);
  tmwtimer_cancel(&p14Sector->backgroundTimer);
  tmwtimer_cancel(&p14Sector->clockValidTimer);

  /* Close database */
  s14data_close(p14Sector->i870.pDbHandle);
  
  /* Close IEC 60870 sector */
  i870sctr_closeSector(pSector);

  /* remove any requests from the message queue */
  i870chnl_deleteMessages(pSector->pSession->pChannel, pSector);

  /* Unlock channel */
  TMWTARG_UNLOCK_SECTION(&pSector->pSession->pChannel->lock);
}

/* function: s14sctr_updateCyclicPeriod */
void TMWDEFS_GLOBAL s14sctr_updateCyclicPeriod(
  TMWSCTR *pSector, 
  TMWTYPES_MILLISECONDS period)
{
  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  if(p14Sector->cyclicPeriod != period)
  {
    /* Cancel the old timer */
    tmwtimer_cancel(&p14Sector->cyclicTimer);

    /* Set period to new value */
    p14Sector->cyclicPeriod = period;

    /* Restart timer if required */
    if(p14Sector->cyclicPeriod != 0)
    {
      tmwtimer_start(&p14Sector->cyclicTimer, 
        p14Sector->cyclicFirstPeriod, pSector->pSession->pChannel,
        s14sctr_cyclicTimeout, pSector);
    }
  }
}

/* function: s14sctr_updateBackgroundPeriod */
void TMWDEFS_GLOBAL s14sctr_updateBackgroundPeriod(
  TMWSCTR *pSector, 
  TMWTYPES_MILLISECONDS period)
{
  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  if(p14Sector->backgroundPeriod != period)
  {
    /* Cancel the old timer */
    tmwtimer_cancel(&p14Sector->backgroundTimer);

    /* Set period to new value */
    p14Sector->backgroundPeriod = period;

    /* Restart timer if required */
    if(p14Sector->backgroundPeriod != 0)
    {
      tmwtimer_start(&p14Sector->backgroundTimer, 
        p14Sector->backgroundPeriod, pSector->pSession->pChannel,
        _backgroundTimeout, pSector);
    }
  }
}

/* function: s14sctr_cyclicTimeout */
void TMWDEFS_GLOBAL s14sctr_cyclicTimeout(
  void *pParam)
{ 
  TMWSCTR *pSector = (TMWSCTR *)pParam;
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  TMWSESN *pSession = (TMWSESN *)pSector->pSession;

  /* If currently sending cyclic data, do not restart at the beginning 
   * This can occur if the cyclic period is smaller than the amount of time
   * it takes the master to poll for all of the data 
   */
  if(pSession->online == TMWSESN_STAT_ONLINE)
  {
    if(p14Sector->cyclicGroupIndex == GROUP_INDEX_IDLE)
    {
      p14Sector->cyclicGroupIndex = 0;
      p14Sector->cyclicPointIndex = 0;
    }
    else
    {
      p14Sector->i870.cyclicPending = TMWDEFS_TRUE;
    } 
    
#if S14DATA_SUPPORT_CICNAWAIT
    /* If the cyclic timer has expired set this so we don't
     * send cyclic data the next time we complete a CICNA 
     */
    p14Sector->CICNAComplete = TMWDEFS_TRUE;
#endif

    /* Tell link layer we have something to transmit */
    pSession->pChannel->pLink->pLinkDataReady(
      pSession->pChannel->pLinkContext, pSession);
  }

  if(p14Sector->cyclicPeriod != 0)
  { 
    tmwtimer_start(&p14Sector->cyclicTimer, 
      p14Sector->cyclicPeriod, pSession->pChannel,
      s14sctr_cyclicTimeout, pSector);
  }
}

/* function: s14sctr_sendCyclic */
void TMWDEFS_GLOBAL s14sctr_sendCyclic(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  TMWSESN *pSession = pSector->pSession;
  p14Sector->cyclicGroupIndex = 0;
  p14Sector->cyclicPointIndex = 0;

  /* Lock channel */
  TMWTARG_LOCK_SECTION(&pSector->pSession->pChannel->lock);

  /* Tell link layer we have something to transmit */
  pSession->pChannel->pLink->pLinkDataReady(
    pSession->pChannel->pLinkContext, pSession);  
  
  /* Unlock channel */
  TMWTARG_UNLOCK_SECTION(&pSector->pSession->pChannel->lock);
}

void TMWDEFS_GLOBAL s14sctr_restartClockValidTime(TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  /* Start clock valid timer */
  if(p14Sector->clockValidPeriod != 0)
  {
    tmwtimer_start(&p14Sector->clockValidTimer, 
      p14Sector->clockValidPeriod, pSector->pSession->pChannel,
      TMWDEFS_NULL, TMWDEFS_NULL);
  }
}


