/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s1ctsna.c
 * description: IEC 60870-5-101 slave CTSNA (Test Command)
 *  functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s1ctsna.h"
#include "tmwscl/i870/s101sctr.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/i870chnl.h"
#include "tmwscl/utils/tmwtarg.h"

#if S14DATA_SUPPORT_CTS

/* function: s1ctsna_processRequest */
void TMWDEFS_CALLBACK s1ctsna_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  S101SCTR *p101Sector = (S101SCTR *)p14Sector;

  /* Store originator address */
  p101Sector->ctsnaOriginator = pMsg->origAddress;  
  
  /* Parse Information Object Address */
  i870util_readInfoObjAddr(pSector->pSession, pMsg, &p101Sector->ctsnaIOA);

  /* Parse test pattern */
  tmwtarg_get16(&pMsg->pRxData->pMsgBuf[pMsg->offset], &p101Sector->pattern);
  pMsg->offset += 2;

  /* Parse received cause of transmission */
  if(pMsg->cot == I14DEF_COT_ACTIVATION)
  {
    p101Sector->ctsnaCOT = I14DEF_COT_ACTCON;
  }
  else
  {
    p101Sector->ctsnaCOT = I14DEF_COT_UNKNOWN_COT | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  if(p101Sector->ctsnaIOA != 0)
  {
    p101Sector->ctsnaCOT = I14DEF_COT_UNKNOWN_IOA | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* Validate pattern */
  if(p101Sector->pattern != 0x55aa)
    p101Sector->ctsnaCOT |= I14DEF_COT_NEGATIVE_CONFIRM;
}

/* function: s1ctsna_buildResponse */
TMWTYPES_BOOL TMWDEFS_CALLBACK s1ctsna_buildResponse(
  TMWSCTR *pSector, 
  TMWTYPES_BOOL buildResponse)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  S101SCTR *p101Sector = (S101SCTR *)p14Sector;

  if(p101Sector->ctsnaCOT != 0)
  {
    if(buildResponse)
    {
      /* Activation confirmation or Deactivation */
      TMWSESN_TX_DATA *pTxData;
      S14SESN *pS14Session = (S14SESN *)pSector->pSession;

      if((pTxData = i870chnl_newTxData(pSector->pSession->pChannel, pSector->pSession, pSector,
        pS14Session->maxASDUSize)) == TMWDEFS_NULL)
      {
        return(TMWDEFS_FALSE);
      }

#if TMWCNFG_SUPPORT_DIAG
      pTxData->pMsgDescription = "Test Command Response";
#endif
      pTxData->txFlags = TMWSESN_TXFLAGS_NO_RESPONSE;
      pTxData->responseTimeout = p14Sector->defaultResponseTimeout;

      /* Build response */
      i870util_buildMessageHeader(pSector->pSession, 
        pTxData, I14DEF_TYPE_CTSNA1, p101Sector->ctsnaCOT, 
        p101Sector->ctsnaOriginator, p14Sector->i870.asduAddress);

      /* Store point number, always 0 */
      i870util_storeInfoObjAddr(pSector->pSession, pTxData, p101Sector->ctsnaIOA);

      /* Store test pattern */
      tmwtarg_store16(&p101Sector->pattern, &pTxData->pMsgBuf[pTxData->msgLength]);
      pTxData->msgLength += 2;

      /* Request is complete */
      p101Sector->ctsnaCOT = 0;

      /* Send the response */
      i870chnl_sendMessage(pTxData);
    }

    return(TMWDEFS_TRUE);
  }

  return(TMWDEFS_FALSE);
}

#endif /* S14DATA_SUPPORT_CTS */
