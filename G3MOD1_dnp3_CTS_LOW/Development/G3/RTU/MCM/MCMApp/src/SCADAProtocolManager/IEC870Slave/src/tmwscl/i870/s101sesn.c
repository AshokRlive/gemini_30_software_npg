/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s101sesn.c
 * description: Slave IEC 60870-5-101 session
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/i870chnl.h"
#include "tmwscl/i870/s101sesn.h"
#include "tmwscl/i870/i870ln1p.h"
#include "tmwscl/i870/s101sctr.h"
#include "tmwscl/i870/s101mem.h"
#include "tmwscl/i870/s14mem.h"
#include "tmwscl/i870/s14sctr.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/s14diag.h"
#include "tmwscl/i870/s14rbe.h"
#include "tmwscl/i870/s14util.h"

/* ASDU Specific Processing */
#include "tmwscl/i870/s14cicna.h"
#include "tmwscl/i870/s14ccina.h"
#include "tmwscl/i870/s14crdna.h"
#include "tmwscl/i870/s14ccsna.h"
#include "tmwscl/i870/s1ctsna.h"
#include "tmwscl/i870/s14crpna.h"
#include "tmwscl/i870/s1ccdna.h"
#include "tmwscl/i870/s14cscna.h"
#include "tmwscl/i870/s14cdcna.h"
#include "tmwscl/i870/s14crcna.h"
#include "tmwscl/i870/s14csena.h"
#include "tmwscl/i870/s14csenb.h"
#include "tmwscl/i870/s14csenc.h"
#include "tmwscl/i870/s14csenz.h"
#include "tmwscl/i870/s14cbona.h"
#include "tmwscl/i870/s14cctna.h"
#include "tmwscl/i870/s14pmena.h"
#include "tmwscl/i870/s14pmenb.h"
#include "tmwscl/i870/s14pmenc.h"
#include "tmwscl/i870/s14pacna.h"
#include "tmwscl/i870/s14ffrna.h"
#include "tmwscl/i870/s14fsrna.h"
#include "tmwscl/i870/s14fsgna.h"
#include "tmwscl/i870/s14flsna.h"
#include "tmwscl/i870/s14fscna.h"
#include "tmwscl/i870/s14fscnb.h"
#include "tmwscl/i870/s14fafna.h"
#include "tmwscl/i870/s14file.h"

/* Table of supported ASDU types and their associated processing functions 
 *  Note that the order of the entries in this table will determine the order
 *  in which each ASDU type is processed. This is defined in table 16 of
 *  IEC 60870-5-101 Edition 2.0.
 */
static const S14SESN_FUNC_ENTRY s101sesn_funcTable[] = {
  /* Command Transmission */
  
#if S14DATA_SUPPORT_MULTICMDS
  /* 'Psuedo Entry' to send responses when simultaneous commands of same type id are allowed
   */
  {254,                0,  TMWDEFS_NULL,            s14util_buildMultiResponse},  
#endif

#if S14DATA_SUPPORT_CSC
#if S14DATA_SUPPORT_MULTICMDS
  {I14DEF_TYPE_CSCNA1, 1,  s14cscna_processRequest, TMWDEFS_NULL},  /* Single Point Command */
#else
  {I14DEF_TYPE_CSCNA1, 1,  s14cscna_processRequest, s14cscna_buildResponse},  /* Single Point Command */
#endif
#endif

#if S14DATA_SUPPORT_CDC
#if S14DATA_SUPPORT_MULTICMDS
  {I14DEF_TYPE_CDCNA1, 1,  s14cdcna_processRequest, TMWDEFS_NULL},  /* Double Point Command */
#else
  {I14DEF_TYPE_CDCNA1, 1,  s14cdcna_processRequest, s14cdcna_buildResponse},  /* Double Point Command */
#endif
#endif

#if S14DATA_SUPPORT_CRC
#if S14DATA_SUPPORT_MULTICMDS
  {I14DEF_TYPE_CRCNA1, 1,  s14crcna_processRequest, TMWDEFS_NULL},  /* Regulating Step Command */
#else
  {I14DEF_TYPE_CRCNA1, 1,  s14crcna_processRequest, s14crcna_buildResponse},  /* Regulating Step Command */
#endif
#endif

#if S14DATA_SUPPORT_CSE_A
#if S14DATA_SUPPORT_MULTICMDS
  {I14DEF_TYPE_CSENA1, 3,  s14csena_processRequest, TMWDEFS_NULL},  /* Set Point Command, Normalized Value */
#else
  {I14DEF_TYPE_CSENA1, 3,  s14csena_processRequest, s14csena_buildResponse},  /* Set Point Command, Normalized Value */
#endif
#endif
  
#if S14DATA_SUPPORT_CSE_B
#if S14DATA_SUPPORT_MULTICMDS
  {I14DEF_TYPE_CSENB1, 3,  s14csenb_processRequest, TMWDEFS_NULL},  /* Set Point Command, Scaled Value */
#else
  {I14DEF_TYPE_CSENB1, 3,  s14csenb_processRequest, s14csenb_buildResponse},  /* Set Point Command, Scaled Value */
#endif
#endif

#if S14DATA_SUPPORT_CSE_C
#if S14DATA_SUPPORT_MULTICMDS
  {I14DEF_TYPE_CSENC1, 5,  s14csenc_processRequest, TMWDEFS_NULL},  /* Set Point Command, Floating Point Value */
#else
  {I14DEF_TYPE_CSENC1, 5,  s14csenc_processRequest, s14csenc_buildResponse},  /* Set Point Command, Floating Point Value */
#endif
#endif

#if S14DATA_SUPPORT_CBO
#if S14DATA_SUPPORT_MULTICMDS
  {I14DEF_TYPE_CBONA1, 4,  s14cbona_processRequest, TMWDEFS_NULL},  /* Bitstring Command */
#else
  {I14DEF_TYPE_CBONA1, 4,  s14cbona_processRequest, s14cbona_buildResponse},  /* Bitstring Command */
#endif
#endif

#if S14DATA_SUPPORT_CSE_Z
#if S14DATA_SUPPORT_MULTICMDS
  {I14DEF_TYPE_CSENZ1, 7,  s14csenz_processRequest, TMWDEFS_NULL},  /* Set Integrated Total Command, BCD Value */
#else
  {I14DEF_TYPE_CSENZ1, 7,  s14csenz_processRequest, s14csenz_buildResponse},  /* Set Integrated Total Command, BCD Value */
#endif
#endif

#if S14DATA_SUPPORT_CCTNA
  {I14DEF_TYPE_CCTNA1,  1, s14cctna_processRequest, s14cctna_buildResponse},  /* Set Configuration Table Command */
#endif

  /* 'Psuedo Entry' to force spontaneous event processing at this 
   *   point in the table 
   */
  {255,                0, s14sesn_processRequest,  s14rbe_processEvents},

  /* Clock Synchronization, and Acquisition of Transmission Delay */
#if S14DATA_SUPPORT_CCSNA
  {I14DEF_TYPE_CCSNA1, 7, s14ccsna_processRequest, s14ccsna_buildResponse},  /* Clock Synchronization Command */
#endif

#if S14DATA_SUPPORT_CCD
  {I14DEF_TYPE_CCDNA1, 2, s1ccdna_processRequest,  s1ccdna_buildResponse},   /* Load Delay Command */
#endif

  /* Read Command, Test Procedure, Reset Process, and Parameter Loading */
#if S14DATA_SUPPORT_CRDNA
  {I14DEF_TYPE_CRDNA1, 0, s14crdna_processRequest, s14crdna_buildResponse},  /* Read Command */
#endif

#if S14DATA_SUPPORT_CTS
  {I14DEF_TYPE_CTSNA1, 2, s1ctsna_processRequest,  s1ctsna_buildResponse},   /* Test Command */
#endif

#if S14DATA_SUPPORT_CRPNA
  {I14DEF_TYPE_CRPNA1, 1, s14crpna_processRequest, s14crpna_buildResponse},  /* Reset Process Command */
#endif

#if S14DATA_SUPPORT_PMENA 
  {I14DEF_TYPE_PMENA1, 3, s14pmena_processRequest, s14pmena_buildResponse},  /* Parameter of Measured Value, Normalized Value */
#endif

#if S14DATA_SUPPORT_PMENB 
  {I14DEF_TYPE_PMENB1, 3, s14pmenb_processRequest, s14pmenb_buildResponse},  /* Parameter of Measured Value, Scaled Value */
#endif

#if S14DATA_SUPPORT_PMENC 
  {I14DEF_TYPE_PMENC1, 5, s14pmenc_processRequest, s14pmenc_buildResponse},  /* Parameter of Measured Value, Short Floating Point Value */
#endif

#if S14DATA_SUPPORT_PACNA
  {I14DEF_TYPE_PACNA1, 1, s14pacna_processRequest, s14pacna_buildResponse},  /* Parameter Activation */
#endif

  /* Station Interrogation, Transmission of Integrated Totals */
#if S14DATA_SUPPORT_CICNA
  {I14DEF_TYPE_CICNA1, 1, s14cicna_processRequest, s14cicna_buildResponse},  /* Interrogation Command */
#endif

#if S14DATA_SUPPORT_CCINA
  {I14DEF_TYPE_CCINA1, 1, s14ccina_processRequest, s14ccina_buildResponse},  /* Counter Interrogation Command */
#endif

  /* File Transfer */
#if S14DATA_SUPPORT_FILE                                                  
  {I14DEF_TYPE_FFRNA1, 6,  s14ffrna_processRequest, s14file_buildResponse},  /* File Ready */

  {I14DEF_TYPE_FSRNA1, 7,  s14fsrna_processRequest, s14file_buildResponse},  /* Section Ready */

  {I14DEF_TYPE_FSGNA1, -1, s14fsgna_processRequest, s14file_buildResponse},  /* Segment */

  {I14DEF_TYPE_FLSNA1, 5,  s14flsna_processRequest, s14file_buildResponse},  /* Last section, last segment */

  {I14DEF_TYPE_FSCNA1, 4,  s14fscna_processRequest, s14file_buildResponse},  /* Call directory, select file, */
                                                                             /* call file, call selection    */
  {I14DEF_TYPE_FAFNA1, 4,  s14fafna_processRequest, s14file_buildResponse},  /* ACK file, ACK section        */

#if S14DATA_SUPPORT_FSCNB
  {I14DEF_TYPE_FSCNB1, 16, s14fscnb_processRequest, s14file_buildResponse},  /* Query Log                    */ 
#endif
#endif

  /* End of Table */
  {0, 0, TMWDEFS_NULL, TMWDEFS_NULL}
};

/* function: _infoCallback */
static void TMWDEFS_CALLBACK _infoCallback( 
  TMWSESN *pSession, TMWSCL_INFO sesnInfo)
{
  s14sesn_infoCallback(pSession, sesnInfo);
}

/* function: s14sesn_parseFrameCallback */
static TMWTYPES_BOOL TMWDEFS_CALLBACK _parseFrameCallback(
  TMWSESN *pSession, 
  TMWSESN_RX_DATA *pRxFrame,
  const void *pFuncTable)
{
  return(s14sesn_parseFrameCallback(pSession, pRxFrame, pFuncTable));
}

/* function: s14sesn_checkClassCallback */
static TMWTYPES_BOOL TMWDEFS_CALLBACK _checkClassCallback( 
  TMWSESN *pSession, 
  TMWDEFS_CLASS_MASK classMask, 
  TMWTYPES_BOOL buildResponse,
  const void *pFuncTable)
{
  return (s14sesn_checkClassCallback(pSession, classMask, buildResponse, pFuncTable));
}

/* function _prepareMessage */
static void TMWDEFS_CALLBACK _prepareMessage(
  TMWSESN_TX_DATA *pTxData)
{  
  s14sesn_prepareMessage(pTxData);
}

/* function: s101sesn_initConfig */
void TMWDEFS_GLOBAL s101sesn_initConfig(
  S101SESN_CONFIG *pConfig)
{
  pConfig->active = TMWDEFS_TRUE;
  pConfig->asduAddrSize = 2;
  pConfig->useDayOfWeek = TMWDEFS_TRUE;
  pConfig->cotSize = 1;
  pConfig->infoObjAddrSize = 2;
  pConfig->linkAddress = 3;
  pConfig->maxASDUSize = 252;   /* Maximum size of ASDU as per IEC 60870-5-101 Edition 2.0 */
  pConfig->maxPollDelay = TMWDEFS_SECONDS(20);
  pConfig->diagFormat = 0;   
  
  pConfig->pStatCallback = TMWDEFS_NULL;
  pConfig->pStatCallbackParam = TMWDEFS_NULL;

  pConfig->pProcessRequestCallback = TMWDEFS_NULL;
  pConfig->pProcessRequestParam = TMWDEFS_NULL; 
  pConfig->pBuildResponseCallback = TMWDEFS_NULL;
  pConfig->pBuildResponseParam = TMWDEFS_NULL; 

#if S14DATA_SUPPORT_CICNAWAIT 
  pConfig->cyclicWaitCICNAComplete = TMWDEFS_FALSE;
#endif
}

/* function: s101sesn_openSession */
TMWSESN * TMWDEFS_GLOBAL s101sesn_openSession(
  TMWCHNL *pChannel,
  const S101SESN_CONFIG *pConfig)
{
  TMWSESN *pSession;
  I870SESN *pI870Session;
  I870LN1P_SESN_CONFIG sessionConfig;
  S101SESN *pS101Session;

  if(!tmwappl_getInitialized(TMWAPPL_INIT_S101))
  {
    if(!s101mem_init(TMWDEFS_NULL))
      return(TMWDEFS_NULL);

    tmwappl_setInitialized(TMWAPPL_INIT_S101);

    /* If s104 was not initialized yet, init s14 */
    if(!tmwappl_getInitialized(TMWAPPL_INIT_S104))
    {
      if(!s14mem_init(TMWDEFS_NULL))
        return(TMWDEFS_NULL);

#if TMWCNFG_SUPPORT_DIAG
      s14diag_init();
#endif
    }
  } 
  
  /* Validate configuration */
  if(pConfig->maxASDUSize > I870CHNL_TX_DATA_BUFFER_MAX)
  {
    return(TMWDEFS_NULL);
  }

  /* Allocate space for session context */
  pS101Session = (S101SESN *)s101mem_alloc(S101MEM_SESN_TYPE);

  if(pS101Session == TMWDEFS_NULL)
  {
    return(TMWDEFS_NULL);
  }

  pSession = (TMWSESN *)pS101Session;

  /* Link Layer Configuration */
  pSession->active = pConfig->active;

  if(((I870LNK1_CONTEXT *)pChannel->pLinkContext)->linkAddressSize > 0)
    pSession->linkAddress = pConfig->linkAddress;
  else
    pSession->linkAddress = 0;

  pSession->pLinkSession = TMWDEFS_NULL;  
  
  pI870Session = (I870SESN *)pS101Session;

  /* Session callback functions */
  pI870Session->pProcessInfoFunc = _infoCallback;
  pI870Session->pParseFrameCallbackFunc = _parseFrameCallback;
  pI870Session->pCheckDataAvailableFunc = _checkClassCallback;
  pI870Session->pPrepareMessageFunc = _prepareMessage;
  pI870Session->pSesnFuncTable = s101sesn_funcTable;

  /* IEC 60870 Configuration */
  pI870Session->useDayOfWeek    = pConfig->useDayOfWeek;
  pI870Session->cotSize         = pConfig->cotSize;
  pI870Session->asduAddrSize    = pConfig->asduAddrSize;
  pI870Session->infoObjAddrSize = pConfig->infoObjAddrSize;
  pI870Session->diagFormat      = pConfig->diagFormat;

  /* IEC 60870-5-101/104 Configuration */
  pS101Session->s14.maxASDUSize = pConfig->maxASDUSize;
  pS101Session->s14.pProcessRequestCallback = pConfig->pProcessRequestCallback;
  pS101Session->s14.pProcessRequestParam = pConfig->pProcessRequestParam;
  pS101Session->s14.pBuildResponseCallback  = pConfig->pBuildResponseCallback;
  pS101Session->s14.pBuildResponseParam = pConfig->pBuildResponseParam;
#if S14DATA_SUPPORT_CICNAWAIT
  pS101Session->s14.cyclicWaitCICNAComplete = pConfig->cyclicWaitCICNAComplete;
#endif
    
  /* Open IEC 60870-5-101/104 slave session */
  s14sesn_openSession(pSession);

  sessionConfig.isRequestPending   = TMWDEFS_NULL;
  sessionConfig.maxPollDelay = pConfig->maxPollDelay;
  
  /* These are not used by s101 slave code, init them to zero */
  sessionConfig.classPendingCount  = 0;
  sessionConfig.class1PollCount    = 0;
  sessionConfig.class1PendingDelay = 0;
  sessionConfig.class1PollDelay    = 0;
  sessionConfig.class2PendingDelay = 0;
  sessionConfig.class2PollDelay    = 0;

  /* Necessary locking is done in i870sesn_openSession */

  /* Open IEC 60870 session */
  i870sesn_openSession(pChannel, pSession, &sessionConfig, pConfig->pStatCallback, 
    pConfig->pStatCallbackParam, TMWTYPES_PROTOCOL_101, TMWTYPES_SESSION_TYPE_SLAVE);

  return(pSession);
}

/* function: s101sesn_getSessionConfig */
TMWTYPES_BOOL TMWDEFS_GLOBAL s101sesn_getSessionConfig(
  TMWSESN *pSession,
  S101SESN_CONFIG *pConfig)
{
  I870SESN *pI870Session = (I870SESN *)pSession;
  S14SESN *pS14Session   = (S14SESN *)pSession;
  I870LNK1_SESSION_INFO *pLinkSession = (I870LNK1_SESSION_INFO *)pSession->pLinkSession;

  pConfig->active                   = pSession->active;
  pConfig->linkAddress              = pSession->linkAddress; 
  pConfig->pStatCallback            = pSession->pStatCallbackFunc;
  pConfig->pStatCallbackParam       = pSession->pStatCallbackParam;

  pConfig->asduAddrSize             = pI870Session->asduAddrSize;
  pConfig->infoObjAddrSize          = pI870Session->infoObjAddrSize;
  pConfig->useDayOfWeek             = pI870Session->useDayOfWeek;
  pConfig->cotSize                  = pI870Session->cotSize;
  pConfig->diagFormat               = pI870Session->diagFormat;

  pConfig->maxASDUSize              = pS14Session->maxASDUSize;
  pConfig->pProcessRequestCallback  = pS14Session->pProcessRequestCallback;
  pConfig->pProcessRequestParam     = pS14Session->pProcessRequestParam;
  pConfig->pBuildResponseCallback   = pS14Session->pBuildResponseCallback;
  pConfig->pBuildResponseParam      = pS14Session->pBuildResponseParam;

  pConfig->maxPollDelay             = pLinkSession->maxPollDelay;
  
#if S14DATA_SUPPORT_CICNAWAIT
  pConfig->cyclicWaitCICNAComplete  = pS14Session->cyclicWaitCICNAComplete;
#endif

  return(TMWDEFS_TRUE);
}

/* function: s101sesn_setSessionConfig */
TMWTYPES_BOOL TMWDEFS_GLOBAL s101sesn_setSessionConfig(
  TMWSESN *pSession,
  const S101SESN_CONFIG *pConfig)
{
  I870SESN *pI870Session = (I870SESN *)pSession;
  S14SESN *pS14Session   = (S14SESN *)pSession;
  I870LNK1_SESSION_INFO *pLinkSession = (I870LNK1_SESSION_INFO *)pSession->pLinkSession;

  pSession->active                     = pConfig->active;
  pSession->linkAddress                = pConfig->linkAddress; 
  pSession->pStatCallbackFunc          = pConfig->pStatCallback;
  pSession->pStatCallbackParam         = pConfig->pStatCallbackParam;

  pI870Session->asduAddrSize           = pConfig->asduAddrSize;
  pI870Session->infoObjAddrSize        = pConfig->infoObjAddrSize;
  pI870Session->useDayOfWeek           = pConfig->useDayOfWeek;
  pI870Session->cotSize                = pConfig->cotSize;
  pI870Session->diagFormat             = pConfig->diagFormat;

  pS14Session->maxASDUSize             = pConfig->maxASDUSize;
  pS14Session->pProcessRequestCallback = pConfig->pProcessRequestCallback;
  pS14Session->pProcessRequestParam    = pConfig->pProcessRequestParam;
  pS14Session->pBuildResponseCallback  = pConfig->pBuildResponseCallback;
  pS14Session->pBuildResponseParam     = pConfig->pBuildResponseParam;

  pLinkSession->maxPollDelay           = pConfig->maxPollDelay;
  
#if S14DATA_SUPPORT_CICNAWAIT
  pS14Session->cyclicWaitCICNAComplete = pConfig->cyclicWaitCICNAComplete;
#endif

  return(TMWDEFS_TRUE);
}

/* function: s101sesn_modifySession */
/* NOTE: Use s101sesn_setSessionConfig() instead of this function */
TMWTYPES_BOOL TMWDEFS_GLOBAL s101sesn_modifySession(
  TMWSESN *pSession,
  const S101SESN_CONFIG *pConfig, 
  TMWTYPES_ULONG configMask)
{
  /* Session active */
  if((configMask & S101SESN_CONFIG_ACTIVE) != 0)
  {
    pSession->active = pConfig->active;
  }

  /* Link Address */
  if(configMask & S101SESN_CONFIG_LINK_ADDRESS)
  {
    pSession->linkAddress = pConfig->linkAddress;
  }

  return(TMWDEFS_TRUE);
}

/* function: s101sesn_closeSession */
TMWTYPES_BOOL TMWDEFS_GLOBAL s101sesn_closeSession(
  TMWSESN *pSession)
{
  /* Get pointer to application session data */
  S101SESN *pS101Session = (S101SESN *)pSession;

  /* Close IEC 60870-5-101/104 slave session */
  s14sesn_closeSession(pSession);

  /* Necessary locking is done in i870sesn_closeSession */

  /* Close IEC 60870 session */
  if(i870sesn_closeSession(pSession) == TMWDEFS_FALSE)
  {
    return(TMWDEFS_FALSE);
  }

  /* Free memory */
  s101mem_free(pS101Session);
  

  return(TMWDEFS_TRUE);
}
