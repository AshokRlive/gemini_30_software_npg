/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:DNP3SlaveProtocolChannel.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   26 Sep 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "SDNP3Debug.h"
#include "DNP3SlaveProtocolChannel.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */


DNP3SlaveProtocolChannel::DNP3SlaveProtocolChannel(GeminiDatabase&                  GDatabase,
                                                   DNP3SlaveProtocol                *protocolManager,
                                                   Config   config)
                                                   : m_GDatabase(GDatabase),
                                                     m_PManager(protocolManager),
                                                     m_modemPtr(NULL),
                                                     m_config(config),
                                                     m_log(Logger::getLogger(SUBSYSTEM_ID_SCADA_HANDLER))
{
    m_SCLChannelPtr = NULL;
}

DNP3SlaveProtocolChannel::~DNP3SlaveProtocolChannel()
{
    // Delete this channel's sessions
    deleteSessions();

    /* Close protocol channel */
    if (m_SCLChannelPtr != NULL)
    {
        dnpchnl_closeChannel(m_SCLChannelPtr);
        m_SCLChannelPtr = NULL;
    }

    delete connMgr;
}

SCADAP_ERROR DNP3SlaveProtocolChannel::init()
{
    // If we have configured FailOver or have a MODEM fitted we need the
    // ConnectionManager
    if ((connMgr == NULL) && ((m_connectionManagerEnabled == true) || (m_modemPtr != NULL)))
    {
        connMgr = new SCADAConnectionManager(*this);

        if (m_connectionManagerEnabled == true)
        {
            applyConnectionManagerConfig();
        }

        // If fitted, observe Modem State
        if (m_modemPtr != NULL)
        {
            m_modemPtr->attach(this);

            // Set the publishRxData callback in the target layer
            // Use pCallbackParam to hold the pointer to the Channel
            m_config.targConfig.pChannelRxDataCallback = publishRxData;
            m_config.targConfig.pChannelInstance       = (void *)this;
        }
    }

    return SCADAP_ERROR_NONE;
}

SCADAP_ERROR DNP3SlaveProtocolChannel::closeChannel()
{
    /* TODO: pueyos_a - Close all the depending sessions before closing the channel! */
//    for (SessionVector::iterator itSession = m_SlaveSessions.begin(); itSession != m_SlaveSessions.end(); ++itSession)
//    {
//        /* Check sclChannelPtr to be not NULL; otherwise DO NOT launch the DNP protocol */
//        (*itSession)->openSession(m_SCLChannelPtr);
//    }

    // Detach Modem observer
    if (m_modemPtr != NULL)
    {
        m_modemPtr->detach(this);
    }

    if (dnpchnl_closeChannel(this->m_SCLChannelPtr) == TMWDEFS_TRUE)
    {
        this->m_SCLChannelPtr = NULL;

        return SCADAP_ERROR_NONE;
    }

    return SCADAP_ERROR_NOT_INITIALIZED;
}

SCADAP_ERROR DNP3SlaveProtocolChannel::openChannel(TMWAPPL *applContextPtr)
{
    if (applContextPtr == NULL)
    {
        return SCADAP_ERROR_PARAM;
    }

    // Start the Connection Manager
    if (connMgr != NULL)
    {
        connMgr->init();
    }

    m_SCLChannelPtr = dnpchnl_openChannel(applContextPtr,
                                        &m_config.channelConfig,
                                        &m_config.tprtConfig,
                                        &m_config.linkConfig,
                                        &m_config.physConfig,
                                        &m_config.IOConfig,
                                        &m_config.targConfig);


    // Set pUserData to this Channel Object
    tmwchnl_setUserDataPtr(m_SCLChannelPtr, this);

    if (m_SCLChannelPtr == NULL)
    {
        m_log.error("Unable to initialise DNP3 Slave Protocol Channel.");
        return SCADAP_ERROR_NOT_INITIALIZED;
    }

    //  Open the Sessions
    for (SessionVector::iterator itSession = m_SlaveSessions.begin(); itSession != m_SlaveSessions.end(); ++itSession)
    {
        /* Check sclChannelPtr to be not NULL; otherwise DO NOT launch the DNP protocol */
        (*itSession)->openSession(m_SCLChannelPtr);
    }

    return SCADAP_ERROR_NONE;
}

lu_uint32_t DNP3SlaveProtocolChannel::countSessionEvents(lu_uint8_t classMask, bool countAll)
{
    lu_uint32_t eventCount = 0;

    if (classMask == 0)
    {
        return 0;
    }

    // Loop over the Current Master Channel's Sessions, looking for CLASS 1 Events
    for (DNP3SlaveProtocolChannel::SessionVector::iterator itSession  = m_SlaveSessions.begin();
                                                            itSession != m_SlaveSessions.end();
                                                          ++itSession)
    {
        ISlaveProtocolSession *sess = *(itSession);

        if ((eventCount += sess->countAllEvents(classMask, countAll)) > 0)
        {
            if (countAll == false)
            {
                return eventCount;
            }
        }
    }

    return eventCount;
}

bool DNP3SlaveProtocolChannel::connectChannel()
{
    bool res = LU_FALSE;

    if (m_modemPtr != NULL)
    {
        res = m_modemPtr->connect();
    }
    else if ((m_SCLChannelPtr != NULL) && (m_SCLChannelPtr->pPhysContext != NULL))
    {
        if (m_config.IOConfig.linTCP.mode == LINTCP_MODE_DUAL_ENDPOINT)
        {
            // This is only used for Dual-Endpoint TCP Comms
            res = (tmwtarg_lucy_connectChannel(m_SCLChannelPtr->pPhysContext->pIOContext) == TMWDEFS_TRUE)
                            ? LU_TRUE : LU_FALSE;
        }
    }

    return res;
}

char *DNP3SlaveProtocolChannel::getIpAddress()
{
    if (m_config.IOConfig.type == LINIO_TYPE_TCP)
    {
        return m_config.IOConfig.linTCP.ipAddress;
    }

    return NULL;
}

lu_int32_t DNP3SlaveProtocolChannel::getListeningPort()
{
    if (m_config.IOConfig.type == LINIO_TYPE_TCP)
    {
        if ((m_config.IOConfig.linTCP.mode == LINTCP_MODE_SERVER) || (m_config.IOConfig.linTCP.mode == LINTCP_MODE_DUAL_ENDPOINT))
        {
            return m_config.IOConfig.linTCP.ipPort;
        }
    }

    return -1;
}

void DNP3SlaveProtocolChannel::publishRxData(void *channelPtr, lu_int8_t *dataPtr, lu_uint32_t len)
{
// Call CommDevice function to publish the RX Data

    if ((dataPtr != NULL) && (len > 0))
    {
        if (channelPtr != NULL)
        {
            DNP3SlaveProtocolChannel *channelInst = (DNP3SlaveProtocolChannel *) channelPtr;
            if (channelInst->m_modemPtr != NULL)
            {
                channelInst->m_modemPtr->sniffRcvData(dataPtr, len);
            }
        }
    }

    return;
}

void DNP3SlaveProtocolChannel::setConnectedStatus(bool connected)
{
    TMWPHYS_CONTEXT *pPhysContext;
    LINIO_CHANNEL *pLinIoChannel;
    TMWDEFS_TARG_OC_REASON  reason = TMWDEFS_TARG_OC_FAILURE;
    TMWTYPES_BOOL           connectStatus = TMWDEFS_FALSE;


    pPhysContext = m_SCLChannelPtr->pPhysContext;
    pLinIoChannel = (LINIO_CHANNEL *) pPhysContext->pIOContext;

    if (connected)
    {
        reason = TMWDEFS_TARG_OC_SUCCESS;
        connectStatus = TMWDEFS_TRUE;
    }

#if LINIOTARG_SUPPORT_232
    if (m_config.IOConfig.type == LINIO_TYPE_232)
    {
        SERIAL_IO_CHANNEL *pLinSerialContext;
        pLinSerialContext = (SERIAL_IO_CHANNEL *) pLinIoChannel->pChannelInfo;
        pLinSerialContext->pChannelCallback(pPhysContext, connectStatus, reason);
    }
#endif
#if LINIOTARG_SUPPORT_TCP
    if (m_config.IOConfig.type == LINIO_TYPE_TCP)
    {
        TCP_IO_CHANNEL *pLinTCPContext;
        pLinTCPContext = (TCP_IO_CHANNEL *) pLinIoChannel->pChannelInfo;
        pLinTCPContext->pChannelCallback(pPhysContext, connectStatus, reason);
    }
#endif

}

void DNP3SlaveProtocolChannel::tickEvent()
{
    if (connMgr != NULL)
    {
        connMgr->tickEvent();
    }
}

lu_uint32_t DNP3SlaveProtocolChannel::getConnectionTimeout()
{
    if (m_config.IOConfig.type == LINIO_TYPE_TCP)
    {
        return m_config.IOConfig.linTCP.ipConnectTimeout;
    }

    return 0;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

void DNP3SlaveProtocolChannel::notifyConnectionState(bool connected, lu_int32_t fd)
{
    TMWPHYS_CONTEXT *pPhysContext;
    LINIO_CHANNEL *pLinIoChannel;

    pPhysContext = m_SCLChannelPtr->pPhysContext;
    pLinIoChannel = (LINIO_CHANNEL *) pPhysContext->pIOContext;

#if LINIOTARG_SUPPORT_232
    if (m_config.IOConfig.type == LINIO_TYPE_232)
    {
        SERIAL_IO_CHANNEL *pLinSerialContext;
        pLinSerialContext = (SERIAL_IO_CHANNEL *) pLinIoChannel->pChannelInfo;

        // Send the file descriptor to the Triangle Library
        pLinSerialContext->ttyfd = fd;
    }
#endif

    setConnectedStatus(connected);
}

void DNP3SlaveProtocolChannel::applyConnectionManagerConfig()
{
    if ((connMgr == NULL) || (m_connectionManagerEnabled == false))
    {
        return;
    }

    SCADAConnectionManager::SocketConfig *configPtr;

    // Get the current SCADA Connection details
    ScadaAddress scadaAddress;

    if (getCurrentSCADAAddress(scadaAddress) == false)
    {
        m_log.error("DNP3SlaveProtocolChannel: Unable to get SCADA Address - disabled connectionGroup");
        m_connectionManagerEnabled = false;
        return;
    }

    if ((m_config.IOConfig.linTCP.mode == LINTCP_MODE_SERVER) ||
        (m_config.IOConfig.linTCP.mode == LINTCP_MODE_DUAL_ENDPOINT))
    {
        if (m_config.IOConfig.linTCP.mode == LINTCP_MODE_DUAL_ENDPOINT)
        {
            DBG_INFO("Dual Endpoint mode.");

            // Copy the SCADA connection details to the ConnectionManager
//            std::string ipAddress = m_config.IOConfig.linTCP.ipAddress;
            connMgr->setSCADAConnectionIPConfig(scadaAddress.ip, (lu_uint32_t)scadaAddress.port);

            configPtr = connMgr->getSCADAConnectionIPConfig();
            strncpy(m_config.IOConfig.linTCP.ipAddress, configPtr->ipAddress.c_str(), 20);

            // Connecting Port
            m_config.IOConfig.linTCP.dualEndPointIpPort = configPtr->port;

            DBG_INFO("Dual Endpoint channel connects to [%s:%d]", m_config.IOConfig.linTCP.ipAddress, m_config.IOConfig.linTCP.dualEndPointIpPort);
        }

        // Copy the config used to listen for incoming SCADA connections - the library should allow any
        // incoming ip address - Connection Manager will check the ip address, no the library
        connMgr->setSCADAListenerIPConfig("*.*.*.*", (lu_int32_t) scadaAddress.port /*IOConfig.linTCP.ipPort*/);

        // Copy the config to the library to allow the Channel to connect to the ConnectionManager
        configPtr = connMgr->getSCADAListenerIPConfig();

        // Force th library to listen to the loopback adaptor
        strncpy(m_config.IOConfig.linTCP.ipAddress, "127.0.0.1", 20);

        // Listening Port
        m_config.IOConfig.linTCP.ipPort = configPtr->port;

        DBG_INFO("SERVER/DUAL ENDPOINT MODE - listens on [%s:%d]", m_config.IOConfig.linTCP.ipAddress, m_config.IOConfig.linTCP.ipPort);
    }

    if (m_config.IOConfig.linTCP.mode == LINTCP_MODE_CLIENT)
    {
        // Copy the SCADA connection details to the ConnectionManager
        connMgr->setSCADAConnectionIPConfig(scadaAddress.ip, (lu_int32_t)scadaAddress.port /*IOConfig.linTCP.ipPort*/);

        // Set the TMW channel details appropriately
        configPtr = connMgr->getSCADAConnectionIPConfig();
        strncpy(m_config.IOConfig.linTCP.ipAddress, configPtr->ipAddress.c_str(), 20);

        // Connecting Port
        m_config.IOConfig.linTCP.ipPort = configPtr->port;

        DBG_INFO("CLIENT MODE - TMW Channel will connect to [%s:%d]", m_config.IOConfig.linTCP.ipAddress, m_config.IOConfig.linTCP.ipPort);
    }

    // Set all sessions for this channel to the same master address
    setMasterAddress(scadaAddress.masterAddress);
}


/*
 *********************** End of file ******************************************
 */
