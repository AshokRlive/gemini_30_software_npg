/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:TMWIncludes.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   26 Sep 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef TMWINCLUDES_H_
#define TMWINCLUDES_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

extern "C"
{
#include "tmwscl/utils/tmwdb.h"
#include "tmwscl/utils/tmwpltmr.h"
#include "tmwscl/utils/tmwtarg.h"

#include "tmwscl/dnp/dnpchnl.h"
#include "tmwscl/dnp/sdnpsesn.h"

#include "tmwscl/dnp/sdnputil.h"
#include "tmwscl/dnp/sdnprbe.h"
#include "tmwscl/dnp/sdnpo002.h" //Binary input event
#include "tmwscl/dnp/sdnpo004.h" //Double Binary input event
#include "tmwscl/dnp/sdnpo022.h" //Counter event
#include "tmwscl/dnp/sdnpo023.h" //Frozen Counter event
#include "tmwscl/dnp/sdnpo032.h" //Analogue input event
#include "tmwscl/dnp/sdnpo042.h" //Analogue output event
#include "tmwscl/dnp/sdnpo043.h" //Analogue output Command event

#include "tmwscl/dnp/database/TMWSDNPDatabase.h" //MG Custom database structure

#include "LinIoTarg/liniotarg.h"
}

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */



#endif /* TMWINCLUDES_H_ */

/*
 *********************** End of file ******************************************
 */
