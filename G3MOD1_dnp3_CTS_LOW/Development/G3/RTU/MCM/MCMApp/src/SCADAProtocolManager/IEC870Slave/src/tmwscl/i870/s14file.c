/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/


/* file: s14file.c
 * description: IEC 60870-5-101 slave File Transfer
 *  functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14file.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/s14sctr.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/s14fdrta.h"
#include "tmwscl/i870/i870chnl.h"
#include "tmwscl/utils/tmwtarg.h"

#if S14DATA_SUPPORT_FILE
 
/* function: s14file__calculateChecksum */
TMWTYPES_UCHAR TMWDEFS_GLOBAL s14file_calculateChecksum(
  TMWTYPES_UCHAR oldChecksum,
  TMWTYPES_UCHAR *pData,
  TMWTYPES_UCHAR dataLength)
{
  int i;
  TMWTYPES_UCHAR newChecksum = oldChecksum;
  for(i=0; i<dataLength; i++)
  {
    newChecksum = (TMWTYPES_UCHAR)(newChecksum + *(pData+i));
  }
  return(newChecksum);
}

/* function: s14file_buildFSCNA */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14file_buildFSCNA(
  TMWSCTR *pSector, 
  TMWTYPES_UCHAR cot, 
  TMWTYPES_UCHAR scq)
{
  TMWSESN_TX_DATA *pTxData;
  S14SESN *pS14Session = (S14SESN *)pSector->pSession;
        
  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;
      
  if((pTxData = i870chnl_newTxData(pSector->pSession->pChannel, 
        pSector->pSession, pSector, pS14Session->maxASDUSize)) == TMWDEFS_NULL)
  {
    return(TMWDEFS_FALSE);
  }
 
#if TMWCNFG_SUPPORT_DIAG
  pTxData->pMsgDescription = "FSCNA Response";
#endif
  pTxData->txFlags = TMWSESN_TXFLAGS_NO_RESPONSE;
  pTxData->responseTimeout = p14Sector->defaultResponseTimeout;

  /* Build response */
  i870util_buildMessageHeader(pSector->pSession, 
    pTxData, I14DEF_TYPE_FSCNA1, cot, 
    p14Sector->fileOriginator, p14Sector->i870.asduAddress);

  /* Store IOA */
  i870util_storeInfoObjAddr(pSector->pSession, pTxData, p14Sector->fileIOA);

  /* Name of File */
  tmwtarg_store16(&p14Sector->fileName, &pTxData->pMsgBuf[pTxData->msgLength]);
  pTxData->msgLength += 2;

  /* Name of Section */
  pTxData->pMsgBuf[pTxData->msgLength++] = p14Sector->fileSectionName;

  /* Section and Call Qualifier */
  pTxData->pMsgBuf[pTxData->msgLength++] = scq;

  /* Reset section checksum and also file checksum if file is called */ 
  p14Sector = (S14SCTR *)pSector;
  p14Sector->fileSectionChecksum = 0;
  if((scq & I14DEF_SCQ_CMD_MASK) == I14DEF_SCQ_CALL_FILE)
  {
    p14Sector->fileChecksum = 0;
  }

  p14Sector->fileState = S14FILE_IDLE_STATE;

  /* Send the response */
  i870chnl_sendMessage(pTxData);

  return(TMWDEFS_TRUE);
}

/* function: s14file_buildFDRTA */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14file_buildFDRTA(
  TMWSCTR *pSector)
{
  TMWSESN_TX_DATA *pTxData;
  TMWTYPES_ULONG    ioa;
  TMWTYPES_USHORT   fileName;
  TMWTYPES_ULONG    fileLength;
  TMWDTIME          fileTime;
  TMWTYPES_UCHAR    sof;
  TMWTYPES_UCHAR    lastSof = 0;
  TMWTYPES_UCHAR    count   = 0;
  TMWTYPES_ULONG    prevIoa = 0;

  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  S14SESN *pS14Session = (S14SESN *)pSector->pSession;

  if((pTxData = i870chnl_newTxData(pSector->pSession->pChannel, 
    pSector->pSession, pSector, pS14Session->maxASDUSize)) == TMWDEFS_NULL)
  {
    return(TMWDEFS_FALSE);
  } 

#if TMWCNFG_SUPPORT_DIAG
  pTxData->pMsgDescription = "FDRTA Response";
#endif
  pTxData->txFlags = TMWSESN_TXFLAGS_NO_RESPONSE;
  pTxData->responseTimeout = p14Sector->defaultResponseTimeout;

  /* Build response */
  i870util_buildMessageHeader(pSector->pSession, 
    pTxData, I14DEF_TYPE_FDRTA1, p14Sector->fileFdrtaCOT, 
    p14Sector->fileOriginator, p14Sector->i870.asduAddress);

  /* Loop through getting file entries. First IOA gets put in ASDU. Entries with sequential IOAs 
   * can be put in same ASDU 
   */
  count = 0;
  while(s14data_fileGetDirectoryEntry(p14Sector->i870.pDbHandle, p14Sector->fileDirIndex,
    &ioa, &fileName, &fileLength, &fileTime, &sof)
      != TMWDEFS_FALSE)
  {
    if(count == 0)
    { 
      /* Store IOA */
      i870util_storeInfoObjAddr(pSector->pSession, pTxData, ioa);
      prevIoa = ioa;
    }
    else
    {
      /* If this new ioa is not sequential, break out of loop and send FDRTA */
      if(ioa != ++prevIoa)
      {
        break;
      }
    }
    
    /* Make sure there is enough room left in the PDU for this entry */
    if(pTxData->msgLength + 13 > pTxData->maxLength)
    {
      break;
    }

    lastSof = sof;

    /* increment entry index */
    p14Sector->fileDirIndex++;

    /* Name of File or subdirectory */
    tmwtarg_store16(&fileName, &pTxData->pMsgBuf[pTxData->msgLength]);
    pTxData->msgLength += 2;

    /* length of file */
    tmwtarg_store24(&fileLength, &pTxData->pMsgBuf[pTxData->msgLength]);
    pTxData->msgLength += 3;

    /* Status of File */
    pTxData->pMsgBuf[pTxData->msgLength++] = sof;

    /* time */
    i870util_write56BitTime(pTxData, (TMWDTIME *)&fileTime, S14DATA_SUPPORT_GENUINE_TIME);

    count++;
  } 

  if(count > 0)
  {
    /* update the number of elements in Variable Structure Qualifier */
    pTxData->pMsgBuf[1] = (TMWTYPES_UCHAR)(0x80 | count);

    /* If the status says this is last file entry, set FDRTA COT back to 0 */
    if(lastSof & I14DEF_SOF_LFD_LAST_FILE)
    {
      p14Sector->fileFdrtaCOT = 0;
    }

    /* Send the response */
    i870chnl_sendMessage(pTxData);

    return(TMWDEFS_TRUE);
  }

  /* No more FDRTA ASDUs to send */
  p14Sector->fileFdrtaCOT = 0; 
  i870chnl_freeTxData(pTxData);
  return(TMWDEFS_FALSE);
}

/* function: s14file_buildFLSNA */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14file_buildFLSNA(
  TMWSCTR *pSector,
  TMWTYPES_UCHAR lsq)
{
  TMWSESN_TX_DATA *pTxData;
  TMWTYPES_UCHAR chs= 0;

  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  S14SESN *pS14Session = (S14SESN *)pSector->pSession;
      
  if((pTxData = i870chnl_newTxData(pSector->pSession->pChannel, 
    pSector->pSession, pSector, pS14Session->maxASDUSize)) == TMWDEFS_NULL)
  {
    return(TMWDEFS_FALSE);
  }
 
#if TMWCNFG_SUPPORT_DIAG
  pTxData->pMsgDescription = "FLSNA Response";
#endif
  pTxData->txFlags = TMWSESN_TXFLAGS_NO_RESPONSE;
  pTxData->responseTimeout = p14Sector->defaultResponseTimeout;

  i870util_buildMessageHeader(pSector->pSession, 
    pTxData, I14DEF_TYPE_FLSNA1, I14DEF_COT_FILE_XFER, 
    p14Sector->fileOriginator, p14Sector->i870.asduAddress);

  /* Store IOA */
  i870util_storeInfoObjAddr(pSector->pSession, pTxData, p14Sector->fileIOA);

  /* Name of File */
  tmwtarg_store16(&p14Sector->fileName, &pTxData->pMsgBuf[pTxData->msgLength]);
  pTxData->msgLength += 2;

  /* Name of Section */
  pTxData->pMsgBuf[pTxData->msgLength++] = p14Sector->fileSectionName;

  /* Last Section or segment qualifier */
  pTxData->pMsgBuf[pTxData->msgLength++] = lsq;
 
  /* Determine which checksum to send, section or file */
  if(lsq == I14DEF_LSQ_SECT_XFER_NO_DEACT)
  {
    chs = p14Sector->fileSectionChecksum;

    /* Add this into file checksum */
    p14Sector->fileChecksum = (TMWTYPES_UCHAR)
      (p14Sector->fileChecksum + p14Sector->fileSectionChecksum);
  }
  else if(lsq == I14DEF_LSQ_FILE_XFER_NO_DEACT)
  {
    chs = p14Sector->fileChecksum;
  }

  /* Checksum */
  pTxData->pMsgBuf[pTxData->msgLength++] = chs;

  p14Sector->fileState = S14FILE_IDLE_STATE;

  /* Send the response */
  i870chnl_sendMessage(pTxData);

  return(TMWDEFS_TRUE);
}

/* function: s14file_buildFAFNA */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14file_buildFAFNA(
  TMWSCTR *pSector)
{
  TMWSESN_TX_DATA *pTxData;
  TMWTYPES_UCHAR afq;
  TMWTYPES_BOOL checksumGood;

  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  S14SESN *pS14Session = (S14SESN *)pSector->pSession;
      
  if((pTxData = i870chnl_newTxData(pSector->pSession->pChannel, 
    pSector->pSession, pSector, pS14Session->maxASDUSize)) == TMWDEFS_NULL)
  {
    return(TMWDEFS_FALSE);
  }
 
#if TMWCNFG_SUPPORT_DIAG
  pTxData->pMsgDescription = "FAFNA Response";
#endif
  pTxData->txFlags = TMWSESN_TXFLAGS_NO_RESPONSE;
  pTxData->responseTimeout = p14Sector->defaultResponseTimeout;

  i870util_buildMessageHeader(pSector->pSession, 
    pTxData, I14DEF_TYPE_FAFNA1, I14DEF_COT_FILE_XFER, 
    p14Sector->fileOriginator, p14Sector->i870.asduAddress);

  /* Store IOA */
  i870util_storeInfoObjAddr(pSector->pSession, pTxData, p14Sector->fileIOA);

  /* Name of File */
  tmwtarg_store16(&p14Sector->fileName, &pTxData->pMsgBuf[pTxData->msgLength]);
  pTxData->msgLength += 2;

  /* Name of Section */
  pTxData->pMsgBuf[pTxData->msgLength++] = p14Sector->fileSectionName;

  /* verify Checksum */
  checksumGood = TMWDEFS_FALSE;
  if(p14Sector->fileLSQ == I14DEF_LSQ_SECT_XFER_NO_DEACT)
  {
    afq = I14DEF_AFQ_SECT_XFER_POS_ACT;
    if(p14Sector->fileSectionChecksum == p14Sector->fileCHS)
    {
      checksumGood = TMWDEFS_TRUE;
      p14Sector->fileChecksum = (TMWTYPES_UCHAR)
        (p14Sector->fileChecksum + p14Sector->fileSectionChecksum);
    } 
  }
  else if(p14Sector->fileLSQ == I14DEF_LSQ_FILE_XFER_NO_DEACT)
  {
    afq = I14DEF_AFQ_FILE_XFER_POS_ACT;
    if(p14Sector->fileChecksum == p14Sector->fileCHS)
    {
      checksumGood = TMWDEFS_TRUE;
    }   
  }

  if(s14data_fileAckLastSectOrSegResp(p14Sector->i870.pDbHandle, p14Sector->fileIOA, 
    p14Sector->fileName, p14Sector->fileSectionName, p14Sector->fileLSQ, 
    checksumGood, &afq) == TMWDEFS_FALSE)
  {
    /* Failure send deactivate file to abort transfer. */
    i870chnl_freeTxData(pTxData);
    return(s14file_buildFLSNA(pSector, I14DEF_LSQ_FILE_XFER_DEACT));
  }

  pTxData->pMsgBuf[pTxData->msgLength++] = afq;

  p14Sector->fileState = S14FILE_IDLE_STATE;

  /* Send the response */
  i870chnl_sendMessage(pTxData);

  return(TMWDEFS_TRUE);
}

/* function: s14file_buildFSGNA */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14file_buildFSGNA(
  TMWSCTR *pSector)
{
  TMWTYPES_UCHAR maxLength;
  TMWSESN_TX_DATA *pTxData;
  TMWTYPES_UCHAR *pSegmentLength;

  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  S14SESN *pS14Session = (S14SESN *)pSector->pSession;
      
  if((pTxData = i870chnl_newTxData(pSector->pSession->pChannel, 
    pSector->pSession, pSector, pS14Session->maxASDUSize)) == TMWDEFS_NULL)
  {
    return(TMWDEFS_FALSE);
  }
 
#if TMWCNFG_SUPPORT_DIAG
  pTxData->pMsgDescription = "FSGNA Response";
#endif
  pTxData->txFlags = TMWSESN_TXFLAGS_NO_RESPONSE;
  pTxData->responseTimeout = p14Sector->defaultResponseTimeout;

  i870util_buildMessageHeader(pSector->pSession, 
    pTxData, I14DEF_TYPE_FSGNA1, I14DEF_COT_FILE_XFER, 
    p14Sector->fileOriginator, p14Sector->i870.asduAddress);

  /* Store IOA */
  i870util_storeInfoObjAddr(pSector->pSession, pTxData, p14Sector->fileIOA);

  /* Name of File */
  tmwtarg_store16(&p14Sector->fileName, &pTxData->pMsgBuf[pTxData->msgLength]);
  pTxData->msgLength += 2;

  /* Name of Section */
  pTxData->pMsgBuf[pTxData->msgLength++] = p14Sector->fileSectionName;

  /* Get a pointer to where the segment length will go                     */
  /* So that s14data function can get both length and actual segment bytes */
  pSegmentLength = &pTxData->pMsgBuf[pTxData->msgLength++];
  
  /* Calculate max number of segment bytes that can be copied into ASDU */
  maxLength = (TMWTYPES_UCHAR)(pTxData->maxLength - pTxData->msgLength);
  if(maxLength > S14FILE_MAX_SEGMENT_LENGTH)
  {
    maxLength = S14FILE_MAX_SEGMENT_LENGTH;
  }

  if(s14data_fileGetSegment(p14Sector->i870.pDbHandle, p14Sector->fileIOA, p14Sector->fileName,
      p14Sector->fileSectionName, maxLength, pSegmentLength, &pTxData->pMsgBuf[pTxData->msgLength]) == TMWDEFS_FALSE)
  {
    /* Failure send deactivate file to abort transfer. */
    i870chnl_freeTxData(pTxData);
    return(s14file_buildFLSNA(pSector, I14DEF_LSQ_FILE_XFER_DEACT));
  }

  if(*pSegmentLength == 0)
  {
    /* No more bytes in this segment, send FLSNA */
    TMWTYPES_UCHAR lsq = I14DEF_LSQ_SECT_XFER_NO_DEACT;
    
    if(s14data_fileGetLastSegmentResp(p14Sector->i870.pDbHandle, p14Sector->fileIOA, 
      p14Sector->fileName, p14Sector->fileSectionName, &lsq) == TMWDEFS_FALSE)
    {
      lsq = I14DEF_LSQ_SECT_XFER_DEACT;
    }

    i870chnl_freeTxData(pTxData);
    return(s14file_buildFLSNA(pSector, lsq));
  }

  /* update the checksum to be sent at end of section */
  p14Sector->fileSectionChecksum = s14file_calculateChecksum(p14Sector->fileSectionChecksum, 
    &pTxData->pMsgBuf[pTxData->msgLength], *pSegmentLength);

  /* Segment bytes were retrieved successfully, send the FSGNA APDU */
  pTxData->msgLength = (TMWTYPES_UCHAR)(pTxData->msgLength + *pSegmentLength);

  /* Send the message */
  i870chnl_sendMessage(pTxData);

  return(TMWDEFS_TRUE);
}

/* function: s14file_buildFFRNA */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14file_buildFFRNA(
  TMWSCTR *pSector, 
  TMWTYPES_ULONG fileLength,
  TMWTYPES_UCHAR frq)
{
  TMWSESN_TX_DATA *pTxData;

  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  S14SESN *pS14Session = (S14SESN *)pSector->pSession;
      
  if((pTxData = i870chnl_newTxData(pSector->pSession->pChannel, 
    pSector->pSession, pSector, pS14Session->maxASDUSize)) == TMWDEFS_NULL)
  {
    return(TMWDEFS_FALSE);
  }
 
#if TMWCNFG_SUPPORT_DIAG
  pTxData->pMsgDescription = "FFRNA Response";
#endif
  pTxData->txFlags = TMWSESN_TXFLAGS_NO_RESPONSE;
  pTxData->responseTimeout = p14Sector->defaultResponseTimeout;

  /* Build response */
  i870util_buildMessageHeader(pSector->pSession, 
    pTxData, I14DEF_TYPE_FFRNA1, I14DEF_COT_FILE_XFER, 
    p14Sector->fileOriginator, p14Sector->i870.asduAddress);

  /* Store IOA */
  i870util_storeInfoObjAddr(pSector->pSession, pTxData, p14Sector->fileIOA);

  /* Name of File */
  tmwtarg_store16(&p14Sector->fileName, &pTxData->pMsgBuf[pTxData->msgLength]);
  pTxData->msgLength += 2;

  /* Length of File */
  tmwtarg_store24(&fileLength, &pTxData->pMsgBuf[pTxData->msgLength]);
  pTxData->msgLength += 3;

  /* File Ready Qualifier */
  pTxData->pMsgBuf[pTxData->msgLength++] = frq;

  p14Sector->fileState = S14FILE_IDLE_STATE;

  /* Send the response */
  i870chnl_sendMessage(pTxData);

  return(TMWDEFS_TRUE);
}

/* function: s14file_buildFSRNA */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14file_buildFSRNA(
  TMWSCTR *pSector, 
  TMWTYPES_ULONG sectionLength,
  TMWTYPES_UCHAR srq)
{
  TMWSESN_TX_DATA *pTxData;

  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  S14SESN *pS14Session = (S14SESN *)pSector->pSession;
      
  if((pTxData = i870chnl_newTxData(pSector->pSession->pChannel, 
    pSector->pSession, pSector, pS14Session->maxASDUSize)) == TMWDEFS_NULL)
  {
    return(TMWDEFS_FALSE);
  }
 
#if TMWCNFG_SUPPORT_DIAG
  pTxData->pMsgDescription = "FSRNA Response";
#endif
  pTxData->txFlags = TMWSESN_TXFLAGS_NO_RESPONSE;
  pTxData->responseTimeout = p14Sector->defaultResponseTimeout;

  /* Build response */
  i870util_buildMessageHeader(pSector->pSession, 
    pTxData, I14DEF_TYPE_FSRNA1, I14DEF_COT_FILE_XFER, 
    p14Sector->fileOriginator, p14Sector->i870.asduAddress);

  /* Store IOA */
  i870util_storeInfoObjAddr(pSector->pSession, pTxData, p14Sector->fileIOA);

  /* Name of File */
  tmwtarg_store16(&p14Sector->fileName, &pTxData->pMsgBuf[pTxData->msgLength]);
  pTxData->msgLength += 2;

  /* Name of Section */
  pTxData->pMsgBuf[pTxData->msgLength++] = p14Sector->fileSectionName;

  /* Length of Section */
  tmwtarg_store24(&sectionLength, &pTxData->pMsgBuf[pTxData->msgLength]);
  pTxData->msgLength += 3;

  /* Section Ready Qualifier */
  pTxData->pMsgBuf[pTxData->msgLength++] = srq;

  p14Sector->fileState = S14FILE_IDLE_STATE;

  /* Send the response */
  i870chnl_sendMessage(pTxData);

  return(TMWDEFS_TRUE);
}

/* function: s14file_buildResponse */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14file_buildResponse( 
  TMWSCTR *pSector, 
  TMWTYPES_BOOL buildResponse)
{
  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  if(p14Sector->fileState != S14FILE_IDLE_STATE)
  {
    if(buildResponse)
    {
      switch(p14Sector->fileState)
      {
      case S14FILE_RCV_FLSNA_STATE:
        {
          return(s14file_buildFAFNA(pSector));
        }
        break; 

      case S14FILE_RCV_FILE_READY_STATE:
        {
          /* Init Select and call qualifier, but let s14data override value */
          TMWTYPES_UCHAR scq = I14DEF_SCQ_CALL_FILE;

          /* Get SCQ from user */
          if(s14data_fileCallFileResp(p14Sector->i870.pDbHandle, p14Sector->fileIOA, 
            p14Sector->fileName, p14Sector->fileLength, p14Sector->fileFRQ,
            &scq) == TMWDEFS_FALSE)
          {
            scq = I14DEF_SCQ_DEACT_FILE;
          }
 
          return(s14file_buildFSCNA(pSector, I14DEF_COT_FILE_XFER, scq));
        }
      break;

      case S14FILE_RCV_SECTION_READY_STATE:
        {
          /* Init Select and call qualifier, but let s14data override value */
          TMWTYPES_UCHAR scq = I14DEF_SCQ_CALL_SECTION;

          /* Get SCQ from user */
          if(s14data_fileCallSectionResp(p14Sector->i870.pDbHandle, p14Sector->fileIOA, 
            p14Sector->fileName, p14Sector->fileSectionName,
            p14Sector->fileSectionLength, p14Sector->fileSRQ, &scq) == TMWDEFS_FALSE)
          {
            scq = I14DEF_SCQ_DEACT_SECTION;
          }
  
          return(s14file_buildFSCNA(pSector, I14DEF_COT_FILE_XFER, scq));
        }
        break; 
      
      case S14FILE_RCV_ACK_STATE:
        {
          TMWTYPES_UCHAR afqCmd = (TMWTYPES_UCHAR)(p14Sector->fileAFQ & I14DEF_AFQ_CMD_MASK);
          if((afqCmd == I14DEF_AFQ_SECT_XFER_POS_ACT)
            || (afqCmd == I14DEF_AFQ_SECT_XFER_NEG_ACT))
          {
            /* Received an Ack or Nack for a section, 
             * If Ack and there are more sections send FSRNA else send FLSNA indicating end of file 
             * If Nack offer up the same section again.
             */
            TMWTYPES_ULONG sectionLength = 0;
            TMWTYPES_UCHAR srq = I14DEF_SRQ_SECT_READY;

            if(afqCmd == I14DEF_AFQ_SECT_XFER_POS_ACT)
            {
              p14Sector->fileSectionName++; 
            }
            
            if(s14data_fileGetSectionReady(p14Sector->i870.pDbHandle, p14Sector->fileIOA, 
              p14Sector->fileName, p14Sector->fileSectionName, &sectionLength, &srq)== TMWDEFS_TRUE)
            {

              return(s14file_buildFSRNA(pSector, sectionLength, srq));
            }
            else
            {
              TMWTYPES_UCHAR lsq = I14DEF_LSQ_FILE_XFER_NO_DEACT;
    
              if(s14data_fileGetLastSectionResp(p14Sector->i870.pDbHandle, p14Sector->fileIOA, 
                p14Sector->fileName, &lsq) == TMWDEFS_FALSE)
              {
                lsq = I14DEF_LSQ_FILE_XFER_DEACT;
              }

              return(s14file_buildFLSNA(pSector, lsq));
            }
          }

          else if(afqCmd == I14DEF_AFQ_FILE_XFER_POS_ACT)
          {
            /* Received an ACK for the whole file, slave can delete file and send directory spontaneously*/
            s14data_fileAckFile(p14Sector->i870.pDbHandle, p14Sector->fileIOA, 
              p14Sector->fileName);
          }

          p14Sector->fileState = S14FILE_IDLE_STATE;

          /* Nothing was sent */
          return(TMWDEFS_FALSE);
        }
        break;

      case S14FILE_SELECT_STATE:
        {
          TMWTYPES_ULONG fileLength = 0;
          TMWTYPES_UCHAR frq = I14DEF_FRQ_POS_CONFIRM;

          /* Get fileLength and File Ready Qualifier */
          if(s14data_fileSelectFile(p14Sector->i870.pDbHandle, p14Sector->fileIOA, 
            p14Sector->fileName, &fileLength, &frq)!= TMWDEFS_TRUE)
          {
            frq = I14DEF_FRQ_NEG_CONFIRM;
          }
          return(s14file_buildFFRNA(pSector, fileLength, frq));
        }
        break;

#if S14DATA_SUPPORT_FSCNB
      case S14FILE_QUERYLOG_STATE:
        {
          TMWTYPES_ULONG fileLength = 0;
          TMWTYPES_UCHAR frq = I14DEF_FRQ_POS_CONFIRM;

          /* Get fileLength and File Ready Qualifier */
          if(s14data_fileQueryLog(p14Sector->i870.pDbHandle, p14Sector->fileIOA, 
            p14Sector->fileName, &p14Sector->fileRangeStartTime, &p14Sector->fileRangeStopTime,
            &fileLength, &frq)!= TMWDEFS_TRUE)
          {
            frq = I14DEF_FRQ_NEG_CONFIRM;
          }
          return(s14file_buildFFRNA(pSector, fileLength, frq));
        }
        break;
#endif
      case S14FILE_CALL_STATE:
      case S14FILE_SECTION_SELECT_STATE:
      case S14FILE_SECTION_DEACTIVATE_STATE:
        {
          TMWTYPES_ULONG sectionLength = 0;
          TMWTYPES_UCHAR srq = I14DEF_SRQ_SECT_READY;

          /* Get section length and section ready qualifier */
          if(s14data_fileGetSectionReady(p14Sector->i870.pDbHandle, p14Sector->fileIOA, 
            p14Sector->fileName, p14Sector->fileSectionName, &sectionLength, &srq)!= TMWDEFS_TRUE)
          {
            /* Failed, deactivate (abort) file transfer */
            return(s14file_buildFLSNA(pSector, I14DEF_LSQ_FILE_XFER_DEACT));
          }
          return(s14file_buildFSRNA(pSector, sectionLength, srq));
        }
        break;

      case S14FILE_SECTION_CALL_STATE:
        {
          return(s14file_buildFSGNA(pSector));
        }
        break;

      case S14FILE_DELETE_STATE:
        {
          /* Delete file and send FFRNA POS_CONFIRM or NEG_CONFIRM */
          TMWTYPES_UCHAR frq = I14DEF_FRQ_POS_CONFIRM;

          /* Delete File and get File Ready Qualifier */
          if(s14data_fileDeleteResp(p14Sector->i870.pDbHandle, p14Sector->fileIOA, 
            p14Sector->fileName, &frq)!= TMWDEFS_TRUE)
          {
            frq = I14DEF_FRQ_NEG_CONFIRM;
          }
          return(s14file_buildFFRNA(pSector, 0, frq));
        }
        break;

      default:
        {
          /* Nothing was sent */
          return(TMWDEFS_FALSE);
        }
      }
    }
    return(TMWDEFS_TRUE);
  }

  else if(p14Sector->fileFdrtaCOT != 0)
  {
    if(buildResponse)
    {
      return(s14file_buildFDRTA(pSector));   
    }
    return(TMWDEFS_TRUE);
  }
  return(TMWDEFS_FALSE);
}

#endif /* S14DATA_SUPPORT_FILE */
