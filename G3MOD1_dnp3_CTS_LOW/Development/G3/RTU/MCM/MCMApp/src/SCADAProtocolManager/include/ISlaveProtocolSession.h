/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:ISlaveProtocolSession.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   27 Oct 2014     andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef ISLAVEPROTOCOLSESSION_H_
#define ISLAVEPROTOCOLSESSION_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Debug.h"

#include "SCADAProtocolCommon.h"
#include "TMWDNP3Includes.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */

class ISlaveProtocolSession
{
public:
    ISlaveProtocolSession() {}
    virtual ~ISlaveProtocolSession() {}

    virtual SCADAP_ERROR openSession(TMWCHNL *SCLChannelPtr) = 0;

    virtual lu_uint32_t countAllEvents(TMWDEFS_CLASS_MASK classMask, lu_bool_t countAll) = 0;

    virtual void setMasterAddress(lu_uint32_t) = 0;

    virtual void setOnline(lu_bool_t online) = 0;
};

class AbstractSlaveProtocolSession:public ISlaveProtocolSession
{
public:
    AbstractSlaveProtocolSession(lu_uint32_t sessID) : m_sessionID(sessID), m_online(LU_FALSE) {}
    virtual ~AbstractSlaveProtocolSession() {}

    lu_uint32_t getSessionID()
    {
        return m_sessionID;
    }

    virtual void setOnline(lu_bool_t online)
    {
        m_online = online;
    }

    virtual lu_bool_t getOnline()
    {
        return m_online;
    }

protected:
    lu_uint32_t m_sessionID;
    lu_bool_t   m_online; // Used by the ConnectionManager
};

#endif /* ISLAVEPROTOCOLSESSION_H_ */

/*
 *********************** End of file ******************************************
 */
