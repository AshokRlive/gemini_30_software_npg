/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:ISlaveProtocolChannel.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25 Sep 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef ISLAVEPROTOCOLCHANNEL_H_
#define ISLAVEPROTOCOLCHANNEL_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include<iostream>
#include<vector>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "Debug.h"
#include "SCADAProtocolCommon.h"
#include "ISCADAConnectionManager.h"
#include "ISlaveProtocolSession.h"



/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */



class ISlaveProtocolChannel
{

public:

struct ScadaAddress
{
	std::string 	ip;
	lu_uint16_t  	port;
    lu_uint32_t  	masterAddress;

//    lu_int32_t      connectionTries;
};


public:
    ISlaveProtocolChannel(){}

    virtual ~ISlaveProtocolChannel(){}

    virtual bool         connectChannel() = 0;

private:

    virtual lu_uint32_t  getNumSCADAAddresses() = 0;

    virtual bool         setNextSCADAAddress() = 0;

    virtual lu_uint32_t  getCurrentSCADAIndex() = 0;

    virtual bool         setCurrentMaster(lu_uint32_t masterAddress) = 0;

    virtual bool         setMasterAddress(lu_uint32_t masterAddress) = 0;

    virtual bool         findSCADAIPAddress(std::string ipAddress) = 0;

    virtual bool         getSCADAAddressIndex(lu_uint32_t index, ScadaAddress& scadaAddr) = 0;

    virtual bool         getCurrentSCADAAddress(ScadaAddress& scadaAddr) = 0;

    virtual lu_uint32_t  getConnectionTimeout() = 0;

    virtual void         tickEvent() = 0;

    virtual lu_uint32_t  countSessionEvents(lu_uint8_t classMask, bool countAll) = 0;

    virtual void         setSessionsOnline(bool online) = 0;

    friend class SCADAConnectionManager;
};


#endif /* ISLAVEPROTOCOLCHANNEL_H_ */

/*
 *********************** End of file ******************************************
 */
