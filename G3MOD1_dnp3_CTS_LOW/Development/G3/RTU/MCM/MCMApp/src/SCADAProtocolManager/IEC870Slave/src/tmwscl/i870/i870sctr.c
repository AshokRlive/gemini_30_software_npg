/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i870sctr.c
 * description: Generic IEC 60870-5 sector
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/i870chnl.h"
#include "tmwscl/i870/i870sctr.h"
#include "tmwscl/i870/i870sesn.h"

/* function: i870sctr_openSector */
TMWTYPES_BOOL TMWDEFS_GLOBAL i870sctr_openSector(
  TMWSCTR               *pSector, 
  TMWSESN               *pSession, 
  TMWSCTR_STAT_CALLBACK  pCallback,
  void                  *pCallbackParam)
{
  /* Get pointer to application level session info */
  I870SESN *pI870Session = (I870SESN *)pSession;

  /* Initialize TMW sector info */
  tmwsctr_openSector(pSector, pSession, pCallback, pCallbackParam);

  /* Add this sector to the session's list of sectors */
  tmwdlist_addEntry(&pI870Session->sectorList, (TMWDLIST_MEMBER *)pSector);

  return(TMWDEFS_TRUE);
}

/* function: i870sctr_closeSector */
void TMWDEFS_GLOBAL i870sctr_closeSector(
  TMWSCTR *pSector)
{
  /* Get pointer to application level session info */
  I870SESN *pI870Session = (I870SESN *)pSector->pSession;

  /* Remove this sector from the session's list of sectors */
  tmwdlist_removeEntry(&pI870Session->sectorList, (TMWDLIST_MEMBER *)pSector);

  /* if this was the last sector being closed on this session 
   * cancel any outstanding broadcast requests.
   */
  if(tmwdlist_size(&pI870Session->sectorList) == 0)
  {
    i870chnl_deleteMessages(pSector->pSession->pChannel, TMWDEFS_NULL);
  }

  /* Close TMW sector info */
  tmwsctr_closeSector(pSector);
}
