/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:DNP3SlaveProtocolSession.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   26 Sep 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "SDNP3Debug.h"
#include "DNP3SlaveProtocolSession.h"
#include "DNP3SlaveProtocolChannel.h"
#include "DNP3SlaveProtocol.h"
#include "tmwscl/utils/tmwcrypto.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static void _configUserDB(LUCYCRYPTO_USER_DB *db, DNP3SlaveProtocolSession::Config& config);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
#if SDNPCNFG_SUPPORT_SA_VERSION5
#define CREATE_V5_TEST_USERS 0
/*
 * Initialise secure authentication of V5
 */
void initSAv5(TMWSESN *pSclSession, SDNPSESN_CONFIG* psesnConfig);

#if CREATE_V5_TEST_USERS
/* These are the default user keys the test harness uses for testing
 * DO NOT USE THESE IN A REAL DEVICE
 */
static TMWTYPES_UCHAR defaultUserKey1[] = {
  0x49, 0xC8, 0x7D, 0x5D, 0x90, 0x21, 0x7A, 0xAF,
  0xEC, 0x80, 0x74, 0xeb, 0x71, 0x52, 0xfd, 0xb5
};
static TMWTYPES_UCHAR defaultUserKeyOther[] = {
  0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
  0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff
};

/* This one is used by the Authority and the Outstation.
 */
static TMWTYPES_UCHAR  authoritySymCertKey[] = {
  0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
  0x09, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06,
  0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
  0x09, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06
};
#endif
#endif

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

DNP3SlaveProtocolSession::DNP3SlaveProtocolSession(GeminiDatabase&  GDatabase,
                                                   DNP3SlaveProtocolChannel* channelPtr,
                                                   Config config,
                                                   lu_uint32_t sessID)
                                                   : AbstractSlaveProtocolSession(sessID),
                                                     m_GDatabase(GDatabase),
                                                     m_log(Logger::getLogger(SUBSYSTEM_ID_SCADA_HANDLER)),
                                                     m_DNP3ChannelPtr(channelPtr),
                                                     m_config(config)
{
    m_SCLChannelPtr = NULL;
    m_SCLSessionPtr = NULL;

    /* Allocate points db for this session */
    m_db = (DNP3SlaveDB*)calloc(1, sizeof(DNP3SlaveDB));

    /* Allocate pointDB */
    m_db->pointDB = (TMWSDNPDatabaseStr*)calloc(1, sizeof(TMWSDNPDatabaseStr));
    m_db->pointDB->allowControlOnBrdCstAddr = (config.lucy_allowControlOnBrdCstAddr)? TMWDEFS_TRUE : TMWDEFS_FALSE;

    m_shtPipe = new Pipe(Pipe::PIPE_TYPE_NONBLOCKING);
}

DNP3SlaveProtocolSession::~DNP3SlaveProtocolSession()
{
    delete m_shtPipe;
    m_shtPipe = NULL;

    /* Clear database: detach observers and free the memory */
    if (m_db != NULL)
    {
        /* Detach and remove observers (analogue input) */
        lu_uint32_t observerListSize = m_db->AIObserverList.getEntries();
        IPointObserver **observerList = m_db->AIObserverList.getTable();
        if (observerList != NULL)
        {
            for(lu_uint32_t i = 0; i < observerListSize; ++i)
            {
                if (observerList[i] != NULL)
                {
                    m_GDatabase.detach(observerList[i]->getPointID(), observerList[i]);
                    delete observerList[i];
                    observerList[i] = NULL;
                }
            }
            free(observerList);
        }

        /* Detach and remove observers (analogue output) */
        observerListSize = m_db->AOObserverList.getEntries();
        observerList = m_db->AOObserverList.getTable();
        if (observerList != NULL)
        {
            for(lu_uint32_t i = 0; i < observerListSize; ++i)
            {
                if (observerList[i] != NULL)
                {
                    m_GDatabase.detach(observerList[i]->getPointID(), observerList[i]);
                    delete observerList[i];
                    observerList[i] = NULL;
                }
            }
            free(observerList);
        }

        /* Detach and remove observers (binary input) */
        observerListSize = m_db->BIObserverList.getEntries();
        observerList = m_db->BIObserverList.getTable();
        if (observerList != NULL)
        {
            for(lu_uint32_t i = 0; i < observerListSize; ++i)
            {
                if (observerList[i] != NULL)
                {
                    m_GDatabase.detach(observerList[i]->getPointID(), observerList[i]);
                    delete observerList[i];
                    observerList[i] = NULL;
                }
            }
            free(observerList);
        }

        /* Detach and remove observers (double binary input) */
        observerListSize = m_db->DBIObserverList.getEntries();
        observerList = m_db->DBIObserverList.getTable();
        if (observerList != NULL)
        {
            for(lu_uint32_t i = 0; i < observerListSize; ++i)
            {
                if (observerList[i] != NULL)
                {
                    m_GDatabase.detach(observerList[i]->getPointID(), observerList[i]);
                    delete observerList[i];
                    observerList[i] = NULL;
                }
            }
            free(observerList);
        }

        /* Detach and remove observers (counter input) */
        observerListSize = m_db->CObserverList.getEntries();
        observerList = m_db->CObserverList.getTable();
        if (observerList != NULL)
        {
            for(lu_uint32_t i = 0; i < observerListSize; ++i)
            {
                if (observerList[i] != NULL)
                {
                    m_GDatabase.detach(observerList[i]->getPointID(), observerList[i]);
                    delete observerList[i];
                    observerList[i] = NULL;
                }
            }
            free(observerList);
        }

        free(m_db->pointDB->analogueInput.list);
        free(m_db->pointDB->binaryInput.list);
        free(m_db->pointDB->dbinaryInput.list);
        free(m_db->pointDB->counterInput.list);

        free(m_db);
        m_db = NULL;
    }
}

lu_bool_t DNP3SlaveProtocolSession::getOnline()
{
    if ((m_DNP3ChannelPtr != NULL) && (m_DNP3ChannelPtr->getConnectionGroupEnabled() == LU_TRUE))
    {
        return m_online;
    }

    return (m_SCLSessionPtr->online == TMWSESN_STAT_ONLINE);
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
#if SDNPCNFG_SUPPORT_SA_VERSION5 && CREATE_V5_TEST_USERS
void initSAv5(TMWSESN *pSclSession, SDNPSESN_CONFIG* psesnConfig) {

    TMWTYPES_CHAR pKey[32];

    /* In SAv5 we no longer use an array of user configuration, instead you can add one user at a time.
     * The Authority can also tell the master to add a user using a globally unique user name
     * and instruct the master to send the update key and role (permissions) for that user over DNP to the outstation.
     */
    if(!psesnConfig->authConfig.operateInV2Mode)
    {
      TMWTYPES_USHORT userNumber = 1;

      /* If using simulated database in tmwcrypto, add the User Update Key for the default user (1) to it.
       * This key should really should be in YOUR crypto database not the simulated one in tmwcrypto.c.
       * You would not normally need to call tmwcrypto_setKeyData to add it to your own database.
       */

      if(!tmwcrypto_setKeyData(TMWDEFS_NULL, TMWCRYPTO_USER_UPDATE_KEY, (void *)userNumber, defaultUserKey1, 16))
      {
        /* failed to add key */
          DBG_ERR("Failed to set key for user number:%d",userNumber);
      }
      /* Add the user to the sdnp library */
      if(sdnpsesn_addAuthUser(pSclSession, userNumber) == TMWDEFS_FALSE)
      {
          DBG_ERR("Failed to add user number:%d",userNumber);
      }


      /* Add second user, not required */
      userNumber = 100;

      /* If using simulated database in tmwcrypto, add the User Update Key for the second user number.
       * This key should really should be in YOUR crypto database not the simulated one in tmwcrypto.c.
       * You would not normally need to call tmwcrypto_setKeyData to add it to your own database.
       */
      if(!tmwcrypto_setKeyData(TMWDEFS_NULL, TMWCRYPTO_USER_UPDATE_KEY, (void *)userNumber, defaultUserKeyOther, 16))
      {
        /* failed to add key */
        DBG_ERR("Failed to set key for user number:%d",userNumber);
      }
      /* Add the user to the sdnp library */
      if(sdnpsesn_addAuthUser(pSclSession, userNumber) == TMWDEFS_FALSE)
      {
        DBG_ERR("Failed to add user number:%d",userNumber);
      }


      /* Configure other values in YOUR crypto database to allow remote user key and role update from Master.*/

      /* This sample uses the same values that the Test Harness Outstation uses by default */

      /* Outstation name must be configured in both Master and Outstation.
       * This is already set in sdnpsim database
       * OutstationName = "SDNP Outstation";
       */

      /* If using simulated database in tmwcrypto, configure the Authority Certification Symmetric Key to it.
       * This key really should be in YOUR crypto database not the simulated one in tmwcrypto.c.
       * You would not normally need to call tmwcrypto_setKeyData to add it to your own database.
       * This key is used by the Central Authority, not the master, but this sample is acting as the Authority.
       */
      if(!tmwcrypto_setKeyData(TMWDEFS_NULL, TMWCRYPTO_AUTH_CERT_SYM_KEY, TMWDEFS_NULL, (TMWTYPES_UCHAR *)&authoritySymCertKey, 32))
      {
        /* failed to add key */
      }

      /* If using simulated database in tmwcrypto, configure Outstation Private Key when Asymmetric Key Update is supported.
       * This really should be in YOUR crypto database not the simulated one.
       * You would not normally need to call tmwcrypto_setKeyData to add it to your own database.
       */
      sprintf( pKey, "TMWTestOSAsymPrvKey.pem");
      TMWTYPES_USHORT keyLength = strlen(pKey);
      if(!tmwcrypto_setKeyData(TMWDEFS_NULL, TMWCRYPTO_OS_ASYM_PRV_KEY, TMWDEFS_NULL, (TMWTYPES_UCHAR *)pKey, keyLength))
      {
        /* failed to add key */
      }

      /* If using simulated database in tmwcrypto, configure Authority Public Key when Asymmetric Key Update is supported
       * This really should be in YOUR crypto database not the simulated one.
       * You would not normally need to call tmwcrypto_setKeyData to add it to your own database.
       */
      sprintf( pKey, "TMWTestRsa2048PubKey.pem");
      keyLength = strlen(pKey);
      if(!tmwcrypto_setKeyData(TMWDEFS_NULL, TMWCRYPTO_AUTH_ASYM_PUB_KEY, TMWDEFS_NULL, (TMWTYPES_UCHAR *)pKey, keyLength))
      {
        /* failed to add key */
      }

    }
}
#endif

SCADAP_ERROR DNP3SlaveProtocolSession::openSession(TMWCHNL *SCLChnlPtr)
{
    if (SCLChnlPtr == NULL)
    {
        return SCADAP_ERROR_NOT_INITIALIZED;
    }

    DBG_INFO("Opening session SCADA:[%d] RTU:[%d]\n", m_config.sessionConfig.source,
                                                      m_config.sessionConfig.destination);

    m_SCLChannelPtr = SCLChnlPtr;

    /* Initialise callback structure */
    m_db->pointDB->callbacks.protocolManager   = m_DNP3ChannelPtr->m_PManager; // Hook to this instance
    m_db->pointDB->callbacks.restartSession    = DNP3SlaveProtocol::restart;
    m_db->pointDB->callbacks.setDateTime       = DNP3SlaveProtocol::setDateTime;
    m_db->pointDB->callbacks.binInGetPoint     = DNP3SlaveProtocol::binInGetPoint;
    m_db->pointDB->callbacks.binOutGetPoint    = DNP3SlaveProtocol::binOutGetPoint;
    m_db->pointDB->callbacks.binOutGetStatus   = DNP3SlaveProtocol::binOutGetStatus;
    m_db->pointDB->callbacks.binOutSelect      = DNP3SlaveProtocol::binOutSelect;
    m_db->pointDB->callbacks.binOutOperate     = DNP3SlaveProtocol::binOutOperate;
    m_db->pointDB->callbacks.anlgOutGetPoint   = DNP3SlaveProtocol::anlgOutGetPoint;
    m_db->pointDB->callbacks.anlgOutSelect     = DNP3SlaveProtocol::anlgOutSelect;
    m_db->pointDB->callbacks.anlgOutOperate    = DNP3SlaveProtocol::anlgOutOperate;
    m_db->pointDB->callbacks.anlgInGetPoint    = DNP3SlaveProtocol::anlgInGetPoint;
    m_db->pointDB->callbacks.dblInGetPoint     = DNP3SlaveProtocol::dblInGetPoint;
    m_db->pointDB->callbacks.ctrInGetPoint     = DNP3SlaveProtocol::ctrInGetPoint;
    m_db->pointDB->callbacks.ctrInFreeze       = DNP3SlaveProtocol::ctrInFreeze;
    m_db->pointDB->callbacks.deleteEventID     = DNP3SlaveProtocol::deleteEventID;
    m_db->pointDB->callbacks.getTimeSyncStatus = DNP3SlaveProtocol::getTimeSyncStatus;
    m_db->pointDB->callbacks.getLocalState     = DNP3SlaveProtocol::getLocalState;

    m_db->pointDB->DNP3ProtocolSessionPtr = (void *)this;

#if DNPCNFG_SUPPORT_AUTHENTICATION
    /* Initialise and configure user database*/
    m_db->pointDB->pUserDB = (LUCYCRYPTO_USER_DB*)lucycrypto_init();
    _configUserDB(m_db->pointDB->pUserDB, m_config);
#endif

    m_SCLSessionPtr = sdnpsesn_openSession(m_SCLChannelPtr,
                                         &m_config.sessionConfig,
                                         m_db->pointDB);

#if SDNPCNFG_SUPPORT_SA_VERSION5 && CREATE_V5_TEST_USERS
   initSAv5(m_SCLSessionPtr, &m_config.sessionConfig);
#endif

    // Add sclSessionPtr to the db to allow events to be added to the correct session
    m_db->pointDB->sclSessionPtr = m_SCLSessionPtr;

    if (m_SCLSessionPtr == TMWDEFS_NULL)
    {
        m_log.error("Unable to initialise DNP3 Slave Protocol Session.");
        return SCADAP_ERROR_NOT_INITIALIZED;
    }

    DBG_INFO("Success opening session SCADA:[%d] RTU:[%d]\n", m_config.sessionConfig.source,
                                                              m_config.sessionConfig.destination);

    /* Set the userDataPtr to the db which will allow the session function
       to reference callbacks */
    tmwsesn_setUserDataPtr(m_SCLSessionPtr, m_db->pointDB);

    return SCADAP_ERROR_NONE;
}


lu_uint32_t DNP3SlaveProtocolSession::countAllEvents(TMWDEFS_CLASS_MASK classMask, lu_bool_t countAll)
{
    // Binary Input Events
    if (m_SCLSessionPtr == NULL)
    {
        return 0;
    }

    return sdnprbe_countEvents(m_SCLSessionPtr, classMask, (countAll == LU_TRUE) ? TMWDEFS_TRUE : TMWDEFS_FALSE);
}

static void _configUserDB(LUCYCRYPTO_USER_DB *db, DNP3SlaveProtocolSession::Config& config)
{
    if(db == NULL)
    {
        return;
    }

    lu_uint32_t i;
    for (i = 0; i < config.usersNumber; ++i) {
        db->users[i].userNumber= config.users[i].userNumber;
        db->users[i].userRole  = config.users[i].userRole;
        db->users[i].keyLen = config.users[i].updateKeyLen;
        memcpy(db->users[i].key, config.users[i].updateKey, LUCYCRYPTO_MAX_KEY_LEN);
        DBG_INFO("Configured DNP3 User Number: %d", db->users[i].userNumber);
    }

}

/*
 *********************** End of file ******************************************
 */
