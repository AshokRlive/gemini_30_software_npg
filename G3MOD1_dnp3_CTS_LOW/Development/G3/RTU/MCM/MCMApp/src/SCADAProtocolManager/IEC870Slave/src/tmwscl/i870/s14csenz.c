/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14csenz.c
 * description: IEC 60870-5-101 slave csenz (floating command)
 *  functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14csenz.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/utils/tmwtarg.h"

#if S14DATA_SUPPORT_CSE_Z 
#if S14DATA_SUPPORT_MULTICMDS
void TMWDEFS_CALLBACK s14csenz_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  S14SCTR *p14Sector;
  S14SCTR_CMD *pContext;
  int i;
  TMWTYPES_BOOL timerIsActive;
  TMWTYPES_ULONG ioa;
  TMWTYPES_ULONG oldIOA;
  TMWTYPES_UCHAR oldQOS;
  TMWTYPES_UCHAR oldBCD[6];

  pContext = s14util_processMultiRequest(pSector, pMsg, I14DEF_TYPE_CSENZ1, &ioa, &timerIsActive);
  if(pContext == TMWDEFS_NULL)
    return;

  /* Command to call for status of this point */
  pContext->pStatus = s14data_csenzStatus;
  
  /* Save old values in case this is an execute and we need to compare with select values.
   * All of the error paths require the p14Sector fields to be filled in with rcvd values.
   */ 
  oldIOA = pContext->ioa;
  for(i=0; i<6; i++)
    oldBCD[i] = pContext->value.bcd[i]; 
  oldQOS = pContext->qualifier;  
  
  /* If only one context per typeId is allowed, this might be cancelling and replacing the previous ioa */
  pContext->ioa = ioa;
  
  /* Parse BCD values */
  for(i=0; i<6; i++)
     pContext->value.bcd[i] = pMsg->pRxData->pMsgBuf[pMsg->offset++];
  pContext->valueType = (TMWTYPES_ANALOG_TYPE)S14SCTR_TYPE_BCD;
  pContext->useValue = TMWDEFS_TRUE;

  /* Parse QOS */
  pContext->qualifier = pMsg->pRxData->pMsgBuf[pMsg->offset++];
  pContext->useQualifier = TMWDEFS_TRUE;

  /* Parse received cause of transmission */
  if(pMsg->cot == I14DEF_COT_ACTIVATION)
  {
    pContext->cot = I14DEF_COT_ACTCON;
  }
  else if(pMsg->cot == I14DEF_COT_DEACTIVATION)
  {
    /* Any deactivate will cause the select to be cancelled, even an incorrect one 
     * There is currently not a s14data function to be called indicating deactivate.
     * 
     * The 101 and 601 specs are unclear about whether a DEACTCON positive or negative 
     * should be sent on error. Since even an incorrect DEACT results in no select being
     * active, a positive reply will be sent (except if ioa is bad below).
     */
    pContext->cot = I14DEF_COT_DEACTCON;
  }
  else
  {
    pContext->cot = I14DEF_COT_UNKNOWN_COT | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* Lookup data point using information object address */
  p14Sector = (S14SCTR *)pSector;
  pContext->pPoint = s14data_csenzLookupPoint(p14Sector->i870.pDbHandle, ioa);
  if(pContext->pPoint == TMWDEFS_NULL)
  {
    pContext->cot = I14DEF_COT_UNKNOWN_IOA | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* if deactivate, context->cot was set to DEACTCON above. */
  if(pMsg->cot == I14DEF_COT_DEACTIVATION)
  {
    return;
  }

  /* Perform select or execute and set operation status */
  if((pContext->qualifier & I14DEF_QOC_SE_SELECT) != 0)
  { 
    pContext->state = TMWDEFS_CMD_STATE_SELECTING;
    pContext->status = s14data_csenzSelect(pContext->pPoint, 
      pMsg->cot, pContext->value.bcd, pContext->qualifier);
  }
  else
  {
    /* If a select is required for this point make sure a valid select
     * operation is pending and compare the execute parameters against
     * the select parameters.
     */
    if(s14data_csenzSelectRequired(pContext->pPoint))
    {
      /* Make sure select is active */
      /* Compare information object address */
      /* Compare qualifier, ignore the select bit */
      if(!timerIsActive
         ||(pContext->ioa != oldIOA)
         ||(pContext->qualifier != (oldQOS & ~I14DEF_QOC_SE_MASK)))
      {
        pContext->cot |= I14DEF_COT_NEGATIVE_CONFIRM;
        return;
      }
      
      /* Compare values */
      for(i=0; i<6; i++)
      {
        if(pContext->value.bcd[i] != oldBCD[i])
        {
          pContext->cot |= I14DEF_COT_NEGATIVE_CONFIRM;
          return;
        }
      }
    }

    pContext->state = TMWDEFS_CMD_STATE_EXECUTING;
    pContext->status = s14data_csenzExecute(pContext->pPoint, 
      pMsg->cot, pContext->value.bcd, pContext->qualifier);
  }
}

#else /* !S14DATA_SUPPORT_MULTICMDS */

/* function: _checkStatus */
static TMWDEFS_COMMAND_STATUS TMWDEFS_CALLBACK _checkStatus(
  TMWSCTR *pSector)
{
  return (s14csenz_checkStatus(pSector));
}

/* function: csenz_checkStatus */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14csenz_checkStatus(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  switch(p14Sector->csenzState)
  {
  case TMWDEFS_CMD_STATE_SELECTING:

    if((p14Sector->csenzStatus != TMWDEFS_CMD_STAT_SUCCESS) 
      && (p14Sector->csenzStatus != TMWDEFS_CMD_STAT_FAILED))
    {
      /* Get Status */
      p14Sector->csenzStatus = s14data_csenzStatus(p14Sector->pCSENZPoint);
    }

    /* If select is complete start select timer */
    if(p14Sector->csenzStatus == TMWDEFS_CMD_STAT_SUCCESS)
    {
      tmwtimer_start(&p14Sector->csenzSelectTimer, 
        p14Sector->selectTimeout, pSector->pSession->pChannel, 
        TMWDEFS_NULL, TMWDEFS_NULL);
    }
    break;

  case TMWDEFS_CMD_STATE_EXECUTING: 

    if((p14Sector->csenzStatus != TMWDEFS_CMD_STAT_SUCCESS) 
      && (p14Sector->csenzStatus != TMWDEFS_CMD_STAT_FAILED))
    {
      /* Get Status */
      p14Sector->csenzStatus = s14data_csenzStatus(p14Sector->pCSENZPoint);
    }
    break;

  default:
    break;
  }

  /* If command failed return to IDLE state */
  if(p14Sector->csenzStatus == TMWDEFS_CMD_STAT_FAILED)
    p14Sector->csenzState = TMWDEFS_CMD_STATE_IDLE;

  return(p14Sector->csenzStatus);
}

/* function: s14csenz_processRequest */
void TMWDEFS_CALLBACK s14csenz_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  int i;
  S14SCTR *p14Sector;
  TMWTYPES_BOOL timerIsActive;
  TMWTYPES_ULONG oldIOA;
  TMWTYPES_UCHAR oldQOS;
  TMWTYPES_UCHAR oldBCD[6];

  p14Sector = (S14SCTR *)pSector;
 
  /* Any new or failed command cancels the select timer 
   * Save the timer state in case this is a valid execute command
   */
  timerIsActive = tmwtimer_isActive(&p14Sector->csenzSelectTimer);
  if(timerIsActive)
  {
    tmwtimer_cancel(&p14Sector->csenzSelectTimer);
  }

  /* If there is a previous command in progress, (response still to be sent)
   * queue a negative ACT CON to be sent. 
   * Don't overwrite the information from the previous command.
   */
  if((pMsg->cot == I14DEF_COT_ACTIVATION)
    && (p14Sector->csenzCOT != 0))
  {
    s14sesn_sendNegActCon(pSector, pMsg, I14DEF_COT_ACTCON); 
    return;
  }

  /* Store originator address */
  p14Sector->csenzOriginator = pMsg->origAddress;
  
  /* Save old values in case this is an execute and we need to compare with select values.
   * All of the error paths require the p14Sector fields to be filled in with rcvd values.
   */
  oldIOA = p14Sector->csenzIOA;
  for(i=0; i<6; i++)
    oldBCD[i] = p14Sector->csenzBCD[i];
  oldQOS = p14Sector->csenzQOS;

  /* Parse Information Object Address */
  i870util_readInfoObjAddr(pSector->pSession, pMsg, &p14Sector->csenzIOA);

  /* Parse BCD values */
  for(i=0; i<6; i++)
     p14Sector->csenzBCD[i] = pMsg->pRxData->pMsgBuf[pMsg->offset++];

  /* Parse QOS */
  p14Sector->csenzQOS = pMsg->pRxData->pMsgBuf[pMsg->offset++];

  /* Parse received cause of transmission */
  if(pMsg->cot == I14DEF_COT_ACTIVATION)
  {
    p14Sector->csenzCOT = I14DEF_COT_ACTCON;
  }
  else if(pMsg->cot == I14DEF_COT_DEACTIVATION)
  {/* Any deactivate will cause the select to be cancelled, even an incorrect one 
     * There is currently not a s14data function to be called indicating deactivate.
     * 
     * The 101 and 601 specs are unclear about whether a DEACTCON positive or negative 
     * should be sent on error. Since even an incorrect DEACT results in no select being
     * active, a positive reply will be sent (except if ioa is bad below).
     */
    p14Sector->csenzCOT = I14DEF_COT_DEACTCON;
  }
  else
  {
    p14Sector->csenzCOT = I14DEF_COT_UNKNOWN_COT | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* Lookup data point using information object address */
  p14Sector->pCSENZPoint = s14data_csenzLookupPoint(p14Sector->i870.pDbHandle, p14Sector->csenzIOA);
  if(p14Sector->pCSENZPoint == TMWDEFS_NULL)
  {

    p14Sector->csenzCOT = I14DEF_COT_UNKNOWN_IOA | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* if deactivate, command is finished */
  if(pMsg->cot == I14DEF_COT_DEACTIVATION)
  {
    return;
  }

  /* Perform select or execute and set operation status */
  if((p14Sector->csenzQOS & I14DEF_QOC_SE_SELECT) != 0)
  {
    p14Sector->csenzState = TMWDEFS_CMD_STATE_SELECTING;
    p14Sector->csenzStatus = s14data_csenzSelect(p14Sector->pCSENZPoint, 
      pMsg->cot, p14Sector->csenzBCD, p14Sector->csenzQOS);
  }
  else
  {
    /* If a select is required for this point make sure a valid select
     * operation is pending and compare the execute parameters against
     * the select parameters.
     */
    if(s14data_csenzSelectRequired(p14Sector->pCSENZPoint))
    {
      /* Make sure select is active */
      /* Compare information object address */
      /* Compare qualifier, ignore the select bit */
      if(!timerIsActive
         ||(p14Sector->csenzIOA != oldIOA)
         ||(p14Sector->csenzQOS != (oldQOS & ~I14DEF_QOC_SE_MASK)))
      {
        p14Sector->csenzCOT |= I14DEF_COT_NEGATIVE_CONFIRM;
        return;
      }

      /* Compare values */
      for(i=0; i<6; i++)
      {
        if(p14Sector->csenzBCD[i] != oldBCD[i])
        {
          p14Sector->csenzCOT |= I14DEF_COT_NEGATIVE_CONFIRM;
          return;
        }
      }
    }

    p14Sector->csenzState = TMWDEFS_CMD_STATE_EXECUTING;
    p14Sector->csenzStatus = s14data_csenzExecute(p14Sector->pCSENZPoint, 
      pMsg->cot, p14Sector->csenzBCD, p14Sector->csenzQOS);
  }
}

/* function: s14csenz_buildResponse */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14csenz_buildResponse( 
  TMWSCTR *pSector, 
  TMWTYPES_BOOL buildResponse)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  return(s14util_buildCommandResponse(pSector, buildResponse, 
    I14DEF_TYPE_CSENZ1, &p14Sector->csenzCOT, p14Sector->csenzOriginator,
    p14Sector->csenzIOA, p14Sector->csenzQOS, TMWDEFS_TRUE, &p14Sector->csenzBCD, 6, TMWDEFS_NULL, 
    _checkStatus));
}
#endif /* !S14DATA_SUPPORT_MULTICMDS */
#endif /* S14DATA_SUPPORT_CSE_Z */
