/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:S104Session.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief Protocol Session
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   30 Sep 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef IEC104SLAVEPROTOCOLSESSION_H_
#define IEC104SLAVEPROTOCOLSESSION_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "S104TMWIncludes.h"
#include "S104Sector.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
class S104Channel;  //Forward declaration


class S104Session
{
public:
    S104Session(S104Channel& channel);

    virtual ~S104Session();

    SCADAP_ERROR openSession(TMWCHNL* tmwchnl);

    void closeSession();

    void configure(const S104SESN_CONFIG& sessionConfig);

    void addSector(S104Sector* sectorPtr);

    S104Channel& getChannel(){return m_channel;}

private:
    typedef std::vector<S104Sector*>        S104SectorVector;

    S104Channel&        m_channel;          // Owner channel reference
    TMWSESN*            mp_tmwsesn;
    S104SESN_CONFIG*    mp_tmwsesnCnfg;
    S104SectorVector    m_sectors;
};




#endif /* IEC104SLAVEPROTOCOLSESSION_H_ */

/*
 *********************** End of file ******************************************
 */
