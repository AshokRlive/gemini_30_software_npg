/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:S101OutputPoint.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       Contains all the functionality(classes) with regards to updating/setting
 *       values to the different Slave101 output points.
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19 Jul 2016     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "S101Debug.h"
#include "S101OutputPoint.h"
#include "S101Database.h"
#include "S101Sector.h"
#include "S101Session.h"
#include "S101Channel.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
void S101OutputPoint::setDatabase(S101Database* db)
{
    this -> mp_db = db;
}

TMWDEFS_COMMAND_STATUS S101OutputPoint::executeSCO(void* gdb, I870OutputPointConfStr groupID, TMWTYPES_UCHAR sco)
{
    SwitchLogicOperation operation;

    if ((sco & I14DEF_SCS_MASK) == I14DEF_SCS_ON)
    {
        operation.operation = SWITCH_OPERATION_CLOSE;
    }
    else if ((sco & I14DEF_SCS_MASK) == I14DEF_SCS_OFF)
    {
        operation.operation = SWITCH_OPERATION_OPEN;
    }
    else
    {
        return TMWDEFS_CMD_STAT_FAILED;
    }

    operation.local = LU_FALSE; // in future set this depending on cot?
    if(((GeminiDatabase*)gdb)->startOperation(groupID.groupID , operation) ==  GDB_ERROR_NONE)
    {
        return TMWDEFS_CMD_STAT_SUCCESS;
    }
    return TMWDEFS_CMD_STAT_FAILED;
}


TMWDEFS_COMMAND_STATUS S101OutputPoint::executeDCO(void* gdb, I870OutputPointConfStr groupID, TMWTYPES_UCHAR sco)
{
    SwitchLogicOperation operation;

    if ((sco & I14DEF_DCS_MASK) == I14DEF_DCS_ON)
    {
        operation.operation = SWITCH_OPERATION_CLOSE;
    }
    else if ((sco & I14DEF_DCS_MASK) == I14DEF_DCS_OFF)
    {
        operation.operation = SWITCH_OPERATION_OPEN;
    }
    else
    {
        return TMWDEFS_CMD_STAT_FAILED;
    }

    operation.local = LU_FALSE; // in future set this depending on cot?
    if(((GeminiDatabase*)gdb)->startOperation(groupID.groupID , operation) == GDB_ERROR_NONE)
    {
        return TMWDEFS_CMD_STAT_SUCCESS;
    }
    return TMWDEFS_CMD_STAT_FAILED;
}

SCADAP_ERROR S101OutputPoint::getSectorHandle(TMWSCTR** ppSectorHdle)
{
    checkNotNull(mp_db,"No sector database");

    return mp_db->getSectorHandle(ppSectorHdle);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
