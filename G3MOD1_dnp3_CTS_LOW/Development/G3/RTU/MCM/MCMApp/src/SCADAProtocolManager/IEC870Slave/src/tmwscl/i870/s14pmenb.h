/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14pmenb.h
 * description: This file is intended for internal SCL use only.
 *  IEC 60870-5-101/104 slave PMENB (Parameter of Measured Value, 
 *  Scaled Value) functionality.
 */
#ifndef S14PMENB_DEFINED
#define S14PMENB_DEFINED

#include "tmwscl/i870/s14sesn.h"
#include "tmwscl/i870/s14sctr.h"
#include "tmwscl/i870/s14dbas.h"
#include "tmwscl/i870/i870util.h"

#ifdef __cplusplus
extern "C" {
#endif

  void TMWDEFS_CALLBACK s14pmenb_processRequest(
    TMWSCTR *pSector, 
    I870UTIL_MESSAGE *pMsgHeader);

  TMWTYPES_BOOL TMWDEFS_CALLBACK s14pmenb_buildResponse( 
    TMWSCTR *pSector, 
    TMWTYPES_BOOL buildResponse);
 
  /* function: s14pmenb_readIntoResponse 
   * purpose: Read the PMENB values for the point specified and store in response
   *  message to be sent to master
   * arguments:
   *  pTxData - pointer
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   *  cot - cause of tranmission
   *  groupMask - mask specifying what group(s) points should be in
   *  ioa - information object address
   *  pPointIndex - pointer to index of point to be read. This should be updated
   *   by this function.
   * returns:
   *   status
   */
  S14DBAS_GROUP_STATUS TMWDEFS_CALLBACK s14pmenb_readIntoResponse(
    TMWSESN_TX_DATA *pTxData, 
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot, 
    TMWDEFS_GROUP_MASK groupMask, 
    TMWTYPES_ULONG ioa,
    TMWTYPES_USHORT *pPointIndex);

#ifdef __cplusplus
}
#endif
#endif /* S14PMENB_DEFINED */
