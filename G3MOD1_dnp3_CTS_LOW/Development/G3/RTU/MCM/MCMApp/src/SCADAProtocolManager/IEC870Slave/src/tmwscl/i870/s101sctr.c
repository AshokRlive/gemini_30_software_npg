/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s101sctr.h
 * description: IEC 60870-5-101 Slave sector
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/s101mem.h"
#include "tmwscl/i870/s101sctr.h"
#include "tmwscl/i870/s14sctr.h"
#include "tmwscl/i870/s14rbe.h"

/* function: _setSectorConfig*/
static void TMWDEFS_LOCAL _setSectorConfig(
  TMWSCTR *pSector,
  const S101SCTR_CONFIG *pConfig)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  p14Sector->i870.asduAddress       = pConfig->asduAddress;
  p14Sector->selectTimeout          = pConfig->selectTimeout;
  p14Sector->defaultResponseTimeout = pConfig->defaultResponseTimeout;
  p14Sector->cmdUseActTerm          = pConfig->cmdUseActTerm;
  p14Sector->cseUseActTerm          = pConfig->cseUseActTerm;
  p14Sector->strictAdherence        = pConfig->strictAdherence;
  p14Sector->localMode              = pConfig->localMode;
  
#if S14DATA_SUPPORT_MULTICMDS
  p14Sector->commandsPerType     = pConfig->commandsPerType;
  p14Sector->commandsPerSector   = pConfig->commandsPerSector; 
#endif

  /* Event configuration */
  p14Sector->sendClockSyncEvents = pConfig->sendClockSyncEvents;
  p14Sector->deleteOldestEvent   = pConfig->deleteOldestEvent;

#if S14DATA_SUPPORT_MSP
  p14Sector->mspScanEnabled = pConfig->mspScanEnabled;
  p14Sector->mspMaxEvents = pConfig->mspMaxEvents;
  p14Sector->mspEventMode = pConfig->mspEventMode;
  p14Sector->mspTimeFormat = pConfig->mspTimeFormat;
#endif

#if S14DATA_SUPPORT_MDP
  p14Sector->mdpScanEnabled = pConfig->mdpScanEnabled;
  p14Sector->mdpMaxEvents = pConfig->mdpMaxEvents;
  p14Sector->mdpEventMode = pConfig->mdpEventMode;
  p14Sector->mdpTimeFormat = pConfig->mdpTimeFormat;
#endif

#if S14DATA_SUPPORT_MST
  p14Sector->mstScanEnabled = pConfig->mstScanEnabled;
  p14Sector->mstMaxEvents = pConfig->mstMaxEvents;
  p14Sector->mstEventMode = pConfig->mstEventMode;
  p14Sector->mstTimeFormat = pConfig->mstTimeFormat;
#endif

#if S14DATA_SUPPORT_MBO
  p14Sector->mboScanEnabled = pConfig->mboScanEnabled;
  p14Sector->mboMaxEvents = pConfig->mboMaxEvents;
  p14Sector->mboEventMode = pConfig->mboEventMode;
  p14Sector->mboTimeFormat = pConfig->mboTimeFormat;
#endif

#if S14DATA_SUPPORT_MME_A
  p14Sector->mmenaScanEnabled = pConfig->mmenaScanEnabled;
  p14Sector->mmenaMaxEvents = pConfig->mmenaMaxEvents;
  p14Sector->mmenaEventMode = pConfig->mmenaEventMode;
  p14Sector->mmenaTimeFormat = pConfig->mmenaTimeFormat;
#endif

#if S14DATA_SUPPORT_MME_B
  p14Sector->mmenbScanEnabled = pConfig->mmenbScanEnabled;
  p14Sector->mmenbMaxEvents = pConfig->mmenbMaxEvents;
  p14Sector->mmenbEventMode = pConfig->mmenbEventMode;
  p14Sector->mmenbTimeFormat = pConfig->mmenbTimeFormat;
#endif

#if S14DATA_SUPPORT_MME_C
  p14Sector->mmencScanEnabled = pConfig->mmencScanEnabled;
  p14Sector->mmencMaxEvents = pConfig->mmencMaxEvents;
  p14Sector->mmencEventMode = pConfig->mmencEventMode;
  p14Sector->mmencTimeFormat = pConfig->mmencTimeFormat;
#endif

#if S14DATA_SUPPORT_MIT
  /* no scanning for integrated totals changes */
  p14Sector->mitMaxEvents = pConfig->mitMaxEvents;
  p14Sector->mitEventMode = pConfig->mitEventMode;
  p14Sector->mitTimeFormat = pConfig->mitTimeFormat;
#endif

#if S14DATA_SUPPORT_MITC
  /* no scanning for integrated BCD totals changes */
  p14Sector->mitcMaxEvents = pConfig->mitcMaxEvents;
  p14Sector->mitcEventMode = pConfig->mitcEventMode;
  p14Sector->mitcTimeFormat = pConfig->mitcTimeFormat;
  /* 24bit is not supported for MITC */
  if(p14Sector->mitcTimeFormat != TMWDEFS_TIME_FORMAT_NONE)
    p14Sector->mitcTimeFormat = TMWDEFS_TIME_FORMAT_56;
#endif

#if S14DATA_SUPPORT_MEPTA
  p14Sector->meptaScanEnabled = pConfig->meptaScanEnabled;
  p14Sector->meptaMaxEvents = pConfig->meptaMaxEvents;
  p14Sector->meptaEventMode = pConfig->meptaEventMode;
  p14Sector->meptaTimeFormat = pConfig->meptaTimeFormat;
#endif

#if S14DATA_SUPPORT_MEPTB
  p14Sector->meptbScanEnabled = pConfig->meptbScanEnabled;
  p14Sector->meptbMaxEvents = pConfig->meptbMaxEvents;
  p14Sector->meptbEventMode = pConfig->meptbEventMode;
  p14Sector->meptbTimeFormat = pConfig->meptbTimeFormat;
#endif

#if S14DATA_SUPPORT_MEPTC
  p14Sector->meptcScanEnabled = pConfig->meptcScanEnabled;
  p14Sector->meptcMaxEvents = pConfig->meptcMaxEvents;
  p14Sector->meptcEventMode = pConfig->meptcEventMode;
  p14Sector->meptcTimeFormat = pConfig->meptcTimeFormat;
#endif

#if S14DATA_SUPPORT_MPS
  p14Sector->mpsnaScanEnabled = pConfig->mpsnaScanEnabled;
  p14Sector->mpsnaMaxEvents = pConfig->mpsnaMaxEvents;
  p14Sector->mpsnaEventMode = pConfig->mpsnaEventMode;
#endif

#if S14DATA_SUPPORT_MMEND
  p14Sector->mmendScanEnabled = pConfig->mmendScanEnabled;
  p14Sector->mmendMaxEvents = pConfig->mmendMaxEvents;
  p14Sector->mmendEventMode = pConfig->mmendEventMode;
#endif

#if S14DATA_SUPPORT_FILE
  /* File transfer Directory */
  p14Sector->fdrtaScanEnabled = pConfig->fdrtaScanEnabled;
#endif

#if S14DATA_SUPPORT_DOUBLE_TRANS 
#if  S14DATA_SUPPORT_MSP
  p14Sector->mspTransmissionMode = pConfig->mspTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MDP
  p14Sector->mdpTransmissionMode = pConfig->mdpTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MST
  p14Sector->mstTransmissionMode = pConfig->mstTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MBO
  p14Sector->mboTransmissionMode = pConfig->mboTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MIT 
  p14Sector->mitTransmissionMode = pConfig->mitTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MITC 
  p14Sector->mitcTransmissionMode = pConfig->mitcTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MME_A 
  p14Sector->mmenaTransmissionMode = pConfig->mmenaTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MME_B
  p14Sector->mmenbTransmissionMode = pConfig->mmenbTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MME_C 
  p14Sector->mmencTransmissionMode = pConfig->mmencTransmissionMode;
#endif  
#endif

  /* Time format for response to read CRDNA */
  p14Sector->readTimeFormat = pConfig->readTimeFormat;
  p14Sector->readMsrndTimeFormat = pConfig->readMsrndTimeFormat;

  /* Time format for response to read CICNA */
  p14Sector->cicnaTimeFormat = pConfig->cicnaTimeFormat;
}

/* function: s101sctr_initConfig */
void TMWDEFS_GLOBAL s101sctr_initConfig(
  S101SCTR_CONFIG *pConfig)
{
  pConfig->asduAddress            = 3;
  pConfig->clockValidPeriod       = TMWDEFS_HOURS(24);
  pConfig->cyclicPeriod           = TMWDEFS_SECONDS(60);
  pConfig->cyclicFirstPeriod      = 500;
  pConfig->rbeScanPeriod          = 0;
  pConfig->selectTimeout          = TMWDEFS_SECONDS(5);
  pConfig->defaultResponseTimeout = TMWDEFS_SECONDS(60);

#if S14DATA_SUPPORT_MULTICMDS
  pConfig->commandsPerType        = 1;
  pConfig->commandsPerSector      = 7; 
#endif

  pConfig->backgroundPeriod       = 0; 
  pConfig->cmdUseActTerm          = TMWDEFS_TRUE;
  pConfig->cseUseActTerm          = TMWDEFS_TRUE;
  pConfig->strictAdherence        = 0;
  pConfig->localMode              = TMWDEFS_FALSE;
  pConfig->pStatCallback          = TMWDEFS_NULL;
  pConfig->pStatCallbackParam     = TMWDEFS_NULL;

  /* Event Configuration */
  pConfig->mspScanEnabled = TMWDEFS_FALSE;
  pConfig->mspMaxEvents = 100; 
  pConfig->mspEventMode = TMWDEFS_EVENT_MODE_SOE;
  pConfig->mspTimeFormat = TMWDEFS_TIME_FORMAT_56;

  pConfig->mdpScanEnabled = TMWDEFS_FALSE;
  pConfig->mdpMaxEvents = 100;
  pConfig->mdpEventMode = TMWDEFS_EVENT_MODE_SOE;
  pConfig->mdpTimeFormat = TMWDEFS_TIME_FORMAT_56;

  pConfig->mstScanEnabled = TMWDEFS_FALSE;
  pConfig->mstMaxEvents = 100;
  pConfig->mstEventMode = TMWDEFS_EVENT_MODE_SOE;
  pConfig->mstTimeFormat = TMWDEFS_TIME_FORMAT_56;

  pConfig->mboScanEnabled = TMWDEFS_FALSE;
  pConfig->mboMaxEvents = 100;
  pConfig->mboEventMode = TMWDEFS_EVENT_MODE_SOE;
  pConfig->mboTimeFormat = TMWDEFS_TIME_FORMAT_56;

  pConfig->mmenaScanEnabled = TMWDEFS_FALSE;
  pConfig->mmenaMaxEvents = 100;
  pConfig->mmenaEventMode = TMWDEFS_EVENT_MODE_MOST_RECENT;
  pConfig->mmenaTimeFormat = TMWDEFS_TIME_FORMAT_NONE;

  pConfig->mmenbScanEnabled = TMWDEFS_FALSE;
  pConfig->mmenbMaxEvents = 100;
  pConfig->mmenbEventMode = TMWDEFS_EVENT_MODE_MOST_RECENT;
  pConfig->mmenbTimeFormat = TMWDEFS_TIME_FORMAT_NONE;

  pConfig->mmencScanEnabled = TMWDEFS_FALSE;
  pConfig->mmencMaxEvents = 100;
  pConfig->mmencEventMode = TMWDEFS_EVENT_MODE_MOST_RECENT;
  pConfig->mmencTimeFormat = TMWDEFS_TIME_FORMAT_NONE;

  /* no scanning for integrated totals changes */
  pConfig->mitMaxEvents = 100;
  pConfig->mitEventMode = TMWDEFS_EVENT_MODE_SOE;
  pConfig->mitTimeFormat = TMWDEFS_TIME_FORMAT_56;

#if S14DATA_SUPPORT_MITC
  /* no scanning for BCD integrated totals changes */
  pConfig->mitcMaxEvents = 100;
  pConfig->mitcEventMode = TMWDEFS_EVENT_MODE_SOE;
  pConfig->mitcTimeFormat = TMWDEFS_TIME_FORMAT_56;
#endif

  pConfig->meptaScanEnabled = TMWDEFS_FALSE;
  pConfig->meptaMaxEvents = 100;
  pConfig->meptaEventMode = TMWDEFS_EVENT_MODE_SOE;
  pConfig->meptaTimeFormat = TMWDEFS_TIME_FORMAT_56;

  pConfig->meptbScanEnabled = TMWDEFS_FALSE;
  pConfig->meptbMaxEvents = 100;
  pConfig->meptbEventMode = TMWDEFS_EVENT_MODE_SOE;
  pConfig->meptbTimeFormat = TMWDEFS_TIME_FORMAT_56;

  pConfig->meptcScanEnabled = TMWDEFS_FALSE;
  pConfig->meptcMaxEvents = 100;
  pConfig->meptcEventMode = TMWDEFS_EVENT_MODE_SOE;
  pConfig->meptcTimeFormat = TMWDEFS_TIME_FORMAT_56;

  pConfig->mpsnaScanEnabled = TMWDEFS_FALSE;
  pConfig->mpsnaMaxEvents = 100;
  pConfig->mpsnaEventMode = TMWDEFS_EVENT_MODE_SOE;

  pConfig->mmendScanEnabled = TMWDEFS_FALSE;
  pConfig->mmendMaxEvents = 100;
  pConfig->mmendEventMode = TMWDEFS_EVENT_MODE_SOE;

#if S14DATA_SUPPORT_DOUBLE_TRANS 
#if  S14DATA_SUPPORT_MSP
  pConfig->mspTransmissionMode = S14DATA_TRANSMISSION_SINGLE;
#endif
#if  S14DATA_SUPPORT_MDP
  pConfig->mdpTransmissionMode = S14DATA_TRANSMISSION_SINGLE;
#endif
#if  S14DATA_SUPPORT_MST
  pConfig->mstTransmissionMode = S14DATA_TRANSMISSION_SINGLE;
#endif
#if  S14DATA_SUPPORT_MBO
  pConfig->mboTransmissionMode = S14DATA_TRANSMISSION_SINGLE;
#endif
#if  S14DATA_SUPPORT_MIT 
  pConfig->mitTransmissionMode = S14DATA_TRANSMISSION_SINGLE;
#endif
#if  S14DATA_SUPPORT_MITC 
  pConfig->mitcTransmissionMode = S14DATA_TRANSMISSION_SINGLE;
#endif
#if  S14DATA_SUPPORT_MME_A 
  pConfig->mmenaTransmissionMode = S14DATA_TRANSMISSION_SINGLE;
#endif
#if  S14DATA_SUPPORT_MME_B
  pConfig->mmenbTransmissionMode = S14DATA_TRANSMISSION_SINGLE;
#endif
#if  S14DATA_SUPPORT_MME_C 
  pConfig->mmencTransmissionMode = S14DATA_TRANSMISSION_SINGLE;
#endif  
#endif

  /* File Transfer Directory */
  pConfig->fdrtaScanEnabled = TMWDEFS_FALSE;

  /* Since default time format is 56 bits spontaneous clock sync events
   * are not required 
   */
  pConfig->sendClockSyncEvents = TMWDEFS_FALSE;

  /* Delete oldest event in queue */
  pConfig->deleteOldestEvent   = TMWDEFS_TRUE;

  /* Time format for response to read CRDNA. This would normally be none
   *  but for completeness we allow time format to be specified
   *  readTimeFormat is also used as the time format for responses to CCINA counter requests
   */
  pConfig->readTimeFormat = TMWDEFS_TIME_FORMAT_NONE;
  pConfig->readMsrndTimeFormat = TMWDEFS_TIME_FORMAT_NONE;

  /* Time format for response to CICNA. According to spec this must be set 
   * to TMWDEFS_TIME_FORMAT_NONE. Setting this to 24bit or 56bit causes those
   * types to be sent instead.
   */
  pConfig->cicnaTimeFormat = TMWDEFS_TIME_FORMAT_NONE;
}

/* function: s101sctr_openSector */
TMWSCTR * TMWDEFS_GLOBAL s101sctr_openSector(
  TMWSESN *pSession, 
  const S101SCTR_CONFIG *pConfig, 
  void *pUserHandle)
{
  S14SCTR *p14Sector;

  /* Allocate space for session context */
  S101SCTR *p101Sector = (S101SCTR *)s101mem_alloc(S101MEM_SCTR_TYPE);
  if(p101Sector == TMWDEFS_NULL)
  {
    return(TMWDEFS_NULL);
  }

  p14Sector = (S14SCTR *)p101Sector;

  /* Configuration */
  p14Sector->i870.pProcessInfoFunc = TMWDEFS_NULL;

  p14Sector->cyclicPeriod = pConfig->cyclicPeriod;
  p14Sector->cyclicFirstPeriod = pConfig->cyclicFirstPeriod;
  p14Sector->backgroundPeriod = pConfig->backgroundPeriod;
  p14Sector->rbeScanPeriod = pConfig->rbeScanPeriod;
  p14Sector->clockValidPeriod = pConfig->clockValidPeriod;
  
  _setSectorConfig((TMWSCTR *)p14Sector, pConfig);

  /* S101 specific Info */

  /* Test Command */
  p101Sector->ctsnaCOT = 0;
  p101Sector->ctsnaOriginator = 0;

  /* Delay Acquisition Command */
  p101Sector->ccdnaCOT = 0;
  p101Sector->ccdnaTime = 0;
  p101Sector->ccdnaOriginator = 0;
  p101Sector->ccdnaFirstByteReceived = 0;

  /* Lock channel */
  TMWTARG_LOCK_SECTION(&pSession->pChannel->lock);

  /* Open IEC 60870 sector */
  i870sctr_openSector((TMWSCTR *)p101Sector, pSession, pConfig->pStatCallback, pConfig->pStatCallbackParam);

  /* Open IEC 60870-5-101/104 slave sector */
  if(!s14sctr_openSector((TMWSCTR *)p101Sector, pUserHandle))
  {
    /* Unlock channel */
    TMWTARG_UNLOCK_SECTION(&pSession->pChannel->lock);

    s101sctr_closeSector((TMWSCTR *)p101Sector);
    return(TMWDEFS_NULL);
  }

  /* Unlock channel */
  TMWTARG_UNLOCK_SECTION(&pSession->pChannel->lock);

  return((TMWSCTR *)p101Sector);
}
  
/* function: s101sctr_getSectorConfig */
TMWTYPES_BOOL TMWDEFS_GLOBAL s101sctr_getSectorConfig(
  TMWSCTR *pSector,
  S101SCTR_CONFIG *pConfig)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  pConfig->asduAddress            = p14Sector->i870.asduAddress;
  
  pConfig->cyclicPeriod           = p14Sector->cyclicPeriod;
  pConfig->cyclicFirstPeriod      = p14Sector->cyclicFirstPeriod;
  pConfig->backgroundPeriod       = p14Sector->backgroundPeriod;
  pConfig->rbeScanPeriod          = p14Sector->rbeScanPeriod;
  pConfig->selectTimeout          = p14Sector->selectTimeout;
  pConfig->defaultResponseTimeout = p14Sector->defaultResponseTimeout;
  pConfig->clockValidPeriod       = p14Sector->clockValidPeriod;
  
#if S14DATA_SUPPORT_MULTICMDS
  pConfig->commandsPerType        = p14Sector->commandsPerType;
  pConfig->commandsPerSector      = p14Sector->commandsPerSector;
#endif

  pConfig->cmdUseActTerm          = p14Sector->cmdUseActTerm;
  pConfig->cseUseActTerm          = p14Sector->cseUseActTerm;
  pConfig->strictAdherence        = p14Sector->strictAdherence;
  pConfig->localMode              = p14Sector->localMode;

  pConfig->sendClockSyncEvents    = p14Sector->sendClockSyncEvents;
  pConfig->deleteOldestEvent      = p14Sector->deleteOldestEvent;

#if S14DATA_SUPPORT_MSP
  pConfig->mspScanEnabled      = p14Sector->mspScanEnabled;
  pConfig->mspMaxEvents        = p14Sector->mspMaxEvents;
  pConfig->mspEventMode        = p14Sector->mspEventMode;
  pConfig->mspTimeFormat       = p14Sector->mspTimeFormat;
#endif

#if S14DATA_SUPPORT_MDP
  pConfig->mdpScanEnabled      = p14Sector->mdpScanEnabled;
  pConfig->mdpMaxEvents        = p14Sector->mdpMaxEvents;
  pConfig->mdpEventMode        = p14Sector->mdpEventMode;
  pConfig->mdpTimeFormat       = p14Sector->mdpTimeFormat;
#endif

#if S14DATA_SUPPORT_MST
  pConfig->mstScanEnabled      = p14Sector->mstScanEnabled;
  pConfig->mstMaxEvents        = p14Sector->mstMaxEvents;
  pConfig->mstEventMode        = p14Sector->mstEventMode;
  pConfig->mstTimeFormat       = p14Sector->mstTimeFormat;
#endif

#if S14DATA_SUPPORT_MBO
  pConfig->mboScanEnabled      = p14Sector->mboScanEnabled;
  pConfig->mboMaxEvents        = p14Sector->mboMaxEvents;
  pConfig->mboEventMode        = p14Sector->mboEventMode;
  pConfig->mboTimeFormat       = p14Sector->mboTimeFormat;
#endif

#if S14DATA_SUPPORT_MME_A
  pConfig->mmenaScanEnabled    = p14Sector->mmenaScanEnabled;
  pConfig->mmenaMaxEvents      = p14Sector->mmenaMaxEvents;
  pConfig->mmenaEventMode      = p14Sector->mmenaEventMode;
  pConfig->mmenaTimeFormat     = p14Sector->mmenaTimeFormat;
#endif

#if S14DATA_SUPPORT_MME_B
  pConfig->mmenbScanEnabled    = p14Sector->mmenbScanEnabled;
  pConfig->mmenbMaxEvents      = p14Sector->mmenbMaxEvents;
  pConfig->mmenbEventMode      = p14Sector->mmenbEventMode;
  pConfig->mmenbTimeFormat     = p14Sector->mmenbTimeFormat;
#endif

#if S14DATA_SUPPORT_MME_C
  pConfig->mmencScanEnabled    = p14Sector->mmencScanEnabled;
  pConfig->mmencMaxEvents      = p14Sector->mmencMaxEvents;
  pConfig->mmencEventMode      = p14Sector->mmencEventMode;
  pConfig->mmencTimeFormat     = p14Sector->mmencTimeFormat;
#endif

#if S14DATA_SUPPORT_MIT
  pConfig->mitMaxEvents        = p14Sector->mitMaxEvents;
  pConfig->mitEventMode        = p14Sector->mitEventMode;
  pConfig->mitTimeFormat       = p14Sector->mitTimeFormat;
#endif

#if S14DATA_SUPPORT_MITC
  pConfig->mitcMaxEvents       = p14Sector->mitcMaxEvents;
  pConfig->mitcEventMode       = p14Sector->mitcEventMode;
  pConfig->mitcTimeFormat      = p14Sector->mitcTimeFormat;
#endif

#if S14DATA_SUPPORT_MEPTA
  pConfig->meptaScanEnabled    = p14Sector->meptaScanEnabled;
  pConfig->meptaMaxEvents      = p14Sector->meptaMaxEvents;
  pConfig->meptaEventMode      = p14Sector->meptaEventMode;
  pConfig->meptaTimeFormat     = p14Sector->meptaTimeFormat;
#endif

#if S14DATA_SUPPORT_MEPTB
  pConfig->meptbScanEnabled    = p14Sector->meptbScanEnabled;
  pConfig->meptbMaxEvents      = p14Sector->meptbMaxEvents;
  pConfig->meptbEventMode      = p14Sector->meptbEventMode;
  pConfig->meptbTimeFormat     = p14Sector->meptbTimeFormat;
#endif

#if S14DATA_SUPPORT_MEPTC
  pConfig->meptcScanEnabled    = p14Sector->meptcScanEnabled;
  pConfig->meptcMaxEvents      = p14Sector->meptcMaxEvents;
  pConfig->meptcEventMode      = p14Sector->meptcEventMode;
  pConfig->meptcTimeFormat     = p14Sector->meptcTimeFormat;
#endif

#if S14DATA_SUPPORT_MPS
  pConfig->mpsnaScanEnabled    = p14Sector->mpsnaScanEnabled;
  pConfig->mpsnaMaxEvents      = p14Sector->mpsnaMaxEvents;
  pConfig->mpsnaEventMode      = p14Sector->mpsnaEventMode;
#endif

#if S14DATA_SUPPORT_MMEND
  pConfig->mmendScanEnabled    = p14Sector->mmendScanEnabled;
  pConfig->mmendMaxEvents      = p14Sector->mmendMaxEvents;
  pConfig->mmendEventMode      = p14Sector->mmendEventMode;
#endif

#if S14DATA_SUPPORT_DOUBLE_TRANS 
#if  S14DATA_SUPPORT_MSP
  pConfig->mspTransmissionMode = p14Sector->mspTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MDP
  pConfig->mdpTransmissionMode = p14Sector->mdpTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MST
  pConfig->mstTransmissionMode = p14Sector->mstTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MBO
  pConfig->mboTransmissionMode = p14Sector->mboTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MIT 
  pConfig->mitTransmissionMode = p14Sector->mitTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MITC 
  pConfig->mitcTransmissionMode = p14Sector->mitcTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MME_A 
  pConfig->mmenaTransmissionMode = p14Sector->mmenaTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MME_B
  pConfig->mmenbTransmissionMode = p14Sector->mmenbTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MME_C 
  pConfig->mmencTransmissionMode = p14Sector->mmencTransmissionMode;
#endif  
#endif

#if S14DATA_SUPPORT_FILE
  pConfig->fdrtaScanEnabled    = p14Sector->fdrtaScanEnabled;
#endif

  pConfig->readTimeFormat      = p14Sector->readTimeFormat;
  pConfig->readMsrndTimeFormat = p14Sector->readMsrndTimeFormat;
  pConfig->cicnaTimeFormat     = p14Sector->cicnaTimeFormat;

  return(TMWDEFS_TRUE);
}

/* function: s101sctr_setSectorConfig*/
TMWTYPES_BOOL TMWDEFS_GLOBAL s101sctr_setSectorConfig(
  TMWSCTR *pSector,
  const S101SCTR_CONFIG *pConfig)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  /* Lock channel */
  TMWTARG_LOCK_SECTION(&pSector->pSession->pChannel->lock);

  if(p14Sector->clockValidPeriod != pConfig->clockValidPeriod)
  {
    p14Sector->clockValidPeriod = pConfig->clockValidPeriod;

    /* See if timer is currently active */
    if(tmwtimer_isActive(&p14Sector->clockValidTimer))
    {
      /* If so, cancel it */
      tmwtimer_cancel(&p14Sector->clockValidTimer);

      /* If the new timeout is not zero restart it using the
       *  new timeout value. For now we will ignore any time
       *  that has already elapsed
       */
      if(p14Sector->clockValidPeriod != 0)
      {
        tmwtimer_start(&p14Sector->clockValidTimer, 
          p14Sector->clockValidPeriod, pSector->pSession->pChannel,
          TMWDEFS_NULL, TMWDEFS_NULL);
      }
    }
  }

  /* Cyclic Period */
  s14sctr_updateCyclicPeriod(pSector, pConfig->cyclicPeriod);

  /* Background scan Period */
  s14sctr_updateBackgroundPeriod(pSector, pConfig->backgroundPeriod);

  /* RBE Scan Period */
  s14rbe_updateRBEScanPeriod(pSector, pConfig->rbeScanPeriod);
  
  _setSectorConfig(pSector, pConfig);

  /* Unlock channel */
  TMWTARG_UNLOCK_SECTION(&pSector->pSession->pChannel->lock);

  return(TMWDEFS_TRUE);
}

/* function: s101sctr_modifySector */
TMWTYPES_BOOL TMWDEFS_GLOBAL s101sctr_modifySector(
  TMWSCTR *pSector, 
  const S101SCTR_CONFIG *pConfig, 
  TMWTYPES_ULONG configMask)
{
  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  /* Lock channel */
  TMWTARG_LOCK_SECTION(&pSector->pSession->pChannel->lock);

  /* Sector Address */
  if(configMask & S101SCTR_CONFIG_ASDU_ADDRESS)
  {
    p14Sector->i870.asduAddress = pConfig->asduAddress;
  }

  /* Clock Valid Period */
  if(configMask & S101SCTR_CONFIG_CLOCK_VALID_PRD)
  {
    p14Sector->clockValidPeriod = pConfig->clockValidPeriod;

    /* See if timer is currently active */
    if(tmwtimer_isActive(&p14Sector->clockValidTimer))
    {
      /* If so, cancel it */
      tmwtimer_cancel(&p14Sector->clockValidTimer);

      /* If the new timeout is not zero restart it using the
       *  new timeout value. For now we will ignore any time
       *  that has already elapsed
       */
      if(p14Sector->clockValidPeriod != 0)
      {
        tmwtimer_start(&p14Sector->clockValidTimer, 
          p14Sector->clockValidPeriod, pSector->pSession->pChannel,
          TMWDEFS_NULL, TMWDEFS_NULL);
      }
    }
  }

  /* Cyclic Period */
  if(configMask & S101SCTR_CONFIG_CYCLIC_PERIOD)
  {
    s14sctr_updateCyclicPeriod(pSector, pConfig->cyclicPeriod);
  }

  /* Background scan Period */
  if(configMask & S101SCTR_CONFIG_BKGRND_PERIOD)
  {
    s14sctr_updateBackgroundPeriod(pSector, pConfig->backgroundPeriod);
  }

  /* RBE Scan Period */
  if(configMask & S101SCTR_CONFIG_RBE_SCAN_PERIOD)
  {
    s14rbe_updateRBEScanPeriod(pSector, pConfig->rbeScanPeriod);
  }

  /* Select Timeout */
  if(configMask & S101SCTR_CONFIG_SELECT_TIMEOUT)
  {
    p14Sector->selectTimeout = pConfig->selectTimeout;
    /* Select timeouts are managed by the individual commands */
  }

  /* Unlock channel */
  TMWTARG_UNLOCK_SECTION(&pSector->pSession->pChannel->lock);

  return(TMWDEFS_TRUE);
}

/* function: s101sctr_closeSector */
TMWTYPES_BOOL TMWDEFS_GLOBAL s101sctr_closeSector(
  TMWSCTR *pSector)
{
  /* Get pointer to application specific sector info */
  S101SCTR *p101Sector = (S101SCTR *)pSector;

  /* Check for NULL since this would be a common error */
  if(pSector == TMWDEFS_NULL)
  {
    return(TMWDEFS_FALSE);
  }

  /* Locking is done in s14sctr_closeSector */
  s14sctr_closeSector(pSector);

  /* i870sctr_closeSector is called from s14sctr_closeSector */ 

  /* Free memory */
  s101mem_free(p101Sector);
 

  return(TMWDEFS_TRUE);
}

/* function: s101sctr_sendCyclic */
void TMWDEFS_GLOBAL s101sctr_sendCyclic(
  TMWSCTR *pSector)
{
  s14sctr_sendCyclic(pSector);
}
