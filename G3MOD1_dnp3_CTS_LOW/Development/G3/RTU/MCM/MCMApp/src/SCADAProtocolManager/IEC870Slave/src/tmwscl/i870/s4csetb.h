/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s4csetb.h
 * description: This file is intended for internal SCL use only.
 *  IEC 60870-5-104 slave CSETB (Scaled Set Point With Time) functionality.
 */
#ifndef S4CSETB_DEFINED
#define S4CSETB_DEFINED

#include "tmwscl/i870/s14sesn.h"
#include "tmwscl/i870/s14sctr.h"
#include "tmwscl/i870/i870util.h"

#ifdef __cplusplus
extern "C" {
#endif

  void TMWDEFS_CALLBACK s4csetb_processRequest(
    TMWSCTR *pSector, 
    I870UTIL_MESSAGE *pMsgHeader);

  TMWTYPES_BOOL TMWDEFS_CALLBACK s4csetb_buildResponse( 
    TMWSCTR *pSector, 
    TMWTYPES_BOOL buildResponse);

#ifdef __cplusplus
}
#endif
#endif /* S4CSETB_DEFINED */
