/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:S104Database.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief Protocol Point database
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   8 Oct 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "S104Database.h"
#include "S104Debug.h"
#include "S104Sector.h"
#include "S104Session.h"
#include "S104Channel.h"
#include "IMCMApplication.h"    //For reset (reboot) message

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static Logger& log = Logger::getLogger(SUBSYSTEM_ID_SCADA_HANDLER);

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
#if USE_DUMMY_104_CONFIG
#include "GeminiDatabase.h"
S104Database::S104Database(S104Sector& sector):
                 NoOfMSP(2),NoOfMDP(1),NoOfMMENA(1),NoOfMMENB(1),NoOfMMENC(0),NoOfMIT(1),NoOfSCO(3),NoOfDCO(1),m_sector(sector)
{
    const int NoOfInputPoints = 6;
    const int NoOfOutputPoints = 4;
    static lu_uint16_t i = 0; // put this in a better place later
    GeminiDatabase& gdb = sector.getSession().getChannel().getG3Database();
    S104InputPoint::Config conf[NoOfInputPoints];
    I870OutputPointConf outputconf[NoOfOutputPoints];

    /*configuring single point*/
    conf[0].vpointID.ID = 28;
    conf[0].vpointID.group = 0;
    conf[0].pointAttributes.ioa= 101;

    /*configuring single point*/
    conf[1].vpointID.ID = 29;
    conf[1].vpointID.group = 0;
    conf[1].pointAttributes.ioa = 102;

    /*configuring double point*/
    conf[2].vpointID.ID = 30;
    conf[2].vpointID.group = 0;
    conf[2].pointAttributes.ioa = 200;

    /*configuring SCALED analog point*/
    conf[3].vpointID.ID = 15;
    conf[3].vpointID.group = 0;
    conf[3].pointAttributes.ioa = 600;

    /*configuring floating analog point*/
    conf[4].vpointID.ID = 14;
    conf[4].vpointID.group = 0;
    conf[4].pointAttributes.ioa = 700;

    /*configuring MIT point*/
    conf[5].vpointID.ID = 31;
    conf[5].vpointID.group = 0;
    conf[5].pointAttributes.groupMask = TMWDEFS_GROUP_MASK_GENERAL;
    conf[5].pointAttributes.ioa = 800;

    /*configuring MIT point*/
    outputconf[0].groupID= 4;
    outputconf[0].ioa = 101;
    outputconf[0].selectRequired = true;

    /*configuring double point*/
    outputconf[1].groupID= 5;
    outputconf[1].ioa = 2102;
    outputconf[1].selectRequired = false;

    /*configuring double point*/
    outputconf[2].groupID = 6;
    outputconf[2].ioa = 2103;

    /*configuring double point*/
    outputconf[3].groupID = 7;
    outputconf[3].ioa = 2104;

    /* Add & Attach MSP*/

    S104MSP ** singlePoint = new S104MSP*[NoOfMSP];

    for(int x=0; x < NoOfMSP ; x ++)
    {
        singlePoint[x]= new S104MSP(conf[i]);
        addInputPoint(S104InputPoint::MSP,*(&singlePoint[x]));
        gdb.attach(conf[i].vpointID, *(&singlePoint[x]));
        i++;
    }


    /* Add & Attach MSP*/

    S104MDP ** doublePoint = new S104MDP*[NoOfMDP];

    for(int y=0 ;y < NoOfMDP ; y ++)
    {
        doublePoint[y]= new S104MDP(conf[i]);
        addInputPoint(S104InputPoint::MDP,*(&doublePoint[y]));
        gdb.attach(conf[i].vpointID, *(&doublePoint[y]));
        i++;
    }

    /* Add & Attach scaled analog point*/
    S104Analog ** sanalogpoint = new S104Analog*[NoOfMMENB];

    for(int z=0 ;z < NoOfMMENB ; z ++)
    {
        sanalogpoint[z]= new S104Analog(S104InputPoint::MMENB,conf[i]);
        addInputPoint(S104InputPoint::MMENB,*(&sanalogpoint[z]));
        gdb.attach(conf[i].vpointID, *(&sanalogpoint[z]));
        i++;
    }

    /* Add & Attach floating analog point*/

    S104Analog ** fanalogpoint = new S104Analog*[NoOfMMENA];

    for(int z=0 ;z < NoOfMMENA ; z ++)
    {
        fanalogpoint[z]= new S104Analog(S104InputPoint::MMENA,conf[i]);
        addInputPoint(S104InputPoint::MMENA,*(&fanalogpoint[z]));
        gdb.attach(conf[i].vpointID, *(&fanalogpoint[z]));
        i++;
    }

    /* Add & Attach MIT point*/
    S104MIT ** Mit = new S104MIT*[NoOfMIT];

    for(int z=0 ;z < NoOfMIT ; z ++)
    {
        Mit[z]= new S104MIT(conf[i]);
        addInputPoint(S104InputPoint::MIT,*(&Mit[z]));
        gdb.attach(conf[i].vpointID, *(&Mit[z]));
    }

    i = 0 ;
    /* Add & Attach single output point*/

    S104OutputPoint ** SCOpoint = new S104OutputPoint*[NoOfSCO];

    for(int z=0 ;z < NoOfSCO ; z ++)
    {
        SCOpoint[z]= new S104OutputPoint(outputconf[i]);
        addOutputPoint(S104OutputPoint::SCO,*(&SCOpoint[z]));
        i++;
    }

    /* Add & Attach double output point*/

    S104OutputPoint ** DCOpoint = new S104OutputPoint*[NoOfDCO];

    for(int z=0 ;z < NoOfDCO ; z ++)
    {
        DCOpoint[z]= new S104OutputPoint(outputconf[i]);
        addOutputPoint(S104OutputPoint::DCO,*(&DCOpoint[z]));
    }

}

#endif

#if !USE_DUMMY_104_CONFIG
S104Database::S104Database(S104Sector& sector):
                    m_sector(sector)
{}
#endif


S104Database::~S104Database()
{
    /* Delete all points*/
     for (S104MSPVect::iterator it = m_MSPVect.begin();
                     it != m_MSPVect.end(); ++it)
     {
         delete (*it);
     }
     m_MSPVect.clear();
     /* Delete all points*/
     for (S104MDPVect::iterator it = m_MDPVect.begin();
                     it != m_MDPVect.end(); ++it)
     {
         delete (*it);
     }
     m_MDPVect.clear();

}

void S104Database::addInputPoint(S104InputPoint::ProtPointType type,S104InputPoint* pInputPoint)
{
    switch(type)
    {
        case (S104InputPoint::MSP):
            m_MSPVect.push_back(pInputPoint);
            pInputPoint->setDatabase(this);
            break;
        case (S104InputPoint::MDP):
            m_MDPVect.push_back(pInputPoint);
            pInputPoint->setDatabase(this);
            break;
        case (S104InputPoint::MMENA):
            m_MMENAVect.push_back(pInputPoint);
            pInputPoint->setDatabase(this);
            break;
        case (S104InputPoint::MMENB):
            m_MMENBVect.push_back(pInputPoint);
            pInputPoint->setDatabase(this);
            break;
        case (S104InputPoint::MMENC):
            m_MMENCVect.push_back(pInputPoint);
            pInputPoint->setDatabase(this);
            break;
        case (S104InputPoint::MIT):
            S104MIT* mit = (S104MIT*)pInputPoint;
            I870DataMITStr* mitData = (I870DataMITStr*)(mit->getTMWPointPtr());
            mitData->pG3DB = (void *)&(m_sector.getSession().getChannel().getG3Database());
            mitData->mitFreeze = S104Database::mitFreeze;
            m_MITVect.push_back(pInputPoint);
            pInputPoint->setDatabase(this);
            break;
    }
}

/* function for adding output points*/
void S104Database::addOutputPoint(S104OutputPoint::ProtPointType type,S104OutputPoint* pOutputPoint)
{
   switch(type)
   {
       case (S104OutputPoint::SCO):
               I870OutputStr scoOutputStr;
               scoOutputStr.pG3DB =   (void *)&(m_sector.getSession().getChannel().getG3Database());
               scoOutputStr.oPointConf = pOutputPoint->getPointConf();
               scoOutputStr.cscExecute       = S104OutputPoint::executeSCO;
//               m_sector.getSectorHandle(scoOutputStr.ppSectorHdle);
               m_SCOMap.insert(std::pair<TMWTYPES_ULONG,I870OutputStr>(pOutputPoint->getPointConf().ioa,scoOutputStr));
               pOutputPoint->setDatabase(this);
       break;
       case (S104OutputPoint::DCO):
               I870OutputStr dcoOutputStr;
               dcoOutputStr.pG3DB =   (void *)&(m_sector.getSession().getChannel().getG3Database());
               dcoOutputStr.oPointConf = pOutputPoint->getPointConf();
               dcoOutputStr.cdcExecute       = S104OutputPoint::executeDCO;//change later!!!!!
//               m_sector.getSectorHandle(scoOutputStr.ppSectorHdle);
               m_DCOMap.insert(std::pair<TMWTYPES_ULONG,I870OutputStr>(pOutputPoint->getPointConf().ioa,dcoOutputStr));
               pOutputPoint->setDatabase(this);
       break;
   }
}

S104InputPoint* S104Database::getInputPoint(S104InputPoint::ProtPointType type,TMWTYPES_USHORT index)
{
    switch(type)
    {
        case  (S104InputPoint::MSP):
            if(index < m_MSPVect.size())
                return m_MSPVect[index];
            else
                return NULL;
        break;
        case  (S104InputPoint::MDP):
            if(index < m_MDPVect.size())
                return m_MDPVect[index];
            else
                return NULL;
        break;
        case  (S104InputPoint::MMENA):
               if(index < m_MMENAVect.size())
                   return m_MMENAVect[index];
               else
                   return NULL;
        break;
        case  (S104InputPoint::MMENB):
               if(index < m_MMENBVect.size())
                   return m_MMENBVect[index];
               else
                   return NULL;
        break;
        case  (S104InputPoint::MMENC):
           if(index < m_MMENCVect.size())
               return m_MMENCVect[index];
           else
               return NULL;
        break;
        case  (S104InputPoint::MIT):
           if(index < m_MITVect.size())
               return m_MITVect[index];
           else
               return NULL;
        break;
        default :
            return NULL;
    }
}


I870OutputStr* S104Database::getOutputPointStr(S104OutputPoint::ProtPointType type,TMWTYPES_ULONG ioa)
{
    switch(type)
    {
        case  (S104OutputPoint::SCO):
            {
            std::map<TMWTYPES_ULONG,I870OutputStr>::iterator it = m_SCOMap.find(ioa);
            if(it != m_SCOMap.end())
            {
                return &(it->second);
            }
                else
                return NULL;
            }
        break;
        case  (S104OutputPoint::DCO):
            {
            std::map<TMWTYPES_ULONG,I870OutputStr>::iterator it = m_DCOMap.find(ioa);
            if(it != m_DCOMap.end())
            {
                return &(it->second);
            }
                else
                return NULL;
            }
        break;
        default :
            return NULL;
    }
}

SCADAP_ERROR S104Database::getSectorHandle(TMWSCTR** ppSectorHdle)
{
    return m_sector.getSectorHandle(ppSectorHdle);
}


/********************************************/
/* TMW Callback Function Implementation     */
/********************************************/

void* S104Database::mspGetPoint(void *pS104DB, TMWTYPES_USHORT index)
{
    S104InputPoint* p;
    S104Database* db =  (S104Database*)pS104DB;
    p = db->getInputPoint(S104InputPoint::MSP,index);
    DBG_TRACK("%s %s with index : %d",TITLE,__func__,index);
    return p == NULL? NULL: p->getTMWPointPtr();
}



void* S104Database::mdpGetPoint(void *pS104DB, TMWTYPES_USHORT index)
{
    S104InputPoint* p;
    S104Database* db =  (S104Database*)pS104DB;
    p = db->getInputPoint(S104InputPoint::MDP,index);
    DBG_TRACK("%s %s with index : %d",TITLE,__func__,index);
    return p == NULL? NULL: p->getTMWPointPtr();
}


void* S104Database::mmenaGetPoint(void *pS104DB, TMWTYPES_USHORT index)
{
    S104InputPoint* p;
    S104Database* db =  (S104Database*)pS104DB;
    p = db->getInputPoint(S104InputPoint::MMENA,index);
    DBG_TRACK("%s %s with index : %d",TITLE,__func__,index);
    return p == NULL? NULL: p->getTMWPointPtr();
}

void* S104Database::mmenbGetPoint(void *pS104DB, TMWTYPES_USHORT index)
{
    S104InputPoint* p;
    S104Database* db =  (S104Database*)pS104DB;
    p = db->getInputPoint(S104InputPoint::MMENB,index);
    DBG_TRACK("%s %s with index : %d",TITLE,__func__,index);
    return p == NULL? NULL: p->getTMWPointPtr();
}


void* S104Database::mmencGetPoint(void *pS104DB, TMWTYPES_USHORT index)
{
    S104InputPoint* p;
    S104Database* db =  (S104Database*)pS104DB;
    p = db->getInputPoint(S104InputPoint::MMENC,index);
    DBG_TRACK("%s %s with index : %d",TITLE,__func__,index);
    return p == NULL? NULL: p->getTMWPointPtr();
}

void* S104Database::mitGetPoint(void *pS104DB, TMWTYPES_USHORT index)
{
    S104InputPoint* p;
    S104Database* db =  (S104Database*)pS104DB;
    p = db->getInputPoint(S104InputPoint::MIT,index);
    DBG_TRACK("%s %s with index : %d",TITLE,__func__,index);
    return p == NULL? NULL: p->getTMWPointPtr();
}


void* S104Database::cscLookupPoint(void *pS104DB, TMWTYPES_ULONG ioa)
{
    S104Database* db =  (S104Database*)pS104DB;
    DBG_TRACK("%s %s with ioa : %lu",TITLE,__func__,ioa);
    return (void*)(db->getOutputPointStr(S104OutputPoint::SCO, ioa));
}


void* S104Database::cdcLookupPoint(void *pS104DB, TMWTYPES_ULONG ioa)
{
    S104Database* db =  (S104Database*)pS104DB;
    DBG_TRACK("%s %s with ioa : %lu",TITLE,__func__,ioa);
    return (void*)(db->getOutputPointStr(S104OutputPoint::DCO, ioa));
}

TMWTYPES_BOOL S104Database::ccsnaSetTime(TMWDTIME *masterTime)
{
    DBG_TRACK("%s %s",TITLE,__func__);
    if(masterTime == NULL)
    {
        return TMWDEFS_FALSE;
    }
    struct tm outTime;
    lu_bool_t synchronised = (masterTime->invalid == TMWDEFS_FALSE)? LU_TRUE : LU_FALSE;
    lu_uint32_t mseconds;
    // Convert TMW Time to tm
    convertTime(*masterTime, outTime, mseconds);

    // Set the time in the Time Manager
    TimeManager::TERROR res;
    res = TimeManager::getInstance().setTime(outTime, mseconds, synchronised, SYNC_SOURCE_SCADA);
    if(res == TimeManager::TERROR_TIMER)
    {
        perror("System Timer not available for synchronisation");
        return TMWDEFS_FALSE;
    }
    return TMWDEFS_TRUE;
}


void S104Database::ccsnaGetTime(TMWDTIME *rtuTime)
{
    DBG_TRACK("%s %s",TITLE,__func__);

    TimeManager::TimeStr outTime;

    // Get the time from the Time Manager
    TimeManager::getInstance().getTime(outTime);

    // Convert timespec to TMW Time
    convertTime(&outTime, rtuTime);
}


TMWTYPES_BOOL S104Database::mitFreeze(void *pPoint, TMWTYPES_UCHAR qcc)
{
    DBG_TRACK("%s %s",TITLE,__func__);

    GDB_ERROR res = GDB_ERROR_NOT_SUPPORTED;
    I870DataMITStr* pointData = (I870DataMITStr*)pPoint;
    GeminiDatabase* g3db = reinterpret_cast<GeminiDatabase*>(pointData->pG3DB);
    PointIdStr vPointID(pointData->vPoint.group, pointData->vPoint.ID);
    if ((qcc & I14DEF_QCC_FRZ_MASK) == I14DEF_QCC_FRZ_RESET_ONLY)
    {
        res = g3db->clear(vPointID);
    }
    else if ( ((qcc & I14DEF_QCC_FRZ_MASK) == I14DEF_QCC_FRZ_RESET) ||
              ((qcc & I14DEF_QCC_FRZ_MASK) == I14DEF_QCC_FRZ_NO_RESET)
             )
    {
        lu_bool_t andClear = ((qcc & I14DEF_QCC_FRZ_MASK) == I14DEF_QCC_FRZ_RESET)? LU_TRUE : LU_FALSE;
        res = g3db->freeze(vPointID, andClear);
    }
    return (res == GDB_ERROR_NONE)? TMWDEFS_TRUE : TMWDEFS_FALSE;
}


lu_bool_t S104Database::deleteEventID(lu_uint32_t eventID, void *pDB)
{
    LU_UNUSED(eventID);

    DBG_TRACK("%s %s",TITLE,__func__);
    S104Database *pDatabase = NULL;
    S104Sector   *pSector;

    if (pDB == NULL)
    {
        return false;
    }

    pDatabase = (S104Database *) pDB;
    pSector = &(pDatabase->getSector());

// TODO - SKA - Tell the EventManager (?) to remove the event for this session
//    id = pSector->getSessionID();
//    DBG_INFO("Delete Event ID [%d] for session [%d]", eventID,  pSector->getSessionID());
//    eventMgr.remove(id);

    return true;
}

TMWTYPES_BOOL S104Database::hasSectorReset(void *pS104DB, TMWTYPES_UCHAR *pResetCOI)
{
    static bool sectorResetReported = false;
    S104Database* db =  (S104Database*)pS104DB;
    if( (!sectorResetReported) && db->getSector().getSession().getChannel().getG3Database().getEOIFlag() == LU_TRUE)
    {
        *pResetCOI = *pResetCOI & I14DEF_COI_POWER_ON;
        sectorResetReported = true;
        return TMWDEFS_TRUE;
    }
    return TMWDEFS_FALSE;
}


TMWTYPES_BOOL S104Database::crpnaExecuteRTUReset(TMWTYPES_UCHAR qrp)
{
    DBG_TRACK("%s %s",TITLE,__func__);

    /* Note that QRP type 2 (I14DEF_QRP_RESET_EVENTS) is already handled by the stack */
    if(qrp == I14DEF_QRP_RESET_GENERAL)
    {
        log.warn("IEC-104 Master requested RTU warm restart");
        //Ask the Application to restart
        MCMBoard::getInstance().getMCMApp()->restart(IMCMApplication::RESTART_TYPE_DELAY);
        return TMWDEFS_TRUE;
    }
    log.warn("IEC-104 Master requested reset with QRP=%d", qrp);
    return TMWDEFS_FALSE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
