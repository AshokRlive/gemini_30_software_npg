/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i870stat.h
 * description: This file is intended for internal SCL use only.
 *  i870 Statistics
 */
#ifndef I870STAT_DEFINED
#define I870STAT_DEFINED
 
#include "tmwscl/utils/tmwcnfg.h"
#include "tmwscl/utils/tmwsesn.h"
#include "tmwscl/utils/tmwchnl.h"
 
#if TMWCNFG_SUPPORT_STATS
  #define I870STAT_CHNL_ERROR(pChannel, pSession, errorCode) \
    i870stat_chnlError(pChannel, pSession, errorCode)

  #define I870STAT_CHECK_STATUS(pSession, status) \
    i870stat_checkStatus(pSession, status) 

#else
  #define I870STAT_CHNL_ERROR(pChannel, pSession, errorCode) \
    TMWTARG_UNUSED_PARAM(pChannel); TMWTARG_UNUSED_PARAM(pSession); TMWTARG_UNUSED_PARAM(errorCode);

  #define I870STAT_CHECK_STATUS(pSession, status) \
  TMWTARG_UNUSED_PARAM(pSession); TMWTARG_UNUSED_PARAM(status);

#endif
 

#ifdef __cplusplus
extern "C" {
#endif

#if TMWCNFG_SUPPORT_STATS
void TMWDEFS_GLOBAL i870stat_chnlError(
  TMWCHNL *pChannel,
  TMWSESN *pSession,
  TMWCHNL_ERROR_CODE errorCode);

void TMWDEFS_GLOBAL i870stat_checkStatus(
  TMWSESN *pSession,
  I870CHNL_RESP_STATUS status);
#endif
 
#ifdef __cplusplus
}
#endif

#endif
