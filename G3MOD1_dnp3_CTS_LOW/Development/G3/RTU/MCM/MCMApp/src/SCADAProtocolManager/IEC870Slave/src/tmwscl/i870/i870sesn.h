/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i870sesn.h
 * description: This file is intended for internal SCL use only.
 *  IEC 60870-5 session
 */
#ifndef I870SESN_DEFINED
#define I870SESN_DEFINED

#include "tmwscl/utils/tmwsctr.h"
#include "tmwscl/utils/tmwsesn.h"
#include "tmwscl/utils/tmwchnl.h"
#include "tmwscl/utils/tmwlink.h"
#include "tmwscl/utils/tmwdlist.h"
#include "tmwscl/utils/tmwtimer.h"

/* Session transmit callback function */
typedef void (*I870SESN_TX_CALLBACK_FUNC)(
  TMWSESN_TX_DATA *pTxData);


/* Session info callback function */
typedef void (*I870SESN_PROCESS_INFO_FUNC)(
  TMWSESN *pSession, 
  TMWSCL_INFO sesnInfo);

/* Session parse frame function */
typedef TMWTYPES_BOOL (*I870SESN_PARSE_FRAME_FUNC)(
  TMWSESN *pSession, 
  TMWSESN_RX_DATA *pRxFrame, 
  const void *pFuncTable);

/* Session check data function */
typedef TMWTYPES_BOOL (*I870SESN_CHECK_DATA_FUNC)(
  TMWSESN *pSession, 
  TMWDEFS_CLASS_MASK classMask, 
  TMWTYPES_BOOL buildResponse, 
  const void *pFuncTable);

typedef void (*I870SESN_PREPARE_MSG_FUNC)(
  TMWSESN_TX_DATA *pTxData);

typedef TMWTYPES_BOOL (*I870SESN_CHECK_SESN_FUNC)(
  TMWSESN *pSession, 
  TMWTYPES_UCHAR *pBuf);


/* I870 Session Context */
typedef struct I870SessionStruct {

  /* Generic TMW session, must be first field */
  TMWSESN tmw;

  /* dayOfWeek field was added to 101 and 104 in Ed 2.
   * Some devices cannot handle receiving it. Setting this to TMWDEFS_FALSE
   * will prevent copying it into time object in a message.
   */
  TMWTYPES_BOOL useDayOfWeek;

  /* Configuration common for all I870 protocols */
  TMWTYPES_UCHAR cotSize;
  TMWTYPES_UCHAR asduAddrSize;
  TMWTYPES_UCHAR infoObjAddrSize;

  /* Bitmask to indicate special diagnostic formatting */
  TMWTYPES_UCHAR diagFormat;  
 
  /* Sectors on this session */
  TMWDLIST sectorList; 
  
  /* Callback functions */
  I870SESN_TX_CALLBACK_FUNC  pBeforeTxCallback;
  I870SESN_TX_CALLBACK_FUNC  pAfterTxCallback;
  I870SESN_TX_CALLBACK_FUNC  pFailedTxCallback;

  I870SESN_PROCESS_INFO_FUNC pProcessInfoFunc;
  I870SESN_PARSE_FRAME_FUNC  pParseFrameCallbackFunc;
  I870SESN_CHECK_DATA_FUNC   pCheckDataAvailableFunc;
  I870SESN_PREPARE_MSG_FUNC  pPrepareMessageFunc;
  const void                *pSesnFuncTable;

  I870SESN_CHECK_SESN_FUNC   pCheckSesnFunc;

} I870SESN;

#ifdef __cplusplus
extern "C" {
#endif

  /* function: i870sesn_openSession  
   * purpose: Open an I870 session
   * arguments:
   *  pChannel - pointer to channel to open session on
   *  pSession - pointer to session to open
   *  pConfig - pointer to session config structure
   *  pStatCallback - pointer to user provided statistics callback function
   *  pStatCallbackParam - parameter to be passed to statistics function
   *  protocol -
   *  type -
   * returns:
   *  TMWDEFS_TRUE if successful
   */
 TMWTYPES_BOOL TMWDEFS_GLOBAL i870sesn_openSession(
  TMWCHNL *pChannel,
  TMWSESN *pSession,
  void    *pConfig,
  TMWSESN_STAT_CALLBACK pStatCallback,
  void *pStatCallbackParam, 
  TMWTYPES_PROTOCOL protocol,
  TMWTYPES_SESSION_TYPE type);

  /* function: i870sesn_closeSession 
   * purpose: 
   * arguments:
   * returns:
   *  void
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL i870sesn_closeSession(
    TMWSESN *pSession);

  /* function: i870sesn_lookupSector 
   * purpose: 
   * arguments:
   * returns:
   *  void
   */
  TMWSCTR * TMWDEFS_GLOBAL i870sesn_lookupSector(
    TMWSESN *pSession, 
    TMWTYPES_USHORT sectorAddress);
  
#ifdef __cplusplus
}
#endif
#endif /* I870SESN_DEFINED */
