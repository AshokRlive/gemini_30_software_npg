/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limitc the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14mitc.h
 * description: This file is intended for internal SCL use only.
 *  IEC 60870-5-101 slave integrated total functionality.
 */
#ifndef S14MITC_DEFINED
#define S14MITC_DEFINED

#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/utils/tmwsesn.h"
#include "tmwscl/i870/s14sesn.h"
#include "tmwscl/i870/s14sctr.h"
#include "tmwscl/i870/s14dbas.h"
#include "tmwscl/i870/s14evnt.h"

/* Structure used to store integrated totals events */
typedef struct S14MITCEventStruct {
  S14EVNT s14Event;
  TMWTYPES_UCHAR bcd[6];
} S14MITC_EVENT;

#ifdef __cplusplus
extern "C" {
#endif
 
  /* function: s14mitc_init
   * purpose: Initialize MITC events
   * arguments:
   *  pSector - identifies sector
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL s14mitc_init(
    TMWSCTR *pSector);
   
  /* function: s14mitc_close 
   * purpose: Close MITC event processing
   * arguments:
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL s14mitc_close(
    TMWSCTR *pSector);

  /* function: s14mitc_addEvent
   * purpose: Create and queue an MITC event
   * arguments:
   *  pSector - identifies sector
   *  cot - cause of transmission
   *  ioa - information object address
   *  pBCD - pointer to 6 bytes of 12 BCD values.
   *  sequenceAndQuality - sequence number and quality bits
   *   I14DEF_BCR_SEQ_MASK  value 0 through 31  
   *   OR in the following bits to indicate quality:
   *    I14DEF_BCR_CY  counter has overflowed 
   *    I14DEF_BCR_CA  counter was adjusted 
   *    I14DEF_BCR_IV  counter reading is invalid  
   *  pTimeStamp - time of event
   *   if pTimeStamp != TMWDEFS_NULL Event will be sent using the ASDU  specified 
   *   by sector config mitcTimeFormat. Either 240 without time or 241 with time
   *    To set RES1 bit to indicate Substituted time, 
   *     set pTimeStamp->genuineTime to TMWEFS_FALSE and make sure
   *     S14DATA_SUPPORT_GENUINE_TIME is defined in s14data.h
   *   if pTimeStamp == TMWDEFS_NULL the event will be sent using ASDU 240 I14DEF_TYPE_MITNC1
   * returns:
   *  void * - non-NULL value indicates success; TMWDEFS_NULL indicates failure
   */
  TMWDEFS_SCL_API void * TMWDEFS_GLOBAL s14mitc_addEvent(
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot,
    TMWTYPES_ULONG ioa, 
    TMWTYPES_UCHAR *pBCD,
    TMWTYPES_UCHAR sequenceAndQuality, 
    TMWDTIME *pTimeStamp);
 
  /* function: s14mitc_countEvents
   * purpose: count MITC events
   * arguments:
   *  pSector - identifies sector
   * returns:
   *  number of events in queue
   */
  TMWTYPES_USHORT TMWDEFS_GLOBAL s14mitc_countEvents(
    TMWSCTR *pSector);
 
  /* function: s14mitc_scanForChanges
   * purpose: Scans slave database for changes to MITC data points. If scanning
   *  is enabled and an MITC point has changed an event will be added to the MITC event 
   *  queue.
   * arguments:
   *  pSector - identifies sector
   * returns:
   *  TMWDEFS_TRUE if change(s) have been found and event(s) have been added to queue
   *  TMWDEFS_FALSE otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14mitc_scanForChanges(
    TMWSCTR *pSector);

  /* function: s14mitc_processEvents
   * purpose: Process MITC events into a message
   * arguments:
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   *  pEventTime - returns time of earliest event if a clock sync is required
   * returns:
   *  S14EVNT_SENT                - ASDU was sent
   *  S14EVNT_CLOCK_SYNC_REQUIRED - Spontaneous CCSNA is required
   *  S14EVNT_NOT_SENT            - No ASDU was sent
   */
  S14EVNT_STATUS TMWDEFS_GLOBAL s14mitc_processEvents(
    TMWSCTR *pSector,
    TMWDTIME *pEventTime);

  /* function: s14mitc_processDblTransEvents
   * purpose: Process Double Transmission high priority events into a message
   * arguments:
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   * returns:
   *  S14EVNT_SENT                - ASDU was sent
   *  S14EVNT_NOT_SENT            - No ASDU was sent
   */
  S14EVNT_STATUS TMWDEFS_GLOBAL s14mitc_processDblTransEvents(
    TMWSCTR *pSector);

  /* function: s14mitc_readIntoResponse 
   * purpose: Read the MITC values for the point specified and store in CRDNA
   *  response message to be sent to master
   * arguments:
   *  pTxData - pointer
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   *  cot - cause of transmission
   *  groupMask - mask specifying what group(s) points should be in
   *  ioa - information object address
   *  pPointIndex - pointer to index of point to be read. This should be updated
   *   by this function.
   * returns:
   *  void
   */
  S14DBAS_GROUP_STATUS TMWDEFS_CALLBACK s14mitc_readIntoResponse(
    TMWSESN_TX_DATA *pTxData, 
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot, 
    TMWDEFS_GROUP_MASK groupMask, 
    TMWTYPES_ULONG ioa,
    TMWTYPES_USHORT *pPointIndex);

  /* function: s14mitc_readIntoCCINAResponse 
   * purpose: Read the MITC values for the point specified and store in CCINA 
   *  response message to be sent to master
   * arguments:
   *  pTxData - pointer
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   *  cot - cause of transmission
   *  groupMask - mask specifying what group(s) points should be in
   *  ioa - information object address
   *  pPointIndex - pointer to index of point to be read. This should be updated
   *   by this function.
   * returns:
   *  void
   */
  S14DBAS_GROUP_STATUS TMWDEFS_CALLBACK s14mitc_readIntoCCINAResponse(
    TMWSESN_TX_DATA *pTxData, 
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot, 
    TMWDEFS_GROUP_MASK groupMask, 
    TMWTYPES_ULONG ioa,
    TMWTYPES_USHORT *pPointIndex);

#ifdef __cplusplus
}
#endif
#endif /* S14MITC_DEFINED */
