/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14mpsna.h
 * description: IEC 60870-5-101/104 slave packed single point information
 *  with status change detection support.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14mpsna.h"
#include "tmwscl/i870/s14mem.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14evnt.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/i870util.h"
#include "tmwscl/utils/tmwtarg.h"

#if S14DATA_SUPPORT_MPS

/* Forward declaration */
static void TMWDEFS_LOCAL _initEventDesc(
  TMWSCTR *pSector,
  S14EVNT_DESC *pDesc);

/* function: _internalAddEvent
 * purpose: Internal function called by SCL to add an event to the queue.
 *  This will not tell the link layer to ask the application for a message
 *  to send. 
 * arguments:
 *  pSector - pointer to sector structure returned by s14sctr_openSector
 *  cot - cause of transmission
 *  ioa - Information object address
 *  scd -
 *  qds -
 *  pTimeStamp - pointer to time structures
 * returns:
 *  void *
 */
static void * TMWDEFS_LOCAL _internalAddEvent(
  TMWSCTR *pSector, 
  TMWTYPES_UCHAR cot,
  TMWTYPES_ULONG ioa, 
  TMWTYPES_ULONG scd, 
  TMWTYPES_UCHAR qds, 
  TMWDTIME *pTimeStamp)
{
  S14EVNT_DESC desc;
  S14MPSNA_EVENT *pEvent;

  _initEventDesc(pSector, &desc);

  pEvent = (S14MPSNA_EVENT *)s14evnt_addEvent(pSector, cot, ioa, qds, pTimeStamp, &desc, TMWDEFS_NULL);

  if(pEvent != TMWDEFS_NULL)
  {
    pEvent->scd = scd; 
  }

  return(pEvent);
}

/* function: _changedFunc 
 * purpose: Determine if the specified point has changed and if so
 *  get the current values and add an event to the queue
 * arguments:
 *  pSector - identifies sector
 * returns:
 *  TMWDEFS_TRUE if an event was added
 *  TMWDEFS_FALSE otherwise
 */
static TMWTYPES_BOOL TMWDEFS_CALLBACK _changedFunc(
  TMWSCTR *pSector,
  void *pPoint, 
  TMWDTIME *pTimeStamp)
{  
  TMWDEFS_CHANGE_REASON reason;
  TMWTYPES_ULONG scd;
  TMWTYPES_UCHAR qds;

  if(s14data_mpsnaChanged(pPoint, &scd, &qds, &reason))
  { 
    if(_internalAddEvent(pSector, s14evnt_reasonToCOT(reason), 
      s14data_mpsnaGetInfoObjAddr(pPoint), scd, qds, 
      pTimeStamp) != TMWDEFS_NULL)
    {
      return(TMWDEFS_TRUE);
    }
  }
  return(TMWDEFS_FALSE);
}

/* function: _eventData 
 * purpose: Insert data from event structure into message to be sent
 * arguments:
 *  pTxData - pointer to transmit data structure
 *  pEvent - pointer to event to be put into message
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _eventData(
  TMWSESN_TX_DATA *pTxData, 
  S14EVNT *pEvent)
{
  S14MPSNA_EVENT *pMPSNAEvent = (S14MPSNA_EVENT *)pEvent;

  tmwtarg_store32(&pMPSNAEvent->scd, &pTxData->pMsgBuf[pTxData->msgLength]);
  pTxData->msgLength += 4;

  pTxData->pMsgBuf[pTxData->msgLength++] = pEvent->quality;
}

/* function: _initEventDesc 
 * purpose: Initialize event descriptor
 * arguments:
 *  pSector - identifies sector
 *  pDesc - pointer to descriptor structure 
 * returns:
 *  void
 */
static void TMWDEFS_LOCAL _initEventDesc(
  TMWSCTR *pSector,
  S14EVNT_DESC *pDesc)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  pDesc->typeId = I14DEF_TYPE_MPSNA1;
  pDesc->woTimeTypeId = I14DEF_TYPE_MPSNA1;
  pDesc->cotSpecified = 0;
  pDesc->eventMemType = S14MEM_MPSNA_EVENT_TYPE;
  pDesc->pEventList = &p14Sector->mpsnaEvents;
  pDesc->eventMode = p14Sector->mpsnaEventMode;
  pDesc->scanEnabled = p14Sector->mpsnaScanEnabled;
  pDesc->maxEvents = p14Sector->mpsnaMaxEvents;
  pDesc->timeFormat = TMWDEFS_TIME_FORMAT_NONE;
  pDesc->doubleTransmission = TMWDEFS_FALSE;
  pDesc->pEventsOverflowedFlag = &p14Sector->mpsnaEventsOverflowed;
  pDesc->pChangedFunc = _changedFunc;
  pDesc->pGetPointFunc = s14data_mpsnaGetPoint;
  pDesc->pEventDataFunc = _eventData;
}

/* function: _storeInResponse */
static void TMWDEFS_CALLBACK _storeInResponse(
  TMWSESN_TX_DATA *pTxData, 
  void *pPoint,
  TMWDEFS_TIME_FORMAT timeFormat)
{
  TMWTYPES_ULONG scd;
  TMWTARG_UNUSED_PARAM(timeFormat);

  scd = s14data_mpsnaGetValue(pPoint);
  tmwtarg_store32(&scd, &pTxData->pMsgBuf[pTxData->msgLength]);
  pTxData->msgLength += 4;

  pTxData->pMsgBuf[pTxData->msgLength++] = s14data_mpsnaGetFlags(pPoint);
}

/* function: s14mpsna_init */
void TMWDEFS_GLOBAL s14mpsna_init(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  tmwdlist_initialize(&p14Sector->mpsnaEvents);
  p14Sector->mpsnaEventsOverflowed = TMWDEFS_FALSE;
}

/* function: s14mpsna_close */
void TMWDEFS_GLOBAL s14mpsna_close(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  tmwdlist_destroy(&p14Sector->mpsnaEvents, s14mem_free);
} 

/* function: s14mpsna_addEvent */
void * TMWDEFS_GLOBAL s14mpsna_addEvent(
  TMWSCTR *pSector, 
  TMWTYPES_UCHAR cot,
  TMWTYPES_ULONG ioa, 
  TMWTYPES_ULONG scd, 
  TMWTYPES_UCHAR qds, 
  TMWDTIME *pTimeStamp)
{
  void *pEvent;
#if TMWCNFG_SUPPORT_THREADS
  TMWDEFS_RESOURCE_LOCK *pLock = &pSector->pSession->pChannel->lock;
#endif

  TMWTARG_LOCK_SECTION(pLock);

  pEvent = _internalAddEvent(pSector, cot, ioa, scd, qds, pTimeStamp);

  if(pEvent != TMWDEFS_NULL)
  {
    /* If an event was added tell link layer we have data */ 
    s14event_linkDataReady(pSector);
  }

  TMWTARG_UNLOCK_SECTION(pLock);
  return(pEvent);
}

/* function: s14mpsna_countEvents */
TMWTYPES_USHORT TMWDEFS_GLOBAL s14mpsna_countEvents(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  return(s14evnt_countEvents(pSector, &p14Sector->mpsnaEvents));
}

/* function: s14mpsna_scanForChanges */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14mpsna_scanForChanges(
  TMWSCTR *pSector)
{
  S14EVNT_DESC desc;

  _initEventDesc(pSector, &desc);

  return(s14evnt_scanForChanges(pSector, &desc));
}

/* function: s14mpsna_processEvents */
S14EVNT_STATUS TMWDEFS_GLOBAL s14mpsna_processEvents(
  TMWSCTR *pSector,
  TMWDTIME *pEventTime)
{ 
  S14EVNT_DESC desc;

  _initEventDesc(pSector, &desc);
  return(s14evnt_processEvents(pSector, &desc, 5, pEventTime, TMWDEFS_FALSE)); 
}

/* function: s14mpsna_readIntoResponse */
S14DBAS_GROUP_STATUS TMWDEFS_CALLBACK s14mpsna_readIntoResponse(
  TMWSESN_TX_DATA *pTxData, 
  TMWSCTR *pSector, 
  TMWTYPES_UCHAR cot, 
  TMWDEFS_GROUP_MASK groupMask, 
  TMWTYPES_ULONG ioa,
  TMWTYPES_USHORT *pPointIndex)
{
  return(s14util_readIntoResponse(pTxData, pSector, cot, 
    groupMask, ioa, I14DEF_TYPE_MPSNA1, TMWDEFS_TIME_FORMAT_NONE, 5, pPointIndex, s14data_mpsnaGetPoint, 
    s14data_mpsnaGetGroupMask, s14data_mpsnaGetInfoObjAddr, s14data_mpsnaGetIndexed, TMWDEFS_NULL,
    _storeInResponse));
}

#endif /* S14DATA_SUPPORT_MPS */
