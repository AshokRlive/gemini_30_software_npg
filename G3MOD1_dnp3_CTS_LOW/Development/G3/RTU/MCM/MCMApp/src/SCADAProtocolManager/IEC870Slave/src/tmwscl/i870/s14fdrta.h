/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14fdrta.h
 * description: This file is intended for internal SCL use only. 
 *  IEC 60870-5-101 slave FLSNA (File Transfer - Directory) functionality.
 */
#ifndef S14FDRTA_DEFINED
#define S14FDRTA_DEFINED

#include "tmwscl/i870/s14sesn.h"
#include "tmwscl/i870/s14sctr.h"
#include "tmwscl/i870/s14util.h"

#ifdef __cplusplus
extern "C" {
#endif

  void TMWDEFS_GLOBAL s14fdrta_init(
    TMWSCTR *pSector);
  
  void TMWDEFS_GLOBAL s14fdrta_close(
    TMWSCTR *pSector);

  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL s14fdrta_addEvent(
    TMWSCTR *pSector);

  TMWTYPES_BOOL TMWDEFS_GLOBAL s14fdrta_scanForChanges(
    TMWSCTR *pSector);

  /* There is no s14fdrta_countEvents() or s14fdrta_processEvents()
   * FDRTA events will not be put in event queue. 
   * They will just set fileFdrtaCOT which will cause 
   * directory FDRTA ASDU to be sent 
   */

#ifdef __cplusplus
}
#endif
#endif /* S14FDRTA_DEFINED */
