/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i870sctr.h
 * description: This file is intended for internal SCL use only.
 *  IEC 60870-5 sector
 */
#ifndef I870SCTR_DEFINED
#define I870SCTR_DEFINED

#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/utils/tmwscl.h"
#include "tmwscl/utils/tmwsctr.h"
#include "tmwscl/utils/tmwsesn.h"
#include "tmwscl/i870/i870util.h"

/* Sector info callback function */
typedef void (*I870SCTR_INFO_FUNC)(TMWSCTR *pSector, TMWSCL_INFO info);

/* I870 Sector Context */
typedef struct I870SectorStruct {

  /* Generic TMW sector, must be first field */
  TMWSCTR tmw;

  /* Configuration */
  TMWTYPES_USHORT asduAddress;

  /* Cyclic timer expired while sending previous cycle */
  TMWTYPES_BOOL cyclicPending;

  /* Database handle for this sector */
  void *pDbHandle;

  I870SCTR_INFO_FUNC  pProcessInfoFunc; 
 
} I870SCTR;

#ifdef __cplusplus
extern "C" {
#endif

  /* function: i870sctr_openSector */
  TMWTYPES_BOOL TMWDEFS_GLOBAL i870sctr_openSector(
    TMWSCTR *pSector, 
    TMWSESN *pSession, 
    TMWSCTR_STAT_CALLBACK pCallback,
    void *pCallbackParam);

  /* function: i870sctr_closeSector */
  void TMWDEFS_GLOBAL i870sctr_closeSector(
    TMWSCTR *pSector);

#ifdef __cplusplus
}
#endif
#endif /* I870SCTR_DEFINED */
