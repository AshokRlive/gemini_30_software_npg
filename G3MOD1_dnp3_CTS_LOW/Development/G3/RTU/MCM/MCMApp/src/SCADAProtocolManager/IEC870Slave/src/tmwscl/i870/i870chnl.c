/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i870chnl.c
 * description: IEC 60870-5  master and slave channel code
 *   used by 101, (102 when implemented), 103 and 104
 */

/* Misc Header Files */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/utils/tmwtarg.h"
#include "tmwscl/utils/tmwscl.h"
#include "tmwscl/utils/tmwmem.h"
#include "tmwscl/i870/i870chnl.h"
#include "tmwscl/i870/i870mem.h"
#include "tmwscl/i870/i870sesn.h"
#include "tmwscl/i870/i870stat.h"


/* function: _removeRequest
 * purpose: Remove request from message queue.
 * arguments:
 *  pTxData - pointer to Tx Data structure
 * returns:
 *  void
 */
static void TMWDEFS_LOCAL _removeRequest(
  TMWSESN_TX_DATA *pTxData)
{ 
  /* Delete request from message queue */
  tmwdlist_removeEntry(&pTxData->pChannel->messageQueue, (TMWDLIST_MEMBER *)pTxData);
  
  if(((I870CHNL_TX_DATA *)pTxData)->state == I870CHNL_BLOCKED)
  {
    pTxData->pChannel->numberQueued--;
  }
  
  /* If nothing left in queue cancel incremental timer */
  if(tmwdlist_size(&pTxData->pChannel->messageQueue) == 0)
  {
    tmwtimer_cancel(&pTxData->pChannel->incrementalTimer);
  }
}

static TMWTYPES_BOOL TMWDEFS_LOCAL _destAndTypeMatch(
  TMWSESN_TX_DATA *pRequest,
  TMWSESN_TX_DATA *pTxData)
{ 
  if(pRequest->pChannel != pTxData->pChannel)
    return TMWDEFS_FALSE;

  /* Take care that identical broadcast and device specific commands are prevented
   * Can't check for null session, since we use pointer to first master session for
   * configuration values
   */
  if((!((pRequest->destAddress == I870CHNL_BROADCAST_ADDRESS)
    || (pTxData->destAddress == I870CHNL_BROADCAST_ADDRESS)))
    &&(pRequest->pSession != pTxData->pSession))
    return TMWDEFS_FALSE;

  /* Take care that identical broadcast and device specific commands are prevented */
  if((!((pRequest->pSector == TMWDEFS_NULL)
     || (pTxData->pSector == TMWDEFS_NULL)))
     &&(pRequest->pSector != pTxData->pSector))
    return TMWDEFS_FALSE;

  /* Check for duplicate ASDU type */
  if(pRequest->pMsgBuf[0] != pTxData->pMsgBuf[0])
    return TMWDEFS_FALSE;

  /* If either of these requests requires a response, don't allow it to be queued */
  if((pRequest->txFlags & TMWSESN_TXFLAGS_NO_RESPONSE ) &&
      (pTxData->txFlags & TMWSESN_TXFLAGS_NO_RESPONSE)) 
    return TMWDEFS_FALSE;

  /* Address and asdu type match */
  return TMWDEFS_TRUE;
}

/* function: _incrementalTimeout
 * purpose: Handle channel layer timeout
 * arguments:
 *  pCallbackParam - pointer to Tx Data structure
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _incrementalTimeout(
  void *pCallbackParam)
{
  TMWSESN_TX_DATA *pTxData;
  TMWCHNL *pChannel = (TMWCHNL *)pCallbackParam;

  /* Diagnostics */
  I870DIAG_ERROR(pChannel, TMWDEFS_NULL, TMWDEFS_NULL, I870DIAG_INCREMENTAL);

  /* Remove all of the requests from the queue, indicating failure */
  while((pTxData = (TMWSESN_TX_DATA *)tmwdlist_getFirst(&pChannel->messageQueue)) != TMWDEFS_NULL)
  {
    TMWSESN_STAT_CALLBACK_FUNC(pTxData->pSession, TMWSESN_STAT_REQUEST_TIMEOUT, TMWDEFS_NULL);
    i870chnl_cleanupMessage(pTxData, TMWDEFS_NULL, I870CHNL_RESP_STATUS_TIMEOUT, 0);
  } 

  pChannel->numberQueued = 0;
}

/* function: _responseTimeout
 * purpose: Handle application layer timeout
 * arguments:
 *  pCallbackParam - pointer to Tx Data structure
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _responseTimeout(
  void *pCallbackParam)
{
  TMWSESN_TX_DATA *pTxData =(TMWSESN_TX_DATA *)pCallbackParam;

  /* Diagnostics */
  I870DIAG_ERROR_MSG(pTxData->pChannel, pTxData->pSession, pTxData->pSector, I870DIAG_RESPONSE_TO, (TMWTYPES_CHAR *)pTxData->pMsgDescription);

  i870chnl_cleanupMessage(pTxData, TMWDEFS_NULL, I870CHNL_RESP_STATUS_TIMEOUT, 0);
}

/* function i870chnl_beforeTxCallback */
void TMWDEFS_GLOBAL i870chnl_beforeTxCallback(
  TMWSESN_TX_DATA *pTxData)
{
  TMWSESN *pSession = pTxData->pSession;

  /* Call session specific before transmit routine */
  if(pSession != TMWDEFS_NULL)
  {
    I870SESN *pI870Session = (I870SESN *)pSession;

    if(pI870Session->pBeforeTxCallback != TMWDEFS_NULL)
      pI870Session->pBeforeTxCallback(pTxData);
  }
}

/* function i870chnl_initConfig */
void TMWDEFS_GLOBAL i870chnl_initConfig(
 I870CHNL_CONFIG *pConfig)
{
  pConfig->incrementalTimeout      = TMWDEFS_SECONDS(30);
  pConfig->maxQueueSize            = 0;
  pConfig->pStatCallback           = TMWDEFS_NULL;
  pConfig->pStatCallbackParam      = TMWDEFS_NULL;
  pConfig->pIdleCallback           = TMWDEFS_NULL;
  pConfig->pIdleCallbackParam      = TMWDEFS_NULL;
}

/* function i870chnl_openConfig */
void TMWDEFS_GLOBAL i870chnl_openChannel(
  TMWCHNL *pChannel,
  const I870CHNL_CONFIG *pConfig)
{
  pChannel->maxQueueSize = pConfig->maxQueueSize;
  pChannel->incrementalTimeout = pConfig->incrementalTimeout;
  tmwtimer_init(&pChannel->incrementalTimer);
}

/* function: i870chnl_getChannelConfig */
TMWTYPES_BOOL TMWDEFS_GLOBAL i870chnl_getChannelConfig(
  TMWCHNL *pChannel,
  I870CHNL_CONFIG  *pConfig,
  TMWPHYS_CONFIG *pPhysConfig)
{
  pConfig->incrementalTimeout  = pChannel->incrementalTimeout;
  pConfig->maxQueueSize        = pChannel->maxQueueSize;
  pConfig->pStatCallback       = pChannel->pStatCallbackFunc;
  pConfig->pStatCallbackParam  = pChannel->pStatCallbackParam;
  pConfig->pIdleCallback       = pChannel->pIdleCallbackFunc;
  pConfig->pIdleCallbackParam  = pChannel->pIdleCallbackParam;

  tmwphys_getChannelConfig(pChannel, pPhysConfig);

  return(TMWDEFS_TRUE);
}

/* function: i870chnl_setChannelConfig */
TMWTYPES_BOOL TMWDEFS_GLOBAL i870chnl_setChannelConfig(
  TMWCHNL *pChannel,
   const I870CHNL_CONFIG *pConfig,
   const TMWPHYS_CONFIG *pPhysConfig)
{
  pChannel->incrementalTimeout  = pConfig->incrementalTimeout;
  pChannel->maxQueueSize        = pConfig->maxQueueSize;
  pChannel->pStatCallbackFunc   = pConfig->pStatCallback;
  pChannel->pStatCallbackParam  = pConfig->pStatCallbackParam;
  pChannel->pIdleCallbackFunc   = pConfig->pIdleCallback;
  pChannel->pIdleCallbackParam  = pConfig->pIdleCallbackParam;
  
  tmwphys_setChannelConfig(pChannel, pPhysConfig);

  return(TMWDEFS_TRUE);
}

/* function i870chnl_modifyChannel */
TMWTYPES_BOOL TMWDEFS_GLOBAL i870chnl_modifyChannel(
  TMWCHNL *pChannel,
  const I870CHNL_CONFIG *pConfig,
  TMWTYPES_ULONG configMask)
{
  if(configMask & I870CHNL_CONFIG_TIMEOUT)
  {
    pChannel->incrementalTimeout = pConfig->incrementalTimeout;
  }

  return(TMWDEFS_TRUE);
}

/* function i870chnl_closeChannel */
void TMWDEFS_GLOBAL i870chnl_closeChannel(
  TMWCHNL *pChannel)
{
  I870CHNL_TX_DATA *pTxData;
  TMWAPPL *pApplContext = pChannel->pApplContext;

  /* Lock application list of channels */
  TMWTARG_LOCK_SECTION(&pApplContext->lock);

  /* Lock channel */
  TMWTARG_LOCK_SECTION(&pChannel->lock);

  /* Cancel timer */
  tmwtimer_cancel(&pChannel->incrementalTimer);

  /* Remove any Tx Datas left on the queue, cancelling the responseTimers */
  while((pTxData = (I870CHNL_TX_DATA *)tmwdlist_getFirst(&pChannel->messageQueue)) != TMWDEFS_NULL)
  {
    tmwtimer_cancel(&pTxData->tmw.responseTimer);
    tmwdlist_removeEntry(&pChannel->messageQueue, (TMWDLIST_MEMBER *)pTxData);
    i870chnl_freeTxData((TMWSESN_TX_DATA *)pTxData);
  }

  /* Delete the channel from the list of channels */
  tmwdlist_removeEntry(&pApplContext->channels, (TMWDLIST_MEMBER *)pChannel);

  /* Delete the physical channel */
  tmwphys_deleteChannel(pChannel);

  /* Unlock channel */
  TMWTARG_UNLOCK_SECTION(&pChannel->lock);
  TMWTARG_UNLOCK_SECTION(&pApplContext->lock);

  /* Cleanup channel structure, including deleting the lock */
  tmwchnl_deleteChannel(pChannel);

  /* Free the channel data structure */
  tmwmem_free(pChannel);
}

/* function i870chnl_afterTxCallback */
void TMWDEFS_GLOBAL i870chnl_afterTxCallback(
  TMWSESN_TX_DATA *pTxData)
{
  TMWCHNL *pChannel = pTxData->pChannel;
  TMWSESN *pSession = pTxData->pSession;

  ((I870CHNL_TX_DATA *)pTxData)->state = I870CHNL_AFTER_TX;

  /* If no response expected cleanup */
  if((pTxData->txFlags & TMWSESN_TXFLAGS_NO_RESPONSE) == 0)
  {
    /* We are expecting a response, start timer if required */
    if(pChannel->incrementalTimeout != 0)
    {
      tmwtimer_start(&pChannel->incrementalTimer,
        pChannel->incrementalTimeout, pChannel,
        _incrementalTimeout, pChannel);
    }
  }

  /* Call session specific after transmit routine */
  if(pSession != TMWDEFS_NULL)
  {
    I870SESN *pI870Session = (I870SESN *)pSession;

    if(pI870Session->pAfterTxCallback != TMWDEFS_NULL)
      pI870Session->pAfterTxCallback(pTxData);
  }
}

/* function i870chnl_failedTxCallback */
void TMWDEFS_GLOBAL i870chnl_failedTxCallback(
  TMWSESN_TX_DATA *pTxData)
{
  TMWSESN *pSession = pTxData->pSession;
  
  if(((I870CHNL_TX_DATA *)pTxData)->state == I870CHNL_SENT)
  {
    /* Diagnostics */
    I870DIAG_ERROR_MSG(pTxData->pChannel, pTxData->pSession, pTxData->pSector, I870DIAG_FAILED_TX, (TMWTYPES_CHAR *)pTxData->pMsgDescription);
    ((I870CHNL_TX_DATA *)pTxData)->state = I870CHNL_AFTER_TX;
  }

  if(pSession != TMWDEFS_NULL)
  {
    I870SESN *pI870Session = (I870SESN *)pSession;
    if(pI870Session->pFailedTxCallback != TMWDEFS_NULL)
      pI870Session->pFailedTxCallback(pTxData);
  }
}

/* function: i870chnl_infoCallback */
void TMWDEFS_GLOBAL i870chnl_infoCallback(
  void *pParam,
  TMWSESN *pSession,
  TMWSCL_INFO sesnInfo)
{
  TMWTARG_UNUSED_PARAM(pParam);

  if(sesnInfo == TMWSCL_INFO_OFFLINE)
  {
    /* Diagnostics */
    I870DIAG_SESSION_ONLINE(pSession, TMWDEFS_FALSE);

    /* Session is offline */
    tmwsesn_setOnline(pSession, TMWDEFS_FALSE);
  }
  else if(sesnInfo == TMWSCL_INFO_ONLINE)
  {
    /* Diagnostics */
    I870DIAG_SESSION_ONLINE(pSession, TMWDEFS_TRUE);

    /* Session is online */
    tmwsesn_setOnline(pSession, TMWDEFS_TRUE);
  }

  if(pSession != TMWDEFS_NULL)
  {
    I870SESN *pI870Session = (I870SESN *)pSession;

    if(pI870Session->pProcessInfoFunc != TMWDEFS_NULL)
      pI870Session->pProcessInfoFunc(pSession, sesnInfo);
  }
}


/* function: i870chnl_parseFrameCallback */
void TMWDEFS_GLOBAL i870chnl_parseFrameCallback(
  void *pParam,
  TMWSESN *pSession,
  TMWSESN_RX_DATA *pRxFrame)
{
  TMWCHNL *pChannel = (TMWCHNL *)pSession->pChannel;
  I870SESN *pI870Session = (I870SESN *)pSession;

  TMWTARG_UNUSED_PARAM(pParam);
  /* Update stats */
  TMWSESN_STAT_CALLBACK_FUNC(pSession,
    TMWSESN_STAT_ASDU_RECEIVED, TMWDEFS_NULL);

  if(pI870Session->pParseFrameCallbackFunc != TMWDEFS_NULL)
    pI870Session->pParseFrameCallbackFunc(pSession, pRxFrame, pI870Session->pSesnFuncTable);

  /* Restart incremental timeout since we got something from this session */
  if((pChannel->incrementalTimeout != 0)
    && tmwtimer_isActive(&pChannel->incrementalTimer))
  {
    tmwtimer_start(&pChannel->incrementalTimer,
      pChannel->incrementalTimeout, pChannel,
      _incrementalTimeout, pChannel);
  }
}

/* function: i870chnl_checkDataAvailable */
TMWTYPES_BOOL TMWDEFS_GLOBAL i870chnl_checkDataAvailable(
  void *pParam,
  TMWSESN *pSession,
  TMWDEFS_CLASS_MASK classMask,
  TMWTYPES_BOOL buildMessage)
{
  I870SESN *pI870Session = (I870SESN *)pSession;
  TMWCHNL *pChannel = (TMWCHNL *)pSession->pChannel;
  I870CHNL_TX_DATA *pTxData = TMWDEFS_NULL;

  TMWTARG_UNUSED_PARAM(pParam);

#ifdef TMW_SUPPORT_MONITOR
  /* If in analyzer or listen only mode, deallocate message and return success */
  if(pChannel->pPhysContext->monitorMode)
  {
    return(TMWDEFS_FALSE);
  }
#endif

  /* Slave will register a function to be called to determine if response is needed.
   * if buildMessage is TRUE it will build a single response and queue it briefly.
   * Master doesn't have to have a pCheckDataAvailable function since it will build a request
   * and queue it as needed.
   */
  if(pI870Session->pCheckDataAvailableFunc != TMWDEFS_NULL)
  {
    TMWTYPES_BOOL retStatus = pI870Session->pCheckDataAvailableFunc(pSession, classMask, buildMessage, pI870Session->pSesnFuncTable);
    if(buildMessage == TMWDEFS_FALSE)
    {
      return(retStatus);
    }
  }

  /* For both master and slave implementations this sends a message if it is queued */
  while((pTxData = (I870CHNL_TX_DATA *)tmwdlist_getAfter(
    &pChannel->messageQueue, (TMWDLIST_MEMBER *)pTxData)) != TMWDEFS_NULL)
  {

    /* if request was blocked because of matching ASDU type and address 
     * see if it is still blocked 
     */
    if(pTxData->state == I870CHNL_BLOCKED)
    {
      TMWTYPES_BOOL match = TMWDEFS_FALSE;
      I870CHNL_TX_DATA *pRequest = TMWDEFS_NULL;

      while((pRequest = (I870CHNL_TX_DATA *)tmwdlist_getAfter(
        &pChannel->messageQueue, (TMWDLIST_MEMBER *)pRequest)) != TMWDEFS_NULL)
      {
        if(pRequest->state != I870CHNL_BLOCKED)
        {
          if(_destAndTypeMatch((TMWSESN_TX_DATA *)pRequest, (TMWSESN_TX_DATA *)pTxData))
          {
            /* Still blocked */
            match = TMWDEFS_TRUE;
            break;
          }
        }
      }
      if(!match)
      { 
        pChannel->numberQueued--;
        pTxData->state = I870CHNL_NOT_SENT;
      }
    }

    if(pTxData->state == I870CHNL_NOT_SENT)
    {
      if(pTxData->tmw.pSession == pSession)
      {
        I870SESN *pI870Session = (I870SESN *)pSession;
        if(pI870Session->pPrepareMessageFunc != TMWDEFS_NULL)
        {
          pI870Session->pPrepareMessageFunc((TMWSESN_TX_DATA *)pTxData);
        }

        pTxData->state = I870CHNL_SENT;

        /* Send frame to link layer */
        pSession->pChannel->pLink->pLinkTransmit(
          pSession->pChannel->pLinkContext, (TMWSESN_TX_DATA *)pTxData);

        /* Update stats */
        TMWSESN_STAT_CALLBACK_FUNC(pSession,
          TMWSESN_STAT_ASDU_SENT, TMWDEFS_NULL);

        return(TMWDEFS_TRUE);
      }
    }
  }
  return(TMWDEFS_FALSE);
}

static TMWTYPES_BOOL TMWDEFS_LOCAL _isDuplicateMsg(
  TMWSESN_TX_DATA *pRequest,
  TMWSESN_TX_DATA *pTxData)
{
  int i;
  /* For 103 several commands have a RII or SCN byte that 
   * changes for each request. Ignore that byte when doing 
   * the comparison 
   */
  TMWTYPES_USHORT ignoreByte = 0xffff;
  if(pRequest->pSession->protocol == TMWTYPES_PROTOCOL_103)
  {
    switch (pRequest->pMsgBuf[0]) {
      case 7:  /* I3DEF_TYPE_GI        */
      case 10 :/* I3DEF_TYPE_GNRC_DATA */
      case 21: /* I3DEF_TYPE_GNRC_CMD  */
        ignoreByte = 6;
        break;
      case 20: /* I3DEF_TYPE_GNRL_CMD */
        ignoreByte = 7;
        break;
    }
  }

  /* Address and ASDU type match, 
   * Check for byte for byte duplicate ASDU
   */
  for(i=0; i < pRequest->msgLength; i++)
  {
    if(pRequest->pMsgBuf[i] != pTxData->pMsgBuf[i])
    {
      if(i != ignoreByte)
      {
        return TMWDEFS_FALSE;
      }
    }
  }

  return TMWDEFS_TRUE;
}

static TMWTYPES_BOOL TMWDEFS_LOCAL _insertBlockedMsg(
  TMWCHNL *pChannel,
  TMWSESN_TX_DATA *pTxData)
{
  I870CHNL_TX_DATA *pRequest = TMWDEFS_NULL;

  if(pChannel->numberQueued >= pChannel->maxQueueSize)
  {
    I870DIAG_ERROR(pChannel, pTxData->pSession, pTxData->pSector, I870DIAG_SAME_TYPE);
    return TMWDEFS_FALSE;
  }

  /* Determine if this is a byte for byte duplicate of a request that is blocked
   * if so, remove the old request that was queued.
   * Keep the new one, so that the order is correct.
   * We don't want to look at ones which were not blocked since it is too late for those.
   */
  while((pRequest = (I870CHNL_TX_DATA *)tmwdlist_getAfter(
    &pChannel->messageQueue, (TMWDLIST_MEMBER *)pRequest)) != TMWDEFS_NULL)
  {
   
    if(pRequest->state == I870CHNL_BLOCKED)
    {
      /* First, just check the type ID and dest address */
      if(!_destAndTypeMatch((TMWSESN_TX_DATA *)pRequest, pTxData))
        continue;
 
      if(_isDuplicateMsg((TMWSESN_TX_DATA *)pRequest, pTxData))
      {
        /* remove old entry */ 
        if(pRequest->pUserCallback != TMWDEFS_NULL)
        {
          I870CHNL_RESPONSE_INFO response;

          response.last = TMWDEFS_TRUE;
          response.pSector = pRequest->tmw.pSector;
          response.pTxData = (TMWSESN_TX_DATA *)pRequest;
          response.pRxData = TMWDEFS_NULL;
          response.status = I870CHNL_RESP_STATUS_CANCELED;

          pRequest->pUserCallback(pRequest->pUserCallbackParam, &response);
        }

        I870DIAG_ERROR(pChannel, pRequest->tmw.pSession, pRequest->tmw.pSector, I870DIAG_DUPLICATE);

        tmwdlist_removeEntry(&pChannel->messageQueue, (TMWDLIST_MEMBER *)pRequest);
        i870chnl_freeTxData((TMWSESN_TX_DATA *)pRequest);

        pChannel->numberQueued--;
 
        /* Don't need to look through rest of msg queue since only one duplicate is possible. */
        break;
      }
    }
  }

  /* Add entry to queue  */
  ((I870CHNL_TX_DATA *)pTxData)->state = I870CHNL_BLOCKED;
  tmwdlist_addEntry(&pChannel->messageQueue, (TMWDLIST_MEMBER *)pTxData);
  pChannel->numberQueued++;
 
  return(TMWDEFS_TRUE);
}
 
/* function: i870chnl_sendMessage */
TMWTYPES_BOOL TMWDEFS_GLOBAL i870chnl_sendMessage(
  TMWSESN_TX_DATA *pTxData)
{
  TMWCHNL *pChannel = pTxData->pChannel;
  I870CHNL_TX_DATA *pRequest = TMWDEFS_NULL;

#ifdef TMW_SUPPORT_MONITOR
  /* If in analyzer or listen only mode, deallocate message and return success */
  if(pChannel->pPhysContext->monitorMode)
  {
    i870chnl_freeTxData(pTxData);
    return(TMWDEFS_TRUE);
  }
#endif

  /* Lock channel */
  TMWTARG_LOCK_SECTION(&pChannel->lock);

  /* Diagnostics */
  I870DIAG_INSERT_QUEUE(pChannel, pTxData->pSession, pTxData->pSector, pTxData->pMsgDescription);

  /* If duplicate requests are not allowed to be sent 
   * For test purposes this flag can be set causing us to skip this duplicate checking
   * This can be used to send a second CICNA before the first completes for example.
   */
  if((pTxData->txFlags & TMWSESN_TXFLAGS_IECDUPALLOWED) == 0)
  {
    /* Determine if this is the same ASDU type being sent to the same sector */
    while((pRequest = (I870CHNL_TX_DATA *)tmwdlist_getAfter(
      &pChannel->messageQueue, (TMWDLIST_MEMBER *)pRequest)) != TMWDEFS_NULL)
    {
      if(((I870CHNL_TX_DATA *)pRequest)->state != I870CHNL_BLOCKED)
      {
        TMWTYPES_BOOL retStatus;

        if(!_destAndTypeMatch((TMWSESN_TX_DATA *)pRequest, pTxData))
          continue;

        pTxData->timeSent = tmwtarg_getMSTime();
   
        /* Start response timer now */
        if(pTxData->responseTimeout != 0)
        {
          tmwtimer_start(&pTxData->responseTimer,
            pTxData->responseTimeout, pChannel, _responseTimeout, pTxData);
        }

        /* Put this on queue as blocked message if allowed */
        retStatus = _insertBlockedMsg(pChannel, pTxData);

        /* Unlock channel */
        TMWTARG_UNLOCK_SECTION(&pChannel->lock);

        return(retStatus);
      }
    }
  }
  /* Add entry to queue  */
  tmwdlist_addEntry(&pChannel->messageQueue, (TMWDLIST_MEMBER *)pTxData);

  pTxData->timeSent = tmwtarg_getMSTime();
 
   /* Start response timer now */
  if(pTxData->responseTimeout != 0)
  {
    tmwtimer_start(&pTxData->responseTimer,
      pTxData->responseTimeout, pChannel, _responseTimeout, pTxData);
  }

  /* Tell link layer we have something to transmit          */
  /* Slave might recurse deeply in balanced mode            */
  if(pTxData->pSession->type != TMWTYPES_SESSION_TYPE_SLAVE)
  {
    pChannel->pLink->pLinkDataReady(
      pChannel->pLinkContext, pTxData->pSession);
  }

  /* Unlock channel */
  TMWTARG_UNLOCK_SECTION(&pChannel->lock);

  return(TMWDEFS_TRUE);
}

/* function: i870chnl_cancelMessage */
TMWTYPES_BOOL TMWDEFS_GLOBAL i870chnl_cancelMessage(
  TMWSESN_TX_DATA *pTxData)
{
  I870CHNL_TX_DATA *pI870ChnlTxData = (I870CHNL_TX_DATA *)pTxData;
  TMWCHNL *pChannel = pTxData->pChannel;

  /* Diagnostics */
  I870DIAG_ERROR_MSG(pTxData->pChannel, pTxData->pSession, pTxData->pSector, I870DIAG_CANCELED, (TMWTYPES_CHAR *)pTxData->pMsgDescription);

  /* Lock channel */
  TMWTARG_LOCK_SECTION(&pChannel->lock);

  /* If the message has been sent to the link layer
   *   send a cancel to the link layer
   * else,
   *   call the user callback and remove the message from the queue and free the transmit data.
   */
  if(pI870ChnlTxData->state == I870CHNL_SENT)
  {
    /* Send cancel to link layer */
    pI870ChnlTxData->state = I870CHNL_CANCELED;
    pChannel->pLink->pLinkCancel(pChannel->pLinkContext, pTxData);
  }
  else
  {
    if(pI870ChnlTxData->pUserCallback != TMWDEFS_NULL)
    {
      I870CHNL_RESPONSE_INFO response;

      response.last = TMWDEFS_TRUE;
      response.pSector = pTxData->pSector;
      response.pTxData = pTxData;
      response.pRxData = TMWDEFS_NULL;
      response.status = I870CHNL_RESP_STATUS_CANCELED;

      pI870ChnlTxData->pUserCallback(pI870ChnlTxData->pUserCallbackParam, &response);
    }

    _removeRequest(pTxData);
    i870chnl_freeTxData(pTxData); 
  }

  /* Unlock channel */
  TMWTARG_UNLOCK_SECTION(&pChannel->lock);

  return(TMWDEFS_TRUE);
}

/* function: i870chnl_isRequestPending */
TMWTYPES_BOOL TMWDEFS_GLOBAL i870chnl_isRequestPending(
  TMWSESN *pSession)
{
  I870CHNL_TX_DATA *pTxData = TMWDEFS_NULL;

  /* Search through requests for one outstanding to this session */
  while((pTxData = (I870CHNL_TX_DATA *)tmwdlist_getAfter(
    &pSession->pChannel->messageQueue, (TMWDLIST_MEMBER *)pTxData)) != TMWDEFS_NULL)
  {
    /* If session matches and it has been sent we have a outstanding request */
    if((pTxData->tmw.pSession == pSession)
      && (pTxData->state == I870CHNL_AFTER_TX))
    {
      return(TMWDEFS_TRUE);
    }
  }

  /* No match */
  return(TMWDEFS_FALSE);
}

/* function: i870chnl_findMessage */
 TMWSESN_TX_DATA * TMWDEFS_GLOBAL i870chnl_findMessage(
  TMWSCTR *pSector,
  TMWTYPES_UCHAR typeId)
{
  TMWSESN_TX_DATA *pTxData = TMWDEFS_NULL;

  /* Search through requests for one that matches */
  while((pTxData = (TMWSESN_TX_DATA *)tmwdlist_getAfter(
    &pSector->pSession->pChannel->messageQueue, (TMWDLIST_MEMBER *)pTxData)) != TMWDEFS_NULL)
  {
    if(((I870CHNL_TX_DATA *)pTxData)->state != I870CHNL_BLOCKED)
    {
      /* If broadcast Address, this was a channel broadcast, if typeId matches it must be the correct one
       * If pTxData->pSector is NULL, this was a session broadcast request, check session 
       * If sectors match
       * and type ids are the same we have found the correct request
       */
      if(((pTxData->destAddress == 0xffff) 
        || ((pTxData->pSector == TMWDEFS_NULL) && (pTxData->pSession == pSector->pSession))
        || (pTxData->pSector == pSector))
        && (pTxData->pMsgBuf[0] == typeId))
      {
        return(pTxData);
      }
    }
  }

  /* No match */
  return(TMWDEFS_NULL);
}

TMWTYPES_ULONG _getRequestedIOA(TMWSESN_TX_DATA *pTxData)
{    
  I870SESN *pI870Session = (I870SESN *)pTxData->pSession;
  TMWTYPES_ULONG ioa = 0;

  /* determine index of IOA in request */
  TMWTYPES_ULONG index = 3;
  if(pI870Session->cotSize == 2)
    index++;
    
  index += pI870Session->asduAddrSize;

  switch(pI870Session->infoObjAddrSize)
    {
    case 1:
      ioa = pTxData->pMsgBuf[index];
      break;

    case 2:
      {
      TMWTYPES_USHORT tmpAddress;
      tmwtarg_get16(&pTxData->pMsgBuf[index], &tmpAddress);
      ioa = tmpAddress;
      break;
      }
    case 3:
      tmwtarg_get24(&pTxData->pMsgBuf[index], &ioa);
      break;
    default:
      ioa = 0;
      break;
    }
  return(ioa);
}

#if M14DATA_SUPPORT_MULTICMDS
/* function: i870chnl_findMultiCmdMessage */
 TMWSESN_TX_DATA * TMWDEFS_GLOBAL i870chnl_findMultiCmdMessage(
  TMWSCTR *pSector,
  TMWTYPES_UCHAR typeId,
  TMWTYPES_ULONG ioa)
{
  TMWSESN_TX_DATA *pTxData = TMWDEFS_NULL;

  /* Search through requests for one that matches */
  while((pTxData = (TMWSESN_TX_DATA *)tmwdlist_getAfter(
    &pSector->pSession->pChannel->messageQueue, (TMWDLIST_MEMBER *)pTxData)) != TMWDEFS_NULL)
  {
    if(((I870CHNL_TX_DATA *)pTxData)->state != I870CHNL_BLOCKED)
    {
      /* If broadcast Address, this was a channel broadcast, if typeId matches it must be the correct one
       * If pTxData->pSector is NULL, this was a session broadcast request, check session 
       * If sectors match
       * and type ids are the same we have found the correct request
       */
      if(((pTxData->destAddress == 0xffff) 
        || ((pTxData->pSector == TMWDEFS_NULL) && (pTxData->pSession == pSector->pSession))
        || (pTxData->pSector == pSector))
        && (pTxData->pMsgBuf[0] == typeId))
      {
        if(ioa == _getRequestedIOA(pTxData))
        {
          return(pTxData);
        }
      }
    }
  }

  /* No match */
  return(TMWDEFS_NULL);
}
#endif

/* function: i870chnl_cleanupMessage */
void TMWDEFS_GLOBAL i870chnl_cleanupMessage(
  TMWSESN_TX_DATA *pTxData,
  TMWSESN_RX_DATA *pRxData,
  I870CHNL_RESP_STATUS status,
  TMWTYPES_UCHAR requestStatus)
{
  I870CHNL_TX_DATA *pI870ChnlTxData = (I870CHNL_TX_DATA *)pTxData;
  TMWCHNL *pChannel = pTxData->pChannel;
  TMWTYPES_BOOL reuseTxData = TMWDEFS_FALSE;

  if((status != I870CHNL_RESP_STATUS_TIMEOUT) &&
    (--pI870ChnlTxData->numberOfResponsesExpected > 0))
  {
    /* expecting more responses to broadcast request, leave this request here */
    I870DIAG_NOT_REMOVED_MESSAGE(pTxData);
    return;
  }

  I870DIAG_REMOVE_MESSAGE(pTxData, status);

  /* Cancel application response timer */
  tmwtimer_cancel(&pTxData->responseTimer);
  _removeRequest(pTxData);

  /* Don't take session offline anymore, let link layer do that, 5/12/2004 */

  /* Cancel this if it has been sent to the link layer */
  /* but it is not finished with it                    */
  if(pI870ChnlTxData->state == I870CHNL_SENT)
  {
    /* Send cancel to link layer */
    pI870ChnlTxData->state = I870CHNL_CANCELED;
    pChannel->pLink->pLinkCancel(pChannel->pLinkContext, pTxData);
  }

  /* Call callbacks */
  if((pI870ChnlTxData->pInternalCallback != TMWDEFS_NULL)
    || (pI870ChnlTxData->pUserCallback != TMWDEFS_NULL))
  {
    I870CHNL_RESPONSE_INFO responseInfo;

    responseInfo.pSector = pTxData->pSector;

    responseInfo.status = status;
    responseInfo.last = TMWDEFS_TRUE;
    responseInfo.requestStatus = requestStatus;

    responseInfo.responseTime = tmwtarg_getMSTime() - pTxData->timeSent;

    responseInfo.pTxData = pTxData;
    responseInfo.pRxData = pRxData;

    if(pI870ChnlTxData->pInternalCallback != TMWDEFS_NULL)
    {
      reuseTxData = pI870ChnlTxData->pInternalCallback(pI870ChnlTxData->pInternalCallbackParam, &responseInfo);
      if(status == I870CHNL_RESP_STATUS_SUCCESS)
      {
        responseInfo.status = I870CHNL_RESP_STATUS_INTERMEDIATE;
        responseInfo.last = TMWDEFS_FALSE;
      }
    }

    if(pI870ChnlTxData->pUserCallback != TMWDEFS_NULL)
      pI870ChnlTxData->pUserCallback(pI870ChnlTxData->pUserCallbackParam, &responseInfo);
  }
  
  I870STAT_CHECK_STATUS(pTxData->pSession, status);

  /* reuseTxData is true if pTxData was requeued ie for an execute request */
  /* Free Tx Data Structure, and change blocked request from BLOCKED state to NOT SENT */
  if(!reuseTxData)
  {
    i870chnl_freeTxData(pTxData);
  }
}

/* function: i870chnl_deleteMessages */
void TMWDEFS_GLOBAL i870chnl_deleteMessages(
  TMWCHNL *pChannel,
  TMWSCTR *pSector)
{
  I870CHNL_TX_DATA *pI870ChnlTxData = TMWDEFS_NULL;

  /* Search through requests for any for this sector */
  while((pI870ChnlTxData = (I870CHNL_TX_DATA *)tmwdlist_getAfter(
    &pChannel->messageQueue, (TMWDLIST_MEMBER *)pI870ChnlTxData)) != TMWDEFS_NULL)
  {
    /* If sectors are the same we have a match */
    if(pI870ChnlTxData->tmw.pSector == pSector)
    {
      /* Cancel this if it has been sent to the link layer */
      /* but it is not finished with it                    */
      if(pI870ChnlTxData->state == I870CHNL_SENT)
      {
        TMWCHNL *pChannel = pI870ChnlTxData->tmw.pChannel;

        /* Send cancel to link layer */
        pI870ChnlTxData->state = I870CHNL_CANCELED;
        pChannel->pLink->pLinkCancel(pI870ChnlTxData->tmw.pChannel->pLinkContext, (TMWSESN_TX_DATA *)pI870ChnlTxData);
      }

      /* Delete request from message queue */
      _removeRequest((TMWSESN_TX_DATA *)pI870ChnlTxData);  
      
      /* Free Trans Data Structure */
      i870chnl_freeTxData((TMWSESN_TX_DATA *)pI870ChnlTxData);

      pI870ChnlTxData = TMWDEFS_NULL;
    }
  }
}

/* function: i870chnl_newTxData */
TMWSESN_TX_DATA * TMWDEFS_GLOBAL i870chnl_newTxData(
  TMWCHNL *pChannel,
  TMWSESN *pSession,
  TMWSCTR *pSector,
  TMWTYPES_USHORT bufLen)
{
  I870CHNL_TX_DATA *pI870ChnlTxData;

  if(bufLen > I870CHNL_TX_DATA_BUFFER_MAX)
  {
    I870DIAG_ERROR(pChannel, pSession, pSector, I870DIAG_BUFLEN);
    return(TMWDEFS_NULL);
  }

  pI870ChnlTxData = (I870CHNL_TX_DATA *)i870mem_alloc(I870MEM_CHNL_TX_DATA_TYPE);
  if(pI870ChnlTxData == TMWDEFS_NULL)
  {
    return(TMWDEFS_NULL);
  }

  tmwsesn_initTxData((TMWSESN_TX_DATA *)pI870ChnlTxData, pI870ChnlTxData->buffer, bufLen);

  pI870ChnlTxData->tmw.pChannel = pChannel;
  pI870ChnlTxData->tmw.pSession = pSession;
  pI870ChnlTxData->tmw.pSector  = pSector;
	pI870ChnlTxData->tmw.pMsgDescription = TMWDEFS_NULL;

  pI870ChnlTxData->tmw.destAddress = pSession->linkAddress;

  pI870ChnlTxData->tmw.responseTimeout = 0;
  tmwtimer_init(&pI870ChnlTxData->tmw.responseTimer);

  pI870ChnlTxData->numberOfResponsesExpected = 1;

  pI870ChnlTxData->state = I870CHNL_NOT_SENT;

  pI870ChnlTxData->pInternalCallback = TMWDEFS_NULL;
  pI870ChnlTxData->pInternalCallbackParam = TMWDEFS_NULL;

  pI870ChnlTxData->pUserCallback = TMWDEFS_NULL;
  pI870ChnlTxData->pUserCallbackParam = TMWDEFS_NULL;

  return((TMWSESN_TX_DATA *)pI870ChnlTxData);
}

/* function: i870chnl_reInitTxData */
void TMWDEFS_GLOBAL i870chnl_reInitTxData(
  TMWSESN_TX_DATA *pTxData)
{
  I870CHNL_TX_DATA *pI870ChnlTxData = (I870CHNL_TX_DATA *)pTxData;

  pI870ChnlTxData->pInternalCallback = TMWDEFS_NULL;
  pI870ChnlTxData->pInternalCallbackParam = TMWDEFS_NULL;
  pI870ChnlTxData->numberOfResponsesExpected = 1;
  pI870ChnlTxData->state = I870CHNL_NOT_SENT;
}

/* function: i870chnl_freeTxData */
void TMWDEFS_GLOBAL i870chnl_freeTxData(
  TMWSESN_TX_DATA *pTxData)
{
  tmwtimer_cancel(&pTxData->responseTimer);
  i870mem_free(pTxData);
}



