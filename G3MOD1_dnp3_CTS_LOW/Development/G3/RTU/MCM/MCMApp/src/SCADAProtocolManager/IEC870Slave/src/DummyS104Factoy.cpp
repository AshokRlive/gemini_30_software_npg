/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Dummy DNP3 Slave protocol manager factory implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   08/09/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "DummyS104Factory.h"
#include "S104TMWIncludes.h"

#include "S104Protocol.h"
#include "S104Channel.h"
#include "S104Session.h"
#include "S104Sector.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
# define AI_NUM  1
# define BI_NUM  1

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
static void configure(S104Channel& channel)
{
    S104Channel::S104ChannelConfig config;
    I870CHNL_CONFIG& chnlConfig = config.pChnlConfig;
    I870LNK4_CONFIG& linkConfig = config.pLinkConfig;
    TMWPHYS_CONFIG&  physConfig = config.pPhysConfig;
    TMWTARG_CONFIG&  targConfig = config.pTMWTargConfig;
    LINIO_CONFIG&    IOCnfg     = config.pIOConfig;

    /* Initialize all configuration structures to defaults
     */
    tmwtarg_initConfig(&targConfig);
    i104chnl_initConfig(&chnlConfig, &linkConfig, &physConfig);

    liniotarg_initConfig(&IOCnfg);
    IOCnfg.type = LINIO_TYPE_TCP;

    /* name displayed in analyzer window */
    strcpy(IOCnfg.linTCP.chnlName, "TEST104");

    /* *.*.*.* allows any client to connect*/
    strcpy(IOCnfg.linTCP.ipAddress, "*.*.*.*");
    //strcpy(IOCnfg.linTCP.ipAddress, "127.0.0.1");

    IOCnfg.linTCP.ipPort = 2404;
    IOCnfg.linTCP.mode = LINTCP_MODE_SERVER;

    linkConfig.isControlling = TMWDEFS_FALSE;

    channel.configure(config);
}

static void configure(S104Session& session)
{
    S104SESN_CONFIG sesnConfig;

    s104sesn_initConfig(&sesnConfig);

    session.configure(sesnConfig);

}

static void configure(S104Sector& sector)
{
    S104SCTR_CONFIG sctrConfig;

    s104sctr_initConfig(&sctrConfig);

    sector.configure(sctrConfig);

}


#if 0
static void configure(S104Database& db)
{
    IPointObserver **observerList;

    db.tmwdb = (TMWS104DatabaseStr*)calloc(1,sizeof(TMWS104DatabaseStr));

    db.tmwdb->binaryInput.size = BI_NUM;
    db.tmwdb->binaryInput.list = (TMWS104BInputStr*)calloc(AI_NUM,sizeof(TMWS104BInputStr));

    /* Allocate analogue input observer list */
    observerList = (IPointObserver**)calloc(AI_NUM, sizeof(IPointObserver*));
    db.AIObserverList.setTable(observerList, AI_NUM);

    for(lu_uint32_t i = 0; i < BI_NUM; ++i)
    {

//        pointPtr->defaultVariation = confPtr->defaultVariation;
//        pointPtr->eventDefaultVariation = confPtr->eventDefaultVariation;
//        pointPtr->enableClass0 = confPtr->enableClass0;
//        pointPtr->eventClass = confPtr->eventClass;
//        pointPtr->value = DNPDEFS_DBAS_FLAG_OFF_LINE;
//
//        /* Create observers */
//        observerList[confPtr->protocolID] = new DNP3SlaveBIObserver(confPtr->GDBId, confPtr->protocolID, db->pointDB);
//        if (GDatabase.attach(confPtr->GDBId, db->BIObserverList[confPtr->protocolID]) == GDB_ERROR_NONE)
//        {
//            pointPtr->enabled = LU_TRUE;
//        }
//        else
//        {
//            /* Invalid virtual point */
//            delete db->BIObserverList[confPtr->protocolID];
//            observerList[confPtr->protocolID] = NULL;
//        }
    }

}
#endif

S104Protocol* newS104Protocol(GeminiDatabase& db)
{
    S104Protocol* protocol;
    S104Channel*  channel;
    S104Session*  session;
    S104Sector*   sector;

    protocol    = new S104Protocol(db);
    channel     = new S104Channel(db,protocol);
    session     = new S104Session(*channel);
    sector      = new S104Sector(*session);

    configure(*channel);
    configure(*session);
    configure(*sector);

    protocol->addChannel(channel);
    channel ->addSession(session);
    session ->addSector(sector);

    return protocol;

#if 0
    TMWTARG_CONFIG targConfig;
    DNPCHNL_CONFIG chnlConfig;
    DNPTPRT_CONFIG tprtConfig;
    DNPLINK_CONFIG linkConfig;
    TMWPHYS_CONFIG physConfig;
    LINIO_CONFIG IOConfig;
    SDNPSESN_CONFIG sessionConfig;
    DNP3SlaveDB *db;
    IPointObserver **observerList;


    /* Initialize channel configuration to defaults */
    tmwtarg_initConfig(&targConfig);
    dnpchnl_initConfig(&chnlConfig, &tprtConfig, &linkConfig, &physConfig);

    /* Override default configuration using user defined parameters */
    linkConfig.rxFrameSize    = DNPLinkConfig.rxFrameSize;
    linkConfig.txFrameSize    = DNPLinkConfig.txFrameSize;
    linkConfig.rxFrameTimeout = DNPLinkConfig.rxFrameTimeout;
    linkConfig.confirmMode    = DNPLinkConfig.confirmMode;
    linkConfig.confirmTimeout = DNPLinkConfig.confirmTimeout;
    linkConfig.maxRetries     = DNPLinkConfig.maxRetries;
    linkConfig.networkType    = DNPLinkConfig.networkType;
    chnlConfig.rxFragmentSize = DNPLinkConfig.rxFragmentSize;
    chnlConfig.txFragmentSize = DNPLinkConfig.txFragmentSize;

    /* Initialize IO Config Structure
     * Call liniotarg_initConfig to initialize default values, then overwrite
     * specific values as needed.
     */
    liniotarg_initConfig(&IOConfig);

    /* Override default configuration using user defined parameters */
    IOConfig.type = LINIO_TYPE_TCP;
    IOConfig.linTCP.disconnectOnNewSyn = TMWDEFS_FALSE;
    IOConfig.linTCP.validateUDPAddress = TMWDEFS_FALSE;
    strncpy(IOConfig.linTCP.chnlName, IOConfigUser.chnlName, sizeof(IOConfig.linTCP.chnlName));
    strncpy(IOConfig.linTCP.ipAddress, IOConfigUser.ipAddress, sizeof(IOConfig.linTCP.ipAddress));
    IOConfig.linTCP.ipPort = IOConfigUser.ipPort;
    IOConfig.linTCP.ipConnectTimeout = IOConfigUser.ipConnectTimeout;
    IOConfig.linTCP.mode = IOConfigUser.mode;
    IOConfig.linTCP.role = IOConfigUser.role;
    IOConfig.linTCP.localUDPPort = IOConfigUser.localUDPPort;
    IOConfig.linTCP.destUDPPort = IOConfigUser.destUDPPort;
    IOConfig.linTCP.initUnsolUDPPort = IOConfigUser.initUnsolUDPPort;

    /* Initialize DNP slave session */
    sdnpsesn_initConfig(&sessionConfig);

    /* Override default configuration using user defined parameters */
    sessionConfig.validateSourceAddress = TMWDEFS_TRUE;
    sessionConfig.authenticationEnabled = TMWDEFS_FALSE;

    sessionConfig.source = SDNPSessionConfig.source;
    sessionConfig.destination = SDNPSessionConfig.destination;
    sessionConfig.linkStatusPeriod = SDNPSessionConfig.linkStatusPeriod;
    sessionConfig.multiFragRespAllowed = SDNPSessionConfig.multiFragRespAllowed;
    sessionConfig.multiFragConfirm = SDNPSessionConfig.multiFragConfirm;
    sessionConfig.respondNeedTime = SDNPSessionConfig.respondNeedTime;
    sessionConfig.clockValidPeriod = SDNPSessionConfig.clockValidPeriod;
    sessionConfig.applConfirmTimeout = SDNPSessionConfig.applConfirmTimeout;
    sessionConfig.selectTimeout = SDNPSessionConfig.selectTimeout;
    sessionConfig.unsolAllowed = SDNPSessionConfig.unsolAllowed;
    sessionConfig.unsolClassMask = SDNPSessionConfig.unsolClassMask;
    sessionConfig.unsolClass1MaxEvents = SDNPSessionConfig.unsolClass1MaxEvents;
    sessionConfig.unsolClass2MaxEvents = SDNPSessionConfig.unsolClass2MaxEvents;
    sessionConfig.unsolClass3MaxEvents = SDNPSessionConfig.unsolClass3MaxEvents;
    sessionConfig.unsolClass1MaxDelay = SDNPSessionConfig.unsolClass1MaxDelay;
    sessionConfig.unsolClass2MaxDelay = SDNPSessionConfig.unsolClass2MaxDelay;
    sessionConfig.unsolClass3MaxDelay = SDNPSessionConfig.unsolClass3MaxDelay;
    sessionConfig.unsolMaxRetries = SDNPSessionConfig.unsolMaxRetries;
    sessionConfig.unsolRetryDelay = SDNPSessionConfig.unsolRetryDelay;
    sessionConfig.unsolOfflineRetryDelay = SDNPSessionConfig.unsolOfflineRetryDelay;
    sessionConfig.obj01DefaultVariation = SDNPSessionConfig.obj01DefaultVariation;
    sessionConfig.obj02DefaultVariation = SDNPSessionConfig.obj02DefaultVariation;
    sessionConfig.obj03DefaultVariation = SDNPSessionConfig.obj03DefaultVariation;
    sessionConfig.obj04DefaultVariation = SDNPSessionConfig.obj04DefaultVariation;
    sessionConfig.obj30DefaultVariation = SDNPSessionConfig.obj30DefaultVariation;
    sessionConfig.obj32DefaultVariation = SDNPSessionConfig.obj32DefaultVariation;

    sessionConfig.binaryInputMaxEvents = SDNPSessionConfig.binaryInputMaxEvents;
    sessionConfig.binaryInputEventMode = SDNPSessionConfig.binaryInputEventMode;

    sessionConfig.doubleInputMaxEvents = SDNPSessionConfig.doubleInputMaxEvents;
    sessionConfig.doubleInputEventMode = SDNPSessionConfig.doubleInputEventMode;

    sessionConfig.analogInputMaxEvents = SDNPSessionConfig.analogInputMaxEvents;
    sessionConfig.analogInputEventMode = SDNPSessionConfig.analogInputEventMode;


    /*****************/
    /* Initialize db */
    /*****************/

    /* Allocate db */
    db = (DNP3SlaveDB*)calloc(1, sizeof(DNP3SlaveDB));

    /* Allocate pointDB */
    db->pointDB = (TMWSDNPDatabaseStr*)calloc(1, sizeof(TMWSDNPDatabaseStr));

    /* Allocate binary input db */
    db->pointDB->binaryInput.size = BIConfigSize;
    db->pointDB->binaryInput.list = (TMWSDNPBInputStr*)calloc( BIConfigSize,
                                                               sizeof(TMWSDNPBInputStr)
                                                             );

    /* Allocate binary input observer list */
    observerList = (IPointObserver**)calloc(BIConfigSize, sizeof(IPointObserver*));
    db->BIObserverList.setTable(observerList, BIConfigSize);

    /* Initialize binary input database and observers */
    for(lu_uint32_t i = 0; i < BIConfigSize; ++i)
    {
        BIConfigStr      *confPtr = &BIConfig[i];
        TMWSDNPBInputStr *pointPtr;

        if(confPtr->protocolID < BIConfigSize)
        {
            pointPtr = &db->pointDB->binaryInput.list[i];
        }
        else
        {
            continue;
        }

        pointPtr->defaultVariation = confPtr->defaultVariation;
        pointPtr->eventDefaultVariation = confPtr->eventDefaultVariation;
        pointPtr->enableClass0 = confPtr->enableClass0;
        pointPtr->eventClass = confPtr->eventClass;
        pointPtr->value = DNPDEFS_DBAS_FLAG_OFF_LINE;

        /* Create observers */
        observerList[confPtr->protocolID] = new DNP3SlaveBIObserver(confPtr->GDBId, confPtr->protocolID, db->pointDB);
        if (GDatabase.attach(confPtr->GDBId, db->BIObserverList[confPtr->protocolID]) == GDB_ERROR_NONE)
        {
            pointPtr->enabled = LU_TRUE;
        }
        else
        {
            /* Invalid virtual point */
            delete db->BIObserverList[confPtr->protocolID];
            observerList[confPtr->protocolID] = NULL;
        }
    }

    /* Allocate double binary input db */
    db->pointDB->dbinaryInput.size = DBIConfigSize;
    db->pointDB->dbinaryInput.list = (TMWSDNPDBInputStr*)calloc( DBIConfigSize,
                                                                 sizeof(TMWSDNPDBInputStr)
                                                               );

    /* Allocate double binary input observer list */
    observerList = (IPointObserver**)calloc(DBIConfigSize, sizeof(IPointObserver*));
    db->DBIObserverList.setTable(observerList, DBIConfigSize);

    /* Initialize double binary input database and observers */
    for(lu_uint32_t i = 0; i < DBIConfigSize; ++i)
    {
        DBIConfigStr      *confPtr = &DBIConfig[i];
        TMWSDNPDBInputStr *pointPtr;

        if(confPtr->protocolID < DBIConfigSize)
        {
            pointPtr = &db->pointDB->dbinaryInput.list[i];
        }
        else
        {
            continue;
        }

        pointPtr->defaultVariation = confPtr->defaultVariation;
        pointPtr->eventDefaultVariation = confPtr->eventDefaultVariation;
        pointPtr->enableClass0 = confPtr->enableClass0;
        pointPtr->eventClass = confPtr->eventClass;
        pointPtr->value = DNPDEFS_DBAS_FLAG_OFF_LINE;

        /* Create observers */
        observerList[confPtr->protocolID] = new DNP3SlaveDBIObserver(confPtr->GDBId, confPtr->protocolID, db->pointDB);
        if (GDatabase.attach(confPtr->GDBId, db->DBIObserverList[confPtr->protocolID]) == GDB_ERROR_NONE)
        {
            pointPtr->enabled = LU_TRUE;
        }
        else
        {
            /* Invalid virtual point */
            delete db->DBIObserverList[confPtr->protocolID];
            observerList[confPtr->protocolID] = NULL;
        }
    }

    /* Allocate analogue binary input db */
    db->pointDB->analogueInput.size = AIConfigSize;
    db->pointDB->analogueInput.list = (TMWSDNPAInputStr*)calloc( AIConfigSize,
                                                                 sizeof(TMWSDNPAInputStr)
                                                               );

    /* Allocate analogue input observer list */
    observerList = (IPointObserver**)calloc(AIConfigSize, sizeof(IPointObserver*));
    db->AIObserverList.setTable(observerList, AIConfigSize);

    /* Initialize analogue input database and observers */
    for(lu_uint32_t i = 0; i < AIConfigSize; ++i)
    {
        AIConfigStr      *confPtr = &AIConfig[i];
        TMWSDNPAInputStr *pointPtr;

        if(confPtr->protocolID < AIConfigSize)
        {
            pointPtr = &db->pointDB->analogueInput.list[i];
        }
        else
        {
            continue;
        }

        pointPtr->eventDefaultVariation = confPtr->eventDefaultVariation;
        pointPtr->enableClass0 = confPtr->enableClass0;
        pointPtr->eventClass = confPtr->eventClass;
        pointPtr->flags = DNPDEFS_DBAS_FLAG_OFF_LINE;
        pointPtr->value.type = TMWTYPES_ANALOG_TYPE_SFLOAT;
        pointPtr->value.value.fval = 0;

        /* Create observers */
        observerList[confPtr->protocolID] = new DNP3SlaveAIObserver(confPtr->GDBId, confPtr->protocolID, db->pointDB);
        if (GDatabase.attach(confPtr->GDBId, db->AIObserverList[confPtr->protocolID]) == GDB_ERROR_NONE)
        {
            pointPtr->enabled = LU_TRUE;
        }
        else
        {
            /* Invalid virtual point */
            delete db->AIObserverList[confPtr->protocolID];
            observerList[confPtr->protocolID] = NULL;
        }
    }

    return new IEC104SlaveProtocol( targConfig,
                                         chnlConfig,
                                         tprtConfig,
                                         linkConfig,
                                         physConfig,
                                         IOConfig,
                                         sessionConfig,
                                         GDatabase,
                                         mcmApp,
                                         db,
                                         slaveSchedType,
                                         slavePriority
                                       );
#endif
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
