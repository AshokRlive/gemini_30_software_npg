/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14sim.c
 * description: Simulates an IEC 60870-5-101/104 slave database.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/i14diag.h"
#include "tmwscl/utils/tmwcnfg.h"
#if TMWCNFG_USE_SIMULATED_DB

#include "tmwscl/i870/s14sim.h"
#include "tmwscl/i870/s14mem.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/s14mit.h"
#include "tmwscl/utils/tmwtarg.h"
 
/* Call update callback if set */
static void _callCallback(
  TMWSIM_POINT *pPoint,
  TMWSIM_EVENT_TYPE type,
  TMWTYPES_USHORT dataType)
{
  S14SIM_DATABASE *pDbHandle;
  pDbHandle = (S14SIM_DATABASE *)pPoint->pDbHandle;
  if(pDbHandle->pUpdateCallback != TMWDEFS_NULL)
  {
    pDbHandle->pUpdateCallback(pDbHandle->pUpdateCallbackParam, type, dataType, tmwsim_getPointNumber(pPoint));
  }
}

/* Call update callback when point is removed, if set */
static void _callRemoveCallback(
  S14SIM_DATABASE *pDbHandle,
  TMWSIM_EVENT_TYPE type,
  TMWTYPES_USHORT dataType,
  TMWTYPES_ULONG pointNumber)
{ 
  if(pDbHandle->pUpdateCallback != TMWDEFS_NULL)
  {
    pDbHandle->pUpdateCallback(pDbHandle->pUpdateCallbackParam, type, dataType, pointNumber);
  }
}
 
#if S14DATA_SUPPORT_FILE
/* function: _fileClearDb()
 * purpose: Deallocate all of the simulated files from the database
 * arguments:
 *  pDbHandle - 
 * returns:
 *  void
 */
static void TMWDEFS_GLOBAL _fileClearDb(
  S14SIM_DATABASE *pDbHandle)
{
  S14SIM_FILE *pFile;
  S14SIM_SECTION *pSection;

  /* File Transfer Database */
  while((pFile = (S14SIM_FILE *)tmwdlist_getFirst(&pDbHandle->file)) != TMWDEFS_NULL)
  {
    while((pSection = (S14SIM_SECTION *)tmwdlist_getFirst(&pFile->sections)) != TMWDEFS_NULL)
    {
      tmwdlist_removeEntry(&pFile->sections, (TMWDLIST_MEMBER *)pSection);
      s14mem_free(pSection);
    }

    tmwdlist_removeEntry(&pDbHandle->file, (TMWDLIST_MEMBER *)pFile);
    s14mem_free(pFile);
  }
}

/* function: _fileBuildDb()
 * purpose: Create example simulated file and directory entries in slave database
 * arguments:
 *  pDbHandle - 
 * returns:
 *  void
 */
static void TMWDEFS_GLOBAL  _fileBuildDb(
  S14SIM_DATABASE *pDbHandle)
{
  int i;
  /* Add some file and directory entries, that need to be reported in more than one FDRTA ASDU */

  /* 6 consecutive IOAs for first FDRTA 4 file, 2 directory entries */
  for(i = 0; i < 4; i++)
  {
    /* Add simulated file */
    /* handle, ioa, filename, SOF */
    s14sim_fileAdd(pDbHandle, S14SIM_FIRST_FILE + i, 1, I14DEF_SOF_FOR_IS_FILE);

    /* handle, ioa, filename, sectionLength, pSectionData */
    s14sim_fileSectionAdd(pDbHandle, S14SIM_FIRST_FILE + i, 1,(TMWTYPES_USHORT)(50*(i+1)), TMWDEFS_NULL);
    s14sim_fileSectionAdd(pDbHandle, S14SIM_FIRST_FILE + i, 1, (TMWTYPES_USHORT)(200*(i+1)), TMWDEFS_NULL);
  }

  /* Add 2 simulated directories */
  /* handle, ioa, filename, SOF */
  s14sim_fileAdd(pDbHandle, S14SIM_FIRST_FILE + i++, S14SIM_FIRST_FILE + 10, I14DEF_SOF_FOR_IS_DIR);
  s14sim_fileAdd(pDbHandle, S14SIM_FIRST_FILE + i,   S14SIM_FIRST_FILE + 20, I14DEF_SOF_FOR_IS_DIR);

  /* 3 more file entries, for second FDRTA */
  for(i = 0; i < 3; i++)
  {
    /* Add simulated file */
    /* handle, ioa, filename, SOF */
    s14sim_fileAdd(pDbHandle, S14SIM_FIRST_FILE + 10 +i, 1, I14DEF_SOF_FOR_IS_FILE);

    /* handle, ioa, filename, sectionLength, pSectionData */
    s14sim_fileSectionAdd(pDbHandle, S14SIM_FIRST_FILE + 10 +i, 1, (TMWTYPES_USHORT)(80*(i+1)), TMWDEFS_NULL);
    s14sim_fileSectionAdd(pDbHandle, S14SIM_FIRST_FILE + 10 +i, 1, (TMWTYPES_USHORT)(100*(i+1)), TMWDEFS_NULL);
    s14sim_fileSectionAdd(pDbHandle, S14SIM_FIRST_FILE + 10 +i, 1, (TMWTYPES_USHORT)(140*(i+1)), TMWDEFS_NULL);
  }

  /* 2 more file entries, for third FDRTA */
  for(i = 0; i < 2; i++)
  {
    /* Add simulated file */
    /* handle, ioa, filename, SOF */
    s14sim_fileAdd(pDbHandle, S14SIM_FIRST_FILE + 20 +i, 1, I14DEF_SOF_FOR_IS_FILE);

    /* handle, ioa, filename, sectionLength, pSectionData */
    s14sim_fileSectionAdd(pDbHandle, S14SIM_FIRST_FILE + 20 +i, 1, (TMWTYPES_USHORT)(100), TMWDEFS_NULL);
    s14sim_fileSectionAdd(pDbHandle, S14SIM_FIRST_FILE + 20 +i, 1, (TMWTYPES_USHORT)(400), TMWDEFS_NULL);
  }

  /* Set changed flag used for spontaneous FDRTA directory messages to false */
  pDbHandle->fileDirChanged = TMWDEFS_FALSE;
}
#endif

/* function: _clearDb()
 * purpose: Delete all points in the database
 * arguments:
 *  pHandle - 
 * returns:
 *  void
 */
static void TMWDEFS_LOCAL _clearDb(
  void *pHandle)
{
  void *pPoint;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;

  _callRemoveCallback(pDbHandle, TMWSIM_CLEAR_DATABASE, 0, 0);
 
  while((pPoint = s14sim_mspGetPoint(pDbHandle, 0)) != TMWDEFS_NULL)
  {
    s14sim_mspDeletePoint(pDbHandle, tmwsim_getPointNumber((TMWSIM_POINT *)pPoint));  
  } 
  tmwsim_tableDestroy(&pDbHandle->msp);
 
  while((pPoint = s14sim_mdpGetPoint(pDbHandle, 0)) != TMWDEFS_NULL)
  {
    s14sim_mdpDeletePoint(pDbHandle, tmwsim_getPointNumber((TMWSIM_POINT *)pPoint));  
  } 
  tmwsim_tableDestroy(&pDbHandle->mdp);

  while((pPoint = s14sim_mstGetPoint(pDbHandle, 0)) != TMWDEFS_NULL)
  {
    s14sim_mstDeletePoint(pDbHandle, tmwsim_getPointNumber((TMWSIM_POINT *)pPoint));  
  } 
  tmwsim_tableDestroy(&pDbHandle->mst);

  while((pPoint = s14sim_mboGetPoint(pDbHandle, 0)) != TMWDEFS_NULL)
  {
    s14sim_mboDeletePoint(pDbHandle, tmwsim_getPointNumber((TMWSIM_POINT *)pPoint));  
  } 
  tmwsim_tableDestroy(&pDbHandle->mbo);

  while((pPoint = s14sim_mitGetPoint(pDbHandle, 0)) != TMWDEFS_NULL)
  {
    s14sim_mitDeletePoint(pDbHandle, tmwsim_getPointNumber((TMWSIM_POINT *)pPoint));  
  } 
  tmwsim_tableDestroy(&pDbHandle->mit);

  while((pPoint = s14sim_mmenaGetPoint(pDbHandle, 0)) != TMWDEFS_NULL)
  {
    s14sim_mmenaDeletePoint(pDbHandle, tmwsim_getPointNumber((TMWSIM_POINT *)pPoint));  
  } 
  tmwsim_tableDestroy(&pDbHandle->mmena);

  while((pPoint = s14sim_mmenbGetPoint(pDbHandle, 0)) != TMWDEFS_NULL)
  {
    s14sim_mmenbDeletePoint(pDbHandle, tmwsim_getPointNumber((TMWSIM_POINT *)pPoint));  
  } 
  tmwsim_tableDestroy(&pDbHandle->mmenb);

#if S14DATA_SUPPORT_MME_C
  while((pPoint = s14sim_mmencGetPoint(pDbHandle, 0)) != TMWDEFS_NULL)
  {
    s14sim_mmencDeletePoint(pDbHandle, tmwsim_getPointNumber((TMWSIM_POINT *)pPoint));  
  } 
  tmwsim_tableDestroy(&pDbHandle->mmenc);
#endif

  /* These four get deleted when mmena/b/c get deleted */
  tmwsim_tableDestroy(&pDbHandle->pmena);
  tmwsim_tableDestroy(&pDbHandle->pmenb); 
#if S14DATA_SUPPORT_PMENC 
  tmwsim_tableDestroy(&pDbHandle->pmenc);
#endif
  tmwsim_tableDestroy(&pDbHandle->pacna); 

  while((pPoint = s14sim_meptaGetPoint(pDbHandle, 0)) != TMWDEFS_NULL)
  {
    s14sim_meptaDeletePoint(pDbHandle, tmwsim_getPointNumber((TMWSIM_POINT *)pPoint));
  } 
  tmwsim_tableDestroy(&pDbHandle->mepta);
  
  while((pPoint = s14sim_meptbGetPoint(pDbHandle, 0)) != TMWDEFS_NULL)
  {
    s14sim_meptbDeletePoint(pDbHandle, tmwsim_getPointNumber((TMWSIM_POINT *)pPoint));
  } 
  tmwsim_tableDestroy(&pDbHandle->meptb); 
  
  while((pPoint = s14sim_meptcGetPoint(pDbHandle, 0)) != TMWDEFS_NULL)
  {
    s14sim_meptcDeletePoint(pDbHandle, tmwsim_getPointNumber((TMWSIM_POINT *)pPoint));
  } 
  tmwsim_tableDestroy(&pDbHandle->meptc);
 
  while((pPoint = s14sim_mpsnaGetPoint(pDbHandle, 0)) != TMWDEFS_NULL)
  {
    s14sim_mpsnaDeletePoint(pDbHandle, tmwsim_getPointNumber((TMWSIM_POINT *)pPoint));
  } 
  tmwsim_tableDestroy(&pDbHandle->mpsna); 

  while((pPoint = s14sim_mmendGetPoint(pDbHandle, 0)) != TMWDEFS_NULL)
  {
    s14sim_mmendDeletePoint(pDbHandle, tmwsim_getPointNumber((TMWSIM_POINT *)pPoint));
  } 
  tmwsim_tableDestroy(&pDbHandle->mmend); 

#if S14DATA_SUPPORT_MITC
  while((pPoint = s14sim_mitcGetPoint(pDbHandle, 0)) != TMWDEFS_NULL)
  {
    s14sim_mitcDeletePoint(pDbHandle, tmwsim_getPointNumber((TMWSIM_POINT *)pPoint));
  } 
  tmwsim_tableDestroy(&pDbHandle->mitc);
#endif
#if S14DATA_SUPPORT_MCTNA
  while((pPoint = s14sim_mctnaGetPoint(pDbHandle, 0)) != TMWDEFS_NULL)
  {
    s14sim_mctnaDeletePoint(pDbHandle, tmwsim_getPointNumber((TMWSIM_POINT *)pPoint));
  } 
  tmwsim_tableDestroy(&pDbHandle->mctna); 
#endif

#if S14DATA_SUPPORT_FILE
  /* Delete the file transfer database */
  _fileClearDb(pDbHandle);
#endif
}

/* function: _buildDb()
 * purpose: Build default database
 * arguments:
 *  pHandle - 
 * returns:
 *  void
 */
static void TMWDEFS_GLOBAL _buildDb(
  void *pHandle)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  TMWTYPES_USHORT i;

  pDbHandle->mspIndexed = TMWDEFS_FALSE;
  tmwsim_tableCreate(&pDbHandle->msp);
  for(i = 0; i < S14SIM_NUM_MSP; i++)
  {
    s14sim_mspAddPoint(pDbHandle, S14SIM_FIRST_MSP + i,
      TMWDEFS_GROUP_MASK_GENERAL | (0x01 << ((i % 10) + 1)),
      0, TMWDEFS_FALSE);
  }

  pDbHandle->mdpIndexed = TMWDEFS_FALSE;
  tmwsim_tableCreate(&pDbHandle->mdp);
  for(i = 0; i < S14SIM_NUM_MDP; i++)
  {
    s14sim_mdpAddPoint(pDbHandle, S14SIM_FIRST_MDP + i,
      TMWDEFS_GROUP_MASK_GENERAL | (0x01 << ((i % 10) + 1)),
      0, TMWDEFS_DPI_OFF);
  }

  pDbHandle->mstIndexed = TMWDEFS_FALSE;
  tmwsim_tableCreate(&pDbHandle->mst);
  for(i = 0; i < S14SIM_NUM_MST; i++)
  {
    s14sim_mstAddPoint(pDbHandle, S14SIM_FIRST_MST + i,
      TMWDEFS_GROUP_MASK_GENERAL | (0x01 << ((i % 10) + 1)),
      0, 0);
  }

  pDbHandle->mboIndexed = TMWDEFS_FALSE;
  tmwsim_tableCreate(&pDbHandle->mbo);
  for(i = 0; i < S14SIM_NUM_MBO; i++)
  {
    s14sim_mboAddPoint(pDbHandle, S14SIM_FIRST_MBO + i,
      TMWDEFS_GROUP_MASK_GENERAL | (0x01 << ((i % 10) + 1)),
      0, 0);
  }

  pDbHandle->mitIndexed = TMWDEFS_FALSE;
  tmwsim_tableCreate(&pDbHandle->mit);
  for(i = 0; i < S14SIM_NUM_MIT; i++)
  {
    s14sim_mitAddPoint(pDbHandle, S14SIM_FIRST_MIT + i,
      TMWDEFS_GROUP_MASK_GENERAL | (0x01 << ((i % 4) + 1)),
      0, 0);
  }
  
  tmwsim_tableCreate(&pDbHandle->mepta);
  for(i = 0; i < S14SIM_NUM_MEPTA; i++)
  {
    s14sim_meptaAddPoint(pDbHandle, S14SIM_FIRST_MEPTA + i,
      0, 0);
  }

  tmwsim_tableCreate(&pDbHandle->meptb);
  for(i = 0; i < S14SIM_NUM_MEPTB; i++)
  {
    s14sim_meptbAddPoint(pDbHandle, S14SIM_FIRST_MEPTB + i,
      0, 0, 0);
  }

  tmwsim_tableCreate(&pDbHandle->meptc);
  for(i = 0; i < S14SIM_NUM_MEPTC; i++)
  {
    s14sim_meptcAddPoint(pDbHandle, S14SIM_FIRST_MEPTC + i,
      0, 0, 0);
  }

  pDbHandle->mpsnaIndexed = TMWDEFS_FALSE;
  tmwsim_tableCreate(&pDbHandle->mpsna);
  for(i = 0; i < S14SIM_NUM_MPSNA; i++)
  {
    s14sim_mpsnaAddPoint(pDbHandle, S14SIM_FIRST_MPSNA + i,
      TMWDEFS_GROUP_MASK_BACKGROUND |TMWDEFS_GROUP_MASK_GENERAL | (0x01 << ((i % 10) + 1)),
      0, 0);
  }

  pDbHandle->mmenaIndexed = TMWDEFS_FALSE;
  pDbHandle->mmenbIndexed = TMWDEFS_FALSE;
  pDbHandle->mmencIndexed = TMWDEFS_FALSE;
  pDbHandle->mmendIndexed = TMWDEFS_FALSE;
  tmwsim_tableCreate(&pDbHandle->mmend);
  for(i = 0; i < S14SIM_NUM_MMEND; i++)
  {
    s14sim_mmendAddPoint(pDbHandle, S14SIM_FIRST_MMEND + i,
      TMWDEFS_GROUP_MASK_CYCLIC,
      0);
  }

  tmwsim_tableCreate(&pDbHandle->mmena);
  tmwsim_tableCreate(&pDbHandle->pmena);
  tmwsim_tableCreate(&pDbHandle->pacna);
  for(i = 0; i < S14SIM_NUM_MMENA; i++)
  {
    TMWTYPES_ULONG pmenaIOAs[4];
    pmenaIOAs[0] = S14SIM_FIRST_PMENA + (i * 4);
    pmenaIOAs[1] = S14SIM_FIRST_PMENA + (i * 4) + 1;
    pmenaIOAs[2] = S14SIM_FIRST_PMENA + (i * 4) + 2;
    pmenaIOAs[3] = S14SIM_FIRST_PMENA + (i * 4) + 3;
    s14sim_mmenaAddPoint(pDbHandle, S14SIM_FIRST_MMENA + i, S14SIM_FIRST_PACNA + i, pmenaIOAs,
       TMWDEFS_GROUP_MASK_CYCLIC,
      0, 0);
  }

  tmwsim_tableCreate(&pDbHandle->mmenb);
  tmwsim_tableCreate(&pDbHandle->pmenb);
  for(i = 0; i < S14SIM_NUM_MMENB; i++)
  {
    TMWTYPES_ULONG pmenbIOAs[4];
    pmenbIOAs[0] = S14SIM_FIRST_PMENB + (i * 4);
    pmenbIOAs[1] = S14SIM_FIRST_PMENB + (i * 4) + 1;
    pmenbIOAs[2] = S14SIM_FIRST_PMENB + (i * 4) + 2;
    pmenbIOAs[3] = S14SIM_FIRST_PMENB + (i * 4) + 3;
    s14sim_mmenbAddPoint(pDbHandle, S14SIM_FIRST_MMENB + i,
      S14SIM_FIRST_PACNA + S14SIM_NUM_MMENA + i, pmenbIOAs,
      TMWDEFS_GROUP_MASK_CYCLIC,
      0, 0);
  }

#if S14DATA_SUPPORT_MME_C
  tmwsim_tableCreate(&pDbHandle->mmenc);
  tmwsim_tableCreate(&pDbHandle->pmenc);
  for(i = 0; i < S14SIM_NUM_MMENC; i++)
  {
    TMWTYPES_ULONG pmencIOAs[4];
    pmencIOAs[0] = S14SIM_FIRST_PMENC + (i * 4);
    pmencIOAs[1] = S14SIM_FIRST_PMENC + (i * 4) + 1;
    pmencIOAs[2] = S14SIM_FIRST_PMENC + (i * 4) + 2;
    pmencIOAs[3] = S14SIM_FIRST_PMENC + (i * 4) + 3;
    s14sim_mmencAddPoint(pDbHandle, S14SIM_FIRST_MMENC + i,
      S14SIM_FIRST_PACNA + S14SIM_NUM_MMENA + S14SIM_NUM_MMENB + i, pmencIOAs,
      TMWDEFS_GROUP_MASK_CYCLIC,
      0, 0);
  }
#endif
  

#if S14DATA_SUPPORT_MITC
  tmwsim_tableCreate(&pDbHandle->mitc);
#endif

#if S14DATA_SUPPORT_MCTNA
  tmwsim_tableCreate(&pDbHandle->mctna);
#endif

  /* File transfer support */ 
  tmwdlist_initialize(&pDbHandle->file);
#if S14DATA_SUPPORT_FILE
  _fileBuildDb(pDbHandle);
#endif

  pDbHandle->cicnaMode = 0;
  pDbHandle->ccinaMode = 0;
  pDbHandle->crdnaMode = 0;

  /* Init QOI to Group Mask mapping array to empty */
  for(i = 0; i < S14SIM_NUMBER_PRIVATE_GROUPS; i++)
  {
    pDbHandle->qoiToGroupMask[i].qoi = 0;
  }
}

/* function: _checkForIndexed()
 * purpose: Determine whether to use indexed or sequential
 *  in Variable Structure Qualifier SQ bit.
 * arguments:
 *  pTable - pointer to list or table used for this data type.
 * returns:
 *  TMWTYPES_TRUE - if indexed IOAs should be used
 *  TMWTYPES_FALSE - if sequential IOAs can be used
 */
static TMWTYPES_BOOL TMWDEFS_LOCAL _checkForIndexed(
  TMWSIM_TABLE_HEAD *pTable)
{
  /* If all of the IOAs are sequential, indexed is not required 
   * If there are any gaps, make response use indexed 
   * Variable Structure Qualifier VSQ SQ bit = 1
   */
  TMWSIM_POINT *pPoint;
  TMWTYPES_ULONG prevIOA = 0;

  pPoint = tmwsim_tableGetFirstPoint(pTable);
  while(pPoint != TMWDEFS_NULL)
  {
    TMWTYPES_ULONG ioa = s14sim_getInfoObjAddr(pPoint);
    if((prevIOA != 0) && (prevIOA+1 != ioa))
      return(TMWDEFS_TRUE);
    prevIOA = ioa;
    pPoint = tmwsim_tableGetNextPoint(pTable, pPoint);
  }
  return(TMWDEFS_FALSE);
}

#if S14DATA_SUPPORT_RTU_CICNA
TMWTYPES_UCHAR gasunieRTU = 0;
TMWTYPES_UCHAR gasunieNextLRU = 0;
TMWTYPES_UCHAR gasunieCurrentLRU = 0;
#endif

/* function: s14sim_init */
void * TMWDEFS_GLOBAL s14sim_init(
  TMWSCTR *pSector)
{
  S14SIM_DATABASE *pDbHandle;

  pDbHandle = (S14SIM_DATABASE *)s14mem_alloc(S14MEM_SIM_DATABASE_TYPE);
  if(pDbHandle == TMWDEFS_NULL)
  {
    return(TMWDEFS_NULL);
  }

#if TMWCNFG_USE_MANAGED_SCL
  memset(pDbHandle, 0, sizeof(S14SIM_DATABASE));
  pDbHandle->pS14Sector = (S14SCTR *)pSector;
  s14sim_clear(pDbHandle);
#else
  pDbHandle->pS14Sector = (S14SCTR *)pSector;
  pDbHandle->pUpdateCallback = TMWDEFS_NULL;
  pDbHandle->pUpdateCallbackParam = TMWDEFS_NULL;
  _buildDb(pDbHandle);
#endif

  pDbHandle->ctrlPointOffset = S14SIM_CTRL_POINT_OFFSET;
  pDbHandle->resetCOI = I14DEF_COI_LOCAL_PARAMS_CHANGED | I14DEF_COI_POWER_ON;

#if S14DATA_SUPPORT_RTU_CICNA
  {
    I870SESN *pI870Session;
    /* Special code for Gasunie CICNA behavior */
    pI870Session = (I870SESN *)pSector->pSession;

    /* If this is the first sector on this session make it the Gasunie RTU */
    if(tmwdlist_size(&pI870Session->sectorList) == 1)
    {
      gasunieRTU = pDbHandle->gasunieLRU = gasunieNextLRU;
      /* Remove the data points from this RTU sector */
      /* s14sim_clear(pDbHandle); */
    }
    pDbHandle->gasunieLRU = gasunieNextLRU++;
    pDbHandle->gasunieCOT = 0;
  }
#endif

  return(pDbHandle);
}

/* function: s14sim_close */
void TMWDEFS_GLOBAL s14sim_close(
  void *pHandle)
{
  _clearDb(pHandle);
  s14mem_free(pHandle);
}

/* function: s14sim_clear */
void TMWDEFS_GLOBAL s14sim_clear(
  void *pHandle)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  _clearDb(pHandle);

  /* Recreate/reinit tables, so points can be added */
  tmwsim_tableCreate(&pDbHandle->msp);
  tmwsim_tableCreate(&pDbHandle->mdp);
  tmwsim_tableCreate(&pDbHandle->mst);
  tmwsim_tableCreate(&pDbHandle->mbo);
  tmwsim_tableCreate(&pDbHandle->mit);
  tmwsim_tableCreate(&pDbHandle->mitc);
  tmwsim_tableCreate(&pDbHandle->mmena);
  tmwsim_tableCreate(&pDbHandle->mmenb);
#if S14DATA_SUPPORT_MME_C
  tmwsim_tableCreate(&pDbHandle->mmenc);
#endif
  tmwsim_tableCreate(&pDbHandle->pmena);
  tmwsim_tableCreate(&pDbHandle->pmenb); 
#if S14DATA_SUPPORT_PMENC
  tmwsim_tableCreate(&pDbHandle->pmenc);
#endif
  tmwsim_tableCreate(&pDbHandle->pacna); 
  tmwsim_tableCreate(&pDbHandle->mepta);
  tmwsim_tableCreate(&pDbHandle->meptb); 
  tmwsim_tableCreate(&pDbHandle->meptc);
  tmwsim_tableCreate(&pDbHandle->mpsna); 
  tmwsim_tableCreate(&pDbHandle->mmend); 
#if S14DATA_SUPPORT_MCTNA
  tmwsim_tableCreate(&pDbHandle->mctna); 
#endif
}

/* function: s14sim_reset */
void TMWDEFS_GLOBAL s14sim_reset(
  void *pHandle)
{
  _clearDb(pHandle);
  _buildDb(pHandle);
}

/* Set update callback and parameter */
void s14sim_setCallback(
  void *pHandle,
  S14SIM_CALLBACK_FUNC pUpdateCallback,
  void *pUpdateCallbackParam)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  pDbHandle->pUpdateCallback = pUpdateCallback;
  pDbHandle->pUpdateCallbackParam = pUpdateCallbackParam;
}

/* function: s14sim_hasSectorReset */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_hasSectorReset(
  void *pHandle,
  TMWTYPES_UCHAR *pResetCOI)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  if(pDbHandle->resetCOI)
  {
    *pResetCOI = pDbHandle->resetCOI;
    return(TMWDEFS_TRUE);
  }
  else
  {
    return(TMWDEFS_FALSE);
  }
}

/* function: s14sim_clearSectorReset */
void TMWDEFS_GLOBAL s14sim_clearSectorReset(
  void *pHandle)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  pDbHandle->resetCOI = 0;
}

/* function: s14sim_dataReady */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_dataReady(
  void *pHandle,
  TMWTYPES_UCHAR type)
{
  static int delayCount;
  TMWSIM_TESTINGMODE testingMode;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;

  switch(type)
  {
    case I14DEF_TYPE_CICNA1:
      testingMode = pDbHandle->cicnaMode;
#if S14DATA_SUPPORT_RTU_CICNA
      /* Special code for Gasunie GI behavior. 
       * All data from one LRU before data from another LRU 
       */
      if((pDbHandle->gasunieCOT == 0)
        ||(pDbHandle->gasunieLRU == gasunieRTU)
        ||(pDbHandle->gasunieLRU == gasunieCurrentLRU))
        return(TMWDEFS_TRUE);
      else
        return(TMWDEFS_FALSE);
#endif
      break;
    case I14DEF_TYPE_CCINA1:
      testingMode = pDbHandle->ccinaMode;
      break;
    case I14DEF_TYPE_CRDNA1:
      testingMode = pDbHandle->crdnaMode;
      break;
    default:
      testingMode = 0;
      break;
  } 
  
  if(testingMode == TMWSIM_TESTINGMODE_DELAY)
  { 
    if(delayCount++ <5)
    {
      return(TMWDEFS_FALSE);
    } 
    delayCount = 0;
  }

  return(TMWDEFS_TRUE);
}

#if S14DATA_SUPPORT_RTU_CICNA
/* function: s14sim_CICNAResponseReady  
 *  This provides support for the behavior required by Gasunie IEC-104 PID
 *   when a GI with broadcast Common ASDU Address is received.
 * 
 *   When a GI with broadcast Common ASDU Address is received, 
 *    send I14DEF_COT_ACTCON from RTU first, 
 *    followed by I14DEF_COT_ACTCON from Logical Units, 
 *    followed by I14DEF_COT_INTG_GEN (or other data) from Logical Units,
 *    followed by I14DEF_COT_ACTTERM from Logical Units, 
 *    followed by I14DEF_COT_ACTTERM from RTU  
 *
 *   This behavior is not specified by 101 and 104 specs.
 */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_CICNAResponseReady(
  void *pHandle,
  TMWTYPES_UCHAR cot)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  I870SESN *pI870Session = (I870SESN *)pDbHandle->pS14Sector->i870.tmw.pSession;

  if(cot == I14DEF_COT_ACTCON)
  {
    /* Send from RTU before LRUs 
     * then LRU 1 ACTCON, DATA, ACTTERM 
     * then LRU 2 ACTCON, DATA, ACTTERM 
     * etc
     */
    if(pDbHandle->gasunieLRU == gasunieRTU)
    {
      pDbHandle->gasunieCOT = I14DEF_COT_ACTCON;
      gasunieCurrentLRU = gasunieRTU +1; 
      return(TMWDEFS_TRUE);
    }
    else
    {
      S14SIM_DATABASE *pHandle;
      S14SCTR *pS14Sector = TMWDEFS_NULL;

      if(pDbHandle->gasunieLRU == gasunieCurrentLRU)
      {
        pDbHandle->gasunieCOT = 0;

        /* gasunie RTU is the first sector */
        pS14Sector = (S14SCTR *)tmwdlist_getFirst(&pI870Session->sectorList);
        pHandle = (S14SIM_DATABASE *)pS14Sector->i870.pDbHandle;

        /* if RTU ACTCON has been sent, it is OK to send LU ACTCON */
        if(pHandle->gasunieCOT == I14DEF_COT_ACTCON)
        { 
          pDbHandle->gasunieCOT = I14DEF_COT_ACTCON;
          return(TMWDEFS_TRUE);
        }
        else
          return(TMWDEFS_FALSE);
      }
      else
        return(TMWDEFS_FALSE);
    }
  }

  else if(cot == I14DEF_COT_ACTTERM)
  {
    /* send from RTU AFTER LRUs */
    if(pDbHandle->gasunieLRU == gasunieRTU)
    {
      /* Check to see that all LUs have sent their ACT TERM */ 
      S14SCTR *pS14Sector = TMWDEFS_NULL;
      while((pS14Sector = (S14SCTR *)tmwdlist_getAfter(&pI870Session->sectorList, (TMWDLIST_MEMBER *)pS14Sector)) != TMWDEFS_NULL)
      {
        if(pS14Sector != pDbHandle->pS14Sector)
        {
          S14SIM_DATABASE *pHandle = (S14SIM_DATABASE *)pS14Sector->i870.pDbHandle;
          if(pHandle->gasunieCOT != I14DEF_COT_ACTTERM)
            return(TMWDEFS_FALSE);
        }
      }

      /* Reinit all for next CICNA request */
      pS14Sector = TMWDEFS_NULL;
      while((pS14Sector = (S14SCTR *)tmwdlist_getAfter(&pI870Session->sectorList, (TMWDLIST_MEMBER *)pS14Sector)) != TMWDEFS_NULL)
      {
        S14SIM_DATABASE *pHandle = (S14SIM_DATABASE *)pS14Sector->i870.pDbHandle;
        pHandle->gasunieCOT = 0;
      }
      return(TMWDEFS_TRUE);
    }
    else
    {
      /* ACTTERM for this LU has been sent */
      pDbHandle->gasunieCOT = I14DEF_COT_ACTTERM;
      gasunieCurrentLRU++;
      return(TMWDEFS_TRUE);
    }
  }

  /* SCL will not use other COTs, but return true anyway */
  return(TMWDEFS_TRUE);
}
#endif

/* function: s14sim_setDataReady */
void TMWDEFS_GLOBAL s14sim_setDataReady( 
  void *pHandle,
  TMWTYPES_UCHAR type,
  TMWSIM_TESTINGMODE mode)
{  
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  switch(type)
  {
    case I14DEF_TYPE_CICNA1:
      pDbHandle->cicnaMode = mode;
      break;
    case I14DEF_TYPE_CCINA1:
      pDbHandle->ccinaMode = mode;
      break;
    case I14DEF_TYPE_CRDNA1:
      pDbHandle->crdnaMode = mode;
      break;
  }
}

/* function: s14sim_setCtrlOffset */
void TMWDEFS_GLOBAL s14sim_setCtrlOffset(
  void *pHandle,
  TMWTYPES_LONG offset)
{
  ((S14SIM_DATABASE *)pHandle)->ctrlPointOffset = offset;
}

/* function: s14sim_getCtrlOffset */
TMWTYPES_LONG TMWDEFS_GLOBAL s14sim_getCtrlOffset(
  void *pHandle)
{
  return ((S14SIM_DATABASE *)pHandle)->ctrlPointOffset;
}

/* function: s14sim_convertQOItoGroupMask  */
TMWDEFS_GROUP_MASK TMWDEFS_GLOBAL s14sim_convertQOItoGroupMask(
  void *pHandle,
  TMWTYPES_UCHAR qoi)
{
  int i;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  for(i = 0; i < S14SIM_NUMBER_PRIVATE_GROUPS; i++)
  {
    if(pDbHandle->qoiToGroupMask[i].qoi == qoi)
      return(pDbHandle->qoiToGroupMask[i].groupMask);
  }
  return(0);
}

/* function: s14sim_addQOItoGroupMask  */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_addQOItoGroupMask(
  void *pHandle,
  TMWTYPES_UCHAR qoi,
  TMWDEFS_GROUP_MASK groupMask)
{
  int i;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  for(i = 0; i < S14SIM_NUMBER_PRIVATE_GROUPS; i++)
  {
    /* if entry is unused, store info here */
    if(pDbHandle->qoiToGroupMask[i].qoi == 0)
    {
      pDbHandle->qoiToGroupMask[i].qoi = qoi;
      pDbHandle->qoiToGroupMask[i].groupMask = groupMask;
      return(TMWDEFS_TRUE);
    }
  }
  return(TMWDEFS_FALSE);
}

/* function: s14sim_removeQOItoGroupMask  */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_removeQOItoGroupMask(
  void *pHandle,
  TMWTYPES_UCHAR qoi)
{
  int i;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  for(i = 0; i < S14SIM_NUMBER_PRIVATE_GROUPS; i++)
  {
    /* if entry contains this QOI, remove it */
    if(pDbHandle->qoiToGroupMask[i].qoi == qoi)
    {
      pDbHandle->qoiToGroupMask[i].qoi = 0;
      return(TMWDEFS_TRUE);
    }
  }
  return(TMWDEFS_FALSE);
}

/* function: s14sim_getInfoObjAddr */
TMWTYPES_ULONG TMWDEFS_GLOBAL s14sim_getInfoObjAddr(
  void *pPoint)
{
  return(tmwsim_getPointNumber((TMWSIM_POINT *)pPoint));
}

/* function: s14sim_getGroupMask */
TMWDEFS_GROUP_MASK TMWDEFS_GLOBAL s14sim_getGroupMask(
  void *pPoint)
{
  return(tmwsim_getGroupMask((TMWSIM_POINT *)pPoint));
}

/* function: s14sim_getFlags */
TMWTYPES_UCHAR TMWDEFS_GLOBAL s14sim_getFlags(
  void *pPoint)
{
  return(tmwsim_getFlags((TMWSIM_POINT *)pPoint));
}

S14DATA_TRANSMISSION_MODE TMWDEFS_GLOBAL s14sim_getTransmissionMode(
  void *pPoint)
{ 
  /* Store transmission mode in default event variation */
  return(tmwsim_getDefEventVariation((TMWSIM_POINT *)pPoint));
}

void TMWDEFS_GLOBAL s14sim_setTransmissionMode(
  void *pPoint,
  S14DATA_TRANSMISSION_MODE value)
{ 
  /* Store transmission mode in default event variation */
  tmwsim_setDefEventVariation((TMWSIM_POINT *)pPoint, (TMWTYPES_UCHAR)value);
}

/* function: s14sim_getChanged */
TMWTYPES_UCHAR TMWDEFS_GLOBAL s14sim_getChanged(
  void *pPoint,
  TMWDEFS_CHANGE_REASON *pReason)
{
  /* Don't return true for remote op since remote events are generated by
   * the SCL because the simulated database defines monitored points for
   * all command points.
   */
  if(tmwsim_getChanged((TMWSIM_POINT *)pPoint, pReason))
  {
    if(*pReason != TMWDEFS_CHANGE_REMOTE_OP)
      return(TMWDEFS_TRUE);
  }

  return(TMWDEFS_FALSE);
}

/* function: s14sim_getTimeFormat */
TMWDEFS_TIME_FORMAT TMWDEFS_GLOBAL s14sim_getTimeFormat(
  void *pPoint)
{
  return(tmwsim_getTimeFormat((TMWSIM_POINT *)pPoint));
}

void TMWDEFS_GLOBAL s14sim_getDateTime(
   void *pPoint, 
   TMWDTIME *pTime)
{
  tmwsim_getTimeStamp((TMWSIM_POINT *)pPoint, pTime);
}

/* function: _s14sim_getSelectStatus  
 * purpose: Determine whether to indicate select succeeded, failed or in progress
 *  depending on testingMode, set by s101testingmode testharness command.
 * arguments:
 *  pPoint
 * returns:
 *  TMWDEFS_COMMAND_STATUS
 */
static TMWDEFS_COMMAND_STATUS TMWDEFS_LOCAL _s14sim_getSelectStatus(
  void *pPoint)
{ 
  TMWSIM_TESTINGMODE testingMode;

  /* See if this point has been set to not return CMD SUCCESS for test purposes */
  testingMode = tmwsim_getTestingMode((TMWSIM_POINT *)pPoint);

  if(testingMode == TMWSIM_TESTINGMODE_FAILURE)
    return(TMWDEFS_CMD_STAT_FAILED);

  if(testingMode == TMWSIM_TESTINGMODE_DELAY)
    return(TMWDEFS_CMD_STAT_SELECTING);

  return(TMWDEFS_CMD_STAT_SUCCESS);
}

/* function: _s14sim_getExecuteStatus  
 * purpose: Determine whether to indicate execute succeeded, failed or in progress
 *  depending on testingMode, set by s101testingmode testharness command.
 * arguments:
 *  pPoint
 * returns:
 *  TMWDEFS_COMMAND_STATUS
 */
static TMWDEFS_COMMAND_STATUS TMWDEFS_LOCAL _s14sim_getExecuteStatus(
  void *pPoint)
{ 
  TMWSIM_TESTINGMODE testingMode;

  /* See if this point has been set to not return CMD SUCCESS for test purposes */
  testingMode = tmwsim_getTestingMode((TMWSIM_POINT *)pPoint);

  if(testingMode == TMWSIM_TESTINGMODE_FAILURE)
    return(TMWDEFS_CMD_STAT_FAILED);

  if(testingMode == TMWSIM_TESTINGMODE_DELAY)
    return(TMWDEFS_CMD_STAT_EXECUTING);
   
  return(TMWDEFS_CMD_STAT_SUCCESS);
}

/* function: _s14sim_getCmdStatus  
 * purpose: Determine whether to indicate command succeeded, failed or in progress
 *  depending on testingMode, set by s101testingmode testharness command.
 * arguments:
 *  pPoint
 * returns:
 *  TMWDEFS_COMMAND_STATUS
 */
static TMWDEFS_COMMAND_STATUS TMWDEFS_LOCAL _s14sim_getCmdStatus(
  void *pPoint)
{ 
  static int delayCount;
  TMWSIM_TESTINGMODE testingMode;

  /* See if this point has been set to not return CMD SUCCESS for test purposes */
  testingMode = tmwsim_getTestingMode((TMWSIM_POINT *)pPoint);
  if(testingMode == TMWSIM_TESTINGMODE_FAILURE)
    return(TMWDEFS_CMD_STAT_FAILED);

  if(testingMode == TMWSIM_TESTINGMODE_DELAY)
  { 
    if(delayCount++ <6)
    {
      return(TMWDEFS_CMD_STAT_EXECUTING);
    } 
    delayCount = 0;
  }
  return(TMWDEFS_CMD_STAT_SUCCESS);
}

/* Monitored Single Point */
/* function: s14sim_mspAddPoint */
void * TMWDEFS_GLOBAL s14sim_mspAddPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa,
  TMWDEFS_GROUP_MASK groupMask,
  TMWTYPES_UCHAR flags,
  TMWTYPES_BOOL value)
{
  TMWSIM_POINT *pPoint;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle; 
  pPoint = tmwsim_tableAdd(&pDbHandle->msp, ioa);
  if(pPoint != TMWDEFS_NULL)
  {
    tmwsim_initBinary(pPoint, pHandle, ioa); 
    pPoint->pSCLHandle = (void*)pDbHandle->pS14Sector;

    tmwsim_setGroupMask(pPoint, groupMask);
    pPoint->flags = flags; 
    pPoint->data.binary.value = value;
    s14util_getDateTime((TMWSCTR*)pPoint->pSCLHandle, &pPoint->timeStamp); 
 
    /* Store transmission mode in default event variation */
    ((TMWSIM_POINT *)pPoint)->defaultEventVariation = S14DATA_TRANSMISSION_SINGLE;
    
    pDbHandle->mspIndexed = _checkForIndexed(&pDbHandle->msp);
    _callCallback(pPoint, TMWSIM_POINT_ADD, I14DEF_TYPE_MSPNA1);
  }
  return(pPoint);
}

/* function: s14sim_mspDeletePoint */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_mspDeletePoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  TMWTYPES_BOOL status;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  _callRemoveCallback(pDbHandle, TMWSIM_POINT_DELETE, I14DEF_TYPE_MSPNA1, ioa);

  status = tmwsim_tableDelete(&pDbHandle->msp, ioa);
  if(status)
  {
    pDbHandle->mspIndexed = _checkForIndexed(&pDbHandle->msp);
  }
  
  return(status);
}

/* function: s14sim_mspGetPoint */
void * TMWDEFS_GLOBAL s14sim_mspGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPointByIndex(&pDbHandle->msp, index));
}

/* function: s14sim_mspLookupPoint */
void * TMWDEFS_GLOBAL s14sim_mspLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPoint(&pDbHandle->msp, ioa));
}

/* function: s14sim_mspGetIndexed */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14sim_mspGetIndexed(
  void *pPoint)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pSimPoint->pDbHandle;
  return(pDbHandle->mspIndexed);
}

/* function: s14sim_mspGetFlagsAndValue */
TMWTYPES_UCHAR TMWDEFS_GLOBAL s14sim_mspGetFlagsAndValue(
  void *pPoint)
{
  TMWTYPES_UCHAR flags = (TMWTYPES_UCHAR)(s14sim_getFlags(pPoint) & ~I14DEF_SIQ_MASK);
  if(tmwsim_getBinaryValue((TMWSIM_POINT *)pPoint))
  {
    flags |= I14DEF_SIQ_ON;
  }
  return(flags);
}

/* function: s14sim_mspGetValue */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_mspGetValue(
  void *pPoint)
{
  return(tmwsim_getBinaryValue((TMWSIM_POINT *)pPoint));
}

/* function: s14sim_mspSetFlags */
void TMWDEFS_GLOBAL s14sim_mspSetFlags(
  void *pPoint, 
  TMWTYPES_UCHAR flags, 
  TMWDEFS_CHANGE_REASON reason)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  tmwsim_setFlags(pSimPoint, flags, reason);
  s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp); 
  _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_MSPNA1);
}

/* function: s14sim_mspSetValue */
void TMWDEFS_GLOBAL s14sim_mspSetValue(
  void *pPoint,
  TMWTYPES_BOOL value,
  TMWDEFS_CHANGE_REASON reason)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  tmwsim_setBinaryValue(pSimPoint, value, reason);
  s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp); 
  _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_MSPNA1);
}

/* Monitored Double Point */
/* function: s14sim_mdpAddPoint */
void * TMWDEFS_GLOBAL s14sim_mdpAddPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa,
  TMWDEFS_GROUP_MASK groupMask,
  TMWTYPES_UCHAR flags,
  TMWDEFS_DPI_TYPE value)
{
  TMWSIM_POINT *pPoint;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle; 
  pPoint = tmwsim_tableAdd(&pDbHandle->mdp, ioa);
  if(pPoint != TMWDEFS_NULL)
  {
    tmwsim_initDoubleBinary(pPoint, pHandle, ioa);
    pPoint->pSCLHandle = (void*)pDbHandle->pS14Sector;

    tmwsim_setGroupMask(pPoint, groupMask);
    pPoint->flags = flags; 
    pPoint->data.doubleBinary.value = value;
    s14util_getDateTime((TMWSCTR*)pPoint->pSCLHandle, &pPoint->timeStamp); 

    /* Store transmission mode in default event variation */
    pPoint->defaultEventVariation = S14DATA_TRANSMISSION_SINGLE;

    pDbHandle->mdpIndexed = _checkForIndexed(&pDbHandle->mdp);
    _callCallback(pPoint, TMWSIM_POINT_ADD, I14DEF_TYPE_MDPNA1);
  }
  return(pPoint);
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_mdpDeletePoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  
  TMWTYPES_BOOL status;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  _callRemoveCallback(pDbHandle, TMWSIM_POINT_DELETE, I14DEF_TYPE_MDPNA1, ioa);
  
  status = tmwsim_tableDelete(&pDbHandle->mdp, ioa);
  if(status)
  {
    pDbHandle->mdpIndexed = _checkForIndexed(&pDbHandle->mdp);
  }

  return(status);
}

void * TMWDEFS_GLOBAL s14sim_mdpGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPointByIndex(&pDbHandle->mdp, index));
}

void * TMWDEFS_GLOBAL s14sim_mdpLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPoint(&pDbHandle->mdp, ioa));
}

/* function: s14sim_mdpGetIndexed */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14sim_mdpGetIndexed(
  void *pPoint)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pSimPoint->pDbHandle;
  return(pDbHandle->mdpIndexed);
}

TMWTYPES_UCHAR TMWDEFS_GLOBAL s14sim_mdpGetFlagsAndValue(
  void *pPoint)
{
  /* Strip off the two low bits of flags to put Double Point value into */
  TMWTYPES_UCHAR flags = (TMWTYPES_UCHAR)(s14sim_getFlags(pPoint) & ~TMWDEFS_DPI_MASK);
  flags |= (TMWTYPES_UCHAR)tmwsim_getDoubleBinaryValue((TMWSIM_POINT *)pPoint);
  return(flags);
}

TMWDEFS_DPI_TYPE TMWDEFS_GLOBAL s14sim_mdpGetValue(
  void *pPoint)
{
  return(tmwsim_getDoubleBinaryValue((TMWSIM_POINT *)pPoint));
}

/* function: s14sim_mdpSetFlags */
void TMWDEFS_GLOBAL s14sim_mdpSetFlags(
  void *pPoint, 
  TMWTYPES_UCHAR flags, 
  TMWDEFS_CHANGE_REASON reason)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  tmwsim_setFlags(pSimPoint, flags, reason);
  s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp); 
  _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_MDPNA1);
}

void TMWDEFS_GLOBAL s14sim_mdpSetValue(
  void *pPoint,
  TMWDEFS_DPI_TYPE value,
  TMWDEFS_CHANGE_REASON reason)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  tmwsim_setDoubleBinaryValue(pSimPoint, value, reason);
  s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp); 
  _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_MDPNA1);
}

/* Step Position Information */
void * TMWDEFS_GLOBAL s14sim_mstAddPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa,
  TMWDEFS_GROUP_MASK groupMask,
  TMWTYPES_UCHAR flags,
  TMWTYPES_UCHAR value)
{
  TMWSIM_POINT *pPoint;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle; 
  pPoint = tmwsim_tableAdd(&pDbHandle->mst, ioa);
  if(pPoint != TMWDEFS_NULL)
  {
    tmwsim_initCounter(pPoint, pHandle, ioa);
    pPoint->pSCLHandle = (void*)pDbHandle->pS14Sector;

    tmwsim_setGroupMask(pPoint, groupMask);
    pPoint->flags = flags; 
    pPoint->data.counter.value = value;
    s14util_getDateTime((TMWSCTR*)pPoint->pSCLHandle, &pPoint->timeStamp); 
    
    /* Store transmission mode in default event variation */
    ((TMWSIM_POINT *)pPoint)->defaultEventVariation = S14DATA_TRANSMISSION_SINGLE;

    pDbHandle->mstIndexed = _checkForIndexed(&pDbHandle->mst);
    _callCallback(pPoint, TMWSIM_POINT_ADD, I14DEF_TYPE_MSTNA1);
  }
  return(pPoint);
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_mstDeletePoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  TMWTYPES_BOOL status;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  _callRemoveCallback(pDbHandle, TMWSIM_POINT_DELETE, I14DEF_TYPE_MSTNA1, ioa);
  
  status = tmwsim_tableDelete(&pDbHandle->mst, ioa);
  if(status)
  {
    pDbHandle->mstIndexed = _checkForIndexed(&pDbHandle->mst);
  }

  return(status);
}

void * TMWDEFS_GLOBAL s14sim_mstGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPointByIndex(&pDbHandle->mst, index));
}

void * TMWDEFS_GLOBAL s14sim_mstLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPoint(&pDbHandle->mst, ioa));
}

/* function: s14sim_mstGetIndexed */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14sim_mstGetIndexed(
  void *pPoint)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pSimPoint->pDbHandle;
  return(pDbHandle->mstIndexed);
}

TMWTYPES_UCHAR TMWDEFS_GLOBAL s14sim_mstGetValue(
  void *pPoint)
{
  return((TMWTYPES_UCHAR)tmwsim_getCounterValue((TMWSIM_POINT *)pPoint));
}

/* function: s14sim_mstSetFlags */
void TMWDEFS_GLOBAL s14sim_mstSetFlags(
  void *pPoint, 
  TMWTYPES_UCHAR flags, 
  TMWDEFS_CHANGE_REASON reason)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  tmwsim_setFlags(pSimPoint, flags, reason);
  s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp); 
  _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_MSTNA1);
}

void TMWDEFS_GLOBAL s14sim_mstSetValue(
  void *pPoint,
  TMWTYPES_UCHAR value,
  TMWDEFS_CHANGE_REASON reason)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  tmwsim_setCounterValue(pSimPoint, value, reason);
  s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp); 
  _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_MSTNA1);
}

/* Bitstring of 32 bits */
void * TMWDEFS_GLOBAL s14sim_mboAddPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa,
  TMWDEFS_GROUP_MASK groupMask,
  TMWTYPES_UCHAR flags,
  TMWTYPES_ULONG value)
{
  TMWSIM_POINT *pPoint;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle; 
  pPoint = tmwsim_tableAdd(&pDbHandle->mbo, ioa);
  if(pPoint != TMWDEFS_NULL)
  {
    tmwsim_initBitstring(pPoint, pHandle, ioa);
    pPoint->pSCLHandle = (void*)pDbHandle->pS14Sector;

    tmwsim_setGroupMask(pPoint, groupMask);
    ((TMWSIM_POINT *)pPoint)->flags = flags; 
    ((TMWSIM_POINT *)pPoint)->data.bitstring.value = value;
    s14util_getDateTime((TMWSCTR*)pPoint->pSCLHandle, &pPoint->timeStamp); 

    /* Store transmission mode in default event variation */
    ((TMWSIM_POINT *)pPoint)->defaultEventVariation = S14DATA_TRANSMISSION_SINGLE;

    pDbHandle->mboIndexed = _checkForIndexed(&pDbHandle->mbo);
    _callCallback((TMWSIM_POINT *)pPoint, TMWSIM_POINT_ADD, I14DEF_TYPE_MBONA1);

  }
  return(pPoint);
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_mboDeletePoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  TMWTYPES_BOOL status;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  _callRemoveCallback(pDbHandle, TMWSIM_POINT_DELETE, I14DEF_TYPE_MBONA1, ioa);
  
  status = tmwsim_tableDelete(&pDbHandle->mbo, ioa);
  if(status)
  {
    pDbHandle->mboIndexed = _checkForIndexed(&pDbHandle->mbo);
  }

  return(status);
}

void * TMWDEFS_GLOBAL s14sim_mboGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPointByIndex(&pDbHandle->mbo, index));
}

void * TMWDEFS_GLOBAL s14sim_mboLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPoint(&pDbHandle->mbo, ioa));
}

/* function: s14sim_mboGetIndexed */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14sim_mboGetIndexed(
  void *pPoint)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pSimPoint->pDbHandle;
  return(pDbHandle->mboIndexed);
}

TMWTYPES_ULONG TMWDEFS_GLOBAL s14sim_mboGetValue(
  void *pPoint)
{
  return(tmwsim_getBitstringValue((TMWSIM_POINT *)pPoint));
}

/* function: s14sim_mboSetFlags */
void TMWDEFS_GLOBAL s14sim_mboSetFlags(
  void *pPoint, 
  TMWTYPES_UCHAR flags, 
  TMWDEFS_CHANGE_REASON reason)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  tmwsim_setFlags(pSimPoint, flags, reason);
  s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp); 
  _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_MBONA1);
}

void TMWDEFS_GLOBAL s14sim_mboSetValue(
  void *pPoint,
  TMWTYPES_ULONG value)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  tmwsim_setBitstringValue(pSimPoint, value, TMWDEFS_CHANGE_SPONTANEOUS);
  s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp); 
  _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_MBONA1);
}

/* Normalized Measurands */
void * TMWDEFS_GLOBAL s14sim_mmenaAddPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa,
  TMWTYPES_ULONG pacnaIOA,
  TMWTYPES_ULONG pmenaIOA[],
  TMWDEFS_GROUP_MASK groupMask,
  TMWTYPES_UCHAR flags,
  TMWTYPES_SHORT value)
{
  TMWSIM_POINT *pPoint;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle; 
  pPoint = tmwsim_tableAdd(&pDbHandle->mmena, ioa);
  if(pPoint != TMWDEFS_NULL)
  {
    TMWTYPES_UCHAR i;
    TMWSIM_POINT *ppacnaPoint = tmwsim_tableAdd(&pDbHandle->pacna, pacnaIOA);
    if(ppacnaPoint == TMWDEFS_NULL)
    {
      tmwsim_tableDelete(&pDbHandle->mmena, ioa);
      return(TMWDEFS_NULL);
    }

    tmwsim_initAnalog(pPoint, pHandle, ioa, TMWDEFS_SHORT_MIN, TMWDEFS_SHORT_MAX, 99, 50);
    pPoint->pSCLHandle = (void*)pDbHandle->pS14Sector;

    tmwsim_setGroupMask(pPoint, groupMask);
    pPoint->flags = flags; 
    pPoint->data.analog.value = value;
    s14util_getDateTime((TMWSCTR*)pPoint->pSCLHandle, &pPoint->timeStamp); 

    /* Store transmission mode in default event variation */
    pPoint->defaultEventVariation = S14DATA_TRANSMISSION_SINGLE;

    tmwsim_initParameter(ppacnaPoint, pHandle, pacnaIOA, pPoint);
    ppacnaPoint->pSCLHandle = (void*)pDbHandle->pS14Sector;
    pPoint->data.analog.pacnaIOA = pacnaIOA;

    for(i = 0; i < 4; i++)
    {
      TMWSIM_POINT *ppmePoint = tmwsim_tableAdd(&pDbHandle->pmena, pmenaIOA[i]);
      if(ppmePoint == TMWDEFS_NULL)
      {
        s14sim_mmenaDeletePoint(pHandle, ioa);
        return(TMWDEFS_NULL);
      }

      ppmePoint->data.parameter.kind = i;
      tmwsim_initParameter(ppmePoint, pHandle, pmenaIOA[i], pPoint);
      tmwsim_setGroupMask(ppmePoint, TMWDEFS_GROUP_MASK_GENERAL);
      ppmePoint->pSCLHandle = (void*)pDbHandle->pS14Sector;
      pPoint->data.analog.parameterIOA[i] = pmenaIOA[i];
      s14util_getDateTime((TMWSCTR*)ppmePoint->pSCLHandle, &ppmePoint->timeStamp); 

      _callCallback((TMWSIM_POINT *)ppmePoint, TMWSIM_POINT_ADD, I14DEF_TYPE_PMENA1);
    }
    pDbHandle->mmenaIndexed = _checkForIndexed(&pDbHandle->mmena);
    _callCallback((TMWSIM_POINT *)pPoint, TMWSIM_POINT_ADD, I14DEF_TYPE_MMENA1);
  }
  return(pPoint);
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_mmenaDeletePoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  TMWTYPES_BOOL status;
  TMWSIM_POINT *pParamPoint;
  TMWSIM_POINT *pPreviousPoint;
  TMWSIM_POINT *pPoint = tmwsim_tableFindPoint(&pDbHandle->mmena, ioa);

  /* Search through PACNA points, deleting all that apply to this measurand */
  pPreviousPoint = TMWDEFS_NULL;
  while((pParamPoint = tmwsim_tableGetNextPoint(
    &pDbHandle->pacna, pPreviousPoint)) != TMWDEFS_NULL)
  {
    if(pParamPoint->data.parameter.pPoint == pPoint)
    {
      TMWTYPES_ULONG paramIOA = tmwsim_getPointNumber(pParamPoint);
      tmwsim_tableDelete(&pDbHandle->pacna, paramIOA);
    }
    else
    {
      pPreviousPoint = pParamPoint;
    }
  }

  /* Search through PMENA points, deleting all that apply to this measurand */
  pPreviousPoint = TMWDEFS_NULL;
  while((pParamPoint = tmwsim_tableGetNextPoint(
    &pDbHandle->pmena, pPreviousPoint)) != TMWDEFS_NULL)
  {
    if(pParamPoint->data.parameter.pPoint == pPoint)
    {
      TMWTYPES_ULONG paramIOA = tmwsim_getPointNumber(pParamPoint);
      _callRemoveCallback(pDbHandle, TMWSIM_POINT_DELETE, I14DEF_TYPE_PMENA1, paramIOA);
      tmwsim_tableDelete(&pDbHandle->pmena, paramIOA);
    } 
    else
    {
      pPreviousPoint = pParamPoint;
    }
  }

  /* Finally delete this measurand */
  _callRemoveCallback(pDbHandle, TMWSIM_POINT_DELETE, I14DEF_TYPE_MMENA1, ioa);
  status = tmwsim_tableDelete(&pDbHandle->mmena, ioa);
  if(status)
  {
    pDbHandle->mmenaIndexed = _checkForIndexed(&pDbHandle->mmena);
  }

  return(status);
}

void * TMWDEFS_GLOBAL s14sim_mmenaGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPointByIndex(&pDbHandle->mmena, index));
}

void * TMWDEFS_GLOBAL s14sim_mmenaLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPoint(&pDbHandle->mmena, ioa));
}

/* function: s14sim_mmenaGetIndexed */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14sim_mmenaGetIndexed(
  void *pPoint)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pSimPoint->pDbHandle;
  return(pDbHandle->mmenaIndexed);
}

TMWTYPES_SHORT TMWDEFS_GLOBAL s14sim_mmenaGetValue(
  void *pPoint)
{
  return((TMWTYPES_SHORT)tmwsim_getAnalogValue((TMWSIM_POINT *)pPoint));
}

/* function: s14sim_mmenaSetFlags */
void TMWDEFS_GLOBAL s14sim_mmenaSetFlags(
  void *pPoint, 
  TMWTYPES_UCHAR flags, 
  TMWDEFS_CHANGE_REASON reason)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  tmwsim_setFlags(pSimPoint, flags, reason);
  s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp); 
  _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_MMENA1);
}

void TMWDEFS_GLOBAL s14sim_mmenaSetValue(
  void *pPoint,
  TMWTYPES_SHORT value)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  tmwsim_setAnalogValue(pSimPoint, value, TMWDEFS_CHANGE_SPONTANEOUS);
  s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp); 
  _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_MMENA1);
}

/* Scaled Measurands */
void * TMWDEFS_GLOBAL s14sim_mmenbAddPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa,
  TMWTYPES_ULONG pacnaIOA,
  TMWTYPES_ULONG pmenbIOA[],
  TMWDEFS_GROUP_MASK groupMask,
  TMWTYPES_UCHAR flags,
  TMWTYPES_SHORT value)
{
  TMWSIM_POINT *pPoint;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle; 
  pPoint = tmwsim_tableAdd(&pDbHandle->mmenb, ioa);
  if(pPoint != TMWDEFS_NULL)
  {
    TMWTYPES_UCHAR i;
    TMWSIM_POINT *ppacnaPoint = tmwsim_tableAdd(&pDbHandle->pacna, pacnaIOA);
    if(ppacnaPoint == TMWDEFS_NULL)
    {      
      tmwsim_tableDelete(&pDbHandle->mmenb, ioa);
      return(TMWDEFS_NULL);
    }

    tmwsim_initAnalog(pPoint, pHandle, ioa, TMWDEFS_SHORT_MIN, TMWDEFS_SHORT_MAX, 99, 50);
    pPoint->pSCLHandle = (void*)pDbHandle->pS14Sector;

    tmwsim_setGroupMask(pPoint, groupMask);
    ((TMWSIM_POINT *)pPoint)->flags = flags; 
    ((TMWSIM_POINT *)pPoint)->data.analog.value = value;
    s14util_getDateTime((TMWSCTR*)pPoint->pSCLHandle, &pPoint->timeStamp); 
    
    /* Store transmission mode in default event variation */
    ((TMWSIM_POINT *)pPoint)->defaultEventVariation = S14DATA_TRANSMISSION_SINGLE;

    tmwsim_initParameter(ppacnaPoint, pHandle, pacnaIOA, pPoint);
    ppacnaPoint->pSCLHandle = (void*)pDbHandle->pS14Sector;
    pPoint->data.analog.pacnaIOA = pacnaIOA;

    for(i = 0; i < 4; i++)
    {      
      TMWSIM_POINT *ppmePoint = tmwsim_tableAdd(&pDbHandle->pmenb, pmenbIOA[i]);
      if(ppmePoint == TMWDEFS_NULL)
      {
        s14sim_mmenbDeletePoint(pHandle, ioa);
        return(TMWDEFS_NULL);
      }
      ppmePoint->data.parameter.kind = i;
      tmwsim_initParameter(ppmePoint, pHandle, pmenbIOA[i], pPoint);
      tmwsim_setGroupMask(ppmePoint, TMWDEFS_GROUP_MASK_GENERAL);
      ppmePoint->pSCLHandle = (void*)pDbHandle->pS14Sector;
      pPoint->data.analog.parameterIOA[i] = pmenbIOA[i];
      s14util_getDateTime((TMWSCTR*)ppmePoint->pSCLHandle, &ppmePoint->timeStamp); 

      _callCallback((TMWSIM_POINT *)ppmePoint, TMWSIM_POINT_ADD, I14DEF_TYPE_PMENB1);
    }

    pDbHandle->mmenbIndexed = _checkForIndexed(&pDbHandle->mmenb);
    _callCallback((TMWSIM_POINT *)pPoint, TMWSIM_POINT_ADD, I14DEF_TYPE_MMENB1);
  }

  return(pPoint);
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_mmenbDeletePoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  TMWTYPES_BOOL status;
  TMWSIM_POINT *pParamPoint;
  TMWSIM_POINT *pPreviousPoint;
  TMWSIM_POINT *pPoint = tmwsim_tableFindPoint(&pDbHandle->mmenb, ioa);

  /* Search through PACNA points, deleting all that apply to this measurand */
  pPreviousPoint = TMWDEFS_NULL;
  while((pParamPoint = tmwsim_tableGetNextPoint(
    &pDbHandle->pacna, pPreviousPoint)) != TMWDEFS_NULL)
  {
    if(pParamPoint->data.parameter.pPoint == pPoint)
    {
      TMWTYPES_ULONG paramIOA = tmwsim_getPointNumber(pParamPoint);
      tmwsim_tableDelete(&pDbHandle->pacna, paramIOA);
    }
    else
    {
      pPreviousPoint = pParamPoint;
    }
  }

  /* Search through PMENB points, deleting all that apply to this measurand */
  pPreviousPoint = TMWDEFS_NULL;
 while((pParamPoint = tmwsim_tableGetNextPoint(
    &pDbHandle->pmenb, pPreviousPoint)) != TMWDEFS_NULL)
  {
    if(pParamPoint->data.parameter.pPoint == pPoint)
    {
      TMWTYPES_ULONG paramIOA = tmwsim_getPointNumber(pParamPoint);
      _callRemoveCallback(pDbHandle, TMWSIM_POINT_DELETE, I14DEF_TYPE_PMENB1, paramIOA);
      tmwsim_tableDelete(&pDbHandle->pmenb, paramIOA);      
    }
    else
    {
      pPreviousPoint = pParamPoint;
    }
  }

  /* Finally delete this measurand */  
  _callRemoveCallback(pDbHandle, TMWSIM_POINT_DELETE, I14DEF_TYPE_MMENB1, ioa);
  status = tmwsim_tableDelete(&pDbHandle->mmenb, ioa);

  if(status)
  {
    pDbHandle->mmenbIndexed = _checkForIndexed(&pDbHandle->mmenb);
  }

  return(status);
}

void * TMWDEFS_GLOBAL s14sim_mmenbGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPointByIndex(&pDbHandle->mmenb, index));
}

void * TMWDEFS_GLOBAL s14sim_mmenbLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPoint(&pDbHandle->mmenb, ioa));
}

/* function: s14sim_mmenbGetIndexed */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14sim_mmenbGetIndexed(
  void *pPoint)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pSimPoint->pDbHandle;
  return(pDbHandle->mmenbIndexed);
}

TMWTYPES_SHORT TMWDEFS_GLOBAL s14sim_mmenbGetValue(
  void *pPoint)
{
  return((TMWTYPES_SHORT)tmwsim_getAnalogValue((TMWSIM_POINT *)pPoint));
}

/* function: s14sim_mmenbSetFlags */
void TMWDEFS_GLOBAL s14sim_mmenbSetFlags(
  void *pPoint, 
  TMWTYPES_UCHAR flags, 
  TMWDEFS_CHANGE_REASON reason)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  tmwsim_setFlags(pSimPoint, flags, reason);
  s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp); 
  _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_MMENB1);
}

void TMWDEFS_GLOBAL s14sim_mmenbSetValue(
  void *pPoint,
  TMWTYPES_SHORT value)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  tmwsim_setAnalogValue(pSimPoint, value, TMWDEFS_CHANGE_SPONTANEOUS);
  s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp); 
  _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_MMENB1);
}
#if S14DATA_SUPPORT_MME_C
/* Floating Point Measurands */
void * TMWDEFS_GLOBAL s14sim_mmencAddPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa,
  TMWTYPES_ULONG pacnaIOA,
  TMWTYPES_ULONG pmencIOA[],
  TMWDEFS_GROUP_MASK groupMask,
  TMWTYPES_UCHAR flags,
  TMWTYPES_SFLOAT value)
{
  TMWSIM_POINT *pPoint;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle; 
  pPoint = tmwsim_tableAdd(&pDbHandle->mmenc, ioa);
  if(pPoint != TMWDEFS_NULL)
  {
    TMWTYPES_UCHAR i;    
    TMWSIM_POINT *ppacnaPoint = tmwsim_tableAdd(&pDbHandle->pacna, pacnaIOA);
    if(ppacnaPoint == TMWDEFS_NULL)
    {      
      tmwsim_tableDelete(&pDbHandle->mmenc, ioa);
      return(TMWDEFS_NULL);
    }

    tmwsim_initAnalog(pPoint, pHandle, ioa, TMWDEFS_SHORT_MIN, TMWDEFS_SHORT_MAX, 99, 50);
    pPoint->pSCLHandle = (void*)pDbHandle->pS14Sector;

    tmwsim_setGroupMask(pPoint, groupMask);
    ((TMWSIM_POINT *)pPoint)->flags = flags; 
    pPoint->data.analog.value = (TMWSIM_DATA_TYPE)value;
    s14util_getDateTime((TMWSCTR*)pPoint->pSCLHandle, &pPoint->timeStamp); 
    
    /* Store transmission mode in default event variation */
    ((TMWSIM_POINT *)pPoint)->defaultEventVariation = S14DATA_TRANSMISSION_SINGLE;

    tmwsim_initParameter(ppacnaPoint, pHandle, pacnaIOA, pPoint);
    ppacnaPoint->pSCLHandle = (void*)pDbHandle->pS14Sector;
    pPoint->data.analog.pacnaIOA = pacnaIOA;

    for(i = 0; i < 4; i++)
    {      
      TMWSIM_POINT *ppmePoint = tmwsim_tableAdd(&pDbHandle->pmenc, pmencIOA[i]);
      if(ppmePoint == TMWDEFS_NULL)
      {        
        s14sim_mmencDeletePoint(pHandle, ioa);
        return(TMWDEFS_NULL);
      }

      ppmePoint->data.parameter.kind = i;
      tmwsim_initParameter(ppmePoint, pHandle, pmencIOA[i], pPoint);
      tmwsim_setGroupMask(ppmePoint, TMWDEFS_GROUP_MASK_GENERAL);      
      ppmePoint->pSCLHandle = (void*)pDbHandle->pS14Sector;
      pPoint->data.analog.parameterIOA[i] = pmencIOA[i];
      s14util_getDateTime((TMWSCTR*)ppmePoint->pSCLHandle, &ppmePoint->timeStamp); 

      _callCallback((TMWSIM_POINT *)ppmePoint, TMWSIM_POINT_ADD, I14DEF_TYPE_PMENC1);
    }

    pDbHandle->mmencIndexed = _checkForIndexed(&pDbHandle->mmenc);
    _callCallback((TMWSIM_POINT *)pPoint, TMWSIM_POINT_ADD, I14DEF_TYPE_MMENC1);
  }

  return(pPoint);
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_mmencDeletePoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  TMWTYPES_BOOL status;
  TMWSIM_POINT *pParamPoint;
  TMWSIM_POINT *pPreviousPoint;
  TMWSIM_POINT *pPoint = tmwsim_tableFindPoint(&pDbHandle->mmenc, ioa);

  /* Search through PACNA points, deleting all that apply to this measurand */
  pPreviousPoint = TMWDEFS_NULL;
  while((pParamPoint = tmwsim_tableGetNextPoint(
    &pDbHandle->pacna, pPreviousPoint)) != TMWDEFS_NULL)
  {
    if(pParamPoint->data.parameter.pPoint == pPoint)
    {
      TMWTYPES_ULONG paramIOA = tmwsim_getPointNumber(pParamPoint);
      tmwsim_tableDelete(&pDbHandle->pacna, paramIOA);
    }
    else
    {
      pPreviousPoint = pParamPoint;
    }
  }

  /* Search through PMENC points, deleting all that apply to this measurand */
  pPreviousPoint = TMWDEFS_NULL;
  while((pParamPoint = tmwsim_tableGetNextPoint(
    &pDbHandle->pmenc, pPreviousPoint)) != TMWDEFS_NULL)
  {
    if(pParamPoint->data.parameter.pPoint == pPoint)
    {
      TMWTYPES_ULONG paramIOA = tmwsim_getPointNumber(pParamPoint);
      _callRemoveCallback(pDbHandle, TMWSIM_POINT_DELETE, I14DEF_TYPE_PMENC1, paramIOA);
      tmwsim_tableDelete(&pDbHandle->pmenc, paramIOA);     
    }
    else
    {
      pPreviousPoint = pParamPoint;
    }
  }

  /* Finally delete this measurand */
  _callRemoveCallback(pDbHandle, TMWSIM_POINT_DELETE, I14DEF_TYPE_MMENC1, ioa);
  status = tmwsim_tableDelete(&pDbHandle->mmenc, ioa);
  if(status)
  {
    pDbHandle->mmencIndexed = _checkForIndexed(&pDbHandle->mmenc);
  }

  return(status);
}

void * TMWDEFS_GLOBAL s14sim_mmencGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPointByIndex(&pDbHandle->mmenc, index));
}

void * TMWDEFS_GLOBAL s14sim_mmencLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPoint(&pDbHandle->mmenc, ioa));
}

/* function: s14sim_mmencGetIndexed */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14sim_mmencGetIndexed(
  void *pPoint)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pSimPoint->pDbHandle;
  return(pDbHandle->mmencIndexed);
}

TMWTYPES_SFLOAT TMWDEFS_GLOBAL s14sim_mmencGetValue(
  void *pPoint)
{
  return((TMWTYPES_SFLOAT)tmwsim_getAnalogValue((TMWSIM_POINT *)pPoint));
}

/* function: s14sim_mmencSetFlags */
void TMWDEFS_GLOBAL s14sim_mmencSetFlags(
  void *pPoint, 
  TMWTYPES_UCHAR flags, 
  TMWDEFS_CHANGE_REASON reason)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  tmwsim_setFlags(pSimPoint, flags, reason);
  s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp); 
  _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_MMENC1);
}

void TMWDEFS_GLOBAL s14sim_mmencSetValue(
  void *pPoint,
  TMWTYPES_SFLOAT value)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  tmwsim_setAnalogValue(pSimPoint, (TMWSIM_DATA_TYPE)value, TMWDEFS_CHANGE_SPONTANEOUS);
  s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp); 
  _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_MMENC1);
}
#endif

TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_mmenGetIOAs(
  void *pPoint,
  TMWTYPES_ULONG *pPacnaIOA,
  TMWTYPES_ULONG pPmenIOA[4])
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint; 
  pPmenIOA[0] = pSimPoint->data.analog.parameterIOA[0];
  pPmenIOA[1] = pSimPoint->data.analog.parameterIOA[1];
  pPmenIOA[2] = pSimPoint->data.analog.parameterIOA[2];
  pPmenIOA[3] = pSimPoint->data.analog.parameterIOA[3];
  *pPacnaIOA = pSimPoint->data.analog.pacnaIOA;
  return(TMWDEFS_TRUE);
}

/* Integrated Totals */
void * TMWDEFS_GLOBAL s14sim_mitAddPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa,
  TMWDEFS_GROUP_MASK groupMask,
  TMWTYPES_UCHAR flags,
  TMWTYPES_ULONG value)
{
  TMWSIM_POINT *pPoint;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle; 
  pPoint = tmwsim_tableAdd(&pDbHandle->mit, ioa);
  if(pPoint != TMWDEFS_NULL)
  {
    tmwsim_initCounter(pPoint, pHandle, ioa);
    pPoint->pSCLHandle = (void*)pDbHandle->pS14Sector;

    tmwsim_setGroupMask(pPoint, groupMask);
    ((TMWSIM_POINT *)pPoint)->flags = flags; 
    pPoint->data.counter.value = value;
    s14util_getDateTime((TMWSCTR*)pPoint->pSCLHandle, &pPoint->timeStamp); 
    
    /* Store transmission mode in default event variation */
    ((TMWSIM_POINT *)pPoint)->defaultEventVariation = S14DATA_TRANSMISSION_SINGLE;

    pDbHandle->mitIndexed = _checkForIndexed(&pDbHandle->mit);
    _callCallback((TMWSIM_POINT *)pPoint, TMWSIM_POINT_ADD, I14DEF_TYPE_MITNA1);
  }
  return(pPoint);
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_mitDeletePoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  TMWTYPES_BOOL status;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  _callRemoveCallback(pDbHandle, TMWSIM_POINT_DELETE, I14DEF_TYPE_MITNA1, ioa);

  status = tmwsim_tableDelete(&pDbHandle->mit, ioa);
  if(status)
  {
    pDbHandle->mitIndexed = _checkForIndexed(&pDbHandle->mit);
  }

  return(status);
}

void * TMWDEFS_GLOBAL s14sim_mitGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPointByIndex(&pDbHandle->mit, index));
}

void * TMWDEFS_GLOBAL s14sim_mitLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPoint(&pDbHandle->mit, ioa));
}

/* function: s14sim_mitGetIndexed */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14sim_mitGetIndexed(
  void *pPoint)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pSimPoint->pDbHandle;
  return(pDbHandle->mitIndexed);
}

TMWTYPES_ULONG TMWDEFS_GLOBAL s14sim_mitGetValue(
  void *pPoint)
{
  return(tmwsim_getCounterValue((TMWSIM_POINT *)pPoint));
}

TMWTYPES_ULONG TMWDEFS_GLOBAL s14sim_mitGetFrozenValue(
  void *pPoint)
{
  return(tmwsim_getFrozenCounterValue((TMWSIM_POINT *)pPoint));
}

/* function: s14sim_mitSetFlags */
void TMWDEFS_GLOBAL s14sim_mitSetFlags(
  void *pPoint, 
  TMWTYPES_UCHAR flags, 
  TMWDEFS_CHANGE_REASON reason)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  tmwsim_setFlags(pSimPoint, flags, reason);
  s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp); 
  _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_MITNA1);
}


void TMWDEFS_GLOBAL s14sim_mitSetValue(
  void *pPoint,
  TMWTYPES_ULONG value)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  tmwsim_setCounterValue(pSimPoint, value, TMWDEFS_CHANGE_SPONTANEOUS);
  s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp); 
  _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_MITNA1);
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_mitFreeze(
  void *pPoint,
  TMWTYPES_UCHAR qcc)
{
  TMWDTIME timeStamp; 
  /* To support generating an event when a counter is frozen */
  TMWTYPES_BOOL generateEvent = TMWDEFS_FALSE;

  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT*)pPoint;
  TMWSCTR *pSector = (TMWSCTR *)pSimPoint->pSCLHandle; 

  switch(qcc & I14DEF_QCC_FRZ_MASK)
  {
  case I14DEF_QCC_FRZ_NO_RESET:
    tmwsim_freezeCounter((TMWSIM_POINT *)pPoint);
    generateEvent = TMWDEFS_TRUE;
    break;

  case I14DEF_QCC_FRZ_RESET:
    tmwsim_freezeCounter((TMWSIM_POINT *)pPoint);
    s14sim_mitSetValue(pPoint, 0);
    generateEvent = TMWDEFS_TRUE;
    break;

  case I14DEF_QCC_FRZ_RESET_ONLY:
    s14sim_mitSetValue(pPoint, 0);
    break;

  default:
    return(TMWDEFS_FALSE);
  } 
  
  /* set time of last change in database */  
  tmwdtime_getDateTime(TMWDEFS_NULL, &timeStamp);
  tmwsim_setTimeStamp((TMWSIM_POINT *)pPoint, &timeStamp);
 
  if(generateEvent)
  {
    s14mit_addEvent(pSector, 3, pSimPoint->pointNumber, pSimPoint->data.counter.frozenValue, 0, &timeStamp); 
  }

  return(TMWDEFS_TRUE);
}

TMWTYPES_UCHAR TMWDEFS_GLOBAL s14sim_mitGetChanged(
  void *pPoint)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  if(pSimPoint->data.counter.frozenValueChanged)
  {
    pSimPoint->data.counter.frozenValueChanged = TMWDEFS_FALSE;
    return(TMWDEFS_TRUE);
  }

  return(TMWDEFS_FALSE);
}

/* Integrated Totals BCD */
void * TMWDEFS_GLOBAL s14sim_mitcAddPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa,
  TMWDEFS_GROUP_MASK groupMask,
  TMWTYPES_UCHAR flags,
  TMWTYPES_UCHAR *pBCD)
{
  TMWSIM_POINT *pPoint;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle; 
  pPoint = tmwsim_tableAdd(&pDbHandle->mitc, ioa);
  if(pPoint != TMWDEFS_NULL)
  {
    tmwsim_initString(pPoint, pHandle, ioa);
    pPoint->pSCLHandle = (void*)pDbHandle->pS14Sector;

    /* Store transmission mode in default event variation */
    ((TMWSIM_POINT *)pPoint)->defaultEventVariation = S14DATA_TRANSMISSION_SINGLE;

    tmwsim_setGroupMask(pPoint, groupMask);
    ((TMWSIM_POINT *)pPoint)->flags = flags; 
    tmwsim_setStringValue(pPoint, pBCD, 6, TMWDEFS_CHANGE_NONE);
    s14util_getDateTime((TMWSCTR*)pPoint->pSCLHandle, &pPoint->timeStamp); 

    _callCallback((TMWSIM_POINT *)pPoint, TMWSIM_POINT_ADD, I14DEF_TYPE_MITNC1);
  }
  return(pPoint);
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_mitcDeletePoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  TMWTYPES_BOOL status;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  _callRemoveCallback(pDbHandle, TMWSIM_POINT_DELETE, I14DEF_TYPE_MITNC1, ioa);
  status = tmwsim_tableDelete(&pDbHandle->mitc, ioa);
  return(status);
}

void * TMWDEFS_GLOBAL s14sim_mitcGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPointByIndex(&pDbHandle->mitc, index));
}

void * TMWDEFS_GLOBAL s14sim_mitcLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPoint(&pDbHandle->mitc, ioa));
}

void TMWDEFS_GLOBAL s14sim_mitcGetValue(
  void *pPoint,
  TMWTYPES_UCHAR *pBCD)
{
  TMWTYPES_UCHAR tmpLength;
  tmwsim_getStringValue((TMWSIM_POINT *)pPoint, 6, pBCD, &tmpLength);
}

/* function: s14sim_mitcSetFlags */
void TMWDEFS_GLOBAL s14sim_mitcSetFlags(
  void *pPoint, 
  TMWTYPES_UCHAR flags, 
  TMWDEFS_CHANGE_REASON reason)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  tmwsim_setFlags(pSimPoint, flags, reason);
  s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp); 
  _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_MITNC1);
}

void TMWDEFS_GLOBAL s14sim_mitcSetValue(
  void *pPoint,
  TMWTYPES_UCHAR *pBCD)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  tmwsim_setStringValue(pSimPoint, pBCD, 6, TMWDEFS_CHANGE_SPONTANEOUS);
  s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp); 
  _callCallback((TMWSIM_POINT *)pPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_MITNC1);
}

/* Event of protection equipment with time tag */
void * TMWDEFS_GLOBAL s14sim_meptaAddPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa,
  TMWTYPES_UCHAR sep,
  TMWTYPES_USHORT elapsedTime)
{
  TMWSIM_POINT *pPoint;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle; 
  pPoint = tmwsim_tableAdd(&pDbHandle->mepta, ioa);
  if(pPoint != TMWDEFS_NULL)
  {
    tmwsim_initDoubleBinary(pPoint, pHandle, ioa);
    pPoint->pSCLHandle = (void*)pDbHandle->pS14Sector;

    pPoint->flags = sep & 0xf8; 
    pPoint->data.doubleBinary.value = (TMWDEFS_DPI_TYPE)(sep & 0x3);
    pPoint->data.doubleBinary.relativeTime = elapsedTime;
    s14util_getDateTime((TMWSCTR*)pPoint->pSCLHandle, &pPoint->timeStamp); 

    _callCallback((TMWSIM_POINT *)pPoint, TMWSIM_POINT_ADD, I14DEF_TYPE_MEPTA1);
  }
  return(pPoint);
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_meptaDeletePoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  _callRemoveCallback(pDbHandle, TMWSIM_POINT_DELETE, I14DEF_TYPE_MEPTA1, ioa);
  return(tmwsim_tableDelete(&pDbHandle->mepta, ioa));
}

void * TMWDEFS_GLOBAL s14sim_meptaGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPointByIndex(&pDbHandle->mepta, index));
}

void * TMWDEFS_GLOBAL s14sim_meptaLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPoint(&pDbHandle->mepta, ioa));
}

/* function: s14sim_meptaSetFlags */
void TMWDEFS_GLOBAL s14sim_meptaSetFlags(
  void *pPoint, 
  TMWTYPES_UCHAR flags, 
  TMWDEFS_CHANGE_REASON reason)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  pSimPoint->flags = flags & 0xf8;
  pSimPoint->changed = TMWDEFS_TRUE;
  pSimPoint->reason = reason;
  s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp); 
  _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_MEPTA1);
}

void TMWDEFS_GLOBAL s14sim_meptaSetValue(
  void *pPoint,
  TMWTYPES_UCHAR sep,
  TMWTYPES_USHORT elapsedTime)
{ 
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp); 

  pSimPoint->flags = sep & 0xf8;
  pSimPoint->data.doubleBinary.value = (TMWDEFS_DPI_TYPE)(sep & 0x3);
  pSimPoint->data.doubleBinary.relativeTime = elapsedTime;
  pSimPoint->changed = TMWDEFS_TRUE;
  pSimPoint->reason = TMWDEFS_CHANGE_SPONTANEOUS;
  _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_MEPTA1);
}

void TMWDEFS_GLOBAL s14sim_meptaGetValue(
  void *pPoint,
  TMWTYPES_UCHAR *pSEP,
  TMWTYPES_USHORT *pElapsedTime)
{
  *pSEP = (TMWTYPES_UCHAR)(((TMWSIM_POINT *)pPoint)->flags | ((TMWSIM_POINT *)pPoint)->data.doubleBinary.value);
  *pElapsedTime = ((TMWSIM_POINT *)pPoint)->data.doubleBinary.relativeTime;
  return;
}

/* Packed start events of protection equipment with time tag */
void * TMWDEFS_GLOBAL s14sim_meptbAddPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa,
  TMWTYPES_UCHAR spe,
  TMWTYPES_UCHAR qdp,
  TMWTYPES_USHORT relayDuration)
{
  TMWSIM_POINT *pPoint;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle; 
  pPoint = tmwsim_tableAdd(&pDbHandle->meptb, ioa);
  if(pPoint != TMWDEFS_NULL)
  {
    tmwsim_initBinary(pPoint, pHandle, ioa);
    pPoint->pSCLHandle = (void*)pDbHandle->pS14Sector;
    pPoint->flags = qdp;
    pPoint->data.binary.faultNumber = spe;
    pPoint->data.binary.relativeTime = relayDuration;
    s14util_getDateTime((TMWSCTR*)pPoint->pSCLHandle, &pPoint->timeStamp); 

    _callCallback((TMWSIM_POINT *)pPoint, TMWSIM_POINT_ADD, I14DEF_TYPE_MEPTB1);
  }
  return(pPoint);
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_meptbDeletePoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  _callRemoveCallback(pDbHandle, TMWSIM_POINT_DELETE, I14DEF_TYPE_MEPTB1, ioa);
  return(tmwsim_tableDelete(&pDbHandle->meptb, ioa));
}

void * TMWDEFS_GLOBAL s14sim_meptbGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPointByIndex(&pDbHandle->meptb, index));
}

void * TMWDEFS_GLOBAL s14sim_meptbLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPoint(&pDbHandle->meptb, ioa));
}

/* function: s14sim_meptbSetFlags */
void TMWDEFS_GLOBAL s14sim_meptbSetFlags(
  void *pPoint, 
  TMWTYPES_UCHAR flags, 
  TMWDEFS_CHANGE_REASON reason)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  pSimPoint->flags = flags;
  pSimPoint->changed = TMWDEFS_TRUE;
  pSimPoint->reason = reason;
  s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp); 
  _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_MEPTB1);
}

void TMWDEFS_GLOBAL s14sim_meptbSetValue(
  void *pPoint,
  TMWTYPES_UCHAR spe,
  TMWTYPES_UCHAR qdp,
  TMWTYPES_USHORT relayDuration)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp); 

  pSimPoint->flags = qdp;
  pSimPoint->data.binary.faultNumber = spe;
  pSimPoint->data.binary.relativeTime = relayDuration;
  pSimPoint->changed = TMWDEFS_TRUE;
  pSimPoint->reason = TMWDEFS_CHANGE_SPONTANEOUS;
  _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_MEPTB1);
}

void TMWDEFS_GLOBAL s14sim_meptbGetValue(
  void *pPoint,
  TMWTYPES_UCHAR *pSPE,
  TMWTYPES_UCHAR *pQDP,
  TMWTYPES_USHORT *pRelayDuration)
{
  *pQDP = ((TMWSIM_POINT *)pPoint)->flags;
  *pSPE = (TMWTYPES_UCHAR)((TMWSIM_POINT *)pPoint)->data.binary.faultNumber;
  *pRelayDuration = ((TMWSIM_POINT *)pPoint)->data.binary.relativeTime;
  return;
}

/* Packed output circuit information of protection equipment with time tag */
void * TMWDEFS_GLOBAL s14sim_meptcAddPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa,
  TMWTYPES_UCHAR oci,
  TMWTYPES_UCHAR qdp,
  TMWTYPES_USHORT relayOperating)
{
  TMWSIM_POINT *pPoint;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle; 
  pPoint = tmwsim_tableAdd(&pDbHandle->meptc, ioa);
  if(pPoint != TMWDEFS_NULL)
  {
    tmwsim_initBinary(pPoint, pHandle, ioa);
    pPoint->pSCLHandle = (void*)pDbHandle->pS14Sector;
    pPoint->flags = qdp;
    pPoint->data.binary.faultNumber = oci;
    pPoint->data.binary.relativeTime = relayOperating;
    s14util_getDateTime((TMWSCTR*)pPoint->pSCLHandle, &pPoint->timeStamp); 

    _callCallback((TMWSIM_POINT *)pPoint, TMWSIM_POINT_ADD, I14DEF_TYPE_MEPTC1);
  }
  return(pPoint);
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_meptcDeletePoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;  
  _callRemoveCallback(pDbHandle, TMWSIM_POINT_DELETE, I14DEF_TYPE_MEPTC1, ioa);
  return(tmwsim_tableDelete(&pDbHandle->meptc, ioa));
}

void * TMWDEFS_GLOBAL s14sim_meptcGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPointByIndex(&pDbHandle->meptc, index));
}

void * TMWDEFS_GLOBAL s14sim_meptcLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPoint(&pDbHandle->meptc, ioa));
}

/* function: s14sim_meptcSetFlags */
void TMWDEFS_GLOBAL s14sim_meptcSetFlags(
  void *pPoint, 
  TMWTYPES_UCHAR flags, 
  TMWDEFS_CHANGE_REASON reason)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  pSimPoint->flags = flags;
  pSimPoint->changed = TMWDEFS_TRUE;
  pSimPoint->reason = reason;
  s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp); 
  _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_MEPTC1);
}

void TMWDEFS_GLOBAL s14sim_meptcSetValue(
  void *pPoint,
  TMWTYPES_UCHAR oci,
  TMWTYPES_UCHAR qdp,
  TMWTYPES_USHORT relayOperating)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp);  

  pSimPoint->flags = qdp;
  pSimPoint->data.binary.faultNumber = oci;
  pSimPoint->data.binary.relativeTime = relayOperating;
  pSimPoint->changed = TMWDEFS_TRUE;
  pSimPoint->reason = TMWDEFS_CHANGE_SPONTANEOUS;
  _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_MEPTC1);
}

void TMWDEFS_GLOBAL s14sim_meptcGetValue(
  void *pPoint,
  TMWTYPES_UCHAR *pOCI,
  TMWTYPES_UCHAR *pQDP,
  TMWTYPES_USHORT *pRelayOperating)
{
  *pQDP = ((TMWSIM_POINT *)pPoint)->flags;
  *pOCI = (TMWTYPES_UCHAR)((TMWSIM_POINT *)pPoint)->data.binary.faultNumber;
  *pRelayOperating = ((TMWSIM_POINT *)pPoint)->data.binary.relativeTime;
  return;
}

/* Packed Single Point Information With Status Change Detection */
void * TMWDEFS_GLOBAL s14sim_mpsnaAddPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa,
  TMWDEFS_GROUP_MASK groupMask,
  TMWTYPES_ULONG scd,
  TMWTYPES_UCHAR qds)
{
  TMWSIM_POINT *pPoint;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle; 
  pPoint = tmwsim_tableAdd(&pDbHandle->mpsna, ioa);
  if(pPoint != TMWDEFS_NULL)
  {
    tmwsim_initBitstring(pPoint, pHandle, ioa);
    pPoint->pSCLHandle = (void*)pDbHandle->pS14Sector;
    tmwsim_setGroupMask(pPoint, groupMask);
    pPoint->flags = qds; 
    pPoint->data.bitstring.value = scd;

    pDbHandle->mpsnaIndexed = _checkForIndexed(&pDbHandle->mpsna);
    s14util_getDateTime((TMWSCTR*)pPoint->pSCLHandle, &pPoint->timeStamp); 

    _callCallback((TMWSIM_POINT *)pPoint, TMWSIM_POINT_ADD, I14DEF_TYPE_MPSNA1);
  }
  return(pPoint);
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_mpsnaDeletePoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  TMWTYPES_BOOL status;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle; 
  _callRemoveCallback(pDbHandle, TMWSIM_POINT_DELETE, I14DEF_TYPE_MPSNA1, ioa);

  status = tmwsim_tableDelete(&pDbHandle->mpsna, ioa);
  if(status)
  {
    pDbHandle->mpsnaIndexed = _checkForIndexed(&pDbHandle->mpsna);
  }

  return(status);
}

void * TMWDEFS_GLOBAL s14sim_mpsnaGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPointByIndex(&pDbHandle->mpsna, index));
}

void * TMWDEFS_GLOBAL s14sim_mpsnaLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPoint(&pDbHandle->mpsna, ioa));
}

/* function: s14sim_mpsnaGetIndexed */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14sim_mpsnaGetIndexed(
  void *pPoint)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pSimPoint->pDbHandle;
  return(pDbHandle->mpsnaIndexed);
}

 
/* function: s14sim_mpsnaSetFlags */
void TMWDEFS_GLOBAL s14sim_mpsnaSetFlags(
  void *pPoint, 
  TMWTYPES_UCHAR flags, 
  TMWDEFS_CHANGE_REASON reason)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  pSimPoint->flags = flags;
  pSimPoint->changed = TMWDEFS_TRUE;
  pSimPoint->reason = reason;
  s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp); 
  _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_MPSNA1);
}

TMWTYPES_ULONG TMWDEFS_GLOBAL s14sim_mpsnaGetValue(
  void *pPoint)
{
  return(tmwsim_getBitstringValue((TMWSIM_POINT *)pPoint));
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_mpsnaSetValue(
  void *pPoint,
  TMWTYPES_ULONG scd,
  TMWTYPES_UCHAR qds,
  TMWDEFS_CHANGE_REASON reason)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  pSimPoint->flags = qds; 
  tmwsim_setBitstringValue((TMWSIM_POINT *)pPoint, scd, reason);
  s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp);  
  _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_MPSNA1);
  return(TMWDEFS_TRUE);
}

/* Normalized Measurands Without Quality Descriptor */
void * TMWDEFS_GLOBAL s14sim_mmendAddPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa,
  TMWDEFS_GROUP_MASK groupMask,
  TMWTYPES_SHORT nva)
{
  TMWSIM_POINT *pPoint;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle; 
  pPoint = tmwsim_tableAdd(&pDbHandle->mmend, ioa);
  if(pPoint != TMWDEFS_NULL)
  {
    tmwsim_initAnalog(pPoint, pHandle, ioa, TMWDEFS_SHORT_MIN, TMWDEFS_SHORT_MAX, 99, 50);
    pPoint->pSCLHandle = (void*)pDbHandle->pS14Sector;

    tmwsim_setGroupMask(pPoint, groupMask);
    pPoint->data.analog.value = nva;

    pDbHandle->mmendIndexed = _checkForIndexed(&pDbHandle->mmend);
    s14util_getDateTime((TMWSCTR*)pPoint->pSCLHandle, &pPoint->timeStamp); 

    _callCallback((TMWSIM_POINT *)pPoint, TMWSIM_POINT_ADD, I14DEF_TYPE_MMEND1);
  }
  return(pPoint);
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_mmendDeletePoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  TMWTYPES_BOOL status;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;  
  _callRemoveCallback(pDbHandle, TMWSIM_POINT_DELETE, I14DEF_TYPE_MMEND1, ioa);

  status = tmwsim_tableDelete(&pDbHandle->mmend, ioa);
  if(status)
  {
    pDbHandle->mmendIndexed = _checkForIndexed(&pDbHandle->mmend);
  }

  return(status);
}

void * TMWDEFS_GLOBAL s14sim_mmendGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPointByIndex(&pDbHandle->mmend, index));
}

void * TMWDEFS_GLOBAL s14sim_mmendLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPoint(&pDbHandle->mmend, ioa));
}

/* function: s14sim_mmendGetIndexed */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14sim_mmendGetIndexed(
  void *pPoint)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pSimPoint->pDbHandle;
  return(pDbHandle->mmendIndexed);
}

TMWTYPES_SHORT TMWDEFS_GLOBAL s14sim_mmendGetValue(
  void *pPoint)
{
  return((TMWTYPES_SHORT)tmwsim_getAnalogValue((TMWSIM_POINT *)pPoint));
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_mmendSetValue(
  void *pPoint,
  TMWTYPES_SHORT nva,
  TMWDEFS_CHANGE_REASON reason)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  tmwsim_setAnalogValue(pSimPoint, nva, reason);
  s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp);  
  _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_MMEND1);
  return(TMWDEFS_TRUE);
}

/* Single Commands */
void * TMWDEFS_GLOBAL s14sim_cscLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  return(s14sim_mspLookupPoint(pHandle, ioa - ((S14SIM_DATABASE *)pHandle)->ctrlPointOffset));
}

void * TMWDEFS_GLOBAL s14sim_cscGetMonitoredPoint(void *pPoint)
{
  return(pPoint);
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_cscSelectRequired(
  void *pPoint)
{
  return(tmwsim_getSelectRequired((TMWSIM_POINT *)pPoint));
}

TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14sim_cscSelect(
  void *pPoint)
{
  return(_s14sim_getSelectStatus(pPoint));
}

TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14sim_cscExecute(
  void *pPoint,
  TMWTYPES_BOOL value)
{
  s14sim_mspSetValue((TMWSIM_POINT *)pPoint, value, TMWDEFS_CHANGE_REMOTE_OP);
  return(_s14sim_getExecuteStatus(pPoint));
}

TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14sim_cscStatus(
  void *pPoint)
{
  return(_s14sim_getCmdStatus(pPoint));
}

/* Double Commands */
void * TMWDEFS_GLOBAL s14sim_cdcLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  return(s14sim_mdpLookupPoint(pHandle, ioa - ((S14SIM_DATABASE *)pHandle)->ctrlPointOffset));
}

void * TMWDEFS_GLOBAL s14sim_cdcGetMonitoredPoint(
  void *pPoint)
{
  return(pPoint);
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_cdcSelectRequired(
  void *pPoint)
{
  return(tmwsim_getSelectRequired((TMWSIM_POINT *)pPoint));
}

TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14sim_cdcSelect(
  void *pPoint)
{
  return(_s14sim_getCmdStatus(pPoint));
}

TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14sim_cdcExecute(
  void *pPoint,
  TMWDEFS_DPI_TYPE value)
{
  s14sim_mdpSetValue(pPoint, value, TMWDEFS_CHANGE_REMOTE_OP);
  return(_s14sim_getExecuteStatus(pPoint));
}

TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14sim_cdcStatus(
  void *pPoint)
{
  return(_s14sim_getCmdStatus(pPoint));
}

/* Regulating Step Commands */
void * TMWDEFS_GLOBAL s14sim_crcLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  return(s14sim_mstLookupPoint(pHandle, ioa - ((S14SIM_DATABASE *)pHandle)->ctrlPointOffset));
}

void * TMWDEFS_GLOBAL s14sim_crcGetMonitoredPoint(
  void *pPoint)
{
  return(pPoint);
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_crcSelectRequired(
  void *pPoint)
{
  return(tmwsim_getSelectRequired((TMWSIM_POINT *)pPoint));
}

TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14sim_crcSelect(
  void *pPoint)
{
  return(_s14sim_getCmdStatus(pPoint));
}

TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14sim_crcExecute(
  void *pPoint,
  TMWTYPES_UCHAR rco)
{
  TMWTYPES_UCHAR oldValue = s14sim_mstGetValue(pPoint);

  switch(rco & I14DEF_RCS_MASK)
  {
  case I14DEF_RCS_STEP_LOWER:
    s14sim_mstSetValue(pPoint, (TMWTYPES_UCHAR)(oldValue - 1), TMWDEFS_CHANGE_REMOTE_OP);
    break;

  case I14DEF_RCS_STEP_HIGHER:
    s14sim_mstSetValue(pPoint, (TMWTYPES_UCHAR)(oldValue + 1), TMWDEFS_CHANGE_REMOTE_OP);
    break;

  default:
    return(TMWDEFS_CMD_STAT_FAILED);
  }

  return(_s14sim_getExecuteStatus(pPoint));
}

TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14sim_crcStatus(
  void *pPoint)
{
  return(_s14sim_getCmdStatus(pPoint));
}

/* Bitstring Commands */
void * TMWDEFS_GLOBAL s14sim_cboLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  return(s14sim_mboLookupPoint(pHandle, ioa - ((S14SIM_DATABASE *)pHandle)->ctrlPointOffset));
}

TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14sim_cboExecute(
  void *pPoint,
  TMWTYPES_ULONG bsi)
{
  s14sim_mboSetValue(pPoint, bsi);
  return(_s14sim_getCmdStatus(pPoint));
}

/* Normalized Measurand Commands */
void * TMWDEFS_GLOBAL s14sim_csenaLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  return(s14sim_mmenaLookupPoint(pHandle, ioa - ((S14SIM_DATABASE *)pHandle)->ctrlPointOffset));
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_csenaSelectRequired(
  void *pPoint)
{
  return(tmwsim_getSelectRequired((TMWSIM_POINT *)pPoint));
}

TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14sim_csenaSelect(
  void *pPoint)
{
  return(_s14sim_getCmdStatus(pPoint));
}

TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14sim_csenaExecute(
  void *pPoint,
  TMWTYPES_SHORT value)
{
  s14sim_mmenaSetValue(pPoint, value);
  return(_s14sim_getCmdStatus(pPoint));
}

TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14sim_csenaStatus(
  void *pPoint)
{
  return(_s14sim_getCmdStatus(pPoint));
}

/* Scaled Measurand Commands */
void * TMWDEFS_GLOBAL s14sim_csenbLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  return(s14sim_mmenbLookupPoint(pHandle, ioa - ((S14SIM_DATABASE *)pHandle)->ctrlPointOffset));
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_csenbSelectRequired(
  void *pPoint)
{
  return(tmwsim_getSelectRequired((TMWSIM_POINT *)pPoint));
}

TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14sim_csenbSelect(
  void *pPoint)
{
  return(_s14sim_getCmdStatus(pPoint));
}

TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14sim_csenbExecute(
  void *pPoint,
  TMWTYPES_SHORT value)
{
  s14sim_mmenbSetValue(pPoint, value);
  return(_s14sim_getCmdStatus(pPoint));
}

TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14sim_csenbStatus(
  void *pPoint)
{
  return(_s14sim_getCmdStatus(pPoint));
}

#if S14DATA_SUPPORT_CSE_C
/* Floating Point Measurand Commands */
void * TMWDEFS_GLOBAL s14sim_csencLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  return(s14sim_mmencLookupPoint(pHandle, ioa - ((S14SIM_DATABASE *)pHandle)->ctrlPointOffset));
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_csencSelectRequired(
  void *pPoint)
{
  return(tmwsim_getSelectRequired((TMWSIM_POINT *)pPoint));
}

TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14sim_csencSelect(
  void *pPoint)
{
  return(_s14sim_getCmdStatus(pPoint));
}

TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14sim_csencExecute(
  void *pPoint,
  TMWTYPES_SFLOAT value)
{
  s14sim_mmencSetValue(pPoint, value);
  return(_s14sim_getCmdStatus(pPoint));
}

TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14sim_csencStatus(
  void *pPoint)
{
  return(_s14sim_getCmdStatus(pPoint));
}
#endif

#if S14DATA_SUPPORT_CSE_Z
/* Set Integrated Totals BCD Commands */
void * TMWDEFS_GLOBAL s14sim_csenzLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  return(s14sim_mitcLookupPoint(pHandle, ioa - ((S14SIM_DATABASE *)pHandle)->ctrlPointOffset));
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_csenzSelectRequired(
  void *pPoint)
{
  return(tmwsim_getSelectRequired((TMWSIM_POINT *)pPoint));
}

TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14sim_csenzSelect(
  void *pPoint)
{
  return(_s14sim_getCmdStatus(pPoint));
}

TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14sim_csenzExecute(
  void *pPoint,
  TMWTYPES_UCHAR *pBCD)
{
  s14sim_mitcSetValue(pPoint, pBCD);
  return(_s14sim_getCmdStatus(pPoint));
}

TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14sim_csenzStatus(
  void *pPoint)
{
  return(_s14sim_getCmdStatus(pPoint));
}
#endif

/* Parameter of Measured Value, Normalized Value */
void * TMWDEFS_GLOBAL s14sim_pmenaGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPointByIndex(&pDbHandle->pmena, index));
}

void * TMWDEFS_GLOBAL s14sim_pmenaLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPoint(&pDbHandle->pmena, ioa));
}

/* s14sim_pmenaGetNextIOA */
TMWTYPES_ULONG TMWDEFS_GLOBAL s14sim_pmenaGetNextIOA(
  void *pHandle)
{
  TMWTYPES_ULONG ioa = S14SIM_FIRST_PMENA-1;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  TMWSIM_POINT *pPoint = tmwsim_tableGetLastPoint(&pDbHandle->pmena); 
  if(pPoint != TMWDEFS_NULL)
    ioa = tmwsim_getPointNumber(pPoint);

  return(ioa+1);
}

TMWTYPES_SHORT TMWDEFS_GLOBAL s14sim_pmenaGetValue(
  void *pPoint)
{
  TMWSIM_POINT *pMeasurand = ((TMWSIM_POINT *)pPoint)->data.parameter.pPoint;

  switch(s14sim_pmenaGetQualifier(pPoint))
  {
  case I14DEF_QPM_KPA_THRESHOLD:
    return((TMWTYPES_SHORT)pMeasurand->data.analog.deadband);

  case I14DEF_QPM_KPA_SMOOTHING:
    return((TMWTYPES_SHORT)pMeasurand->data.analog.smoothing);

  case I14DEF_QPM_KPA_LOW_LIMIT:
    return((TMWTYPES_SHORT)pMeasurand->data.analog.lowLimit);

  case I14DEF_QPM_KPA_HIGH_LIMIT:
    return((TMWTYPES_SHORT)pMeasurand->data.analog.highLimit);
  }

  return(0);
}

TMWTYPES_UCHAR TMWDEFS_GLOBAL s14sim_pmenaGetQualifier(
  void *pPoint)
{
  return((TMWTYPES_UCHAR)(((TMWSIM_POINT *)pPoint)->data.parameter.kind + 1));
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_pmenaStore(
  void *pPoint,
  TMWTYPES_SHORT *pValue,
  TMWTYPES_UCHAR *pQualifier)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  TMWSIM_POINT *pMeasurand = pSimPoint->data.parameter.pPoint;
  TMWTYPES_UCHAR type = (TMWTYPES_UCHAR)(*pQualifier & I14DEF_QPM_KPA_MASK);
  TMWTYPES_BOOL status = TMWDEFS_FALSE;

  /* Verify type of parameter matches this object */
  if(type == s14sim_pmenaGetQualifier(pPoint))
  {
    switch(type)
    {
    case I14DEF_QPM_KPA_THRESHOLD:
      pMeasurand->data.analog.deadband = *pValue;
      status = TMWDEFS_TRUE;
      break;

    case I14DEF_QPM_KPA_SMOOTHING:
      pMeasurand->data.analog.smoothing = *pValue;
      status = TMWDEFS_TRUE;
      break;

    case I14DEF_QPM_KPA_LOW_LIMIT:
      pMeasurand->data.analog.lowLimit = *pValue;
      status = TMWDEFS_TRUE;
      break;

    case I14DEF_QPM_KPA_HIGH_LIMIT:
      pMeasurand->data.analog.highLimit = *pValue;
      status = TMWDEFS_TRUE;
      break;
    }
  }
  if(status)
  {   
    s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp); 
    _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_PMENA1);
  }
  else
  {
    /* Something went wrong */
    *pValue = 0;
    *pQualifier |= I14DEF_QPM_POP;
  }

  return(status);
}

/* Parameter of Measured Value, Scaled Value */
void * TMWDEFS_GLOBAL s14sim_pmenbGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPointByIndex(&pDbHandle->pmenb, index));
}

void * TMWDEFS_GLOBAL s14sim_pmenbLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPoint(&pDbHandle->pmenb, ioa));
}

/* s14sim_pmenbGetNextIOA */
TMWTYPES_ULONG TMWDEFS_GLOBAL s14sim_pmenbGetNextIOA(
  void *pHandle)
{
  TMWTYPES_ULONG ioa = S14SIM_FIRST_PMENB-1;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  TMWSIM_POINT *pPoint = tmwsim_tableGetLastPoint(&pDbHandle->pmenb); 
  if(pPoint != TMWDEFS_NULL)
    ioa = tmwsim_getPointNumber(pPoint);

  return(ioa+1);
}

TMWTYPES_SHORT TMWDEFS_GLOBAL s14sim_pmenbGetValue(
  void *pPoint)
{
  TMWSIM_POINT *pMeasurand = ((TMWSIM_POINT *)pPoint)->data.parameter.pPoint;

  switch(s14sim_pmenbGetQualifier(pPoint))
  {
  case I14DEF_QPM_KPA_THRESHOLD:
    return((TMWTYPES_SHORT)pMeasurand->data.analog.deadband);

  case I14DEF_QPM_KPA_SMOOTHING:
    return((TMWTYPES_SHORT)pMeasurand->data.analog.smoothing);

  case I14DEF_QPM_KPA_LOW_LIMIT:
    return((TMWTYPES_SHORT)pMeasurand->data.analog.lowLimit);

  case I14DEF_QPM_KPA_HIGH_LIMIT:
    return((TMWTYPES_SHORT)pMeasurand->data.analog.highLimit);
  }

  return(0);
}

TMWTYPES_UCHAR TMWDEFS_GLOBAL s14sim_pmenbGetQualifier(
  void *pPoint)
{
  return((TMWTYPES_UCHAR)(((TMWSIM_POINT *)pPoint)->data.parameter.kind + 1));
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_pmenbStore(
  void *pPoint,
  TMWTYPES_SHORT *pValue,
  TMWTYPES_UCHAR *pQualifier)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  TMWSIM_POINT *pMeasurand = pSimPoint->data.parameter.pPoint;
  TMWTYPES_UCHAR type = (TMWTYPES_UCHAR)(*pQualifier & I14DEF_QPM_KPA_MASK);
  TMWTYPES_BOOL status = TMWDEFS_FALSE;


  /* Verify type of parameter matches this object */
  if(type == s14sim_pmenbGetQualifier(pPoint))
  {
    switch(type)
    {
    case I14DEF_QPM_KPA_THRESHOLD:
      pMeasurand->data.analog.deadband = *pValue;
      status = TMWDEFS_TRUE;
      break;

    case I14DEF_QPM_KPA_SMOOTHING:
      pMeasurand->data.analog.smoothing = *pValue;
      status = TMWDEFS_TRUE;
      break;

    case I14DEF_QPM_KPA_LOW_LIMIT:
      pMeasurand->data.analog.lowLimit = *pValue;
      status = TMWDEFS_TRUE;
      break;

    case I14DEF_QPM_KPA_HIGH_LIMIT:
      pMeasurand->data.analog.highLimit = *pValue;
      status = TMWDEFS_TRUE;
      break;
    }
  }

  if(status)
  {
    s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp); 
    _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_PMENB1);
  }
  else
  {
    /* Something went wrong */
    *pValue = 0;
    *pQualifier |= I14DEF_QPM_POP;
  }
  return(status);
}

#if S14DATA_SUPPORT_PMENC
/* Parameter of Measured Value, Short Floating Point Value */
void * TMWDEFS_GLOBAL s14sim_pmencGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPointByIndex(&pDbHandle->pmenc, index));
}

void * TMWDEFS_GLOBAL s14sim_pmencLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPoint(&pDbHandle->pmenc, ioa));
}

/* s14sim_pmencGetNextIOA */
TMWTYPES_ULONG TMWDEFS_GLOBAL s14sim_pmencGetNextIOA(
  void *pHandle)
{
  TMWTYPES_ULONG ioa = S14SIM_FIRST_PMENC-1;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  TMWSIM_POINT *pPoint = tmwsim_tableGetLastPoint(&pDbHandle->pmenc); 
  if(pPoint != TMWDEFS_NULL)
    ioa = tmwsim_getPointNumber(pPoint);

  return(ioa+1);
}

TMWTYPES_SFLOAT TMWDEFS_GLOBAL s14sim_pmencGetValue(
  void *pPoint)
{
  TMWSIM_POINT *pMeasurand = ((TMWSIM_POINT *)pPoint)->data.parameter.pPoint;

  switch(s14sim_pmencGetQualifier(pPoint))
  {
  case I14DEF_QPM_KPA_THRESHOLD:
    return((TMWTYPES_SFLOAT)pMeasurand->data.analog.deadband);

  case I14DEF_QPM_KPA_SMOOTHING:
    return((TMWTYPES_SFLOAT)pMeasurand->data.analog.smoothing);

  case I14DEF_QPM_KPA_LOW_LIMIT:
    return((TMWTYPES_SFLOAT)pMeasurand->data.analog.lowLimit);

  case I14DEF_QPM_KPA_HIGH_LIMIT:
    return((TMWTYPES_SFLOAT)pMeasurand->data.analog.highLimit);
  }

  return(0);
}

TMWTYPES_UCHAR TMWDEFS_GLOBAL s14sim_pmencGetQualifier(
  void *pPoint)
{
  return((TMWTYPES_UCHAR)(((TMWSIM_POINT *)pPoint)->data.parameter.kind + 1));
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_pmencStore(
  void *pPoint,
  TMWTYPES_SFLOAT *pValue,
  TMWTYPES_UCHAR *pQualifier)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  TMWSIM_POINT *pMeasurand = pSimPoint->data.parameter.pPoint;
  TMWTYPES_UCHAR type = (TMWTYPES_UCHAR)(*pQualifier & I14DEF_QPM_KPA_MASK);
  TMWTYPES_BOOL status = TMWDEFS_FALSE;

  /* Verify type of parameter matches this object */
  if(type == s14sim_pmencGetQualifier(pPoint))
  {
    switch(type)
    {
    case I14DEF_QPM_KPA_THRESHOLD:
      pMeasurand->data.analog.deadband = (TMWSIM_DATA_TYPE)*pValue;
      status = TMWDEFS_TRUE;
      break;

    case I14DEF_QPM_KPA_SMOOTHING:
      pMeasurand->data.analog.smoothing = (TMWSIM_DATA_TYPE)*pValue;
      status = TMWDEFS_TRUE;
      break;

    case I14DEF_QPM_KPA_LOW_LIMIT:
      pMeasurand->data.analog.lowLimit = (TMWSIM_DATA_TYPE)*pValue;
      status = TMWDEFS_TRUE;
      break;

    case I14DEF_QPM_KPA_HIGH_LIMIT:
      pMeasurand->data.analog.highLimit = (TMWSIM_DATA_TYPE)*pValue;
      status = TMWDEFS_TRUE;
      break;
    }
  }

  if(status)
  {    
    s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp); 
    _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_PMENC1);
  }
  else
  {
    /* Something went wrong */
    *pValue = 0;
    *pQualifier |= I14DEF_QPM_POP;
  }
  return(status);
}
#endif

/* Parameter Activation */
/* s14sim_pacnaLookupPoint */
void * TMWDEFS_GLOBAL s14sim_pacnaLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPoint(&pDbHandle->pacna, ioa));
}

/* s14sim_pacnaGetPoint */
void * TMWDEFS_GLOBAL s14sim_pacnaGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPointByIndex(&pDbHandle->pacna, index));
}

/* s14sim_pacnaGetNextIOA */
TMWTYPES_ULONG TMWDEFS_GLOBAL s14sim_pacnaGetNextIOA(
  void *pHandle)
{
  TMWTYPES_ULONG ioa = S14SIM_FIRST_PACNA-1;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  TMWSIM_POINT *pPoint = tmwsim_tableGetLastPoint(&pDbHandle->pacna); 
  if(pPoint != TMWDEFS_NULL)
    ioa = tmwsim_getPointNumber(pPoint);
 
  return(ioa+1);
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_pacnaStore(
  void *pPoint,
  TMWTYPES_UCHAR cot,
  TMWTYPES_UCHAR qpa)
{
  /* 3 is the only valid value for QPA as per the 101 specification */
  switch(qpa)
  {
  case 3:
    {
      TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
      /* Get the measurand point this parameter controls cyclic data for */
      TMWSIM_POINT *pMeasurand = ((TMWSIM_POINT *)pPoint)->data.parameter.pPoint;
      TMWDEFS_GROUP_MASK oldMask = tmwsim_getGroupMask(pMeasurand);

      if(cot == I14DEF_COT_ACTIVATION)
        oldMask |= TMWDEFS_GROUP_MASK_CYCLIC;
      else
        oldMask &= ~TMWDEFS_GROUP_MASK_CYCLIC;

      tmwsim_setGroupMask(pMeasurand, oldMask);   
      s14util_getDateTime((TMWSCTR*)pSimPoint->pSCLHandle, &pSimPoint->timeStamp); 
      return(TMWDEFS_TRUE);
    }

  default:
    return(TMWDEFS_FALSE);
  }
}

#if S14DATA_SUPPORT_MCTNA
void * TMWDEFS_GLOBAL s14sim_mctnaAddPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa,
  TMWTYPES_UCHAR *pCTIValues,
  TMWTYPES_UCHAR quantity)
{
  TMWSIM_POINT *pPoint;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle; 
  pPoint = tmwsim_tableAdd(&pDbHandle->mctna, ioa);
  if(pPoint != TMWDEFS_NULL)
  {
    tmwsim_initString(pPoint, pHandle, ioa);
    pPoint->pSCLHandle = (void*)pDbHandle->pS14Sector;
    tmwsim_setStringValue(pPoint, pCTIValues, quantity, TMWDEFS_CHANGE_NONE);
    s14util_getDateTime((TMWSCTR*)pPoint->pSCLHandle, &pPoint->timeStamp); 

    _callCallback((TMWSIM_POINT *)pPoint, TMWSIM_POINT_ADD, I14DEF_TYPE_MCTNA1);
  }
  return(pPoint);
}

/* function: s14sim_mctnaDeletePoint */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_mctnaDeletePoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  TMWTYPES_BOOL status;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;  
  _callRemoveCallback(pDbHandle, TMWSIM_POINT_DELETE, I14DEF_TYPE_MCTNA1, ioa);
  status = tmwsim_tableDelete(&pDbHandle->mctna, ioa);
  return(status);
}

/* function: s14sim_mctnaGetPoint */
void * TMWDEFS_GLOBAL s14sim_mctnaGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPointByIndex(&pDbHandle->mctna, index));
}

/* function: s14sim_mctnaLookupPoint */
void * TMWDEFS_GLOBAL s14sim_mctnaLookupPoint(
  void *pHandle, 
  TMWTYPES_ULONG ioa)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwsim_tableFindPoint(&pDbHandle->mctna, ioa));
}

/* function: s14sim_mctnaGetValues */
TMWTYPES_UCHAR TMWDEFS_GLOBAL s14sim_mctnaGetValues(
  void *pPoint,
  TMWTYPES_UCHAR maxQuantity,
  TMWTYPES_UCHAR *pBuf)
{
  TMWTYPES_UCHAR quantity;
  tmwsim_getStringValue((TMWSIM_POINT *)pPoint, maxQuantity, pBuf, &quantity);
  return(quantity);
}

void TMWDEFS_GLOBAL s14sim_mctnaSetValues(
  void *pPoint,
  TMWTYPES_UCHAR *pCtiValues,
  TMWTYPES_UCHAR quantity)
{
  TMWSIM_POINT *pSimPoint = (TMWSIM_POINT *)pPoint;
  tmwsim_setStringValue(pSimPoint, pCtiValues, quantity, TMWDEFS_CHANGE_SPONTANEOUS);
  _callCallback(pSimPoint, TMWSIM_POINT_UPDATE, I14DEF_TYPE_MCTNA1);
}
#endif


#if S14DATA_SUPPORT_FILE
/* File Transfer */
static TMWDEFS_GLOBAL S14SIM_FILE *_fileAdd(void *pHandle,
  TMWTYPES_ULONG  ioa,
  TMWTYPES_USHORT fileName,
  TMWTYPES_UCHAR  sof)
{
  TMWTYPES_USHORT index;
  S14SIM_FILE *pFile;
  S14SIM_FILE *pListEntry;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;

  if((pFile = (S14SIM_FILE *)s14mem_alloc(S14MEM_SIM_FILE_TYPE)) != TMWDEFS_NULL)
  {
    pFile->ioa                   = ioa;
    pFile->fileName              = fileName;
    pFile->fileLength            = 0;
    pFile->expectedFileLength    = 0;
    pFile->sof                   = sof;

    tmwdtime_getDateTime(TMWDEFS_NULL, &pFile->fileTime);

    tmwdlist_initialize(&pFile->sections);

    /* Add this to sorted list of files */
    index = 0;
    while((pListEntry = (S14SIM_FILE *)tmwdlist_getEntry(&pDbHandle->file, index++)) != TMWDEFS_NULL)
    {
      /* If the ioa in the list is greater, insert this entry before it. */
      if(pListEntry->ioa > ioa)
      {
        tmwdlist_insertEntryBefore(&pDbHandle->file, (TMWDLIST_MEMBER *)pListEntry, (TMWDLIST_MEMBER *)pFile);
        return(pFile);
      }
    }

    /* This new entry will be the last in the list 
     * Get last entry in list, and change SOF to not last file 
     * Then insert this new entry and set SOF to last file  
     */
    if((pListEntry = (S14SIM_FILE *)tmwdlist_getLast(&pDbHandle->file)) != TMWDEFS_NULL)
    {
      pListEntry->sof &= ~I14DEF_SOF_LFD_LAST_FILE;
    }

    tmwdlist_addEntry(&pDbHandle->file, (TMWDLIST_MEMBER *)pFile);
    pFile->sof = (TMWTYPES_UCHAR)(sof|I14DEF_SOF_LFD_LAST_FILE);
  }

  return(pFile);
}

/* function: _fileSectionAdd */
S14SIM_SECTION * TMWDEFS_GLOBAL _fileSectionAdd(
  void *pHandle,
  TMWTYPES_ULONG  ioa,
  TMWTYPES_USHORT fileName,
  TMWTYPES_USHORT sectionLength,
  TMWTYPES_UCHAR  *pSectionData)
{
  S14SIM_SECTION *pSection = TMWDEFS_NULL;
  S14SIM_FILE *pFile = (S14SIM_FILE *)s14sim_fileLookup(pHandle, ioa, fileName);

  if(pFile != TMWDEFS_NULL)
  {
    if((pSection = (S14SIM_SECTION *)s14mem_alloc(S14MEM_SIM_SECTION_TYPE)) != TMWDEFS_NULL)
    {
      if(sectionLength > S14SIM_FILE_MAX_SECTION_SIZE)
      {
        sectionLength = S14SIM_FILE_MAX_SECTION_SIZE;
      }
      if(pSectionData != TMWDEFS_NULL)
      {
        memcpy(pSection->data, pSectionData, sectionLength);
      }
      else
      {
        int i;
        for(i=0; i< sectionLength; i++)
        {
          pSection->data[i] = (TMWTYPES_UCHAR)(i%10 + 0x30);
        }
      }
      tmwdlist_addEntry(&pFile->sections, (TMWDLIST_MEMBER *)pSection);
      pSection->sectionLength = sectionLength;
      pSection->byteIndex = 0;

      pFile->fileLength += sectionLength;
    }
  }

  return(pSection);
}

/* function: s14sim_fileAdd */
void * TMWDEFS_GLOBAL s14sim_fileAdd(
  void *pHandle,
  TMWTYPES_ULONG  ioa,
  TMWTYPES_USHORT fileName,
  TMWTYPES_UCHAR  sof)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  pDbHandle->fileDirChanged = TMWDEFS_TRUE;

  return((void*)_fileAdd(pHandle, ioa, fileName, sof));
}

/* function: _fileDelete */
static TMWTYPES_BOOL TMWDEFS_GLOBAL _fileDelete(
  void *pHandle, 
  TMWTYPES_ULONG  ioa,
  TMWTYPES_USHORT fileName)
{ 
  S14SIM_FILE *pFile;
  S14SIM_SECTION *pSection;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;

  if((pFile = (S14SIM_FILE *)s14sim_fileLookup(pHandle, ioa, fileName)) != TMWDEFS_NULL)
  {
    while((pSection = (S14SIM_SECTION *)tmwdlist_getFirst(&pFile->sections)) != TMWDEFS_NULL)
    {
      tmwdlist_removeEntry(&pFile->sections, (TMWDLIST_MEMBER *)pSection);
      s14mem_free(pSection);
    }

    tmwdlist_removeEntry(&pDbHandle->file, (TMWDLIST_MEMBER *)pFile);
    s14mem_free(pFile);

    /* Make sure last entry in database has SOF set to indicate it is last file */
    pFile = (S14SIM_FILE *)tmwdlist_getLast(&pDbHandle->file);
    if(pFile != TMWDEFS_NULL)
    {
      pFile->sof = (TMWTYPES_UCHAR)(pFile->sof | I14DEF_SOF_LFD_LAST_FILE);
    }

    return(TMWDEFS_TRUE);
  }

  return(TMWDEFS_FALSE);
}

/* function: s14sim_fileSectionAdd */
void * TMWDEFS_GLOBAL s14sim_fileSectionAdd(
  void *pHandle,
  TMWTYPES_ULONG  ioa,
  TMWTYPES_USHORT fileName,
  TMWTYPES_USHORT sectionLength,
  TMWTYPES_UCHAR  *pSectionData)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  pDbHandle->fileDirChanged = TMWDEFS_TRUE;

  return((void *)_fileSectionAdd(pHandle,ioa,fileName, sectionLength, pSectionData));
}

/* function: s14sim_fileLookup */
void * TMWDEFS_GLOBAL s14sim_fileLookup(
  void *pHandle, 
  TMWTYPES_ULONG  ioa,
  TMWTYPES_USHORT fileName)
{
  S14SIM_FILE *pFile;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  TMWTYPES_UCHAR index = 0;

  /* Search through list of files to find this file */
  while((pFile = (S14SIM_FILE *)tmwdlist_getEntry(&pDbHandle->file, index++))
    != TMWDEFS_NULL)
  {
    if((pFile->ioa == ioa) && (pFile->fileName == fileName))
    {
      return(pFile);
    }
  }
  return(TMWDEFS_NULL);
}

/* function: s14sim_fileSectionLookup */
void * TMWDEFS_GLOBAL s14sim_fileSectionLookup(
  void *pHandle, 
  TMWTYPES_ULONG  ioa,
  TMWTYPES_USHORT fileName,
  TMWTYPES_UCHAR  sectionName)
{
  S14SIM_FILE *pFile;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  TMWTYPES_UCHAR index = 0;

  /* sectionName is 1 based, but my list of sections is 0 based */
  sectionName--;

  /* Search through list of files to find this file */
  while((pFile = (S14SIM_FILE *)tmwdlist_getEntry(&pDbHandle->file, index++))
    != TMWDEFS_NULL)
  {
    if((pFile->ioa == ioa) && (pFile->fileName == fileName))
    {
      return((S14SIM_SECTION *)tmwdlist_getEntry(&pFile->sections, sectionName));
    }
  }
  return(TMWDEFS_NULL);
}

/* function: s14sim_callFileResponse */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_fileCallFileResp(
  void *pHandle, 
  TMWTYPES_ULONG  ioa,
  TMWTYPES_USHORT fileName,
  TMWTYPES_ULONG  fileLength,
  TMWTYPES_UCHAR *pScq)
{
  S14SIM_FILE *pFile = (S14SIM_FILE *)s14sim_fileLookup(pHandle, ioa, fileName);
  
  /* If file was found, delete it, so we can start fresh */
  if(pFile != TMWDEFS_NULL)
  {
    _fileDelete(pHandle, ioa, fileName);
  }

  /* Add the requested file     */
  /* handle, ioa, fileName, SOF */
  pFile = _fileAdd(pHandle, ioa, fileName, I14DEF_SOF_FOR_IS_FILE);

  if(pFile != TMWDEFS_NULL)
  {
    pFile->expectedFileLength = fileLength;
    *pScq = I14DEF_SCQ_CALL_FILE;
  }
  else
  {
    /* couldn't create file, tell master this failed */
    *pScq = I14DEF_SCQ_DEACT_FILE; 
  }

  return(TMWDEFS_TRUE);
}

/* function: s14sim_fileCallSectionResp */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_fileCallSectionResp(
  void *pHandle, 
  TMWTYPES_ULONG  ioa,
  TMWTYPES_USHORT fileName,
  TMWTYPES_UCHAR  sectionName,
  TMWTYPES_ULONG  sectionLength,
  TMWTYPES_UCHAR *pScq)
{
  S14SIM_SECTION *pSection;

  /* See if section already exists */
  pSection = (S14SIM_SECTION *)s14sim_fileSectionLookup(pHandle, ioa, fileName, sectionName);

  /* if not add the section */
  if(pSection == TMWDEFS_NULL)
  {
    pSection = _fileSectionAdd(pHandle, ioa, fileName, 0, TMWDEFS_NULL);
  }

  if(pSection != TMWDEFS_NULL)
  {
    pSection->expectedSectionLength = sectionLength;
    pSection->sectionLength = 0;
    *pScq = I14DEF_SCQ_CALL_SECTION;
    return(TMWDEFS_TRUE);
  }
  
  /* if failure, deactivate section */
  *pScq = I14DEF_SCQ_DEACT_SECTION;
  return(TMWDEFS_FALSE);
}

/* function: s14sim_fileSegmentStore */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_fileSegmentStore(
  void *pHandle, 
  TMWTYPES_ULONG  ioa,
  TMWTYPES_USHORT fileName,
  TMWTYPES_UCHAR  sectionName,
  TMWTYPES_ULONG  segmentLength,
  TMWTYPES_UCHAR *pSegmentData)
{
  S14SIM_SECTION *pSection;

  /* Get pointer to section */
  pSection = (S14SIM_SECTION *)s14sim_fileSectionLookup(pHandle, ioa, fileName, sectionName);

  /* If section exists copy segment data onto end of section */
  if(pSection != TMWDEFS_NULL)
  {
    if((pSection->sectionLength + segmentLength) <= S14SIM_FILE_MAX_SECTION_SIZE)
    {
      memcpy((pSection->data + pSection->sectionLength), pSegmentData, segmentLength);
      pSection->sectionLength += segmentLength;
      return(TMWDEFS_TRUE);
    }
  }
  
  return(TMWDEFS_FALSE);
}

/* function: s14sim_fileAckLastSegmentResp */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_fileAckLastSectOrSegResp(
  void *pHandle, 
  TMWTYPES_ULONG  ioa,
  TMWTYPES_USHORT fileName,
  TMWTYPES_UCHAR  sectionName,
  TMWTYPES_UCHAR  lsq,
  TMWTYPES_BOOL   checksumGood,
  TMWTYPES_UCHAR  *pAfq)
{
  S14SIM_FILE *pFile;
  S14SIM_SECTION *pSection;

  /* If FLSNA indicated end of section */
  if(lsq == I14DEF_LSQ_SECT_XFER_NO_DEACT)
  {
    *pAfq = I14DEF_AFQ_SECT_XFER_NEG_ACT;
    if(checksumGood == TMWDEFS_FALSE)
    {
      return(TMWDEFS_TRUE);
    }

    /* Verify entire length of section has been received */
    if(((pSection = (S14SIM_SECTION *)s14sim_fileSectionLookup(pHandle, ioa, fileName, sectionName)) != TMWDEFS_NULL) 
      && (pSection->expectedSectionLength == pSection->sectionLength))
    {
      if((pFile = (S14SIM_FILE *)s14sim_fileLookup(pHandle, ioa, fileName)) != TMWDEFS_NULL)
      {
        pFile->fileLength += pSection->sectionLength;
        *pAfq = I14DEF_AFQ_SECT_XFER_POS_ACT;
      }
    }
    return(TMWDEFS_TRUE);
  }

  /* If FLSNA indicated end of file */
  else if(lsq == I14DEF_LSQ_FILE_XFER_NO_DEACT)
  {
    *pAfq = I14DEF_AFQ_FILE_XFER_NEG_ACT;
    if(checksumGood == TMWDEFS_FALSE)
    {
      return(TMWDEFS_TRUE);
    }

    if((pFile = (S14SIM_FILE *)s14sim_fileLookup(pHandle, ioa, fileName)) != TMWDEFS_NULL)
    {
      /* Verify entire length of file has been received */
      if(pFile->expectedFileLength == pFile->fileLength)
      {
        *pAfq = I14DEF_AFQ_FILE_XFER_POS_ACT;
      }
    }
    return(TMWDEFS_TRUE);
  }

  return(TMWDEFS_FALSE);
}

/* function: s14sim_fileGetDirectoryEntry */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_fileGetDirectoryEntry(
  void *pHandle, 
  TMWTYPES_UCHAR   index,
  TMWTYPES_ULONG  *pIoa,
  TMWTYPES_USHORT *pFileName,
  TMWTYPES_ULONG  *pFileLength,
  TMWDTIME       *pFileTime,
  TMWTYPES_UCHAR  *pSof)
{
  S14SIM_FILE *pFile;
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;

  /* return information from directory entry number 'index' */
  if((pFile = (S14SIM_FILE *)tmwdlist_getEntry(&pDbHandle->file, index))
    != TMWDEFS_NULL)
  {
    *pIoa        = pFile->ioa;
    *pFileName   = pFile->fileName;
    *pFileLength = pFile->fileLength; 
    *pFileTime   = pFile->fileTime;
    *pSof        = pFile->sof;
    return(TMWDEFS_TRUE);
  }
  
  return(TMWDEFS_FALSE);
} 

/* function: s14sim_fileSelectFile */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_fileSelectFile(
  void *pHandle, 
  TMWTYPES_ULONG   ioa,
  TMWTYPES_USHORT  fileName,
  TMWTYPES_ULONG  *pFileLength,
  TMWTYPES_UCHAR  *pFrq)
{
  S14SIM_FILE *pFile;

  if((pFile = (S14SIM_FILE *)s14sim_fileLookup(pHandle, ioa, fileName)) != TMWDEFS_NULL)
  {
    /* If it is a file say yes, if a directory say no */
    if((pFile->sof & I14DEF_SOF_FOR_MASK) == I14DEF_SOF_FOR_IS_FILE)
    {
      *pFileLength = pFile->fileLength;
      *pFrq = I14DEF_FRQ_POS_CONFIRM;
      return(TMWDEFS_TRUE);
    }
  }
  return(TMWDEFS_FALSE);
}

/* function: s14sim_fileQueryLog */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_fileQueryLog(
  void            *pHandle, 
  TMWTYPES_ULONG   ioa,
  TMWTYPES_USHORT  fileName,
  TMWDTIME        *pRangeStartTime,
  TMWDTIME        *pRangeStopTime, 
  TMWTYPES_ULONG  *pFileLength,
  TMWTYPES_UCHAR  *pFrq)
{   
  S14SIM_FILE *pFile;

  /* don't do anything with start and stop time in this simulation */
  TMWTARG_UNUSED_PARAM(pRangeStartTime);
  TMWTARG_UNUSED_PARAM(pRangeStopTime);

  if((pFile = (S14SIM_FILE *)s14sim_fileLookup(pHandle, ioa, fileName)) != TMWDEFS_NULL)
  {
    /* If it is a file say yes, if a directory say no */
    if((pFile->sof & I14DEF_SOF_FOR_MASK) == I14DEF_SOF_FOR_IS_FILE)
    {
      *pFileLength = pFile->fileLength;
      *pFrq = I14DEF_FRQ_POS_CONFIRM;
      return(TMWDEFS_TRUE);
    }
  }
  return(TMWDEFS_FALSE);
}

/* function: s14sim_fileGetSegment */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_fileGetSegment(
  void *pHandle, 
  TMWTYPES_ULONG  ioa,
  TMWTYPES_USHORT fileName,
  TMWTYPES_UCHAR  sectionName,
  TMWTYPES_UCHAR  maxLength,
  TMWTYPES_UCHAR *pSegmentLength,
  TMWTYPES_UCHAR *pSegmentData)
{
  S14SIM_SECTION *pSection;
  TMWTYPES_LONG bytesToSend;

  if((pSection = (S14SIM_SECTION *)s14sim_fileSectionLookup(pHandle, ioa, fileName, sectionName)) != TMWDEFS_NULL)
  {
    if((bytesToSend = pSection->sectionLength - pSection->byteIndex) > 0)
    {
      if(bytesToSend > maxLength)
      {
        bytesToSend = maxLength;
      }
      
      *pSegmentLength = (TMWTYPES_UCHAR)bytesToSend;
      memcpy(pSegmentData, (pSection->data+pSection->byteIndex), bytesToSend);

      pSection->byteIndex = (TMWTYPES_USHORT)(pSection->byteIndex + bytesToSend);

      return(TMWDEFS_TRUE);
    }
    else
    {
      /* No more bytes in this section */
      *pSegmentLength = 0;
      return(TMWDEFS_TRUE);
    }
  }
  return(TMWDEFS_FALSE);
}

/* function: s14sim_fileGetLastSegmentResp */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_fileGetLastSegmentResp(
  void *pHandle, 
  TMWTYPES_ULONG  ioa,
  TMWTYPES_USHORT fileName,
  TMWTYPES_UCHAR  sectionName,
  TMWTYPES_UCHAR  *pLsq)
{
  S14SIM_SECTION *pSection;

  if((pSection = (S14SIM_SECTION *)s14sim_fileSectionLookup(pHandle, ioa, fileName, sectionName)) != TMWDEFS_NULL)
  {
    *pLsq = I14DEF_LSQ_SECT_XFER_NO_DEACT;
    return(TMWDEFS_TRUE);
  }
  return(TMWDEFS_FALSE);
}

/* function: s14sim_fileGetLastSectionResp */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_fileGetLastSectionResp(
  void *pHandle, 
  TMWTYPES_ULONG  ioa,
  TMWTYPES_USHORT fileName,
  TMWTYPES_UCHAR  *pLsq)
{
  S14SIM_FILE *pFile;

  if((pFile = (S14SIM_FILE *)s14sim_fileLookup(pHandle, ioa, fileName)) != TMWDEFS_NULL)
  {
    *pLsq = I14DEF_LSQ_FILE_XFER_NO_DEACT;
    return(TMWDEFS_TRUE);
  }
  return(TMWDEFS_FALSE);
}

/* function: s14sim_fileAckFile */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_fileAckFile(
  void *pHandle, 
  TMWTYPES_ULONG  ioa,
  TMWTYPES_USHORT fileName)
{
  /* Call exported function, since it causes directory to be sent.*/
  return(s14sim_fileDelete(pHandle, ioa, fileName));
}

/* function: s14sim_fileGetSectionReady */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_fileGetSectionReady(
  void *pHandle, 
  TMWTYPES_ULONG  ioa,
  TMWTYPES_USHORT fileName,
  TMWTYPES_UCHAR  sectionName,
  TMWTYPES_ULONG *pSectionLength,
  TMWTYPES_UCHAR *pSrq)
{
  S14SIM_SECTION *pSection;

  if((pSection = (S14SIM_SECTION *)s14sim_fileSectionLookup(pHandle, ioa, fileName, sectionName)) != TMWDEFS_NULL)
  {
    /* Set index to read from */
    pSection->byteIndex = 0;

    *pSectionLength = pSection->sectionLength;
    *pSrq = I14DEF_SRQ_SECT_READY;
    return(TMWDEFS_TRUE);
  }
  return(TMWDEFS_FALSE);
}

/* function: s14sim_fileDelete */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_fileDelete(
  void *pHandle, 
  TMWTYPES_ULONG  ioa,
  TMWTYPES_USHORT fileName)
{

  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  pDbHandle->fileDirChanged = TMWDEFS_TRUE;

  return(_fileDelete(pHandle, ioa, fileName));
}


/* function: s14sim_fileDeleteResp */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_fileDeleteResp(
  void *pHandle, 
  TMWTYPES_ULONG  ioa,
  TMWTYPES_USHORT fileName,
  TMWTYPES_UCHAR *pFrq)
{ 
  if(_fileDelete(pHandle, ioa, fileName) == TMWDEFS_TRUE)
  {
    *pFrq = I14DEF_FRQ_POS_CONFIRM;
    return(TMWDEFS_TRUE);
  }

  return(TMWDEFS_FALSE);
}

/* function: s14sim_fileDirChanged */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14sim_fileDirectoryChanged(
  void *pHandle)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  if(pDbHandle->fileDirChanged == TMWDEFS_TRUE)
  {
    pDbHandle->fileDirChanged = TMWDEFS_FALSE;
    return(TMWDEFS_TRUE);
  }
  return(TMWDEFS_FALSE);
}

/* function: s14sim_fileGetFile */
void * TMWDEFS_GLOBAL s14sim_fileGetFile(
  void *pHandle, 
  TMWTYPES_USHORT index)
{
  S14SIM_DATABASE *pDbHandle = (S14SIM_DATABASE *)pHandle;
  return(tmwdlist_getEntry(&pDbHandle->file, index));
}

/* function: s14sim_fileDiagShow */
void TMWDEFS_GLOBAL s14sim_fileDiagShow(
  TMWSCTR *pSector,
  void *pPoint)
{  
#if TMWCNFG_SUPPORT_DIAG
  TMWDIAG_ANLZ_ID anlzId;
  char ioaBuf[32];
  char buf[128];
  S14SIM_FILE *pFile = (S14SIM_FILE *)pPoint;
 
  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, TMWDIAG_ID_MMI) == TMWDEFS_FALSE)
  {
    return;
  }
  i14diag_formatIOA(pSector, pFile->ioa, (TMWTYPES_CHAR *)ioaBuf);
  tmwtarg_snprintf(buf, sizeof(buf), "%-20s %s  %5d    %6d    %4d       0x%02x\n", " ", ioaBuf, pFile->fileName, 
    pFile->fileLength, tmwdlist_size(&pFile->sections), pFile->sof);
  tmwdiag_putLine(&anlzId, buf);
#else
  TMWTARG_UNUSED_PARAM(pSector);
  TMWTARG_UNUSED_PARAM(pPoint);
#endif
}
#endif /* S14DATA_SUPPORT_FILE */

static TMWTYPES_CHAR * TMWDEFS_LOCAL _putBoolField(
  TMWTYPES_CHAR *result, 
  TMWTYPES_CHAR *prefix, 
  TMWTYPES_CHAR *name, 
  TMWTYPES_BOOL flag)
{
  TMWTYPES_CHAR buf[128];
  tmwtarg_snprintf(buf, sizeof(buf), "%s<%s>%s</%s>\n", prefix, name, flag ? "true" : "false", name);
  return(tmwtarg_appendString(result, buf));
}

static TMWTYPES_CHAR * TMWDEFS_LOCAL _putFlags(
  TMWTYPES_CHAR *result,
  TMWTYPES_CHAR flags)
{
    result = tmwtarg_appendString(result, "    <flags>\n");
    result = _putBoolField(result, "      ", "overflow",    TMWDEFS_TOBOOL(flags, I14DEF_QUALITY_OV));
    result = _putBoolField(result, "      ", "invalid",     TMWDEFS_TOBOOL(flags, I14DEF_QUALITY_IV));
    result = _putBoolField(result, "      ", "notTopical",  TMWDEFS_TOBOOL(flags, I14DEF_QUALITY_NT));
    result = _putBoolField(result, "      ", "substituted", TMWDEFS_TOBOOL(flags, I14DEF_QUALITY_SB));
    result = _putBoolField(result, "      ", "blocked",     TMWDEFS_TOBOOL(flags, I14DEF_QUALITY_BL));
    result = tmwtarg_appendString(result, "    </flags>\n");
    return(result);
}

#if S14DATA_SUPPORT_MIT
static TMWTYPES_CHAR * TMWDEFS_LOCAL _putMitFlags(
  TMWTYPES_CHAR *result,
  TMWTYPES_CHAR flags)
{
  result = tmwtarg_appendString(result, "    <flags>\n");
  result = _putBoolField(result, "      ", "counterOverflow", TMWDEFS_TOBOOL(flags, I14DEF_BCR_CY));
  result = _putBoolField(result, "      ", "counterAdjusted", TMWDEFS_TOBOOL(flags, I14DEF_BCR_CA));
  result = _putBoolField(result, "      ", "counterInvalid",  TMWDEFS_TOBOOL(flags, I14DEF_BCR_IV));
  result = tmwtarg_appendString(result, "    </flags>\n");
  return(result);
}
#endif

#if 0
Not in schema 
static TMWTYPES_CHAR * TMWDEFS_LOCAL _putMsrndIOAs(
  TMWTYPES_CHAR *result,
  void *pHandle,
  void *pPoint)
{
  TMWTYPES_ULONG pacnaIOA;
  TMWTYPES_ULONG pmenaIOAs[4];
  TMWTYPES_CHAR buf[128];
    
  if(s14sim_mmenaGetIOAs(pHandle, pPoint, &pacnaIOA, &pmenaIOAs[0]))
  {
    result = tmwtarg_appendString(result, "    <parameters>\n");
    tmwtarg_snprintf(buf, sizeof(buf), "      <pacnaIOA>%d</pacnaIOA>\n", pacnaIOA);
    result = tmwtarg_appendString(result, buf);
    tmwtarg_snprintf(buf, sizeof(buf), "      <lowLimitIOA>%d</lowLimitIOA>\n", pmenaIOAs[0]);
    result = tmwtarg_appendString(result, buf);
    tmwtarg_snprintf(buf, sizeof(buf), "      <highLimitIOA>%d</highLimitIOA>\n", pmenaIOAs[1]);
    result = tmwtarg_appendString(result, buf);
    tmwtarg_snprintf(buf, sizeof(buf), "      <thresholdIOA>%d</thresholdIOA>\n", pmenaIOAs[2]);
    result = tmwtarg_appendString(result, buf);
    tmwtarg_snprintf(buf, sizeof(buf), "      <smoothingIOA>%d</smoothingIOA>\n", pmenaIOAs[3]);
    result = tmwtarg_appendString(result, buf);
    result = tmwtarg_appendString(result, "    </parameters>\n");
  }
  return (result);
}
#endif

static TMWTYPES_CHAR * TMWDEFS_LOCAL _putGroupMask(
  TMWTYPES_CHAR *result, 
  TMWDEFS_GROUP_MASK mask)
{
  if(mask & TMWDEFS_GROUP_MASK_ANY)
  {
    if(mask & TMWDEFS_GROUP_MASK_GENERAL)
      result = tmwtarg_appendString(result, "    <includedInGeneralInterrogation>true</includedInGeneralInterrogation>\n");
    else
      /* Since restore from xml assumes true for some types, explicitly set this to false */
      result = tmwtarg_appendString(result, "    <includedInGeneralInterrogation>false</includedInGeneralInterrogation>\n");

    if(mask & TMWDEFS_GROUP_MASK_CYCLIC)
      result = tmwtarg_appendString(result, "    <includedInCyclicResponse>true</includedInCyclicResponse>\n"); 
    else
      /* Since restore from xml assumes true for some types, explicitly set this to false */
      result = tmwtarg_appendString(result, "    <includedInCyclicResponse>false</includedInCyclicResponse>\n");

    if(mask & TMWDEFS_GROUP_MASK_BACKGROUND)
      result = tmwtarg_appendString(result, "    <includedInBackgroundScan>true</includedInBackgroundScan>\n");

    result = tmwtarg_appendString(result, "    <interrogationGroups>\n");

    if(mask & TMWDEFS_GROUP_MASK_1)
      result = tmwtarg_appendString(result, "      <interrogationGroup>1</interrogationGroup>\n");

    if(mask & TMWDEFS_GROUP_MASK_2)
      result = tmwtarg_appendString(result, "      <interrogationGroup>2</interrogationGroup>\n");

    if(mask & TMWDEFS_GROUP_MASK_3)
      result = tmwtarg_appendString(result, "      <interrogationGroup>3</interrogationGroup>\n");

    if(mask & TMWDEFS_GROUP_MASK_4)
      result = tmwtarg_appendString(result, "      <interrogationGroup>4</interrogationGroup>\n");

    if(mask & TMWDEFS_GROUP_MASK_5)
      result = tmwtarg_appendString(result, "      <interrogationGroup>5</interrogationGroup>\n");

    if(mask & TMWDEFS_GROUP_MASK_6)
      result = tmwtarg_appendString(result, "      <interrogationGroup>6</interrogationGroup>\n");

    if(mask & TMWDEFS_GROUP_MASK_7)
      result = tmwtarg_appendString(result, "      <interrogationGroup>7</interrogationGroup>\n");

    if(mask & TMWDEFS_GROUP_MASK_8)
      result = tmwtarg_appendString(result, "      <interrogationGroup>8</interrogationGroup>\n");

    if(mask & TMWDEFS_GROUP_MASK_9)
      result = tmwtarg_appendString(result, "      <interrogationGroup>9</interrogationGroup>\n");

    if(mask & TMWDEFS_GROUP_MASK_10)
      result = tmwtarg_appendString(result, "      <interrogationGroup>10</interrogationGroup>\n");

    if(mask & TMWDEFS_GROUP_MASK_11)
      result = tmwtarg_appendString(result, "      <interrogationGroup>11</interrogationGroup>\n");

    if(mask & TMWDEFS_GROUP_MASK_12)
      result = tmwtarg_appendString(result, "      <interrogationGroup>12</interrogationGroup>\n");

    if(mask & TMWDEFS_GROUP_MASK_13)
      result = tmwtarg_appendString(result, "      <interrogationGroup>13</interrogationGroup>\n");

    if(mask & TMWDEFS_GROUP_MASK_14)
      result = tmwtarg_appendString(result, "      <interrogationGroup>14</interrogationGroup>\n");

    if(mask & TMWDEFS_GROUP_MASK_15)
      result = tmwtarg_appendString(result, "      <interrogationGroup>15</interrogationGroup>\n");

    if(mask & TMWDEFS_GROUP_MASK_16)
      result = tmwtarg_appendString(result, "      <interrogationGroup>16</interrogationGroup>\n");

    result = tmwtarg_appendString(result, "    </interrogationGroups>\n");
  }

  return(result);
}
  
/* routine: s14sim_saveDatabase */
TMWTYPES_CHAR * TMWDEFS_GLOBAL s14sim_saveDatabase(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  TMWTYPES_CHAR *result = TMWDEFS_NULL;
  TMWTYPES_CHAR buf[512];
  TMWTYPES_USHORT index;
  void *pPoint;

  result = tmwtarg_appendString(result, "<?xml version=\"1.0\"?>\n");
  result = tmwtarg_appendString(result, "<tmw:i14data\n");
  result = tmwtarg_appendString(result, " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n");
  result = tmwtarg_appendString(result, " xmlns:tmw=\"http://www.TriangleMicroWorks.com/TestHarness/Schemas/i14data\">\n");
  result = tmwtarg_appendString(result, " <device>\n");

#if S14DATA_SUPPORT_MSP
  index = 0;
  while((pPoint = s14sim_mspGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    TMWTYPES_CHAR *desc = tmwsim_getDescription((TMWSIM_POINT *)pPoint);
    TMWTYPES_UCHAR flags = s14sim_mspGetFlagsAndValue(pPoint);
    TMWDEFS_GROUP_MASK groupMask = s14sim_getGroupMask(pPoint);
    TMWTYPES_ULONG address = s14sim_getInfoObjAddr(pPoint);

    result = tmwtarg_appendString(result, "   <msp>\n");

    tmwtarg_snprintf(buf, sizeof(buf), "    <address>%d</address>\n", address);
    result = tmwtarg_appendString(result, buf);

    if(desc && (strlen(desc) > 0))
    {
      tmwtarg_snprintf(buf, sizeof(buf), "    <description>%s</description>\n", desc);
      result = tmwtarg_appendString(result, buf);
    }
    
    result = _putGroupMask(result, groupMask);

    if(tmwsim_getSelectRequired((TMWSIM_POINT *)pPoint))
      result = tmwtarg_appendString(result, "    <selectRequired>true</selectRequired>\n");

    result = _putFlags(result, (TMWTYPES_CHAR)(flags & 0xf0));

    result = _putBoolField(result, "    ", "value", (TMWTYPES_CHAR)(flags & I14DEF_SIQ_MASK));

    result = tmwtarg_appendString(result, "   </msp>\n");
    index += 1;
  }
#endif

#if S14DATA_SUPPORT_MDP
  index = 0;
  while((pPoint = s14sim_mdpGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    TMWTYPES_CHAR *desc = tmwsim_getDescription((TMWSIM_POINT *)pPoint);
    TMWTYPES_UCHAR flags = s14sim_mdpGetFlagsAndValue(pPoint);
    TMWDEFS_GROUP_MASK groupMask = s14sim_getGroupMask(pPoint);
    TMWTYPES_ULONG address = s14sim_getInfoObjAddr(pPoint);

    result = tmwtarg_appendString(result, "   <mdp>\n");

    tmwtarg_snprintf(buf, sizeof(buf), "    <address>%d</address>\n", address);
    result = tmwtarg_appendString(result, buf);

    if(desc && (strlen(desc) > 0))
    {
      tmwtarg_snprintf(buf, sizeof(buf), "    <description>%s</description>\n", desc);
      result = tmwtarg_appendString(result, buf);
    }
    
    result = _putGroupMask(result, groupMask);

    if(tmwsim_getSelectRequired((TMWSIM_POINT *)pPoint))
      result = tmwtarg_appendString(result, "    <selectRequired>true</selectRequired>\n");

    result = _putFlags(result, (TMWTYPES_CHAR)(flags & 0xf0));

    switch(flags & I14DEF_DIQ_MASK)
    {
    case I14DEF_DIQ_INTERMEDIATE:
      result = tmwtarg_appendString(result, "    <value>intermediate</value>\n");
      break;

    case I14DEF_DIQ_OFF:
      result = tmwtarg_appendString(result, "    <value>off</value>\n");
      break;

    case I14DEF_DIQ_ON:
      result = tmwtarg_appendString(result, "    <value>on</value>\n");
      break;

    case I14DEF_DIQ_INDETERMINATE:
      result = tmwtarg_appendString(result, "    <value>indeterminate</value>\n");
      break;
    }

    result = tmwtarg_appendString(result, "   </mdp>\n");
    index += 1;
  }
#endif

#if S14DATA_SUPPORT_MST
  index = 0;
  while((pPoint = s14sim_mstGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    TMWTYPES_CHAR *desc = tmwsim_getDescription((TMWSIM_POINT *)pPoint);
    TMWDEFS_GROUP_MASK groupMask = s14sim_getGroupMask(pPoint);
    TMWTYPES_ULONG address = s14sim_getInfoObjAddr(pPoint);
    TMWTYPES_UCHAR value = s14sim_mstGetValue(pPoint);
    TMWTYPES_UCHAR flags = s14sim_getFlags(pPoint);

    result = tmwtarg_appendString(result, "   <mst>\n");

    tmwtarg_snprintf(buf, sizeof(buf), "    <address>%d</address>\n", address);
    result = tmwtarg_appendString(result, buf);

    if(desc && (strlen(desc) > 0))
    {
      tmwtarg_snprintf(buf, sizeof(buf), "    <description>%s</description>\n", desc);
      result = tmwtarg_appendString(result, buf);
    }
    
    result = _putGroupMask(result, groupMask);

    if(tmwsim_getSelectRequired((TMWSIM_POINT *)pPoint))
      result = tmwtarg_appendString(result, "    <selectRequired>true</selectRequired>\n");

    result = _putFlags(result, flags);

    tmwtarg_snprintf(buf, sizeof(buf), "    <value>%d</value>\n", value);
    result = tmwtarg_appendString(result, buf);

    result = tmwtarg_appendString(result, "   </mst>\n");
    index += 1;
  }
#endif

#if S14DATA_SUPPORT_MBO
  index = 0;
  while((pPoint = s14sim_mboGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    TMWTYPES_CHAR *desc = tmwsim_getDescription((TMWSIM_POINT *)pPoint);
    TMWDEFS_GROUP_MASK groupMask = s14sim_getGroupMask(pPoint);
    TMWTYPES_ULONG address = s14sim_getInfoObjAddr(pPoint);
    TMWTYPES_ULONG value = s14sim_mboGetValue(pPoint);
    TMWTYPES_UCHAR flags = s14sim_getFlags(pPoint);

    result = tmwtarg_appendString(result, "   <mbo>\n");

    tmwtarg_snprintf(buf, sizeof(buf), "    <address>%d</address>\n", address);
    result = tmwtarg_appendString(result, buf);

    if(desc && (strlen(desc) > 0))
    {
      tmwtarg_snprintf(buf, sizeof(buf), "    <description>%s</description>\n", desc);
      result = tmwtarg_appendString(result, buf);
    }
    
    result = _putGroupMask(result, groupMask);

    /* selectRequired does not apply to mbo or cbona */

    result = _putFlags(result, flags);

    tmwtarg_snprintf(buf, sizeof(buf), "    <value>%u</value>\n", value);
    result = tmwtarg_appendString(result, buf);

    result = tmwtarg_appendString(result, "   </mbo>\n");
    index += 1;
  }
#endif

#if S14DATA_SUPPORT_MME_A
  index = 0;
  while((pPoint = s14sim_mmenaGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    TMWTYPES_ULONG pacnaIOA;
    TMWTYPES_ULONG pmenIOA[4];
    TMWTYPES_CHAR *desc = tmwsim_getDescription((TMWSIM_POINT *)pPoint);
    TMWDEFS_GROUP_MASK groupMask = s14sim_getGroupMask(pPoint);
    TMWTYPES_ULONG address = s14sim_getInfoObjAddr(pPoint);
    TMWTYPES_SHORT value = s14sim_mmenaGetValue(pPoint);
    TMWTYPES_UCHAR flags = s14sim_getFlags(pPoint);

    result = tmwtarg_appendString(result, "   <mmena>\n");

    tmwtarg_snprintf(buf, sizeof(buf), "    <address>%d</address>\n", address);
    result = tmwtarg_appendString(result, buf);

    if(desc && (strlen(desc) > 0))
    {
      tmwtarg_snprintf(buf, sizeof(buf), "    <description>%s</description>\n", desc);
      result = tmwtarg_appendString(result, buf);
    }
    
    result = _putGroupMask(result, groupMask);

    s14sim_mmenGetIOAs(pPoint, &pacnaIOA, pmenIOA);
    tmwtarg_snprintf(buf, sizeof(buf), "    <pmenaThresholdIOA>%d</pmenaThresholdIOA>\n", pmenIOA[0]);
    result = tmwtarg_appendString(result, buf);
    tmwtarg_snprintf(buf, sizeof(buf), "    <pmenaSmoothingIOA>%d</pmenaSmoothingIOA>\n",  pmenIOA[1]);
    result = tmwtarg_appendString(result, buf);
    tmwtarg_snprintf(buf, sizeof(buf), "    <pmenaLowIOA>%d</pmenaLowIOA>\n",  pmenIOA[2]);
    result = tmwtarg_appendString(result, buf);
    tmwtarg_snprintf(buf, sizeof(buf), "    <pmenaHighIOA>%d</pmenaHighIOA>\n",  pmenIOA[3]);
    result = tmwtarg_appendString(result, buf);
    tmwtarg_snprintf(buf, sizeof(buf), "    <pacnaIOA>%d</pacnaIOA>\n", pacnaIOA);
    result = tmwtarg_appendString(result, buf);

    if(tmwsim_getSelectRequired((TMWSIM_POINT *)pPoint))
      result = tmwtarg_appendString(result, "    <selectRequired>true</selectRequired>\n");

    result = _putFlags(result, flags);

    /* The real type is signed 16 bit, even though we like to think of it as just 16 bits 
     * Store it as signed to match schema for save/restore
     */
    tmwtarg_snprintf(buf, sizeof(buf), "    <value>%d</value>\n", value);
    result = tmwtarg_appendString(result, buf);

    result = tmwtarg_appendString(result, "   </mmena>\n");
    index += 1;
  }
#endif

#if S14DATA_SUPPORT_MME_B
  index = 0;
  while((pPoint = s14sim_mmenbGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    TMWTYPES_CHAR *desc = tmwsim_getDescription((TMWSIM_POINT *)pPoint);
    TMWDEFS_GROUP_MASK groupMask = s14sim_getGroupMask(pPoint);
    TMWTYPES_ULONG address = s14sim_getInfoObjAddr(pPoint);
    TMWTYPES_SHORT value = s14sim_mmenbGetValue(pPoint);
    TMWTYPES_UCHAR flags = s14sim_getFlags(pPoint);

    result = tmwtarg_appendString(result, "   <mmenb>\n");

    tmwtarg_snprintf(buf, sizeof(buf), "    <address>%d</address>\n", address);
    result = tmwtarg_appendString(result, buf);

    if(desc && (strlen(desc) > 0))
    {
      tmwtarg_snprintf(buf, sizeof(buf), "    <description>%s</description>\n", desc);
      result = tmwtarg_appendString(result, buf);
    }
    
    result = _putGroupMask(result, groupMask);

    if(tmwsim_getSelectRequired((TMWSIM_POINT *)pPoint))
      result = tmwtarg_appendString(result, "    <selectRequired>true</selectRequired>\n");

    result = _putFlags(result, flags);

    tmwtarg_snprintf(buf, sizeof(buf), "    <value>%d</value>\n", value);
    result = tmwtarg_appendString(result, buf);

    result = tmwtarg_appendString(result, "   </mmenb>\n");
    index += 1;
  }
#endif

#if S14DATA_SUPPORT_MME_C
  index = 0;
  while((pPoint = s14sim_mmencGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    TMWTYPES_CHAR *desc = tmwsim_getDescription((TMWSIM_POINT *)pPoint);
    TMWDEFS_GROUP_MASK groupMask = s14sim_getGroupMask(pPoint);
    TMWTYPES_ULONG address = s14sim_getInfoObjAddr(pPoint);
    TMWTYPES_SFLOAT value = s14sim_mmencGetValue(pPoint);
    TMWTYPES_UCHAR flags = s14sim_getFlags(pPoint);

    result = tmwtarg_appendString(result, "   <mmenc>\n");

    tmwtarg_snprintf(buf, sizeof(buf), "    <address>%d</address>\n", address);
    result = tmwtarg_appendString(result, buf);

    if(desc && (strlen(desc) > 0))
    {
      tmwtarg_snprintf(buf, sizeof(buf), "    <description>%s</description>\n", desc);
      result = tmwtarg_appendString(result, buf);
    }
    
    result = _putGroupMask(result, groupMask);

    if(tmwsim_getSelectRequired((TMWSIM_POINT *)pPoint))
      result = tmwtarg_appendString(result, "    <selectRequired>true</selectRequired>\n");

    result = _putFlags(result, flags);

    tmwtarg_snprintf(buf, sizeof(buf), "    <value>%f</value>\n", value);
    result = tmwtarg_appendString(result, buf);

    result = tmwtarg_appendString(result, "   </mmenc>\n");
    index += 1;
  }
#endif

#if S14DATA_SUPPORT_MMEND
  index = 0;
  while((pPoint = s14sim_mmendGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    TMWTYPES_CHAR *desc = tmwsim_getDescription((TMWSIM_POINT *)pPoint);
    TMWDEFS_GROUP_MASK groupMask = s14sim_getGroupMask(pPoint);
    TMWTYPES_ULONG address = s14sim_getInfoObjAddr(pPoint);
    TMWTYPES_SHORT value = s14sim_mmendGetValue(pPoint);

    result = tmwtarg_appendString(result, "   <mmend>\n");

    tmwtarg_snprintf(buf, sizeof(buf), "    <address>%d</address>\n", address);
    result = tmwtarg_appendString(result, buf);

    if(desc && (strlen(desc) > 0))
    {
      tmwtarg_snprintf(buf, sizeof(buf), "    <description>%s</description>\n", desc);
      result = tmwtarg_appendString(result, buf);
    }
    
    result = _putGroupMask(result, groupMask);
    
    /* The real type is signed 16 bit, even though we like to think of it as just 16 bits 
     * Store it as signed to match schema for save/restore
     */
    tmwtarg_snprintf(buf, sizeof(buf), "    <value>%d</value>\n", value);
    result = tmwtarg_appendString(result, buf);

    result = tmwtarg_appendString(result, "   </mmend>\n");
    index += 1;
  }
#endif

#if S14DATA_SUPPORT_MIT
  index = 0;
  while((pPoint = s14sim_mitGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    TMWTYPES_CHAR *desc = tmwsim_getDescription((TMWSIM_POINT *)pPoint);
    TMWDEFS_GROUP_MASK groupMask = s14sim_getGroupMask(pPoint);
    TMWTYPES_ULONG address = s14sim_getInfoObjAddr(pPoint);
    TMWTYPES_ULONG value = s14sim_mitGetValue(pPoint);
    TMWTYPES_UCHAR flags = s14sim_getFlags(pPoint);

    result = tmwtarg_appendString(result, "   <mit>\n");

    tmwtarg_snprintf(buf, sizeof(buf), "    <address>%d</address>\n", address);
    result = tmwtarg_appendString(result, buf);

    if(desc && (strlen(desc) > 0))
    {
      tmwtarg_snprintf(buf, sizeof(buf), "    <description>%s</description>\n", desc);
      result = tmwtarg_appendString(result, buf);
    }
    
    result = _putGroupMask(result, groupMask);
    result = _putMitFlags(result, flags);

    tmwtarg_snprintf(buf, sizeof(buf), "    <sequence>%u</sequence>\n", (flags & I14DEF_BCR_SEQ_MASK));
    result = tmwtarg_appendString(result, buf);

    tmwtarg_snprintf(buf, sizeof(buf), "    <value>%u</value>\n", value);
    result = tmwtarg_appendString(result, buf);

    result = tmwtarg_appendString(result, "   </mit>\n");
    index += 1;
  }
#endif

#if S14DATA_SUPPORT_MITC
  index = 0;
  while((pPoint = s14sim_mitcGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    int i;
    int offset;
    TMWTYPES_CHAR *desc = tmwsim_getDescription((TMWSIM_POINT *)pPoint);
    TMWDEFS_GROUP_MASK groupMask = s14sim_getGroupMask(pPoint);
    TMWTYPES_ULONG address = s14sim_getInfoObjAddr(pPoint);
    TMWTYPES_UCHAR value[6];
    TMWTYPES_UCHAR flags = s14sim_getFlags(pPoint);

    s14sim_mitcGetValue(pPoint, value);

    result = tmwtarg_appendString(result, "   <mitc>\n");

    tmwtarg_snprintf(buf, sizeof(buf), "    <address>%d</address>\n", address);
    result = tmwtarg_appendString(result, buf);

    if(desc && (strlen(desc) > 0))
    {
      tmwtarg_snprintf(buf, sizeof(buf), "    <description>%s</description>\n", desc);
      result = tmwtarg_appendString(result, buf);
    }

    result = _putGroupMask(result, groupMask); 

    if(tmwsim_getSelectRequired((TMWSIM_POINT *)pPoint))
      result = tmwtarg_appendString(result, "    <selectRequired>true</selectRequired>\n");

    result = _putMitFlags(result, flags);

    tmwtarg_snprintf(buf, sizeof(buf), "    <sequence>%u</sequence>\n", (flags & I14DEF_BCR_SEQ_MASK));
    result = tmwtarg_appendString(result, buf);

    /* value is 6 bytes, convert to ascii string  */ 
    tmwtarg_snprintf(buf, sizeof(buf), "    <value> ");
    offset = 12;
    for(i=5; i>=0; i--)
    {
      tmwtarg_snprintf((TMWTYPES_CHAR*)(buf+offset), (sizeof(buf)-offset), "0x%02x ", value[i]);
      offset +=5;
    }
    tmwtarg_snprintf((TMWTYPES_CHAR*)(buf+offset), (sizeof(buf)-offset), "</value>\n");
    result = tmwtarg_appendString(result, buf);

    result = tmwtarg_appendString(result, "   </mitc>\n");
    index += 1;
  }
#endif

#if S14DATA_SUPPORT_MCTNA
  index = 0;
  while((pPoint = s14sim_mctnaGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    TMWTYPES_UCHAR len;
    int i;
    int offset;
    TMWTYPES_CHAR *desc = tmwsim_getDescription((TMWSIM_POINT *)pPoint);
    TMWTYPES_ULONG address = s14sim_getInfoObjAddr(pPoint);
    TMWTYPES_UCHAR value[100];

    len =  s14sim_mctnaGetValues(pPoint, 100, value); 
 
    result = tmwtarg_appendString(result, "   <mctna>\n");

    tmwtarg_snprintf(buf, sizeof(buf), "    <address>%d</address>\n", address);
    result = tmwtarg_appendString(result, buf);

    if(desc && (strlen(desc) > 0))
    {
      tmwtarg_snprintf(buf, sizeof(buf), "    <description>%s</description>\n", desc);
      result = tmwtarg_appendString(result, buf);
    }

    if(tmwsim_getSelectRequired((TMWSIM_POINT *)pPoint))
      result = tmwtarg_appendString(result, "    <selectRequired>true</selectRequired>\n");

    /* value may be up to 100 bytes, convert to ascii string */
    tmwtarg_snprintf(buf, sizeof(buf), "    <value> ");
    offset = 12;
    for(i=0; i<len; i++)
    {
      tmwtarg_snprintf((TMWTYPES_CHAR*)(buf+offset), (sizeof(buf)-offset), "0x%02x ", value[i]);
      offset +=5;
    }
    tmwtarg_snprintf((TMWTYPES_CHAR*)(buf+offset), (sizeof(buf)-offset), "</value>\n");
    result = tmwtarg_appendString(result, buf);

    result = tmwtarg_appendString(result, "   </mctna>\n");
    index += 1;
  }
#endif

  
#if S14DATA_SUPPORT_MEPTA
  index = 0;
  while((pPoint = s14sim_meptaGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    TMWTYPES_USHORT elapsedTime;
    TMWTYPES_UCHAR sep;
    TMWTYPES_CHAR *desc = tmwsim_getDescription((TMWSIM_POINT *)pPoint);
    TMWTYPES_ULONG address = s14sim_getInfoObjAddr(pPoint); 
  
    s14sim_meptaGetValue(pPoint, &sep, &elapsedTime);
    result = tmwtarg_appendString(result, "   <mepta>\n");

    tmwtarg_snprintf(buf, sizeof(buf), "    <address>%d</address>\n", address);
    result = tmwtarg_appendString(result, buf);

    if(desc && (strlen(desc) > 0))
    {
      tmwtarg_snprintf(buf, sizeof(buf), "    <description>%s</description>\n", desc);
      result = tmwtarg_appendString(result, buf);
    }

    tmwtarg_snprintf(buf, sizeof(buf), "    <value>%u</value>\n", sep);
    result = tmwtarg_appendString(result, buf); 

    tmwtarg_snprintf(buf, sizeof(buf), "    <time>%u</time>\n", elapsedTime);
    result = tmwtarg_appendString(result, buf); 
 
    result = tmwtarg_appendString(result, "   </mepta>\n");
    index += 1;
  }
#endif

#if S14DATA_SUPPORT_MEPTB
  index = 0;
  while((pPoint = s14sim_meptbGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    TMWTYPES_USHORT relayDuration;
    TMWTYPES_UCHAR spe;
    TMWTYPES_UCHAR qdp;
    TMWTYPES_CHAR *desc = tmwsim_getDescription((TMWSIM_POINT *)pPoint);
    TMWTYPES_ULONG address = s14sim_getInfoObjAddr(pPoint); 
  
    s14sim_meptbGetValue(pPoint, &spe, &qdp, &relayDuration); 
    result = tmwtarg_appendString(result, "   <meptb>\n");

    tmwtarg_snprintf(buf, sizeof(buf), "    <address>%d</address>\n", address);
    result = tmwtarg_appendString(result, buf);

    if(desc && (strlen(desc) > 0))
    {
      tmwtarg_snprintf(buf, sizeof(buf), "    <description>%s</description>\n", desc);
      result = tmwtarg_appendString(result, buf);
    }

    tmwtarg_snprintf(buf, sizeof(buf), "    <value>%u</value>\n", ((spe <<8) | (qdp)));
    result = tmwtarg_appendString(result, buf); 

    tmwtarg_snprintf(buf, sizeof(buf), "    <time>%u</time>\n", relayDuration);
    result = tmwtarg_appendString(result, buf); 
  
    result = tmwtarg_appendString(result, "   </meptb>\n");
    index += 1;
  }
#endif

#if S14DATA_SUPPORT_MEPTC
  index = 0;
  while((pPoint = s14sim_meptcGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    TMWTYPES_USHORT relayOperating;
    TMWTYPES_UCHAR oci;
    TMWTYPES_UCHAR qdp;
    TMWTYPES_CHAR *desc = tmwsim_getDescription((TMWSIM_POINT *)pPoint);
    TMWTYPES_ULONG address = s14sim_getInfoObjAddr(pPoint); 
  
    s14sim_meptcGetValue(pPoint, &oci, &qdp, &relayOperating);
    result = tmwtarg_appendString(result, "   <meptc>\n");

    tmwtarg_snprintf(buf, sizeof(buf), "    <address>%d</address>\n", address);
    result = tmwtarg_appendString(result, buf);

    if(desc && (strlen(desc) > 0))
    {
      tmwtarg_snprintf(buf, sizeof(buf), "    <description>%s</description>\n", desc);
      result = tmwtarg_appendString(result, buf);
    } 
    
    tmwtarg_snprintf(buf, sizeof(buf), "    <value>%u</value>\n", ((oci <<8) | (qdp)));
    result = tmwtarg_appendString(result, buf); 

    tmwtarg_snprintf(buf, sizeof(buf), "    <time>%u</time>\n", relayOperating);
    result = tmwtarg_appendString(result, buf); 

    result = tmwtarg_appendString(result, "   </meptc>\n");
    index += 1;
  }
#endif

#if S14DATA_SUPPORT_MPS  
  index = 0;
  while((pPoint = s14sim_mpsnaGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    TMWTYPES_ULONG value; 
    TMWTYPES_CHAR *desc = tmwsim_getDescription((TMWSIM_POINT *)pPoint);
    TMWTYPES_ULONG address = s14sim_getInfoObjAddr(pPoint); 
    TMWTYPES_UCHAR flags = s14sim_getFlags(pPoint);
  
    value = s14sim_mpsnaGetValue(pPoint);
    result = tmwtarg_appendString(result, "   <mpsna>\n");

    tmwtarg_snprintf(buf, sizeof(buf), "    <address>%d</address>\n", address);
    result = tmwtarg_appendString(result, buf);

    if(desc && (strlen(desc) > 0))
    {
      tmwtarg_snprintf(buf, sizeof(buf), "    <description>%s</description>\n", desc);
      result = tmwtarg_appendString(result, buf);
    }

    tmwtarg_snprintf(buf, sizeof(buf), "    <value>%u</value>\n", value);
    result = tmwtarg_appendString(result, buf);
 
    result = _putFlags(result, flags);

    result = tmwtarg_appendString(result, "   </mpsna>\n");
    index += 1;
  }
#endif

#if S14DATA_SUPPORT_PMENA  
  index = 0;
  while((pPoint = s14sim_pmenaGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    TMWTYPES_SHORT value; 
    TMWTYPES_UCHAR qualifier;
    TMWTYPES_CHAR *desc = tmwsim_getDescription((TMWSIM_POINT *)pPoint);
    TMWDEFS_GROUP_MASK groupMask = s14sim_getGroupMask(pPoint);
    TMWTYPES_ULONG address = s14sim_getInfoObjAddr(pPoint);  
  
    value = s14sim_pmenaGetValue(pPoint);
    qualifier = s14sim_pmenaGetQualifier(pPoint);

    result = tmwtarg_appendString(result, "   <pmena>\n");

    tmwtarg_snprintf(buf, sizeof(buf), "    <address>%d</address>\n", address);
    result = tmwtarg_appendString(result, buf);

    if(desc && (strlen(desc) > 0))
    {
      tmwtarg_snprintf(buf, sizeof(buf), "    <description>%s</description>\n", desc);
      result = tmwtarg_appendString(result, buf);
    }

    result = _putGroupMask(result, groupMask);

    tmwtarg_snprintf(buf, sizeof(buf), "    <value>%d</value>\n", value);
    result = tmwtarg_appendString(result, buf);

    tmwtarg_snprintf(buf, sizeof(buf), "    <qualifier>%u</qualifier>\n", qualifier);
    result = tmwtarg_appendString(result, buf);
  
    result = tmwtarg_appendString(result, "   </pmena>\n");
    index += 1;
  } 
#endif

#if S14DATA_SUPPORT_PMENB  
  index = 0;
  while((pPoint = s14sim_pmenbGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    TMWTYPES_SHORT value; 
    TMWTYPES_UCHAR qualifier;
    TMWTYPES_CHAR *desc = tmwsim_getDescription((TMWSIM_POINT *)pPoint);
    TMWDEFS_GROUP_MASK groupMask = s14sim_getGroupMask(pPoint);
    TMWTYPES_ULONG address = s14sim_getInfoObjAddr(pPoint);  
  
    value = s14sim_pmenbGetValue(pPoint);
    qualifier = s14sim_pmenbGetQualifier(pPoint);

    result = tmwtarg_appendString(result, "   <pmenb>\n");

    tmwtarg_snprintf(buf, sizeof(buf), "    <address>%d</address>\n", address);
    result = tmwtarg_appendString(result, buf);

    if(desc && (strlen(desc) > 0))
    {
      tmwtarg_snprintf(buf, sizeof(buf), "    <description>%s</description>\n", desc);
      result = tmwtarg_appendString(result, buf);
    }

    result = _putGroupMask(result, groupMask);

    tmwtarg_snprintf(buf, sizeof(buf), "    <value>%d</value>\n", value);
    result = tmwtarg_appendString(result, buf);

    tmwtarg_snprintf(buf, sizeof(buf), "    <qualifier>%u</qualifier>\n", qualifier);
    result = tmwtarg_appendString(result, buf);
  
    result = tmwtarg_appendString(result, "   </pmenb>\n");
    index += 1;
  }  
#endif
#if S14DATA_SUPPORT_PMENC 
  index = 0;
  while((pPoint = s14sim_pmencGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    TMWTYPES_SFLOAT value; 
    TMWTYPES_UCHAR qualifier;
    TMWTYPES_CHAR *desc = tmwsim_getDescription((TMWSIM_POINT *)pPoint);
    TMWDEFS_GROUP_MASK groupMask = s14sim_getGroupMask(pPoint);
    TMWTYPES_ULONG address = s14sim_getInfoObjAddr(pPoint);  
  
    value = s14sim_pmencGetValue(pPoint);
    qualifier = s14sim_pmencGetQualifier(pPoint);

    result = tmwtarg_appendString(result, "   <pmenc>\n");

    tmwtarg_snprintf(buf, sizeof(buf), "    <address>%d</address>\n", address);
    result = tmwtarg_appendString(result, buf);

    if(desc && (strlen(desc) > 0))
    {
      tmwtarg_snprintf(buf, sizeof(buf), "    <description>%s</description>\n", desc);
      result = tmwtarg_appendString(result, buf);
    }

    result = _putGroupMask(result, groupMask);

    tmwtarg_snprintf(buf, sizeof(buf), "    <value>%f</value>\n", value);
    result = tmwtarg_appendString(result, buf);

    tmwtarg_snprintf(buf, sizeof(buf), "    <qualifier>%u</qualifier>\n", qualifier);
    result = tmwtarg_appendString(result, buf);
  
    result = tmwtarg_appendString(result, "   </pmenc>\n");
    index += 1;
  }     
#endif
#if S14DATA_SUPPORT_PACNA  
  index = 0;
  while((pPoint = s14sim_pacnaGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    TMWTYPES_CHAR *desc = tmwsim_getDescription((TMWSIM_POINT *)pPoint);
    TMWTYPES_ULONG address = s14sim_getInfoObjAddr(pPoint);  
   
    result = tmwtarg_appendString(result, "   <pacna>\n");

    tmwtarg_snprintf(buf, sizeof(buf), "    <address>%d</address>\n", address);
    result = tmwtarg_appendString(result, buf);

    if(desc && (strlen(desc) > 0))
    {
      tmwtarg_snprintf(buf, sizeof(buf), "    <description>%s</description>\n", desc);
      result = tmwtarg_appendString(result, buf);
    }

    result = tmwtarg_appendString(result, "   </pacna>\n");
    index += 1;
  }     
#endif

  result = tmwtarg_appendString(result, " </device>\n");
  result = tmwtarg_appendString(result, "</tmw:i14data>\n");
  return(result);
}

#if TMWCNFG_SUPPORT_DIAG
/* routine: s14sim_showData */
void TMWDEFS_GLOBAL s14sim_showData(TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  TMWTYPES_USHORT index;
  TMWDIAG_ANLZ_ID id;
  void *pPoint;

  if(tmwdiag_initId(&id, TMWDEFS_NULL, TMWDEFS_NULL, pSector, TMWDIAG_ID_MMI) == TMWDEFS_FALSE)
  {
    return;
  }

#if S14DATA_SUPPORT_MSP
  index = 0;
  tmwdiag_skipLine(&id);
  tmwdiag_putLine(&id, "Monitored Single Points:\n");
  while((pPoint = s14sim_mspGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    i14diag_showMSP(pSector, 0, s14sim_getInfoObjAddr(pPoint), 
      s14sim_mspGetFlagsAndValue(pPoint), TMWDEFS_NULL);

    index += 1;
  }
#endif

#if S14DATA_SUPPORT_MDP
  index = 0;
  tmwdiag_skipLine(&id);
  tmwdiag_putLine(&id, "Monitored Double Points:\n");
  while((pPoint = s14sim_mdpGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    i14diag_showMDP(pSector, 0, s14sim_getInfoObjAddr(pPoint), 
      s14sim_mdpGetFlagsAndValue(pPoint), TMWDEFS_NULL);

    index += 1;
  }
#endif

#if S14DATA_SUPPORT_MST
  index = 0;
  tmwdiag_skipLine(&id);
  tmwdiag_putLine(&id, "Monitored Step Position Information:\n");
  while((pPoint = s14sim_mstGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    i14diag_showMST(pSector, 0, s14sim_getInfoObjAddr(pPoint), 
      s14sim_mstGetValue(pPoint), s14sim_getFlags(pPoint), TMWDEFS_NULL);

    index += 1;
  }
#endif

#if S14DATA_SUPPORT_MBO
  index = 0;
  tmwdiag_skipLine(&id);
  tmwdiag_putLine(&id, "Monitored Bitstring of 32 Bits:\n");
  while((pPoint = s14sim_mboGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    i14diag_showMBO(pSector, 0, s14sim_getInfoObjAddr(pPoint), 
      s14sim_mboGetValue(pPoint), s14sim_getFlags(pPoint), TMWDEFS_NULL);

    index += 1;
  }
#endif

#if S14DATA_SUPPORT_MME_A
  index = 0;
  tmwdiag_skipLine(&id);
  tmwdiag_putLine(&id, "Normalized Measurands:\n");
  while((pPoint = s14sim_mmenaGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    i14diag_showMMENA(pSector, 0, s14sim_getInfoObjAddr(pPoint), 
      s14sim_mmenaGetValue(pPoint), s14sim_getFlags(pPoint), TMWDEFS_NULL);

    index += 1;
  }
#endif

#if S14DATA_SUPPORT_MME_B
  index = 0;
  tmwdiag_skipLine(&id);
  tmwdiag_putLine(&id, "Scaled Measurands:\n");
  while((pPoint = s14sim_mmenbGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    i14diag_showMMENB(pSector, 0, s14sim_getInfoObjAddr(pPoint), 
      s14sim_mmenbGetValue(pPoint), s14sim_getFlags(pPoint), TMWDEFS_NULL);

    index += 1;
  }
#endif

#if S14DATA_SUPPORT_MME_C
  index = 0;
  tmwdiag_skipLine(&id);
  tmwdiag_putLine(&id, "Floating Point Measurands:\n");
  while((pPoint = s14sim_mmencGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    i14diag_showMMENC(pSector, 0, s14sim_getInfoObjAddr(pPoint), 
      s14sim_mmencGetValue(pPoint), s14sim_getFlags(pPoint), TMWDEFS_NULL);

    index += 1;
  }
#endif

#if S14DATA_SUPPORT_MIT
  index = 0;
  tmwdiag_skipLine(&id);
  tmwdiag_putLine(&id, "Integrated Totals:\n");
  while((pPoint = s14sim_mitGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    i14diag_showMIT(pSector, 0, s14sim_getInfoObjAddr(pPoint), 
      s14sim_mitGetValue(pPoint), s14sim_getFlags(pPoint), TMWDEFS_NULL);

    index += 1;
  }
#endif

#if TMWCNFG_USE_SIMULATED_DB
#if S14DATA_SUPPORT_MEPTA
  index = 0;
  tmwdiag_skipLine(&id);
  tmwdiag_putLine(&id, "Event of Protection Equipment:\n");
  while((pPoint = s14sim_meptaGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    TMWTYPES_USHORT elapsedTime;
    TMWTYPES_UCHAR sep;
    s14sim_meptaGetValue(pPoint, &sep, &elapsedTime);
    i14diag_showMEPTA(pSector, 0, s14sim_getInfoObjAddr(pPoint), sep, elapsedTime, TMWDEFS_NULL);
    index += 1;
  }
#endif

#if S14DATA_SUPPORT_MEPTB
  index = 0;
  tmwdiag_skipLine(&id);
  tmwdiag_putLine(&id, "Packed Start Events of Protection Equipment:\n");
  while((pPoint = s14sim_meptbGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    TMWTYPES_USHORT relayDuration;
    TMWTYPES_UCHAR spe;
    TMWTYPES_UCHAR qdp;
    s14sim_meptbGetValue(pPoint, &spe, &qdp, &relayDuration);
    i14diag_showMEPTB(pSector, 0, s14sim_getInfoObjAddr(pPoint), spe, qdp, relayDuration, TMWDEFS_NULL);
    index += 1;
  }
#endif

#if S14DATA_SUPPORT_MEPTC
  index = 0;
  tmwdiag_skipLine(&id);
  tmwdiag_putLine(&id, "Packed Output Circuit Information of Protection Equipment:\n");
  while((pPoint = s14sim_meptcGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    TMWTYPES_USHORT relayOperating;
    TMWTYPES_UCHAR oci;
    TMWTYPES_UCHAR qdp;
    s14sim_meptcGetValue(pPoint, &oci, &qdp, &relayOperating);
    i14diag_showMEPTC(pSector, 0, s14sim_getInfoObjAddr(pPoint), oci, qdp, relayOperating, TMWDEFS_NULL);
    index += 1;
  }
#endif

#if S14DATA_SUPPORT_MPS 
  index = 0;
  tmwdiag_skipLine(&id);
  tmwdiag_putLine(&id, "Packed Single Point Information With Status Change Detection:\n");
  while((pPoint = s14sim_mpsnaGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    i14diag_showMPSNA(pSector, 0, s14sim_getInfoObjAddr(pPoint), s14sim_mpsnaGetValue(pPoint), s14sim_getFlags(pPoint));
    index += 1;
  }
#endif

#if S14DATA_SUPPORT_MMEND
  index = 0;
  tmwdiag_skipLine(&id);
  tmwdiag_putLine(&id, "Measured Value, Normalized Value Without Quality Descriptor:\n");
  while((pPoint = s14sim_mmendGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    i14diag_showMMEND(pSector, 0, s14sim_getInfoObjAddr(pPoint), s14sim_mmendGetValue(pPoint));
    index += 1;
  }
#endif
#if S14DATA_SUPPORT_PMENA 
  index = 0;
  tmwdiag_skipLine(&id);
  tmwdiag_putLine(&id, "Parameter of Measured Values, normalized value:\n");
  while((pPoint = s14sim_pmenaGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    i14diag_showPMENA(pSector, s14sim_getInfoObjAddr(pPoint), s14sim_pmenaGetValue(pPoint), s14sim_pmenaGetQualifier(pPoint));
    index += 1;
  }
#endif
#if S14DATA_SUPPORT_PMENB
  index = 0;
  tmwdiag_skipLine(&id);
  tmwdiag_putLine(&id, "Parameter of Measured Values, scaled value:\n");
  while((pPoint = s14sim_pmenbGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    i14diag_showPMENB(pSector, s14sim_getInfoObjAddr(pPoint), s14sim_pmenbGetValue(pPoint), s14sim_pmenbGetQualifier(pPoint));
    index += 1;
  }
#endif
#if S14DATA_SUPPORT_PMENC
  index = 0;
  tmwdiag_skipLine(&id);
  tmwdiag_putLine(&id, "Parameter of Measured Values, short floating point number:\n");
  while((pPoint = s14sim_pmencGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    i14diag_showPMENC(pSector, s14sim_getInfoObjAddr(pPoint), s14sim_pmencGetValue(pPoint), s14sim_pmencGetQualifier(pPoint));
    index += 1;
  }
#endif
#if S14DATA_SUPPORT_PACNA 
  index = 0;
  tmwdiag_skipLine(&id);
  tmwdiag_putLine(&id, "Parameter Activation:\n");
  while((pPoint = s14sim_pacnaGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    i14diag_showPACNA(pSector, s14sim_getInfoObjAddr(pPoint));
    index += 1;
  }
#endif
#endif

#if S14DATA_SUPPORT_MITC
  index = 0;
  tmwdiag_skipLine(&id);
  tmwdiag_putLine(&id, "Integrated Totals BCD:\n");
  while((pPoint = s14sim_mitcGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    TMWTYPES_UCHAR bcd[6];
    s14sim_mitcGetValue(pPoint, bcd);
    i14diag_showMITC(pSector, 0, s14sim_getInfoObjAddr(pPoint), 
      bcd, s14sim_getFlags(pPoint), TMWDEFS_NULL);

    index += 1;
  }
#endif

#if M14DATA_SUPPORT_MCTNA
  index = 0;
  tmwdiag_skipLine(&id);
  tmwdiag_putLine(&id, "Configuration Table Information:\n");
  while((pPoint = s14sim_mctnaGetPoint(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    TMWTYPES_UCHAR quantity;
    TMWTYPES_UCHAR ctiValues[100];
    quantity = s14sim_mctnaGetValues(pPoint, 100, ctiValues);
    i14diag_showMCT(pSector, tmwsim_getPointNumber((TMWSIM_POINT *)pPoint), quantity, ctiValues);
    index += 1;
  }
#endif

#if S14DATA_SUPPORT_FILE
  /* Display File transfer database */
  index = 0;
  tmwdiag_skipLine(&id);
  tmwdiag_putLine(&id, "File Transfer - File Summary:\n");  
  tmwdiag_putLine(&id, "                      IOA     File    File   Number of    SOF\n");
  tmwdiag_putLine(&id, "                              Name   Length   Sections \n");
  while((pPoint = s14sim_fileGetFile(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    s14sim_fileDiagShow(pSector, pPoint);
    index += 1;
  }
#endif
}
#endif

#endif /* TMWCNFG_SUPPORT_SIMULATED_DB */
