/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/
/* file: s104mem.c
 * description:  Implementation of Slave 104 specific utility functions.
 */

#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/i870dia4.h"
#include "tmwscl/i870/s104mem.h"
#include "tmwscl/i870/s104sesn.h"
#include "tmwscl/i870/s104sctr.h"
#include "tmwscl/i870/s14diag.h"

typedef struct{
  TMWMEM_HEADER     header;
  S104SESN          data;
} S104MEM_SESN;

typedef struct{
  TMWMEM_HEADER     header;
  S104SCTR           data;
} S104MEM_SCTR;

static const TMWTYPES_CHAR *_nameTable[S104MEM_ALLOC_TYPE_MAX] = {
  "S104SESN",
  "S104CTR"
};

#if !TMWCNFG_USE_DYNAMIC_MEMORY
/* Use static allocated array of memory instead of dynamic memory */
static S104MEM_SESN          s104mem_sesns[S104CNFG_NUMALLOC_SESNS];
static S104MEM_SCTR          s104mem_sctrs[S104CNFG_NUMALLOC_SCTRS];
#endif

static TMWMEM_POOL_STRUCT _s104allocTable[S104MEM_ALLOC_TYPE_MAX] ;

#if TMWCNFG_USE_DYNAMIC_MEMORY
static void TMWDEFS_LOCAL _initConfig(
  S104MEM_CONFIG *pConfig)
{
  pConfig->numSessions = S104CNFG_NUMALLOC_SESNS;
  pConfig->numSectors  = S104CNFG_NUMALLOC_SCTRS;
}

void TMWDEFS_GLOBAL s104mem_initConfig(
  S104MEM_CONFIG  *pS104Config,
  S14MEM_CONFIG   *pS14MemConfig,
  I870MEM4_CONFIG *pI870Mem4Config,
  I870MEM_CONFIG  *pI870MemConfig,
  TMWMEM_CONFIG   *pTmwConfig)
{
  tmwmem_initConfig(pTmwConfig);
  i870mem_initConfig(pI870MemConfig);
  i870mem4_initConfig(pI870Mem4Config);
  s14mem_initConfig(pS14MemConfig);
  _initConfig(pS104Config);
}

/* function: s104mem_initMemory */
TMWTYPES_BOOL TMWDEFS_GLOBAL s104mem_initMemory(
  S104MEM_CONFIG  *pS104Config,
  S14MEM_CONFIG   *pS14MemConfig,
  I870MEM4_CONFIG *pI870Mem4Config,
  I870MEM_CONFIG  *pI870MemConfig,
  TMWMEM_CONFIG   *pTmwConfig)
{
   /* Initialize memory management and diagnostics for
    * 104 if not yet done 
    */
  if(!tmwmem_init(pTmwConfig))
    return TMWDEFS_FALSE;

  if(!tmwappl_getInitialized(TMWAPPL_INIT_S104))
  {
    if(!s104mem_init(pS104Config))
      return TMWDEFS_FALSE;

    tmwappl_setInitialized(TMWAPPL_INIT_S104);

    if(!tmwappl_getInitialized(TMWAPPL_INIT_S101))
    {
      if(!s14mem_init(pS14MemConfig))
        return TMWDEFS_FALSE;

#if TMWCNFG_SUPPORT_DIAG
      s14diag_init();
#endif
    }

    if(!tmwappl_getInitialized(TMWAPPL_INIT_I870))
    {
      if(!i870mem_init(pI870MemConfig))
        return TMWDEFS_FALSE;

#if TMWCNFG_SUPPORT_DIAG
      i870diag_init();
#endif
      tmwappl_setInitialized(TMWAPPL_INIT_I870);
    }

    if(!tmwappl_getInitialized(TMWAPPL_INIT_104))
    {
      if(!i870mem4_init(pI870Mem4Config))
        return TMWDEFS_FALSE;

#if TMWCNFG_SUPPORT_DIAG
      i870dia4_init();
#endif
      tmwappl_setInitialized(TMWAPPL_INIT_104);
    }
  } 
  return TMWDEFS_TRUE;
}
#endif

/* routine: s104mem_init */
TMWTYPES_BOOL TMWDEFS_GLOBAL s104mem_init(
  S104MEM_CONFIG  *pConfig)
{
#if TMWCNFG_USE_DYNAMIC_MEMORY
  /* dynamic memory allocation supported */
  S104MEM_CONFIG  config; 

  /* If caller has not specified memory pool configuration, use the
   * default compile time values 
   */
  if(pConfig == TMWDEFS_NULL)
  {
    pConfig = &config;
    _initConfig(pConfig);
  }

  if(!tmwmem_lowInit(_s104allocTable, S104MEM_SESN_TYPE, pConfig->numSessions, sizeof(S104MEM_SESN), TMWDEFS_NULL))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_s104allocTable, S104MEM_SCTR_TYPE, pConfig->numSectors,  sizeof(S104MEM_SCTR), TMWDEFS_NULL))
    return TMWDEFS_FALSE;
#else
  /* static memory allocation supported */
  TMWTARG_UNUSED_PARAM(pConfig);

  if(!tmwmem_lowInit(_s104allocTable, S104MEM_SESN_TYPE, S104CNFG_NUMALLOC_SESNS, sizeof(S104MEM_SESN), (TMWTYPES_UCHAR*)s104mem_sesns))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_s104allocTable, S104MEM_SCTR_TYPE, S104CNFG_NUMALLOC_SCTRS, sizeof(S104MEM_SCTR), (TMWTYPES_UCHAR*)s104mem_sctrs))
    return TMWDEFS_FALSE;
#endif
  return TMWDEFS_TRUE;
}

/* function: s104mem_alloc */
void * TMWDEFS_GLOBAL s104mem_alloc(
  S104MEM_ALLOC_TYPE type)
{
  if(type >= S104MEM_ALLOC_TYPE_MAX)
  {
    return(TMWDEFS_NULL);
  }

  return(tmwmem_lowAlloc(&_s104allocTable[type]));
}

/* function: s104mem_free */
void TMWDEFS_GLOBAL s104mem_free(
  void *pBuf)
{    
  TMWMEM_HEADER *pHeader = TMWMEM_GETHEADER(pBuf);
  TMWTYPES_UCHAR   type = TMWMEM_GETTYPE(pHeader);

  if(type >= S104MEM_ALLOC_TYPE_MAX)
  {
    return;
  }

  tmwmem_lowFree(&_s104allocTable[type], pHeader);
}

/* function: s104mem_getUsage */
TMWTYPES_BOOL TMWDEFS_GLOBAL s104mem_getUsage(
  TMWTYPES_UCHAR index,
  const TMWTYPES_CHAR **pName,
  TMWMEM_POOL_STRUCT *pStruct)
{
  if(index >= S104MEM_ALLOC_TYPE_MAX)
  {
    return(TMWDEFS_FALSE);
  }

  *pName = _nameTable[index];
  *pStruct = _s104allocTable[index];
  return(TMWDEFS_TRUE);
}
