/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Dummy DNP3 Slave protocol manager factory implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   08/09/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "DummyDNP3SlaveProtocolManagerFactoy.h"

extern "C"
{
#include "tmwscl/utils/tmwpltmr.h"
#include "tmwscl/utils/tmwtarg.h"

#include "tmwscl/dnp/dnpchnl.h"
#include "tmwscl/dnp/sdnpsesn.h"

#include "LinIoTarg/liniotarg.h"
}

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

typedef struct DNPLinkConfigDef
{
    /* Receive and transmit link layer frame size
     * This includes room for link header and CRCs
     * maximum value 292
     */
    TMWTYPES_USHORT        rxFrameSize;
    TMWTYPES_USHORT        txFrameSize;

    /* Maximum amount of time to wait for a complete frame after
     * receiving valid frame sync characters
     */
    TMWTYPES_MILLISECONDS rxFrameTimeout;

    /* When should we ask for link layer confirmations. The
     * options are:
     *  TMWDEFS_LINKCNFM_NEVER     (not for any frame)
     *  TMWDEFS_LINKCNFM_SOMETIMES (multi-frame fragments)
     *  TMWDEFS_LINKCNFM_ALWAYS    (for all frames)
     */
    TMWDEFS_LINKCNFM confirmMode;

    /* Maximum amount of time to wait for a link level confirm
     * if requested. Even if confirmMode is set to NEVER this will
     * still be used for Link Test Frame and Request Link Status
     * if they are sent.
     */
    TMWTYPES_MILLISECONDS confirmTimeout;

    /* Maximum number of link layer retries if link layer confirm
     * times out.
     */
    TMWTYPES_UCHAR maxRetries;

    /* Specify networking support as described in DNP3 Specification -
     * IP Networking. See enums above
     */
    DNPLINK_NETWORK_TYPE networkType;

    /* Maximum receive and transmit application fragment sizes
     *  The DNP3 specification recommends a value of 2048.
     */
    TMWTYPES_USHORT        rxFragmentSize;
    TMWTYPES_USHORT        txFragmentSize;

}DNPLinkConfigStr;

typedef struct IOConfigDef
{
    /* User specified channel name */
    TMWTYPES_CHAR chnlName[LINIOCNFG_MAX_NAME_LEN];

    /* On client -
     *      this is the IP address to set up TCP connection to
     * On server and Dual End Point Device -
     *      this is the IP address to accept TCP connection from
     *      May be *.*.*.* indicating accept connection from any client
     */
    TMWTYPES_CHAR   ipAddress[20];

    /* On client -
     *      this is the port to connect to
     * On server and Dual End Point Device -
     *      this is the port to listen on
     */
    TMWTYPES_USHORT ipPort;

    /* Number of milliseconds to wait for TCP connect to succeed or fail */
    TMWTYPES_ULONG  ipConnectTimeout;

    /* Indicate CLIENT, SERVER, DUAL END POINT, or UDP only
     *  (DUAL END POINT provides both CLIENT and SERVER functionality but
     *  with only one connection at a time)
     */
    LINTCP_MODE     mode;

    /* NOTE: The following configuration parameters are required to support
     *   DNP3 Specification IP Networking. These are not required for
     *   the IEC or Modbus protocols.

     *  Indicate master or outstation (slave) role in dnp networking
     *  as specified by DNP3 Specification IP Networking
     */
    LINTCP_ROLE     role;

    /* Local port for sending and receiving UDP datagrams on.
     * If this is set to TMWTARG_UDP_PORT_NONE, UDP will not be enabled.
     * For DNP networking UDP should be supported.
     * It is not needed for any of the current IEC or modbus protocols.
     * On Master - If this is set to TMWTARG_UDP_PORT_ANY, an unspecified available
     *             port will be used.
     * On Slave  - This should be chosen to match the UDP port that the master uses
     *             to send Datagram messages to.
     *             This must not be TMWTARGP_UDP_PORT_ANY or TMWTARG_UDP_PORT_SRC.
     */
    TMWTYPES_USHORT localUDPPort;

    /* On Master - if TCP and UDP is configured this specifies the destination UDP/IP
     *              port to send broadcast requests in UDP datagrams to.
     *             if UDP ONLY is configured this specifies the destination UDP/IP
     *              port to send all requests in UDP datagrams to.
     *             This must match the "localUDPPort" on the slave.
     * On Slave  - if TCP and UDP this is not used.
     *             if UDP ONLY is configured this specifies the destination UDP/IP
     *              port to send responses to.
     *              Can be TMWTARG_UDP_PORT_SRC indicating use the src port from a
     *              UDP request received from master.
     */
    TMWTYPES_USHORT destUDPPort;

    /* On master - Not used.
     * On Slave  - if TCP and UDP not used.
     *             if UDP ONLY is configured this specifies the destination UDP/IP
     *              port to send the initial Unsolicited Null response to.
     *              After receiving a UDP request from master, destUDPPort (which)
     *              may indicate use src port) will be used for all responses.
     *              This must not be TMWTARG_UDP_PORT_NONE, TMWTARG_UDP_PORT_ANY, or
     *              TMWTARG_UDP_PORT_SRC for a slave that supports UDP.
     */
    TMWTYPES_USHORT initUnsolUDPPort;
}IOConfigStr;

typedef struct SDNPSessionConfigDef
{
    /* Source address (outstation address)for this session */
    TMWTYPES_USHORT source;

    /* Destination address (master address) for this session
     * If validateSourceAddress is TMWDEFS_TRUE, this will be the
     * address to compare the masters source address to.
     * If validateSourceAddress is TMWDEFS_FALSE, this address
     * will be used as the address to send unsolicited responses.
     * All other responses will be sent to the source address received
     * from the master
     */
    TMWTYPES_USHORT destination;

    /* How often to send link status requests
     * if no DNP3 frames have been received on this session.
     * In DNP3 IP Networking spec this is called keep-alive interval
     * A value of zero will turn off keep alives.
     */
    TMWTYPES_MILLISECONDS linkStatusPeriod;

    /* Specify whether application is allowed to send multi fragment responses.
     */
    TMWTYPES_BOOL multiFragRespAllowed;

    /* Specify whether application layer confirmations will be requested
     * for non final fragments of a multi fragment response. Application layer
     * confirmations are always requested for responses that contain events.
     */
    TMWTYPES_BOOL multiFragConfirm;

    /* Specifies whether this device will set the Need Time IIN bit
     * in response to this session at startup and after the clock valid
     * period has elapsed. If this bit is set the master will respond
     * with a time synchronization request. Typically this parameter
     * should be true for one session for each slave device. Set this
     * parameter to TMWDEFS_FALSE if report by exception is not supported
     * or there is no reason this device needs to be synchronized from
     * the master.
     */
    TMWTYPES_BOOL respondNeedTime;

    /* Specifies how long the local clock will remain valid after receiving
     * a time synchronization. (or after sdnpsesn_restartClockValidTime is called)
     */
    TMWTYPES_MILLISECONDS clockValidPeriod;

    /* Application confirm timeout specifies how long the slave DNP device will
     * wait for an application layer confirmation from the master. This in combination
     * with unsolRetryDelay or unsolOfflineRetryDelay will determine how frequently
     * an unsolicited response will be resent.
     */
    TMWTYPES_MILLISECONDS applConfirmTimeout;

    /* selectTimeout specifies the maximum amount of time that a select
     * will remain valid before the corresponding operate is received. If
     * an operate request is received after this period has elapsed since
     * the previous select the select will not be valid and the operate
     * request will fail.
     */
    TMWTYPES_MILLISECONDS selectTimeout;

    /* Determines whether unsolicited responses are allowed. If unsolAllowed
     * is set to TMWDEFS_FALSE no unsolicited responses will be generated and
     * requests to enable or disable unsolicited responses will fail.
     */
    TMWTYPES_BOOL unsolAllowed;

    /* Specify the initial/new state of the unsolicited event mask. This mask
     * is used to determine which event class(es) will generate unsolicited
     * responses. According to the DNP specification, unsolicited responses
     * should be disabled until an 'Enable Unsolicited Response' request is
     * received from the master. Hence this value should generally be
     * TMWDEFS_CLASS_MASK_NONE, but some masters do not generate the 'Enable
     * Unsolicited Response' message, in which case they must be enabled here.
     */
    TMWDEFS_CLASS_MASK unsolClassMask;

    /* If unsolicited responses are enabled, unsolClassXMaxEvents specifies
     * the maximum number of events in the corresponding class to be allowed
     * before an unsolicited response will be generated.
     */
    TMWTYPES_UCHAR unsolClass1MaxEvents;
    TMWTYPES_UCHAR unsolClass2MaxEvents;
    TMWTYPES_UCHAR unsolClass3MaxEvents;

    /* If unsolicited responses are enabled, unsolClassXMaxDelay specifies
     * the maximum amount of time in milliseconds after an event in the
     * corresponding class is received before an unsolicited response will
     * be generated.
     */
    TMWTYPES_MILLISECONDS unsolClass1MaxDelay;
    TMWTYPES_MILLISECONDS unsolClass2MaxDelay;
    TMWTYPES_MILLISECONDS unsolClass3MaxDelay;

    /* Specify the maximum number of unsolicited retries before changing to
     * the 'offline' retry period described below. This parameter allows you
     * to specify up to 65535 retries. If you want an infinite number of
     * retries set unsolOfflineRetryDelay to the same value as unsolRetryDelay
     * in which case this parameter becomes a don't care.
     */
    TMWTYPES_USHORT unsolMaxRetries;

    /* Specifies the time, in milliseconds, to delay after an unsolicited confirm
     * timeout before retrying the unsolicited response.
     */
    TMWTYPES_MILLISECONDS unsolRetryDelay;

    /* Specifies the time, in milliseconds, to delay after an unsolicited
     * timeout before retrying the unsolicited response after unsolMaxRetries
     * have been attempted. To disable retries after unsolMaxRetries set this
     * value to the maximum value for a TMW timer which is TMWDEFS_DAYS(31).
     * This will limit retries to one every 31 days which is effectively
     * disabled.
     */
    TMWTYPES_MILLISECONDS unsolOfflineRetryDelay;

    /* Default variations
     * Specifies the variation that will be used for unsolicited responses
     * and in response to a read requesting variation 0.
     *
     * If any of these configuration variables is set to zero the
     * variation will be defined individually point by point
     */
    TMWTYPES_UCHAR obj01DefaultVariation; /* Binary Input */
    TMWTYPES_UCHAR obj02DefaultVariation; /* Binary Input Event */
    TMWTYPES_UCHAR obj03DefaultVariation; /* Double Binary Input */
    TMWTYPES_UCHAR obj04DefaultVariation; /* Double Binary Input Event */
    TMWTYPES_UCHAR obj30DefaultVariation; /* Analogue Input */
    TMWTYPES_UCHAR obj32DefaultVariation; /* Analogue Input Event  */

    TMWTYPES_USHORT binaryInputMaxEvents;
    TMWDEFS_EVENT_MODE binaryInputEventMode;

    TMWTYPES_USHORT doubleInputMaxEvents;
    TMWDEFS_EVENT_MODE doubleInputEventMode;

    TMWTYPES_USHORT analogInputMaxEvents;
    TMWDEFS_EVENT_MODE analogInputEventMode;
}SDNPSessionConfigStr;



typedef struct BIConfigDef
{
    PointIdStr GDBId;
    lu_uint16_t protocolID;
    TMWTYPES_USHORT defaultVariation; /* object 1 */
    TMWTYPES_USHORT eventDefaultVariation; /* object 2 */
    TMWDEFS_CLASS_MASK eventClass;
    TMWTYPES_BOOL enableClass0;
}BIConfigStr;

typedef struct DBIConfigDef
{
    PointIdStr GDBId;
    lu_uint16_t protocolID;
    TMWTYPES_USHORT defaultVariation; /* object 3 */
    TMWTYPES_USHORT eventDefaultVariation;/* object 4 */
    TMWDEFS_CLASS_MASK eventClass;
    TMWTYPES_BOOL enableClass0;
}DBIConfigStr;

typedef struct AIConfigDef
{
    PointIdStr GDBId;
    lu_uint16_t protocolID;
    TMWTYPES_USHORT defaultVariation;/* object 30 */
    TMWTYPES_USHORT eventDefaultVariation;/* object 32 */
    TMWDEFS_CLASS_MASK eventClass;
    TMWTYPES_BOOL enableClass0;
}AIConfigStr;

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static DNPLinkConfigStr DNPLinkConfig =
{
    292,                        /*rxFrameSize */
    292,                        /* txFrameSize */
    TMWDEFS_SECONDS(5),         /* rxFrameTimeout */
    TMWDEFS_LINKCNFM_SOMETIMES, /* confirmMode */
    TMWDEFS_SECONDS(3),         /* confirmTimeout */
    3,                          /* maxRetries */
    DNPLINK_NETWORK_TCP_ONLY,   /* networkType */
    2048,                       /* rxFragmentSize */
    2048                        /* txFragmentSize */
};

static IOConfigStr IOConfigUser =
{
    "sDNP"                , /* chnlName */
    "*.*.*.*"             , /* ipAddress */
    20000                 , /* ipPort */
    TMWDEFS_SECONDS(5)    , /* ipConnectTimeout */
    LINTCP_MODE_SERVER    , /* mode */
    LINTCP_ROLE_OUTSTATION, /* role */
    20000                 , /* localUDPPort */
    20000                 , /* destUDPPort */
    20000                   /* initUnsolUDPPort */
};

static SDNPSessionConfigStr SDNPSessionConfig =
{
    4, /* source */
    3, /* destination */
    TMWDEFS_SECONDS(5), /* linkStatusPeriod */
    TMWDEFS_TRUE, /* multiFragRespAllowed */
    TMWDEFS_FALSE, /* multiFragConfirm */
    TMWDEFS_TRUE, /* respondNeedTime */
    TMWDEFS_MINUTES(15),/* clockValidPeriod */
    TMWDEFS_SECONDS(10),/* applConfirmTimeout */
    TMWDEFS_SECONDS(5),/* selectTimeout */
    TMWDEFS_TRUE, /* unsolAllowed */
    TMWDEFS_CLASS_MASK_NONE, /* unsolClassMask */
    10,/* unsolClass1MaxEvents */
    10,/* unsolClass2MaxEvents */
    10,/* unsolClass3MaxEvents */
    500,/* unsolClass1MaxDelay */
    500,/* unsolClass2MaxDelay */
    500,/* unsolClass3MaxDelay */
    3, /* unsolMaxRetries */
    TMWDEFS_SECONDS(1),/* unsolRetryDelay */
    TMWDEFS_SECONDS(60),/* unsolOfflineRetryDelay */
    2,/* obj01DefaultVariation */
    2,/* obj02DefaultVariation */
    2,/* obj03DefaultVariation */
    2,/* obj04DefaultVariation */
    0,/* obj30DefaultVariation */
    0,/* obj32DefaultVariation */
    200,/* binaryInputMaxEvents */
    TMWDEFS_EVENT_MODE_SOE, /* binaryInputEventMode */
    200,/* doubleInputMaxEvents */
    TMWDEFS_EVENT_MODE_SOE, /* doubleInputEventMode */
    200, /* analogInputMaxEvents */
    TMWDEFS_EVENT_MODE_MOST_RECENT /* analogInputEventMode */
};

static BIConfigStr BIConfig[] =
{
        /* G3 group  G3 ID  protocolID  defaultVariation eventDefaultVariation maxEvents eventClass               enableClass0 */
        { {0       , 0   }, 0         , 2              , 3                   , TMWDEFS_CLASS_MASK_ONE  , TMWDEFS_TRUE },
        { {0       , 1   }, 1         , 2              , 3                   , TMWDEFS_CLASS_MASK_ONE  , TMWDEFS_TRUE },
        { {0       , 2   }, 2         , 2              , 3                   , TMWDEFS_CLASS_MASK_ONE  , TMWDEFS_TRUE },
        { {0       , 3   }, 3         , 2              , 3                   , TMWDEFS_CLASS_MASK_ONE  , TMWDEFS_TRUE },
        { {0       , 4   }, 4         , 2              , 3                   , TMWDEFS_CLASS_MASK_ONE  , TMWDEFS_TRUE },
        { {0       , 5   }, 5         , 2              , 3                   , TMWDEFS_CLASS_MASK_ONE  , TMWDEFS_TRUE },
        { {0       , 6   }, 6         , 2              , 3                   , TMWDEFS_CLASS_MASK_ONE  , TMWDEFS_TRUE },
        { {0       , 7   }, 7         , 2              , 3                   , TMWDEFS_CLASS_MASK_ONE  , TMWDEFS_TRUE },
        { {0       , 8   }, 8         , 2              , 3                   , TMWDEFS_CLASS_MASK_ONE  , TMWDEFS_TRUE },
        { {0       , 9   }, 9         , 2              , 3                   , TMWDEFS_CLASS_MASK_ONE  , TMWDEFS_TRUE },
        { {0       , 10  }, 10        , 2              , 3                   , TMWDEFS_CLASS_MASK_ONE  , TMWDEFS_TRUE },
        { {0       , 11  }, 11        , 2              , 3                   , TMWDEFS_CLASS_MASK_ONE  , TMWDEFS_TRUE },
        { {0       , 12  }, 12        , 2              , 3                   , TMWDEFS_CLASS_MASK_ONE  , TMWDEFS_TRUE },
        { {0       , 13  }, 13        , 2              , 3                   , TMWDEFS_CLASS_MASK_ONE  , TMWDEFS_TRUE },
        { {0       , 14  }, 14        , 2              , 3                   , TMWDEFS_CLASS_MASK_ONE  , TMWDEFS_TRUE },
        { {0       , 15  }, 15        , 2              , 3                   , TMWDEFS_CLASS_MASK_ONE  , TMWDEFS_TRUE },
        { {0       , 16  }, 16        , 2              , 3                   , TMWDEFS_CLASS_MASK_ONE  , TMWDEFS_TRUE },
        { {0       , 17  }, 17        , 2              , 3                   , TMWDEFS_CLASS_MASK_ONE  , TMWDEFS_TRUE },
        { {0       , 18  }, 18        , 2              , 3                   , TMWDEFS_CLASS_MASK_ONE  , TMWDEFS_TRUE },
        { {0       , 19  }, 19        , 2              , 3                   , TMWDEFS_CLASS_MASK_ONE  , TMWDEFS_TRUE },
        { {0       , 20  }, 20        , 2              , 3                   , TMWDEFS_CLASS_MASK_ONE  , TMWDEFS_TRUE },
        { {0       , 21  }, 21        , 2              , 3                   , TMWDEFS_CLASS_MASK_ONE  , TMWDEFS_TRUE },
        { {0       , 22  }, 22        , 2              , 3                   , TMWDEFS_CLASS_MASK_ONE  , TMWDEFS_TRUE },
        { {0       , 23  }, 23        , 2              , 3                   , TMWDEFS_CLASS_MASK_ONE  , TMWDEFS_TRUE },
        { {0       , 24  }, 24        , 2              , 3                   , TMWDEFS_CLASS_MASK_ONE  , TMWDEFS_TRUE },
        { {0       , 25  }, 25        , 2              , 3                   , TMWDEFS_CLASS_MASK_TWO  , TMWDEFS_TRUE },
        { {0       , 26  }, 26        , 2              , 3                   , TMWDEFS_CLASS_MASK_THREE, TMWDEFS_TRUE },

        { {1       , 0   }, 27        , 2              , 3                   , TMWDEFS_CLASS_MASK_THREE, TMWDEFS_TRUE },
        { {1       , 1   }, 28        , 2              , 3                   , TMWDEFS_CLASS_MASK_THREE, TMWDEFS_TRUE },
        { {1       , 2   }, 29        , 2              , 3                   , TMWDEFS_CLASS_MASK_THREE, TMWDEFS_TRUE },

        { {2       , 0   }, 30        , 2              , 3                   , TMWDEFS_CLASS_MASK_THREE, TMWDEFS_TRUE },

        { {4       , 0   }, 31        , 2              , 3                   , TMWDEFS_CLASS_MASK_THREE, TMWDEFS_TRUE }, //Invalid point

        { {3       , 0   }, 32        , 2              , 3                   , TMWDEFS_CLASS_MASK_THREE, TMWDEFS_TRUE }


};
static const lu_uint32_t BIConfigSize = sizeof(BIConfig)/ sizeof(BIConfigStr);

static DBIConfigStr DBIConfig[] =
{
        /* G3 group  G3 ID  protocolID  defaultVariation eventDefaultVariation eventClass                enableClass0 */
        { {0       , 27  }, 0         , 2              , 3                   , TMWDEFS_CLASS_MASK_ONE  , TMWDEFS_TRUE },
        { {0       , 28  }, 1         , 2              , 3                   , TMWDEFS_CLASS_MASK_TWO  , TMWDEFS_TRUE },
        { {0       , 29  }, 2         , 2              , 3                   , TMWDEFS_CLASS_MASK_THREE, TMWDEFS_TRUE },
        { {1       , 3   }, 3         , 2              , 3                   , TMWDEFS_CLASS_MASK_THREE, TMWDEFS_TRUE },
};
static const lu_uint32_t DBIConfigSize = sizeof(DBIConfig)/ sizeof(DBIConfigStr);

static AIConfigStr AIConfig[] =
{
        /* G3 group  G3 ID  protocolID  defaultVariation eventDefaultVariation eventClass              enableClass0 */
        { {0       , 30  }, 0         , 4              , 4                   , TMWDEFS_CLASS_MASK_ONE, TMWDEFS_TRUE },
        { {0       , 31  }, 1         , 4              , 4                   , TMWDEFS_CLASS_MASK_ONE, TMWDEFS_TRUE },
        { {0       , 32  }, 2         , 4              , 4                   , TMWDEFS_CLASS_MASK_ONE, TMWDEFS_TRUE },
        { {0       , 33  }, 3         , 4              , 4                   , TMWDEFS_CLASS_MASK_ONE, TMWDEFS_TRUE },
        { {0       , 34  }, 4         , 4              , 4                   , TMWDEFS_CLASS_MASK_ONE, TMWDEFS_TRUE },
        { {0       , 35  }, 5         , 4              , 4                   , TMWDEFS_CLASS_MASK_ONE, TMWDEFS_TRUE },
        { {0       , 36  }, 6         , 4              , 4                   , TMWDEFS_CLASS_MASK_ONE, TMWDEFS_TRUE },
        { {0       , 37  }, 7         , 4              , 4                   , TMWDEFS_CLASS_MASK_ONE, TMWDEFS_TRUE },
        { {0       , 38  }, 8         , 4              , 4                   , TMWDEFS_CLASS_MASK_ONE, TMWDEFS_TRUE },
        { {0       , 39  }, 9         , 4              , 4                   , TMWDEFS_CLASS_MASK_ONE, TMWDEFS_TRUE },
        { {0       , 40  }, 10        , 4              , 4                   , TMWDEFS_CLASS_MASK_ONE, TMWDEFS_TRUE },
        { {0       , 41  }, 11        , 4              , 4                   , TMWDEFS_CLASS_MASK_ONE, TMWDEFS_TRUE },
        { {0       , 42  }, 12        , 4              , 4                   , TMWDEFS_CLASS_MASK_ONE, TMWDEFS_TRUE },
        { {0       , 43  }, 13        , 4              , 4                   , TMWDEFS_CLASS_MASK_ONE, TMWDEFS_TRUE },
        { {0       , 44  }, 14        , 4              , 4                   , TMWDEFS_CLASS_MASK_ONE, TMWDEFS_TRUE },
        { {0       , 45  }, 15        , 4              , 4                   , TMWDEFS_CLASS_MASK_ONE, TMWDEFS_TRUE }
};
static const lu_uint32_t AIConfigSize = sizeof(AIConfig)/ sizeof(AIConfigStr);

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */


lu_uint32_t DummyDNP3SlaveProtocolManagerFactoy::protocolManagerNumber()
{
    /* The dummy factory can generate only one protocol stack manager */
    return  1;
}


DNP3SlaveProtocol* DummyDNP3SlaveProtocolManagerFactoy::newProtocolManager
                                               ( lu_uint32_t     idx           ,
                                                 SCHED_TYPE      slaveSchedType,
                                                 lu_uint32_t     slavePriority ,
                                                 GeminiDatabase &GDatabase,
                                                 IStatusManager &statusManager
                                               )
{
    TMWTARG_CONFIG targConfig;
    DNPCHNL_CONFIG chnlConfig;
    DNPTPRT_CONFIG tprtConfig;
    DNPLINK_CONFIG linkConfig;
    TMWPHYS_CONFIG physConfig;
    LINIO_CONFIG IOConfig;
    SDNPSESN_CONFIG sessionConfig;
    DNP3SlaveDB *db;
    IPointObserver **observerList;

    if(idx > 0)
    {
        return NULL;
    }

    /* Initialize channel configuration to defaults */
    tmwtarg_initConfig(&targConfig);
    dnpchnl_initConfig(&chnlConfig, &tprtConfig, &linkConfig, &physConfig);

    /* Override default configuration using user defined parameters */
    linkConfig.rxFrameSize    = DNPLinkConfig.rxFrameSize;
    linkConfig.txFrameSize    = DNPLinkConfig.txFrameSize;
    linkConfig.rxFrameTimeout = DNPLinkConfig.rxFrameTimeout;
    linkConfig.confirmMode    = DNPLinkConfig.confirmMode;
    linkConfig.confirmTimeout = DNPLinkConfig.confirmTimeout;
    linkConfig.maxRetries     = DNPLinkConfig.maxRetries;
    linkConfig.networkType    = DNPLinkConfig.networkType;
    chnlConfig.rxFragmentSize = DNPLinkConfig.rxFragmentSize;
    chnlConfig.txFragmentSize = DNPLinkConfig.txFragmentSize;

    /* Initialize IO Config Structure
     * Call liniotarg_initConfig to initialize default values, then overwrite
     * specific values as needed.
     */
    liniotarg_initConfig(&IOConfig);

    /* Override default configuration using user defined parameters */
    IOConfig.type = LINIO_TYPE_TCP;
    IOConfig.linTCP.disconnectOnNewSyn = TMWDEFS_FALSE;
    IOConfig.linTCP.validateUDPAddress = TMWDEFS_FALSE;
    strncpy(IOConfig.linTCP.chnlName, IOConfigUser.chnlName, sizeof(IOConfig.linTCP.chnlName));
    strncpy(IOConfig.linTCP.ipAddress, IOConfigUser.ipAddress, sizeof(IOConfig.linTCP.ipAddress));
    IOConfig.linTCP.ipPort = IOConfigUser.ipPort;
    IOConfig.linTCP.ipConnectTimeout = IOConfigUser.ipConnectTimeout;
    IOConfig.linTCP.mode = IOConfigUser.mode;
    IOConfig.linTCP.role = IOConfigUser.role;
    IOConfig.linTCP.localUDPPort = IOConfigUser.localUDPPort;
    IOConfig.linTCP.destUDPPort = IOConfigUser.destUDPPort;
    IOConfig.linTCP.initUnsolUDPPort = IOConfigUser.initUnsolUDPPort;

    /* Initialize DNP slave session */
    sdnpsesn_initConfig(&sessionConfig);

    /* Override default configuration using user defined parameters */
    sessionConfig.validateSourceAddress = TMWDEFS_TRUE;
    sessionConfig.authenticationEnabled = TMWDEFS_FALSE;

    sessionConfig.source = SDNPSessionConfig.source;
    sessionConfig.destination = SDNPSessionConfig.destination;
    sessionConfig.linkStatusPeriod = SDNPSessionConfig.linkStatusPeriod;
    sessionConfig.multiFragRespAllowed = SDNPSessionConfig.multiFragRespAllowed;
    sessionConfig.multiFragConfirm = SDNPSessionConfig.multiFragConfirm;
    sessionConfig.respondNeedTime = SDNPSessionConfig.respondNeedTime;
    sessionConfig.clockValidPeriod = SDNPSessionConfig.clockValidPeriod;
    sessionConfig.applConfirmTimeout = SDNPSessionConfig.applConfirmTimeout;
    sessionConfig.selectTimeout = SDNPSessionConfig.selectTimeout;
    sessionConfig.unsolAllowed = SDNPSessionConfig.unsolAllowed;
    sessionConfig.unsolClassMask = SDNPSessionConfig.unsolClassMask;
    sessionConfig.unsolClass1MaxEvents = SDNPSessionConfig.unsolClass1MaxEvents;
    sessionConfig.unsolClass2MaxEvents = SDNPSessionConfig.unsolClass2MaxEvents;
    sessionConfig.unsolClass3MaxEvents = SDNPSessionConfig.unsolClass3MaxEvents;
    sessionConfig.unsolClass1MaxDelay = SDNPSessionConfig.unsolClass1MaxDelay;
    sessionConfig.unsolClass2MaxDelay = SDNPSessionConfig.unsolClass2MaxDelay;
    sessionConfig.unsolClass3MaxDelay = SDNPSessionConfig.unsolClass3MaxDelay;
    sessionConfig.unsolMaxRetries = SDNPSessionConfig.unsolMaxRetries;
    sessionConfig.unsolRetryDelay = SDNPSessionConfig.unsolRetryDelay;
    sessionConfig.unsolOfflineRetryDelay = SDNPSessionConfig.unsolOfflineRetryDelay;
    sessionConfig.obj01DefaultVariation = SDNPSessionConfig.obj01DefaultVariation;
    sessionConfig.obj02DefaultVariation = SDNPSessionConfig.obj02DefaultVariation;
    sessionConfig.obj03DefaultVariation = SDNPSessionConfig.obj03DefaultVariation;
    sessionConfig.obj04DefaultVariation = SDNPSessionConfig.obj04DefaultVariation;
    sessionConfig.obj30DefaultVariation = SDNPSessionConfig.obj30DefaultVariation;
    sessionConfig.obj32DefaultVariation = SDNPSessionConfig.obj32DefaultVariation;

    sessionConfig.binaryInputMaxEvents = SDNPSessionConfig.binaryInputMaxEvents;
    sessionConfig.binaryInputEventMode = SDNPSessionConfig.binaryInputEventMode;

    sessionConfig.doubleInputMaxEvents = SDNPSessionConfig.doubleInputMaxEvents;
    sessionConfig.doubleInputEventMode = SDNPSessionConfig.doubleInputEventMode;

    sessionConfig.analogInputMaxEvents = SDNPSessionConfig.analogInputMaxEvents;
    sessionConfig.analogInputEventMode = SDNPSessionConfig.analogInputEventMode;


    /*****************/
    /* Initialize db */
    /*****************/

    /* Allocate db */
    db = (DNP3SlaveDB*)calloc(1, sizeof(DNP3SlaveDB));

    /* Allocate pointDB */
    db->pointDB = (TMWSDNPDatabaseStr*)calloc(1, sizeof(TMWSDNPDatabaseStr));

    /* Allocate binary input db */
    db->pointDB->binaryInput.size = BIConfigSize;
    db->pointDB->binaryInput.list = (TMWSDNPBInputStr*)calloc( BIConfigSize,
                                                               sizeof(TMWSDNPBInputStr)
                                                             );

    /* Allocate binary input observer list */
    observerList = (IPointObserver**)calloc(BIConfigSize, sizeof(IPointObserver*));
    db->BIObserverList.setTable(observerList, BIConfigSize);

    /* Initialize binary input database and observers */
    for(lu_uint32_t i = 0; i < BIConfigSize; ++i)
    {
        BIConfigStr      *confPtr = &BIConfig[i];
        TMWSDNPBInputStr *pointPtr;

        if(confPtr->protocolID < BIConfigSize)
        {
            pointPtr = &db->pointDB->binaryInput.list[i];
        }
        else
        {
            continue;
        }

        pointPtr->defaultVariation = confPtr->defaultVariation;
        pointPtr->eventDefaultVariation = confPtr->eventDefaultVariation;
        pointPtr->enableClass0 = confPtr->enableClass0;
        pointPtr->eventClass = confPtr->eventClass;
        pointPtr->value = DNPDEFS_DBAS_FLAG_OFF_LINE;

        /* Create observers */
        observerList[confPtr->protocolID] = new DNP3SlaveBIObserver(confPtr->GDBId, confPtr->protocolID, db->pointDB);
        if (GDatabase.attach(confPtr->GDBId, db->BIObserverList[confPtr->protocolID]) == GDB_ERROR_NONE)
        {
            pointPtr->enabled = LU_TRUE;
        }
        else
        {
            /* Invalid virtual point */
            delete db->BIObserverList[confPtr->protocolID];
            observerList[confPtr->protocolID] = NULL;
        }
    }

    /* Allocate double binary input db */
    db->pointDB->dbinaryInput.size = DBIConfigSize;
    db->pointDB->dbinaryInput.list = (TMWSDNPDBInputStr*)calloc( DBIConfigSize,
                                                                 sizeof(TMWSDNPDBInputStr)
                                                               );

    /* Allocate double binary input observer list */
    observerList = (IPointObserver**)calloc(DBIConfigSize, sizeof(IPointObserver*));
    db->DBIObserverList.setTable(observerList, DBIConfigSize);

    /* Initialize double binary input database and observers */
    for(lu_uint32_t i = 0; i < DBIConfigSize; ++i)
    {
        DBIConfigStr      *confPtr = &DBIConfig[i];
        TMWSDNPDBInputStr *pointPtr;

        if(confPtr->protocolID < DBIConfigSize)
        {
            pointPtr = &db->pointDB->dbinaryInput.list[i];
        }
        else
        {
            continue;
        }

        pointPtr->defaultVariation = confPtr->defaultVariation;
        pointPtr->eventDefaultVariation = confPtr->eventDefaultVariation;
        pointPtr->enableClass0 = confPtr->enableClass0;
        pointPtr->eventClass = confPtr->eventClass;
        pointPtr->value = DNPDEFS_DBAS_FLAG_OFF_LINE;

        /* Create observers */
        observerList[confPtr->protocolID] = new DNP3SlaveDBIObserver(confPtr->GDBId, confPtr->protocolID, db->pointDB);
        if (GDatabase.attach(confPtr->GDBId, db->DBIObserverList[confPtr->protocolID]) == GDB_ERROR_NONE)
        {
            pointPtr->enabled = LU_TRUE;
        }
        else
        {
            /* Invalid virtual point */
            delete db->DBIObserverList[confPtr->protocolID];
            observerList[confPtr->protocolID] = NULL;
        }
    }

    /* Allocate analogue binary input db */
    db->pointDB->analogueInput.size = AIConfigSize;
    db->pointDB->analogueInput.list = (TMWSDNPAInputStr*)calloc( AIConfigSize,
                                                                 sizeof(TMWSDNPAInputStr)
                                                               );

    /* Allocate analogue input observer list */
    observerList = (IPointObserver**)calloc(AIConfigSize, sizeof(IPointObserver*));
    db->AIObserverList.setTable(observerList, AIConfigSize);

    /* Initialize analogue input database and observers */
    for(lu_uint32_t i = 0; i < AIConfigSize; ++i)
    {
        AIConfigStr      *confPtr = &AIConfig[i];
        TMWSDNPAInputStr *pointPtr;

        if(confPtr->protocolID < AIConfigSize)
        {
            pointPtr = &db->pointDB->analogueInput.list[i];
        }
        else
        {
            continue;
        }

        pointPtr->eventDefaultVariation = confPtr->eventDefaultVariation;
        pointPtr->enableClass0 = confPtr->enableClass0;
        pointPtr->eventClass = confPtr->eventClass;
        pointPtr->flags = DNPDEFS_DBAS_FLAG_OFF_LINE;
        pointPtr->value.type = TMWTYPES_ANALOG_TYPE_SFLOAT;
        pointPtr->value.value.fval = 0;

        /* Create observers */
        observerList[confPtr->protocolID] = new DNP3SlaveAIObserver(confPtr->GDBId, confPtr->protocolID, db->pointDB);
        if (GDatabase.attach(confPtr->GDBId, db->AIObserverList[confPtr->protocolID]) == GDB_ERROR_NONE)
        {
            pointPtr->enabled = LU_TRUE;
        }
        else
        {
            /* Invalid virtual point */
            delete db->AIObserverList[confPtr->protocolID];
            observerList[confPtr->protocolID] = NULL;
        }
    }

    return new DNP3SlaveProtocol( targConfig,
                                         chnlConfig,
                                         tprtConfig,
                                         linkConfig,
                                         physConfig,
                                         IOConfig,
                                         sessionConfig,
                                         GDatabase,
                                         mcmApp,
                                         db,
                                         slaveSchedType,
                                         slavePriority
                                       );
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
