/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i870dia4.h
 * description: This file is intended for internal SCL use only.
 *  IEC 60870-5 diagnostics
 */
#ifndef I870DIA4_DEFINED
#define I870DIA4_DEFINED

#include "tmwscl/utils/tmwcnfg.h"
#include "tmwscl/i870/i870lnk4.h"

/* Define error numbers used by master and slave 104 */
typedef enum {
  I870DIA4_INVALID_SEQ,
  I870DIA4_INVALID_SENDSEQ,
  I870DIA4_INV_UFRAME ,
  I870DIA4_START_CHAR,
  I870DIA4_RCV_LENGTH,
  I870DIA4_IFRAME_TO,
  I870DIA4_UFRAME_TO,
  I870DIA4_STOPDT_TO,
  I870DIA4_TX_FULL,
  I870DIA4_K_TOO_BIG,
  I870DIA4_DISCARD_STOPPED,
  I870DIA4_DISCARD_INACTIVE,
   
  /* This must be last entry */
  I870DIA4_ERROR_ENUM_MAX

} I870DIA4_ERROR_ENUM; 

#if !TMWCNFG_SUPPORT_DIAG

#define I870DIA4_LINK_FRAME_SENT(pSession, pBuf, numBytes) \
  TMWTARG_UNUSED_PARAM(pSession); TMWTARG_UNUSED_PARAM(pBuf); TMWTARG_UNUSED_PARAM(numBytes);

#define I870DIA4_LINK_FRAME_RECEIVED(pSession, pBuf, numBytes, elapsedTime) \
  TMWTARG_UNUSED_PARAM(pSession); TMWTARG_UNUSED_PARAM(pBuf); TMWTARG_UNUSED_PARAM(numBytes); TMWTARG_UNUSED_PARAM(elapsedTime);

#define I870DIA4_ERROR(pChannel, errorNumber) \
  TMWTARG_UNUSED_PARAM(pChannel); TMWTARG_UNUSED_PARAM(errorNumber);

#else

#define I870DIA4_LINK_FRAME_SENT(pSession, pBuf, numBytes) \
  i870dia4_linkFrameSent(pSession, pBuf, numBytes)

#define I870DIA4_LINK_FRAME_RECEIVED(pSession, pBuf, numBytes, elapsedTime) \
  i870dia4_linkFrameReceived(pSession, pBuf, numBytes, elapsedTime)

#define I870DIA4_ERROR(pChannel, errorNumber) \
  i870dia4_error(pChannel, errorNumber)

#ifdef __cplusplus
extern "C" {
#endif

  /* routine: i870dia4_init
   * purpose: internal diagnostic init function
   * arguments:
   *  void
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL i870dia4_init(void);
  
  /* routine: i870dia4_validateErrorTable
   * purpose: Called only to verify if error message table is correct.
   *  This is intended for test purposes only.
   * arguments:
   *  void
   * returns:
   *  TMWDEFS_TRUE if formatted correctly
   *  TMWDEFS_FALSE if there is an error in the table.
   */
  TMWTYPES_BOOL i870dia4_validateErrorTable(void);

  /* function: i870dia4_linkFrameSent
   * purpose: display link level frame
   * arguments:
   *  channelName - name of channel
   *  pBuf - pointer to message
   *  numBytes - number of bytes in message
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL i870dia4_linkFrameSent(
    TMWCHNL *pChannel,
    const TMWTYPES_UCHAR *pBuf, 
    TMWTYPES_USHORT numBytes);

  /* function: i870dia4_link4FrameReceived
   * purpose: display link level frame
   * arguments:
   *  channelName - name of channel
   *  pBuf - pointer to message
   *  numBytes - number of bytes in message
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL i870dia4_linkFrameReceived(
    TMWCHNL *pChannel,
    const TMWTYPES_UCHAR *pBuf, 
    TMWTYPES_USHORT numBytes,
    TMWTYPES_MILLISECONDS elapsedTime);
 
  /* function: i870dia4_error
   * purpose: Display error message
   * arguments:
   *  pChannel - pointer from which this message originated
   *  errorNumber - enum indicating what error message to display
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL i870dia4_error(
    TMWCHNL *pChannel,
    I870DIA4_ERROR_ENUM errorNumber);

  /* function: i870dia4_errorMsgEnable
   * purpose: Enable/Disable specific error message output
   * arguments:
   *  errorNumber - enum indicating what error message
   *  enabled - TMWDEFS_TRUE if error message should be enabled
   *            TMWDEFS_FALSE if error message should be disabled
   * returns:
   *  void
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL i870dia4_errorMsgEnable(
    I870DIA4_ERROR_ENUM errorNumber,
    TMWTYPES_BOOL enabled);

#ifdef __cplusplus
}
#endif
#endif /* TMWCNFG_SUPPORT_DIAG */
#endif /* I870DIA4_DEFINED */
