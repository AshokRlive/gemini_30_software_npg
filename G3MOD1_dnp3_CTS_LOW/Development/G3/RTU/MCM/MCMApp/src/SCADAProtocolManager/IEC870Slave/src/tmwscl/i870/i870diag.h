/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i870diag.h
 * description: Generic I870 diagnostics
 */
#ifndef I870DIAG_DEFINED
#define I870DIAG_DEFINED

#include "tmwscl/utils/tmwcnfg.h"
#include "tmwscl/utils/tmwtarg.h"
#include "tmwscl/utils/tmwchnl.h"
#include "tmwscl/utils/tmwsesn.h"
#include "tmwscl/utils/tmwdtime.h"

#include "tmwscl/i870/i870chnl.h"

/* Structure to be used in error tables */
typedef struct i870DiagErrorEntry {
  TMWTYPES_USHORT errorNumber;
  const TMWTYPES_CHAR  *pErrorMsg;
} I870DIAG_ERROR_ENTRY;

/* Define error numbers used by master and slave IEC protocols*/
typedef enum {
  I870DIAG_INCREMENTAL,
  I870DIAG_RESPONSE_TO,
  I870DIAG_FAILED_TX,
  I870DIAG_SAME_TYPE,
  I870DIAG_DUPLICATE,
  I870DIAG_CANCELED,
  I870DIAG_BUFLEN,
  I870DIAG_INVALID_SIZE,

  /* This must be last entry */
  I870DIAG_ERROR_ENUM_MAX

} I870DIAG_ERROR_ENUM; 

/* IOA for 101/104 is structured and should be displayed a byte at a time */
#define I870DIAG_STRUCTURED_IOA 0x01


#if !TMWCNFG_SUPPORT_DIAG

#define I870DIAG_SESSION_ONLINE(pSession, online) \
  TMWTARG_UNUSED_PARAM(pSession); TMWTARG_UNUSED_PARAM(online);

#define I870DIAG_INSERT_QUEUE(pChannel, pSession, pSector, description) \
  TMWTARG_UNUSED_PARAM(pChannel); TMWTARG_UNUSED_PARAM(pSession); TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(description);

#define I870DIAG_REMOVE_MESSAGE(pTxData, status) \
  TMWTARG_UNUSED_PARAM(pTxData); TMWTARG_UNUSED_PARAM(status);

#define I870DIAG_NOT_REMOVED_MESSAGE(pTxData) \
  TMWTARG_UNUSED_PARAM(pTxData);

#define I870DIAG_ERROR(pChannel, pSession, pSector, errorNumber) \
  TMWTARG_UNUSED_PARAM(pChannel); TMWTARG_UNUSED_PARAM(pSession); TMWTARG_UNUSED_PARAM(pSector);\
  TMWTARG_UNUSED_PARAM(errorNumber);

#define I870DIAG_ERROR_MSG(pChannel, pSession, pSector, errorNumber, pExtraTextMsg) \
  TMWTARG_UNUSED_PARAM(pChannel); TMWTARG_UNUSED_PARAM(pSession); TMWTARG_UNUSED_PARAM(pSector);\
  TMWTARG_UNUSED_PARAM(errorNumber); TMWTARG_UNUSED_PARAM(pExtraTextMsg);

#else

#define I870DIAG_SESSION_ONLINE(pSession, online) \
  i870diag_sessionOnline(pSession, online);

#define I870DIAG_INSERT_QUEUE(pChannel, pSession, pSector, description) \
  i870diag_insertQueue(pChannel, pSession, pSector, description)

#define I870DIAG_REMOVE_MESSAGE(pTxData, status) \
  i870diag_removeMessage(pTxData, status)

#define I870DIAG_NOT_REMOVED_MESSAGE(pTxData) \
  i870diag_notRemovedMessage(pTxData)

#define I870DIAG_ERROR(pChannel, pSession, pSector, errorNumber) \
  i870diag_errorMsg(pChannel, pSession, pSector, errorNumber, TMWDEFS_NULL)

#define I870DIAG_ERROR_MSG(pChannel, pSession, pSector, errorNumber, pExtraTextMsg) \
  i870diag_errorMsg(pChannel, pSession, pSector, errorNumber, pExtraTextMsg)

#ifdef __cplusplus
extern "C" {
#endif

  /* routine: i870diag_init
   * purpose: internal diagnostic init function
   * arguments:
   *  void
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL i870diag_init(void);
  
  /* routine: i870diag_validateErrorTable
   * purpose: Called only to verify if error message table is correct.
   *  This is intended for test purposes only.
   * arguments:
   *  void
   * returns:
   *  TMWDEFS_TRUE if formatted correctly
   *  TMWDEFS_FALSE if there is an error in the table.
   */
  TMWTYPES_BOOL i870diag_validateErrorTable(void);

  /* function: i870diag_sessionOnline
   * purpose: display session online/offline
   * arguments:
   *  pSession - pointer to session
   *  online - TMWDEFS_TRUE if online, else TMWDEFS_FALSE
   * returns:
   *  void
   */
  void i870diag_sessionOnline(
    TMWSESN *pSession,
    TMWTYPES_BOOL online);

  /* function: i870diag_insertQueue
   * purpose:
   * arguments:
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL i870diag_insertQueue(
    TMWCHNL *pChannel,
    TMWSESN *pSession, 
    TMWSCTR *pSector,
    const char *description);

  /* function: m14diag_removeMessage 
   * purpose: Display removed from queue message
   * arguments:
   *  pChannel
   *  pSession
   *  pSector
   *  description
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL i870diag_removeMessage(
    TMWSESN_TX_DATA *pTxData,
    I870CHNL_RESP_STATUS status); 
  
  /* function: i870diag_notRemovedMessage 
   * purpose: Display not removed from queue message
   *  when expecting more responses to broadcast request
   * arguments:
   *  pChannel
   *  pSession
   *  pSector
   *  description
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL i870diag_notRemovedMessage(
    TMWSESN_TX_DATA *pTxData);

  /* function: i870diag_error
   * purpose: Display error message
   * arguments:
   *  pChannel - channel from which this message originated
   *  pSession - session from which this message originated
   *  errorNumber - enum indicating what error message to display
   *  pExtraTextMsg - pointer to additional text to display with error msg
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL i870diag_errorMsg(
    TMWCHNL *pChannel,
    TMWSESN *pSession,  
    TMWSCTR *pSector,
    I870DIAG_ERROR_ENUM errorNumber,
    TMWTYPES_CHAR *pExtraTextMsg);

  /* function: i870diag_errorMsgEnable
   * purpose: Enable/Disable specific error message output
   * arguments:
   *  errorNumber - enum indicating what error message
   *  enabled - TMWDEFS_TRUE if error message should be enabled
   *            TMWDEFS_FALSE if error message should be disabled
   * returns:
   *  void
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL i870diag_errorMsgEnable(
    I870DIAG_ERROR_ENUM errorNumber,
    TMWTYPES_BOOL enabled);


#ifdef __cplusplus
}
#endif
#endif /* TMWCNFG_SUPPORT_DIAG */
#endif /* I870DIAG_DEFINED */
