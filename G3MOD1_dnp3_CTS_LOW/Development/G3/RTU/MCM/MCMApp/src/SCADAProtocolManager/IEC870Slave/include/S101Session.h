/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:S101Session.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief Protocol Session
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19 Jul 2016     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef S101SESSION_H_
#define S101SESSION_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "S101TMWIncludes.h"
#include "S101Sector.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
class S101Channel;  //Forward declaration


class S101Session
{
public:
    S101Session(S101Channel& channel);
    virtual ~S101Session();

    SCADAP_ERROR openSession(TMWCHNL* tmwchnl);

    void closeSession();

    void configure(const S101SESN_CONFIG& sessionConfig);

    void addSector(S101Sector* sectorPtr);

    S101Channel& getChannel(){return m_channel;}

private:
    typedef std::vector<S101Sector*>        S101SectorVector;

    S101Channel&        m_channel;          // Owner channel reference
    TMWSESN*            mp_tmwsesn;
    S101SESN_CONFIG*    mp_tmwsesnCnfg;
    S101SectorVector    m_sectors;
};


#endif /* S101SESSION_H_ */

/*
 *********************** End of file ******************************************
 */
