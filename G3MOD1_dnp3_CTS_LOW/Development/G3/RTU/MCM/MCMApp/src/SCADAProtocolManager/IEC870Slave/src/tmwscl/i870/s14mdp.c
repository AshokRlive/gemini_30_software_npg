/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14mdp.c
 * description: IEC 60870-5-101 slave monitored double point functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14mdp.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14evnt.h"
#include "tmwscl/i870/s14mem.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/i870util.h"
#include "tmwscl/utils/tmwmem.h"
#include "tmwscl/utils/tmwtarg.h"

#if S14DATA_SUPPORT_MDP

/* function: _changedFunc 
 * purpose: Determine if the specified point has changed and if so
 *  get the current values and add an event to the queue
 * arguments:
 *  pSector - identifies sector
 * returns:
 *  TMWDEFS_TRUE if an event was added
 *  TMWDEFS_FALSE otherwise
 */
static TMWTYPES_BOOL TMWDEFS_CALLBACK _changedFunc(
  TMWSCTR *pSector,
  void *pPoint, 
  TMWDTIME *pTimeStamp)
{  
  TMWDEFS_CHANGE_REASON reason;
  TMWTYPES_UCHAR diq;

  if(s14data_mdpChanged(pPoint, &diq, &reason))
  { 
    if(s14mdp_internalAddEvent(pSector, s14evnt_reasonToCOT(reason), 
      s14data_mdpGetInfoObjAddr(pPoint), diq, 
      pTimeStamp) != TMWDEFS_NULL)
    {
      return(TMWDEFS_TRUE);
    }
  }
  return(TMWDEFS_FALSE);
}

/* function: _eventData 
 * purpose: Insert data from event structure into message to be sent
 * arguments:
 *  pTxData - pointer to transmit data structure
 *  pEvent - pointer to event to be put into message
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _eventData(
  TMWSESN_TX_DATA *pTxData, 
  S14EVNT *pEvent)
{
  /* Store quality */
  pTxData->pMsgBuf[pTxData->msgLength++] = pEvent->quality;
}

/* function: _initEventDesc 
 * purpose: Initialize event descriptor
 * arguments:
 *  pSector - identifies sector
 *  pDesc - pointer to descriptor structure 
 * returns:
 *  void
 */
static void TMWDEFS_LOCAL _initEventDesc(
  TMWSCTR *pSector,
  S14EVNT_DESC *pDesc)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  pDesc->typeId = I14DEF_TYPE_MDPNA1;
  pDesc->woTimeTypeId = I14DEF_TYPE_MDPNA1;
  pDesc->cotSpecified = 0;
  pDesc->eventMemType = S14MEM_MDP_EVENT_TYPE;
  pDesc->pEventList = &p14Sector->mdpEvents;
  pDesc->eventMode = p14Sector->mdpEventMode;
  pDesc->scanEnabled = p14Sector->mdpScanEnabled;
  pDesc->maxEvents = p14Sector->mdpMaxEvents;
  pDesc->timeFormat = p14Sector->mdpTimeFormat;
  pDesc->doubleTransmission = TMWDEFS_FALSE;
  pDesc->pEventsOverflowedFlag = &p14Sector->mdpEventsOverflowed;
  pDesc->pChangedFunc = _changedFunc;
  pDesc->pGetPointFunc = s14data_mdpGetPoint;
  pDesc->pEventDataFunc = _eventData;
}

/* function: _storeInResponse */
static void TMWDEFS_CALLBACK _storeInResponse(
  TMWSESN_TX_DATA *pTxData, 
  void *pPoint,
  TMWDEFS_TIME_FORMAT timeFormat)
{
  if(timeFormat == TMWDEFS_TIME_FORMAT_NONE)
  {
    pTxData->pMsgBuf[pTxData->msgLength++] = s14data_mdpGetFlagsAndValue(pPoint);
  }
  else
  {
    TMWDTIME timeStamp;
    pTxData->pMsgBuf[pTxData->msgLength++] = s14data_mdpGetFlagsValueTime(pPoint, &timeStamp);
    if(timeFormat == TMWDEFS_TIME_FORMAT_24)
    {
      i870util_write24BitTime(pTxData, &timeStamp, S14DATA_SUPPORT_GENUINE_TIME);
    }
    else if(timeFormat == TMWDEFS_TIME_FORMAT_56)
    {
      i870util_write56BitTime(pTxData, &timeStamp, S14DATA_SUPPORT_GENUINE_TIME);
    }
  }
}

/* function: s14mdp_init */
void TMWDEFS_GLOBAL s14mdp_init(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  tmwdlist_initialize(&p14Sector->mdpEvents);
  p14Sector->mdpEventsOverflowed = TMWDEFS_FALSE;
}

/* function: s14mdp_close */
void TMWDEFS_GLOBAL s14mdp_close(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  tmwdlist_destroy(&p14Sector->mdpEvents, s14mem_free);
}

/* function: s14mdp_internalAddEvent */
void * TMWDEFS_GLOBAL s14mdp_internalAddEvent(
  TMWSCTR *pSector, 
  TMWTYPES_UCHAR cot,
  TMWTYPES_ULONG ioa, 
  TMWTYPES_UCHAR diq, 
  TMWDTIME *pTimeStamp)
{
  S14EVNT_DESC desc;
  void *pEvent;

  _initEventDesc(pSector, &desc);

#if S14DATA_SUPPORT_DOUBLE_TRANS
  { 
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  if(p14Sector->mdpTransmissionMode == S14DATA_TRANSMISSION_DOUBLE)
  {
    desc.doubleTransmission = TMWDEFS_TRUE;
  }
  else if(p14Sector->mdpTransmissionMode == S14DATA_TRANSMISSION_PERPOINT)
  { 
    void *pPoint = s14data_mdpLookupPoint(p14Sector->i870.pDbHandle, ioa);
    if((pPoint != TMWDEFS_NULL)
      && s14data_mdpGetTransmissionMode(pPoint))
    {
      desc.doubleTransmission = TMWDEFS_TRUE;
    }
  }
  }
#endif
  pEvent = s14evnt_addEvent(pSector, cot, ioa, diq, pTimeStamp, &desc, TMWDEFS_NULL);
  
  return(pEvent);
}

/* function: s14mdp_addEvent */
void * TMWDEFS_GLOBAL s14mdp_addEvent(
  TMWSCTR *pSector, 
  TMWTYPES_UCHAR cot,
  TMWTYPES_ULONG ioa, 
  TMWTYPES_UCHAR diq, 
  TMWDTIME *pTimeStamp)
{
  void *pEvent;
  TMWTARG_LOCK_SECTION(&pSector->pSession->pChannel->lock);

  pEvent = s14mdp_internalAddEvent(pSector, cot, ioa, diq, pTimeStamp);
  if(pEvent != TMWDEFS_NULL)
  {
    /* If an event was added tell link layer we have data */ 
    s14event_linkDataReady(pSector);
  }

  TMWTARG_UNLOCK_SECTION(&pSector->pSession->pChannel->lock);
  return(pEvent);
}

/* function: s14mdp_countEvents */
TMWTYPES_USHORT TMWDEFS_GLOBAL s14mdp_countEvents(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  return(s14evnt_countEvents(pSector, &p14Sector->mdpEvents));
}

/* function: s14mdp_scanForChanges */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14mdp_scanForChanges(
  TMWSCTR *pSector)
{
  S14EVNT_DESC desc;

  _initEventDesc(pSector, &desc);

  return(s14evnt_scanForChanges(pSector, &desc));
}

/* function: s14mdp_processEvents */
S14EVNT_STATUS TMWDEFS_GLOBAL s14mdp_processEvents(
  TMWSCTR *pSector, 
  TMWDTIME *pEventTime,
  TMWTYPES_UCHAR cotSpecified)
{
  S14EVNT_DESC desc;
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  TMWTYPES_UCHAR typeId = I14DEF_TYPE_MDPNA1;

  if(p14Sector->mdpTimeFormat == TMWDEFS_TIME_FORMAT_24) 
  {
    typeId = I14DEF_TYPE_MDPTA1;
  }
  else if(p14Sector->mdpTimeFormat == TMWDEFS_TIME_FORMAT_56)
  {
    typeId = I14DEF_TYPE_MDPTB1;
  }

  _initEventDesc(pSector, &desc);
  desc.typeId = typeId;
  desc.cotSpecified = cotSpecified;
  return(s14evnt_processEvents(pSector, &desc, 1, pEventTime, TMWDEFS_FALSE)); 
}

/* function: s14mdp_processDblTransEvents */
S14EVNT_STATUS TMWDEFS_GLOBAL s14mdp_processDblTransEvents(
  TMWSCTR *pSector)
{ 
  S14EVNT_DESC desc;
  _initEventDesc(pSector, &desc);
  return(s14evnt_processEvents(pSector, &desc, 1, TMWDEFS_NULL, TMWDEFS_TRUE));
}

/* function: s14mdp_readIntoResponse */
S14DBAS_GROUP_STATUS TMWDEFS_CALLBACK s14mdp_readIntoResponse(
  TMWSESN_TX_DATA *pTxData, 
  TMWSCTR *pSector, 
  TMWTYPES_UCHAR cot, 
  TMWDEFS_GROUP_MASK groupMask, 
  TMWTYPES_ULONG ioa,
  TMWTYPES_USHORT *pPointIndex)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  TMWDEFS_TIME_FORMAT timeFormat = TMWDEFS_TIME_FORMAT_NONE;
 
  /* if this was a read CRDNA, then return the correct time variation 
   * Spec says for background, cyclic or CICNA always send without time variation
   * Some customers require Time Variation in response to CICNA, so allow that.
   */
  if(groupMask == TMWDEFS_GROUP_MASK_ANY)
  {
    /* CRDNA */
    timeFormat = p14Sector->readTimeFormat;
  }
  else if(groupMask < TMWDEFS_GROUP_MASK_CYCLIC)
  {
    /* CICNA */
    timeFormat = p14Sector->cicnaTimeFormat;
  } 

  return(s14util_readIntoResponse(pTxData, pSector, cot, 
    groupMask, ioa, I14DEF_TYPE_MDPNA1, timeFormat, 1, pPointIndex, s14data_mdpGetPoint, 
    s14data_mdpGetGroupMask, s14data_mdpGetInfoObjAddr, s14data_mdpGetIndexed, s14data_getTimeFormat,
    _storeInResponse));
}

#endif /* S14DATA_SUPPORT_MDP */
