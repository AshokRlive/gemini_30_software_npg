/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i870lnk1.c
 * description: IEC 60870-5 FT12 Link Layer Implementation
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/utils/tmwtarg.h"
#include "tmwscl/utils/tmwscl.h"
#include "tmwscl/utils/tmwtimer.h"

#include "tmwscl/i870/i870lnk1.h"
#include "tmwscl/i870/i870chnl.h"
#include "tmwscl/i870/i870mem1.h"
#include "tmwscl/i870/i870dia1.h"
#include "tmwscl/i870/i870ft12.h"
#include "tmwscl/i870/i870stat.h"

/* Forward declarations */
static void TMWDEFS_LOCAL _sendFixedFrame(
  I870LNK1_CONTEXT *pLinkContext,
  TMWSESN *pSession,
  TMWTYPES_UCHAR control);

static void TMWDEFS_LOCAL _balancedRetryTimeout(
  void *pCallbackParam);

static void TMWDEFS_LOCAL _sendTestFrame(
  void *pCallbackParam);

/* Local function declarations */

static void TMWDEFS_LOCAL _setLastSessionPolled(
  I870LNK1_CONTEXT *pLinkContext, 
  TMWSESN *pSession)
{
  /* Only set lastSessionPolled if there is more than 1 session,
   * this will cause class1PollCount to be ignored as we promised
   */
  if(tmwdlist_size(&pLinkContext->tmw.sessions) > 1)
    pLinkContext->pLastSessionPolled = pSession;
}

/* function: _getSessions
 * purpose: return list of sessions on this channel
 * arguments:
 *  pContext - link layer context
 * returns:
 *  pointer to list of sessions
 */
static TMWDLIST * TMWDEFS_CALLBACK _getSessions(
  TMWLINK_CONTEXT *pContext)
{
  return(tmwlink_getSessions(pContext));
}

/* function: _openSession
 * purpose: opens a new session on this channel
 * arguments:
 *  pContext - link layer context returned from i870lnk1_initChannel
 * returns
 *  TMWDEFS_TRUE if successful, else TMWDEFS_FALSE
 */
static TMWTYPES_BOOL TMWDEFS_CALLBACK _openSession(
  TMWLINK_CONTEXT *pContext,
  TMWSESN *pSession,
  void *pConfig)
{
  I870LNK1_CONTEXT *pLinkContext = (I870LNK1_CONTEXT *)pContext;
  I870LN1P_SESN_CONFIG *pSessionConfig = (I870LN1P_SESN_CONFIG *)pConfig;
  I870LNK1_SESSION_INFO *pLinkSession;

  /* For now prevent a 103 session on a balanced link.
   * This should work, but it is not in 870-5-103 and we  
   * don't currently run regression tests for this. When it  
   * is requested, test this works properly.
   */
  if((pSession->protocol == TMWTYPES_PROTOCOL_103) 
    && (pLinkContext->linkMode == TMWDEFS_LINK_MODE_BALANCED))
  {
    I870DIA1_ERROR(pLinkContext->tmw.pChannel, pSession, I870DIA1_BALANCED_NA);
    return(TMWDEFS_FALSE);
  }

  /* Allocate and initialize protocol specific session context */
  pLinkSession = (I870LNK1_SESSION_INFO *)i870mem1_alloc(I870MEM1_SESSION_INFO_TYPE);
  if(pLinkSession == TMWDEFS_NULL)
  {
    I870DIA1_ERROR(pLinkContext->tmw.pChannel, pSession, I870DIA1_ALLOC_SESN);
    return(TMWDEFS_FALSE);
  }

  pLinkSession->sesnState = I870LNK1_SESSION_STATE_STARTUP;
  pLinkSession->secondaryNotReset = TMWDEFS_TRUE;
  pLinkSession->balancedMasterRestart = TMWDEFS_FALSE;
  pLinkSession->class1DataAvailable = TMWDEFS_FALSE;
  pLinkSession->txFrameCountBit = TMWDEFS_TRUE;
  pLinkSession->rxFrameCountBit = TMWDEFS_TRUE;
  pLinkSession->classPollSent = TMWDEFS_FALSE;
  pLinkSession->DFC1Received = TMWDEFS_FALSE;
  pLinkSession->sendDFC1 = TMWDEFS_FALSE;
  pLinkSession->fixedControlNeeded = 0xff;

  pLinkSession->autoClassPolling   = pSessionConfig->autoClassPolling;
  pLinkSession->classPendingCount  = pSessionConfig->classPendingCount;
  pLinkSession->class1PollCount    = pSessionConfig->class1PollCount;
  pLinkSession->class1PendingDelay = pSessionConfig->class1PendingDelay;
  pLinkSession->class1PollDelay    = pSessionConfig->class1PollDelay;
  pLinkSession->class2PendingDelay = pSessionConfig->class2PendingDelay;
  pLinkSession->class2PollDelay    = pSessionConfig->class2PollDelay;
  pLinkSession->isRequestPending   = pSessionConfig->isRequestPending;
  pLinkSession->maxPollDelay       = pSessionConfig->maxPollDelay;
  pLinkSession->send103ResetFCB    = pSessionConfig->send103ResetFCB;

  tmwtimer_init(&pLinkSession->pollTimer);

  pSession->pLinkSession = pLinkSession;

  /* Open channel if required */
  tmwlink_openChannel(pContext);

  /* Open generic session */
  if(tmwlink_openSession(pContext, pSession))
  {
    /* See if channel is already open and not busy */
    if(pContext->isOpen 
      && (pLinkContext->pCurrentSession == TMWDEFS_NULL))
    {
      I870LNK1_SESSION_INFO *pLinkSession = 
        (I870LNK1_SESSION_INFO *)pSession->pLinkSession;

      /* Protocol specific session initialization */
      if(pLinkContext->linkMode == TMWDEFS_LINK_MODE_UNBALANCED)
      { 
        if(pSession->type == TMWTYPES_SESSION_TYPE_MASTER)
        {
          /* For 103, send RESET_FCB */
          if(pSession->protocol == TMWTYPES_PROTOCOL_103)
          {
            /* Waiting for reset response */
            pLinkSession->sesnState = I870LNK1_SESSION_STATE_RESET_PENDING;
            if(pLinkSession->send103ResetFCB)
              _sendFixedFrame(pLinkContext, pSession, I870FT12_CONTROL_PRM | I870FT12_FC_LINK_RESET_FCB);
            else
              _sendFixedFrame(pLinkContext, pSession, I870FT12_CONTROL_PRM | I870FT12_FC_LINK_RESET_CU);
          }
          /* For 101 and 102, send request status */
          else
          {
            /* Waiting for status of link */
            pLinkSession->sesnState = I870LNK1_SESSION_STATE_REQUEST_STATUS;
            _sendFixedFrame(pLinkContext, pSession, I870FT12_CONTROL_PRM | I870FT12_FC_REQUEST_STATUS);
          }
        }
      } 
      else /* TMWDEFS_LINK_MODE_BALANCED */
      {
        /* Waiting for status of link */
        pLinkSession->sesnState = I870LNK1_SESSION_STATE_REQUEST_STATUS;
        _sendFixedFrame(pLinkContext, pSession, I870FT12_CONTROL_PRM | I870FT12_FC_REQUEST_STATUS);
      }
    }

    if(pLinkContext->linkMode == TMWDEFS_LINK_MODE_BALANCED)
    {
      if(pSession->type == TMWTYPES_SESSION_TYPE_SLAVE) 
      {
       /* Start a periodic timer, so there is an event that causes us to ask the application
        * if it has anything to send. Otherwise in balanced mode if a command takes time it
        * will not be sent immediately and since the master does not poll us, we need an event
        * to cause us to ask again.
        */
      tmwtimer_start(&pLinkContext->balancedRetryTimer, 
        500, pSession->pChannel, 
        _balancedRetryTimeout, pLinkContext);
      }
      else
      {
       /* For 101 test case 5.4.17.1 of IEC60870-5-601 says "If the controlling station was 
        * re-initialised before the controlled station detected the lost link, the controlled 
        * station can start sending data immediately".
        * So allow it.
        */
        pLinkSession->balancedMasterRestart = TMWDEFS_TRUE;
      }

      if(pLinkContext->testFramePeriod != 0)
      {
        /* Start a periodic timer to send a test frame to verify if link is up */
        tmwtimer_start(&pLinkContext->testFrameTimer, 
          pLinkContext->testFramePeriod, pSession->pChannel, 
          _sendTestFrame, pLinkContext);
      }
    }

    /* Success */
    return(TMWDEFS_TRUE);
  }

  /* Open session failed */
  i870mem1_free(pSession->pLinkSession);
  return(TMWDEFS_FALSE);
}

/* function: _closeSession
 * purpose: closes specified session
 * arguments:
 *  pContext - link layer context returned from i870lnk1_initChannel
 * returns
 *  TMWDEFS_TRUE if successful, else TMWDEFS_FALSE
 */
static TMWTYPES_BOOL TMWDEFS_CALLBACK _closeSession(
  TMWLINK_CONTEXT *pContext,
  TMWSESN *pSession)
{
  I870LNK1_SESSION_INFO *pLinkSession = (I870LNK1_SESSION_INFO *)pSession->pLinkSession;
  I870LNK1_CONTEXT *pLinkContext = (I870LNK1_CONTEXT *)pContext;

  /* If this session has a pending request cancel it */
  if(pSession == pLinkContext->pCurrentSession)
  {
    tmwtimer_cancel(&pLinkContext->confirmTimer);
    pLinkContext->pCurrentSession = TMWDEFS_NULL;
  }

  /* Perform protocol specific session shutdown */
  tmwtimer_cancel(&pLinkSession->pollTimer);
  i870mem1_free(pSession->pLinkSession);

  /* in case last session polled is the session being closed */
  pLinkContext->pLastSessionPolled = TMWDEFS_NULL;

  pLinkContext->tmw.pInfoFunc(
    pLinkContext->tmw.pCallbackParam, 
    pSession, TMWSCL_INFO_OFFLINE);
   
  /* Perform generic session shutdown and return */
  return(tmwlink_closeSession(pContext, pSession));
}

/* function: _pollDelayTimeout
 * purpose: Handle timeouts where this device was not polled fast enough,
 *  assume remote device is offline.
 * arguments:
 *  pCallbackParam - points to session
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _pollDelayTimeout(
  void *pCallbackParam)
{
  TMWSESN *pSession = (TMWSESN *)pCallbackParam;
  I870LNK1_SESSION_INFO *pLinkSession = (I870LNK1_SESSION_INFO *)pSession->pLinkSession;
  I870LNK1_CONTEXT *pLinkContext = (I870LNK1_CONTEXT *)pSession->pChannel->pLinkContext;

  /* Tell application layer we are offline */
  pLinkContext->tmw.pInfoFunc(
    pLinkContext->tmw.pCallbackParam, pSession, TMWSCL_INFO_OFFLINE);
  
  /* Reset session status */
  pLinkSession->sesnState = I870LNK1_SESSION_STATE_STARTUP;
}

/* function: _checkForIdleChannel
 * purpose: Determine if callback function has been registered and channel is idle
 *  if so call idle callback function
 * arguments:
 *  pContext - link layer context returned from i870lnk1_initChannel
 * returns
 *  void
 */
static void TMWDEFS_LOCAL _checkForIdleChannel(
 I870LNK1_CONTEXT *pLinkContext)
{
  TMWCHNL *pChannel;

  /* Determine idle callback function has been registered */
  /* If so determine if channel is idle and call idle callback */
  pChannel = pLinkContext->tmw.pChannel;
  if(  (pChannel->pIdleCallbackFunc != TMWDEFS_NULL)
    && (pLinkContext->channelIdle == TMWDEFS_TRUE)
    && (tmwdlist_size(&pChannel->messageQueue) == 0)
    && (!pLinkContext->transmitInProgress))
  {
    pChannel->pIdleCallbackFunc(pChannel->pIdleCallbackParam);
  } 
}

/* function: _infoFunc
 * purpose: send TMWSCL_INFO to the layer above. If redundancy is not
 *  enabled, perform some basic retry strategy first to hide details 
 *  from application layer. If redundancy is enabled just pass the 
 *  information up and let it decide what to do.
 * arguments:
 *  pContext - pointer to link context structure
 *  pSession - pointer to session structure
 *  sesnInfo - event such as OFFLINE
 * returns
 *  void
 */
static void TMWDEFS_CALLBACK _infoFunc(
  TMWLINK_CONTEXT *pContext,
  TMWCHNL *pChannel,
  TMWSCL_INFO sesnInfo)
{
  I870LNK1_CONTEXT *pLinkContext = (I870LNK1_CONTEXT *)pContext;
  TMWSESN *pSession = TMWDEFS_NULL;
  TMWTARG_UNUSED_PARAM(pChannel);

  switch(sesnInfo)
  {
  case TMWSCL_INFO_OPENED:
     
    /* Send online to all current sessions */
    while((pSession = (TMWSESN *)tmwdlist_getAfter(
      &pContext->sessions, (TMWDLIST_MEMBER *)pSession)) != TMWDEFS_NULL)
    {
      I870LNK1_SESSION_INFO *pLinkSession = (I870LNK1_SESSION_INFO *)pSession->pLinkSession;

      /* Protocol specific session initialization */
      if(pLinkContext->linkMode == TMWDEFS_LINK_MODE_UNBALANCED)
      {
        if(pSession->type == TMWTYPES_SESSION_TYPE_MASTER)
        {
          /* For 103, send RESET_FCB */
          if(pSession->protocol == TMWTYPES_PROTOCOL_103)
          {
            /* Set session state to waiting for reset response */
            pLinkSession->sesnState = I870LNK1_SESSION_STATE_RESET_PENDING;
            if(pLinkSession->send103ResetFCB)
              _sendFixedFrame(pLinkContext, pSession, I870FT12_CONTROL_PRM | I870FT12_FC_LINK_RESET_FCB);
            else
              _sendFixedFrame(pLinkContext, pSession, I870FT12_CONTROL_PRM | I870FT12_FC_LINK_RESET_CU);
          }
          /* For 101 and 102, send request status */
          else
          {
            /* Set session state to waiting for status of link */
            pLinkSession->sesnState = I870LNK1_SESSION_STATE_REQUEST_STATUS;
            _sendFixedFrame(pLinkContext, pSession, I870FT12_CONTROL_PRM | I870FT12_FC_REQUEST_STATUS);
          }
        }

        /* Next transmitted FCB should be 1 */
        pLinkSession->txFrameCountBit = TMWDEFS_TRUE;
      } 
      else if(pLinkContext->pCurrentSession == TMWDEFS_NULL)
      {
        /* BALANCED and channel is not busy.*/
        /* Set session state to waiting for status of link */
        pLinkSession->sesnState = I870LNK1_SESSION_STATE_REQUEST_STATUS;
        _sendFixedFrame(pLinkContext, pSession, I870FT12_CONTROL_PRM | I870FT12_FC_REQUEST_STATUS);
      
        /* Next transmitted FCB should be 1 */
        pLinkSession->txFrameCountBit = TMWDEFS_TRUE;
      }
    }
    break;

  case TMWSCL_INFO_CLOSED:
    /* Send offline to all current sessions */
    while((pSession = (TMWSESN *)tmwdlist_getAfter(
      &pContext->sessions, (TMWDLIST_MEMBER *)pSession)) != TMWDEFS_NULL)
    {
      I870LNK1_SESSION_INFO *pLinkSession = (I870LNK1_SESSION_INFO *)pSession->pLinkSession;

      /* Back to square one */
      pLinkSession->sesnState = I870LNK1_SESSION_STATE_STARTUP;
      pLinkSession->secondaryNotReset = TMWDEFS_TRUE;

      pLinkContext->tmw.pInfoFunc(
        pLinkContext->tmw.pCallbackParam, 
        pSession, TMWSCL_INFO_OFFLINE);
    }

    /* Send channel closed to application layer */
    pLinkContext->tmw.pInfoFunc(
      pLinkContext->tmw.pCallbackParam, TMWDEFS_NULL, sesnInfo);

    break;

  default:
    /* Something other than channel opened or closed, 
     * pass it on to application layer 
     */
    pLinkContext->tmw.pInfoFunc(
      pLinkContext->tmw.pCallbackParam, TMWDEFS_NULL, sesnInfo);

    break;
  }
}

/* function: _computeChecksum
 * purpose:
 * arguments:
 * returns:
 *  void
 */
static TMWTYPES_UCHAR TMWDEFS_LOCAL _computeChecksum(
  TMWTYPES_UCHAR *pBuffer,
  int length)
{
  TMWTYPES_UCHAR checksum = 0;

  while(length--)
    checksum = (TMWTYPES_UCHAR)(checksum + pBuffer[length]);

  return(checksum);
}

/* function: _getMasterOrSlaveSession
 * purpose:
 * arguments:
 * returns:
 *  void
 */
static TMWTYPES_BOOL TMWDEFS_LOCAL _getMasterOrSlaveSession(
  TMWSESN *pSession, 
  I870LNK1_CONTEXT *pContext)
{
  if(pContext->linkMode == TMWDEFS_LINK_MODE_UNBALANCED)
  { 
    /* For Unbalanced mode, bit 0x80 is RES not DIR 
     * PRM indicates primary or master transmitted 
     */
    if(pContext->rxControl & I870FT12_CONTROL_PRM)
    {
      if(pSession->type == TMWTYPES_SESSION_TYPE_SLAVE)
      {
        return TMWDEFS_TRUE;
      }
    }
    else if(pSession->type == TMWTYPES_SESSION_TYPE_MASTER)
    {
      return TMWDEFS_TRUE;
    }
  }
  else /* BALANCED */
  {
    /* For Balanced mode, bit 0x80 is DIR 
     * DIR indicates master transmitted 
     */
    if(pContext->rxControl & I870FT12_CONTROL_DIR)
    {
      if(pSession->type == TMWTYPES_SESSION_TYPE_SLAVE)
      {
        return TMWDEFS_TRUE;
      }
    }
    else if(pSession->type == TMWTYPES_SESSION_TYPE_MASTER)
    {
      return TMWDEFS_TRUE;
    }
  }
  return TMWDEFS_FALSE;
}

/* function: _findSession
 * purpose:
 * arguments:
 * returns:
 *  void
 */
static TMWSESN * TMWDEFS_LOCAL _findSession(
  I870LNK1_CONTEXT *pContext,
  TMWTYPES_USHORT linkAddress)
{
  TMWSESN *pSession = TMWDEFS_NULL;
  pContext->broadcast = TMWDEFS_FALSE;
  
  while((pSession = (TMWSESN *)tmwdlist_getAfter(
    &pContext->tmw.sessions, (TMWDLIST_MEMBER *)pSession)) != TMWDEFS_NULL)
  {  
    if(pContext->linkAddressSize == 0)
    {
      if(tmwdlist_size(&pContext->tmw.sessions) == 1)
      { 
        /* No link address and only one session */
        return((TMWSESN *)tmwdlist_getFirst(&pContext->tmw.sessions));
      }
      else
      {
        /* determine if for master or slave */
        if(_getMasterOrSlaveSession(pSession, pContext))
          return pSession;
      }
    }

    /* If the address matches, make sure master/slave is correct
     * this allows peer or dual-mode with both a master and slave session with same link address
     */
    if(pSession->linkAddress == linkAddress)
    {
      if(tmwdlist_size(&pContext->tmw.sessions) == 1)
      { 
        /* Only one session */
        return(pSession);
      }

      /* determine if for master or slave */
      if(_getMasterOrSlaveSession(pSession, pContext))
         return pSession;
    }
      
    /* Broadcast must be for slave session */
    if(((pContext->linkAddressSize == 1)
      && (linkAddress == I870FT12_BROADCAST_ADDRESS_8))
      || (linkAddress == I870FT12_BROADCAST_ADDRESS_16))
    {
      pContext->broadcast = TMWDEFS_TRUE;
    }

    if((pContext->broadcast)
      && (pSession->type == TMWTYPES_SESSION_TYPE_SLAVE))
    {
      return(pSession);
    }
  }

  return(TMWDEFS_NULL);
}

#ifdef TMW_SUPPORT_MONITOR 
static TMWSESN * TMWDEFS_LOCAL _monitorModeFindSession(
  I870LNK1_CONTEXT *pContext,
  TMWTYPES_USHORT linkAddress)
{
  TMWSESN *pSession;
  TMWCHNL_STAT_UNKNOWN_SESN_TYPE data;
  
  int loopCount = 0;
  while(loopCount++ < 2)
  { 
    pSession = _findSession(pContext, linkAddress);

    if(pSession != TMWDEFS_NULL)
      return pSession; 

    if(loopCount < 2)
    {
      /* Session was not found, call statistics function 
       * and then loop again to see if a session was opened 
      */
      data.linkAddress = linkAddress;
      /* Initialize to master, and set it to slave if required */ 
      data.sessionType = TMWTYPES_SESSION_TYPE_MASTER;
      if((pContext->linkMode == TMWDEFS_LINK_MODE_UNBALANCED)
        &&(pContext->rxControl & I870FT12_CONTROL_PRM))
      {
        data.sessionType = TMWTYPES_SESSION_TYPE_SLAVE;
      }
      else if((pContext->linkMode == TMWDEFS_LINK_MODE_BALANCED)
        &&(pContext->rxControl & I870FT12_CONTROL_DIR))
      {
        data.sessionType = TMWTYPES_SESSION_TYPE_SLAVE;
      }

      TMWCHNL_STAT_CALLBACK_FUNC(
        pContext->tmw.pChannel,
        TMWCHNL_STAT_UNKNOWN_SESSION, &data);
    }
    else
    {
      /* We already called TMWCHNL_STAT_CALLBACK_FUNC(), 
       * since there is still no session return failure
       */
      return(TMWDEFS_NULL);
    }
  }

  return(TMWDEFS_NULL);
}
#endif


/* function: _findNextSlaveSession
 * purpose: find next slave session on list of sessions
 * arguments:
 * returns:
 *  void
 */
static TMWSESN * TMWDEFS_LOCAL _findNextSlaveSession(
  I870LNK1_CONTEXT *pContext,
  TMWSESN *pSession)
{
  /* Find next slave session */
  while((pSession = (TMWSESN *)tmwdlist_getAfter(
    &pContext->tmw.sessions, (TMWDLIST_MEMBER *)pSession)) != TMWDEFS_NULL)
  {
    if(pSession->type == TMWTYPES_SESSION_TYPE_SLAVE)
    {
      return(pSession);
    }
  }
  return(TMWDEFS_NULL);
}

static TMWSESN * TMWDEFS_LOCAL _getNextSessionToPoll(
  TMWSESN *pOldSession)
{
  I870LNK1_SESSION_INFO *pLinkSession;
  I870LNK1_CONTEXT      *pLinkContext = (I870LNK1_CONTEXT *)pOldSession->pChannel->pLinkContext;
  TMWSESN               *pSession = pOldSession;
 
  do
  {
    pSession = (TMWSESN *)tmwdlist_getAfter(
      &pLinkContext->tmw.sessions, (TMWDLIST_MEMBER *)pSession);

    if(pSession == TMWDEFS_NULL)
      pSession = (TMWSESN *)tmwdlist_getFirst(&pLinkContext->tmw.sessions);

    if(pSession->type == TMWTYPES_SESSION_TYPE_MASTER)
    {
      pLinkSession = (I870LNK1_SESSION_INFO *)pSession->pLinkSession;

      if(pLinkSession->sesnState != I870LNK1_SESSION_STATE_READY)
        continue;

      /* If pollTimer is not running, we can send a poll on this session
       * The session to be returned must also be active      
       */
      if(!tmwtimer_isActive(&pLinkSession->pollTimer) && pSession->active)
        return(pSession);
    }
  } while(pSession != pOldSession);

  return(pSession);
}

/* function: _pollSession
 * purpose:
 * arguments:
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _pollSession(
  void *pCallbackParam)
{
  TMWTYPES_UCHAR control = 0;
  TMWSESN *pSession = (TMWSESN *)pCallbackParam;
  I870LNK1_SESSION_INFO *pLinkSession = (I870LNK1_SESSION_INFO *)pSession->pLinkSession;
  I870LNK1_CONTEXT *pLinkContext = (I870LNK1_CONTEXT *)pSession->pChannel->pLinkContext;

  /* If user has turned automatic sending of class 1 and class 2 polls off, just return */
  if(pLinkSession->autoClassPolling == TMWDEFS_FALSE)
    return;

  /* See if channel is busy */
  if(pLinkContext->pCurrentSession == TMWDEFS_NULL)
  {
    /* Nope, poll this session */
    TMWTYPES_BOOL pendingRequest;
    while(pSession != TMWDEFS_NULL)
    {
      I870LNK1_SESSION_INFO *pLinkSession = (I870LNK1_SESSION_INFO *)pSession->pLinkSession;
      
      pendingRequest = pLinkSession->isRequestPending(pSession);
      if(pendingRequest == TMWDEFS_TRUE)
      {
        if(pLinkContext->pLastSessionPolled != pSession) 
        {
          pLinkContext->numberOfPollsSent = 0;
        }
        else if(pLinkContext->numberOfPollsSent > pLinkSession->classPendingCount)
        {
          pSession = _getNextSessionToPoll(pSession);
          pLinkContext->pLastSessionPolled = TMWDEFS_NULL;
          pLinkContext->numberOfPollsSent = 0;
          continue;
        }

        if(pLinkSession->class1DataAvailable)
        {
          control = I870FT12_CONTROL_PRM | I870FT12_FC_REQUEST_DATA_C1;
          pLinkSession->class1DataAvailable = TMWDEFS_FALSE;
          break;
        }
        else
        {
          control = I870FT12_CONTROL_PRM | I870FT12_FC_REQUEST_DATA_C2;
          break;
        }
      }
      else /* Request is not pending */
      { 
        if(pLinkSession->class1DataAvailable)
        {
          pLinkSession->class1DataAvailable = TMWDEFS_FALSE;
          control = I870FT12_CONTROL_PRM | I870FT12_FC_REQUEST_DATA_C1;
          if(pLinkContext->pLastSessionPolled != pSession) 
          {
            pLinkContext->numberOfPollsSent = 0;
          }
          else if(pLinkContext->numberOfPollsSent > pLinkSession->class1PollCount)
          {
            pSession = _getNextSessionToPoll(pSession);
            pLinkContext->pLastSessionPolled = TMWDEFS_NULL;
            pLinkContext->numberOfPollsSent = 0;
            continue;
          } 

          break;
        }
        else /* No class 1 data available */
        {
          pLinkContext->numberOfPollsSent = 0;
          if(pLinkContext->pLastSessionPolled == pSession) 
          {
            pSession = _getNextSessionToPoll(pSession);
            pLinkContext->pLastSessionPolled = TMWDEFS_NULL;
            continue;
          }
          control = I870FT12_CONTROL_PRM | I870FT12_FC_REQUEST_DATA_C2;
          break;
        }
      }
      
    }
    if(pSession != TMWDEFS_NULL)
    {
      _setLastSessionPolled(pLinkContext, pSession); 

      pLinkContext->numberOfPollsSent++;
      _sendFixedFrame(pLinkContext, pSession, control);
    }
  }
}

/* If this session has a request pending, see if we need to send it a poll */  
static TMWTYPES_BOOL TMWDEFS_LOCAL _pollIfPendingSession(
  TMWSESN *pSession)
{
  I870LNK1_SESSION_INFO *pLinkSession = (I870LNK1_SESSION_INFO *)pSession->pLinkSession;
  if((pLinkSession->sesnState == I870LNK1_SESSION_STATE_READY)
    &&(pLinkSession->isRequestPending(pSession)))
  {
    /* If poll delay timer is not running */
    if(tmwtimer_isActive(&pLinkSession->pollTimer) == TMWDEFS_FALSE)
    { 
      TMWTYPES_UCHAR control = 0;
      I870LNK1_CONTEXT *pLinkContext = (I870LNK1_CONTEXT *)pSession->pChannel->pLinkContext;

       /* If user has turned automatic sending of class 1 and class 2 polls off, just return */
      if(pLinkSession->autoClassPolling == TMWDEFS_FALSE)
        return TMWDEFS_FALSE;
   
      if(pLinkContext->pLastSessionPolled != pSession) 
      {
        pLinkContext->numberOfPollsSent = 0;
        _setLastSessionPolled(pLinkContext, pSession);  
      }

      if(pLinkContext->numberOfPollsSent <= pLinkSession->classPendingCount)
      { 
        if(pLinkSession->class1DataAvailable)
        {
          control = I870FT12_CONTROL_PRM | I870FT12_FC_REQUEST_DATA_C1;
          pLinkSession->class1DataAvailable = TMWDEFS_FALSE;
        }
        /* don't keep sending class 2 polls just because of a pending request */
        else if(pLinkContext->numberOfPollsSent < 1)
        {
          control = I870FT12_CONTROL_PRM | I870FT12_FC_REQUEST_DATA_C2;
        }
        else
        {
          return TMWDEFS_FALSE;
        }

        pLinkContext->numberOfPollsSent++;
        _sendFixedFrame(pLinkContext, pSession, control);
        return TMWDEFS_TRUE;
      }
    }
  }
  return TMWDEFS_FALSE;
}


static void TMWDEFS_LOCAL _tryToSend(
  TMWLINK_CONTEXT *pContext,
  TMWSESN *pRxSession)
{
  I870LNK1_CONTEXT *pLinkContext = (I870LNK1_CONTEXT *)pContext;
  I870LNK1_SESSION_INFO *pLinkSession;
  TMWSESN *pSession;
   
  if(!pLinkContext->tmw.isOpen)
    return;

  if(pLinkContext->needFixedFrameXmt)
  {
    pSession = TMWDEFS_NULL; 
    while((pSession = (TMWSESN *)tmwdlist_getAfter(
      &pContext->sessions, (TMWDLIST_MEMBER *)pSession)) != TMWDEFS_NULL)
    {
      TMWTYPES_UCHAR control;
      pLinkSession = (I870LNK1_SESSION_INFO *)pSession->pLinkSession;
      if(pLinkSession->fixedControlNeeded != 0xff)
      {
        control = pLinkSession->fixedControlNeeded;
        pLinkSession->fixedControlNeeded = 0xff;
        _sendFixedFrame(pLinkContext, pSession, control); 
        return;
      }
    }

    /* Clear this only if no sessions were found that needed to send fixed frame */
    pLinkContext->needFixedFrameXmt = TMWDEFS_FALSE;
  }

  if((pLinkContext->pCurrentSession == TMWDEFS_NULL)
    &&(!pLinkContext->transmitInProgress))
  {

    /* First see if any session has an application layer packet to send */
    if(pLinkContext->tmw.pCheckClassFunc != TMWDEFS_NULL)
    {
      pSession = pRxSession;
      do
      {
        /* Get next session */
        pSession = (TMWSESN *)tmwdlist_getAfter(
          &pLinkContext->tmw.sessions, (TMWDLIST_MEMBER *)pSession);

        if(pSession == TMWDEFS_NULL)
        {
          if(pRxSession == TMWDEFS_NULL)
            break;
          pSession = (TMWSESN *)tmwdlist_getFirst(&pLinkContext->tmw.sessions);
        }

        if((pSession->type == TMWTYPES_SESSION_TYPE_MASTER )|| 
          (pLinkContext->linkMode == TMWDEFS_LINK_MODE_BALANCED))
        {

          /* Make sure session is ready */
          pLinkSession = (I870LNK1_SESSION_INFO *)pSession->pLinkSession;
          if((pLinkSession->sesnState != I870LNK1_SESSION_STATE_READY) 
            || pLinkSession->DFC1Received)
            continue;

          /* Check for application layer frame */
          if(pLinkContext->tmw.pCheckClassFunc(
            pLinkContext->tmw.pCallbackParam, 
            pSession, TMWDEFS_CLASS_MASK_ALL, TMWDEFS_TRUE))
          {
           return;
          }
        }
      } while(pSession != pRxSession);
    }

    /* For an unbalanced master, check if there is a request pending for this session that needs to be polled for */
    if(pRxSession != TMWDEFS_NULL)
    {
      if((pRxSession->type == TMWTYPES_SESSION_TYPE_MASTER )
        &&(pLinkContext->linkMode == TMWDEFS_LINK_MODE_UNBALANCED)
        &&(_pollIfPendingSession(pRxSession)))
      {
        return;
      }
    }

    /* Next see if any session is ready to poll or is still in startup state */
    pSession = pLinkContext->pLastSessionPolled; 
    do
    {
      /* Get next session */
      pSession = (TMWSESN *)tmwdlist_getAfter(
        &pLinkContext->tmw.sessions, (TMWDLIST_MEMBER *)pSession);

      if(pSession == TMWDEFS_NULL)
      { 
        if(pLinkContext->pLastSessionPolled == TMWDEFS_NULL)
          break;
        pSession = (TMWSESN *)tmwdlist_getFirst(&pLinkContext->tmw.sessions);
      }

      /* If session is not configured to be active, just skip it */
      if(!pSession->active)
        continue;

      /* Make sure session is ready */
      pLinkSession = (I870LNK1_SESSION_INFO *)pSession->pLinkSession; 
   
      if(pLinkSession->sesnState != I870LNK1_SESSION_STATE_READY)
      {
        if(((pLinkSession->sesnState == I870LNK1_SESSION_STATE_STARTUP)
          ||(pLinkSession->sesnState == I870LNK1_SESSION_STATE_REQUEST_STATUS))
          && (!tmwtimer_isActive(&pLinkSession->pollTimer)))
        { 
          /* For 103, send RESET_FCB */
          if((pSession->protocol == TMWTYPES_PROTOCOL_103)
            && (pSession->type == TMWTYPES_SESSION_TYPE_MASTER))
          {
            /* Waiting for reset response */
            pLinkSession->sesnState = I870LNK1_SESSION_STATE_RESET_PENDING;

            /* treat this as a poll, so we start after this session next time through
             * otherwise will start after the last polled and could get stuck on these offline sessions 
             */
            _setLastSessionPolled(pLinkContext, pSession); 

            if(pLinkSession->send103ResetFCB)
              _sendFixedFrame(pLinkContext, pSession, I870FT12_CONTROL_PRM | I870FT12_FC_LINK_RESET_FCB);
            else
              _sendFixedFrame(pLinkContext, pSession, I870FT12_CONTROL_PRM | I870FT12_FC_LINK_RESET_CU);

            /* Next transmitted FCB should be 1 */
            pLinkSession->txFrameCountBit = TMWDEFS_TRUE;
          }
          else if(((pSession->protocol != TMWTYPES_PROTOCOL_103)
            && (pSession->type == TMWTYPES_SESSION_TYPE_MASTER))
            || (pLinkContext->linkMode == TMWDEFS_LINK_MODE_BALANCED))
          {
            /* Waiting for status of link */
            pLinkSession->sesnState = I870LNK1_SESSION_STATE_REQUEST_STATUS;
 
            /* treat this as a poll, so we start after this session next time through
             * otherwise will start after the last polled and could get stuck on these offline sessions 
             */
            _setLastSessionPolled(pLinkContext, pSession); 
   
            _sendFixedFrame(pLinkContext, pSession, I870FT12_CONTROL_PRM | I870FT12_FC_REQUEST_STATUS); 
            /* Next transmitted FCB should be 1 */
            pLinkSession->txFrameCountBit = TMWDEFS_TRUE;
          }
          else if(pSession->type == TMWTYPES_SESSION_TYPE_SLAVE)
          {            
            /* else slaves waiting for reset request */
            continue;
          }
          return;
        }

        /* Skip this session for now. */
        continue;
      }  
      else if((pLinkSession->DFC1Received)
        &&(pLinkContext->linkMode == TMWDEFS_LINK_MODE_BALANCED))
      {    
        _sendFixedFrame(pLinkContext, pSession, I870FT12_CONTROL_PRM | I870FT12_FC_REQUEST_STATUS); 
        return;
      }
     
      /* Poll this session? */
      if((pSession->type == TMWTYPES_SESSION_TYPE_MASTER )&&
        (pLinkContext->linkMode == TMWDEFS_LINK_MODE_UNBALANCED))
      {
        /* See if a poll timer has expired */
        if(tmwtimer_isActive(&pLinkSession->pollTimer) == TMWDEFS_FALSE)
        {
          _pollSession(pSession);
          return;
        }
      }
    } while(pSession != pLinkContext->pLastSessionPolled);

  }
}

/* function: _offlineTimeout
 * purpose: Attempt to bring session back online
 * arguments:
 *  pCallbackParam - pointer to session
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _offlineTimeout(
  void *pCallbackParam)
{
  TMWSESN *pSession = (TMWSESN *)pCallbackParam;
  _tryToSend(pSession->pChannel->pLinkContext, pSession);
}

/* function: _balancedRetryTimeout
 * purpose: Ask the sessions if they have anything to send. Otherwise if a command 
 *  takes time it will not be sent immediately and since in unbalanced mode
 *  the master does not poll the slave, it needs an event to cause it to ask again.
 * arguments:
 *  pCallbackParam - pointer to link context
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _balancedRetryTimeout(
  void *pCallbackParam)
{
  I870LNK1_CONTEXT *pLinkContext = (I870LNK1_CONTEXT *)pCallbackParam;
  TMWSESN *pSession = (TMWSESN *)tmwdlist_getFirst(&pLinkContext->tmw.sessions);

  /* Make sure at least one session is still open. */
  if(pSession != TMWDEFS_NULL)
  {
    _tryToSend((TMWLINK_CONTEXT *)pLinkContext, pSession); 
  
    /* Restart offline poll timer */
    tmwtimer_start(&pLinkContext->balancedRetryTimer, 
      500, pLinkContext->tmw.pChannel, 
      _balancedRetryTimeout, pLinkContext);
  }
}

/* function: _sendTestFrame 
 * purpose:  Send a test frame to determine if the link is still up.
 * arguments:
 *  pCallbackParam - pointer to link context
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _sendTestFrame(
  void *pCallbackParam)
{
  I870LNK1_CONTEXT *pLinkContext = (I870LNK1_CONTEXT *)pCallbackParam;
  TMWSESN *pSession = (TMWSESN *)tmwdlist_getFirst(&pLinkContext->tmw.sessions);

  /* Make sure at least one session is still open. */
  if(pSession != TMWDEFS_NULL)
  {
    I870LNK1_SESSION_INFO *pLinkSession = (I870LNK1_SESSION_INFO *)pSession->pLinkSession;
    if(pLinkSession->sesnState == I870LNK1_SESSION_STATE_READY)
    {
      _sendFixedFrame(pLinkContext, pSession,
        (TMWTYPES_UCHAR)(I870FT12_CONTROL_PRM | I870FT12_FC_TEST_FUNCTION));
    }

    /* Check for >0 in case test frame timeout was turned off */
    if(pLinkContext->testFramePeriod >0)
    {
      /* Restart offline poll timer */
      tmwtimer_start(&pLinkContext->testFrameTimer, 
        pLinkContext->testFramePeriod, pLinkContext->tmw.pChannel, 
        _sendTestFrame, pLinkContext);
    }
  }
}

/* function: _primaryRetry
 * purpose: retry a primary frame
 * arguments:
 *  pCallbackParam - pointer to link context
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _primaryRetry(
  void *pCallbackParam)
{
  I870LNK1_CONTEXT *pLinkContext = (I870LNK1_CONTEXT *)pCallbackParam;
  TMWSESN *pSession = pLinkContext->pCurrentSession;
  I870LNK1_SESSION_INFO *pLinkSession;

  /* Make sure there is actually a current request pending */
  if(pSession == TMWDEFS_NULL)
  {
    return;
  }

  /* Get pointer to session specific link info */
  pLinkSession = (I870LNK1_SESSION_INFO *)pSession->pLinkSession;

  /* Don't retry on a closed channel */
  if(!pLinkContext->tmw.isOpen)
  {
    /* Cleanup this frame and reset session state */
    if(pLinkContext->pLinkTxDescriptor != TMWDEFS_NULL)
    {
      TMWSESN_TX_DATA *pOldTxDescriptor = pLinkContext->pLinkTxDescriptor;

      pLinkContext->pLinkTxDescriptor = TMWDEFS_NULL;

      /* Call failed transmission callback */
      if(pLinkContext->tmw.pFailedTxCallback)
      {
        pLinkContext->tmw.pFailedTxCallback(pOldTxDescriptor);
      }
    }

    pLinkContext->pCurrentSession = TMWDEFS_NULL;
    return;
  }

  I870STAT_CHNL_ERROR(pLinkContext->tmw.pChannel, pLinkContext->pCurrentSession, TMWCHNL_ERROR_LINK_CNFM_TIMEOUT);
 
  /* If auto polling is disabled, be tolerant of manual poll timing out */
  if((pLinkSession->autoClassPolling == TMWDEFS_FALSE)
	  && (pLinkSession->classPollSent))
  { 
    /* cleanup this frame and reset session state */
    pLinkContext->pCurrentSession = TMWDEFS_NULL;
	 
    /* Try to send next message to channel if needed */
    _tryToSend((TMWLINK_CONTEXT *)pLinkContext, pSession);

    _checkForIdleChannel(pLinkContext);
    return;
  }

  /* See if more retries are allowed */
  if((pLinkSession->sesnState == I870LNK1_SESSION_STATE_READY)
    && (pLinkContext->retryCount < pLinkContext->maxRetries))
  {
    /* Yep, send it again */
    pLinkContext->retryCount += 1;

    /* Diagnostics */
    I870DIA1_LINK_FRAME_SENT(pSession,
      pLinkContext->variablePrimaryTxDescriptor.pTxBuffer, 
      pLinkContext->variablePrimaryTxDescriptor.numBytesToTx,
      pLinkContext->retryCount);

    /* Send frame */
    pLinkContext->transmitInProgress = TMWDEFS_TRUE;
    pLinkContext->confirmExpected = TMWDEFS_TRUE;
    pLinkContext->tmw.pChannel->pPhys->pPhysTransmit(
      pLinkContext->tmw.pChannel->pPhysContext, &pLinkContext->variablePrimaryTxDescriptor);
  }
  else
  {
    /* Nope, cleanup this frame and reset session state */
    pLinkContext->pCurrentSession = TMWDEFS_NULL;
  
    if(pLinkContext->pLinkTxDescriptor != TMWDEFS_NULL)
    {
      TMWSESN_TX_DATA *pOldTxDescriptor = pLinkContext->pLinkTxDescriptor;

      pLinkContext->pLinkTxDescriptor = TMWDEFS_NULL;

      /* Call failed transmission callback */
      if(pLinkContext->tmw.pFailedTxCallback)
      {
        pLinkContext->tmw.pFailedTxCallback(pOldTxDescriptor);
      }
    }

    /* If session is currently online take it offline */
    if(pLinkSession->sesnState == I870LNK1_SESSION_STATE_READY)
    {
    
      /* Tell application layer we are offline */
      pLinkContext->tmw.pInfoFunc(
        pLinkContext->tmw.pCallbackParam, pSession, TMWSCL_INFO_OFFLINE);
    
      /* Stop polling this session */
      tmwtimer_cancel(&pLinkSession->pollTimer);
    }

    /* Reset session status */
    pLinkSession->sesnState = I870LNK1_SESSION_STATE_STARTUP;

    /* Let balanced slave try to send link status and link reset immediately,
     * it may be that master has restarted and needs the reset
     */
    if((pSession->type != TMWTYPES_SESSION_TYPE_SLAVE)
      || (pLinkContext->linkMode != TMWDEFS_LINK_MODE_BALANCED))
    {
      /* Start offline poll timer to delay more sends on this link. */
      tmwtimer_start(&pLinkSession->pollTimer, 
        pLinkContext->tmw.offlinePollPeriod, pSession->pChannel, 
        _offlineTimeout, pSession);
    }

    /* Try to send next message to channel if needed */
    _tryToSend((TMWLINK_CONTEXT *)pLinkContext, pSession);
    
    _checkForIdleChannel(pLinkContext);
  }
}

/* function: _primaryFixedRetry
 * purpose: retry a primary fixed length frame
 * arguments:
 *  pCallbackParam - pointer to link context
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _primaryFixedRetry(
  void *pCallbackParam)
{
  I870LNK1_CONTEXT *pLinkContext = (I870LNK1_CONTEXT *)pCallbackParam;
  TMWSESN *pSession = pLinkContext->pCurrentSession;
  TMWPHYS_TX_DESCRIPTOR *pTxDescriptor = &pLinkContext->variablePrimaryTxDescriptor;

  /* Make sure there is actually a current request pending */
  if(pSession == TMWDEFS_NULL)
  {
    return;
  }

  /* RESET_CU will not be retried for 101/2 protocol. Set retryCount to max to prevent it */
  if((pSession->protocol != TMWTYPES_PROTOCOL_103)
    && (pTxDescriptor->pTxBuffer[1]  & I870FT12_CONTROL_FC_MASK) == I870FT12_FC_LINK_RESET_CU)
  {
    pLinkContext->retryCount = pLinkContext->maxRetries;
  }

  if(pLinkContext->retryCount < pLinkContext->maxRetries)
  {  
    /* Send it again */
    pLinkContext->retryCount += 1;

    /* Diagnostics */
    I870DIA1_LINK_FRAME_SENT(pSession, pTxDescriptor->pTxBuffer, pTxDescriptor->numBytesToTx, pLinkContext->retryCount);

    pLinkContext->confirmExpected = TMWDEFS_TRUE;
    pLinkContext->transmitInProgress = TMWDEFS_TRUE;

    /* Send frame */
    pLinkContext->tmw.pChannel->pPhys->pPhysTransmit(
      pLinkContext->tmw.pChannel->pPhysContext, pTxDescriptor);
  }
  else
  { 
    I870LNK1_SESSION_INFO *pLinkSession = (I870LNK1_SESSION_INFO *)pSession->pLinkSession;

    /* Tell application layer we are offline */
    pLinkContext->tmw.pInfoFunc(
      pLinkContext->tmw.pCallbackParam, pSession, TMWSCL_INFO_OFFLINE);

    pLinkContext->pCurrentSession = TMWDEFS_NULL;

    /* Reset session status */
    pLinkSession->sesnState = I870LNK1_SESSION_STATE_STARTUP;

    /* Start offline poll timer */
    tmwtimer_start(&pLinkSession->pollTimer, 
      pLinkContext->tmw.offlinePollPeriod, pSession->pChannel, 
      _offlineTimeout, pSession);

    /* Try to send next message to channel if needed */
    _tryToSend((TMWLINK_CONTEXT *)pLinkContext, pSession);
    
    _checkForIdleChannel(pLinkContext);
  }
}

/* function: _rxFrameTimeout
 * purpose:
 * arguments:
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _rxFrameTimeout(
  void *pCallbackParam)
{
  I870LNK1_CONTEXT *pLinkContext = (I870LNK1_CONTEXT *)pCallbackParam;
  I870STAT_CHNL_ERROR(pLinkContext->tmw.pChannel, TMWDEFS_NULL, TMWCHNL_ERROR_LINK_FRAME_TIMEOUT);
  pLinkContext->rxState = RX_STATE_IDLE;
}

/* function: _beforeTxCallback
 * purpose:
 * arguments:
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _beforeTxCallback(
  void *pCallbackParam)
{
  I870LNK1_CONTEXT *pLinkContext = (I870LNK1_CONTEXT *)pCallbackParam;
  TMWTYPES_UCHAR *msgBuf = pLinkContext->variablePrimaryTxDescriptor.pTxBuffer;

  if((pLinkContext->pLinkTxDescriptor != TMWDEFS_NULL) 
    &&(pLinkContext->tmw.pBeforeTxCallback != TMWDEFS_NULL))
  {
    TMWTYPES_UCHAR frameLength;

    /* Call application layer callback */
    pLinkContext->tmw.pBeforeTxCallback(pLinkContext->pLinkTxDescriptor);

    /* Get frame length from message */
    frameLength = msgBuf[I870FT12_INDEX_VAR_LENGTH];

    /* Recalculate the checksum with all the last minute changes */
    msgBuf[I870FT12_INDEX_VAR_CONTROL + frameLength] =
    _computeChecksum(&msgBuf[I870FT12_INDEX_VAR_CONTROL], frameLength);
  }
}

/* function: _afterTxCallback
 * purpose:
 * arguments:
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _afterTxCallback(
  void *pCallbackParam)
{
  I870LNK1_CONTEXT *pLinkContext = (I870LNK1_CONTEXT *)pCallbackParam;

  pLinkContext->transmitInProgress = TMWDEFS_FALSE;

  /* Update statistics */
  TMWCHNL_STAT_CALLBACK_FUNC(
    pLinkContext->tmw.pChannel,
    TMWCHNL_STAT_FRAME_SENT, TMWDEFS_NULL);
  
   pLinkContext->expectEventConfirm = TMWDEFS_FALSE;

  /* Start link layer confirm timer */
  if(pLinkContext->confirmExpected)
  {
    tmwtimer_start(&pLinkContext->confirmTimer, 
      pLinkContext->confirmTimeout, pLinkContext->tmw.pChannel, 
     _primaryRetry, pLinkContext);
  }
  else 
  { 
    if((pLinkContext->linkMode == TMWDEFS_LINK_MODE_UNBALANCED)
      &&(pLinkContext->pLinkTxDescriptor != TMWDEFS_NULL)
      && (pLinkContext->pLinkTxDescriptor->txFlags & TMWSESN_TXFLAGS_CONTAINS_EVENTS))
    {
      pLinkContext->expectEventConfirm = TMWDEFS_TRUE;
    }
    else
    { 
      pLinkContext->channelIdle = TMWDEFS_TRUE;

      if(pLinkContext->pLinkTxDescriptor != TMWDEFS_NULL)
      {
        TMWSESN_TX_DATA *pOldTxDescriptor = pLinkContext->pLinkTxDescriptor;

        /* Done with this frame */
        pLinkContext->pLinkTxDescriptor = TMWDEFS_NULL;

        /* Call after transmission callback */
        if(pLinkContext->tmw.pAfterTxCallback != TMWDEFS_NULL)
        {
          /* Call application layer callback */
          pLinkContext->tmw.pAfterTxCallback(pOldTxDescriptor);
        }
      }
      
      /* If sending unconfirmed data, check to see if any more to send */
      _tryToSend((TMWLINK_CONTEXT *)pLinkContext, 
        (TMWSESN *)tmwdlist_getFirst(&pLinkContext->tmw.sessions));

      _checkForIdleChannel(pLinkContext);
    } 
  }
}

/* function: _failedTxCallback
 * purpose:
 * arguments:
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _failedTxCallback(
  void *pCallbackParam)
{
  I870LNK1_CONTEXT *pLinkContext = (I870LNK1_CONTEXT *)pCallbackParam;
  
  pLinkContext->transmitInProgress = TMWDEFS_FALSE;

  /* instead of retrying immediately since this would probably fail also,
   * if confirmed data, resend when timer expires,
   * if not tell layer above this failed.
   */
 
  /* Start link layer confirm timer */
  if(pLinkContext->confirmExpected)
  {
    tmwtimer_start(&pLinkContext->confirmTimer, 
      pLinkContext->confirmTimeout, pLinkContext->tmw.pChannel, 
      _primaryRetry, pLinkContext);
  }
  else 
  { 
    pLinkContext->channelIdle = TMWDEFS_TRUE;

    if(pLinkContext->pLinkTxDescriptor != TMWDEFS_NULL)
    {
      TMWSESN_TX_DATA *pOldTxDescriptor = pLinkContext->pLinkTxDescriptor;

      /* Done with this frame */
      pLinkContext->pLinkTxDescriptor = TMWDEFS_NULL;

      /* Call failed transmission callback */
      if(pLinkContext->tmw.pFailedTxCallback)
      {
        pLinkContext->tmw.pFailedTxCallback(pOldTxDescriptor);
      }
    }
      
    /* If sending unconfirmed data, check to see if any more to send */
    _tryToSend((TMWLINK_CONTEXT *)pLinkContext, 
      (TMWSESN *)tmwdlist_getFirst(&pLinkContext->tmw.sessions));

    _checkForIdleChannel(pLinkContext);
  }
}

/* function: _afterFixedTxCallback
 * purpose:
 * arguments:
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _afterFixedTxCallback(
  void *pCallbackParam)
{
  I870LNK1_CONTEXT *pLinkContext = (I870LNK1_CONTEXT *)pCallbackParam;

  pLinkContext->transmitInProgress = TMWDEFS_FALSE;

  /* Update statistics */
  TMWCHNL_STAT_CALLBACK_FUNC(
    pLinkContext->tmw.pChannel,
    TMWCHNL_STAT_FRAME_SENT, TMWDEFS_NULL);

  /* Start link layer confirm timer */
  if(pLinkContext->confirmExpected)
  {
    tmwtimer_start(&pLinkContext->confirmTimer, 
      pLinkContext->confirmTimeout, pLinkContext->tmw.pChannel, 
      _primaryFixedRetry, pLinkContext);
  }
  else 
  {
    pLinkContext->channelIdle = TMWDEFS_TRUE;

    /* If sending unconfirmed data, check to see if any more to send */
    _tryToSend((TMWLINK_CONTEXT *)pLinkContext, 
      (TMWSESN *)tmwdlist_getFirst(&pLinkContext->tmw.sessions));

    _checkForIdleChannel(pLinkContext);
  }
}

/* Calculate how long to wait before next poll on this session
 * based on whether a request is pending and if the ACD bit is set.
 * Start the pollTimer for this session for that amount of time.
 */
static void TMWDEFS_LOCAL _startPollTimer(
  TMWSESN *pSession)
{  
  TMWTYPES_MILLISECONDS pollTime;
  TMWTYPES_BOOL pendingRequest;
  I870LNK1_SESSION_INFO *pLinkSession = (I870LNK1_SESSION_INFO *)pSession->pLinkSession;
  
  /* If user has turned automatic class 1 and 2 polling off, just return */
  if(pLinkSession->autoClassPolling == TMWDEFS_FALSE)
    return;

  pendingRequest = pLinkSession->isRequestPending(pSession);

  if(pendingRequest == TMWDEFS_TRUE)
  {
    if(pLinkSession->class1DataAvailable)
    {
      pollTime = pLinkSession->class1PendingDelay;
    }
    else
    {
      pollTime = pLinkSession->class2PendingDelay;
    }
  }
  else
  {
    if(pLinkSession->class1DataAvailable)
    {
      pollTime = pLinkSession->class1PollDelay;
    }
    else
    {
      pollTime = pLinkSession->class2PollDelay;
    }
  }

  if(pollTime != 0)
  {
    /* Start poll timer */
    tmwtimer_start(&pLinkSession->pollTimer, 
      pollTime, pSession->pChannel, _pollSession, pSession);
  }
  else
  {
    /* Cancel the poll timer if it is currently running */
    if(tmwtimer_isActive(&pLinkSession->pollTimer))
      tmwtimer_cancel(&pLinkSession->pollTimer);
  }
}

/* function: _checkForUserLinkCommand
 * purpose:
 * arguments:
 * returns:
 *  void
 */
static TMWTYPES_BOOL TMWDEFS_LOCAL _checkForUserLinkCommand(
  I870LNK1_CONTEXT *pLinkContext,
  TMWSESN_TX_DATA *pTxDescriptor)
{
  I870LNK1_SESSION_INFO *pLinkSession = (I870LNK1_SESSION_INFO *)pTxDescriptor->pSession->pLinkSession;

  if(pTxDescriptor->txFlags & TMWSESN_TXFLAGS_LINK_COMMAND)
  {
    TMWSESN *pSession = pTxDescriptor->pSession;

    if((pTxDescriptor->pMsgBuf[0] == I870FT12_FC_LINK_RESET_CU)
      || (pTxDescriptor->pMsgBuf[0] == I870FT12_FC_RESET_USER))
    {
      /* Next transmitted frame count bit should be 1 */
      pLinkSession->txFrameCountBit = TMWDEFS_TRUE;
    }

    _sendFixedFrame(pLinkContext, pSession,
        (TMWTYPES_UCHAR)(I870FT12_CONTROL_PRM | pTxDescriptor->pMsgBuf[0]));

    if(pLinkContext->tmw.pAfterTxCallback != TMWDEFS_NULL)
    {
      /* Call application layer callback */
      pLinkContext->tmw.pAfterTxCallback(pTxDescriptor);
    }

    return(TMWDEFS_TRUE);
  }

  return(TMWDEFS_FALSE);
}

/* function: _sendFixedFrame
 * purpose:
 * arguments:
 * returns:
 *  void
 */
static void TMWDEFS_LOCAL _sendFixedFrame(
  I870LNK1_CONTEXT *pLinkContext,
  TMWSESN *pSession,
  TMWTYPES_UCHAR control)
{
  I870LNK1_SESSION_INFO *pLinkSession = (I870LNK1_SESSION_INFO *)pSession->pLinkSession;
  TMWPHYS_TX_DESCRIPTOR *pTxDescriptor;
  TMWTYPES_UCHAR offset = 0;
  TMWTYPES_UCHAR *msgPtr;
  TMWTYPES_BOOL setFCV;

#ifdef TMW_SUPPORT_MONITOR
  /* If this channel is in passive listen only analyzer mode
   * dont send any frames 
   */
  if(pSession->pChannel->pPhysContext->monitorMode)
  {
    return;
  }
#endif

  /* If the session is not active return */
  if(!pSession->active)
  {
    pLinkSession->sesnState = I870LNK1_SESSION_STATE_STARTUP;
    return;
  }	
 
  /* See if it is safe to transmit this fixed frame.
   * Is a transmit currently waiting to go out at the physical layer
   * Or is this a primary frame which would require pCurrentSession
   * that is already in use 
   */
  if((pLinkContext->transmitInProgress)
    ||((control & I870FT12_CONTROL_PRM) && pLinkContext->pCurrentSession != TMWDEFS_NULL))
  {
    pLinkContext->needFixedFrameXmt = TMWDEFS_TRUE;
    pLinkSession->fixedControlNeeded = control;
    return;
  }

  /* Initialize this to false, it will be set if poll is being sent */
  pLinkSession->classPollSent = TMWDEFS_FALSE;

  /* Init to false */
  setFCV = TMWDEFS_FALSE;

  /* All fixed length primary messages require some type of response */
  if(control & I870FT12_CONTROL_PRM)
  {
    TMWTYPES_UCHAR tempControl; 

    pTxDescriptor = &pLinkContext->variablePrimaryTxDescriptor;
    pLinkContext->confirmExpected = TMWDEFS_TRUE;
    pLinkContext->pCurrentSession = pSession;
    pLinkContext->retryCount = 0;

    /* Check primary frames to determine if FCV should be set */
    tempControl = (control & I870FT12_CONTROL_FC_MASK);
    if((tempControl == I870FT12_FC_REQUEST_DATA_C1)
      ||(tempControl == I870FT12_FC_REQUEST_DATA_C2))
    {
      setFCV = TMWDEFS_TRUE;
      pLinkSession->classPollSent = TMWDEFS_TRUE;
    }
    if(tempControl == I870FT12_FC_TEST_FUNCTION)
    {
      setFCV = TMWDEFS_TRUE;
    }
  }
  else
  {
    pLinkContext->pSecondaryTxDescriptor = &pLinkContext->fixedSecondaryTxDescriptor;
    pTxDescriptor = &pLinkContext->fixedSecondaryTxDescriptor;
    pLinkContext->confirmExpected = TMWDEFS_FALSE;
 
    /* Send flow control */
    if(pLinkSession->sendDFC1)
    {
      control |= I870FT12_CONTROL_DFC;
    }
  }

  /* Set ACD if we're an unbalanced slave and class 1 data is available */
  if((pSession->type == TMWTYPES_SESSION_TYPE_SLAVE)
    && (pLinkContext->linkMode == TMWDEFS_LINK_MODE_UNBALANCED))
  {
    if(pLinkContext->tmw.pCheckClassFunc(
      pLinkContext->tmw.pCallbackParam, 
      pSession, TMWDEFS_CLASS_MASK_ONE, TMWDEFS_FALSE))
    {
      control |= I870FT12_CONTROL_ACD;
    }
  }

  /* Get pointer to frame buffer */
  msgPtr = pTxDescriptor->pTxBuffer;

  /* determine if we can use single character ack or response  
   * ACK and no access demand or respond NACK and no access demand 
   */
  if(((control == I870FT12_FC_CONFIRM_ACK) && pLinkContext->oneCharAckAllowed) || 
    ((control == I870FT12_FC_RESPOND_NO_DATA) && pLinkContext->oneCharResponseAllowed))
  {
    /* single character */
    msgPtr[offset++] = I870FT12_SYNC_OCTET_SINGLE; 
  }
  else
  {
    /* Set DIR bit if a balanced master */
    if((pSession->type == TMWTYPES_SESSION_TYPE_MASTER)
      &&(pLinkContext->linkMode == TMWDEFS_LINK_MODE_BALANCED))
    {
      control |= I870FT12_CONTROL_DIR;
    }

    if(setFCV)
    {
      control |= I870FT12_CONTROL_FCV;

      /* Set frame count bit */
      if(pLinkSession->txFrameCountBit)
        control |= I870FT12_CONTROL_FCB;

      /* Toggle frame count bit */
      pLinkSession->txFrameCountBit =
        (TMWTYPES_BOOL)!pLinkSession->txFrameCountBit;
    }

    /* Start sync character */
    msgPtr[offset++] = I870FT12_SYNC_OCTET_FIXED;

    /* Control byte */
    msgPtr[offset++] = control;

    /* Link address */
    switch(pLinkContext->linkAddressSize)
    {
    case 1:
      msgPtr[offset++] = (TMWTYPES_UCHAR)pSession->linkAddress;
      break;

    case 2:
      tmwtarg_store16(&pSession->linkAddress, &msgPtr[offset]);
      offset += 2;
      break;
    }

    /* Checksum */
    msgPtr[offset++] = _computeChecksum(
      &pTxDescriptor->pTxBuffer[I870FT12_INDEX_FIX_CONTROL],
      pLinkContext->linkAddressSize + 1);

    /* End sync character */
    msgPtr[offset++] = I870FT12_SYNC_OCTET_END;
  }

  /* Set message length */
  pTxDescriptor->numBytesToTx = offset;

  /* Set callbacks */
  pTxDescriptor->pCallbackParam = pLinkContext;
  pTxDescriptor->beforeTxCallback = TMWDEFS_NULL;
  pTxDescriptor->afterTxCallback = _afterFixedTxCallback;
  pTxDescriptor->failedTxCallback = _afterFixedTxCallback;

  /* Diagnostics */
  I870DIA1_LINK_FRAME_SENT(pSession, pTxDescriptor->pTxBuffer, pTxDescriptor->numBytesToTx, 0);

  pLinkContext->channelIdle = TMWDEFS_FALSE;
  pLinkContext->transmitInProgress = TMWDEFS_TRUE;
  
  /* Send frame */
  pLinkContext->tmw.pChannel->pPhys->pPhysTransmit(
    pLinkContext->tmw.pChannel->pPhysContext, pTxDescriptor);
}

/* function: _sendVariableFrame
 * purpose:
 * arguments:
 * returns:
 *  void
 */
static void TMWDEFS_LOCAL _sendVariableFrame(
  I870LNK1_CONTEXT *pLinkContext)
{
  TMWSESN *pSession = pLinkContext->pLinkTxDescriptor->pSession;
  I870LNK1_SESSION_INFO *pLinkSession = (I870LNK1_SESSION_INFO *)pSession->pLinkSession;
  TMWTYPES_UCHAR *msgPtr = pLinkContext->variablePrimaryTxDescriptor.pTxBuffer;
  TMWTYPES_UCHAR frameLength;

  /* If the session is not active simply return */
  if(!pLinkContext->pLinkTxDescriptor->pSession->active)
  {
    /* Make a copy of the TxDescriptor since the callback may want to transmit
     * another frame
     */
    TMWSESN_TX_DATA *pOldTxDescriptor = pLinkContext->pLinkTxDescriptor;
    pLinkContext->pLinkTxDescriptor = TMWDEFS_NULL;

    /* Call failed callback if specified */
    if(pLinkContext->tmw.pFailedTxCallback)
    {
      pLinkContext->tmw.pFailedTxCallback(pOldTxDescriptor);
    }

    pLinkSession->sesnState = I870LNK1_SESSION_STATE_STARTUP;
    return;
  }

  /* Calculate actual frame length
   * control byte + link address + asdu length
   */
  frameLength = (TMWTYPES_UCHAR)
    (1 + pLinkContext->linkAddressSize
    + pLinkContext->pLinkTxDescriptor->msgLength);

  /* Store message header */
  *msgPtr++ = I870FT12_SYNC_OCTET_VARIABLE;
  *msgPtr++ = frameLength;
  *msgPtr++ = frameLength;
  *msgPtr++ = I870FT12_SYNC_OCTET_VARIABLE;

  /* Store control byte based on session parameters */
  if((pSession->type == TMWTYPES_SESSION_TYPE_MASTER) ||
    (pLinkContext->linkMode == TMWDEFS_LINK_MODE_BALANCED))
  {
    /* Primary Frame */
    TMWTYPES_UCHAR control = 0;

    if((pLinkContext->confirmMode == TMWDEFS_LINKCNFM_NEVER)
      || (pLinkContext->pLinkTxDescriptor->destAddress == I870FT12_BROADCAST_ADDRESS_16))
    {
      pLinkContext->confirmExpected = TMWDEFS_FALSE;
      control = I870FT12_FC_UNCONFIRMED_DATA | I870FT12_CONTROL_PRM;
    }
    else
    {
      pLinkContext->retryCount = 0;
      pLinkContext->pCurrentSession = pSession;
      pLinkContext->confirmExpected = TMWDEFS_TRUE;
      control = I870FT12_FC_USER_DATA | I870FT12_CONTROL_PRM | I870FT12_CONTROL_FCV;

      if(pLinkSession->txFrameCountBit)
        control |= I870FT12_CONTROL_FCB;

      /* Toggle frame count bit */
      pLinkSession->txFrameCountBit =
        (TMWTYPES_BOOL)!pLinkSession->txFrameCountBit;
    }

    /* Set DIR bit if a balanced mode master */
    if((pSession->type == TMWTYPES_SESSION_TYPE_MASTER)
      && (pLinkContext->linkMode == TMWDEFS_LINK_MODE_BALANCED))
    {
      control |= I870FT12_CONTROL_DIR;
    }

    *msgPtr++ = control;
    
    /* Store link address */
    switch(pLinkContext->linkAddressSize)
    {
    case 1:
      *msgPtr++ = (TMWTYPES_UCHAR)pLinkContext->pLinkTxDescriptor->destAddress;
      break;

    case 2:
      tmwtarg_store16(&pLinkContext->pLinkTxDescriptor->destAddress, msgPtr);
      msgPtr += 2;
      break;
    }
  }
  else
  {
    TMWTYPES_UCHAR control = I870FT12_FC_RESPOND_USER_DATA;

    /* Send flow control */
    if(pLinkSession->sendDFC1)
    {
      control |= I870FT12_CONTROL_DFC;
    }

    /* Secondary Frame */
    pLinkContext->pSecondaryTxDescriptor = &pLinkContext->variablePrimaryTxDescriptor;

    /* No response expected */
    pLinkContext->confirmExpected = TMWDEFS_FALSE;

    /* Set ACD bit if class 1 data is available  */
    if(pLinkContext->tmw.pCheckClassFunc(
      pLinkContext->tmw.pCallbackParam, 
      pSession, TMWDEFS_CLASS_MASK_ONE, TMWDEFS_FALSE))
    {
      control |= I870FT12_CONTROL_ACD;
    }

    *msgPtr++ = control;
  
    /* Store link address */
    switch(pLinkContext->linkAddressSize)
    {
    case 1:
      *msgPtr++ = (TMWTYPES_UCHAR)pSession->linkAddress;
      break;

    case 2:
      tmwtarg_store16(&pSession->linkAddress, msgPtr);
      msgPtr += 2;
      break;
    }
  }

  /* Store user data */
  pLinkContext->pTxStartASDU = msgPtr;

  memcpy(msgPtr,
    pLinkContext->pLinkTxDescriptor->pMsgBuf,
    pLinkContext->pLinkTxDescriptor->msgLength);

  msgPtr += pLinkContext->pLinkTxDescriptor->msgLength;

  /* Calculate and store checksum */
  *msgPtr++ = _computeChecksum(
    &pLinkContext->variablePrimaryTxDescriptor.pTxBuffer[I870FT12_INDEX_VAR_CONTROL],
    frameLength);

  /* End sync character */
  *msgPtr++ = I870FT12_SYNC_OCTET_END;

  /* Actual message length */
  pLinkContext->variablePrimaryTxDescriptor.numBytesToTx =
    (TMWTYPES_USHORT)(frameLength + I870FT12_NUM_NOT_IN_LENGTH);

  /* Callbacks */
  pLinkContext->variablePrimaryTxDescriptor.pCallbackParam = pLinkContext;
  pLinkContext->variablePrimaryTxDescriptor.beforeTxCallback = _beforeTxCallback;
  pLinkContext->variablePrimaryTxDescriptor.afterTxCallback = _afterTxCallback;
  pLinkContext->variablePrimaryTxDescriptor.failedTxCallback = _failedTxCallback;

  /* Diagnostics */
  I870DIA1_LINK_FRAME_SENT(pSession,
    pLinkContext->variablePrimaryTxDescriptor.pTxBuffer,
    pLinkContext->variablePrimaryTxDescriptor.numBytesToTx, 0);

  pLinkContext->channelIdle = TMWDEFS_FALSE;
  pLinkContext->transmitInProgress = TMWDEFS_TRUE;

  /* Transmit frame */
  pLinkContext->tmw.pChannel->pPhys->pPhysTransmit(
    pLinkContext->tmw.pChannel->pPhysContext, &pLinkContext->variablePrimaryTxDescriptor);
}

/* function: _updateMsg
 * purpose:
 * arguments:
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _updateMsg(
  TMWLINK_CONTEXT *pContext,
  TMWTYPES_USHORT offset,
  TMWTYPES_UCHAR *pData,
  TMWTYPES_USHORT length)
{
  I870LNK1_CONTEXT *pLinkContext = (I870LNK1_CONTEXT *)pContext;
  memcpy(pLinkContext->pTxStartASDU + offset, pData, length);
}

/* function: _checkDataReady
 * purpose:
 * arguments:
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _checkDataReady(
  void *pContext,
  TMWSESN *pSession)
{
  I870LNK1_CONTEXT *pLinkContext = (I870LNK1_CONTEXT *)pContext;
  if(pLinkContext->tmw.pCheckClassFunc == TMWDEFS_NULL)
    return;

  /* If we are not currently waiting for a response AND a
   * transmit is not in progress
   */
  if((pLinkContext->pCurrentSession == TMWDEFS_NULL)
    && !pLinkContext->transmitInProgress)
  {
    I870LNK1_SESSION_INFO *pLinkSession = (I870LNK1_SESSION_INFO *)pSession->pLinkSession;
    if((pLinkSession->sesnState == I870LNK1_SESSION_STATE_READY) && !pLinkSession->DFC1Received)
    {
      /* if this is a balanced mode master or slave
       * or unbalanced master
       * see if application has data to send
       */
      if((pLinkContext->linkMode == TMWDEFS_LINK_MODE_BALANCED)
        ||(pSession->type == TMWTYPES_SESSION_TYPE_MASTER))
      {
        pLinkContext->tmw.pCheckClassFunc(
          pLinkContext->tmw.pCallbackParam, pSession, TMWDEFS_CLASS_MASK_ALL, TMWDEFS_TRUE);
      }
      /* else */
      /* Unbalanced slaves will only send user data when they are polled by master */
    }
  }
}

/* function: _transmitFrame
 * purpose: transmit frame
 * arguments:
 *  pContext - link layer context returned from i870lnk1_initChannel
 *  pTxDescriptor - data to transmit
 * returns
 *  void
 */
static void TMWDEFS_CALLBACK _transmitFrame(
  TMWLINK_CONTEXT *pContext,
  TMWSESN_TX_DATA *pTxDescriptor)
{
  I870LNK1_CONTEXT *pLinkContext = (I870LNK1_CONTEXT *)pContext;

  /* Check for send link command from user */
  if(_checkForUserLinkCommand(pLinkContext, pTxDescriptor) == TMWDEFS_TRUE)
    return;
 
  /* If user has registered a callback function for testing call it */
  if(pLinkContext->pUserTxCallback != TMWDEFS_NULL)
  {
    pLinkContext->pUserTxCallback(pLinkContext->pCallbackParam, pTxDescriptor);
  }

  pLinkContext->pLinkTxDescriptor = pTxDescriptor;
  _sendVariableFrame(pLinkContext);
}

/* function: _cancelFrame
 * purpose: cancel frame
 * arguments:
 *  pContext - link layer context returned from i870lnk1_initChannel
 *  pTxDescriptor - data frame to cancel
 * returns
 *  void
 */
static void TMWDEFS_CALLBACK _cancelFrame(
  TMWLINK_CONTEXT *pContext,
  TMWSESN_TX_DATA *pTxDescriptor)
{
  TMWSESN *pSession;
  I870LNK1_CONTEXT *pLinkContext = (I870LNK1_CONTEXT *)pContext;

  if(pTxDescriptor == pLinkContext->pLinkTxDescriptor)
  {
    /* Set this to NULL to prevent retries and/or callbacks to application */
    pLinkContext->pLinkTxDescriptor = TMWDEFS_NULL;

    /* No longer waiting for a reply */
    pSession = pLinkContext->pCurrentSession;
    pLinkContext->pCurrentSession = TMWDEFS_NULL;
    pLinkContext->channelIdle = TMWDEFS_TRUE;
    pLinkContext->transmitInProgress = TMWDEFS_FALSE;
    tmwtimer_cancel(&pLinkContext->confirmTimer);

    /* Cancel at physical layer also */
    pContext->pChannel->pPhysContext->pTxDescriptor = TMWDEFS_NULL; 

    _tryToSend((TMWLINK_CONTEXT *)pLinkContext, pSession);
  }
}

/* function: _processFrame
 * purpose:
 * arguments:
 * returns:
 *  void
 */
static void TMWDEFS_LOCAL _processFrame(I870LNK1_CONTEXT *pLinkContext)
{
  I870LNK1_SESSION_INFO *pLinkSession;
  TMWSESN *pSession;
  TMWTYPES_BOOL startPollTimer = TMWDEFS_FALSE;

  /* Lookup session based on received link address */
#ifdef TMW_SUPPORT_MONITOR 
  if(pLinkContext->tmw.pChannel->pPhysContext->monitorMode)
  {    
    pSession =_monitorModeFindSession(pLinkContext, pLinkContext->rxAddress);
  }
  else
#endif
  pSession = _findSession(pLinkContext, pLinkContext->rxAddress);
  if(pSession == TMWDEFS_NULL)
  {
    I870STAT_CHNL_ERROR(pLinkContext->tmw.pChannel, TMWDEFS_NULL, TMWCHNL_ERROR_LINK_ADDRESS_UNKNOWN);
    I870DIA1_LINK_ADDRESS_UNKNOWN(pLinkContext->tmw.pChannel, pLinkContext->rxAddress);
    return;
  }
 
  /* Discard primary messages received for our master session. 
   * Message may be echoed back on a Fiber Optic ring that forwards all messages.
   */
  if((pSession->type == TMWTYPES_SESSION_TYPE_MASTER)
    && (pLinkContext->linkMode == TMWDEFS_LINK_MODE_UNBALANCED)
    && (pLinkContext->rxControl & I870FT12_CONTROL_PRM))
  {
    return;
  }
      
  /* Whenever we receive a frame from a session assume it is back online */
  I870DIAG_SESSION_ONLINE(pSession, TMWDEFS_TRUE);
  tmwsesn_setOnline(pSession, TMWDEFS_TRUE);

  /* Get pointer to protocol specific session info */
  pLinkSession = (I870LNK1_SESSION_INFO *)pSession->pLinkSession;

  /* Diagnostics */
  I870DIA1_LINK_FRAME_RECEIVED(pSession,
    pLinkContext->rxFrameBuffer, pLinkContext->rxCurrentLength);

  /* Update statistics */
  TMWCHNL_STAT_CALLBACK_FUNC(
    pLinkContext->tmw.pChannel,
    TMWCHNL_STAT_FRAME_RECEIVED, TMWDEFS_NULL);

  /* Validate DIR bit */
  if(pLinkContext->linkMode == TMWDEFS_LINK_MODE_BALANCED)
  {
    TMWTYPES_BOOL dirBit = (TMWTYPES_BOOL)
      ((pLinkContext->rxControl & I870FT12_CONTROL_DIR) != 0);

    if(((pSession->type == TMWTYPES_SESSION_TYPE_MASTER) && dirBit)
      || ((pSession->type == TMWTYPES_SESSION_TYPE_SLAVE) && !dirBit))
    {
      /* If length is 1 its a single char ack 
       * there is no control field and dirBit in that case 
       */
      if(pLinkContext->rxCurrentLength != 1)
      {
        I870STAT_CHNL_ERROR(pLinkContext->tmw.pChannel, pSession, TMWCHNL_ERROR_LINK_INV_DIR);
        I870DIA1_ERROR(pLinkContext->tmw.pChannel, pSession, I870DIA1_RCVD_INV_DIR);
        return;
      }
    }
  }

  /* See if this is a primary or secondary message */
  if(pLinkContext->rxControl & I870FT12_CONTROL_PRM)
  {
    /* Process primary messages */

    /* If we are BALANCED or an UNBALANCED Slave  
     * process only Reset(FC0) and Request status(FC9)
     * and Reset(FC7) for 103 
     */
    if(pLinkSession->secondaryNotReset)
    {
      TMWTYPES_UCHAR fc = (TMWTYPES_UCHAR) (pLinkContext->rxControl & I870FT12_CONTROL_FC_MASK);
      if((fc != I870FT12_FC_LINK_RESET_CU) 
        && (fc != I870FT12_FC_LINK_RESET_FCB) 
        && (fc != I870FT12_FC_REQUEST_STATUS))
      {
        /*
         * For 101, test case 5.4.17.1 of IEC60870-5-601 says "If the controlling station was 
         * re-initialised before the controlled station detected the lost link, the controlled 
         * station can start sending data immediately".
         */
        if(pLinkSession->balancedMasterRestart)
        {
          pLinkSession->balancedMasterRestart = TMWDEFS_FALSE;
          pLinkSession->secondaryNotReset = TMWDEFS_FALSE;
          /* Set expected FCB to allow this frame, otherwise it would be wrong 50% of the time. */
          pLinkSession->rxFrameCountBit = (TMWTYPES_BOOL)((pLinkContext->rxControl & I870FT12_CONTROL_FCB) != 0);
        }
        else
        {
          I870STAT_CHNL_ERROR(pLinkContext->tmw.pChannel, pSession, TMWCHNL_ERROR_LINK_NOT_RESET);

#ifdef TMW_SUPPORT_MONITOR
          if(!pLinkContext->tmw.pChannel->pPhysContext->monitorMode)
          {
            I870DIA1_ERROR(pSession->pChannel, pSession, I870DIA1_NOT_RESET);
            return;
          }
#else
          I870DIA1_ERROR(pSession->pChannel, pSession, I870DIA1_NOT_RESET);
          return;
#endif
        }
      }
    }

    /* See if Frame Count Valid is set in received message */
    if(pLinkContext->rxControl & I870FT12_CONTROL_FCV)
    {
      TMWTYPES_BOOL receivedFCB;

      /* Get the state of the received Frame Count Bit */
      receivedFCB = (TMWTYPES_BOOL)((pLinkContext->rxControl & I870FT12_CONTROL_FCB) != 0);

      /* See if FCB matches expected */
      if(receivedFCB != pLinkSession->rxFrameCountBit)
      {
        /* Nope, must have dropped a frame */
        I870STAT_CHNL_ERROR(pSession->pChannel, pSession, TMWCHNL_ERROR_LINK_FCB);
        I870DIA1_ERROR(pSession->pChannel, pSession, I870DIA1_INV_FCB);

#ifdef TMW_SUPPORT_MONITOR 
        if(pSession->pChannel->pPhysContext->monitorMode)
        {   
          /* Toggle expected frame count bit */
          pLinkSession->rxFrameCountBit =
           (TMWTYPES_BOOL)(!pLinkSession->rxFrameCountBit);
        }
#endif
        /* Resend the last secondary response */
        if(pLinkContext->pSecondaryTxDescriptor->numBytesToTx != 0)
        {
          if(!pLinkContext->transmitInProgress)
          {
            pLinkContext->transmitInProgress = TMWDEFS_TRUE;
            pLinkContext->tmw.pChannel->pPhys->pPhysTransmit(
              pLinkContext->tmw.pChannel->pPhysContext, pLinkContext->pSecondaryTxDescriptor);
          }
          return;
        }
      }    
      else
      {
        if(pLinkContext->expectEventConfirm)
        {
          pLinkContext->channelIdle = TMWDEFS_TRUE;

          if(pLinkContext->pLinkTxDescriptor != TMWDEFS_NULL)
          {
            TMWSESN_TX_DATA *pOldTxDescriptor = pLinkContext->pLinkTxDescriptor;

            /* Done with this frame */
            pLinkContext->pLinkTxDescriptor = TMWDEFS_NULL;

            /* Call after transmission callback */
            if(pLinkContext->tmw.pAfterTxCallback != TMWDEFS_NULL)
            {
              /* Call application layer callback */
              pLinkContext->tmw.pAfterTxCallback(pOldTxDescriptor);
            }
          } 
      
          /* If sending unconfirmed data, check to see if any more to send */
          _tryToSend((TMWLINK_CONTEXT *)pLinkContext, 
            (TMWSESN *)tmwdlist_getFirst(&pLinkContext->tmw.sessions));

          _checkForIdleChannel(pLinkContext);
          pLinkContext->expectEventConfirm = TMWDEFS_FALSE;
        }
      }

      /* Toggle expected frame count bit */
      pLinkSession->rxFrameCountBit =
        (TMWTYPES_BOOL)(!pLinkSession->rxFrameCountBit);
    }
   
    /* For unbalanced slaves we are online */
    if((pSession->type == TMWTYPES_SESSION_TYPE_SLAVE)
      && (pLinkContext->linkMode == TMWDEFS_LINK_MODE_UNBALANCED)
      && (pLinkSession->sesnState == I870LNK1_SESSION_STATE_STARTUP))
    {
      TMWTYPES_UCHAR fc = (TMWTYPES_UCHAR) (pLinkContext->rxControl & I870FT12_CONTROL_FC_MASK);

      if((fc == I870FT12_FC_LINK_RESET_FCB)
        || (fc == I870FT12_FC_LINK_RESET_CU))
      {
        pLinkContext->tmw.pInfoFunc(
          pLinkContext->tmw.pCallbackParam, pSession, TMWSCL_INFO_ONLINE);

        /* Start poll delay timer if required */
        if(pLinkSession->maxPollDelay != 0)
        {
          tmwtimer_start(
            &pLinkSession->pollTimer, pLinkSession->maxPollDelay, 
            pSession->pChannel, _pollDelayTimeout, pSession);
        }

        /* Session is now ready */
        pLinkSession->sesnState = I870LNK1_SESSION_STATE_READY;
      }
    }

    /* Process message based on function code */
    switch(pLinkContext->rxControl & I870FT12_CONTROL_FC_MASK)
    {
    case I870FT12_FC_LINK_RESET_CU:
      /* Clear this flag which allowed slave to not notice restart of balanced master.*/
      pLinkSession->balancedMasterRestart = TMWDEFS_FALSE;

      if(!pLinkSession->sendDFC1)
      {
         /* Reset Comm Unit, reply with Ack */
        _sendFixedFrame(pLinkContext, pSession, I870FT12_FC_CONFIRM_ACK);

        /* 101 spec says a pending secondary link layer message is deleted. */
        pLinkContext->pSecondaryTxDescriptor->numBytesToTx = 0;

        /* If waiting for event confirm, call the failed tx callback */
        if(pLinkContext->expectEventConfirm)
        {
          pLinkContext->channelIdle = TMWDEFS_TRUE;

          if(pLinkContext->pLinkTxDescriptor != TMWDEFS_NULL)
          {
            TMWSESN_TX_DATA *pOldTxDescriptor = pLinkContext->pLinkTxDescriptor;

            /* Done with this frame */
            pLinkContext->pLinkTxDescriptor = TMWDEFS_NULL;

            /* Call failed transmission callback */
            if(pLinkContext->tmw.pFailedTxCallback)
            {
              pLinkContext->tmw.pFailedTxCallback(pOldTxDescriptor);
            }
          } 
      
          pLinkContext->expectEventConfirm = TMWDEFS_FALSE;
        }
 
        /* Next received FCB will be 1 */
        pLinkSession->rxFrameCountBit = TMWDEFS_TRUE;
        pLinkSession->secondaryNotReset = TMWDEFS_FALSE;

        /* Send reset CU to application layer for processing */
        pLinkContext->tmw.pInfoFunc(pLinkContext->tmw.pCallbackParam, 
          pSession, TMWSCL_INFO_RESET_CU);
      }
      else
      {
        _sendFixedFrame(pLinkContext, pSession, I870FT12_FC_CONFIRM_NACK);
      }

      break;

    case I870FT12_FC_RESET_USER:
      /* Reset User, reply with Ack */
      _sendFixedFrame(pLinkContext, pSession, I870FT12_FC_CONFIRM_ACK);
 
      /* Next transmitted and received FCB will be 1 */
      pLinkSession->txFrameCountBit = TMWDEFS_TRUE;
      pLinkSession->rxFrameCountBit = TMWDEFS_TRUE;

      /* Send user reset to application layer for processing */
      pLinkContext->tmw.pInfoFunc(pLinkContext->tmw.pCallbackParam, 
        pSession, TMWSCL_INFO_RESET_USER);
      
      break;

    case I870FT12_FC_TEST_FUNCTION:
      /* Test Function */

      /* This function code only support in balanced mode */
      if(pLinkContext->linkMode == TMWDEFS_LINK_MODE_UNBALANCED)
      {
        I870STAT_CHNL_ERROR(pSession->pChannel, pSession, TMWCHNL_ERROR_LINK_ILLEGAL_FUNCTION);
        I870DIA1_ERROR(pSession->pChannel, pSession,  I870DIA1_TEST_BALANCED);
        _sendFixedFrame(pLinkContext, pSession, I870FT12_FC_NOT_USED);
        return;
      }

      /* Send acknowledge, if reset has been received */
      _sendFixedFrame(pLinkContext, pSession, I870FT12_FC_CONFIRM_ACK);

      break;

    case I870FT12_FC_USER_DATA:
      /* User Data Confirm Expected */

      /* verify this was received in a variable length frame for sanity */
      if (pLinkContext->rxState != RX_STATE_READ_VARIABLE)
        break;

      /* Send confirm */
      _sendFixedFrame(pLinkContext, pSession, I870FT12_FC_CONFIRM_ACK);

      /* Send user data to transport/application layer */
      if(pSession->active && (pLinkContext->tmw.pParseFunc != TMWDEFS_NULL))
      {
        pLinkContext->rxFrame.msgLength =
          (TMWTYPES_USHORT)(pLinkContext->rxCurrentLength -
          (I870FT12_NUM_NOT_IN_LENGTH + 1 + pLinkContext->linkAddressSize));

        pLinkContext->rxFrame.pMsgBuf =
          pLinkContext->rxFrameBuffer + 5 + pLinkContext->linkAddressSize;

        pLinkContext->tmw.pParseFunc(
          pLinkContext->tmw.pCallbackParam, pSession, &pLinkContext->rxFrame);
      }
      break;

    case I870FT12_FC_UNCONFIRMED_DATA:
      {
      /* User Data, No Confirm Expected */
      TMWSESN *pRxSession = pSession;

      /* verify this was received in a variable length frame for sanity */
      if (pLinkContext->rxState != RX_STATE_READ_VARIABLE)
        break;

      /* If this is to broadcast address, send message to all slave sessions */
      do 
      {
        /* Send user data to application layer */
        if(pSession->active && (pLinkContext->tmw.pParseFunc != TMWDEFS_NULL))
        {
          pLinkContext->rxFrame.msgLength =
            (TMWTYPES_USHORT)(pLinkContext->rxCurrentLength
            - (I870FT12_NUM_NOT_IN_LENGTH + 1 + pLinkContext->linkAddressSize));

          pLinkContext->rxFrame.pMsgBuf =
            pLinkContext->rxFrameBuffer + 5 + pLinkContext->linkAddressSize;

          pLinkContext->tmw.pParseFunc(
            pLinkContext->tmw.pCallbackParam, pSession, &pLinkContext->rxFrame);


          /* if not broadcast, exit from loop */
          if(!pLinkContext->broadcast)
            break;
        }
      }
      while((pSession = _findNextSlaveSession(pLinkContext, pSession))!= TMWDEFS_NULL);

      /* Restore the session pointer */
      pSession = pRxSession;
      break;
      }
    case  I870FT12_FC_LINK_RESET_FCB:
      _sendFixedFrame(pLinkContext, pSession, I870FT12_FC_CONFIRM_ACK);

      /* Next received FCB will be 1 */
      pLinkSession->rxFrameCountBit = TMWDEFS_TRUE;
      pLinkSession->secondaryNotReset = TMWDEFS_FALSE;

      pLinkContext->tmw.pInfoFunc(pLinkContext->tmw.pCallbackParam, 
        pSession, TMWSCL_INFO_RESET_FCB);
     
      break;

    case I870FT12_FC_REQUEST_ACCESS:
      /* Request Access Demand */

      /* This function code only support in unbalanced mode */
      if(pLinkContext->linkMode == TMWDEFS_LINK_MODE_BALANCED)
      {
        I870STAT_CHNL_ERROR(pSession->pChannel, pSession, TMWCHNL_ERROR_LINK_ILLEGAL_FUNCTION);
        I870DIA1_ERROR(pSession->pChannel, pSession, I870DIA1_ACD_BALANCED);
        _sendFixedFrame(pLinkContext, pSession, I870FT12_FC_NOT_USED);
        return;
      }

      /* Otherwise treat the same as a status request, reply with status */
      _sendFixedFrame(pLinkContext, pSession, I870FT12_FC_RESPOND_STATUS);
      break;

    case I870FT12_FC_REQUEST_STATUS:
      /* Status request, reply with status */
      _sendFixedFrame(pLinkContext, pSession, I870FT12_FC_RESPOND_STATUS);
      break;

    case I870FT12_FC_REQUEST_DATA_C1:
      /* Request for class 1 data, reply with data if available */
      {
        TMWTYPES_BOOL dataSent = TMWDEFS_FALSE;

        /* This function code only support in unbalanced mode */
        if(pLinkContext->linkMode == TMWDEFS_LINK_MODE_BALANCED)
        {
          I870STAT_CHNL_ERROR(pSession->pChannel, pSession, TMWCHNL_ERROR_LINK_ILLEGAL_FUNCTION);
          I870DIA1_ERROR(pSession->pChannel, pSession, I870DIA1_USER_DATA_BALANCED);
          _sendFixedFrame(pLinkContext, pSession, I870FT12_FC_NOT_USED);
          return;
        }

        /* Cancel poll delay timer since we got a request */
        tmwtimer_cancel(&pLinkSession->pollTimer);

        /* See if data is available */
        if(pLinkContext->tmw.pCheckClassFunc != TMWDEFS_NULL)
        {
          dataSent = pLinkContext->tmw.pCheckClassFunc(
            pLinkContext->tmw.pCallbackParam, pSession, TMWDEFS_CLASS_MASK_ONE, TMWDEFS_TRUE);
        }

        /* If no data sent reply with NO DATA */
        if(!dataSent)
          _sendFixedFrame(pLinkContext, pSession, I870FT12_FC_RESPOND_NO_DATA);

        /* Restart poll delay timer if required */
        if(pLinkSession->maxPollDelay != 0)
        {
          tmwtimer_start(
            &pLinkSession->pollTimer, pLinkSession->maxPollDelay, 
            pSession->pChannel, _pollDelayTimeout, pSession);
        }
      }

      break;

    case I870FT12_FC_REQUEST_DATA_C2:
      /* Request for class 2 data, reply with data if available */
      {
        TMWTYPES_BOOL dataSent = TMWDEFS_FALSE;

        /* This function code only support in unbalanced mode */
        if(pLinkContext->linkMode == TMWDEFS_LINK_MODE_BALANCED)
        {
          I870STAT_CHNL_ERROR(pSession->pChannel, pSession, TMWCHNL_ERROR_LINK_ILLEGAL_FUNCTION);
          I870DIA1_ERROR(pSession->pChannel, pSession, I870DIA1_USER_DATA_BALANCED);
          _sendFixedFrame(pLinkContext, pSession, I870FT12_FC_NOT_USED);
          return;
        }

        /* Cancel poll delay timer since we got a request */
        tmwtimer_cancel(&pLinkSession->pollTimer);

        /* See if data is available */
        if(pLinkContext->tmw.pCheckClassFunc != TMWDEFS_NULL)
        {
          dataSent = pLinkContext->tmw.pCheckClassFunc(
            pLinkContext->tmw.pCallbackParam, pSession, TMWDEFS_CLASS_MASK_TWO, TMWDEFS_TRUE);

          /* Addendum 2 of IEC 870-5-101 allows class 1 response when no class 2
           * data available
           */
          if(!dataSent)
          {
            dataSent = pLinkContext->tmw.pCheckClassFunc(
              pLinkContext->tmw.pCallbackParam, pSession, TMWDEFS_CLASS_MASK_ONE, TMWDEFS_TRUE);
          }
        }

        /* If no data sent reply with NO DATA */
        if(!dataSent)
		      _sendFixedFrame(pLinkContext, pSession, I870FT12_FC_RESPOND_NO_DATA);

        /* Restart poll delay timer if required */
        if(pLinkSession->maxPollDelay != 0)
        {
          tmwtimer_start(
            &pLinkSession->pollTimer, pLinkSession->maxPollDelay, 
            pSession->pChannel, _pollDelayTimeout, pSession);
        }
      }

      break;

    default:
      I870STAT_CHNL_ERROR(pSession->pChannel, pSession, TMWCHNL_ERROR_LINK_ILLEGAL_FUNCTION);
      I870DIA1_ERROR(pSession->pChannel, pSession, I870DIA1_PRIMARY_FC);
      _sendFixedFrame(pLinkContext, pSession, I870FT12_FC_NOT_USED);
      break;
    }
  }
  else
  {
    /* Process secondary messages */

    /* Ignore secondary messages if we are an unbalanced slave */
    if((pSession->type == TMWTYPES_SESSION_TYPE_SLAVE)
      && (pLinkContext->linkMode == TMWDEFS_LINK_MODE_UNBALANCED))
    {
      I870STAT_CHNL_ERROR(pSession->pChannel, pSession, TMWCHNL_ERROR_LINK_ILLEGAL_FUNCTION);

#ifdef TMW_SUPPORT_MONITOR 
      if(!pSession->pChannel->pPhysContext->monitorMode)
      {
        I870DIA1_ERROR(pSession->pChannel, pSession, I870DIA1_RCVD_INV_SECONDARY);
        return;
      }
#else

      I870DIA1_ERROR(pSession->pChannel, pSession, I870DIA1_RCVD_INV_SECONDARY);
      return;
#endif
    }

    /* Make sure this message was from the session we are currently
     *  expecting a reply from
     */
    if(pSession != pLinkContext->pCurrentSession)
    {
      I870STAT_CHNL_ERROR(pSession->pChannel, pSession, TMWCHNL_ERROR_LINK_WRONG_SESN);

#ifdef TMW_SUPPORT_MONITOR 
      if(!pSession->pChannel->pPhysContext->monitorMode)
      {
        I870DIA1_ERROR(pSession->pChannel, pSession, I870DIA1_REPLY_WRONG_SESN);
        return;
      }
#else
      I870DIA1_ERROR(pSession->pChannel, pSession, I870DIA1_REPLY_WRONG_SESN);
      return;
#endif
    }

    /* If state is not READY, make sure we got the expected response
     * If a master is restarted it may receive responses to previous requests
     */
    if(pLinkSession->sesnState != I870LNK1_SESSION_STATE_READY)
    {
#ifdef TMW_SUPPORT_MONITOR 
      if(!pSession->pChannel->pPhysContext->monitorMode)
      {
#endif
      if((pLinkSession->sesnState == I870LNK1_SESSION_STATE_RESET_PENDING)
        &&(pLinkContext->rxControl & I870FT12_CONTROL_FC_MASK) != I870FT12_FC_CONFIRM_ACK)
      {  
        /* According to spec if we receive a NACK1(1) (or 14 or 15) we should retry the RESET 
         * We will retry when nack is received.
         */
        if((pLinkContext->rxControl & I870FT12_CONTROL_FC_MASK) == I870FT12_FC_CONFIRM_NACK)
        {
          /* No longer waiting for a reply */
          tmwtimer_cancel(&pLinkContext->confirmTimer);
          pLinkContext->channelIdle = TMWDEFS_TRUE;
          pLinkContext->pCurrentSession = TMWDEFS_NULL;

          if((pSession->protocol == TMWTYPES_PROTOCOL_103)
            && pLinkSession->send103ResetFCB)
          {
            /* For 103, send RESET_FCB if configured to do so */
            _sendFixedFrame(pLinkContext, pSession, I870FT12_CONTROL_PRM | I870FT12_FC_LINK_RESET_FCB);
          }
          else
          {
            _sendFixedFrame(pLinkContext, pSession, I870FT12_CONTROL_PRM | I870FT12_FC_LINK_RESET_CU);  
          }
          return;
        }

        I870STAT_CHNL_ERROR(pSession->pChannel, pSession, TMWCHNL_ERROR_LINK_WRONG_REPLY); 
        I870DIA1_ERROR(pSession->pChannel, pSession, I870DIA1_WRONG_REPLY);
        return;
      }

      if((pLinkSession->sesnState == I870LNK1_SESSION_STATE_REQUEST_STATUS)
        &&((pLinkContext->rxControl & I870FT12_CONTROL_FC_MASK) != I870FT12_FC_RESPOND_STATUS))
      {
        I870STAT_CHNL_ERROR(pSession->pChannel, pSession, TMWCHNL_ERROR_LINK_WRONG_REPLY);
        I870DIA1_ERROR(pSession->pChannel, pSession, I870DIA1_WRONG_REPLY);
        return;
      }
#ifdef TMW_SUPPORT_MONITOR 
      }
#endif
    }

    /* Cancel confirm timeout since we received a reply */
    tmwtimer_cancel(&pLinkContext->confirmTimer);

    /* No longer waiting for a reply */
    pLinkContext->channelIdle = TMWDEFS_TRUE;
    pLinkContext->pCurrentSession = TMWDEFS_NULL;

    /* Check Access Demand Bit */
    if(pLinkContext->rxControl & I870FT12_CONTROL_ACD)
      pLinkSession->class1DataAvailable = TMWDEFS_TRUE;

    /* Check DFC Bit */
    if(pLinkContext->rxControl & I870FT12_CONTROL_DFC)
      pLinkSession->DFC1Received = TMWDEFS_TRUE;
    else
      pLinkSession->DFC1Received = TMWDEFS_FALSE;

    /* Process response by function code */
    switch(pLinkContext->rxControl & I870FT12_CONTROL_FC_MASK)
    {
    case I870FT12_FC_CONFIRM_ACK:
      if(pLinkSession->sesnState != I870LNK1_SESSION_STATE_READY)
      {
        /* Must have been waiting for response to Reset CU or Reset FCB.*/
        /* Call application layer info function */
        pLinkContext->tmw.pInfoFunc(pLinkContext->tmw.pCallbackParam,
          pSession, TMWSCL_INFO_ONLINE);

        /* Session is ready to communicate */
        pLinkSession->sesnState = I870LNK1_SESSION_STATE_READY;

        /* If unbalanced mode and we are the master
         * start polling 
         */
        if((pSession->type == TMWTYPES_SESSION_TYPE_MASTER)
          && (pLinkContext->linkMode == TMWDEFS_LINK_MODE_UNBALANCED))
        {
          _pollSession(pSession);
        }
      }
      else
      {
        TMWSESN_TX_DATA *pOldTxDescriptor = pLinkContext->pLinkTxDescriptor;

        /* Done with this frame */
        pLinkContext->pLinkTxDescriptor = TMWDEFS_NULL;

        /* Call after transmission callback */
        if((pOldTxDescriptor != TMWDEFS_NULL)
          && (pLinkContext->tmw.pAfterTxCallback != TMWDEFS_NULL))
        {
          /* Call application layer callback */
          pLinkContext->tmw.pAfterTxCallback(pOldTxDescriptor);
        }

        if((pSession->type == TMWTYPES_SESSION_TYPE_MASTER)
          && (pLinkContext->linkMode == TMWDEFS_LINK_MODE_UNBALANCED))
        { 
          /* check to see if we should adjust the poll timer,
           * in case there is now a pending request 
           */
          startPollTimer = TMWDEFS_TRUE;
        }
      }
      break;

    case I870FT12_FC_CONFIRM_NACK:
      /* Ignore NACK */
      break;

    case I870FT12_FC_RESPOND_USER_DATA:
      /* This function code only support in unbalanced mode */
      if(pLinkContext->linkMode == TMWDEFS_LINK_MODE_BALANCED)
      {
        I870STAT_CHNL_ERROR(pSession->pChannel, pSession, TMWCHNL_ERROR_LINK_ILLEGAL_FUNCTION);
        I870DIA1_ERROR(pSession->pChannel, pSession, I870DIA1_USER_DATA);
        return;
      }
	
      if(pLinkContext->tmw.pParseFunc != TMWDEFS_NULL)
      {
        pLinkContext->rxFrame.msgLength =
          (TMWTYPES_USHORT)(pLinkContext->rxCurrentLength
          - (I870FT12_NUM_NOT_IN_LENGTH + 1 + pLinkContext->linkAddressSize));

        pLinkContext->rxFrame.pMsgBuf =
          pLinkContext->rxFrameBuffer + 5 + pLinkContext->linkAddressSize;

        pLinkContext->tmw.pParseFunc(
          pLinkContext->tmw.pCallbackParam, pSession, &pLinkContext->rxFrame);
      }
      startPollTimer = TMWDEFS_TRUE;
      break;

    case I870FT12_FC_RESPOND_NO_DATA:
      /* Respond No Data */

      /* This function code only support in unbalanced mode */
      if(pLinkContext->linkMode == TMWDEFS_LINK_MODE_BALANCED)
      {
        I870STAT_CHNL_ERROR(pSession->pChannel, pSession, TMWCHNL_ERROR_LINK_ILLEGAL_FUNCTION);
        I870DIA1_ERROR(pSession->pChannel, pSession, I870DIA1_RESPOND_NO_DATA);
        return;
      }
      startPollTimer = TMWDEFS_TRUE;
      break;

    case I870FT12_FC_RESPOND_STATUS:
      if(pLinkSession->sesnState == I870LNK1_SESSION_STATE_REQUEST_STATUS) 
      {
        /* For 103, send RESET_FCB not REQUEST_STATUS followed by RESET_CU */
        if((pSession->protocol != TMWTYPES_PROTOCOL_103)
          || (pSession->type != TMWTYPES_SESSION_TYPE_MASTER))
        {
          if(!pLinkSession->DFC1Received) 
          {
            pLinkSession->sesnState = I870LNK1_SESSION_STATE_RESET_PENDING;
            _sendFixedFrame(pLinkContext, pSession, I870FT12_CONTROL_PRM | I870FT12_FC_LINK_RESET_CU);
          }
        }
      }
     
      break;

    case I870FT12_FC_NOT_FUNCTIONING:
      I870DIA1_ERROR(pSession->pChannel, pSession, I870DIA1_UNSUPPORTED_FC);
      break;

    case I870FT12_FC_NOT_USED:
      I870DIA1_ERROR(pSession->pChannel, pSession, I870DIA1_FC_NOT_USED);
      break;

    default:
      I870STAT_CHNL_ERROR(pSession->pChannel, pSession, TMWCHNL_ERROR_LINK_ILLEGAL_FUNCTION);
      I870DIA1_ERROR(pSession->pChannel, pSession, I870DIA1_UNRECOGNIZED_FC);
      break;
    }
  }

#ifdef TMW_SUPPORT_MONITOR 
  if(pSession->pChannel->pPhysContext->monitorMode)
  {
    return;
  }
#endif

  if(startPollTimer)
  {
    _startPollTimer(pSession);
  }

  /* Try to send next message to channel if needed */
  _tryToSend((TMWLINK_CONTEXT *)pLinkContext, pSession);

  _checkForIdleChannel(pLinkContext);
}

/* function: _getNeededBytes
 * purpose: return the number of bytes required to finish the
 *  current block
 * arguments:
 *  pCallbackParam - callback data, contains link layer context
 * returns
 *  number of characters to read
 */
/* function: _getNeededBytes */
static TMWTYPES_USHORT TMWDEFS_CALLBACK _getNeededBytes(
  void *pCallbackParam)
{
  I870LNK1_CONTEXT *pLinkContext = (I870LNK1_CONTEXT *)pCallbackParam;

  /* If not idle, rxLength contains the number of bytes we are currently
   * waiting for, so subtract the number of bytes already read and return
   */
  if(pLinkContext->rxState != RX_STATE_IDLE)
    return((TMWTYPES_USHORT)(pLinkContext->rxExpectedLength - pLinkContext->rxCurrentLength));

  /* If idle, read one byte at a time until we receive a sync character
   */
  return(1);
}

/* function: _parseBytes
 * purpose: parse incoming data
 * arguments:
 *  pCallbackParam - callback data, contains link layer context
 *  pRxBuf - received characters
 *  numBytes - number of bytes
 * returns
 *  void
 */
static void TMWDEFS_CALLBACK _parseBytes(
  void *pCallbackParam,
  TMWTYPES_UCHAR *pRxBuf,
  TMWTYPES_USHORT numBytes,
  TMWTYPES_MILLISECONDS firstByteTime)
{
  I870LNK1_CONTEXT *pLinkContext = (I870LNK1_CONTEXT *)pCallbackParam;

  if(pLinkContext->rxState == RX_STATE_IDLE)
  { 
    if(firstByteTime == 0)
    {
      pLinkContext->rxFrame.firstByteTime = tmwtarg_getMSTime();
    }
    else
    {
      pLinkContext->rxFrame.firstByteTime = firstByteTime;
    }

    /* In IDLE state we are reading one byte at a time waiting for a sync
     *  character. If we get one start a new frame and return. If not just
     *  ignore this byte.
     */
    if(pRxBuf[0] == I870FT12_SYNC_OCTET_SINGLE)
    {
      /* Single character ack  
       * This can be received by slaves running in balanced mode 
       * or master in either balanced or unbalanced mode
       * If we are expecting a reply and master or balanced mode
       */
      if((pLinkContext->pCurrentSession != TMWDEFS_NULL)
        &&((pLinkContext->pCurrentSession->type == TMWTYPES_SESSION_TYPE_MASTER)
        ||(pLinkContext->linkMode == TMWDEFS_LINK_MODE_BALANCED)))
      {
        pLinkContext->rxCurrentLength = 1;
        pLinkContext->rxFrameBuffer[0] = pRxBuf[0];
        pLinkContext->rxControl = I870FT12_FC_CONFIRM_ACK;
        pLinkContext->rxAddress = pLinkContext->pCurrentSession->linkAddress;  
   
        _processFrame(pLinkContext);
      }
    }
    else if(pRxBuf[0] == I870FT12_SYNC_OCTET_FIXED)
    {
      /* Fixed frame, setup to read the rest of the frame */
      pLinkContext->rxCurrentLength = 1;
      pLinkContext->rxFrameBuffer[0] = pRxBuf[0];
      pLinkContext->rxExpectedLength = (TMWTYPES_USHORT)(4 + pLinkContext->linkAddressSize);
      pLinkContext->rxState = RX_STATE_READ_FIXED;
    }
    else if(pRxBuf[0] == I870FT12_SYNC_OCTET_VARIABLE)
    {
      /* Variable frame, setup to read the length */
      pLinkContext->rxCurrentLength = 1;
      pLinkContext->rxFrameBuffer[0] = pRxBuf[0];
      pLinkContext->rxExpectedLength = (TMWTYPES_USHORT)(5 + pLinkContext->linkAddressSize);
      pLinkContext->rxState = RX_STATE_READ_HEADER;
    }

    /* If we are receiving a frame, start frame timer */
    if(pLinkContext->rxState != RX_STATE_IDLE)
    {
      if(pLinkContext->rxFrameTimeout != 0)
      {
        tmwtimer_start(&pLinkContext->rxFrameTimer,
          pLinkContext->rxFrameTimeout, pLinkContext->tmw.pChannel,
          _rxFrameTimeout, pLinkContext);
      }
    }

    return;
  }
  else
  {
    TMWTYPES_UCHAR expectedChecksum;
    TMWTYPES_UCHAR receivedChecksum;
    TMWTYPES_UCHAR frameLength;

    /* Currently reading a frame, 
     * If new bytes would not fit in Rx Frame Buffer, discard this frame 
     * Otherwise copy received data into frame buffer 
     */
    if((pLinkContext->rxCurrentLength + numBytes)> pLinkContext->rxFrameSize )
    {        
      pLinkContext->rxState = RX_STATE_IDLE;
      I870STAT_CHNL_ERROR(pLinkContext->tmw.pChannel, TMWDEFS_NULL, TMWCHNL_ERROR_LINK_FRAME_LENGTH);
      I870DIA1_ERROR(pLinkContext->tmw.pChannel, TMWDEFS_NULL, I870DIA1_RCV_LENGTH);
	    return;
    }

    memcpy(pLinkContext->rxFrameBuffer + pLinkContext->rxCurrentLength, pRxBuf, numBytes);
    pLinkContext->rxCurrentLength = (TMWTYPES_USHORT)(pLinkContext->rxCurrentLength + numBytes);

    /* See if we have read the number of bytes we are currently waiting for */
    if(pLinkContext->rxCurrentLength == pLinkContext->rxExpectedLength)
    {

#if defined(TMW_PRIVATE)
      /* Received a complete frame, call user callback if specified */
      if(pLinkContext->tmw.pUserRxFrameCallback != TMWDEFS_NULL)
      {
        pLinkContext->tmw.pUserRxFrameCallback(
          pLinkContext->tmw.pUserRxFrameCallbackParam,
          pLinkContext->rxFrame.firstByteTime,
          pLinkContext->rxFrameBuffer, 
          &pLinkContext->rxCurrentLength);
      }
#endif
      switch(pLinkContext->rxState)
      {
      case RX_STATE_READ_FIXED:
        /* Fixed frame complete, process it and return */

        /* Cancel frame timer */
        tmwtimer_cancel(&pLinkContext->rxFrameTimer);

        /* If last character is not end character the length must be wrong */
        if(pLinkContext->rxFrameBuffer[pLinkContext->rxCurrentLength -1] != I870FT12_SYNC_OCTET_END)
        { 
          I870STAT_CHNL_ERROR(pLinkContext->tmw.pChannel, TMWDEFS_NULL, TMWCHNL_ERROR_LINK_INVALID_END_CHAR);
          I870DIA1_ERROR(pLinkContext->tmw.pChannel, TMWDEFS_NULL, I870DIA1_INVALID_LENGTH);
          pLinkContext->rxState = RX_STATE_IDLE;
          break;
        }

        /* Copy frame info into context */
        pLinkContext->rxControl = pLinkContext->rxFrameBuffer[I870FT12_INDEX_FIX_CONTROL];

        switch(pLinkContext->linkAddressSize)
        {
        case 1:
          pLinkContext->rxAddress = pLinkContext->rxFrameBuffer[I870FT12_INDEX_FIX_ADDRESS];
          break;

        case 2:
          tmwtarg_get16(&pLinkContext->rxFrameBuffer[I870FT12_INDEX_FIX_ADDRESS], &pLinkContext->rxAddress);
          break;
        }

        /* Check checksum */
        expectedChecksum = _computeChecksum(
          &pLinkContext->rxFrameBuffer[I870FT12_INDEX_FIX_CONTROL],
          pLinkContext->linkAddressSize + 1);

        receivedChecksum = pLinkContext->rxFrameBuffer[
          I870FT12_INDEX_FIX_ADDRESS + pLinkContext->linkAddressSize];

        if(expectedChecksum != receivedChecksum)
        {
          I870STAT_CHNL_ERROR(pLinkContext->tmw.pChannel, TMWDEFS_NULL, TMWCHNL_ERROR_LINK_INVALID_CHECKSUM);
          I870DIA1_ERROR(pLinkContext->tmw.pChannel, TMWDEFS_NULL, I870DIA1_INVALID_CHKS);

#ifdef TMW_SUPPORT_MONITOR
          if(pLinkContext->tmw.pChannel->pPhysContext->monitorMode)
          {
            TMWDIAG_CHNL_ERROR(pLinkContext->tmw.pChannel, "Try changing linkAddressSize");
          }
#endif

          pLinkContext->rxState = RX_STATE_IDLE;
          break;
        }

        /* Process Frame */
        _processFrame(pLinkContext);

        /* Set state back to idle and return */
        pLinkContext->rxState = RX_STATE_IDLE;
        break;

      case RX_STATE_READ_HEADER:
        /* Validate length from header */
        if(pLinkContext->rxFrameBuffer[I870FT12_INDEX_VAR_LENGTH]
          != pLinkContext->rxFrameBuffer[I870FT12_INDEX_VAR_LENGTH_COPY])
        {
          /* Log error */ 
          I870STAT_CHNL_ERROR(pLinkContext->tmw.pChannel, TMWDEFS_NULL, TMWCHNL_ERROR_LINK_MISMATCHING_LENGTH);
          pLinkContext->rxState = RX_STATE_IDLE;
          break;
        }

        /* Validate second sync character */
        if(pLinkContext->rxFrameBuffer[I870FT12_INDEX_VAR_SYNC_SECOND]
          != I870FT12_SYNC_OCTET_VARIABLE)
        {
          /* Log error */
          I870STAT_CHNL_ERROR(pLinkContext->tmw.pChannel, TMWDEFS_NULL, TMWCHNL_ERROR_LINK_INVALID_2ND_CHAR);
          pLinkContext->rxState = RX_STATE_IDLE;
          break;
        }

        pLinkContext->rxControl = pLinkContext->rxFrameBuffer[I870FT12_INDEX_VAR_CONTROL];
        switch(pLinkContext->linkAddressSize)
        {
        case 1:
          pLinkContext->rxAddress = pLinkContext->rxFrameBuffer[I870FT12_INDEX_VAR_ADDRESS];
          break;

        case 2:
          tmwtarg_get16(&pLinkContext->rxFrameBuffer[I870FT12_INDEX_VAR_ADDRESS], &pLinkContext->rxAddress);
          break;
        }

        pLinkContext->rxExpectedLength = (TMWTYPES_USHORT)
          (pLinkContext->rxFrameBuffer[I870FT12_INDEX_VAR_LENGTH] + I870FT12_NUM_NOT_IN_LENGTH);

        pLinkContext->rxState = RX_STATE_READ_VARIABLE;
        break;

      case RX_STATE_READ_VARIABLE:
        /* Variable frame complete, process it and return */

        /* Cancel frame timer */
        tmwtimer_cancel(&pLinkContext->rxFrameTimer);   
        
        /* If last character is not end character the length must be wrong */
        if(pLinkContext->rxFrameBuffer[pLinkContext->rxCurrentLength -1] != I870FT12_SYNC_OCTET_END)
        { 
          I870STAT_CHNL_ERROR(pLinkContext->tmw.pChannel, TMWDEFS_NULL, TMWCHNL_ERROR_LINK_INVALID_END_CHAR);
          I870DIA1_ERROR(pLinkContext->tmw.pChannel, TMWDEFS_NULL, I870DIA1_INVALID_LENGTH);
          pLinkContext->rxState = RX_STATE_IDLE;
          break;
        }
        
        /* Validate checksum */
        frameLength = pLinkContext->rxFrameBuffer[I870FT12_INDEX_VAR_LENGTH];

        expectedChecksum = _computeChecksum(
          &pLinkContext->rxFrameBuffer[I870FT12_INDEX_VAR_CONTROL],
          frameLength);

        receivedChecksum = pLinkContext->rxFrameBuffer[
          I870FT12_INDEX_VAR_CONTROL + frameLength];

        if(expectedChecksum != receivedChecksum)
        {
          I870STAT_CHNL_ERROR(pLinkContext->tmw.pChannel, TMWDEFS_NULL, TMWCHNL_ERROR_LINK_INVALID_CHECKSUM);
          I870DIA1_ERROR(pLinkContext->tmw.pChannel, TMWDEFS_NULL, I870DIA1_INVALID_CHKS);

          pLinkContext->rxState = RX_STATE_IDLE;
          break;
        }

        /* Process frame */
        _processFrame(pLinkContext);

        /* Set state back to idle and return */
        pLinkContext->rxState = RX_STATE_IDLE;
        break;

      default:
        break;
      }
    }
  }
}

/* function: _checkAddressMatchCallback
 * purpose: parse incoming data to determine if it is for this channel
 * arguments:
 *  pCallbackParam - callback data, contains link layer context
 *  recvBuf - received characters
 *  numBytes - number of bytes
 *  firstByteTime - time first byte was received
 * returns
 *  void
 */
static TMWPHYS_ADDRESS_MATCH_TYPE TMWDEFS_CALLBACK _checkAddressMatchCallback(
  void *pCallbackParam,
  TMWTYPES_UCHAR *recvBuf,
  TMWTYPES_USHORT numBytes,
  TMWTYPES_MILLISECONDS firstByteTime)
{    
  TMWTYPES_USHORT rxAddress;
  I870LNK1_CONTEXT *pLinkContext = (I870LNK1_CONTEXT *)pCallbackParam;

  if(numBytes == 0)
    return(TMWPHYS_ADDRESS_MATCH_FAILED);
 
  /* If msg is for 101/103 it must have one of these start characters */
  if(recvBuf[0] == I870FT12_SYNC_OCTET_FIXED) 
  {
    if(numBytes == 1)
      return(TMWPHYS_ADDRESS_MATCH_MAYBE);

   if(numBytes < (2 + pLinkContext->linkAddressSize))
      return(TMWPHYS_ADDRESS_MATCH_MAYBE);
    
    switch(pLinkContext->linkAddressSize)
    {
    case 1:
      rxAddress = pLinkContext->rxFrameBuffer[I870FT12_INDEX_FIX_ADDRESS];
      break;

    case 2:
      tmwtarg_get16(&pLinkContext->rxFrameBuffer[I870FT12_INDEX_FIX_ADDRESS], &rxAddress);
      break;

    default:
      /* if address size is zero or invalid we have no hope of matching */
      return(TMWPHYS_ADDRESS_MATCH_FAILED);
    }
  }
  else if(recvBuf[0] == I870FT12_SYNC_OCTET_VARIABLE)
  {
    if(numBytes == 1)
      return(TMWPHYS_ADDRESS_MATCH_MAYBE);
 
    if(numBytes < (5 + pLinkContext->linkAddressSize))
      return(TMWPHYS_ADDRESS_MATCH_MAYBE);
   
    switch(pLinkContext->linkAddressSize)
    {
    case 1:
      rxAddress = pLinkContext->rxFrameBuffer[I870FT12_INDEX_VAR_ADDRESS];
      break;

    case 2:
      tmwtarg_get16(&pLinkContext->rxFrameBuffer[I870FT12_INDEX_VAR_ADDRESS], &rxAddress);
      break;
 
    default:
      /* if address size is zero or invalid we have no hope of matching */
      return(TMWPHYS_ADDRESS_MATCH_FAILED);
    }
  }
  else /* start character does not match */
  {
    return(TMWPHYS_ADDRESS_MATCH_FAILED);
  }
  
  /* Lookup session based on received link address */
  if(_findSession(pLinkContext, rxAddress) != TMWDEFS_NULL)
  {
    int index = 1;
    _parseBytes(pCallbackParam, recvBuf, 1, firstByteTime);
    while(index < numBytes)
    {
      TMWTYPES_USHORT needed;
      TMWTYPES_USHORT numBytesAvailable;

      needed = _getNeededBytes(pCallbackParam);
      numBytesAvailable = (TMWTYPES_USHORT)(numBytes - index);
      if(needed < numBytesAvailable)
        numBytesAvailable = needed;

      _parseBytes(pCallbackParam, &recvBuf[index], numBytesAvailable, firstByteTime);
      index += numBytesAvailable;
    }
    return(TMWPHYS_ADDRESS_MATCH_SUCCESS);
  }

  return(TMWPHYS_ADDRESS_MATCH_FAILED);
}

/* Global function implementation */

/* function: i870lnk1_initConfig */
void TMWDEFS_GLOBAL i870lnk1_initConfig(
  I870LNK1_CONFIG *pConfig)
{
  pConfig->offlinePollPeriod = TMWDEFS_SECONDS(10);
  pConfig->confirmMode = TMWDEFS_LINKCNFM_ALWAYS;
  pConfig->confirmTimeout = TMWDEFS_SECONDS(2);
  pConfig->linkAddressSize = 1;
  pConfig->linkMode = TMWDEFS_LINK_MODE_UNBALANCED; 
  pConfig->oneCharAckAllowed = TMWDEFS_FALSE; 
  pConfig->oneCharResponseAllowed = TMWDEFS_FALSE;
  pConfig->maxRetries = 3;
  pConfig->rxFrameSize = I870LNK1_MAX_RX_BUFFER_LENGTH;
  pConfig->rxFrameTimeout = TMWDEFS_SECONDS(15);
  pConfig->txFrameSize = I870LNK1_MAX_TX_BUFFER_LENGTH;
  pConfig->testFramePeriod = 0;
}

/* function: i870lnk1_initChannel */
TMWTYPES_BOOL TMWDEFS_GLOBAL i870lnk1_initChannel(
  TMWCHNL *pChannel,
  const I870LNK1_CONFIG *pConfig)
{
  static TMWLINK_INTERFACE _i870lnk1Interface = {
    (TMWLINK_OPEN_SESSION_FUNC)_openSession,
    (TMWLINK_CLOSE_SESSION_FUNC)_closeSession,
    (TMWLINK_GET_SESSIONS_FUNC)_getSessions,
    (TMWLINK_DATA_READY_FUNC)_checkDataReady,
    (TMWLINK_SET_CALLBACKS_FUNC)tmwlink_setCallbacks,
    (TMWLINK_TRANSMIT_FUNC)_transmitFrame,
    (TMWLINK_UPDATE_MSG_FUNC)_updateMsg,
    (TMWLINK_CANCEL_FUNC)_cancelFrame
  };

  I870LNK1_CONTEXT *pLinkContext;

  if(pConfig->rxFrameSize > I870LNK1_MAX_RX_BUFFER_LENGTH)
  {
    return(TMWDEFS_FALSE);
  }

  /* Allocate space for context */
  pLinkContext = (I870LNK1_CONTEXT *)i870mem1_alloc(I870MEM1_CONTEXT_TYPE);
  if(pLinkContext == TMWDEFS_NULL)
  {
    return(TMWDEFS_FALSE);
  }

  /* Set link layer points in channel */
  pLinkContext->tmw.pChannel = pChannel;
  pChannel->pLink = &_i870lnk1Interface;
  pChannel->pLinkContext = (TMWLINK_CONTEXT *)pLinkContext;

  /* Initialize generic link layer info */
  tmwlink_initChannel(pChannel, pConfig->offlinePollPeriod,
    tmwlink_channelCallback, _getNeededBytes, _parseBytes, 
    _infoFunc, _checkAddressMatchCallback);

  /* Store config */
  i870lnk1_setChannelConfig(pChannel, pConfig);

  /* Set up frame buffers */
  pLinkContext->variablePrimaryTxDescriptor.UDPPort = TMWTARG_UDP_NONE;
  pLinkContext->variablePrimaryTxDescriptor.numBytesToTx = 0;
  pLinkContext->variablePrimaryTxDescriptor.pTxBuffer = pLinkContext->secondaryTxFrameBuffer;

  pLinkContext->fixedSecondaryTxDescriptor.UDPPort = TMWTARG_UDP_NONE;
  pLinkContext->fixedSecondaryTxDescriptor.numBytesToTx = 0;
  pLinkContext->fixedSecondaryTxDescriptor.pTxBuffer  = pLinkContext->fixedSecondaryTxFrameBuffer;

  pLinkContext->pSecondaryTxDescriptor = &pLinkContext->fixedSecondaryTxDescriptor;

  /* Initialization */
  pLinkContext->pCurrentSession    = TMWDEFS_NULL;
  pLinkContext->channelIdle        = TMWDEFS_TRUE;
  pLinkContext->needFixedFrameXmt  = TMWDEFS_FALSE;
  pLinkContext->transmitInProgress = TMWDEFS_FALSE;
  pLinkContext->expectEventConfirm = TMWDEFS_FALSE;
  pLinkContext->pLinkTxDescriptor  = TMWDEFS_NULL;
  pLinkContext->pLastSessionPolled = TMWDEFS_NULL;
  pLinkContext->numberOfPollsSent  = 0;

  pLinkContext->rxState = RX_STATE_IDLE;
  
  pLinkContext->pUserTxCallback = TMWDEFS_NULL;
  pLinkContext->pCallbackParam = TMWDEFS_NULL;

  tmwtimer_init(&pLinkContext->confirmTimer);
  tmwtimer_init(&pLinkContext->rxFrameTimer);
  tmwtimer_init(&pLinkContext->balancedRetryTimer);
  tmwtimer_init(&pLinkContext->testFrameTimer);

  /* Open channel (try to connect) if monitor mode */
#ifdef TMW_SUPPORT_MONITOR 
  if(pChannel->pPhysContext->monitorMode)
  {
    tmwlink_openChannel((TMWLINK_CONTEXT *)pLinkContext);
  }
#endif

  return(TMWDEFS_TRUE);
}

/* function: i870lnk1_getChannelConfig */
TMWTYPES_BOOL TMWDEFS_GLOBAL i870lnk1_getChannelConfig(
  TMWCHNL *pChannel,
  I870LNK1_CONFIG *pConfig)
{
  TMWLINK_CONTEXT *pContext       = pChannel->pLinkContext;
  I870LNK1_CONTEXT *pLinkContext  = (I870LNK1_CONTEXT *)pContext;

  pConfig->offlinePollPeriod      = pContext->offlinePollPeriod;

  pConfig->confirmMode            = pLinkContext->confirmMode;
  pConfig->confirmTimeout         = pLinkContext->confirmTimeout;
  pConfig->linkAddressSize        = pLinkContext->linkAddressSize;
  pConfig->linkMode               = pLinkContext->linkMode; 
  pConfig->oneCharAckAllowed      = pLinkContext->oneCharAckAllowed; 
  pConfig->oneCharResponseAllowed = pLinkContext->oneCharResponseAllowed;
  pConfig->maxRetries             = pLinkContext->maxRetries;
  pConfig->rxFrameSize            = pLinkContext->rxFrameSize;
  pConfig->rxFrameTimeout         = pLinkContext->rxFrameTimeout;
  pConfig->testFramePeriod        = pLinkContext->testFramePeriod;

  return(TMWDEFS_TRUE);
}

/* function: i870lnk1_setChannelConfig */
TMWTYPES_BOOL TMWDEFS_GLOBAL i870lnk1_setChannelConfig(
  TMWCHNL *pChannel,
  const I870LNK1_CONFIG *pConfig)
{
  TMWLINK_CONTEXT *pContext      = pChannel->pLinkContext;
  I870LNK1_CONTEXT *pLinkContext = (I870LNK1_CONTEXT *)pContext;

  pContext->offlinePollPeriod          = pConfig->offlinePollPeriod;

  pLinkContext->confirmMode            = pConfig->confirmMode;
  pLinkContext->confirmTimeout         = pConfig->confirmTimeout;
  pLinkContext->linkAddressSize        = pConfig->linkAddressSize;

  pLinkContext->linkMode               = pConfig->linkMode; 
  pLinkContext->oneCharAckAllowed      = pConfig->oneCharAckAllowed; 
  pLinkContext->oneCharResponseAllowed = pConfig->oneCharResponseAllowed;
  pLinkContext->maxRetries             = pConfig->maxRetries;
  pLinkContext->rxFrameSize            = pConfig->rxFrameSize;
  pLinkContext->rxFrameTimeout         = pConfig->rxFrameTimeout;
  pLinkContext->testFramePeriod        = pConfig->testFramePeriod;

  return(TMWDEFS_TRUE);
}

/* function: i870lnk1_modifyChannel */
TMWTYPES_BOOL TMWDEFS_GLOBAL i870lnk1_modifyChannel(
  TMWCHNL *pChannel,
  const I870LNK1_CONFIG *pConfig,
  TMWTYPES_ULONG configMask)
{
  I870LNK1_CONTEXT *pContext = (I870LNK1_CONTEXT *)pChannel->pLinkContext;

  if((configMask & I870LNK1_CONFIG_RX_FRAME_TIMEOUT) != 0)
  {
    pContext->rxFrameTimeout = pConfig->rxFrameTimeout;

    /* See if timer is currently active */
    if(tmwtimer_isActive(&pContext->rxFrameTimer))
    {
      /* If so, cancel it */
      tmwtimer_cancel(&pContext->rxFrameTimer);

      /* If the new timeout is not zero restart it using the
       *  new timeout value. For now we will ignore any time
       *  that has already elapsed
       */
      if(pContext->rxFrameTimeout != 0)
      {
        tmwtimer_start(&pContext->rxFrameTimer,
          pContext->rxFrameTimeout, pContext->tmw.pChannel,
          _rxFrameTimeout, pContext);
      }
    }
  }

  if((configMask & I870LNK1_CONFIG_CONFIRM_TIMEOUT) != 0)
  {
    pContext->confirmTimeout = pConfig->confirmTimeout;

    /* See if timer is currently active */
    if(tmwtimer_isActive(&pContext->confirmTimer))
    {
      /* If so, cancel it */
      tmwtimer_cancel(&pContext->confirmTimer);

      /* If the new timeout is not zero restart it using the
       *  new timeout value. For now we will ignore any time
       *  that has already elapsed
       */
      if(pContext->confirmTimeout != 0)
      {
        tmwtimer_start(&pContext->confirmTimer,
          pContext->confirmTimeout, pContext->tmw.pChannel,
          _primaryRetry, pContext);
      }
    }
  }

  if((configMask & I870LNK1_CONFIG_MAX_RETRIES) != 0)
  {
    pContext->maxRetries = pConfig->maxRetries;
  }

  if((configMask & I870LNK1_CONFIG_LINK_ADDR_SIZE) != 0)
  {
    pContext->linkAddressSize = pConfig->linkAddressSize;
  }

  return(TMWDEFS_TRUE);
}

/* function: i870lnk1_deleteChannel */
TMWTYPES_BOOL TMWDEFS_GLOBAL i870lnk1_deleteChannel(
  TMWCHNL *pChannel)
{
  I870LNK1_CONTEXT *pLinkContext = (I870LNK1_CONTEXT *)pChannel->pLinkContext;

  /* Lock channel */
  TMWTARG_LOCK_SECTION(&pChannel->lock);

  /* Cleanup generic link layer info */
  if(tmwlink_deleteChannel(pChannel) == TMWDEFS_FALSE)
  {
    /* Unlock channel */
    TMWTARG_UNLOCK_SECTION(&pChannel->lock);
    return(TMWDEFS_FALSE);
  }

  /* Cancel any outstanding timers */
  tmwtimer_cancel(&pLinkContext->balancedRetryTimer);
  tmwtimer_cancel(&pLinkContext->rxFrameTimer);
  tmwtimer_cancel(&pLinkContext->confirmTimer);
  tmwtimer_cancel(&pLinkContext->testFrameTimer);

  /* Free context */
  i870mem1_free(pLinkContext);

  /* Make sure we don't do this again */
  pChannel->pLink = TMWDEFS_NULL;
  pChannel->pLinkContext = TMWDEFS_NULL;

  /* Unlock channel */
  TMWTARG_UNLOCK_SECTION(&pChannel->lock);

  return(TMWDEFS_TRUE);
}

/* function: i870lnk1_sessionActive */
void TMWDEFS_GLOBAL i870lnk1_sessionActive(
  TMWSESN *pSession)
{ 
  /* Lock channel */
  TMWTARG_LOCK_SECTION(&pSession->pChannel->lock);

  _tryToSend(pSession->pChannel->pLinkContext, pSession);
 
  /* Unlock channel */
  TMWTARG_UNLOCK_SECTION(&pSession->pChannel->lock);
}

/* function: i870lnk1_setFlowControl */
void TMWDEFS_GLOBAL i870lnk1_setFlowControl(
  TMWSESN *pSession,
  TMWTYPES_BOOL dfcValue)
{
  I870LNK1_SESSION_INFO *pLinkSession = (I870LNK1_SESSION_INFO*)pSession->pLinkSession;
  pLinkSession->sendDFC1 = dfcValue;
}

/* function: i870lnk1_registerCallback */
void TMWDEFS_GLOBAL i870lnk1_registerCallback(
  TMWCHNL *pChannel,
  I870LNK1_TX_CALLBACK_FUNC pUserTxCallback,
  void *pCallbackParam)
{
  I870LNK1_CONTEXT *pLinkContext = (I870LNK1_CONTEXT *)pChannel->pLinkContext;
  pLinkContext->pUserTxCallback = pUserTxCallback;
  pLinkContext->pCallbackParam = pCallbackParam;
}
