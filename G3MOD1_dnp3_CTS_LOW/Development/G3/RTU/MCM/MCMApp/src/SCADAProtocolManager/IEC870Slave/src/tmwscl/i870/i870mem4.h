/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i870mem4.h
 * description: IEC 60870-5-104 profile Link Layer Implementation
 */
#ifndef I870MEM4_DEFINED
#define I870MEM4_DEFINED

#include "tmwscl/utils/tmwcnfg.h"
#include "tmwscl/utils/tmwmem.h"

/* Memory allocation defines */
typedef enum I870mem4AllocType {
  I870MEM4_CONTEXT_TYPE,
  I870MEM4_RDCY_GROUP_TYPE,
  I870MEM4_RDCY_CONNECT_TYPE,
  I870MEM4_ALLOC_TYPE_MAX
} I870MEM4_ALLOC_TYPE;

typedef struct {
  /* Specify max number 104 channels including 1 for each redundant connection channel */
  TMWTYPES_UINT numContexts;

  /* Specify max number 104 redundancy groups */
  TMWTYPES_UINT numRdcyGrps;

  /* Specify max number 104 redundant connections */
  TMWTYPES_UINT numRdcyConnects;
} I870MEM4_CONFIG;

/* Specify max number 104 channels, including 1 for each redundant connection channel */
#define I870MEM4_NUMALLOC_CONTEXTS        TMWCNFG_MAX_CHANNELS

/* Specify max number 104 redundancy groups */
#define I870MEM4_NUMALLOC_RDCY_GROUPS     TMWCNFG_MAX_SESSIONS

/* Specify max number 104 redundant connections */
#define I870MEM4_NUMALLOC_RDCY_CONNECTS   (TMWCNFG_MAX_SESSIONS*2)

#ifdef __cplusplus
extern "C" {
#endif
 
  /* routine: i870mem4_initConfig
   * purpose:  INTERNAL FUNCTION to initialize the memory configuration 
   *  structure indicating how many of each structure type to put in
   *  the memory pool. These will be initialized according 
   *  to the compile time defines.
   *  NOTE: user should call m104mem_initConfig()or s104mem_initConfig()
   *  to modify the number of buffers allowed for each type.
   * arguments:
   *  pConfig - pointer to memory configuration structure to be filled in
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL i870mem4_initConfig(
    I870MEM4_CONFIG   *pConfig);

  /* routine: i870mem4_init
   * purpose: INTERNAL memory management init function.
   *  NOTE: user should call m104mem_initMemory() or s104mem_initMemory() 
   *  to modify the number of buffers allowed for each type.
   * arguments:
   *  pConfig - pointer to memory configuration structure to be used
   * returns:
   *  TMWDEFS_TRUE if successful
   *  TMWDEFS_FALSE otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL i870mem4_init(
    I870MEM4_CONFIG *pConfig);

  /* function: i870mem4_alloc
   * purpose:  Allocate memory  
   * arguments: 
   *  type - enum value indicating what structure to allocate
   * returns:
   *   TMWDEFS_NULL if allocation failed
   *   void * pointer to allocated memory if successful
   */
  void * TMWDEFS_GLOBAL i870mem4_alloc(
    I870MEM4_ALLOC_TYPE type);

  /* function: i870mem4_free
   * purpose:  Deallocate memory
   * arguments: 
   *  pBuf - pointer to buffer to be deallocated
   * returns:    
   *   void  
   */
  void TMWDEFS_GLOBAL i870mem4_free(
    void *pBuf);

  /* function: i870mem4_getUsage
   * purpose:  Determine memory usage for each type of memory
   *    managed by this file.
   * arguments: 
   *  index: index of pool, starting with 0 caller can call
   *    this function repeatedly, while incrementing index. When
   *     index is larger than number of pools, this function
   *     will return TMWDEFS_FALSE
   *  pName: pointer to a char pointer to be filled in
   *  pStruct: pointer to structure to be filled in.
   * returns:    
   *  TMWDEFS_TRUE  if successfully returned usage statistics.
   *  TMWDEFS_FALSE if failure because index is too large.
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL i870mem4_getUsage(
    TMWTYPES_UCHAR index,
    const TMWTYPES_CHAR **pName,
    TMWMEM_POOL_STRUCT *pStruct);
 
#ifdef __cplusplus
}
#endif

#endif /* I870MEM4_DEFINED */
