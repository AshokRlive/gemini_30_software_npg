/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:lucy_s14data.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief Definitions linked to TMW's Slave IEC60870-5-101 & 104 s14data.h
 *
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15 Oct 2014   wang_p      Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef LUCY_I870_S14DATA_H_
#define LUCY_I870_S14DATA_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/utils/tmwtypes.h"

#include "LinIoTarg/lucydefs.h"
#include "lucy_common_defs.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 * Forward Declarations
 */
typedef struct I870DBHandleDef I870DBHandleStr;

typedef struct I870OutputPointConf I870OutputPointConfStr;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/**
 * Callback Signatures
 */

typedef void* (*CALLBACK_mspGetPoint)(void *pHandle, TMWTYPES_USHORT index);

typedef TMWTYPES_BOOL (*CALLBACK_ccsnaSetTime)(TMWDTIME *pNewTime);

typedef void (*CALLBACK_ccsnaGetTime)(TMWDTIME *pNewTime);

//typedef TMWTYPES_UCHAR (*CALLBACK_mspGetFlagsAndValue)(void *pPoint);

typedef void* (*CALLBACK_mdpGetPoint)(void *pHandle, TMWTYPES_USHORT index);

typedef void* (*CALLBACK_mmenaGetPoint)(void *pHandle, TMWTYPES_USHORT index);

//typedef TMWTYPES_UCHAR (*CALLBACK_mmenaGetFlags)(void *pPoint);
//
//typedef TMWTYPES_SHORT (*CALLBACK_mmenaGetValue)(void *pPoint);

typedef void* (*CALLBACK_mmenbGetPoint)(void *pHandle, TMWTYPES_USHORT index);

//typedef TMWTYPES_UCHAR (*CALLBACK_mmenbGetFlags)(void *pPoint);
//
//typedef TMWTYPES_SHORT (*CALLBACK_mmenbGetValue)(void *pPoint);

typedef void* (*CALLBACK_mmencGetPoint)(void *pHandle, TMWTYPES_USHORT index);

//typedef TMWTYPES_UCHAR (*CALLBACK_mmencGetFlags)(void *pPoint);
//
//typedef TMWTYPES_SFLOAT (*CALLBACK_mmencGetValue)(void *pPoint);

typedef void* (*CALLBACK_mitGetPoint)(void *pHandle, TMWTYPES_USHORT index);

typedef void * (*CALLBACK_cscLookupPoint)(void *pHandle, TMWTYPES_ULONG ioa);

typedef TMWTYPES_BOOL (*CALLBACK_hasSectorReset)(void *pHandle,TMWTYPES_UCHAR *pResetCOI);

typedef TMWTYPES_BOOL (*CALLBACK_crpnaExecuteRTUReset)(TMWTYPES_UCHAR qrp);

typedef void * (*CALLBACK_cdcLookupPoint)(void *pHandle, TMWTYPES_ULONG ioa);

//typedef TMWTYPES_UCHAR (*CALLBACK_mitGetFlags)(void *pPoint);
//
//typedef TMWTYPES_ULONG (*CALLBACK_mitGetValue)(void *pPoint);

typedef TMWTYPES_BOOL (*CALLBACK_mitFreeze)(void *pHandle, TMWTYPES_UCHAR qcc);

typedef TMWDEFS_COMMAND_STATUS (*CALLBACK_cscSelect)( void* gdb, I870OutputPointConfStr groupID, TMWTYPES_UCHAR sco);

typedef TMWDEFS_COMMAND_STATUS (*CALLBACK_cscExecute)( void* gdb, I870OutputPointConfStr groupID, TMWTYPES_UCHAR sco);


typedef TMWDEFS_COMMAND_STATUS (*CALLBACK_cdcSelect)( void* gdb, I870OutputPointConfStr groupID, TMWTYPES_UCHAR dco);

typedef TMWDEFS_COMMAND_STATUS (*CALLBACK_cdcExecute)( void* gdb, I870OutputPointConfStr groupID, TMWTYPES_UCHAR dco);
//typedef TMWDEFS_COMMAND_STATUS (*CALLBACK_cscExecute)(void *pPoint,
//               TMWTYPES_UCHAR cot, TMWTYPES_UCHAR sco);

//typedef TMWDEFS_COMMAND_STATUS (*CALLBACK_cdcSelect)(void *pPoint,
//                TMWTYPES_UCHAR cot, TMWTYPES_UCHAR dco);
//
//typedef TMWDEFS_COMMAND_STATUS (*CALLBACK_cdcExecute)(void *pPoint,
//                TMWTYPES_UCHAR cot, TMWTYPES_UCHAR dco);


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
typedef enum {
    TIME_TAG_WITH_TIMETAG,
    TIME_TAG_WITHOUT_TIMETAG,
    TIME_TAG_BOTH,
}TIME_TAG; /* Duplicated enum with LU_TIMETGA_MODE*/

/*Structure holding output point info*/
struct I870OutputPointConf
{

    lu_uint16_t groupID;
    lu_uint32_t ioa;
    lu_bool_t   selectRequired;
    lu_bool_t   enable;
    TIME_TAG    timeTag;
};


/*This structure contains all the attributes common to all input points*/

typedef struct I870CommonElementsStr
{
    TMWTYPES_ULONG       ioa;
    TMWTYPES_BOOL        enable;
    TMWTYPES_BOOL        spontaneousEvent;
    TMWDEFS_GROUP_MASK   groupMask;
    TMWDTIME             dtime;
    TMWTYPES_UCHAR       flags;

    TMWDEFS_TIME_FORMAT  timeformat;
    TMWTYPES_BOOL        eventOnlyWhenOnline;   /* Only generate events when session is marked as online */
    TMWTYPES_BOOL        eventLogStore;         /* Mirror events to the RTU Event log */
}I870CommonElements;

/*Structure holding single point info*/
typedef struct I870DataMSPStr
{
    I870CommonElements   commonElements;
    TMWTYPES_UCHAR       value;   /*Note: contains value AND flags */
}I870DataMSP;


/*Structure holding double point info*/
typedef struct I870DataMDPStr
{
    I870CommonElements   commonElements;
    TMWTYPES_UCHAR       value;   /*Note: contains value AND flags */
}I870DataMDP;

/*Structure holding analog value (scaled, normalized & floating) info*/
typedef struct I870DataAnalogStr
{
    I870CommonElements    commonElements;
   TMWTYPES_ANALOG_VALUE value;   /*Note: contains value & type of analog*/
}I870DataAnalog;


/*Structure holding MIT value info*/
typedef struct I870DataMITStr
{
    I870CommonElements    commonElements;
    TMWTYPES_ULONG value;   /*Note: contains value only */
    /* Freeze-related parameters */
    CALLBACK_mitFreeze mitFreeze;   //I870 Database freeze command
    void*       pG3DB;               //Pointer to G3 database
    struct PointID
    {
        lu_uint16_t group;
        lu_uint16_t ID;
    } vPoint;                       //Virtual Point ID
}I870DataMIT;


/**
 * Lucy I870 Database handle structure.
 * It holds the S101/104 Database pointer and callback functions that can be
 * called by TMW library.
 *
 */
typedef struct I870OutputDef
{
    void*                  pG3DB;           //Pointer to G3 database
    /* Pointer to the sector Pointer */
    TMWSCTR **ppSectorHdle;
    I870OutputPointConfStr oPointConf;
    CALLBACK_cscSelect            cscSelect            ;
    CALLBACK_cscExecute           cscExecute           ;
    CALLBACK_cdcSelect            cdcSelect            ;
    CALLBACK_cdcExecute           cdcExecute           ;
}I870OutputStr;


/**
 * Lucy S101/104 Database handle structure.
 * It holds the S101/104 Database pointer and callback functions that can be
 * called by TMW library.
 *
 */
struct I870DBHandleDef
{
    void*       pIEC870DB;    // The pointer of G3 database
    TMWSCTR*    pTMWSector;   // The pointer of sector

    /* TMW Callbacks */
    CALLBACK_ccsnaSetTime         ccsnaSetTime         ;
    CALLBACK_ccsnaGetTime         ccsnaGetTime         ;
    CALLBACK_mspGetPoint          mspGetPoint          ;
    CALLBACK_mdpGetPoint          mdpGetPoint          ;
    CALLBACK_mmenaGetPoint        mmenaGetPoint        ;
    CALLBACK_mmenbGetPoint        mmenbGetPoint        ;
    CALLBACK_mmencGetPoint        mmencGetPoint        ;
    CALLBACK_mitGetPoint          mitGetPoint          ;
    CALLBACK_cscLookupPoint       cscLookupPoint       ;
    CALLBACK_cdcLookupPoint       cdcLookupPoint       ;
    CALLBACK_hasSectorReset       hasSectorReset       ;
    CALLBACK_crpnaExecuteRTUReset crpnaExecuteRTUReset;
    CALLBACK_deleteEventIDDef     deleteEventID;
};



#endif /* LUCY_I870_S14DATA_H_ */

/*
 *********************** End of file ******************************************
 */
