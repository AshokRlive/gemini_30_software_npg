/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14csenb.c
 * description: IEC 60870-5-101 slave csenb (scaled command)
 *  functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14csenb.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/utils/tmwtarg.h"

#if S14DATA_SUPPORT_CSE_B

#if S14DATA_SUPPORT_MULTICMDS
void TMWDEFS_CALLBACK s14csenb_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  S14SCTR *p14Sector;
  S14SCTR_CMD *pContext;
  TMWTYPES_BOOL timerIsActive;
  TMWTYPES_ULONG ioa;
  TMWTYPES_ULONG oldIOA; 
  TMWTYPES_SHORT oldSVA; 
  TMWTYPES_UCHAR oldQOS;
  
  pContext = s14util_processMultiRequest(pSector, pMsg, I14DEF_TYPE_CSENB1, &ioa, &timerIsActive);
  if(pContext == TMWDEFS_NULL)
    return;
  
  /* Command to call for status of this point */
  pContext->pStatus = s14data_csenbStatus;
  
  /* Save old values in case this is an execute and we need to compare with select values.
   * All of the error paths require the context fields to be filled in with rcvd values.
   */
  oldIOA = pContext->ioa;
  oldSVA = pContext->value.sval; 
  oldQOS = pContext->qualifier;  
  
  /* If only one context per typeId is allowed, this might be cancelling and replacing the previous ioa */
  pContext->ioa = ioa;

  /* Parse SVA */
  tmwtarg_get16(&pMsg->pRxData->pMsgBuf[pMsg->offset], (TMWTYPES_USHORT *)&pContext->value.sval);
  pContext->valueType = TMWTYPES_ANALOG_TYPE_SHORT;
  pContext->useValue = TMWDEFS_TRUE;
  pMsg->offset += 2;

  /* Parse QOS */
  pContext->qualifier = pMsg->pRxData->pMsgBuf[pMsg->offset++];
  pContext->useQualifier = TMWDEFS_TRUE;

  /* Parse received cause of transmission */
  if(pMsg->cot == I14DEF_COT_ACTIVATION)
  {
    pContext->cot = I14DEF_COT_ACTCON;
  }
  else if(pMsg->cot == I14DEF_COT_DEACTIVATION)
  {
     /* Any deactivate will cause the select to be cancelled, even an incorrect one 
     * There is currently not a s14data function to be called indicating deactivate.
     * 
     * The 101 and 601 specs are unclear about whether a DEACTCON positive or negative 
     * should be sent on error. Since even an incorrect DEACT results in no select being
     * active, a positive reply will be sent (except if ioa is bad below).
     */
    pContext->cot = I14DEF_COT_DEACTCON;
  }
  else
  {
    pContext->cot = I14DEF_COT_UNKNOWN_COT | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* Lookup data point using information object address */
  p14Sector = (S14SCTR *)pSector;
  pContext->pPoint = s14data_csenbLookupPoint(p14Sector->i870.pDbHandle, ioa);
  if(pContext->pPoint == TMWDEFS_NULL)
  {
    pContext->cot = I14DEF_COT_UNKNOWN_IOA | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* if deactivate, context->cot was set to DEACTCON above. */
  if(pMsg->cot == I14DEF_COT_DEACTIVATION)
  {
    return;
  }

  /* Perform select or execute and set operation status */
  if((pContext->qualifier & I14DEF_QOC_SE_SELECT) != 0)
  { 
    pContext->state = TMWDEFS_CMD_STATE_SELECTING;
    pContext->status = s14data_csenbSelect(pContext->pPoint, 
      pMsg->cot, pContext->value.sval, pContext->qualifier);
  }
  else
  {
    /* If a select is required for this point make sure a valid select
     * operation is pending and compare the execute parameters against
     * the select parameters.
     */
    if(s14data_csenbSelectRequired(pContext->pPoint))
    {
      /* Make sure select is active */
      /* Compare information object address */
      /* Compare qualifier, ignore the select bit */
      if(!timerIsActive
         ||(pContext->ioa != oldIOA)
         ||(pContext->value.sval != oldSVA)
         ||(pContext->qualifier != (oldQOS & ~I14DEF_QOC_SE_MASK)))
      {
        pContext->cot |= I14DEF_COT_NEGATIVE_CONFIRM;
        return;
      }
    }

    pContext->state = TMWDEFS_CMD_STATE_EXECUTING;
    pContext->status = s14data_csenbExecute(pContext->pPoint, 
      pMsg->cot, pContext->value.sval, pContext->qualifier);
  }
}

#else /* !S14DATA_SUPPORT_MULTICMDS */
/* function: _checkStatus */
static TMWDEFS_COMMAND_STATUS TMWDEFS_CALLBACK _checkStatus(
  TMWSCTR *pSector)
{
  return (s14csenb_checkStatus(pSector));
}

/* function: csenb_checkStatus */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14csenb_checkStatus(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  switch(p14Sector->csenbState)
  {
  case TMWDEFS_CMD_STATE_SELECTING:

    if((p14Sector->csenbStatus != TMWDEFS_CMD_STAT_SUCCESS) 
      && (p14Sector->csenbStatus != TMWDEFS_CMD_STAT_FAILED))
    {
      /* Get Status */
      p14Sector->csenbStatus = s14data_csenbStatus(p14Sector->pCSENBPoint);
    }
    
    /* If select is complete start select timer */
    if(p14Sector->csenbStatus == TMWDEFS_CMD_STAT_SUCCESS)
    {
      tmwtimer_start(&p14Sector->csenbSelectTimer, 
        p14Sector->selectTimeout, pSector->pSession->pChannel, 
        TMWDEFS_NULL, TMWDEFS_NULL);
    }

    break;

  case TMWDEFS_CMD_STATE_EXECUTING:  

    if((p14Sector->csenbStatus != TMWDEFS_CMD_STAT_SUCCESS) 
      && (p14Sector->csenbStatus != TMWDEFS_CMD_STAT_FAILED))
    {
      /* Get Status */
      p14Sector->csenbStatus = s14data_csenbStatus(p14Sector->pCSENBPoint);
    }
    break;

  default:
    break;
  }

  /* If command failed return to IDLE state */
  if(p14Sector->csenbStatus == TMWDEFS_CMD_STAT_FAILED)
    p14Sector->csenbState = TMWDEFS_CMD_STATE_IDLE;

  return(p14Sector->csenbStatus);
}

/* function: s14csenb_processRequest */
void TMWDEFS_CALLBACK s14csenb_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  S14SCTR *p14Sector;
  TMWTYPES_BOOL timerIsActive;
  TMWTYPES_ULONG oldIOA;
  TMWTYPES_SHORT oldSVA;
  TMWTYPES_UCHAR oldQOS;

  p14Sector = (S14SCTR *)pSector;

  /* Any new or failed command cancels the select timer 
   * Save the timer state in case this is a valid execute command
   */
  timerIsActive = tmwtimer_isActive(&p14Sector->csenbSelectTimer);
  if(timerIsActive)
  {
    tmwtimer_cancel(&p14Sector->csenbSelectTimer);
  }
 
  /* If there is a previous command in progress, (response still to be sent)
   * queue a negative ACT CON to be sent. 
   * Don't overwrite the information from the previous command.
   */
  if((pMsg->cot == I14DEF_COT_ACTIVATION)
    && ((p14Sector->csenbCOT != 0) || (p14Sector->csetbCOT != 0)))
  {
    s14sesn_sendNegActCon(pSector, pMsg, I14DEF_COT_ACTCON); 
    return;
  }

  /* Store originator address */
  p14Sector->csenbOriginator = pMsg->origAddress;

  /* Save old values in case this is an execute and we need to compare with select values.
   * All of the error paths require the p14Sector fields to be filled in with rcvd values.
   */
  oldIOA = p14Sector->csenbIOA;
  oldSVA = p14Sector->csenbSVA;
  oldQOS = p14Sector->csenbQOS;

  /* Parse Information Object Address */
  i870util_readInfoObjAddr(pSector->pSession, pMsg, &p14Sector->csenbIOA);

  /* Parse SVA */
  tmwtarg_get16(&pMsg->pRxData->pMsgBuf[pMsg->offset], (TMWTYPES_USHORT *)&p14Sector->csenbSVA);
  pMsg->offset += 2;

  /* Parse QOS */
  p14Sector->csenbQOS = pMsg->pRxData->pMsgBuf[pMsg->offset++];

  /* Parse received cause of transmission */
  if(pMsg->cot == I14DEF_COT_ACTIVATION)
  {
    p14Sector->csenbCOT = I14DEF_COT_ACTCON;
  }
  else if(pMsg->cot == I14DEF_COT_DEACTIVATION)
  {
    /* Any deactivate will cause the select to be cancelled, even an incorrect one 
     * There is currently not a s14data function to be called indicating deactivate.
     * 
     * The 101 and 601 specs are unclear about whether a DEACTCON positive or negative 
     * should be sent on error. Since even an incorrect DEACT results in no select being
     * active, a positive reply will be sent (except if ioa is bad below).
     */
    p14Sector->csenbCOT = I14DEF_COT_DEACTCON;
  }
  else
  {
    p14Sector->csenbCOT = I14DEF_COT_UNKNOWN_COT | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* Lookup data point using information object address */
  p14Sector->pCSENBPoint = s14data_csenbLookupPoint(p14Sector->i870.pDbHandle, p14Sector->csenbIOA);
  if(p14Sector->pCSENBPoint == TMWDEFS_NULL)
  {
    p14Sector->csenbCOT = I14DEF_COT_UNKNOWN_IOA | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* if deactivate, command is finished */
  if(pMsg->cot == I14DEF_COT_DEACTIVATION)
  {
    return;
  }

  /* Perform select or execute and set operation status */
  if((p14Sector->csenbQOS & I14DEF_QOC_SE_SELECT) != 0)
  {
    p14Sector->csenbState = TMWDEFS_CMD_STATE_SELECTING;
    p14Sector->csenbStatus = s14data_csenbSelect(p14Sector->pCSENBPoint, 
      pMsg->cot, p14Sector->csenbSVA, p14Sector->csenbQOS);
  }
  else
  {
    /* If a select is required for this point make sure a valid select
     * operation is pending and compare the execute parameters against
     * the select parameters.
     */
    if(s14data_csenbSelectRequired(p14Sector->pCSENBPoint))
    {
      /* Make sure select is active */
      /* Compare information object address */
      /* Compare qualifier, ignore the select bit */
      if(!timerIsActive
         ||(p14Sector->csenbIOA != oldIOA)
         ||(p14Sector->csenbSVA != oldSVA)
         ||(p14Sector->csenbQOS != (oldQOS & ~I14DEF_QOC_SE_MASK)))
      {
        p14Sector->csenbCOT |= I14DEF_COT_NEGATIVE_CONFIRM;
        return;
      }
    }

    p14Sector->csenbState = TMWDEFS_CMD_STATE_EXECUTING;
    p14Sector->csenbStatus = s14data_csenbExecute(p14Sector->pCSENBPoint, 
      pMsg->cot, p14Sector->csenbSVA, p14Sector->csenbQOS);
  }
}

/* function: s14csenb_buildResponse */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14csenb_buildResponse(
  TMWSCTR *pSector, 
  TMWTYPES_BOOL buildResponse)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  return(s14util_buildCommandResponse(pSector, buildResponse, 
    I14DEF_TYPE_CSENB1, &p14Sector->csenbCOT, p14Sector->csenbOriginator,
    p14Sector->csenbIOA, p14Sector->csenbQOS, TMWDEFS_TRUE, &p14Sector->csenbSVA, 2, TMWDEFS_NULL, 
    _checkStatus));
}
#endif /* !S14DATA_SUPPORT_MULTICMDS */
#endif /* S14DATA_SUPPORT_CSE_B */
