/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:AbstractSlaveProtocolChannel.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   27 Oct 2014     andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "AbstractSlaveProtocolChannel.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
AbstractSlaveProtocolChannel::~AbstractSlaveProtocolChannel()
{
    deleteSessions();
}

void AbstractSlaveProtocolChannel::addScadaAddress(const ScadaAddress& newAddress)
{
    m_scadaAddresses.push_back(newAddress);
    m_numAddresses++;
}

void AbstractSlaveProtocolChannel::setConnectionManagerEnabled(bool val)
{
    m_connectionManagerEnabled = val;
}

void AbstractSlaveProtocolChannel::setConnectionManagerConfig(ISCADAConnectionManager::SCADAConnManConfig scadaConnManConfig)
{
    if (connMgr == NULL)
    {
        return;
    }

    connMgr->setSCADAConnManConfig(scadaConnManConfig);

}

SCADAP_ERROR AbstractSlaveProtocolChannel::addSession(ISlaveProtocolSession &session)
{
    m_SlaveSessions.push_back(&session);

    return SCADAP_ERROR_NONE;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

bool AbstractSlaveProtocolChannel::setNextSCADAAddress()
{
    if (m_numAddresses == 0)
    {
        return false;
    }

    m_curAddressIdx++;

    if (m_curAddressIdx >= m_scadaAddresses.size())
    {
        m_curAddressIdx = 0;
    }

    DBG_INFO("Current SCADA set to [%s]", m_scadaAddresses[m_curAddressIdx].ip.c_str());

    return true;
}

bool AbstractSlaveProtocolChannel::setCurrentMaster(lu_uint32_t masterAddress)
{
    for (lu_uint32_t i = 0; i < m_scadaAddresses.size(); i++)
    {
        if (m_scadaAddresses[i].masterAddress == masterAddress)
        {
            m_curAddressIdx = i;
            return true;
        }
    }

    return false;
}

bool AbstractSlaveProtocolChannel::findSCADAIPAddress(std::string ipAddress)
{
    for (lu_uint32_t i = 0; i < m_scadaAddresses.size(); i++)
    {
        if (m_scadaAddresses[i].ip == ipAddress)
        {
            return true;
        }
    }

    return false;
}

bool AbstractSlaveProtocolChannel::getSCADAAddressIndex(lu_uint32_t index, ScadaAddress& scadaAddr)
{
    if (m_numAddresses == 0)
    {
        return false;
    }

    scadaAddr = m_scadaAddresses[index];

    return true;
}

bool AbstractSlaveProtocolChannel::getCurrentSCADAAddress(ScadaAddress& scadaAddr)
{
    return getSCADAAddressIndex(m_curAddressIdx, scadaAddr);
}

SCADAP_ERROR AbstractSlaveProtocolChannel::deleteSessions()
{
    // Delete Session Objects
    for (lu_uint32_t i = 0; i < m_SlaveSessions.size(); i++)
    {
        delete m_SlaveSessions[i];
    }

    // Empty the vector
    m_SlaveSessions.clear();

    return SCADAP_ERROR_NONE;
}

bool AbstractSlaveProtocolChannel::setMasterAddress(lu_uint32_t masterAddress)
{
    if (setCurrentMaster(masterAddress) == false)
    {
        return false;
    }

    DBG_INFO("Setting Master Address to [%d] in ALL Sessions", masterAddress);

    // Loop over all the sessions, inserting new Master Address
    for (SessionVector::iterator itSession  = m_SlaveSessions.begin();
                                 itSession != m_SlaveSessions.end();
                                 ++itSession)
    {
        ISlaveProtocolSession *session = *(itSession);

        // Set the master address in the session's config too, this is in case
        // the session has not been opened yet
        session->setMasterAddress(masterAddress);
    }

    return true;
}

void AbstractSlaveProtocolChannel::setSessionsOnline(bool online)
{
    // Loop over all the sessions, inserting new Master Address
    for (SessionVector::iterator itSession  = m_SlaveSessions.begin();
                                 itSession != m_SlaveSessions.end();
                                 ++itSession)
    {
        ISlaveProtocolSession *session = *(itSession);

        // Set the master address in the session's config too, this is in case
        // the session has not been opened yet
        session->setOnline(online);
    }

}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
