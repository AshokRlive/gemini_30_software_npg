/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14crpna.c
 * description: IEC 60870-5-101 slave CRPNA (Reset Process Command)
 *  functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/utils/tmwtarg.h"
#include "tmwscl/i870/s14crpna.h"
#include "tmwscl/i870/s14rbe.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/i870chnl.h"

#if S14DATA_SUPPORT_CRPNA

/* function: _doResetProcess */
static void TMWDEFS_CALLBACK _doResetProcess(
  void *pCallbackParam,
  TMWSESN_TX_DATA *pTxData)
{
  TMWSCTR *pSector = (TMWSCTR *)pCallbackParam;
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  TMWTARG_UNUSED_PARAM(pTxData);
   
  s14data_crpnaExecute(pSector, p14Sector->crpnaQRP);
}

/* function: s14crpna_processRequest */
void TMWDEFS_CALLBACK s14crpna_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  
  /* Store originator address */
  p14Sector->crpnaOriginator = pMsg->origAddress;

  /* Parse Information Object Address */
  i870util_readInfoObjAddr(pSector->pSession, pMsg, &p14Sector->crpnaIOA);

  /* Parse Qualifier of Reset Process Command */
  p14Sector->crpnaQRP = pMsg->pRxData->pMsgBuf[pMsg->offset++]; 
  
  /* Parse received cause of transmission */
  if(pMsg->cot == I14DEF_COT_ACTIVATION)
  {
    p14Sector->crpnaCOT = I14DEF_COT_ACTCON;
  }
  else
  {
    p14Sector->crpnaCOT = I14DEF_COT_UNKNOWN_COT | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  if(p14Sector->crpnaIOA != 0)
  {
    p14Sector->crpnaCOT = I14DEF_COT_UNKNOWN_IOA | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  if(p14Sector->crpnaQRP == I14DEF_QRP_RESET_EVENTS)
  {
    /* Close and reopen report by exception handling
     * to reset the event buffers, discarding any events
     */
    s14rbe_closeSector(pSector);
    s14rbe_initSector(pSector);
  }
  else
  {
    /* Call target code to make sure this qualifier is supported */
    if(!s14data_crpnaSupport(pSector, p14Sector->crpnaQRP))
      p14Sector->crpnaCOT |= I14DEF_COT_NEGATIVE_CONFIRM;
  }
}

/* function: s14crpna_buildResponse */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14crpna_buildResponse(
  TMWSCTR *pSector, 
  TMWTYPES_BOOL buildResponse)
{
  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  if(p14Sector->crpnaCOT != 0)
  {
    if(buildResponse)
    {
      /* Activation confirmation or Deactivation */
      TMWSESN_TX_DATA *pTxData;
      S14SESN *pS14Session = (S14SESN *)pSector->pSession;
        
      if((pTxData = i870chnl_newTxData(pSector->pSession->pChannel, 
        pSector->pSession, pSector, pS14Session->maxASDUSize)) == TMWDEFS_NULL)
      {
        return(TMWDEFS_FALSE);
      } 
      
#if TMWCNFG_SUPPORT_DIAG
      pTxData->pMsgDescription = "Reset Process Response";
#endif
      pTxData->txFlags = TMWSESN_TXFLAGS_NO_RESPONSE;
      pTxData->responseTimeout = p14Sector->defaultResponseTimeout;

      /* Build response */
      i870util_buildMessageHeader(pSector->pSession, 
        pTxData, I14DEF_TYPE_CRPNA1, p14Sector->crpnaCOT, 
        p14Sector->crpnaOriginator, p14Sector->i870.asduAddress);

      /* Store point number */
      i870util_storeInfoObjAddr(pSector->pSession, pTxData, p14Sector->crpnaIOA);

      /* Store qualifier of reset process command */
      pTxData->pMsgBuf[pTxData->msgLength++] = p14Sector->crpnaQRP;

      if(p14Sector->crpnaCOT == I14DEF_COT_ACTCON)
      {
        /* doResetProcess must be called After response is sent */
        pTxData->pAfterTxCallback = _doResetProcess;
        pTxData->pCallbackData = pSector;
      }

      /* Request is complete */
      p14Sector->crpnaCOT = 0;

      /* Send the response */
      i870chnl_sendMessage(pTxData);
    }

    return(TMWDEFS_TRUE);
  }

  return(TMWDEFS_FALSE);
}

#endif /* S14DATA_SUPPORT_CRPNA */
