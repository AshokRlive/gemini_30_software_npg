/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14cicna.c
 * description: IEC 60870-5-101 slave CICNA (general interrogation)
 *  functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14cicna.h"
#include "tmwscl/i870/s14dbas.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/i14diag.h"
#include "tmwscl/i870/i870chnl.h"

#if S14DATA_SUPPORT_CICNA

/* function: _responseNotConfirmed 
 * purpose:   since the response was not confirmed by the master
 *  reset cot so it will be sent again. This handles the case where
 *  a balanced slave does not know that a master has restarted and 
 *  needs a link reset before it will accept confirmed user data from
 *  slave.
 * arguments: 
 *  pCallbackParam - unused
 *  pTxData - unused
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _responseNotConfirmed(
  void *pCallbackParam,
  TMWSESN_TX_DATA *pTxData)
{ 
  S14SCTR *p14Sector = (S14SCTR *)pTxData->pSector;
  TMWTARG_UNUSED_PARAM(pCallbackParam);

  /* if ACTTERM now, then ACTCON was not confirmed, set cicnaCOT
   * so it will be sent again
   */
  if(p14Sector->cicnaCOT == I14DEF_COT_ACTTERM)
  {
    p14Sector->cicnaCOT = I14DEF_COT_ACTCON;
  }
}

/* function: s14cicna_processRequest */
void TMWDEFS_CALLBACK s14cicna_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  /* Store originator address */
  p14Sector->cicnaOriginator = pMsg->origAddress;

  /* Parse Information Object Address */
  i870util_readInfoObjAddr(pSector->pSession, pMsg, &p14Sector->cicnaIOA);

  /* Parse qualifier of interrogation */
  p14Sector->cicnaQualifier = pMsg->pRxData->pMsgBuf[pMsg->offset++];

  /* Parse received cause of transmission */
  if(pMsg->cot == I14DEF_COT_ACTIVATION)
    p14Sector->cicnaCOT = I14DEF_COT_ACTCON;
  else if(pMsg->cot == I14DEF_COT_DEACTIVATION)
    p14Sector->cicnaCOT = I14DEF_COT_DEACTCON;
  else
  {
    p14Sector->cicnaCOT = I14DEF_COT_UNKNOWN_COT | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  if(p14Sector->cicnaIOA != 0)
  {
    p14Sector->cicnaCOT = I14DEF_COT_UNKNOWN_IOA | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

#if S14DATA_SUPPORT_RTU_CICNA  
  {
  S14SESN *pS14Session = (S14SESN *)pSector->pSession;
  TMWTYPES_USHORT globalAddress = (pS14Session->i870.asduAddrSize == 1) 
    ? I14DEF_BROADCAST_ADDRESS_8 : I14DEF_BROADCAST_ADDRESS_16;

  if(pMsg->asduAddress == globalAddress)
    p14Sector->cicnaIsBroadcast = TMWDEFS_TRUE;
  else
    p14Sector->cicnaIsBroadcast = TMWDEFS_FALSE;
  }
#endif

  if((p14Sector->cicnaQualifier < I14DEF_QOI_STATION_GLOBAL)
    || (p14Sector->cicnaQualifier > I14DEF_QOI_GROUP_16))
  {
    /* See if this is a private or reserved QOI value the user wants to support */
    p14Sector->groupMask = s14data_convertQOItoGroupMask(p14Sector->i870.pDbHandle, p14Sector->cicnaQualifier);
    if(p14Sector->groupMask == 0)
    {
      p14Sector->cicnaCOT |= I14DEF_COT_NEGATIVE_CONFIRM;
      return;
    }
  }
  else
  { 
    p14Sector->groupMask =  (TMWTYPES_ULONG)(0x01 << (p14Sector->cicnaQualifier - I14DEF_QOI_STATION_GLOBAL));
  }

  /* Initialize for cicna response */
  p14Sector->cicnaGroupIndex = 0;
  p14Sector->cicnaPointIndex = 0;
  p14Sector->cicnaDataReady  = TMWDEFS_FALSE;
}

/* function: s14cicna_buildResponse */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14cicna_buildResponse(
  TMWSCTR *pSector, 
  TMWTYPES_BOOL buildResponse)
{
  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  /* See if a General Interrogation request is pending */
  if(p14Sector->cicnaCOT != 0)
  {

#if S14DATA_SUPPORT_RTU_CICNA
    /* Special code to support sending ACT CON from physical RTU, before sending ACT CON from logical units
     * and then to send data and ACT TERM from logical units before sending ACT TERM from physical RTU
     * as required by Gasunie. This is not specified in 101/104 specs and is only necessary if that behavior
     * is desired.
     */
    if(p14Sector->cicnaIsBroadcast 
      &&(p14Sector->cicnaCOT == I14DEF_COT_ACTCON) 
      &&(!s14data_CICNAResponseReady(p14Sector->i870.pDbHandle, p14Sector->cicnaCOT)))
    {
      /* Don't send response yet, wait for other sectors first */
      return(TMWDEFS_FALSE);
    }
#endif

    /* If COT is ACTTERM this means it will try to read data
     * See if database said it was ready yet.
     * Send ACTCON or DEACTCON right away.
     */
    if((p14Sector->cicnaCOT == I14DEF_COT_ACTTERM) 
      &&(!p14Sector->cicnaDataReady))
    {
      p14Sector->cicnaDataReady = s14data_dataReady(p14Sector->i870.pDbHandle, I14DEF_TYPE_CICNA1);
   
      if(!p14Sector->cicnaDataReady)
      {
        /* Database is not ready, so no GI data to send at this time */
        return(TMWDEFS_FALSE);
      }
    }

    /* See if we need to actually build the response */
    if(buildResponse)
    {
      TMWSESN_TX_DATA *pTxData;
      S14SESN *pS14Session = (S14SESN *)pSector->pSession;
        
      if((pTxData = i870chnl_newTxData(pSector->pSession->pChannel, 
        pSector->pSession, pSector, pS14Session->maxASDUSize)) == TMWDEFS_NULL)
      {
        return(TMWDEFS_FALSE);
      } 

#if TMWCNFG_SUPPORT_DIAG
      pTxData->pMsgDescription = "General Interrogation Response";
#endif
      pTxData->txFlags = TMWSESN_TXFLAGS_NO_RESPONSE; 
      pTxData->responseTimeout = p14Sector->defaultResponseTimeout;
      
      /* Generate appropriate response based on current state */
      if(p14Sector->cicnaCOT != I14DEF_COT_ACTTERM)
      {
        /* Activation confirmation or Deactivation */

        /* Build response */
        i870util_buildMessageHeader(pSector->pSession, 
          pTxData, I14DEF_TYPE_CICNA1, p14Sector->cicnaCOT, 
          p14Sector->cicnaOriginator, p14Sector->i870.asduAddress);

        /* Store point number */
        i870util_storeInfoObjAddr(pSector->pSession, pTxData, p14Sector->cicnaIOA);

        /* Qualifier of Interrogation */
        pTxData->pMsgBuf[pTxData->msgLength++] = p14Sector->cicnaQualifier;

        /* If activation confirmation change to next state, else cicna
         * request is complete
         */
        if(p14Sector->cicnaCOT == I14DEF_COT_ACTCON)
          p14Sector->cicnaCOT = I14DEF_COT_ACTTERM;
        else
          p14Sector->cicnaCOT = 0;
 
        pTxData->pFailedTxCallback = _responseNotConfirmed;

        /* Send the response */
        i870chnl_sendMessage(pTxData);
      }
      else
      {
        TMWTYPES_UCHAR cot;
        TMWTYPES_BOOL dataSent = TMWDEFS_FALSE;

        if((p14Sector->cicnaQualifier < I14DEF_QOI_STATION_GLOBAL)
          || (p14Sector->cicnaQualifier > I14DEF_QOI_GROUP_16))
        {
          /* If reserved or private range QOI is used, 101
           * does not specifically say what COT to send back,
           * we will send back COT for general interrogation
           */
          cot = I14DEF_QOI_STATION_GLOBAL;
        }
        else
        {
          cot = p14Sector->cicnaQualifier;
        }

        if(s14dbas_readGroup(
          pTxData, pSector, cot, p14Sector->groupMask, 
          &p14Sector->cicnaGroupIndex, &p14Sector->cicnaPointIndex))
        {
          dataSent = TMWDEFS_TRUE;
          i870chnl_sendMessage(pTxData);
        }

        /* No data returned above so send ACTTERM */
        if(!dataSent)
        {

#if S14DATA_SUPPORT_RTU_CICNA
          if(p14Sector->cicnaIsBroadcast 
            &&(!s14data_CICNAResponseReady(p14Sector->i870.pDbHandle, I14DEF_COT_ACTTERM)))
          {
            /* Don't send response yet, wait for other sectors first */
            return(TMWDEFS_FALSE);
          }
#endif

          /* Build ACTTERM */
          i870util_buildMessageHeader(pSector->pSession, 
            pTxData, I14DEF_TYPE_CICNA1, p14Sector->cicnaCOT, 
            p14Sector->cicnaOriginator, p14Sector->i870.asduAddress);

          p14Sector->cicnaCOT = 0;

          /* Store point number, always 0 for ACTTERM */
          i870util_storeInfoObjAddr(pSector->pSession, pTxData, 0);

          /* Qualifier of Interrogation */
          pTxData->pMsgBuf[pTxData->msgLength++] = p14Sector->cicnaQualifier;

          /* Send response */
          i870chnl_sendMessage(pTxData);
           
#if S14DATA_SUPPORT_CICNAWAIT
          if(!p14Sector->CICNAComplete)
          {
            /* If this is the end of initial CICNA, set cyclic timer small to generate cyclic data */
            p14Sector->CICNAComplete = TMWDEFS_TRUE;
            if(pS14Session->cyclicWaitCICNAComplete &&
               (p14Sector->cyclicPeriod > 0))
            {
              tmwtimer_start(&p14Sector->cyclicTimer, 
                1, pSector->pSession->pChannel,
                s14sctr_cyclicTimeout, pSector);
            }
          }
#endif
        }
      }
    }

    /* Return true (i.e. data available, or sent) */
    return(TMWDEFS_TRUE);
  }

  return(TMWDEFS_FALSE);
}

#endif /* S14DATA_SUPPORT_CICNA */
