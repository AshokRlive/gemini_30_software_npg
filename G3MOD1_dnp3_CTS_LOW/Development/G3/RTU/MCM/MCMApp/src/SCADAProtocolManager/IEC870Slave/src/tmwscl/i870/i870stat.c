/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i870stat.c
 * description: I870 Statistics
 */ 
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/i870stat.h"
#include "tmwscl/utils/tmwchnl.h"

#if TMWCNFG_SUPPORT_STATS
void TMWDEFS_GLOBAL i870stat_chnlError(
  TMWCHNL *pChannel,
  TMWSESN *pSession,
  TMWCHNL_ERROR_CODE errorCode)
{  
  TMWCHNL_STAT_ERROR_TYPE errorInfo;
  errorInfo.errorCode = errorCode;
  errorInfo.pSession = pSession;
  TMWCHNL_STAT_CALLBACK_FUNC(pChannel, TMWCHNL_STAT_ERROR, &errorInfo);
}

void TMWDEFS_GLOBAL i870stat_checkStatus(
  TMWSESN *pSession,
  I870CHNL_RESP_STATUS status)
{
  if(status == I870CHNL_RESP_STATUS_TIMEOUT)
    TMWSESN_STAT_CALLBACK_FUNC(pSession, TMWSESN_STAT_REQUEST_TIMEOUT, TMWDEFS_NULL);
  else if(status == I870CHNL_RESP_STATUS_FAILURE)
    TMWSESN_STAT_CALLBACK_FUNC(pSession, TMWSESN_STAT_REQUEST_FAILED, TMWDEFS_NULL);
}
#endif
