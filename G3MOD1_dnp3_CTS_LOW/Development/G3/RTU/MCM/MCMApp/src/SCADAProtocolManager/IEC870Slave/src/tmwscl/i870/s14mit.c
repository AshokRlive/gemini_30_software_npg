/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14mit.c
 * description: IEC 60870-5-101 slave integrated totals functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14mit.h"
#include "tmwscl/i870/s14mem.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14evnt.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/i870util.h"
#include "tmwscl/utils/tmwtarg.h"

#if S14DATA_SUPPORT_MIT

/* Forward declaration */
static void TMWDEFS_LOCAL _initEventDesc(
  TMWSCTR *pSector,
  S14EVNT_DESC *pDesc);

/* function: _internalAddEvent
 * purpose: Internal function called by SCL to add an event to the queue.
 *  This will not tell the link layer to ask the application for a message
 *  to send. 
 * arguments:
 *  pSector - pointer to sector structure returned by s14sctr_openSector
 *  cot - cause of transmission
 *  ioa - Information object address
 *  value -
 *  quality -
 *  pTimeStamp - pointer to time structures
 * returns:
 *  void *
 */
static void * TMWDEFS_LOCAL _internalAddEvent(
  TMWSCTR *pSector, 
  TMWTYPES_UCHAR cot,
  TMWTYPES_ULONG ioa, 
  TMWTYPES_LONG value, 
  TMWTYPES_UCHAR quality, 
  TMWDTIME *pTimeStamp)
{
  S14EVNT_DESC desc;
  S14MIT_EVENT *pEvent;

  _initEventDesc(pSector, &desc);

#if S14DATA_SUPPORT_DOUBLE_TRANS
  {
  S14MIT_EVENT *pDoubleTransEvent;
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  if(p14Sector->mitTransmissionMode == S14DATA_TRANSMISSION_DOUBLE)
  {
    desc.doubleTransmission = TMWDEFS_TRUE;
  }
  else if(p14Sector->mitTransmissionMode == S14DATA_TRANSMISSION_PERPOINT)
  { 
    void *pPoint = s14data_mitLookupPoint(p14Sector->i870.pDbHandle, ioa);
    if((pPoint != TMWDEFS_NULL)
      && s14data_mitGetTransmissionMode(pPoint))
    {
      desc.doubleTransmission = TMWDEFS_TRUE;
    }
  }
  pEvent = (S14MIT_EVENT *)s14evnt_addEvent(pSector, cot, ioa, quality, pTimeStamp, &desc, (void **)&pDoubleTransEvent);

  if(pEvent != TMWDEFS_NULL)
  {
    pEvent->value = value; 
    /* Set value of lower priority double transmission entry */
    if(pDoubleTransEvent != TMWDEFS_NULL)
    {
      pDoubleTransEvent->value = value;
    } 
  }
  }
#else

  pEvent = (S14MIT_EVENT *)s14evnt_addEvent(pSector, cot, ioa, quality, pTimeStamp, &desc, TMWDEFS_NULL);

  if(pEvent != TMWDEFS_NULL)
  {
    pEvent->value = value;
  }
#endif
  return(pEvent);
}

/* function: _eventData 
 * purpose: Insert data from event structure into message to be sent
 * arguments:
 *  pTxData - pointer to transmit data structure
 *  pEvent - pointer to event to be put into message
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _eventData(
  TMWSESN_TX_DATA *pTxData, 
  S14EVNT *pEvent)
{
  S14MIT_EVENT *pMITEvent = (S14MIT_EVENT *)pEvent;

  /* Store value */
  tmwtarg_store32((TMWTYPES_ULONG *)&pMITEvent->value, &pTxData->pMsgBuf[pTxData->msgLength]);
  pTxData->msgLength += 4;

  /* Store quality */
  pTxData->pMsgBuf[pTxData->msgLength++] = pEvent->quality;
}

/* function: _initEventDesc 
 * purpose: Initialize event descriptor
 * arguments:
 *  pSector - identifies sector
 *  pDesc - pointer to descriptor structure 
 * returns:
 *  void
 */
static void TMWDEFS_LOCAL _initEventDesc(
  TMWSCTR *pSector,
  S14EVNT_DESC *pDesc)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  pDesc->typeId = I14DEF_TYPE_MITNA1;
  pDesc->woTimeTypeId = I14DEF_TYPE_MITNA1;
  pDesc->cotSpecified = 0;
  pDesc->eventMemType = S14MEM_MIT_EVENT_TYPE;
  pDesc->pEventList = &p14Sector->mitEvents;
  pDesc->eventMode = p14Sector->mitEventMode;
  pDesc->scanEnabled = TMWDEFS_FALSE;
  pDesc->maxEvents = p14Sector->mitMaxEvents;
  pDesc->timeFormat = p14Sector->mitTimeFormat;
  pDesc->doubleTransmission = TMWDEFS_FALSE;
  pDesc->pEventsOverflowedFlag = &p14Sector->mitEventsOverflowed;
  pDesc->pChangedFunc = TMWDEFS_NULL;
  pDesc->pGetPointFunc = s14data_mitGetPoint;
  pDesc->pEventDataFunc = _eventData;
}

/* function: _storeInResponse */
static void TMWDEFS_CALLBACK _storeInResponse(
  TMWSESN_TX_DATA *pTxData, 
  void *pPoint,
  TMWDEFS_TIME_FORMAT timeFormat)
{
  TMWTYPES_ULONG value; 

  if(timeFormat == TMWDEFS_TIME_FORMAT_NONE)
  {
    value =  s14data_mitGetValue(pPoint);
    tmwtarg_store32(&value, &pTxData->pMsgBuf[pTxData->msgLength]);
    pTxData->msgLength += 4;
    pTxData->pMsgBuf[pTxData->msgLength++] = s14data_mitGetFlags(pPoint);
  }
  else
  {
    TMWDTIME timeStamp;
    TMWTYPES_UCHAR flags;
    value = s14data_mitGetValueFlagsTime(pPoint, &flags, &timeStamp);
    tmwtarg_store32(&value, &pTxData->pMsgBuf[pTxData->msgLength]);
    pTxData->msgLength += 4;
    pTxData->pMsgBuf[pTxData->msgLength++] = flags; 

    if(timeFormat == TMWDEFS_TIME_FORMAT_24)
    {
      i870util_write24BitTime(pTxData, &timeStamp, S14DATA_SUPPORT_GENUINE_TIME);
    }
    else if(timeFormat == TMWDEFS_TIME_FORMAT_56)
    {
      i870util_write56BitTime(pTxData, &timeStamp, S14DATA_SUPPORT_GENUINE_TIME);
    }
  }
}

/* function: s14mit_init */
void TMWDEFS_GLOBAL s14mit_init(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  tmwdlist_initialize(&p14Sector->mitEvents);
  p14Sector->mitEventsOverflowed = TMWDEFS_FALSE;
}

/* function: s14mit_close */
void TMWDEFS_GLOBAL s14mit_close(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  tmwdlist_destroy(&p14Sector->mitEvents, s14mem_free);
} 

/* function: s14mit_addEvent */
void * TMWDEFS_GLOBAL s14mit_addEvent(
  TMWSCTR *pSector, 
  TMWTYPES_UCHAR cot,
  TMWTYPES_ULONG ioa, 
  TMWTYPES_LONG value, 
  TMWTYPES_UCHAR quality, 
  TMWDTIME *pTimeStamp)
{
  void *pEvent;
  TMWTARG_LOCK_SECTION(&pSector->pSession->pChannel->lock);

  pEvent = _internalAddEvent(pSector, cot, ioa, value, quality, pTimeStamp);

  if(pEvent != TMWDEFS_NULL)
  {
    /* If an event was added tell link layer we have data */ 
    s14event_linkDataReady(pSector);
  }

  TMWTARG_UNLOCK_SECTION(&pSector->pSession->pChannel->lock);
  return(pEvent);
}

/* function: s14mit_countEvents */
TMWTYPES_USHORT TMWDEFS_GLOBAL s14mit_countEvents(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  return(s14evnt_countEvents(pSector, &p14Sector->mitEvents));
}

/* function: s14mit_processEvents */
S14EVNT_STATUS TMWDEFS_GLOBAL s14mit_processEvents(
  TMWSCTR *pSector, 
  TMWDTIME *pEventTime)
{
  S14EVNT_DESC desc;
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  TMWTYPES_UCHAR typeId = I14DEF_TYPE_MITNA1;

  /* If clock sync response is still to be sent, don't send spontaneous events.
   * This allows ACTCON to be sent before events when using freeze Mode D.
   * 
   * Andrew West and I think the conformance test is wrong to require this.
   * Until this is resolved we will not use this change. 
   * if(p14Sector->ccinaCOT != 0)
   * {
   *   return(S14EVNT_NOT_SENT);
   * }
   */

  if(p14Sector->mitTimeFormat == TMWDEFS_TIME_FORMAT_24)
  {
    typeId = I14DEF_TYPE_MITTA1;
  }
  else if(p14Sector->mitTimeFormat == TMWDEFS_TIME_FORMAT_56) 
  {
    typeId = I14DEF_TYPE_MITTB1;
  }

  _initEventDesc(pSector, &desc);
  desc.typeId = typeId;
  return(s14evnt_processEvents(pSector, &desc, 5, pEventTime, TMWDEFS_FALSE)); 
}

#if S14DATA_SUPPORT_DOUBLE_TRANS
/* function: s14mit_processDblTransEvents */
S14EVNT_STATUS TMWDEFS_GLOBAL s14mit_processDblTransEvents(
  TMWSCTR *pSector)
{ 
  S14EVNT_DESC desc;
  _initEventDesc(pSector, &desc);
  return(s14evnt_processEvents(pSector, &desc, 5, TMWDEFS_NULL, TMWDEFS_TRUE));
}
#endif

/* function: s14mit_readIntoResponse */
S14DBAS_GROUP_STATUS TMWDEFS_CALLBACK s14mit_readIntoResponse(
  TMWSESN_TX_DATA *pTxData, 
  TMWSCTR *pSector, 
  TMWTYPES_UCHAR cot, 
  TMWDEFS_GROUP_MASK groupMask, 
  TMWTYPES_ULONG ioa,
  TMWTYPES_USHORT *pPointIndex)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  return(s14util_readIntoResponse(pTxData, pSector, cot, 
    groupMask, ioa, I14DEF_TYPE_MITNA1, p14Sector->readTimeFormat, 5, pPointIndex, s14data_mitGetPoint, 
    s14data_mitGetGroupMask, s14data_mitGetInfoObjAddr, s14data_mitGetIndexed, s14data_getTimeFormat,
    _storeInResponse));
}

/* function: s14mit_processFreeze */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14mit_processFreeze(
  TMWSCTR *pSector, 
  TMWTYPES_UCHAR qcc,
  TMWDEFS_GROUP_MASK groupMask)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  TMWTYPES_BOOL isSuccess = TMWDEFS_TRUE;
  TMWTYPES_USHORT pointIndex = 0;
  void *pPoint;

  while((pPoint = s14data_mitGetPoint(p14Sector->i870.pDbHandle, pointIndex)) != TMWDEFS_NULL)
  {
    if((s14data_mitGetGroupMask(pPoint) & groupMask) != 0)
    {
      if(!s14data_mitFreeze(pPoint, qcc))
      {
        /* If any freeze fails, return failure, but try rest of points */
        isSuccess = TMWDEFS_FALSE;
      }
    }

    pointIndex += 1;
  }

  return(isSuccess);
}

#endif /* S14DATA_SUPPORT_MIT */
