/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i870lnk1.h
 * description: IEC 60870-5 FT12 Link Layer Implementation
 */
#ifndef I870LNK1_DEFINED
#define I870LNK1_DEFINED

#include "tmwscl/utils/tmwlink.h"
#include "tmwscl/utils/tmwchnl.h"
#include "tmwscl/i870/i870ft12.h"

#define I870LNK1_MAX_TX_BUFFER_LENGTH 261 /* This does not affect library. 
                                             Frame size is controlled by
                                             session maxASDUSize */
#define I870LNK1_MAX_RX_BUFFER_LENGTH 261

/* Define configuration parameters supported by this link layer 
 * implementation.
 */
typedef struct i870lnk1Config {

   /* Maximum receive frame size */
  TMWTYPES_USHORT rxFrameSize;

  /* Maximum transmit frame size. 
   * This is not used by library, but is left here for backward compatibility,
   * The max size of a frame is dependent on session maxASDUSize. 
   */  
  TMWTYPES_USHORT txFrameSize;   

  /* Link Address Size, 0, 1, or 2 octets (for IEC 60870-5-103 it is always 1)*/
  TMWTYPES_UCHAR linkAddressSize;

  /* Link Mode, balanced or unbalanced */
  TMWDEFS_LINK_MODE linkMode;

  /* Allowed to send one character ack instead of fixed length ACK 
   * (secondary function code zero), in response to primary link
   * function codes 0, 1 and 3 in balanced transmission when acting as
   * an IEC 60870-5 Master, or if there is no access demand for class 
   * 1 data when acting as a IEC 60870-5 slave.   
   */
  TMWTYPES_BOOL oneCharAckAllowed; 

  /* Allowed to send one character response instead of a fixed length NACK
   * (secondary function code 9) when no respond data available 
   */
  TMWTYPES_BOOL oneCharResponseAllowed;

  /* Maximum amount of time to wait for a complete frame after
   * receiving the frame sync
   */
  TMWTYPES_MILLISECONDS rxFrameTimeout;

  /* When should we ask for link layer confirmations for variable
   * sized frames containing user data not sent to broadcast address. 
   * The options are:
   *  TMWDEFS_LINKCNFM_NEVER    SEND/NO REPLY function code
   *  TMWDEFS_LINKCNFM_ALWAYS   SEND/CONF function code
   *
   * NOTE: This will almost always be set to ALWAYS requesting
   *   a confirm for all variable sized frames except those sent to
   *   the broadcast address.
   */
  TMWDEFS_LINKCNFM confirmMode;

  /* Maximum amount of time to wait for a link level confirm
   * if requested.
   */
  TMWTYPES_MILLISECONDS confirmTimeout;

  /* Maximum number of link layer retries if link layer confirm
   * times out.
   */
  TMWTYPES_UCHAR maxRetries;

  /* For devices operating in balanced mode this parameter specifies how 
   * often to transmit a test frame to verify that the remote device is 
   * still online. 0 disables the periodic sending of test frames.
   * This parameter is ignored if linkmode is unbalanced
   * Since 103 is unbalanced this applies only to 101 master and slave devices.
   */
  TMWTYPES_MILLISECONDS testFramePeriod;

  /* This parameter specifies how often a session that is offline will
   * attempt to reestablish communication. This includes attempting to
   * open/reopen a channel and/or issueing request status messages as
   * appropriate for the current configuration.
   */
  TMWTYPES_MILLISECONDS offlinePollPeriod;

} I870LNK1_CONFIG;

/* Define bit masks used to specify which configuration parameters
 *  should be modified in a call to i870lnk1_modifyChannel. Parameters
 *  that do not have a corresponding bit mask can not be modified at
 *  runtime.
 */
#define I870LNK1_CONFIG_RX_FRAME_TIMEOUT   0x00000001
#define I870LNK1_CONFIG_CONFIRM_TIMEOUT    0x00000002
#define I870LNK1_CONFIG_MAX_RETRIES        0x00000004
#define I870LNK1_CONFIG_LINK_ADDR_SIZE     0x00000008


/* Prototype for user callback function */
typedef void (*I870LNK1_TX_CALLBACK_FUNC)(
  void *pCallbackParam,
  TMWSESN_TX_DATA *pTxData);


/* Include private information */
#include "tmwscl/i870/i870ln1p.h"

#ifdef __cplusplus
extern "C" {
#endif

  /* function: i870lnk1_initConfig
   * purpose: Initialize a i870lnk1 configuration data structure
   * arguments:
   *  pConfig - pointer to i870lnk1 configuration structure to be
   *   initialized.
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL i870lnk1_initConfig(
    I870LNK1_CONFIG *pConfig);

  /* function: i870lnk1_initChannel
   * purpose: initialize a new channel.
   * arguments:
   *  pConfig - pointer to i870lnk1 configuration information
   *  pPhys - pointer to physical layer interface to use
   *  pPhysContext - pointer to physical layer context
   * returns:
   *  A pointer to a context that is used to identify this channel
   *  for all future operations.
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL i870lnk1_initChannel(
    TMWCHNL *pChannel,
    const I870LNK1_CONFIG *pConfig);

  /* function: i870lnk1_getChannelConfig 
   * purpose: Get current configuration from a currently open channel
   * arguments:
   *  pChannel - channel to get configuration from
   *  pConfig - configuration data structure to be filled in
   * returns:
   *  TMWDEFS_TRUE if successful
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL i870lnk1_getChannelConfig(
    TMWCHNL *pChannel,
    I870LNK1_CONFIG *pConfig);

  /* function: i870lnk1_setChannelConfig 
    * purpose: Modify a currently open channel
   *  NOTE: normally i870lnk1_getChannelConfig() will be called
   *   to get the current config, some values will be changed 
   *   and this function will be called to set the values.
   * arguments:
   *  pChannel - channel to modify
   *  pConfig - configuration data structure
   * returns:
   *  TMWDEFS_TRUE if successful
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL i870lnk1_setChannelConfig(
    TMWCHNL *pChannel,
    const I870LNK1_CONFIG *pConfig);

  /* function: i870lnk1_modifyChannel 
   * DEPRECATED FUNCTION, SHOULD USE i870lnk1_setChannelConfig
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL i870lnk1_modifyChannel(
    TMWCHNL *pChannel,
    const I870LNK1_CONFIG *pConfig,
    TMWTYPES_ULONG configMask);

  /* function: i870lnk1_deleteChannel
   * purpose: delete this channel, freeing all allocated memory and releasing
   *  resources.
   * arguments:
   *  pContext - context returned from i870lnk1_initChannel
   * returns:
   *  TMWDEFS_TRUE if successful
   *  TMWDEFS_FALSE otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL i870lnk1_deleteChannel(
    TMWCHNL *pChannel);
  
  /* function: i870lnk1_sessionActive   
   * purpose: Tell link layer that this session has been made
   *  active again.
   * arguments:
   *  pSession - pointer to this session
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL i870lnk1_sessionActive(
    TMWSESN *pSession);

  /* function: i870lnk1_setFlowControl
   * purpose: Set flow control for this session. This will set the DFC bit appropriately, however
   *  if the master does not observe this bit and continues to send data the link layer will not 
   *  discard the data, but will try to process it if possible. Setting flow control can be used to
   *  test the master and to ask him to stop sending more data. If a full implementation of flow control
   *  is required, more work may need to be done, especially for 103 which should discard data and send 
   *  CONFIRM NACK (FC1) instead.
   * arguments:
   *  pSession - pointer to this session
   *  dfcValue - if TMWDEFS_TRUE  DFC bit==1 will be sent in appropriate messages
   *             if TMWDEFS_FALSE DFC bit==0 will be sent
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL i870lnk1_setFlowControl(
    TMWSESN *pSession,
    TMWTYPES_BOOL dfcValue);

  /* function: i870lnk1_registerCallback
   * purpose: User callback function to be called when message is given to link layer
   * Most implementations will not need to provide this callback function. This 
   * allows the message to be modified before it is transmitted for test purposes,
   * for example to modify some bytes or introduce errors.
   */
  void TMWDEFS_GLOBAL i870lnk1_registerCallback(
    TMWCHNL *pChannel,
    I870LNK1_TX_CALLBACK_FUNC pUserTxCallback,
    void *pCallbackParam);

#ifdef __cplusplus
}
#endif

#endif /* I870LNK1_DEFINED */
