/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14cnfg.h
 * description: IEC 60870-5-101/104 slave configuration definitions
 */
#ifndef S14CNFG_DEFINED
#define S14CNFG_DEFINED

#include "tmwscl/utils/tmwcnfg.h"

/* Specify for each event type the number of events that can be queued at once.
 * This number specifies the maximum number of events per data type for 
 * all 101/104 slaves in the system so it should generally be set to
 * the maximum number per sector times the number of sectors.
 */
#define S14CNFG_NUMALLOC_MBO_EVENTS      TMWCNFG_MAX_EVENTS
#define S14CNFG_NUMALLOC_MDP_EVENTS      TMWCNFG_MAX_EVENTS
#define S14CNFG_NUMALLOC_MEPTA_EVENTS    TMWCNFG_MAX_EVENTS
#define S14CNFG_NUMALLOC_MEPTB_EVENTS    TMWCNFG_MAX_EVENTS
#define S14CNFG_NUMALLOC_MEPTC_EVENTS    TMWCNFG_MAX_EVENTS
#define S14CNFG_NUMALLOC_MIT_EVENTS      TMWCNFG_MAX_EVENTS
#define S14CNFG_NUMALLOC_MITC_EVENTS     TMWCNFG_MAX_EVENTS
#define S14CNFG_NUMALLOC_MMENA_EVENTS    TMWCNFG_MAX_EVENTS
#define S14CNFG_NUMALLOC_MMENB_EVENTS    TMWCNFG_MAX_EVENTS
#define S14CNFG_NUMALLOC_MMENC_EVENTS    TMWCNFG_MAX_EVENTS
#define S14CNFG_NUMALLOC_MMEND_EVENTS    TMWCNFG_MAX_EVENTS
#define S14CNFG_NUMALLOC_MPSNA_EVENTS    TMWCNFG_MAX_EVENTS
#define S14CNFG_NUMALLOC_MSP_EVENTS      TMWCNFG_MAX_EVENTS
#define S14CNFG_NUMALLOC_MST_EVENTS      TMWCNFG_MAX_EVENTS

/* Specify the max number of slave 101/104 command contexts.
 * These are used when supporting multiple simultaneous commands for the 
 * same typeId. (S14DATA_SUPPORT_MULTICMDS==TMWDEFS_TRUE)
 * There are 8 different commands that use this pool of memory
 * if they are all supported. CSCNA/CSCTA, CDCNA/CDCTA, CRCNA/CRCTA, 
 * CBONA/CBOTA, CSENA/CSETA, CSENB/CSETB, CSENC/CSETC, CSENZ(Gasunie)
 */
#define S14CNFG_NUMALLOC_SCTR_CMDS       TMWDEFS_NO_LIMIT

/* Specify the max number of slave 101/104 simulated databases. The TMW SCL
 * will allocate a new simulated database for each slave 101/104 sector.
 * These are not be needed once an actual database is implemented.
 */
#define S14CNFG_NUMALLOC_SIM_DATABASES   TMWCNFG_MAX_SIM_DATABASES

/* Number of files and sections that can be stored in the simulated
 * databases. These two are only relevant if simulated database AND file transfer
 * are supported.
 */
#define S14CNFG_NUMALLOC_SIM_FILES       (TMWCNFG_MAX_SIM_DATABASES*8)
#define S14CNFG_NUMALLOC_SIM_SECTIONS    (S14CNFG_NUMALLOC_SIM_FILES*4)

#endif /* S14CNFG_DEFINED */
