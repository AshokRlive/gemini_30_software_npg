/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i14diag.h
 * description: This file is intended for internal SCL use only.
 *  IEC 60870-5-101/104 diagnostics
 */
#ifndef I14DIAG_DEFINED
#define I14DIAG_DEFINED

#include "tmwscl/utils/tmwcnfg.h"
#include "tmwscl/utils/tmwtarg.h"
#include "tmwscl/i870/i870sesn.h"
#include "tmwscl/i870/i870util.h"

#if !TMWCNFG_SUPPORT_DIAG

#define I14DIAG_SHOW_RESPONSE(pSector, pTxData, pMsg) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(pTxData); TMWTARG_UNUSED_PARAM(pMsg);

#define I14DIAG_SHOW_STORE_TYPE(pSector, pMsg) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(pMsg);

#define I14DIAG_SHOW_COI(pSector, coi) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(coi);

#define I14DIAG_SHOW_MSP(pSector, cot, ioa, flags, pTimeStamp) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(cot); TMWTARG_UNUSED_PARAM(ioa); TMWTARG_UNUSED_PARAM(flags); TMWTARG_UNUSED_PARAM(pTimeStamp);

#define I14DIAG_SHOW_MDP(pSector, cot, ioa, flags, pTimeStamp) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(cot); TMWTARG_UNUSED_PARAM(ioa); TMWTARG_UNUSED_PARAM(flags); TMWTARG_UNUSED_PARAM(pTimeStamp);

#define I14DIAG_SHOW_MST(pSector, cot, ioa, valueAndState, flags, pTimeStamp) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(cot); TMWTARG_UNUSED_PARAM(ioa); TMWTARG_UNUSED_PARAM(valueAndState); TMWTARG_UNUSED_PARAM(flags); TMWTARG_UNUSED_PARAM(pTimeStamp);

#define I14DIAG_SHOW_MBO(pSector, cot, ioa, value, flags, pTimeStamp) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(cot); TMWTARG_UNUSED_PARAM(ioa); TMWTARG_UNUSED_PARAM(value); TMWTARG_UNUSED_PARAM(flags); TMWTARG_UNUSED_PARAM(pTimeStamp);

#define I14DIAG_SHOW_MMENA(pSector, cot, ioa, value, flags, pTimeStamp) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(cot); TMWTARG_UNUSED_PARAM(ioa); TMWTARG_UNUSED_PARAM(value); TMWTARG_UNUSED_PARAM(flags); TMWTARG_UNUSED_PARAM(pTimeStamp);

#define I14DIAG_SHOW_MMENB(pSector, cot, ioa, value, flags, pTimeStamp) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(cot); TMWTARG_UNUSED_PARAM(ioa); TMWTARG_UNUSED_PARAM(value); TMWTARG_UNUSED_PARAM(flags); TMWTARG_UNUSED_PARAM(pTimeStamp);

#define I14DIAG_SHOW_MMENC(pSector, cot, ioa, value, flags, pTimeStamp) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(cot); TMWTARG_UNUSED_PARAM(ioa); TMWTARG_UNUSED_PARAM(value); TMWTARG_UNUSED_PARAM(flags); TMWTARG_UNUSED_PARAM(pTimeStamp);

#define I14DIAG_SHOW_MIT(pSector, cot, ioa, value, flags, pTimeStamp) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(cot); TMWTARG_UNUSED_PARAM(ioa); TMWTARG_UNUSED_PARAM(value); TMWTARG_UNUSED_PARAM(flags); TMWTARG_UNUSED_PARAM(pTimeStamp);

#define I14DIAG_SHOW_MITC(pSector, cot, ioa, pBCD, flags, pTimeStamp) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(cot); TMWTARG_UNUSED_PARAM(ioa); TMWTARG_UNUSED_PARAM(pBCD); TMWTARG_UNUSED_PARAM(flags); TMWTARG_UNUSED_PARAM(pTimeStamp);

#define I14DIAG_SHOW_MPSNA(pSector, cot, ioa, scd, qds) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(cot); TMWTARG_UNUSED_PARAM(ioa); TMWTARG_UNUSED_PARAM(scd); TMWTARG_UNUSED_PARAM(qds);

#define I14DIAG_SHOW_MMEND(pSector, cot, ioa, nva) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(cot); TMWTARG_UNUSED_PARAM(ioa); TMWTARG_UNUSED_PARAM(nva);

#define I14DIAG_SHOW_MEPTA(pSector, cot, ioa, sep, elapsedTime, pTimeStamp) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(cot); TMWTARG_UNUSED_PARAM(ioa); TMWTARG_UNUSED_PARAM(sep); TMWTARG_UNUSED_PARAM(elapsedTime); TMWTARG_UNUSED_PARAM(pTimeStamp);

#define I14DIAG_SHOW_MEPTB(pSector, cot, ioa, spe, qdp, relayDuration, pTimeStamp) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(cot); TMWTARG_UNUSED_PARAM(ioa); TMWTARG_UNUSED_PARAM(spe); TMWTARG_UNUSED_PARAM(qdp); TMWTARG_UNUSED_PARAM(relayDuration); TMWTARG_UNUSED_PARAM(pTimeStamp);

#define I14DIAG_SHOW_MEPTC(pSector, cot, ioa, oci, qdp, relayOperating, pTimeStamp) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(cot); TMWTARG_UNUSED_PARAM(ioa); TMWTARG_UNUSED_PARAM(oci); TMWTARG_UNUSED_PARAM(qdp); TMWTARG_UNUSED_PARAM(relayOperating); TMWTARG_UNUSED_PARAM(pTimeStamp);

#define I14DIAG_SHOW_PMENA(pSector, ioa, value, qualifier) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(ioa); TMWTARG_UNUSED_PARAM(value); TMWTARG_UNUSED_PARAM(qualifier);

#define I14DIAG_SHOW_PMENB(pSector, ioa, value, qualifier) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(ioa); TMWTARG_UNUSED_PARAM(value); TMWTARG_UNUSED_PARAM(qualifier);

#define I14DIAG_SHOW_PMENC(pSector, ioa, value, qualifier) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(ioa); TMWTARG_UNUSED_PARAM(value); TMWTARG_UNUSED_PARAM(qualifier);

#define I14DIAG_SHOW_FFRNA(pSector, ioa, fileName, fileLength, frq) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(ioa); TMWTARG_UNUSED_PARAM(fileName); TMWTARG_UNUSED_PARAM(fileLength); TMWTARG_UNUSED_PARAM(frq);

#define I14DIAG_SHOW_FSRNA(pSector, ioa, fileName, sectionName, sectionLength, srq) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(ioa); TMWTARG_UNUSED_PARAM(fileName); TMWTARG_UNUSED_PARAM(sectionName); TMWTARG_UNUSED_PARAM(sectionLength); TMWTARG_UNUSED_PARAM(srq);

#define I14DIAG_SHOW_FSCNA(pSector, ioa, fileName, sectionName, scq) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(ioa); TMWTARG_UNUSED_PARAM(fileName); TMWTARG_UNUSED_PARAM(sectionName); TMWTARG_UNUSED_PARAM(scq);

#define I14DIAG_SHOW_FSCNB(pSector, ioa, fileName) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(ioa); TMWTARG_UNUSED_PARAM(fileName);

#define I14DIAG_SHOW_FLSNA(pSector, ioa, fileName, sectionName, lsq, chs) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(ioa); TMWTARG_UNUSED_PARAM(fileName); TMWTARG_UNUSED_PARAM(sectionName); TMWTARG_UNUSED_PARAM(lsq); TMWTARG_UNUSED_PARAM(chs);

#define I14DIAG_SHOW_FAFNA(pSector, ioa, fileName, sectionName, afq ) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(ioa); TMWTARG_UNUSED_PARAM(fileName); TMWTARG_UNUSED_PARAM(sectionName); TMWTARG_UNUSED_PARAM(afq);

#define I14DIAG_SHOW_FSGNA(pSector, ioa, fileName, sectionName, segmentLength, pSegmentData) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(ioa); TMWTARG_UNUSED_PARAM(fileName); TMWTARG_UNUSED_PARAM(sectionName); TMWTARG_UNUSED_PARAM(segmentLength); TMWTARG_UNUSED_PARAM(pSegmentData);

#define I14DIAG_SHOW_FDRTA(pSector, ioa, fileName, fileLength, sof, pTimeStamp) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(ioa); TMWTARG_UNUSED_PARAM(fileName); TMWTARG_UNUSED_PARAM(fileLength); TMWTARG_UNUSED_PARAM(sof); TMWTARG_UNUSED_PARAM(pTimeStamp);

#define I14DIAG_SHOW_MCT(pSector, ioa, quantity, pCtiValues) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(ioa); TMWTARG_UNUSED_PARAM(quantity); TMWTARG_UNUSED_PARAM(pCtiValues);

#define I14DIAG_SHOW_TIME(pSector, pDateTime) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(pDateTime);

#define I14DIAG_SHOW_PROPAGATION_DELAY(pSector, delay) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(delay);

#define I14DIAG_FRAME_SENT(pTxData) \
  TMWTARG_UNUSED_PARAM(pTxData);

#define I14DIAG_FRAME_RECEIVED(pSector, pRxData) \
  TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(pRxData);

#define I14DIAG_FRAME_ERROR(pSession, pMsgHeader, pRxData) \
  TMWTARG_UNUSED_PARAM(pSession); TMWTARG_UNUSED_PARAM(pMsgHeader); TMWTARG_UNUSED_PARAM(pRxData);

#else

#define I14DIAG_SHOW_RESPONSE(pSector, pTxData, pMsg) \
  i14diag_showResponse(pSector, pTxData, pMsg)

#define I14DIAG_SHOW_STORE_TYPE(pSector, pMsg) \
  i14diag_showStoreType(pSector, pMsg)

#define I14DIAG_SHOW_COI(pSector, coi) \
  i14diag_showCOI(pSector, coi)

#define I14DIAG_SHOW_MSP(pSector, cot, ioa, flags, pTimeStamp) \
  i14diag_showMSP(pSector, cot, ioa, flags, pTimeStamp)

#define I14DIAG_SHOW_MDP(pSector, cot, ioa, flags, pTimeStamp) \
  i14diag_showMDP(pSector, cot, ioa, flags, pTimeStamp)

#define I14DIAG_SHOW_MST(pSector, cot, ioa, valueAndState, flags, pTimeStamp) \
  i14diag_showMST(pSector, cot, ioa, valueAndState, flags, pTimeStamp)

#define I14DIAG_SHOW_MBO(pSector, cot, ioa, value, flags, pTimeStamp) \
  i14diag_showMBO(pSector, cot, ioa, value, flags, pTimeStamp)

#define I14DIAG_SHOW_MMENA(pSector, cot, ioa, value, flags, pTimeStamp) \
  i14diag_showMMENA(pSector, cot, ioa, value, flags, pTimeStamp)

#define I14DIAG_SHOW_MMENB(pSector, cot, ioa, value, flags, pTimeStamp) \
  i14diag_showMMENB(pSector, cot, ioa, value, flags, pTimeStamp)

#define I14DIAG_SHOW_MMENC(pSector, cot, ioa, value, flags, pTimeStamp) \
  i14diag_showMMENC(pSector, cot, ioa, value, flags, pTimeStamp)

#define I14DIAG_SHOW_MIT(pSector, cot, ioa, value, flags, pTimeStamp) \
  i14diag_showMIT(pSector, cot, ioa, value, flags, pTimeStamp)

#define I14DIAG_SHOW_MITC(pSector, cot, ioa, pBCD, flags, pTimeStamp) \
  i14diag_showMITC(pSector, cot, ioa, pBCD, flags, pTimeStamp)

#define I14DIAG_SHOW_MPSNA(pSector, cot, ioa, scd, qds) \
  i14diag_showMPSNA(pSector, cot, ioa, scd, qds)

#define I14DIAG_SHOW_MMEND(pSector, cot, ioa, nva) \
  i14diag_showMMEND(pSector, cot, ioa, nva)

#define I14DIAG_SHOW_MEPTA(pSector, cot, ioa, sep, elapsedTime, pTimeStamp) \
  i14diag_showMEPTA(pSector, cot, ioa, sep, elapsedTime, pTimeStamp)

#define I14DIAG_SHOW_MEPTB(pSector, cot, ioa, spe, qdp, relayDuration, pTimeStamp) \
  i14diag_showMEPTB(pSector, cot, ioa, spe, qdp, relayDuration, pTimeStamp)

#define I14DIAG_SHOW_MEPTC(pSector, cot, ioa, oci, qdp, relayOperating, pTimeStamp) \
  i14diag_showMEPTC(pSector, cot, ioa, oci, qdp, relayOperating, pTimeStamp)

#define I14DIAG_SHOW_PMENA(pSector, ioa, value, qualifier) \
  i14diag_showPMENA(pSector, ioa, value, qualifier)

#define I14DIAG_SHOW_PMENB(pSector, ioa, value, qualifier) \
  i14diag_showPMENB(pSector, ioa, value, qualifier)

#define I14DIAG_SHOW_PMENC(pSector, ioa, value, qualifier) \
  i14diag_showPMENC(pSector, ioa, value, qualifier)

#define I14DIAG_SHOW_FFRNA(pSector, ioa, fileName, fileLength, frq) \
  i14diag_showFFRNA(pSector, ioa, fileName, fileLength, frq)

#define I14DIAG_SHOW_FSRNA(pSector, ioa, fileName, sectionName, sectionLength, srq) \
  i14diag_showFSRNA(pSector, ioa, fileName, sectionName, sectionLength, srq)

#define I14DIAG_SHOW_FSCNA(pSector, ioa, fileName, sectionName, scq) \
  i14diag_showFSCNA(pSector, ioa, fileName, sectionName, scq)

#define I14DIAG_SHOW_FSCNB(pSector, ioa, fileName) \
  i14diag_showFSCNB(pSector, ioa, fileName)

#define I14DIAG_SHOW_FLSNA(pSector, ioa, fileName, sectionName, lsq, chs) \
  i14diag_showFLSNA(pSector, ioa, fileName, sectionName, lsq, chs)

#define I14DIAG_SHOW_FAFNA(pSector, ioa, fileName, sectionName, afq ) \
  i14diag_showFAFNA(pSector, ioa, fileName, sectionName, afq)

#define I14DIAG_SHOW_FSGNA(pSector, ioa, fileName, sectionName, segmentLength, pSegmentData) \
  i14diag_showFSGNA(pSector, ioa, fileName, sectionName, segmentLength, pSegmentData)

#define I14DIAG_SHOW_FDRTA(pSector, ioa, fileName, fileLength, sof, pTimeStamp) \
  i14diag_showFDRTA(pSector, ioa, fileName, fileLength, sof, pTimeStamp)

#define I14DIAG_SHOW_MCT(pSector, ioa, quantity, pCtiValues) \
  i14diag_showMCT(pSector, ioa, quantity, pCtiValues)

#define I14DIAG_SHOW_TIME(pSector, pDateTime) \
  i14diag_showTime(pSector, pDateTime)

#define I14DIAG_SHOW_PROPAGATION_DELAY(pSector, delay) \
  i14diag_showPropagationDelay(pSector, delay)

#define I14DIAG_FRAME_SENT(pTxData) \
  i14diag_frameSent(pTxData)

#define I14DIAG_FRAME_RECEIVED(pSector, pRxData) \
  i14diag_frameReceived(pSector, pRxData)

#define I14DIAG_FRAME_ERROR(pSession, pMsgHeader, pRxData) \
  i14diag_frameError(pSession, pMsgHeader, pRxData)

#ifdef __cplusplus
extern "C" {
#endif

  /* function: i14diag_formatIOA */
  void TMWDEFS_GLOBAL i14diag_formatIOA(
    TMWSCTR *pSector,
    TMWTYPES_ULONG ioa,
    TMWTYPES_CHAR *pBuf);

  /* function: i14diag_showResponse */
  void TMWDEFS_GLOBAL i14diag_showResponse(
    TMWSCTR *pSector, 
    TMWSESN_TX_DATA *pTxData, 
    I870UTIL_MESSAGE *pMsg);

  /* function: i14diag_showStoreType */
  void TMWDEFS_GLOBAL i14diag_showStoreType(
    TMWSCTR *pSector, 
    I870UTIL_MESSAGE *pMsg);

  /* function: i14diag_showCOI */
  void TMWDEFS_GLOBAL i14diag_showCOI(
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR coi);

  /* function: i14diag_showMSP */
  void TMWDEFS_GLOBAL i14diag_showMSP(
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot,
    TMWTYPES_ULONG ioa, 
    TMWTYPES_UCHAR flags, 
    TMWDTIME *pTimeStamp);

  /* function: i14diag_showMDP */
  void TMWDEFS_GLOBAL i14diag_showMDP(
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot,
    TMWTYPES_ULONG ioa, 
    TMWTYPES_UCHAR flags, 
    TMWDTIME *pTimeStamp);

  /* function: i14diag_showMST */
  void TMWDEFS_GLOBAL i14diag_showMST(
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot,
    TMWTYPES_ULONG ioa, 
    TMWTYPES_UCHAR valueAndState, 
    TMWTYPES_UCHAR flags, 
    TMWDTIME *pTimeStamp);

  /* function: i14diag_showMBO */
  void TMWDEFS_GLOBAL i14diag_showMBO(
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot,
    TMWTYPES_ULONG ioa, 
    TMWTYPES_ULONG value, 
    TMWTYPES_UCHAR flags, 
    TMWDTIME *pTimeStamp);

  /* function: i14diag_showMMENA */
  void TMWDEFS_GLOBAL i14diag_showMMENA(
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot,
    TMWTYPES_ULONG ioa, 
    TMWTYPES_SHORT value, 
    TMWTYPES_UCHAR flags, 
    TMWDTIME *pTimeStamp);

  /* function: i14diag_showMMENB */
  void TMWDEFS_GLOBAL i14diag_showMMENB(
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot,
    TMWTYPES_ULONG ioa, 
    TMWTYPES_SHORT value, 
    TMWTYPES_UCHAR flags, 
    TMWDTIME *pTimeStamp);

#if TMWCNFG_SUPPORT_FLOAT
  /* function: i14diag_showMMENC */
  void TMWDEFS_GLOBAL i14diag_showMMENC(
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot,
    TMWTYPES_ULONG ioa, 
    TMWTYPES_SFLOAT value, 
    TMWTYPES_UCHAR flags, 
    TMWDTIME *pTimeStamp);
#endif

  /* function: i14diag_showMIT */
  void TMWDEFS_GLOBAL i14diag_showMIT(
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot,
    TMWTYPES_ULONG ioa, 
    TMWTYPES_ULONG value, 
    TMWTYPES_UCHAR flags, 
    TMWDTIME *pTimeStamp);

  /* function: i14diag_showMITC */
  void TMWDEFS_GLOBAL i14diag_showMITC(
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot,
    TMWTYPES_ULONG ioa, 
    TMWTYPES_UCHAR *pBCD, 
    TMWTYPES_UCHAR flags, 
    TMWDTIME *pTimeStamp);

  /* function: i14diag_showMPSNA */
  void TMWDEFS_GLOBAL i14diag_showMPSNA(
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot,
    TMWTYPES_ULONG ioa,
    TMWTYPES_ULONG scd,
    TMWTYPES_UCHAR qds);

  /* function: i14diag_showMMEND */
  void TMWDEFS_GLOBAL i14diag_showMMEND(
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot,
    TMWTYPES_ULONG ioa,
    TMWTYPES_SHORT nva);

  /* function: i14diag_showMEPTA */
  void TMWDEFS_GLOBAL i14diag_showMEPTA(
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot,
    TMWTYPES_ULONG ioa,
    TMWTYPES_UCHAR sep,
    TMWTYPES_USHORT elapsedTime,
    TMWDTIME *pTimeStamp);

  /* function: i14diag_showMEPTB */
  void TMWDEFS_GLOBAL i14diag_showMEPTB(
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot,
    TMWTYPES_ULONG ioa,
    TMWTYPES_UCHAR spe,
    TMWTYPES_UCHAR qdp,
    TMWTYPES_USHORT relayDuration,
    TMWDTIME *pTimeStamp);

  /* function: i14diag_showMEPTC */
  void TMWDEFS_GLOBAL i14diag_showMEPTC(
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot,
    TMWTYPES_ULONG ioa,
    TMWTYPES_UCHAR oci,
    TMWTYPES_UCHAR qdp,
    TMWTYPES_USHORT relayOperating,
    TMWDTIME *pTimeStamp);

  /* function: i14diag_showPMENA */
  void TMWDEFS_GLOBAL i14diag_showPMENA(
    TMWSCTR *pSector, 
    TMWTYPES_ULONG ioa, 
    TMWTYPES_SHORT value, 
    TMWTYPES_UCHAR qualifier);

  /* function: i14diag_showPMENB */
  void TMWDEFS_GLOBAL i14diag_showPMENB(
    TMWSCTR *pSector, 
    TMWTYPES_ULONG ioa, 
    TMWTYPES_SHORT value, 
    TMWTYPES_UCHAR qualifier);

#if TMWCNFG_SUPPORT_FLOAT
  /* function: i14diag_showPMENC */
  void TMWDEFS_GLOBAL i14diag_showPMENC(
    TMWSCTR *pSector, 
    TMWTYPES_ULONG ioa, 
    TMWTYPES_SFLOAT value, 
    TMWTYPES_UCHAR qualifier);
#endif

  /* function: i14diag_showPACNA */
  void TMWDEFS_GLOBAL i14diag_showPACNA(
    TMWSCTR *pSector,
    TMWTYPES_ULONG ioa);

  /* function: i14diag_showFFRNA */
  void TMWDEFS_GLOBAL i14diag_showFFRNA(
    TMWSCTR *pSector, 
    TMWTYPES_ULONG ioa, 
    TMWTYPES_USHORT fileName,
    TMWTYPES_ULONG fileLength,
    TMWTYPES_UCHAR frq);

  /* function: i14diag_showFSRNA */
  void TMWDEFS_GLOBAL i14diag_showFSRNA(
    TMWSCTR *pSector, 
    TMWTYPES_ULONG ioa, 
    TMWTYPES_USHORT fileName,
    TMWTYPES_UCHAR sectionName,
    TMWTYPES_ULONG sectionLength,
    TMWTYPES_UCHAR srq);

  /* function: i14diag_showFSCNA */
  void TMWDEFS_GLOBAL i14diag_showFSCNA(
    TMWSCTR *pSector, 
    TMWTYPES_ULONG ioa,
    TMWTYPES_USHORT fileName,
    TMWTYPES_UCHAR sectionName,
    TMWTYPES_UCHAR scq);
   
  /* function: i14diag_showFSCNB */
  void TMWDEFS_GLOBAL i14diag_showFSCNB(
    TMWSCTR *pSector, 
    TMWTYPES_ULONG ioa,
    TMWTYPES_USHORT fileName);

  /* function: i14diag_showFLSNA */
  void TMWDEFS_GLOBAL i14diag_showFLSNA(
    TMWSCTR *pSector, 
    TMWTYPES_ULONG ioa,
    TMWTYPES_USHORT fileName,
    TMWTYPES_UCHAR sectionName,
    TMWTYPES_UCHAR lsq,
    TMWTYPES_UCHAR chs);

  /* function: i14diag_showFAFNA */
  void TMWDEFS_GLOBAL i14diag_showFAFNA(
    TMWSCTR *pSector, 
    TMWTYPES_ULONG ioa,
    TMWTYPES_USHORT fileName,
    TMWTYPES_UCHAR sectionName,
    TMWTYPES_UCHAR afq);

  /* function: i14diag_showFSGNA */
  void TMWDEFS_GLOBAL i14diag_showFSGNA(
    TMWSCTR *pSector, 
    TMWTYPES_ULONG ioa,
    TMWTYPES_USHORT fileName,
    TMWTYPES_UCHAR sectionName,
    TMWTYPES_ULONG segmentLength,
    TMWTYPES_UCHAR *pSegmentData);

  /* function: i14diag_showFDRTA */
  void TMWDEFS_GLOBAL i14diag_showFDRTA(
    TMWSCTR *pSector, 
    TMWTYPES_ULONG ioa,
    TMWTYPES_USHORT fileName,
    TMWTYPES_ULONG fileLength,
    TMWTYPES_UCHAR sof,
    TMWDTIME *pTimeStamp);

  /* function: i14diag_showMCT */
  void TMWDEFS_GLOBAL i14diag_showMCT(
    TMWSCTR *pSector, 
    TMWTYPES_ULONG ioa,
    TMWTYPES_UCHAR quantity,
    TMWTYPES_UCHAR *pCtiValues);

  /* function: i14diag_showTime */
  void TMWDEFS_GLOBAL i14diag_showTime(
    TMWSCTR *pSector, 
    TMWDTIME *pDateTime);

  /* function: i14diag_showPropagationDelay */
  void TMWDEFS_GLOBAL i14diag_showPropagationDelay(
    TMWSCTR *pSector, 
    TMWTYPES_USHORT delay);

  /* function: i14diag_frameSent */
  void TMWDEFS_GLOBAL i14diag_frameSent(
    TMWSESN_TX_DATA *pTxData);

  /* function: i14diag_frameReceived */
  void TMWDEFS_GLOBAL i14diag_frameReceived(
    TMWSCTR *pSector, 
    TMWSESN_RX_DATA *pRxData);

  /* function: i14diag_frameError
   * purpose: Display errored frame
   * arguments:
   *  pSession - pointer to session structure
   *  pRxData - pointer to received message structure
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL i14diag_frameError(
    TMWSESN *pSession, 
    I870UTIL_MESSAGE *pMsgHeader,
    TMWSESN_RX_DATA *pRxData);

#ifdef __cplusplus
}
#endif
#endif /* TMWCNFG_SUPPORT_DIAG */
#endif /* I14DIAG_DEFINED */
