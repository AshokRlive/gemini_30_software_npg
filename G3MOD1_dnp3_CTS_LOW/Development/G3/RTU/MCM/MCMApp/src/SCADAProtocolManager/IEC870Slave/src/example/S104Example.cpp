/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2009 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

// 104Slave.cpp : Sample 104 Slave console application.
//
#define TMW_LINUX_TARGET


#include "stdafx.h"

extern "C" {
#include "tmwscl/utils/tmwpltmr.h"
#include "tmwscl/utils/tmwtarg.h"

#include "tmwscl/i870/i104chnl.h"
#include "tmwscl/i870/s104sesn.h"
#include "tmwscl/i870/s104sctr.h"

#if defined(TMW_WTK_TARGET) 
#include <windows.h>
#include "WinIoTarg/include/winiotarg.h"
#endif

#if defined(TMW_LINUX_TARGET)
#include "LinIoTarg/liniotarg.h"
#endif
}

/* Config structures */
#if defined(TMW_WTK_TARGET) 
WINIO_CONFIG IOCnfg;
#endif

#if defined(TMW_LINUX_TARGET)
LINIO_CONFIG IOCnfg;
#endif

#if !TMWCNFG_MULTIPLE_TIMER_QS

/* forward references */
static void mySleep(int milliseconds);
void myPutDiagString(const TMWDIAG_ANLZ_ID *pAnlzId, const TMWTYPES_CHAR *pString);

/* Main entry point */
int main(int argc, char* argv[])
{
  TMWAPPL *pApplContext;
  TMWCHNL *pSclChannel;
  TMWSESN *pSclSession;
  TMWSCTR *pSclSector;
  I870CHNL_CONFIG chnlConfig;
  I870LNK4_CONFIG linkConfig; 
  TMWPHYS_CONFIG  physConfig;
  TMWTARG_CONFIG targConfig;
  S104SESN_CONFIG sesnConfig;
  S104SCTR_CONFIG sctrConfig;

  TMWTARG_UNUSED_PARAM(argc);
  TMWTARG_UNUSED_PARAM(argv);

#if TMW_WTK_TARGET
#if TMWCNFG_SUPPORT_DIAG 
  /* Register function to display diagnostic strings to console 
   * This is only necessary if using the WinIOTarg target layer.
   * If implementing a new target layer, tmwtarg_putDiagString()
   * should be modified if diagnostic output is desired.
   */
  tmwtargp_registerPutDiagStringFunc(myPutDiagString);
#endif
  
  /* Register poll timer timer functions.
   * This is only necessary if using the WinIOTarg target layer.
   * If implementing a new target layer tmwtarg_startTimer and
   * tmwtarg_cancelTimer will call poll timer functions by default,
   * or can be modified to call target provided timer functions.
   */
  tmwtargp_registerStartTimerFunc(tmwpltmr_startTimer);
  tmwtargp_registerCancelTimerFunc(tmwpltmr_cancelTimer);
#endif


  /*
   * Initialize S101 SCL. This includes:
   *  - initialize polled timer
   *  - initialize application context 
   */
  tmwtimer_initialize();
  pApplContext = tmwappl_initApplication();

  /* Initialize all configuration structures to defaults
   */
  tmwtarg_initConfig(&targConfig);
  i104chnl_initConfig(&chnlConfig, &linkConfig, &physConfig);
  s104sesn_initConfig(&sesnConfig);
  s104sctr_initConfig(&sctrConfig);

  /* If needed, overwrite the default values
   */
  linkConfig.isControlling = TMWDEFS_FALSE;

  /* Initialize target structures (in this case, the IO Config Structure)
   * Call winio_initConfig to initialize default values, then overwrite
   * values as needed.
   *
   * Also, if needed, overwrite the default configuration values
   * with user settings. This example configures the Slave for a TCP/IP
   * session using the Loopback address and default Session values. This
   * configuration is compatible with the example scripts that ship with the
   * Communication Protocol Test Harnes..
   */
#if defined(TMW_WTK_TARGET)  
  WinIoTarg_initConfig(&IOCnfg);
  IOCnfg.type = WINIO_TYPE_TCP;

  /* name displayed in analyzer window */
  strcpy(IOCnfg.winTCP.chnlName, "Slave");
  
  /* *.*.*.* allows any client to connect*/
  strcpy(IOCnfg.winTCP.ipAddress, "*.*.*.*");
  //strcpy(IOCnfg.winTCP.ipAddress, "127.0.0.1");

  IOCnfg.winTCP.ipPort = 2404;
  IOCnfg.winTCP.mode = WINTCP_MODE_SERVER;
#endif

#if defined(TMW_LINUX_TARGET) 
  liniotarg_initConfig(&IOCnfg);
  IOCnfg.type = LINIO_TYPE_TCP;

  /* name displayed in analyzer window */
  strcpy(IOCnfg.linTCP.chnlName, "S104");

  /* *.*.*.* allows any client to connect*/
  strcpy(IOCnfg.linTCP.ipAddress, "*.*.*.*");
  //strcpy(IOCnfg.linTCP.ipAddress, "127.0.0.1");

  IOCnfg.linTCP.ipPort = 2404;
  IOCnfg.linTCP.mode = LINTCP_MODE_SERVER;
#endif

  /* Open the Channel, Session, and Sector
   */
  pSclChannel = i104chnl_openChannel(pApplContext, &chnlConfig, &linkConfig, 
    &physConfig, &IOCnfg, &targConfig);
  pSclSession = (TMWSESN *)s104sesn_openSession(pSclChannel, &sesnConfig);
  pSclSector = s104sctr_openSector(pSclSession, &sctrConfig, TMWDEFS_NULL);

  /* Begin the main loop.
   * This example uses the Source Code Library in Polled mode.
   * In Polled mode, you must periodically:
   *  - check the timer
   *  - check for (and process) any received data
   */
  while(1)
  {
    tmwpltmr_checkTimer(); 
    tmwappl_checkForInput(pApplContext);

    /* Sleep for 50 milliseconds */
    mySleep(50);
  }
 
  return 0;
}

/* Sample function to sleep for x milliseconds */
static void  mySleep(int milliseconds)
{
#if defined(TMW_WTK_TARGET) 
    WinIoTarg_Sleep(milliseconds);
#endif
#if defined(TMW_LINUX_TARGET) 
    Sleep(milliseconds*100);
#endif
}

/* The following functions are called by the SCL */

#if TMWCNFG_SUPPORT_DIAG
/* Simple diagnostic output function, registered with the Source Code Library */
void myPutDiagString(const TMWDIAG_ANLZ_ID *pAnlzId,const TMWTYPES_CHAR *pString)
{
  TMWDIAG_ID id = pAnlzId->sourceId;

  if((TMWDIAG_ID_ERROR & id) 
    ||(TMWDIAG_ID_APPL & id)
    ||(TMWDIAG_ID_USER & id))
  {
    printf((char *)pString);  
    return;
  }

  /* Comment this out to turn off verbose diagnostics */
  /* For now print everything */
  /* printf((char *)pString); */
}
#endif
#endif
