/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14cscna.c
 * description: IEC 60870-5-101 slave CSCNA (single command)
 *  functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14cscna.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/s14msp.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/utils/tmwtarg.h"

#if S14DATA_SUPPORT_CSC

#if S14DATA_SUPPORT_MULTICMDS

void TMWDEFS_CALLBACK s14cscna_checkMonitorPoint(TMWSCTR *pSector, S14SCTR_CMD *pContext)
{
#if S14DATA_SUPPORT_MSP
  void *pMonitoredPoint;
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  /* See if point is being monitored */
  if((pMonitoredPoint = s14data_cscGetMonitoredPoint(pContext->pPoint)) != TMWDEFS_NULL)
  {
    /* Get current time */
    TMWDTIME timeStamp;
    s14util_getDateTime(pSector, &timeStamp);

    /* Add event with remote cause of transmission */
    s14msp_internalAddEvent(pSector, S14UTIL_MODE_TO_REMLOC_COT(p14Sector->localMode), 
      s14data_mspGetInfoObjAddr(pMonitoredPoint), 
      s14data_mspGetFlagsAndValue(pMonitoredPoint), &timeStamp);
  }
#endif
}

/* function: s14cscna_processRequest */
void TMWDEFS_CALLBACK s14cscna_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  S14SCTR *p14Sector;
  S14SCTR_CMD *pContext;
  TMWTYPES_BOOL timerIsActive;
  TMWTYPES_ULONG ioa;
  TMWTYPES_ULONG oldIOA;
  TMWTYPES_UCHAR oldSCO;
  
  pContext = s14util_processMultiRequest(pSector, pMsg, I14DEF_TYPE_CSCNA1, &ioa, &timerIsActive);
  if(pContext == TMWDEFS_NULL)
    return;

  /* Command to call for status of this point */
  pContext->pStatus = s14data_cscStatus;

  /* Save old values in case this is an execute and we need to compare with select values.
   * All of the error paths require the p14Sector fields to be filled in with rcvd values.
   */
  oldIOA = pContext->ioa;
  oldSCO = pContext->qualifier; 

  /* If only one context per typeId is allowed, this might be cancelling and replacing the previous ioa */
  pContext->ioa = ioa;

  /* Parse SCO */
  pContext->qualifier = pMsg->pRxData->pMsgBuf[pMsg->offset++]; 
  pContext->useQualifier = TMWDEFS_TRUE;

  /* Parse received cause of transmission */
  if(pMsg->cot == I14DEF_COT_ACTIVATION)
  {
    pContext->cot = I14DEF_COT_ACTCON;
  }
  else if(pMsg->cot == I14DEF_COT_DEACTIVATION)
  {
    /* Any deactivate will cause the select to be cancelled, even an incorrect one 
     * There is currently not an s14data function to be called indicating deactivate.
     * 
     * The 101 and 601 specs are unclear about whether a DEACTCON positive or negative 
     * should be sent on error. Since even an incorrect DEACT results in no select being
     * active, a positive reply will be sent (except if ioa is bad below).
     */
    pContext->cot = I14DEF_COT_DEACTCON;
  }
  else
  {
    pContext->cot = I14DEF_COT_UNKNOWN_COT | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* Lookup data point using information object address */
  p14Sector = (S14SCTR *)pSector;
  pContext->pPoint = s14data_cscLookupPoint(p14Sector->i870.pDbHandle, ioa);
  if(pContext->pPoint == TMWDEFS_NULL)
  {
    pContext->cot = I14DEF_COT_UNKNOWN_IOA | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* if deactivate, context->cot was set to DEACTCON above. */
  if(pMsg->cot == I14DEF_COT_DEACTIVATION)
  {
    return;
  }

  /* Perform select or execute and set operation status */
  if((pContext->qualifier & I14DEF_QOC_SE_SELECT) != 0)
  {
    pContext->state = TMWDEFS_CMD_STATE_SELECTING;
    pContext->status = s14data_cscSelect(pContext->pPoint, 
      pMsg->cot, pContext->qualifier);
  }
  else
  {
    /* If a select is required for this point make sure a valid select
     * operation is pending and compare the execute parameters against
     * the select parameters.
     */
    if(s14data_cscSelectRequired(pContext->pPoint))
    {
      /* Make sure select is active */
      /* Compare information object address */
      /* Compare qualifier, ignore the select bit */
      if(!timerIsActive
         ||(pContext->ioa != oldIOA)
         ||(pContext->qualifier != (oldSCO & ~I14DEF_QOC_SE_MASK)))
      {
        pContext->cot |= I14DEF_COT_NEGATIVE_CONFIRM;
        return;
      }
    }
  
    pContext->pCheckMonitoredPoint = s14cscna_checkMonitorPoint;
 
    pContext->state = TMWDEFS_CMD_STATE_EXECUTING;
    pContext->status = s14data_cscExecute(pContext->pPoint, 
      pMsg->cot, pContext->qualifier);
  }
}

#else /* !S14DATA_SUPPORT_MULTICMDS */

/* function: _checkStatus */
static TMWDEFS_COMMAND_STATUS TMWDEFS_CALLBACK _checkStatus(
  TMWSCTR *pSector)
{
  return (s14cscna_checkStatus(pSector));
}

/* function: cscna_checkStatus */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14cscna_checkStatus(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  switch(p14Sector->cscnaState)
  {
  case TMWDEFS_CMD_STATE_SELECTING:
    {  
      if((p14Sector->cscnaStatus != TMWDEFS_CMD_STAT_SUCCESS) 
        && (p14Sector->cscnaStatus != TMWDEFS_CMD_STAT_FAILED))
      {
        /* Get Status */
        p14Sector->cscnaStatus = s14data_cscStatus(p14Sector->pCSCPoint);
      }

      /* If select is complete start select timer */
      if(p14Sector->cscnaStatus == TMWDEFS_CMD_STAT_SUCCESS)
      {
        tmwtimer_start(&p14Sector->cscnaSelectTimer, 
          p14Sector->selectTimeout, pSector->pSession->pChannel, 
          TMWDEFS_NULL, TMWDEFS_NULL);
      }
    }
    break;

  case TMWDEFS_CMD_STATE_EXECUTING:
    {  
      if((p14Sector->cscnaStatus != TMWDEFS_CMD_STAT_SUCCESS) 
        && (p14Sector->cscnaStatus != TMWDEFS_CMD_STAT_FAILED))
      {
        /* Get Status */
        p14Sector->cscnaStatus = s14data_cscStatus(p14Sector->pCSCPoint);
      }

      /* If execute is complete change state to monitoring and status
       * to in progress so we will check the monitored state at least
       * once.
       */
      if((p14Sector->cscnaStatus == TMWDEFS_CMD_STAT_SUCCESS)
        || (p14Sector->cscnaStatus == TMWDEFS_CMD_STAT_MONITORING))
      {
        p14Sector->cscnaState = TMWDEFS_CMD_STATE_MONITORING;
        p14Sector->cscnaStatus = TMWDEFS_CMD_STAT_MONITORING;
        return(TMWDEFS_CMD_STAT_SUCCESS);
      }
    }
    break;

  case TMWDEFS_CMD_STATE_MONITORING:
    {
      if((p14Sector->cscnaStatus != TMWDEFS_CMD_STAT_SUCCESS) 
        && (p14Sector->cscnaStatus != TMWDEFS_CMD_STAT_FAILED))
      {
        /* Get Status */
        p14Sector->cscnaStatus = s14data_cscStatus(p14Sector->pCSCPoint);
      }

      /* Check monitored feedback point status */
      if(p14Sector->cscnaStatus == TMWDEFS_CMD_STAT_SUCCESS)
      {
        void *pMonitoredPoint;

        /* If monitor is complete change state to idle */
        p14Sector->cscnaState = TMWDEFS_CMD_STATE_IDLE;

#if S14DATA_SUPPORT_MSP
        /* See if point is being monitored */
        if((pMonitoredPoint = s14data_cscGetMonitoredPoint(p14Sector->pCSCPoint)) != TMWDEFS_NULL)
        {
          /* Get current time */
          TMWDTIME timeStamp;
          s14util_getDateTime(pSector, &timeStamp);

          /* Add event with remote cause of transmission */
          s14msp_internalAddEvent(pSector, S14UTIL_MODE_TO_REMLOC_COT(p14Sector->localMode), 
            s14data_mspGetInfoObjAddr(pMonitoredPoint), 
            s14data_mspGetFlagsAndValue(pMonitoredPoint), &timeStamp);
        }
#endif
      }
    }
    break;
  default:
    break;
  }

  /* If command failed return to IDLE state */
  if(p14Sector->cscnaStatus == TMWDEFS_CMD_STAT_FAILED)
    p14Sector->cscnaState = TMWDEFS_CMD_STATE_IDLE;

  return(p14Sector->cscnaStatus);
}

/* function: s14cscna_processRequest */
void TMWDEFS_CALLBACK s14cscna_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  S14SCTR *p14Sector;
  TMWTYPES_BOOL timerIsActive;
  TMWTYPES_ULONG oldIOA;
  TMWTYPES_UCHAR oldSCO;
  
  p14Sector = (S14SCTR *)pSector;

  /* Any new or failed command cancels the select timer 
   * Save the timer state in case this is a valid execute command
   */
  timerIsActive = tmwtimer_isActive(&p14Sector->cscnaSelectTimer);
  if(timerIsActive)
  {
    tmwtimer_cancel(&p14Sector->cscnaSelectTimer);
  }

  /* If there is a previous command in progress, (response still to be sent)
   * queue a negative ACT CON to be sent. 
   * Don't overwrite the information from the previous command.
   */
  if((pMsg->cot == I14DEF_COT_ACTIVATION)
    && ((p14Sector->cscnaCOT != 0) || (p14Sector->csctaCOT != 0)))
  {
    s14sesn_sendNegActCon(pSector, pMsg, I14DEF_COT_ACTCON); 
    return;
  }

  /* Store originator address */
  p14Sector->cscnaOriginator = pMsg->origAddress;

  /* Save old values in case this is an execute and we need to compare with select values.
   * All of the error paths require the p14Sector fields to be filled in with rcvd values.
   */
  oldIOA = p14Sector->cscnaIOA;
  oldSCO = p14Sector->cscnaSCO;

  /* Parse Information Object Address */
  i870util_readInfoObjAddr(pSector->pSession, pMsg, &p14Sector->cscnaIOA);

  /* Parse SCO */
  p14Sector->cscnaSCO = pMsg->pRxData->pMsgBuf[pMsg->offset++];

  /* Parse received cause of transmission */
  if(pMsg->cot == I14DEF_COT_ACTIVATION)
  {
    p14Sector->cscnaCOT = I14DEF_COT_ACTCON;
  }
  else if(pMsg->cot == I14DEF_COT_DEACTIVATION)
  {
    /* Any deactivate will cause the select to be cancelled, even an incorrect one 
     * There is currently not a s14data function to be called indicating deactivate.
     * 
     * The 101 and 601 specs are unclear about whether a DEACTCON positive or negative 
     * should be sent on error. Since even an incorrect DEACT results in no select being
     * active, a positive reply will be sent (except if ioa is bad below).
     */
    p14Sector->cscnaCOT = I14DEF_COT_DEACTCON;
  }
  else
  {
    p14Sector->cscnaCOT = I14DEF_COT_UNKNOWN_COT | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* Lookup data point using information object address */
  p14Sector->pCSCPoint = s14data_cscLookupPoint(p14Sector->i870.pDbHandle, p14Sector->cscnaIOA);
  if(p14Sector->pCSCPoint == TMWDEFS_NULL)
  {
    p14Sector->cscnaCOT = I14DEF_COT_UNKNOWN_IOA | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* if deactivate, command is finished */
  if(pMsg->cot == I14DEF_COT_DEACTIVATION)
  {
    return;
  }

  /* Perform select or execute and set operation status */
  if((p14Sector->cscnaSCO & I14DEF_QOC_SE_SELECT) != 0)
  {
    p14Sector->cscnaState = TMWDEFS_CMD_STATE_SELECTING;
    p14Sector->cscnaStatus = s14data_cscSelect(p14Sector->pCSCPoint, pMsg->cot, p14Sector->cscnaSCO);
  }
  else
  {
    /* If a select is required for this point make sure a valid select
     * operation is pending and compare the execute parameters against
     * the select parameters.
     */
    if(s14data_cscSelectRequired(p14Sector->pCSCPoint))
    {
      /* Make sure select is active */
      /* Compare information object address */
      /* Compare qualifier, ignore the select bit */
      if(!timerIsActive
         ||(p14Sector->cscnaIOA != oldIOA)
         ||(p14Sector->cscnaSCO != (oldSCO & ~I14DEF_QOC_SE_MASK)))
      {
        p14Sector->cscnaCOT |= I14DEF_COT_NEGATIVE_CONFIRM;
        return;
      }
    }
 
    p14Sector->cscnaState = TMWDEFS_CMD_STATE_EXECUTING;
    p14Sector->cscnaStatus = s14data_cscExecute(p14Sector->pCSCPoint, pMsg->cot, p14Sector->cscnaSCO);
  }
}

/* function: s14cscna_buildResponse */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14cscna_buildResponse(
  TMWSCTR *pSector, 
  TMWTYPES_BOOL buildResponse)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  return(s14util_buildCommandResponse(pSector, buildResponse, 
    I14DEF_TYPE_CSCNA1, &p14Sector->cscnaCOT, p14Sector->cscnaOriginator,
    p14Sector->cscnaIOA, p14Sector->cscnaSCO, TMWDEFS_TRUE, TMWDEFS_NULL, 0, TMWDEFS_NULL, 
    _checkStatus));
}
#endif /* !S14DATA_SUPPORT_MULTICMDS */
#endif /* S14DATA_SUPPORT_CSC */
