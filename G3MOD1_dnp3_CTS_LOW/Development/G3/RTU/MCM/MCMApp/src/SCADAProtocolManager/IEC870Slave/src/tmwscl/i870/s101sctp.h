/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s101sctp.h
 * description: This file is intended for internal SCL use only.
 *  IEC 60870-5-101 Slave sector
 */
#ifndef S101SCTP_DEFINED
#define S101SCTP_DEFINED

#include "tmwscl/i870/s14sctr.h"

/* S101 Sector Context */
typedef struct S101SectorStruct {
  /* Generic IEC 60870-5-101/104 Sector Info, Must be first entry */
  S14SCTR s14;

  /* S101 Specific Info */

  /* Test Command */
  TMWTYPES_UCHAR ctsnaCOT; 
  TMWTYPES_UCHAR ctsnaOriginator;
  TMWTYPES_USHORT pattern;
  TMWTYPES_ULONG ctsnaIOA;

  /* Delay Acquisition Command */
  TMWTYPES_UCHAR ccdnaCOT;
  TMWTYPES_USHORT ccdnaTime;
  TMWTYPES_UCHAR ccdnaOriginator;
  TMWTYPES_MILLISECONDS ccdnaFirstByteReceived;

} S101SCTR;

#endif /* S101SCTP_DEFINED */
