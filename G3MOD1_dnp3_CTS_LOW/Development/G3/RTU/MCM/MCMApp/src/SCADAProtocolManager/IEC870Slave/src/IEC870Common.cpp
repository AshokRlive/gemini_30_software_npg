/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:IEC870Common.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   10 Aug 2017     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <math.h>    //pow(), round()

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "IEC870Common.h"
//#include "S104Debug.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
/**
 * \brief Applies a Scaling to a given value, clamping values outside range
 *
 * \param inValue Incoming value to convert
 * \param minValue Min value of the original scale
 * \param maxValue Max value of the original scale
 * \param scaleValueMin Min value of the new scale
 * \param scaleRange Range (scaleValueMax-scaleValueMin) value of the new scale
 *
 * \return Scaled value inside scaleRange
 */
static lu_float32_t IEC870ApplyScale(const lu_float32_t inValue,
                                const lu_float32_t minValue,
                                const lu_float32_t maxValue,
                                const lu_float32_t scaleValueMin,
                                const lu_float32_t scaleRange
                                );

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
lu_int16_t IEC870Scaled(const lu_float32_t inValue,
                        const lu_float32_t minValue,
                        const lu_float32_t maxValue)
{
    /**
     * Note that Scaled range is [-2^15, 2^15 - 1], that is [-32768, 32767]
     * But we are using here the full scale [-32768, 32768] due to the Scaled
     * range being asymmetric while the _usual_ scale is not. For example:
     *      Incoming scale  | IEC870 Scaled     | Expected result
     *       [-100, 100]    | [-32768, 32767]   | by user
     *      ----------------+-------------------+------------------
     *          100         |   32767           |   32767
     *          0           |   -1 (!)          |   0
     *          -100        |   -32768          |   -32768
     *
     * (!): Depending on the original scale, the 0 may be skewed to -1
     *
     * We resort to make the Scaled result symmetric for being close to what the
     * user would expect. In order to avoid the max values becoming 32768 (that
     * is invalid in the IEC870 Scale), we cap them off back to 32767.
     */
    static const lu_int32_t realScaledValueMax = 32767;
    static const lu_int32_t scaledValueMax = 32768; //Not 32767: see note above
    static const lu_int32_t scaledValueMin = -32768;
    static const lu_int32_t scaledRange = scaledValueMax - scaledValueMin;

    lu_float32_t value;     //Calculated & corrected value
    value = round(IEC870ApplyScale(inValue, minValue, maxValue, scaledValueMin, scaledRange));
    //Clamp upper value of the IEC870 Scale
    if(value >= realScaledValueMax)
    {
        value = realScaledValueMax;
    }
    return (lu_int16_t)value;
}


lu_float32_t IEC870Normalized(const lu_float32_t inValue,
                            const lu_float32_t minValue,
                            const lu_float32_t maxValue)
{
    /**
     * Note that Normalized range is [-1, 1),
     *  that in 16-bit is equivalent to [-1, 1-(2^(-15))]
     *  or,  applying 16-bit resolution [-1, 0.999969482421875]
     * Normalized values follows the rule:
     *      Bit      16  15      14      13  ... 2       1
     *      Value  sign  2^-1    2^-2    2^-3    2^-14   2^-15
     * Examples:
     *      0x6000 = 2^(-1) + 2^(-2) = 0.5 + 0.25 = 0.75
     *      0xC000 = -1 * ( 2^(-1) ) = -0.5
     * As a curiosity: 1-(2^(-15)) = 32767.0/32768.0
     *
     * Note that Normalized range is [-1, 1-(2^(-15))], but we are using here
     * the full scale [-1, 1] due to the Normalized range being asymmetric while
     * the _usual_ scale is not. For example:
     *      Incoming scale  | IEC870 Normalized         | Expected result
     *       [-100, 100]    | [-1, 0.9999...]           | by user
     *      ----------------+---------------------------+------------------
     *          100         |   0.9999        0x7fff    |   0.9999  0x7fff
     *          0           |   -0.000015 (!) 0x0000    |   0       0x0000
     *          -100        |   -1            0x8000    |   -1      0x8000
     *
     * (!): Depending on the original scale, the 0 may be skewed to -0.xxx
     *
     * We resort to make the Normalized result symmetric for being close to what
     * the user would expect. In order to avoid the max values becoming 1 (that
     * is invalid in the IEC870 Normalized scale), we cap them off back to 0.9999
     */
    static const lu_float32_t realNormalizedValueMax = (1 - pow(2.0,-15.0));
    static const lu_float32_t normalizedValueMax = 1; //Not (1-pow(2.0,-15.0)), see note above
    static const lu_float32_t normalizedValueMin = -1;
    static const lu_float32_t normalizedRange = normalizedValueMax - normalizedValueMin;

    lu_float32_t value;     //Calculated & corrected value
    value = IEC870ApplyScale(inValue, minValue, maxValue, normalizedValueMin, normalizedRange);
    if(value >= realNormalizedValueMax)
    {
        value = realNormalizedValueMax;
    }
#if 0
    static const lu_uint32_t normalizedScalingFactor = 32768;
    lu_float32_t v = value;
    lu_float32_t y = (normalizedScalingFactor * v);
    DBG_INFO("---\nNormalising [%f, %f] into [%f, %f];\n"
             " value %f = N %f (h%04x) || NN %f (h%04x)\n",
                    minValue, maxValue,
                    normalizedValueMin, normalizedValueMax,
                    inValue,
                    v, (short)(v),
                    y, (short)(y)
                    );
#endif

    return value;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
lu_float32_t IEC870ApplyScale(const lu_float32_t inValue,
                                const lu_float32_t minValue,
                                const lu_float32_t maxValue,
                                const lu_float32_t scaleValueMin,
                                const lu_float32_t scaleRange
                                )
{
    lu_float32_t scalingFactor;
    lu_float32_t offset;
    lu_float32_t value = inValue;

    /* Clamp the value to the given range */
    if(value > maxValue)
    {
        value = maxValue;
    }
    if(value < minValue)
    {
        value = minValue;
    }

    /* Calculate the factors for scaling */
    scalingFactor = (scaleRange)/(maxValue - minValue);
    offset = scaleValueMin - (minValue * scalingFactor);

    return ((scalingFactor * value) +  offset);
}


/*
 *********************** End of file ******************************************
 */
