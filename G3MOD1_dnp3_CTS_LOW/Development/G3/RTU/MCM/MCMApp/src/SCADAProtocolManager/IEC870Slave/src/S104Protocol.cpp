/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:S104Protocol.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   30 Sep 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "S104Protocol.h"
#include "S104Debug.h"
#include "SCADAProtocolManager.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static const lu_uint32_t PROTOCOL_POOL_DELAY_US = 50000; //Delay for protocol loop, in usec

static Logger& log(Logger::getLogger(SUBSYSTEM_ID_SCADA_HANDLER));


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
S104Protocol::S104Protocol(GeminiDatabase&  g3database):
                               Thread( SCADAProtocolManager::SCADAThreadSched,
                                       SCADAProtocolManager::SCADAThreadPrio,
                                       Thread::TYPE_JOINABLE,
                                       "S104SlaveProtocol"
                                       ),
                               m_g3db(g3database),
                               mp_twmappl(NULL),
                               m_channels()
{

}

S104Protocol::~S104Protocol()
{
    /* Close protocol application */
    if (mp_twmappl != NULL)
    {
      tmwappl_closeApplication(mp_twmappl, TMWDEFS_TRUE);
      mp_twmappl = NULL;
    }

    if(isRunning() == LU_TRUE)
    {
        stopProtocol();
    }
}


SCADAP_ERROR S104Protocol::startProtocol()
{
    DBG_INFO("%s starting IEC104 Slave Protocol",TITLE);

    mp_twmappl = tmwappl_initApplication();
    checkNotNull(mp_twmappl, "IEC104 application context creation failed");


    for (ChannelVector::iterator it = m_channels.begin();
                                 it != m_channels.end();
                                 ++it)
    {
        (*it)->openChannel(mp_twmappl);
    }

    /* Start protocol stack main loop thread */
    return (Thread::start() == THREAD_ERR_NONE)? SCADAP_ERROR_NONE :
                                                 SCADAP_ERROR_NOT_INITIALIZED;
}


SCADAP_ERROR S104Protocol::stopProtocol()
{
    if(Thread::stop() == THREAD_ERR_NONE)
    {
        Thread::join();
        return SCADAP_ERROR_NONE;
    }
    return SCADAP_ERROR_THREAD;
}


void S104Protocol::addChannel(S104Channel* channelPtr)
{
    if(channelPtr != NULL)
    {
        m_channels.push_back(channelPtr);
    }
    else
    {
        DBG_ERR("%s Try to add null channel!", __AT__);
    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
void S104Protocol::threadBody()
{
    log.info("IEC104 Slave Protocol Running");

    while(isRunning() == LU_TRUE)
    {
        if(isInterrupting() == LU_TRUE)
        {
            continue;   //exit thread;
        }

        checkConnections();

        tmwpltmr_checkTimer();

        tmwappl_checkForInput(mp_twmappl);

        g_s14EventProc.publishEvents();

        lucy_usleep(PROTOCOL_POOL_DELAY_US);
    }

    deleteAllChannels();
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void S104Protocol::checkConnections()
{
    //TODO to be implemented
}

void S104Protocol::deleteAllChannels()
{
    /* Close all channels*/
    for (ChannelVector::iterator it = m_channels.begin();
                    it != m_channels.end(); ++it)
    {
        DBG_INFO("%s closing channel...",TITLE);
        (*it)->closeChannel();
    }

    m_channels.clear();
}


/*
 *********************** End of file ******************************************
 */
