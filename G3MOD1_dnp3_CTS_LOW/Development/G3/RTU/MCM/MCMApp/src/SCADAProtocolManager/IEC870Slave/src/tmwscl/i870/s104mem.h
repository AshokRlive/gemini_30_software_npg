/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s104mem.h
 * description: IEC 60870-5-104 slave memory functions
 */
#ifndef S104MEM_DEFINED
#define S104MEM_DEFINED

#include "tmwscl/utils/tmwcnfg.h"
#include "tmwscl/utils/tmwmem.h"
#include "tmwscl/i870/s104cnfg.h"
#include "tmwscl/i870/i870mem.h"
#include "tmwscl/i870/i870mem4.h"
#include "tmwscl/i870/s14mem.h"

typedef enum S104memAllocType {
  S104MEM_SESN_TYPE,
  S104MEM_SCTR_TYPE,

  /* indicates end of enumeration */
  S104MEM_ALLOC_TYPE_MAX
} S104MEM_ALLOC_TYPE;

typedef struct {
  /* Specify number of slave 104 sessions that can be open. */
  TMWTYPES_UINT numSessions;

  /* Specify number of slave 104 sectors that can be open. */
  TMWTYPES_UINT numSectors;
} S104MEM_CONFIG;
    
#ifdef __cplusplus
extern "C" 
{
#endif

  /* routine: s104mem_initConfig
   * purpose:  initialize specified memory configuration structure,
   *  indicating the number of buffers of each structure type to 
   *  put in each memory pool. These will be initialized according 
   *  to the compile time defines. The user can change the desired
   *  fields and call s104mem_initMemory()
   * arguments:
   *  pS104Config - pointer to memory configuration structure to be filled in
   *  pS14MemConfig - pointer to memory configuration structure to be filled in
   *  pI870Mem4Config - pointer to memory configuration structure to be filled in
   *  pI870MemConfig - pointer to memory configuration structure to be filled in
   *  pTmwConfig - pointer to memory configuration structure to be filled in
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL s104mem_initConfig(
    S104MEM_CONFIG  *pS104Config,
    S14MEM_CONFIG   *pS14MemConfig,
    I870MEM4_CONFIG *pI870Mem4Config,
    I870MEM_CONFIG  *pI870MemConfig,
    TMWMEM_CONFIG   *pTmwConfig);

  /* routine: s104mem_initMemory
   * purpose: memory management init function. Can be used
   *  to modify the number of buffers that will be allowed in each
   *  buffer pool. This can only be used when TMWCNFG_USE_DYNAMIC_MEMORY
   *  is set to TMWDEFS_TRUE
   *  NOTE: This should be called before calling tmwappl_initApplication()
   * arguments:
   *  pS104Config - pointer to memory configuration structure to be used
   *  pS14MemConfig - pointer to memory configuration structure to be used
   *  pI870Mem4Config - pointer to memory configuration structure to be used
   *  pI870MemConfig - pointer to memory configuration structure to be used
   *  pTmwConfig - pointer to memory configuration structure to be used
   * returns:
   *  TMWDEFS_TRUE if successful
   *  TMWDEFS_FALSE otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s104mem_initMemory(
    S104MEM_CONFIG  *pS104Config,
    S14MEM_CONFIG   *pS14MemConfig,
    I870MEM4_CONFIG *pI870Mem4Config,
    I870MEM_CONFIG  *pI870MemConfig,
    TMWMEM_CONFIG   *pTmwConfig);

  /* routine: s104mem_init
   * purpose: INTERNAL memory management init function.
   *  NOTE: user should call s104mem_initMemory() to modify the number
   *  of buffers allowed for each type.
   * arguments:
   *  pConfig - pointer to memory configuration structure to be used
   * returns:
   *  TMWDEFS_TRUE if successful
   *  TMWDEFS_FALSE otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s104mem_init(
    S104MEM_CONFIG *pS101Config);

  /* function: s104mem_alloc
   * purpose:  Allocate memory  
   * arguments: 
   *  type - enum value indicating what structure to allocate
   * returns:
   *   TMWDEFS_NULL if allocation failed
   *   void * pointer to allocated memory if successful
   */
   void * TMWDEFS_GLOBAL s104mem_alloc(
     S104MEM_ALLOC_TYPE type);

  /* function: s104mem_free
   * purpose:  Deallocate memory
   * arguments: 
   *  pBuf - pointer to buffer to be deallocated
   * returns:    
   *   void  
   */
   void TMWDEFS_GLOBAL s104mem_free(
    void *pBuf);

  /* function: s104mem_getUsage
   * purpose:  Determine memory usage for each type of memory
   *    managed by this file.
   * arguments: 
   *  index: index of pool, starting with 0 caller can call
   *    this function repeatedly, while incrementing index. When
   *     index is larger than number of pools, this function
   *     will return TMWDEFS_FALSE
   *  pName: pointer to a char pointer to be filled in
   *  pStruct: pointer to structure to be filled in.
   * returns:    
   *  TMWDEFS_TRUE  if successfully returned usage statistics.
   *  TMWDEFS_FALSE if failure because index is too large.
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL s104mem_getUsage(
    TMWTYPES_UCHAR index,
    const TMWTYPES_CHAR **pName,
    TMWMEM_POOL_STRUCT *pStruct);

#ifdef __cplusplus
}
#endif

#endif /* S104MEM_DEFINED */

