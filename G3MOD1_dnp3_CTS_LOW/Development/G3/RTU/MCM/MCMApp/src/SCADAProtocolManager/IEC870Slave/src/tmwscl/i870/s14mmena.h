/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14mmena.h
 * description: This file is intended for internal SCL use only.
 *  IEC 60870-5-101 slave normalized measurand functionality.
 */
#ifndef S14MMENA_DEFINED
#define S14MMENA_DEFINED

#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/utils/tmwsesn.h"
#include "tmwscl/i870/s14sesn.h"
#include "tmwscl/i870/s14sctr.h"
#include "tmwscl/i870/s14dbas.h"
#include "tmwscl/i870/s14evnt.h"

/* Structure used to store step position events */
typedef struct S14MMENAEventStruct {
  S14EVNT s14Event;
  TMWTYPES_SHORT value;
} S14MMENA_EVENT;

#ifdef __cplusplus
extern "C" {
#endif
 
  /* function: s14mmena_init 
   * purpose: Initialize MMENA event processing.
   * arguments:
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL s14mmena_init(
    TMWSCTR *pSector);

    /* function: s14mmena_close 
   * purpose: Close MMENA event processing
   * arguments:
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL s14mmena_close(
    TMWSCTR *pSector);
 
  /* function: s14mmena_addEvent
   * purpose: Create and queue an MMENA event
   * arguments:
   *  pSector - identifies sector
   *  cot - cause of transmission
   *  ioa - information object address
   *  value - value, defined in 7.2.6.6
   *  flags - quality descriptor, defined in 7.2.6.3
   *  pTimeStamp - time of event
   *   if pTimeStamp != TMWDEFS_NULL Event will be sent using the ASDU specified 
   *   by sector config mmenaTimeFormat
   *    To set RES1 bit to indicate Substituted time, 
   *     set pTimeStamp->genuineTime to TMWEFS_FALSE and make sure
   *     S14DATA_SUPPORT_GENUINE_TIME is defined in s14data.h
   *   if pTimeStamp == TMWDEFS_NULL the event will be sent using ASDU 9 MMENA
   * returns:
   *  void * - non-NULL value indicates success; TMWDEFS_NULL indicates failure
   */
  TMWDEFS_SCL_API void * TMWDEFS_GLOBAL s14mmena_addEvent(
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot,
    TMWTYPES_ULONG ioa, 
    TMWTYPES_SHORT value, 
    TMWTYPES_UCHAR flags, 
    TMWDTIME *pTimeStamp);
 
  /* function: s14mmena_countEvents
   * purpose: count MMENA events
   * arguments:
   *  pSector - identifies sector
   * returns:
   *  number of events in queue
   */
  TMWTYPES_USHORT TMWDEFS_GLOBAL s14mmena_countEvents(
    TMWSCTR *pSector);

  /* function: s14mmena_scanForChanges
   * purpose: Process MMENA events into a message
   * arguments:
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   *  buildResponse - TMWDEFS_TRUE if this function should build
   *   and send the response message, otherwise simply return
   *   TMWDEFS_TRUE if data is ready to send.
   * returns:
   *  TMWDEFS_TRUE data is ready to send or was sent, else TMWDEFS_FALSE
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14mmena_scanForChanges(
    TMWSCTR *pSector);

  /* function: s14mmena_processEvents
   * purpose: Process MMENA events into a message
   * arguments:
   * arguments:
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   *  pEventTime - returns time of earliest event if a clock sync is required
   * returns:
   *  S14EVNT_SENT                - ASDU was sent
   *  S14EVNT_CLOCK_SYNC_REQUIRED - Spontaneous CCSNA is required
   *  S14EVNT_NOT_SENT            - No ASDU was sent
   */
  S14EVNT_STATUS TMWDEFS_GLOBAL s14mmena_processEvents(
    TMWSCTR *pSector,
    TMWDTIME *pEventTime);
 
  /* function: s14mmena_readIntoResponse 
   * purpose: Read the MMENA values for the point specified and store in response
   *  message to be sent to master
   * arguments:
   *  pTxData - pointer
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   *  cot - cause of tranmission
   *  groupMask - mask specifying what group(s) points should be in
   *  ioa - information object address
   *  pPointIndex - pointer to index of point to be read. This should be updated
   *   by this function.
   * returns:
   *   status
   */
  S14DBAS_GROUP_STATUS TMWDEFS_CALLBACK s14mmena_readIntoResponse(
    TMWSESN_TX_DATA *pTxData, 
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot, 
    TMWDEFS_GROUP_MASK groupMask, 
    TMWTYPES_ULONG ioa,
    TMWTYPES_USHORT *pPointIndex);

#ifdef __cplusplus
}
#endif
#endif /* S14MMENA_DEFINED */
