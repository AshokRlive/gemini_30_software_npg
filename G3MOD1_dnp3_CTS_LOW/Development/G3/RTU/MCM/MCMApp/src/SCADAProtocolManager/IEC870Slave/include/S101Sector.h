/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:S101Sector.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief Protocol Sector - TMW artifact for handling part of a session
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19 Jul 2016     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef S101SECTOR_H_
#define S101SECTOR_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "S101TMWIncludes.h"
#include "S101Database.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class   S101Session;    /* Forward declaration*/


class S101Sector
{
public:
    S101Sector(S101Session& session);
    virtual ~S101Sector();

    S101Database& getS101Database();

    SCADAP_ERROR openSector(TMWSESN* tmwsesn);

    void closeSector();

    void configure(const S101SCTR_CONFIG& cnfg);

    SCADAP_ERROR getSectorHandle(TMWSCTR** ppSectorHdle)
    {
        *ppSectorHdle = mp_tmwsector;
        return (mp_tmwsector != NULL)? SCADAP_ERROR_NONE : SCADAP_ERROR_NULL_POINTER;
    }

    S101Session& getSession() {return m_session;}

private:
    S101Session&  m_session;      // Owner session reference

    TMWSCTR*            mp_tmwsector;
    S101SCTR_CONFIG*    mp_tmwsectorCnfg;

    S101Database        m_s101db;       // Sector database
    I870DBHandleStr     m_twmdbhandle;  // Database handle passed to TMW lib.

};


#endif /* S101SECTOR_H_ */

/*
 *********************** End of file ******************************************
 */
