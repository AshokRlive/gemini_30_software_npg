/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i870dia1.h
 * description: Diagnostic routines for i870lnk1
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/utils/tmwtarg.h"
#include "tmwscl/utils/tmwcnfg.h"

#include "tmwscl/i870/i870dia1.h"
#include "tmwscl/i870/i870ft12.h"

#if TMWCNFG_SUPPORT_DIAG

/* Maximum number of bytes to display on a single row */
#define MAX_ROW_LENGTH 16

/* Error messages used by both master and slave ft1.2 101/103 */
static const I870DIAG_ERROR_ENTRY i870Dia1ErrorMsg[] = {
  {I870DIA1_BALANCED_NA        ,  "Balanced Mode not supported for IEC 60870-5-103"},
  {I870DIA1_RCVD_INV_DIR       ,  "Received invalid dir bit in control"},
  {I870DIA1_ALLOC_SESN         ,  "Error allocating session info"},
  {I870DIA1_INV_FCB            ,  "Invalid frame count bit"},
  {I870DIA1_TEST_BALANCED      ,  "Test function request received on unbalanced link"},
  {I870DIA1_ACD_BALANCED       ,  "Request for access demand received on balanced link"},
  {I870DIA1_USER_DATA_BALANCED ,  "Request for user data received on balanced link"},
  {I870DIA1_PRIMARY_FC         ,  "Unrecognized primary function code"},
  {I870DIA1_REPLY_WRONG_SESN   ,  "Received reply from wrong session"},
  {I870DIA1_RCVD_INV_SECONDARY ,  "Received invalid secondary frame"},
  {I870DIA1_USER_DATA          ,  "User data response received on balanced link"},
  {I870DIA1_RESPOND_NO_DATA    ,  "No data response received on balanced link"},
  {I870DIA1_UNSUPPORTED_FC     ,  "Unsupported function code received: NOT_FUNCTIONING"},
  {I870DIA1_FC_NOT_USED        ,  "Remote device returned 'Function Code Not Used'"},
  {I870DIA1_UNRECOGNIZED_FC    ,  "Unrecognized function code received"},
  {I870DIA1_INVALID_LENGTH     ,  "Invalid last character received, frame rejected, verify link address size"},
  {I870DIA1_INVALID_CHKS       ,  "Invalid checksum, frame rejected"},
  {I870DIA1_NOT_RESET          ,  "Link has not been reset, frame rejected"},
  {I870DIA1_WRONG_REPLY        ,  "Received unexpected reply, frame rejected"},
  {I870DIA1_RCV_LENGTH         ,  "Received length exceeds RX frame size"},
    
  {I870DIA1_ERROR_ENUM_MAX     , ""}
};

/* Array to determine if specific error messages are disabled.
 */
static TMWTYPES_UCHAR _errorMsgDisabled[(I870DIA1_ERROR_ENUM_MAX/8)+1];


/* function: _getFunctionCodeString
 * purpose: return a string representation of the function code
 *  to display in diagnostics messages.
 * arguments:
 *  ctrl - link level control field
 * returns:
 *  string to display
 */
static const char * TMWDEFS_LOCAL _getFunctionCodeString(
  TMWTYPES_UCHAR ctrl)
{
  if(ctrl & I870FT12_CONTROL_PRM)
  {
    switch(ctrl & I870FT12_CONTROL_FC_MASK)
    {
    case I870FT12_FC_LINK_RESET_CU:
      return("Primary Frame - Reset of Remote Link");
    case I870FT12_FC_RESET_USER:
      return("Primary Frame - Reset of User Process");
    case I870FT12_FC_TEST_FUNCTION:
      return("Primary Frame - Test Function");
    case I870FT12_FC_USER_DATA:
      return("Primary Frame - Confirmed User Data");
    case I870FT12_FC_UNCONFIRMED_DATA:
      return("Primary Frame - Unconfirmed Data");
    case I870FT12_FC_LINK_RESET_FCB:
      return("Primary Frame - Reset Frame Count Bit");
    case I870FT12_FC_REQUEST_ACCESS:
      return("Primary Frame - Request Access Demand");
    case I870FT12_FC_REQUEST_STATUS:
      return("Primary Frame - Request Status of Link");
    case I870FT12_FC_REQUEST_DATA_C1:
      return("Primary Frame - Request User Data Class 1");
    case I870FT12_FC_REQUEST_DATA_C2:
      return("Primary Frame - Request User Data Class 2");
    }
  }
  else
  {
    switch(ctrl & I870FT12_CONTROL_FC_MASK)
    {
    case I870FT12_FC_CONFIRM_ACK:
      return("Secondary Frame - Confirm ACK");
    case I870FT12_FC_CONFIRM_NACK:
      return("Secondary Frame - Confirm NACK");
    case I870FT12_FC_RESPOND_USER_DATA:
      return("Secondary Frame - Respond User Data");
    case I870FT12_FC_RESPOND_NO_DATA:
      return("Secondary Frame - Respond Data Not Available");
    case I870FT12_FC_RESPOND_STATUS:
      return("Secondary Frame - Respond Status Of Link");
    case I870FT12_FC_NOT_FUNCTIONING:
      return("Secondary Frame - Not Functioning");
    case I870FT12_FC_NOT_USED:
      return("Secondary Frame - Function Code Not Used");
    case I870FT12_FC_NO_CONFIRM:
      return("Secondary Frame - No Confirm");
    }
  }

  return("Unknown frame");
}

/* function: _displayLinkFrame
 * purpose: display link level frame
 * arguments:
 *  prompt - prompt for text
 *  channelName - name of channel
 *  pBuf - header data
 *  numBytes - number of bytes
 * returns:
 *  void
 */
static void TMWDEFS_LOCAL _displayLinkFrame(
  TMWSESN *pSession,
  const char *prompt, 
  const TMWTYPES_UCHAR *pBuf, 
  TMWTYPES_USHORT numBytes,
  TMWTYPES_USHORT retry,
  TMWDIAG_ID direction)
{
  I870LNK1_CONTEXT *pLinkContext = (I870LNK1_CONTEXT *)pSession->pChannel->pLinkContext;
  TMWTYPES_UCHAR headerLength;
  TMWTYPES_USHORT address = 0;
  TMWTYPES_CHAR retryBuf[32];
  TMWTYPES_UCHAR control;
  TMWTYPES_CHAR buf[256];
  TMWDIAG_ANLZ_ID id;
  int rowLength;
  int index;
  int len;
  int i;

  if(tmwdiag_initId(&id, TMWDEFS_NULL, pSession, TMWDEFS_NULL, TMWDIAG_ID_LINK | direction) == TMWDEFS_FALSE)
  {
    return;
  }

  switch(pBuf[I870FT12_INDEX_SYNC])
  {
  case I870FT12_SYNC_OCTET_SINGLE:
    tmwtarg_snprintf(buf, sizeof(buf), "%s %-10s %s\n", 
      prompt, tmwsesn_getChannelName(pSession),
      "Single character acknowledge");

    tmwdiag_skipLine(&id);
    tmwdiag_putLine(&id, buf);
    return;

  case I870FT12_SYNC_OCTET_FIXED:
    control = pBuf[I870FT12_INDEX_FIX_CONTROL];

    switch(pLinkContext->linkAddressSize)
    {
    case 0:
      address = 0;
      break;
    case 1:
      address = pBuf[I870FT12_INDEX_FIX_ADDR1_ADDR];
      break;
    case 2:
      tmwtarg_get16(&pBuf[I870FT12_INDEX_FIX_ADDR1_ADDR], &address);
      break;
    }

    headerLength = (TMWTYPES_UCHAR)(4 + pLinkContext->linkAddressSize);
    break;

  case I870FT12_SYNC_OCTET_VARIABLE:
    control = pBuf[I870FT12_INDEX_VAR_CONTROL];

    switch(pLinkContext->linkAddressSize)
    {
    case 0:
      address = 0;
      break;
    case 1:
      address = pBuf[I870FT12_INDEX_VAR_ADDRESS];
      break;
    case 2:
      tmwtarg_get16(&pBuf[I870FT12_INDEX_VAR_ADDRESS], &address);
      break;
    }

    headerLength = (TMWTYPES_UCHAR)(5 + pLinkContext->linkAddressSize);
    break;

  default:
    tmwtarg_snprintf(buf, sizeof(buf), "%s %-10s %s\n", 
      prompt, tmwsesn_getChannelName(pSession), "Unknown sync character in frame");

    tmwdiag_skipLine(&id);
    tmwdiag_putLine(&id, buf);
    return;
  }

  /* Retry Info */
  retryBuf[0] = '\0';
  if(retry != 0)
    tmwtarg_snprintf(retryBuf, sizeof(retryBuf), "Retry %d", retry);

  /* Display header information */
  tmwtarg_snprintf(buf, sizeof(buf), "%s %-10s %s %s\n", 
    prompt, tmwsesn_getChannelName(pSession), _getFunctionCodeString(control), retryBuf);

  tmwdiag_skipLine(&id);
  tmwdiag_putLine(&id, buf);

  if(control & I870FT12_CONTROL_PRM)
  {
    tmwtarg_snprintf(buf, sizeof(buf), "%16sDIR(%s) PRM(%s) FCV(%s) FCB(%s) ADDR(%d)\n", 
      " ",
      ((control & I870FT12_CONTROL_DIR) == 0) ? "0" : "1",    
      ((control & I870FT12_CONTROL_PRM) == 0) ? "0" : "1",    
      ((control & I870FT12_CONTROL_FCV) == 0) ? "0" : "1",    
      ((control & I870FT12_CONTROL_FCB) == 0) ? "0" : "1",    
      address); 

    tmwdiag_putLine(&id, buf);
  }
  else
  {
    tmwtarg_snprintf(buf, sizeof(buf), "%16sDIR(%s) PRM(%s) DFC(%s) ACD(%s) ADDR(%d)\n", 
      " ",
      ((control & I870FT12_CONTROL_DIR) == 0) ? "0" : "1",    
      ((control & I870FT12_CONTROL_PRM) == 0) ? "0" : "1",    
      ((control & I870FT12_CONTROL_DFC) == 0) ? "0" : "1",    
      ((control & I870FT12_CONTROL_ACD) == 0) ? "0" : "1",    
      address); 

    tmwdiag_putLine(&id, buf);
  }

  /* Now display the actual bytes */
  len = tmwtarg_snprintf(buf, sizeof(buf), "%16s", " ");
  for(i = 0; i < headerLength; i++)
    len += tmwtarg_snprintf(buf + len, sizeof(buf) - len, "%02x ", pBuf[i]);

  tmwtarg_snprintf(buf + len, sizeof(buf) - len, "\n");
  tmwdiag_putLine(&id, buf);

  index = headerLength;
  while(index < numBytes)
  {
    len = tmwtarg_snprintf(buf, sizeof(buf), "%16s", " ");

    rowLength = numBytes - index;
    if(rowLength > MAX_ROW_LENGTH)
      rowLength = MAX_ROW_LENGTH;

    for(i = 0; i < rowLength; i++)
      len += tmwtarg_snprintf(buf + len, sizeof(buf) - len, "%02x ", pBuf[index++]);

    tmwtarg_snprintf(buf + len, sizeof(buf) - len, "\n");
    tmwdiag_putLine(&id, buf);
  }
}

/* routine: i870dia1_validateErrorTable */
TMWTYPES_BOOL i870dia1_validateErrorTable(void)
{
  int i;
  for(i=0; i<I870DIA1_ERROR_ENUM_MAX;i++)
  {
    if(i870Dia1ErrorMsg[i].errorNumber != i)
      return(TMWDEFS_FALSE);
  }

  return(TMWDEFS_TRUE);
}

/* routine: i870dia1_init */
void TMWDEFS_GLOBAL i870dia1_init()
{
  /* No error messages are disabled by default. */
  memset(_errorMsgDisabled, 0, (I870DIA1_ERROR_ENUM_MAX/8)+1);
}

/* function: i870dia1_linkFrameSent */
void TMWDEFS_GLOBAL i870dia1_linkFrameSent(
  TMWSESN *pSession, 
  const TMWTYPES_UCHAR *pBuf, 
  TMWTYPES_USHORT numBytes,
  TMWTYPES_USHORT retry)
{
  _displayLinkFrame(pSession, "<---", pBuf, numBytes, retry, 0);
}

/* function: i870dia1_linkFrameReceived */
void TMWDEFS_GLOBAL i870dia1_linkFrameReceived(
  TMWSESN *pSession, 
  const TMWTYPES_UCHAR *pBuf, 
  TMWTYPES_USHORT numBytes)
{
  _displayLinkFrame(pSession, "--->", pBuf, numBytes, 0, TMWDIAG_ID_RX);
}

/* function: i870dia1_linkFrameReceived */
void TMWDEFS_GLOBAL i870dia1_linkAddressUnknown(
  TMWCHNL *pChannel,
  TMWTYPES_USHORT rxAddress)
{
  char buf[64];
  (void)tmwtarg_snprintf(buf, sizeof(buf), "Link address unknown, frame rejected, address %d", rxAddress);
  TMWDIAG_CHNL_ERROR(pChannel, buf);
}

/* function: i870dia1_errorMsgEnable */
TMWTYPES_BOOL TMWDEFS_GLOBAL i870dia1_errorMsgEnable(
  I870DIA1_ERROR_ENUM errorNumber,
  TMWTYPES_BOOL value)
{
  if(errorNumber > I870DIA1_ERROR_ENUM_MAX)
    return(TMWDEFS_FALSE);

  if(value)
    _errorMsgDisabled[errorNumber/8] &=  ~(1 <<(errorNumber%8));
  else
    _errorMsgDisabled[errorNumber/8] |=  (1 <<(errorNumber%8));

  return(TMWDEFS_TRUE);
}

/* function: _errorMsgEnabled */
static TMWTYPES_BOOL TMWDEFS_LOCAL _errorMsgDisabledFunc(
   I870DIA1_ERROR_ENUM errorNumber)
{
  int value;

  if(errorNumber > I870DIA1_ERROR_ENUM_MAX)
    return(TMWDEFS_TRUE);

  value = _errorMsgDisabled[errorNumber/8] & (1 <<(errorNumber%8));
  if(value == 0)
    return(TMWDEFS_FALSE);
  else
    return(TMWDEFS_TRUE);
}

/* function: i870dia1_error */
void TMWDEFS_GLOBAL i870dia1_error(
  TMWCHNL *pChannel,
  TMWSESN *pSession, 
  I870DIA1_ERROR_ENUM errorNumber)
{
  const char *pName;
  TMWDIAG_ANLZ_ID anlzId;
  char buf[256];

#ifdef TMW_SUPPORT_MONITOR
  /* If in analyzer or listen only mode, do not display this error */
  if(pChannel != TMWDEFS_NULL && pChannel->pPhysContext->monitorMode)
#endif
  if(_errorMsgDisabledFunc(errorNumber))
    return;

  if(tmwdiag_initId(&anlzId, pChannel, pSession, TMWDEFS_NULL, TMWDIAG_ID_LINK | TMWDIAG_ID_ERROR) == TMWDEFS_FALSE)
  {
    return;
  }

  if(pSession == TMWDEFS_NULL)
  {
    pName = tmwchnl_getChannelName(pChannel);
  }
  else
  {
    pName = tmwsesn_getSessionName(pSession);
  }

  if(pName == (const char *)0)
    pName = " ";

  (void)tmwtarg_snprintf(buf, sizeof(buf), "**** %s   %s ****\n", pName, i870Dia1ErrorMsg[errorNumber].pErrorMsg);

  tmwdiag_skipLine(&anlzId);
  tmwdiag_putLine(&anlzId, buf);
}

#endif /* TMWCNFG_SUPPORT_DIAG */
