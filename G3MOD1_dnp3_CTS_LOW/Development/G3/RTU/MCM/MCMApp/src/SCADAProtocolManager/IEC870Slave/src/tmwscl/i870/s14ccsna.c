/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/


/* file: s14ccsna.c
 * description: IEC 60870-5-101 slave CCSNA (Clock Synchronization Command)
 *  functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14ccsna.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/i14diag.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/i870chnl.h"
#include "tmwscl/utils/tmwtarg.h"

#if S14DATA_SUPPORT_CCSNA

/* function: s14ccsna_processRequest */
void TMWDEFS_CALLBACK s14ccsna_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  TMWTYPES_BOOL sendNegativeConfirm;
  TMWTYPES_USHORT processingDelay;
  TMWTYPES_USHORT savedOffset;
  TMWDTIME newTime;

  /* Store originator address */
  p14Sector->ccsnaOriginator = pMsg->origAddress;
 
  /* Parse Information Object Address */
  i870util_readInfoObjAddr(pSector->pSession, pMsg, &p14Sector->ccsnaIOA);
    
  /* Store current time for clock sync response */
  s14util_getDateTime(pSector, &p14Sector->ccsnaTime);

  /* Parse Time */
  savedOffset = pMsg->offset;
  i870util_read56BitTime(pMsg, &newTime);
 
  /* Parse received cause of transmission */
  sendNegativeConfirm = TMWDEFS_FALSE;
  if(pMsg->cot == I14DEF_COT_ACTIVATION)
  {
    if(p14Sector->ccsnaIOA == 0)
    {
      p14Sector->ccsnaCOT = I14DEF_COT_ACTCON;
    }
    else
    {
      p14Sector->ccsnaCOT = I14DEF_COT_UNKNOWN_IOA | I14DEF_COT_NEGATIVE_CONFIRM;
      sendNegativeConfirm = TMWDEFS_TRUE;
    }
  }
  else
  {
    p14Sector->ccsnaCOT = I14DEF_COT_UNKNOWN_COT | I14DEF_COT_NEGATIVE_CONFIRM;
    sendNegativeConfirm = TMWDEFS_TRUE;
  }
 
  if(sendNegativeConfirm)
  {
    /* get message time again so that it can be sent in negative response */
    pMsg->offset = savedOffset;
    i870util_read56BitTime(pMsg, &p14Sector->ccsnaTime);
    return;
  }

  /* Add in processing delay */
  processingDelay = (TMWTYPES_USHORT)
    (tmwtarg_getMSTime() - pMsg->pRxData->firstByteTime);

  tmwdtime_addOffset(&newTime, processingDelay);
  tmwdtime_subtractOffset(&p14Sector->ccsnaTime, processingDelay);

  /* Add in propagation delay */
  tmwdtime_addOffset(&newTime, p14Sector->propagationDelay);
  tmwdtime_subtractOffset(&p14Sector->ccsnaTime, p14Sector->propagationDelay);

  /* Diagnostics */
  I14DIAG_SHOW_STORE_TYPE(pSector, pMsg);
  I14DIAG_SHOW_TIME(pSector, &newTime);

#ifdef TMW_SUPPORT_MONITOR 
  /* If in monitor mode, do not set the time */
  if(!pSector->pSession->pChannel->pPhysContext->monitorMode)
#endif
  /* Call database function to set new time
   * this allows customer flexibility to disable this
   * or adjust this time.
   */
  if(s14data_ccsnaSetTime(p14Sector->i870.pDbHandle, &newTime))
  {
    /* Start clock valid timer */
    if(p14Sector->clockValidPeriod != 0)
    {
      tmwtimer_start(&p14Sector->clockValidTimer, 
        p14Sector->clockValidPeriod, pSector->pSession->pChannel,
        TMWDEFS_NULL, TMWDEFS_NULL);
    }
  }
  else
  { 
    /* get message time again so that it can be sent in negative response */
    pMsg->offset = savedOffset;
    i870util_read56BitTime(pMsg, &p14Sector->ccsnaTime);
    p14Sector->ccsnaCOT |= I14DEF_COT_NEGATIVE_CONFIRM;
  }
}

/* function: s14ccsna_buildResponse */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14ccsna_buildResponse( 
  TMWSCTR *pSector, 
  TMWTYPES_BOOL buildResponse)
{
  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  if(p14Sector->ccsnaCOT != 0)
  {
    if(buildResponse)
    {
      /* Activation confirmation or Deactivation */
      TMWSESN_TX_DATA *pTxData;
      S14SESN *pS14Session = (S14SESN *)pSector->pSession;

      if((pTxData = i870chnl_newTxData(pSector->pSession->pChannel, 
        pSector->pSession, pSector, pS14Session->maxASDUSize)) == TMWDEFS_NULL)
      {
        return(TMWDEFS_FALSE);
      }

#if TMWCNFG_SUPPORT_DIAG
      pTxData->pMsgDescription = "Clock Synchronization Response";
#endif
      pTxData->txFlags = TMWSESN_TXFLAGS_NO_RESPONSE;
      
      /* Build response */
      i870util_buildMessageHeader(pSector->pSession, 
        pTxData, I14DEF_TYPE_CCSNA1, p14Sector->ccsnaCOT, 
        p14Sector->ccsnaOriginator, p14Sector->i870.asduAddress);

      /* Store point number */
      i870util_storeInfoObjAddr(pSector->pSession, pTxData, p14Sector->ccsnaIOA);

      /* Store time */
      i870util_write56BitTime(pTxData, &p14Sector->ccsnaTime, TMWDEFS_FALSE);

      p14Sector->ccsnaRespNumber++;

      /* Request is complete */
      p14Sector->ccsnaCOT = 0;

      /* Send the response */
      i870chnl_sendMessage(pTxData);
    }

    return(TMWDEFS_TRUE);
  }

  return(TMWDEFS_FALSE);
}

/* function: s14ccsna_getRespNumber */
TMWTYPES_UCHAR TMWDEFS_GLOBAL s14ccsna_getRespNumber(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  /* If a time sync response is outstanding, return 1 more
   * than the current number to delay the spontaneous event
   * until after the response to the time sync is sent.
   */
  if(p14Sector->ccsnaCOT)
  {
    return((TMWTYPES_UCHAR)(p14Sector->ccsnaRespNumber + 1));
  }
  else
  {
    return(p14Sector->ccsnaRespNumber);
  }
}
 
/* function: s14ccsna_responseRequired */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14ccsna_responseRequired(
  TMWSCTR *pSector,
  TMWTYPES_UCHAR ccsnaRespNumber)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector; 
 
  if((!p14Sector->ccsnaCOT)
    ||(ccsnaRespNumber == p14Sector->ccsnaRespNumber))
  {
    return(TMWDEFS_FALSE);
  }
  else
  {
    return(TMWDEFS_TRUE);
  }
}

#endif /* S14DATA_SUPPORT_CCSNA */
