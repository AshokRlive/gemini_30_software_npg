/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:ISCADAConnectionManager.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   29 Sep 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef ISCADACONNECTIONMANAGER_H_
#define ISCADACONNECTIONMANAGER_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"

#include "IChannel.h"
#include "PointID.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
class ISCADAConnectionManager
{
protected:

    enum TIMER
    {
        TIMER_CONNECT = 0,
        TIMER_COMS_INACTIVITY,
        TIMER_CONN_CHECK,
        TIMER_CONN_CHECK_CONNECT,
        TIMER_CONNECTION_DELAY,
        TIMER_NO_INCOMING_CALL,
        TIMER_LAST
    };

public:

    struct SocketConfig
    {
        SocketConfig():ipAddress(""),port(0) {}

        std::string ipAddress;
        lu_int32_t  port;
    };

    struct CommsPowerSupplyConfig
    {
        CommsPowerSupplyConfig():powerSupplyChanPtr(NULL), durationSecs(0), initTimeSecs(0) {}

        IChannel    *powerSupplyChanPtr;    // Channel reference for output channel
        PointIdStr  powerCycleInhibit;  // Point reference for power cycle inhibition
        lu_uint32_t durationSecs;           // Length of time to cycle power for
        lu_uint32_t initTimeSecs;           // Time for comms device to initialise
    };

    struct IPConnectivityCheckConfig
    {
        IPConnectivityCheckConfig():enabled(LU_FALSE),checkIPPort(0),periodMin(0),retries(0) {}

        lu_bool_t   enabled;             // Regular SCADA connectivity check
        lu_uint32_t checkIPPort;         // Regular SCADA connectivity IP Port
        lu_uint32_t periodMin;           // Regular SCADA connectivity check period
        lu_uint32_t retries;             // Retries
    };

    struct OutgoingConnectionsConfig
    {
        OutgoingConnectionsConfig():enabled(LU_FALSE), retries(0) {}

        lu_bool_t   enabled;
        lu_uint32_t retries;  // Number of tries of the whole Failover Group
    };

    struct IncomingConnectionsConfig
    {
        IncomingConnectionsConfig():enabled(LU_FALSE), notReceivedHrs(0) {}

        lu_bool_t   enabled;
        lu_uint32_t notReceivedHrs;  // If no incoming call received for this time, reset comms device
    };

    struct ConnectionSettingsConfig
    {
        ConnectionSettingsConfig():connectOnEventClassMask(0), inactivityTimeSecs(30), connectionRetryDelaySecs(0) {}

        lu_uint8_t  connectOnEventClassMask;   // >0 Events of this class causes SCADA Connection
        lu_uint32_t inactivityTimeSecs;        // Close sockets after this time of no comms
        lu_uint32_t connectionRetryDelaySecs;  // Time to wait between retries of the failover group
    };

    struct SCADAConnManConfig
    {
        SCADAConnManConfig() : m_validateMasterIP(false) {}

        CommsPowerSupplyConfig    m_commsPowerSupplyConfig;
        IPConnectivityCheckConfig m_connectivityCheckConfig;
        OutgoingConnectionsConfig m_outgoingConConfig;
        IncomingConnectionsConfig m_incomingConConfig;
        ConnectionSettingsConfig  m_connectionSettingsConfig;
        bool                      m_validateMasterIP;    // Only allow incoming connections from IP Addresses in failover group
    };

public:

    ISCADAConnectionManager(){};
    virtual ~ISCADAConnectionManager(){};

    virtual void setSCADAConnectionIPConfig(std::string ipAddress, lu_int32_t port) = 0;

    virtual void setSCADAListenerIPConfig(std::string ipAddress, lu_int32_t port) = 0;

    virtual void setSCADAConnManConfig(SCADAConnManConfig config) = 0;

    virtual void init() = 0;

    virtual SocketConfig *getSCADAConnectionIPConfig() = 0;

    virtual SocketConfig *getSCADAListenerIPConfig() = 0;

    /**
     * Checks current connection status and switches channel as necessary.
     */
    virtual void tickEvent() = 0;

    virtual void checkTimers() = 0;
};


#endif /* ISCADACONNECTIONMANAGER_H_ */

/*
 *********************** End of file ******************************************
 */
