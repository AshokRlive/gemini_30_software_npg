/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14evnt.h
 * description: This file is intended for internal SCL use only.
 *   Base class for IEC 60870-5-101/104 slave events types
 */
#ifndef S14EVNT_DEFINED
#define S14EVNT_DEFINED

#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/utils/tmwdtime.h"
#include "tmwscl/utils/tmwsctr.h"
#include "tmwscl/utils/tmwdlist.h"

#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/s14mem.h"

/* Base structure used to store 101 and 104 events */
typedef struct S14EventStruct {
  /* List Member, must be first entry */
  TMWDLIST_MEMBER listMember;

  TMWTYPES_ULONG   ioa; 
  TMWTYPES_UCHAR   typeId;
  TMWTYPES_UCHAR   cot;
  TMWTYPES_UCHAR   quality;
  TMWTYPES_UCHAR   ccsnaRespNumber;
  TMWTYPES_BOOL    timeSpecified;
  TMWTYPES_BOOL    eventSent;
  TMWTYPES_BOOL    doubleTransmission;
  TMWSESN_TX_DATA *pTxData;
  TMWDTIME timeStamp;
} S14EVNT;

typedef enum {
  S14EVNT_SENT,
  S14EVNT_CCSNA_RESPONSE_REQUIRED,
  S14EVNT_CCSNA_SPONT_REQUIRED,
  S14EVNT_NOT_SENT
} S14EVNT_STATUS;

/* function to get point handle */
typedef void *(*S14EVNT_GET_POINT_FUNC)(
  void *pHandle,
  TMWTYPES_USHORT index);

/* function to determine if a data point changed and if it did
 * add an event to the queue with the current values
 */
typedef TMWTYPES_BOOL (*S14EVNT_CHANGED_FUNC)(
  TMWSCTR *pSector, 
  void *pPoint,
  TMWDTIME *pTimeStamp);

/* function to copy the data from the event into the message 
 * to be sent to master 
 */
typedef void TMWDEFS_GLOBAL (*S14EVNT_DATA_FUNC)(
  TMWSESN_TX_DATA *pTxData, 
  S14EVNT *pEvent);

/* Event descriptor structure */
typedef struct S14EventDesc {
  TMWTYPES_UCHAR typeId;
  TMWTYPES_UCHAR woTimeTypeId;

  /* This is used for sending only events with COT REMOTE during a csc, cdc, crc command 
   * Other events should be sent after a command completes
   */
  TMWTYPES_UCHAR cotSpecified;

  S14MEM_ALLOC_TYPE eventMemType;
  TMWDEFS_EVENT_MODE eventMode;
  TMWTYPES_BOOL scanEnabled;
  TMWTYPES_USHORT maxEvents;
  TMWDLIST *pEventList;
  TMWDEFS_TIME_FORMAT timeFormat;
  S14EVNT_DATA_FUNC pEventDataFunc;
  S14EVNT_CHANGED_FUNC pChangedFunc;
  S14EVNT_GET_POINT_FUNC pGetPointFunc;
  TMWTYPES_BOOL *pEventsOverflowedFlag;

  TMWTYPES_BOOL doubleTransmission;
} S14EVNT_DESC;

#ifdef __cplusplus
extern "C" {
#endif

  /* function: s14evnt_scanForChanges 
   * purpose: Common function to scan for changes to any
   *  points of the type specified by the event descriptor.
   *  This function will check to see whether scanning is enabled
   *  for this type of point. If a point changed, an event will 
   *  be added for this point.
   * arguments:
   *  pSector - identifies sector
   *  pDesc - pointer to event descriptor
   * returns:
   *  TMWDEFS_TRUE if any points of the type specified have changed
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14evnt_scanForChanges(
    TMWSCTR *pSector,
    S14EVNT_DESC *pDesc);
  
  /* function: s14evnt_countEvents  
   * purpose: Count the number of events in the list that have not been sent
   * arguments: 
   *  pSector - identifies sector
   *  pEventList - pointer to the event list
   * returns:
   *  Number of events in this list.
   */
  TMWTYPES_USHORT TMWDEFS_GLOBAL s14evnt_countEvents(
    TMWSCTR *pSector,
    TMWDLIST *pEventList);

  /* function: s14evnt_addEvent 
   * purpose: Allocate memory for the type of event specified by the event descriptor
   *  and add the event to the list specified. This function will check for mode and
   *  list full. Upon return from this function the caller can add their type specific
   *  data to the event structure that was allocated and queued.
   * arguments:
   *  pSector - identifies sector
   *  cot - cause of transmission for this event
   *  ioa - information object address for this event
   *  quality - quality bits for this event
   *  pTimeStamp - pointer to structure containing time that this event occurred
   *  pDesc - pointer to event descriptor structure 
   * returns:
   *  void
   */
  TMWDEFS_SCL_API S14EVNT * TMWDEFS_GLOBAL s14evnt_addEvent(
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot,
    TMWTYPES_ULONG ioa, 
    TMWTYPES_UCHAR quality,
    TMWDTIME *pTimeStamp,
    S14EVNT_DESC *pDesc,
    void **pDoubleTransEvent
  ); 

  /* function: s14evnt_reasonToCOT  
   * purpose:  Convert reason to cause of transmission
   * arguments:
   *  reason - reason for event
   * returns:
   *  cause of transmission
   */
  TMWDEFS_SCL_API TMWTYPES_UCHAR TMWDEFS_GLOBAL s14evnt_reasonToCOT(
    TMWDEFS_CHANGE_REASON reason);

  /* function: s14evnt_processEvents 
   * purpose: process events for the type described by this evvent descriptor
   * arguments:
   *  pSector - identifies sector
   *  pDesc - pointer to descriptor structure 
   *  dataSize - size of data in ASDU
   *  pEventTime - pointer to time structure to be filled in if spontaneous
   *   clock sync is required
   * returns:
   *  S14EVENT_SENT                - ASDU was sent
   *  S14EVENT_CLOCK_SYNC_REQUIRED - Spontaneous CCSNA is required
   *  S14EVENT_NOT_SENT            - No ASDU was sent
   */
  S14EVNT_STATUS TMWDEFS_GLOBAL s14evnt_processEvents(
    TMWSCTR *pSector,
    S14EVNT_DESC *pDesc,
    TMWTYPES_UCHAR dataSize,
    TMWDTIME *pEventTime,
    TMWTYPES_BOOL onlyDoubleTrans);

  /* function: s14event_linkDataReady
   * purpose: Tell the link layer we have data to send
   * arguments:
   *  pSector - identifies sector
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL s14event_linkDataReady(
    TMWSCTR *pSector);

#ifdef __cplusplus
}
#endif
#endif /* S14EVNT_DEFINED */
