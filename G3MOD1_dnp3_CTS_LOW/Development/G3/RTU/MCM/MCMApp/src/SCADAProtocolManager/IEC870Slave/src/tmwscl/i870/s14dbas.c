/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14dbas.c
 * description: IEC 60870-5-101/104 slave database interface
 */
/* Misc Header Files */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/s14dbas.h"
#include "tmwscl/i870/s14data.h"

/* ASDU Specific Processing */
#include "tmwscl/i870/s14msp.h"
#include "tmwscl/i870/s14mdp.h"
#include "tmwscl/i870/s14mst.h"
#include "tmwscl/i870/s14mbo.h"
#include "tmwscl/i870/s14mit.h"
#include "tmwscl/i870/s14mitc.h"
#include "tmwscl/i870/s14mctna.h"
#include "tmwscl/i870/s14mmena.h"
#include "tmwscl/i870/s14mmenb.h"
#include "tmwscl/i870/s14mmenc.h"
#include "tmwscl/i870/s14mpsna.h"
#include "tmwscl/i870/s14mmend.h"
#include "tmwscl/i870/s14pmena.h"
#include "tmwscl/i870/s14pmenb.h"
#include "tmwscl/i870/s14pmenc.h"

typedef S14DBAS_GROUP_STATUS (*S14DBAS_GROUP_READ_FUNC)(
  TMWSESN_TX_DATA *pTxData, TMWSCTR *pSector, 
  TMWTYPES_UCHAR cot, TMWDEFS_GROUP_MASK groupMask, 
  TMWTYPES_ULONG ioa, TMWTYPES_USHORT *pPointIndex);

typedef struct S14GroupEntry {
  S14DBAS_GROUP_READ_FUNC pReadFunc;
} S14DBAS_GROUP_ENTRY;

static S14DBAS_GROUP_ENTRY _groupTable[] = {
#if S14DATA_SUPPORT_MSP
  {s14msp_readIntoResponse},
#endif
#if S14DATA_SUPPORT_MDP
  {s14mdp_readIntoResponse},
#endif
#if S14DATA_SUPPORT_MST
  {s14mst_readIntoResponse},
#endif
#if S14DATA_SUPPORT_MBO
  {s14mbo_readIntoResponse},
#endif
#if S14DATA_SUPPORT_MME_A
  {s14mmena_readIntoResponse},
#endif
#if S14DATA_SUPPORT_MME_B
  {s14mmenb_readIntoResponse},
#endif
#if S14DATA_SUPPORT_MME_C
  {s14mmenc_readIntoResponse},
#endif
#if S14DATA_SUPPORT_MPS
  {s14mpsna_readIntoResponse},
#endif
#if S14DATA_SUPPORT_MMEND
  {s14mmend_readIntoResponse},
#endif
#if S14DATA_SUPPORT_PMENA
  {s14pmena_readIntoResponse},
#endif
#if S14DATA_SUPPORT_PMENB
  {s14pmenb_readIntoResponse},
#endif
#if S14DATA_SUPPORT_PMENC
  {s14pmenc_readIntoResponse},
#endif
#if S14DATA_SUPPORT_MITC
  /* Only in response to CRDNA */
  {s14mitc_readIntoResponse},
#endif
#if S14DATA_SUPPORT_MCTNA
  /* Only in response to CRDNA */
  {s14mctna_readIntoResponse},
#endif
  {TMWDEFS_NULL}
};

 
static S14DBAS_GROUP_ENTRY _counterTable[] = {
#if S14DATA_SUPPORT_MIT
  {s14mit_readIntoResponse},
#endif
#if S14DATA_SUPPORT_MITC
  {s14mitc_readIntoCCINAResponse},
#endif
  {TMWDEFS_NULL}
};

 
/* function: _readGroup */
static TMWTYPES_BOOL TMWDEFS_GLOBAL _readGroup(
  TMWSESN_TX_DATA *pTxData, 
  TMWSCTR *pSector, 
  S14DBAS_GROUP_ENTRY groupTable[], 
  TMWTYPES_ULONG ioa, 
  TMWTYPES_UCHAR cot, 
  TMWDEFS_GROUP_MASK groupMask,  
  TMWTYPES_UCHAR *pGroupIndex, 
  TMWTYPES_USHORT *pPointIndex)
{
  TMWTYPES_BOOL pointsRead = TMWDEFS_FALSE;
  TMWTYPES_UCHAR groupIndex = *pGroupIndex;

  /* See if there is any data to return */
  while((groupIndex != GROUP_INDEX_IDLE)
     && (groupTable[groupIndex].pReadFunc != TMWDEFS_NULL))
  {
    /* Call function to read points into response */
    S14DBAS_GROUP_STATUS status = 
      groupTable[groupIndex].pReadFunc(pTxData, pSector, cot, groupMask, ioa, pPointIndex);

    /* If no more data in this group, increment group index */
    if(status != S14DBAS_GROUP_STATUS_MORE_DATA)
    {
      groupIndex += 1;
      *pPointIndex = 0;

      /* If we are done reading all groups */
      if(groupTable[groupIndex].pReadFunc == TMWDEFS_NULL) 
      {
        /* If the cylic timer expired while in the process of sending cyclic data,
         * the cyclicPending flag will be set, if it was then start back at the beginning
         */
        I870SCTR *pI870Sector = (I870SCTR *)pSector; 
        if((cot == I14DEF_COT_CYCLIC)
          && (pI870Sector->cyclicPending))
        {
          pI870Sector->cyclicPending = TMWDEFS_FALSE;
          groupIndex = 0;
        }
        else
          /* Finished, set group index to idle */
          groupIndex = GROUP_INDEX_IDLE;  
      }
    }

    /* If data points were added to the message from this group,
     * send message and break out of the loop.
     */
    if(status != S14DBAS_GROUP_STATUS_NO_DATA)
    {
      pointsRead = TMWDEFS_TRUE;
      break;
    }
  }

  /* Update original group index before returning */
  *pGroupIndex = groupIndex;
  return(pointsRead);
}

/* function: s14dbas_readPoint */
/* CRDNA */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14dbas_readPoint(
  TMWSESN_TX_DATA *pTxData,
  TMWSCTR *pSector, 
  TMWTYPES_ULONG ioa)
{
  TMWTYPES_USHORT pointIndex = 0;
  TMWTYPES_UCHAR groupIndex = 0;

  return(_readGroup(pTxData, pSector, 
    _groupTable, ioa, I14DEF_COT_REQUEST, 
    TMWDEFS_GROUP_MASK_ANY, &groupIndex, &pointIndex));
}
    
/* function: s14dbas_readGroup */
/* CICNA, Background or Cyclic */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14dbas_readGroup(
  TMWSESN_TX_DATA *pTxData,
  TMWSCTR *pSector, 
  TMWTYPES_UCHAR cot, 
  TMWDEFS_GROUP_MASK groupMask,
  TMWTYPES_UCHAR *pGroupIndex, 
  TMWTYPES_USHORT *pPointIndex)
{
  return(_readGroup(pTxData, pSector, 
    _groupTable, 0, cot, groupMask,
    pGroupIndex, pPointIndex));
}

/* function: s14dbas_readCounterGroup */
/* CCINA */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14dbas_readCounterGroup(
  TMWSESN_TX_DATA *pTxData,
  TMWSCTR *pSector, 
  TMWTYPES_UCHAR cot, 
  TMWDEFS_GROUP_MASK groupMask,
  TMWTYPES_UCHAR *pGroupIndex, 
  TMWTYPES_USHORT *pPointIndex)
{
  return(_readGroup(pTxData, pSector, 
    _counterTable, 0, cot, groupMask,
    pGroupIndex, pPointIndex));
}
