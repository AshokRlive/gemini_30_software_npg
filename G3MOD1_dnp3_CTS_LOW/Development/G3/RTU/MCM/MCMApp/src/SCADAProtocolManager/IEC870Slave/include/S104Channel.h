/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:S104Channel.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief Protocol Physical Channel
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   30 Sep 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef IEC104SLAVEPROTOCOLCHANNEL_H_
#define IEC104SLAVEPROTOCOLCHANNEL_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "AbstractSlaveProtocolChannel.h"
#include "S104TMWIncludes.h"
#include "GeminiDatabase.h"
#include "S104Session.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
class S104Protocol; // Forward declaration


class S104Channel : public AbstractSlaveProtocolChannel
{
public:
    struct RedundancyConfig
    {
        TMWTYPES_CHAR   ipAddress[32];
        TMWTYPES_USHORT ipPort;
    };

    struct S104ChannelConfig
    {
        I870CHNL_CONFIG    pChnlConfig;
        I870LNK4_CONFIG    pLinkConfig;
        TMWPHYS_CONFIG     pPhysConfig;
        LINIO_CONFIG       pIOConfig  ;
        TMWTARG_CONFIG     pTMWTargConfig;
        RedundancyConfig** rdcyConfig;
        lu_uint16_t        rdcySize;
    };

public:
    S104Channel(GeminiDatabase& g3database, S104Protocol* protocol);

    virtual ~S104Channel();

    SCADAP_ERROR openChannel(TMWAPPL* tmwappl);

    void closeChannel();

    void configure(const S104ChannelConfig& channelConfig);

    void addSession(S104Session* sessionPtr);


    /**Override*/
    virtual bool         connectChannel()
    {
        //TODO to be implemented
        return false;
    }

    /**Override*/
    virtual lu_uint32_t  getConnectionTimeout()
    {
        //TODO to be implemented
        return 0;
    }

    GeminiDatabase&     getG3Database(){return m_g3db;}

    const S104Protocol* const getProtocol(){return mp_protocol;}

    bool isRedundancyEnabled ()
    {
        return m_rdcyEnabled;
    }

private:
    TMWCHNL* openGroupChannel(TMWAPPL* tmwappl);


    /**Override*/
    virtual void         tickEvent()
    {
        //TODO to be implemented
    };

    /**Override*/
    virtual lu_uint32_t  countSessionEvents(lu_uint8_t classMask, bool countAll)
    {
        //TODO to be implemented
        return 0;
    };


private:
    typedef std::vector<S104Session*> S104SessionVect;
    typedef std::vector<TMWCHNL*>     RdcyChnlVect;

    const S104Protocol* const   mp_protocol;
    GeminiDatabase&     m_g3db;
    TMWCHNL*            mp_tmwchnl;
    S104ChannelConfig*  mp_tmwchnlCnfg;
    S104SessionVect     m_sessions;
    lu_bool_t           m_rdcyEnabled;
    RdcyChnlVect        m_rdcychnls;

};



#endif /* IEC104SLAVEPROTOCOLCHANNEL_H_ */

/*
 *********************** End of file ******************************************
 */
