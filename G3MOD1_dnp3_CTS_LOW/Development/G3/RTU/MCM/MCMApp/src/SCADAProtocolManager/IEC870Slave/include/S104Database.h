/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:S104Database.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief Protocol Point database
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   8 Oct 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef S104DATABASE_H_
#define S104DATABASE_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <vector>
#include <map>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "S104TMWIncludes.h"
#include "S104InputPoint.h"
#include "S104OutputPoint.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define USE_DUMMY_104_CONFIG 0
/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
class S104Sector;   // Forward declaration

/**
 * \brief Database structure used by the IEC104 slave protocol.
 */
class S104Database
{

public:
    S104Database(S104Sector& sector);
    virtual ~S104Database();

    void addInputPoint(S104InputPoint::ProtPointType,S104InputPoint* pInputPoint);

    void addOutputPoint(S104OutputPoint::ProtPointType,S104OutputPoint* pOutputPoint);

    S104InputPoint* getInputPoint(S104InputPoint::ProtPointType type,TMWTYPES_USHORT index);

    I870OutputStr* getOutputPointStr(S104OutputPoint::ProtPointType type,TMWTYPES_ULONG ioa);

    S104Sector& getSector(){return m_sector;}

    SCADAP_ERROR getSectorHandle(TMWSCTR** ppSectorHdle);

    /********************************************/
    /* TMW Callback Function Interface          */
    /********************************************/

    static void* mspGetPoint(void *pS104DB, TMWTYPES_USHORT index);

    static void* mdpGetPoint(void *pS104DB, TMWTYPES_USHORT index);

    static void* mmenaGetPoint(void *pS104DB, TMWTYPES_USHORT index);

    static void* mmenbGetPoint(void *pS104DB, TMWTYPES_USHORT index);

    static void* mmencGetPoint(void *pS104DB, TMWTYPES_USHORT index);

    static void* mitGetPoint(void *pS104DB, TMWTYPES_USHORT index);

    static void* cscLookupPoint(void *pS104DB, TMWTYPES_ULONG ioa);

    static void* cdcLookupPoint(void *pS104DB, TMWTYPES_ULONG ioa);

    static TMWTYPES_BOOL ccsnaSetTime(TMWDTIME *pNewTime);

    static void ccsnaGetTime(TMWDTIME *pNewTime);

    static TMWTYPES_BOOL mitFreeze(void *pPoint, TMWTYPES_UCHAR qcc);

    static TMWTYPES_BOOL hasSectorReset(void *pS104DB, TMWTYPES_UCHAR *pResetCOI);

    static TMWTYPES_BOOL crpnaExecuteRTUReset(TMWTYPES_UCHAR qrp);

//
//    static TMWDEFS_COMMAND_STATUS cscSelect(void *pPoint, TMWTYPES_UCHAR cot,
//                    TMWTYPES_UCHAR sco);
//
//    static TMWDEFS_COMMAND_STATUS cscExecute(void *pPoint, TMWTYPES_UCHAR cot,
//                    TMWTYPES_UCHAR sco);

//    static TMWDEFS_COMMAND_STATUS cdcSelect(void *pPoint, TMWTYPES_UCHAR cot,
//                    TMWTYPES_UCHAR dco);
//
//    static TMWDEFS_COMMAND_STATUS cdcExecute(void *pPoint, TMWTYPES_UCHAR cot,
//                    TMWTYPES_UCHAR dco);

    static lu_bool_t deleteEventID(lu_uint32_t eventID, void *pDB);

private:
    #if USE_DUMMY_104_CONFIG
        const lu_uint8_t NoOfMSP ;
        const lu_uint8_t NoOfMDP ;
        const lu_uint8_t NoOfMMENA;
        const lu_uint8_t NoOfMMENB;
        const lu_uint8_t NoOfMMENC;
        const lu_uint8_t NoOfMIT ;
        const lu_uint8_t NoOfSCO;
        const lu_uint8_t NoOfDCO;
    #endif

    /*Vectors of S104InputPoint*/
    typedef std::vector<S104InputPoint*> S104MSPVect;
    typedef std::vector<S104InputPoint*> S104MDPVect;
    typedef std::vector<S104InputPoint*> S104MMENAVect;
    typedef std::vector<S104InputPoint*> S104MMENBVect;
    typedef std::vector<S104InputPoint*> S104MMENCVect;
    typedef std::vector<S104InputPoint*> S104MITVect;

    /*Maps of output point structures against IOA*/
    typedef std::map<TMWTYPES_ULONG, I870OutputStr> S104SCOMap;
    typedef std::map<TMWTYPES_ULONG, I870OutputStr> S104DCOMap;

    S104Sector&         m_sector;

    S104MSPVect         m_MSPVect;
    S104MDPVect         m_MDPVect;
    S104MMENAVect       m_MMENAVect;
    S104MMENBVect       m_MMENBVect;
    S104MMENCVect       m_MMENCVect;
    S104MITVect         m_MITVect;
    S104SCOMap          m_SCOMap;
    S104DCOMap          m_DCOMap;

};


#endif /* S104DATABASE_H_ */

/*
 *********************** End of file ******************************************
 */
