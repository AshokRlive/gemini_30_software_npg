/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14meptc.h
 * description: IEC 60870-5-101/104 slave MEPTC/MEPTD support
 *
 * This file supports ASDU types 
 *  19/40 Packed output curcuit information of protection equipment 
 *        with 24/56 bit time tag 
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14meptc.h"
#include "tmwscl/i870/s14mem.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14evnt.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/i870util.h"
#include "tmwscl/utils/tmwtarg.h"

#if S14DATA_SUPPORT_MEPTC

/* Forward declaration */
static void TMWDEFS_LOCAL _initEventDesc(
  TMWSCTR *pSector,
  S14EVNT_DESC *pDesc);

/* function: _internalAddEvent
 * purpose: Internal function called by SCL to add an event to the queue.
 *  This will not tell the link layer to ask the application for a message
 *  to send. 
 * arguments:
 *  pSector - pointer to sector structure returned by s14sctr_openSector
 *  cot - cause of transmission
 *  ioa - Information object address
 *  oci -
 *  qdp -
 *  relayOperatingTime -
 *  pTimeStamp - pointer to time structures
 * returns:
 *  void *
 */
static void * TMWDEFS_LOCAL _internalAddEvent(
  TMWSCTR *pSector, 
  TMWTYPES_ULONG ioa, 
  TMWTYPES_UCHAR oci,
  TMWTYPES_UCHAR qdp,
  TMWTYPES_USHORT relayOperatingTime,
  TMWDTIME *pTimeStamp)
{
  S14EVNT_DESC desc;
  S14MEPTC_EVENT *pEvent;

  _initEventDesc(pSector, &desc);

  pEvent = (S14MEPTC_EVENT *)s14evnt_addEvent(pSector, I14DEF_COT_SPONTANEOUS, 
    ioa, qdp, pTimeStamp, &desc, TMWDEFS_NULL);

  if(pEvent != TMWDEFS_NULL)
  {
    pEvent->oci = oci;
    pEvent->relayOperatingTime = relayOperatingTime;
  }

  return(pEvent);
}

/* function: _changedFunc 
 * purpose: Determine if the specified point has changed and if so
 *  get the current values and add an event to the queue
 * arguments:
 *  pSector - identifies sector
 * returns:
 *  TMWDEFS_TRUE if an event was added
 *  TMWDEFS_FALSE otherwise
 */
static TMWTYPES_BOOL TMWDEFS_CALLBACK _changedFunc(
  TMWSCTR *pSector,
  void *pPoint, 
  TMWDTIME *pTimeStamp)
{  
  TMWTYPES_USHORT relayOperating;
  TMWTYPES_UCHAR oci;
  TMWTYPES_UCHAR qdp;

  if(s14data_meptcChanged(pPoint, &oci, &qdp, &relayOperating))
  { 
    if(_internalAddEvent(pSector, 
      s14data_meptcGetInfoObjAddr(pPoint), oci, qdp, relayOperating,
        pTimeStamp) != TMWDEFS_NULL)
    {
      return(TMWDEFS_TRUE);
    }
  }
  return(TMWDEFS_FALSE);
}

/* function: _eventData 
 * purpose: Insert data from event structure into message to be sent
 * arguments:
 *  pTxData - pointer to transmit data structure
 *  pEvent - pointer to event to be put into message
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _eventData(
  TMWSESN_TX_DATA *pTxData, 
  S14EVNT *pEvent)
{
  S14MEPTC_EVENT *pMEPTCEvent = (S14MEPTC_EVENT *)pEvent;

  /* Store OCI */
  pTxData->pMsgBuf[pTxData->msgLength++] = pMEPTCEvent->oci;

  /* Store QDP */
  pTxData->pMsgBuf[pTxData->msgLength++] = pEvent->quality;

  /* Store elapsed Time */
  tmwtarg_store16(&pMEPTCEvent->relayOperatingTime, &pTxData->pMsgBuf[pTxData->msgLength]);
  pTxData->msgLength += 2;
}

/* function: _initEventDesc 
 * purpose: Initialize event descriptor
 * arguments:
 *  pSector - identifies sector
 *  pDesc - pointer to descriptor structure 
 * returns:
 *  void
 */
static void TMWDEFS_LOCAL _initEventDesc(
  TMWSCTR *pSector,
  S14EVNT_DESC *pDesc)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  pDesc->typeId = I14DEF_TYPE_MEPTC1;
  pDesc->cotSpecified = 0;
  pDesc->eventMemType = S14MEM_MEPTC_EVENT_TYPE;
  pDesc->pEventList = &p14Sector->meptcEvents;
  pDesc->eventMode = p14Sector->meptcEventMode;
  pDesc->scanEnabled = p14Sector->meptcScanEnabled;
  pDesc->maxEvents = p14Sector->meptcMaxEvents;
  pDesc->timeFormat = p14Sector->meptcTimeFormat;
  pDesc->doubleTransmission = TMWDEFS_FALSE;
  pDesc->pEventsOverflowedFlag = &p14Sector->meptcEventsOverflowed;
  pDesc->pChangedFunc = _changedFunc;
  pDesc->pGetPointFunc = s14data_meptcGetPoint;
  pDesc->pEventDataFunc = _eventData;
}

/* function: s14meptc_init */
void TMWDEFS_GLOBAL s14meptc_init(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  tmwdlist_initialize(&p14Sector->meptcEvents);
  p14Sector->meptcEventsOverflowed = TMWDEFS_FALSE;
}

/* function: s14meptc_close */
void TMWDEFS_GLOBAL s14meptc_close(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  tmwdlist_destroy(&p14Sector->meptcEvents, s14mem_free);
} 

/* function: s14meptc_addEvent */
void * TMWDEFS_GLOBAL s14meptc_addEvent(
  TMWSCTR *pSector, 
  TMWTYPES_ULONG ioa, 
  TMWTYPES_UCHAR oci,
  TMWTYPES_UCHAR qdp,
  TMWTYPES_USHORT relayOperatingTime,
  TMWDTIME *pTimeStamp)
{
  void *pEvent;

  /* This requires a time stamp */
  if(pTimeStamp == TMWDEFS_NULL)
    return(TMWDEFS_NULL);

  TMWTARG_LOCK_SECTION(&pSector->pSession->pChannel->lock);

  pEvent = _internalAddEvent(pSector,
    ioa, oci, qdp, relayOperatingTime, pTimeStamp);

  if(pEvent != TMWDEFS_NULL)
  {
    /* If an event was added tell link layer we have data */ 
    s14event_linkDataReady(pSector);
  }

  TMWTARG_UNLOCK_SECTION(&pSector->pSession->pChannel->lock);
  return(pEvent);
}

/* function: s14meptc_countEvents */
TMWTYPES_USHORT TMWDEFS_GLOBAL s14meptc_countEvents(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  return(s14evnt_countEvents(pSector, &p14Sector->meptcEvents));
}

/* function: s14meptc_scanForChanges */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14meptc_scanForChanges(
  TMWSCTR *pSector)
{
  S14EVNT_DESC desc;

  _initEventDesc(pSector, &desc);

  return(s14evnt_scanForChanges(pSector, &desc));
}

/* function: s14meptc_processEvents */
S14EVNT_STATUS TMWDEFS_GLOBAL s14meptc_processEvents(
  TMWSCTR *pSector, 
  TMWDTIME *pEventTime)
{
  S14EVNT_DESC desc;
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  TMWTYPES_UCHAR typeId = I14DEF_TYPE_MEPTC1;
  if(p14Sector->meptcTimeFormat == TMWDEFS_TIME_FORMAT_24) typeId = I14DEF_TYPE_MEPTC1;
  if(p14Sector->meptcTimeFormat == TMWDEFS_TIME_FORMAT_56) typeId = I14DEF_TYPE_MEPTF1;

  _initEventDesc(pSector, &desc);
  desc.typeId = typeId;
  return(s14evnt_processEvents(pSector, &desc, 4, pEventTime, TMWDEFS_FALSE));
}

#endif /* S14DATA_SUPPORT_MEPTC */
