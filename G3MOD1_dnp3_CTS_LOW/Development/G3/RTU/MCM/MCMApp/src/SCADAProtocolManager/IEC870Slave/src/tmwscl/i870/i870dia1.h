/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i870dia1.h
 * description: This file is intended for internal SCL use only.
 *  IEC 60870-5 diagnostics
 */
#ifndef I870DIA1_DEFINED
#define I870DIA1_DEFINED

#include "tmwscl/utils/tmwcnfg.h"
#include "tmwscl/i870/i870lnk1.h"

/* Define error numbers used by master and slave ft1.2 101/103 */
typedef enum {
  I870DIA1_BALANCED_NA,
  I870DIA1_RCVD_INV_DIR,
  I870DIA1_ALLOC_SESN,
  I870DIA1_INV_FCB,
  I870DIA1_TEST_BALANCED,
  I870DIA1_ACD_BALANCED,
  I870DIA1_USER_DATA_BALANCED,
  I870DIA1_PRIMARY_FC,
  I870DIA1_REPLY_WRONG_SESN,
  I870DIA1_RCVD_INV_SECONDARY,
  I870DIA1_USER_DATA,
  I870DIA1_RESPOND_NO_DATA,
  I870DIA1_UNSUPPORTED_FC,
  I870DIA1_FC_NOT_USED,
  I870DIA1_UNRECOGNIZED_FC,
  I870DIA1_INVALID_LENGTH,
  I870DIA1_INVALID_CHKS,
  I870DIA1_NOT_RESET,
  I870DIA1_WRONG_REPLY,
  I870DIA1_RCV_LENGTH,

  /* This must be last entry */
  I870DIA1_ERROR_ENUM_MAX

} I870DIA1_ERROR_ENUM; 

#if !TMWCNFG_SUPPORT_DIAG

#define I870DIA1_LINK_FRAME_SENT(pSession, pBuf, numBytes, retry) \
  TMWTARG_UNUSED_PARAM(pSession); TMWTARG_UNUSED_PARAM(pBuf); TMWTARG_UNUSED_PARAM(numBytes); TMWTARG_UNUSED_PARAM(retry);

#define I870DIA1_LINK_FRAME_RECEIVED(pSession, pBuf, numBytes) \
  TMWTARG_UNUSED_PARAM(pSession); TMWTARG_UNUSED_PARAM(pBuf); TMWTARG_UNUSED_PARAM(numBytes);
 
#define I870DIA1_LINK_ADDRESS_UNKNOWN(pChannel, rxAddress) \
  TMWTARG_UNUSED_PARAM(pChannel); TMWTARG_UNUSED_PARAM(rxAddress);
    
#define I870DIA1_ERROR(pChannel, pSession, errorNumber) \
  TMWTARG_UNUSED_PARAM(pChannel); TMWTARG_UNUSED_PARAM(pSession); TMWTARG_UNUSED_PARAM(errorNumber);

#else

#define I870DIA1_LINK_FRAME_SENT(pSession, pBuf, numBytes, retry) \
  i870dia1_linkFrameSent(pSession, pBuf, numBytes, retry)

#define I870DIA1_LINK_FRAME_RECEIVED(pSession, pBuf, numBytes) \
  i870dia1_linkFrameReceived(pSession, pBuf, numBytes)

#define I870DIA1_LINK_ADDRESS_UNKNOWN(pChannel, rxAddress) \
  i870dia1_linkAddressUnknown(pChannel, rxAddress);

#define I870DIA1_ERROR(pChannel, pSession, errorNumber) \
  i870dia1_error(pChannel, pSession, errorNumber)

#ifdef __cplusplus
extern "C" {
#endif

  /* routine: i870dia1_init
   * purpose: internal diagnostic init function
   * arguments:
   *  void
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL i870dia1_init(void);
  
  /* routine: i870dia1_validateErrorTable
   * purpose: Called only to verify if error message table is correct.
   *  This is intended for test purposes only.
   * arguments:
   *  void
   * returns:
   *  TMWDEFS_TRUE if formatted correctly
   *  TMWDEFS_FALSE if there is an error in the table.
   */
  TMWTYPES_BOOL i870dia1_validateErrorTable(void);

  /* function: i870dia1_linkFrameSent
   * purpose: display link level frame
   * arguments:
   *  pSession - pointer to session
   *  pBuf - pointer to message
   *  numBytes - number of bytes in message
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL i870dia1_linkFrameSent(
    TMWSESN *pSession,
    const TMWTYPES_UCHAR *pBuf, 
    TMWTYPES_USHORT numBytes,
    TMWTYPES_USHORT retry);

  /* function: i870dia1_linkFrameReceived
   * purpose: display link level frame
   * arguments:
   *  pSession - pointer to session
   *  pBuf - pointer to message
   *  numBytes - number of bytes in message
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL i870dia1_linkFrameReceived(
    TMWSESN *pSession,
    const TMWTYPES_UCHAR *pBuf, 
    TMWTYPES_USHORT numBytes);
    
  /* function: i870dia1_linkFrameReceived
   * purpose: Display a message indicating received link address was not for
   *  a currently configured session
   * arguments:
   *  pChannel - pointer to channel
   *  rxAddress - received address 
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL i870dia1_linkAddressUnknown(
    TMWCHNL *pChannel,
    TMWTYPES_USHORT rxAddress);

  /* function: i870dia1_error
   * purpose: Display error message
   * arguments:
   *  pChannel - pointer from which this message originated
   *  pSession - session from which this message originated
   *  errorNumber - enum indicating what error message to display
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL i870dia1_error(
    TMWCHNL *pChannel,
    TMWSESN *pSession,  
    I870DIA1_ERROR_ENUM errorNumber);

  /* function: i870dia1_errorMsgEnable
   * purpose: Enable/Disable specific error message output
   * arguments:
   *  errorNumber - enum indicating what error message
   *  enabled - TMWDEFS_TRUE if error message should be enabled
   *            TMWDEFS_FALSE if error message should be disabled
   * returns:
   *  void
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL i870dia1_errorMsgEnable(
    I870DIA1_ERROR_ENUM errorNumber,
    TMWTYPES_BOOL enabled);

#ifdef __cplusplus
}
#endif
#endif /* TMWCNFG_SUPPORT_DIAG */
#endif /* I870DIA1_DEFINED */
