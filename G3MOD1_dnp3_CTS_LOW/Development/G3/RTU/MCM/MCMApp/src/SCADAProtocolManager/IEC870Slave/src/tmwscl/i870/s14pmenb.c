/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14pmenb.c
 * description: IEC 60870-5-101/104 slave PMENB (Parameter of Measured Value,
 *  Scaled Value) functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14pmenb.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/i870chnl.h"
#include "tmwscl/utils/tmwtarg.h"

#if S14DATA_SUPPORT_PMENB

/* function: s14pmenb_processRequest */
void TMWDEFS_CALLBACK s14pmenb_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  void *pPoint;

  /* Store originator address */
  p14Sector->pmenbOriginator = pMsg->origAddress;
 
  /* Parse Information Object Address */
  i870util_readInfoObjAddr(pSector->pSession, pMsg, &p14Sector->pmenbIOA);

  /* Parse SVA */
  tmwtarg_get16(&pMsg->pRxData->pMsgBuf[pMsg->offset], (TMWTYPES_USHORT *)&p14Sector->pmenbSVA);
  pMsg->offset += 2;

  /* Parse QPM */
  p14Sector->pmenbQPM = pMsg->pRxData->pMsgBuf[pMsg->offset++];

  /* Parse received cause of transmission */
  if(pMsg->cot == I14DEF_COT_ACTIVATION)
  {
    p14Sector->pmenbCOT = I14DEF_COT_ACTCON;
  }
  else
  {
    p14Sector->pmenbCOT = I14DEF_COT_UNKNOWN_COT | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* Get database point */
  pPoint = s14data_pmenbLookupPoint(p14Sector->i870.pDbHandle, p14Sector->pmenbIOA);
  if(pPoint == TMWDEFS_NULL)
  {
    p14Sector->pmenbCOT = I14DEF_COT_UNKNOWN_IOA | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* Store New Value */
  if(!s14data_pmenbStore(pPoint, &p14Sector->pmenbSVA, &p14Sector->pmenbQPM))
    p14Sector->pmenbCOT |= I14DEF_COT_NEGATIVE_CONFIRM;
}

/* function: s14pmenb_buildResponse */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14pmenb_buildResponse( 
  TMWSCTR *pSector,
  TMWTYPES_BOOL buildResponse)
{
  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  if(p14Sector->pmenbCOT != 0)
  {
    if(buildResponse)
    {
      /* Activation confirmation or Deactivation */
      TMWSESN_TX_DATA *pTxData;
      S14SESN *pS14Session = (S14SESN *)pSector->pSession;

      if((pTxData = i870chnl_newTxData(pSector->pSession->pChannel, 
        pSector->pSession, pSector, pS14Session->maxASDUSize)) == TMWDEFS_NULL)
      {
        return(TMWDEFS_FALSE);
      }

#if TMWCNFG_SUPPORT_DIAG
      pTxData->pMsgDescription = "Parameter of Measured Value, Scaled Value Response";
#endif
      pTxData->txFlags = TMWSESN_TXFLAGS_NO_RESPONSE;
      pTxData->responseTimeout = p14Sector->defaultResponseTimeout;

      /* Build response */
      i870util_buildMessageHeader(pSector->pSession, 
        pTxData, I14DEF_TYPE_PMENB1, p14Sector->pmenbCOT, 
        p14Sector->pmenbOriginator, p14Sector->i870.asduAddress);

      /* Store Information Object Address */
      i870util_storeInfoObjAddr(pSector->pSession, pTxData, p14Sector->pmenbIOA);

      /* Store SVA */
      tmwtarg_store16((TMWTYPES_USHORT *)&p14Sector->pmenbSVA, &pTxData->pMsgBuf[pTxData->msgLength]);
      pTxData->msgLength += 2;

      /* Store Qualifier of Parameter of Measured Value */
      pTxData->pMsgBuf[pTxData->msgLength++] = p14Sector->pmenbQPM;

      /* Request is complete */
      p14Sector->pmenbCOT = 0;

      /* Send the response */
      i870chnl_sendMessage(pTxData);
    }

    return(TMWDEFS_TRUE);
  }

  return(TMWDEFS_FALSE);
}

/* function: _storeInResponse */
static void TMWDEFS_CALLBACK _storeInResponse(
  TMWSESN_TX_DATA *pTxData, 
  void *pPoint,
  TMWDEFS_TIME_FORMAT timeFormat)
{
  TMWTYPES_SHORT value = s14data_pmenbGetValue(pPoint);
  TMWTARG_UNUSED_PARAM(timeFormat);

  tmwtarg_store16((TMWTYPES_USHORT *)&value, &pTxData->pMsgBuf[pTxData->msgLength]);
  pTxData->msgLength += 2;


  pTxData->pMsgBuf[pTxData->msgLength++] = s14data_pmenbGetQualifier(pPoint);
}

/* function: s14pmenb_readIntoResponse */
S14DBAS_GROUP_STATUS TMWDEFS_CALLBACK s14pmenb_readIntoResponse(
  TMWSESN_TX_DATA *pTxData, 
  TMWSCTR *pSector, 
  TMWTYPES_UCHAR cot, 
  TMWDEFS_GROUP_MASK groupMask, 
  TMWTYPES_ULONG ioa,
  TMWTYPES_USHORT *pPointIndex)
{
  return(s14util_readIntoResponse(pTxData, pSector, cot, 
    groupMask, ioa, I14DEF_TYPE_PMENB1, TMWDEFS_TIME_FORMAT_NONE, 3, pPointIndex, s14data_pmenbGetPoint, 
    s14data_pmenbGetGroupMask, s14data_pmenbGetInfoObjAddr, s14util_alwaysUseIndexed, s14data_getTimeFormat, 
    _storeInResponse));
}

#endif /* S14DATA_SUPPORT_PMENB */
