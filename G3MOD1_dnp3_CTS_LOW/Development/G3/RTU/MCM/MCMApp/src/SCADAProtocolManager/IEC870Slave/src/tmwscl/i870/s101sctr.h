/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s101sctr.h
 * description: IEC 60870-5-101 Slave sector
 */
#ifndef S101SCTR_DEFINED
#define S101SCTR_DEFINED

#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/i870/i870sctr.h"
#include "tmwscl/i870/s14data.h"

/* S101 Sector Configuration Info */
typedef struct S101SectorConfigStruct {

  /* Specify the sector address */
  TMWTYPES_USHORT asduAddress;

  /* Specify the period, in milliseconds, at which to generate cyclic data
   * on this sector. 0 means no cyclic data will be sent.
   */
  TMWTYPES_MILLISECONDS cyclicPeriod;

  /* Specify the period, in milliseconds, at which to generate the FIRST cyclic
   * data response on this sector. The default small value will cause the first cyclic 
   * data to be sent shortly after connecting instead of waiting cyclicPeriod.
   * 
   * If S14DATA_SUPPORT_CICNAWAIT is defined and cyclicWaitCICNAComplete is set to 
   * TMWDEFS_TRUE, to delay cyclic data from being sent until a CICNA completes, 
   * setting a larger cyclicFirstPeriod can be used to force cyclic data to be sent 
   * even if no CICNA request is received after that period of time.
   *  
   * A zero value for cyclicFirstPeriod, with a nonzero for cyclicPeriod, will result 
   * in this first cyclic timer period expiring immediately.
   */
  TMWTYPES_MILLISECONDS cyclicFirstPeriod;
 
  /* Specify the period, in milliseconds, at which to generate background scan
   * data on this sector. 0 means no background data will be sent.
   */
  TMWTYPES_MILLISECONDS backgroundPeriod;

  /* Specify the period, in milliseconds, at which to scan for events on
   * this sector
   */
  TMWTYPES_MILLISECONDS rbeScanPeriod;

  /* Specify the period, in milliseconds, after which a previously received
   * select will timeout. An execute command must be received before this
   * timeout to be considered valid.
   */
  TMWTYPES_MILLISECONDS selectTimeout;

  /* To prevent responses from staying queued for long periods of time and
   * no longer being relevant, for example if the master is turned off before
   * a response can be acked and then the master is restarted later, this 
   * timeout value will be used to delete old responses.
   */
  TMWTYPES_MILLISECONDS defaultResponseTimeout;
  
#if S14DATA_SUPPORT_MULTICMDS 
  /* Number of simultaneous commands per ASDU type supported by this sector */
  TMWTYPES_UCHAR  commandsPerType;

  /* Total number of simultaneous commands supported by this sector */
  TMWTYPES_USHORT commandsPerSector;
#endif

  /* Specify whether to send ACT TERM upon completion of set point commands.
   * CSENA CSENB CSENC CSETA CSETB CSETC 
   */
  TMWTYPES_BOOL cseUseActTerm;

  /* Specify whether to send ACT TERM upon completion of commands other
   * than set point commands .
   */
  TMWTYPES_BOOL cmdUseActTerm;

  /* Whether or not this sector is in local mode. Sector is NOT normally in local mode.
   * Currently the only behavior changes associated with this mode are as follows.
   * When a command is received an event may be automatically generated for the 
   * monitored point if one is present. 
   * TMWDEFS_FALSE, the default value would send COT 11 I14DEF_COT_RETURN_REMOTE
   * TMWDEFS_TRUE,  this will send COT 12 I14DEF_COT_RETURN_LOCAL instead
   */
  TMWTYPES_BOOL localMode;
  
  /* To limit some optional functionality to interoperate with masters that  
   * do not support this behavior, set the following bit mask appropriately.
   * S14SCTR_STRICT_PMENABC limits responses of these three types to a single point.
   */
  TMWTYPES_ULONG   strictAdherence;

  /* Specify how long the system clock will remain valid after a clock
   * synchronization.  (or after s14sctr_restartClockValidTime is called)
   * If this period expires without a clock synchronization
   * all times will be reported invalid.
   */
  TMWTYPES_MILLISECONDS clockValidPeriod;

  /* This parameter controls whether spontaneous clock synchronization
   * events will be generated on this sector. The user can specify the
   * use of no time, 24 bit time, or 56 bit time when reporting events.
   * The 24 bit time only reports the time up to the hour, hence it is
   * necessary to report the full time once per hour to make sure the
   * master can reproduce the actual time of the event. This is 
   * accomplished using a spontaneous clock synchronization message from
   * the slave to the master. 
   * 
   * If using 24 bit time when reporting events this parameter should 
   * be set to TMWDEFS_TRUE. Unfortunately some master devices do not 
   * support spontaneous clock synchronization events so it might be 
   * necessary to set this to TMWDEFS_FALSE for some sectors.
   */
  TMWTYPES_BOOL sendClockSyncEvents; 
 
  /* If this is TMWDEFS_TRUE the oldest event will be deleted when a 
   * new event is added to an event queue that is full.
   */
  TMWTYPES_BOOL deleteOldestEvent;
  
  /* User registered statistics callback function and parameter */
  TMWSCTR_STAT_CALLBACK           pStatCallback;
  void                           *pStatCallbackParam;

  /* Event configuration 
   *  xxxScanEnabled - enable scanning for events from this data type
   *  xxxMaxEvents - maximum number of events that may be queued simultaneously
   *    for this data type
   *  xxxEventMode - specifies whether multiple events will be queued for
   *    a specific data point or only the most recent
   *  xxxTimeFormat - specify what time format to use for events of this
   *    type. Options are none, 24, or 56 bit. When the event is added if
   *    pTimeStamp == TMWDEFS_NULL, that particular event will be sent using
   *    the ASDU with no time if allowed for that type.
   *  xxxTransmissionMode - specifies Double Transmission (if compiled in).
   *    S14DATA_TRANSMISSION_SINGLE Sends single spontaneous msg per addEvent()
   *    S14DATA_TRANSMISSION_DOUBLE Sends two spontaneous events. One high 
   *     priority without time. Followed by low priority with time.
   *    S14DATA_TRANSMISSION_PERPOINT calls s14data_xxx to determine per point
   */
  TMWTYPES_BOOL mspScanEnabled;
  TMWTYPES_USHORT mspMaxEvents;
  TMWDEFS_EVENT_MODE mspEventMode;
  /* 
   * TMWDEFS_TIME_FORMAT_NONE to send I14DEF_TYPE_MSPNA1 
   * TMWDEFS_TIME_FORMAT_24   to send I14DEF_TYPE_MSPTA1 
   * TMWDEFS_TIME_FORMAT_56   to send I14DEF_TYPE_MSPTB1
   */
  TMWDEFS_TIME_FORMAT mspTimeFormat;

  TMWTYPES_BOOL mdpScanEnabled;
  TMWTYPES_USHORT mdpMaxEvents;
  TMWDEFS_EVENT_MODE mdpEventMode;
  /* 
   * TMWDEFS_TIME_FORMAT_NONE to send I14DEF_TYPE_MDPNA1 
   * TMWDEFS_TIME_FORMAT_24   to send I14DEF_TYPE_MDPTA1 
   * TMWDEFS_TIME_FORMAT_56   to send I14DEF_TYPE_MDPTB1
   */
  TMWDEFS_TIME_FORMAT mdpTimeFormat;

  TMWTYPES_BOOL mstScanEnabled;
  TMWTYPES_USHORT mstMaxEvents;
  TMWDEFS_EVENT_MODE mstEventMode;
  /* 
   * TMWDEFS_TIME_FORMAT_NONE to send I14DEF_TYPE_MSTNA1 
   * TMWDEFS_TIME_FORMAT_24   to send I14DEF_TYPE_MSTTA1 
   * TMWDEFS_TIME_FORMAT_56   to send I14DEF_TYPE_MSTTB1 
   */
  TMWDEFS_TIME_FORMAT mstTimeFormat;

  TMWTYPES_BOOL mboScanEnabled;
  TMWTYPES_USHORT mboMaxEvents;
  TMWDEFS_EVENT_MODE mboEventMode;
  /* 
   * TMWDEFS_TIME_FORMAT_NONE to send I14DEF_TYPE_MBONA1 
   * TMWDEFS_TIME_FORMAT_24   to send I14DEF_TYPE_MBOTA1 
   * TMWDEFS_TIME_FORMAT_56   to send I14DEF_TYPE_MBOTB1 
   */
  TMWDEFS_TIME_FORMAT mboTimeFormat;

  TMWTYPES_BOOL mmenaScanEnabled;
  TMWTYPES_USHORT mmenaMaxEvents;
  TMWDEFS_EVENT_MODE mmenaEventMode;
  /* 
   * TMWDEFS_TIME_FORMAT_NONE to send I14DEF_TYPE_MMENA1 
   * TMWDEFS_TIME_FORMAT_24   to send I14DEF_TYPE_MMETA1 
   * TMWDEFS_TIME_FORMAT_56   to send I14DEF_TYPE_MMETD1 
   */
  TMWDEFS_TIME_FORMAT mmenaTimeFormat;

  TMWTYPES_BOOL mmenbScanEnabled;
  TMWTYPES_USHORT mmenbMaxEvents;
  TMWDEFS_EVENT_MODE mmenbEventMode;
  /* 
   * TMWDEFS_TIME_FORMAT_NONE to send I14DEF_TYPE_MMENB1 
   * TMWDEFS_TIME_FORMAT_24   to send I14DEF_TYPE_MMETB1 
   * TMWDEFS_TIME_FORMAT_56   to send I14DEF_TYPE_MMETE1 
   */
  TMWDEFS_TIME_FORMAT mmenbTimeFormat;

  TMWTYPES_BOOL mmencScanEnabled;
  TMWTYPES_USHORT mmencMaxEvents;
  TMWDEFS_EVENT_MODE mmencEventMode;
  /* 
   * TMWDEFS_TIME_FORMAT_NONE to send I14DEF_TYPE_MMENC1
   * TMWDEFS_TIME_FORMAT_24   to send I14DEF_TYPE_MMETC1 
   * TMWDEFS_TIME_FORMAT_56   to send I14DEF_TYPE_MMETF1
   */
  TMWDEFS_TIME_FORMAT mmencTimeFormat;

  /* no scanning for integrated totals changes */
  TMWTYPES_USHORT mitMaxEvents;
  TMWDEFS_EVENT_MODE mitEventMode;
  /* 
   * TMWDEFS_TIME_FORMAT_NONE to send I14DEF_TYPE_MITNA1
   * TMWDEFS_TIME_FORMAT_24   to send I14DEF_TYPE_MITTA1
   * TMWDEFS_TIME_FORMAT_56   to send I14DEF_TYPE_MITTB1
   */
  TMWDEFS_TIME_FORMAT mitTimeFormat;

#if S14DATA_SUPPORT_MITC
  /* no scanning for integrated BCD totals changes */
  TMWTYPES_USHORT mitcMaxEvents;
  TMWDEFS_EVENT_MODE mitcEventMode;
  /* 
   * TMWDEFS_TIME_FORMAT_NONE to send I14DEF_TYPE_MITNC1
   * TMWDEFS_TIME_FORMAT_24   is not valid
   * TMWDEFS_TIME_FORMAT_56   to send I14DEF_TYPE_MITTC1
   */
  TMWDEFS_TIME_FORMAT mitcTimeFormat;
#endif

  TMWTYPES_BOOL meptaScanEnabled;
  TMWTYPES_USHORT meptaMaxEvents;
  TMWDEFS_EVENT_MODE meptaEventMode;
  /*  
   * TMWDEFS_TIME_FORMAT_NONE is not allowed 
   * TMWDEFS_TIME_FORMAT_24   to send I14DEF_TYPE_MEPTA1
   * TMWDEFS_TIME_FORMAT_56   to send I14DEF_TYPE_MEPTD1
   */
  TMWDEFS_TIME_FORMAT meptaTimeFormat;

  TMWTYPES_BOOL meptbScanEnabled;
  TMWTYPES_USHORT meptbMaxEvents;
  TMWDEFS_EVENT_MODE meptbEventMode;
  /*
   * TMWDEFS_TIME_FORMAT_NONE is not allowed 
   * TMWDEFS_TIME_FORMAT_24   to send I14DEF_TYPE_MEPTB1
   * TMWDEFS_TIME_FORMAT_56   to send I14DEF_TYPE_MEPTE1
   */
  TMWDEFS_TIME_FORMAT meptbTimeFormat;

  TMWTYPES_BOOL meptcScanEnabled;
  TMWTYPES_USHORT meptcMaxEvents;
  TMWDEFS_EVENT_MODE meptcEventMode;
  /* 
   * TMWDEFS_TIME_FORMAT_NONE is not allowed 
   * TMWDEFS_TIME_FORMAT_24   to send 14DEF_TYPE_MEPTC1
   * TMWDEFS_TIME_FORMAT_56   to send 14DEF_TYPE_MEPTF1
   */
  TMWDEFS_TIME_FORMAT meptcTimeFormat;

  TMWTYPES_BOOL mpsnaScanEnabled;
  TMWTYPES_USHORT mpsnaMaxEvents;
  TMWDEFS_EVENT_MODE mpsnaEventMode;

  TMWTYPES_BOOL mmendScanEnabled;
  TMWTYPES_USHORT mmendMaxEvents;
  TMWDEFS_EVENT_MODE mmendEventMode;

#if S14DATA_SUPPORT_DOUBLE_TRANS 
  /* 
   *  xxxTransmissionMode - specifies Double Transmission (if compiled in).
   *    S14DATA_TRANSMISSION_SINGLE Sends single spontaneous msg per addEvent()
   *    S14DATA_TRANSMISSION_DOUBLE Sends two spontaneous events. One high 
   *     priority without time. Followed by low priority with time.
   *    S14DATA_TRANSMISSION_PERPOINT calls s14data_xxx to determine per point
   */
#if  S14DATA_SUPPORT_MSP
  S14DATA_TRANSMISSION_MODE mspTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MDP
  S14DATA_TRANSMISSION_MODE mdpTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MST
  S14DATA_TRANSMISSION_MODE mstTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MBO
  S14DATA_TRANSMISSION_MODE mboTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MIT 
  S14DATA_TRANSMISSION_MODE mitTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MITC 
  S14DATA_TRANSMISSION_MODE mitcTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MME_A 
  S14DATA_TRANSMISSION_MODE mmenaTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MME_B
  S14DATA_TRANSMISSION_MODE mmenbTransmissionMode;
#endif
#if  S14DATA_SUPPORT_MME_C 
  S14DATA_TRANSMISSION_MODE mmencTransmissionMode;
#endif
#endif

  /* File Transfer Directory scan */
  TMWTYPES_BOOL fdrtaScanEnabled;

  /* Time format for responses to read CRDNA are almost always NONE, 
   * but for completeness are allowed to be 24 or 56bit 
   */

  /* Time format for response to read CRDNA for all data types except measurands 
   * This is also used as the time format for responses to CCINA counter requests 
   * Setting this to TMWDEFS_TIME_FORMAT_UNKNOWN will cause a database function
   *  s14data_getTimeFormat() to be called per point to determine what timeFormat
   *  to use for each IOA.
   */
  TMWDEFS_TIME_FORMAT readTimeFormat;
  
  /* Time format for response to read CRDNA for measurands 
   * Setting this to TMWDEFS_TIME_FORMAT_UNKNOWN will cause a database function
   *  s14data_getTimeFormat() to be called per point to determine what timeFormat
   *  to use for each IOA.
   */
  TMWDEFS_TIME_FORMAT readMsrndTimeFormat;

  /* The spec says not to send ASDUs with timeStamps in response to CICNA (GI) 
   * However, some customers require this capability. 
   * Setting this to TMWDEFS_TIME_FORMAT_24 will cause:
   *      Type I14DEF_TYPE_MSPTA1 to be sent instead of I14DEF_TYPE_MSPNA1,
   *      Type I14DEF_TYPE_MDPTA1 to be sent instead of I14DEF_TYPE_MDPNA1,
   *      Type I14DEF_TYPE_MSTTA1 to be sent instead of I14DEF_TYPE_MSTNA1,
   *      Type I14DEF_TYPE_MBOTA1 to be sent instead of I14DEF_TYPE_MBONA1,
   *      Type I14DEF_TYPE_MMETA1 to be sent instead of I14DEF_TYPE_MMENA1,
   *      Type I14DEF_TYPE_MMETB1 to be sent instead of I14DEF_TYPE_MMENB1,
   *  and Type I14DEF_TYPE_MMETC1 to be sent instead of I14DEF_TYPE_MMENC1.
   *
   * Setting this to TMWDEFS_TIME_FORMAT_56 will cause the 56bit types to be sent.
   *
   * Setting this to TMWDEFS_TIME_FORMAT_UNKNOWN will cause a database function
   *  s14data_getTimeFormat() to be called per point to determine what timeFormat
   *  to use for each IOA.
   *
   *  Normally this should be set to TMWDEFS_TIME_FORMAT_NONE
   */
  TMWDEFS_TIME_FORMAT cicnaTimeFormat;

} S101SCTR_CONFIG;

/* DEPRECATED SHOULD USE s101sctr_getSectorConfig and 
 *  s101sctr_setSectorConfig
 */
#define S101SCTR_CONFIG_ASDU_ADDRESS      0x00000001
#define S101SCTR_CONFIG_CYCLIC_PERIOD     0x00000002
#define S101SCTR_CONFIG_BKGRND_PERIOD     0x00000004
#define S101SCTR_CONFIG_RBE_SCAN_PERIOD   0x00000008
#define S101SCTR_CONFIG_SELECT_TIMEOUT    0x00000010
#define S101SCTR_CONFIG_CLOCK_VALID_PRD   0x00000020

/* Include IEC 60870-5-101 slave 'private' sector info */
#include "tmwscl/i870/s101sctp.h"

#ifdef __cplusplus
extern "C" {
#endif

  /* function: s101sctr_initConfig
   * purpose: Initialize 101 slave sector configuration data structure.
   *  This routine should be called to initialize all the members of the
   *  data structure. Then the user should modify the data members they
   *  need in user code. Then pass the resulting structure to 
   *  s101sesn_openSector.
   * arguments:
   *  pConfig - pointer to configuration data structure to initialize
   * returns:
   *  void
   */
  TMWDEFS_SCL_API void TMWDEFS_GLOBAL s101sctr_initConfig(
    S101SCTR_CONFIG *pConfig);

  /* function: s101sctr_openSector 
   * purpose: Open a 101 slave sector
   * arguments:
   *  pSession - session to open sector on
   *  pConfig - 101 slave configuration data structure
   *  pUserHandle - handle passed to session database initialization routine
   * returns:
   *  Pointer to new sector or TMWDEFS_NULL.
   */
  TMWDEFS_SCL_API TMWSCTR * TMWDEFS_GLOBAL s101sctr_openSector(
    TMWSESN *pSession, 
    const S101SCTR_CONFIG *pConfig,
    void *pUserHandle);
 
  /* function: s101sctr_getSectorConfig 
   * purpose: Get current configuration from a currently open sector
   * arguments:
   *  pSector - sector to get configuration from
   *  pConfig - 101 slave configuration data structure to be filled in
   * returns:
   *  TMWDEFS_TRUE if successful
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL s101sctr_getSectorConfig(
    TMWSCTR *pSector,
    S101SCTR_CONFIG *pConfig);

  /* function: s101sctr_setSectorConfig 
   * purpose: Modify a currently open sector
   *  NOTE: normally s101sctr_getSectorConfig() will be called
   *   to get the current config, some values will be changed 
   *   and this function will be called to set the values.
   * arguments:
   *  pSector - sector to modify
   *  pConfig - 101 slave configuration data structure
   * returns:
   *  TMWDEFS_TRUE if successful
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL s101sctr_setSectorConfig(
    TMWSCTR *pSector,
    const S101SCTR_CONFIG *pConfig);

  /* function: s101sctr_modifySector
   * DEPRECATED FUNCTION, SHOULD USE s101sctr_setSectorConfig
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s101sctr_modifySector(
    TMWSCTR *pSector, 
    const S101SCTR_CONFIG *pConfig, 
    TMWTYPES_ULONG configMask);

  /* function: s101sctr_closeSector 
   * purpose: Close a currently open sector
   * arguments:
   *  pSector - sector to close
   * returns:
   *  TMWDEFS_TRUE if successful, else TMWDEFS_FALSE
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL s101sctr_closeSector(
    TMWSCTR *pSector);

  /* function: s101sctr_sendCyclic
   * purpose: Start sending cylic data now.
   *   This exported interface can be used when S101SCTR_CONFIG cyclic 
   *   is set to 0, which disables the SCL timer that causes cyclic data 
   *   to be sent periodically. 
   * arguments:
   *  pSector - pointer to sector returned by s101sctr_openSector.
   *   Indicates sector on which to send cyclic data.
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL s101sctr_sendCyclic(
    TMWSCTR *pSector);


#ifdef __cplusplus
}
#endif
#endif /* S101SCTR_DEFINED */
