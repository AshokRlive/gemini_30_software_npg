/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i870diag.h
 * description: Generic I870 diagnostics.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/utils/tmwtarg.h"
#include "tmwscl/utils/tmwcnfg.h"
#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/utils/tmwdiag.h"


#if TMWCNFG_SUPPORT_DIAG

/* Error messages used by both master and slave 101/103/104*/
static const I870DIAG_ERROR_ENTRY i870diagErrorMsg[] = {
  {I870DIAG_INCREMENTAL   ,  "Incremental timeout for request"},
  {I870DIAG_RESPONSE_TO   ,  "Response timeout for request,  "},
  {I870DIAG_FAILED_TX     ,  "Transmission failed for request,  "},
  {I870DIAG_SAME_TYPE     ,  "Request not queued, requested same ASDU type to same destination"},
  {I870DIAG_DUPLICATE     ,  "Duplicate request removed from message queue"},
  {I870DIAG_CANCELED      ,  "Request canceled, "},
  {I870DIAG_BUFLEN        ,  "New Tx Data bufLen too large"},
  {I870DIAG_INVALID_SIZE  ,  "Invalid message size received"},

  {I870DIAG_ERROR_ENUM_MAX,  ""}
};

/* Array to determine if specific error messages are disabled.
 */
static TMWTYPES_UCHAR _errorMsgDisabled[(I870DIAG_ERROR_ENUM_MAX/8)+1];


/* Global Functions */

/* routine: i870diag_validateErrorTable */
TMWTYPES_BOOL i870diag_validateErrorTable(void)
{
  int i;
  for(i=0; i<I870DIAG_ERROR_ENUM_MAX;i++)
  {
    if(i870diagErrorMsg[i].errorNumber != i)
      return(TMWDEFS_FALSE);
  }

  return(TMWDEFS_TRUE);
}

/* routine: i870diag_init */
void TMWDEFS_GLOBAL i870diag_init()
{
  /* No error messages are disabled by default. */
  memset(_errorMsgDisabled, 0, (I870DIAG_ERROR_ENUM_MAX/8)+1);
}

/* function: i870diag_sessionOnline */
void i870diag_sessionOnline(
  TMWSESN *pSession,
  TMWTYPES_BOOL online)
{
  TMWDIAG_ANLZ_ID id;
  char buf[256];

  if(online) 
  { 
    if(pSession->online != TMWSESN_STAT_ONLINE)
    {
      if(tmwdiag_initId(&id, TMWDEFS_NULL, pSession, TMWDEFS_NULL, TMWDIAG_ID_APPL | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
      {
        return;
      }
      tmwtarg_snprintf(buf, sizeof(buf), "%s: Session 0x%1x Online\n", tmwsesn_getSessionName(pSession), pSession);
      tmwdiag_skipLine(&id);
      tmwdiag_putLine(&id, buf);
    }
  }
  else
  {
    if(pSession->online != TMWSESN_STAT_OFFLINE)
    {
      if(tmwdiag_initId(&id, TMWDEFS_NULL, pSession, TMWDEFS_NULL, TMWDIAG_ID_APPL | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
      {
        return;
      }
      tmwtarg_snprintf(buf, sizeof(buf), "%s: Session 0x%1x Offline\n", tmwsesn_getSessionName(pSession), pSession);
      tmwdiag_skipLine(&id);
      tmwdiag_putLine(&id, buf);
    }
  }
}

/* function: i870diag_insertQueue */
void TMWDEFS_GLOBAL i870diag_insertQueue(
  TMWCHNL *pChannel,
  TMWSESN *pSession,
  TMWSCTR *pSector,
  const char *description)
{
  TMWDIAG_ANLZ_ID id;
  char buf[256];

  if(tmwdiag_initId(&id, pChannel, pSession, pSector, TMWDIAG_ID_USER) == TMWDEFS_FALSE)
  {
    return;
  }

  tmwtarg_snprintf(buf, sizeof(buf), "<+++ %-10s Insert request in queue: %s\n", 
    (pSector != TMWDEFS_NULL)? tmwsctr_getSectorName(pSector) : "broadcast", description);

  tmwdiag_skipLine(&id);
  tmwdiag_putLine(&id, buf);
}

/* function: i870diag_removeMessage */
void TMWDEFS_GLOBAL i870diag_removeMessage(
  TMWSESN_TX_DATA *pTxData,
  I870CHNL_RESP_STATUS status)
{
  TMWDIAG_ANLZ_ID id;
  char buf[256];
  const char *pStatusString;

  switch(status)
  {
  case I870CHNL_RESP_STATUS_FAILURE:
    pStatusString = "Failure";
    break;
  case I870CHNL_RESP_STATUS_TIMEOUT:
    pStatusString = "Timeout";
    break;
  case I870CHNL_RESP_STATUS_CANCELED:
    pStatusString = "Canceled";
    break;
  default:
    return;
  }

  if(tmwdiag_initId(&id, pTxData->pChannel, pTxData->pSession, pTxData->pSector, TMWDIAG_ID_USER) == TMWDEFS_FALSE)
  {
    return;
  }

  tmwtarg_snprintf(buf, sizeof(buf), "<+++ %-10s Remove request from queue:\n", 
    (pTxData->pSector != TMWDEFS_NULL)? tmwsctr_getSectorName(pTxData->pSector) : "broadcast");

  tmwdiag_skipLine(&id);
  tmwdiag_putLine(&id, buf);

  tmwtarg_snprintf(buf, sizeof(buf), "%16s%s, %s\n", 
    " ", pTxData->pMsgDescription, pStatusString);

  tmwdiag_putLine(&id, buf);
}

/* function: i870diag_notRemovedMessage */
void TMWDEFS_GLOBAL i870diag_notRemovedMessage(
  TMWSESN_TX_DATA *pTxData)
{
  TMWDIAG_ANLZ_ID id;
  char buf[128];

  if(tmwdiag_initId(&id, pTxData->pChannel, pTxData->pSession, pTxData->pSector, TMWDIAG_ID_USER) == TMWDEFS_FALSE)
  {
    return;
  }

  tmwtarg_snprintf(buf, sizeof(buf), "<+++ %-10s Broadcast, request not removed from queue:\n", 
    (pTxData->pSector != TMWDEFS_NULL)? tmwsctr_getSectorName(pTxData->pSector) : "broadcast");

  tmwdiag_skipLine(&id);tmwdiag_putLine(&id, buf);

  tmwtarg_snprintf(buf, sizeof(buf), "%16s%s\n", 
    " ", pTxData->pMsgDescription);

  tmwdiag_putLine(&id, buf);
}


/* function: i870diag_errorMsgEnable */
TMWTYPES_BOOL TMWDEFS_GLOBAL i870diag_errorMsgEnable(
  I870DIAG_ERROR_ENUM errorNumber,
  TMWTYPES_BOOL value)
{
  if(errorNumber > I870DIAG_ERROR_ENUM_MAX)
    return(TMWDEFS_FALSE);

  if(value)
    _errorMsgDisabled[errorNumber/8] &=  ~(1 <<(errorNumber%8));
  else
    _errorMsgDisabled[errorNumber/8] |=  (1 <<(errorNumber%8));

  return(TMWDEFS_TRUE);
}

/* function: _errorMsgDisabledFunc */
static TMWTYPES_BOOL TMWDEFS_LOCAL _errorMsgDisabledFunc(
   I870DIAG_ERROR_ENUM errorNumber)
{
  int value;

  if(errorNumber > I870DIAG_ERROR_ENUM_MAX)
    return(TMWDEFS_TRUE);

  value = _errorMsgDisabled[errorNumber/8] & (1 <<(errorNumber%8));
  if(value == 0)
    return(TMWDEFS_FALSE);
  else
    return(TMWDEFS_TRUE);
}

/* function: i870diag_errorMsg */
void TMWDEFS_GLOBAL i870diag_errorMsg(
  TMWCHNL *pChannel,
  TMWSESN *pSession, 
  TMWSCTR *pSector,
  I870DIAG_ERROR_ENUM errorNumber,
  TMWTYPES_CHAR *pExtraTextMsg)
{
  const char *pName;
  TMWDIAG_ANLZ_ID anlzId;
  char buf[256];

#ifdef TMW_SUPPORT_MONITOR
  /* If in analyzer or listen only mode, do not display this error */
  if(pChannel != TMWDEFS_NULL && pChannel->pPhysContext->monitorMode)
#endif
  if(_errorMsgDisabledFunc(errorNumber))
    return;

  if(tmwdiag_initId(&anlzId, pChannel, pSession, pSector, TMWDIAG_ID_APPL | TMWDIAG_ID_ERROR) == TMWDEFS_FALSE)
  {
    return;
  }
  
  if(pSector != TMWDEFS_NULL)
  {
    pName = tmwsctr_getSectorName(pSector);
  }
  else if(pSession != TMWDEFS_NULL)
  {
    pName = tmwsesn_getSessionName(pSession);
  }
  else 
  {
    pName = tmwchnl_getChannelName(pChannel);
  }

  if(pName == (const char *)0)
    pName = " ";

  if(pExtraTextMsg == TMWDEFS_NULL)
    (void)tmwtarg_snprintf(buf, sizeof(buf), "**** %s   %s ****\n", pName, i870diagErrorMsg[errorNumber].pErrorMsg);
  else
    (void)tmwtarg_snprintf(buf, sizeof(buf), "**** %s   %s  %s****\n", pName, i870diagErrorMsg[errorNumber].pErrorMsg, pExtraTextMsg);

  tmwdiag_skipLine(&anlzId);
  tmwdiag_putLine(&anlzId, buf);
}


#endif /* TMWCNFG_SUPPORT_DIAG */
