/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/


/* file: s14fscna.c
 * description: IEC 60870-5-101 slave FSCNA (File Transfer -File Ready)
 *  functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14fscna.h"
#include "tmwscl/i870/s14file.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/i14diag.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/utils/tmwtarg.h"

#if S14DATA_SUPPORT_FILE

/* function: s14fscna_processRequest */
void TMWDEFS_CALLBACK s14fscna_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  TMWTYPES_ULONG ioa;
  TMWTYPES_USHORT fileName;
  TMWTYPES_UCHAR sectionName;
  TMWTYPES_UCHAR scq;

  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  /* Store originator address */
  p14Sector->fileOriginator = pMsg->origAddress;

  /* Diagnostics */
  I14DIAG_SHOW_STORE_TYPE(pSector, pMsg);
  
  /* Parse Information Object Address */
  i870util_readInfoObjAddr(pSector->pSession, pMsg, &ioa);
  
  /* Read name of file */
  tmwtarg_get16(&pMsg->pRxData->pMsgBuf[pMsg->offset], &fileName);
  pMsg->offset += 2;

  /* Read name of section */
  sectionName = pMsg->pRxData->pMsgBuf[pMsg->offset++];  

  /* Read scq */
  scq = pMsg->pRxData->pMsgBuf[pMsg->offset++];   
 
  /* Diagnostics */
  I14DIAG_SHOW_FSCNA(pSector, ioa, fileName, sectionName, scq);

  /* Save information from APDU */
  p14Sector->fileIOA           = ioa;
  p14Sector->fileName          = fileName;
  p14Sector->fileSectionName   = sectionName; 

  if(pMsg->cot == I14DEF_COT_REQUEST)
  {
    /* This is a Call Directory Request */
    p14Sector->fileDirIndex = 0;
    p14Sector->fileFdrtaCOT = I14DEF_COT_REQUEST;
  }
  else if(pMsg->cot == I14DEF_COT_FILE_XFER)
  {
    switch(scq & I14DEF_SCQ_CMD_MASK)
    {
      case I14DEF_SCQ_SELECT_FILE: /* select file */
      {
        /* set state to select file */
        p14Sector->fileState = S14FILE_SELECT_STATE;
      }
      break;

      case I14DEF_SCQ_CALL_FILE: /* call/request file */
      {
        /* Reset file checksum to get ready to send file */ 
        p14Sector->fileChecksum = 0;
  
        /* set state to file call state */
        p14Sector->fileState = S14FILE_CALL_STATE;
      }
      break;

      case I14DEF_SCQ_DEACT_FILE: /* deactivate file */
      {
        /* deactivate means error stop file transfer
         */
      }
      break;

      case I14DEF_SCQ_DELETE_FILE: /* delete file */
      {
        /* set state to delete file */
        p14Sector->fileState = S14FILE_DELETE_STATE;
      }
      break;

      case I14DEF_SCQ_SELECT_SECTION: /* select section */
      {
        /* set state to select section */
        p14Sector->fileState = S14FILE_SECTION_SELECT_STATE;
      }
      break;

      case I14DEF_SCQ_CALL_SECTION: /* call/request section */
      {  
        /* Reset section checksum to get ready to send section */ 
        p14Sector->fileSectionChecksum = 0;

        /* set state to request/call section */
        p14Sector->fileState = S14FILE_SECTION_CALL_STATE;
      }
      break;

      case I14DEF_SCQ_DEACT_SECTION: /* deactivate section */
      {
        /* The master did not want this section. Slave should offer */ 
        /* up next section by sending a FSRNA ADPU                  */
        p14Sector->fileSectionName++;
        p14Sector->fileState = S14FILE_SECTION_DEACTIVATE_STATE;
      }
      break;
    }
  }
  else
  { 
    /* Build and send a Negative Activation Confirmation */
    s14sesn_sendNegActCon(pSector, pMsg, I14DEF_COT_UNKNOWN_COT);
    return;
  }
}

#endif /* S14DATA_SUPPORT_FILE */
