/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:S101Session.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19 Jul 2016     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "S101Debug.h"
#include "S101Channel.h"
#include "S101Session.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static  Logger& log(Logger::getLogger(SUBSYSTEM_ID_SCADA_HANDLER));

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
S101Session::S101Session(
                    S101Channel& channel):
                    m_channel(channel),
                    mp_tmwsesn(NULL),
                    mp_tmwsesnCnfg(NULL)
{}


S101Session::~S101Session()
{
    /* Delete all sectors*/
    for (S101SectorVector::iterator it = m_sectors.begin();
                    it != m_sectors.end(); ++it)
    {
      delete (*it);
    }
    m_sectors.clear();

    closeSession();

    delete mp_tmwsesnCnfg;
    mp_tmwsesnCnfg  = NULL;
}

SCADAP_ERROR S101Session::openSession(TMWCHNL* tmwchnl)
{
    DBG_INFO("%s opening session",TITLE);

    checkNotNull(tmwchnl,       "Cannot open IEC101 session. tmwchnl is null!");
    checkNotNull(mp_tmwsesnCnfg,"Cannot open IEC101 session. No configuration found!");
    checkNull   (mp_tmwsesn,    "Cannot open IEC101 session. It is already open!");

    /* Open session*/
    mp_tmwsesn = s101sesn_openSession(tmwchnl,mp_tmwsesnCnfg);
    checkNotNull(mp_tmwsesn,"Unable to initialise S101 Session.");

    /* Open sectors*/
    for (S101SectorVector::iterator itSector = m_sectors.begin();
                    itSector != m_sectors.end(); ++itSector)
    {
         (*itSector)->openSector(mp_tmwsesn);
    }

    return SCADAP_ERROR_NONE;
}

void S101Session::closeSession()
{
    /* Close all channels*/
    for (S101SectorVector::iterator it = m_sectors.begin();
                    it != m_sectors.end(); ++it)
    {
        DBG_INFO("%s closing sector...",TITLE);
        (*it)->closeSector();
    }

    if(mp_tmwsesn != NULL)
    {
        s101sesn_closeSession(mp_tmwsesn);
        mp_tmwsesn = NULL;
    }
}

void S101Session::configure(const S101SESN_CONFIG& sessionConfig)
{
    if(mp_tmwsesnCnfg == NULL)
    {
        mp_tmwsesnCnfg = new S101SESN_CONFIG();
	}
	
    memcpy(mp_tmwsesnCnfg, &sessionConfig, sizeof(S101SESN_CONFIG));

    /* Apply configuration to existing session*/
    if(mp_tmwsesn != NULL)
    {
        s101sesn_setSessionConfig(mp_tmwsesn, mp_tmwsesnCnfg);
    }

}

void S101Session::addSector(S101Sector* sectorPtr)
{
    if(sectorPtr != NULL)
    {
        m_sectors.push_back(sectorPtr);
    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
