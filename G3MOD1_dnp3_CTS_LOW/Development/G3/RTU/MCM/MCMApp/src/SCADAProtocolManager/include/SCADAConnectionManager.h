/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:SCADAConnectionManager.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   18 Jul 2014     andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef SCADACONNECTIONMANAGER_H_
#define SCADACONNECTIONMANAGER_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <string>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <signal.h>

#include <unistd.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <asm/errno.h>
#include <asm/ioctls.h>
#include <fcntl.h>

#include <net/if.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>

#include <errno.h>
#include <string.h>

#include <vector>
#include <string>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "GlobalDefs.h"
#include "Debug.h"
#include "timeOperations.h"
#include "Logger.h"

#include "TCPSocket.h"
#include "ISlaveProtocolChannel.h"
#include "ISCADAConnectionManager.h"
#include "ConnectionTimer.h"
#include "PowerSupplyCANChannel.h"
#include "TimeManager.h"
#include "IPointObserver.h"

#define BUFFER_SIZE    2048
#define LOCALPORTSPOOL 64
#define LOCALPORTSTART 500  // Start allocating local listening ports at 500

static lu_int32_t localPorts[LOCALPORTSPOOL] = { 0 };
static TimeManager& TimeMan = TimeManager::getInstance();



enum CONNMAN_ERROR
{
    CONNMAN_ERROR_NONE        = 0,
    CONNMAN_ERROR_CONFIG         ,
    CONNMAN_ERROR_SOCKET         ,
    CONNMAN_ERROR_LAST
};



/**
 * This component is for controlling the connection status of a slave protocol channel.
 * It is responsible for:
 * 1. check connection status, switch SCADA address when it detects current
 *    connection fails (fail over enabled).
 * 2. check incoming SCADA message, make a connection between SCADA and TMW lib.
 * 3. check events queue, initiates a connection to send events if pending
 *    events are detected.
 *
 */
class SCADAConnectionManager : public ISCADAConnectionManager
{
private:

    struct SocketPair
    {
        SocketPair() : listenerConfigPtr(NULL),
                       connectorConfigPtr(NULL),
                       listenerPtr(NULL),
                       acceptedPtr(NULL),
                       connectorPtr(NULL),
                       active(false),
                       numBytesIn(0),
                       numBytesOut(0) {};

        SocketConfig* listenerConfigPtr;
        SocketConfig* connectorConfigPtr;

        TCPSocket*    listenerPtr;  /* Socket to listen on */

        TCPSocket*    acceptedPtr;  /* Incoming socket */
        TCPSocket*    connectorPtr  /* Outgoing socket */;

        bool          active;

        // Simple stats
        lu_uint32_t   numBytesIn;
        lu_uint32_t   numBytesOut;
    };

public:
    SCADAConnectionManager(ISlaveProtocolChannel& channel);
    virtual                 ~SCADAConnectionManager();

    virtual void            setSCADAConnectionIPConfig(std::string ipAddress, lu_int32_t port);

    virtual void            setSCADAListenerIPConfig(std::string ipAddress, lu_int32_t port);

    virtual void            setSCADAConnManConfig(SCADAConnManConfig config);

    void                    init();

    /** Override */
    virtual void            tickEvent();

    virtual SocketConfig    *getSCADAConnectionIPConfig();

    virtual SocketConfig    *getSCADAListenerIPConfig();

    static lu_int32_t       getLocalPort();

private:

    /**
     * \brief
     *
     * \return Error code
     */
    lu_int32_t getUint16FromBuff(lu_uint8_t *buf, lu_uint16_t *val);

    /**
     * \brief
     *
     * \return Error Code
     */
    lu_int32_t parseDNP3Packet(lu_uint8_t *buf, lu_int32_t bufLen, lu_uint16_t *srcAddr, lu_uint16_t *destAddr);

    /**
     * \brief
     *
     * \return N/A
     */
    bool checkListener(SocketPair &socketPair);

    /**
     * \brief
     *
     * \return N/A
     */
    void checkSCADAConnectionProgress();

    /**
     * \brief
     *
     * \return N/A
     */
    void connectToSCADAFailed();

    /**
     * \brief
     *
     * \return N/A
     */
    void checkConnectivityConnectionProgress();

    /**
     * \brief
     *
     * \return N/A
     */
    void setNextConnectivityCheck();

    /**
     * \brief
     *
     * \return N/A
     */
    void incrementConnectionTries();

    /**
     * \brief
     *
     * \return N/A
     */
    void resetConnectivityRetries();

    /**
     * \brief
     *
     * \return N/A
     */
    void resetConnectionGroupRetries();

    /**
     * \brief
     *
     * \return N/A
     */
    void connectivityCheckFailed();

    /**
     * \brief
     *
     * \return N/A
     */
    lu_int32_t sniffIncomingPacket(TCPSocket& sock, lu_uint16_t *address, bool *validMaster);

    /**
     * \brief
     *
     * \return Error Code
     */
    CONNMAN_ERROR transferSocketData(SocketPair &sockPair);

    /**
     * \brief
     *
     * \return N/A
     */
    void deactivateSocketPair(SocketPair &sockPair);

    /**
     * \brief
     *
     * \return N/A
     */
    void sniffIncomingSCADAData();

    /**
     * \brief
     *
     * \return N/A
     */
    void connectToSCADA();

    /**
     * \brief
     *
     * \return N/A
     */
    bool connectivityCheck();

    /**
     * \brief
     *
     * \return N/A
     */
    void resetSockets();

    /**
     * \brief
     *
     * \return N/A
     */
    IOM_ERROR cycleCommsPower();

    /**
     * \brief
     *
     * \return N/A
     */
    void checkTimers();

    /**
     * \brief
     *
     * \return N/A
     */
    void startTimer(TIMER timer);

    /**
     * \brief
     *
     * \return N/A
     */
    void resetTimer(TIMER timer);

    /**
     * \brief
     *
     * \return N/A
     */
    void stopTimer(TIMER timer);

private:
    static const lu_uint16_t Dnp3Header =  0x6405;

    Logger&                 m_log;

    SocketPair              m_scadaListener;
    SocketPair              m_scadaConnector;

    ISlaveProtocolChannel&  m_slaveProtoCh;

    lu_uint8_t              mp_buf[BUFFER_SIZE];

    bool                    m_initialised;
    bool                    m_connectionInProgress;

    bool                    m_connectionDelayInOperation;

    /* static Timer Signal Pool implementation */
    lu_uint32_t             m_timeouts[TIMER_LAST];
    ConnectionTimer         m_timers[TIMER_LAST];

    TCPSocket               m_connectivityCheckSock;

    SCADAConnManConfig      m_config;

    lu_uint32_t             m_connectionTriesLeft;        // Number of SCADA connection attempts left

    lu_int32_t              m_connectivityCheckTriesLeft; // Stores connectivity check retries
    lu_uint32_t             m_connectivityCheckIndex;     // Current connectivityCheck SCADA Index

private:
    class InhibitObserver : public IPointObserver
     {
     public:
         InhibitObserver(PointIdStr& pid);
         virtual void update(PointIdStr pointID, PointData *pointDataPtr);
         virtual PointIdStr getPointID();
         lu_uint8_t getValue(){return m_value;}
     private:
         PointIdStr m_pid;
         lu_uint8_t m_value;
     };
    InhibitObserver*        mp_inhibit;
};
#endif /* SCADACONNECTIONMANAGER_H_ */

/*
 *********************** End of file ******************************************
 */
