/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Dummy DNP3 Slave protocol manager factory interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   08/09/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_35CF1912_937A_45ee_8448_8001922BD379__INCLUDED_)
#define EA_35CF1912_937A_45ee_8448_8001922BD379__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class DummyDNP3SlaveProtocolManagerFactoy : public IDNP3SlaveProtocolManagerFactory
{

public:
    DummyDNP3SlaveProtocolManagerFactoy() {};
    virtual ~DummyDNP3SlaveProtocolManagerFactoy() {};

    virtual lu_uint32_t protocolManagerNumber();
    virtual DNP3SlaveProtocol* newProtocolManager( lu_uint32_t     idx           ,
                                                          SCHED_TYPE      slaveSchedType,
                                                          lu_uint32_t     slavePriority ,
                                                          GeminiDatabase &GDatabase,
                                                          IStatusManager &statusManager
                                                        );

};
#endif // !defined(EA_35CF1912_937A_45ee_8448_8001922BD379__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
