/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i14def.h
 * description: IEC 60870-5-101/104 definitions
 */
#ifndef I14DEF_DEFINED
#define I14DEF_DEFINED

/* Type Ids using labels defined in:
 * IEC 60870-5-101 clause 7.2.1.1 & IEC 60870-5-104 clause 6
 */

/* Process information in monitor direction */
#define I14DEF_TYPE_LINK_RESET   0  /* used for local link reset request */
#define I14DEF_TYPE_MSPNA1       1  /* Single-point information          */
#define I14DEF_TYPE_MSPTA1       2  /* Single-point information with     */
                                    /* time tag                          */
#define I14DEF_TYPE_MDPNA1       3  /* Double-point information          */
#define I14DEF_TYPE_MDPTA1       4  /* Double-point information with     */
                                      /* time tag                        */
#define I14DEF_TYPE_MSTNA1       5  /* Step position information         */
#define I14DEF_TYPE_MSTTA1       6  /* Step position information with    */
                                      /* time tag                          */
#define I14DEF_TYPE_MBONA1       7  /* Bitstring of 32 bits              */
#define I14DEF_TYPE_MBOTA1       8  /* Bitstring of 32 bits with time    */
                                      /* tag                               */
#define I14DEF_TYPE_MMENA1       9  /* Measured value, normalized value  */
#define I14DEF_TYPE_MMETA1      10  /* Measured value, normalized value  */
                                      /* with time tag                     */
#define I14DEF_TYPE_MMENB1      11  /* Measured value, scaled value      */
#define I14DEF_TYPE_MMETB1      12  /* Measured value, scaled value with */
                                      /* time tag                          */
#define I14DEF_TYPE_MMENC1      13  /* Measured value, short floating    */
                                      /* point number                      */
#define I14DEF_TYPE_MMETC1      14  /* Measured value, short floating    */
                                      /* point number with time tag        */
#define I14DEF_TYPE_MITNA1      15  /* Integrated totals                 */
#define I14DEF_TYPE_MITTA1      16  /* Integrated totals with time tag   */
#define I14DEF_TYPE_MEPTA1      17  /* Event of protection equipment     */
                                      /* with time tag                     */
#define I14DEF_TYPE_MEPTB1      18  /* Packed start events of protection */
                                      /* equipment with time tag           */
#define I14DEF_TYPE_MEPTC1      19  /* Packed output circuit information */
                                      /* protection equipment with time    */
                                      /* tag                               */
#define I14DEF_TYPE_MPSNA1      20  /* Packed single-point information   */
                                      /* with status change detection      */
#define I14DEF_TYPE_MMEND1      21  /* Measured value, normalized value  */
                                      /* without quality descriptor        */

/* 22..29 reserved for further compatible definitions */

#define I14DEF_TYPE_MSPTB1      30  /* Single-point information with     */
                                      /* 56-bit time tag                   */
#define I14DEF_TYPE_MDPTB1      31  /* Double-point information with     */
                                      /* 56-bit time tag                   */
#define I14DEF_TYPE_MSTTB1      32  /* Step position information with    */
                                      /* 56-bit time tag                   */
#define I14DEF_TYPE_MBOTB1      33  /* Bitstring of 32 bits with 56-bit  */
                                      /* time tag                          */
#define I14DEF_TYPE_MMETD1      34  /* Measured value, normalized value  */
                                      /* with 56-bit time tag              */
#define I14DEF_TYPE_MMETE1      35  /* Measured value, scaled value with */
                                      /* 56-bit time tag                   */
#define I14DEF_TYPE_MMETF1      36  /* Measured value, short floating    */
                                      /* point number with 56-bit time tag */
#define I14DEF_TYPE_MITTB1      37  /* Integrated totals with 56-bit     */
                                      /* time tag                          */
#define I14DEF_TYPE_MEPTD1      38  /* Event of protection equipment     */
                                      /* with 56-bit time tag              */
#define I14DEF_TYPE_MEPTE1      39  /* Packed start events of protection */
                                      /* equipment with 56-bit time tag    */
#define I14DEF_TYPE_MEPTF1      40  /* Packed output circuit information */
                                      /* protection equipment with 56-bit  */
                                      /* time tag                          */

/* Process information in control direction */
#define I14DEF_TYPE_CSCNA1      45  /* Single command                    */
#define I14DEF_TYPE_CDCNA1      46  /* Double command                    */
#define I14DEF_TYPE_CRCNA1      47  /* Regulating step command           */
#define I14DEF_TYPE_CSENA1      48  /* Set point command, normalized     */
                                      /* value                             */
#define I14DEF_TYPE_CSENB1      49  /* Set point command, scaled value   */
#define I14DEF_TYPE_CSENC1      50  /* Set point command, short floating */
                                      /* point number                      */
#define I14DEF_TYPE_CBONA1      51  /* Bitstring of 32 bits              */

/* 52..57 reserved for further compatible definitions */

/* Process information in control direction with time tag (CP56Time2a)     */
#define I14DEF_TYPE_CSCTA1      58  /* Single command with time tag      */
#define I14DEF_TYPE_CDCTA1      59  /* Double command with time tag      */
#define I14DEF_TYPE_CRCTA1      60  /* Regulating step command with time */
                                      /* tag                               */
#define I14DEF_TYPE_CSETA1      61  /* Set point command, normalized     */
                                      /* value with time tag               */
#define I14DEF_TYPE_CSETB1      62  /* Set point command, scaled value   */
                                      /* with time tag                     */
#define I14DEF_TYPE_CSETC1      63  /* Set point command, short floating */
                                      /* point number with time tag        */
#define I14DEF_TYPE_CBOTA1      64  /* Bitstring of 32 bits with time    */
                                      /* tag                               */

/* 65..69 reserved for further compatible definitions */

/* System Information in monitor direction */
#define I14DEF_TYPE_MEINA1      70  /* End of initialization             */

/* 71..99 reserved for further compatible definitions */

/* System Information in control direction */
#define I14DEF_TYPE_CICNA1     100  /* interrogation Command             */
#define I14DEF_TYPE_CCINA1     101  /* Counter interrogation command     */
#define I14DEF_TYPE_CRDNA1     102  /* read command                      */
#define I14DEF_TYPE_CCSNA1     103  /* Clock synchronization command     */
#define I14DEF_TYPE_CTSNA1     104  /* Test Command                      */
#define I14DEF_TYPE_CRPNA1     105  /* Reset Process command             */
#define I14DEF_TYPE_CCDNA1     106  /* delay acquisition command         */
#define I14DEF_TYPE_CTSTA1     107  /* Test Command with time tag        */

/* 108..109 reserved for further compatible definitions */

/* Parameter in control direction */
#define I14DEF_TYPE_PMENA1     110  /* parameter of measured value,      */
                                    /* normalized value                  */
#define I14DEF_TYPE_PMENB1     111  /* parameter of measured value,      */
                                    /* scaled value                      */
#define I14DEF_TYPE_PMENC1     112  /* parameter of measured value,      */
                                    /* short floating point number       */
#define I14DEF_TYPE_PACNA1     113  /* parameter activation              */

/* 114..119 reserved for further compatible definitions */

/* File transfer */
#define I14DEF_TYPE_FFRNA1     120  /* File ready                        */
#define I14DEF_TYPE_FSRNA1     121  /* Section ready                     */
#define I14DEF_TYPE_FSCNA1     122  /* Call directory, select file, call */
                                    /* file, call section, delete file   */
#define I14DEF_TYPE_FLSNA1     123  /* Last section, last segment        */
#define I14DEF_TYPE_FAFNA1     124  /* Ack file, ack section             */
#define I14DEF_TYPE_FSGNA1     125  /* Segment                           */
#define I14DEF_TYPE_FDRTA1     126  /* Directory                         */
#define I14DEF_TYPE_FSCNB1     127  /* Query Log, Request File Archive   */

/* The following ASDU types are custom ASDUs as specified by Gasunie
 * and are not defined in the IEC 60870-5-101 specification.  
 */
#define I14DEF_TYPE_MITNC1     240  /* Monitor Integrated Totals BCD     */
#define I14DEF_TYPE_MITTC1     241  /* Monitor Integrated Totals BCD     */
                                    /* with Time                         */
#define I14DEF_TYPE_CSENZ1     242  /* Set Integrated Total Command BCD  */
#define I14DEF_TYPE_MCTNA1     245  /* Monitor Configuration Table       */
                                    /* Information                       */
#define I14DEF_TYPE_CCTNA1     246  /* Command Configuration Table       */
                                    /* Information                       */

/* Now define default strings which can be used to describe each of the
 * ASDU types
 */

/* Process information in monitor direction */
#define I14DEF_TSTR_LINK_RESET  "Local link layer reset request"
#define I14DEF_TSTR_MSPNA1      "Single-point information"
#define I14DEF_TSTR_MSPTA1      "Single-point information with time tag"
#define I14DEF_TSTR_MDPNA1      "Double-point information"
#define I14DEF_TSTR_MDPTA1      "Double-point information with time tag"
#define I14DEF_TSTR_MSTNA1      "Step position information"
#define I14DEF_TSTR_MSTTA1      "Step position information with time tag"
#define I14DEF_TSTR_MBONA1      "Bitstring of 32 bits"
#define I14DEF_TSTR_MBOTA1      "Bitstring of 32 bits with time tag"
#define I14DEF_TSTR_MMENA1      "Measured value, normalized value"
#define I14DEF_TSTR_MMETA1      "Measured value, normalized value with time tag"
#define I14DEF_TSTR_MMENB1      "Measured value, scaled value"
#define I14DEF_TSTR_MMETB1      "Measured value, scaled value with time tag"
#define I14DEF_TSTR_MMENC1      "Measured value, short floating point number"
#define I14DEF_TSTR_MMETC1      "Measured value, short floating point number with time tag"
#define I14DEF_TSTR_MITNA1      "Integrated totals"
#define I14DEF_TSTR_MITTA1      "Integrated totals with time tag"
#define I14DEF_TSTR_MEPTA1      "Event of protection equipment with time tag"
#define I14DEF_TSTR_MEPTB1      "Packed start events of protection equipment with time tag"
#define I14DEF_TSTR_MEPTC1      "Packed output circuit information protection equipment with time tag"
#define I14DEF_TSTR_MPSNA1      "Packed single-point information with status change detection"
#define I14DEF_TSTR_MMEND1      "Measured value, normalized value without quality descriptor"

/* 22..29 reserved for further compatible definitions */

#define I14DEF_TSTR_MSPTB1 "Single-point information with 56-bit time tag"
#define I14DEF_TSTR_MDPTB1 "Double-point information with 56-bit time tag"
#define I14DEF_TSTR_MSTTB1 "Step position information with 56-bit time tag"
#define I14DEF_TSTR_MBOTB1 "Bitstring of 32 bits with 56-bit time tag"
#define I14DEF_TSTR_MMETD1 "Measured value, normalized value with 56-bit time tag"
#define I14DEF_TSTR_MMETE1 "Measured value, scaled value with 56-bit time tag"
#define I14DEF_TSTR_MMETF1 "Measured value, short floating point number with 56-bit time tag"
#define I14DEF_TSTR_MITTB1 "Integrated totals with 56-bit time tag"
#define I14DEF_TSTR_MEPTD1 "Event of protection equipment with 56-bit time tag"
#define I14DEF_TSTR_MEPTE1 "Packed start events of protection equipment with 56-bit time tag"
#define I14DEF_TSTR_MEPTF1 "Packed output circuit information protection equipment with 56-bit time tag"

/* Process information in control direction */
#define I14DEF_TSTR_CSCNA1 "Single command"
#define I14DEF_TSTR_CDCNA1 "Double command"
#define I14DEF_TSTR_CRCNA1 "Regulating step command"
#define I14DEF_TSTR_CSENA1 "Set point command, normalized value"
#define I14DEF_TSTR_CSENB1 "Set point command, scaled value"
#define I14DEF_TSTR_CSENC1 "Set point command, short floating point number"
#define I14DEF_TSTR_CBONA1 "Bitstring of 32 bits"

/* 52..57 reserved for further compatible definitions */

/* Process information in control direction with time tag (CP56Time2a)     */
#define I14DEF_TSTR_CSCTA1 "Single command with time tag"
#define I14DEF_TSTR_CDCTA1 "Double command with time tag"
#define I14DEF_TSTR_CRCTA1 "Regulating step command with time tag"
#define I14DEF_TSTR_CSETA1 "Set point command, normalized value with time tag"
#define I14DEF_TSTR_CSETB1 "Set point command, scaled value with time tag"
#define I14DEF_TSTR_CSETC1 "Set point command, short floating point number with time tag"
#define I14DEF_TSTR_CBOTA1 "Bitstring of 32 bits with time tag"

/* 65..69 reserved for further compatible definitions */

/* System Information in monitor direction */
#define I14DEF_TSTR_MEINA1 "End of initialization"

/* 71..99 reserved for further compatible definitions */

/* System Information in control direction */
#define I14DEF_TSTR_CICNA1 "Interrogation Command"
#define I14DEF_TSTR_CCINA1 "Counter interrogation command"
#define I14DEF_TSTR_CRDNA1 "Read command"
#define I14DEF_TSTR_CCSNA1 "Clock synchronization command"
#define I14DEF_TSTR_CTSNA1 "Test Command"
#define I14DEF_TSTR_CRPNA1 "Reset Process command"
#define I14DEF_TSTR_CCDNA1 "delay acquisition command"
#define I14DEF_TSTR_CTSTA1 "Test Command with time tag"

/* 108..109 reserved for further compatible definitions */

/* Parameter in control direction */
#define I14DEF_TSTR_PMENA1 "Parameter of measured value, normalized value"
#define I14DEF_TSTR_PMENB1 "Parameter of measured value, scaled value"
#define I14DEF_TSTR_PMENC1 "Parameter of measured value, short floating point number"
#define I14DEF_TSTR_PACNA1 "Parameter activation"

/* 114..119 reserved for further compatible definitions */

/* File transfer */
#define I14DEF_TSTR_FFRNA1 "File ready"
#define I14DEF_TSTR_FSRNA1 "Section ready"
#define I14DEF_TSTR_FSCNA1 "Call directory, select file, call file, call section, delete file"
#define I14DEF_TSTR_FLSNA1 "Last section, last segment"
#define I14DEF_TSTR_FAFNA1 "Ack file, ack section"
#define I14DEF_TSTR_FSGNA1 "Segment"
#define I14DEF_TSTR_FDRTA1 "Directory"
#define I14DEF_TSTR_FSCNB1 "Query Log"

/* The following ASDU types are custom ASDUs as specified by Gasunie
 * and are not defined in the IEC 60870-5-101 specification.  
 */
#define I14DEF_TSTR_MITNC1 "Monitor Integrated Totals BCD"
#define I14DEF_TSTR_MITTC1 "Monitor Integrated Totals BCD with Time"
#define I14DEF_TSTR_CSENZ1 "Set Integrated Total Command BCD"
#define I14DEF_TSTR_MCTNA1 "Monitor Configuration Table Information"
#define I14DEF_TSTR_CCTNA1 "Command Configuration Table Information"

/***************************************************************************/
/* CAUSE OF TRANSMISSION - IEC 60870-5-101 clause 7.2.3                    */
/***************************************************************************/

#define I14DEF_COT_MASK              0x3f /* Mask for COT value          */
#define I14DEF_COT_NEGATIVE_CONFIRM  0x40 /* can be or'd with ACTCON or  */
                                            /* DEACTCON                    */
#define I14DEF_COT_TEST              0x80 /* test mode or'd with any COT */

#define I14DEF_COT_NOT_DEFINED          0 /* Not defined                 */
#define I14DEF_COT_CYCLIC               1 /* periodic, cyclic            */
#define I14DEF_COT_BACKGROUND           2 /* background scan             */
#define I14DEF_COT_SPONTANEOUS          3 /* spontaneous                 */
#define I14DEF_COT_INITIALIZED          4 /* initialized                 */
#define I14DEF_COT_REQUEST              5 /* request or requested        */
#define I14DEF_COT_ACTIVATION           6 /* activation                  */
#define I14DEF_COT_ACTCON               7 /* activation confirmation     */
#define I14DEF_COT_DEACTIVATION         8 /* deactivation                */
#define I14DEF_COT_DEACTCON             9 /* deactivation confirmation   */
#define I14DEF_COT_ACTTERM             10 /* activation termination      */
#define I14DEF_COT_RETURN_REMOTE       11 /* return information caused   */
                                            /* by a remote command         */
#define I14DEF_COT_RETURN_LOCAL        12 /* return information caused   */
                                            /* by a local command          */
#define I14DEF_COT_FILE_XFER           13 /* file transfer               */

/* 14..19 reserved for further compatible definitions */
#define I14DEF_COT_INTG_GEN            20 /* interrogated by general     */
                                            /* interrogation               */
#define I14DEF_COT_INTG1               21 /* interrogated by group 1     */
                                            /* interrogation               */
#define I14DEF_COT_INTG2               22 /* interrogated by group 2     */
                                            /* interrogation               */
#define I14DEF_COT_INTG3               23 /* interrogated by group 3     */
                                            /* interrogation               */
#define I14DEF_COT_INTG4               24 /* interrogated by group 4     */
                                            /* interrogation               */
#define I14DEF_COT_INTG5               25 /* interrogated by group 5     */
                                            /* interrogation               */
#define I14DEF_COT_INTG6               26 /* interrogated by group 6     */
                                            /* interrogation               */
#define I14DEF_COT_INTG7               27 /* interrogated by group 7     */
                                            /* interrogation               */
#define I14DEF_COT_INTG8               28 /* interrogated by group 8     */
                                            /* interrogation               */
#define I14DEF_COT_INTG9               29 /* interrogated by group 9     */
                                            /* interrogation               */
#define I14DEF_COT_INTG10              30 /* interrogated by group 10    */
                                            /* interrogation               */
#define I14DEF_COT_INTG11              31 /* interrogated by group 11    */
                                            /* interrogation               */
#define I14DEF_COT_INTG12              32 /* interrogated by group 12    */
                                            /* interrogation               */
#define I14DEF_COT_INTG13              33 /* interrogated by group 13    */
                                            /* interrogation               */
#define I14DEF_COT_INTG14              34 /* interrogated by group 14    */
                                            /* interrogation               */
#define I14DEF_COT_INTG15              35 /* interrogated by group 15    */
                                            /* interrogation               */
#define I14DEF_COT_INTG16              36 /* interrogated by group 16    */
                                            /* interrogation               */
#define I14DEF_COT_REQCOGEN            37 /* requested by general        */
                                            /* counter request             */
#define I14DEF_COT_REQCO1              38 /* requested by group 1        */
                                            /* counter request             */
#define I14DEF_COT_REQCO2              39 /* requested by group 2        */
                                            /* counter request             */
#define I14DEF_COT_REQCO3              40 /* requested by group 3        */
                                            /* counter request             */
#define I14DEF_COT_REQCO4              41 /* requested by group 4        */
                                            /* counter request             */
/* 42..43 reserved for further compatible definitions */

/* The following are reported by remote devices when they receive requests */
/* they cannot handle.  These COTs are defined in IEC 101 Addendum 2.      */
#define I14DEF_COT_UNKNOWN_ASDU_TYPE   44 /* unknown type identification        */
#define I14DEF_COT_UNKNOWN_COT         45 /* unknown cause of transmission      */
#define I14DEF_COT_UNKNOWN_ASDU_ADDR   46 /* unknown common address of ASDU     */
#define I14DEF_COT_UNKNOWN_IOA         47 /* unknown information object address */

/* 48..63 for special use (private range) */

/***************************************************************************/
/* COMMON ADDRESS OF ASDU - IEC 60870-5-101 clause 7.2.4                   */
/***************************************************************************/
#define I14DEF_BROADCAST_ADDRESS_8     0xFF
#define I14DEF_BROADCAST_ADDRESS_16    0xFFFF

/***************************************************************************/
/* INFORMATION ELEMENTS - IEC 60870-5-101 clause 7.2.6                     */
/***************************************************************************/

/* Quality descriptors described in clause 7.2.6.3 (applicable to          */
/* SIQ, DIQ, QDS, QDP & SEP: clauses 7.2.6.1 - 7.2.6.4 & 7.2.6.10)         */
#define I14DEF_QUALITY_OV    0x01  /* value has overflowed                 */
#define I14DEF_QUALITY_EI    0x08  /* elapsed time invalid                 */
#define I14DEF_QUALITY_BL    0x10  /* value is blocked for transmission    */
#define I14DEF_QUALITY_SB    0x20  /* substituted by operator or an        */
                                   /* automatic source                     */
#define I14DEF_QUALITY_NT    0x40  /* not topical (not updated             */
                                   /* successfully)                        */
#define I14DEF_QUALITY_IV    0x80  /* invalid, corresponding value is      */
                                   /* incorrect and cannot be used         */

/* Single-point Information with Quality descriptor (SIQ) clause 7.2.6.1   */
#define I14DEF_SIQ_MASK (0x01)
#define I14DEF_SIQ_OFF  (0x00)
#define I14DEF_SIQ_ON   (0x01)

/* Double-point Information with Quality descriptor (DIQ) clause 7.2.6.2   */
#define I14DEF_DIQ_MASK           (0x03)
#define I14DEF_DIQ_INTERMEDIATE   (0x00)
#define I14DEF_DIQ_OFF            (0x01)
#define I14DEF_DIQ_ON             (0x02)
#define I14DEF_DIQ_INDETERMINATE  (0x03)

/* Quality descriptor (QDS) clause 7.2.6.3 */
#define I14DEF_QDS_OV      (0x01)  /* overflow                             */

/* Quality descriptor for protection equipment events (QDP) clause 7.2.6.4 */
#define I14DEF_QDP_EI      (0x08)  /* elapsed time invalid                 */

/* Value with Transient state Indication (VTI) clause 7.2.6.5 */
/* used with step position information information element */
#define I14DEF_VTI_TRANSIENT_STATE  0x80

/* Binary Counter Reading (BCR) Sequence and Quality Bits clause 7.2.6.9 */
#define I14DEF_BCR_SEQ_MASK         0x1f /* 0 through 31                 */
#define I14DEF_BCR_CY               0x20 /* 1 = counter has overflowed   */
#define I14DEF_BCR_CA               0x40 /* 1 = counter was adjusted     */
#define I14DEF_BCR_IV               0x80 /* 1 = counter reading is       */
                                         /* invalid                      */

/* Single event of protection equipment clause 7.2.6.10 */
#define I14DEF_SEP_ES               0x03 /* Bit mask for Event State  */
#define I14DEF_SEP_INDETERMINATE0   0x00 /* Indeterminate             */
#define I14DEF_SEP_OFF              0x01 /* OFF                       */
#define I14DEF_SEP_ON               0x02 /* ON                        */
#define I14DEF_SEP_INDETERMINATE3   0x03 /* Indeterminate             */

/* Start events protection equipment clause 7.2.6.11    */
#define I14DEF_SPE_GS               0x01 /* General start of operation     */
#define I14DEF_SPE_SL1              0x02 /* Start of operation of phase L1 */
#define I14DEF_SPE_SL2              0x04 /* Start of operation of phase L2 */
#define I14DEF_SPE_SL3              0x08 /* Start of operation of phase L3 */
#define I14DEF_SPE_SIE              0x10 /* Start of operation IE (earth   */
                                         /* current)                       */
#define I14DEF_SPE_SRD              0x20 /* Start of operation in reverse  */
                                         /* direction */

/* Output circuit information of protection equipment clause 7.2.6.12      */
#define I14DEF_OCI_GC               0x01 /* General command to output      */
                                         /* circuit                        */
#define I14DEF_OCI_CL1              0x02 /* Command to output circuit      */
                                         /* phase L1                       */
#define I14DEF_OCI_CL2              0x04 /* Command to output circuit      */
                                         /* phase L2                       */
#define I14DEF_OCI_CL3              0x08 /* Command to output circuit      */
                                         /* phase L3  */

/* Single Command State (SCS) clause 7.2.6.15 */
#define I14DEF_SCS_MASK             0x01 /* Mask for Single Command State  */
#define I14DEF_SCS_OFF              0x00 /* Single command state = OFF     */
#define I14DEF_SCS_ON               0x01 /* Single command state = ON      */

/* Double Command State (DCS) clause 7.2.6.16 */
#define I14DEF_DCS_MASK             0x03 /* Double Command State Mask      */
#define I14DEF_DCS_OFF              0x01 /* Double Command State OFF       */
#define I14DEF_DCS_ON               0x02 /* Double Command State ON        */

/* Regulating step Command State (RCS) clause 7.2.6.17 */
#define I14DEF_RCS_MASK             0x03 /* Regulating step command state  */
                                         /* Mask                           */
#define I14DEF_RCS_STEP_LOWER       0x01 /* next step LOWER                */
#define I14DEF_RCS_STEP_HIGHER      0x02 /* next step HIGHER               */

/* Two Octet Binary Time clause 7.2.6.20
 * No #define statments are required for this field.
 * The value is a 2 octet value representing an elapsed time, such as
 * "Relay operating time" or "Relay duration time." The range is:
 *  0 to 59,999 milliseconds
 */

/* CauseOf Initialization (COI) clause 7.2.6.21 */
typedef unsigned char I14DEF_COI;
#define I14DEF_COI_POWER_ON        0          /* Local power switch on     */
#define I14DEF_COI_MANUAL_RESET    1          /* Local manual reset        */
#define I14DEF_COI_REMOTE_RESET    2          /* Remote reset              */
#define I14DEF_COI_LOCAL_PARAMS_CHANGED  0x80 /* Initialization after      */
                                              /* change of local parameters*/

/* Qualifier of Interrogation (General Interrogation) (QOI -- 7.2.6.22 */
typedef unsigned char I14DEF_QOI;
#define I14DEF_QOI_STATION_GLOBAL   20   /* Station interrogation (global) */
#define I14DEF_QOI_GROUP_1          21   /* Interrogation of group 1       */
#define I14DEF_QOI_GROUP_2          22   /* Interrogation of group 2       */
#define I14DEF_QOI_GROUP_3          23   /* Interrogation of group 3       */
#define I14DEF_QOI_GROUP_4          24   /* Interrogation of group 4       */
#define I14DEF_QOI_GROUP_5          25   /* Interrogation of group 5       */
#define I14DEF_QOI_GROUP_6          26   /* Interrogation of group 6       */
#define I14DEF_QOI_GROUP_7          27   /* Interrogation of group 7       */
#define I14DEF_QOI_GROUP_8          28   /* Interrogation of group 8       */
#define I14DEF_QOI_GROUP_9          29   /* Interrogation of group 9       */
#define I14DEF_QOI_GROUP_10         30   /* Interrogation of group 10      */
#define I14DEF_QOI_GROUP_11         31   /* Interrogation of group 11      */
#define I14DEF_QOI_GROUP_12         32   /* Interrogation of group 12      */
#define I14DEF_QOI_GROUP_13         33   /* Interrogation of group 13      */
#define I14DEF_QOI_GROUP_14         34   /* Interrogation of group 14      */
#define I14DEF_QOI_GROUP_15         35   /* Interrogation of group 15      */
#define I14DEF_QOI_GROUP_16         36   /* Interrogation of group 16      */

/* Qualifier of Counter interrogation Command (QCC) clause 7.2.6.23 */
typedef unsigned char I14DEF_QCC;
#define I14DEF_QCC_RQT_MASK       (0x3f) /* RQT (request) mask             */
#define I14DEF_QCC_RQT_NONE       (0x00) /* no couner requested (not used) */
#define I14DEF_QCC_RQT_GROUP1     (0x01) /* Request counter group 1        */
#define I14DEF_QCC_RQT_GROUP2     (0x02) /* Request counter group 2        */
#define I14DEF_QCC_RQT_GROUP3     (0x03) /* Request counter group 3        */
#define I14DEF_QCC_RQT_GROUP4     (0x04) /* Request counter group 4        */
#define I14DEF_QCC_RQT_GENERAL    (0x05) /* general request counter        */

#define I14DEF_QCC_FRZ_MASK         0xc0 /* FRZ (Freeze) Mask              */
#define I14DEF_QCC_FRZ_READ_ONLY    0x00 /* Read (no freeze or reset)      */
#define I14DEF_QCC_FRZ_NO_RESET     0x40 /* Counter freeze without reset   */
                                         /*  (value frozen represents      */
                                         /*  integrated total)             */
#define I14DEF_QCC_FRZ_RESET        0x80 /* Counter freeze with reset      */
                                         /*  (value frozen represents      */
                                         /*  incremental information       */
#define I14DEF_QCC_FRZ_RESET_ONLY   0xc0 /* Counter reset                  */

/* Qualifier Of Parameter (QPM) of measured values clause 7.2.6.24 */
typedef unsigned char I14DEF_QPM;
#define I14DEF_QPM_KPA_MASK         0x2f /* KPA (Kind of Parameter) mask   */
#define I14DEF_QPM_KPA_UNUSED       0x00 /* Not used                       */
#define I14DEF_QPM_KPA_THRESHOLD    0x01 /* Threshold value                */
#define I14DEF_QPM_KPA_SMOOTHING    0x02 /* Smoothing factor (filter time  */
                                         /*   constant)                    */
#define I14DEF_QPM_KPA_LOW_LIMIT    0x03 /* Low limit for transmission of  */
                                         /*   measured values              */
#define I14DEF_QPM_KPA_HIGH_LIMIT   0x04 /* High limit for transmission    */
                                         /*   of measured values           */
#define I14DEF_QPM_LPC              0x40 /* Local parameter change         */
#define I14DEF_QPM_POP              0x80 /* Parameter not in operation     */

/* Qualifier of Parameter Activation (QPA) clause 7.2.6.25
 * Notes:
 *   - Act/Deact is defined in the Cause of Transmission (COT)
 *   - I14DEF_QPA_LOADED_PARAMS and I14DEF_QPA_ADDRESSED_PARAMS are
 *     currently unused in IEC 60870-5-101/104, but are reserved for
 *     future extensions
 */
typedef unsigned char I14DEF_QPA;
#define I14DEF_QPA_LOADED_PARAMS    0x01 /* Act/deact of the previously    */
                                         /*   loaded parameters            */
#define I14DEF_QPA_ADDRESSED_PARAMS 0x02 /* Act/deact of the parameter of  */
                                         /*   the addressed object         */
#define I14DEF_QPA_CYCLIC_PARAMS    0x03 /* Act/deact of persistent cyclic */
                                         /*   or periodic transmission of  */
                                         /*   the addressed object         */

/* Qualifier Of Command (QOC) clause 7.2.6.26
 * Note:
 *   - The duration of short- and long- pulse is determined by a system
 *     parameter in the outstation.
 */
typedef unsigned char I14DEF_QOC;
#define I14DEF_QOC_QU_MASK          0x7C  /* Mask for QU field             */
#define I14DEF_QOC_QU_USE_DEFAULT   0x00  /* No additional definition      */
#define I14DEF_QOC_QU_SHORT_PULSE   0x04  /* Short pulse duration          */
                                          /*   (circuit-breaker)           */
#define I14DEF_QOC_QU_LONG_PULSE    0x08  /* Long pulse duration           */
#define I14DEF_QOC_QU_PERSISTENT    0x0C  /* Persistent output             */

#define I14DEF_QOC_SE_MASK          0x80  /* Mask for S/E (Select/Execute) */
                                          /*   Field                       */
#define I14DEF_QOC_SE_EXECUTE       0x00  /* Execute                       */
#define I14DEF_QOC_SE_SELECT        0x80  /* Select                        */

/* Qualifier Of reset Process Command (QRP) clause 7.2.6.27 */
typedef unsigned char I14DEF_QRP;
#define I14DEF_QRP_RESET_GENERAL    0x01  /* General reset of process      */
#define I14DEF_QRP_RESET_EVENTS     0x02  /* Reset of pending information  */
                                          /*   with time tag of the event  */
                                          /*   buffer                      */

/* Qualifier Of Set-Point Command (QOS) clause 7.2.6.39 */
typedef unsigned char I14DEF_QOS;
#define I14DEF_QOS_QL_MASK          0x7F  /* Mask for QL field             */
#define I14DEF_QOS_QL_USE_DEFAULT   0x00  /* Default                       */

#define I14DEF_QOS_SE_MASK          0x80  /* Mask for S/E (Select/Execute) */
                                          /*   field                       */
#define I14DEF_QOS_SE_EXECUTE       0x00  /* Execute                       */
#define I14DEF_QOS_SE_SELECT        0x80  /* Select                        */

/* File Ready Qualifiers (FRQ) clause 7.2.6.28 */
#define I14DEF_FRQ_MASK             0x80  /* Mask for File Ready Qualifier */
#define I14DEF_FRQ_POS_CONFIRM      0x00  /* Positive confirm of select,   */
                                          /*   request,deactivate or       */
                                          /*   delete                      */
#define I14DEF_FRQ_NEG_CONFIRM      0x80  /* Negative confirm of select,   */
                                          /*   request, deactivate or      */
                                          /*   delete                      */

/* Section Ready Qualifiers (SRQ) clause 7.2.6.29 */
#define I14DEF_SRQ_MASK             0x80  /* Mask for Section Ready        */
                                          /*  Qualifier (SRQ)              */
#define I14DEF_SRQ_SECT_READY       0x00  /* Section ready to load         */
#define I14DEF_SRQ_SECT_NOT_READY   0x80  /* Section not ready to load     */

/* File Select and Call Qualifiers (SCQ) clause 7.2.6.30
 * See also I14DEF_FILE_XFER_ERR_xxx fields defined below
 */
#define I14DEF_SCQ_CMD_MASK         0x0F  /* Mask for command field        */
#define I14DEF_SCQ_SELECT_FILE      0x01  /* Select file                   */
#define I14DEF_SCQ_CALL_FILE        0x02  /* Request file                  */
#define I14DEF_SCQ_DEACT_FILE       0x03  /* Deactivate file               */
#define I14DEF_SCQ_DELETE_FILE      0x04  /* Delete file                   */
#define I14DEF_SCQ_SELECT_SECTION   0x05  /* Select section                */
#define I14DEF_SCQ_CALL_SECTION     0x06  /* Request section               */
#define I14DEF_SCQ_DEACT_SECTION    0x07  /* Deactivate section            */

/* Last section or segment Qualifiers (LSQ) clause 7.2.6.31 */
#define I14DEF_LSQ_FILE_XFER_NO_DEACT 0x01 /* File transfer without        */
                                           /*   deactivation               */
#define I14DEF_LSQ_FILE_XFER_DEACT    0x02 /* File transfer with           */
                                           /*   deactivation               */
#define I14DEF_LSQ_SECT_XFER_NO_DEACT 0x03 /* Section transfer without     */
                                           /*   deactivation               */
#define I14DEF_LSQ_SECT_XFER_DEACT    0x04 /* Section transfer with        */
                                           /*   deactivation               */

/* ACK file or section Qualifiers (AFQ) clause 7.2.6.32
 * See also I14DEF_FILE_XFER_ERR_xxx fields defined below
 */
#define I14DEF_AFQ_CMD_MASK           0x0F /* Mask for AFQ command field   */
#define I14DEF_AFQ_FILE_XFER_POS_ACT  0x01 /* Positive acknowledge of file */
                                           /*   transfer              */
#define I14DEF_AFQ_FILE_XFER_NEG_ACT  0x02 /* Negative acknowledge of file */
                                           /*   transfer                   */
#define I14DEF_AFQ_SECT_XFER_POS_ACT  0x03 /* Positive acknowledge of      */
                                           /*   section transfer           */
#define I14DEF_AFQ_SECT_XFER_NEG_ACT  0x04 /* Negative acknowledge of      */
                                           /*   section transfer           */

/* file transfer error clause 7.2.6.30 and 7.2.6.32 */
#define I14DEF_FILE_XFER_ERR_MASK     0xF0 /* Mask for File Transfer Error */
#define I14DEF_FILE_XFER_ERR_NO_MEM   0x10 /* Requested memory space not   */
                                           /*   available                  */
#define I14DEF_FILE_XFER_ERR_BAD_CHS  0x20 /* Checksum failed              */
#define I14DEF_FILE_XFER_ERR_NO_COMM  0x30 /* Unexpected communication     */
                                           /*    service                   */
#define I14DEF_FILE_XFER_ERR_BAD_FILE 0x40 /* Unexpected name of file      */
#define I14DEF_FILE_XFER_ERR_BAD_SECT 0x50 /* Unexpected name of section   */

/* SOF status of file (SOF) clause 7.2.6.38 */
#define I14DEF_SOF_LFD_MASK           0x20 /* Mask for LFD (Last File of   */
                                           /*   Directory) field           */
#define I14DEF_SOF_LFD_MORE_FILES     0x00 /* Additional file of the       */
                                           /*    directory follows         */
#define I14DEF_SOF_LFD_LAST_FILE      0x20 /* Last file of the directory   */

#define I14DEF_SOF_FOR_MASK           0x40 /* Mask for FOR (name) field    */
#define I14DEF_SOF_FOR_IS_FILE        0x00 /* Name defines file            */
#define I14DEF_SOF_FOR_IS_DIR         0x40 /* Name defines subdirectory    */

#endif /* I14DEF_DEFINED */

