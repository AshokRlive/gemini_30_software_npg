/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s101mem.h
 * description: IEC 60870-5-101 slave memory functions
 */
#ifndef S101MEM_DEFINED
#define S101MEM_DEFINED

#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/utils/tmwmem.h"
#include "tmwscl/i870/s101cnfg.h"
#include "tmwscl/i870/s14mem.h"
#include "tmwscl/i870/i870mem1.h"
#include "tmwscl/i870/i870mem.h"

typedef enum S101memAllocType {
  S101MEM_SESN_TYPE,
  S101MEM_SCTR_TYPE,
  S101MEM_ALLOC_TYPE_MAX
} S101MEM_ALLOC_TYPE;

typedef struct {
  /* Specify number of slave 101 sessions that can be open. */
  TMWTYPES_UINT numSessions;

  /* Specify number of slave 101 sectors that can be open. */
  TMWTYPES_UINT numSectors;
} S101MEM_CONFIG;

    
#ifdef __cplusplus
extern "C" 
{
#endif

  /* routine: s101mem_initConfig
   *  indicating the number of buffers of each structure type to 
   *  put in each memory pool. These will be initialized according 
   *  to the compile time defines. The user can change the desired
   *  fields and call s101mem_initMemory()
   * arguments:
   *  pS104Config - pointer to memory configuration structure to be filled in
   *  pS14MemConfig - pointer to memory configuration structure to be filled in
   *  pI870Mem1Config - pointer to memory configuration structure to be filled in
   *  pI870MemConfig - pointer to memory configuration structure to be filled in
   *  pTmwConfig - pointer to memory configuration structure to be filled in
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL s101mem_initConfig(
    S101MEM_CONFIG  *pS101Config,
    S14MEM_CONFIG   *pS14MemConfig,
    I870MEM1_CONFIG *pI870Mem1Config,
    I870MEM_CONFIG  *pI870MemConfig,
    TMWMEM_CONFIG   *pTmwConfig);

  /* routine: s101mem_initMemory
   * purpose: memory management init function. Can be used
   *  to modify the number of buffers that will be allowed in each
   *  buffer pool. This can only be used when TMWCNFG_USE_DYNAMIC_MEMORY
   *  is set to TMWDEFS_TRUE
   *  NOTE: This should be called before calling tmwappl_initApplication()
   * arguments:
   *  pS104Config - pointer to memory configuration structure to be used
   *  pS14MemConfig - pointer to memory configuration structure to be used
   *  pI870Mem1Config - pointer to memory configuration structure to be used
   *  pI870MemConfig - pointer to memory configuration structure to be used
   *  pTmwConfig - pointer to memory configuration structure to be used
   * returns:
   *  TMWDEFS_TRUE if successful
   *  TMWDEFS_FALSE otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s101mem_initMemory(
    S101MEM_CONFIG  *pS101Config,
    S14MEM_CONFIG   *pS14MemConfig,
    I870MEM1_CONFIG *pI870Mem1Config,
    I870MEM_CONFIG  *pI870MemConfig,
    TMWMEM_CONFIG   *pTmwConfig);

  /* routine: s101mem_init
   * purpose: INTERNAL memory management init function.
   *  NOTE: user should call s101mem_initMemory() to modify the number
   *  of buffers allowed for each type.
   * arguments:
   *  pConfig - pointer to memory configuration structure to be used
   * returns:
   *  TMWDEFS_TRUE if successful
   *  TMWDEFS_FALSE otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s101mem_init(
    S101MEM_CONFIG *pS101Config);

  /* function: s101mem_alloc
   * purpose:  Allocate memory  
   * arguments: 
   *  type - enum value indicating what structure to allocate
   * returns:
   *   TMWDEFS_NULL if allocation failed
   *   void * pointer to allocated memory if successful
   */
  void * TMWDEFS_GLOBAL s101mem_alloc(
    S101MEM_ALLOC_TYPE type);

  /* function: s101mem_free
   * purpose:  Deallocate memory
   * arguments: 
   *  pBuf - pointer to buffer to be deallocated
   * returns:    
   *   void  
   */
   void TMWDEFS_GLOBAL s101mem_free(
    void *pBuf);

  /* function: s101mem_getUsage
   * purpose:  Determine memory usage for each type of memory
   *    managed by this file.
   * arguments: 
   *  index: index of pool, starting with 0 caller can call
   *    this function repeatedly, while incrementing index. When
   *     index is larger than number of pools, this function
   *     will return TMWDEFS_FALSE
   *  pName: pointer to a char pointer to be filled in
   *  pStruct: pointer to structure to be filled in.
   * returns:    
   *  TMWDEFS_TRUE  if successfully returned usage statistics.
   *  TMWDEFS_FALSE if failure because index is too large.
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL s101mem_getUsage(
    TMWTYPES_UCHAR index,
    const TMWTYPES_CHAR **pName,
    TMWMEM_POOL_STRUCT *pStruct);
 
#ifdef __cplusplus
}
#endif
#endif /* S101MEM_DEFINED */

