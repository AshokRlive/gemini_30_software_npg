/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:S101OutputPoint.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       Contains all the functionality(classes) with regards to updating/setting
 *       values to the different Slave101 output points.
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19 Jul 2016     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef S101OUTPUTPOINT_H_
#define S101OUTPUTPOINT_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "SCADAProtocolCommon.h"
#include "ControlLogicCommon.h"
#include "S101TMWIncludes.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
class S101Database; // Forward declaration

/**
 * \brief Abstract S101 Output point.
 */
class S101OutputPoint
{
public:

    typedef enum {
        SCO, //single command
        DCO, //double command
    }ProtPointType;

public:
    S101OutputPoint(I870OutputPointConfStr& oPointConf):
                    m_oPointConf(oPointConf),
                    mp_db(NULL)
    {}
    ~S101OutputPoint(){};

    /**
     * /brief Get output point config structure.
     *
     * /returns Reference to output point config structure
     */
    const I870OutputPointConfStr& getPointConf() {return m_oPointConf;}

    /**
     * /brief Execute single point based on the Group ID
     *
     * /param pointer to the G3 database,
     *        output point config structure ,
     *        sco - single command as defined in 7.2.6.15.
     *       This contains single command state (SCS) and Qualifier of Command QOC
     *       combined into a single byte
     *
     * /returns SUCCESS/FAILURE
     */
    static TMWDEFS_COMMAND_STATUS executeSCO(void* gdb, I870OutputPointConfStr groupID, TMWTYPES_UCHAR sco);

    /**
     * /brief Execute double point based on the Group ID
     *
     * /param pointer to the G3 database,
     *        output point config structure ,
     *        dco - double command as defined in 7.2.6.16.
     *        This contains double command state (DCS) and Qualifier of Command QOC
     *        combined into a single byte
     *
     * /returns SUCCESS/FAILURE
     */
    static TMWDEFS_COMMAND_STATUS executeDCO(void* gdb, I870OutputPointConfStr groupID, TMWTYPES_UCHAR dco);


    /**
     * /brief set the database pointer member of this class
     *
     * /param pointer to S101Database
     */
    void setDatabase(S101Database* db);

    /**
     * /brief get the pointer to the sector
     *
     * /param pointer to 101 sector
     *
     *  /returns SCADAP_ERROR_NONE or SCADAP_ERROR_NULL_POINTER
     */
    SCADAP_ERROR getSectorHandle(TMWSCTR** ppSectorHdle);

private:
    S101Database*       mp_db;
    I870OutputPointConfStr m_oPointConf;
};



#endif /* S101OUTPUTPOINT_H_ */

/*
 *********************** End of file ******************************************
 */
