/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i870dia4.h
 * description: Diagnostic routines for i870lnk4
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/utils/tmwtarg.h"
#include "tmwscl/utils/tmwcnfg.h"

#include "tmwscl/i870/i870dia4.h"

#if TMWCNFG_SUPPORT_DIAG

/* Maximum number of bytes to display on a single row */
#define MAX_ROW_LENGTH 16

/* Error messages used by both master and slave 104 */
static const I870DIAG_ERROR_ENTRY i870Dia4ErrorMsg[] = {
  {I870DIA4_INVALID_SEQ      ,  "Invalid received Receive sequence"},
  {I870DIA4_INVALID_SENDSEQ  ,  "Invalid received Send sequence, frame discarded"},
  {I870DIA4_INV_UFRAME       ,  "Invalid Unnumbered control"},
  {I870DIA4_START_CHAR       ,  "Start Char not received"},
  {I870DIA4_RCV_LENGTH       ,  "Receive length exceeds RX frame size"},
  {I870DIA4_IFRAME_TO        ,  "I Format APDU t1 Timeout"},
  {I870DIA4_UFRAME_TO        ,  "U Format APDU t1 timeout"},
  {I870DIA4_STOPDT_TO        ,  "STOP DT U Format APDU t1 timeout"},
  {I870DIA4_TX_FULL          ,  "Tx Descriptor list is full"},
  {I870DIA4_K_TOO_BIG        ,  "'k' value is too big"},
  {I870DIA4_DISCARD_STOPPED  ,  "Discarded frame received on channel needing STARTDT"},
  {I870DIA4_DISCARD_INACTIVE ,  "Discarded frame received on inactive session"},

  {I870DIA4_ERROR_ENUM_MAX   ,  ""}
};

/* Array to determine if specific error messages are disabled.
 */
static TMWTYPES_UCHAR _errorMsgDisabled[(I870DIA4_ERROR_ENUM_MAX/8)+1];


/* function: _displayLinkFrame
 * purpose: display link level frame
 * arguments:
 *  prompt - prompt for text
 *  channelName - name of channel
 *  pBuf - header data
 *  numBytes - number of bytes
 * returns:
 *  void
 */
static void TMWDEFS_LOCAL _displayLinkFrame(
  TMWCHNL *pChannel,
  const char *prompt, 
  const TMWTYPES_UCHAR *pBuf, 
  TMWTYPES_USHORT numBytes,
  TMWTYPES_MILLISECONDS elapsedTime,
  TMWDIAG_ID direction)
{
  TMWTYPES_UCHAR control;
  TMWTYPES_CHAR buf[256];
  TMWDIAG_ANLZ_ID id;
  TMWTYPES_USHORT rcvSequence;  
  TMWTYPES_USHORT sendSequence;
  int rowLength;
  int index;
  int len;
  int i;

  if(tmwdiag_initId(&id, pChannel, TMWDEFS_NULL, TMWDEFS_NULL, TMWDIAG_ID_LINK | direction) == TMWDEFS_FALSE)
  {
    return;
  }

  control = (TMWTYPES_UCHAR)
    (pBuf[I870DEF4_APDU_INDEX_CONTROL_1] & I870DEF4_APDU_FORMAT_MASK);
 
  /* Display header information */
  if (control == I870DEF4_APDU_FORMAT_SUPER)
  {
    tmwtarg_snprintf(buf, sizeof(buf), "%s %-10s %s\n", 
      prompt, tmwchnl_getChannelName(pChannel), "Supervisory");
    
    tmwdiag_skipLine(&id);
    tmwdiag_putLine(&id, buf);
    
    tmwtarg_get16(&pBuf[I870DEF4_APDU_INDEX_CONTROL_3], &rcvSequence);
    if(elapsedTime == 0)
    {
      tmwtarg_snprintf(buf, sizeof(buf), "%15s N(R) %d\n",  " ", (rcvSequence>>1));
    }
    else
    {
      tmwtarg_snprintf(buf, sizeof(buf), "%15s N(R) %d   T1 elapsed time = %d milliseconds\n",
        " ", (rcvSequence>>1), elapsedTime); 
    }
    tmwdiag_putLine(&id, buf); 
  }	  
  else if (control == I870DEF4_APDU_FORMAT_UNNUM)
  {
    tmwtarg_snprintf(buf, sizeof(buf), "%s %-10s %s\n", 
      prompt, tmwchnl_getChannelName(pChannel), "Unnumbered");
    
    tmwdiag_skipLine(&id);
    tmwdiag_putLine(&id, buf);
    
    switch(pBuf[I870DEF4_APDU_INDEX_CONTROL_1])
    {
    case(I870DEF4_APDU_CTRL_STARTDT_ACT):
      {
        
        tmwtarg_snprintf(buf, sizeof(buf), "%15s %s  \n", " ","STARTDT ACT");      
        break;
      }
    case(I870DEF4_APDU_CTRL_STOPDT_ACT):
      {
        tmwtarg_snprintf(buf, sizeof(buf), "%15s %s  \n", " ","STOPDT ACT");
        break;
      }
    case(I870DEF4_APDU_CTRL_TESTFR_ACT):
      {
        tmwtarg_snprintf(buf, sizeof(buf), "%15s %s  \n", " ","TESTFR ACT");
        break;
      }
    case(I870DEF4_APDU_CTRL_STARTDT_CON):
      {   
        tmwtarg_snprintf(buf, sizeof(buf), "%15s %s  \n", " ","STARTDT CON");
        break;
      }
    case(I870DEF4_APDU_CTRL_STOPDT_CON):
      {
        tmwtarg_snprintf(buf, sizeof(buf), "%15s %s  \n", " ","STOPDT CON");
        break;
      }
    case(I870DEF4_APDU_CTRL_TESTFR_CON):
      {	 
        tmwtarg_snprintf(buf, sizeof(buf), "%15s %s  \n", " ","TESTFR CON");
        break;
      }
    default:
      {
        tmwtarg_snprintf(buf, sizeof(buf), "%15s %s  \n", " ","INVALID FUNCTION");
        break;
      }
    }
    tmwdiag_putLine(&id, buf);
  }
  else  
  { 
    tmwtarg_snprintf(buf, sizeof(buf), "%s %-10s %s\n", 
      prompt, tmwchnl_getChannelName(pChannel), "Information");
    
    tmwdiag_skipLine(&id);
    tmwdiag_putLine(&id, buf);
    
    tmwtarg_get16(&pBuf[I870DEF4_APDU_INDEX_CONTROL_1], &sendSequence);
    
    tmwtarg_get16(&pBuf[I870DEF4_APDU_INDEX_CONTROL_3], &rcvSequence);
    
    if(elapsedTime == 0)
    {
      tmwtarg_snprintf(buf, sizeof(buf), "%15s N(S) %d  N(R) %d \n", 
        " ",(sendSequence>>1), (rcvSequence>>1)); 
    }
    else
    {
      tmwtarg_snprintf(buf, sizeof(buf), "%15s N(S) %d  N(R) %d   T1 elapsed time = %d milliseconds\n", 
        " ",(sendSequence>>1), (rcvSequence>>1), elapsedTime); 
    }
    tmwdiag_putLine(&id, buf); 
  }
  
  /* Now display the actual bytes */
  len = tmwtarg_snprintf(buf, sizeof(buf), "%16s", " ");
  for(i = 0; i < I870DEF4_APDU_INDEX_ASDU; i++)
    len += tmwtarg_snprintf(buf + len, sizeof(buf) - len, "%02x ", pBuf[i]);
  
  tmwtarg_snprintf(buf + len, sizeof(buf) - len, "\n");
  tmwdiag_putLine(&id, buf);
  
  index = I870DEF4_APDU_INDEX_ASDU;
  while(index < numBytes)
  {
    len = tmwtarg_snprintf(buf, sizeof(buf), "%16s", " ");
    
    rowLength = numBytes - index;
    if(rowLength > MAX_ROW_LENGTH)
      rowLength = MAX_ROW_LENGTH;
    
    for(i = 0; i < rowLength; i++)
      len += tmwtarg_snprintf(buf + len, sizeof(buf) - len, "%02x ", pBuf[index++]);
    
    tmwtarg_snprintf(buf + len, sizeof(buf) - len, "\n");
    tmwdiag_putLine(&id, buf);
  }
}

/* routine: i870dia4_validateErrorTable */
TMWTYPES_BOOL i870dia4_validateErrorTable(void)
{
  int i;
  for(i=0; i<I870DIA4_ERROR_ENUM_MAX;i++)
  {
    if(i870Dia4ErrorMsg[i].errorNumber != i)
      return(TMWDEFS_FALSE);
  }

  return(TMWDEFS_TRUE);
}

/* routine: i870dia4_init */
void TMWDEFS_GLOBAL i870dia4_init()
{
  /* No error messages are disabled by default. */
  memset(_errorMsgDisabled, 0, (I870DIA4_ERROR_ENUM_MAX/8)+1);
}

/* function: i870dia4_linkFrameSent */
void TMWDEFS_GLOBAL i870dia4_linkFrameSent(
  TMWCHNL *pChannel,
  const TMWTYPES_UCHAR *pBuf, 
  TMWTYPES_USHORT numBytes)
{
  _displayLinkFrame(pChannel, "<---", pBuf, numBytes, 0, 0);
}

/* function: i870dia4_linkFrameReceived */
void TMWDEFS_GLOBAL i870dia4_linkFrameReceived(
  TMWCHNL *pChannel,
  const TMWTYPES_UCHAR *pBuf, 
  TMWTYPES_USHORT numBytes,
  TMWTYPES_MILLISECONDS elapsedTime)
{
  _displayLinkFrame(pChannel, "--->", pBuf, numBytes, elapsedTime, TMWDIAG_ID_RX);
}

/* function: i870dia4_errorMsgEnable */
TMWTYPES_BOOL TMWDEFS_GLOBAL i870dia4_errorMsgEnable(
  I870DIA4_ERROR_ENUM errorNumber,
  TMWTYPES_BOOL value)
{
  if(errorNumber > I870DIA4_ERROR_ENUM_MAX)
    return(TMWDEFS_FALSE);

  if(value)
    _errorMsgDisabled[errorNumber/8] &=  ~(1 <<(errorNumber%8));
  else
    _errorMsgDisabled[errorNumber/8] |=  (1 <<(errorNumber%8));

  return(TMWDEFS_TRUE);
}

/* function: _errorMsgDisabledFunc */
static TMWTYPES_BOOL TMWDEFS_LOCAL _errorMsgDisabledFunc(
   I870DIA4_ERROR_ENUM errorNumber)
{
  int value;

  if(errorNumber > I870DIA4_ERROR_ENUM_MAX)
    return(TMWDEFS_TRUE);

  value = _errorMsgDisabled[errorNumber/8] & (1 <<(errorNumber%8));
  if(value == 0)
    return(TMWDEFS_FALSE);
  else
    return(TMWDEFS_TRUE);
}

/* function: i870dia4_error */
void TMWDEFS_GLOBAL i870dia4_error(
  TMWCHNL *pChannel,
  I870DIA4_ERROR_ENUM errorNumber)
{
  const char *pName;
  TMWDIAG_ANLZ_ID anlzId;
  char buf[256];

#ifdef TMW_SUPPORT_MONITOR
  /* If in analyzer or listen only mode, do not display this error */
  if(pChannel != TMWDEFS_NULL && pChannel->pPhysContext->monitorMode)
#endif
  if(_errorMsgDisabledFunc(errorNumber))
    return;

  if(tmwdiag_initId(&anlzId, pChannel, TMWDEFS_NULL, TMWDEFS_NULL, TMWDIAG_ID_LINK | TMWDIAG_ID_ERROR) == TMWDEFS_FALSE)
  {
    return;
  }
 
  pName = tmwchnl_getChannelName(pChannel);

  if(pName == (const char *)0)
    pName = " ";

  (void)tmwtarg_snprintf(buf, sizeof(buf), "**** %s   %s ****\n", pName, i870Dia4ErrorMsg[errorNumber].pErrorMsg);

  tmwdiag_skipLine(&anlzId);
  tmwdiag_putLine(&anlzId, buf);
}

#endif /* TMWCNFG_SUPPORT_DIAG */
