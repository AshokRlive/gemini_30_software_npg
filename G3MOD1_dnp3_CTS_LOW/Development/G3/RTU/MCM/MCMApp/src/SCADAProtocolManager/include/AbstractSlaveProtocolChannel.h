/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:AbstractSlaveProtocolChannel.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   27 Oct 2014     andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef ABSTRACTSLAVEPROTOCOLCHANNEL_H_
#define ABSTRACTSLAVEPROTOCOLCHANNEL_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "ISlaveProtocolChannel.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


class AbstractSlaveProtocolChannel:public ISlaveProtocolChannel
{
public:
    AbstractSlaveProtocolChannel():m_connectivityCheckEnabled(LU_FALSE),
                                   m_connectionManagerEnabled(LU_FALSE),
                                   connMgr(NULL),
                                   m_scadaAddresses(),
                                   m_numAddresses(0),
                                   m_curAddressIdx(0)
    {}

    virtual ~AbstractSlaveProtocolChannel();

    void addScadaAddress(const ScadaAddress& newAddress);

    void setConnectionManagerEnabled(bool val);

    void setConnectionManagerConfig(ISCADAConnectionManager::SCADAConnManConfig scadaConnManConfig);

    lu_bool_t getConnectionGroupEnabled()
    {
        return m_connectionManagerEnabled;
    }

    SCADAP_ERROR addSession(ISlaveProtocolSession &session);

protected:

    virtual lu_uint32_t getNumSCADAAddresses()
    {
        return m_numAddresses;
    }

    virtual bool setNextSCADAAddress();

    virtual bool setCurrentMaster(lu_uint32_t masterAddress);

    virtual bool findSCADAIPAddress(std::string ipAddress);

    virtual bool getSCADAAddressIndex(lu_uint32_t index, ScadaAddress& scadaAddr);

    virtual bool getCurrentSCADAAddress(ScadaAddress& scadaAddr);

    virtual SCADAP_ERROR deleteSessions();

    virtual bool setMasterAddress(lu_uint32_t masterAddress);

    virtual void setSessionsOnline(bool online);

    virtual lu_uint32_t getCurrentSCADAIndex()
    {
        return m_curAddressIdx;
    }

protected:
    lu_bool_t           m_connectivityCheckEnabled; // Perform a regular connectivity check to Masters
    lu_bool_t           m_connectionManagerEnabled;   // This channel is part of a connection group

    ISCADAConnectionManager* connMgr;

private:
    typedef std::vector<ScadaAddress> ScadaAddressVector;

    ScadaAddressVector  m_scadaAddresses;
    lu_uint32_t         m_numAddresses;
    lu_uint32_t         m_curAddressIdx;

protected:
    typedef std::vector<ISlaveProtocolSession*> SessionVector;
    SessionVector       m_SlaveSessions;  // Vector of Session Objects
};


#endif /* ABSTRACTSLAVEPROTOCOLCHANNEL_H_ */

/*
 *********************** End of file ******************************************
 */
