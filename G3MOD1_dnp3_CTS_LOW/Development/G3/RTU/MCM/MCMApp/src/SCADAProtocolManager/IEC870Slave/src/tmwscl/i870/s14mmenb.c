/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14mmenb.c
 * description: IEC 60870-5-101 slave scaled value point measurand functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14mmenb.h"
#include "tmwscl/i870/s14mem.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14evnt.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/i870util.h"
#include "tmwscl/utils/tmwmem.h"
#include "tmwscl/utils/tmwtarg.h"

#if S14DATA_SUPPORT_MME_B 

/* Forward declaration */
static void TMWDEFS_LOCAL _initEventDesc(
  TMWSCTR *pSector,
  S14EVNT_DESC *pDesc);

/* function: _internalAddEvent
 * purpose: Internal function called by SCL to add an event to the queue.
 *  This will not tell the link layer to ask the application for a message
 *  to send. 
 * arguments:
 *  pSector - pointer to sector structure returned by s14sctr_openSector
 *  cot - cause of transmission
 *  ioa - Information object address
 *  value -
 *  quality -
 *  pTimeStamp - pointer to time structures
 * returns:
 *  void *
 */
static void * TMWDEFS_LOCAL _internalAddEvent(
  TMWSCTR *pSector, 
  TMWTYPES_UCHAR cot,
  TMWTYPES_ULONG ioa, 
  TMWTYPES_SHORT value, 
  TMWTYPES_UCHAR quality, 
  TMWDTIME *pTimeStamp)
{
  S14EVNT_DESC desc;
  S14MMENB_EVENT *pEvent;
  _initEventDesc(pSector, &desc);

#if S14DATA_SUPPORT_DOUBLE_TRANS
  {
  S14MMENB_EVENT *pDoubleTransEvent;
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  if(p14Sector->mmenbTransmissionMode == S14DATA_TRANSMISSION_DOUBLE)
  {
    desc.doubleTransmission = TMWDEFS_TRUE;
  }
  else if(p14Sector->mmenbTransmissionMode == S14DATA_TRANSMISSION_PERPOINT)
  { 
    void *pPoint = s14data_mmenbLookupPoint(p14Sector->i870.pDbHandle, ioa);
    if((pPoint != TMWDEFS_NULL)
      && s14data_mmenbGetTransmissionMode(pPoint))
    {
      desc.doubleTransmission = TMWDEFS_TRUE;
    }
  }
  pEvent = (S14MMENB_EVENT *)s14evnt_addEvent(pSector, cot, ioa, quality, pTimeStamp, &desc, (void **)&pDoubleTransEvent);

  if(pEvent != TMWDEFS_NULL)
  {
    pEvent->value = value;
    /* Set value of lower priority double transmission entry */
    if(pDoubleTransEvent != TMWDEFS_NULL)
    {
      pDoubleTransEvent->value = value;
    } 
  }
  }
#else
  pEvent = (S14MMENB_EVENT *)s14evnt_addEvent(pSector, cot, ioa, quality, pTimeStamp, &desc, TMWDEFS_NULL);

  if(pEvent != TMWDEFS_NULL)
  {
    pEvent->value = value;
  }
#endif
  return(pEvent);
}
/* function: _changedFunc 
 * purpose: Determine if the specified point has changed and if so
 *  get the current values and add an event to the queue
 * arguments:
 *  pSector - identifies sector
 * returns:
 *  TMWDEFS_TRUE if an event was added
 *  TMWDEFS_FALSE otherwise
 */
static TMWTYPES_BOOL TMWDEFS_CALLBACK _changedFunc(
  TMWSCTR *pSector,
  void *pPoint, 
  TMWDTIME *pTimeStamp)
{  
  TMWDEFS_CHANGE_REASON reason;
  TMWTYPES_SHORT sva;
  TMWTYPES_UCHAR qds;

  if(s14data_mmenbChanged(pPoint, &sva, &qds, &reason))
  { 
    if(_internalAddEvent(pSector, s14evnt_reasonToCOT(reason), 
      s14data_mmenbGetInfoObjAddr(pPoint), sva, qds, 
      pTimeStamp) != TMWDEFS_NULL)
    {
      return(TMWDEFS_TRUE);
    }
  }
  return(TMWDEFS_FALSE);
}

/* function: _eventData 
 * purpose: Insert data from event structure into message to be sent
 * arguments:
 *  pTxData - pointer to transmit data structure
 *  pEvent - pointer to event to be put into message
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _eventData(
  TMWSESN_TX_DATA *pTxData, 
  S14EVNT *pEvent)
{
  S14MMENB_EVENT *pMMENBEvent = (S14MMENB_EVENT *)pEvent;

  /* Store SVA */
  tmwtarg_store16((TMWTYPES_USHORT *)&pMMENBEvent->value, &pTxData->pMsgBuf[pTxData->msgLength]);
  pTxData->msgLength += 2;

  /* Store quality */
  pTxData->pMsgBuf[pTxData->msgLength++] = pEvent->quality;
}

/* function: _initEventDesc 
 * purpose: Initialize event descriptor
 * arguments:
 *  pSector - identifies sector
 *  pDesc - pointer to descriptor structure 
 * returns:
 *  void
 */
static void TMWDEFS_LOCAL _initEventDesc(
  TMWSCTR *pSector,
  S14EVNT_DESC *pDesc)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  pDesc->typeId = I14DEF_TYPE_MMENB1;
  pDesc->cotSpecified = 0;
  pDesc->woTimeTypeId = I14DEF_TYPE_MMENB1;
  pDesc->eventMemType = S14MEM_MMENB_EVENT_TYPE;
  pDesc->pEventList = &p14Sector->mmenbEvents;
  pDesc->eventMode = p14Sector->mmenbEventMode;
  pDesc->scanEnabled = p14Sector->mmenbScanEnabled;
  pDesc->maxEvents = p14Sector->mmenbMaxEvents;
  pDesc->timeFormat = p14Sector->mmenbTimeFormat;
  pDesc->doubleTransmission = TMWDEFS_FALSE;
  pDesc->pEventsOverflowedFlag = &p14Sector->mmenbEventsOverflowed;
  pDesc->pChangedFunc = _changedFunc;
  pDesc->pGetPointFunc = s14data_mmenbGetPoint;
  pDesc->pEventDataFunc = _eventData;
}

/* function: _storeInResponse */
static void TMWDEFS_CALLBACK _storeInResponse(
  TMWSESN_TX_DATA *pTxData, 
  void *pPoint,
  TMWDEFS_TIME_FORMAT timeFormat)
{
  TMWTYPES_SHORT value;

  if(timeFormat == TMWDEFS_TIME_FORMAT_NONE)
  {
    value = s14data_mmenbGetValue(pPoint);
    tmwtarg_store16((TMWTYPES_USHORT *)&value, &pTxData->pMsgBuf[pTxData->msgLength]);
    pTxData->msgLength += 2;
    pTxData->pMsgBuf[pTxData->msgLength++] = s14data_mmenbGetFlags(pPoint);
  }
  else
  {
    TMWTYPES_UCHAR flags;
    TMWDTIME timeStamp;

    value = s14data_mmenbGetValueFlagsTime(pPoint, &flags, &timeStamp);
    tmwtarg_store16((TMWTYPES_USHORT *)&value, &pTxData->pMsgBuf[pTxData->msgLength]);
    pTxData->msgLength += 2;

    pTxData->pMsgBuf[pTxData->msgLength++] = flags;

    if(timeFormat == TMWDEFS_TIME_FORMAT_24)
    {
      i870util_write24BitTime(pTxData, &timeStamp, S14DATA_SUPPORT_GENUINE_TIME);
    }
    else if(timeFormat == TMWDEFS_TIME_FORMAT_56)
    {
      i870util_write56BitTime(pTxData, &timeStamp, S14DATA_SUPPORT_GENUINE_TIME);
    }
  }
}

/* function: s14mmenb_init */
void TMWDEFS_GLOBAL s14mmenb_init(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  tmwdlist_initialize(&p14Sector->mmenbEvents);
  p14Sector->mmenbEventsOverflowed = TMWDEFS_FALSE;
}

/* function: s14mmenb_close */
void TMWDEFS_GLOBAL s14mmenb_close(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  tmwdlist_destroy(&p14Sector->mmenbEvents, s14mem_free);
} 

/* function: s14mmenb_addEvent */
void * TMWDEFS_GLOBAL s14mmenb_addEvent(
  TMWSCTR *pSector, 
  TMWTYPES_UCHAR cot,
  TMWTYPES_ULONG ioa, 
  TMWTYPES_SHORT value, 
  TMWTYPES_UCHAR quality, 
  TMWDTIME *pTimeStamp)
{
  void *pEvent;
#if TMWCNFG_SUPPORT_THREADS
  TMWDEFS_RESOURCE_LOCK *pLock = &pSector->pSession->pChannel->lock;
#endif

  TMWTARG_LOCK_SECTION(pLock);

  pEvent = _internalAddEvent(pSector, cot, ioa, value, quality, pTimeStamp);

  if(pEvent != TMWDEFS_NULL)
  {
    /* If an event was added tell link layer we have data */ 
    s14event_linkDataReady(pSector);
  }

  TMWTARG_UNLOCK_SECTION(pLock);
  return(pEvent);
}

/* function: s14mmenb_countEvents */
TMWTYPES_USHORT TMWDEFS_GLOBAL s14mmenb_countEvents(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  return(s14evnt_countEvents(pSector, &p14Sector->mmenbEvents));
}

/* function: s14mmenb_scanForChanges */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14mmenb_scanForChanges(
  TMWSCTR *pSector)
{
   S14EVNT_DESC desc;

  _initEventDesc(pSector, &desc);

  return(s14evnt_scanForChanges(pSector, &desc));
}

/* function: s14mmenb_processEvents */
S14EVNT_STATUS TMWDEFS_GLOBAL s14mmenb_processEvents(
  TMWSCTR *pSector,
  TMWDTIME *pEventTime)
{
  S14EVNT_DESC desc;
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  TMWTYPES_UCHAR typeId = I14DEF_TYPE_MMENB1;

  if(p14Sector->mmenbTimeFormat == TMWDEFS_TIME_FORMAT_24) 
  {
    typeId = I14DEF_TYPE_MMETB1;
  }
  else if(p14Sector->mmenbTimeFormat == TMWDEFS_TIME_FORMAT_56) 
  {
    typeId = I14DEF_TYPE_MMETE1;
  }

  _initEventDesc(pSector, &desc);
  desc.typeId = typeId;
  return(s14evnt_processEvents(pSector, &desc, 3, pEventTime, TMWDEFS_FALSE));
}

#if S14DATA_SUPPORT_DOUBLE_TRANS
/* function: s14mmenb_processDblTransEvents */
S14EVNT_STATUS TMWDEFS_GLOBAL s14mmenb_processDblTransEvents(
  TMWSCTR *pSector)
{ 
  S14EVNT_DESC desc;
  _initEventDesc(pSector, &desc);
  return(s14evnt_processEvents(pSector, &desc, 3, TMWDEFS_NULL, TMWDEFS_TRUE));
}
#endif
  
/* function: s14mmenb_readIntoResponse */
S14DBAS_GROUP_STATUS TMWDEFS_CALLBACK s14mmenb_readIntoResponse(
  TMWSESN_TX_DATA *pTxData, 
  TMWSCTR *pSector, 
  TMWTYPES_UCHAR cot, 
  TMWDEFS_GROUP_MASK groupMask, 
  TMWTYPES_ULONG ioa,
  TMWTYPES_USHORT *pPointIndex)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  TMWDEFS_TIME_FORMAT timeFormat = TMWDEFS_TIME_FORMAT_NONE;
 
  /* if this was a read CRDNA, then return the correct time variation 
   * Spec says for background, cyclic or CICNA always send without time variation
   * Some customers require Time Variation in response to CICNA, so allow that.
   */
  if(groupMask == TMWDEFS_GROUP_MASK_ANY)
  {
    /* CRDNA */
    timeFormat = p14Sector->readMsrndTimeFormat;
  }
  else if(groupMask < TMWDEFS_GROUP_MASK_CYCLIC)
  {
    /* CICNA */
    timeFormat = p14Sector->cicnaTimeFormat;
  } 

  return(s14util_readIntoResponse(pTxData, pSector, cot, 
    groupMask, ioa, I14DEF_TYPE_MMENB1, timeFormat, 3, pPointIndex, s14data_mmenbGetPoint, 
    s14data_mmenbGetGroupMask, s14data_mmenbGetInfoObjAddr, s14data_mmenbGetIndexed, s14data_getTimeFormat,
    _storeInResponse));
}

#endif /* S14DATA_SUPPORT_MME_B */
