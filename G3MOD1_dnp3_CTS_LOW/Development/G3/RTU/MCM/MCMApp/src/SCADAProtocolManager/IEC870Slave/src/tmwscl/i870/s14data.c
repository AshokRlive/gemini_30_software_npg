/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14data.h
 * description: This file defines the interface between the Triangle
 *  MicroWorks, Inc. IEC 60870-5-101/104 slave source code library and
 *  the target database. The default implementation calls methods in
 *  the IEC 60870-5-101/104 Slave Database simulator. These need to be
 *  replaced with code to interface with the device's database.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/utils/tmwtarg.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/s14sim.h"
#include "tmwscl/i870/s14rbe.h"

#if TMWCNFG_USE_MANAGED_SCL
#undef TMWCNFG_USE_SIMULATED_DB
#define TMWCNFG_USE_SIMULATED_DB TMWDEFS_FALSE
#endif

#if TMWCNFG_USE_MANAGED_SCL
#include "tmwscl/.NET/TMW.SCL/S14DataBaseWrapper.h"
#endif

/* XXX Changed by Lucy */
#include "tmwscl/i870/lucy/lucy_s14data.h"
/* TODO: pueyos_a - enable S101 debug */
#include "S101Debug.h"
#include "S104Debug.h"
//mdpGetPoint
//mmenaGetPoint
//mmenaGetFlags
//mmenaGetValue
//mmenbGetPoint
//mmenbGetFlags
//mmenbGetValue
//mmencGetPoint
//mmencGetFlags
//mmencGetValue
//mitGetPoint
//mitGetFlags
//mitGetValue
//mitFreeze
//cscSelect
//cscExecute
//cdcSelect
//cdcExecute

/* function: s14data_init */
void * TMWDEFS_GLOBAL s14data_init(
  TMWSCTR *pSector,
  void *pUserHandle)
{
#if TMWCNFG_USE_SIMULATED_DB
  TMWTARG_UNUSED_PARAM(pUserHandle);
  return(s14sim_init(pSector));
#elif TMWCNFG_USE_MANAGED_SCL
  return (S14DatabaseWrapper_Init(pSector, pUserHandle));
#else
  /* Put target code here */
  /* XXX Changed by Lucy */
  TMWTARG_UNUSED_PARAM(pSector);
  DBG_INFO("s14data_init");
  return (pUserHandle);

#endif
}

/* function: s14data_close */
void TMWDEFS_GLOBAL s14data_close(
  void *pHandle)
{
#if TMWCNFG_USE_SIMULATED_DB
  s14sim_close(pHandle);
#elif TMWCNFG_USE_MANAGED_SCL
  return (S14DatabaseWrapper_Close(pHandle));
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
#endif
} 

/* function: s14data_hasSectorReset */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_hasSectorReset(
  void *pHandle,
  TMWTYPES_UCHAR *pResetCOI)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_hasSectorReset(pHandle, pResetCOI));
#elif TMWCNFG_USE_MANAGED_SCL
  return(S14DatabaseWrapper_HasSectorReset(pHandle, pResetCOI));
#else
  /* XXX Changed by Lucy */
  if (pHandle != NULL)
  {
      I870DBHandleStr* db= (I870DBHandleStr*) pHandle;
      return (db->hasSectorReset(db->pIEC870DB, pResetCOI));
  }
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_clearSectorReset */
void TMWDEFS_GLOBAL s14data_clearSectorReset(
  void *pHandle)
{
#if TMWCNFG_USE_SIMULATED_DB
  s14sim_clearSectorReset(pHandle);
#elif TMWCNFG_USE_MANAGED_SCL
  S14DatabaseWrapper_ClearSectorReset(pHandle);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
#endif
}

/* function: s14data_dataReady */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_dataReady(
  void *pHandle,
  TMWTYPES_UCHAR type)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_dataReady(pHandle, type));
#elif TMWCNFG_USE_MANAGED_SCL
  return(S14DatabaseWrapper_DataReady(pHandle, type));
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(type);
  return(TMWDEFS_TRUE);
#endif
}

/* function: s14data_ccsnaSetTime */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_ccsnaSetTime(
  void *pHandle,
  TMWDTIME *pNewTime)
{
  /* Default behavior is to set the clock. Target implementations
   * may only want 1 sector/session on a device to set the time. 
   * Otherwise multiple masters might set the time differently.
   *
   * Another use for this would be multiple masters in separate
   * time zones requiring that time to be kept per sector. The 
   * recommended method however, is for all devices to use GMT.
   *
   *  this function should return
   *  TMWDEFS_TRUE if you want ACT CON positive to be sent back to the master
   *  TMWDEFS_FALSE if you want ACT CON negative to be sent back to the master
   */
#if TMWCNFG_USE_SIMULATED_DB
  TMWTARG_UNUSED_PARAM(pHandle);
  return tmwtarg_setDateTime(pNewTime);
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CcsnaSetTime(pHandle, pNewTime);
#else
  /* XXX Changed by Lucy */
  DBG_INFO("s14data_ccsnaSetTime");
  if (pHandle != NULL)
  {
      I870DBHandleStr* db= (I870DBHandleStr*) pHandle;
      return db->ccsnaSetTime(pNewTime);
  }
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_ccsnaGetTime */
void s14data_ccsnaGetTime(
  void *pHandle,
  TMWDTIME *pTime)
{
  /* Default behavior is to get the target clock time. If separate clocks
   * per sector must be maintained, this implementation may be modified to
   * to do so.
   */
#if TMWCNFG_USE_SIMULATED_DB
  TMWTARG_UNUSED_PARAM(pHandle);
  tmwdtime_getDateTime(TMWDEFS_NULL, pTime);
#elif TMWCNFG_USE_MANAGED_SCL
  S14DatabaseWrapper_CcsnaGetTime(pHandle, pTime);
#else
  /* XXX Changed by Lucy */
  DBG_INFO("s14data_ccsnaGetTime");
  if (pHandle != NULL)
  {
      I870DBHandleStr* db= (I870DBHandleStr*) pHandle;
      db->ccsnaGetTime(pTime);
  }
#endif
}

/* function: s14data_convertQOItoGroupMask */
TMWDEFS_GROUP_MASK TMWDEFS_GLOBAL s14data_convertQOItoGroupMask(
  void *pHandle,
  TMWTYPES_UCHAR qoi)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_convertQOItoGroupMask(pHandle, qoi));
#elif TMWCNFG_USE_MANAGED_SCL
  return(S14DatabaseWrapper_ConvertQOItoGroupMask(pHandle, qoi));
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(qoi);  
  return(0);
#endif
}

/* function: s14data_getTimeFormat */
TMWDEFS_TIME_FORMAT TMWDEFS_CALLBACK s14data_getTimeFormat(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getTimeFormat(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_GetTimeFormat(pPoint);
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
      I870CommonElements* db = (I870CommonElements *)pPoint;
      DBG_INFO("s14data_getTimeFormat: %d",db->timeformat);
      return db->timeformat;
  }
  return(TMWDEFS_TIME_FORMAT_NONE);
#endif
}

#if S14DATA_SUPPORT_MSP
/* Monitored Single Points */
/* function: s14data_mspGetPoint */
void * TMWDEFS_CALLBACK s14data_mspGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mspGetPoint(pHandle, index));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MspGetPoint(pHandle, index);
#else
  /* XXX Changed by Lucy */
  if (pHandle != NULL)
  {
      DBG_INFO("CALLBACK : s14data_mspGetPoint, with index : %d ",index);
      I870DBHandleStr* db= (I870DBHandleStr*) pHandle;
      return db->mspGetPoint(db->pIEC870DB, index);
  }
  return(TMWDEFS_NULL);
#endif
} 

#if S14DATA_SUPPORT_DOUBLE_TRANS
/* function: s14data_mspLookupPoint */
void * TMWDEFS_CALLBACK s14data_mspLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mspLookupPoint(pHandle, ioa));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MspLookupPoint(pHandle, ioa);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);  
  return(TMWDEFS_NULL);
#endif
}
 
S14DATA_TRANSMISSION_MODE TMWDEFS_CALLBACK s14data_mspGetTransmissionMode(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getTransmissionMode(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MspGetTransmissionMode(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_NULL);
#endif
}
#endif

/* function: s14data_mspGetInfoObjAddr */
TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_mspGetInfoObjAddr(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getInfoObjAddr(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MspGetInfoObjAddr(pPoint);
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
      I870DataMSP* msp = (I870DataMSP*) pPoint;
      DBG_INFO("CALLBACK : s14data_mspGetInfoObjAddr for IOA: %lu",msp->commonElements.ioa);
      return msp->commonElements.ioa;
  }
  return(0);
#endif
}

/* function: s14data_mspGetIndexed */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14data_mspGetIndexed(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mspGetIndexed(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MspGetIndexed(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_mspGetGroupMask */
TMWDEFS_GROUP_MASK TMWDEFS_CALLBACK s14data_mspGetGroupMask(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getGroupMask(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MspGetGroupMask(pPoint);
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
      I870DataMSP* msp = (I870DataMSP* )pPoint;
      DBG_INFO("CALLBACK : s14data_mspGetGroupMask for IOA: %lu",msp->commonElements.ioa);
      return msp->commonElements.groupMask;
  }
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_mspGetFlagsAndValue */
TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mspGetFlagsAndValue(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mspGetFlagsAndValue(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MspGetFlagsAndValue(pPoint);
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
      I870DataMSP* msp = (I870DataMSP* )pPoint;
      DBG_INFO("CALLBACK : s14data_mspGetFlagsAndValue for IOA: %lu",msp->commonElements.ioa);
      return msp->value;
  }
  return(TMWDEFS_FALSE);
#endif
}

TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mspGetFlagsValueTime(
  void *pPoint,
  TMWDTIME *pTime)
{
#if TMWCNFG_USE_SIMULATED_DB
  s14sim_getDateTime(pPoint, pTime);
  return(s14sim_mspGetFlagsAndValue(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MspGetFlagsValueTime(pPoint, pTime);
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
      I870DataMSP* msp = (I870DataMSP* )pPoint;
      DBG_INFO("CALLBACK : s14data_mspGetFlagsValueTime for IOA: %lu",msp->commonElements.ioa);
      *pTime=msp->commonElements.dtime;
      return msp->value;
  }
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_mspChanged */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_mspChanged(
  void *pPoint,
  TMWTYPES_UCHAR *pSIQ,
  TMWDEFS_CHANGE_REASON *pReason)
{
#if TMWCNFG_USE_SIMULATED_DB
  if(s14sim_getChanged(pPoint, pReason))
  {
    *pSIQ = s14sim_mspGetFlagsAndValue(pPoint);
    return(TMWDEFS_TRUE);
  }
  return(TMWDEFS_FALSE);
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MspChanged(pPoint, pSIQ, pReason);
#else
//  /* XXX Changed by Lucy */
//    if(pPoint != NULL)
//    {
//        static TMWTYPES_UCHAR previousFlagsValue = 0;
//        I870DataMSP* msp = (I870DataMSP* )pPoint;
//        if(msp->value != previousFlagsValue)
//        {
//          DBG_INFO("CALLBACK : s14data_mspChanged for IOA: %lu",msp->commonElements.ioa);
//          *pSIQ = msp->value;
//          previousFlagsValue = msp->value;
//          *pReason = TMWDEFS_CHANGE_SPONTANEOUS;
//          return TMWDEFS_TRUE;
//        }
//    }
    TMWTARG_UNUSED_PARAM(pPoint);
    TMWTARG_UNUSED_PARAM(pSIQ);
    TMWTARG_UNUSED_PARAM(pReason);
    return(TMWDEFS_FALSE);
#endif
}
#endif

#if S14DATA_SUPPORT_MDP
/* Monitored Double Points */
/* function: s14data_mdpGetPoint */
void * TMWDEFS_CALLBACK s14data_mdpGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mdpGetPoint(pHandle, index));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MdpGetPoint(pHandle, index);
#else
  /* XXX Changed by Lucy */
  if (pHandle != NULL)
  {
      DBG_INFO("CALLBACK : s14data_mdpGetPoint, with index : %d ",index);
      I870DBHandleStr* db= (I870DBHandleStr*) pHandle;
      return db->mdpGetPoint(db->pIEC870DB, index);
  }
  return(TMWDEFS_NULL);
#endif
}

#if S14DATA_SUPPORT_DOUBLE_TRANS
/* function: s14data_mspLookupPoint */
void * TMWDEFS_CALLBACK s14data_mdpLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mdpLookupPoint(pHandle, ioa));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MdpLookupPoint(pHandle, ioa);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);  
  return(TMWDEFS_NULL);
#endif
}  
 
S14DATA_TRANSMISSION_MODE TMWDEFS_CALLBACK s14data_mdpGetTransmissionMode(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getTransmissionMode(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MdpGetTransmissionMode(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_NULL);
#endif
}
#endif

/* function: s14data_mdpGetInfoObjAddr */
TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_mdpGetInfoObjAddr(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getInfoObjAddr(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MdpGetInfoObjAddr(pPoint);
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
      I870DataMDP* mdp = (I870DataMDP*) pPoint;
      DBG_INFO("CALLBACK : s14data_mdpGetInfoObjAddr for IOA: %lu",mdp->commonElements.ioa);
      return mdp->commonElements.ioa;
  }
  return(0);
#endif
}

/* function: s14data_mdpGetGroupMask */
TMWDEFS_GROUP_MASK TMWDEFS_CALLBACK s14data_mdpGetGroupMask(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getGroupMask(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MdpGetGroupMask(pPoint);
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
      I870DataMDP* mdp = (I870DataMDP* )pPoint;
      DBG_INFO("CALLBACK : s14data_mdpGetGroupMask for IOA: %lu",mdp->commonElements.ioa);
      return mdp->commonElements.groupMask;
  }
  return(0);
#endif
}

/* function: s14data_mdpGetIndexed */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14data_mdpGetIndexed(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mdpGetIndexed(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MdpGetIndexed(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_mdpGetFlagsAndValue */
TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mdpGetFlagsAndValue(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mdpGetFlagsAndValue(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MdpGetFlagsAndValue(pPoint);
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
      I870DataMDP* mdp = (I870DataMDP* )pPoint;
      DBG_INFO("CALLBACK : s14data_mdpGetFlagsAndValue for IOA: %lu",mdp->commonElements.ioa);
      return mdp->value;
  }

  return(TMWDEFS_FALSE);
#endif
}

TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mdpGetFlagsValueTime(
  void *pPoint,
  TMWDTIME *pTime)
{
#if TMWCNFG_USE_SIMULATED_DB
  s14sim_getDateTime(pPoint, pTime);
  return(s14sim_mdpGetFlagsAndValue(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  s14sim_getDateTime(pPoint, pTime);
  return S14DatabaseWrapper_MdpGetFlagsValueTime(pPoint, pTime);
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
      I870DataMDP* mdp = (I870DataMDP* )pPoint;
      DBG_INFO("CALLBACK : s14data_mdpGetFlagsValueTime for IOA: %lu",mdp->commonElements.ioa);
      *pTime=mdp->commonElements.dtime;
      return mdp->value;
  }
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_mdpChanged */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_mdpChanged(
  void *pPoint,
  TMWTYPES_UCHAR *pDIQ,
  TMWDEFS_CHANGE_REASON *pReason)
{
#if TMWCNFG_USE_SIMULATED_DB
  if(s14sim_getChanged(pPoint, pReason))
  {
    *pDIQ = s14sim_mdpGetFlagsAndValue(pPoint);
    return(TMWDEFS_TRUE);
  }
  return(TMWDEFS_FALSE);
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MdpChanged(pPoint, pDIQ, pReason);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(pDIQ);
  TMWTARG_UNUSED_PARAM(pReason);
  return(TMWDEFS_FALSE);
#endif
}
#endif

#if S14DATA_SUPPORT_MST
/* Step Position Information */
/* function: s14data_mstGetPoint */
void * TMWDEFS_CALLBACK s14data_mstGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mstGetPoint(pHandle, index));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MstGetPoint(pHandle, index);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(index);  
  return(TMWDEFS_NULL);
#endif
}

#if S14DATA_SUPPORT_DOUBLE_TRANS
/* function: s14data_mstLookupPoint */
void * TMWDEFS_CALLBACK s14data_mstLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mstLookupPoint(pHandle, ioa));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MstLookupPoint(pHandle, ioa);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);  
  return(TMWDEFS_NULL);
#endif
}
 
S14DATA_TRANSMISSION_MODE TMWDEFS_CALLBACK s14data_mstGetTransmissionMode(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getTransmissionMode(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MstGetTransmissionMode(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_NULL);
#endif
}
#endif

/* function: s14data_mstGetInfoObjAddr */
TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_mstGetInfoObjAddr(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getInfoObjAddr(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MstGetInfoObjAddr(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_mstGetGroupMask */
TMWDEFS_GROUP_MASK TMWDEFS_CALLBACK s14data_mstGetGroupMask(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getGroupMask(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MstGetGroupMask(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_mstGetIndexed */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14data_mstGetIndexed(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mstGetIndexed(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MstGetIndexed(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_mstGetFlags */
TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mstGetFlags(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getFlags(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MstGetFlags(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_mstGetValueAndState */
TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mstGetValueAndState(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mstGetValue(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MstGetValueAndState(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
} 

/* function: s14data_mstGetValueStateFlagsTime */
TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mstGetValueStateFlagsTime(
  void *pPoint,
  TMWTYPES_UCHAR *pFlags,
  TMWDTIME *pTime)
{
#if TMWCNFG_USE_SIMULATED_DB
  *pFlags = s14sim_getFlags(pPoint);
  s14sim_getDateTime(pPoint, pTime);
  return(s14sim_mstGetValue(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MstGetValueStateFlagsTime(pPoint, pFlags, pTime);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(pFlags);
  TMWTARG_UNUSED_PARAM(pTime);
  return(0);
#endif
}

/* function: s14data_mstChanged */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_mstChanged(
  void *pPoint,
  TMWTYPES_UCHAR *pVTI,
  TMWTYPES_UCHAR *pQDS,
  TMWDEFS_CHANGE_REASON *pReason)
{
#if TMWCNFG_USE_SIMULATED_DB
  if(s14sim_getChanged(pPoint, pReason))
  {
    *pVTI = s14sim_mstGetValue(pPoint);
    *pQDS = s14sim_getFlags(pPoint);
    return(TMWDEFS_TRUE);
  }

  return(TMWDEFS_FALSE);
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MstChanged(pPoint, pVTI, pQDS, pReason);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(pVTI);
  TMWTARG_UNUSED_PARAM(pQDS);
  TMWTARG_UNUSED_PARAM(pReason);
  return(TMWDEFS_FALSE);
#endif
}
#endif

#if S14DATA_SUPPORT_MBO
/* Bitstring of 32 bit */
/* function: s14data_mboGetPoint */
void * TMWDEFS_CALLBACK s14data_mboGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mboGetPoint(pHandle, index));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MboGetPoint(pHandle, index);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(index);  
  return(TMWDEFS_NULL);
#endif
}

#if S14DATA_SUPPORT_DOUBLE_TRANS
/* function: s14data_mboLookupPoint */
void * TMWDEFS_CALLBACK s14data_mboLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mboLookupPoint(pHandle, ioa));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MboLookupPoint(pHandle, ioa);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);  
  return(TMWDEFS_NULL);
#endif
} 
 
S14DATA_TRANSMISSION_MODE TMWDEFS_CALLBACK s14data_mboGetTransmissionMode(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getTransmissionMode(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MboGetTransmissionMode(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_NULL);
#endif
}
#endif 

/* function: s14data_mboGetInfoObjAddr */
TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_mboGetInfoObjAddr(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getInfoObjAddr(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MboGetInfoObjAddr(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_mboGetGroupMask */
TMWDEFS_GROUP_MASK TMWDEFS_CALLBACK s14data_mboGetGroupMask(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getGroupMask(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MboGetGroupMask(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_mboGetIndexed */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14data_mboGetIndexed(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mboGetIndexed(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MboGetIndexed(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_mboGetFlags */
TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mboGetFlags(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getFlags(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MboGetFlags(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_mboGetValue */
TMWTYPES_ULONG TMWDEFS_GLOBAL s14data_mboGetValue(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mboGetValue(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MboGetValue(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_mboGetValueFlagsTime */
TMWTYPES_ULONG TMWDEFS_GLOBAL s14data_mboGetValueFlagsTime(
  void *pPoint,
  TMWTYPES_UCHAR *pFlags,
  TMWDTIME *pTime)
{
#if TMWCNFG_USE_SIMULATED_DB
  s14sim_getDateTime(pPoint, pTime);
  *pFlags = s14sim_getFlags(pPoint);
  return(s14sim_mboGetValue(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MboGetValueFlagsTime(pPoint, pFlags, pTime);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(pFlags);
  TMWTARG_UNUSED_PARAM(pTime);
  return(0);
#endif
}

/* function: s14data_mboChanged */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_mboChanged(
  void *pPoint,
  TMWTYPES_ULONG *pBSI,
  TMWTYPES_UCHAR *pQDS,
  TMWDEFS_CHANGE_REASON *pReason)
{
#if TMWCNFG_USE_SIMULATED_DB
  if(s14sim_getChanged(pPoint, pReason))
  {
    *pBSI = s14sim_mboGetValue(pPoint);
    *pQDS = s14sim_getFlags(pPoint);
    return(TMWDEFS_TRUE);
  }
  return(TMWDEFS_FALSE);
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MboChanged(pPoint, pBSI, pQDS, pReason);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(pBSI);
  TMWTARG_UNUSED_PARAM(pQDS);
  TMWTARG_UNUSED_PARAM(pReason);
  return(TMWDEFS_FALSE);
#endif
}
#endif

#if S14DATA_SUPPORT_MME_A
/* Normalized Measurands */
/* function: s14data_mmenaGetPoint */
void * TMWDEFS_CALLBACK s14data_mmenaGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mmenaGetPoint(pHandle, index));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmenaGetPoint(pHandle, index);
#else
  /* XXX Changed by Lucy */
  if (pHandle != NULL)
  {
      DBG_INFO("CALLBACK : s14data_mmenaGetPoint, with index : %d ",index);
      I870DBHandleStr* db= (I870DBHandleStr*) pHandle;
      return db->mmenaGetPoint(db->pIEC870DB, index);
  }
  return(TMWDEFS_NULL);
#endif
}

#if S14DATA_SUPPORT_DOUBLE_TRANS
/* function: s14data_mmenaLookupPoint */
void * TMWDEFS_CALLBACK s14data_mmenaLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mmenaLookupPoint(pHandle, ioa));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmenaLookupPoint(pHandle, ioa);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);  
  return(TMWDEFS_NULL);
#endif
}

S14DATA_TRANSMISSION_MODE TMWDEFS_CALLBACK s14data_mmenaGetTransmissionMode(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getTransmissionMode(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmenaGetTransmissionMode(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_NULL);
#endif
}
#endif 

/* function: s14data_mmenaGetInfoObjAddr */
TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_mmenaGetInfoObjAddr(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getInfoObjAddr(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmenaGetInfoObjAddr(pPoint);
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
      I870DataAnalog* mmenap = (I870DataAnalog*) pPoint;
      DBG_INFO("CALLBACK : s14data_mmenaGetInfoObjAddr for IOA: %lu",mmenap->commonElements.ioa);
      return mmenap->commonElements.ioa;
  }
  return(0);
#endif
}

/* function: s14data_mmenaGetGroupMask */
TMWDEFS_GROUP_MASK TMWDEFS_CALLBACK s14data_mmenaGetGroupMask(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getGroupMask(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmenaGetGroupMask(pPoint);
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
      I870DataAnalog* mmenap = (I870DataAnalog* )pPoint;
      DBG_INFO("CALLBACK : s14data_mmenaGetGroupMask for IOA: %lu",mmenap->commonElements.ioa);
      return mmenap->commonElements.groupMask;
  }
  return(0);
#endif
}

/* function: s14data_mmenaGetIndexed() */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14data_mmenaGetIndexed(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mmenaGetIndexed(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmenaGetIndexed(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_mmenaGetFlags */
TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mmenaGetFlags(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getFlags(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmenaGetFlags(pPoint);
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
      I870DataAnalog* mmena = (I870DataAnalog* )pPoint;
      DBG_INFO("CALLBACK : s14data_mmenaGetFlags for IOA: %lu",mmena->commonElements.ioa);
      return mmena->commonElements.flags;
  }
  return(0);
#endif
}

/* function: s14data_mmenaGetValue */
TMWTYPES_SHORT TMWDEFS_GLOBAL s14data_mmenaGetValue(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mmenaGetValue(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmenaGetValue(pPoint);
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
      I870DataAnalog* mmena = (I870DataAnalog* )pPoint;
      DBG_INFO("CALLBACK : s14data_mmenaGetValue for IOA: %lu",mmena->commonElements.ioa);
      return mmena->value.value.sval;
  }
  return(0);
#endif
}

/* function: s14data_mmenaGetValueFlagsTime */
TMWTYPES_SHORT TMWDEFS_GLOBAL s14data_mmenaGetValueFlagsTime(
  void *pPoint,
  TMWTYPES_UCHAR *pFlags,
  TMWDTIME *pTime)
{
#if TMWCNFG_USE_SIMULATED_DB
  *pFlags = s14sim_getFlags(pPoint);
  s14sim_getDateTime(pPoint, pTime);
  return(s14sim_mmenaGetValue(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmenaGetValueFlagsTime(pPoint, pFlags, pTime);
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
      I870DataAnalog* mmena = (I870DataAnalog* )pPoint;
      DBG_INFO("CALLBACK : s14data_mmenaGetValueFlagsTime for IOA: %lu",mmena->commonElements.ioa);
      *pTime = mmena->commonElements.dtime;
      *pFlags = mmena->commonElements.flags;
      return mmena->value.value.sval;
  }
  return(0);
#endif
}

/* function: s14data_mmenaChanged */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_mmenaChanged(
  void *pPoint,
  TMWTYPES_SHORT *pNVA,
  TMWTYPES_UCHAR *pQDS,
  TMWDEFS_CHANGE_REASON *pReason)
{
#if TMWCNFG_USE_SIMULATED_DB
  if(s14sim_getChanged(pPoint, pReason))
  {
    *pNVA = s14sim_mmenaGetValue(pPoint);
    *pQDS = s14sim_getFlags(pPoint);
    return(TMWDEFS_TRUE);
  }

  return(TMWDEFS_FALSE);
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmenaChanged(pPoint, pNVA, pQDS, pReason);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(pNVA);
  TMWTARG_UNUSED_PARAM(pQDS);
  TMWTARG_UNUSED_PARAM(pReason);
  return(TMWDEFS_FALSE);
#endif
}
#endif

#if S14DATA_SUPPORT_MME_B
/* Scaled Measurands */
/* function: s14data_mmenbGetPoint */
void * TMWDEFS_CALLBACK s14data_mmenbGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mmenbGetPoint(pHandle, index));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmenbGetPoint(pHandle, index);
#else
  /* XXX Changed by Lucy */
  if (pHandle != NULL)
  {
      DBG_INFO("CALLBACK : s14data_mmenbGetPoint, with index : %d ",index);
      I870DBHandleStr* db= (I870DBHandleStr*) pHandle;
      return db->mmenbGetPoint(db->pIEC870DB, index);
  }
  return(TMWDEFS_NULL);
#endif
}

#if S14DATA_SUPPORT_DOUBLE_TRANS
/* function: s14data_mmenbLookupPoint */
void * TMWDEFS_CALLBACK s14data_mmenbLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mmenbLookupPoint(pHandle, ioa));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmenbLookupPoint(pHandle, ioa);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);  
  return(TMWDEFS_NULL);
#endif
}

S14DATA_TRANSMISSION_MODE TMWDEFS_CALLBACK s14data_mmenbGetTransmissionMode(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getTransmissionMode(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmenbGetTransmissionMode(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_NULL);
#endif
}
#endif

/* function: s14data_mmenbGetInfoObjAddr */
TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_mmenbGetInfoObjAddr(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getInfoObjAddr(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmenbGetInfoObjAddr(pPoint);
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
      I870DataAnalog* mmenbp = (I870DataAnalog*) pPoint;
      DBG_INFO("CALLBACK : s14data_mmenbGetInfoObjAddr for IOA: %lu",mmenbp->commonElements.ioa);
      return mmenbp->commonElements.ioa;
  }
  return(0);
#endif
}

/* function: s14data_mmenbGetGroupMask */
TMWDEFS_GROUP_MASK TMWDEFS_CALLBACK s14data_mmenbGetGroupMask(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getGroupMask(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmenbGetGroupMask(pPoint);
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
      I870DataAnalog* mmenbp = (I870DataAnalog* )pPoint;
      DBG_INFO("CALLBACK : s14data_mmenbGetGroupMask for IOA: %lu",mmenbp->commonElements.ioa);
      return mmenbp->commonElements.groupMask;
  }
  return(0);
#endif
}

/* function: s14data_mmenbGetIndexed() */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14data_mmenbGetIndexed(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mmenbGetIndexed(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmenbGetIndexed(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_mmenbGetFlags */
TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mmenbGetFlags(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getFlags(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmenbGetFlags(pPoint);
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
      I870DataAnalog* mmenb = (I870DataAnalog* )pPoint;
      DBG_INFO("CALLBACK : s14data_mmenbGetFlags for IOA: %lu",mmenb->commonElements.ioa);
      return mmenb->commonElements.flags;
  }
  return(0);
#endif
}

/* function: s14data_mmenbGetValue */
TMWTYPES_SHORT TMWDEFS_GLOBAL s14data_mmenbGetValue(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mmenbGetValue(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmenbGetValue(pPoint);
#else
  /* XXX Changed by Lucy */
  if(pPoint != NULL)
  {
      I870DataAnalog* mmenb = (I870DataAnalog* )pPoint;
      DBG_INFO("CALLBACK : s14data_mmenbGetValue for IOA: %lu",mmenb->commonElements.ioa);
      return mmenb->value.value.sval;
  }
  return(0);
#endif
}

TMWTYPES_SHORT TMWDEFS_GLOBAL s14data_mmenbGetValueFlagsTime(
  void *pPoint,
  TMWTYPES_UCHAR *pFlags,
  TMWDTIME *pTime)
{
#if TMWCNFG_USE_SIMULATED_DB
  *pFlags = s14sim_getFlags(pPoint);
  s14sim_getDateTime(pPoint, pTime);
  return(s14sim_mmenbGetValue(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmenbGetValueFlagsTime(pPoint, pFlags, pTime);
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
      I870DataAnalog* mmenb = (I870DataAnalog* )pPoint;
      DBG_INFO("CALLBACK : s14data_mmenbGetValueFlagsTime for IOA: %lu",mmenb->commonElements.ioa);
      *pTime = mmenb->commonElements.dtime;
      *pFlags = mmenb->commonElements.flags;
      return mmenb->value.value.sval;
  }
  return(0);
#endif
}

/* function: s14data_mmenbChanged */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_mmenbChanged(
  void *pPoint,
  TMWTYPES_SHORT *pSVA,
  TMWTYPES_UCHAR *pQDS,
  TMWDEFS_CHANGE_REASON *pReason)
{
#if TMWCNFG_USE_SIMULATED_DB
  if(s14sim_getChanged(pPoint, pReason))
  {
    *pSVA = s14sim_mmenbGetValue(pPoint);
    *pQDS = s14sim_getFlags(pPoint);
    return(TMWDEFS_TRUE);
  }

  return(TMWDEFS_FALSE);
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmenbChanged(pPoint, pSVA, pQDS, pReason);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(pSVA);
  TMWTARG_UNUSED_PARAM(pQDS);
  TMWTARG_UNUSED_PARAM(pReason);
  return(TMWDEFS_FALSE);
#endif
}
#endif

#if S14DATA_SUPPORT_MME_C
/* Floating Point Measurands */
/* function: s14data_mmencGetPoint */
void * TMWDEFS_CALLBACK s14data_mmencGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mmencGetPoint(pHandle, index));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmencGetPoint(pHandle, index);
#else
  /* XXX Changed by Lucy */
  if (pHandle != NULL)
  {
      DBG_INFO("CALLBACK : s14data_mmencGetPoint, with index : %d ",index);
      I870DBHandleStr* db= (I870DBHandleStr*) pHandle;
      return db->mmencGetPoint(db->pIEC870DB, index);
  }
  return(TMWDEFS_NULL);
#endif
}

#if S14DATA_SUPPORT_DOUBLE_TRANS
/* function: s14data_mmencLookupPoint */
void * TMWDEFS_CALLBACK s14data_mmencLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mmencLookupPoint(pHandle, ioa));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmencLookupPoint(pHandle, ioa);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);  
  return(TMWDEFS_NULL);
#endif
}

S14DATA_TRANSMISSION_MODE TMWDEFS_CALLBACK s14data_mmencGetTransmissionMode(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getTransmissionMode(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmencGetTransmissionMode(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_NULL);
#endif
}
#endif

/* function: s14data_mmencGetInfoObjAddr */
TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_mmencGetInfoObjAddr(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getInfoObjAddr(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmencGetInfoObjAddr(pPoint);
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
      I870DataAnalog* mmencp = (I870DataAnalog*) pPoint;
      DBG_INFO("CALLBACK : s14data_mmencGetInfoObjAddr for IOA: %lu",mmencp->commonElements.ioa);
      return mmencp->commonElements.ioa;
  }
  return(0);
#endif
}

/* function: s14data_mmencGetGroupMask */
TMWDEFS_GROUP_MASK TMWDEFS_CALLBACK s14data_mmencGetGroupMask(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getGroupMask(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmencGetGroupMask(pPoint);
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
      I870DataAnalog* mmenc = (I870DataAnalog* )pPoint;
      DBG_INFO("CALLBACK : s14data_mmencGetGroupMask for IOA: %lu",mmenc->commonElements.ioa);
      return mmenc->commonElements.groupMask;
  }
  return(0);
#endif
}

/* function: s14data_mmencGetIndexed */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14data_mmencGetIndexed(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mmencGetIndexed(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmencGetIndexed(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_mmencGetFlags */
TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mmencGetFlags(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getFlags(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmencGetFlags(pPoint);
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
      I870DataAnalog* mmenc = (I870DataAnalog* )pPoint;
      DBG_INFO("CALLBACK : s14data_mmencGetFlags for IOA: %lu",mmenc->commonElements.ioa);
      return mmenc->commonElements.flags;
  }
  return(0);
#endif
}

/* function: s14data_mmencGetValue */
TMWTYPES_SFLOAT TMWDEFS_GLOBAL s14data_mmencGetValue(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mmencGetValue(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmencGetValue(pPoint);
#else
  /* XXX Changed by Lucy */
  if(pPoint != NULL)
  {
      I870DataAnalog* mmenc = (I870DataAnalog* )pPoint;
      DBG_INFO("CALLBACK : s14data_mmencGetValue for IOA: %lu",mmenc->commonElements.ioa);
      return mmenc->value.value.fval;
  }
  return(0);
#endif
}

TMWTYPES_SFLOAT TMWDEFS_GLOBAL s14data_mmencGetValueFlagsTime(
  void *pPoint,
  TMWTYPES_UCHAR *pFlags,
  TMWDTIME *pTime)
{
#if TMWCNFG_USE_SIMULATED_DB
  *pFlags = s14sim_getFlags(pPoint);
  s14sim_getDateTime(pPoint, pTime);
  return(s14sim_mmencGetValue(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmencGetValueFlagsTime(pPoint, pFlags, pTime);
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
      I870DataAnalog* mmenc = (I870DataAnalog* )pPoint;
      DBG_INFO("CALLBACK : s14data_mmencGetValueFlagsTime for IOA: %lu",mmenc->commonElements.ioa);
      *pFlags=mmenc->commonElements.flags;
      *pTime=mmenc->commonElements.dtime;
      return mmenc->value.value.fval;
  }
  return(0);
#endif
}

/* function: s14data_mmencChanged */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_mmencChanged(
  void *pPoint,
  TMWTYPES_SFLOAT *pFVA,
  TMWTYPES_UCHAR *pQDS,
  TMWDEFS_CHANGE_REASON *pReason)
{
#if TMWCNFG_USE_SIMULATED_DB
  if(s14sim_getChanged(pPoint, pReason))
  {
    *pFVA = s14sim_mmencGetValue(pPoint);
    *pQDS = s14sim_getFlags(pPoint);
    return(TMWDEFS_TRUE);
  }

  return(TMWDEFS_FALSE);
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmencChanged(pPoint, pFVA, pQDS, pReason);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(pFVA);
  TMWTARG_UNUSED_PARAM(pQDS);
  TMWTARG_UNUSED_PARAM(pReason);
  return(TMWDEFS_FALSE);
#endif
}
#endif

#if S14DATA_SUPPORT_MIT
/* Integrated Totals */
/* function: s14data_mitGetPoint */
void * TMWDEFS_CALLBACK s14data_mitGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mitGetPoint(pHandle, index));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MitGetPoint(pHandle, index);
#else
  /* XXX Changed by Lucy */
  DBG_INFO("CALLBACK : s14data_mitGetPoint with index : %d",index);
  if (pHandle != NULL)
  {
      I870DBHandleStr* db= (I870DBHandleStr*) pHandle;
      return db->mitGetPoint(db->pIEC870DB, index);
  }
  return(TMWDEFS_NULL);
#endif
}

#if S14DATA_SUPPORT_DOUBLE_TRANS
/* function: s14data_mitLookupPoint */
void * TMWDEFS_CALLBACK s14data_mitLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mitLookupPoint(pHandle, ioa));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MitLookupPoint(pHandle, ioa);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);  
  return(TMWDEFS_NULL);
#endif
}

S14DATA_TRANSMISSION_MODE TMWDEFS_CALLBACK s14data_mitGetTransmissionMode(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getTransmissionMode(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MitGetTransmissionMode(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_NULL);
#endif
}
#endif

/* function: s14data_mitGetInfoObjAddr */
TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_mitGetInfoObjAddr(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getInfoObjAddr(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MitGetInfoObjAddr(pPoint);
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
      I870DataMIT* mit = (I870DataMIT*) pPoint;
      DBG_INFO("CALLBACK : s14data_mitGetInfoObjAddr for IOA: %lu",mit->commonElements.ioa);
      return mit->commonElements.ioa;
  }
  return(0);
#endif
}

/* function: s14data_mitGetGroupMask */
TMWDEFS_GROUP_MASK TMWDEFS_CALLBACK s14data_mitGetGroupMask(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getGroupMask(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MitGetGroupMask(pPoint);
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
      I870DataMIT* mit = (I870DataMIT* )pPoint;
      DBG_INFO("CALLBACK : s14data_mitGetGroupMask for IOA: %lu",mit->commonElements.ioa);
      return mit->commonElements.groupMask;
  }
  return(0);
#endif
}

/* function: s14data_mitGetIndexed */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14data_mitGetIndexed(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mitGetIndexed(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MitGetIndexed(pPoint);
#else
  /* XXX Changed by Lucy */
  DBG_INFO("s14data_mitGetIndexed");
  
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_mitGetFlags */
TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mitGetFlags(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getFlags(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MitGetFlags(pPoint);
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
      I870DataMIT* mit = (I870DataMIT* )pPoint;
      DBG_INFO("CALLBACK : s14data_mitGetFlags for IOA: %lu",mit->commonElements.ioa);
      return mit->commonElements.flags;
  }
  return(0);
#endif
}

/* function: s14data_mitGetValue */
TMWTYPES_ULONG TMWDEFS_GLOBAL s14data_mitGetValue(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mitGetFrozenValue(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MitGetValue(pPoint);
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
      I870DataMIT* mit = (I870DataMIT* )pPoint;
      DBG_INFO("CALLBACK : s14data_mitGetValue for IOA: %lu",mit->commonElements.ioa);
      return mit->value;
  }
  return(0);
#endif
}

TMWTYPES_ULONG TMWDEFS_GLOBAL s14data_mitGetValueFlagsTime(
  void *pPoint,
  TMWTYPES_UCHAR *pFlags,
  TMWDTIME *pTime)
{
#if TMWCNFG_USE_SIMULATED_DB
  *pFlags = s14sim_getFlags(pPoint);
  s14sim_getDateTime(pPoint, pTime);
  return(s14sim_mitGetFrozenValue(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MitGetValueFlagsTime(pPoint, pFlags, pTime);
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
      I870DataMIT* mit = (I870DataMIT* )pPoint;
      DBG_INFO("CALLBACK : s14data_mitGetValueFlagsTime for IOA: %lu",mit->commonElements.ioa);
      *pTime = mit->commonElements.dtime;
      *pFlags = mit->commonElements.flags;
      return mit->value;
  }
  return(0);
#endif
}

/* function: s14data_mitFreeze */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_mitFreeze(
  void *pPoint,
  TMWTYPES_UCHAR qcc)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mitFreeze(pPoint, qcc));
#elif TMWCNFG_USE_MANAGED_SCL
  return(S14DatabaseWrapper_MitFreeze(pPoint, qcc));
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
      DBG_INFO("CALLBACK : s14data_mitFreeze does %s for IOA: %lu",
              ((qcc & I14DEF_QCC_FRZ_MASK) == I14DEF_QCC_FRZ_RESET)? "Freeze&Clear" :
                   ((qcc & I14DEF_QCC_FRZ_MASK) == I14DEF_QCC_FRZ_NO_RESET) ? "Freeze" :
                       ((qcc & I14DEF_QCC_FRZ_MASK) == I14DEF_QCC_FRZ_RESET_ONLY) ? "Clear" :
                                                                                    "Read",
              ((I870DataMIT*)pPoint)->commonElements.ioa
              );

      I870DataMIT* mit = (I870DataMIT*)pPoint;
      if (mit->mitFreeze != NULL)
      {
          return(mit->mitFreeze(pPoint, qcc));  //This includes "clear only" operation
      }
  }
  return(TMWDEFS_FALSE);
#endif
}
#endif

#if S14DATA_SUPPORT_MITC
/* Integrated BCD Totals */
/* function: s14data_mitcGetPoint */
void * TMWDEFS_CALLBACK s14data_mitcGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mitcGetPoint(pHandle, index));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MitcGetPoint(pHandle, index);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(index);
  return(TMWDEFS_NULL);
#endif
}

#if S14DATA_SUPPORT_DOUBLE_TRANS
/* function: s14data_mitcLookupPoint */
void * TMWDEFS_CALLBACK s14data_mitcLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mitcLookupPoint(pHandle, ioa));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MitcLookupPoint(pHandle, ioa);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);  
  return(TMWDEFS_NULL);
#endif
}

S14DATA_TRANSMISSION_MODE TMWDEFS_CALLBACK s14data_mitcGetTransmissionMode(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getTransmissionMode(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MitcGetTransmissionMode(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_NULL);
#endif
}
#endif

/* function: s14data_mitcGetInfoObjAddr */
TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_mitcGetInfoObjAddr(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getInfoObjAddr(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MitcGetInfoObjAddr(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_mitcGetGroupMask */
TMWDEFS_GROUP_MASK TMWDEFS_CALLBACK s14data_mitcGetGroupMask(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getGroupMask(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MitcGetGroupMask(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_mitcGetFlags */
TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mitcGetFlags(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getFlags(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MitcGetFlags(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_mitGetValue */
void TMWDEFS_GLOBAL s14data_mitcGetValue(
  void *pPoint, 
  TMWTYPES_UCHAR *pBCD)
{
#if TMWCNFG_USE_SIMULATED_DB
  s14sim_mitcGetValue(pPoint, pBCD);
#elif TMWCNFG_USE_MANAGED_SCL
  S14DatabaseWrapper_MitcGetValue(pPoint, pBCD);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(pBCD);
#endif
}

void TMWDEFS_GLOBAL s14data_mitcGetValueFlagsTime(
  void *pPoint,
  TMWTYPES_UCHAR *pBCD,
  TMWTYPES_UCHAR *pFlags,
  TMWDTIME *pTime)
{
#if TMWCNFG_USE_SIMULATED_DB
  *pFlags = s14sim_getFlags(pPoint);
  s14sim_getDateTime(pPoint, pTime);
  s14sim_mitcGetValue(pPoint, pBCD);
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MitcGetValueFlagsTime(pPoint, pBCD, pFlags, pTime);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(pBCD);
  TMWTARG_UNUSED_PARAM(pFlags);
  TMWTARG_UNUSED_PARAM(pTime);
#endif
}

#endif


#if S14DATA_SUPPORT_MEPTA
/* Event of protection equipment with time tag */
void * TMWDEFS_CALLBACK s14data_meptaGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_meptaGetPoint(pHandle, index));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MeptaGetPoint(pHandle, index);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(index);
  return(TMWDEFS_NULL);
#endif
}

TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_meptaGetInfoObjAddr(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getInfoObjAddr(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MeptaGetInfoObjAddr(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_meptaChanged(
  void *pPoint,
  TMWTYPES_UCHAR *pSEP,
  TMWTYPES_USHORT *pElapsedTime)
{
#if TMWCNFG_USE_SIMULATED_DB
  TMWDEFS_CHANGE_REASON reason;

  if(s14sim_getChanged(pPoint, &reason))
  {
    s14sim_meptaGetValue(pPoint, pSEP, pElapsedTime);
    return(TMWDEFS_TRUE);
  }
  return(TMWDEFS_FALSE);
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MeptaChanged(pPoint, pSEP, pElapsedTime);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(pSEP);
  TMWTARG_UNUSED_PARAM(pElapsedTime);
  return(TMWDEFS_FALSE);
#endif
}
#endif

#if S14DATA_SUPPORT_MEPTB
/* Packed start events of protection equipment with time tag */
void * TMWDEFS_CALLBACK s14data_meptbGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_meptbGetPoint(pHandle, index));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MeptbGetPoint(pHandle, index);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(index);
  return(TMWDEFS_NULL);
#endif
}

TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_meptbGetInfoObjAddr(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getInfoObjAddr(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MeptbGetInfoObjAddr(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_meptbChanged(
  void *pPoint,
  TMWTYPES_UCHAR *pSPE,
  TMWTYPES_UCHAR *pQDP,
  TMWTYPES_USHORT *pRelayDuration)
{
#if TMWCNFG_USE_SIMULATED_DB
  TMWDEFS_CHANGE_REASON reason;
  if(s14sim_getChanged(pPoint, &reason))
  {
    s14sim_meptbGetValue(pPoint, pSPE, pQDP, pRelayDuration);
    return(TMWDEFS_TRUE);
  }
  return(TMWDEFS_FALSE);
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MeptbChanged(pPoint, pSPE, pQDP, pRelayDuration);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(pSPE);
  TMWTARG_UNUSED_PARAM(pQDP);
  TMWTARG_UNUSED_PARAM(pRelayDuration);
  return(TMWDEFS_FALSE);
#endif
}
#endif

#if S14DATA_SUPPORT_MEPTC
/* Packed output circuit information of protection equipment with time tag */
void * TMWDEFS_CALLBACK s14data_meptcGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_meptcGetPoint(pHandle, index));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MeptcGetPoint(pHandle, index);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(index);
  return(TMWDEFS_NULL);
#endif
}

TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_meptcGetInfoObjAddr(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getInfoObjAddr(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MeptcGetInfoObjAddr(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_meptcChanged(
  void *pPoint,
  TMWTYPES_UCHAR *pOCI,
  TMWTYPES_UCHAR *pQDP,
  TMWTYPES_USHORT *pRelayOperating)
{
#if TMWCNFG_USE_SIMULATED_DB
  TMWDEFS_CHANGE_REASON reason;
  if(s14sim_getChanged(pPoint, &reason))
  {
    s14sim_meptcGetValue(pPoint, pOCI, pQDP, pRelayOperating);
    return(TMWDEFS_TRUE);
  }
  return(TMWDEFS_FALSE);
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MeptcChanged(pPoint, pOCI, pQDP, pRelayOperating);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(pOCI);
  TMWTARG_UNUSED_PARAM(pQDP);
  TMWTARG_UNUSED_PARAM(pRelayOperating);
  return(TMWDEFS_FALSE);
#endif
}
#endif

#if S14DATA_SUPPORT_MPS
/* Packed Single Point Information With Status Change Detection */
/* function: s14data_mpsnaGetPoint */
void * TMWDEFS_CALLBACK s14data_mpsnaGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mpsnaGetPoint(pHandle, index));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MpsnaGetPoint(pHandle, index);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(index);
  return(TMWDEFS_NULL);
#endif
}

/* function: s14data_mpsnaGetInfoObjAddr */
TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_mpsnaGetInfoObjAddr(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getInfoObjAddr(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MpsnaGetInfoObjAddr(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_mpsnaGetIndexed */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14data_mpsnaGetIndexed(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mpsnaGetIndexed(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MpsnaGetIndexed(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_mpsnaGetGroupMask */
TMWDEFS_GROUP_MASK TMWDEFS_CALLBACK s14data_mpsnaGetGroupMask(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getGroupMask(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MpsnaGetGroupMask(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_mpsnaGetFlags */
TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mpsnaGetFlags(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getFlags(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MpsnaGetFlags(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_mpsnaGetValue */
TMWTYPES_ULONG TMWDEFS_GLOBAL s14data_mpsnaGetValue(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mpsnaGetValue(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MpsnaGetValue(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_mpsnaChanged */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_mpsnaChanged(
  void *pPoint,
  TMWTYPES_ULONG *pSCD,
  TMWTYPES_UCHAR *pQDS,
  TMWDEFS_CHANGE_REASON *pReason)
{
#if TMWCNFG_USE_SIMULATED_DB
  if(s14sim_getChanged(pPoint, pReason))
  {
    *pSCD = s14sim_mpsnaGetValue(pPoint);
    *pQDS = s14sim_getFlags(pPoint);
    return(TMWDEFS_TRUE);
  }

  return(TMWDEFS_FALSE);
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MpsnaChanged(pPoint, pSCD, pQDS, pReason);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(pSCD);
  TMWTARG_UNUSED_PARAM(pQDS);
  TMWTARG_UNUSED_PARAM(pReason);
  return(TMWDEFS_FALSE);
#endif
}
#endif

#if S14DATA_SUPPORT_MMEND
/* Normalized Measurands Without Quality Descriptor */
/* function: s14data_mmendGetPoint */
void * TMWDEFS_CALLBACK s14data_mmendGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mmendGetPoint(pHandle, index));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmendGetPoint(pHandle, index);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(index);
  return(TMWDEFS_NULL);
#endif
}

/* function: s14data_mmendGetInfoObjAddr */
TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_mmendGetInfoObjAddr(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getInfoObjAddr(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmendGetInfoObjAddr(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_mmendGetGroupMask */
TMWDEFS_GROUP_MASK TMWDEFS_CALLBACK s14data_mmendGetGroupMask(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getGroupMask(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmendGetGroupMask(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_mmendGetIndexed() */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14data_mmendGetIndexed(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mmendGetIndexed(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmendGetIndexed(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_mmendGetValue */
TMWTYPES_SHORT TMWDEFS_GLOBAL s14data_mmendGetValue(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mmendGetValue(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmendGetValue(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_mmendChanged */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_mmendChanged(
  void *pPoint,
  TMWTYPES_SHORT *pNVA,
  TMWDEFS_CHANGE_REASON *pReason)
{
#if TMWCNFG_USE_SIMULATED_DB
  if(s14sim_getChanged(pPoint, pReason))
  {
    *pNVA = s14sim_mmendGetValue(pPoint);
    return(TMWDEFS_TRUE);
  }

  return(TMWDEFS_FALSE);
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MmendChanged(pPoint, pNVA, pReason);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(pNVA);
  TMWTARG_UNUSED_PARAM(pReason);
  return(TMWDEFS_FALSE);
#endif
}
#endif

#if S14DATA_SUPPORT_CSC
/* Single Commands */
/* function: s14data_cscGetPoint */
void * TMWDEFS_CALLBACK s14data_cscGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{ 
  /* Not currently called by SCL */
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(index);
  return(TMWDEFS_NULL);
}

/* function: s14data_cscLookupPoint */
void * TMWDEFS_GLOBAL s14data_cscLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_cscLookupPoint(pHandle, ioa));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CscLookupPoint(pHandle, ioa);
#else
  /* XXX Changed by Lucy */
  DBG_INFO("s14data_cscLookupPoint with ioa : %lu",ioa);
  if (pHandle != NULL)
  {
      I870DBHandleStr* db= (I870DBHandleStr*) pHandle;
      return db->cscLookupPoint(db->pIEC870DB, ioa);
  }
  return(TMWDEFS_NULL);
#endif
}

/* function: s14data_cscGetInfoObjAddr */
TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_cscGetInfoObjAddr(
  void *pPoint)
{
  /* Not currently called by SCL */
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
}

/* function: s14data_cscSelectRequired */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_cscSelectRequired(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_cscSelectRequired(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CscSelectRequired(pPoint);
#else
  /* XXX Changed by Lucy */
    if(pPoint != NULL)
    {
        I870OutputStr* csc = (I870OutputStr*)pPoint;
        if(csc->oPointConf.selectRequired == TMWDEFS_TRUE )
            return(TMWDEFS_TRUE);
        else
            return(TMWDEFS_FALSE);
    }
    return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_cscGetMonitoredPoint */
void * TMWDEFS_GLOBAL s14data_cscGetMonitoredPoint(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_cscGetMonitoredPoint(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CscGetMonitoredPoint(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_NULL);
#endif
}

/* function: s14data_cscSelect */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_cscSelect(
  void *pPoint,
  TMWTYPES_UCHAR cot,
  TMWTYPES_UCHAR sco)
{
#if TMWCNFG_USE_SIMULATED_DB
  TMWTARG_UNUSED_PARAM(cot);
  TMWTARG_UNUSED_PARAM(sco);
  return(s14sim_cscSelect(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CscSelect(pPoint, cot, sco);
#else
  /* XXX Changed by Lucy */
    DBG_INFO("s14data_cscSelect with COT : %d & sco(single command) : %d",cot,sco);
    TMWTARG_UNUSED_PARAM(sco);
    if( (pPoint != NULL) && (cot == I14DEF_COT_ACTIVATION))
    {
        I870OutputStr* csc = (I870OutputStr*)pPoint;
        if (csc->oPointConf.timeTag == TIME_TAG_BOTH
            || csc->oPointConf.timeTag == TIME_TAG_WITHOUT_TIMETAG)
        {
            if(csc->oPointConf.selectRequired)
            return (TMWDEFS_CMD_STAT_SUCCESS);
        }
    }
  return(TMWDEFS_CMD_STAT_FAILED);
#endif
}

/* XXX added by Lucy */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s4data_cscSelect(
  void *pPoint,
  TMWTYPES_UCHAR cot,
  TMWTYPES_UCHAR sco)
{
#if TMWCNFG_USE_SIMULATED_DB
  TMWTARG_UNUSED_PARAM(cot);
  TMWTARG_UNUSED_PARAM(sco);
  return(s14sim_cscSelect(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CscSelect(pPoint, cot, sco);
#else
  /* XXX Changed by Lucy */
    DBG_INFO("s14data_cscSelect with COT : %d & sco(single command) : %d",cot,sco);
    TMWTARG_UNUSED_PARAM(sco);
    if( (pPoint != NULL) && (cot == I14DEF_COT_ACTIVATION))
    {
        I870OutputStr* csc = (I870OutputStr*)pPoint;
        if (csc->oPointConf.timeTag == TIME_TAG_BOTH
           || csc->oPointConf.timeTag == TIME_TAG_WITH_TIMETAG)
        {
            if(csc->oPointConf.selectRequired)
            return(TMWDEFS_CMD_STAT_SUCCESS);
        }
    }
  return(TMWDEFS_CMD_STAT_FAILED);
#endif
}

/* function: s14data_cscExecute */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_cscExecute(
  void *pPoint,
  TMWTYPES_UCHAR cot,
  TMWTYPES_UCHAR sco)
{
#if TMWCNFG_USE_SIMULATED_DB
  TMWTYPES_BOOL value = (TMWTYPES_BOOL)((sco & I14DEF_SCS_ON) == I14DEF_SCS_ON);
  TMWTARG_UNUSED_PARAM(cot);

  return(s14sim_cscExecute(pPoint, value));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CscExecute(pPoint, cot, sco);
#else
  /* XXX Changed by Lucy */
    DBG_INFO("s14data_cscExecute with COT : %d & sco(single command) : %d",cot,sco);
    TMWTARG_UNUSED_PARAM(cot);
    if(pPoint != NULL)
    {
        I870OutputStr* csc = (I870OutputStr*)pPoint;
         if (csc->oPointConf.timeTag == TIME_TAG_BOTH
             || csc->oPointConf.timeTag == TIME_TAG_WITHOUT_TIMETAG)
         {
             return(csc->cscExecute(csc->pG3DB,csc->oPointConf,sco));
         }
    }
  return(TMWDEFS_CMD_STAT_FAILED);
#endif
}

/* XXX added by Lucy */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s4data_cscExecute(
  void *pPoint,
  TMWTYPES_UCHAR cot,
  TMWTYPES_UCHAR sco)
{
#if TMWCNFG_USE_SIMULATED_DB
  TMWTYPES_BOOL value = (TMWTYPES_BOOL)((sco & I14DEF_SCS_ON) == I14DEF_SCS_ON);
  TMWTARG_UNUSED_PARAM(cot);

  return(s14sim_cscExecute(pPoint, value));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CscExecute(pPoint, cot, sco);
#else
  /* XXX Changed by Lucy */
    DBG_INFO("s14data_cscExecute with COT : %d & sco(single command) : %d",cot,sco);
    TMWTARG_UNUSED_PARAM(cot);
    if(pPoint != NULL)
    {
        I870OutputStr* csc = (I870OutputStr*)pPoint;
        if (csc->oPointConf.timeTag == TIME_TAG_BOTH
           || csc->oPointConf.timeTag == TIME_TAG_WITH_TIMETAG)
        {
            return(csc->cscExecute(csc->pG3DB,csc->oPointConf,sco));
        }
    }
  return(TMWDEFS_CMD_STAT_FAILED);
#endif
}

/* function: s14data_cscStatus */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_cscStatus(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_cscStatus(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CscStatus(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_CMD_STAT_FAILED);
#endif
}
#endif

#if S14DATA_SUPPORT_CDC
/* Double Commands */
/* function: s14data_cdcGetPoint */
void * TMWDEFS_CALLBACK s14data_cdcGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
  /* Not currently called by SCL */
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(index);
  return(TMWDEFS_NULL);
}

/* function: s14data_cdcLookupPoint */
void * TMWDEFS_GLOBAL s14data_cdcLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_cdcLookupPoint(pHandle, ioa));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CdcLookupPoint(pHandle, ioa);
#else
  /* XXX Changed by Lucy */
  DBG_INFO("s14data_cdcLookupPoint with ioa : %lu",ioa);
  if (pHandle != NULL)
  {
      I870DBHandleStr* db= (I870DBHandleStr*) pHandle;
      return db->cdcLookupPoint(db->pIEC870DB, ioa);
  }
  return(TMWDEFS_NULL);
#endif
}

/* function: s14data_cdcGetInfoObjAddr */
TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_cdcGetInfoObjAddr(
  void *pPoint)
{
  /* Not currently called by SCL */
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
}

/* function: s14data_cdcSelectRequired */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_cdcSelectRequired(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_cdcSelectRequired(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CdcSelectRequired(pPoint);
#else
  /* XXX Changed by Lucy */
  if (pPoint != NULL)
  {
       I870OutputStr* cdc = (I870OutputStr*)pPoint;
       if (cdc->oPointConf.selectRequired == TMWDEFS_TRUE )
       {
           return(TMWDEFS_TRUE);
       }
       else
       {
           return(TMWDEFS_FALSE);
       }
  }
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_cdcGetMonitoredPoint */
void * TMWDEFS_GLOBAL s14data_cdcGetMonitoredPoint(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_cdcGetMonitoredPoint(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CdcGetMonitoredPoint(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_NULL);
#endif
}

/* function: s14data_cdcSelect */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_cdcSelect(
  void *pPoint,
  TMWTYPES_UCHAR cot,
  TMWTYPES_UCHAR dco)
{
#if TMWCNFG_USE_SIMULATED_DB
  TMWTARG_UNUSED_PARAM(cot);
  TMWTARG_UNUSED_PARAM(dco);
  return(s14sim_cdcSelect(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CdcSelect(pPoint, cot, dco);
#else
  /* XXX Changed by Lucy */
  DBG_INFO("s14data_cdcSelect with COT : %d & dco(double command) : %d",cot,dco);
  TMWTARG_UNUSED_PARAM(dco);
  if ((pPoint != NULL) && (cot == I14DEF_COT_ACTIVATION))
  {
      I870OutputStr* cdc = (I870OutputStr*)pPoint;
       if (cdc->oPointConf.timeTag == TIME_TAG_BOTH
           || cdc->oPointConf.timeTag == TIME_TAG_WITHOUT_TIMETAG)
       {
           if(cdc->oPointConf.selectRequired)
           return(TMWDEFS_CMD_STAT_SUCCESS);
       }
  }
  return(TMWDEFS_CMD_STAT_FAILED);
#endif
}

/* function: s4data_cdcSelect */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s4data_cdcSelect(
  void *pPoint,
  TMWTYPES_UCHAR cot,
  TMWTYPES_UCHAR dco)
{
#if TMWCNFG_USE_SIMULATED_DB
  TMWTARG_UNUSED_PARAM(cot);
  TMWTARG_UNUSED_PARAM(dco);
  return(s14sim_cdcSelect(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CdcSelect(pPoint, cot, dco);
#else
  /* XXX Changed by Lucy */
  DBG_INFO("s14data_cdcSelect with COT : %d & dco(double command) : %d",cot,dco);
  TMWTARG_UNUSED_PARAM(dco);
  if ((pPoint != NULL) && (cot == I14DEF_COT_ACTIVATION))
  {
    I870OutputStr* cdc = (I870OutputStr*)pPoint;
    if (cdc->oPointConf.timeTag == TIME_TAG_BOTH
       || cdc->oPointConf.timeTag == TIME_TAG_WITH_TIMETAG)
    {
        if(cdc->oPointConf.selectRequired)
            return (TMWDEFS_CMD_STAT_SUCCESS);
    }
  }
  return(TMWDEFS_CMD_STAT_FAILED);
#endif
}

/* function: s14data_cdcExecute */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_cdcExecute(
  void *pPoint,
  TMWTYPES_UCHAR cot,
  TMWTYPES_UCHAR dco)
{
#if TMWCNFG_USE_SIMULATED_DB
  TMWDEFS_DPI_TYPE value = (TMWDEFS_DPI_TYPE)(dco & I14DEF_DCS_MASK);
  TMWTARG_UNUSED_PARAM(cot);

  return(s14sim_cdcExecute(pPoint, value));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CdcExecute(pPoint, cot, dco);
#else
  /* XXX Changed by Lucy */
  DBG_INFO("s14data_cdcExecute with COT : %d & dco(double command) : %d",cot,dco);
  TMWTARG_UNUSED_PARAM(cot);
  if (pPoint != NULL)
  {
      I870OutputStr* cdc = (I870OutputStr*)pPoint;
      if (cdc->oPointConf.timeTag == TIME_TAG_BOTH
                 || cdc->oPointConf.timeTag == TIME_TAG_WITHOUT_TIMETAG)
      {
          return(cdc->cdcExecute(cdc->pG3DB,cdc->oPointConf,dco));
      }
  }
  return(TMWDEFS_CMD_STAT_FAILED);
#endif
}

/* luc */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s4data_cdcExecute(
  void *pPoint,
  TMWTYPES_UCHAR cot,
  TMWTYPES_UCHAR dco)
{
#if TMWCNFG_USE_SIMULATED_DB
  TMWDEFS_DPI_TYPE value = (TMWDEFS_DPI_TYPE)(dco & I14DEF_DCS_MASK);
  TMWTARG_UNUSED_PARAM(cot);

  return(s14sim_cdcExecute(pPoint, value));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CdcExecute(pPoint, cot, dco);
#else
  /* XXX Changed by Lucy */
  DBG_INFO("s14data_cdcExecute with COT : %d & dco(double command) : %d",cot,dco);
  TMWTARG_UNUSED_PARAM(cot);
  if (pPoint != NULL)
  {
      I870OutputStr* cdc = (I870OutputStr*)pPoint;
      if(cdc->oPointConf.timeTag== TIME_TAG_BOTH
      ||cdc->oPointConf.timeTag== TIME_TAG_WITH_TIMETAG)
      {
          return(cdc->cdcExecute(cdc->pG3DB,cdc->oPointConf,dco));
      }
  }
  return(TMWDEFS_CMD_STAT_FAILED);
#endif
}

/* function: s14data_cdcStatus */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_cdcStatus(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_cdcStatus(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CdcStatus(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_CMD_STAT_FAILED);
#endif
}
#endif

#if S14DATA_SUPPORT_CRC
/* Regulating Step Commands */
/* function: s14data_crcGetPoint */
void * TMWDEFS_CALLBACK s14data_crcGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
  /* Not currently called by SCL */
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(index);
  return(TMWDEFS_NULL);
}

/* function: s14data_crcLookupPoint */
void * TMWDEFS_GLOBAL s14data_crcLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_crcLookupPoint(pHandle, ioa));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CrcLookupPoint(pHandle, ioa);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);
  return(TMWDEFS_NULL);
#endif
}

/* function: s14data_crcGetInfoObjAddr */
TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_crcGetInfoObjAddr(
  void *pPoint)
{
  /* Not currently called by SCL */
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
}

/* function: s14data_crcGetMonitoredPoint */
void * TMWDEFS_GLOBAL s14data_crcGetMonitoredPoint(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_crcGetMonitoredPoint(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CrcGetMonitoredPoint(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_NULL);
#endif
}

/* function: s14data_crcSelectRequired */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_crcSelectRequired(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_crcSelectRequired(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CrcSelectRequired(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_crcSelect */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_crcSelect(
  void *pPoint,
  TMWTYPES_UCHAR cot,
  TMWTYPES_UCHAR rco)
{
#if TMWCNFG_USE_SIMULATED_DB
  TMWTARG_UNUSED_PARAM(cot);
  TMWTARG_UNUSED_PARAM(rco);
  return(s14sim_crcSelect(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CrcSelect(pPoint, cot, rco);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(cot);
  TMWTARG_UNUSED_PARAM(rco);
  return(TMWDEFS_CMD_STAT_FAILED);
#endif
}

/* function: s14data_crcExecute */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_crcExecute(
  void *pPoint,
  TMWTYPES_UCHAR cot,
  TMWTYPES_UCHAR rco)
{
#if TMWCNFG_USE_SIMULATED_DB
  TMWTARG_UNUSED_PARAM(cot);
  return(s14sim_crcExecute(pPoint, rco));
#elif TMWCNFG_USE_MANAGED_SCL
  TMWTARG_UNUSED_PARAM(cot);
  return S14DatabaseWrapper_CrcExecute(pPoint, cot, rco);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(cot);
  TMWTARG_UNUSED_PARAM(rco);
  return(TMWDEFS_CMD_STAT_FAILED);
#endif
}

/* function: s14data_crcStatus */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_crcStatus(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_crcStatus(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CrcStatus(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_CMD_STAT_FAILED);
#endif
}
#endif

#if S14DATA_SUPPORT_CBO
/* Bitstring Commands */
/* function: s14data_cboGetPoint */
void * TMWDEFS_CALLBACK s14data_cboGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
  /* Not currently called by SCL */
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(index);
  return(TMWDEFS_NULL);
}

/* function: s14data_cboLookupPoint */
void * TMWDEFS_GLOBAL s14data_cboLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_cboLookupPoint(pHandle, ioa));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CboLookupPoint(pHandle, ioa);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);
  return(TMWDEFS_NULL);
#endif
}

/* function: s14data_cboExecute */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_cboExecute(
  void *pPoint,
  TMWTYPES_ULONG value)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_cboExecute(pPoint, value));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CboExecute(pPoint, value);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(value);
  return(TMWDEFS_CMD_STAT_FAILED);
#endif
}
#endif

#if S14DATA_SUPPORT_CSE_A
/* Normalized Measurand Commands */
/* function: s14data_csenaGetPoint */
void * TMWDEFS_CALLBACK s14data_csenaGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
  /* Not currently called by SCL */
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(index);
  return(TMWDEFS_NULL);
}

/* function: s14data_csenaLookupPoint */
void * TMWDEFS_GLOBAL s14data_csenaLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_csenaLookupPoint(pHandle, ioa));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CsenaLookupPoint(pHandle, ioa);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);
  return(TMWDEFS_NULL);
#endif
}

/* function: s14data_csenaGetInfoObjAddr */
TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_csenaGetInfoObjAddr(
  void *pPoint)
{
  /* Not currently called by SCL */
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
}

/* function: s14data_csenaSelectRequired */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_csenaSelectRequired(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_csenaSelectRequired(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CsenaSelectRequired(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_csenaSelect */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_csenaSelect(
  void *pPoint,
  TMWTYPES_UCHAR cot,
  TMWTYPES_SHORT value,
  TMWTYPES_UCHAR qos)
{
#if TMWCNFG_USE_SIMULATED_DB
  TMWTARG_UNUSED_PARAM(cot);
  TMWTARG_UNUSED_PARAM(value);
  TMWTARG_UNUSED_PARAM(qos);
  return(s14sim_csenaSelect(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CsenaSelect(pPoint, cot, value, qos);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(cot);
  TMWTARG_UNUSED_PARAM(value);
  TMWTARG_UNUSED_PARAM(qos);
  return(TMWDEFS_CMD_STAT_FAILED);
#endif
}

/* function: s14data_csenaExecute */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_csenaExecute(
  void *pPoint,
  TMWTYPES_UCHAR cot,
  TMWTYPES_SHORT value,
  TMWTYPES_UCHAR qos)
{
#if TMWCNFG_USE_SIMULATED_DB
  TMWTARG_UNUSED_PARAM(cot);
  TMWTARG_UNUSED_PARAM(qos);
  return(s14sim_csenaExecute(pPoint, value));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CsenaExecute(pPoint, cot, value, qos);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(cot);
  TMWTARG_UNUSED_PARAM(value);
  TMWTARG_UNUSED_PARAM(qos);
  return(TMWDEFS_CMD_STAT_FAILED);
#endif
}

/* function: s14data_csenaStatus */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_csenaStatus(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_csenaStatus(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CsenaStatus(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_CMD_STAT_FAILED);
#endif
}
#endif

#if S14DATA_SUPPORT_CSE_B
/* Scaled Measurand Commands */
/* function: s14data_csenbGetPoint */
void * TMWDEFS_CALLBACK s14data_csenbGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
  /* Not currently called by SCL */
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(index);
  return(TMWDEFS_NULL);
}

/* function: s14data_csenbLookupPoint */
void * TMWDEFS_GLOBAL s14data_csenbLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_csenbLookupPoint(pHandle, ioa));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CsenbLookupPoint(pHandle, ioa);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);
  return(TMWDEFS_NULL);
#endif
}

/* function: s14data_csenbGetInfoObjAddr */
TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_csenbGetInfoObjAddr(
  void *pPoint)
{
  /* Not currently called by SCL */
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
}

/* function: s14data_csenbSelectRequired */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_csenbSelectRequired(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_csenbSelectRequired(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CsenbSelectRequired(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_csenbSelect */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_csenbSelect(
  void *pPoint,
  TMWTYPES_UCHAR cot,
  TMWTYPES_SHORT value,
  TMWTYPES_UCHAR qos)
{
#if TMWCNFG_USE_SIMULATED_DB
  TMWTARG_UNUSED_PARAM(cot);
  TMWTARG_UNUSED_PARAM(value);
  TMWTARG_UNUSED_PARAM(qos);
  return(s14sim_csenbSelect(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CsenbSelect(pPoint, cot, value, qos);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(cot);
  TMWTARG_UNUSED_PARAM(value);
  TMWTARG_UNUSED_PARAM(qos);
  return(TMWDEFS_CMD_STAT_FAILED);
#endif
}

/* function: s14data_csenbExecute */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_csenbExecute(
  void *pPoint,
  TMWTYPES_UCHAR cot,
  TMWTYPES_SHORT value,
  TMWTYPES_UCHAR qos)
{
#if TMWCNFG_USE_SIMULATED_DB
  TMWTARG_UNUSED_PARAM(cot);
  TMWTARG_UNUSED_PARAM(qos);
  return(s14sim_csenbExecute(pPoint, value));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CsenbExecute(pPoint, cot, value, qos);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(cot);
  TMWTARG_UNUSED_PARAM(value);
  TMWTARG_UNUSED_PARAM(qos);
  return(TMWDEFS_CMD_STAT_FAILED);
#endif
}

/* function: s14data_csenbStatus */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_csenbStatus(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_csenbStatus(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CsenbStatus(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_CMD_STAT_FAILED);
#endif
}
#endif

#if S14DATA_SUPPORT_CSE_C
/* Floating Point Measurand Commands */
/* function: s14data_csencGetPoint */
void * TMWDEFS_CALLBACK s14data_csencGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
  /* Not currently called by SCL */
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(index);
  return(TMWDEFS_NULL);
}

/* function: s14data_csencLookupPoint */
void * TMWDEFS_GLOBAL s14data_csencLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
  
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_csencLookupPoint(pHandle, ioa));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CsencLookupPoint(pHandle, ioa);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);
  return(TMWDEFS_NULL);
#endif
}

/* function: s14data_csencGetInfoObjAddr */
TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_csencGetInfoObjAddr(
  void *pPoint)
{
  /* Not currently called by SCL */
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
}

/* function: s14data_csencSelectRequired */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_csencSelectRequired(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_csencSelectRequired(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CsencSelectRequired(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_csencSelect */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_csencSelect(
  void *pPoint,
  TMWTYPES_UCHAR cot,
  TMWTYPES_SFLOAT value,
  TMWTYPES_UCHAR qos)
{
#if TMWCNFG_USE_SIMULATED_DB
  TMWTARG_UNUSED_PARAM(cot);
  TMWTARG_UNUSED_PARAM(value);
  TMWTARG_UNUSED_PARAM(qos);
  return(s14sim_csencSelect(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CsencSelect(pPoint, cot, value, qos);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(cot);
  TMWTARG_UNUSED_PARAM(value);
  TMWTARG_UNUSED_PARAM(qos);
  return(TMWDEFS_CMD_STAT_FAILED);
#endif
}

/* function: s14data_csencExecute */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_csencExecute(
  void *pPoint,
  TMWTYPES_UCHAR cot,
  TMWTYPES_SFLOAT value,
  TMWTYPES_UCHAR qos)
{
#if TMWCNFG_USE_SIMULATED_DB
  TMWTARG_UNUSED_PARAM(cot);
  TMWTARG_UNUSED_PARAM(qos);
  return(s14sim_csencExecute(pPoint, value));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CsencExecute(pPoint, cot, value, qos);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(cot);
  TMWTARG_UNUSED_PARAM(value);
  TMWTARG_UNUSED_PARAM(qos);
  return(TMWDEFS_CMD_STAT_FAILED);
#endif
}

/* function: s14data_csencStatus */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_csencStatus(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_csencStatus(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CsencStatus(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_CMD_STAT_FAILED);
#endif
}
#endif

#if S14DATA_SUPPORT_PMENA
/* Parameter of Measured Value, Normalized Value */
/* function: s14data_pmenaGetPoint */
void * TMWDEFS_CALLBACK s14data_pmenaGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_pmenaGetPoint(pHandle, index));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_PmenaGetPoint(pHandle, index);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(index);
  return(TMWDEFS_NULL);
#endif
}

/* function: s14data_pmenaLookupPoint */
void * TMWDEFS_GLOBAL s14data_pmenaLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_pmenaLookupPoint(pHandle, ioa));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_PmenaLookupPoint(pHandle, ioa);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);
  return(TMWDEFS_NULL);
#endif
}

/* function: s14data_pmenaGetInfoObjAddr */
TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_pmenaGetInfoObjAddr(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getInfoObjAddr(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_PmenaGetInfoObjAddr(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_pmenaGetGroupMask */
TMWDEFS_GROUP_MASK TMWDEFS_CALLBACK s14data_pmenaGetGroupMask(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getGroupMask(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_PmenaGetGroupMask(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_pmenaGetValue */
TMWTYPES_SHORT TMWDEFS_GLOBAL s14data_pmenaGetValue(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_pmenaGetValue(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_PmenaGetValue(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_pmenaGetQualifier */
TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_pmenaGetQualifier(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_pmenaGetQualifier(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_PmenaGetQualifier(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_pmenaStore */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_pmenaStore(
  void *pPoint,
  TMWTYPES_SHORT *pValue,
  TMWTYPES_UCHAR *pQualifier)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_pmenaStore(pPoint, pValue, pQualifier));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_PmenaStore(pPoint, pValue, pQualifier);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(pValue);
  TMWTARG_UNUSED_PARAM(pQualifier);
  return(TMWDEFS_FALSE);
#endif
}
#endif

#if S14DATA_SUPPORT_PMENB
/* Parameter of Measured Value, Scaled Value */
/* function: s14data_pmenbGetPoint */
void * TMWDEFS_CALLBACK s14data_pmenbGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_pmenbGetPoint(pHandle, index));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_PmenbGetPoint(pHandle, index);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(index);
  return(TMWDEFS_NULL);
#endif
}

/* function: s14data_pmenbLookupPoint */
void * TMWDEFS_GLOBAL s14data_pmenbLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_pmenbLookupPoint(pHandle, ioa));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_PmenbLookupPoint(pHandle, ioa);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);
  return(TMWDEFS_NULL);
#endif
}

/* function: s14data_pmenbGetInfoObjAddr */
TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_pmenbGetInfoObjAddr(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getInfoObjAddr(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_PmenbGetInfoObjAddr(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_pmenbGetGroupMask */
TMWDEFS_GROUP_MASK TMWDEFS_CALLBACK s14data_pmenbGetGroupMask(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getGroupMask(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_PmenbGetGroupMask(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_pmenbGetValue */
TMWTYPES_SHORT TMWDEFS_GLOBAL s14data_pmenbGetValue(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_pmenbGetValue(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_PmenbGetValue(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_pmenbGetQualifier */
TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_pmenbGetQualifier(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_pmenbGetQualifier(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_PmenbGetQualifier(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_pmenbStore */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_pmenbStore(
  void *pPoint,
  TMWTYPES_SHORT *pValue,
  TMWTYPES_UCHAR *pQualifier)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_pmenbStore(pPoint, pValue, pQualifier));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_PmenbStore(pPoint, pValue, pQualifier);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(pValue);
  TMWTARG_UNUSED_PARAM(pQualifier);
  return(TMWDEFS_FALSE);
#endif
}
#endif

#if S14DATA_SUPPORT_PMENC
/* Parameter of Measured Value, Short Floating Point Value */
/* function: s14data_pmencGetPoint */
void * TMWDEFS_CALLBACK s14data_pmencGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_pmencGetPoint(pHandle, index));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_PmencGetPoint(pHandle, index);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(index);
  return(TMWDEFS_NULL);
#endif
}

/* function: s14data_pmencLookupPoint */
void * TMWDEFS_GLOBAL s14data_pmencLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_pmencLookupPoint(pHandle, ioa));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_PmencLookupPoint(pHandle, ioa);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);
  return(TMWDEFS_NULL);
#endif
}

/* function: s14data_pmencGetInfoObjAddr */
TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_pmencGetInfoObjAddr(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getInfoObjAddr(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_PmencGetInfoObjAddr(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_pmencGetGroupMask */
TMWDEFS_GROUP_MASK TMWDEFS_CALLBACK s14data_pmencGetGroupMask(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_getGroupMask(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_PmencGetGroupMask(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_pmencGetValue */
TMWTYPES_SFLOAT TMWDEFS_GLOBAL s14data_pmencGetValue(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_pmencGetValue(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_PmencGetValue(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_pmencGetQualifier */
TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_pmencGetQualifier(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_pmencGetQualifier(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_PmencGetQualifier(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
#endif
}

/* function: s14data_pmencStore */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_pmencStore(
  void *pPoint,
  TMWTYPES_SFLOAT *pValue,
  TMWTYPES_UCHAR *pQualifier)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_pmencStore(pPoint, pValue, pQualifier));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_PmencStore(pPoint, pValue, pQualifier);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(pValue);
  TMWTARG_UNUSED_PARAM(pQualifier);
  return(TMWDEFS_FALSE);
#endif
}
#endif

#if S14DATA_SUPPORT_PACNA
/* Parameter Activation */
/* function: s14data_pacnaLookupPoint */
void * TMWDEFS_GLOBAL s14data_pacnaLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_pacnaLookupPoint(pHandle, ioa));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_PacnaLookupPoint(pHandle, ioa);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);
  return(TMWDEFS_NULL);
#endif
}

/* function: s14data_pacnaStore */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_pacnaStore(
  void *pPoint,
  TMWTYPES_UCHAR cot,
  TMWTYPES_UCHAR qpa)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_pacnaStore(pPoint, cot, qpa));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_PacnaStore(pPoint, cot, qpa);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(cot);
  TMWTARG_UNUSED_PARAM(qpa);
  return(TMWDEFS_FALSE);
#endif
}
#endif

#if S14DATA_SUPPORT_CRPNA
/* function: s14data_crpnaSupport */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_crpnaSupport(
  TMWSCTR *pSector,
  TMWTYPES_UCHAR qrp)
{
#if TMWCNFG_USE_SIMULATED_DB
  TMWTARG_UNUSED_PARAM(qrp);
  TMWTARG_UNUSED_PARAM(pSector);
  return(TMWDEFS_TRUE);
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CrpnaSupport(pSector, qrp);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(qrp);
  TMWTARG_UNUSED_PARAM(pSector);
  return(TMWDEFS_TRUE);
#endif
}

/* function: s14data_crpnaExecute */
void TMWDEFS_GLOBAL s14data_crpnaExecute(
  TMWSCTR *pSector,
  TMWTYPES_UCHAR qrp)
{
#if TMWCNFG_USE_SIMULATED_DB
  TMWTARG_UNUSED_PARAM(qrp);
  TMWTARG_UNUSED_PARAM(pSector);
  return;
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CrpnaExecute(pSector, qrp);
#else
    /* XXX Changed by Lucy */
    if (pSector == NULL)
    {
        return;
    }
    if(pSector->pUserData == NULL)
    {
        DBG_INFO("CRPNA_Execute: No DB available");
        return;
    }
    I870DBHandleStr* db = (I870DBHandleStr*)(pSector->pUserData);
    if(db->crpnaExecuteRTUReset != NULL)
    {
        db->crpnaExecuteRTUReset(qrp);
    }
  return;
#endif
}
#endif

#if S14DATA_SUPPORT_FILE
/* File Transfer */

/* function: s14data_callFileResponse */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_fileCallFileResp(
  void *pHandle, 
  TMWTYPES_ULONG  ioa,
  TMWTYPES_USHORT fileName,
  TMWTYPES_ULONG  fileLength,
  TMWTYPES_UCHAR  frq,
  TMWTYPES_UCHAR *pScq)
{
#if TMWCNFG_USE_SIMULATED_DB
  TMWTARG_UNUSED_PARAM(frq);
  return(s14sim_fileCallFileResp(pHandle, ioa, fileName, fileLength, pScq));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_FileCallFileResp(pHandle, ioa, fileName, fileLength, frq, pScq);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);
  TMWTARG_UNUSED_PARAM(fileName);
  TMWTARG_UNUSED_PARAM(fileLength);
  TMWTARG_UNUSED_PARAM(frq);
  TMWTARG_UNUSED_PARAM(pScq);
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_fileCallSectionResp */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_fileCallSectionResp(
  void *pHandle, 
  TMWTYPES_ULONG  ioa,
  TMWTYPES_USHORT fileName,
  TMWTYPES_UCHAR  sectionName,
  TMWTYPES_ULONG  sectionLength,
  TMWTYPES_UCHAR  srq,
  TMWTYPES_UCHAR *pScq)
{
#if TMWCNFG_USE_SIMULATED_DB
  TMWTARG_UNUSED_PARAM(srq);
  return(s14sim_fileCallSectionResp(pHandle, ioa, fileName, sectionName, sectionLength, pScq));
#elif TMWCNFG_USE_MANAGED_SCL
   return S14DatabaseWrapper_FileCallSectionResp(pHandle, ioa, fileName, sectionName, sectionLength, srq, pScq);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);
  TMWTARG_UNUSED_PARAM(fileName);
  TMWTARG_UNUSED_PARAM(sectionName);
  TMWTARG_UNUSED_PARAM(sectionLength);
  TMWTARG_UNUSED_PARAM(srq);
  TMWTARG_UNUSED_PARAM(pScq);
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_fileAckLastSectOrSegResp */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_fileAckLastSectOrSegResp(
  void *pHandle, 
  TMWTYPES_ULONG  ioa,
  TMWTYPES_USHORT fileName,
  TMWTYPES_UCHAR  sectionName,
  TMWTYPES_UCHAR  lsq,
  TMWTYPES_BOOL   checksumGood,
  TMWTYPES_UCHAR  *pAfq)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_fileAckLastSectOrSegResp(pHandle, ioa, fileName, sectionName, lsq, checksumGood, pAfq));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_FileAckLastSectOrSegResp(pHandle, ioa, fileName, sectionName, lsq, checksumGood, pAfq);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);
  TMWTARG_UNUSED_PARAM(fileName);
  TMWTARG_UNUSED_PARAM(sectionName);
  TMWTARG_UNUSED_PARAM(lsq);
  TMWTARG_UNUSED_PARAM(checksumGood);
  TMWTARG_UNUSED_PARAM(pAfq);
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_fileGetDirectoryEntry */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_fileGetDirectoryEntry(
  void *pHandle, 
  TMWTYPES_UCHAR   index,
  TMWTYPES_ULONG  *pIoa,
  TMWTYPES_USHORT *pFileName,
  TMWTYPES_ULONG  *pFileLength,
  TMWDTIME        *pFileTime,
  TMWTYPES_UCHAR  *pSof)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_fileGetDirectoryEntry(pHandle, index, pIoa, pFileName, pFileLength, pFileTime, pSof));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_FileGetDirectoryEntry(pHandle, index, pIoa, pFileName, pFileLength, pFileTime, pSof);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(index);
  TMWTARG_UNUSED_PARAM(pIoa);
  TMWTARG_UNUSED_PARAM(pFileName);
  TMWTARG_UNUSED_PARAM(pFileLength);
  TMWTARG_UNUSED_PARAM(pFileTime);
  TMWTARG_UNUSED_PARAM(pSof);
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_fileSelectFile */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_fileSelectFile(
  void *pHandle, 
  TMWTYPES_ULONG   ioa,
  TMWTYPES_USHORT  fileName,
  TMWTYPES_ULONG  *pFileLength,
  TMWTYPES_UCHAR  *pFrq)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_fileSelectFile(pHandle, ioa, fileName, pFileLength, pFrq));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_FileSelectFile(pHandle, ioa, fileName, pFileLength, pFrq);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);
  TMWTARG_UNUSED_PARAM(fileName);
  TMWTARG_UNUSED_PARAM(pFileLength);
  TMWTARG_UNUSED_PARAM(pFrq);
  return(TMWDEFS_FALSE);
#endif
} 

#if S14DATA_SUPPORT_FSCNB
/* Only necessary if Query Log added in IEC60870-5-104 Ed 2 is required */
/* function: s14data_fileQueryLog */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_fileQueryLog(
  void *pHandle, 
  TMWTYPES_ULONG   ioa,
  TMWTYPES_USHORT  fileName,
  TMWDTIME *pRangeStartTime,
  TMWDTIME *pRangeStopTime, 
  TMWTYPES_ULONG  *pFileLength,
  TMWTYPES_UCHAR  *pFrq)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_fileQueryLog(pHandle, ioa, fileName, pRangeStartTime, pRangeStopTime, pFileLength, pFrq));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_FileQueryLog(pHandle, ioa, fileName, pRangeStartTime, pRangeStopTime, pFileLength, pFrq);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);
  TMWTARG_UNUSED_PARAM(fileName);
  TMWTARG_UNUSED_PARAM(pFileLength);
  TMWTARG_UNUSED_PARAM(pFrq);
  return(TMWDEFS_FALSE);
#endif
}
#endif

/* function: s14data_fileSegmentStore */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_fileSegmentStore(
  void *pHandle, 
  TMWTYPES_ULONG  ioa,
  TMWTYPES_USHORT fileName,
  TMWTYPES_UCHAR  sectionName,
  TMWTYPES_ULONG  segmentLength,
  TMWTYPES_UCHAR *pSegmentData)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_fileSegmentStore(pHandle, ioa, fileName, sectionName, segmentLength, pSegmentData));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_FileSegmentStore(pHandle, ioa, fileName, sectionName, segmentLength, pSegmentData);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);
  TMWTARG_UNUSED_PARAM(fileName);
  TMWTARG_UNUSED_PARAM(sectionName);
  TMWTARG_UNUSED_PARAM(segmentLength);
  TMWTARG_UNUSED_PARAM(pSegmentData);
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_fileGetSegment */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_fileGetSegment(
  void *pHandle, 
  TMWTYPES_ULONG  ioa,
  TMWTYPES_USHORT fileName,
  TMWTYPES_UCHAR  sectionName,
  TMWTYPES_UCHAR  maxLength,
  TMWTYPES_UCHAR *pSegmentLength,
  TMWTYPES_UCHAR *pSegmentData)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_fileGetSegment(pHandle, ioa, fileName, sectionName, maxLength, pSegmentLength, pSegmentData));
#elif TMWCNFG_USE_MANAGED_SCL
  return(S14DatabaseWrapper_FileGetSegment(pHandle, ioa, fileName, sectionName, maxLength, pSegmentLength, pSegmentData));
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);
  TMWTARG_UNUSED_PARAM(fileName);
  TMWTARG_UNUSED_PARAM(sectionName);
  TMWTARG_UNUSED_PARAM(maxLength);
  TMWTARG_UNUSED_PARAM(pSegmentLength);
  TMWTARG_UNUSED_PARAM(pSegmentData);
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_fileGetLastSegmentResp */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_fileGetLastSegmentResp(
  void *pHandle, 
  TMWTYPES_ULONG  ioa,
  TMWTYPES_USHORT fileName,
  TMWTYPES_UCHAR  sectionName,
  TMWTYPES_UCHAR  *pLsq)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_fileGetLastSegmentResp(pHandle, ioa, fileName, sectionName, pLsq));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_FileGetLastSegmentResp(pHandle, ioa, fileName, sectionName, pLsq);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);
  TMWTARG_UNUSED_PARAM(fileName);
  TMWTARG_UNUSED_PARAM(sectionName);
  TMWTARG_UNUSED_PARAM(pLsq);
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_fileGetLastSectionResp */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_fileGetLastSectionResp(
  void *pHandle, 
  TMWTYPES_ULONG  ioa,
  TMWTYPES_USHORT fileName,
  TMWTYPES_UCHAR  *pLsq)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_fileGetLastSectionResp(pHandle, ioa, fileName, pLsq));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_FileGetLastSectionResp(pHandle, ioa, fileName, pLsq);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);
  TMWTARG_UNUSED_PARAM(fileName);
  TMWTARG_UNUSED_PARAM(pLsq);
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_fileAckFile */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_fileAckFile(
  void *pHandle, 
  TMWTYPES_ULONG  ioa,
  TMWTYPES_USHORT fileName)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_fileAckFile(pHandle, ioa, fileName));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_FileAckFile(pHandle, ioa, fileName);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);
  TMWTARG_UNUSED_PARAM(fileName);
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_fileGetSectionReady */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_fileGetSectionReady(
  void *pHandle, 
  TMWTYPES_ULONG  ioa,
  TMWTYPES_USHORT fileName,
  TMWTYPES_UCHAR  sectionName,
  TMWTYPES_ULONG *pSectionLength,
  TMWTYPES_UCHAR *pSrq)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_fileGetSectionReady(pHandle, ioa, fileName, sectionName, pSectionLength, pSrq));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_FileGetSectionReady(pHandle, ioa, fileName, sectionName, pSectionLength, pSrq);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);
  TMWTARG_UNUSED_PARAM(fileName);
  TMWTARG_UNUSED_PARAM(sectionName);
  TMWTARG_UNUSED_PARAM(pSectionLength);
  TMWTARG_UNUSED_PARAM(pSrq);
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_fileDeleteResp */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_fileDeleteResp(
  void *pHandle, 
  TMWTYPES_ULONG  ioa,
  TMWTYPES_USHORT fileName,
  TMWTYPES_UCHAR *pFrq)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_fileDeleteResp(pHandle, ioa, fileName, pFrq));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_FileDeleteResp(pHandle, ioa, fileName, pFrq);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);
  TMWTARG_UNUSED_PARAM(fileName);
  TMWTARG_UNUSED_PARAM(pFrq);
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_fileDirectoryChanged */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_fileDirectoryChanged(
  void *pHandle)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_fileDirectoryChanged(pHandle));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_FileDirectoryChanged(pHandle);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_fileGetFile */
void * TMWDEFS_GLOBAL s14data_fileGetFile(
  void *pHandle, 
  TMWTYPES_USHORT index)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_fileGetFile(pHandle, index));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_FileGetFile(pHandle, index);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(index);
  return(TMWDEFS_NULL);
#endif
}

/* function: s14data_fileDiagShow */
void TMWDEFS_GLOBAL s14data_fileDiagShow(
  TMWSCTR *pSector,        
  void *pFile)
{ 
#if TMWCNFG_USE_SIMULATED_DB
  s14sim_fileDiagShow(pSector, pFile);
#elif TMWCNFG_USE_MANAGED_SCL
  S14DatabaseWrapper_FileDiagShow(pSector, pFile);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pSector);
  TMWTARG_UNUSED_PARAM(pFile);
#endif
}
#endif /* S14DATA_SUPPORT_FILE */

/* THE FOLLOWING SECTIONS CSENZ, MCTNA, CCTNA and s14data_CICNAResponseReady()
* ARE ONLY NECESSARY IF GASUNIE PRIVATE ASDU SUPPORT IS REQUIRED 
*/

#if S14DATA_SUPPORT_CSE_Z
/* Set Integrated Totals BCD Command, Private ASDU Support */
/* function: s14data_csenzGetPoint */
void * TMWDEFS_CALLBACK s14data_csenzGetPoint(
  void *pHandle,
  TMWTYPES_USHORT index)
{
  /* Not currently called by SCL */
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(index);
  return(TMWDEFS_NULL);
}

/* function: s14data_csenzLookupPoint */
void * TMWDEFS_GLOBAL s14data_csenzLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa)
{

#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_csenzLookupPoint(pHandle, ioa));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CsenzLookupPoint(pHandle, ioa);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);
  return(TMWDEFS_NULL);
#endif
}

/* function: s14data_csenzGetInfoObjAddr */
TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_csenzGetInfoObjAddr(
  void *pPoint)
{
  /* Not currently called by SCL */
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(0);
}

/* function: s14data_csenzSelectRequired */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_csenzSelectRequired(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_csenzSelectRequired(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CsenzSelectRequired(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_FALSE);
#endif
}

/* function: s14data_csenzSelect */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_csenzSelect(
  void *pPoint,
  TMWTYPES_UCHAR cot,
  TMWTYPES_UCHAR *pBCD,
  TMWTYPES_UCHAR qos)
{
#if TMWCNFG_USE_SIMULATED_DB
  TMWTARG_UNUSED_PARAM(cot);
  TMWTARG_UNUSED_PARAM(pBCD);
  TMWTARG_UNUSED_PARAM(qos);
  return(s14sim_csenzSelect(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CsenzSelect(pPoint, cot, pBCD, qos);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(cot);
  TMWTARG_UNUSED_PARAM(pBCD);
  TMWTARG_UNUSED_PARAM(qos);
  return(TMWDEFS_CMD_STAT_FAILED);
#endif
}

/* function: s14data_csenzExecute */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_csenzExecute(
  void *pPoint,
  TMWTYPES_UCHAR cot,
  TMWTYPES_UCHAR *pBCD,
  TMWTYPES_UCHAR qos)
{
#if TMWCNFG_USE_SIMULATED_DB
  TMWTARG_UNUSED_PARAM(cot);
  TMWTARG_UNUSED_PARAM(qos);
  return(s14sim_csenzExecute(pPoint, pBCD));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CsenzExecute(pPoint, cot, pBCD, qos);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(cot);
  TMWTARG_UNUSED_PARAM(pBCD);
  TMWTARG_UNUSED_PARAM(qos);
  return(TMWDEFS_CMD_STAT_FAILED);
#endif
}

/* function: s14data_csenzStatus */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_csenzStatus(
  void *pPoint)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_csenzStatus(pPoint));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CsenzStatus(pPoint);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_CMD_STAT_FAILED);
#endif
}
#endif

#if S14DATA_SUPPORT_MCTNA
/* Configuration Table, Private ASDU Support */
void * TMWDEFS_GLOBAL s14data_mctnaLookupPoint(
  void *pHandle, 
  TMWTYPES_ULONG ioa)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mctnaLookupPoint(pHandle, ioa));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MctnaLookupPoint(pHandle, ioa);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);
  return(TMWDEFS_NULL);
#endif
}

/* function: s14data_mctnaGetValues */ 
TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mctnaGetValues(
  void *pPoint,
  TMWTYPES_UCHAR maxQuantity,
  TMWTYPES_UCHAR *pCtiValues)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mctnaGetValues(pPoint, maxQuantity, pCtiValues));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_MctnaGetValues(pPoint, maxQuantity, pCtiValues);

#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(maxQuantity);
  TMWTARG_UNUSED_PARAM(pCtiValues);
  return(0);
#endif
}
#endif

#if S14DATA_SUPPORT_CCTNA
/* Configuration Table Command, Private ASDU Support */
void * TMWDEFS_GLOBAL s14data_cctnaLookupPoint(
  void *pHandle, 
  TMWTYPES_ULONG ioa)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_mctnaLookupPoint(pHandle, ioa));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CctnaLookupPoint(pHandle, ioa);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(ioa);
  return(TMWDEFS_NULL);
#endif
}

/* function: s14data_cctnaSetValues */ 
void TMWDEFS_GLOBAL s14data_cctnaSetValues(
  void *pPoint,
  TMWTYPES_UCHAR *pCtiValues,
  TMWTYPES_UCHAR quantity)
{
#if TMWCNFG_USE_SIMULATED_DB
  s14sim_mctnaSetValues(pPoint, pCtiValues, quantity);
#elif TMWCNFG_USE_MANAGED_SCL
  S14DatabaseWrapper_CctnaSetValues(pPoint, pCtiValues, quantity);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pPoint);
  TMWTARG_UNUSED_PARAM(pCtiValues);
  TMWTARG_UNUSED_PARAM(quantity);
#endif
}
#endif

#if S14DATA_SUPPORT_RTU_CICNA
/* function: s14data_CICNAResponseReady */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_CICNAResponseReady(
  void *pHandle,
  TMWTYPES_UCHAR cot)
{
#if TMWCNFG_USE_SIMULATED_DB
  return(s14sim_CICNAResponseReady(pHandle, cot));
#elif TMWCNFG_USE_MANAGED_SCL
  return S14DatabaseWrapper_CICNAResponseReady(pHandle, cot);
#else
  /* Put target code here */
  TMWTARG_UNUSED_PARAM(pHandle);
  TMWTARG_UNUSED_PARAM(cot);
  return(TMWDEFS_TRUE);
#endif
}
#endif
