/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14crcna.c
 * description: IEC 60870-5-101 slave crcna (regulating step command)
 *  functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14crcna.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/s14mst.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/utils/tmwtarg.h"

#if S14DATA_SUPPORT_CRC

#if S14DATA_SUPPORT_MULTICMDS
void TMWDEFS_CALLBACK s14crcna_checkMonitorPoint(TMWSCTR *pSector, S14SCTR_CMD *pContext)
{
#if S14DATA_SUPPORT_MST
  void *pMonitoredPoint;
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  /* See if point is being monitored */
  if((pMonitoredPoint = s14data_crcGetMonitoredPoint(pContext->pPoint)) != TMWDEFS_NULL)
  {
    /* Get current time */
    TMWDTIME timeStamp;
    s14util_getDateTime(pSector, &timeStamp);

    /* Add event with remote cause of transmission */    
    s14mst_internalAddEvent(pSector, S14UTIL_MODE_TO_REMLOC_COT(p14Sector->localMode), 
       s14data_mstGetInfoObjAddr(pMonitoredPoint), 
       s14data_mstGetValueAndState(pMonitoredPoint),
       s14data_mstGetFlags(pMonitoredPoint), &timeStamp);
  }
#endif
}

/* function: s14crcna_processRequest */
void TMWDEFS_CALLBACK s14crcna_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  S14SCTR *p14Sector;
  S14SCTR_CMD *pContext;
  TMWTYPES_BOOL timerIsActive;
  TMWTYPES_ULONG ioa;
  TMWTYPES_ULONG oldIOA;
  TMWTYPES_UCHAR oldRCO;
  
  pContext = s14util_processMultiRequest(pSector, pMsg, I14DEF_TYPE_CRCNA1, &ioa, &timerIsActive);
  if(pContext == TMWDEFS_NULL)
    return;

  /* Command to call for status of this point */
  pContext->pStatus = s14data_crcStatus;

  /* Save old values in case this is an execute and we need to compare with select values.
   * All of the error paths require the p14Sector fields to be filled in with rcvd values.
   */
  oldIOA = pContext->ioa;
  oldRCO = pContext->qualifier; 

  /* If only one context per typeId is allowed, this might be cancelling and replacing the previous ioa */
  pContext->ioa = ioa;

  /* Parse RCO */
  pContext->qualifier = pMsg->pRxData->pMsgBuf[pMsg->offset++]; 
  pContext->useQualifier = TMWDEFS_TRUE;

  /* Parse received cause of transmission */
  if(pMsg->cot == I14DEF_COT_ACTIVATION)
  {
    pContext->cot = I14DEF_COT_ACTCON;
  }
  else if(pMsg->cot == I14DEF_COT_DEACTIVATION)
  {
    /* Any deactivate will cause the select to be cancelled, even an incorrect one 
     * There is currently not an s14data function to be called indicating deactivate.
     * 
     * The 101 and 601 specs are unclear about whether a DEACTCON positive or negative 
     * should be sent on error. Since even an incorrect DEACT results in no select being
     * active, a positive reply will be sent (except if ioa is bad below).
     */
    pContext->cot = I14DEF_COT_DEACTCON;
  }
  else
  {
    pContext->cot = I14DEF_COT_UNKNOWN_COT | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* Lookup data point using information object address */
  p14Sector = (S14SCTR *)pSector;
  pContext->pPoint = s14data_crcLookupPoint(p14Sector->i870.pDbHandle, ioa);
  if(pContext->pPoint == TMWDEFS_NULL)
  {
    pContext->cot = I14DEF_COT_UNKNOWN_IOA | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* if deactivate, context->cot was set to DEACTCON above. */
  if(pMsg->cot == I14DEF_COT_DEACTIVATION)
  {
    return;
  }

  /* Perform select or execute and set operation status */
  if((pContext->qualifier & I14DEF_QOC_SE_SELECT) != 0)
  {
    pContext->state = TMWDEFS_CMD_STATE_SELECTING;
    pContext->status = s14data_crcSelect(pContext->pPoint, 
      pMsg->cot, pContext->qualifier);
  }
  else
  {
    /* If a select is required for this point make sure a valid select
     * operation is pending and compare the execute parameters against
     * the select parameters.
     */
    if(s14data_crcSelectRequired(pContext->pPoint))
    {
      /* Make sure select is active */
      /* Compare information object address */
      /* Compare qualifier, ignore the select bit */
      if(!timerIsActive
         ||(pContext->ioa != oldIOA)
         ||(pContext->qualifier != (oldRCO & ~I14DEF_QOC_SE_MASK)))
      {
        pContext->cot |= I14DEF_COT_NEGATIVE_CONFIRM;
        return;
      }
    }
  
    pContext->pCheckMonitoredPoint = s14crcna_checkMonitorPoint;
 
    pContext->state = TMWDEFS_CMD_STATE_EXECUTING;
    pContext->status = s14data_crcExecute(pContext->pPoint, 
      pMsg->cot, pContext->qualifier);
  }
}

#else

/* function: _checkStatus */
static TMWDEFS_COMMAND_STATUS TMWDEFS_CALLBACK _checkStatus(
  TMWSCTR *pSector)
{
  return (s14crcna_checkStatus(pSector));
}

/* function: crcna_checkStatus */
TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14crcna_checkStatus(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  switch(p14Sector->crcnaState)
  {
  case TMWDEFS_CMD_STATE_SELECTING:
    {  
      if((p14Sector->crcnaStatus != TMWDEFS_CMD_STAT_SUCCESS) 
        && (p14Sector->crcnaStatus != TMWDEFS_CMD_STAT_FAILED))
      {
        /* Get Status */
        p14Sector->crcnaStatus = s14data_crcStatus(p14Sector->pCRCPoint);
      }

      /* If select is complete start select timer */
      if(p14Sector->crcnaStatus == TMWDEFS_CMD_STAT_SUCCESS)
      {
        tmwtimer_start(&p14Sector->crcnaSelectTimer, 
          p14Sector->selectTimeout, pSector->pSession->pChannel, 
          TMWDEFS_NULL, TMWDEFS_NULL);
      }
    }
    break;

  case TMWDEFS_CMD_STATE_EXECUTING:
    { 
      if((p14Sector->crcnaStatus != TMWDEFS_CMD_STAT_SUCCESS) 
        && (p14Sector->crcnaStatus != TMWDEFS_CMD_STAT_FAILED))
      {
        /* Get Status */
        p14Sector->crcnaStatus = s14data_crcStatus(p14Sector->pCRCPoint);
      }

      /* If execute is complete change state to monitoring and status
       * to in progress so we will check the monitored state at least
       * once.
       */
      if((p14Sector->crcnaStatus == TMWDEFS_CMD_STAT_SUCCESS)
        || (p14Sector->crcnaStatus == TMWDEFS_CMD_STAT_MONITORING))
      {
        p14Sector->crcnaState = TMWDEFS_CMD_STATE_MONITORING;
        p14Sector->crcnaStatus = TMWDEFS_CMD_STAT_MONITORING;
        return(TMWDEFS_CMD_STAT_SUCCESS);
      }
    }
    break;

  case TMWDEFS_CMD_STATE_MONITORING:
    { 
      if((p14Sector->crcnaStatus != TMWDEFS_CMD_STAT_SUCCESS) 
        && (p14Sector->crcnaStatus != TMWDEFS_CMD_STAT_FAILED))
      {
        /* Get Status */
        p14Sector->crcnaStatus = s14data_crcStatus(p14Sector->pCRCPoint);
      }

      /* Check monitored feedback point status */
      if(p14Sector->crcnaStatus == TMWDEFS_CMD_STAT_SUCCESS)
      {
        void *pMonitoredPoint;

        /* If monitor is complete change state to idle */
        p14Sector->crcnaState = TMWDEFS_CMD_STATE_IDLE;

#if S14DATA_SUPPORT_MST
        /* See if point is being monitored */
        if((pMonitoredPoint = s14data_crcGetMonitoredPoint(p14Sector->pCRCPoint)) != TMWDEFS_NULL)
        {
          /* Get current time */
          TMWDTIME timeStamp;
          s14util_getDateTime(pSector, &timeStamp);

          /* Add event with remote cause of transmission */
          s14mst_internalAddEvent(pSector, S14UTIL_MODE_TO_REMLOC_COT(p14Sector->localMode), 
            s14data_mstGetInfoObjAddr(pMonitoredPoint), 
            s14data_mstGetValueAndState(pMonitoredPoint),
            s14data_mstGetFlags(pMonitoredPoint), &timeStamp);
        }
#endif
      }
    }
    break;

  default:
    break;
  }

  /* If command failed return to IDLE state */
  if(p14Sector->crcnaStatus == TMWDEFS_CMD_STAT_FAILED)
    p14Sector->crcnaState = TMWDEFS_CMD_STATE_IDLE;

  return(p14Sector->crcnaStatus);
}

/* function: s14crcna_processRequest */
void TMWDEFS_CALLBACK s14crcna_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  S14SCTR *p14Sector;
  TMWTYPES_BOOL timerIsActive;
  TMWTYPES_ULONG oldIOA;
  TMWTYPES_UCHAR oldRCO;

  p14Sector = (S14SCTR *)pSector;

  /* Any new or failed command cancels the select timer 
   * Save the timer state in case this is a valid execute command
   */
  timerIsActive = tmwtimer_isActive(&p14Sector->crcnaSelectTimer);
  if(timerIsActive)
  {
    tmwtimer_cancel(&p14Sector->crcnaSelectTimer);
  }

  /* If there is a previous command in progress, (response still to be sent)
   * queue a negative ACT CON to be sent. 
   * Don't overwrite the information from the previous command.
   */
  if((pMsg->cot == I14DEF_COT_ACTIVATION)
    && ((p14Sector->crcnaCOT != 0) || (p14Sector->crctaCOT != 0)))
  {
    s14sesn_sendNegActCon(pSector, pMsg, I14DEF_COT_ACTCON); 
    return;
  }

  /* Store originator address */
  p14Sector->crcnaOriginator = pMsg->origAddress;
  
  /* Save old values in case this is an execute and we need to compare with select values.
   * All of the error paths require the p14Sector fields to be filled in with rcvd values.
   */
  oldIOA = p14Sector->crcnaIOA;
  oldRCO = p14Sector->crcnaRCO;

  /* Parse Information Object Address */
  i870util_readInfoObjAddr(pSector->pSession, pMsg, &p14Sector->crcnaIOA);

  /* Parse RCO */
  p14Sector->crcnaRCO = pMsg->pRxData->pMsgBuf[pMsg->offset++];

  /* Parse received cause of transmission */
  if(pMsg->cot == I14DEF_COT_ACTIVATION)
  {
    p14Sector->crcnaCOT = I14DEF_COT_ACTCON;
  }
  else if(pMsg->cot == I14DEF_COT_DEACTIVATION)
  {
    /* Any deactivate will cause the select to be cancelled, even an incorrect one 
     * There is currently not a s14data function to be called indicating deactivate.
     * 
     * The 101 and 601 specs are unclear about whether a DEACTCON positive or negative 
     * should be sent on error. Since even an incorrect DEACT results in no select being
     * active, a positive reply will be sent.
     */
    p14Sector->crcnaCOT = I14DEF_COT_DEACTCON;
  }
  else
  {
    p14Sector->crcnaCOT = I14DEF_COT_UNKNOWN_COT | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* Lookup data point using information object address */
  p14Sector->pCRCPoint = s14data_crcLookupPoint(p14Sector->i870.pDbHandle, p14Sector->crcnaIOA);
  if(p14Sector->pCRCPoint == TMWDEFS_NULL)
  {
    p14Sector->crcnaCOT = I14DEF_COT_UNKNOWN_IOA | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* if deactivate, command is finished */
  if(pMsg->cot == I14DEF_COT_DEACTIVATION)
  {
    return;
  }

  /* Perform select or execute and set operation status */
  if((p14Sector->crcnaRCO & I14DEF_QOC_SE_SELECT) != 0)
  {
    p14Sector->crcnaState = TMWDEFS_CMD_STATE_SELECTING;
    p14Sector->crcnaStatus = s14data_crcSelect(p14Sector->pCRCPoint, pMsg->cot, p14Sector->crcnaRCO);
  }
  else
  {
    /* If a select is required for this point make sure a valid select
     * operation is pending and compare the execute parameters against
     * the select parameters.
     */
    if(s14data_crcSelectRequired(p14Sector->pCRCPoint))
    {
      /* Make sure select is active */
      /* Compare information object address */
      /* Compare qualifier, ignore the select bit */
      if(!timerIsActive
         ||(p14Sector->crcnaIOA != oldIOA)
         ||((p14Sector->crcnaRCO != (oldRCO & ~I14DEF_QOC_SE_MASK))))
      {
        p14Sector->crcnaCOT |= I14DEF_COT_NEGATIVE_CONFIRM;
        return;
      }
    }

    p14Sector->crcnaState = TMWDEFS_CMD_STATE_EXECUTING;
    p14Sector->crcnaStatus = s14data_crcExecute(p14Sector->pCRCPoint, pMsg->cot, p14Sector->crcnaRCO);
  }
}

/* function: s14crcna_buildResponse */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14crcna_buildResponse(
  TMWSCTR *pSector, 
  TMWTYPES_BOOL buildResponse)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  return(s14util_buildCommandResponse(pSector, buildResponse, 
    I14DEF_TYPE_CRCNA1, &p14Sector->crcnaCOT, p14Sector->crcnaOriginator, 
    p14Sector->crcnaIOA, p14Sector->crcnaRCO, TMWDEFS_TRUE, TMWDEFS_NULL, 0, TMWDEFS_NULL, 
    _checkStatus));
}
#endif
#endif /* S14DATA_SUPPORT_CRC */
