/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14pacna.c
 * description: IEC 60870-5-101/104 slave PACNA (Parameter Activation)
 *  functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14pacna.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/i870chnl.h"
#include "tmwscl/utils/tmwtarg.h"

#if S14DATA_SUPPORT_PACNA 

/* function: s14pacna_processRequest */
void TMWDEFS_CALLBACK s14pacna_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  void *pPoint;

  /* Store originator address */
  p14Sector->pacnaOriginator = pMsg->origAddress;
  
  /* Parse Information Object Address */
  i870util_readInfoObjAddr(pSector->pSession, pMsg, &p14Sector->pacnaIOA);

  /* Parse Qualifier of Parameter Activation */
  p14Sector->pacnaQPA = pMsg->pRxData->pMsgBuf[pMsg->offset++];

  /* Parse received cause of transmission */
  if(pMsg->cot == I14DEF_COT_ACTIVATION)
  {
    p14Sector->pacnaCOT = I14DEF_COT_ACTCON;
  }
  else if(pMsg->cot == I14DEF_COT_DEACTIVATION)
  {
    p14Sector->pacnaCOT = I14DEF_COT_DEACTCON;
  }
  else
  {
    p14Sector->pacnaCOT = I14DEF_COT_UNKNOWN_COT | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* Lookup point */
  if((pPoint = s14data_pacnaLookupPoint(p14Sector->i870.pDbHandle, p14Sector->pacnaIOA)) == TMWDEFS_NULL)
  {
    p14Sector->pacnaCOT = I14DEF_COT_UNKNOWN_IOA | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* if deactivate, command is finished */
  if(pMsg->cot == I14DEF_COT_DEACTIVATION)
  {
    return;
  }

  /* Attempt to perform requested operation */
  if(!s14data_pacnaStore(pPoint, pMsg->cot, p14Sector->pacnaQPA))
    p14Sector->pacnaCOT |= I14DEF_COT_NEGATIVE_CONFIRM;
}

/* function: s14pacna_buildResponse */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14pacna_buildResponse(
  TMWSCTR *pSector, 
  TMWTYPES_BOOL buildResponse)
{
  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  if(p14Sector->pacnaCOT != 0)
  {
    if(buildResponse)
    {
      /* Activation confirmation or Deactivation */
      TMWSESN_TX_DATA *pTxData;
      S14SESN *pS14Session = (S14SESN *)pSector->pSession;

      if((pTxData = i870chnl_newTxData(pSector->pSession->pChannel, 
        pSector->pSession, pSector, pS14Session->maxASDUSize)) == TMWDEFS_NULL)
      {
        return(TMWDEFS_FALSE);
      }

#if TMWCNFG_SUPPORT_DIAG
      pTxData->pMsgDescription = "Parameter Activation Response";
#endif
      pTxData->txFlags = TMWSESN_TXFLAGS_NO_RESPONSE;
      pTxData->responseTimeout = p14Sector->defaultResponseTimeout;

      /* Build response */
      i870util_buildMessageHeader(pSector->pSession, 
        pTxData, I14DEF_TYPE_PACNA1, p14Sector->pacnaCOT, 
        p14Sector->pacnaOriginator, p14Sector->i870.asduAddress);

      /* Store Information Object Address */
      i870util_storeInfoObjAddr(pSector->pSession, pTxData, p14Sector->pacnaIOA);

      /* Store Qualifier of Parameter Activation */
      pTxData->pMsgBuf[pTxData->msgLength++] = p14Sector->pacnaQPA;

      /* Request is complete */
      p14Sector->pacnaCOT = 0;

      /* Send the response */
      i870chnl_sendMessage(pTxData);
    }

    return(TMWDEFS_TRUE);
  }

  return(TMWDEFS_FALSE);
}

#endif /* S14DATA_SUPPORT_PACNA */
