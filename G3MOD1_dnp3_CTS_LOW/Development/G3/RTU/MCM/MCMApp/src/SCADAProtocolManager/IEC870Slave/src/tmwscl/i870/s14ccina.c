/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14ccina.c
 * description: IEC 60870-5-101 slave CCINA (ccina interrogation)
 *  functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14ccina.h"
#include "tmwscl/i870/s14dbas.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/s14mit.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/i870chnl.h"
#include "tmwscl/i870/i14diag.h"

#if S14DATA_SUPPORT_CCINA

static TMWTYPES_UCHAR TMWDEFS_LOCAL _convertQccToCot(
  TMWTYPES_UCHAR qcc)
{
  switch(qcc & I14DEF_QCC_RQT_MASK) {
  case 1:
    return(I14DEF_COT_REQCO1);
  case 2:
    return(I14DEF_COT_REQCO2);
  case 3:
    return(I14DEF_COT_REQCO3);
  case 4:
    return(I14DEF_COT_REQCO4);
  default:
    return(I14DEF_COT_REQCOGEN);
  }
}

void TMWDEFS_CALLBACK s14ccina_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  TMWTYPES_UCHAR ccinaGroup;

  /* Store originator address */
  p14Sector->ccinaOriginator = pMsg->origAddress;
 
  /* Parse Information Object Address */
  i870util_readInfoObjAddr(pSector->pSession, pMsg, &p14Sector->ccinaIOA);

  /* Parse qualifier of ccina command */
  p14Sector->ccinaQCC = pMsg->pRxData->pMsgBuf[pMsg->offset++];

  /* Parse received cause of transmission */
  if(pMsg->cot == I14DEF_COT_ACTIVATION)
    p14Sector->ccinaCOT = I14DEF_COT_ACTCON;
  else
  {
    p14Sector->ccinaCOT = I14DEF_COT_UNKNOWN_COT | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  if(p14Sector->ccinaIOA != 0)
  {
    p14Sector->ccinaCOT = I14DEF_COT_UNKNOWN_IOA | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  ccinaGroup = (TMWTYPES_UCHAR)(p14Sector->ccinaQCC & I14DEF_QCC_RQT_MASK);
  if((ccinaGroup < I14DEF_QCC_RQT_GROUP1)
    || (ccinaGroup > I14DEF_QCC_RQT_GENERAL))
  {
    p14Sector->ccinaCOT |= I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* Initialize for ccina command response */
  p14Sector->ccinaGroupIndex = 0;
  p14Sector->ccinaPointIndex = 0;
  p14Sector->ccinaDataReady = TMWDEFS_FALSE;

  /* Process counter freeze and/or reset */
  if((p14Sector->ccinaQCC & I14DEF_QCC_FRZ_MASK) != I14DEF_QCC_FRZ_READ_ONLY)
  {
    if(ccinaGroup == I14DEF_QCC_RQT_GENERAL)
      ccinaGroup = TMWDEFS_GROUP_MASK_GENERAL;
    else
      ccinaGroup = 0x01 << ccinaGroup;

    if(!s14mit_processFreeze(pSector, p14Sector->ccinaQCC, ccinaGroup))
      p14Sector->ccinaCOT |= I14DEF_COT_NEGATIVE_CONFIRM;
  }
}

TMWTYPES_BOOL TMWDEFS_CALLBACK s14ccina_buildResponse(
  TMWSCTR *pSector, 
  TMWTYPES_BOOL buildResponse)
{
  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  /* See if a ccina Interrogation request is pending */
  if(p14Sector->ccinaCOT != 0)
  {

    /* If COT is ACTTERM this means it will try to read counters,
     * See if database said it was ready yet.
     * Send ACTCON or DEACTCON right away.
     */
    if((p14Sector->ccinaCOT == I14DEF_COT_ACTTERM) 
      &&(!p14Sector->ccinaDataReady))
    {
      p14Sector->ccinaDataReady = s14data_dataReady(p14Sector->i870.pDbHandle, I14DEF_TYPE_CCINA1);
   
      if(!p14Sector->ccinaDataReady)
      {
        /* Database is not ready, so no Counter Interrogation data to send at this time */
        return(TMWDEFS_FALSE);
      }
    }

    /* See if we need to actually build the response */
    if(buildResponse)
    {
      TMWSESN_TX_DATA *pTxData;
      S14SESN *pS14Session = (S14SESN *)pSector->pSession;
  
      if((pTxData = i870chnl_newTxData(pSector->pSession->pChannel, pSector->pSession, pSector,
        pS14Session->maxASDUSize)) == TMWDEFS_NULL)
      {
        return(TMWDEFS_FALSE);
      }

#if TMWCNFG_SUPPORT_DIAG
      pTxData->pMsgDescription = "Counter Interrogation Response";
#endif
      pTxData->txFlags = TMWSESN_TXFLAGS_NO_RESPONSE;
      pTxData->responseTimeout = p14Sector->defaultResponseTimeout;
      
      /* Generate appropriate response based on current state */
      if(p14Sector->ccinaCOT != I14DEF_COT_ACTTERM)
      {
        /* Activation confirmation or Deactivation */

        /* Build response */
        i870util_buildMessageHeader(pSector->pSession, 
          pTxData, I14DEF_TYPE_CCINA1, p14Sector->ccinaCOT, 
          p14Sector->ccinaOriginator, p14Sector->i870.asduAddress);

        /* Store point number */
        i870util_storeInfoObjAddr(pSector->pSession, pTxData, p14Sector->ccinaIOA);

        /* Qualifier of Interrogation */
        pTxData->pMsgBuf[pTxData->msgLength++] = p14Sector->ccinaQCC;

        /* If activation confirmation and qualifier is read only
         *  proceed to actterm. If qualifier is not read only this was
         *  a counter freeze/reset command and we do not need actterm.
         */
        if((p14Sector->ccinaCOT == I14DEF_COT_ACTCON)
          && ((p14Sector->ccinaQCC & I14DEF_QCC_FRZ_MASK) == I14DEF_QCC_FRZ_READ_ONLY))
        {
          p14Sector->ccinaCOT = I14DEF_COT_ACTTERM;
        }
        else
        {
          p14Sector->ccinaCOT = 0;
        }

        i870chnl_sendMessage(pTxData);
      }
      else
      {
        TMWTYPES_BOOL dataSent = TMWDEFS_FALSE;
        TMWTYPES_ULONG groupMask;
        
        if(p14Sector->ccinaQCC == I14DEF_QCC_RQT_GENERAL)
          groupMask = TMWDEFS_GROUP_MASK_GENERAL;
        else
          groupMask = 0x01 << p14Sector->ccinaQCC;

        if(s14dbas_readCounterGroup(
          pTxData, pSector, _convertQccToCot(p14Sector->ccinaQCC), groupMask, 
          &p14Sector->ccinaGroupIndex, &p14Sector->ccinaPointIndex))
        {
          dataSent = TMWDEFS_TRUE;
          i870chnl_sendMessage(pTxData);
        }

        /* No data returned above so send ACTTERM */
        if(!dataSent)
        {
          /* Build ACTTERM */
          i870util_buildMessageHeader(pSector->pSession, pTxData, 
            I14DEF_TYPE_CCINA1, p14Sector->ccinaCOT, 
            p14Sector->ccinaOriginator, p14Sector->i870.asduAddress);

          p14Sector->ccinaCOT = 0;

          /* Store point number, always 0 for ACTTERM */
          i870util_storeInfoObjAddr(pSector->pSession, pTxData, 0);

          /* Qualifier of Counter Interrogation */
          pTxData->pMsgBuf[pTxData->msgLength++] = p14Sector->ccinaQCC;

          /* Send response */
          i870chnl_sendMessage(pTxData);
        }
      }
    }

    /* Return true (i.e. data available, or sent) */
    return(TMWDEFS_TRUE);
  }

  return(TMWDEFS_FALSE);
}

#endif /* S14DATA_SUPPORT_CCINA */
