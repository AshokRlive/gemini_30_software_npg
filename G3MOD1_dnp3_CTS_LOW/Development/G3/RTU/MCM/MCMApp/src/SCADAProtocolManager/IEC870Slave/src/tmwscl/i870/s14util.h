/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14util.h
 * description: This file is intended for internal SCL use only.
 *  IEC 60870-5-101/104 slave utilities.
 */
#ifndef S14UTIL_DEFINED
#define S14UTIL_DEFINED

#include "tmwscl/i870/s14dbas.h"
#include "tmwscl/utils/tmwsesn.h"

typedef void *(*S1UTIL_GET_POINT_FUNC)(
  void *pHandle, 
  TMWTYPES_USHORT index);

typedef TMWDEFS_GROUP_MASK (*S1UTIL_GET_GROUP_FUNC)(
  void *);

typedef TMWTYPES_ULONG (*S1UTIL_GET_IOA_FUNC)(
  void *);

typedef TMWTYPES_BOOL (*S1UTIL_GET_INDEXED_FUNC)(
  void *);

typedef TMWDEFS_TIME_FORMAT (*S1UTIL_GET_TIME_FORMAT_FUNC)(
  void *);

typedef void (*S1UTIL_STORE_RESP_FUNC)(
  TMWSESN_TX_DATA *pTxData, 
  void *pPoint,
  TMWDEFS_TIME_FORMAT timeFormat);

typedef TMWDEFS_COMMAND_STATUS (*S1UTIL_STATUS_FUNC)(
  TMWSCTR *pSector);

typedef TMWDEFS_COMMAND_STATUS (*S14UTIL_MULTISTATUS_FUNC)(
  void *pPoint);


/* MACRO to determine whether to send COT 11 or 12 when event is automatically 
 * generated for a monitor point after a command is received from the master
 */
#define S14UTIL_MODE_TO_REMLOC_COT(localMode)  ((localMode) ? I14DEF_COT_RETURN_LOCAL : I14DEF_COT_RETURN_REMOTE)
 
#ifdef __cplusplus
extern "C" {
#endif

  /* function: s14util_determineAsduType */
  TMWTYPES_UCHAR TMWDEFS_GLOBAL s14util_determineAsduType(
    TMWTYPES_UCHAR typeId, 
    TMWDEFS_TIME_FORMAT timeFormat);

  /* function: s14util_readIntoResponse */
  S14DBAS_GROUP_STATUS TMWDEFS_GLOBAL s14util_readIntoResponse(
    TMWSESN_TX_DATA *pTxData, 
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot, 
    TMWDEFS_GROUP_MASK groupMask, 
    TMWTYPES_ULONG ioa, 
    TMWTYPES_UCHAR typeId, 
    TMWDEFS_TIME_FORMAT timeFormat,
    TMWTYPES_UCHAR dataSize, 
    TMWTYPES_USHORT *pPointIndex,
    S1UTIL_GET_POINT_FUNC pGetPoint, 
    S1UTIL_GET_GROUP_FUNC pGetGroupMask,
    S1UTIL_GET_IOA_FUNC pGetIOA, 
    S1UTIL_GET_INDEXED_FUNC pGetIndexed,
    S1UTIL_GET_TIME_FORMAT_FUNC pGetTimeFormat,
    S1UTIL_STORE_RESP_FUNC pStoreResp);

  /* function: s14util_buildCommandResponse */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14util_buildCommandResponse(
    TMWSCTR *pSector, 
    TMWTYPES_BOOL buildResponse, 
    TMWTYPES_UCHAR typeId,
    TMWTYPES_UCHAR *pCOT, 
    TMWTYPES_UCHAR originatorAddress, 
    TMWTYPES_ULONG ioa, 
    TMWTYPES_UCHAR qualifier, 
    TMWTYPES_BOOL useQualifier,
    void *pData, 
    TMWTYPES_UCHAR numDataBytes, 
    TMWDTIME *pDateTime, 
    S1UTIL_STATUS_FUNC pStatus);

  /* function: s14util_getDateTime */
  TMWDEFS_SCL_API void TMWDEFS_GLOBAL s14util_getDateTime(
    TMWSCTR *pSector, 
    TMWDTIME *pDateTime);

  /* function: s14util_alwaysUseIndexed */
  TMWTYPES_BOOL TMWDEFS_CALLBACK s14util_alwaysUseIndexed(
    void *pPoint);
   
#if S14DATA_SUPPORT_MULTICMDS
  /* function: s14util_findCmdCtxt */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14util_findCmdCtxt(
    S14SCTR *pS14Sector, 
    TMWTYPES_UCHAR typeId, 
    TMWTYPES_ULONG ioa, 
    S14SCTR_CMD **pRetContext); 

  /* function: s14util_freeCmdCtxt */
  void TMWDEFS_GLOBAL s14util_freeCmdCtxt(
    S14SCTR *p14Sector, 
    S14SCTR_CMD*pContext);

  /* function: s14util_processMultiRequest */
  S14SCTR_CMD * TMWDEFS_GLOBAL s14util_processMultiRequest(
    TMWSCTR *pSector, 
    I870UTIL_MESSAGE *pMsg,
    TMWTYPES_UCHAR typeId,
    TMWTYPES_ULONG *pIOA,
    TMWTYPES_BOOL  *pTimerIsActive);
  
  /* function: s14util_buildMultiResponse */
  TMWTYPES_BOOL TMWDEFS_CALLBACK s14util_buildMultiResponse(
    TMWSCTR *pSector, 
    TMWTYPES_BOOL buildResponse);
#endif

#ifdef __cplusplus
}
#endif
#endif /* S14UTIL_DEFINED */
