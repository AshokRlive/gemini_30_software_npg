/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i14diag.c
 * description: IEC 60870-5-101/104 Diagnostics
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/utils/tmwtarg.h"
#include "tmwscl/utils/tmwcnfg.h"
#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/utils/tmwdiag.h"

#include "tmwscl/i870/i14diag.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/i870sctr.h"

#if TMWCNFG_SUPPORT_DIAG

/* Maximum number of bytes to display on a single row */
#define MAX_ROW_LENGTH 16

/* function: _cotToHdrId */
static TMWDIAG_ID TMWDEFS_LOCAL _cotToHdrId(TMWTYPES_UCHAR cot)
{
  switch(cot)
  {
  case I14DEF_COT_SPONTANEOUS:
    return(TMWDIAG_ID_EVENT_HDRS);

  case I14DEF_COT_CYCLIC:
    return(TMWDIAG_ID_CYCLIC_HDRS);

  case I14DEF_COT_BACKGROUND:
    return(TMWDIAG_ID_CYCLIC_HDRS);

  case I14DEF_COT_NOT_DEFINED:
    return(0);

  default:
    return(TMWDIAG_ID_STATIC_HDRS);
  }
}

/* function: _cotToDataId */
static TMWDIAG_ID TMWDEFS_LOCAL _cotToDataId(TMWTYPES_UCHAR cot)
{
  switch(cot)
  {
  case I14DEF_COT_SPONTANEOUS:
    return(TMWDIAG_ID_EVENT_DATA);

  case I14DEF_COT_CYCLIC:
    return(TMWDIAG_ID_CYCLIC_DATA);

  case I14DEF_COT_BACKGROUND:
    return(TMWDIAG_ID_CYCLIC_DATA);

  case I14DEF_COT_NOT_DEFINED:
    return(TMWDIAG_ID_MMI);

  default:
    return(TMWDIAG_ID_STATIC_DATA);
  }
}

/* function: _kindString */
static const char * TMWDEFS_LOCAL _kindString(
  TMWTYPES_UCHAR qpm)
{
  switch(qpm & I14DEF_QPM_KPA_MASK)
  {
  case I14DEF_QPM_KPA_THRESHOLD:
    return("Threshold");
  case I14DEF_QPM_KPA_SMOOTHING:
    return("Smoothing");
  case I14DEF_QPM_KPA_LOW_LIMIT:
    return("Low Limit");
  case I14DEF_QPM_KPA_HIGH_LIMIT:
    return("High Limit");
  }

  return("Unknown Type");
}

/* function: _cotString */
static const char * TMWDEFS_LOCAL _cotString(
  TMWTYPES_UCHAR cot, 
  char *cotBuf,
  TMWTYPES_USHORT cotBufLen)
{
  int curBufLen;
  curBufLen = tmwtarg_snprintf(cotBuf, cotBufLen, "%d", cot);

  /* mask off Test Bit */
  switch(cot & 0x7f)
  {
    case(I14DEF_COT_CYCLIC):       
      curBufLen += tmwtarg_snprintf(cotBuf + curBufLen, cotBufLen - curBufLen, "%s", ", periodic, cyclic");
      break;

    case(I14DEF_COT_BACKGROUND):
      curBufLen +=tmwtarg_snprintf(cotBuf + curBufLen, cotBufLen - curBufLen, "%s", ", background scan");
      break;

    case(I14DEF_COT_SPONTANEOUS):  
      curBufLen += tmwtarg_snprintf(cotBuf + curBufLen, cotBufLen - curBufLen, "%s", ", spontaneous");
      break;

    case(I14DEF_COT_INITIALIZED):  
      curBufLen += tmwtarg_snprintf(cotBuf + curBufLen, cotBufLen - curBufLen, "%s", ", initialized");
      break;

    case(I14DEF_COT_REQUEST):   
      curBufLen += tmwtarg_snprintf(cotBuf + curBufLen, cotBufLen - curBufLen, "%s", ", request or requested");
      break;

    case(I14DEF_COT_ACTIVATION):
      curBufLen += tmwtarg_snprintf(cotBuf + curBufLen, cotBufLen - curBufLen, "%s", ", activation");
      break;

    case(I14DEF_COT_ACTCON):    
      curBufLen += tmwtarg_snprintf(cotBuf + curBufLen, cotBufLen - curBufLen, "%s", ", positive activation confirmation");
      break;

    case(I14DEF_COT_ACTCON | I14DEF_COT_NEGATIVE_CONFIRM):
      curBufLen += tmwtarg_snprintf(cotBuf + curBufLen, cotBufLen - curBufLen, "%s", ", negative activation confirmation");
      break;

    case(I14DEF_COT_DEACTIVATION):
      curBufLen += tmwtarg_snprintf(cotBuf + curBufLen, cotBufLen - curBufLen, "%s", ", deactivation");
      break;

    case(I14DEF_COT_DEACTCON):
      curBufLen += tmwtarg_snprintf(cotBuf + curBufLen, cotBufLen - curBufLen, "%s", ", positive deactivation confirmation"); 
      break;

    case(I14DEF_COT_DEACTCON | I14DEF_COT_NEGATIVE_CONFIRM):
      curBufLen += tmwtarg_snprintf(cotBuf + curBufLen, cotBufLen - curBufLen, "%s", ", negative deactivation confirmation");
      break;

    case(I14DEF_COT_ACTTERM):    
      curBufLen += tmwtarg_snprintf(cotBuf + curBufLen, cotBufLen - curBufLen, "%s", ", activation termination"); 
      break;

    case(I14DEF_COT_RETURN_REMOTE): 
      curBufLen += tmwtarg_snprintf(cotBuf + curBufLen, cotBufLen - curBufLen, "%s", ", return information caused by a remote command"); 
      break;

    case(I14DEF_COT_RETURN_LOCAL):  
      curBufLen += tmwtarg_snprintf(cotBuf + curBufLen, cotBufLen - curBufLen, "%s", ", return information caused by a local command");  
      break;

    case(I14DEF_COT_FILE_XFER):  
      curBufLen += tmwtarg_snprintf(cotBuf + curBufLen, cotBufLen - curBufLen, "%s", ", file transfer");       
      break;

    case(I14DEF_COT_INTG_GEN):   
      curBufLen += tmwtarg_snprintf(cotBuf + curBufLen, cotBufLen - curBufLen, "%s", ", interrogated by station interrogation"); 
      break;

    case(I14DEF_COT_INTG1):
    case(I14DEF_COT_INTG2):
    case(I14DEF_COT_INTG3):
    case(I14DEF_COT_INTG4):
    case(I14DEF_COT_INTG5):
    case(I14DEF_COT_INTG6):
    case(I14DEF_COT_INTG7):
    case(I14DEF_COT_INTG8):
    case(I14DEF_COT_INTG9):
    case(I14DEF_COT_INTG10):
    case(I14DEF_COT_INTG11):
    case(I14DEF_COT_INTG12):
    case(I14DEF_COT_INTG13):
    case(I14DEF_COT_INTG14):
    case(I14DEF_COT_INTG15):
    case(I14DEF_COT_INTG16):
      curBufLen += tmwtarg_snprintf(cotBuf + curBufLen , cotBufLen - curBufLen, ", interrogated by group %u interrogation", cot - 20);
      break;

    case(I14DEF_COT_REQCOGEN):  
      curBufLen += tmwtarg_snprintf(cotBuf + curBufLen, cotBufLen - curBufLen, "%s", ", requested by general counter request"); 
      break;

    case(I14DEF_COT_REQCO1):
    case(I14DEF_COT_REQCO2):
    case(I14DEF_COT_REQCO3):
    case(I14DEF_COT_REQCO4):
      curBufLen += tmwtarg_snprintf(cotBuf + curBufLen, cotBufLen - curBufLen, ", requested by group %u counter request", cot - 37);
      break;

    /* Error condition COTs defined in 60870-5-101 addendum 2 */
    case(I14DEF_COT_UNKNOWN_ASDU_TYPE | I14DEF_COT_NEGATIVE_CONFIRM): 
      curBufLen += tmwtarg_snprintf(cotBuf + curBufLen, cotBufLen - curBufLen, "%s", ", unknown Type ID");
      break;

    case(I14DEF_COT_UNKNOWN_COT | I14DEF_COT_NEGATIVE_CONFIRM):    
      curBufLen += tmwtarg_snprintf(cotBuf + curBufLen, cotBufLen - curBufLen, "%s", ", unknown cause of transmission"); 
      break;

    case(I14DEF_COT_UNKNOWN_ASDU_ADDR | I14DEF_COT_NEGATIVE_CONFIRM): 
      curBufLen += tmwtarg_snprintf(cotBuf + curBufLen, cotBufLen - curBufLen, "%s", ", unknown common address of ASDU"); 
      break;

    case(I14DEF_COT_UNKNOWN_IOA | I14DEF_COT_NEGATIVE_CONFIRM):    
      curBufLen += tmwtarg_snprintf(cotBuf + curBufLen, cotBufLen - curBufLen, "%s", ", unknown information object address"); 
      break;

    default: 
      curBufLen += tmwtarg_snprintf(cotBuf + curBufLen, cotBufLen - curBufLen, "%s", ", Unknown");
      break;
  }
  if(cot & I14DEF_COT_TEST)
  {
    /* Test flag in COT is set */
    tmwtarg_snprintf(cotBuf + curBufLen, cotBufLen - curBufLen, "%s", " (TEST BIT == 1)");
  }

  return(cotBuf);
}

/* function: _typeString */
static const char * TMWDEFS_LOCAL _typeString(
  TMWTYPES_UCHAR typeId)
{
  switch(typeId)
  {
  case I14DEF_TYPE_LINK_RESET: return(I14DEF_TSTR_LINK_RESET);
  case I14DEF_TYPE_MMETB1:     return(I14DEF_TSTR_MMETB1);
  case I14DEF_TYPE_MMENC1:     return(I14DEF_TSTR_MMENC1);
  case I14DEF_TYPE_MMETC1:     return(I14DEF_TSTR_MMETC1);
  case I14DEF_TYPE_MITNA1:     return(I14DEF_TSTR_MITNA1);
  case I14DEF_TYPE_MITTA1:     return(I14DEF_TSTR_MITTA1);
  case I14DEF_TYPE_MEPTA1:     return(I14DEF_TSTR_MEPTA1);
  case I14DEF_TYPE_MSPNA1:     return(I14DEF_TSTR_MSPNA1);
  case I14DEF_TYPE_MSPTA1:     return(I14DEF_TSTR_MSPTA1);
  case I14DEF_TYPE_MDPNA1:     return(I14DEF_TSTR_MDPNA1);
  case I14DEF_TYPE_MDPTA1:     return(I14DEF_TSTR_MDPTA1);
  case I14DEF_TYPE_MSTNA1:     return(I14DEF_TSTR_MSTNA1);
  case I14DEF_TYPE_MSTTA1:     return(I14DEF_TSTR_MSTTA1);
  case I14DEF_TYPE_MBONA1:     return(I14DEF_TSTR_MBONA1);
  case I14DEF_TYPE_MBOTA1:     return(I14DEF_TSTR_MBOTA1);
  case I14DEF_TYPE_MMENA1:     return(I14DEF_TSTR_MMENA1);
  case I14DEF_TYPE_MMETA1:     return(I14DEF_TSTR_MMETA1);
  case I14DEF_TYPE_MMENB1:     return(I14DEF_TSTR_MMENB1);
  case I14DEF_TYPE_MEPTB1:     return(I14DEF_TSTR_MEPTB1);
  case I14DEF_TYPE_MEPTC1:     return(I14DEF_TSTR_MEPTC1);
  case I14DEF_TYPE_MPSNA1:     return(I14DEF_TSTR_MPSNA1);
  case I14DEF_TYPE_MMEND1:     return(I14DEF_TSTR_MMEND1);
  case I14DEF_TYPE_MSPTB1:     return(I14DEF_TSTR_MSPTB1);
  case I14DEF_TYPE_MDPTB1:     return(I14DEF_TSTR_MDPTB1);
  case I14DEF_TYPE_MSTTB1:     return(I14DEF_TSTR_MSTTB1);
  case I14DEF_TYPE_MBOTB1:     return(I14DEF_TSTR_MBOTB1);
  case I14DEF_TYPE_MMETD1:     return(I14DEF_TSTR_MMETD1);
  case I14DEF_TYPE_MMETE1:     return(I14DEF_TSTR_MMETE1);
  case I14DEF_TYPE_MMETF1:     return(I14DEF_TSTR_MMETF1);
  case I14DEF_TYPE_MITTB1:     return(I14DEF_TSTR_MITTB1);
  case I14DEF_TYPE_MEPTD1:     return(I14DEF_TSTR_MEPTD1);
  case I14DEF_TYPE_MEPTE1:     return(I14DEF_TSTR_MEPTE1);
  case I14DEF_TYPE_MEPTF1:     return(I14DEF_TSTR_MEPTF1);
  case I14DEF_TYPE_CSCNA1:     return(I14DEF_TSTR_CSCNA1);
  case I14DEF_TYPE_CDCNA1:     return(I14DEF_TSTR_CDCNA1);
  case I14DEF_TYPE_CRCNA1:     return(I14DEF_TSTR_CRCNA1);
  case I14DEF_TYPE_CSENA1:     return(I14DEF_TSTR_CSENA1);
  case I14DEF_TYPE_CSENB1:     return(I14DEF_TSTR_CSENB1);
  case I14DEF_TYPE_CSENC1:     return(I14DEF_TSTR_CSENC1);
  case I14DEF_TYPE_CBONA1:     return(I14DEF_TSTR_CBONA1);
  case I14DEF_TYPE_CSCTA1:     return(I14DEF_TSTR_CSCTA1);
  case I14DEF_TYPE_CDCTA1:     return(I14DEF_TSTR_CDCTA1);
  case I14DEF_TYPE_CRCTA1:     return(I14DEF_TSTR_CRCTA1);
  case I14DEF_TYPE_CSETA1:     return(I14DEF_TSTR_CSETA1);
  case I14DEF_TYPE_CSETB1:     return(I14DEF_TSTR_CSETB1);
  case I14DEF_TYPE_CSETC1:     return(I14DEF_TSTR_CSETC1);
  case I14DEF_TYPE_CBOTA1:     return(I14DEF_TSTR_CBOTA1);
  case I14DEF_TYPE_MEINA1:     return(I14DEF_TSTR_MEINA1);
  case I14DEF_TYPE_CICNA1:     return(I14DEF_TSTR_CICNA1);
  case I14DEF_TYPE_CCINA1:     return(I14DEF_TSTR_CCINA1);
  case I14DEF_TYPE_CRDNA1:     return(I14DEF_TSTR_CRDNA1);
  case I14DEF_TYPE_CCSNA1:     return(I14DEF_TSTR_CCSNA1);
  case I14DEF_TYPE_CTSNA1:     return(I14DEF_TSTR_CTSNA1);
  case I14DEF_TYPE_CRPNA1:     return(I14DEF_TSTR_CRPNA1);
  case I14DEF_TYPE_CCDNA1:     return(I14DEF_TSTR_CCDNA1);
  case I14DEF_TYPE_CTSTA1:     return(I14DEF_TSTR_CTSTA1);
  case I14DEF_TYPE_PMENA1:     return(I14DEF_TSTR_PMENA1);
  case I14DEF_TYPE_PMENB1:     return(I14DEF_TSTR_PMENB1);
  case I14DEF_TYPE_PMENC1:     return(I14DEF_TSTR_PMENC1);
  case I14DEF_TYPE_PACNA1:     return(I14DEF_TSTR_PACNA1);
  case I14DEF_TYPE_FFRNA1:     return(I14DEF_TSTR_FFRNA1);
  case I14DEF_TYPE_FSRNA1:     return(I14DEF_TSTR_FSRNA1);
  case I14DEF_TYPE_FSCNA1:     return(I14DEF_TSTR_FSCNA1);
  case I14DEF_TYPE_FSCNB1:     return(I14DEF_TSTR_FSCNB1);
  case I14DEF_TYPE_FLSNA1:     return(I14DEF_TSTR_FLSNA1);
  case I14DEF_TYPE_FAFNA1:     return(I14DEF_TSTR_FAFNA1);
  case I14DEF_TYPE_FSGNA1:     return(I14DEF_TSTR_FSGNA1);
  case I14DEF_TYPE_FDRTA1:     return(I14DEF_TSTR_FDRTA1);
  case I14DEF_TYPE_MITNC1:     return(I14DEF_TSTR_MITNC1);
  case I14DEF_TYPE_MITTC1:     return(I14DEF_TSTR_MITTC1);
  case I14DEF_TYPE_CSENZ1:     return(I14DEF_TSTR_CSENZ1);
  case I14DEF_TYPE_MCTNA1:     return(I14DEF_TSTR_MCTNA1);
  case I14DEF_TYPE_CCTNA1:     return(I14DEF_TSTR_CCTNA1);
  default: return("Unknown Type ID");
  }
}


/* function: _dayOfWeekString */
static const char * TMWDEFS_LOCAL _dayOfWeekString(
  TMWTYPES_UCHAR day)
{ 
  switch (day)
  {
  case 0:
    return "";
  case 1:
    return "Mon";
  case 2:
    return "Tue";
  case 3:
    return "Wed";
  case 4:
    return "Thu";
  case 5:
    return "Fri";
  case 6:
    return "Sat";
  case 7:
    return "Sun";
  default:
    return "InvalidDayOfWeek";
  }
}

/* function: _time2string */
void TMWDEFS_GLOBAL _time2string(
  const TMWDTIME *pTimeStamp, 
  TMWTYPES_CHAR *pTimeString,
  TMWTYPES_UINT bufferSize)
{
  TMWTYPES_UCHAR month;
  TMWTYPES_CHAR *invalid;
  TMWTYPES_CHAR *substituted;
  TMWTYPES_CHAR *summertime;
  
  invalid = "\0";
  if (pTimeStamp->invalid)
  {
    invalid = " (invalid)";
  }

  substituted = "\0";
  if (!pTimeStamp->genuineTime)
  {
    substituted = " (substituted time)";
  }

  summertime = "\0";
  if (pTimeStamp->dstInEffect)
  {
    summertime = " (summer)";
  }

  /* Validate month */
  month = pTimeStamp->month;
  if ((month < 1) || (month > 12))
  {
    month = 13;
  }

  /* display date string in format of ddMMMyy hh:mm:ss.mmm */
  (void)tmwtarg_snprintf(pTimeString, bufferSize, "%s%02u%3s%02u %02u:%02u:%02u.%03u%s%s%s",
    _dayOfWeekString(pTimeStamp->dayOfWeek), pTimeStamp->dayOfMonth, tmwdiag_monthNames[month - 1],
    pTimeStamp->year % 100, pTimeStamp->hour, pTimeStamp->minutes, 
    pTimeStamp->mSecsAndSecs/1000, pTimeStamp->mSecsAndSecs%1000, invalid, substituted, summertime);
}

/* function: _displayFrame */
static void TMWDEFS_LOCAL _displayFrame(
  const char *prompt, 
  TMWSESN *pSession,
  TMWSCTR *pSector, 
  TMWTYPES_USHORT address,
  const TMWTYPES_UCHAR *pBuf, 
  TMWTYPES_USHORT numBytes,
  TMWDIAG_ID direction)
{
  TMWDIAG_ANLZ_ID anlzId;
  const char *pName;
  TMWTYPES_UCHAR typeId;
  TMWTYPES_UCHAR qual;
  TMWTYPES_UCHAR cot;
  char cotBuf[64];
  char buf[256];
  int rowLength;
  int index;
  int len;
  int i;

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, pSession, pSector, TMWDIAG_ID_APPL | direction) == TMWDEFS_FALSE)
  {
    return;
  }

  typeId = pBuf[0];
  qual = pBuf[1];
  cot = pBuf[2];

  if(pSector != TMWDEFS_NULL)
  {
    pName = tmwsctr_getSectorName(pSector);
  } 
  else
  {
    pName = tmwsesn_getSessionName(pSession);  
  }

  tmwtarg_snprintf(buf, sizeof(buf), "%s %-10s Addr(%d) Sector(%d) Application Header, %s\n", 
    prompt, pName, address, 
    (pSector != TMWDEFS_NULL)? ((I870SCTR *)pSector)->asduAddress : I14DEF_BROADCAST_ADDRESS_16,
    _typeString(typeId));

  tmwdiag_skipLine(&anlzId);
  tmwdiag_putLine(&anlzId, buf);

  tmwtarg_snprintf(buf, sizeof(buf), "%16sQuantity(%d) SQ(%s) COT(%s)\n", " ", 
    qual & 0x7f, (qual & 0x80) ? "1" : "0", 
    _cotString(cot, cotBuf, sizeof(cotBuf)));

  tmwdiag_putLine(&anlzId, buf);

  index = 0;
  while(index < numBytes)
  {
    len = tmwtarg_snprintf(buf, sizeof(buf), "%16s", " ");

    rowLength = numBytes - index;
    if(rowLength > MAX_ROW_LENGTH)
      rowLength = MAX_ROW_LENGTH;

    for(i = 0; i < rowLength; i++)
      len += tmwtarg_snprintf(buf + len, sizeof(buf) - len, "%02x ", pBuf[index++]);

    tmwtarg_snprintf(buf + len, sizeof(buf) - len, "\n");
    tmwdiag_putLine(&anlzId, buf);
  }
}

void TMWDEFS_GLOBAL i14diag_formatIOA(
  TMWSCTR *pSector,
  TMWTYPES_ULONG ioa,
  TMWTYPES_CHAR *pBuf)
{
  I870SESN *pI870Session = (I870SESN*)pSector->pSession;
  TMWTYPES_UCHAR *pIOA = (TMWTYPES_UCHAR *)&ioa;

  if(pI870Session->diagFormat & I870DIAG_STRUCTURED_IOA)
  {
    if(pI870Session->infoObjAddrSize == 2)
    {
      tmwtarg_snprintf(pBuf, 32, "%d:%d", *(pIOA+1), *pIOA);
      return;
    }
    else if(pI870Session->infoObjAddrSize == 3)
    {
      tmwtarg_snprintf(pBuf, 32, "%d:%d:%d", *(pIOA+2), *(pIOA+1), *pIOA);
      return;
    }
  }

  /* if unstructured, or 1 byte IOA*/
  tmwtarg_snprintf(pBuf, 32, "%6d", ioa);
}

/* function: i14diag_showResponse */
void TMWDEFS_GLOBAL i14diag_showResponse(
  TMWSCTR *pSector, 
  TMWSESN_TX_DATA *pTxData, 
  I870UTIL_MESSAGE *pMsg)
{
  TMWSESN *pSession = pSector->pSession;
  TMWDIAG_ANLZ_ID anlzId;
  char cotBuf[64];
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, TMWDIAG_ID_USER | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }

  tmwtarg_snprintf(buf, sizeof(buf), "+++> %-10s Addr(%d) Sector(%d) Result Of Request For\n", 
    tmwsctr_getSectorName(pSector), pSession->linkAddress, pMsg->asduAddress);

  tmwdiag_skipLine(&anlzId);
  tmwdiag_putLine(&anlzId, buf);

  tmwtarg_snprintf(buf, sizeof(buf), "%16s%s\n", 
    " ", pTxData->pMsgDescription);

  tmwdiag_putLine(&anlzId, buf);

  tmwtarg_snprintf(buf, sizeof(buf), "%16sQuantity(%d) SQ(%s) COT(%s)\n", " ", 
    pMsg->quantity, (pMsg->indexed) ? "0" : "1", 
    _cotString(pMsg->cot, cotBuf, sizeof(cotBuf)));

  tmwdiag_putLine(&anlzId, buf);
}

/* function: i14diag_showStoreType */
void TMWDEFS_GLOBAL i14diag_showStoreType(
  TMWSCTR *pSector,
  I870UTIL_MESSAGE *pMsg)
{
  TMWSESN *pSession = pSector->pSession;
  TMWDIAG_ANLZ_ID anlzId;
  char cotBuf[64];
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, (_cotToHdrId(pMsg->cot) | TMWDIAG_ID_RX)) == TMWDEFS_FALSE)
  {
    return;
  }

  tmwtarg_snprintf(buf, sizeof(buf), "@@@> %-10s Addr(%d) Sector(%d) Store Type Id %d, %s\n", 
    tmwsctr_getSectorName(pSector), pSession->linkAddress, 
    pMsg->asduAddress, pMsg->typeId, 
    _typeString(pMsg->typeId));

  tmwdiag_skipLine(&anlzId);
  tmwdiag_putLine(&anlzId, buf);

  tmwtarg_snprintf(buf, sizeof(buf), "%16sQuantity(%d) SQ(%s) COT(%s)\n", " ", 
    pMsg->quantity, (pMsg->indexed) ? "0" : "1", 
    _cotString(pMsg->cot, cotBuf, sizeof(cotBuf)));

  tmwdiag_putLine(&anlzId, buf);
}

/* function: i14diag_showCOI */
void TMWDEFS_GLOBAL i14diag_showCOI(
  TMWSCTR *pSector,
  TMWTYPES_UCHAR coi)
{
  TMWDIAG_ANLZ_ID anlzId;
  char *coiString = "Unknown";
  char buf[256];

  switch(coi & 0x7f)
  {
  case I14DEF_COI_POWER_ON:
    coiString = "Power On";
    break;

  case I14DEF_COI_MANUAL_RESET:
    coiString = "Manual Reset";
    break;

  case I14DEF_COI_REMOTE_RESET:
    coiString = "Remote Reset";
    break;
  }

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, TMWDIAG_ID_STATIC_DATA | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }

  tmwtarg_snprintf(buf, sizeof(buf), "%18sCOI 0x%02x, %s - Local Parameters %s\n", " ", coi,
    coiString, ((coi & 0x80) != 0) ? "Changed" : "Unchanged");

  tmwdiag_putLine(&anlzId, buf);
}

/* function: i14diag_showMSP */
void TMWDEFS_GLOBAL i14diag_showMSP(
  TMWSCTR *pSector,
  TMWTYPES_UCHAR cot,
  TMWTYPES_ULONG ioa, 
  TMWTYPES_UCHAR flags, 
  TMWDTIME *pTimeStamp)
{
  TMWDIAG_ANLZ_ID anlzId;
  char ioaBuf[32];
  char timeBuf[64];
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, _cotToDataId(cot) | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }

  if(pTimeStamp != TMWDEFS_NULL)
    _time2string(pTimeStamp, timeBuf, sizeof(timeBuf));
  else
    tmwtarg_snprintf(timeBuf, sizeof(timeBuf), " ");

  i14diag_formatIOA(pSector, ioa, (TMWTYPES_CHAR *)ioaBuf);
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s Single Point %s = 0x%02x\n", timeBuf, ioaBuf, flags);
  tmwdiag_putLine(&anlzId, buf);
}

/* function: i14diag_showMDP */
void TMWDEFS_GLOBAL i14diag_showMDP(
  TMWSCTR *pSector,
  TMWTYPES_UCHAR cot,
  TMWTYPES_ULONG ioa, 
  TMWTYPES_UCHAR flags, 
  TMWDTIME *pTimeStamp)
{
  TMWDIAG_ANLZ_ID anlzId;
  char ioaBuf[32];
  char timeBuf[64];
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, _cotToDataId(cot) | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }

  if(pTimeStamp != TMWDEFS_NULL)
    _time2string(pTimeStamp, timeBuf, sizeof(timeBuf));
  else
    tmwtarg_snprintf(timeBuf, sizeof(timeBuf), " ");

  i14diag_formatIOA(pSector, ioa, (TMWTYPES_CHAR *)ioaBuf);
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s Double Point %s = 0x%02x\n", timeBuf, ioaBuf, flags);
  tmwdiag_putLine(&anlzId, buf);
}

/* function: i14diag_showMST */
void TMWDEFS_GLOBAL i14diag_showMST(
  TMWSCTR *pSector,
  TMWTYPES_UCHAR cot,
  TMWTYPES_ULONG ioa, 
  TMWTYPES_UCHAR valueAndState, 
  TMWTYPES_UCHAR flags, 
  TMWDTIME *pTimeStamp)
{
  TMWDIAG_ANLZ_ID anlzId;
  char ioaBuf[32];
  char timeBuf[64];
  char buf[256];

  /* signed value is in 7 bits */
  char value = valueAndState & 0x7f;
  if(value & 0x40)
  {
    /* if this bit is set value is negative, set top bit of signed char */
    value |= 0x80;
  }

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, _cotToDataId(cot) | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }

  if(pTimeStamp != TMWDEFS_NULL)
    _time2string(pTimeStamp, timeBuf, sizeof(timeBuf));
  else
    tmwtarg_snprintf(timeBuf, sizeof(timeBuf), " ");

  i14diag_formatIOA(pSector, ioa, (TMWTYPES_CHAR *)ioaBuf);
  if((valueAndState & I14DEF_VTI_TRANSIENT_STATE))
    tmwtarg_snprintf(buf, sizeof(buf), "%-21s Regulating Step %s = %d transient, quality = 0x%02x\n", timeBuf, ioaBuf, value, flags);
  else
    tmwtarg_snprintf(buf, sizeof(buf), "%-21s Regulating Step %s = %d, quality = 0x%02x\n", timeBuf, ioaBuf, value, flags);
  tmwdiag_putLine(&anlzId, buf);
}

/* function: i14diag_showMBO */
void TMWDEFS_GLOBAL i14diag_showMBO(
  TMWSCTR *pSector,
  TMWTYPES_UCHAR cot,
  TMWTYPES_ULONG ioa, 
  TMWTYPES_ULONG value, 
  TMWTYPES_UCHAR flags, 
  TMWDTIME *pTimeStamp)
{
  TMWDIAG_ANLZ_ID anlzId;
  char ioaBuf[32];
  char timeBuf[64];
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, _cotToDataId(cot) | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }

  if(pTimeStamp != TMWDEFS_NULL)
    _time2string(pTimeStamp, timeBuf, sizeof(timeBuf));
  else
    tmwtarg_snprintf(timeBuf, sizeof(timeBuf), " ");

  i14diag_formatIOA(pSector, ioa, (TMWTYPES_CHAR *)ioaBuf);
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s Bitstring %s = 0x%08x, flags = 0x%02x\n", timeBuf, ioaBuf, value, flags);
  tmwdiag_putLine(&anlzId, buf);
}

/* function: i14diag_showMMENA */
void TMWDEFS_GLOBAL i14diag_showMMENA(
  TMWSCTR *pSector,
  TMWTYPES_UCHAR cot,
  TMWTYPES_ULONG ioa, 
  TMWTYPES_SHORT value, 
  TMWTYPES_UCHAR flags, 
  TMWDTIME *pTimeStamp)
{
  TMWDIAG_ANLZ_ID anlzId;
  char ioaBuf[32];
  char timeBuf[64];
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, _cotToDataId(cot) | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }

  if(pTimeStamp != TMWDEFS_NULL)
    _time2string(pTimeStamp, timeBuf, sizeof(timeBuf));
  else
    tmwtarg_snprintf(timeBuf, sizeof(timeBuf), " ");

  i14diag_formatIOA(pSector, ioa, (TMWTYPES_CHAR *)ioaBuf);
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s Normalized Measurand %s = 0x%02x, flags = 0x%02x\n", timeBuf, ioaBuf, (TMWTYPES_USHORT)value, flags);
  tmwdiag_putLine(&anlzId, buf);
}

/* function: i14diag_showMMENB */
void TMWDEFS_GLOBAL i14diag_showMMENB(
  TMWSCTR *pSector,
  TMWTYPES_UCHAR cot,
  TMWTYPES_ULONG ioa, 
  TMWTYPES_SHORT value, 
  TMWTYPES_UCHAR flags, 
  TMWDTIME *pTimeStamp)
{
  TMWDIAG_ANLZ_ID anlzId;
  char ioaBuf[32];
  char timeBuf[64];
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, _cotToDataId(cot) | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }

  if(pTimeStamp != TMWDEFS_NULL)
    _time2string(pTimeStamp, timeBuf, sizeof(timeBuf));
  else
    tmwtarg_snprintf(timeBuf, sizeof(timeBuf), " ");

  i14diag_formatIOA(pSector, ioa, (TMWTYPES_CHAR *)ioaBuf);
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s Scaled Measurand %s = %6d, flags = 0x%02x\n", timeBuf, ioaBuf, value, flags);
  tmwdiag_putLine(&anlzId, buf);
}

#if TMWCNFG_SUPPORT_FLOAT
/* function: i14diag_showMMENC */
void TMWDEFS_GLOBAL i14diag_showMMENC(
  TMWSCTR *pSector,
  TMWTYPES_UCHAR cot,
  TMWTYPES_ULONG ioa, 
  TMWTYPES_SFLOAT value, 
  TMWTYPES_UCHAR flags, 
  TMWDTIME *pTimeStamp)
{
  TMWDIAG_ANLZ_ID anlzId;
  char ioaBuf[32];
  char timeBuf[64];
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, _cotToDataId(cot) | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }

  if(pTimeStamp != TMWDEFS_NULL)
    _time2string(pTimeStamp, timeBuf, sizeof(timeBuf));
  else
    tmwtarg_snprintf(timeBuf, sizeof(timeBuf), " ");

  i14diag_formatIOA(pSector, ioa, (TMWTYPES_CHAR *)ioaBuf);
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s Short Float Measurand %s = %10g, flags = 0x%02x\n", timeBuf, ioaBuf, value, flags);
  tmwdiag_putLine(&anlzId, buf);
}
#endif

/* function: i14diag_showMIT */
void TMWDEFS_GLOBAL i14diag_showMIT(
  TMWSCTR *pSector,
  TMWTYPES_UCHAR cot,
  TMWTYPES_ULONG ioa, 
  TMWTYPES_ULONG value, 
  TMWTYPES_UCHAR flags, 
  TMWDTIME *pTimeStamp)
{
  TMWDIAG_ANLZ_ID anlzId;
  char ioaBuf[32];
  char timeBuf[64];
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, _cotToDataId(cot) | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }

  if(pTimeStamp != TMWDEFS_NULL)
    _time2string(pTimeStamp, timeBuf, sizeof(timeBuf));
  else
    tmwtarg_snprintf(timeBuf, sizeof(timeBuf), " ");

  i14diag_formatIOA(pSector, ioa, (TMWTYPES_CHAR *)ioaBuf);
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s Integrated Total %s = %d, flags = 0x%02x\n", timeBuf, ioaBuf, value, flags);
  tmwdiag_putLine(&anlzId, buf);
}

/* function: i14diag_showMITC */
void TMWDEFS_GLOBAL i14diag_showMITC(
  TMWSCTR *pSector,
  TMWTYPES_UCHAR cot,
  TMWTYPES_ULONG ioa, 
  TMWTYPES_UCHAR *pBCD, 
  TMWTYPES_UCHAR flags, 
  TMWDTIME *pTimeStamp)
{
  int i, len;
  TMWDIAG_ANLZ_ID anlzId;
  char ioaBuf[32];
  char valueBuf[32];
  char timeBuf[64];
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, _cotToDataId(cot) | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }

  if(pTimeStamp != TMWDEFS_NULL)
    _time2string(pTimeStamp, timeBuf, sizeof(timeBuf));
  else
    tmwtarg_snprintf(timeBuf, sizeof(timeBuf), " ");

  i14diag_formatIOA(pSector, ioa, (TMWTYPES_CHAR *)ioaBuf);

  len = 0;
  for(i = 5; i >=0; i--)
      len += tmwtarg_snprintf(&valueBuf[len], sizeof(valueBuf) - len, "%02x", pBCD[i]);

  tmwtarg_snprintf(buf, sizeof(buf), "%-21s Integrated Total BCD %s = %6s, flags = 0x%02x\n", timeBuf, ioaBuf, valueBuf, flags);
  tmwdiag_putLine(&anlzId, buf);
}

/* function: i14diag_showMEPTA */
void TMWDEFS_GLOBAL i14diag_showMEPTA(
  TMWSCTR *pSector,
  TMWTYPES_UCHAR cot,
  TMWTYPES_ULONG ioa,
  TMWTYPES_UCHAR sep,
  TMWTYPES_USHORT elapsedTime,
  TMWDTIME *pTimeStamp)
{
  TMWDIAG_ANLZ_ID anlzId;
  char ioaBuf[32];
  char timeBuf[64];
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, _cotToDataId(cot) | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }

  if(pTimeStamp != TMWDEFS_NULL)
    _time2string(pTimeStamp, timeBuf, sizeof(timeBuf));
  else
    tmwtarg_snprintf(timeBuf, sizeof(timeBuf), " ");

  i14diag_formatIOA(pSector, ioa, (TMWTYPES_CHAR *)ioaBuf);
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s Event of PE %s = 0x%x, elapsed time = %d\n", 
    timeBuf, ioaBuf, sep, elapsedTime);

  tmwdiag_putLine(&anlzId, buf);
}

/* function: i14diag_showMEPTB */
void TMWDEFS_GLOBAL i14diag_showMEPTB(
  TMWSCTR *pSector,
  TMWTYPES_UCHAR cot,
  TMWTYPES_ULONG ioa,
  TMWTYPES_UCHAR spe,
  TMWTYPES_UCHAR qdp,
  TMWTYPES_USHORT relayDuration,
  TMWDTIME *pTimeStamp)
{
  TMWDIAG_ANLZ_ID anlzId;
  char ioaBuf[32];
  char timeBuf[64];
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, _cotToDataId(cot) | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }

  if(pTimeStamp != TMWDEFS_NULL)
    _time2string(pTimeStamp, timeBuf, sizeof(timeBuf));
  else
    tmwtarg_snprintf(timeBuf, sizeof(timeBuf), " ");

  i14diag_formatIOA(pSector, ioa, (TMWTYPES_CHAR *)ioaBuf);
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s Packed Start Events of PE %s = 0x%x, qdp = 0x%x, relay duration = %d\n", 
    timeBuf, ioaBuf, spe, qdp, relayDuration);

  tmwdiag_putLine(&anlzId, buf);
}

/* function: i14diag_showMEPTC */
void TMWDEFS_GLOBAL i14diag_showMEPTC(
  TMWSCTR *pSector,
  TMWTYPES_UCHAR cot,
  TMWTYPES_ULONG ioa,
  TMWTYPES_UCHAR oci,
  TMWTYPES_UCHAR qdp,
  TMWTYPES_USHORT relayOperating,
  TMWDTIME *pTimeStamp)
{
  TMWDIAG_ANLZ_ID anlzId;
  char ioaBuf[32];
  char timeBuf[64];
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, _cotToDataId(cot) | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }

  if(pTimeStamp != TMWDEFS_NULL)
    _time2string(pTimeStamp, timeBuf, sizeof(timeBuf));
  else
    tmwtarg_snprintf(timeBuf, sizeof(timeBuf), " ");

  i14diag_formatIOA(pSector, ioa, (TMWTYPES_CHAR *)ioaBuf);
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s Packed Output Circuit Information of PE %s = 0x%x, qdp = 0x%x, relay operating time = %d\n", 
    timeBuf, ioaBuf, oci, qdp, relayOperating);

  tmwdiag_putLine(&anlzId, buf);
}

/* function: i14diag_showMPSNA */
void TMWDEFS_GLOBAL i14diag_showMPSNA(
  TMWSCTR *pSector,
  TMWTYPES_UCHAR cot,
  TMWTYPES_ULONG ioa,
  TMWTYPES_ULONG scd,
  TMWTYPES_UCHAR qds)
{
  TMWDIAG_ANLZ_ID anlzId;
  char ioaBuf[32];
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, _cotToDataId(cot) | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }
  i14diag_formatIOA(pSector, ioa, (TMWTYPES_CHAR *)ioaBuf);
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s Packed Single Point Information %s = 0x%x, qds = 0x%x\n", " ", ioaBuf, scd, qds);
  tmwdiag_putLine(&anlzId, buf);
}

/* function: i14diag_showMMEND */
void TMWDEFS_GLOBAL i14diag_showMMEND(
  TMWSCTR *pSector,
  TMWTYPES_UCHAR cot,
  TMWTYPES_ULONG ioa,
  TMWTYPES_SHORT nva)
{
  TMWDIAG_ANLZ_ID anlzId;
  char ioaBuf[32];
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, _cotToDataId(cot) | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }
  i14diag_formatIOA(pSector, ioa, (TMWTYPES_CHAR *)ioaBuf);
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s Normalized Measurand W/O Quality %s = 0x%02x\n", " ", ioaBuf, (TMWTYPES_USHORT)nva);
  tmwdiag_putLine(&anlzId, buf);
}

/* function: i14diag_showPMENA */
void TMWDEFS_GLOBAL i14diag_showPMENA(
  TMWSCTR *pSector,
  TMWTYPES_ULONG ioa, 
  TMWTYPES_SHORT value, 
  TMWTYPES_UCHAR qualifier)
{
  TMWDIAG_ANLZ_ID anlzId;
  char ioaBuf[32];
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, TMWDIAG_ID_STATIC_DATA | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }

  i14diag_formatIOA(pSector, ioa, (TMWTYPES_CHAR *)ioaBuf);
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s Parameter of Measured Value, Normalized %s = 0x%02x, qualifier = 0x%02x(%s)\n", 
    " ", ioaBuf, (TMWTYPES_USHORT)value, qualifier, _kindString(qualifier));

  tmwdiag_putLine(&anlzId, buf);
}

/* function: i14diag_showPMENB */
void TMWDEFS_GLOBAL i14diag_showPMENB(
  TMWSCTR *pSector,
  TMWTYPES_ULONG ioa, 
  TMWTYPES_SHORT value, 
  TMWTYPES_UCHAR qualifier)
{
  TMWDIAG_ANLZ_ID anlzId;
  char ioaBuf[32];
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, TMWDIAG_ID_STATIC_DATA | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }

  i14diag_formatIOA(pSector, ioa, (TMWTYPES_CHAR *)ioaBuf);
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s Parameter of Measured Value, Scaled %s = %6d, qualifier = 0x%02x(%s)\n", 
    " ", ioaBuf, value, qualifier, _kindString(qualifier));

  tmwdiag_putLine(&anlzId, buf);
}

#if TMWCNFG_SUPPORT_FLOAT
/* function: i14diag_showPMENC */
void TMWDEFS_GLOBAL i14diag_showPMENC(
  TMWSCTR *pSector,
  TMWTYPES_ULONG ioa, 
  TMWTYPES_SFLOAT value, 
  TMWTYPES_UCHAR qualifier)
{
  TMWDIAG_ANLZ_ID anlzId;
  char ioaBuf[32];
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, TMWDIAG_ID_STATIC_DATA | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }

  i14diag_formatIOA(pSector, ioa, (TMWTYPES_CHAR *)ioaBuf);
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s Parameter of Measured Value, Short Float %s = %10g, qualifier = 0x%02x(%s)\n", 
    " ", ioaBuf, value, qualifier, _kindString(qualifier));

  tmwdiag_putLine(&anlzId, buf);
}
#endif

/* function: i14diag_showPACNA */
void TMWDEFS_GLOBAL i14diag_showPACNA(
  TMWSCTR *pSector,
  TMWTYPES_ULONG ioa)
{
  TMWDIAG_ANLZ_ID anlzId;
  char ioaBuf[32];
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, TMWDIAG_ID_STATIC_DATA | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }

  i14diag_formatIOA(pSector, ioa, (TMWTYPES_CHAR *)ioaBuf);
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s Parameter Activation %s\n", " ", ioaBuf);

  tmwdiag_putLine(&anlzId, buf);
}

/* function: i14diag_showFFRNA */
void TMWDEFS_GLOBAL i14diag_showFFRNA(
  TMWSCTR *pSector, 
  TMWTYPES_ULONG ioa, 
  TMWTYPES_USHORT fileName,
  TMWTYPES_ULONG fileLength,
  TMWTYPES_UCHAR frq)
{
  TMWDIAG_ANLZ_ID anlzId;
  char ioaBuf[32];
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, TMWDIAG_ID_STATIC_DATA | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }

  i14diag_formatIOA(pSector, ioa, (TMWTYPES_CHAR *)ioaBuf);
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s File Transfer - File Ready %s\n", " ", ioaBuf);
  tmwdiag_putLine(&anlzId, buf);

  tmwtarg_snprintf(buf, sizeof(buf), "%-21s file name = %d file length = %d qualifier = 0x%02x\n", " ", fileName, fileLength, frq);
  tmwdiag_putLine(&anlzId, buf);
}

/* function: i14diag_showFSRNA */
void TMWDEFS_GLOBAL i14diag_showFSRNA(
  TMWSCTR *pSector, 
  TMWTYPES_ULONG ioa, 
  TMWTYPES_USHORT fileName,
  TMWTYPES_UCHAR sectionName,
  TMWTYPES_ULONG sectionLength,
  TMWTYPES_UCHAR srq)
{
  TMWDIAG_ANLZ_ID anlzId;
  char ioaBuf[32];
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, TMWDIAG_ID_STATIC_DATA | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }

  i14diag_formatIOA(pSector, ioa, (TMWTYPES_CHAR *)ioaBuf);
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s File Transfer- Section Ready %s\n", " ", ioaBuf);
  tmwdiag_putLine(&anlzId, buf);

  tmwtarg_snprintf(buf, sizeof(buf), "%-21s file name = %d section name = %d section length = %d qualifier = 0x%02x\n", 
    " ", fileName, sectionName, sectionLength, srq);
  tmwdiag_putLine(&anlzId, buf);
}

/* function: i14diag_showFSCNA */
void TMWDEFS_GLOBAL i14diag_showFSCNA(
  TMWSCTR *pSector, 
  TMWTYPES_ULONG ioa,
  TMWTYPES_USHORT fileName,
  TMWTYPES_UCHAR sectionName,
  TMWTYPES_UCHAR scq)
{
  TMWDIAG_ANLZ_ID anlzId;
  char ioaBuf[32];
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, TMWDIAG_ID_STATIC_DATA | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }

  i14diag_formatIOA(pSector, ioa, (TMWTYPES_CHAR *)ioaBuf);
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s File Transfer- Call directory, file or section %s\n", " ", ioaBuf);
  tmwdiag_putLine(&anlzId, buf);

  tmwtarg_snprintf(buf, sizeof(buf), "%-21s file name = %d section name = %d qualifier = 0x%02x\n", 
    " ", fileName, sectionName, scq);
  tmwdiag_putLine(&anlzId, buf);
}

/* function: i14diag_showFSCNB */
void TMWDEFS_GLOBAL i14diag_showFSCNB(
  TMWSCTR *pSector, 
  TMWTYPES_ULONG ioa,
  TMWTYPES_USHORT fileName)
{
  TMWDIAG_ANLZ_ID anlzId;
  char ioaBuf[32];
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, TMWDIAG_ID_STATIC_DATA | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }

  i14diag_formatIOA(pSector, ioa, (TMWTYPES_CHAR *)ioaBuf);
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s File Transfer- Query Log %s\n", " ", ioaBuf);
  tmwdiag_putLine(&anlzId, buf);

  tmwtarg_snprintf(buf, sizeof(buf), "%-21s file name = %d\n", 
    " ", fileName);
  tmwdiag_putLine(&anlzId, buf);
}

/* function: i14diag_showFLSNA */
void TMWDEFS_GLOBAL i14diag_showFLSNA(
  TMWSCTR *pSector, 
  TMWTYPES_ULONG ioa,
  TMWTYPES_USHORT fileName,
  TMWTYPES_UCHAR sectionName,
  TMWTYPES_UCHAR lsq,
  TMWTYPES_UCHAR chs)
{
  TMWDIAG_ANLZ_ID anlzId;
  char ioaBuf[32];
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, TMWDIAG_ID_STATIC_DATA | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }

  i14diag_formatIOA(pSector, ioa, (TMWTYPES_CHAR *)ioaBuf);
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s File Transfer- Last section, last segment %s\n",
    " ", ioaBuf);

  tmwdiag_putLine(&anlzId, buf);

  tmwtarg_snprintf(buf, sizeof(buf), "%-21s file name = %d section name = %d  qualifier = 0x%02x checksum = 0x%02x\n", 
    " ", fileName, sectionName, lsq, chs);

  tmwdiag_putLine(&anlzId, buf);
}

/* function: i14diag_showFAFNA */
void TMWDEFS_GLOBAL i14diag_showFAFNA(
  TMWSCTR *pSector, 
  TMWTYPES_ULONG ioa,
  TMWTYPES_USHORT fileName,
  TMWTYPES_UCHAR sectionName,
  TMWTYPES_UCHAR afq)
{
  TMWDIAG_ANLZ_ID anlzId;
  char ioaBuf[32];
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, TMWDIAG_ID_STATIC_DATA | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }

  i14diag_formatIOA(pSector, ioa, (TMWTYPES_CHAR *)ioaBuf);
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s File Transfer- ACK file, ACK section %s file name = %d section name = %d qualifier = 0x%02x\n", 
    " ", ioaBuf, fileName, sectionName, afq);

  tmwdiag_putLine(&anlzId, buf);
}

/* function: i14diag_showFSGNA */
void TMWDEFS_GLOBAL i14diag_showFSGNA(
  TMWSCTR *pSector, 
  TMWTYPES_ULONG ioa,
  TMWTYPES_USHORT fileName,
  TMWTYPES_UCHAR sectionName,
  TMWTYPES_ULONG segmentLength,
  TMWTYPES_UCHAR *pSegmentData)
{
  TMWDIAG_ANLZ_ID anlzId;
  char ioaBuf[32];
  char buf[256];

  TMWTARG_UNUSED_PARAM(pSegmentData);

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, TMWDIAG_ID_STATIC_DATA | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }

  i14diag_formatIOA(pSector, ioa, (TMWTYPES_CHAR *)ioaBuf);
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s File Transfer- Segment %s\n", " ", ioaBuf);
  tmwdiag_putLine(&anlzId, buf);

  tmwtarg_snprintf(buf, sizeof(buf), "%-21s file name = %d section name = %d segment length = %d \n", 
    " ",fileName, sectionName, segmentLength);
  tmwdiag_putLine(&anlzId, buf);
}

/* function: i14diag_showFDRTA */
void TMWDEFS_GLOBAL i14diag_showFDRTA(
  TMWSCTR *pSector, 
  TMWTYPES_ULONG ioa,
  TMWTYPES_USHORT fileName,
  TMWTYPES_ULONG fileLength,
  TMWTYPES_UCHAR sof,
  TMWDTIME *pTimeStamp)
{
  TMWDIAG_ANLZ_ID anlzId;
  char ioaBuf[32];
  char timeBuf[64];
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, TMWDIAG_ID_STATIC_DATA | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }

  if(pTimeStamp != TMWDEFS_NULL)
    _time2string(pTimeStamp, timeBuf, sizeof(timeBuf));
  else
    tmwtarg_snprintf(timeBuf, sizeof(timeBuf), " ");

  i14diag_formatIOA(pSector, ioa, (TMWTYPES_CHAR *)ioaBuf);
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s File Transfer- Directory %s\n", timeBuf, ioaBuf);
  tmwdiag_putLine(&anlzId, buf);

  tmwtarg_snprintf(buf, sizeof(buf), "%-21s file name = %d file length = %d sof = 0x%02x\n", 
    " ", fileName, fileLength, sof);

  tmwdiag_putLine(&anlzId, buf);
}

/* function: i14diag_showMCT */
void TMWDEFS_GLOBAL i14diag_showMCT(
  TMWSCTR *pSector, 
  TMWTYPES_ULONG ioa,
  TMWTYPES_UCHAR quantity,
  TMWTYPES_UCHAR *pCtiValues)
{
  TMWDIAG_ANLZ_ID anlzId;
  int rowLength;
  int index;
  int len;
  int i;
  int numBytes;
  char ioaBuf[32];
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, TMWDIAG_ID_STATIC_DATA | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }

  i14diag_formatIOA(pSector, ioa, (TMWTYPES_CHAR *)ioaBuf);
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s Configuration Table %s =\n", " ", ioaBuf);
  tmwdiag_putLine(&anlzId, buf);
 
  numBytes = quantity;
  index = 0;
  while(index < numBytes)
  {
    len = tmwtarg_snprintf(buf, sizeof(buf), "%24s", " ");

    rowLength = numBytes - index;
    if(rowLength > MAX_ROW_LENGTH)
      rowLength = MAX_ROW_LENGTH;

    for(i = 0; i < rowLength; i++)
      len += tmwtarg_snprintf(buf + len, sizeof(buf) - len, "%02x ", pCtiValues[index++]);

    tmwtarg_snprintf(buf + len, sizeof(buf) - len, "\n");
    tmwdiag_putLine(&anlzId, buf);
  }
}

/* function: i14diag_showPropagationDelay */
void TMWDEFS_GLOBAL i14diag_showPropagationDelay(
  TMWSCTR *pSector,
  TMWTYPES_USHORT delay)
{
  TMWDIAG_ANLZ_ID anlzId;
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, TMWDIAG_ID_STATIC_DATA | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s One Way Propagation Delay = %d\n", " ", delay);
  tmwdiag_putLine(&anlzId, buf);
}

/* function: i14diag_showTime */
void TMWDEFS_GLOBAL i14diag_showTime(
  TMWSCTR *pSector,
  TMWDTIME *pDateTime)
{
  TMWDIAG_ANLZ_ID anlzId;
  char timeBuf[64];
  char buf[256];

  if(tmwdiag_initId(&anlzId, TMWDEFS_NULL, TMWDEFS_NULL, pSector, TMWDIAG_ID_STATIC_DATA | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }
  _time2string(pDateTime, timeBuf, sizeof(timeBuf));
  tmwtarg_snprintf(buf, sizeof(buf), "%-21s Absolute Time = %s\n", " ", timeBuf);
  tmwdiag_putLine(&anlzId, buf);
}

/* function: i14diag_frameSent */
void TMWDEFS_GLOBAL i14diag_frameSent(
  TMWSESN_TX_DATA *pTxData)
{
  /* For now don't try to display link commands, they have a function code in the first byte */
  if(pTxData->txFlags & TMWSESN_TXFLAGS_LINK_COMMAND)
    return;

  _displayFrame("<===", pTxData->pSession, pTxData->pSector, pTxData->destAddress, pTxData->pMsgBuf, pTxData->msgLength, 0);
}

/* function: i14diag_frameReceived */
void TMWDEFS_GLOBAL i14diag_frameReceived(
  TMWSCTR *pSector, 
  TMWSESN_RX_DATA *pRxData)
{
  _displayFrame("===>", pSector->pSession, pSector, pSector->pSession->linkAddress, pRxData->pMsgBuf, pRxData->msgLength, TMWDIAG_ID_RX);
}

/* function: i14diag_frameError */
void TMWDEFS_GLOBAL i14diag_frameError(
  TMWSESN *pSession, 
  I870UTIL_MESSAGE *pMsgHeader,
  TMWSESN_RX_DATA *pRxData)
{
  TMWDIAG_ANLZ_ID id;
  int rowLength;
  int index;
  int len;
  int i;
  int numBytes;
  char buf[256];
  TMWTYPES_ULONG ioa = 0;
  TMWTYPES_USHORT tmpAddress;

  if(tmwdiag_initId(&id, TMWDEFS_NULL, pSession, TMWDEFS_NULL, TMWDIAG_ID_APPL | TMWDIAG_ID_ERROR | TMWDIAG_ID_RX) == TMWDEFS_FALSE)
  {
    return;
  }

  switch(((I870SESN *)pSession)->infoObjAddrSize)
  {
    case 1:
      ioa = pRxData->pMsgBuf[pMsgHeader->offset];
      break;

    case 2:
      tmwtarg_get16(&pRxData->pMsgBuf[pMsgHeader->offset], &tmpAddress);
      ioa = (TMWTYPES_ULONG)tmpAddress;
      break;

    case 3:
      tmwtarg_get24(&pRxData->pMsgBuf[pMsgHeader->offset], &ioa);
      break;
  }

  tmwtarg_snprintf(buf, sizeof(buf), "===> %-10s %s, Receive size %d Quantity(%d) SQ(%s)\n", 
    tmwsesn_getSessionName(pSession), _typeString(pMsgHeader->typeId), pRxData->msgLength,
    pMsgHeader->quantity, pMsgHeader->indexed ? "0" : "1");

  tmwdiag_putLine(&id, buf);

  if(pMsgHeader->origAddress != 0)
  {
    tmwtarg_snprintf(buf, sizeof(buf), "===> %-10s COT(%d) Originator address(%d), Common Address(%d) IOA(%d)\n", 
      tmwsesn_getSessionName(pSession), pMsgHeader->cot, pMsgHeader->origAddress, pMsgHeader->asduAddress, ioa);
  }
  else
  {  
    tmwtarg_snprintf(buf, sizeof(buf), "===> %-10s COT(%d), Common Address(%d) IOA(%d)\n", 
      tmwsesn_getSessionName(pSession), pMsgHeader->cot, pMsgHeader->asduAddress, ioa);
  }

  tmwdiag_putLine(&id, buf);

  numBytes = pRxData->msgLength;

  index = 0;
  while(index < numBytes)
  {
    len = tmwtarg_snprintf(buf, sizeof(buf), "%16s", " ");

    rowLength = numBytes - index;
    if(rowLength > MAX_ROW_LENGTH)
      rowLength = MAX_ROW_LENGTH;

    for(i = 0; i < rowLength; i++)
      len += tmwtarg_snprintf(buf + len, sizeof(buf) - len, "%02x ", pRxData->pMsgBuf[index++]);

    tmwtarg_snprintf(buf + len, sizeof(buf) - len, "\n");
    tmwdiag_putLine(&id, buf);
  }
}

#endif /* TMWCNFG_SUPPORT_DIAG */
