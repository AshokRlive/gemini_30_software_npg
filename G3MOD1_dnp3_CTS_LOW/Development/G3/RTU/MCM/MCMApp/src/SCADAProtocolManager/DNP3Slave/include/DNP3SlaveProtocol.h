/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       DNP3 Slave protocol public interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   06/09/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_3D5EDE5E_9F9E_4288_88EF_333599253D42__INCLUDED_)
#define EA_3D5EDE5E_9F9E_4288_88EF_333599253D42__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <vector>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "Table.h"
#include "Thread.h"
#include "IPoint.h"
#include "IPointObserver.h"
#include "DialupModem.h"
#include "IMCMApplication.h"
#include "GeminiDatabase.h"
#include "ISlaveProtocol.h"
#include "EventLogManager.h"
#include "Logger.h"
#include "TMWDNP3Includes.h"
#include "DNP3SlaveProtocolChannel.h"
#include "DNP3SlaveProtocolSession.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define DNP3SLAVE_ANLGOUT_EVENT_SUPPORT 1
#define DNP3SLAVE_ANLGOUT_CMDEVENT_SUPPORT 0
/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief GeminiDatabase observer interface implementation common across all
 * DNP3 observers
 */
class DNP3SlaveObserver : public IPointObserver
{

public:
    DNP3SlaveObserver(PointIdStr GDBID, lu_uint16_t dnpID, TMWSDNPDatabaseStr *db) :
                                                               GDBID(GDBID),
                                                               dnpID(dnpID),
                                                               db(db)
    {
        eventLog = EventLogManager::getInstance();
    };

    virtual ~DNP3SlaveObserver() {};

    /**
     * \brief Custom observer function
     *
     * \param pointID Point ID
     * \param pointDataPtr Point data
     *
     * \return none
     */
    virtual void update(PointIdStr pointID, PointData *pointDataPtr) = 0;

    /**
     * \brief Get the ID of the subscribed point
     *
     * \return Point ID
     */
    virtual inline PointIdStr getPointID() { return GDBID;};

protected:
    PointIdStr GDBID;
    lu_uint16_t dnpID;
    TMWSDNPDatabaseStr *db;
    EventLogManager* eventLog;
};


class DNP3SlaveAIObserver : public DNP3SlaveObserver
{
public:
    DNP3SlaveAIObserver(PointIdStr GDBID, lu_uint16_t dnpID, TMWSDNPDatabaseStr *db) :
                                         DNP3SlaveObserver(GDBID, dnpID, db)
                                         {};

    virtual ~DNP3SlaveAIObserver() {};

    /**
     * \brief Custom observer function
     *
     * \param pointID Point ID
     * \param pointDataPtr Point data
     *
     * \return none
     */
    virtual void update(PointIdStr pointID, PointData *pointDataPtr);
};

class DNP3SlaveAOObserver : public DNP3SlaveObserver
{
public:
	DNP3SlaveAOObserver(PointIdStr GDBID, lu_uint16_t dnpID, TMWSDNPDatabaseStr *db) :
                                         DNP3SlaveObserver(GDBID, dnpID, db)
                                         {};

    virtual ~DNP3SlaveAOObserver() {};

    /**
     * \brief Custom observer function
     *
     * \param pointID Point ID
     * \param pointDataPtr Point data
     *
     * \return none
     */
    virtual void update(PointIdStr pointID, PointData *pointDataPtr);
};


class DNP3SlaveBIObserver : public DNP3SlaveObserver
{
public:
    DNP3SlaveBIObserver(PointIdStr GDBID, lu_uint16_t dnpID, TMWSDNPDatabaseStr *db) :
                                         DNP3SlaveObserver(GDBID, dnpID, db) {};
    virtual ~DNP3SlaveBIObserver() {};

    /**
     * \brief Custom observer function
     *
     * \param pointID Point ID
     * \param pointDataPtr Point data
     *
     * \return none
     */
    virtual void update(PointIdStr pointID, PointData *pointDataPtr);
};


class DNP3SlaveDBIObserver : public DNP3SlaveObserver
{
public:
    DNP3SlaveDBIObserver(PointIdStr GDBID, lu_uint16_t dnpID, TMWSDNPDatabaseStr *db) :
                                         DNP3SlaveObserver(GDBID, dnpID, db) {};
    virtual ~DNP3SlaveDBIObserver() {};

    /**
     * \brief Custom observer function
     *
     * \param pointID Point ID
     * \param pointDataPtr Point data
     *
     * \return none
     */
    virtual void update(PointIdStr pointID, PointData *pointDataPtr);
};


class DNP3SlaveCounterObserver : public DNP3SlaveObserver
{
public:
    DNP3SlaveCounterObserver(PointIdStr GDBID, lu_uint16_t dnpID, TMWSDNPDatabaseStr *db) :
                                         DNP3SlaveObserver(GDBID, dnpID, db)
    {};
    virtual ~DNP3SlaveCounterObserver() {};

    /**
     * \brief Custom observer function
     *
     * \param pointID Point ID
     * \param pointDataPtr Point data
     *
     * \return none
     */
    virtual void update(PointIdStr pointID, PointData *pointDataPtr);
};






class DNP3SlaveProtocol : public ISlaveProtocol, private Thread, private TimeObserver
{
private:
    /**
     * \brief Struct for event decoupling
     *
     * Used for decoupling the Virtual Point update from the protocol stack update.
     */
    struct /*__attribute__((packed, aligned(1)))*/ EventDef
    {
        lu_uint16_t     eventCode;
        TMWSESN*        pSession;
        TMWTYPES_USHORT point;

        union {
          TMWTYPES_ANALOG_VALUE AnalogueValue;
          TMWTYPES_UCHAR        BinaryValue;
          TMWTYPES_ULONG        CounterValue;   //32-bit counter value (16-bit as well)
        } DataValue;

        TMWTYPES_UCHAR flags;
        TMWDTIME       TimeStamp;
    };

    /**
     * \brief Struct for event storage before processing
     *
     * This structure is used for storage before timestamp correction.
     */
    struct __attribute__((packed, aligned(1))) VPEventStr
    {
    public:
        TMWTYPES_USHORT dnpID;      //DNP3 point ID
        TMWTYPES_UCHAR flags;       //DNP flags
        lu_uint8_t  sessionID;      //Session ID (instead of the session pointer)
        lu_uint16_t eventCode;      //DNP event code (follows DNPDEFS_OBJ_XX enum)
        timespec relaTime;          //Relative (monotonic) time - for timestamp correction only
        lu_uint8_t valueType;       //TMW value type. Follows TMWTYPES_ANALOG_TYPE
        union
        {
            lu_uint32_t value;      //Digital/counter
            lu_float32_t aValue;    //Analogue
        };
    public:
        const std::string toString();

    private:
        const std::string commonFlags();
    };

public:
    /**
     * \Custom constructor
     *
     * \param GDatabase       Gemini database reference
     * \param mcmApplication  Reference to the Main App Object
     * \param invalidEventTimeCorrection Apply correction to invalid times in events
     */
    DNP3SlaveProtocol( GeminiDatabase&  GDatabase    ,
                       IMCMApplication& mcmApplication,
                       const bool invalidEventTimeCorrection
                        );

    virtual ~DNP3SlaveProtocol();

    /**
     * \brief Add a communications channel to the protocol
     *
     * \param   channel  Channel Reference
     *
     * \return Error code
     */
    virtual SCADAP_ERROR addChannel(DNP3SlaveProtocolChannel& channel);

    /**
     * \brief Add a protocol Session to its internal list
     *
     * \param session Session object to add
     */
    virtual void addSession(DNP3SlaveProtocolSession& pSession);

    /**
     * \brief Delete all communications channels
     *
     * \return Error code
     */
    virtual SCADAP_ERROR deleteChannels();

    /**
     * \brief Start the protocol
     *
     * \return Error code
     */
    virtual SCADAP_ERROR startProtocol();

    /**
     * \brief Stop the protocol
     *
     * This function blocks waiting for the scheduler thread shutdown
     *
     * \return Error code
     */
    virtual SCADAP_ERROR stopProtocol();

    /**
     * \brief Add a Binary event to the eventPipe
     *
     * \param eventLogPtr  Pointer to the eventlog
     * \param dbPtr        Pointer to the DNP Database
     * \param pointDataPtr Pointer to Point Data
     * \param dnpID        DNP Protocol ID
     *
     * \return Error code
     */
    virtual SCADAP_ERROR addBinaryEvent(EventLogManager* eventLogPtr, TMWSDNPDatabaseStr *dbPtr, PointData *pointDataPtr, lu_uint16_t dnpID);

    /**
     * \brief Add a Double Binary event to the eventPipe
     *
     * \param eventLogPtr  Pointer to the eventlog
     * \param dbPtr        Pointer to the DNP Database
     * \param pointDataPtr Pointer to Point Data
     * \param dnpID        DNP Protocol ID
     *
     * \return Error code
     */
    virtual SCADAP_ERROR addDBinaryEvent(EventLogManager* eventLogPtr, TMWSDNPDatabaseStr *dbPtr, PointData *pointDataPtr, lu_uint16_t dnpID);

    /**
     * \brief Add an Analogue event to the eventPipe
     *
     * \param eventLogPtr  Pointer to the eventlog
     * \param dbPtr        Pointer to the DNP Database
     * \param pointDataPtr Pointer to Point Data
     * \param dnpID        DNP Protocol ID
     *
     * \return Error code
     */
    virtual SCADAP_ERROR addAnalogueEvent(EventLogManager* eventLogPtr, TMWSDNPDatabaseStr *dbPtr, PointData *pointDataPtr, lu_uint16_t dnpID);

#if DNP3SLAVE_ANLGOUT_EVENT_SUPPORT
	/**
	 * \brief Add an Analogue output point value change event to the eventPipe
	 *
	 * \param eventLogPtr  Pointer to the eventlog
	 * \param dbPtr        Pointer to the DNP Database
	 * \param pointDataPtr Pointer to Point Data
	 * \param dnpID        DNP Protocol ID
	 *
	 * \return Error code
	 */
    virtual SCADAP_ERROR addAnlgOutEvent(EventLogManager* eventLogPtr, TMWSDNPDatabaseStr *dbPtr, PointData *pointDataPtr, lu_uint16_t dnpID);
#endif
#if DNP3SLAVE_ANLGOUT_CMDEVENT_SUPPORT
	/**
	 * \brief Add an Analogue output point being commanded event to the eventPipe
	 *
	 * \param eventLogPtr  Pointer to the eventlog
	 * \param dbPtr        Pointer to the DNP Database
	 * \param pointDataPtr Pointer to Point Data
	 * \param dnpID        DNP Protocol ID
	 *
	 * \return Error code
	 */
    virtual SCADAP_ERROR addAnlgOutCmdEvent(EventLogManager* eventLogPtr, TMWSDNPDatabaseStr *dbPtr, TMWSDNPAOutputStr *pointDataPtr, lu_uint16_t dnpID);
#endif

    /**
     * \brief Add a Counter event to the eventPipe
     *
     * \param eventLogPtr  Pointer to the eventlog
     * \param dbPtr        Pointer to the DNP Database
     * \param pointDataPtr Pointer to Point Data
     * \param dnpID        DNP Protocol ID
     *
     * \return Error code
     */
    virtual SCADAP_ERROR addCounterEvent(EventLogManager* eventLogPtr, TMWSDNPDatabaseStr *dbPtr, PointData *pointDataPtr, lu_uint16_t dnpID);

    /**
     * \brief Add an event to the eventPipe
     *
     * \param eventPtr  Pointer to event object to add
     *
     * \return Error code
     */
    virtual SCADAP_ERROR addEvent(EventDef *eventPtr);

    virtual inline void getEventLogLock() { };
    /**
     * \brief Execute a cold or warm restart
     *
     * This function is registered in the Triangle Microworks stack, and it is
     * used to require a cold or warm RTU restart.
     * This function is the "bridge" used to connect the Triangle Microworks
     * stack Framework with the G3 framework.
     * It is defined as "static" because it is used as a function pointer
     * in a "C" library.
     *
     * \param dbPtr        Pointer to the DNP Database
     * \param coldRestart  True for cold restart (reboot), false for warm restart
     */
    static void restart(void *tmwSesnPtr, lu_bool_t coldRestart);

    /**
     * \brief Set the Date/Time
     *
     * This function is called directly from the Triangle Microworks stack, and it is
     * used to send the DNP Master's time to the Time Manager.
     *
     * \param masterTime True for cold restart (reboot), false for warm restart
     */
    static void setDateTime(TMWDTIME *masterTime);

    /**
     * \brief Get Binary Input Point
     *
     * This function is called directly from the Triangle Microworks stack, and it is
     * used to retrieve a Binary Input Point.
     *
     * \param dbPtr    Instance of TMWSDNPDatabaseStr  (As this is a static function)
     *        pointNum Point number of required Binary Input point
     */
    static void *binInGetPoint(TMWSDNPDatabaseStr *dbPtr, TMWTYPES_USHORT pointNum);

    /**
     * \brief Get Binary Output Point
     *
     * This function is called directly from the Triangle Microworks stack, and it is
     * used to retrieve a Binary Output Point.
     *
     * \param dbPtr    Instance of TMWSDNPDatabaseStr (As this is a static function)
     *        pointNum Point number of required Binary Output point
     */
    static void *binOutGetPoint(TMWSDNPDatabaseStr *dbPtr, TMWTYPES_USHORT pointNum);

    /**
     * \brief Get Binary Output Point Status
     *
     * This function is called directly from the Triangle Microworks stack, and it is
     * used to retrieve a Binary Output Point status (online/offline).
     *
     * \param pointPtr      Pointer to the Binary Output Point
     *
     * \return Online status -- value TMWDEFS_TRUE when online
     */
    static TMWTYPES_BOOL binOutGetStatus(TMWSDNPBOutputStr *pointPtr);

    /**
     * \brief Binary Output Select
     *
     * This function is called directly from the Triangle Microworks stack, and it is
     * used to check support for the requested CROB Type.
     *
     * \param pointPtr      Pointer to the Binary Output Point
     *        controlCode   Control Code
     *        count         Control Count
     */
    static DNPDEFS_CROB_ST binOutSelect(TMWSDNPBOutputStr *pointPtr,
                                        TMWTYPES_UCHAR controlCode,
                                        TMWTYPES_UCHAR count);

    /**
     * \brief Binary Output Operate
     *
     * This function is called directly from the Triangle Microworks stack, and it is
     * used to check support for the requested CROB Type.  This calls the output point's
     * 'operateswitch' callback
     *
     * \param pointPtr      Pointer to the Binary Output Point
     *        controlCode   Control Code
     *        count         Control Count
     */
    static DNPDEFS_CROB_ST binOutOperate(TMWSDNPBOutputStr *pointPtr,
                                         TMWTYPES_UCHAR controlCode,
                                         TMWTYPES_UCHAR count);


    static void* anlgOutGetPoint(TMWSDNPDatabaseStr *dbPtr,
                                   TMWTYPES_USHORT pointNum);

    static DNPDEFS_CTLSTAT anlgOutSelect(TMWSDNPAOutputStr *pointPtr,
                                         TMWTYPES_ANALOG_VALUE *pValue);

    static DNPDEFS_CTLSTAT anlgOutOperate(TMWSDNPAOutputStr *pointPtr,
                                          TMWTYPES_ANALOG_VALUE *pValue);

    /**
     * \brief Get Analogue Input Point
     *
     * This function is called directly from the Triangle Microworks stack, and it is
     * used to retrieve a Binary Input Point.
     *
     * \param dbPtr    Instance of TMWSDNPDatabaseStr  (As this is a static function)
     *        pointNum Point number of required Analogue Input point
     */
    static void* anlgInGetPoint(TMWSDNPDatabaseStr *dbPtr, TMWTYPES_USHORT pointNum);

    /**
     * \brief Get Double Binary Input Point
     *
     * This function is called directly from the Triangle Microworks stack, and it is
     * used to retrieve a Binary Input Point.
     *
     * \param dbPtr    Instance of TMWSDNPDatabaseStr  (As this is a static function)
     *        pointNum Point number of required Double Binary Input point
     */
    static void* dblInGetPoint(TMWSDNPDatabaseStr *dbPtr, TMWTYPES_USHORT pointNum);

    /**
     * \brief Get Counter Input Point
     *
     * This function is called directly from the Triangle Microworks stack, and it is
     * used to retrieve a Counter Input Point.
     *
     * \param dbPtr    Instance of TMWSDNPDatabaseStr  (As this is a static function)
     *        pointNum Point number of required Counter Input point
     */
    static void* ctrInGetPoint(TMWSDNPDatabaseStr *dbPtr, TMWTYPES_USHORT pointNum);

    /**
     * \brief Get Counter Input Point
     *
     * This function is called directly from the Triangle Microworks stack, and it is
     * used to retrieve a Counter Input Point.
     *
     * \param pointPtr Instance of the protocol pointer (As this is a static function)
     *        freezeAndClear True when ordered to freeze and clear value afterwards
     *
     * \return Result of the operation. LU_TRUE for success.
     */
    static TMWTYPES_BOOL ctrInFreeze(TMWSDNPCInputStr* pointPtr, TMWTYPES_BOOL freezeAndClear);

    /**
     * \brief Delete an event in FRAM for this Session
     *
     * \param eventID               Unique ID for this event
     *        protocolSessionPtr    Pointer to the Session
     */
    static lu_bool_t deleteEventID(lu_uint32_t eventID, void *protocolSessionPtr);

    /**
     * \brief Get Time Synchronisation Status
     *
     * \param none
     *
     * \returns true or false
     */
    static lu_bool_t getTimeSyncStatus();

    /**
     * \brief Get LOCAL Status
     *
     * \param dbPtr
     *
     * \returns true or false
     */
    static lu_bool_t getLocalState(void *dbPtr);

protected:
    /**
     * \brief Thread main loop
     *
     * This function is called automatically when a thread is started
     */
    virtual void threadBody();

private:
    /**
     * \brief Check channel connections
     */
    void tickEvent();

    /**
     * \brief Flushes SHT session buffers and re-timestamps the entries
     */
    virtual void flushSessions(TimeManager::TimeStr& currentTime);
    /**
     * \brief write a Virtual Point event to the session buffer
     *
     * \param eventVP virtual point event
     * \param session Pointer to session where to write the event
     *
     * \return error code
     */
    SCADAP_ERROR writeEventToSession(VPEventStr& eventVP,
                                    DNP3SlaveProtocolSession* session);
    /**
     * \brief Notification of first synchronisation from Time Manager
     */
    virtual void updateTime(TimeManager::TimeStr& currentTime);

    /*
     * Below are static functions that can be used by the Triangle Source Code library
     */

    /**
     * \brief Translate the TMW request into a G3 operation
     *
     * \param sDNPSession Reference to the slave DNP session
     * \param sessionDB Reference to the session's database
     * \param controlCode TMW's control code
     *
     * \return operation structure filled in
     */
    static SwitchLogicOperation binOutFillOperation(SDNPSESN& sDNPSession,
                                                    TMWSDNPDatabaseStr& sessionDB,
                                                    TMWTYPES_UCHAR controlCode
                                                    );

    /**
     * \brief Translate a GDB_ERROR into a TMW's CROB code
     *
     * Use it for CROB (Control Relay Output Block) ONLY.
     *
     * \param errorCode Error code in GDB format
     *
     * \return Translated error code
     */
    static DNPDEFS_CROB_ST translateCROB(const GDB_ERROR errorCode);

    /* Simple diagnostic output function, registered with the Source Code Library */
    static void logDiagnostic( const TMWDIAG_ANLZ_ID *pAnlzId,
                               const TMWTYPES_CHAR *pString
                             );

private:
    typedef std::vector<DNP3SlaveProtocolChannel*> ChannelVector;

//protected:
    /* XXX: pueyos_a - deprecated (to be removed) */
    //MasterMutex eventPipeLock;  // Enables the Event Pipe reading to be atomic

public:
    GeminiDatabase &GDatabase;

private:
    /* References */
    TMWAPPL *applContextPtr;
    Logger& log;

    // Vector of DNP3SlaveProtocolChannels
    ChannelVector DNP3SlaveChannels;

    Pipe  eventPipe;      // Pipe to ensure all DNP Eventing is performed in this thread

    /* Members needed for RE-TIMESTAMPING functionality */
    typedef std::vector<DNP3SlaveProtocolSession*> SessionStorage;
    SessionStorage sessionList;
    MasterMutex synchPipeLock;  //Controls the access to the Event Pipe for proper flushing
    bool firstSynch;    //Time has been synchronised and pending events flushed
    bool m_useUTC;      //Use UTC (or local) time for events
};


#endif // !defined(EA_3D5EDE5E_9F9E_4288_88EF_333599253D42__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
