/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14mpsna.h
 * description: This file is intended for internal SCL use only.
 *  IEC 60870-5-101/104 slave packed single point information
 *  with status change detection support.
 */
#ifndef S14MPSNA_DEFINED
#define S14MPSNA_DEFINED

#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/utils/tmwdtime.h"
#include "tmwscl/utils/tmwsesn.h"
#include "tmwscl/i870/s14sesn.h"
#include "tmwscl/i870/s14sctr.h"
#include "tmwscl/i870/s14dbas.h"
#include "tmwscl/i870/s14evnt.h"

/* Structure used to store single point events */
typedef struct S14MPSNAEventStruct {
  S14EVNT s14Event;
  TMWTYPES_ULONG scd;
} S14MPSNA_EVENT;

#ifdef __cplusplus
extern "C" {
#endif

  /* function: s14mpsna_init 
   * purpose: Initialize MPSNA event processing.
   * arguments:
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL s14mpsna_init(
    TMWSCTR *pSector);
 
  /* function: s14mpsna_close 
   * purpose: Close MPSNA event processing
   * arguments:
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL s14mpsna_close(
    TMWSCTR *pSector);
 
  /* function: s14mpsna_addEvent
   * purpose: Create and queue an MPSNA event
   * arguments:
   *  pSector - identifies sector
   *  cot - cause of transmission
   *   normally I14DEF_COT_SPONTANEOUS or I14DEF_COT_RETURN_LOCAL
   *  ioa - information object address
   *  scd - status and status change detection, defined in 7.2.6.40
   *  qds - quality descriptor, defined in 7.2.6.3
   *  pTimeStamp - time of event
   *   Note: timeStamp is not sent in a MPSNA spontaneous message
   *   This can be TMWDEFS_NULL or a pointer to a TMWDTIME
   * returns:
   *  void * - non-NULL value indicates success; TMWDEFS_NULL indicates failure
   */
  TMWDEFS_SCL_API void * TMWDEFS_GLOBAL s14mpsna_addEvent(
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot,
    TMWTYPES_ULONG ioa, 
    TMWTYPES_ULONG scd, 
    TMWTYPES_UCHAR qds, 
    TMWDTIME *pTimeStamp);

  /* function: s14mpsna_countEvents
   * purpose: count MPSNA events
   * arguments:
   *  pSector - identifies sector
   * returns:
   *  number of events in queue
   */
  TMWTYPES_USHORT TMWDEFS_GLOBAL s14mpsna_countEvents(
    TMWSCTR *pSector);

  /* function: s14mpsna_scanForChanges
   * purpose: Process MPSNA events into a message
   * arguments:
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   *  buildResponse - TMWDEFS_TRUE if this function should build
   *   and send the response message, otherwise simply return
   *   TMWDEFS_TRUE if data is ready to send.
   * returns:
   *  TMWDEFS_TRUE data is ready to send or was sent, else TMWDEFS_FALSE
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14mpsna_scanForChanges(
    TMWSCTR *pSector);

  /* function: s14mpsna_processEvents
   * purpose: Process MPSNA events into a message
   * arguments:
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   *  pEventTime - returns time of earliest event if a clock sync is required
   * returns:
   *  S14EVNT_SENT                - ASDU was sent
   *  S14EVNT_CLOCK_SYNC_REQUIRED - Spontaneous CCSNA is required
   *  S14EVNT_NOT_SENT            - No ASDU was sent
   */
  S14EVNT_STATUS TMWDEFS_GLOBAL s14mpsna_processEvents(
    TMWSCTR *pSector,
    TMWDTIME *pEventTime);

  /* function: s14mpsna_readIntoResponse 
   * purpose: Read the MPSNA values for the point specified and store in response
   *  message to be sent to master
   * arguments:
   *  pTxData - pointer
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   *  cot - cause of tranmission
   *  groupMask - mask specifying what group(s) points should be in
   *  ioa - information object address
   *  pPointIndex - pointer to index of point to be read. This should be updated
   *   by this function.
   * returns:
   *   status
   */
  S14DBAS_GROUP_STATUS TMWDEFS_CALLBACK s14mpsna_readIntoResponse(
    TMWSESN_TX_DATA *pTxData, 
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot, 
    TMWDEFS_GROUP_MASK groupMask, 
    TMWTYPES_ULONG ioa,
    TMWTYPES_USHORT *pPointIndex);

#ifdef __cplusplus
}
#endif
#endif /* S14MPSNA_DEFINED */
