/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:lucy_common_defs.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24 Oct 2014     andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef LUCY_COMMON_DEFS_H_
#define LUCY_COMMON_DEFS_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/**
 * \brief Signature for the restart callback function
 *
 * \param coldRestart True for cold restart (reboot), false for warm restart
 */
typedef void (*CALLBACK_restartSessionDef)(void *dbPtr, lu_bool_t coldRestart);


/**
 * \brief Signature for the Delete Event ID function
 *
 * \detail This function marks a specific event (for the supplied sessionID)
 *         as "sent to SCADA" (i.e. no longer required for this session).
 *
 * \param eventID            Event ID
 * \param protRefPtr         Lucy Protocol Pointer (used to get access to session)
 *
 * \return true if successful
 */
typedef lu_bool_t (*CALLBACK_deleteEventIDDef)(lu_uint32_t eventID, void *protRefPtr);

/**
 * \brief Signature for the Get Time Sync Status function
 *
 * \detail This function returns the Synchronisation status
 *
 * \return synchronisation status
 */
typedef lu_bool_t (*CALLBACK_getTimeSyncStatusDef)(void);

/**
 * \brief Signature for the Get Local State function
 *
 * \detail This function returns the LOCAL status
 *
 * \return LOCAL status
 */
typedef lu_bool_t (*CALLBACK_getLocalState)(void *dbPtr);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

#endif /* LUCY_COMMON_DEFS_H_ */

/*
 *********************** End of file ******************************************
 */
