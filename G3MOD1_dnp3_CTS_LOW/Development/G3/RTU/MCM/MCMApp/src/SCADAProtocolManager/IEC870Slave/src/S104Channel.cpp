/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:S104Channel.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief Protocol Physical Channel
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   30 Sep 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "S104Channel.h"
#include "S104TMWIncludes.h"

#include "S104Session.h"
#include "ISlaveProtocolChannel.h"
#include "ISCADAConnectionManager.h"

#include "S104Debug.h"

// For S104 Redundancy
extern "C"
{
#include "tmwscl/i870/lnk4rdcy.h"
}

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static  Logger& log(Logger::getLogger(SUBSYSTEM_ID_SCADA_HANDLER));

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
S104Channel::S104Channel(GeminiDatabase&  g3database, S104Protocol* protocol):
                        mp_protocol(protocol),
                        m_g3db(g3database),
                        mp_tmwchnl(NULL),
                        mp_tmwchnlCnfg(NULL),
                        m_sessions(),
                        m_rdcyEnabled(LU_FALSE),
                        m_rdcychnls()

{
}

S104Channel::~S104Channel()
{
    /* Delete all sessions*/
    for (S104SessionVect::iterator it = m_sessions.begin();
                    it != m_sessions.end(); ++it)
    {
        delete (*it);
    }
    m_sessions.clear();

    closeChannel();


    /* Delete config */
    if(mp_tmwchnlCnfg != NULL)
    {
        // Delete redundancy channels
        for (int i = 0; i < mp_tmwchnlCnfg->rdcySize; ++i)
        {
            delete mp_tmwchnlCnfg->rdcyConfig[i];
            mp_tmwchnlCnfg->rdcyConfig[i] = NULL;
        }
        delete[] mp_tmwchnlCnfg->rdcyConfig;
        mp_tmwchnlCnfg->rdcyConfig = NULL;


        // Delete channel config
        delete mp_tmwchnlCnfg;
        mp_tmwchnlCnfg = NULL;
    }

}

SCADAP_ERROR S104Channel::openChannel(TMWAPPL* tmwappl)
{
    DBG_INFO("%s opening channel",TITLE);

    checkNotNull(tmwappl,          "IEC104 application context is null");
    checkNotNull(mp_tmwchnlCnfg,   "IEC104 channel not configured");

    /*Open channel*/
    if(isRedundancyEnabled())
    {
        mp_tmwchnl = openGroupChannel(tmwappl);
    }
    else
    {
        mp_tmwchnl = i104chnl_openChannel(tmwappl,
                        &(mp_tmwchnlCnfg->pChnlConfig),
                        &(mp_tmwchnlCnfg->pLinkConfig),
                        &(mp_tmwchnlCnfg->pPhysConfig),
                        &(mp_tmwchnlCnfg->pIOConfig  ),
                        &(mp_tmwchnlCnfg->pTMWTargConfig)
                        );
    }

    checkNotNull(mp_tmwchnl,   "IEC104 channel open failed");


    /* Open all Sessions*/
    for (S104SessionVect::iterator itSession = m_sessions.begin();
                    itSession != m_sessions.end(); ++itSession)
    {
        (*itSession)->openSession(mp_tmwchnl);
    }

    return  SCADAP_ERROR_NONE;
}

TMWCHNL* S104Channel::openGroupChannel(TMWAPPL* tmwappl)
{
    TMWCHNL *pRdcyGroup;
    TMWCHNL *pRdntChnl;
    LNK4RDCY_CONFIG rdcyConfig;

    lnk4rdcy_initConfig(&rdcyConfig);
    rdcyConfig.isControlling = TMWDEFS_FALSE;
    rdcyConfig.incrementalTimeout = TMWDEFS_SECONDS(30);//TODO get value from config
    mp_tmwchnlCnfg->pLinkConfig.isControlling = TMWDEFS_FALSE;

    pRdcyGroup = lnk4rdcy_initRdcyGroup(&rdcyConfig);
    if(pRdcyGroup == TMWDEFS_NULL)
    {
        log.error("Failed to open S104 group channel");
        return NULL;
    }

    LINIO_CONFIG pIOConfig = mp_tmwchnlCnfg->pIOConfig;

    for (int i = -1; i < mp_tmwchnlCnfg ->rdcySize; ++i) {
        if(i >= 0)
        {
            // Get redundancy channel config
            strcpy(pIOConfig.linTCP.ipAddress, mp_tmwchnlCnfg->rdcyConfig[i]->ipAddress);
            pIOConfig.linTCP.ipPort = mp_tmwchnlCnfg->rdcyConfig[i]->ipPort;
        }

        pRdntChnl = lnk4rdcy_openRedundantChannel(tmwappl, pRdcyGroup,
                        &(mp_tmwchnlCnfg->pChnlConfig),
                        &(mp_tmwchnlCnfg->pLinkConfig),
                        &(mp_tmwchnlCnfg->pPhysConfig),
                        & pIOConfig,
                        &(mp_tmwchnlCnfg->pTMWTargConfig)
                        );

        if(pRdntChnl != TMWDEFS_NULL)
        {
            m_rdcychnls.push_back(pRdntChnl);
            DBG_INFO("%s Succeeded to open redundant channel.ip:%s port:%d",
                            TITLE,pIOConfig.linTCP.ipAddress, pIOConfig.linTCP.ipPort);
        }
        else
        {
           log.error("Failed to open S104 redundant channel");
        }
    };

    log.info("%s Open S104 group channel successfully with rdcy num:%d",
                    TITLE, m_rdcychnls.size());
    return pRdcyGroup;
}

void S104Channel::closeChannel()
{
    /* Close all channel's sessions first */
    lu_uint32_t i = 0;
    for (S104SessionVect::iterator it = m_sessions.begin(); it != m_sessions.end(); ++it)
    {
        DBG_INFO("%s closing S104 session %u ...",TITLE, i++);
        (*it)->closeSession();
    }

    if(mp_tmwchnl != NULL && isRedundancyEnabled())
    {
        /* Close all redundant channels*/
        for (RdcyChnlVect::iterator it = m_rdcychnls.begin();
                        it != m_rdcychnls.end(); ++it)
        {
            if(lnk4rdcy_closeRedundantChannel(mp_tmwchnl, *it) == TMWDEFS_FALSE)
                log.error("Failed to close S104 redundant channel");
        }

        // Delete group
        if (lnk4rdcy_deleteRdcyGroup(mp_tmwchnl) == TMWDEFS_FALSE)
        {
            log.error("Failed to close S104 redundant group channel");
        }

    }
    else if(mp_tmwchnl != NULL)
    {
        if (i104chnl_closeChannel(mp_tmwchnl) == TMWDEFS_FALSE)
        {
            log.error("Failed to close S104 channel");
        }
    }

    mp_tmwchnl = NULL;
}

void S104Channel::configure(const S104ChannelConfig& channelConfig)
{
    if(mp_tmwchnlCnfg == NULL)
    {
        mp_tmwchnlCnfg = new S104ChannelConfig();
    }
    memcpy(mp_tmwchnlCnfg, &channelConfig, sizeof(channelConfig));

    m_rdcyEnabled = ( (mp_tmwchnlCnfg != NULL) && (mp_tmwchnlCnfg->rdcySize > 0) );

    /** TODO: apply configuration to existing channel. */
}



void S104Channel::addSession(S104Session* sessionPtr)
{
    if(sessionPtr != NULL)
    {
        m_sessions.push_back(sessionPtr);
    }
}




/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
