/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:TMWIncludes.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief s104 common protocol stack includes
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   26 Sep 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef S104TMWINCLUDES_H
#define S104TMWINCLUDES_H
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

extern "C"
{
#include "tmwscl/utils/tmwdb.h"
#include "tmwscl/utils/tmwpltmr.h"
#include "tmwscl/utils/tmwtargp.h"

#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/i104chnl.h"
#include "tmwscl/i870/s104sesn.h"
#include "tmwscl/i870/s104sctr.h"
#include "tmwscl/i870/s14sesn.h"
#include "tmwscl/i870/i870util.h"
#include "tmwscl/i870/s14rbe.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/i870chnl.h"
#include "tmwscl/i870/s14msp.h"
#include "tmwscl/i870/s14mdp.h"
#include "tmwscl/i870/s14mmena.h"
#include "tmwscl/i870/s14mmenb.h"
#include "tmwscl/i870/s14mmenc.h"
#include "tmwscl/i870/s14mit.h"
#include "tmwscl/i870/s14cscna.h"
#include "tmwscl/i870/s4ctsta.h"

#include "tmwscl/i870/lucy/lucy_s14data.h"

#include "LinIoTarg/liniotarg.h"
}

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

#endif /* S104TMWINCLUDES_H */

/*
 *********************** End of file ******************************************
 */
