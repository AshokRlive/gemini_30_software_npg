/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i870ft12.h
 * description: IEC 60870-5 FT12 definitions
 */
#ifndef I870FT12_DEFINED
#define I870FT12_DEFINED

#define I870FT12_BROADCAST_ADDRESS_8     0xFF
#define I870FT12_BROADCAST_ADDRESS_16    0xFFFF

/* frame synchronization octets */
#define I870FT12_SYNC_OCTET_SINGLE    0xE5
#define I870FT12_SYNC_OCTET_FIXED     0x10
#define I870FT12_SYNC_OCTET_VARIABLE  0x68
#define I870FT12_SYNC_OCTET_END       0x16

/* bit masks for Data Link Header CONTROL byte */
#define I870FT12_CONTROL_DIR          0x80   /* Direction                   */
#define I870FT12_CONTROL_PRM          0x40   /* Primary Message             */
#define I870FT12_CONTROL_FCB          0x20   /* Frame Count Bit             */
#define I870FT12_CONTROL_FCV          0x10   /* Frame Count bit Valid       */
#define I870FT12_CONTROL_ACD          0x20   /* Access Demand for class 1   */
                                             /* data                        */
#define I870FT12_CONTROL_DFC          0x10   /* Data Flow Control bit       */
#define I870FT12_CONTROL_FC_MASK      0x0F   /* defines the frame type      */

/* Primary Frame Control Byte function codes  */
#define I870FT12_FC_LINK_RESET_CU        0
#define I870FT12_FC_RESET_USER           1
#define I870FT12_FC_TEST_FUNCTION        2
#define I870FT12_FC_USER_DATA            3
#define I870FT12_FC_UNCONFIRMED_DATA     4
#define I870FT12_FC_LINK_RESET_FCB       7
#define I870FT12_FC_REQUEST_ACCESS       8
#define I870FT12_FC_REQUEST_STATUS       9
#define I870FT12_FC_REQUEST_DATA_C1     10
#define I870FT12_FC_REQUEST_DATA_C2     11

/* Secondary Frame Control Byte Function Codes  */
#define I870FT12_FC_CONFIRM_ACK          0
#define I870FT12_FC_CONFIRM_NACK         1
#define I870FT12_FC_RESPOND_USER_DATA    8
#define I870FT12_FC_RESPOND_NO_DATA      9
#define I870FT12_FC_RESPOND_STATUS      11
#define I870FT12_FC_NOT_FUNCTIONING     14
#define I870FT12_FC_NOT_USED            15
#define I870FT12_FC_NO_CONFIRM          16   /* used internally to signal   */
                                             /* no confirm                  */

/* Indices of fields within the IEC 870-5 FT1.2 frame */
#define I870FT12_INDEX_SYNC              0

/* Indices for fixed length frames */
#define I870FT12_INDEX_FIX_CONTROL       1
#define I870FT12_INDEX_FIX_ADDRESS       2

/* Indices for fixed length frames using a link address size of 0 */
#define I870FT12_INDEX_FIX_ADDR0_CHKSUM  2
#define I870FT12_INDEX_FIX_ADDR0_END     3
#define I870FT12_LENGTH_FIX_ADDR0        4

/* Indices for fixed length frames using a link address size of 1 */
#define I870FT12_INDEX_FIX_ADDR1_ADDR    2
#define I870FT12_INDEX_FIX_ADDR1_CHKSUM  3
#define I870FT12_INDEX_FIX_ADDR1_END     4
#define I870FT12_LENGTH_FIX_ADDR1        5

/* Indices for fixed length frames using a link address size of 2 */
#define I870FT12_INDEX_FIX_ADDR2_ADDR    2
#define I870FT12_INDEX_FIX_ADDR2_CHKSUM  4
#define I870FT12_INDEX_FIX_ADDR2_END     5
#define I870FT12_LENGTH_FIX_ADDR2        6

/* Indices for variable length frames */
#define I870FT12_INDEX_VAR_LENGTH        1
#define I870FT12_INDEX_VAR_LENGTH_COPY   2
#define I870FT12_INDEX_VAR_SYNC_SECOND   3
#define I870FT12_INDEX_VAR_CONTROL       4
#define I870FT12_INDEX_VAR_ADDRESS       5

/* Indices for start of ASDU for variable length   */
/* frames using link address sizes of 0, 1, and 2. */
#define I870FT12_INDEX_VAR_ADDR0_ASDU    5
#define I870FT12_INDEX_VAR_ADDR1_ASDU    6
#define I870FT12_INDEX_VAR_ADDR2_ASDU    7

/* Other definitions */
#define I870FT12_NUM_NOT_IN_LENGTH  6  /* For variable length frames, the  */
                                       /* value of the length octect does  */
                                       /* not include the two sync octets, */
                                       /* the two length octects, the      */
                                       /* checksum octect, and the end     */
                                       /* sync octect                      */

#define I870FT12_NUM_IN_LENGTH      3  /* For variable length frames, the  */
                                       /* link control byte (1 byte) and   */
                                       /* the link address (2-bytes) are   */
                                       /* counted in length                */

#define I870FT12_ASDU_MAX_SIZE   (255-I870FT12_NUM_IN_LENGTH) /* Maximum   */
                                       /* ASDU size for FT 1.2 frames is   */
                                       /* max link layer length (255       */
                                       /* bytes) minus the link fields     */
                                       /* that are counted in the length   */

#endif /* I870FT12_DEFINED */
 
