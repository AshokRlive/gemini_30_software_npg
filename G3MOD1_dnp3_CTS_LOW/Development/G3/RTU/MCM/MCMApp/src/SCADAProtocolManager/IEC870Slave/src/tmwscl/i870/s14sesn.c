/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14sesn.c
 * description: Slave IEC 60870-5-101/104 session
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/utils/tmwtarg.h"
#include "tmwscl/utils/tmwchnl.h"
#include "tmwscl/utils/tmwscl.h"
#include "tmwscl/utils/tmwdiag.h"
#include "tmwscl/i870/s14mem.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/i14diag.h"
#include "tmwscl/i870/s14diag.h"
#include "tmwscl/i870/s14dbas.h"
#include "tmwscl/i870/s14sesn.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/i870chnl.h"


/* function: _buildCyclicResponse */
static TMWTYPES_BOOL TMWDEFS_LOCAL _buildCyclicResponse(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  S14SESN *pS14Session = (S14SESN *)pSector->pSession;

  TMWSESN_TX_DATA *pTxData = i870chnl_newTxData(pSector->pSession->pChannel, 
    pSector->pSession, pSector, pS14Session->maxASDUSize);
 
  if(pTxData == TMWDEFS_NULL)
  {
    S14DIAG_ERROR(pSector->pSession, pSector, S14DIAG_ALLOC_CYCLIC);
    return(TMWDEFS_FALSE);
  } 
  
#if TMWCNFG_SUPPORT_DIAG
  pTxData->pMsgDescription = "Cyclic Response";
#endif
  pTxData->txFlags = TMWSESN_TXFLAGS_NO_RESPONSE;
  pTxData->responseTimeout = p14Sector->defaultResponseTimeout;

  if(s14dbas_readGroup(pTxData, 
    pSector, I14DEF_COT_CYCLIC, TMWDEFS_GROUP_MASK_CYCLIC,
    &p14Sector->cyclicGroupIndex, &p14Sector->cyclicPointIndex))
  {
    i870chnl_sendMessage(pTxData);
    return(TMWDEFS_TRUE);
  }
  else
  {
    i870chnl_freeTxData(pTxData);
    return(TMWDEFS_FALSE);
  }
}

/* function: _buildBackgroundResponse */
static TMWTYPES_BOOL TMWDEFS_LOCAL _buildBackgroundResponse(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  S14SESN *pS14Session = (S14SESN *)pSector->pSession;

  TMWSESN_TX_DATA *pTxData = i870chnl_newTxData(pSector->pSession->pChannel, 
        pSector->pSession, pSector, pS14Session->maxASDUSize);
 
  if(pTxData == TMWDEFS_NULL)
  {
    S14DIAG_ERROR(pSector->pSession, pSector, S14DIAG_ALLOC_BACKGROUND);
    return(TMWDEFS_FALSE);
  } 
  
#if TMWCNFG_SUPPORT_DIAG
  pTxData->pMsgDescription = "Background Response";
#endif
  pTxData->txFlags = TMWSESN_TXFLAGS_NO_RESPONSE;
  pTxData->responseTimeout = p14Sector->defaultResponseTimeout;

  if(s14dbas_readGroup(pTxData, 
    pSector, I14DEF_COT_BACKGROUND, TMWDEFS_GROUP_MASK_BACKGROUND,
    &p14Sector->backgroundGroupIndex, &p14Sector->backgroundPointIndex))
  {
    i870chnl_sendMessage(pTxData);
    return(TMWDEFS_TRUE);
  }
  else
  {
    i870chnl_freeTxData(pTxData);
    return(TMWDEFS_FALSE);
  }
}

/* function: s14sesn_openSession */
void TMWDEFS_GLOBAL s14sesn_openSession(TMWSESN *pSession)
{
  S14SESN *pS14Session = (S14SESN *)pSession;
  pS14Session->pLastSectorPolled = TMWDEFS_NULL;
}

/* function: s14sesn_closeSession */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14sesn_closeSession(TMWSESN *pSession)
{
  /* Check pSession for NULL if code is added here */

  TMWTARG_UNUSED_PARAM(pSession);
  return(TMWDEFS_TRUE);
}

/* function: s14sesn_infoCallback */
void TMWDEFS_GLOBAL s14sesn_infoCallback( 
  TMWSESN *pSession, TMWSCL_INFO sesnInfo)
{
  S14SESN *pS14Session = (S14SESN *)pSession;
  S14SCTR *p14Sector = TMWDEFS_NULL;

  /* Loop through sectors, marking each as reset */
  while((p14Sector = (S14SCTR *)tmwdlist_getAfter(
    &pS14Session->i870.sectorList, (TMWDLIST_MEMBER *)p14Sector)) != TMWDEFS_NULL)
  {
    p14Sector->i870.pProcessInfoFunc((TMWSCTR*)p14Sector, sesnInfo);
  }
 
  return;
}

/* function: s14sesn_sendNegActCon */
void TMWDEFS_GLOBAL s14sesn_sendNegActCon(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg,
  TMWTYPES_UCHAR cot)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  /* If there is already a Negative response to send don't overwrite it */
  if(p14Sector->pUnknownResponse == TMWDEFS_NULL)
  {
    TMWSESN *pSession = pSector->pSession;
    /*  
     * allocate a message to build response in 
     */
    TMWSESN_TX_DATA *pTxData = i870chnl_newTxData(pSession->pChannel, pSession, pSector,
      ((S14SESN *)pSession)->maxASDUSize);

    if(pTxData == TMWDEFS_NULL)
    {
      S14DIAG_ERROR(pSession, pSector, S14DIAG_ALLOC_UNKNOWN);
      return;
    } 

  #if TMWCNFG_SUPPORT_DIAG
    pTxData->pMsgDescription = "Negative Response";
  #endif
    pTxData->txFlags = TMWSESN_TXFLAGS_NO_RESPONSE;
    pTxData->responseTimeout = p14Sector->defaultResponseTimeout;

    /* Build response */
    i870util_buildMessageHeader(pSession, pTxData,
      pMsg->typeId, (TMWTYPES_UCHAR)(cot | I14DEF_COT_NEGATIVE_CONFIRM), 
      pMsg->origAddress, pMsg->asduAddress);

    memcpy(&pTxData->pMsgBuf[pTxData->msgLength], &pMsg->pRxData->pMsgBuf[pTxData->msgLength], 
      pMsg->pRxData->msgLength - pTxData->msgLength);

    pTxData->msgLength = pMsg->pRxData->msgLength;
  
    /* Put this here so it can be sent when sending responses */
    p14Sector->pUnknownResponse = pTxData;
  }
}

/* function: _checkForValidBroadcast */
static TMWSCTR * TMWDEFS_LOCAL _checkForValidBroadcast(
  S14SESN *pS14Session, 
  TMWTYPES_USHORT sectorAddress, 
  TMWTYPES_UCHAR typeId)
{  
  TMWTYPES_USHORT globalAddress = (pS14Session->i870.asduAddrSize == 1) 
    ? I14DEF_BROADCAST_ADDRESS_8 : I14DEF_BROADCAST_ADDRESS_16;

  if(sectorAddress == globalAddress)
  { 
    /* only these 4 are allowed to be sent as broadcast requests */
    if( (typeId == I14DEF_TYPE_CICNA1) 
      ||(typeId == I14DEF_TYPE_CCINA1)
      ||(typeId == I14DEF_TYPE_CCSNA1)
      ||(typeId == I14DEF_TYPE_CRPNA1))
    {
      return (TMWSCTR *)tmwdlist_getFirst(&pS14Session->i870.sectorList); 
    } 
  }

  return TMWDEFS_NULL;
}

/* function: _validateMessageSize */
static TMWTYPES_BOOL TMWDEFS_LOCAL _validateMessageSize(
  TMWSESN *pSession,
  I870UTIL_MESSAGE *pMsg,
  TMWTYPES_SHORT dataSize)
{
#if S14DATA_SUPPORT_CCTNA 
  /* CCTNA is not indexed and can have a quantity other than 1 */
  if(pMsg->typeId == I14DEF_TYPE_CCTNA1)
  {
    if(pMsg->indexed)
    {
      return TMWDEFS_FALSE;
    }
  }
  /* All other 101/104 typeIds in the control direction are indexed with a quantity of 1 */
  else 
#endif
  if((pMsg->quantity != 1) 
      || (!pMsg->indexed))
  {
    return TMWDEFS_FALSE;
  }
  return i870util_validateMessageSize(pSession, pMsg, dataSize);
}
    
/* function: s14sesn_parseFrameCallback */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14sesn_parseFrameCallback(
  TMWSESN *pSession, 
  TMWSESN_RX_DATA *pRxFrame,
  const void *pFuncTable)
{
  S14SESN_FUNC_ENTRY *pFuncEntry = TMWDEFS_NULL;
  I870UTIL_MESSAGE msgHeader;
  TMWTYPES_USHORT tableIndex;
  TMWTYPES_UCHAR cot=0;
  S14SCTR *p14Sector;
  TMWSCTR *pSector;
  S14SESN *pS14Session = (S14SESN *)pSession;
  S14SESN_FUNC_ENTRY *funcTable = (S14SESN_FUNC_ENTRY *) pFuncTable;
  TMWTYPES_BOOL isBroadcast = TMWDEFS_FALSE;

  /* Parse message header */
  if(!i870util_readMessageHeader(pSession, &msgHeader, pRxFrame))
  {
    return(TMWDEFS_FALSE);
  }

  /* Lookup processing info based on type id */
  for(tableIndex = 0; funcTable[tableIndex].typeId != 0; tableIndex++)
  {
    if(msgHeader.typeId == funcTable[tableIndex].typeId)
    {
      pFuncEntry = &funcTable[tableIndex];
      break;
    }
  }
  
  /* If this ASDU type id is not supported, 
   * this needs to be echoed back to master with a COT of UNKNOWN_TYPE 
   */
  if(pFuncEntry == TMWDEFS_NULL)
  {
    cot = I14DEF_COT_UNKNOWN_ASDU_TYPE;
  }
   
  /* Else validate message size based on header info */
  else if(!_validateMessageSize(pSession, &msgHeader, pFuncEntry->dataSize))
  {
    S14DIAG_ERROR(pSession, TMWDEFS_NULL, S14DIAG_INVALID_SIZE);
    I14DIAG_FRAME_ERROR(pSession, &msgHeader, pRxFrame);
#ifdef TMW_SUPPORT_MONITOR
    /* If monitor mode, let it pass on to see what we can decipher */
    if(!pSession->pChannel->pPhysContext->monitorMode)
    {
      return(TMWDEFS_FALSE);
    }
#else
  return(TMWDEFS_FALSE);
#endif
  }

  /* See if sector is in our list */
  pSector = i870sesn_lookupSector(pSession, msgHeader.asduAddress);

  /* if address was not found see if it is a valid broadcast request */
  if(pSector == TMWDEFS_NULL)
  {
    pSector = _checkForValidBroadcast(pS14Session, msgHeader.asduAddress, msgHeader.typeId);
    /* Assume it is broadcast, if pSector is NULL we will not use this flag */
    isBroadcast = TMWDEFS_TRUE;
  }

  if(pSector == TMWDEFS_NULL)
  {
    TMWSESN_STAT_CALLBACK_FUNC(pSession, TMWSESN_STAT_UNKNOWN_SECTOR, &msgHeader.asduAddress);

#ifdef TMW_SUPPORT_MONITOR 
    if(pSession->pChannel->pPhysContext->monitorMode)
    {
      pSector = i870sesn_lookupSector(pSession, msgHeader.asduAddress);
    }
    if(pSector == TMWDEFS_NULL)
#endif
    {
      /* Send reply with COT unknown ASDU address */
      cot = I14DEF_COT_UNKNOWN_ASDU_ADDR;
      S14DIAG_ERROR(pSession, pSector, S14DIAG_UNKNOWN_SECTOR);

      /* Put the "unknown sector" msg on the first slave sector of this session */
      while((pSector = (TMWSCTR *)tmwdlist_getAfter(
        &pS14Session->i870.sectorList, (TMWDLIST_MEMBER *)pSector)) != TMWDEFS_NULL)
      {
        p14Sector = (S14SCTR *)pSector;
        if(p14Sector != TMWDEFS_NULL)
        {
          break;
        }
      } 
      /* If sector is still NULL, just return false */
      if(pSector == TMWDEFS_NULL)
      {
        return(TMWDEFS_FALSE);
      }
    }
  }
 
  /* If there is a function in the table, and no other error yet */
  if((pFuncEntry != TMWDEFS_NULL) && (cot == 0))
  { 
    int offset = -1;

    /* Loop to allow for broadcast address sending to all sectors */
    do 
    {
      /* Diagnostics */
      I14DIAG_FRAME_RECEIVED(pSector, pRxFrame);

      /* If this was a broadcast message, we need to save offset first time through
       * and reset it for each sector we give message to.
       */
      if(offset == -1)
        offset = msgHeader.offset;
      else
        msgHeader.offset = (TMWTYPES_UCHAR)offset;

      /* Call appropriate processing function from table */
      pFuncEntry->pProcessRequestFunc(pSector, &msgHeader);
      
      if(!isBroadcast)
        break;

      /* If it is a valid broadcast request, get the next sector. */
    } while((pSector = (TMWSCTR *)tmwdlist_getAfter(
        &pS14Session->i870.sectorList, (TMWDLIST_MEMBER *)pSector)) != TMWDEFS_NULL);

    return(TMWDEFS_TRUE);
  }

  /* If ASDU typeId is not supported by SCL */ 
  if(cot == I14DEF_COT_UNKNOWN_ASDU_TYPE)
  {
    /* If user has registered a function to process ASDU types, call it */ 
    if((pS14Session->pProcessRequestCallback != TMWDEFS_NULL)
    && (pS14Session->pProcessRequestCallback(pS14Session->pProcessRequestParam, 
      pSector, &msgHeader) == TMWDEFS_TRUE))
    {
      return(TMWDEFS_TRUE);
    }
    else 
    {
      S14DIAG_ERROR(pSession, pSector, S14DIAG_UNKNOWN_ASDU);
    }
  }

  /* Build and send a Negative Activation Confirmation */
  s14sesn_sendNegActCon(pSector, &msgHeader, cot);

  return(TMWDEFS_TRUE);
}

/* function: _nextSector */
static TMWSCTR * TMWDEFS_LOCAL _nextSector(
  TMWSESN *pSession,
  TMWSCTR *pSector)
{
  S14SESN *pS14Session = (S14SESN *)pSession;

  TMWSCTR *pNextSector = (TMWSCTR *)tmwdlist_getAfter(
    &pS14Session->i870.sectorList, (TMWDLIST_MEMBER *)pSector);

  if(pNextSector == TMWDEFS_NULL)
    pNextSector = (TMWSCTR *)tmwdlist_getFirst(&pS14Session->i870.sectorList);

  return(pNextSector);
}

/* function: s14sesn_checkClassCallback */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14sesn_checkClassCallback( 
  TMWSESN *pSession, 
  TMWDEFS_CLASS_MASK classMask, 
  TMWTYPES_BOOL buildResponse,
  const void *pFuncTable)
{
  S14SESN *pS14Session = (S14SESN *)pSession;
  S14SESN_FUNC_ENTRY *funcTable = (S14SESN_FUNC_ENTRY *)pFuncTable;
  TMWTYPES_USHORT tableIndex;
  TMWSCTR *pLastSector;
  TMWSCTR *pSector;

  pSector = pS14Session->pLastSectorPolled;

  pLastSector = pSector;
  if(pLastSector == TMWDEFS_NULL)
    pLastSector = (TMWSCTR *)tmwdlist_getFirst(&pS14Session->i870.sectorList);

  if(pLastSector == TMWDEFS_NULL)
    return(TMWDEFS_FALSE);

  do
  {
    S14SCTR *p14Sector;

    pSector = _nextSector(pSession, pSector);
    if(pSector == TMWDEFS_NULL)
      break;

    p14Sector = (S14SCTR *)pSector;
    if(buildResponse)
    {
      pS14Session->pLastSectorPolled = pSector;   
    }

    /* If there is a response to be sent for unknown type id, COT, ASDU address or IOA */
    if(p14Sector->pUnknownResponse != TMWDEFS_NULL)
    {
      if(buildResponse)
      {
        i870chnl_sendMessage(p14Sector->pUnknownResponse);
        p14Sector->pUnknownResponse = TMWDEFS_NULL;
      }
      return(TMWDEFS_TRUE);
    }

    /* Check for sector reset */
    if((classMask & TMWDEFS_CLASS_MASK_ONE) != 0)
    {
      TMWTYPES_UCHAR resetCOI;

      if(s14data_hasSectorReset(p14Sector->i870.pDbHandle, &resetCOI))
      {
        if(buildResponse)
        {
          TMWSESN_TX_DATA *pTxData = i870chnl_newTxData(pSector->pSession->pChannel, 
            pSector->pSession, pSector, pS14Session->maxASDUSize);

          if(pTxData == TMWDEFS_NULL)
            return(TMWDEFS_FALSE);

#if TMWCNFG_SUPPORT_DIAG
          pTxData->pMsgDescription = "End of Initialization";
#endif
          pTxData->txFlags = TMWSESN_TXFLAGS_NO_RESPONSE;
     
          i870util_buildMessageHeader(pSession, pTxData, 
            I14DEF_TYPE_MEINA1, I14DEF_COT_INITIALIZED, 0, 
            p14Sector->i870.asduAddress);

          i870util_storeInfoObjAddr(pSession, pTxData, 0);

          pTxData->pMsgBuf[pTxData->msgLength++] = resetCOI;

          s14data_clearSectorReset(p14Sector->i870.pDbHandle);

          i870chnl_sendMessage(pTxData);
          
#if S14DATA_SUPPORT_CICNAWAIT
          if(!pS14Session->cyclicWaitCICNAComplete) 
#endif
          {
            /* cyclicPeriod of 0 turns cyclic data off,
             * no matter what value cyclicFirstPeriod is
             */
            if(p14Sector->cyclicPeriod != 0)
            {
              tmwtimer_start(&p14Sector->cyclicTimer,
                p14Sector->cyclicFirstPeriod, pSession->pChannel,
                s14sctr_cyclicTimeout, pSector);
            }
          }
        }
        return(TMWDEFS_TRUE);
      }
    }

    /* Generate response to open requests */
    if((classMask & TMWDEFS_CLASS_MASK_ONE) != 0)
    {
      tableIndex = 0;
      while(funcTable[tableIndex].typeId != 0)
      {
        if(funcTable[tableIndex].pBuildResponseFunc != TMWDEFS_NULL)
        {
          if(funcTable[tableIndex].pBuildResponseFunc(pSector, buildResponse))
            return(TMWDEFS_TRUE);
        }
        tableIndex += 1;
      } 
      
      if(pS14Session->pBuildResponseCallback != TMWDEFS_NULL)
      {
        if(pS14Session->pBuildResponseCallback(
          pS14Session->pBuildResponseParam, pSector, buildResponse))
          return(TMWDEFS_TRUE);
      }
    }

    /* Generate cyclic data */
    if((classMask & TMWDEFS_CLASS_MASK_TWO) != 0)
    {
      /* If cyclic data ready, send it */
      if(p14Sector->cyclicGroupIndex != GROUP_INDEX_IDLE)
      {
        /* Note that if buildResponse is false we really need to go through
         * the data and make sure there are really data points to send, but 
         * this would only be an issue if there was no cyclic data in which 
         * case one would hope that cyclic data would be disabled.
         */
        if(buildResponse)
          return(_buildCyclicResponse(pSector));

        return(TMWDEFS_FALSE);
      }

      /* If background scan data ready, send it */
      if(p14Sector->backgroundGroupIndex != GROUP_INDEX_IDLE)
      {
        if(buildResponse)
          return(_buildBackgroundResponse(pSector));

        return(TMWDEFS_FALSE);
      }
    }
  } while(pSector != pLastSector);

  return(TMWDEFS_FALSE);
}

/* function s14sesn_prepareMessage */
void TMWDEFS_GLOBAL s14sesn_prepareMessage(
  TMWSESN_TX_DATA *pTxData)
{  
  /* Diagnostics */
  I14DIAG_FRAME_SENT(pTxData);
}

/* function: s14sesn_processRequest */
/* Empty routine to avoid null pointer in function table */
void TMWDEFS_CALLBACK s14sesn_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg) 
{
  TMWTARG_UNUSED_PARAM(pSector);
  TMWTARG_UNUSED_PARAM(pMsg);
  /* just return */ 
}
