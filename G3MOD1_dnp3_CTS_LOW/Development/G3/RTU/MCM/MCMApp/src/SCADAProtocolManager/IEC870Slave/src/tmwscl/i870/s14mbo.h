/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14mbo.h
 * description: This file is intended for internal SCL use only.
 *  IEC 60870-5-101 slave bitstring functionality.
 */
#ifndef S14MBO_DEFINED
#define S14MBO_DEFINED

#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/utils/tmwsesn.h"
#include "tmwscl/i870/s14sesn.h"
#include "tmwscl/i870/s14sctr.h"
#include "tmwscl/i870/s14dbas.h"
#include "tmwscl/i870/s14evnt.h"

/* Structure used to store bitstring events */
typedef struct S14MBOEventStruct {
  S14EVNT s14Event;
  TMWTYPES_ULONG bsi;
} S14MBO_EVENT;

#ifdef __cplusplus
extern "C" {
#endif

  /* function: s14mbo_init 
   * purpose: Initialize MBO event processing.
   * arguments:
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL s14mbo_init(
    TMWSCTR *pSector);

  /* function: s14mbo_close 
   * purpose: Close MBO event processing
   * arguments:
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL s14mbo_close(
    TMWSCTR *pSector);

  /* function: s14mbo_addEvent
   * purpose: Create and queue an MBO event
   * arguments:
   *  pSector - identifies sector
   *  cot - cause of transmission
   *  ioa - information object address
   *  bsi - binary state information, defined in 7.2.6.13
   *  flags - quality descriptor, defined in 7.2.6.3
   *  pTimeStamp - time of event
   *   if pTimeStamp != TMWDEFS_NULL Event will be sent using the ASDU specified 
   *   by sector config mboTimeFormat
   *    To set RES1 bit to indicate Substituted time, 
   *     set pTimeStamp->genuineTime to TMWEFS_FALSE and make sure
   *     S14DATA_SUPPORT_GENUINE_TIME is defined in s14data.h
   *   if pTimeStamp == TMWDEFS_NULL the event will be sent using ASDU 7 MBONA
   *   
   * returns:
   *  void * - non-NULL value indicates success; TMWDEFS_NULL indicates failure
   */
  TMWDEFS_SCL_API void * TMWDEFS_GLOBAL s14mbo_addEvent(
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot,
    TMWTYPES_ULONG ioa, 
    TMWTYPES_ULONG bsi, 
    TMWTYPES_UCHAR flags, 
    TMWDTIME *pTimeStamp);

  /* function: s14mbo_countEvents
   * purpose: count MBO events
   * arguments:
   *  pSector - identifies sector
   * returns:
   *  number of events in queue
   */
  TMWTYPES_USHORT TMWDEFS_GLOBAL s14mbo_countEvents(
    TMWSCTR *pSector);
 
  /* function: s14mbo_scanForChanges
   * purpose: Scans slave database for changes to MBO data points. If scanning
   *  is enabled and an MBO point has changed an event will be added to the MBO event 
   *  queue.
   * arguments:
   *  pSector - identifies sector
   * returns:
   *  TMWDEFS_TRUE if change(s) have been found and event(s) have been added to queue
   *  TMWDEFS_FALSE otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14mbo_scanForChanges(
    TMWSCTR *pSector);

  /* function: s14mbo_processEvents
   * purpose: Process MBO events into a message
   * arguments:
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   *  pEventTime - returns time of earliest event if a clock sync is required
   * returns:
   *  S14EVNT_SENT                - ASDU was sent
   *  S14EVNT_CLOCK_SYNC_REQUIRED - Spontaneous CCSNA is required
   *  S14EVNT_NOT_SENT            - No ASDU was sent
   */
  S14EVNT_STATUS TMWDEFS_GLOBAL s14mbo_processEvents(
    TMWSCTR *pSector, 
    TMWDTIME *pEventTime);

  /* function: s14mbo_processDblTransEvents  
   * purpose: Process MBO Double Transmission high priority events into a message
   * arguments:
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   * returns:
   *  S14EVNT_SENT                - ASDU was sent
   *  S14EVNT_NOT_SENT            - No ASDU was sent
   */
  S14EVNT_STATUS TMWDEFS_GLOBAL s14mbo_processDblTransEvents(
    TMWSCTR *pSector);

  /* function: s14mbo_readIntoResponse 
   * purpose: Read the MBO values for the point specified and store in response
   *  message to be sent to master
   * arguments:
   *  pTxData - pointer
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   *  cot - cause of tranmission
   *  groupMask - mask specifying what group(s) points should be in
   *  ioa - information object address
   *  pPointIndex - pointer to index of point to be read. This should be updated
   *   by this function.
   * returns:
   *  void
   */
  S14DBAS_GROUP_STATUS TMWDEFS_CALLBACK s14mbo_readIntoResponse(
    TMWSESN_TX_DATA *pTxData, 
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot,
    TMWDEFS_GROUP_MASK groupMask, 
    TMWTYPES_ULONG ioa, 
    TMWTYPES_USHORT *pPointIndex);

#ifdef __cplusplus
}
#endif
#endif /* S14MBO_DEFINED */
