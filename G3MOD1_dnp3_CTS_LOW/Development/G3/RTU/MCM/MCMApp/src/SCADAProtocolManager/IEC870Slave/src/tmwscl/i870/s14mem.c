/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14mem.c
 * description: IEC 60870-5-101/104 slave utilities.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/utils/tmwmem.h"
#include "tmwscl/i870/s14mem.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/s14mbo.h"
#include "tmwscl/i870/s14mdp.h"
#include "tmwscl/i870/s14mepta.h"
#include "tmwscl/i870/s14meptb.h"
#include "tmwscl/i870/s14meptc.h"
#include "tmwscl/i870/s14mit.h"
#include "tmwscl/i870/s14mitc.h"
#include "tmwscl/i870/s14mmena.h"
#include "tmwscl/i870/s14mmenb.h"
#include "tmwscl/i870/s14mmenc.h"
#include "tmwscl/i870/s14mmend.h"
#include "tmwscl/i870/s14mpsna.h"
#include "tmwscl/i870/s14msp.h"
#include "tmwscl/i870/s14mst.h"
#include "tmwscl/i870/s14fdrta.h"
#include "tmwscl/i870/s14sim.h"


#if S14DATA_SUPPORT_MULTICMDS
typedef struct{
  TMWMEM_HEADER     header;
  S14SCTR_CMD       data;
} S14MEM_SCTR_CMD;
#endif

typedef struct{
  TMWMEM_HEADER     header;
  S14MBO_EVENT      data;
} S14MEM_MBO_EVENT;

typedef struct{
  TMWMEM_HEADER     header;
  S14MDP_EVENT      data;
} S14MEM_MDP_EVENT;

typedef struct{
  TMWMEM_HEADER     header;
  S14MEPTA_EVENT    data;
} S14MEM_MEPTA_EVENT;

typedef struct{
  TMWMEM_HEADER     header;
  S14MEPTB_EVENT    data;
} S14MEM_MEPTB_EVENT;

typedef struct{
  TMWMEM_HEADER     header;
  S14MEPTC_EVENT    data;
} S14MEM_MEPTC_EVENT;

typedef struct{
  TMWMEM_HEADER     header;
  S14MIT_EVENT      data;
} S14MEM_MIT_EVENT;

#if S14DATA_SUPPORT_MITC
typedef struct{
  TMWMEM_HEADER     header;
  S14MITC_EVENT     data;
} S14MEM_MITC_EVENT;
#endif

typedef struct{
  TMWMEM_HEADER     header;
  S14MMENA_EVENT    data;
} S14MEM_MMENA_EVENT;

typedef struct{
  TMWMEM_HEADER     header;
  S14MMENB_EVENT    data;
} S14MEM_MMENB_EVENT;

#if S14DATA_SUPPORT_MME_C
typedef struct{
  TMWMEM_HEADER     header;
  S14MMENC_EVENT    data;
} S14MEM_MMENC_EVENT;
#endif

typedef struct{
  TMWMEM_HEADER     header;
  S14MMEND_EVENT    data;
} S14MEM_MMEND_EVENT;

typedef struct{
  TMWMEM_HEADER     header;
  S14MPSNA_EVENT    data;
} S14MEM_MPSNA_EVENT;

typedef struct{
  TMWMEM_HEADER     header;
  S14MSP_EVENT      data;
} S14MEM_MSP_EVENT;
 
typedef struct{
  TMWMEM_HEADER     header;
  S14MST_EVENT      data;
} S14MEM_MST_EVENT;

#if TMWCNFG_USE_SIMULATED_DB
typedef struct{
  TMWMEM_HEADER     header;
  S14SIM_DATABASE   data;
} S14MEM_SIM_DATABASE;

#if S14DATA_SUPPORT_FILE
typedef struct{
  TMWMEM_HEADER     header;
  S14SIM_FILE       data;
} S14MEM_SIM_FILE;

typedef struct{
  TMWMEM_HEADER     header;
  S14SIM_SECTION    data;
} S14MEM_SIM_SECTION;
#endif
#endif

static const TMWTYPES_CHAR *_nameTable[S14MEM_ALLOC_TYPE_MAX] = {
#if S14DATA_SUPPORT_MULTICMDS
  "S14SCTR_CMD",
#endif
  "S14MBO_EVENT", 
  "S14MDP_EVENT",
  "S14MEPTA_EVENT", 
  "S14MEPTB_EVENT",
  "S14MEPTC_EVENT",
  "S14MIT_EVENT", 
#if S14DATA_SUPPORT_MITC
  "S14MITC_EVENT", 
#endif
  "S14MMENA_EVENT",
  "S14MMENB_EVENT", 
#if S14DATA_SUPPORT_MME_C
  "S14MMENC_EVENT",
#endif
  "S14MMEND_EVENT",
  "S14MPSNA_EVENT", 
  "S14MSP_EVENT",
  "S14MST_EVENT",

#if TMWCNFG_USE_SIMULATED_DB
#if S14DATA_SUPPORT_FILE
  "S14SIM_FILE",
  "S14SIM_SECTION",
#endif
  "S14SIM_DATABASE"
#endif
};

#if !TMWCNFG_USE_DYNAMIC_MEMORY
/* Use static allocated array of memory instead of dynamic memory */

static S14MEM_SCTR_CMD          s14mem_sctr_cmds[S14CNFG_NUMALLOC_SCTR_CMDS];
static S14MEM_MBO_EVENT         s14mem_mbo_events[S14CNFG_NUMALLOC_MBO_EVENTS];
static S14MEM_MDP_EVENT         s14mem_mdp_events[S14CNFG_NUMALLOC_MDP_EVENTS];
static S14MEM_MEPTA_EVENT       s14mem_mepta_events[S14CNFG_NUMALLOC_MEPTA_EVENTS];
static S14MEM_MEPTB_EVENT       s14mem_meptb_events[S14CNFG_NUMALLOC_MEPTB_EVENTS];
static S14MEM_MEPTC_EVENT       s14mem_meptc_events[S14CNFG_NUMALLOC_MEPTC_EVENTS];
static S14MEM_MIT_EVENT         s14mem_mit_events[S14CNFG_NUMALLOC_MIT_EVENTS];
#if S14DATA_SUPPORT_MITC
static S14MEM_MITC_EVENT        s14mem_mitc_events[S14CNFG_NUMALLOC_MITC_EVENTS];
#endif
static S14MEM_MMENA_EVENT       s14mem_mmena_events[S14CNFG_NUMALLOC_MMENA_EVENTS];
static S14MEM_MMENB_EVENT       s14mem_mmenb_events[S14CNFG_NUMALLOC_MMENB_EVENTS];
#if S14DATA_SUPPORT_MME_C
static S14MEM_MMENC_EVENT       s14mem_mmenc_events[S14CNFG_NUMALLOC_MMENC_EVENTS];
#endif
static S14MEM_MMEND_EVENT       s14mem_mmend_events[S14CNFG_NUMALLOC_MMEND_EVENTS];
static S14MEM_MPSNA_EVENT       s14mem_mpsna_events[S14CNFG_NUMALLOC_MPSNA_EVENTS];
static S14MEM_MSP_EVENT         s14mem_msp_events[S14CNFG_NUMALLOC_MSP_EVENTS];
static S14MEM_MST_EVENT         s14mem_mst_events[S14CNFG_NUMALLOC_MST_EVENTS];

#if TMWCNFG_USE_SIMULATED_DB
#if S14DATA_SUPPORT_FILE
static S14MEM_SIM_FILE          s14mem_sim_files[S14CNFG_NUMALLOC_SIM_FILES];
static S14MEM_SIM_SECTION       s14mem_sim_sections[S14CNFG_NUMALLOC_SIM_SECTIONS];
#endif
static S14MEM_SIM_DATABASE      s14mem_sim_databases[S14CNFG_NUMALLOC_SIM_DATABASES];
#endif

#endif

static TMWMEM_POOL_STRUCT _s14allocTable[S14MEM_ALLOC_TYPE_MAX];

#if TMWCNFG_USE_DYNAMIC_MEMORY
void TMWDEFS_GLOBAL s14mem_initConfig(
  S14MEM_CONFIG   *pConfig)
{
  pConfig->numSctrCmds     = S14CNFG_NUMALLOC_SCTR_CMDS;
  pConfig->numMBOEvents    = S14CNFG_NUMALLOC_MBO_EVENTS;
  pConfig->numMDPEvents    = S14CNFG_NUMALLOC_MDP_EVENTS;
  pConfig->numMEPTAEvents  = S14CNFG_NUMALLOC_MEPTA_EVENTS;
  pConfig->numMEPTBEvents  = S14CNFG_NUMALLOC_MEPTB_EVENTS;
  pConfig->numMEPTCEvents  = S14CNFG_NUMALLOC_MEPTC_EVENTS;
  pConfig->numMITEvents    = S14CNFG_NUMALLOC_MIT_EVENTS;
#if S14DATA_SUPPORT_MITC
  pConfig->numMITCEvents   = S14CNFG_NUMALLOC_MITC_EVENTS;
#endif
  pConfig->numMMENAEvents  = S14CNFG_NUMALLOC_MMENA_EVENTS;
  pConfig->numMMENBEvents  = S14CNFG_NUMALLOC_MMENB_EVENTS;
  pConfig->numMMENCEvents  = S14CNFG_NUMALLOC_MMENC_EVENTS;
  pConfig->numMMENDEvents  = S14CNFG_NUMALLOC_MMEND_EVENTS;
  pConfig->numMPSNAEvents  = S14CNFG_NUMALLOC_MPSNA_EVENTS;
  pConfig->numMSPEvents    = S14CNFG_NUMALLOC_MSP_EVENTS;
  pConfig->numMSTEvents    = S14CNFG_NUMALLOC_MST_EVENTS;
#if TMWCNFG_USE_SIMULATED_DB
#if S14DATA_SUPPORT_FILE
  pConfig->numSimFiles     = S14CNFG_NUMALLOC_SIM_FILES;
  pConfig->numSimSections  = S14CNFG_NUMALLOC_SIM_SECTIONS;
#endif
  pConfig->numSimDbases    = S14CNFG_NUMALLOC_SIM_DATABASES;
#endif
}
#endif

/* routine: s14mem_init */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14mem_init(
 S14MEM_CONFIG *pConfig)
{
#if TMWCNFG_USE_DYNAMIC_MEMORY
  /* dynamic memory allocation supported */
  
  S14MEM_CONFIG  config; 

  /* If caller has not specified memory pool configuration, use the
   * default compile time values 
   */
  if(pConfig == TMWDEFS_NULL)
  {
    pConfig = &config;
    s14mem_initConfig(pConfig);
  }
  
#if S14DATA_SUPPORT_MULTICMDS
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_SCTR_CMD_TYPE,    pConfig->numSctrCmds,    sizeof(S14MEM_SCTR_CMD),    TMWDEFS_NULL))
    return TMWDEFS_FALSE;
#endif
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_MBO_EVENT_TYPE,   pConfig->numMBOEvents,   sizeof(S14MEM_MBO_EVENT),   TMWDEFS_NULL))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_MDP_EVENT_TYPE,   pConfig->numMDPEvents,   sizeof(S14MEM_MDP_EVENT),   TMWDEFS_NULL))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_MEPTA_EVENT_TYPE, pConfig->numMEPTAEvents, sizeof(S14MEM_MEPTA_EVENT), TMWDEFS_NULL))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_MEPTB_EVENT_TYPE, pConfig->numMEPTBEvents, sizeof(S14MEM_MEPTB_EVENT), TMWDEFS_NULL))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_MEPTC_EVENT_TYPE, pConfig->numMEPTCEvents, sizeof(S14MEM_MEPTC_EVENT), TMWDEFS_NULL))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_MIT_EVENT_TYPE,   pConfig->numMITEvents,   sizeof(S14MEM_MIT_EVENT),   TMWDEFS_NULL))
    return TMWDEFS_FALSE;
#if S14DATA_SUPPORT_MITC
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_MITC_EVENT_TYPE,  pConfig->numMITCEvents,  sizeof(S14MEM_MITC_EVENT),  TMWDEFS_NULL))
    return TMWDEFS_FALSE;
#endif
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_MMENA_EVENT_TYPE, pConfig->numMMENAEvents, sizeof(S14MEM_MMENA_EVENT), TMWDEFS_NULL))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_MMENB_EVENT_TYPE, pConfig->numMMENBEvents, sizeof(S14MEM_MMENB_EVENT), TMWDEFS_NULL))
    return TMWDEFS_FALSE;
#if S14DATA_SUPPORT_MME_C
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_MMENC_EVENT_TYPE, pConfig->numMMENCEvents, sizeof(S14MEM_MMENC_EVENT), TMWDEFS_NULL))
    return TMWDEFS_FALSE;
#endif
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_MMEND_EVENT_TYPE, pConfig->numMMENDEvents, sizeof(S14MEM_MMEND_EVENT), TMWDEFS_NULL))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_MPSNA_EVENT_TYPE, pConfig->numMPSNAEvents, sizeof(S14MEM_MPSNA_EVENT), TMWDEFS_NULL))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_MSP_EVENT_TYPE,   pConfig->numMSPEvents,   sizeof(S14MEM_MSP_EVENT),   TMWDEFS_NULL))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_MST_EVENT_TYPE,   pConfig->numMSTEvents,   sizeof(S14MEM_MST_EVENT),   TMWDEFS_NULL))
    return TMWDEFS_FALSE;
#if TMWCNFG_USE_SIMULATED_DB
#if S14DATA_SUPPORT_FILE
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_SIM_FILE_TYPE,    pConfig->numSimFiles,    sizeof(S14MEM_SIM_FILE),    TMWDEFS_NULL))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_SIM_SECTION_TYPE, pConfig->numSimSections, sizeof(S14MEM_SIM_SECTION), TMWDEFS_NULL))
    return TMWDEFS_FALSE;
#endif
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_SIM_DATABASE_TYPE,pConfig->numSimDbases,   sizeof(S14MEM_SIM_DATABASE),TMWDEFS_NULL))
    return TMWDEFS_FALSE;
#endif

#else
  /* static memory allocation supported */ 
  TMWTARG_UNUSED_PARAM(pConfig);  
  
#if S14DATA_SUPPORT_MULTICMDS
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_SCTR_CMD_TYPE,    S14CNFG_NUMALLOC_SCTR_CMDS,   sizeof(S14MEM_SCTR_CMD),     (TMWTYPES_UCHAR*)s14mem_sctr_cmds))
    return TMWDEFS_FALSE;
#endif
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_MBO_EVENT_TYPE,   S14CNFG_NUMALLOC_MBO_EVENTS,   sizeof(S14MEM_MBO_EVENT),   (TMWTYPES_UCHAR*)s14mem_mbo_events))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_MDP_EVENT_TYPE,   S14CNFG_NUMALLOC_MDP_EVENTS,   sizeof(S14MEM_MDP_EVENT),   (TMWTYPES_UCHAR*)s14mem_mdp_events))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_MEPTA_EVENT_TYPE, S14CNFG_NUMALLOC_MEPTA_EVENTS, sizeof(S14MEM_MEPTA_EVENT), (TMWTYPES_UCHAR*)s14mem_mepta_events))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_MEPTB_EVENT_TYPE, S14CNFG_NUMALLOC_MEPTB_EVENTS, sizeof(S14MEM_MEPTB_EVENT), (TMWTYPES_UCHAR*)s14mem_meptb_events))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_MEPTC_EVENT_TYPE, S14CNFG_NUMALLOC_MEPTC_EVENTS, sizeof(S14MEM_MEPTC_EVENT), (TMWTYPES_UCHAR*)s14mem_meptc_events))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_MIT_EVENT_TYPE,   S14CNFG_NUMALLOC_MIT_EVENTS,   sizeof(S14MEM_MIT_EVENT),   (TMWTYPES_UCHAR*)s14mem_mit_events))
    return TMWDEFS_FALSE;
#if S14DATA_SUPPORT_MITC
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_MITC_EVENT_TYPE,  S14CNFG_NUMALLOC_MITC_EVENTS,  sizeof(S14MEM_MITC_EVENT),  (TMWTYPES_UCHAR*)s14mem_mitc_events))
    return TMWDEFS_FALSE;
#endif
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_MMENA_EVENT_TYPE, S14CNFG_NUMALLOC_MMENA_EVENTS, sizeof(S14MEM_MMENA_EVENT), (TMWTYPES_UCHAR*)s14mem_mmena_events))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_MMENB_EVENT_TYPE, S14CNFG_NUMALLOC_MMENB_EVENTS, sizeof(S14MEM_MMENB_EVENT), (TMWTYPES_UCHAR*)s14mem_mmenb_events))
    return TMWDEFS_FALSE;
  
#if S14DATA_SUPPORT_MME_C
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_MMENC_EVENT_TYPE, S14CNFG_NUMALLOC_MMENC_EVENTS, sizeof(S14MEM_MMENC_EVENT), (TMWTYPES_UCHAR*)s14mem_mmenc_events))
    return TMWDEFS_FALSE;
#endif
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_MMEND_EVENT_TYPE, S14CNFG_NUMALLOC_MMEND_EVENTS, sizeof(S14MEM_MMEND_EVENT), (TMWTYPES_UCHAR*)s14mem_mmend_events))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_MPSNA_EVENT_TYPE, S14CNFG_NUMALLOC_MPSNA_EVENTS, sizeof(S14MEM_MPSNA_EVENT), (TMWTYPES_UCHAR*)s14mem_mpsna_events))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_MSP_EVENT_TYPE,   S14CNFG_NUMALLOC_MSP_EVENTS,   sizeof(S14MEM_MSP_EVENT),   (TMWTYPES_UCHAR*)s14mem_msp_events))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_MST_EVENT_TYPE,   S14CNFG_NUMALLOC_MST_EVENTS,   sizeof(S14MEM_MST_EVENT),   (TMWTYPES_UCHAR*)s14mem_mst_events))
    return TMWDEFS_FALSE;
#if TMWCNFG_USE_SIMULATED_DB
#if S14DATA_SUPPORT_FILE
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_SIM_FILE_TYPE,    S14CNFG_NUMALLOC_SIM_FILES,    sizeof(S14MEM_SIM_FILE),    (TMWTYPES_UCHAR*)s14mem_sim_files))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_SIM_SECTION_TYPE, S14CNFG_NUMALLOC_SIM_SECTIONS, sizeof(S14MEM_SIM_SECTION), (TMWTYPES_UCHAR*)s14mem_sim_sections))
    return TMWDEFS_FALSE;
#endif
  if(!tmwmem_lowInit(_s14allocTable, S14MEM_SIM_DATABASE_TYPE,S14CNFG_NUMALLOC_SIM_DATABASES,sizeof(S14MEM_SIM_DATABASE),(TMWTYPES_UCHAR*)s14mem_sim_databases))
    return TMWDEFS_FALSE;
#endif
 
#endif
  return TMWDEFS_TRUE;
}

/* function: s14mem_alloc */
void * TMWDEFS_GLOBAL s14mem_alloc(
  S14MEM_ALLOC_TYPE type)
{
  if(type >= S14MEM_ALLOC_TYPE_MAX)
  {
    return(TMWDEFS_NULL);
  }

  return(tmwmem_lowAlloc(&_s14allocTable[type]));
}

/* function: s14mem_free */
void TMWDEFS_CALLBACK s14mem_free(
  void *pBuf)
{    
  TMWMEM_HEADER *pHeader = TMWMEM_GETHEADER(pBuf);
  TMWTYPES_UCHAR   type = TMWMEM_GETTYPE(pHeader);

  if(type >= S14MEM_ALLOC_TYPE_MAX)
  {
    return;
  }

  tmwmem_lowFree(&_s14allocTable[type], pHeader);
}

/* function: s14mem_getUsage */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14mem_getUsage(
  TMWTYPES_UCHAR index,
  const TMWTYPES_CHAR **name,
  TMWMEM_POOL_STRUCT *pStruct)
{
  if(index >= S14MEM_ALLOC_TYPE_MAX)
  {
    return(TMWDEFS_FALSE);
  }

  *name = _nameTable[index];
  *pStruct = _s14allocTable[index];
  return(TMWDEFS_TRUE);
}

 
