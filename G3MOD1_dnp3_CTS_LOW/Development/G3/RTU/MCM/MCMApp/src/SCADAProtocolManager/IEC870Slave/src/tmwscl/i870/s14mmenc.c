/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14mmenc.c
 * description: IEC 60870-5-101 slave short floating point measurand functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14mmenc.h"
#include "tmwscl/i870/s14mem.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14evnt.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/i870util.h"
#include "tmwscl/utils/tmwtarg.h"

#if S14DATA_SUPPORT_MME_C

/* Forward declaration */
static void TMWDEFS_LOCAL _initEventDesc(
  TMWSCTR *pSector,
  S14EVNT_DESC *pDesc);

/* function: _internalAddEvent
 * purpose: Internal function called by SCL to add an event to the queue.
 *  This will not tell the link layer to ask the application for a message
 *  to send. 
 * arguments:
 *  pSector - pointer to sector structure returned by s14sctr_openSector
 *  cot - cause of transmission
 *  ioa - Information object address
 *  value -
 *  quality -
 *  pTimeStamp - pointer to time structures
 * returns:
 *  void *
 */
static void * TMWDEFS_LOCAL _internalAddEvent(
  TMWSCTR *pSector, 
  TMWTYPES_UCHAR cot,
  TMWTYPES_ULONG ioa, 
  TMWTYPES_SFLOAT value, 
  TMWTYPES_UCHAR quality, 
  TMWDTIME *pTimeStamp)
{
  S14EVNT_DESC desc;
  S14MMENC_EVENT *pEvent;
  _initEventDesc(pSector, &desc);

#if S14DATA_SUPPORT_DOUBLE_TRANS
  {
  S14MMENC_EVENT *pDoubleTransEvent;
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  if(p14Sector->mmencTransmissionMode == S14DATA_TRANSMISSION_DOUBLE)
  {
    desc.doubleTransmission = TMWDEFS_TRUE;
  }
  else if(p14Sector->mmencTransmissionMode == S14DATA_TRANSMISSION_PERPOINT)
  { 
    void *pPoint = s14data_mmencLookupPoint(p14Sector->i870.pDbHandle, ioa);
    if((pPoint != TMWDEFS_NULL)
      && s14data_mmencGetTransmissionMode(pPoint))
    {
      desc.doubleTransmission = TMWDEFS_TRUE;
    }
  } 
  pEvent = (S14MMENC_EVENT *)s14evnt_addEvent(pSector, cot, ioa, quality, pTimeStamp, &desc, (void **)&pDoubleTransEvent);

  if(pEvent != TMWDEFS_NULL)
  {
    pEvent->value = value;
    /* Set value of lower priority double transmission entry */
    if(pDoubleTransEvent != TMWDEFS_NULL)
    {
      pDoubleTransEvent->value = value;
    } 
  }
  }
#else

  pEvent = (S14MMENC_EVENT *)s14evnt_addEvent(pSector, cot, ioa, quality, pTimeStamp, &desc, TMWDEFS_NULL);

  if(pEvent != TMWDEFS_NULL)
  {
    pEvent->value = value;
  }
#endif
  return(pEvent);
}
/* function: _changedFunc 
 * purpose: Determine if the specified point has changed and if so
 *  get the current values and add an event to the queue
 * arguments:
 *  pSector - identifies sector
 * returns:
 *  TMWDEFS_TRUE if an event was added
 *  TMWDEFS_FALSE otherwise
 */
static TMWTYPES_BOOL TMWDEFS_CALLBACK _changedFunc(
  TMWSCTR *pSector,
  void *pPoint, 
  TMWDTIME *pTimeStamp)
{  
  TMWDEFS_CHANGE_REASON reason;
  TMWTYPES_SFLOAT fva;
    TMWTYPES_UCHAR qds;

  if(s14data_mmencChanged(pPoint, &fva, &qds, &reason))
  { 
    if(_internalAddEvent(pSector, s14evnt_reasonToCOT(reason), 
      s14data_mmencGetInfoObjAddr(pPoint), fva, qds, 
      pTimeStamp) != TMWDEFS_NULL)
    {
      return(TMWDEFS_TRUE);
    }
  }
  return(TMWDEFS_FALSE);
}

/* function: _eventData 
 * purpose: Insert data from event structure into message to be sent
 * arguments:
 *  pTxData - pointer to transmit data structure
 *  pEvent - pointer to event to be put into message
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _eventData(
  TMWSESN_TX_DATA *pTxData, 
  S14EVNT *pEvent)
{
  S14MMENC_EVENT *pMMENCEvent = (S14MMENC_EVENT *)pEvent;

  /* Store FVA */
  tmwtarg_store32((TMWTYPES_ULONG *)&pMMENCEvent->value, &pTxData->pMsgBuf[pTxData->msgLength]);
  pTxData->msgLength += 4;

  /* Store quality */
  pTxData->pMsgBuf[pTxData->msgLength++] = pEvent->quality;
}

/* function: _initEventDesc 
 * purpose: Initialize event descriptor
 * arguments:
 *  pSector - identifies sector
 *  pDesc - pointer to descriptor structure 
 * returns:
 *  void
 */
static void TMWDEFS_LOCAL _initEventDesc(
  TMWSCTR *pSector,
  S14EVNT_DESC *pDesc)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  pDesc->typeId = I14DEF_TYPE_MMENC1;
  pDesc->woTimeTypeId = I14DEF_TYPE_MMENC1;
  pDesc->cotSpecified = 0;
  pDesc->eventMemType = S14MEM_MMENC_EVENT_TYPE;
  pDesc->pEventList = &p14Sector->mmencEvents;
  pDesc->eventMode = p14Sector->mmencEventMode;
  pDesc->scanEnabled = p14Sector->mmencScanEnabled;
  pDesc->maxEvents = p14Sector->mmencMaxEvents;
  pDesc->timeFormat = p14Sector->mmencTimeFormat;
  pDesc->doubleTransmission = TMWDEFS_FALSE;
  pDesc->pEventsOverflowedFlag = &p14Sector->mmencEventsOverflowed;
  pDesc->pChangedFunc = _changedFunc;
  pDesc->pGetPointFunc = s14data_mmencGetPoint;
  pDesc->pEventDataFunc = _eventData;
}

/* function: s14mmenc_init */
void TMWDEFS_GLOBAL s14mmenc_init(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  tmwdlist_initialize(&p14Sector->mmencEvents);
  p14Sector->mmencEventsOverflowed = TMWDEFS_FALSE;
}

/* function: s14mmenc_close */
void TMWDEFS_GLOBAL s14mmenc_close(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  tmwdlist_destroy(&p14Sector->mmencEvents, s14mem_free);
} 

/* function: s14mmenc_addEvent */
void * TMWDEFS_GLOBAL s14mmenc_addEvent(
  TMWSCTR *pSector, 
  TMWTYPES_UCHAR cot,
  TMWTYPES_ULONG ioa, 
  TMWTYPES_SFLOAT value, 
  TMWTYPES_UCHAR quality, 
  TMWDTIME *pTimeStamp)
{
  void *pEvent;
#if TMWCNFG_SUPPORT_THREADS
  TMWDEFS_RESOURCE_LOCK *pLock = &pSector->pSession->pChannel->lock;
#endif

  TMWTARG_LOCK_SECTION(pLock);

  pEvent = _internalAddEvent(pSector, cot, ioa, value, quality, pTimeStamp);

  if(pEvent != TMWDEFS_NULL)
  {
    /* If an event was added tell link layer we have data */ 
    s14event_linkDataReady(pSector);
  }

  TMWTARG_UNLOCK_SECTION(pLock);
  return(pEvent);
}

/* function: s14mmenc_countEvents */
TMWTYPES_USHORT TMWDEFS_GLOBAL s14mmenc_countEvents(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  return(s14evnt_countEvents(pSector, &p14Sector->mmencEvents));
}

/* function: s14mmenc_scanForChanges */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14mmenc_scanForChanges(
  TMWSCTR *pSector)
{
   S14EVNT_DESC desc;

  _initEventDesc(pSector, &desc);

  return(s14evnt_scanForChanges(pSector, &desc));
}

/* function: s14mmenc_processEvents */
S14EVNT_STATUS TMWDEFS_GLOBAL s14mmenc_processEvents(
  TMWSCTR *pSector, 
  TMWDTIME *pEventTime)
{
  S14EVNT_DESC desc;
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  TMWTYPES_UCHAR typeId = I14DEF_TYPE_MMENC1;
  if(p14Sector->mmencTimeFormat == TMWDEFS_TIME_FORMAT_24) 
  {
    typeId = I14DEF_TYPE_MMETC1;
  }
  else if(p14Sector->mmencTimeFormat == TMWDEFS_TIME_FORMAT_56) 
  {
    typeId = I14DEF_TYPE_MMETF1;
  }

  _initEventDesc(pSector, &desc);
  desc.typeId = typeId;
  return(s14evnt_processEvents(pSector, &desc, 5, pEventTime, TMWDEFS_FALSE));
}

#if S14DATA_SUPPORT_DOUBLE_TRANS
/* function: s14mmenc_processDblTransEvents */
S14EVNT_STATUS TMWDEFS_GLOBAL s14mmenc_processDblTransEvents(
  TMWSCTR *pSector)
{ 
  S14EVNT_DESC desc;
  _initEventDesc(pSector, &desc);
  return(s14evnt_processEvents(pSector, &desc, 5, TMWDEFS_NULL, TMWDEFS_TRUE));
}
#endif
  
/* function: _storeInResponse */
static void TMWDEFS_CALLBACK _storeInResponse(
  TMWSESN_TX_DATA *pTxData, 
  void *pPoint,
  TMWDEFS_TIME_FORMAT timeFormat)
{
  TMWTYPES_SFLOAT value;

  if(timeFormat == TMWDEFS_TIME_FORMAT_NONE)
  {
    value = s14data_mmencGetValue(pPoint);
    tmwtarg_store32((TMWTYPES_ULONG *)&value, &pTxData->pMsgBuf[pTxData->msgLength]);
    pTxData->msgLength += 4;

    pTxData->pMsgBuf[pTxData->msgLength++] = s14data_mmencGetFlags(pPoint);
  }
  else
  {
    TMWTYPES_UCHAR flags;
    TMWDTIME timeStamp;

    value = s14data_mmencGetValueFlagsTime(pPoint, &flags, &timeStamp);
    tmwtarg_store32((TMWTYPES_ULONG *)&value, &pTxData->pMsgBuf[pTxData->msgLength]);
    pTxData->msgLength += 4;

    pTxData->pMsgBuf[pTxData->msgLength++] = flags;

    if(timeFormat == TMWDEFS_TIME_FORMAT_24)
    {
      i870util_write24BitTime(pTxData, &timeStamp, S14DATA_SUPPORT_GENUINE_TIME);
    }
    else if(timeFormat == TMWDEFS_TIME_FORMAT_56)
    {
      i870util_write56BitTime(pTxData, &timeStamp, S14DATA_SUPPORT_GENUINE_TIME);
    }
  }
}

/* function: s14mmenc_readIntoResponse */
S14DBAS_GROUP_STATUS TMWDEFS_CALLBACK s14mmenc_readIntoResponse(
  TMWSESN_TX_DATA *pTxData, 
  TMWSCTR *pSector, 
  TMWTYPES_UCHAR cot, 
  TMWDEFS_GROUP_MASK groupMask, 
  TMWTYPES_ULONG ioa,
  TMWTYPES_USHORT *pPointIndex)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  TMWDEFS_TIME_FORMAT timeFormat = TMWDEFS_TIME_FORMAT_NONE;
  
  /* if this was a read CRDNA, then return the correct time variation 
   * Spec says for background, cyclic or CICNA always send without time variation
   * Some customers require Time Variation in response to CICNA, so allow that.
   */
  if(groupMask == TMWDEFS_GROUP_MASK_ANY)
  {
    /* CRDNA */
    timeFormat = p14Sector->readMsrndTimeFormat;
  }
  else if(groupMask < TMWDEFS_GROUP_MASK_CYCLIC)
  {
    /* CICNA */
    timeFormat = p14Sector->cicnaTimeFormat;
  } 

  return(s14util_readIntoResponse(pTxData, pSector, cot, 
    groupMask, ioa, I14DEF_TYPE_MMENC1, timeFormat, 5, pPointIndex, s14data_mmencGetPoint, 
    s14data_mmencGetGroupMask, s14data_mmencGetInfoObjAddr, s14data_mmencGetIndexed, s14data_getTimeFormat,
    _storeInResponse));
}

#endif /* S14DATA_SUPPORT_MME_C */
