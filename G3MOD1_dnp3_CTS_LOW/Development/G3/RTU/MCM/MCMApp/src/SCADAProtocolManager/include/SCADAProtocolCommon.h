/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:SCADAProtocolCommon.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14 Oct 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef SCADAPROTOCOLCOMMON_H_
#define SCADAPROTOCOLCOMMON_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
extern "C"
{
#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/utils/tmwdtime.h"
}
#include "timeOperations.h"
#include "TimeManager.h"

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define checkNotNull(pointer,message)\
        if (pointer == TMWDEFS_NULL)\
           {\
               DBG_ERR(message);\
               return SCADAP_ERROR_NULL_POINTER;\
           }\

#define checkNull(pointer,message)\
        if (pointer != TMWDEFS_NULL)\
           {\
               DBG_ERR(message);\
               return SCADAP_ERROR_NULL_POINTER;\
           }\

#define checkSCADAErrNone(err)\
                if(err != SCADAP_ERROR_NONE)\
                    return err;
/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
enum SCADAP_ERROR
{
    SCADAP_ERROR_NONE        = 0,
    SCADAP_ERROR_CONFIG         ,
    SCADAP_ERROR_PARAM          ,
    SCADAP_ERROR_NOT_INITIALIZED,
    SCADAP_ERROR_NULL_POINTER,
    SCADAP_ERROR_NOT_CONFIGURED,
    SCADAP_ERROR_INIT_ALREADY,
    SCADAP_ERROR_THREAD         ,   //error creating/destroying thread
    SCADAP_ERROR_EVENT_PIPE     ,   //error writing to event pipe

    SCADAP_ERROR_LAST
};


/**
 * \brief Convert absolute time to TMW format
 *
 * \param inTimePtr  Input time to convert (will use absolute time)
 * \param outTimePtr Resulting time converted to TMW format
 */
void convertTime(TimeManager::TimeStr *inTimePtr, TMWDTIME *outTimePtr);

/**
 * \brief Convert TMW to absolute time format
 *
 * \param inTime    Time in TMW format
 * \param outTime   Resulting time converted to absolute time
 * \param msecs     Resulting milliseconds specified by the TMW incoming value
 */
void convertTime(TMWDTIME& inTime, struct tm& outTime, lu_uint32_t& msecs);
void convertTime(TMWDTIME& inTime, TimeManager::TimeStr& outTime);


#endif /* SCADAPROTOCOLCOMMON_H_ */

/*
 *********************** End of file ******************************************
 */
