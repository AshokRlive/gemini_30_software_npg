/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:S101Database.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief Protocol Point database
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19 Jul 2016     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "S101Database.h"
#include "S101Debug.h"
#include "S101Sector.h"
#include "S101Session.h"
#include "S101Channel.h"
#include "IMCMApplication.h"    //For reset (reboot) message

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static Logger& log = Logger::getLogger(SUBSYSTEM_ID_SCADA_HANDLER);

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
S101Database::S101Database(S101Sector& sector):
                    m_sector(sector)
{}

S101Database::~S101Database()
{
    /* Delete all points*/
     for (S101MSPVect::iterator it = m_MSPVect.begin();
                     it != m_MSPVect.end(); ++it)
     {
         delete (*it);
     }
     m_MSPVect.clear();
     /* Delete all points*/
     for (S101MDPVect::iterator it = m_MDPVect.begin();
                     it != m_MDPVect.end(); ++it)
     {
         delete (*it);
     }
     m_MDPVect.clear();

}

void S101Database::addInputPoint(S101InputPoint::ProtPointType type,S101InputPoint* pInputPoint)
{
    switch(type)
    {
        case (S101InputPoint::MSP):
            m_MSPVect.push_back(pInputPoint);
            pInputPoint->setDatabase(this);
            break;
        case (S101InputPoint::MDP):
            m_MDPVect.push_back(pInputPoint);
            pInputPoint->setDatabase(this);
            break;
        case (S101InputPoint::MMENA):
            m_MMENAVect.push_back(pInputPoint);
            pInputPoint->setDatabase(this);
            break;
        case (S101InputPoint::MMENB):
            m_MMENBVect.push_back(pInputPoint);
            pInputPoint->setDatabase(this);
            break;
        case (S101InputPoint::MMENC):
            m_MMENCVect.push_back(pInputPoint);
            pInputPoint->setDatabase(this);
            break;
        case (S101InputPoint::MIT):
            S101MIT* mit = (S101MIT*)pInputPoint;
            I870DataMITStr* mitData = (I870DataMITStr*)(mit->getTMWPointPtr());
            mitData->pG3DB = (void *)&(m_sector.getSession().getChannel().getG3Database());
            mitData->mitFreeze = S101Database::mitFreeze;
            m_MITVect.push_back(pInputPoint);
            pInputPoint->setDatabase(this);
            break;
    }
}

/* function for adding output points*/
void S101Database::addOutputPoint(S101OutputPoint::ProtPointType type,S101OutputPoint* pOutputPoint)
{
   switch(type)
   {
       case (S101OutputPoint::SCO):
               I870OutputStr scoOutputStr;
               scoOutputStr.pG3DB =   (void *)&(m_sector.getSession().getChannel().getG3Database());
               scoOutputStr.oPointConf = pOutputPoint->getPointConf();
               scoOutputStr.cscExecute       = S101OutputPoint::executeSCO;
//               m_sector.getSectorHandle(scoOutputStr.ppSectorHdle);
               m_SCOMap.insert(std::pair<TMWTYPES_ULONG,I870OutputStr>(pOutputPoint->getPointConf().ioa,scoOutputStr));
               pOutputPoint->setDatabase(this);
       break;
       case (S101OutputPoint::DCO):
               I870OutputStr dcoOutputStr;
               dcoOutputStr.pG3DB =   (void *)&(m_sector.getSession().getChannel().getG3Database());
               dcoOutputStr.oPointConf = pOutputPoint->getPointConf();
               dcoOutputStr.cdcExecute       = S101OutputPoint::executeDCO;//change later!!!!!
//               m_sector.getSectorHandle(scoOutputStr.ppSectorHdle);
               m_DCOMap.insert(std::pair<TMWTYPES_ULONG,I870OutputStr>(pOutputPoint->getPointConf().ioa,dcoOutputStr));
               pOutputPoint->setDatabase(this);
       break;
   }
}

S101InputPoint* S101Database::getInputPoint(S101InputPoint::ProtPointType type,TMWTYPES_USHORT index)
{
    switch(type)
    {
        case  (S101InputPoint::MSP):
            if(index < m_MSPVect.size())
                return m_MSPVect[index];
            else
                return NULL;
        break;
        case  (S101InputPoint::MDP):
            if(index < m_MDPVect.size())
                return m_MDPVect[index];
            else
                return NULL;
        break;
        case  (S101InputPoint::MMENA):
               if(index < m_MMENAVect.size())
                   return m_MMENAVect[index];
               else
                   return NULL;
        break;
        case  (S101InputPoint::MMENB):
               if(index < m_MMENBVect.size())
                   return m_MMENBVect[index];
               else
                   return NULL;
        break;
        case  (S101InputPoint::MMENC):
           if(index < m_MMENCVect.size())
               return m_MMENCVect[index];
           else
               return NULL;
        break;
        case  (S101InputPoint::MIT):
           if(index < m_MITVect.size())
               return m_MITVect[index];
           else
               return NULL;
        break;
        default :
            return NULL;
    }
}


I870OutputStr* S101Database::getOutputPointStr(S101OutputPoint::ProtPointType type,TMWTYPES_ULONG ioa)
{
    switch(type)
    {
        case  (S101OutputPoint::SCO):
            {
            std::map<TMWTYPES_ULONG,I870OutputStr>::iterator it = m_SCOMap.find(ioa);
            if(it != m_SCOMap.end())
            {
                return &(it->second);
            }
                else
                return NULL;
            }
        break;
        case  (S101OutputPoint::DCO):
            {
            std::map<TMWTYPES_ULONG,I870OutputStr>::iterator it = m_DCOMap.find(ioa);
            if(it != m_DCOMap.end())
            {
                return &(it->second);
            }
                else
                return NULL;
            }
        break;
        default :
            return NULL;
    }
}

SCADAP_ERROR S101Database::getSectorHandle(TMWSCTR** ppSectorHdle)
{
    return m_sector.getSectorHandle(ppSectorHdle);
}


/********************************************/
/* TMW Callback Function Implementation     */
/********************************************/
void* S101Database::mspGetPoint(void *pS101DB, TMWTYPES_USHORT index)
{
    S101InputPoint* p;
    S101Database* db =  (S101Database*)pS101DB;
    p = db->getInputPoint(S101InputPoint::MSP,index);
    DBG_TRACK("%s %s with index : %d",TITLE,__func__,index);
    return p == NULL? NULL: p->getTMWPointPtr();
}



void* S101Database::mdpGetPoint(void *pS101DB, TMWTYPES_USHORT index)
{
    S101InputPoint* p;
    S101Database* db =  (S101Database*)pS101DB;
    p = db->getInputPoint(S101InputPoint::MDP,index);
    DBG_TRACK("%s %s with index : %d",TITLE,__func__,index);
    return p == NULL? NULL: p->getTMWPointPtr();
}


void* S101Database::mmenaGetPoint(void *pS101DB, TMWTYPES_USHORT index)
{
    S101InputPoint* p;
    S101Database* db =  (S101Database*)pS101DB;
    p = db->getInputPoint(S101InputPoint::MMENA,index);
    DBG_TRACK("%s %s with index : %d",TITLE,__func__,index);
    return p == NULL? NULL: p->getTMWPointPtr();
}

void* S101Database::mmenbGetPoint(void *pS101DB, TMWTYPES_USHORT index)
{
    S101InputPoint* p;
    S101Database* db =  (S101Database*)pS101DB;
    p = db->getInputPoint(S101InputPoint::MMENB,index);
    DBG_TRACK("%s %s with index : %d",TITLE,__func__,index);
    return p == NULL? NULL: p->getTMWPointPtr();
}


void* S101Database::mmencGetPoint(void *pS101DB, TMWTYPES_USHORT index)
{
    S101InputPoint* p;
    S101Database* db =  (S101Database*)pS101DB;
    p = db->getInputPoint(S101InputPoint::MMENC,index);
    DBG_TRACK("%s %s with index : %d",TITLE,__func__,index);
    return p == NULL? NULL: p->getTMWPointPtr();
}

void* S101Database::mitGetPoint(void *pS101DB, TMWTYPES_USHORT index)
{
    S101InputPoint* p;
    S101Database* db =  (S101Database*)pS101DB;
    p = db->getInputPoint(S101InputPoint::MIT,index);
    DBG_TRACK("%s %s with index : %d",TITLE,__func__,index);
    return p == NULL? NULL: p->getTMWPointPtr();
}


void* S101Database::cscLookupPoint(void *pS101DB, TMWTYPES_ULONG ioa)
{
    S101Database* db =  (S101Database*)pS101DB;
    DBG_TRACK("%s %s with ioa : %lu",TITLE,__func__,ioa);
    return (void*)(db->getOutputPointStr(S101OutputPoint::SCO, ioa));
}


void* S101Database::cdcLookupPoint(void *pS101DB, TMWTYPES_ULONG ioa)
{
    S101Database* db =  (S101Database*)pS101DB;
    DBG_TRACK("%s %s with ioa : %lu",TITLE,__func__,ioa);
    return (void*)(db->getOutputPointStr(S101OutputPoint::DCO, ioa));
}

TMWTYPES_BOOL S101Database::ccsnaSetTime(TMWDTIME *masterTime)
{
    DBG_TRACK("%s %s",TITLE,__func__);
    if(masterTime == NULL)
    {
        return TMWDEFS_FALSE;
    }
    struct tm outTime;
    lu_bool_t synchronised = (masterTime->invalid == TMWDEFS_FALSE)? LU_TRUE : LU_FALSE;
    lu_uint32_t mseconds;
    // Convert TMW Time to tm
    convertTime(*masterTime, outTime, mseconds);

    // Set the time in the Time Manager
    TimeManager::TERROR res;
    res = TimeManager::getInstance().setTime(outTime, mseconds, synchronised, SYNC_SOURCE_SCADA);
    if(res == TimeManager::TERROR_TIMER)
    {
        perror("System Timer not available for synchronisation");
        return TMWDEFS_FALSE;
    }
    return TMWDEFS_TRUE;
}


void S101Database::ccsnaGetTime(TMWDTIME *rtuTime)
{
    DBG_TRACK("%s %s",TITLE,__func__);

    TimeManager::TimeStr outTime;

     // Get the time from the Time Manager
     TimeManager::getInstance().getTime(outTime);

     // Convert timespec to TMW Time
     convertTime(&outTime, rtuTime);
}


TMWTYPES_BOOL S101Database::mitFreeze(void *pPoint, TMWTYPES_UCHAR qcc)
{
    DBG_TRACK("%s %s",TITLE,__func__);

    GDB_ERROR res = GDB_ERROR_NOT_SUPPORTED;
    I870DataMITStr* pointData = (I870DataMITStr*)pPoint;
    GeminiDatabase* g3db = reinterpret_cast<GeminiDatabase*>(pointData->pG3DB);
    PointIdStr vPointID(pointData->vPoint.group, pointData->vPoint.ID);
    if ((qcc & I14DEF_QCC_FRZ_MASK) == I14DEF_QCC_FRZ_RESET_ONLY)
    {
        res = g3db->clear(vPointID);
    }
    else if ( ((qcc & I14DEF_QCC_FRZ_MASK) == I14DEF_QCC_FRZ_RESET) ||
              ((qcc & I14DEF_QCC_FRZ_MASK) == I14DEF_QCC_FRZ_NO_RESET)
             )
    {
        lu_bool_t andClear = ((qcc & I14DEF_QCC_FRZ_MASK) == I14DEF_QCC_FRZ_RESET)? LU_TRUE : LU_FALSE;
        res = g3db->freeze(vPointID, andClear);
    }
    return (res == GDB_ERROR_NONE)? TMWDEFS_TRUE : TMWDEFS_FALSE;
}


lu_bool_t S101Database::deleteEventID(lu_uint32_t eventID, void *pDB)
{
    LU_UNUSED(eventID);

    DBG_TRACK("%s %s",TITLE,__func__);
    S101Database *pDatabase = NULL;
    S101Sector   *pSector;

    if (pDB == NULL)
    {
        return false;
    }

    pDatabase = (S101Database *) pDB;
    pSector = &(pDatabase->getSector());

    // TODO - SKA - Tell the EventManager (?) to remove the event for this session
    //    id = pSector->getSessionID();
    //    DBG_INFO("Delete Event ID [%d] for session [%d]", eventID,  pSector->getSessionID());
    //    eventMgr.remove(id);

    return true;
}

TMWTYPES_BOOL S101Database::hasSectorReset(void *pS101DB, TMWTYPES_UCHAR *pResetCOI)
{
    static bool sectorResetReported = false;
    S101Database* db =  (S101Database*)pS101DB;
    if( (!sectorResetReported) && db->getSector().getSession().getChannel().getG3Database().getEOIFlag() == LU_TRUE)
    {
        *pResetCOI = *pResetCOI & I14DEF_COI_POWER_ON;
        sectorResetReported = true;
        return TMWDEFS_TRUE;
    }
    return TMWDEFS_FALSE;
}


TMWTYPES_BOOL S101Database::crpnaExecuteRTUReset(TMWTYPES_UCHAR qrp)
{
    DBG_TRACK("%s %s",TITLE,__func__);

    /* Note that QRP type 2 (I14DEF_QRP_RESET_EVENTS) is already handled by the stack */
    if(qrp == I14DEF_QRP_RESET_GENERAL)
    {
        log.warn("IEC-101 Master requested RTU to warm restart");
        //Ask the Application to restart
        MCMBoard::getInstance().getMCMApp()->restart(IMCMApplication::RESTART_TYPE_DELAY);
        return TMWDEFS_TRUE;
    }
    log.warn("IEC-101 Master requested reset with QRP=%d", qrp);
    return TMWDEFS_FALSE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
