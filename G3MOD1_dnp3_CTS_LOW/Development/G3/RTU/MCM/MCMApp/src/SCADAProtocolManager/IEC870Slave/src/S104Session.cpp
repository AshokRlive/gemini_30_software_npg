/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:S104Session.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   30 Sep 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "S104Session.h"
#include "S104Channel.h"
#include "S104Debug.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static  Logger& log(Logger::getLogger(SUBSYSTEM_ID_SCADA_HANDLER));

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
S104Session::S104Session(
                    S104Channel& channel):
                    m_channel(channel),
                    mp_tmwsesn(NULL),
                    mp_tmwsesnCnfg(NULL)
{}


S104Session::~S104Session()
{
    /* Delete all sectors*/
    for (S104SectorVector::iterator it = m_sectors.begin();
                    it != m_sectors.end(); ++it)
    {
      delete (*it);
    }
    m_sectors.clear();

    closeSession();

    delete mp_tmwsesnCnfg;
    mp_tmwsesnCnfg  = NULL;
}

SCADAP_ERROR S104Session::openSession(TMWCHNL* tmwchnl)
{
    DBG_INFO("%s opening session",TITLE);

    checkNotNull(tmwchnl,       "Cannot open IEC104 session. tmwchnl is null!");
    checkNotNull(mp_tmwsesnCnfg,"Cannot open IEC104 session. No configuration found!");
    checkNull   (mp_tmwsesn,    "Cannot open IEC104 session. It is already open!");

    /* Open session*/
    mp_tmwsesn = s104sesn_openSession(tmwchnl,mp_tmwsesnCnfg);
    checkNotNull(mp_tmwsesn,"Unable to initialise S104 Session.");

    /* Open sectors*/
    for (S104SectorVector::iterator itSector = m_sectors.begin();
                    itSector != m_sectors.end(); ++itSector)
    {
         (*itSector)->openSector(mp_tmwsesn);
    }

    return SCADAP_ERROR_NONE;
}

void S104Session::closeSession()
{
    /* Close all sectors */
    lu_uint32_t i = 0;
    for (S104SectorVector::iterator it = m_sectors.begin(); it != m_sectors.end(); ++it)
    {
        DBG_INFO("%s closing sector %u...",TITLE, i++);
        (*it)->closeSector();
    }

    if(mp_tmwsesn != NULL)
    {
        s104sesn_closeSession(mp_tmwsesn);
        mp_tmwsesn = NULL;
    }
}

void S104Session::configure(const S104SESN_CONFIG& sessionConfig)
{
    if(mp_tmwsesnCnfg == NULL)
    {
        mp_tmwsesnCnfg = new S104SESN_CONFIG();
	}
	
    memcpy(mp_tmwsesnCnfg, &sessionConfig, sizeof(S104SESN_CONFIG));

    /* Apply configuration to existing session*/
    if(mp_tmwsesn != NULL)
    {
        s104sesn_setSessionConfig(mp_tmwsesn, mp_tmwsesnCnfg);
    }

}

void S104Session::addSector(S104Sector* sectorPtr)
{
    if(sectorPtr != NULL)
    {
        m_sectors.push_back(sectorPtr);
    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
