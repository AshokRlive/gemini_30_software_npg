/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i870lnk4.h
 * description: IEC 60870-5-104 profile Link Layer Implementation
 */
#ifndef I870LNK4_DEFINED
#define I870LNK4_DEFINED

#include "tmwscl/utils/tmwcnfg.h"
#include "tmwscl/utils/tmwlink.h"
#include "tmwscl/utils/tmwchnl.h"

/* Maximum lengths for transmit and receive buffers */
/* According to 60970-5-104 Ed. 2, MaxAPDU is now fixed at 253, 
 * plus 2 bytes for start stop, so these should be left at 255.
 */
#define I870LNK4_MAX_TX_BUFFER_LENGTH 255 /*This does not affect library. 
                                            Frame size is controlled by
                                            session maxASDUSize */
#define I870LNK4_MAX_RX_BUFFER_LENGTH 255

/* Maximum kValue, number of unacknowledged transmitted APDUs */
#ifndef I870LNK4_MAX_K_VALUE
#define I870LNK4_MAX_K_VALUE 12
#endif 

/* Define configuration parameters supported by this link layer 
 * implementation.
 */
typedef struct i870lnk4Config {
  /*  
   * The rx and tx frame size are the maximum APDU length, 
   * including START octet and length octet, as defined in 60870-5 clause 5 
   */
  TMWTYPES_UCHAR  rxFrameSize;          /* Maximum received frame size */
  TMWTYPES_UCHAR  txFrameSize;          /* Maximum transmit frame size 
                                         * This is not used by library, but is
                                         * left here for backward compatibility,
                                         * The max size of a frame is dependent
                                         * on session maxASDUSize. 
                                         */
				 
  TMWTYPES_MILLISECONDS t1AckPeriod;    /* time to wait for ack to a transmitted APDU */  
  TMWTYPES_MILLISECONDS t2SFramePeriod; /* time to wait before sending Supervisory APDU "ack" */
  TMWTYPES_MILLISECONDS t3TestPeriod;   /* Idle time before sending TEST APDU         */ 

  TMWTYPES_USHORT kValue;               /* Max unacknowledged transmitted APDUs */
  TMWTYPES_USHORT wValue;               /* Max unacknowledged received APDUs    */

  TMWTYPES_BOOL   isControlling;        /* If TRUE this side sends STARTDT etc. */

  TMWTYPES_BOOL   isRedundant;          /* If TRUE this is a redundant channel 
                                         * NOTE: this will be set internally and 
                                         * should NEVER be set by the application 
                                         */
   
  /* This parameter specifies how often a session that is offline will
   * attempt to reestablish communication. This includes attempting to
   * open/reopen a channel and/or issueing request status messages as
   * appropriate for the current configuration.
   */
  TMWTYPES_MILLISECONDS offlinePollPeriod;

   /* Setting this to TMWDEFS_TRUE on a slave will cause unacked responses 
   * (Information Frames) to be discarded when the TCP connection is broken.
   * If a slave has sent responses, but has not yet received a link layer ack,
   * and the master is restarted and reconnects, the "old" unacked responses
   * will be sent again. This is required behavior when redundancy is enabled.
   * However, with redundancy not enabled, some masters and some customers get
   * bothered by these "old" responses being received by the master.   
   *
   * DO NOT SET THIS TO TRUE WHEN REDUNDANCY IS ENABLED!
   */
  TMWTYPES_BOOL   discardIFramesOnDisconnect;

} I870LNK4_CONFIG;

/* Define bit masks used to specify which configuration parameters
 * should be modified in a call to i870lnk4_modifyChannel.
 */
#define I870LNK4CONFIG_RX_FRAME_SIZE      0x00000001
#define I870LNK4CONFIG_TX_FRAME_SIZE      0x00000002
#define I870LNK4CONFIG_T1_PERIOD          0x00000004
#define I870LNK4CONFIG_T2_PERIOD	        0x00000008
#define I870LNK4CONFIG_T3_PERIOD          0x00000010
#define I870LNK4CONFIG_K_VALUE	          0x00000020
#define I870LNK4CONFIG_W_VALUE	          0x00000040
#define I870LNK4CONFIG_IS_CONTROLLING     0x00000080


/* Prototype for user callback function */
typedef void (*I870LNK4_TX_CALLBACK_FUNC)(
  void *pCallbackParam,
  TMWSESN_TX_DATA *pTxData);
 
/* Include private information */
#include "tmwscl/i870/i870ln4p.h"

#ifdef __cplusplus
extern "C" {
#endif

  /* function: i870lnk1_initConfig
   * purpose: initialize a lnk 104 configuration structure 
   * arguments:
   *  pConfig - pointer to i870lnk4 configuration information
   *      to be filled in
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL i870lnk4_initConfig(
    I870LNK4_CONFIG *pConfig);

  /* function: i870lnk4_initChannel
   * purpose: initialize a new channel.
   * arguments:
   *  pConfig - pointer to i870lnk4 configuration information
   *  pPhys - pointer to physical layer interface to use
   *  pPhysContext - pointer to physical layer context
   * returns:
   *  A pointer to a context that is used to identify this channel
   *  for all future operations.
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL i870lnk4_initChannel(
    TMWCHNL *pChannel,
    const I870LNK4_CONFIG *pConfig);
 
  /* function: i870lnk4_getChannelConfig 
   * purpose: Get current configuration from a currently open channel
   * arguments:
   *  pChannel - channel to get configuration from
   *  pConfig - configuration data structure to be filled in
   * returns:
   *  TMWDEFS_TRUE if successful
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL i870lnk4_getChannelConfig(
    TMWCHNL *pChannel,
    I870LNK4_CONFIG *pConfig);

  /* function: i870lnk4_setChannelConfig 
    * purpose: Modify a currently open channel
   *  NOTE: normally i870lnk4_getChannelConfig() will be called
   *   to get the current config, some values will be changed 
   *   and this function will be called to set the values.
   * arguments:
   *  pChannel - channel to modify
   *  pConfig - configuration data structure
   * returns:
   *  TMWDEFS_TRUE if successful
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL i870lnk4_setChannelConfig(
    TMWCHNL *pChannel,
    const I870LNK4_CONFIG *pConfig);
 
  /* function: i870lnk4_modifyChannel 
   * DEPRECATED FUNCTION, SHOULD USE i870lnk4_setChannelConfig
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL i870lnk4_modifyChannel(
    TMWCHNL *pChannel,
    const I870LNK4_CONFIG *pConfig,
    TMWTYPES_ULONG configMask);

  /* function: i870lnk4_deleteChannel
   * purpose: delete this channel, freeing all allocated memory and releasing
   *  resources.
   * arguments:
   *  pContext - context returned from i870lnk4_initChannel
   * returns:
   *  TMWDEFS_TRUE if successful
   *  TMWDEFS_FALSE otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL i870lnk4_deleteChannel(
    TMWCHNL *pChannel);

  /* function: i870lnk4_cancelRedundantData 
   * purpose: cancel all of the data the is in the link layer
   *  since this is in a redundancy group, that redundancy group
   *  has a queue of all of this data and will decide what 
   *  channel to send it on.
   * arguments:
   *  pContext - pointer to link context structure
   * returns
   *  void
   */
  void TMWDEFS_GLOBAL i870lnk4_cancelRedundantData(
    I870LNK4_CONTEXT *pLinkContext);

  /* function: i870lnk4_restartRedundantLink
   * purpose: close and reopen link to cause it to 
   *  disconnect and reconnect
   * arguments:
   *  pContext - pointer to link context structure
   * returns
   *  void
   */
  void TMWDEFS_GLOBAL i870lnk4_restartRedundantLink(
    I870LNK4_CONTEXT *pLinkContext);

  /* function: i870lnk4_sendStartDt
   * purpose: Send a STARTDT Unnumbered APDU  
   * arguments:
   *  pLinkContext - pointer to link context structure
   * returns
   *  void
   */
  TMWDEFS_SCL_API void TMWDEFS_GLOBAL i870lnk4_sendStartDt(
    TMWLINK_CONTEXT *pLinkContext);

  /* function: i870lnk4_sendStopDt
   * purpose: Send a STOPDT Unnumbered APDU  
   * arguments:
   *  pLinkContext - pointer to link context structure
   * returns
   *  void
   */
  TMWDEFS_SCL_API void TMWDEFS_GLOBAL i870lnk4_sendStopDt(
    TMWLINK_CONTEXT *pLinkContext);

  /* The following functions are exported for redundancy. */

  /* function: i870lnk4_enableRedundancy
   * purpose:  Tell link layer that redundancy will be supported on this
   *    link. Otherwise the 104 link layer is taking care of STARTDT etc
   *    on behalf of the application.  
   * arguments:
   *  pLinkContext - pointer to link context structure
   * returns
   *  void
   */
  void TMWDEFS_GLOBAL i870lnk4_enableRedundancy(
    TMWLINK_CONTEXT *pContext);

  /* function: _updateMsg
   * purpose: Update the ASDU that was given to link layer for transmitting.
   *          This allows application to update time portion of ASDU just
   *		    before transmition.
   *  pContext - void pointer to link context structure
   *  offset   - offset into ASDU to copy data
   *  pData    - ptr to data to copy into APDU
   *  length   - length of data to copy into APDU
   * returns
   *  void
   */
  void TMWDEFS_GLOBAL i870lnk4_updateMsg(
    TMWLINK_CONTEXT *pContext,
    TMWTYPES_USHORT offset,
    TMWTYPES_UCHAR *pData,
    TMWTYPES_USHORT length);

  /* function: _checkDataReady
   * purpose: Function called by application to indicate it has an ASDU to transmit.
   *          This function will call application function when it can accept more
   *          data. Allowed up to kValue APDUs.
   *  pContext - void pointer to link context structure
   *  pSession - pointer to session structure
   * returns
   *  void
   */
  void TMWDEFS_GLOBAL i870lnk4_checkDataReady(
    void *pContext,
    TMWSESN *pSession);

  /* function: _transmitFrame
   * purpose: transmit frame
   * arguments:
   *  pContext - link layer context returned from i870lnk4_initChannel
   *  pTxDescriptor - data to transmit
   * returns
   *  void
   */
  void TMWDEFS_GLOBAL i870lnk4_transmitFrame(
    TMWLINK_CONTEXT *pContext,
    TMWSESN_TX_DATA *pTxData);

  /* function: _cancelFrame
   * purpose: cancel frame
   * arguments:
   *  pContext - link layer context returned from i870lnk1_initChannel
   *  pTxDescriptor - data frame to cancel
   * returns
   *  void
   */
  void TMWDEFS_GLOBAL i870lnk4_cancelFrame(
    TMWLINK_CONTEXT *pContext,
    TMWSESN_TX_DATA *pTxData);

    /* function: i870lnk4_registerCallback
   * purpose: User callback function to be called when message is given to link layer
   * Most implementations will not need to provide this callback function. This 
   * allows the message to be modified before it is transmitted for test purposes,
   * for example to modify some bytes or introduce errors.
   */
  void TMWDEFS_GLOBAL i870lnk4_registerCallback(
    TMWCHNL *pChannel,
    I870LNK4_TX_CALLBACK_FUNC pUserTxCallback,
    void *pCallbackParam);

#ifdef __cplusplus
}
#endif
#endif /* I870LNK4_DEFINED */
