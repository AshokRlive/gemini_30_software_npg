/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:S104Protocol.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   30 Sep 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef IEC104SLAVEPROTOCOL_H_
#define IEC104SLAVEPROTOCOL_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "Thread.h"
#include "ISlaveProtocol.h"
#include "GeminiDatabase.h"
#include "S104Channel.h"
#include "S104EventPublisher.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
class S104Protocol: public ISlaveProtocol, private Thread
{
public:
    S104Protocol(GeminiDatabase&  g3database);
    virtual ~S104Protocol();

    /** Override*/
    virtual SCADAP_ERROR startProtocol();

    /** Override*/
    virtual SCADAP_ERROR stopProtocol();


    void addChannel(S104Channel* channelPtr);


private:
    /** Override*/
    virtual void threadBody();

    /**
     * Checks connection status.
     */
    void checkConnections();

    void deleteAllChannels();

private:
    typedef std::vector<S104Channel*> ChannelVector;

    GeminiDatabase&     m_g3db;
    TMWAPPL*            mp_twmappl;
    ChannelVector       m_channels;
};

#endif /* IEC104SLAVEPROTOCOL_H_ */

/*
 *********************** End of file ******************************************
 */
