/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:ConnectionTimer.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   3 Oct 2014     andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "ConnectionTimer.h"
#include "timeOperations.h"
#include "Debug.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
ConnectionTimer::ConnectionTimer():
            m_timeoutMs(0),
            m_startTime(),
            m_started(false)
{

}

ConnectionTimer::~ConnectionTimer()
{
}

void ConnectionTimer::start(lu_uint32_t timeoutMs)
{
    clock_gettime(CLOCK_MONOTONIC, &m_startTime);

    m_timeoutMs = timeoutMs;

    m_started = true;
}

void ConnectionTimer::stop()
{
    m_started = false;
}

void ConnectionTimer::reset()
{
    clock_gettime(CLOCK_MONOTONIC, &m_startTime);
    m_started = true;
}

void ConnectionTimer::reset(timespec& startTime)
{
    m_startTime = startTime;
    m_started = true;
}

bool ConnectionTimer::isTimeout()
{
   if(m_started == false)
   {
       return false; //  Timer not started
   }

   struct timespec currentTime;
   clock_gettime(CLOCK_MONOTONIC, &currentTime);

   lu_uint64_t elapsedMs;
   elapsedMs = timespec_elapsed_ms(&m_startTime, &currentTime);

   return elapsedMs > m_timeoutMs;
}

bool ConnectionTimer::isStarted()
{
    return m_started;
}
/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
