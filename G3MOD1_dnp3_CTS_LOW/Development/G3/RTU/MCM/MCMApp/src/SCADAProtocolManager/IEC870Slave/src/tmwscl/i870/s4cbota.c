/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s4cbota.c
 * description: IEC 60870-5-104 slave CBOTA (Bitstring Command With Time)
 *  functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s4cbota.h"
#include "tmwscl/i870/s14cbona.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14diag.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/s104sctr.h"
#include "tmwscl/i870/s4util.h"
#include "tmwscl/utils/tmwtarg.h"

#if (S14DATA_SUPPORT_CBO && S14DATA_SUPPORT_TIMETAG_COMMANDS)
#if S14DATA_SUPPORT_MULTICMDS
  
/* function: s4cseta_processRequest */
void TMWDEFS_CALLBACK s4cbota_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  S14SCTR *p14Sector;
  S14SCTR_CMD *pContext;
  TMWTYPES_ULONG ioa;

  pContext = s14util_processMultiRequest(pSector, pMsg, I14DEF_TYPE_CBOTA1, &ioa, TMWDEFS_NULL);
  if(pContext == TMWDEFS_NULL)
    return;
   
  /* No command to call for status of this point */
  pContext->pStatus = TMWDEFS_NULL;
  
  /* If only one context per typeId is allowed, this might be cancelling and replacing the previous ioa */
  pContext->ioa = ioa;

  /* Parse BSI */
  tmwtarg_get32(&pMsg->pRxData->pMsgBuf[pMsg->offset], &pContext->value.ulval);
  pContext->valueType = TMWTYPES_ANALOG_TYPE_ULONG;
  pContext->useValue = TMWDEFS_TRUE;
  pMsg->offset += 4;

  /* Check command age */
  pContext->useDateTime = TMWDEFS_TRUE;
  i870util_read56BitTime(pMsg, &pContext->requestTime);

  /* Parse received cause of transmission */
  if(pMsg->cot == I14DEF_COT_ACTIVATION)
  {
    pContext->cot = I14DEF_COT_ACTCON;
  }
  else if(pMsg->cot == I14DEF_COT_DEACTIVATION)
  {
    pContext->cot = I14DEF_COT_DEACTCON;
  }
  else
  {
    pContext->cot = I14DEF_COT_UNKNOWN_COT | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* Lookup data point using information object address */
  p14Sector = (S14SCTR *)pSector;
  pContext->pPoint = s14data_cboLookupPoint(p14Sector->i870.pDbHandle, ioa);
  if(pContext->pPoint == TMWDEFS_NULL)
  {
    pContext->cot = I14DEF_COT_UNKNOWN_IOA | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* if deactivate, context->cot was set to DEACTCON above. */
  if(pMsg->cot == I14DEF_COT_DEACTIVATION)
  {
    return;
  }
  
  if(s4util_checkCommandAge((S104SCTR *)pSector, &pContext->requestTime) == TMWDEFS_FALSE)
  {
    /* 104 Edition 2 says there should be no response sent */
    s14util_freeCmdCtxt(p14Sector, pContext);
    S14DIAG_ERROR(p14Sector->i870.tmw.pSession, pSector, S14DIAG_CMD_DELAYED);
    return;
  }
  
  pContext->state = TMWDEFS_CMD_STATE_EXECUTING;
  pContext->status = s14data_cboExecute(pContext->pPoint, pContext->value.ulval);
  if(pContext->status != TMWDEFS_CMD_STAT_SUCCESS)
  {
    pContext->status = TMWDEFS_CMD_STAT_FAILED;
  }
}

#else /* !S14DATA_SUPPORT_MULTICMDS */
    
/* function: _checkStatus */
static TMWDEFS_COMMAND_STATUS TMWDEFS_CALLBACK _checkStatus(
  TMWSCTR *pSector)
{
  return (s14cbona_checkStatus(pSector));
}

/* function: s4cbota_processRequest */
void TMWDEFS_CALLBACK s4cbota_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  S104SCTR *p104Sector = (S104SCTR *)pSector;
  
  /* If there is a previous command in progress, queue a negative ACT CON
   * to be sent. Don't overwrite the information from the previous command.
   */
  if((pMsg->cot == I14DEF_COT_ACTIVATION)
    && ((p14Sector->cbotaCOT != 0) || (p14Sector->cbonaCOT != 0)))
  {
    s14sesn_sendNegActCon(pSector, pMsg, I14DEF_COT_ACTCON); 
    return;
  }

  /* Store originator address */
  p14Sector->cbonaOriginator = pMsg->origAddress;
  
  /* Parse Information Object Address */
  i870util_readInfoObjAddr(pSector->pSession, pMsg, &p14Sector->cbonaIOA);

  /* Parse BSI */
  tmwtarg_get32(&pMsg->pRxData->pMsgBuf[pMsg->offset], &p14Sector->cbonaBSI);
  pMsg->offset += 4;

  /* Check command age */
  i870util_read56BitTime(pMsg, &p104Sector->cbotaRequestTime);

  /* Parse received cause of transmission */
  if(pMsg->cot == I14DEF_COT_ACTIVATION)
  {
    p14Sector->cbotaCOT = I14DEF_COT_ACTCON;
  }
  else
  {
    p14Sector->cbotaCOT = I14DEF_COT_UNKNOWN_COT | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* Lookup data point using information object address */
  p104Sector->s14.pCBOPoint = s14data_cboLookupPoint(p14Sector->i870.pDbHandle, p14Sector->cbonaIOA);
  if(p104Sector->s14.pCBOPoint == TMWDEFS_NULL)
  {
    p14Sector->cbotaCOT = I14DEF_COT_UNKNOWN_IOA | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  if(s4util_checkCommandAge(p104Sector, &p104Sector->cbotaRequestTime) == TMWDEFS_FALSE)
  {
    /* 104 Edition 2 says there should be no response sent */
    p14Sector->cbotaCOT = 0;
    S14DIAG_ERROR(p104Sector->s14.i870.tmw.pSession, (TMWSCTR *)p104Sector, S14DIAG_CMD_DELAYED);
    return;
  }

  /* Start execute */
  p14Sector->cbonaStatus = s14data_cboExecute(p14Sector->pCBOPoint, p14Sector->cbonaBSI);
}

/* function: s4cbota_buildResponse */
TMWTYPES_BOOL TMWDEFS_CALLBACK s4cbota_buildResponse(
  TMWSCTR *pSector, 
  TMWTYPES_BOOL buildResponse)
{
  S104SCTR *p104Sector = (S104SCTR *)pSector;

  return(s14util_buildCommandResponse(pSector, buildResponse, 
    I14DEF_TYPE_CBOTA1, &p104Sector->s14.cbotaCOT, p104Sector->s14.cbonaOriginator,
    p104Sector->s14.cbonaIOA, 0, TMWDEFS_FALSE, &p104Sector->s14.cbonaBSI, 4, &p104Sector->cbotaRequestTime, 
    _checkStatus));
}

#endif /* !S14DATA_SUPPORT_MULTICMDS */
#endif /* S14DATA_SUPPORT_CBO && S14DATA_SUPPORT_TIMETAG_COMMANDS */
