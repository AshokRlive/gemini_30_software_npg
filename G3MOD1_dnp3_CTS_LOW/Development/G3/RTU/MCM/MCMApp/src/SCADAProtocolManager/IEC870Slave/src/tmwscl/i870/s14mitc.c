/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14mitc.c
 * description: IEC 60870-5-101 slave integrated totals BCD ASDU functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14mitc.h"
#include "tmwscl/i870/s14mem.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14evnt.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/i870util.h"
#include "tmwscl/utils/tmwtarg.h"

#if S14DATA_SUPPORT_MITC

/* Forward declaration */
static void TMWDEFS_LOCAL _initEventDesc(
  TMWSCTR *pSector,
  S14EVNT_DESC *pDesc);

/* function: _internalAddEvent
 * purpose: Internal function called by SCL to add an event to the queue.
 *  This will not tell the link layer to ask the application for a message
 *  to send. 
 * arguments:
 *  pSector - pointer to sector structure returned by s14sctr_openSector
 *  cot - cause of transmission
 *  ioa - Information object address
 *  pBCD - pointer to 6 bytes of value
 *  quality - sequence number and quality bits
 *  pTimeStamp - pointer to time structures
 * returns:
 *  void *
 */
static void * TMWDEFS_LOCAL _internalAddEvent(
  TMWSCTR *pSector, 
  TMWTYPES_UCHAR cot,
  TMWTYPES_ULONG ioa, 
  TMWTYPES_UCHAR *pBCD,
  TMWTYPES_UCHAR sequenceAndQuality, 
  TMWDTIME *pTimeStamp)
{
  S14EVNT_DESC desc;
  S14MITC_EVENT *pEvent;

  _initEventDesc(pSector, &desc);

#if S14DATA_SUPPORT_DOUBLE_TRANS
  {
  S14MITC_EVENT *pDoubleTransEvent;
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  if(p14Sector->mitcTransmissionMode == S14DATA_TRANSMISSION_DOUBLE)
  {
    desc.doubleTransmission = TMWDEFS_TRUE;
  }
  else if(p14Sector->mitcTransmissionMode == S14DATA_TRANSMISSION_PERPOINT)
  { 
    void *pPoint = s14data_mitcLookupPoint(p14Sector->i870.pDbHandle, ioa);
    if((pPoint != TMWDEFS_NULL)
      && s14data_mitcGetTransmissionMode(pPoint))
    {
      desc.doubleTransmission = TMWDEFS_TRUE;
    }
  }
  pEvent = (S14MITC_EVENT *)s14evnt_addEvent(pSector, cot, ioa, sequenceAndQuality, pTimeStamp, &desc, (void **)&pDoubleTransEvent);

  if(pEvent != TMWDEFS_NULL)
  {
    memcpy(pEvent->bcd, pBCD, 6);
    /* Set value of lower priority double transmission entry */
    if(pDoubleTransEvent != TMWDEFS_NULL)
    { 
      memcpy(pDoubleTransEvent->bcd, pBCD, 6);
    } 
  }
  }
#else
  pEvent = (S14MITC_EVENT *)s14evnt_addEvent(pSector, cot, ioa, sequenceAndQuality, pTimeStamp, &desc, TMWDEFS_NULL);

  if(pEvent != TMWDEFS_NULL)
  {
    memcpy(pEvent->bcd, pBCD, 6);
  }
#endif
  return(pEvent);
}

/* function: _eventData 
 * purpose: Insert data from event structure into message to be sent
 * arguments:
 *  pTxData - pointer to transmit data structure
 *  pEvent - pointer to event to be put into message
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _eventData(
  TMWSESN_TX_DATA *pTxData, 
  S14EVNT *pEvent)
{
  int i;
  S14MITC_EVENT *pMITCEvent = (S14MITC_EVENT *)pEvent;

  /* Store value */
  for(i=0; i<6; i++)
    pTxData->pMsgBuf[pTxData->msgLength++] = pMITCEvent->bcd[i];

  /* Store quality */
  pTxData->pMsgBuf[pTxData->msgLength++] = pEvent->quality;
}

/* function: _initEventDesc 
 * purpose: Initialize event descriptor
 * arguments:
 *  pSector - identifies sector
 *  pDesc - pointer to descriptor structure 
 * returns:
 *  void
 */
static void TMWDEFS_LOCAL _initEventDesc(
  TMWSCTR *pSector,
  S14EVNT_DESC *pDesc)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  pDesc->typeId = I14DEF_TYPE_MITTC1;
  pDesc->woTimeTypeId = I14DEF_TYPE_MITNC1;
  pDesc->cotSpecified = 0;
  pDesc->eventMemType = S14MEM_MITC_EVENT_TYPE;
  pDesc->pEventList = &p14Sector->mitcEvents;
  pDesc->eventMode = p14Sector->mitcEventMode;
  pDesc->scanEnabled = TMWDEFS_FALSE;
  pDesc->maxEvents = p14Sector->mitcMaxEvents;
  pDesc->timeFormat = p14Sector->mitcTimeFormat; 
  pDesc->doubleTransmission = TMWDEFS_FALSE;
  pDesc->pEventsOverflowedFlag = &p14Sector->mitcEventsOverflowed;
  pDesc->pChangedFunc = TMWDEFS_NULL;
  pDesc->pGetPointFunc = s14data_mitcGetPoint;
  pDesc->pEventDataFunc = _eventData;
}

/* function: _storeInResponse */
static void TMWDEFS_CALLBACK _storeInResponse(
  TMWSESN_TX_DATA *pTxData, 
  void *pPoint,
  TMWDEFS_TIME_FORMAT timeFormat)
{
  int i;
  TMWTYPES_UCHAR bcd[6];
 
  if(timeFormat == TMWDEFS_TIME_FORMAT_NONE)
  {
    s14data_mitcGetValue(pPoint, bcd);
    for(i=0; i<6; i++)
      pTxData->pMsgBuf[pTxData->msgLength++] = bcd[i];

    pTxData->pMsgBuf[pTxData->msgLength++] = s14data_mitcGetFlags(pPoint);
  }
  else
  {
    TMWDTIME timeStamp;
    TMWTYPES_UCHAR flags;
    s14data_mitcGetValueFlagsTime(pPoint, bcd, &flags, &timeStamp);
    for(i=0; i<6; i++)
      pTxData->pMsgBuf[pTxData->msgLength++] = bcd[i];

    pTxData->pMsgBuf[pTxData->msgLength++] = flags; 

    /* There is no 24bit mitc type */
    if(timeFormat == TMWDEFS_TIME_FORMAT_56)
    {
      i870util_write56BitTime(pTxData, &timeStamp, S14DATA_SUPPORT_GENUINE_TIME);
    }
  }

}

/* function: _mitcGetIndexed */
static TMWTYPES_BOOL TMWDEFS_CALLBACK _mitcGetIndexed(
  void *pPoint)
{
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_TRUE);
}

/* function: s14mitc_init */
void TMWDEFS_GLOBAL s14mitc_init(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  tmwdlist_initialize(&p14Sector->mitcEvents);
  p14Sector->mitcEventsOverflowed = TMWDEFS_FALSE;
}

/* function: s14mitc_close */
void TMWDEFS_GLOBAL s14mitc_close(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  tmwdlist_destroy(&p14Sector->mitcEvents, s14mem_free);
} 

/* function: s14mitc_addEvent */
void * TMWDEFS_GLOBAL s14mitc_addEvent(
  TMWSCTR *pSector, 
  TMWTYPES_UCHAR cot,
  TMWTYPES_ULONG ioa, 
  TMWTYPES_UCHAR *pBCD,
  TMWTYPES_UCHAR sequenceAndQuality, 
  TMWDTIME *pTimeStamp)
{
  void *pEvent;
  TMWTARG_LOCK_SECTION(&pSector->pSession->pChannel->lock);

  pEvent = _internalAddEvent(pSector, cot, ioa, pBCD, sequenceAndQuality, pTimeStamp);

  if(pEvent != TMWDEFS_NULL)
  {
    /* If an event was added tell link layer we have data */ 
    s14event_linkDataReady(pSector);
  }

  TMWTARG_UNLOCK_SECTION(&pSector->pSession->pChannel->lock);
  return(pEvent);
}

/* function: s14mitc_countEvents */
TMWTYPES_USHORT TMWDEFS_GLOBAL s14mitc_countEvents(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  return(s14evnt_countEvents(pSector, &p14Sector->mitcEvents));
}

/* function: s14mitc_processEvents */
S14EVNT_STATUS TMWDEFS_GLOBAL s14mitc_processEvents(
  TMWSCTR *pSector, 
  TMWDTIME *pEventTime)
{
  S14EVNT_DESC desc;
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  TMWTYPES_UCHAR typeId = I14DEF_TYPE_MITNC1;
  if(p14Sector->mitcTimeFormat != TMWDEFS_TIME_FORMAT_NONE) 
  {
    typeId = I14DEF_TYPE_MITTC1;
  }

  _initEventDesc(pSector, &desc);
  desc.typeId = typeId;
  return(s14evnt_processEvents(pSector, &desc, 7, pEventTime, TMWDEFS_FALSE)); 
}

#if S14DATA_SUPPORT_DOUBLE_TRANS
/* function: s14mitc_processDblTransEvents */
S14EVNT_STATUS TMWDEFS_GLOBAL s14mitc_processDblTransEvents(
  TMWSCTR *pSector)
{ 
  S14EVNT_DESC desc;
  _initEventDesc(pSector, &desc);
  return(s14evnt_processEvents(pSector, &desc, 7, TMWDEFS_NULL, TMWDEFS_TRUE));
}
#endif

/* function: s14mitc_readIntoResponse */
/* CRDNA */
S14DBAS_GROUP_STATUS TMWDEFS_CALLBACK s14mitc_readIntoResponse(
  TMWSESN_TX_DATA *pTxData, 
  TMWSCTR *pSector, 
  TMWTYPES_UCHAR cot, 
  TMWDEFS_GROUP_MASK groupMask, 
  TMWTYPES_ULONG ioa,
  TMWTYPES_USHORT *pPointIndex)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  TMWDEFS_TIME_FORMAT timeFormat = p14Sector->readTimeFormat;

  /* Only CRDNA is supported, not CICNA(GI) */
  if(groupMask != TMWDEFS_GROUP_MASK_ANY)
    return(S14DBAS_GROUP_STATUS_NO_DATA);

  /* There is no 24 bit time format for MITC */
  if(timeFormat == TMWDEFS_TIME_FORMAT_24) 
  {
    timeFormat = TMWDEFS_TIME_FORMAT_56;
  }

  return(s14util_readIntoResponse(pTxData, pSector, cot, 
    groupMask, ioa, I14DEF_TYPE_MITNC1, timeFormat, 7, pPointIndex, s14data_mitcGetPoint, 
    s14data_mitcGetGroupMask, s14data_mitcGetInfoObjAddr, _mitcGetIndexed, s14data_getTimeFormat,
    _storeInResponse));
}

/* function: s14mitc_readIntoCCINAResponse */
/* CCINA */
S14DBAS_GROUP_STATUS TMWDEFS_CALLBACK s14mitc_readIntoCCINAResponse(
  TMWSESN_TX_DATA *pTxData, 
  TMWSCTR *pSector, 
  TMWTYPES_UCHAR cot, 
  TMWDEFS_GROUP_MASK groupMask, 
  TMWTYPES_ULONG ioa,
  TMWTYPES_USHORT *pPointIndex)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  TMWDEFS_TIME_FORMAT timeFormat = p14Sector->readTimeFormat;

  /* There is no 24 bit time format for MITC */
  if(timeFormat == TMWDEFS_TIME_FORMAT_24) 
  {
    timeFormat = TMWDEFS_TIME_FORMAT_56;
  }

  return(s14util_readIntoResponse(pTxData, pSector, cot, 
    groupMask, ioa, I14DEF_TYPE_MITNC1, timeFormat, 7, pPointIndex, s14data_mitcGetPoint, 
    s14data_mitcGetGroupMask, s14data_mitcGetInfoObjAddr, _mitcGetIndexed, s14data_getTimeFormat,
    _storeInResponse));
}

#endif /* S14DATA_SUPPORT_MITC */
