/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i870mem1.h
 * description: IEC 60870-5 FT12 Link Layer Memory Implementation
 */
#ifndef I870MEM1_DEFINED
#define I870MEM1_DEFINED

#include "tmwscl/utils/tmwcnfg.h"
#include "tmwscl/utils/tmwmem.h"

/* Memory allocation defines */
typedef enum I870lnk1AllocType {
  I870MEM1_SESSION_INFO_TYPE,
  I870MEM1_CONTEXT_TYPE,
  I870MEM1_ALLOC_TYPE_MAX
} I870MEM1_ALLOC_TYPE;
 
typedef struct {
  /* Specify number of 101 or 103 master or slave sessions that can be open. */
  TMWTYPES_UINT numSessions;

  /* Specify number of 101 or 103 sectors that can be open. */
  TMWTYPES_UINT numContexts;
} I870MEM1_CONFIG;

/* Specify number of 101 or 103 master or slave sessions that can be open */
#define I870MEM1_NUMALLOC_SESNS         TMWCNFG_MAX_SESSIONS

/* Specify number of 101 and 103 channels that can be open */
#define I870MEM1_NUMALLOC_CONTEXTS      TMWCNFG_MAX_CHANNELS

#ifdef __cplusplus
extern "C" {
#endif

  /* routine: i870mem1_initConfig
   * purpose:  INTERNAL FUNCTION to initialize the memory configuration 
   *  structure indicating how many of each structure type to put in
   *  the memory pool. These will be initialized according 
   *  to the compile time defines.
   *  NOTE: user should call m101mem_initConfig()s101mem_initConfig() 
   *   m103mem_initConfig() or s103mem_initConfig()to modify the number 
   *   of buffers allowed for each type.
   * arguments:
   *  pConfig - pointer to memory configuration structure to be filled in
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL i870mem1_initConfig(
    I870MEM1_CONFIG   *pConfig);

  /* routine: i870mem1_init
   * purpose: INTERNAL memory management init function.
   *  NOTE: user should call m101mem_initConfig()s101mem_initConfig() 
   *   m103mem_initConfig() or s103mem_initConfig()to modify the number 
   *   of buffers allowed for each type.
   * arguments:
   *  pConfig - pointer to memory configuration structure to be used
   * returns:
   *  TMWDEFS_TRUE if successful
   *  TMWDEFS_FALSE otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL i870mem1_init(
    I870MEM1_CONFIG *pConfig);

  /* function: i870mem1_alloc
   * purpose:  Allocate memory  
   * arguments: 
   *  type - enum value indicating what structure to allocate
   * returns:
   *   TMWDEFS_NULL if allocation failed
   *   void * pointer to allocated memory if successful
   */
  void * TMWDEFS_GLOBAL i870mem1_alloc(
    I870MEM1_ALLOC_TYPE type);

  /* function: i870mem1_free
   * purpose:  Deallocate memory
   * arguments: 
   *  pBuf - pointer to buffer to be deallocated
   * returns:    
   *   void  
   */
  void TMWDEFS_GLOBAL i870mem1_free(
    void *pBuf);

  /* function: i870mem1_getUsage
   * purpose:  Determine memory usage for each type of memory
   *    managed by this file.
   * arguments: 
   *  index: index of pool, starting with 0 caller can call
   *    this function repeatedly, while incrementing index. When
   *     index is larger than number of pools, this function
   *     will return TMWDEFS_FALSE
   *  pName: pointer to a char pointer to be filled in
   *  pStruct: pointer to structure to be filled in.
   * returns:    
   *  TMWDEFS_TRUE  if successfully returned usage statistics.
   *  TMWDEFS_FALSE if failure because index is too large.
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL i870mem1_getUsage(
    TMWTYPES_UCHAR index,
    const TMWTYPES_CHAR **pName,
    TMWMEM_POOL_STRUCT *pStruct);
 
#ifdef __cplusplus
}
#endif

#endif /* I870MEM1_DEFINED */
