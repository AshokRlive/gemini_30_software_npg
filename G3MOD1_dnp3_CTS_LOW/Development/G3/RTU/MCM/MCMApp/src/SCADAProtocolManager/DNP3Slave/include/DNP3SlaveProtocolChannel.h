/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:DNP3SlaveProtocolChannel.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   26 Sep 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef DNP3SLAVEPROTOCOLCHANNEL_H_
#define DNP3SLAVEPROTOCOLCHANNEL_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "TMWDNP3Includes.h"
#include "DNP3SlaveProtocolSession.h"
#include "AbstractSlaveProtocolChannel.h"
#include "ISCADAConnectionManager.h"
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
class DNP3SlaveProtocol;

/*
 * DNP3SlaveProtocolChannel
 *
 * Defines the Communications Channel for the protocol
 *
 */
class DNP3SlaveProtocolChannel :/*public  ISlaveProtocolChannel,*/
                                public AbstractSlaveProtocolChannel,
                                private ICommsDeviceObserver
{
public:
    class Config
    {
    public:
        std::string      channelName;   // Name assigned in XML

        lu_uint16_t      masterAddress; // Master Address defined when Connection Group is enabled

        DNPCHNL_CONFIG   channelConfig;
        TMWTARG_CONFIG   targConfig;
        DNPTPRT_CONFIG   tprtConfig;    // This will remain empty and unused
        LINIO_CONFIG     IOConfig;
        DNPLINK_CONFIG   linkConfig;
        TMWPHYS_CONFIG   physConfig;
    };

public:
    DNP3SlaveProtocolChannel(GeminiDatabase&  GDatabase,
                             DNP3SlaveProtocol *protocolManager,
                             Config config);

    virtual ~DNP3SlaveProtocolChannel();

    SCADAP_ERROR init();

    SCADAP_ERROR closeChannel();

    /**
     * \brief Open Channel in SCL
     *
     * \param applContextPtr   TMW SCL Application Pointer
     *
     * \return Error code
     */
    SCADAP_ERROR openChannel(TMWAPPL *applContextPtr);

    /**
     * \brief Count all the events in this channel's sessions
     *
     * \param classMask Mask of the event classes
     * \param countAll  Count all events or return if at least one exists
     *
     * \return Event Count
     */
    lu_uint32_t countSessionEvents(lu_uint8_t classMask, bool countAll);

    /**
     * \brief Retrieve this channel's outgoing ipAddress
     *
     * \return IP Address or NULL
     */
    char *getIpAddress();

    /**
     * \brief Retrieve this channel's listening port
     *
     * \return port num or -1
     */
    lu_int32_t getListeningPort();

    /**
     * \brief Callback for TMW SCL to publish received data
     *
     * \param channelPtr Pointer to the channel instance
     * \param dataPtr    Pointer to data buffer
     * \param len        Length of data buffer
     *
     * \return N/A
     */
    static void         publishRxData(void *channelPtr, lu_int8_t *dataPtr, lu_uint32_t len);

    void                setConnectedStatus(bool connected);

    void                setModem(DialupModem *dialupModemPtr) { m_modemPtr = dialupModemPtr; };
    DialupModem*        getModem() { return m_modemPtr; };

    virtual bool        connectChannel();

    virtual void        tickEvent();

    virtual lu_uint32_t getConnectionTimeout();

private:

    virtual void        notifyConnectionState(bool connected, lu_int32_t commDevFD);

    void                applyConnectionManagerConfig();

    bool                checkMasterIPAddress(std::string ipAddress);

public:
    GeminiDatabase&    m_GDatabase;
    DNP3SlaveProtocol* m_PManager;

    TMWCHNL*           m_SCLChannelPtr;

private:
    DialupModem*       m_modemPtr;

    Config m_config;

    Logger&            m_log;
};

#endif /* DNP3SLAVEPROTOCOLCHANNEL_H_ */

/*
 *********************** End of file ******************************************
 */
