/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:S104Sector.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief Protocol Sector - TMW artifact for handling part of a session
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   6 Oct 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "S104Sector.h"
#include "S104Protocol.h"
#include "S104Debug.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
//static  Logger& log(Logger::getLogger(SUBSYSTEM_ID_SCADA_HANDLER));

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
S104Sector::S104Sector(S104Session& session):
                       m_session(session),
                       mp_tmwsector(NULL),
                       mp_tmwsectorCnfg(NULL),
                       m_s104db(*this)
{
    /** Initialise handle which will be passed to TMW*/
    memset(&m_twmdbhandle,0,sizeof(I870DBHandleStr));
    m_twmdbhandle.pIEC870DB        = &m_s104db;

    /** Register callback functions */
    m_twmdbhandle.ccsnaSetTime           = S104Database::ccsnaSetTime          ;
    m_twmdbhandle.ccsnaGetTime           = S104Database::ccsnaGetTime          ;
    m_twmdbhandle.mspGetPoint            = S104Database::mspGetPoint          ;
    m_twmdbhandle.mdpGetPoint            = S104Database::mdpGetPoint          ;
    m_twmdbhandle.mmenaGetPoint          = S104Database::mmenaGetPoint        ;
    m_twmdbhandle.mmenbGetPoint          = S104Database::mmenbGetPoint        ;
    m_twmdbhandle.mmencGetPoint          = S104Database::mmencGetPoint        ;
    m_twmdbhandle.mitGetPoint            = S104Database::mitGetPoint          ;
    m_twmdbhandle.cscLookupPoint         = S104Database::cscLookupPoint       ;
    m_twmdbhandle.cdcLookupPoint         = S104Database::cdcLookupPoint       ;
    m_twmdbhandle.hasSectorReset         = S104Database::hasSectorReset       ;
    m_twmdbhandle.crpnaExecuteRTUReset   = S104Database::crpnaExecuteRTUReset;
//    m_twmdbhandle.mitFreeze              = S104Database::mitFreeze            ;
//    m_twmdbhandle.cscSelect              = S104Database::cscSelect            ;
//    m_twmdbhandle.cscExecute             = S104Database::cscExecute           ;
//    m_twmdbhandle.cdcSelect              = S104Database::cdcSelect            ;
//    m_twmdbhandle.cdcExecute             = S104Database::cdcExecute           ;
    m_twmdbhandle.deleteEventID          = S104Database::deleteEventID        ;
}

S104Sector::~S104Sector()
{
    closeSector();

    delete mp_tmwsectorCnfg;
    mp_tmwsectorCnfg = NULL;
}


SCADAP_ERROR S104Sector::openSector(TMWSESN* tmwsesn)
{
    checkNotNull(mp_tmwsectorCnfg, "Sector not configured");

    /*Open sector and register callback functions*/
    mp_tmwsector = s104sctr_openSector(tmwsesn, mp_tmwsectorCnfg, &m_twmdbhandle);
    checkNotNull(mp_tmwsector,"S104 Sector open failed");

    tmwsctr_setUserDataPtr(mp_tmwsector,&m_twmdbhandle);

    /* Update sector handle*/
    m_twmdbhandle.pTMWSector = mp_tmwsector;

    DBG_INFO("%s open sector: %p",TITLE, (void*)mp_tmwsector);
    return SCADAP_ERROR_NONE;
}

void S104Sector::closeSector()
{
    if(mp_tmwsector != NULL)
    {
        s104sctr_closeSector(mp_tmwsector);
        mp_tmwsector = NULL;
    }
}

void S104Sector::configure(const S104SCTR_CONFIG& sectorConfig)
{
    if(mp_tmwsectorCnfg == NULL)
    {
        mp_tmwsectorCnfg = new S104SCTR_CONFIG();
    }

    memcpy(mp_tmwsectorCnfg, &sectorConfig, sizeof(sectorConfig));

    /* Apply configuration to existing sector*/
    if(mp_tmwsector != NULL)
    {
        s104sctr_setSectorConfig(mp_tmwsector, mp_tmwsectorCnfg);
    }
}

S104Database& S104Sector::getS104Database()
{
    return m_s104db;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
