/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: ft12chnl.c
 * description: IEC 60870-5-2 ft1.2 master and slave channel code
 *   used by 101 and 103 (and 102 when implemented)
 */

/* Misc Header Files */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/utils/tmwtarg.h"
#include "tmwscl/utils/tmwscl.h"
#include "tmwscl/utils/tmwmem.h"
#include "tmwscl/i870/ft12chnl.h"
#include "tmwscl/i870/i870chnl.h"
#include "tmwscl/i870/i870mem.h"
#include "tmwscl/i870/i870mem1.h"
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/i870dia1.h"

/* function _beforeTxCallback */
static void TMWDEFS_CALLBACK _beforeTxCallback(
  TMWSESN_TX_DATA *pTxData)
{
  i870chnl_beforeTxCallback(pTxData);
}

/* function _afterTxCallback */
static void TMWDEFS_CALLBACK _afterTxCallback(
  TMWSESN_TX_DATA *pTxData)
{
  i870chnl_afterTxCallback(pTxData);
}

/* function _failedTxCallback */
static void TMWDEFS_CALLBACK _failedTxCallback(
  TMWSESN_TX_DATA *pTxData)
{
  i870chnl_failedTxCallback(pTxData);
}

/* function: _infoCallback */
static void TMWDEFS_CALLBACK _infoCallback(
  void *pParam,
  TMWSESN *pSession,
  TMWSCL_INFO sesnInfo)
{
  i870chnl_infoCallback(pParam, pSession, sesnInfo);
}

/* function: _parseFrameCallback */
static void TMWDEFS_CALLBACK _parseFrameCallback(
  void *pParam,
  TMWSESN *pSession,
  TMWSESN_RX_DATA *pRxFrame)
{
  i870chnl_parseFrameCallback(pParam, pSession, pRxFrame);
}

/* function: _checkDataAvailable */
static TMWTYPES_BOOL TMWDEFS_CALLBACK _checkDataAvailable(
  void *pParam,
  TMWSESN *pSession,
  TMWDEFS_CLASS_MASK classMask,
  TMWTYPES_BOOL buildMessage)
{
  return (i870chnl_checkDataAvailable(pParam, pSession, classMask, buildMessage));
}

/* function: ft12chnl_initConfig */
void TMWDEFS_GLOBAL ft12chnl_initConfig( 
  I870CHNL_CONFIG *pChnlConfig,
  I870LNK1_CONFIG *pLinkConfig,
  TMWPHYS_CONFIG *pPhysConfig)
{
  /* Call generic initialization routines */
  i870chnl_initConfig(pChnlConfig);
  i870lnk1_initConfig(pLinkConfig);
  tmwphys_initConfig(pPhysConfig);
}

/* function: ft12chnl_openChannel */
TMWCHNL * TMWDEFS_GLOBAL ft12chnl_openChannel(
  TMWAPPL *pApplContext,
  const I870CHNL_CONFIG *pChnlConfig,
  const I870LNK1_CONFIG *pLinkConfig,
  const TMWPHYS_CONFIG *pPhysConfig,
  const void *pIOConfig,
  TMWTARG_CONFIG *pTmwTargConfig)
{
  TMWCHNL *pChannel;

  /* Initialize memory management and diagnostics if not yet done */
  if(!tmwappl_getInitialized(TMWAPPL_INIT_I870))
  { 
    if(!i870mem_init(TMWDEFS_NULL))
      return(TMWDEFS_NULL);

#if TMWCNFG_SUPPORT_DIAG
    i870diag_init();
#endif
    tmwappl_setInitialized(TMWAPPL_INIT_I870);
  }

  if(!tmwappl_getInitialized(TMWAPPL_INIT_FT12))
  { 
    if(!i870mem1_init(TMWDEFS_NULL))
      return(TMWDEFS_NULL);

#if TMWCNFG_SUPPORT_DIAG
    i870dia1_init();
#endif
    tmwappl_setInitialized(TMWAPPL_INIT_FT12);
  }

  pChannel = (TMWCHNL *)tmwmem_alloc(TMWMEM_CHNL_TYPE);
  if(pChannel != TMWDEFS_NULL)
  {
    i870chnl_openChannel(pChannel, pChnlConfig);

    tmwchnl_initChannel(pApplContext, pChannel, pChnlConfig->pStatCallback, pChnlConfig->pStatCallbackParam,
      pChnlConfig->pIdleCallback, pChnlConfig->pIdleCallbackParam, TMWDEFS_NULL, TMWDEFS_NULL, TMWDEFS_TRUE);

    if(tmwphys_initChannel(pChannel, pPhysConfig, pIOConfig, pTmwTargConfig))
    {
      if(i870lnk1_initChannel(pChannel, pLinkConfig))
      {
        pChannel->pLink->pLinkSetCallbacks(pChannel->pLinkContext,
          pChannel, _infoCallback, _parseFrameCallback, _checkDataAvailable,
          _beforeTxCallback, _afterTxCallback, _failedTxCallback);

        TMWTARG_LOCK_SECTION(&pApplContext->lock);
        tmwdlist_addEntry(&pApplContext->channels, (TMWDLIST_MEMBER *)pChannel);
        TMWTARG_UNLOCK_SECTION(&pApplContext->lock);

        return(pChannel);
      }

      tmwphys_deleteChannel(pChannel);
    }

    tmwchnl_deleteChannel(pChannel);
    tmwmem_free(pChannel);
  }

  return(TMWDEFS_NULL);
}

/* function: ft12chnl_modifyPhys */
TMWTYPES_BOOL TMWDEFS_GLOBAL ft12chnl_modifyPhys(
  TMWCHNL *pChannel,
  const TMWPHYS_CONFIG *pPhysConfig,
  TMWTYPES_ULONG configMask)
{
  return(tmwphys_modifyChannel(pChannel, pPhysConfig, configMask));
}

/* function: ft12chnl_modifyLink */
TMWTYPES_BOOL TMWDEFS_GLOBAL ft12chnl_modifyLink(
  TMWCHNL *pChannel,
  const I870LNK1_CONFIG *pLinkConfig,
  TMWTYPES_ULONG configMask)
{
  return(i870lnk1_modifyChannel(pChannel, pLinkConfig, configMask));
}

/* function: ft12chnl_closeChannel */
TMWTYPES_BOOL TMWDEFS_GLOBAL ft12chnl_closeChannel(
  TMWCHNL *pChannel)
{
  /* Check for NULL since this would be a common error */
  if(pChannel == TMWDEFS_NULL)
  {
    return(TMWDEFS_FALSE);
  }

  /* Channel locking is done in these two functions */

  /* Delete the FT1.2 link layer */
  if(i870lnk1_deleteChannel(pChannel) == TMWDEFS_FALSE)
  {
    return(TMWDEFS_FALSE);
  }

  /* Close the generic i870 channel */
  i870chnl_closeChannel(pChannel);

  return(TMWDEFS_TRUE);
}
