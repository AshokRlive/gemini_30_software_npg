/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/


/* file: s14fdrta.c
 * description: IEC 60870-5-101 slave FDRTA (File Transfer - 
 *  Directory) functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14fdrta.h"
#include "tmwscl/i870/s14file.h"
#include "tmwscl/i870/s14mem.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/s14evnt.h"
#include "tmwscl/utils/tmwtarg.h"

#if S14DATA_SUPPORT_FILE

/* function: s14fdrta_init */
void TMWDEFS_GLOBAL s14fdrta_init(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  p14Sector->fileDirIndex = 0;
  p14Sector->fileFdrtaCOT = 0;
}

/* function: s14fdrtaclose */
void TMWDEFS_GLOBAL s14fdrta_close(
  TMWSCTR *pSector)
{
  TMWTARG_UNUSED_PARAM(pSector);
} 

/* function: s14fdrta_addEvent */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14fdrta_addEvent(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  TMWTARG_LOCK_SECTION(&pSector->pSession->pChannel->lock);

  p14Sector->fileDirIndex = 0;
  p14Sector->fileFdrtaCOT = I14DEF_COT_SPONTANEOUS;

  /* Tell link layer we have data to send */
  s14event_linkDataReady(pSector);

  TMWTARG_UNLOCK_SECTION(&pSector->pSession->pChannel->lock);
  return(TMWDEFS_TRUE);
}

/* function: s14fdrta_scanForChanges */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14fdrta_scanForChanges(
  TMWSCTR *pSector)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  
  /* If scanning not enabled on this sector return */
  if(!p14Sector->fdrtaScanEnabled)
    return(TMWDEFS_FALSE);

  if(s14data_fileDirectoryChanged(p14Sector->i870.pDbHandle))
  {
    p14Sector->fileDirIndex = 0;
    p14Sector->fileFdrtaCOT = I14DEF_COT_SPONTANEOUS;
    return(TMWDEFS_TRUE);
  }
  
  return(TMWDEFS_FALSE);
}

#endif /* S14DATA_SUPPORT_FILE */
