/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i870mem.h
 * description: IEC 60870-5 memory allocation routines
 */
#ifndef I870MEM_DEFINED
#define I870MEM_DEFINED

#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/utils/tmwmem.h"

typedef enum I870MemUtilAllocType {
  I870MEM_CHNL_TX_DATA_TYPE,
  I870MEM_ALLOC_TYPE_MAX
} I870MEM_ALLOC_TYPE;
 
typedef struct {
  /* see description below for I870MEM_NUMALLOC_SESN_TX_DATAS */
  TMWTYPES_UINT numTxDatas;
} I870MEM_CONFIG;

/* Number of simultaneous requests or responses for all I870 channels 
 * (and therefore session and sectors on those channels). 
 * recommend at least 1 per sector for 101 or 103 slaves
 *   104 slaves can have multiple messages outstanding simultaneously.
 *   and could require window size k times the number of 104 channels
 * recommend 2 or more per sector for master
 *   master requires more if multiple requests can be outstanding or queued.
 *   and if requests are sent automatically 
 *       for 101/104 if xxxActionMask != 0, 
 *       for 103 eoiMask, onlineMask or blockingDisabledMask != 0;
 *     1 may be needed for each simultaneous request.
 */
#define I870MEM_NUMALLOC_SESN_TX_DATAS      TMWCNFG_MAX_APPL_MSGS   

#ifdef __cplusplus
extern "C" {
#endif
   
  /* routine: i870mem_initConfig
   * purpose:  INTERNAL FUNCTION to initialize the memory configuration 
   *  structure indicating how many of each structure type to put in
   *  the memory pool. These will be initialized according 
   *  to the compile time defines.
   *  NOTE: user should call s101mem_initConfig() or s104mem_initConfig()
   *   m101mem_initConfig() m104mem_initConfig() s103mem_initConfig()
   *   or m103mem_initConfig()to get the number of buffers allowed for 
   *   each type.
   * arguments:
   *  pConfig - pointer to memory configuration structure to be filled in
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL i870mem_initConfig(
    I870MEM_CONFIG   *pConfig);

  /* routine: i870mem_init
   * purpose: INTERNAL memory management init function.
   *  NOTE: user should call s101mem_initMemory() or s104mem_initMemory()
   *   m101mem_initMemory() m104mem_initMemory() s103mem_initMemory()
   *   or m103mem_initMemory()to modify the number of buffers allowed for 
   *   each type.
   * arguments:
   *  pConfig - pointer to memory configuration structure to be used
   * returns:
   *  TMWDEFS_TRUE if successful
   *  TMWDEFS_FALSE otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL i870mem_init(
    I870MEM_CONFIG *pConfig);

  /* function: i870mem_alloc
   * purpose:  Allocate memory  
   * arguments: 
   *  type - enum value indicating what structure to allocate
   * returns:
   *   TMWDEFS_NULL if allocation failed
   *   void * pointer to allocated memory if successful
   */
  void * TMWDEFS_GLOBAL i870mem_alloc(
    I870MEM_ALLOC_TYPE type);

  /* function: i870mem_free
   * purpose:  Deallocate memory
   * arguments: 
   *  pBuf - pointer to buffer to be deallocated
   * returns:    
   *   void  
   */
  void TMWDEFS_GLOBAL i870mem_free(
    void *pBuf);
 
  /* function: i870mem_getUsage
   * purpose:  Determine memory usage for each type of memory
   *    managed by this file.
   * arguments: 
   *  index: index of pool, starting with 0 caller can call
   *    this function repeatedly, while incrementing index. When
   *     index is larger than number of pools, this function
   *     will return TMWDEFS_FALSE
   *  pName: pointer to a char pointer to be filled in
   *  pStruct: pointer to structure to be filled in.
   * returns:    
   *  TMWDEFS_TRUE  if successfully returned usage statistics.
   *  TMWDEFS_FALSE if failure because index is too large.
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL i870mem_getUsage(
    TMWTYPES_UCHAR index,
    const TMWTYPES_CHAR **pName,
    TMWMEM_POOL_STRUCT *pStruct);
 
#ifdef __cplusplus
}
#endif
#endif /* I870MEM_DEFINED */
