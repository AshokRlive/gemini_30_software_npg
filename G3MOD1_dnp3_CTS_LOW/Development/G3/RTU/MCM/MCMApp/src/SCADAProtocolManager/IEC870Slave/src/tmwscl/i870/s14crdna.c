/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/


/* file: s14crdna.c
 * description: IEC 60870-5-101 slave CRDNA (Read Command)
 *  functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14crdna.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/s14dbas.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/i870chnl.h"

#if S14DATA_SUPPORT_CRDNA

/* function: s14crdna_processRequest */
void TMWDEFS_CALLBACK s14crdna_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  /* Store originator address */
  p14Sector->crdnaOriginator = pMsg->origAddress;
  
  /* Parse Information Object Address */
  i870util_readInfoObjAddr(pSector->pSession, pMsg, &p14Sector->crdnaIOA);

  /* Parse received cause of transmission */
  if(pMsg->cot == I14DEF_COT_REQUEST)
  {
    p14Sector->crdnaCOT = I14DEF_COT_REQUEST;
  }
  else
  {
    p14Sector->crdnaCOT = I14DEF_COT_UNKNOWN_COT | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  if(p14Sector->crdnaIOA == 0)
  {
    p14Sector->crdnaCOT = I14DEF_COT_UNKNOWN_IOA | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  p14Sector->crdnaDataReady = TMWDEFS_FALSE;
}

/* function: s14crdna_buildResponse */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14crdna_buildResponse(
  TMWSCTR *pSector, 
  TMWTYPES_BOOL buildResponse)
{
  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  if(p14Sector->crdnaCOT != 0)
  {  
    /* If COT is REQUEST this means it will try to read data.
     * See if database said it was ready yet.
     * Send ACTCON NEGATIVE right away.
     */
    if((p14Sector->crdnaCOT == I14DEF_COT_REQUEST) 
      &&(!p14Sector->crdnaDataReady))
    {
      p14Sector->crdnaDataReady = s14data_dataReady(p14Sector->i870.pDbHandle, I14DEF_TYPE_CRDNA1);
   
      if(!p14Sector->crdnaDataReady)
      {
        /* Database is not ready, so no read response to send at this time */
        return(TMWDEFS_FALSE);
      }
    }

    if(buildResponse)
    {
      TMWSESN_TX_DATA *pTxData;
      S14SESN *pS14Session = (S14SESN *)pSector->pSession;
        
      if((pTxData = i870chnl_newTxData(pSector->pSession->pChannel, 
        pSector->pSession, pSector, pS14Session->maxASDUSize)) == TMWDEFS_NULL)
      {
        return(TMWDEFS_FALSE);
      }

#if TMWCNFG_SUPPORT_DIAG
      pTxData->pMsgDescription = "Read Response";
#endif
      pTxData->txFlags = TMWSESN_TXFLAGS_NO_RESPONSE;
      pTxData->responseTimeout = p14Sector->defaultResponseTimeout;

      if(p14Sector->crdnaCOT == I14DEF_COT_REQUEST)
      {
        if(s14dbas_readPoint(pTxData, pSector, p14Sector->crdnaIOA))
        {
          /* Request is complete */
          p14Sector->crdnaCOT = 0;
         
          /* Send the response */
          i870chnl_sendMessage(pTxData);

          return(TMWDEFS_TRUE);
        }
        else
        {
          p14Sector->crdnaCOT = I14DEF_COT_UNKNOWN_IOA | I14DEF_COT_NEGATIVE_CONFIRM;
        }
      }

      /* Some kind of error, so transmit response */
      i870util_buildMessageHeader(pSector->pSession, 
        pTxData, I14DEF_TYPE_CRDNA1, p14Sector->crdnaCOT, 
        p14Sector->crdnaOriginator, p14Sector->i870.asduAddress);

      /* Store point number, always 0 for ACTCON or DEACTCON */
      i870util_storeInfoObjAddr(pSector->pSession, pTxData, p14Sector->crdnaIOA);

      /* Request is complete */
      p14Sector->crdnaCOT = 0;

      /* Send the response */
      i870chnl_sendMessage(pTxData);
    }

    return(TMWDEFS_TRUE);
  }

  return(TMWDEFS_FALSE);
}

#endif /* S14DATA_SUPPORT_CRDNA */
