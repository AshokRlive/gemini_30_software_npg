/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14util.c
 * description: IEC 60870-5-101/104 slave utilities.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/s14rbe.h"
#include "tmwscl/i870/s14mem.h"
#include "tmwscl/i870/i870util.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/i870chnl.h"
#include "tmwscl/i870/i14diag.h"
#include "tmwscl/utils/tmwtarg.h"

typedef struct {
  TMWTYPES_UCHAR typeId24;
  TMWTYPES_UCHAR typeId56;
} S14UTIL_CONVERT_ASDUTYPE; 

#define S14UTIL_INVALID 0
#define S14UTIL_MSPNA 1
#define S14UTIL_MDPNA 2
#define S14UTIL_MSTNA 3
#define S14UTIL_MBONA 4
#define S14UTIL_MMENA 5
#define S14UTIL_MMENB 6
#define S14UTIL_MMENC 7
#define S14UTIL_MITNA 8
#define S14UTIL_MITNC 9
#define S14UTIL_MEPTA 10
#define S14UTIL_MEPTB 11
#define S14UTIL_MEPTC 12

const S14UTIL_CONVERT_ASDUTYPE convertAsduType[] = {
  {0, 0},
  {I14DEF_TYPE_MSPTA1, I14DEF_TYPE_MSPTB1},
  {I14DEF_TYPE_MDPTA1, I14DEF_TYPE_MDPTB1},
  {I14DEF_TYPE_MSTTA1, I14DEF_TYPE_MSTTB1},
  {I14DEF_TYPE_MBOTA1, I14DEF_TYPE_MBOTB1},
  {I14DEF_TYPE_MMETA1, I14DEF_TYPE_MMETD1},
  {I14DEF_TYPE_MMETB1, I14DEF_TYPE_MMETE1},
  {I14DEF_TYPE_MMETC1, I14DEF_TYPE_MMETF1},
  {I14DEF_TYPE_MITTA1, I14DEF_TYPE_MITTB1},
  {0, I14DEF_TYPE_MITTC1}
};



#if TMWCNFG_SUPPORT_DIAG
/* function: _msgDescription() 
 * purpose: Return a message description based on ASDU Type ID
 * arguments:
 *  typeId - ASDU Type ID
 * returns:
 *  pointer to string containing message description
 */
static TMWTYPES_CHAR * TMWDEFS_CALLBACK _msgDescription(
  TMWTYPES_UCHAR typeId)
{
  switch(typeId)
  {
  case I14DEF_TYPE_CBONA1:
    case I14DEF_TYPE_CBOTA1:
      return("Bitstring Response");
    
    case I14DEF_TYPE_CDCNA1:
    case I14DEF_TYPE_CDCTA1:
      return("Double Command Response");

    case I14DEF_TYPE_CRCNA1:
    case I14DEF_TYPE_CRCTA1:
      return("Step Command Response");

    case I14DEF_TYPE_CSCNA1:
    case I14DEF_TYPE_CSCTA1:
      return("Single Command Response");

    case I14DEF_TYPE_CSENA1: 
    case I14DEF_TYPE_CSETA1:
      return("Set Point Command Normalized Response");

    case I14DEF_TYPE_CSENB1:
    case I14DEF_TYPE_CSETB1:
      return("Set Point Command Scaled Response");

    case I14DEF_TYPE_CSENC1:
    case I14DEF_TYPE_CSETC1:
      return("Set Point Command Short FloatingResponse");
      
    case I14DEF_TYPE_CSENZ1:
      return("Set Integrated Total Command BCD Response");

    default:
      return("Unknown Response");
  }
}
#endif

/* function: _checkSendActterm 
 * purpose: Determine whether an ACT TERM should be sent based
 *  on the ASDU Type ID and the configuration parameters for the
 *  set commands and other commands.
 * arguments:
 *  typeId - ASDU Type ID
 *  pS14Sector - pointer to Slave 101/104 sector structure
 * returns: 
 *  TMWDEFS_TRUE if ACT TERM should be sent. 
 *  TMWDEFS_FALSE if not.
 */
static TMWTYPES_BOOL TMWDEFS_GLOBAL _checkSendActterm(
  TMWTYPES_UCHAR typeId, 
  S14SCTR *p14Sector)
{
  TMWTYPES_BOOL rc = TMWDEFS_TRUE;
  switch (typeId )
  {
  case I14DEF_TYPE_CSENA1:
  case I14DEF_TYPE_CSENB1:
  case I14DEF_TYPE_CSENC1:
  case I14DEF_TYPE_CSETA1:
  case I14DEF_TYPE_CSETB1:
  case I14DEF_TYPE_CSETC1:
    if (p14Sector->cseUseActTerm == TMWDEFS_FALSE)
    {
      rc = TMWDEFS_FALSE;
    }
    break;
  default:
    if (p14Sector->cmdUseActTerm == TMWDEFS_FALSE)
    {
      rc = TMWDEFS_FALSE;
    }
  }

  return(rc);
}

/* function: _getOrigAddress */
static TMWTYPES_UCHAR TMWDEFS_LOCAL _getOrigAddress(
  TMWSCTR *pSector,
  TMWTYPES_UCHAR cot)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;

#if S14DATA_SUPPORT_CICNA  
  if((cot >= I14DEF_COT_INTG_GEN) && (cot <= I14DEF_COT_INTG16))
    return(p14Sector->cicnaOriginator);
#endif

#if S14DATA_SUPPORT_CCINA  
  if((cot >= I14DEF_COT_REQCOGEN) && (cot <= I14DEF_COT_REQCO4))
    return(p14Sector->ccinaOriginator);
#endif

#if S14DATA_SUPPORT_CRDNA  
  if(cot == I14DEF_COT_REQUEST)
    return(p14Sector->crdnaOriginator);
#endif

  return(0);
}

/* function to convert from without time to with time typeId */
TMWTYPES_UCHAR TMWDEFS_GLOBAL s14util_determineAsduType(
  TMWTYPES_UCHAR typeId, 
  TMWDEFS_TIME_FORMAT timeFormat)
{
  int index = S14UTIL_INVALID;
  switch(typeId)
  {
    case I14DEF_TYPE_MSPNA1:
      index = S14UTIL_MSPNA;
      break;
    case I14DEF_TYPE_MDPNA1:
      index = S14UTIL_MDPNA;
      break;
    case I14DEF_TYPE_MSTNA1:
      index = S14UTIL_MSTNA;
      break;
    case I14DEF_TYPE_MBONA1:
      index = S14UTIL_MBONA;
      break;
    case I14DEF_TYPE_MMENA1:
      index = S14UTIL_MMENA;
      break;
    case I14DEF_TYPE_MMENB1:
      index = S14UTIL_MMENB;
      break;
    case I14DEF_TYPE_MMENC1:
      index = S14UTIL_MMENC;
      break;
    case I14DEF_TYPE_MITNA1:
      index = S14UTIL_MITNA;
      break; 
    case I14DEF_TYPE_MITNC1:
      index = S14UTIL_MITNC;
      break; 
  }

  if(index != S14UTIL_INVALID)
  {
    if(timeFormat == TMWDEFS_TIME_FORMAT_24) 
    {
      typeId = convertAsduType[index].typeId24;
    }
    else if(timeFormat == TMWDEFS_TIME_FORMAT_56) 
    {
      typeId = convertAsduType[index].typeId56;
    }
  }

  return(typeId);
}

/* function: s14util_readIntoResponse */
S14DBAS_GROUP_STATUS TMWDEFS_GLOBAL s14util_readIntoResponse(
  TMWSESN_TX_DATA *pTxData, 
  TMWSCTR *pSector,
  TMWTYPES_UCHAR cot, 
  TMWDEFS_GROUP_MASK groupMask, 
  TMWTYPES_ULONG ioa,
  TMWTYPES_UCHAR typeId, 
  TMWDEFS_TIME_FORMAT timeFormat,
  TMWTYPES_UCHAR dataSize, 
  TMWTYPES_USHORT *pPointIndex,
  S1UTIL_GET_POINT_FUNC pGetPoint,
  S1UTIL_GET_GROUP_FUNC pGetGroupMask,
  S1UTIL_GET_IOA_FUNC pGetIOA,
  S1UTIL_GET_INDEXED_FUNC pGetIndexed,
  S1UTIL_GET_TIME_FORMAT_FUNC pGetTimeFormat,
  S1UTIL_STORE_RESP_FUNC pStoreResp)
{
  /* Initialize status to NO DATA and try to read next point */
  S14DBAS_GROUP_STATUS status = S14DBAS_GROUP_STATUS_NO_DATA;
  S14SESN *p14Session = (S14SESN *)pSector->pSession;
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  void *pPoint = pGetPoint(p14Sector->i870.pDbHandle, *pPointIndex);

  /* See if there is any data to send */
  if(pPoint != TMWDEFS_NULL)
  {
    TMWTYPES_BOOL timeFormatPerPoint = TMWDEFS_FALSE;
    TMWTYPES_BOOL useIndexed = TMWDEFS_TRUE;
    TMWTYPES_USHORT index = *pPointIndex;
    TMWTYPES_USHORT numSent = 0;
    TMWTYPES_ULONG lastIOA = 0;
    TMWTYPES_UCHAR timeSize = 0;
    TMWTYPES_UCHAR ioaSize = p14Session->i870.infoObjAddrSize;
    TMWTYPES_UCHAR origAddress = _getOrigAddress(pSector, cot);

    while(pPoint != TMWDEFS_NULL)
    {
      /* See if this point's group mask matches */
      if((groupMask & pGetGroupMask(pPoint)) != 0)
      {
        /* If requested IOA is not 0 see if it matches */
        if((ioa == 0) || (ioa == pGetIOA(pPoint)))
        {
          /* Found a match, add to message */
          if(lastIOA == 0)
          {
            /* This is the first point in the message */

            /* If this sector is configured for a per point time format, 
             * determine the time format for the first point and adjust the typeId to match 
             */
            if(timeFormat == TMWDEFS_TIME_FORMAT_UNKNOWN)
            {
              timeFormatPerPoint = TMWDEFS_TRUE;
              timeFormat = pGetTimeFormat(pPoint);
            }
            if(timeFormat == TMWDEFS_TIME_FORMAT_24)
            {
              timeSize = 3; 
            }
            else if(timeFormat == TMWDEFS_TIME_FORMAT_56)
            {
              timeSize = 7;
            }

            /* Convert type id to support correct timeFormat if needed */
            typeId = s14util_determineAsduType(typeId, timeFormat);

            /* conversion failed */
            if(typeId == 0)
              return(S14DBAS_GROUP_STATUS_NO_DATA);

            i870util_buildMessageHeader(pSector->pSession,
              pTxData, typeId, cot, origAddress, p14Sector->i870.asduAddress);
           
            /* Determine whether message will use indexed or sequential addressing 
             *  and store first information object address
             *  NOTE: All messages that contain time use indexed
             */
            lastIOA = pGetIOA(pPoint);
            if(timeFormat == TMWDEFS_TIME_FORMAT_NONE)
            {
              useIndexed = pGetIndexed(pPoint);
            }
            i870util_storeInfoObjAddr(pSector->pSession, pTxData, lastIOA);
          }
          else
          {

            /* If this is not the first point in response, first check to make sure 
             *  the address mode for this point is compatible with this message.
             */
            if((timeFormat == TMWDEFS_TIME_FORMAT_NONE)
              &&(pGetIndexed(pPoint) != useIndexed))
            {
              status = S14DBAS_GROUP_STATUS_MORE_DATA;
              break;
            }

            if((timeFormatPerPoint) 
              &&(pGetTimeFormat(pPoint) != timeFormat))
            {
              status = S14DBAS_GROUP_STATUS_MORE_DATA;
              break;
            }

            /* Next, if using indexed addressing store the information object
             *  address, else verify that the information object address is in
             *  fact sequential
             */
            if(useIndexed)
            {
              i870util_storeInfoObjAddr(pSector->pSession, pTxData, pGetIOA(pPoint));
            }
            else
            {
              TMWTYPES_ULONG thisIOA = pGetIOA(pPoint);
              if(thisIOA != (lastIOA + 1))
              {
                status = S14DBAS_GROUP_STATUS_MORE_DATA;
                break;
              }

              lastIOA = thisIOA;
              /* 
               * ioaSize is only used to determine if another point will fit in response.
               * If sequential no additional IOA will be put in response
               */
              ioaSize = 0;
            }
          }

          /* Store response data */
          pStoreResp(pTxData, pPoint, timeFormat);  

          /* Increment the number of information objects sent */
          numSent += 1;

          /* Assume we are done */
          status = S14DBAS_GROUP_STATUS_COMPLETE;

          /* If searching for a specific point we're done */
          if(ioa != 0)
            break;

          /* PMENA/B/C can only be sent one point at a time according to both 101 Ed 1 and Ed 2*/
          if((p14Sector->strictAdherence & S14SCTR_STRICT_PMENABC)
            && (typeId >= I14DEF_TYPE_PMENA1)
            && (typeId <= I14DEF_TYPE_PMENC1))
          {
            status = S14DBAS_GROUP_STATUS_MORE_DATA;
            ++index;
            break;
          }

          /* If another data point will not fit, set status to
           *  more data and break
           */
          if(((pTxData->msgLength + ioaSize + dataSize + timeSize) > pTxData->maxLength)
            ||(numSent == 127))
          {
            status = S14DBAS_GROUP_STATUS_MORE_DATA;
            ++index;
            break;
          }
        }
      }

      /* Get the next point */
      pPoint = pGetPoint(p14Sector->i870.pDbHandle, ++index);
    }

    /* Update quantity in response */
    pTxData->pMsgBuf[1] = (TMWTYPES_UCHAR)numSent;
    if(!useIndexed) pTxData->pMsgBuf[1] |= 0x80;

    /* Update passed point index */
    *pPointIndex = index;
  }

  return(status);
}

/* function: s14util_buildCommandResponse */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14util_buildCommandResponse(
  TMWSCTR *pSector,
  TMWTYPES_BOOL buildResponse,
  TMWTYPES_UCHAR typeId,
  TMWTYPES_UCHAR *pCOT,
  TMWTYPES_UCHAR originatorAddress,
  TMWTYPES_ULONG ioa,
  TMWTYPES_UCHAR qualifier,
  TMWTYPES_BOOL useQualifier,
  void *pData,
  TMWTYPES_UCHAR numDataBytes,
  TMWDTIME *pDateTime,
  S1UTIL_STATUS_FUNC pStatus)
{
  /* See if a command of this type is pending */
  if(*pCOT != 0)
  {
    /* See if we need to actually build the response */
    if(buildResponse)
    {
      TMWSESN_TX_DATA *pTxData;
      S14SCTR *p14Sector = (S14SCTR *)pSector;
      S14SESN *p14Session = (S14SESN *)pSector->pSession;

      if((pTxData = i870chnl_newTxData(pSector->pSession->pChannel, pSector->pSession, pSector,
        p14Session->maxASDUSize)) == TMWDEFS_NULL)
      {
        return(TMWDEFS_FALSE);
      }

#if TMWCNFG_SUPPORT_DIAG
      pTxData->pMsgDescription = _msgDescription(typeId);
#endif
      pTxData->txFlags = TMWSESN_TXFLAGS_NO_RESPONSE;
      pTxData->responseTimeout = p14Sector->defaultResponseTimeout;

      /* Generate appropriate response based on current state */
      if(*pCOT != I14DEF_COT_ACTTERM)
      {
        /* Activation confirmation or deactivation */

        /* If ACTCON try to perform requested operation */
        if(*pCOT == I14DEF_COT_ACTCON)
        {
          switch(pStatus(pSector))
          {
          case TMWDEFS_CMD_STAT_FAILED:
            /* Select or Execute failed, negate confirmation and
             * send reply
             */
            *pCOT |= I14DEF_COT_NEGATIVE_CONFIRM;
            break;

          case TMWDEFS_CMD_STAT_SUCCESS:
            /* Select or Execute successful, send confirmation
             */
            break;

          default:
            /* Command must still be in progress */
            i870chnl_freeTxData(pTxData);
            return(TMWDEFS_FALSE);
          }
        }

        /* Build response */
        i870util_buildMessageHeader(pSector->pSession, pTxData,
          typeId, *pCOT, originatorAddress, p14Sector->i870.asduAddress);

        /* Store point number */
        i870util_storeInfoObjAddr(pSector->pSession, pTxData, ioa);

        /* Store data */
        if(numDataBytes > 0)
        {
          switch(numDataBytes)
          {
          case 2:
            tmwtarg_store16((TMWTYPES_USHORT *)pData, &pTxData->pMsgBuf[pTxData->msgLength]);
            pTxData->msgLength += 2;
            break;

          case 4:
            tmwtarg_store32((TMWTYPES_ULONG *)pData, &pTxData->pMsgBuf[pTxData->msgLength]);
            pTxData->msgLength += 4;
            break;
          
          case 6:
            {
            int i;
            TMWTYPES_UCHAR *pBCD = (TMWTYPES_UCHAR*)pData;
            for(i=0; i<6; i++)
              pTxData->pMsgBuf[pTxData->msgLength++] = *pBCD++;
            }
            break;

          default:
            i870chnl_freeTxData(pTxData);
            return(TMWDEFS_FALSE);
          }
        }

        /* Store qualifier */
        if(useQualifier)
          pTxData->pMsgBuf[pTxData->msgLength++] = qualifier;

        /* Insert time if required */
        if(pDateTime != TMWDEFS_NULL)
        {
          i870util_write56BitTime(pTxData, pDateTime, S14DATA_SUPPORT_GENUINE_TIME);
        }

        /* If ACTCON and not select, and configured to send ACTTERM, 
         *  set up to send ACTTERM
         *  else request is complete
         */
        if(((qualifier & I14DEF_QOC_SE_SELECT) == 0)
          && (*pCOT == I14DEF_COT_ACTCON)
          && _checkSendActterm(typeId, p14Sector))
        {
          *pCOT = I14DEF_COT_ACTTERM;
        }
        else
        {
          *pCOT = 0;
        }

        /* Send the response */
        i870chnl_sendMessage(pTxData);
        return(TMWDEFS_TRUE);
      }
      else
      {
        TMWDEFS_COMMAND_STATUS cmdStatus;

        /* Check status of command, anything other than MONITORING is
         * considered complete. We do not need to check for failure 
         * because there is no such thing as an ACT TERM Negative. A
         * failure in monitoring could be sent back by setting the
         * Invalid bit in the monitored data.
         */
        cmdStatus = pStatus(pSector);
        if(cmdStatus == TMWDEFS_CMD_STAT_MONITORING)
        {
          i870chnl_freeTxData(pTxData);
          return(TMWDEFS_FALSE);
        }

        /* Check for monitored point COT RETURN_REMOTE "events" */
        if(s14rbe_processMonitorEvents(pSector, typeId))
        {
          i870chnl_freeTxData(pTxData);
          return(TMWDEFS_TRUE);
        }

        /* Build ACTTERM */
        i870util_buildMessageHeader(pSector->pSession, pTxData,
          typeId, *pCOT, originatorAddress, p14Sector->i870.asduAddress);

        *pCOT = 0;

        /* Store point number */
        i870util_storeInfoObjAddr(pSector->pSession, pTxData, ioa);

        /* Store data */
        if(numDataBytes > 0)
        {
          switch(numDataBytes)
          {
          case 2:
            tmwtarg_store16((TMWTYPES_USHORT *)pData, &pTxData->pMsgBuf[pTxData->msgLength]);
            pTxData->msgLength += 2;
            break;

          case 4:
            tmwtarg_store32((TMWTYPES_ULONG *)pData, &pTxData->pMsgBuf[pTxData->msgLength]);
            pTxData->msgLength += 4;
            break;

          case 6:
            {
            int i;
            TMWTYPES_UCHAR *pBCD = (TMWTYPES_UCHAR*)pData;
            for(i=0; i<6; i++)
              pTxData->pMsgBuf[pTxData->msgLength++] = *pBCD++;
            }
            break;

          default:
            i870chnl_freeTxData(pTxData);
            return(TMWDEFS_FALSE);
          }
        }

        /* Store qualifier */
        if(useQualifier)
          pTxData->pMsgBuf[pTxData->msgLength++] = qualifier;

        /* Insert time if required */
        if(pDateTime != TMWDEFS_NULL)
        {
          i870util_write56BitTime(pTxData, pDateTime, S14DATA_SUPPORT_GENUINE_TIME);
        }

        /* Send response */
        i870chnl_sendMessage(pTxData);
        return(TMWDEFS_TRUE);
      }
    }

    /* It is not sufficient to assume just because we have a command in
     * progress, that we are ready to send the response. Some commands take
     * time, so we really need to check the status, as we do when building
     * the response above.
     */
    else 
    {
      TMWDEFS_COMMAND_STATUS cmdStatus;

      /* Check status of command */
      cmdStatus = pStatus(pSector);
      if( (cmdStatus == TMWDEFS_CMD_STAT_FAILED) || (cmdStatus == TMWDEFS_CMD_STAT_SUCCESS))
      {
        /* Ready to send a response, if buildResponse was true */
        return(TMWDEFS_TRUE);
      }
      else
      {
        /* Command must still be in progress */
        return(TMWDEFS_FALSE);
      }
    }
  }

  /* No data available or sent */
  return(TMWDEFS_FALSE);
}

/* function: s14util_getDateTime() */
void TMWDEFS_GLOBAL s14util_getDateTime(
  TMWSCTR *pSector, 
  TMWDTIME *pDateTime)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  /* Get current date and time */
  s14data_ccsnaGetTime(p14Sector->i870.pDbHandle, pDateTime);

  pDateTime->genuineTime = TMWDEFS_TRUE;

  /* If periodic clock syncs required and period has expired since
   * last clock sync, set invalid bit.
   */
  if((p14Sector->clockValidPeriod != 0)
    && !tmwtimer_isActive(&p14Sector->clockValidTimer))
  {
    pDateTime->invalid = TMWDEFS_TRUE;
  }
}

/* function: s14util_alwaysUseIndexed */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14util_alwaysUseIndexed(
  void *pPoint)
{
  TMWTARG_UNUSED_PARAM(pPoint);
  return(TMWDEFS_TRUE);
}
#if S14DATA_SUPPORT_MULTICMDS
TMWTYPES_BOOL TMWDEFS_GLOBAL s14util_findCmdCtxt(
   S14SCTR *pS14Sector, 
   TMWTYPES_UCHAR typeId, 
   TMWTYPES_ULONG ioa, 
   S14SCTR_CMD **pRetContext)
{
  int cmdsForThisTypeId = 0;
  S14SCTR_CMD *pContext = TMWDEFS_NULL;
  if(tmwdlist_size(&pS14Sector->commandList) > 0)
  {  
    pContext = (S14SCTR_CMD *)tmwdlist_getFirst(&pS14Sector->commandList);
    while(pContext != TMWDEFS_NULL)
    { 
      if(pContext->typeId == typeId)
      {
        cmdsForThisTypeId++;
     
        if((pContext->ioa == ioa) 
          ||(pS14Sector->commandsPerType == 1))
        {
          /* found a matching typeId and ioa, don't look for any more */
          /* Or if only one context is allowed per typeId, use it
           * even though this will cancel the previous command to be
           * compatible with the previous behavior of the SCL.
           */
          *pRetContext = pContext;
          return TMWDEFS_TRUE;
        }
      }
      pContext = (S14SCTR_CMD *)tmwdlist_getNext((TMWDLIST_MEMBER *)pContext);
    }
  } 

  /* Did not find an existing context for this typeId and ioa combination */
  if((cmdsForThisTypeId < pS14Sector->commandsPerType)
    && (tmwdlist_size(&pS14Sector->commandList) < pS14Sector->commandsPerSector))
  {
    pContext = (S14SCTR_CMD*)s14mem_alloc(S14MEM_SCTR_CMD_TYPE); 
    if(pContext != TMWDEFS_NULL)
    {
      pContext->pS14Sector = pS14Sector;
      pContext->pPoint = TMWDEFS_NULL;
      pContext->ioa = ioa;
      pContext->typeId = typeId;
      pContext->cot = 0;
      pContext->qualifier = 0;
      pContext->useValue = TMWDEFS_FALSE;
      pContext->useQualifier = TMWDEFS_FALSE;
#if S14DATA_SUPPORT_TIMETAG_COMMANDS
      pContext->useDateTime = TMWDEFS_FALSE;
#endif
      pContext->pCheckMonitoredPoint = TMWDEFS_NULL;
      tmwtimer_init(&pContext->selectTimer);
      *pRetContext = pContext;
      tmwdlist_addEntry(&pS14Sector->commandList, (TMWDLIST_MEMBER*)pContext);
      return TMWDEFS_TRUE;
    }
  }

  *pRetContext = TMWDEFS_NULL;
  return TMWDEFS_FALSE;
}

void TMWDEFS_GLOBAL s14util_freeCmdCtxt(S14SCTR *p14Sector, S14SCTR_CMD*pContext)
{
  tmwtimer_cancel(&pContext->selectTimer);
  tmwdlist_removeEntry(&p14Sector->commandList, (TMWDLIST_MEMBER *)pContext);
  s14mem_free(pContext);
}

S14SCTR_CMD * TMWDEFS_GLOBAL s14util_processMultiRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg,
  TMWTYPES_UCHAR typeId,
  TMWTYPES_ULONG *pIOA,
  TMWTYPES_BOOL  *pTimerIsActive)
{ 
  S14SCTR_CMD *pContext;
  
  /* Parse Information Object Address */
  i870util_readInfoObjAddr(pSector->pSession, pMsg, pIOA);

  if(!s14util_findCmdCtxt((S14SCTR *)pSector, typeId, *pIOA, &pContext))
  {
    /* If we can't find a command in progress for this IOA, or an available command context
     * send negative response
     */
    if(pMsg->cot == I14DEF_COT_ACTIVATION)
    {
      s14sesn_sendNegActCon(pSector, pMsg, I14DEF_COT_ACTCON); 
    }
    else if(pMsg->cot == I14DEF_COT_DEACTIVATION)
    {
      s14sesn_sendNegActCon(pSector, pMsg, I14DEF_COT_DEACTCON); 
    }
    else
    {
      s14sesn_sendNegActCon(pSector, pMsg, I14DEF_COT_UNKNOWN_COT); 
    }
    return TMWDEFS_NULL;
  }

  /* If there is a previous command in progress, (response still to be sent)
   * queue a negative ACT CON to be sent. 
   * Don't overwrite the information from the previous command.
   */
  if((pMsg->cot == I14DEF_COT_ACTIVATION)
    && (pContext->cot != 0))
  {
    s14sesn_sendNegActCon(pSector, pMsg, I14DEF_COT_ACTCON); 
    return TMWDEFS_NULL;
  }

  if(pTimerIsActive != TMWDEFS_NULL)
  {
    /* Any new command cancels the select timer 
     * Save the timer state in case this is a valid execute command
     */
    *pTimerIsActive = tmwtimer_isActive(&pContext->selectTimer);
    if(*pTimerIsActive)
    {
      tmwtimer_cancel(&pContext->selectTimer);
    }
  }

  /* Store originator address */
  pContext->originatorAddress = pMsg->origAddress;
  return pContext;
}

static void TMWDEFS_LOCAL _selectTimeout(
  void *pCallbackParam)
{ 
  S14SCTR_CMD *pContext = (S14SCTR_CMD *)pCallbackParam;
  s14util_freeCmdCtxt(pContext->pS14Sector, pContext);
}

static TMWDEFS_COMMAND_STATUS TMWDEFS_LOCAL _checkStatus(TMWSCTR *pSector, S14SCTR_CMD *pContext)     
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  switch(pContext->state)
  {
  case TMWDEFS_CMD_STATE_SELECTING:

    if((pContext->status != TMWDEFS_CMD_STAT_SUCCESS) 
      && (pContext->status != TMWDEFS_CMD_STAT_FAILED))
    {
      /* Get Status */
      pContext->status = pContext->pStatus(pContext->pPoint);
    }

    /* If select is complete start select timer */
    if(pContext->status == TMWDEFS_CMD_STAT_SUCCESS)
    {
      tmwtimer_start(&pContext->selectTimer, 
        p14Sector->selectTimeout, pSector->pSession->pChannel, 
        _selectTimeout, pContext);
    }
    break;

  case TMWDEFS_CMD_STATE_EXECUTING: 

    if((pContext->status != TMWDEFS_CMD_STAT_SUCCESS) 
      && (pContext->status != TMWDEFS_CMD_STAT_FAILED))
    {
      /* Get Status */
      pContext->status = pContext->pStatus(pContext->pPoint);
    }
    
    /* If this command includes monitoring function 
     * such as cscna, cdcna, etc
     */
    if(pContext->pCheckMonitoredPoint != TMWDEFS_NULL)
    {
      /* If execute is complete change state to monitoring and status
        * to in progress so we will check the monitored state at least
        * once.
        */
      if((pContext->status == TMWDEFS_CMD_STAT_SUCCESS)
        || (pContext->status == TMWDEFS_CMD_STAT_MONITORING))
      {
        pContext->state = TMWDEFS_CMD_STATE_MONITORING;
        pContext->status = TMWDEFS_CMD_STAT_MONITORING;
        return(TMWDEFS_CMD_STAT_SUCCESS);
      }
    }

    break;
    
  case TMWDEFS_CMD_STATE_MONITORING:
    /* This state only applies to commands that include monitoring function */
    {
      if((pContext->status != TMWDEFS_CMD_STAT_SUCCESS) 
        && (pContext->status != TMWDEFS_CMD_STAT_FAILED))
      {
        /* Get Status */
        pContext->status = s14data_cscStatus(pContext->pPoint);
      }

      /* Check monitored feedback point status */
      if(pContext->status == TMWDEFS_CMD_STAT_SUCCESS)
      {

        /* If monitor is complete change state to idle */
        pContext->state = TMWDEFS_CMD_STATE_IDLE;

        if(pContext->pCheckMonitoredPoint != TMWDEFS_NULL)
          pContext->pCheckMonitoredPoint(pSector, pContext);
      }
    }

  default:
    break;
  }

  /* If command failed return to IDLE state */
  if(pContext->status == TMWDEFS_CMD_STAT_FAILED)
    pContext->state = TMWDEFS_CMD_STATE_IDLE;

  return(pContext->status);
}

static TMWTYPES_BOOL TMWDEFS_LOCAL _buildMsg(
  TMWSCTR *pSector,
  TMWSESN_TX_DATA *pTxData,
  TMWTYPES_UCHAR numDataBytes,
  void *pData,
  S14SCTR_CMD *pContext)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  /* Build response */
  i870util_buildMessageHeader(pSector->pSession, pTxData,
    pContext->typeId, pContext->cot, pContext->originatorAddress, p14Sector->i870.asduAddress);

  /* Store point number */
  i870util_storeInfoObjAddr(pSector->pSession, pTxData, pContext->ioa);

  /* Store data */
  if(numDataBytes > 0)
  {
    switch(numDataBytes)
    {
    case 2:
      tmwtarg_store16((TMWTYPES_USHORT *)pData, &pTxData->pMsgBuf[pTxData->msgLength]);
      pTxData->msgLength += 2;
      break;

    case 4:
      tmwtarg_store32((TMWTYPES_ULONG *)pData, &pTxData->pMsgBuf[pTxData->msgLength]);
      pTxData->msgLength += 4;
      break;
          
    case 6:
      {
      int i;
      TMWTYPES_UCHAR *pBCD = (TMWTYPES_UCHAR*)pData;
      for(i=0; i<6; i++)
        pTxData->pMsgBuf[pTxData->msgLength++] = *pBCD++;
      }
      break;

    default:
      i870chnl_freeTxData(pTxData);
      return(TMWDEFS_FALSE);
    }
  }

  /* Store qualifier */
  if(pContext->useQualifier)
    pTxData->pMsgBuf[pTxData->msgLength++] = pContext->qualifier;
  

#if S14DATA_SUPPORT_TIMETAG_COMMANDS
  /* Insert time if required */
  if(pContext->useDateTime)
  {
    i870util_write56BitTime(pTxData, &pContext->requestTime, S14DATA_SUPPORT_GENUINE_TIME);
  }
#endif
  
  return(TMWDEFS_TRUE);
}

static TMWTYPES_BOOL TMWDEFS_LOCAL _buildMultiCommandResponse(
  TMWSCTR *pSector,
  TMWTYPES_BOOL buildResponse,
  S14SCTR_CMD *pContext,
  TMWTYPES_BOOL *pCmdComplete)
{
  void *pData = TMWDEFS_NULL;
  TMWTYPES_UCHAR numDataBytes = 0;

  *pCmdComplete = TMWDEFS_FALSE;
  if(pContext->useValue)
  {
#ifdef TMWCNFG_SUPPORT_FLOAT
    if(pContext->valueType == TMWTYPES_ANALOG_TYPE_SFLOAT)
    {
      pData = &pContext->value.fval;
      numDataBytes = 4;
    }
#endif  
    if(pContext->valueType == TMWTYPES_ANALOG_TYPE_SHORT)
    {
      pData = &pContext->value.sval;
      numDataBytes = 2;
    }
    else if(pContext->valueType == TMWTYPES_ANALOG_TYPE_ULONG)
    {
      pData = &pContext->value.ulval;
      numDataBytes = 4;
    }
    else if(pContext->valueType == S14SCTR_TYPE_BCD)
    {
      pData = &pContext->value.bcd;
      numDataBytes = 6;
    }
    /* useValue is set to false for cscna and cscta */
  } 

  /* See if we need to actually build the response */
  if(buildResponse)
  {
    TMWSESN_TX_DATA *pTxData;
    S14SCTR *p14Sector = (S14SCTR *)pSector;
    S14SESN *p14Session = (S14SESN *)pSector->pSession;

    if((pTxData = i870chnl_newTxData(pSector->pSession->pChannel, pSector->pSession, pSector,
      p14Session->maxASDUSize)) == TMWDEFS_NULL)
    {
      return(TMWDEFS_FALSE);
    }

#if TMWCNFG_SUPPORT_DIAG
      pTxData->pMsgDescription = _msgDescription(pContext->typeId);
#endif
    pTxData->txFlags = TMWSESN_TXFLAGS_NO_RESPONSE;
    pTxData->responseTimeout = p14Sector->defaultResponseTimeout;

    /* Generate appropriate response based on current state */
    if(pContext->cot != I14DEF_COT_ACTTERM)
    {
      /* Activation confirmation or deactivation */

      /* If ACTCON try to perform requested operation */
      if(pContext->cot == I14DEF_COT_ACTCON)
      {
        switch(_checkStatus(pSector, pContext))
        {
        case TMWDEFS_CMD_STAT_FAILED:
          /* Select or Execute failed, negate confirmation and
            * send reply
            */
          pContext->cot |= I14DEF_COT_NEGATIVE_CONFIRM;
          break;

        case TMWDEFS_CMD_STAT_SUCCESS:
          /* Select or Execute successful, send confirmation
            */
          break;

        default:
          /* Command must still be in progress */
          i870chnl_freeTxData(pTxData);
          return(TMWDEFS_FALSE);
        }
      }

      if(!_buildMsg(pSector, pTxData, numDataBytes, pData, pContext))
        return(TMWDEFS_FALSE);
     
      if((pContext->qualifier & I14DEF_QOC_SE_SELECT) == 0)
      {
        /* If ACTCON and not select, and configured to send ACTTERM, 
         *  set up to send ACTTERM, otherwise the command is complete.
         */
        if((pContext->cot == I14DEF_COT_ACTCON)
          && _checkSendActterm(pContext->typeId, p14Sector))
        {
          pContext->cot = I14DEF_COT_ACTTERM;
        }
        else
        {
          pContext->cot = 0;
          *pCmdComplete = TMWDEFS_TRUE;
        }
      }
      else
      {
        /* If ACTCON and select, nothing to else to send */
        pContext->cot = 0;

        /* command is complete if select timer is not running */
        if(!tmwtimer_isActive(&pContext->selectTimer))
        {
          *pCmdComplete = TMWDEFS_TRUE; 
        }
      }
     
      /* Send the response */
      i870chnl_sendMessage(pTxData);
      return(TMWDEFS_TRUE);
    }
    else
    {
      TMWDEFS_COMMAND_STATUS cmdStatus;

      /* Check status of command, anything other than MONITORING is
        * considered complete. We do not need to check for failure 
        * because there is no such thing as an ACT TERM Negative. A
        * failure in monitoring could be sent back by setting the
        * Invalid bit in the monitored data.
        */
      cmdStatus = _checkStatus(pSector, pContext);
      if(cmdStatus == TMWDEFS_CMD_STAT_MONITORING)
      {
        i870chnl_freeTxData(pTxData);
        return(TMWDEFS_FALSE);
      }

      /* Check for monitored point COT RETURN_REMOTE "events" */
      if(s14rbe_processMonitorEvents(pSector, pContext->typeId))
      {
        i870chnl_freeTxData(pTxData);
        return(TMWDEFS_TRUE);
      }

      /* Build ACTTERM */
      if(!_buildMsg(pSector, pTxData, numDataBytes, pData, pContext))
        return(TMWDEFS_FALSE);

      pContext->cot = 0;
      *pCmdComplete = TMWDEFS_TRUE;
  
      /* Send response */
      i870chnl_sendMessage(pTxData);

      return(TMWDEFS_TRUE);
    }
  }

  /* It is not sufficient to assume just because we have a command in
   * progress, that we are ready to send the response. Some commands take
   * time, so we really need to check the status, as we do when building
   * the response above.
   */
  else 
  {
    TMWDEFS_COMMAND_STATUS cmdStatus;

    /* Check status of command */
    cmdStatus = _checkStatus(pSector, pContext);
    if( (cmdStatus == TMWDEFS_CMD_STAT_FAILED) || (cmdStatus == TMWDEFS_CMD_STAT_SUCCESS))
    {
      /* Ready to send a response, if buildResponse was true */
      return(TMWDEFS_TRUE);
    }
    else
    {
      /* Command must still be in progress */
      return(TMWDEFS_FALSE);
    }
  }
}

TMWTYPES_BOOL TMWDEFS_CALLBACK s14util_buildMultiResponse(
  TMWSCTR *pSector, 
  TMWTYPES_BOOL buildResponse)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  if(tmwdlist_size(&p14Sector->commandList) > 0)
  {  
    S14SCTR_CMD *pBaseContext = (S14SCTR_CMD *)tmwdlist_getFirst(&p14Sector->commandList);
    while(pBaseContext != TMWDEFS_NULL)
    { 
      /* If context indicates a response needs to be sent */
      if(pBaseContext->cot != 0)
      {
        TMWTYPES_BOOL cmdComplete = TMWDEFS_FALSE;
        TMWTYPES_BOOL status = _buildMultiCommandResponse(pSector, buildResponse, pBaseContext,
          &cmdComplete);

        if(cmdComplete)
        {
         s14util_freeCmdCtxt(p14Sector, pBaseContext);
         return status;
        }
    
        if(status)
          return TMWDEFS_TRUE;
      }
      pBaseContext = (S14SCTR_CMD *)tmwdlist_getNext((TMWDLIST_MEMBER *)pBaseContext);
    }
  }
  return TMWDEFS_FALSE;
}
#endif

