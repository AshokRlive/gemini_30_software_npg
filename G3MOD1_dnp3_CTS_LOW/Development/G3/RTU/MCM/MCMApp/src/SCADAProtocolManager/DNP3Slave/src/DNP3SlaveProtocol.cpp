 /******************************************************************************
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       DNP3 Slave protocol implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   06/09/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "SCADAProtocolManager.h"
#include "DNP3SlaveProtocol.h"
#include "TimeManager.h"
#include "timeOperations.h"
#include "bitOperations.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static IMCMApplication *mcmAppInstance = NULL;   //keep an instance of the Application for static method(s)

static const lu_uint32_t PROTOCOL_POOL_DELAY_US = 50000;

static TimeManager& timeMger = TimeManager::getInstance();
static EventLogManager& eventlog = *(EventLogManager::getInstance());
static Logger& logger = Logger::getLogger(SUBSYSTEM_ID_SCADA_HANDLER);

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
const std::string DNP3SlaveProtocol::VPEventStr::commonFlags()
{
    std::stringstream ss;
    ss
    << ((flags & DNPDEFS_DBAS_FLAG_ON_LINE)? "OnL" : "OffL") << "|"
    << ((flags & DNPDEFS_DBAS_FLAG_RESTART)? "Rest" : "-") << "|"
    << ((flags & DNPDEFS_DBAS_FLAG_COMM_LOST)? "Comm" : "-") << "|"
    << ((flags & DNPDEFS_DBAS_FLAG_REMOTE_FORCED)? "Rem" : "-") << "|"
    << ((flags & DNPDEFS_DBAS_FLAG_LOCAL_FORCED)? "Loc" : "-");
    return ss.str();
}


const std::string DNP3SlaveProtocol::VPEventStr::toString()
{
    std::stringstream ss;
    ss << (lu_uint32_t)sessionID << ":"
       << (lu_uint32_t)dnpID << " - ";
    switch(eventCode)
    {
        case DNPDEFS_OBJ_2_BIN_CHNG_EVENTS:
        {
            const TMWTYPES_UCHAR VALUEMASK = DNPDEFS_DBAS_FLAG_BINARY_ON | DNPDEFS_DBAS_FLAG_BINARY_OFF;
            ss << "Bin " << (((value & VALUEMASK) == 0)? 0 : 1)
               << " F=["
               << commonFlags() << "|"
               << ((flags & DNPDEFS_DBAS_FLAG_CHATTER)? "Chat" : "-")
               << "]";
        }
            break;
        case DNPDEFS_OBJ_4_DBL_CHNG_EVENTS:
        {
            const TMWTYPES_UCHAR VALUEMASK = DNPDEFS_DBAS_FLAG_DOUBLE_INDET |
                                             DNPDEFS_DBAS_FLAG_DOUBLE_OFF |
                                             DNPDEFS_DBAS_FLAG_DOUBLE_ON |
                                             DNPDEFS_DBAS_FLAG_DOUBLE_INTER;
            ss << "DBin " << LU_GETBITMASKVALUE(value, VALUEMASK, 7)
               << " F=["
               << commonFlags() << "|"
               << ((flags & DNPDEFS_DBAS_FLAG_CHATTER)? "Chat" : "-")
               << "]";
        }
            break;
        case DNPDEFS_OBJ_32_ANA_CHNG_EVENTS:
            ss << "Ana " << aValue
               << " F=["
               << commonFlags() << "|"
               << ((flags & DNPDEFS_DBAS_FLAG_OVER_RANGE)? "Over" : "-") << "|"
               << ((flags & DNPDEFS_DBAS_FLAG_REFERENCE_CHK)? "Ref" : "-")
               << "]";
            break;
        case DNPDEFS_OBJ_23_FCTR_EVENTS:
            ss << "FCntr " << value
               << " F=["
               << commonFlags() << "|"
               << ((flags & DNPDEFS_DBAS_FLAG_CNTR_ROLLOVER)? "Roll" : "-") << "|"
               << ((flags & DNPDEFS_DBAS_FLAG_DISCONTINUITY)? "Dis" : "-")
               << "]";
            break;
        case DNPDEFS_OBJ_22_CNTR_EVENTS:
            ss << "Cntr " << value
               << " F=["
               << commonFlags() << "|"
               << ((flags & DNPDEFS_DBAS_FLAG_CNTR_ROLLOVER)? "Roll" : "-") << "|"
               << ((flags & DNPDEFS_DBAS_FLAG_DISCONTINUITY)? "Dis" : "-")
               << "]";
            break;
        default:
            ss << "Unsupported Object";
            break;
    }
    return ss.str();
}


DNP3SlaveProtocol::DNP3SlaveProtocol(GeminiDatabase  &GDatabase,
                                     IMCMApplication &mcmApplication,
                                     const bool invalidEventTimeCorrection) :
                                         Thread(SCADAProtocolManager::SCADAThreadSched,
                                                SCADAProtocolManager::SCADAThreadPrio,
                                                Thread::TYPE_JOINABLE,
                                                "DNP3SlaveProtocol"),
                                         GDatabase(GDatabase),
                                         log(Logger::getLogger(SUBSYSTEM_ID_SCADA_HANDLER)),
                                         eventPipe(Pipe::PIPE_TYPE_NONBLOCKING),
                                         firstSynch(!invalidEventTimeCorrection)
{
    mcmAppInstance = &mcmApplication;

    /* Attach to time validity for applying re-timestamping */
    timeMger.attachFirstSynch(this);    //Attach to the 1st synchronisation
    
    /* Register function to display diagnostic strings to console */
    //tmwtargp_registerPutDiagStringFunc(logDiagnostic);

    //TODO to be moved to higher level
    /* Register poll timer timer functions */
    tmwtargp_registerStartTimerFunc(tmwpltmr_startTimer);
    tmwtargp_registerCancelTimerFunc(tmwpltmr_cancelTimer);

    applContextPtr = tmwappl_initApplication();

    if(applContextPtr == TMWDEFS_NULL)
    {
        log.error("%s Unable to initialise DNP3 Slave Protocol context.", __AT__);
        return;
    }
}


DNP3SlaveProtocol::~DNP3SlaveProtocol()
{
    // Stop the running Thread
    stopProtocol();

    // Delete all the channels/sessions and db objects
    deleteChannels();

    /* Close protocol application */
    if (applContextPtr != NULL)
    {
        tmwappl_closeApplication(applContextPtr, TMWDEFS_TRUE);
        applContextPtr = NULL;
    }
}

SCADAP_ERROR DNP3SlaveProtocol::addChannel(DNP3SlaveProtocolChannel &channel)
{
    DNP3SlaveChannels.push_back(&channel);

    return SCADAP_ERROR_NONE;
}


void DNP3SlaveProtocol::addSession(DNP3SlaveProtocolSession& session)
{
    sessionList.push_back(&session);
}


SCADAP_ERROR DNP3SlaveProtocol::deleteChannels()
{

    for (ChannelVector::iterator itChannel  = DNP3SlaveChannels.begin();
                                 itChannel != DNP3SlaveChannels.end();
                                 ++itChannel)
    {
        delete (*itChannel);
    }

    DNP3SlaveChannels.clear();

    return SCADAP_ERROR_NONE;
}


SCADAP_ERROR DNP3SlaveProtocol::startProtocol()
{
    SCADAP_ERROR res;

    log.info("Starting DNP3 Slave Protocols");

    // Loop over channels, opening them
    lu_uint32_t i = 0;
    for (ChannelVector::iterator itChannel = DNP3SlaveChannels.begin();
                                 itChannel != DNP3SlaveChannels.end();
                                 ++itChannel)
    {
        DBG_INFO("Opening DNP3 channel %u...\n ", ++i);

        /* Open DNP channel */
        /* Check sclChannelPtr to be not NULL; otherwise DO NOT launch the DNP protocol */
        res = (*itChannel)->openChannel(applContextPtr);
        if(res != SCADAP_ERROR_NONE)
        {
            log.error("Failure opening DNP3 channel %u: %i",
                        i,
                        res);
        }

    }

    /* Start protocol stack main loop thread */
    return (Thread::start() == THREAD_ERR_NONE)? SCADAP_ERROR_NONE : SCADAP_ERROR_NOT_INITIALIZED;
}


SCADAP_ERROR DNP3SlaveProtocol::stopProtocol()
{
    THREAD_ERR result;
    SCADAP_ERROR res;
    // Stop the Connection Manager
//    SCADAConnectionMan->stopConnectionManager();

    // Loop over channels, closing them
    lu_uint32_t i = 0;
    for (ChannelVector::iterator itChannel = DNP3SlaveChannels.begin();
                                 itChannel != DNP3SlaveChannels.end();
                                 ++itChannel)
    {
        DBG_INFO("Closing DNP3 channel %u...\n ", ++i);

        /* Close DNP channel */
        res = (*itChannel)->closeChannel();
        if(res != SCADAP_ERROR_NONE)
        {
            log.error("Failure closing DNP3 channel %u: %i", i, res);
        }
    }

    /* Wait protocol stack termination */
    result = Thread::stop();
    /* FIXME: pueyos_a - [DNPSCADA] Thread is failing to join, disabled for now */
    //result = Thread::join();

    /* TODO: pueyos_a - Log the error when destroying thread */

    return (result == THREAD_ERR_NONE)? SCADAP_ERROR_NONE : SCADAP_ERROR_THREAD;
}


SCADAP_ERROR DNP3SlaveProtocol::addBinaryEvent(EventLogManager* eventLogPtr,
                TMWSDNPDatabaseStr *dbPtr,
                PointData *pointDataPtr,
                lu_uint16_t dnpID)
{
    TMWSDNPBInputStr *dnpPointPtr;
    TimeManager::TimeStr g3Time;

    // Extract the DNP Point
    dnpPointPtr = &dbPtr->binaryInput.list[dnpID];
    pointDataPtr->getTime(g3Time);

    MasterLockingMutex lMutex(synchPipeLock);
    if ((!firstSynch) && (g3Time.badTime))
    {
        VPEventStr eventVP;
        eventVP.dnpID = dnpID;
        eventVP.eventCode = DNPDEFS_OBJ_2_BIN_CHNG_EVENTS;
        eventVP.flags = dnpPointPtr->value;
        eventVP.value = dnpPointPtr->value;
        eventVP.relaTime = g3Time.relatime;
        return writeEventToSession(eventVP,
                        reinterpret_cast<DNP3SlaveProtocolSession*>(dbPtr->DNP3ProtocolSessionPtr)
                        );
    }

    EventDef DNPEvent;
    TMWDTIME DNPTimeStamp;
    SLEventEntryStr event;
    CTEventDNP3PointStr eventDNP;

    // Create a TMW format time
    convertTime(&g3Time, &DNPTimeStamp);

    if(dnpPointPtr->eventLogStore)
    {
        // Create a local event
        eventDNP.pointType = EVENT_TYPE_DNP3SCADA_SINGLEBINARY;
        eventDNP.dnpPointID = dnpID;
        eventDNP.status = dnpPointPtr->value;    //flags are within DNP3 value
        eventDNP.value.valueDig = (lu_uint8_t)(*pointDataPtr);
        event.eventClass = eventDNP.EventClass;
        event.timestamp.sec = g3Time.time.tv_sec;
        event.timestamp.nsec = g3Time.time.tv_nsec;
        event.payload.eventDNP3 = eventDNP;
        eventLogPtr->logEvent(event);           //Log the Event in the event log
    }

    // Create a DNP Event
    DNPEvent.eventCode = DNPDEFS_OBJ_2_BIN_CHNG_EVENTS;
    DNPEvent.pSession = dbPtr->sclSessionPtr;
    DNPEvent.point = dnpID;
    DNPEvent.DataValue.BinaryValue = dnpPointPtr->value;

// TODO - SKA - Get the EventID from the PointData object
    DNPTimeStamp.lucyEventID = pointDataPtr->getEventID();
    DNPEvent.TimeStamp = DNPTimeStamp;

    return this->addEvent(&DNPEvent);
}

SCADAP_ERROR DNP3SlaveProtocol::addDBinaryEvent(EventLogManager* eventLogPtr, TMWSDNPDatabaseStr *dbPtr, PointData *pointDataPtr, lu_uint16_t dnpID)
{
    TMWSDNPDBInputStr *dnpPointPtr;
    TimeManager::TimeStr g3Time;

    // Extract the DNP Point
    dnpPointPtr = &dbPtr->dbinaryInput.list[dnpID];
    pointDataPtr->getTime(g3Time);

    MasterLockingMutex lMutex(synchPipeLock);
    if ((!firstSynch) && (g3Time.badTime))
    {
        VPEventStr eventVP;
        eventVP.dnpID = dnpID;
        eventVP.eventCode = DNPDEFS_OBJ_4_DBL_CHNG_EVENTS;
        eventVP.flags = dnpPointPtr->value;
        eventVP.value = dnpPointPtr->value;
        eventVP.relaTime = g3Time.relatime;
        return writeEventToSession(eventVP,
                        reinterpret_cast<DNP3SlaveProtocolSession*>(dbPtr->DNP3ProtocolSessionPtr)
                        );
    }

    EventDef DNPEvent;
    TMWDTIME DNPTimeStamp;
    SLEventEntryStr event;
    CTEventDNP3PointStr eventDNP;

    // Create a TMW format time
    convertTime(&g3Time, &DNPTimeStamp);

    if(dnpPointPtr->eventLogStore)
    {
        // Create a local event
        eventDNP.pointType = EVENT_TYPE_DNP3SCADA_DOUBLEBINARY;
        eventDNP.dnpPointID = dnpID;
        eventDNP.status = dnpPointPtr->value;    //flags are within DNP3 value
        eventDNP.value.valueDig = (lu_uint8_t)(*pointDataPtr);
        event.eventClass = eventDNP.EventClass;
        event.timestamp.sec = g3Time.time.tv_sec;
        event.timestamp.nsec = g3Time.time.tv_nsec;
        event.payload.eventDNP3 = eventDNP;
        eventLogPtr->logEvent(event);           //Log the Event in the event log
    }

    // Create a DNP Event
    DNPEvent.eventCode = DNPDEFS_OBJ_4_DBL_CHNG_EVENTS;
    DNPEvent.pSession = dbPtr->sclSessionPtr;
    DNPEvent.point = dnpID;
    DNPEvent.DataValue.BinaryValue = dnpPointPtr->value;

    // TODO - SKA - Get the EventID from the PointData object
    DNPTimeStamp.lucyEventID = pointDataPtr->getEventID();
    DNPEvent.TimeStamp = DNPTimeStamp;

    return this->addEvent(&DNPEvent);
}

SCADAP_ERROR DNP3SlaveProtocol::addAnalogueEvent(EventLogManager* eventLogPtr, TMWSDNPDatabaseStr *dbPtr, PointData *pointDataPtr, lu_uint16_t dnpID)
{
    TMWSDNPAInputStr *dnpPointPtr;
    TimeManager::TimeStr g3Time;

    // Extract the DNP Point
    dnpPointPtr = &dbPtr->analogueInput.list[dnpID];
    pointDataPtr->getTime(g3Time);

    MasterLockingMutex lMutex(synchPipeLock);
    if ((!firstSynch) && (g3Time.badTime))
    {
        VPEventStr eventVP;
        eventVP.dnpID = dnpID;
        eventVP.eventCode = DNPDEFS_OBJ_32_ANA_CHNG_EVENTS;
        eventVP.flags = dnpPointPtr->flags;
        eventVP.aValue = (lu_float32_t)(dnpPointPtr->value.value.fval);
        eventVP.valueType = (lu_uint8_t)dnpPointPtr->value.type;
        eventVP.relaTime = g3Time.relatime;
        return writeEventToSession(eventVP,
                        reinterpret_cast<DNP3SlaveProtocolSession*>(dbPtr->DNP3ProtocolSessionPtr)
                        );
    }

    EventDef DNPEvent;
    TMWDTIME DNPTimeStamp;
    SLEventEntryStr event;
    CTEventDNP3PointStr eventDNP;

    // Create a TMW format time
    convertTime(&g3Time, &DNPTimeStamp);

    if(dnpPointPtr->eventLogStore)
    {
        // Create a local event
        eventDNP.pointType = EVENT_TYPE_DNP3SCADA_ANALOGUE;
        eventDNP.dnpPointID = dnpID;
        eventDNP.status = dnpPointPtr->flags;
        eventDNP.value.valueAna = *pointDataPtr;
        event.eventClass = eventDNP.EventClass;
        event.timestamp.sec = g3Time.time.tv_sec;
        event.timestamp.nsec = g3Time.time.tv_nsec;
        event.payload.eventDNP3 = eventDNP;
        eventLogPtr->logEvent(event);   // Log the Event in the event log
    }

    // Create a TMW DNP Event
    DNPEvent.eventCode = DNPDEFS_OBJ_32_ANA_CHNG_EVENTS;
    DNPEvent.pSession = dbPtr->sclSessionPtr;
    DNPEvent.point = dnpID;
    DNPEvent.DataValue.AnalogueValue = dnpPointPtr->value;
    DNPEvent.flags = dnpPointPtr->flags;

    // TODO - SKA - Get the EventID from the PointData object
    DNPTimeStamp.lucyEventID = pointDataPtr->getEventID();
    DNPEvent.TimeStamp = DNPTimeStamp;

    return this->addEvent(&DNPEvent);
}

#if DNP3SLAVE_ANLGOUT_EVENT_SUPPORT
SCADAP_ERROR DNP3SlaveProtocol::addAnlgOutEvent(EventLogManager* eventLogPtr, TMWSDNPDatabaseStr *dbPtr, PointData *pointDataPtr, lu_uint16_t dnpID)
{
    TMWSDNPAOutputStr *dnpPointPtr;
    TimeManager::TimeStr g3Time;

    // Extract the DNP Point
    dnpPointPtr = &dbPtr->analogueOutput.list[dnpID];
    pointDataPtr->getTime(g3Time);

    MasterLockingMutex lMutex(synchPipeLock);
    if ((!firstSynch) && (g3Time.badTime))
    {
        VPEventStr eventVP;
        eventVP.dnpID = dnpID;
        eventVP.eventCode = DNPDEFS_OBJ_42_ANA_OUT_EVENTS;
        eventVP.flags = dnpPointPtr->flags;
        eventVP.aValue = (lu_float32_t)(dnpPointPtr->value.value.fval);
        eventVP.valueType = (lu_uint8_t)dnpPointPtr->value.type;
        eventVP.relaTime = g3Time.relatime;
        return writeEventToSession(eventVP,
                        reinterpret_cast<DNP3SlaveProtocolSession*>(dbPtr->DNP3ProtocolSessionPtr)
                        );
    }

    EventDef DNPEvent;
    TMWDTIME DNPTimeStamp;
    SLEventEntryStr event;
    CTEventDNP3PointStr eventDNP;

    // Create a TMW format time
    convertTime(&g3Time, &DNPTimeStamp);

    if(dnpPointPtr->eventLogStore)
    {
        // Create a local event
        eventDNP.pointType = EVENT_TYPE_DNP3SCADA_ANLGOUT;
        eventDNP.dnpPointID = dnpID;
        eventDNP.status = dnpPointPtr->flags;
        eventDNP.value.valueAna = *pointDataPtr;
        event.eventClass = eventDNP.EventClass;
        event.timestamp.sec = g3Time.time.tv_sec;
        event.timestamp.nsec = g3Time.time.tv_nsec;
        event.payload.eventDNP3 = eventDNP;
        eventLogPtr->logEvent(event);   // Log the Event in the event log
    }

    // Create a TMW DNP Event
    DNPEvent.eventCode = DNPDEFS_OBJ_42_ANA_OUT_EVENTS;
    DNPEvent.pSession = dbPtr->sclSessionPtr;
    DNPEvent.point = dnpID;
    DNPEvent.DataValue.AnalogueValue = dnpPointPtr->value;
    DNPEvent.flags = dnpPointPtr->flags;

    // TODO - SKA - Get the EventID from the PointData object
    DNPTimeStamp.lucyEventID = pointDataPtr->getEventID();
    DNPEvent.TimeStamp = DNPTimeStamp;

    return this->addEvent(&DNPEvent);
}
#endif
#if DNP3SLAVE_ANLGOUT_CMDEVENT_SUPPORT
SCADAP_ERROR DNP3SlaveProtocol::addAnlgOutCmdEvent(EventLogManager* eventLogPtr, TMWSDNPDatabaseStr *dbPtr, TMWSDNPAOutputStr *pointPtr, lu_uint16_t dnpID)
{
    TMWSDNPAOutputStr *dnpPointPtr;
    TimeManager::TimeStr g3Time;

    // Extract the DNP Point
    dnpPointPtr = &dbPtr->analogueOutput.list[dnpID];
    TimeManager::getInstance().getTime(g3Time);
    //pointDataPtr->getTime(g3Time);

    MasterLockingMutex lMutex(synchPipeLock);
    if ((!firstSynch) && (g3Time.badTime))
    {
        VPEventStr eventVP;
        eventVP.dnpID = dnpID;
        eventVP.eventCode = DNPDEFS_OBJ_43_ANA_CMD_EVENTS;
        eventVP.flags = dnpPointPtr->flags;
        eventVP.aValue = (lu_float32_t)(dnpPointPtr->value.value.fval);
        eventVP.valueType = (lu_uint8_t)dnpPointPtr->value.type;
        eventVP.relaTime = g3Time.relatime;
        return writeEventToSession(eventVP,
                        reinterpret_cast<DNP3SlaveProtocolSession*>(dbPtr->DNP3ProtocolSessionPtr)
                        );
    }

    EventDef DNPEvent;
    TMWDTIME DNPTimeStamp;
    SLEventEntryStr event;
    CTEventDNP3PointStr eventDNP;

    // Create a TMW format time
    convertTime(&g3Time, &DNPTimeStamp);

    if(dnpPointPtr->eventLogStore)
    {
        // Create a local event
        eventDNP.pointType = EVENT_TYPE_DNP3SCADA_ANLGOUT;
        eventDNP.dnpPointID = dnpID;
        eventDNP.status = dnpPointPtr->flags;
        eventDNP.value.valueAna = pointPtr->value.value.fval;
        //eventDNP.value.valueAna = *pointDataPtr;
        event.eventClass = eventDNP.EventClass;
        event.timestamp.sec = g3Time.time.tv_sec;
        event.timestamp.nsec = g3Time.time.tv_nsec;
        event.payload.eventDNP3 = eventDNP;
        eventLogPtr->logEvent(event);   // Log the Event in the event log
    }
    // Create a TMW DNP Event
    DNPEvent.eventCode = DNPDEFS_OBJ_43_ANA_CMD_EVENTS;
    DNPEvent.pSession = dbPtr->sclSessionPtr;
    DNPEvent.point = dnpID;
    DNPEvent.DataValue.AnalogueValue = dnpPointPtr->value;
    DNPEvent.flags = dnpPointPtr->flags;

    // TODO - SKA - Get the EventID from the PointData object
    DNPTimeStamp.lucyEventID = 0;//pointPtr->getEventID();//TODO event id?
    //DNPTimeStamp.lucyEventID = pointDataPtr->getEventID();
    DNPEvent.TimeStamp = DNPTimeStamp;

    return this->addEvent(&DNPEvent);
}
#endif

SCADAP_ERROR DNP3SlaveProtocol::addCounterEvent(EventLogManager* eventLogPtr, TMWSDNPDatabaseStr *dbPtr, PointData *pointDataPtr, lu_uint16_t dnpID)
{
    TMWSDNPCInputStr *dnpPointPtr;
    TimeManager::TimeStr g3Time;

    // Extract the DNP Point
    dnpPointPtr = &dbPtr->counterInput.list[dnpID];
    pointDataPtr->getTime(g3Time);

    MasterLockingMutex lMutex(synchPipeLock);

    /*Note: Rollover is set by the point itself: the fact that we are offering
     *      the counter value as 16-bit is independent: the user should configure
     *      it accordingly.
     *      Anyway, the library converts the value to 16 bit whe configured with
     *      16-bit variations, going back to 0 - even when the Virtual Point is
     *      configured with a non-zero Reset Value (the library does not know
     *      the Reset Value so it only rounds up the number with no rollover
     *      update as well). This might be OK since the user has to configure it
     *      properly for 16-bit.
     */
    TMWTYPES_ULONG value = (lu_uint32_t)(*pointDataPtr);

    if( (pointDataPtr->getEdge() == EDGE_TYPE_NEGATIVE) &&
        (dnpPointPtr->frozenFlag == TMWDEFS_FALSE)
        )
    {
        //Change due to timed event: frozen report discarded
        log.info("Frozen event on DNP3 point %u discarded", dnpID);
        return SCADAP_ERROR_NONE;
    }

    if ((!firstSynch) && (g3Time.badTime))
    {
        VPEventStr eventVP;
        eventVP.dnpID = dnpID;
        eventVP.eventCode = (pointDataPtr->getEdge() == EDGE_TYPE_POSITIVE)? DNPDEFS_OBJ_22_CNTR_EVENTS :  //Normal
                                                                             DNPDEFS_OBJ_23_FCTR_EVENTS;   //Frozen
        eventVP.flags = dnpPointPtr->flags;
        eventVP.value = value;
        eventVP.relaTime = g3Time.relatime;
        return writeEventToSession(eventVP,
                        reinterpret_cast<DNP3SlaveProtocolSession*>(dbPtr->DNP3ProtocolSessionPtr)
                        );
    }

    EventDef DNPEvent;
    TMWDTIME DNPTimeStamp;
    SLEventEntryStr event;
    CTEventDNP3PointStr eventDNP;

    // Create a TMW format time
    pointDataPtr->getTime(g3Time);
    convertTime(&g3Time, &DNPTimeStamp);

    if(dnpPointPtr->eventLogStore)
    {
        // Create a local event
        eventDNP.pointType = EVENT_TYPE_DNP3SCADA_COUNTER;
        eventDNP.dnpPointID = dnpID;
        eventDNP.status = dnpPointPtr->flags;
        eventDNP.value.valueDig = *pointDataPtr;
        event.eventClass = eventDNP.EventClass;
        event.timestamp.sec = g3Time.time.tv_sec;
        event.timestamp.nsec = g3Time.time.tv_nsec;
        event.payload.eventDNP3 = eventDNP;
        eventLogPtr->logEvent(event);
    }

    // Create a TMW DNP Event depending on the type of freezing/event
    DNPEvent.eventCode = (pointDataPtr->getEdge() == EDGE_TYPE_POSITIVE)? DNPDEFS_OBJ_22_CNTR_EVENTS :  //Normal
                                                                          DNPDEFS_OBJ_23_FCTR_EVENTS;   //Frozen
    DNPEvent.pSession = dbPtr->sclSessionPtr;
    DNPEvent.point = dnpID;
    DNPEvent.DataValue.CounterValue = value;
    DNPEvent.flags = dnpPointPtr->flags;

    // TODO - SKA - Get the EventID from the PointData object
    DNPTimeStamp.lucyEventID = pointDataPtr->getEventID();
    DNPEvent.TimeStamp = DNPTimeStamp;

    return this->addEvent(&DNPEvent);
}


SCADAP_ERROR DNP3SlaveProtocol::addEvent(EventDef *eventPtr)
{
    lu_int32_t ret = -1;

    if (eventPtr != NULL)
    {
        ret = eventPipe.writeP((lu_uint8_t *) eventPtr, (lu_uint32_t) sizeof(EventDef));
    }

    return (ret >= 0) ? SCADAP_ERROR_NONE : SCADAP_ERROR_EVENT_PIPE;
}



void DNP3SlaveProtocol::restart(void *tmwSesnPtr, lu_bool_t coldRestart)
{
    SDNPSESN           *SDNPSessionPtr;

    if (tmwSesnPtr == NULL)
    {
        return;
    }

    /* Extract the Slave Session Pointer */
    SDNPSessionPtr = (SDNPSESN *) tmwSesnPtr;

    // TODO - SKA - DNP3 TMW library 3.17 - Evaluate if we need the protection for Broadcast/Self Address here
    /*
     * Check if the session allows Controls using the Broadcast Address
     */
    if ((SDNPSessionPtr->lastRcvdRequest.isBroadcast   == TMWDEFS_TRUE) ||
        (SDNPSessionPtr->lastRcvdRequest.isSelfAddress == TMWDEFS_TRUE))
    {
        DBG_WARN("DNP3 Restart request made on Broadcast or Self Address - ignored");
        return;
    }


    /* Translate the TMW request into a G3 operation */
    if (mcmAppInstance != NULL)
    {
        if(coldRestart == LU_TRUE)
        {
            logger.warn("DNP3 Master requested RTU reboot");
            //use instance since we cannot use object in static method
            mcmAppInstance->reboot(IMCMApplication::RESTART_TYPE_DELAY);
        }
        else
        {
            logger.warn("DNP3 Master requested RTU warm restart");
            mcmAppInstance->restart(IMCMApplication::RESTART_TYPE_DELAY);
        }
    }
}

void DNP3SlaveProtocol::setDateTime(TMWDTIME *masterTime)
{
    if(masterTime == NULL)
    {
        return;
    }

    //Initial time for logging time difference
    timespec initial;
    clock_gettimespec(&initial);

    struct tm outTime;
    lu_bool_t synchronised = (masterTime->invalid == TMWDEFS_FALSE)? LU_TRUE : LU_FALSE;
    lu_uint32_t mseconds;
    // Convert TMW Time to tm
    convertTime(*masterTime, outTime, mseconds);

    // Set the time in the Time Manager
    TimeManager::TERROR res;
    res = TimeManager::getInstance().setTime(outTime, mseconds, synchronised, SYNC_SOURCE_SCADA);
    if(res == TimeManager::TERROR_TIMER)
    {
        perror("System Timer not available for synchronisation");
        return;
    }
    timespec_normalise(&initial);
    Logger::getLogger(SUBSYSTEM_ID_TEST).info("DNP3 req time set at mark %i.%09i", initial.tv_sec, initial.tv_nsec);
}

void *DNP3SlaveProtocol::binInGetPoint(TMWSDNPDatabaseStr *dbPtr, TMWTYPES_USHORT pointNum)
{
    void *ret = TMWDEFS_NULL;

    if (dbPtr == NULL)
    {
        return ret;
    }

    if ((pointNum < dbPtr->binaryInput.size) &&
        (dbPtr->binaryInput.list[pointNum].enabled))
    {
        ret = &dbPtr->binaryInput.list[pointNum];
    }

    return ret;
}

void *DNP3SlaveProtocol::binOutGetPoint(TMWSDNPDatabaseStr *dbPtr, TMWTYPES_USHORT pointNum)
{
    void *ret = TMWDEFS_NULL;

    if (dbPtr == NULL)
    {
        return ret;
    }

    if( (pointNum < dbPtr->binaryOutput.size) &&
        (dbPtr->binaryOutput.list[pointNum].enabled)
      )
    {
        ret = &dbPtr->binaryOutput.list[pointNum];
    }

    return ret;
}

TMWTYPES_BOOL DNP3SlaveProtocol::binOutGetStatus(TMWSDNPBOutputStr *pointPtr)
{
    TMWTYPES_BOOL ret = TMWDEFS_FALSE;

    if (pointPtr == NULL)
    {
        return ret;
    }

    /* Get the instance of the DNP3SlaveProtocolManager (this is a static method!) */
    DNP3SlaveProtocol* protocolInstance = (DNP3SlaveProtocol*)(pointPtr->callbacks->protocolManager);

    if (protocolInstance == NULL)
    {
        return ret;
    }

    /* Ask status to the Gemini3 DB */
    PointIdStr pointID(pointPtr->switchID, 0);
    PointDataUint8 data;
    GDB_ERROR res = protocolInstance->GDatabase.getOutputValue(pointID, &data);
    if(res == GDB_ERROR_NONE)
    {
        ret = (data.getQuality() == POINT_QUALITY_ON_LINE)? TMWDEFS_TRUE : TMWDEFS_FALSE;
    }
    return ret;
}

DNPDEFS_CROB_ST DNP3SlaveProtocol::binOutSelect(TMWSDNPBOutputStr *pointPtr,
                                                       TMWTYPES_UCHAR controlCode,
                                                       TMWTYPES_UCHAR count)
{
    TMWSESN            *sessionPtr     = *(pointPtr->sclSessionPtrPtr);
    SDNPSESN           *SDNPSessionPtr;
    TMWSDNPDatabaseStr *db;
    SwitchLogicOperation operation;

    if (sessionPtr == NULL)
    {
        return DNPDEFS_CROB_ST_NOT_SUPPORTED;
    }

    /* Extract the Slave Session Pointer */
    SDNPSessionPtr = (SDNPSESN *)sessionPtr;
    if (SDNPSessionPtr == NULL)
    {
        return DNPDEFS_CROB_ST_NOT_SUPPORTED;
    }

    /* Extract the database pointer */
    db = (TMWSDNPDatabaseStr *) SDNPSessionPtr->pDbHandle;
    if(db == NULL)
    {
        return DNPDEFS_CROB_ST_NOT_SUPPORTED;
    }

    // TODO - SKA - DNP3 TMW library 3.17 - Evaluate if we need the protection for Broadcast/Self Address here
    /*
     * Check if the session allows Controls using the Broadcast Address
     */
    if (((SDNPSessionPtr->lastRcvdRequest.isBroadcast   == TMWDEFS_TRUE) ||
         (SDNPSessionPtr->lastRcvdRequest.isSelfAddress == TMWDEFS_TRUE)) &&
         (db->allowControlOnBrdCstAddr == TMWDEFS_FALSE))
    {
        return DNPDEFS_CROB_ST_NOT_AUTHORIZED;
    }

    /* Control logic doesn't support select.
     * Just check that the operation is supported */
    if( (((controlCode & SDNPDATA_CROB_CTRL_SUPPORTED) == 0) || (count > 1)) ||
        (pointPtr->acceptSBO == TMWDEFS_FALSE)  //Refuse Select when SBO
        )
    {
        /* Operation not supported */
        return DNPDEFS_CROB_ST_NOT_SUPPORTED;
    }
    if (count == 0)
    {
        /* Simulate a successful operation */
        return DNPDEFS_CROB_ST_SUCCESS;
    }

    return(DNPDEFS_CROB_ST_SUCCESS);
}


DNPDEFS_CROB_ST DNP3SlaveProtocol::binOutOperate(TMWSDNPBOutputStr *pointPtr,
                                                        TMWTYPES_UCHAR controlCode,
                                                        TMWTYPES_UCHAR count)
{
    SwitchLogicOperation  operation;
    TMWSESN              *sessionPtr     = *(pointPtr->sclSessionPtrPtr);
    SDNPSESN             *SDNPSessionPtr;
    TMWSDNPDatabaseStr   *db;

    if (sessionPtr == NULL)
    {
        return DNPDEFS_CROB_ST_NOT_SUPPORTED;
    }

    /* Extract the Slave Session Pointer */
    SDNPSessionPtr = (SDNPSESN *)sessionPtr;
    if (SDNPSessionPtr == NULL)
    {
        return DNPDEFS_CROB_ST_NOT_SUPPORTED;
    }

    /* Extract the database pointer */
    db = (TMWSDNPDatabaseStr *) SDNPSessionPtr->pDbHandle;
    if(db == NULL)
    {
        return DNPDEFS_CROB_ST_NOT_SUPPORTED;
    }

    // TODO - SKA - DNP3 TMW library 3.17 - Evaluate if we need the protection for Broadcast/Self Address here
    /*
     * Check if the session allows Controls using the Broadcast Address
     */
    if (((SDNPSessionPtr->lastRcvdRequest.isBroadcast   == TMWDEFS_TRUE) ||
         (SDNPSessionPtr->lastRcvdRequest.isSelfAddress == TMWDEFS_TRUE)) &&
         (db->allowControlOnBrdCstAddr == TMWDEFS_FALSE))
    {
        return DNPDEFS_CROB_ST_NOT_AUTHORIZED;
    }

    /* Get the instance of the DNP3SlaveProtocolManager (this is a static method!) */
    DNP3SlaveProtocol *protocolInstance = (DNP3SlaveProtocol *) pointPtr->callbacks->protocolManager;

    if (protocolInstance == NULL)
    {
        return DNPDEFS_CROB_ST_NOT_SUPPORTED;
    }

    /* Check Direct Operate and Select-Before-Operate availability */
    switch(SDNPSessionPtr->lastRcvdRequest.fc)
    {
        case DNPDEFS_FC_OPERATE:
            if(pointPtr->acceptSBO == TMWDEFS_FALSE)
            {
                return DNPDEFS_CROB_ST_NOT_SUPPORTED;
            }
            break;
        case DNPDEFS_FC_DIRECT_OP:
        case DNPDEFS_FC_DIRECT_OP_NOACK:
            if(pointPtr->acceptDO == TMWDEFS_FALSE)
            {
                return DNPDEFS_CROB_ST_NOT_SUPPORTED;
            }
            break;
        default:
            break;
    }

    /* Decode counter */
    if (count > 1)
    {
        /* Operation not supported */
        return DNPDEFS_CROB_ST_NOT_SUPPORTED;
    }
    if (count == 0)
    {
        /* Simulate a successful operation */
        return DNPDEFS_CROB_ST_SUCCESS;
    }

    /* Translate the TMW request into a G3 operation */
    operation = binOutFillOperation(*SDNPSessionPtr, *db, controlCode);
    if(operation.operation == SWITCH_OPERATION_LAST)
    {
        return DNPDEFS_CROB_ST_NOT_SUPPORTED;   //Operation not supported
    }

    /* Start the operation */
    GDB_ERROR opRet;
    opRet = protocolInstance->GDatabase.startOperation(pointPtr->switchID, operation);
    return translateCROB(opRet);
}


void* DNP3SlaveProtocol::anlgOutGetPoint(TMWSDNPDatabaseStr *dbPtr, TMWTYPES_USHORT pointNum)
{

    void *ret = TMWDEFS_NULL;

    if (dbPtr == NULL)
    {
        return ret;
    }

    if( (pointNum < dbPtr->analogueOutput.size) &&
        (dbPtr->analogueOutput.list[pointNum].enabled)
      )
    {
        ret = &dbPtr->analogueOutput.list[pointNum];
    }

    return ret;
}

DNPDEFS_CTLSTAT DNP3SlaveProtocol::anlgOutSelect(TMWSDNPAOutputStr *pointPtr,
                                                TMWTYPES_ANALOG_VALUE *pValue)
{
    LU_UNUSED(pValue);

    TMWSESN            *sessionPtr     = *(pointPtr->sclSessionPtrPtr);
    SDNPSESN           *SDNPSessionPtr;
    TMWSDNPDatabaseStr *db;


    if (sessionPtr == NULL)
    {
        return DNPDEFS_CTLSTAT_NOT_SUPPORTED;
    }

    /* Extract the Slave Session Pointer */
    SDNPSessionPtr = (SDNPSESN *)sessionPtr;

    if (SDNPSessionPtr == NULL)
    {
        return DNPDEFS_CTLSTAT_NOT_SUPPORTED;
    }

    /* Extract the database pointer */
    db = (TMWSDNPDatabaseStr *) SDNPSessionPtr->pDbHandle;

    /*
     * Check if the session allows Controls using the Broadcast Address
     */
    if (((SDNPSessionPtr->lastRcvdRequest.isBroadcast   == TMWDEFS_TRUE) ||
         (SDNPSessionPtr->lastRcvdRequest.isSelfAddress == TMWDEFS_TRUE)) &&
         (db->allowControlOnBrdCstAddr == TMWDEFS_FALSE))
    {
        return DNPDEFS_CTLSTAT_NOT_AUTHORIZED;
    }

    //TODO Do select @pei

    return(DNPDEFS_CTLSTAT_SUCCESS);
}

DNPDEFS_CTLSTAT DNP3SlaveProtocol::anlgOutOperate(TMWSDNPAOutputStr *pointPtr,
                                                TMWTYPES_ANALOG_VALUE *pValue)
{
    DNPDEFS_CROB_ST       ret;
    AnalogueOutputOperation          outputVal;
    TMWSESN              *sessionPtr     = *(pointPtr->sclSessionPtrPtr);
    SDNPSESN             *SDNPSessionPtr;
    TMWSDNPDatabaseStr   *db;


    if (sessionPtr == NULL)
    {
        return DNPDEFS_CTLSTAT_NOT_SUPPORTED;
    }

    /* Extract the Slave Session Pointer */
    SDNPSessionPtr = (SDNPSESN *)sessionPtr;

    if (SDNPSessionPtr == NULL)
    {
        return DNPDEFS_CTLSTAT_NOT_SUPPORTED;
    }

    /* Extract the database pointer */
    db = (TMWSDNPDatabaseStr *) SDNPSessionPtr->pDbHandle;

    // TODO - SKA - DNP3 TMW library 3.17 - Evaluate if we need the protection for Broadcast/Self Address here
    /*
     * Check if the session allows Controls using the Broadcast Address
     */
    if (((SDNPSessionPtr->lastRcvdRequest.isBroadcast   == TMWDEFS_TRUE) ||
         (SDNPSessionPtr->lastRcvdRequest.isSelfAddress == TMWDEFS_TRUE)) &&
         (db->allowControlOnBrdCstAddr == TMWDEFS_FALSE))
    {
        return DNPDEFS_CTLSTAT_NOT_AUTHORIZED;
    }

    if (pValue == NULL)
    {
        return DNPDEFS_CTLSTAT_NOT_SUPPORTED;
    }

    /* Update G3 database */
    outputVal.value = dnputil_getAnalogValueFloat(pValue, NULL); //TODO review!
    outputVal.local = LU_FALSE;

    DBG_INFO("anlgOutOperate value, raw:%f %f",pValue->value.fval, outputVal.value);

    /* Get the instance of the DNP3SlaveProtocolManager (this is a static method!) */
    DNP3SlaveProtocol *protocolInstance = (DNP3SlaveProtocol *) pointPtr->callbacks->protocolManager;

    if (protocolInstance == NULL)
    {
        return DNPDEFS_CTLSTAT_NOT_SUPPORTED;
    }

    /* Start the operation */
    GDB_ERROR operateRet = protocolInstance->GDatabase.writeAnalogOperation(pointPtr->logicGroup, outputVal);

    /* Translate return code */
    switch (operateRet)
    {
        case GDB_ERROR_NONE:
            ret = DNPDEFS_CTLSTAT_SUCCESS;
        break;

        case GDB_ERROR_ALREADY_ACTIVE:
            ret = DNPDEFS_CTLSTAT_ALREADY_ACTIVE;
        break;

        case GDB_ERROR_AUTHORITY:
            ret = DNPDEFS_CTLSTAT_NOT_AUTHORIZED;
        break;

        case GDB_ERROR_POSITION:
            ret = DNPDEFS_CTLSTAT_SUCCESS;
        break;

        case GDB_ERROR_LOCAL_REMOTE:
            ret = DNPDEFS_CTLSTAT_LOCAL;
        break;

        case GDB_ERROR_INHIBIT:
            ret = DNPDEFS_CTLSTAT_AUTO_INHIBIT;
        break;

        default:
            ret = DNPDEFS_CTLSTAT_UNDEFINED;
        break;
    }

    if(operateRet == GDB_ERROR_NONE)
    {
        /* Update protocol point value*/
        // pointPtr->value = *pValue; // Protocol point value has been updated by the observer of database

        // pointPtr->flags &= DNPDEFS_DBAS_FLAG_ //TODO need update flags ?

    	/* TODO: pueyos_a Generate command event*/
#if DNP3SLAVE_ANLGOUT_CMDEVENT_SUPPORT
    	DNP3SlaveProtocol *DNP3SlavePMObj = (DNP3SlaveProtocol *) db->callbacks.protocolManager;
		DNP3SlavePMObj->addAnlgOutCmdEvent(EventLogManager::getInstance(), db, pointPtr, pointPtr->dnp3Id);// where to get dnp3 id
#endif
    }

    return ret;
}

void* DNP3SlaveProtocol::anlgInGetPoint(TMWSDNPDatabaseStr *dbPtr, TMWTYPES_USHORT pointNum)
{
    void *ret = TMWDEFS_NULL;

    if (dbPtr == NULL)
    {
        return ret;
    }

    if ((pointNum < dbPtr->analogueInput.size) &&
        (dbPtr->analogueInput.list[pointNum].enabled))
    {
        ret = &dbPtr->analogueInput.list[pointNum];
    }

    return ret;
}

void* DNP3SlaveProtocol::dblInGetPoint(TMWSDNPDatabaseStr *dbPtr, TMWTYPES_USHORT pointNum)
{
    void *ret = TMWDEFS_NULL;

    if (dbPtr == NULL)
    {
        return ret;
    }

    if ((pointNum < dbPtr->dbinaryInput.size) &&
        (dbPtr->dbinaryInput.list[pointNum].enabled))
    {
        ret = &dbPtr->dbinaryInput.list[pointNum];
    }

    return ret;
}

void* DNP3SlaveProtocol::ctrInGetPoint(TMWSDNPDatabaseStr *dbPtr, TMWTYPES_USHORT pointNum)
{
    void *ret = TMWDEFS_NULL;

    if (dbPtr == NULL)
    {
        return ret;
    }

    if( (pointNum < dbPtr->counterInput.size) && (dbPtr->counterInput.list[pointNum].enabled) )
    {
        ret = &dbPtr->counterInput.list[pointNum];
    }

    return ret;
}

TMWTYPES_BOOL DNP3SlaveProtocol::ctrInFreeze(TMWSDNPCInputStr* pointPtr, TMWTYPES_BOOL freezeAndClear)
{
    TMWSESN              *sessionPtr;
    SDNPSESN             *SDNPSessionPtr;
    TMWSDNPDatabaseStr   *db;

    if(pointPtr->sclSessionPtrPtr == NULL)
    {
        return TMWDEFS_FALSE;
    }
    sessionPtr = *(pointPtr->sclSessionPtrPtr);
    if (sessionPtr == NULL)
    {
        return TMWDEFS_FALSE;
    }

    /* Extract the Slave Session Pointer */
    SDNPSessionPtr = (SDNPSESN *)sessionPtr;

    if (SDNPSessionPtr == NULL)
    {
        return TMWDEFS_FALSE;
    }

    /* Extract the database pointer */
    db = (TMWSDNPDatabaseStr *) SDNPSessionPtr->pDbHandle;

    if(db == NULL)
    {
        return TMWDEFS_FALSE;
    }

    /* Get the instance of the DNP3SlaveProtocolManager (this is a static method!) */
    DNP3SlaveProtocol* protocolInstance = (DNP3SlaveProtocol*)(pointPtr->callbacks->protocolManager);

    if (protocolInstance == NULL)
    {
        return TMWDEFS_FALSE;
    }

    /* Start the operation */
    PointIdStr pointID(pointPtr->vPointRef.group, pointPtr->vPointRef.ID);
    GDB_ERROR operateRet = protocolInstance->GDatabase.freeze(pointID, freezeAndClear);

    DBG_INFO("ctrInFreeze counter %s%s%s",
             pointID.toString().c_str(),
             (freezeAndClear == TMWDEFS_TRUE)? " with clear" : "",
             (operateRet != GDB_ERROR_NONE)? " Failed": "");

    return (operateRet == GDB_ERROR_NONE)? TMWDEFS_TRUE : TMWDEFS_FALSE;
}


lu_bool_t DNP3SlaveProtocol::deleteEventID(lu_uint32_t eventID, void *protocolSessionPtr)
{
    DNP3SlaveProtocolSession *sessionPtr = NULL;

    if (protocolSessionPtr == NULL)
    {
        return false;
    }

    sessionPtr = (DNP3SlaveProtocolSession *) protocolSessionPtr;

    DBG_INFO("Delete Event ID [%d] for session [%d]", eventID, sessionPtr->getSessionID());

    // TODO - SKA - Tell the EventManager (?) to remove the event for this session
    return true;
}

lu_bool_t DNP3SlaveProtocol::getTimeSyncStatus()
{
    TimeManager& timeManager = TimeManager::getInstance();

    return timeManager.getSyncStatus();
}

lu_bool_t DNP3SlaveProtocol::getLocalState(void *dbPtr)
{
    TMWSDNPDatabaseStr *tmwDbPtr = NULL;
    OLR_STATE          olr;

    if (dbPtr == NULL)
    {
        return LU_TRUE;
    }

    tmwDbPtr = (TMWSDNPDatabaseStr*) dbPtr;

    DNP3SlaveProtocol* DNP3SlavePMObj = (DNP3SlaveProtocol*) tmwDbPtr->callbacks.protocolManager;

    DNP3SlavePMObj->GDatabase.getLocalRemote(olr);

    // If OLR is OFF/LOCAL or INVALID then report LOCAL, else REMOTE
    if (olr == OLR_STATE_REMOTE)
    {
        return LU_FALSE;
    }

    return LU_TRUE;
}


void DNP3SlaveBIObserver::update(PointIdStr pointID, PointData *pointDataPtr)
{
    LU_UNUSED(pointID);

    bool inChatter = false;
    TMWSDNPBInputStr *dnpPointPtr;  //Value+Flags to be updated
    DNP3SlaveProtocolSession *sessionObjPtr = (DNP3SlaveProtocolSession *) db->DNP3ProtocolSessionPtr;

    /* Get stored data */
    dnpPointPtr = &db->binaryInput.list[dnpID];
    const TMWTYPES_UCHAR VALUEMASK = DNPDEFS_DBAS_FLAG_BINARY_ON | DNPDEFS_DBAS_FLAG_BINARY_OFF;
    TMWTYPES_UCHAR lastFlags = dnpPointPtr->value & (~VALUEMASK);  //Store previous flag state

    /* Update value */
    if((lu_uint8_t)(*pointDataPtr) == 0)
    {
        dnpPointPtr->value &= ~DNPDEFS_DBAS_FLAG_BINARY_ON;
        dnpPointPtr->value |= DNPDEFS_DBAS_FLAG_BINARY_OFF;
    }
    else
    {
        dnpPointPtr->value &= ~DNPDEFS_DBAS_FLAG_BINARY_OFF;
        dnpPointPtr->value |= DNPDEFS_DBAS_FLAG_BINARY_ON;
    }

    /* Update flags */
    bool chatter = pointDataPtr->getChatterFlag();
    LU_SETALLMASKBITS(dnpPointPtr->value, DNPDEFS_DBAS_FLAG_RESTART, pointDataPtr->getInitialFlag() );
    LU_SETALLMASKBITS(dnpPointPtr->value, DNPDEFS_DBAS_FLAG_ON_LINE,
                      (pointDataPtr->getQuality() == POINT_QUALITY_ON_LINE)? 1 : 0
                      );
    LU_SETALLMASKBITS(dnpPointPtr->value, DNPDEFS_DBAS_FLAG_CHATTER, chatter);
    TMWTYPES_UCHAR newFlags = dnpPointPtr->value & (~VALUEMASK);  //New flag state

    /* FIXME: pueyos_a - chatter change check made redundant from new point behaviour */
    /* Check chatter change in order to inhibit events to be generated when in chatter.
     * This is not done at the point level.
     */
    if( (chatter) && (lastFlags == newFlags) )
    {
        inChatter = true; //Already in Chatter: do NOT generate event unless flag change
    }

    /* Generate event when not in chatter OR when flags change */
    if( (db->sclSessionPtr != NULL) && (db->callbacks.protocolManager != NULL) &&
        ( (dnpPointPtr->eventClass & TMWDEFS_CLASS_MASK_ALL) != TMWDEFS_CLASS_MASK_NONE) &&
        ( (!inChatter) || (lastFlags != newFlags) ) &&
          ( (dnpPointPtr->eventOnlyWhenOnline == LU_FALSE) ||
            ( (dnpPointPtr->eventOnlyWhenOnline == LU_TRUE) && (sessionObjPtr->getOnline() == LU_TRUE) )
          )
        && !(pointDataPtr->getInitialFlag())             //Not the initial value
        )
    {
        /* Generate Event: Add a Binary Event to the DNP Event Pipe */
        DNP3SlaveProtocol* DNP3SlavePMObj = (DNP3SlaveProtocol*) db->callbacks.protocolManager;
        DNP3SlavePMObj->addBinaryEvent(eventLog, db, pointDataPtr, dnpID);
    }
}


void DNP3SlaveDBIObserver::update(PointIdStr pointID, PointData *pointDataPtr)
{
    LU_UNUSED(pointID);

    bool inChatter = false;
    TMWSDNPDBInputStr *dnpPointPtr;  //Value+Flags to be updated
    DNP3SlaveProtocolSession *sessionObjPtr = (DNP3SlaveProtocolSession *) db->DNP3ProtocolSessionPtr;

    /* Get stored data */
    dnpPointPtr = &db->dbinaryInput.list[dnpID];
    TMWTYPES_UCHAR value = dnpPointPtr->value;
    const TMWTYPES_UCHAR VALUEMASK = DNPDEFS_DBAS_FLAG_DOUBLE_INDET |
                                     DNPDEFS_DBAS_FLAG_DOUBLE_OFF |
                                     DNPDEFS_DBAS_FLAG_DOUBLE_ON |
                                     DNPDEFS_DBAS_FLAG_DOUBLE_INTER;
    TMWTYPES_UCHAR lastFlags = value & (~VALUEMASK);  //Store previous flag state

    /* Update value */
    value &= ~VALUEMASK;    //Clear value field in order to set only the bits needed
    switch((lu_uint8_t)(*pointDataPtr))
    {
        case 0: /* Intermediate */
            value |= DNPDEFS_DBAS_FLAG_DOUBLE_INTER;
        break;

        case 1: /* Off */
            value |= DNPDEFS_DBAS_FLAG_DOUBLE_OFF;
        break;

        case 2: /* On */
            value |= DNPDEFS_DBAS_FLAG_DOUBLE_ON;
        break;

        case 3: /* Indeterminate */
            value |= DNPDEFS_DBAS_FLAG_DOUBLE_INDET;
        break;

        default:
            return; //invalid value: do not generate any event
        break;
    }

    /* Update flags */
    lu_bool_t chatter = pointDataPtr->getChatterFlag();
    LU_SETALLMASKBITS(value, DNPDEFS_DBAS_FLAG_RESTART, pointDataPtr->getInitialFlag() );
    LU_SETALLMASKBITS(value, DNPDEFS_DBAS_FLAG_ON_LINE,
                      (pointDataPtr->getQuality() == POINT_QUALITY_ON_LINE)? 1 : 0
                      );
    LU_SETALLMASKBITS(value, DNPDEFS_DBAS_FLAG_CHATTER, chatter);
    TMWTYPES_UCHAR newFlags = value & (~VALUEMASK);  //New flag state

    /* Save new value */
    dnpPointPtr->value = value;

    /* Check chatter change in order to inhibit events to be generated when in chatter.
     * This is not done at the point level.
     */
    if( (chatter == LU_TRUE) && (lastFlags == newFlags) )
    {
        inChatter = true; //Already in Chatter: do NOT generate event unless flag change
    }

    /* Generate event when not in chatter OR when flags change */
    if( (db->sclSessionPtr != NULL) && (db->callbacks.protocolManager != NULL) &&
        ( (dnpPointPtr->eventClass & TMWDEFS_CLASS_MASK_ALL) != TMWDEFS_CLASS_MASK_NONE) &&
        ( (!inChatter) || (lastFlags != newFlags) ) &&
        ( (dnpPointPtr->eventOnlyWhenOnline == LU_FALSE) ||
          ( (dnpPointPtr->eventOnlyWhenOnline == LU_TRUE) && (sessionObjPtr->getOnline() == LU_TRUE) )
        )
        && !(pointDataPtr->getInitialFlag())             //Not the initial value
      )
    {
        /* Generate Event: Add a Double Binary Event to the DNP Event Pipe */
        DNP3SlaveProtocol* DNP3SlavePMObj = (DNP3SlaveProtocol*) db->callbacks.protocolManager;
        DNP3SlavePMObj->addDBinaryEvent(eventLog, db, pointDataPtr, dnpID);
    }
}


void DNP3SlaveAIObserver::update(PointIdStr pointID, PointData *pointDataPtr)
{
    if((db->sclSessionPtr == NULL) || (db->callbacks.protocolManager == NULL))
    {
        return; //Nothing to do
    }

    bool isCounter = false;
    DNP3SlaveProtocol *DNP3SProtocol = (DNP3SlaveProtocol *)(db->callbacks.protocolManager);
    switch(DNP3SProtocol->GDatabase.getPointType(pointID))
    {
        case POINT_TYPE_ANALOGUE:
            break;  //OK
        case POINT_TYPE_COUNTER:
            isCounter = true;
            break;
        default:
            return;
    }

    TMWSDNPAInputStr* dnpPointPtr;
    DNP3SlaveProtocolSession* sessionObjPtr = (DNP3SlaveProtocolSession *) db->DNP3ProtocolSessionPtr;

    dnpPointPtr = &db->analogueInput.list[dnpID];

    /* update database */
    dnpPointPtr->value.value.fval = (lu_float32_t)(*pointDataPtr);
    TMWTYPES_UCHAR lastFlags = dnpPointPtr->flags;  //Store previous flag state

    /* Update flags */
    LU_SETALLMASKBITS(dnpPointPtr->flags, DNPDEFS_DBAS_FLAG_RESTART, pointDataPtr->getInitialFlag() );
    LU_SETALLMASKBITS(dnpPointPtr->flags,
                      DNPDEFS_DBAS_FLAG_ON_LINE,
                      (pointDataPtr->getQuality() == POINT_QUALITY_ON_LINE)? 1 : 0
                      );
    if(!isCounter)
    {
        LU_SETALLMASKBITS( dnpPointPtr->flags, DNPDEFS_DBAS_FLAG_OVER_RANGE, pointDataPtr->getOverflowFlag() );
    }
    LU_SETALLMASKBITS( dnpPointPtr->flags, DNPDEFS_DBAS_FLAG_REFERENCE_CHK, pointDataPtr->getInvalidFlag() );

    /* Report points to Master only when flag changes, or filter says it */
    if( (db->sclSessionPtr != NULL) && (db->callbacks.protocolManager != NULL) &&
        ( (dnpPointPtr->eventClass & TMWDEFS_CLASS_MASK_ALL) != TMWDEFS_CLASS_MASK_NONE) &&
        (   (lastFlags != dnpPointPtr->flags) ||    //Change in flags
            isCounter ||                            //Source is counter
            ( ((!isCounter) &&                      //Source is analogue and after filter
              (pointDataPtr->getAFilterStatus() == AFILTER_RESULT_OUT_LIMIT_THR)) )
            ) &&
            ( (dnpPointPtr->eventOnlyWhenOnline == LU_FALSE) ||     //Always event
              ( (dnpPointPtr->eventOnlyWhenOnline == LU_TRUE) &&    //Event when online
                (sessionObjPtr->getOnline() == LU_TRUE) )
            )
        && !(pointDataPtr->getInitialFlag())             //Not the initial value
      )
    {
        /* Generate Event: Add a Binary Event to the DNP Event Pipe */
        DNP3SlaveProtocol *DNP3SlavePMObj = (DNP3SlaveProtocol *) db->callbacks.protocolManager;
        DNP3SlavePMObj->addAnalogueEvent(eventLog, db, pointDataPtr, dnpID);
    }
}


void DNP3SlaveAOObserver::update(PointIdStr pointID, PointData *pointDataPtr)
{
    LU_UNUSED(pointID);

    TMWSDNPAOutputStr *dnpPointPtr;
    DNP3SlaveProtocolSession *sessionObjPtr = (DNP3SlaveProtocolSession *) db->DNP3ProtocolSessionPtr;

    /* update database */
    dnpPointPtr = &db->analogueOutput.list[dnpID];
    dnpPointPtr->value.value.fval = (lu_float32_t)(*pointDataPtr);
    TMWTYPES_UCHAR lastFlags = dnpPointPtr->flags;  //Store previous flag state

    /* Update flags */
    LU_SETALLMASKBITS(dnpPointPtr->flags, DNPDEFS_DBAS_FLAG_RESTART, pointDataPtr->getInitialFlag() );
    LU_SETALLMASKBITS(dnpPointPtr->flags,
                      DNPDEFS_DBAS_FLAG_ON_LINE,
                      (pointDataPtr->getQuality() == POINT_QUALITY_ON_LINE)? 1 : 0
                      );
    LU_SETALLMASKBITS( dnpPointPtr->flags, DNPDEFS_DBAS_FLAG_OVER_RANGE, pointDataPtr->getOverflowFlag() );
    LU_SETALLMASKBITS( dnpPointPtr->flags, DNPDEFS_DBAS_FLAG_REFERENCE_CHK, pointDataPtr->getInvalidFlag() );

#if DNP3SLAVE_ANLGOUT_EVENT_SUPPORT
    /* Report points to Master only when flag changes, or filter says it */
    if( (db->sclSessionPtr != NULL) && (db->callbacks.protocolManager != NULL) &&
        (   (lastFlags != dnpPointPtr->flags) ||
            (pointDataPtr->getAFilterStatus() == AFILTER_RESULT_OUT_LIMIT_THR) ) &&
            ( (dnpPointPtr->eventOnlyWhenOnline == LU_FALSE) ||
              ( (dnpPointPtr->eventOnlyWhenOnline == LU_TRUE) && (sessionObjPtr->getOnline() == LU_TRUE) )
            )
      )
    {
        /* Generate Event: Add a Binary Event to the DNP Event Pipe */
        DNP3SlaveProtocol* DNP3SlavePMObj = (DNP3SlaveProtocol*) db->callbacks.protocolManager;
        DNP3SlavePMObj->addAnlgOutEvent(eventLog, db, pointDataPtr, dnpID);
    }
#endif
}


void DNP3SlaveCounterObserver::update(PointIdStr pointID, PointData *pointDataPtr)
{
    LU_UNUSED(pointID);

    TMWSDNPCInputStr *dnpPointPtr;
    DNP3SlaveProtocolSession *sessionObjPtr = (DNP3SlaveProtocolSession *) db->DNP3ProtocolSessionPtr;

    /* update database */
    dnpPointPtr = &db->counterInput.list[dnpID];

    if(pointDataPtr->getEdge() == EDGE_TYPE_POSITIVE)
    {
        //Normal counting event
        dnpPointPtr->value = (lu_uint32_t)(*pointDataPtr);
    }
    else
    {
        //Change coming from frozen event
        dnpPointPtr->frzValue = (lu_uint32_t)(*pointDataPtr);
    }

    /* Update flags */
    LU_SETALLMASKBITS(dnpPointPtr->flags, DNPDEFS_DBAS_FLAG_RESTART, pointDataPtr->getInitialFlag() );
    LU_SETALLMASKBITS(dnpPointPtr->flags,
                      DNPDEFS_DBAS_FLAG_ON_LINE,
                      (pointDataPtr->getQuality() == POINT_QUALITY_ON_LINE)? 1 : 0
                      );

    /* set DISCONTINUITY DNP Flag based in invalid/restart PointData flag */
    LU_SETALLMASKBITS(dnpPointPtr->flags,
                      DNPDEFS_DBAS_FLAG_DISCONTINUITY,
                      pointDataPtr->getInitialFlag() || pointDataPtr->getInvalidFlag()
                      );

    /* Depending on the DNP point config, set DNP's ROLLOVER flag from overflow */
    TMWTYPES_UCHAR flagMask = 0;
    flagMask = (dnpPointPtr->useRolloverFlag)? DNPDEFS_DBAS_FLAG_CNTR_ROLLOVER : 0;
    LU_SETALLMASKBITS( dnpPointPtr->flags, flagMask, pointDataPtr->getOverflowFlag() );

    /* Report points to Master */
    if(db->sclSessionPtr != NULL)
    {
        // Add a Counter Event to the DNP Event Pipe
        if ( (db->callbacks.protocolManager != NULL) &&
               ( (dnpPointPtr->eventClass & TMWDEFS_CLASS_MASK_ALL) != TMWDEFS_CLASS_MASK_NONE) &&
               ( (dnpPointPtr->eventOnlyWhenOnline == LU_FALSE) ||
                 ( (dnpPointPtr->eventOnlyWhenOnline == LU_TRUE) && (sessionObjPtr->getOnline() == LU_TRUE) )
               )
               && !(pointDataPtr->getInitialFlag())             //Not the initial value
           )
        {
            DNP3SlaveProtocol *DNP3SlavePMObj = (DNP3SlaveProtocol *) db->callbacks.protocolManager;
            DNP3SlavePMObj->addCounterEvent(eventLog, db, pointDataPtr, dnpID);
        }
    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
void DNP3SlaveProtocol::threadBody()
{
    const lu_char_t* FTITLE = "DNP3SlaveProtocol::threadBody:";
    const struct timeval eventReadTout = { 0, PROTOCOL_POOL_DELAY_US };

    lu_int32_t ret;

    /* Protocol manager main loop */

    log.info("%s DNP3 Slave Protocol Running", FTITLE);

    while(isRunning() == LU_TRUE)
    {
        struct timeval eventReadTimeout = eventReadTout;

        tickEvent();

        tmwpltmr_checkTimer();

        if(isInterrupting() == LU_TRUE)
            continue;   //exit thread;

        tmwappl_checkForInput(applContextPtr);

        ret = 0;

        // Keep reading Events from the pipe until the timeout
        while ((ret == 0) && (timeval_to_ms(&eventReadTimeout) > 0))
        {
            EventDef DNPEvent;
            TMWDTIME timeDate;

            if ((ret = eventPipe.readP((lu_uint8_t *) &DNPEvent, (lu_uint32_t) sizeof(EventDef), &eventReadTimeout)) == 0)
            {
                timeDate = DNPEvent.TimeStamp;

                switch(DNPEvent.eventCode)
                {
                    case DNPDEFS_OBJ_2_BIN_CHNG_EVENTS:
                        sdnpo002_addEvent(DNPEvent.pSession, DNPEvent.point, DNPEvent.DataValue.BinaryValue, &timeDate);
                        break;

                    case DNPDEFS_OBJ_4_DBL_CHNG_EVENTS:
                        sdnpo004_addEvent(DNPEvent.pSession, DNPEvent.point, DNPEvent.DataValue.BinaryValue, &timeDate);
                        break;

                    case DNPDEFS_OBJ_32_ANA_CHNG_EVENTS:
                        sdnpo032_addEvent(DNPEvent.pSession, DNPEvent.point, &(DNPEvent.DataValue.AnalogueValue), DNPEvent.flags, &timeDate);
                        break;

                    case DNPDEFS_OBJ_22_CNTR_EVENTS:
                        sdnpo022_addEvent(DNPEvent.pSession, DNPEvent.point, DNPEvent.DataValue.CounterValue, DNPEvent.flags, &timeDate);
                        break;

                    case DNPDEFS_OBJ_23_FCTR_EVENTS:
                        sdnpo023_addEvent(DNPEvent.pSession, DNPEvent.point, DNPEvent.DataValue.CounterValue, DNPEvent.flags, &timeDate);
                        break;
#if SDNPDATA_SUPPORT_OBJ42
                    case DNPDEFS_OBJ_42_ANA_OUT_EVENTS:
                        sdnpo042_addEvent(DNPEvent.pSession, DNPEvent.point, &(DNPEvent.DataValue.AnalogueValue), DNPEvent.flags, &timeDate);
                        break;
#endif
#if SDNPDATA_SUPPORT_OBJ43
                    case DNPDEFS_OBJ_43_ANA_CMD_EVENTS:
                        sdnpo043_addEvent(DNPEvent.pSession, DNPEvent.point, &(DNPEvent.DataValue.AnalogueValue), DNPEvent.flags, &timeDate);
                        break;
#endif
                }
            }
            else if (ret < 0)
            {
                log.error("%s Error %i reading DNP3 event pipe; errno:[%d]", FTITLE, ret, errno);
            }
        }
    }

    /* Close the protocol stack */
    log.info("%s DNP3 Slave Protocol Closing", FTITLE);

    /* Close protocol session */
    /* FIXME: pueyos_a - stopping the protocol here hangs the RTU for too long! (27 seconds?)
     *          The thread should not stop the protocol by itself!!!!
     *          Disabled for now. */
    //stopProtocol();
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void DNP3SlaveProtocol::tickEvent()
{
    // Loop over channels, opening them
    for (ChannelVector::iterator itChannel = DNP3SlaveChannels.begin();
                                 itChannel != DNP3SlaveChannels.end();
                                 ++itChannel)
    {
        (*itChannel)->tickEvent();
    }
}


void DNP3SlaveProtocol::flushSessions(TimeManager::TimeStr& currentTime)
{
    /* Extract entries from all session storages */
    for (SessionStorage::iterator itSession = sessionList.begin();
                                 itSession != sessionList.end();
                                 ++itSession)
    {
        VPEventStr rawEvent;            //Incoming event
        EventDef DNPEvent;              //DNP3 Event for library storage
        SLEventEntryStr localEvent;     //local event for Event Log
        CTEventDNP3PointStr localEventDNP;   //local event content

        Pipe& shtPipe = (*itSession)->getSHTPipe();
        do
        {
            /* Read shtPipe until empty */
            lu_int32_t ret;
            if ((ret = shtPipe.readP((lu_uint8_t *) &rawEvent, (lu_uint32_t) sizeof(VPEventStr))) >= 0)
            {
                /* re-generate event */
                DNPEvent.pSession = *((*itSession)->getSCLSessionPtr());
                DNPEvent.point = rawEvent.dnpID;
                DNPEvent.eventCode = rawEvent.eventCode;
                switch (rawEvent.eventCode)
                {
                    case DNPDEFS_OBJ_2_BIN_CHNG_EVENTS:
                    {
                        const TMWTYPES_UCHAR VALUEMASK = DNPDEFS_DBAS_FLAG_BINARY_ON | DNPDEFS_DBAS_FLAG_BINARY_OFF;
                        DNPEvent.DataValue.BinaryValue = rawEvent.value;
                        localEventDNP.pointType = EVENT_TYPE_DNP3SCADA_SINGLEBINARY;
                        localEventDNP.value.valueDig = ((rawEvent.value & VALUEMASK) == 0)? 0 : 1;
                    }
                        break;
                    case DNPDEFS_OBJ_4_DBL_CHNG_EVENTS:
                    {
                        DNPEvent.DataValue.BinaryValue = rawEvent.value;
                        localEventDNP.pointType = EVENT_TYPE_DNP3SCADA_DOUBLEBINARY;
                        const TMWTYPES_UCHAR VALUEMASK = DNPDEFS_DBAS_FLAG_DOUBLE_INDET |
                                                         DNPDEFS_DBAS_FLAG_DOUBLE_OFF |
                                                         DNPDEFS_DBAS_FLAG_DOUBLE_ON |
                                                         DNPDEFS_DBAS_FLAG_DOUBLE_INTER;
                        localEventDNP.value.valueDig = LU_GETBITMASKVALUE(rawEvent.value, VALUEMASK, 7);
                    }
                        break;
                    case DNPDEFS_OBJ_32_ANA_CHNG_EVENTS:
                        DNPEvent.DataValue.AnalogueValue.value.fval = rawEvent.aValue;
                        DNPEvent.DataValue.AnalogueValue.type = static_cast<TMWTYPES_ANALOG_TYPE>(rawEvent.valueType);
                        localEventDNP.pointType = EVENT_TYPE_DNP3SCADA_ANALOGUE;
                        localEventDNP.value.valueAna = rawEvent.aValue;
                        break;
                    case DNPDEFS_OBJ_22_CNTR_EVENTS:
                        DNPEvent.DataValue.BinaryValue = rawEvent.value;
                        localEventDNP.pointType = EVENT_TYPE_DNP3SCADA_COUNTER;
                        localEventDNP.value.valueDig = rawEvent.value;
                        break;
                    case DNPDEFS_OBJ_23_FCTR_EVENTS:
                        DNPEvent.DataValue.BinaryValue = rawEvent.value;
                        localEventDNP.pointType = EVENT_TYPE_DNP3SCADA_COUNTER;
                        localEventDNP.value.valueDig = rawEvent.value;
                        break;
                    default:
                        //invalid type!
                        continue;
                        break;
                }
                DNPEvent.flags = rawEvent.flags;
                /* Timestamp correction: use monotonic time difference to calculate
                 * realtime and monotonic time applied to the current time.
                 * Note that monotonic time always increase, so realtime
                 * calculations always result in times placed the past of current
                 * time.
                 * Note that re-calculated time stamps become as valid (synchronised)
                 * as the current time is.
                 */
                TimeManager::TimeStr timestamp(currentTime);//New timestamp
                timespec rawTime = rawEvent.relaTime;   //Make a copy of the time since it comes from a PACKED STRUCTURE
                lu_uint64_t elapsedms = timespec_elapsed_ms(&(rawTime), &(timestamp.relatime));

                timespec_sub_ms(&(timestamp.time), elapsedms);

                //Not used: timestamp.relatime = timestamp.relatime - rawEvent.relaTime;
                convertTime(&timestamp, &DNPEvent.TimeStamp);
                /* TODO: pueyos_a - use eventID from the point event? */
                //DNPEvent.TimeStamp.lucyEventID;

                // Create a local event
                localEventDNP.dnpPointID = rawEvent.dnpID;
                localEventDNP.status = rawEvent.flags;
                localEvent.eventClass = localEventDNP.EventClass;
                localEvent.timestamp.sec = timestamp.time.tv_sec;
                localEvent.timestamp.nsec = timestamp.time.tv_nsec;
                localEvent.payload.eventDNP3 = localEventDNP;
                eventlog.logEvent(localEvent);

                //Store event in Protocol Stack
                addEvent(&DNPEvent);
            }
            else
                break;  //exit loop
        } while(1);
    }
}


void DNP3SlaveProtocol::updateTime(TimeManager::TimeStr& currentTime)
{
    flushSessions(currentTime);    //Empty the pipes
    /* Lock eventing and repeat pipe flushing for entries added a second ago */
    MasterLockingMutex lMutex(synchPipeLock, LU_TRUE);
    flushSessions(currentTime);    //Empty the pipe for new arrivals
    firstSynch = true;  //Grant event bypass to event Pipe
    //Remove pipe:
    for (SessionStorage::iterator itSession = sessionList.begin();
                                     itSession != sessionList.end();
                                     ++itSession)
    {
        (*itSession)->dropSHTPipe();
    }
    SessionStorage().swap(sessionList); //Session list not needed anymore
}


SCADAP_ERROR DNP3SlaveProtocol::writeEventToSession(VPEventStr& eventVP,
                                                    DNP3SlaveProtocolSession* session
                                                    )
{
    if(session == NULL)
        return SCADAP_ERROR_NULL_POINTER;

    lu_int32_t ret;

    eventVP.sessionID = session->getSessionID();
    Pipe& shtPipe = session->getSHTPipe();
    bool repeat;
    do
    {
        repeat = false;
        ret = shtPipe.writeP((lu_uint8_t*)&eventVP, (lu_uint32_t)sizeof(VPEventStr));
        if( (ret < 0) && (errno == EAGAIN) )
        {
            //Discard older entry and retry
            VPEventStr discarded;   //event to be discarded
            if(shtPipe.readP((lu_uint8_t *) &discarded, (lu_uint32_t) sizeof(VPEventStr)) >= 0)
            {
                log.debug("Discarding DNP3 event %i:%i", discarded.sessionID, discarded.dnpID);
                repeat = true;
            }
        }
        if(ret >= 0)
        {
            log.debug(eventVP.toString().c_str());  //add protocol point values into log
        }
    } while(repeat);
    return (ret >= 0) ? SCADAP_ERROR_NONE : SCADAP_ERROR_EVENT_PIPE;
}


SwitchLogicOperation DNP3SlaveProtocol::binOutFillOperation(
                SDNPSESN& sDNPSession, TMWSDNPDatabaseStr& sessionDB,
                TMWTYPES_UCHAR controlCode)
{
    LU_UNUSED(sessionDB);   /* TODO: pueyos_a - for CLogic SBO only */

    SwitchLogicOperation  operation;
    TMWTYPES_BOOL         openIt;

    if( ((controlCode & DNPDEFS_CROB_CTRL_PAIRED_OP) == DNPDEFS_CROB_CTRL_PAIRED_CLOSE) ||
        ((controlCode & DNPDEFS_CROB_CTRL_MASK) == DNPDEFS_CROB_CTRL_LATCH_ON)
        )
    {
        openIt = TMWDEFS_FALSE;
    }
    else if( ((controlCode & DNPDEFS_CROB_CTRL_PAIRED_OP) == DNPDEFS_CROB_CTRL_PAIRED_TRIP) ||
             ((controlCode & DNPDEFS_CROB_CTRL_MASK) == DNPDEFS_CROB_CTRL_LATCH_OFF)
             )
    {
        openIt = TMWDEFS_TRUE;
    }
    else
    {
        /* Operation not supported */
        operation.operation = SWITCH_OPERATION_LAST;    //Operation not supported
        return operation;
    }

    operation.operation = (openIt == TMWDEFS_TRUE) ? SWITCH_OPERATION_OPEN :
                                                     SWITCH_OPERATION_CLOSE;
    operation.local = LU_FALSE; //Operation from SCADA is always remote
    operation.duration = sDNPSession.selectTimeout;
    //operation.uid = sessionDB.sessionUID; /* TODO: pueyos_a - for CLogic SBO only */
    return operation;
}


DNPDEFS_CROB_ST DNP3SlaveProtocol::translateCROB(const GDB_ERROR errorCode)
{
    DNPDEFS_CROB_ST ret = DNPDEFS_CROB_ST_UNDEFINED;

    /* Translate return code */
    switch (errorCode)
    {
        case GDB_ERROR_NONE:
            ret = DNPDEFS_CROB_ST_SUCCESS;
        break;

        case GDB_ERROR_ALREADY_ACTIVE:
            ret = DNPDEFS_CROB_ST_ALREADY_ACTIVE;
        break;

        case GDB_ERROR_AUTHORITY:
            ret = DNPDEFS_CROB_ST_NOT_AUTHORIZED;
        break;

        case GDB_ERROR_POSITION:
            ret = DNPDEFS_CROB_ST_SUCCESS;
        break;

        case GDB_ERROR_LOCAL_REMOTE:
            ret = DNPDEFS_CROB_ST_LOCAL;
        break;

        case GDB_ERROR_INHIBIT:
            ret = DNPDEFS_CROB_ST_AUTO_INHIBIT;
        break;

        default:
        break;
    }
    return ret;
}

void DNP3SlaveProtocol::logDiagnostic(const TMWDIAG_ANLZ_ID *pAnlzId,
                                      const TMWTYPES_CHAR *pString
                                    )
{
    const lu_char_t* FTITLE = "DNP3SlaveProtocol::logDiagnostic:";
    TMWDIAG_ID sourceId;

    sourceId = pAnlzId->sourceId;
    /* skip timestamp */
    if((sourceId & TMWDIAG_ID_TIMESTAMP) == TMWDIAG_ID_TIMESTAMP)
    {
        return;
    }
    /* Send entries to the suitable log */
    Logger& logDiag = Logger::getLogger(SUBSYSTEM_ID_SCADA_PROTOCOL);
    if((sourceId & TMWDIAG_ID_PHYS) == TMWDIAG_ID_PHYS)
    {
        /* physical layer */
        logDiag.debug("%s %s", FTITLE, (char *)pString);
    }
    else
    {
        /* other entries */
        logDiag.info("%s %s", FTITLE, (char *)pString);
    }
}


/*
 *********************** End of file ******************************************
 */


