/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i870mem4.c
 * description: IEC 60870-5-104 profile Link Layer Memory Implementation
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/i870mem4.h"
#include "tmwscl/i870/lnk4rdcy.h"
#include "tmwscl/i870/i870lnk4.h"

typedef struct{
  TMWMEM_HEADER           header;
  I870LNK4_CONTEXT        data;
} I870MEM4_ALLOC_CONTEXT;

typedef struct{
  TMWMEM_HEADER           header;
  LNK4RDCY_GROUP_CONTEXT  data;
} I870MEM4_ALLOC_RDCY_GROUP;

typedef struct{
  TMWMEM_HEADER            header;
  LNK4RDCY_CONNECT_CONTEXT data;
} I870MEM4_ALLOC_RDCY_CONNECT;
 
static const TMWTYPES_CHAR *_nameTable[I870MEM4_ALLOC_TYPE_MAX] = {
  "I870LNK4_CONTEXT",
  "LNK4RDCY_GROUP_CONTEXT",
  "LNK4RDCY_CONNECT_CONTEXT"
};

#if! TMWCNFG_USE_DYNAMIC_MEMORY
/* Use static allocated memory instead of dynamic memory */
static I870MEM4_ALLOC_CONTEXT       i870mem4Contexts[I870MEM4_NUMALLOC_CONTEXTS];
static I870MEM4_ALLOC_RDCY_GROUP    i870mem4GroupContexts[I870MEM4_NUMALLOC_RDCY_GROUPS];
static I870MEM4_ALLOC_RDCY_CONNECT  i870mem4ConnectContexts[I870MEM4_NUMALLOC_RDCY_CONNECTS];
#endif

static TMWMEM_POOL_STRUCT _i870mem4AllocTable[I870MEM4_ALLOC_TYPE_MAX];

#if TMWCNFG_USE_DYNAMIC_MEMORY
void TMWDEFS_GLOBAL i870mem4_initConfig(
  I870MEM4_CONFIG   *pConfig)
{
  pConfig->numContexts     = I870MEM4_NUMALLOC_CONTEXTS;
  pConfig->numRdcyGrps     = I870MEM4_NUMALLOC_RDCY_GROUPS;
  pConfig->numRdcyConnects = I870MEM4_NUMALLOC_RDCY_CONNECTS;
}
#endif

/* routine: i870mem4_init */
TMWTYPES_BOOL TMWDEFS_GLOBAL i870mem4_init(
 I870MEM4_CONFIG *pConfig)
{
#if TMWCNFG_USE_DYNAMIC_MEMORY
  /* dynamic memory allocation supported */
  
  I870MEM4_CONFIG  config; 

  /* If caller has not specified memory pool configuration, use the
   * default compile time values 
   */
  if(pConfig == TMWDEFS_NULL)
  {
    pConfig = &config;
    i870mem4_initConfig(pConfig);
  }

  if(!tmwmem_lowInit(_i870mem4AllocTable, I870MEM4_CONTEXT_TYPE,      pConfig->numContexts,     sizeof(I870MEM4_ALLOC_CONTEXT),      TMWDEFS_NULL))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_i870mem4AllocTable, I870MEM4_RDCY_GROUP_TYPE,   pConfig->numRdcyGrps,     sizeof(I870MEM4_ALLOC_RDCY_GROUP),   TMWDEFS_NULL))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_i870mem4AllocTable, I870MEM4_RDCY_CONNECT_TYPE, pConfig->numRdcyConnects, sizeof(I870MEM4_ALLOC_RDCY_CONNECT), TMWDEFS_NULL))
    return TMWDEFS_FALSE;
#else
  /* static memory allocation supported */
  TMWTARG_UNUSED_PARAM(pConfig);  

  if(!tmwmem_lowInit(_i870mem4AllocTable, I870MEM4_CONTEXT_TYPE,      I870MEM4_NUMALLOC_CONTEXTS,      sizeof(I870MEM4_ALLOC_CONTEXT),     (TMWTYPES_UCHAR*)i870mem4Contexts))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_i870mem4AllocTable, I870MEM4_RDCY_GROUP_TYPE,   I870MEM4_NUMALLOC_RDCY_GROUPS,   sizeof(I870MEM4_ALLOC_RDCY_GROUP),  (TMWTYPES_UCHAR*)i870mem4GroupContexts))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_i870mem4AllocTable, I870MEM4_RDCY_CONNECT_TYPE, I870MEM4_NUMALLOC_RDCY_CONNECTS, sizeof(I870MEM4_ALLOC_RDCY_CONNECT),(TMWTYPES_UCHAR*)i870mem4ConnectContexts))
    return TMWDEFS_FALSE;

#endif
  return TMWDEFS_TRUE;
}

/* function: i870mem4_alloc */
void * TMWDEFS_GLOBAL i870mem4_alloc(
  I870MEM4_ALLOC_TYPE type)
{
  if(type >= I870MEM4_ALLOC_TYPE_MAX)
  {
    return(TMWDEFS_NULL);
  }

  return(tmwmem_lowAlloc(&_i870mem4AllocTable[type]));
}

/* function: i870mem4_free */
void TMWDEFS_GLOBAL i870mem4_free(
  void *pBuf)
{    
  TMWMEM_HEADER *pHeader = TMWMEM_GETHEADER(pBuf);
  TMWTYPES_UCHAR   type = TMWMEM_GETTYPE(pHeader);

  if(type >= I870MEM4_ALLOC_TYPE_MAX)
  {
    return;
  }

  tmwmem_lowFree(&_i870mem4AllocTable[type], pHeader);
}

/* function: i870mem4_getUsage */
TMWTYPES_BOOL TMWDEFS_GLOBAL i870mem4_getUsage(
  TMWTYPES_UCHAR index,
  const TMWTYPES_CHAR **pName,
  TMWMEM_POOL_STRUCT *pStruct)
{
  if(index >= I870MEM4_ALLOC_TYPE_MAX)
  {
    return(TMWDEFS_FALSE);
  }

  *pName = _nameTable[index];
  *pStruct = _i870mem4AllocTable[index];
  return(TMWDEFS_TRUE);
}

 
