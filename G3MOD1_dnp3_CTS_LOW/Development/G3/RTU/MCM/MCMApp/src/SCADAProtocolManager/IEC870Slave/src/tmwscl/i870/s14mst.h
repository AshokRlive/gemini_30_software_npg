/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14mst.h
 * description: This file is intended for internal SCL use only.
 *  IEC 60870-5-101 slave step position functionality.
 */
#ifndef S14MST_DEFINED
#define S14MST_DEFINED

#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/utils/tmwsesn.h"
#include "tmwscl/i870/s14sesn.h"
#include "tmwscl/i870/s14sctr.h"
#include "tmwscl/i870/s14dbas.h"
#include "tmwscl/i870/s14evnt.h"

/* Structure used to store step position events */
typedef struct S14MSTEventStruct {
  S14EVNT s14Event;
  TMWTYPES_UCHAR valueAndState;
} S14MST_EVENT;

#ifdef __cplusplus
extern "C" {
#endif

  /* function: s14mst_init 
   * purpose: Initialize step position event processing
   * arguments:
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL s14mst_init(
    TMWSCTR *pSector);

  /* function: s14mst_close 
   * purpose: Close step position event processing
   * arguments:
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL s14mst_close(
    TMWSCTR *pSector);

  /* function: s14mst_internalAddEvent 
   * purpose: Internal function called by SCL to add an event to the queue.
   *  This will not tell the link layer to ask the application for a message
   *  to send. 
   * arguments:
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   *  cot - cause of transmission
   *  ioa - Information object address
   *  valueAndState - Value with transient state indication, defined in 7.2.6.5
   *  quality - Quality descriptor, defined in 7.2.6.3
   *  pTimeStamp - pointer to time structures
   * returns:
   *  pointer to event structure if event was added, TMWDEFS_NULL otherwise
   */
  void * TMWDEFS_GLOBAL s14mst_internalAddEvent(
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot,
    TMWTYPES_ULONG ioa, 
    TMWTYPES_UCHAR valueAndState, 
    TMWTYPES_UCHAR quality, 
    TMWDTIME *pTimeStamp);

  /* function: s14mst_addEvent 
   * purpose: Exported function called by user to add an step position event to the queue.
   * arguments:
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   *  cot - cause of transmission
   *   normally I14DEF_COT_SPONTANEOUS or I14DEF_COT_RETURN_LOCAL
   *  ioa - Information object address
   *  valueAndState - Value with transient state indication, defined in 7.2.6.5
   *  quality - Quality descriptor, defined in 7.2.6.3
   *  pTimeStamp - time of event
   *   if pTimeStamp != TMWDEFS_NULL Event will be sent using the ASDU specified 
   *   by sector config mstTimeFormat
   *    To set RES1 bit to indicate Substituted time, 
   *     set pTimeStamp->genuineTime to TMWEFS_FALSE and make sure
   *     S14DATA_SUPPORT_GENUINE_TIME is defined in s14data.h
   *   if pTimeStamp == TMWDEFS_NULL the event will be sent using ASDU 5 MSTNA
   * returns:
   *  void * - non-NULL value indicates success; TMWDEFS_NULL indicates failure
   */
  TMWDEFS_SCL_API void * TMWDEFS_GLOBAL s14mst_addEvent(
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot,
    TMWTYPES_ULONG ioa, 
    TMWTYPES_UCHAR valueAndState, 
    TMWTYPES_UCHAR quality, 
    TMWDTIME *pTimeStamp);
 
  /* function: s14mst_countEvents
   * purpose: Count the number of step position change events in
   *  queue
   * arguments:
   *  pSector - pointer to sector
   * returns:
   *  void
   */
  TMWTYPES_USHORT TMWDEFS_GLOBAL s14mst_countEvents(
    TMWSCTR *pSector);

  /* function: s14mst_scanForChanges 
   * purpose: Function to scan for step position changes.
   * arguments:
   *  pSector - identifies sector
   * returns:
   *  TMWDEFS_TRUE if any points have changed
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14mst_scanForChanges(
    TMWSCTR *pSector);
 
  /* function: s14mst_processEvents 
   * purpose: Process step position events
   * arguments:
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   *  pEventTime - returns time of earliest event if a clock sync is required
   * returns:
   *  S14EVNT_SENT                - ASDU was sent
   *  S14EVNT_CLOCK_SYNC_REQUIRED - Spontaneous CCSNA is required
   *  S14EVNT_NOT_SENT            - No ASDU was sent
   */
  S14EVNT_STATUS TMWDEFS_GLOBAL s14mst_processEvents(
    TMWSCTR *pSector,
    TMWDTIME *pEventTime,
    TMWTYPES_UCHAR cotSpecified);
   
  /* function: s14mst_processDblTransEvents
   * purpose: Process Double Transmission high priority events into a message
   * arguments:
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   * returns:
   *  S14EVNT_SENT                - ASDU was sent
   *  S14EVNT_NOT_SENT            - No ASDU was sent
   */
  S14EVNT_STATUS TMWDEFS_GLOBAL s14mst_processDblTransEvents(
    TMWSCTR *pSector);

  /* function: s14mst_readIntoResponse 
   * purpose: Read the MST values for the point specified and store in response
   *  message to be sent to master
   * arguments:
   *  pTxData - pointer
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   *  cot - cause of tranmission
   *  groupMask - mask specifying what group(s) points should be in
   *  ioa - information object address
   *  pPointIndex - pointer to index of point to be read. This should be updated
   *   by this function.
   * returns:
   *   status
   */
  S14DBAS_GROUP_STATUS TMWDEFS_CALLBACK s14mst_readIntoResponse(
    TMWSESN_TX_DATA *pTxData, 
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot, 
    TMWDEFS_GROUP_MASK groupMask, 
    TMWTYPES_ULONG ioa,
    TMWTYPES_USHORT *pPointIndex);

#ifdef __cplusplus
}
#endif
#endif /* S14MST_DEFINED */
