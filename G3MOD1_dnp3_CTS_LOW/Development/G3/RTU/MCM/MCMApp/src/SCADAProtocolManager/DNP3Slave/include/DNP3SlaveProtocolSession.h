/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:DNP3SlaveProtocolSession.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   26 Sep 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef DNP3SLAVEPROTOCOLSESSION_H_
#define DNP3SLAVEPROTOCOLSESSION_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "Table.h"
#include "Pipe.h"

#include "ISlaveProtocolSession.h"
#include "IPoint.h"
#include "IPointObserver.h"

#include "TMWDNP3Includes.h"
#include "SCADAProtocolCommon.h"
#include "DialupModem.h"
#include "IMCMApplication.h"
#include "GeminiDatabase.h"
#include "EventLogManager.h"
#include "Logger.h"
#include "SCADAConnectionManager.h"
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/**
 * \brief Database structure used by the DNP3 slave protocol manager
 */
struct DNP3SlaveDB
{
public:
    /**
     * Reference to the Triangle MicroWorks point database
     */
    TMWSDNPDatabaseStr *pointDB;
    /**
     * Binary input observer list
     */
    PointObserverList   BIObserverList;
    /**
     * Double Binary input observer list
     */
    PointObserverList   DBIObserverList;
    /**
     * Analogue input observer list
     */
    PointObserverList   AIObserverList;
    /**
     * Analogue output observer list
     */
    PointObserverList   AOObserverList;
    /**
     * Counter input observer list
     */
    PointObserverList   CObserverList;
};

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

//Forward declaration
class DNP3SlaveProtocolChannel;

/*
 * DNP3SlaveProtocolSession
 *
 * Defines the Communications Session for the protocol.  This runs over a DNP3SlaveProtocolChannel.
 *
 */
class DNP3SlaveProtocolSession : public AbstractSlaveProtocolSession
{
public:
    struct UserConfig
    {
    public:
        lu_uint32_t     userNumber;
        lu_uint16_t     userRole;
        lu_uint16_t     updateKeyLen;
        lu_uint8_t      updateKey[LUCYCRYPTO_MAX_KEY_LEN];
    public:
        UserConfig() :  userNumber(0),
                        userRole(0),
                        updateKeyLen(0)
        {
            memset(updateKey, 0, LUCYCRYPTO_MAX_KEY_LEN);
        }
    };

    class Config
    {
    public:
        SDNPSESN_CONFIG  sessionConfig;

        // TODO - SKA - will be replaced with a broadcast controls mask
        bool             lucy_allowControlOnBrdCstAddr;

        lu_uint32_t     usersNumber;
        UserConfig      users[LUCYCRYPTO_MAX_USERS];
    public:
        Config() :  lucy_allowControlOnBrdCstAddr(false),
                    usersNumber(0)
        {}
    };

public:
    DNP3SlaveProtocolSession(GeminiDatabase&  GDatabase,
                             DNP3SlaveProtocolChannel* channelPtr,
                             Config config,
                             lu_uint32_t sessID);
    virtual ~DNP3SlaveProtocolSession();

    /**
     * /brief Get pointer to Session Object
     *
     * /param None
     *
     *  /returns Reference to m_SCLSessionPtr
     *  Output points require the address of the m_SCLSessionPtr
     *
     */
    TMWSESN **getSCLSessionPtr()
    {
        return &m_SCLSessionPtr;
    }

    /**
     * /brief Get the online status of the session
     *
     * /detail If our channel is controlled by the ConnectionManager
     * the SCL's online indicator will return 'online' most of the time
     * as it will have a TCP connection to the ConnectionManagermost of the
     * time - this is where we use this object's online instead.
     *
     * /param N/A
     *
     * /returns online status
     */
    lu_bool_t getOnline();

    /* TODO: pueyos_a - Temporal fix, to be removed */
    /**
     * \brief Get a reference of Session's Storage Holder for Timestamp pipe
     *
     * \warning Temporal implementation: calling dropSHTPipe() makes any further
     *          getSHTPipe() call unsafe (Segmentation fault!)
     *
     * \return Pipe reference
     */
    Pipe& getSHTPipe()
    {
        return *m_shtPipe;
    }
    void dropSHTPipe()
    {
        delete m_shtPipe;
        m_shtPipe = NULL;
    }


private:
    /**
     * /brief Open a SDNP Session
     *
     * /param SCLChannelPtr - Pointer to the Channel Object
     */
    SCADAP_ERROR openSession(TMWCHNL *SCLChannelPtr);

    /**
     * /brief Count the number of stored in the session
     *
     * /param classMask - Classes of events to count
     * /param countAll  - return full count of events or 1 to indicate there are events
     */
    lu_uint32_t countAllEvents(TMWDEFS_CLASS_MASK classMask, lu_bool_t countAll);

    /**
     * /brief Set the Master Address for this SEssion
     *
     * /param masterAddress - Master Address
     */
    void setMasterAddress(lu_uint32_t masterAddress)
    {
        // Set the Master Address in the Config and the SCL
        m_config.sessionConfig.destination = masterAddress;
        m_SCLSessionPtr->destAddress       = masterAddress;
    }

public:
    DNP3SlaveDB                     *m_db;  // Points for this session

private:
    GeminiDatabase&                 m_GDatabase;
    Logger&                         m_log;

    DNP3SlaveProtocolChannel        *m_DNP3ChannelPtr;
    TMWCHNL                         *m_SCLChannelPtr;  // Reference to owning channel
    TMWSESN                         *m_SCLSessionPtr;

    Config  m_config;

    Pipe* m_shtPipe;  //Storage Holder for Timestamp pipe: stores DNP events when RTU is not yet synchronised

};

#endif /* DNP3SLAVEPROTOCOLSESSION_H_ */

/*
 *********************** End of file ******************************************
 */
