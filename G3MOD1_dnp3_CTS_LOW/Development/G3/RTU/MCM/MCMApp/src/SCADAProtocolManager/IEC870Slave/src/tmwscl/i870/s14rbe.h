/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14rbe.h
 * description: This file is intended for internal SCL use only.
 *  IEC 60870-5-101/104 Slave Report By Exception support.
 */
#ifndef S14RBE_DEFINED
#define S14RBE_DEFINED

#include "tmwscl/i870/s14sesn.h"
#include "tmwscl/i870/s14sctr.h"

#ifdef __cplusplus
extern "C" {
#endif

  /* function: s14rbe_initSector */
  void TMWDEFS_GLOBAL s14rbe_initSector(
    TMWSCTR *pSector);

  /* function: s14rbe_closeSector */
  void TMWDEFS_GLOBAL s14rbe_closeSector(
    TMWSCTR *pSector);

  /* function: s14rbe_updateRBEScanPeriod */
  void TMWDEFS_GLOBAL s14rbe_updateRBEScanPeriod(
    TMWSCTR *pSector, 
    TMWTYPES_MILLISECONDS period);

  /* function: s14rbe_scanForChanges */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14rbe_scanForChanges(
    TMWSCTR *pSector);
  
  /* function: s14rbe_countEvents */
  TMWTYPES_USHORT TMWDEFS_GLOBAL s14rbe_countEvents(
    TMWSCTR *pSector,
    TMWTYPES_BOOL countAll);

  /* function: s14rbe_processEvents */
  TMWTYPES_BOOL TMWDEFS_CALLBACK s14rbe_processEvents(
    TMWSCTR *pSector, 
    TMWTYPES_BOOL buildResponse);
    
  TMWTYPES_BOOL TMWDEFS_CALLBACK s14rbe_processMonitorEvents(
    TMWSCTR *pSector,
    TMWTYPES_UCHAR asduType);

#ifdef __cplusplus
}
#endif
#endif /* S14RBE_DEFINED */
