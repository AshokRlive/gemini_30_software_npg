/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:S101Database.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief Protocol Point database
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19 Jul 2016     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef S101DATABASE_H_
#define S101DATABASE_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <vector>
#include <map>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "S101TMWIncludes.h"
#include "S101InputPoint.h"
#include "S101OutputPoint.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
class S101Sector;   // Forward declaration

/**
 * \brief Database structure used by the IEC101 slave protocol.
 */
class S101Database
{
public:
    S101Database(S101Sector& sector);
    virtual ~S101Database();

    void addInputPoint(S101InputPoint::ProtPointType,S101InputPoint* pInputPoint);

    void addOutputPoint(S101OutputPoint::ProtPointType,S101OutputPoint* pOutputPoint);

    S101InputPoint* getInputPoint(S101InputPoint::ProtPointType type,TMWTYPES_USHORT index);

    I870OutputStr* getOutputPointStr(S101OutputPoint::ProtPointType type,TMWTYPES_ULONG ioa);

    S101Sector& getSector(){return m_sector;}

    SCADAP_ERROR getSectorHandle(TMWSCTR** ppSectorHdle);

private:
    /*Vectors of S101InputPoint*/
    typedef std::vector<S101InputPoint*> S101MSPVect;
    typedef std::vector<S101InputPoint*> S101MDPVect;
    typedef std::vector<S101InputPoint*> S101MMENAVect;
    typedef std::vector<S101InputPoint*> S101MMENBVect;
    typedef std::vector<S101InputPoint*> S101MMENCVect;
    typedef std::vector<S101InputPoint*> S101MITVect;

    /*Maps of output point structures against IOA*/
    typedef std::map<TMWTYPES_ULONG, I870OutputStr> S101SCOMap;
    typedef std::map<TMWTYPES_ULONG, I870OutputStr> S101DCOMap;

    S101Sector&         m_sector;

    S101MSPVect         m_MSPVect;
    S101MDPVect         m_MDPVect;
    S101MMENAVect       m_MMENAVect;
    S101MMENBVect       m_MMENBVect;
    S101MMENCVect       m_MMENCVect;
    S101MITVect         m_MITVect;
    S101SCOMap          m_SCOMap;
    S101DCOMap          m_DCOMap;


public:
    /********************************************/
    /* TMW Callback Function Interface          */
    /********************************************/

    static void* mspGetPoint(void *pS101DB, TMWTYPES_USHORT index);

    static void* mdpGetPoint(void *pS101DB, TMWTYPES_USHORT index);

    static void* mmenaGetPoint(void *pS101DB, TMWTYPES_USHORT index);

    static void* mmenbGetPoint(void *pS101DB, TMWTYPES_USHORT index);

    static void* mmencGetPoint(void *pS101DB, TMWTYPES_USHORT index);

    static void* mitGetPoint(void *pS101DB, TMWTYPES_USHORT index);

    static void* cscLookupPoint(void *pS101DB, TMWTYPES_ULONG ioa);

    static void* cdcLookupPoint(void *pS101DB, TMWTYPES_ULONG ioa);

    static TMWTYPES_BOOL ccsnaSetTime(TMWDTIME *pNewTime);

    static void ccsnaGetTime(TMWDTIME *pNewTime);

    static TMWTYPES_BOOL mitFreeze(void *pPoint, TMWTYPES_UCHAR qcc);

    static TMWTYPES_BOOL hasSectorReset(void *pS101DB, TMWTYPES_UCHAR *pResetCOI);

    static TMWTYPES_BOOL crpnaExecuteRTUReset(TMWTYPES_UCHAR qrp);

//
//    static TMWDEFS_COMMAND_STATUS cscSelect(void *pPoint, TMWTYPES_UCHAR cot,
//                    TMWTYPES_UCHAR sco);
//
//    static TMWDEFS_COMMAND_STATUS cscExecute(void *pPoint, TMWTYPES_UCHAR cot,
//                    TMWTYPES_UCHAR sco);

//    static TMWDEFS_COMMAND_STATUS cdcSelect(void *pPoint, TMWTYPES_UCHAR cot,
//                    TMWTYPES_UCHAR dco);
//
//    static TMWDEFS_COMMAND_STATUS cdcExecute(void *pPoint, TMWTYPES_UCHAR cot,
//                    TMWTYPES_UCHAR dco);

    static lu_bool_t deleteEventID(lu_uint32_t eventID, void *pDB);

};


#endif /* S101DATABASE_H_ */

/*
 *********************** End of file ******************************************
 */
