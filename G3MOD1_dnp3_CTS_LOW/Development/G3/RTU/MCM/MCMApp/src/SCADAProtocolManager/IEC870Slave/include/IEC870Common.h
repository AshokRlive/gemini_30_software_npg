/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:IEC870Common.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   10 Aug 2017     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef IEC870COMMON_H_
#define IEC870COMMON_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */
/**
 * \brief Scale a given value inside the IEC-60870 Measurement Scaled value
 *
 * This converts a given value from the original scale into the IEC-60870
 * Measurement value - Scaled value: [-32768, 32767]
 * Any value outside the range is clamped to the max/min values of the scale.
 *
 * \param value Incoming value to convert
 * \param minValue Min value of the original scale
 * \param maxValue Max value of the original scale
 *
 * \return Scaled value in the range [-32768, 32767]
 */
lu_int16_t IEC870Scaled(const lu_float32_t value,
                        const lu_float32_t minValue,
                        const lu_float32_t maxValue);

/**
 * \brief Scale a given value inside the IEC-60870 Measurement Normalized value
 *
 * This converts a given value from the original scale into the IEC-60870
 * Measurement value - Normalized value: [-1, 0.999969482421875]
 * Any value outside the range is clamped to the max/min values of the scale.
 *
 * Note that the returned value may need to be converted into the IEC-60870
 * 16-bit Normalized value for storage by multiplying the obtained value by the
 * Normalization Scaling Factor (32768). Example of use:
 *      lu_float32_t norm = NORMALIZE_SCALING_FACTOR * IEC870Normalized(value, min, max);
 *      tmwAnalogValue.value.sval = (TMWTYPES_SHORT)(norm);
 *
 * \param value Incoming value to convert
 * \param minValue Min value of the original scale
 * \param maxValue Max value of the original scale
 *
 * \return Normalized value in the range [-1, 0.999969482421875]
 */
lu_float32_t IEC870Normalized(const lu_float32_t value,
                                const lu_float32_t minValue,
                                const lu_float32_t maxValue);

#endif /* IEC870COMMON_H_ */

/*
 *********************** End of file ******************************************
 */
