/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s1ccdna.c
 * description: IEC 60870-5-101 slave CCDNA (Delay Acquisition Command)
 *  functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s1ccdna.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/i14diag.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/s101sctr.h"
#include "tmwscl/i870/i870chnl.h"
#include "tmwscl/utils/tmwtarg.h"

#if S14DATA_SUPPORT_CCD

/* function: s1ccdna_processRequest */
void TMWDEFS_CALLBACK s1ccdna_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  S101SCTR *p101Sector = (S101SCTR *)pSector;
  TMWTYPES_ULONG ioa;

  /* Parse Information Object Address */
  i870util_readInfoObjAddr(pSector->pSession, pMsg, &ioa);  
  
  /* Parse time */
  p101Sector->ccdnaFirstByteReceived = pMsg->pRxData->firstByteTime;
  tmwtarg_get16(&pMsg->pRxData->pMsgBuf[pMsg->offset], &p101Sector->ccdnaTime);
  pMsg->offset += 2;

  /* Parse received cause of transmission */
  if(pMsg->cot == I14DEF_COT_ACTIVATION)
  {
    p101Sector->ccdnaCOT = I14DEF_COT_ACTCON;
  }
  else if(pMsg->cot == I14DEF_COT_SPONTANEOUS)
  {
    /* Load Delay, no response required */
  }
  else
  {
    p101Sector->ccdnaCOT = I14DEF_COT_UNKNOWN_COT | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  if(ioa != 0)
  {
    p101Sector->ccdnaCOT = I14DEF_COT_UNKNOWN_IOA | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  if(pMsg->cot == I14DEF_COT_SPONTANEOUS)
  {
    /* Diagnostics */
    I14DIAG_SHOW_STORE_TYPE(pSector, pMsg);
    I14DIAG_SHOW_PROPAGATION_DELAY(pSector, p101Sector->ccdnaTime);

    /* Load delay, set sector specific delay */
    p101Sector->s14.propagationDelay = p101Sector->ccdnaTime;
  }
}

/* function: s1ccdna_buildResponse */
TMWTYPES_BOOL TMWDEFS_CALLBACK s1ccdna_buildResponse(
  TMWSCTR *pSector, 
  TMWTYPES_BOOL buildResponse)
{
  S101SCTR *p101Sector = (S101SCTR *)pSector;

  if(p101Sector->ccdnaCOT != 0)
  {
    if(buildResponse)
    {
      /* Activation confirmation or Deactivation */
      TMWSESN_TX_DATA *pTxData;
      S14SESN *pS14Session = (S14SESN *)pSector->pSession;

      if((pTxData = i870chnl_newTxData(pSector->pSession->pChannel, pSector->pSession, pSector,
        pS14Session->maxASDUSize)) == TMWDEFS_NULL)
      {
        return(TMWDEFS_FALSE);
      }

#if TMWCNFG_SUPPORT_DIAG
      pTxData->pMsgDescription = "Delay Acquisition Response";
#endif
      pTxData->txFlags = TMWSESN_TXFLAGS_NO_RESPONSE;

      /* Build response */
      i870util_buildMessageHeader(pSector->pSession, 
        pTxData, I14DEF_TYPE_CCDNA1, p101Sector->ccdnaCOT, 
        p101Sector->ccdnaOriginator, p101Sector->s14.i870.asduAddress);

      /* Store information object address, always 0 */
      i870util_storeInfoObjAddr(pSector->pSession, pTxData, 0);

      /* Store time */
      if(p101Sector->ccdnaCOT == I14DEF_COT_ACTCON)
      {
        TMWTYPES_ULONG processingDelay;
        TMWTYPES_ULONG oneWayDelay;
        TMWTYPES_USHORT tmpDelay;

        processingDelay = (tmwtarg_getMSTime() - p101Sector->ccdnaFirstByteReceived);
        oneWayDelay = p101Sector->ccdnaTime + processingDelay;
        if(oneWayDelay >= 60000) 
          oneWayDelay -= 60000;

        tmpDelay = (TMWTYPES_USHORT)oneWayDelay;
        tmwtarg_store16(&tmpDelay, &pTxData->pMsgBuf[pTxData->msgLength]);
        pTxData->msgLength += 2;
      }
      else
      {
        tmwtarg_store16(&p101Sector->ccdnaTime, &pTxData->pMsgBuf[pTxData->msgLength]);
        pTxData->msgLength += 2;
      }

      /* Request is complete */
      p101Sector->ccdnaCOT = 0;

      /* Send the response */
      i870chnl_sendMessage(pTxData);
    }

    return(TMWDEFS_TRUE);
  }

  return(TMWDEFS_FALSE);
}

#endif /* S14DATA_SUPPORT_CCD */
