/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s104sctp.h
 * description: This file is intended for internal SCL use only.
 *  IEC 60870-5-104 Slave sector
 */
#ifndef S104SCTP_DEFINED
#define S104SCTP_DEFINED

#include "tmwscl/utils/tmwdtime.h"
#include "tmwscl/i870/s14sctr.h"

/* S104 Sector Context */
typedef struct S104SectorStruct {
  /* Generic IEC 60870-5-101/104 Sector Info, Must be first entry */
  S14SCTR s14;

  /* Misc Configuration */
  TMWTYPES_MILLISECONDS maxCommandAge;
  TMWTYPES_MILLISECONDS maxCommandFuture;

  /* Time Tag Commands Support */
  TMWDTIME csctaRequestTime;

  TMWDTIME cdctaRequestTime;

  TMWDTIME crctaRequestTime;

  TMWDTIME cbotaRequestTime;

  TMWDTIME csetaRequestTime;

  TMWDTIME csetbRequestTime;

  TMWDTIME csetcRequestTime;

  /* 104 specific commands */
  TMWTYPES_UCHAR ctstaCOT;
  TMWTYPES_ULONG ctstaIOA;
  TMWDTIME ctstaRequestTime;
  TMWTYPES_UCHAR ctstaOriginator;

  /* Test Sequence Number */
  TMWTYPES_USHORT testSequenceNumber;

#if S14DATA_SUPPORT_104CTSNA
  /* For supporting CTSNA in 104 */
  TMWTYPES_UCHAR ctsnaCOT;
  TMWTYPES_USHORT pattern;
#endif

} S104SCTR;

#endif /* S104SCTP_DEFINED */
