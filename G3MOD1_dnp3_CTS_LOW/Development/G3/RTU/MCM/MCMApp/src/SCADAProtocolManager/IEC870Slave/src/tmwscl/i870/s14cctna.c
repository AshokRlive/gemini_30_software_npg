/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14cctna.c
 * description: 101/104 Private ASDU slave CCTNA Set Configuration Table Command
 *  functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14cctna.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/utils/tmwtarg.h"

#if S14DATA_SUPPORT_CCTNA 
 
/* function: s14cctna_processRequest */
void TMWDEFS_CALLBACK s14cctna_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  TMWTYPES_UCHAR *pCtiValues;
  void *pPoint;
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  /* Store originator address */
  p14Sector->cctnaOriginator = pMsg->origAddress;
  
  /* Parse Information Object Address */
  i870util_readInfoObjAddr(pSector->pSession, pMsg, &p14Sector->cctnaIOA);
 
  pCtiValues = &pMsg->pRxData->pMsgBuf[pMsg->offset];
  pMsg->offset = (TMWTYPES_USHORT)(pMsg->offset + pMsg->quantity);

  /* Parse received cause of transmission */
  if(pMsg->cot == I14DEF_COT_ACTIVATION)
    p14Sector->cctnaCOT = I14DEF_COT_ACTCON;
  else
  {
    p14Sector->cctnaCOT = I14DEF_COT_UNKNOWN_COT | I14DEF_COT_NEGATIVE_CONFIRM;

    return;
  }

  /* Lookup data point using information object address */
  pPoint = s14data_cctnaLookupPoint(p14Sector->i870.pDbHandle, p14Sector->cctnaIOA);
  if(pPoint == TMWDEFS_NULL)
  {

    p14Sector->cctnaCOT = I14DEF_COT_UNKNOWN_IOA | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  s14data_cctnaSetValues(pPoint, pCtiValues, pMsg->quantity); 
}

/* function: s14cctna_buildResponse */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14cctna_buildResponse( 
  TMWSCTR *pSector, 
  TMWTYPES_BOOL buildResponse)
{
  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  if(p14Sector->cctnaCOT != 0)
  {
    if(buildResponse)
    {
      /* Activation confirmation */
      TMWSESN_TX_DATA *pTxData;
      S14SESN *pS14Session = (S14SESN *)pSector->pSession;

      if((pTxData = i870chnl_newTxData(pSector->pSession->pChannel, 
        pSector->pSession, pSector, pS14Session->maxASDUSize)) == TMWDEFS_NULL)
      {
        return(TMWDEFS_FALSE);
      }

#if TMWCNFG_SUPPORT_DIAG
      pTxData->pMsgDescription = "Set Configuration Table Response";
#endif
      pTxData->txFlags = TMWSESN_TXFLAGS_NO_RESPONSE;
      
      /* Build response */
      i870util_buildMessageHeader(pSector->pSession, 
        pTxData, I14DEF_TYPE_CCTNA1, p14Sector->cctnaCOT, 
        p14Sector->cctnaOriginator, p14Sector->i870.asduAddress);

      /* Store point number */
      i870util_storeInfoObjAddr(pSector->pSession, pTxData, p14Sector->cctnaIOA);

      /* Request is complete */
      p14Sector->cctnaCOT = 0;

      /* Send the response */
      i870chnl_sendMessage(pTxData);
    }

    return(TMWDEFS_TRUE);
  }

  return(TMWDEFS_FALSE);
}
#endif /* S14DATA_SUPPORT_CCTNA */
