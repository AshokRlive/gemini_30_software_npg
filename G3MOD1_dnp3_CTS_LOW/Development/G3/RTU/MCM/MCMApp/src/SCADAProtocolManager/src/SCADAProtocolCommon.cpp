/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:SCADAProtocolCommon.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14 Oct 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "SCADAProtocolCommon.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */





/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
void convertTime(TimeManager::TimeStr *inTimePtr, TMWDTIME *outTimePtr)
{
    if ((inTimePtr == NULL) || (outTimePtr == NULL))
    {
        return;
    }

    /* Current time, is always genuine, not substituted */
    outTimePtr->genuineTime = TMWDEFS_TRUE;
    outTimePtr->invalid = (inTimePtr->synchronised)? TMWDEFS_FALSE : TMWDEFS_TRUE;

    struct tm timeTMW;
    TimeManager::TimeToStructtm(inTimePtr->time, &timeTMW);

    outTimePtr->year = timeTMW.tm_year + 1900;  /* tm_year is year since 1900 */
    outTimePtr->month = timeTMW.tm_mon + 1;     /* tm_month is 0..11          */
    /* tm_wday is 0-7 Sunday-Saturday, dayOfWeek is 1-7 Monday-Sunday, 0 is not used */
    outTimePtr->dayOfWeek = (timeTMW.tm_wday == 0)? 7: timeTMW.tm_wday;
    outTimePtr->dayOfMonth = timeTMW.tm_mday;
    outTimePtr->hour = timeTMW.tm_hour;
    outTimePtr->minutes = timeTMW.tm_min;
    outTimePtr->dstInEffect = timeTMW.tm_isdst;
    outTimePtr->mSecsAndSecs = SEC_TO_MSEC(timeTMW.tm_sec) + NSEC_TO_MSEC(inTimePtr->time.tv_nsec);
}


void convertTime(TMWDTIME& inTime, struct tm& outTime, lu_uint32_t& msecs)
{
    outTime.tm_year  = inTime.year - 1900;
    outTime.tm_mon   = (int)(inTime.month) - 1;
    outTime.tm_mday  = inTime.dayOfMonth;
    outTime.tm_hour  = inTime.hour;
    outTime.tm_min   = inTime.minutes;
    outTime.tm_sec   = inTime.mSecsAndSecs / 1000;
    outTime.tm_isdst = (inTime.dstInEffect)? 1 : 0;

    msecs = inTime.mSecsAndSecs % 1000;
}


void convertTime(TMWDTIME& inTime, TimeManager::TimeStr& outTime)
{
    struct tm timeToSet;
    lu_uint32_t msecs;
    convertTime(inTime, timeToSet, msecs);
    outTime.time = TimeManager::getInstance().structtmtoTimespec(timeToSet, msecs);
}




/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */



/*
 *********************** End of file ******************************************
 */
