/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:S101EventPublisher.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief Protocol point eventing for IEC-101
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19 Jul 2016     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "Logger.h"
#include "EventLogManager.h"
#include "EventTypeDef.h"

#include "S101EventPublisher.h"
#include "S101Debug.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define DBG_TMW_EVENT_PUBLISH(event)\
                DBG_INFO("%s publishing event,sector:%p type:%d ioa:%lu value:%d",\
                            TITLE,\
                            (void*)(event.pSector),\
                            event.eventType,\
                            event.ioa,\
                            event.InformationElement)\

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
S101EventPublisher g_s101EventPub;  //global instance initialisation

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static const lu_uint32_t PROTOCOL_POOL_DELAY_US = 50000; //Amount of usec to wait for new events

static Logger& log(Logger::getLogger(SUBSYSTEM_ID_SCADA_HANDLER));

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
S101EventPublisher::S101EventPublisher() : m_eventPipe(Pipe::PIPE_TYPE_NONBLOCKING)
{}

S101EventPublisher::~S101EventPublisher()
{}


SCADAP_ERROR S101EventPublisher::addMSPEvent(S101MSP& evtSrc, PointData& evtData)
{
    SCADAP_ERROR            err;
    TimeManager::TimeStr    evtime;
    TMWEvent                event;
    TMWSCTR*                pSector;

    /*Instance of Eventlog for event logging*/
    EventLogManager& eventlog = *(EventLogManager::getInstance());
    CTEventSi870PointStr     event101; // Create a local event for event log

    // Init event
    memset(&event,0,sizeof(event));

    /* Get sector handle
     * Note we should not pass the address of event.PSector cause it is packed!
     */
    err = evtSrc.getSectorHandle(&pSector);
    checkSCADAErrNone(err);
    const I870DataMSP&  mspdata = evtSrc.getData();

    /* Generate event only when online */
    if( (mspdata.commonElements.eventOnlyWhenOnline == TMWDEFS_TRUE) &&
        ( (pSector->pSession->online == TMWSESN_STAT_OFFLINE) ||
          (pSector->pSession->online == TMWSESN_STAT_ERROR) )
        )
    {
        return SCADAP_ERROR_NONE;   //Do not generate any event
    }

    // Fill in event time stamp
    evtData.getTime(evtime);
    convertTime(&evtime, &(event.timestamp));

    // Fill in event content
    event.eventType      = MSP_EVENT;
    event.ioa            = mspdata.commonElements.ioa;
    event.pSector        = pSector;
    event.InformationElement.siq = mspdata.value;


#if S14_LOCAL_EVENT_SUPPORT
    if(mspdata.commonElements.eventLogStore == TMWDEFS_TRUE)
    {
        //fill in log event content
        event101.pointType = EVENT_TYPE_S101_SINGLEBINARY;
        event101.value.valueDig = mspdata.value;
        event101.protocolID = mspdata.commonElements.ioa;
        event101.status = mspdata.value;
        // Create a log event
        SLEventEntryStr         logEvent;
        logEvent.eventClass = event101.EventClassS101;
        logEvent.timestamp.sec = evtime.time.tv_sec;
        logEvent.timestamp.nsec = evtime.time.tv_nsec;
        logEvent.payload.eventSi870 = event101;
         // Log the Event in the event log
        eventlog.logEvent(logEvent);
    }
#endif

    return this->addEvent(event);
}



SCADAP_ERROR S101EventPublisher::addMDPEvent(S101MDP& evtSrc, PointData& evtData)
{
    SCADAP_ERROR            err;
    TimeManager::TimeStr    evtime;
    TMWEvent                event;
    TMWSCTR*                pSector;

    /*Instance of Eventlog for event logging*/
    EventLogManager& eventlog = *(EventLogManager::getInstance());
    CTEventSi870PointStr     event101; // Create a local event for event log

    // Init event
    memset(&event,0,sizeof(event));

    /* Get sector handle
     * Note we should not pass the address of event.PSector cause it is packed!
     */
    err = evtSrc.getSectorHandle(&pSector);
    checkSCADAErrNone(err);
    const I870DataMDP&  mdpdata = evtSrc.getData();

    /* Generate event only when online */
    if( (mdpdata.commonElements.eventOnlyWhenOnline == TMWDEFS_TRUE) &&
        ( (pSector->pSession->online == TMWSESN_STAT_OFFLINE) ||
          (pSector->pSession->online == TMWSESN_STAT_ERROR) )
        )
    {
        return SCADAP_ERROR_NONE;   //Do not generate any event
    }

    // Fill in event time stamp
    evtData.getTime(evtime);
    convertTime(&evtime, &(event.timestamp));

    // Fill in event content
    event.eventType      = MDP_EVENT;
    event.ioa            = mdpdata.commonElements.ioa;
    event.pSector        = pSector;
    event.InformationElement.diq   = mdpdata.value;

#if S14_LOCAL_EVENT_SUPPORT
    if(mdpdata.commonElements.eventLogStore == TMWDEFS_TRUE)
    {
        //fill in log event content
        event101.pointType = EVENT_TYPE_S101_DOUBLEBINARY;
        event101.value.valueDig = mdpdata.value;
        event101.protocolID = mdpdata.commonElements.ioa;
        event101.status = mdpdata.value;
        // Create a log event
        SLEventEntryStr         logEvent;
        logEvent.eventClass = event101.EventClassS101;
        logEvent.timestamp.sec = evtime.time.tv_sec;
        logEvent.timestamp.nsec = evtime.time.tv_nsec;
        logEvent.payload.eventSi870 = event101;
         // Log the Event in the event log
        eventlog.logEvent(logEvent);
    }
#endif

    return this->addEvent(event);
}

SCADAP_ERROR S101EventPublisher::addAnalogEvent(S101Analog& evtSrc, PointData& evtData)
{
    SCADAP_ERROR            err;
    TimeManager::TimeStr    evtime;
    TMWEvent                event;
    TMWSCTR*                pSector;

    /*Instance of Eventlog for event logging*/
    EventLogManager& eventlog = *(EventLogManager::getInstance());
    CTEventSi870PointStr     event101; // Create a local event for event log

    // Init event
    memset(&event,0,sizeof(event));

    /* Get sector handle
     * Note we should not pass the address of event.PSector cause it is packed!
     */
    err = evtSrc.getSectorHandle(&pSector);
    checkSCADAErrNone(err);
    const I870DataAnalog&  mmendata = evtSrc.getData();

    /* Generate event only when online */
    if( (mmendata.commonElements.eventOnlyWhenOnline == TMWDEFS_TRUE) &&
        ( (pSector->pSession->online == TMWSESN_STAT_OFFLINE) ||
          (pSector->pSession->online == TMWSESN_STAT_ERROR) )
        )
    {
        return SCADAP_ERROR_NONE;   //Do not generate any event
    }

    //Fill in event content
    event.ioa           = mmendata.commonElements.ioa;
    event.pSector       = pSector;
    event.flags         = mmendata.commonElements.flags;
    //Fill in local event content
    event101.protocolID = mmendata.commonElements.ioa;
    event101.status     = mmendata.commonElements.flags;

    // Fill in event time stamp
    evtData.getTime(evtime);
    convertTime(&evtime, &(event.timestamp));

    switch(evtSrc.getType())
    {
    case S101InputPoint::MMENC:
        //Fill in event content
        event.eventType      = MMENC_EVENT;
        event.InformationElement.NVC.value.fval   = mmendata.value.value.fval;

        //Fill in local event content
        event101.pointType = EVENT_TYPE_S101_ANALOGUE_FLOAT;
        event101.value.valueAna = mmendata.value.value.fval;
        break;
    case S101InputPoint::MMENB:
        //Fill in event content
        event.eventType      = MMENB_EVENT;
        event.InformationElement.NVC.value.sval   = mmendata.value.value.sval;

        //fill in local event content
        event101.pointType = EVENT_TYPE_S101_ANALOGUE_SCALED;
        event101.value.valueScaled = mmendata.value.value.sval;
        break;
    case S101InputPoint::MMENA:
        //Fill in event content
        event.eventType      = MMENA_EVENT;
        event.InformationElement.NVC.value.sval   = mmendata.value.value.sval;

        //fill in local event content
        event101.pointType = EVENT_TYPE_S101_ANALOGUE_NORMAL;
        //Convert back from 16-bit storage to Normalized value [-1, 1)
        event101.value.valueAna = ((lu_float32_t)mmendata.value.value.sval /
                                   (lu_float32_t)S101Analog::NORMALIZE_SCALING_FACTOR);
        break;
    default:
        return SCADAP_ERROR_NONE;   //Unsupported: ignored
    }
#if S14_LOCAL_EVENT_SUPPORT
    if(mmendata.commonElements.eventLogStore == TMWDEFS_TRUE)
    {
        // Create a log event
        SLEventEntryStr         logEvent;
        logEvent.eventClass = event101.EventClassS101;
        logEvent.timestamp.sec = evtime.time.tv_sec;
        logEvent.timestamp.nsec = evtime.time.tv_nsec;
        logEvent.payload.eventSi870 = event101;
         // Log the Event in the event log
        eventlog.logEvent(logEvent);
    }
#endif

    return this->addEvent(event);
}


SCADAP_ERROR S101EventPublisher::addMITEvent(S101MIT& evtSrc, PointData& evtData)
{
    SCADAP_ERROR            err;
    TimeManager::TimeStr    evtime;
    TMWEvent                event;
    TMWSCTR*                pSector;

    /*Instance of Eventlog for event logging*/
    EventLogManager& eventlog = *(EventLogManager::getInstance());
    CTEventSi870PointStr     event101; // Create a local event for event log

    // Init event
    memset(&event,0,sizeof(event));

    /* Get sector handle
     * Note we should not pass the address of event.PSector cause it is packed!
     */
    err = evtSrc.getSectorHandle(&pSector);
    checkSCADAErrNone(err);
    const I870DataMIT&  mitdata = evtSrc.getData();

    /* Generate event only when online */
    if( (mitdata.commonElements.eventOnlyWhenOnline == TMWDEFS_TRUE) &&
        ( (pSector->pSession->online == TMWSESN_STAT_OFFLINE) ||
          (pSector->pSession->online == TMWSESN_STAT_ERROR) )
        )
    {
        return SCADAP_ERROR_NONE;   //Do not generate any event
    }

    // Fill in event time stamp
    evtData.getTime(evtime);
    convertTime(&evtime, &(event.timestamp));

    // Fill in event content
    event.eventType      = MIT_EVENT;
    event.ioa            = mitdata.commonElements.ioa;
    event.pSector        = pSector;
    event.InformationElement.bcr   = mitdata.value;
    event.flags          = mitdata.commonElements.flags;

#if S14_LOCAL_EVENT_SUPPORT
    if(mitdata.commonElements.eventLogStore == TMWDEFS_TRUE)
    {
        //fill in log event content
        event101.pointType = EVENT_TYPE_S101_COUNTER;
        event101.value.valueDig = mitdata.value;
        event101.protocolID = mitdata.commonElements.ioa;
        event101.status = mitdata.commonElements.flags;
        // Create a log event
        SLEventEntryStr         logEvent;
        logEvent.eventClass = event101.EventClassS101;
        logEvent.timestamp.sec = evtime.time.tv_sec;
        logEvent.timestamp.nsec = evtime.time.tv_nsec;
        logEvent.payload.eventSi870 = event101;
         // Log the Event in the event log
        eventlog.logEvent(logEvent);
    }
#endif

    return this->addEvent(event);
}




void S101EventPublisher::publishEvents()
{
    lu_int32_t  ret = 0;
    TMWEvent    event;
    struct timeval eventReadTimeout = { 0, PROTOCOL_POOL_DELAY_US};

    while ((ret == 0) && (timeval_to_ms(&eventReadTimeout) > 0))
    {
        {
            LockingMutex locker(m_eventPipeLock);

            if ((ret = m_eventPipe.readP((lu_uint8_t*) &event, (lu_uint32_t) sizeof(TMWEvent), &eventReadTimeout)) != 0)
            {
                /* Fail to read event from pipe*/
                if(ret < 0)
                {
                    log.error("Error %i reading S101 event pipe; errno:[%d]", ret, errno);
                    DBG_ERR("%s read s101 event error:%d ", TITLE,  errno);
                }
                continue;
            }
        }

        /* Reading event succeeded, publish event*/
        DBG_TMW_EVENT_PUBLISH(event);

        switch(event.eventType)
        {
            case MSP_EVENT:
                s14msp_addEvent(event.pSector,
                            I14DEF_COT_SPONTANEOUS,//TMWTYPES_UCHAR cot,
                            event.ioa,//TMWTYPES_ULONG ioa,
                            event.InformationElement.siq,//TMWTYPES_UCHAR siq,
                            &(event.timestamp)
                            );
                break;
            case MDP_EVENT:
                s14mdp_addEvent(event.pSector,
                            I14DEF_COT_SPONTANEOUS,//TMWTYPES_UCHAR cot,
                            event.ioa,//TMWTYPES_ULONG ioa,
                            event.InformationElement.diq,//TMWTYPES_UCHAR diq,
                            &(event.timestamp)
                            );
                break;
            case MMENA_EVENT:
                s14mmena_addEvent(event.pSector,
                            I14DEF_COT_SPONTANEOUS,//TMWTYPES_UCHAR cot,
                            event.ioa,//TMWTYPES_ULONG ioa,
                            event.InformationElement.NVC.value.sval,//TMWTYPES_SFLOAT NVC,
                            event.flags,
                            &(event.timestamp)
                            );
                break;
            case MMENB_EVENT:
                s14mmenb_addEvent(event.pSector,
                            I14DEF_COT_SPONTANEOUS,//TMWTYPES_UCHAR cot,
                            event.ioa,//TMWTYPES_ULONG ioa,
                            event.InformationElement.NVC.value.sval,//TMWTYPES_SFLOAT NVC,
                            event.flags,
                            &(event.timestamp)
                            );
                break;
            case MMENC_EVENT:
                s14mmenc_addEvent(event.pSector,
                            I14DEF_COT_SPONTANEOUS,//TMWTYPES_UCHAR cot,
                            event.ioa,//TMWTYPES_ULONG ioa,
                            event.InformationElement.NVC.value.fval,//TMWTYPES_SFLOAT NVC,
                            event.flags,
                            &(event.timestamp)
                            );
                break;
            case MIT_EVENT:
                s14mit_addEvent(event.pSector,
                            I14DEF_COT_SPONTANEOUS,//TMWTYPES_UCHAR cot,
                            event.ioa,//TMWTYPES_ULONG ioa,
                            event.InformationElement.bcr,
                            event.flags,
                            &(event.timestamp)
                            );
                break;
            default:
                DBG_ERR("%s unsupported S101 event type:%d",TITLE,event.eventType);
        }
    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
SCADAP_ERROR S101EventPublisher::addEvent(TMWEvent& event)
{
    if(event.pSector == NULL)
    {
        DBG_ERR("%s cannot add TMW s14 event, sector is null",TITLE);
        return SCADAP_ERROR_NULL_POINTER;
    }

    lu_int32_t ret;
    LockingMutex locker(m_eventPipeLock);

    ret = m_eventPipe.writeP((lu_uint8_t *) (&event), (lu_uint32_t) sizeof(TMWEvent));

    DBG_INFO("%s added event, sector:%p ret:%d ", TITLE, event.pSector, ret);

    return (ret > 0) ? SCADAP_ERROR_NONE : SCADAP_ERROR_EVENT_PIPE;
}


/*
 *********************** End of file ******************************************
 */
