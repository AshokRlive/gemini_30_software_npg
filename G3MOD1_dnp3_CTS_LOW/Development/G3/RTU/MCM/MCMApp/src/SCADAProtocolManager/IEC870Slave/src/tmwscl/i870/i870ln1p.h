/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i870ln1p.h
 * description: This file is intended for internal SCL use only.
 *  IEC 60870-5 FT12 Link Layer Implementation
 */
#ifndef I870LN1P_DEFINED
#define I870LN1P_DEFINED

#include "tmwscl/utils/tmwlink.h"
#include "tmwscl/utils/tmwchnl.h"
#include "tmwscl/i870/i870ft12.h"
#include "tmwscl/i870/i870lnk1.h"

/* i870lnk1 Context */
typedef enum {
  RX_STATE_READ_FIXED,
  RX_STATE_READ_HEADER,
  RX_STATE_READ_VARIABLE,
  RX_STATE_IDLE
} RX_STATE;

typedef struct i870lnk1Context {
  /* Generic link layer info, must be first entry */
  TMWLINK_CONTEXT tmw;

  /* Configuration */
  TMWTYPES_USHORT rxFrameSize;
  TMWTYPES_MILLISECONDS rxFrameTimeout;
  TMWTYPES_MILLISECONDS confirmTimeout;
  TMWTYPES_MILLISECONDS testFramePeriod;
  TMWTYPES_UCHAR maxRetries;
  TMWTYPES_UCHAR linkAddressSize; 
  TMWDEFS_LINK_MODE linkMode; 
  TMWDEFS_LINKCNFM confirmMode;
  TMWTYPES_BOOL oneCharAckAllowed; 
  TMWTYPES_BOOL oneCharResponseAllowed;

  /* Receive */
  RX_STATE rxState;
  TMWTIMER rxFrameTimer;
  TMWTYPES_UCHAR rxControl;
  TMWTYPES_USHORT rxAddress;
  TMWTYPES_BOOL broadcast;
  TMWSESN_RX_DATA rxFrame;
  TMWTYPES_UCHAR rxFrameBuffer[I870LNK1_MAX_RX_BUFFER_LENGTH];  
  TMWTYPES_USHORT rxCurrentLength;
  TMWTYPES_USHORT rxExpectedLength;

  /* Transmit */
  TMWTIMER confirmTimer;
  TMWTYPES_UCHAR retryCount;

  /* Pointer to session we expect to receive a response from.
   * This could either be a link confirm or a response message of some kind.
   */
  TMWSESN *pCurrentSession;

  /* Keep track of whether a transmit is in progress 
   * This includes fixed or variable length messages that have been given
   * to physical layer but have not yet received _afterTxCallback.
   */
  TMWTYPES_BOOL channelIdle;

  TMWTYPES_UCHAR *pTxStartASDU;
  TMWTYPES_BOOL confirmExpected;
  /* allows event confirm mechanism for unbalanced 101 */
  TMWTYPES_BOOL expectEventConfirm;
  TMWSESN_TX_DATA *pLinkTxDescriptor;
  TMWPHYS_TX_DESCRIPTOR *pSecondaryTxDescriptor;
  TMWTYPES_UCHAR secondaryTxFrameBuffer[I870LNK1_MAX_TX_BUFFER_LENGTH];  
  TMWPHYS_TX_DESCRIPTOR variablePrimaryTxDescriptor;
  TMWPHYS_TX_DESCRIPTOR fixedSecondaryTxDescriptor;
  TMWTYPES_UCHAR fixedSecondaryTxFrameBuffer[I870FT12_LENGTH_FIX_ADDR2];

  TMWSESN *pLastSessionPolled;
  TMWTYPES_USHORT numberOfPollsSent;

  TMWTIMER balancedRetryTimer;
  TMWTIMER testFrameTimer;

  /* ->pPhysTransmit() has been called, but _afterTxCallback or failedTxCallback
   * has not been called yet
   */
  TMWTYPES_BOOL transmitInProgress;

  /* TRUE if a fixed frame cannot be sent because a previous transmit 
   * is still in progress.
   */
  TMWTYPES_BOOL needFixedFrameXmt;

  /* User callback function to be called when message is given to link layer
   * Most implementations will not need to provide this callback function. This 
   * allows the message to be modified before it is transmitted for test purposes,
   * for example to modify some bytes or introduce errors.
   */
  I870LNK1_TX_CALLBACK_FUNC pUserTxCallback;
  void *pCallbackParam;

} I870LNK1_CONTEXT;

/* FT1.2 Link Layer Specific Session Info */

/* Maintain session state */
typedef enum {
  I870LNK1_SESSION_STATE_STARTUP,
  I870LNK1_SESSION_STATE_REQUEST_STATUS,
  I870LNK1_SESSION_STATE_RESET_PENDING,
  I870LNK1_SESSION_STATE_READY,
} I870LNK1_SESSION_STATE;

/* 
 * return value 
 *   TMWTYPES_TRUE indicates 
 *   TMWTYPES_FALSE indicates 
 */
typedef TMWTYPES_BOOL (*I870LN1P_PENDING_CB_FUNC)(
  TMWSESN *pSession);

typedef struct { 
 
  /* For an unbalanced master, if TMWDEFS_TRUE SCL will send Class 1 and Class 2 polls
   * according to the following parameters. If this is set to TMWDEFS_FALSE
   * the SCL will not send these polls automatically. The user code would need
   * to call m103brm_sendPoll() to cause a Class 1 or Class 2 poll to be sent.
   * This is almost always set to TRUE. Setting this to FALSE allows SCL to work
   * with slaves that do not respond to class poll with I870FT12_FC_RESPOND_NO_DATA  
   * when they have no data to send.
   */
  TMWTYPES_BOOL                autoClassPolling; 

  /* For an unbalanced master, the total number of consecutive
   * Class 1 and Class 2 request frames that may be sent to one device when
   * an application layer response message is pending from this device, before
   * moving on to the next device on a multidrop network. This parameter
   * has no effect if only one session is configured for a communication channel.
   */
  TMWTYPES_USHORT             classPendingCount;
 
  /* For an unbalanced master, the total number of consecutive
   * Class 1 request frames that may be sent to one device before
   * moving on to the next device on a multidrop network. (Class 2 is always 
   * limited to one request frame unless an application layer response is pending). 
   * This parameter has no effect if only one session is configured for a 
   * communication channel.
   */
  TMWTYPES_USHORT             class1PollCount;
 
  /* For an unbalanced master, the minimum delay in milliseconds before a Class 1
   * or Class 2 request will be sent. Separate parameters are provided for Class 1
   * and Class 2 and if an application layer response is pending for this session.
   * These parameters may be used to limit the bandwidth used on a shared media 
   * like ethernet or to prevent taxing the target device with unnecessary 
   * communication overhead.
   */
  TMWTYPES_MILLISECONDS       class1PendingDelay;
  TMWTYPES_MILLISECONDS       class1PollDelay;
  TMWTYPES_MILLISECONDS       class2PendingDelay;
  TMWTYPES_MILLISECONDS       class2PollDelay;

  /* Callback function to determine if an application layer response is pending 
   * on this session 
   */
  I870LN1P_PENDING_CB_FUNC isRequestPending;

  /* For an unbalanced slave this parameter specifies how long to allow
   * between received frames before declaring this session offline.
   * Unbalanced slaves will never transmit a message that has not been
   * requested from the remote device, hence unbalanced slaves have no
   * way of determining if the remote device is online or not. In most
   * systems however an unbalanced slave will be polled for data at regular
   * intervals. This parameter uses the lack of a data poll to determine
   * that the remote device is offline. Set this parameter to 0 to disable
   * this feature. This parameter is ignored for balanced links and on
   * unbalanced masters.
   */
  TMWTYPES_MILLISECONDS    maxPollDelay;

  /* For IEC 60870-5-103, specifies whether to send RESET FCB or RESET CU. 
   * Normally a RESET FCB is sent, but a RESET CU will also clear the transmit 
   * buffers on the receiving slave device.
   */
  TMWTYPES_BOOL            send103ResetFCB;
} I870LN1P_SESN_CONFIG;

typedef struct { 
  TMWTYPES_BOOL            autoClassPolling;
  TMWTYPES_BOOL            classPollSent;
 
  /* Tell the primary station to stop sending data by setting DFC bit in some responses.
   * This should cause the master (or balanced slave) to stop sending data.
   * If a 103 master ignores this bit and continues to send data the slave should
   * discard the data and send CONFIRM NACK back to master. Our link layer behaves as 101
   * specifies, ie the slave can still accept the data and send CONFIRM with DFC==1.
   */
  TMWTYPES_BOOL            sendDFC1;     

  /* We have been told to stop sending data by the remote device */
  TMWTYPES_BOOL            DFC1Received; 

  /* send RESET FCB (or RESET CU) only applies to 103 protocol */
  TMWTYPES_BOOL            send103ResetFCB;

  TMWTYPES_USHORT          classPendingCount;
  TMWTYPES_USHORT          class1PollCount;
  TMWTYPES_MILLISECONDS    class1PendingDelay;
  TMWTYPES_MILLISECONDS    class1PollDelay;
  TMWTYPES_MILLISECONDS    class2PendingDelay;
  TMWTYPES_MILLISECONDS    class2PollDelay;
  I870LN1P_PENDING_CB_FUNC isRequestPending;
  TMWTIMER                 pollTimer;

  TMWTYPES_MILLISECONDS    maxPollDelay;

  TMWTIMER                 testFrameTimer;
  TMWTYPES_BOOL            txFrameCountBit;
  TMWTYPES_BOOL            rxFrameCountBit;
  TMWTYPES_BOOL            class1DataAvailable;
  TMWTYPES_BOOL            balancedMasterRestart;

  /* Indicates what type of fixed Frame needs to be sent
   * This gets set if another frame is at the physical layer
   * being transmitted 
   */
  TMWTYPES_UCHAR           fixedControlNeeded;

  /* Secondary needs to receive a reset before it can respond, 
   * except to reset or link status request.
   * In unbalanced mode; master is primary, slave is secondary.
   * In balanced mode; master is both primary and secondary,
   *  and slave is both primary and secondary.
   */
  TMWTYPES_BOOL            secondaryNotReset;

  I870LNK1_SESSION_STATE   sesnState;
} I870LNK1_SESSION_INFO;

#endif /* I870LN1P_DEFINED */
