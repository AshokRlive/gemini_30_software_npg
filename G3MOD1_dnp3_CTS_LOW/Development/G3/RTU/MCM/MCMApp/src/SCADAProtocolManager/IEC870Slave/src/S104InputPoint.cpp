/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:S104InputPoint.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       Contains all the functionality(classes) with regards to updating/getting
 *       values from the different Slave101 input points: single point(MSP),
 *       double point(MDP), floating analog(scaled,normalized and floating),
 *       and integrated totals.
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14 Oct 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "bitOperations.h"
#include "IEC870Common.h"
#include "S104Debug.h"
#include "S104InputPoint.h"
#include "S104Database.h"
#include "S104Sector.h"
#include "S104Session.h"
#include "S104Channel.h"
#include "S104EventPublisher.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
void S104InputPoint::setDatabase(S104Database* db)
{
    this->mp_db = db;
}

SCADAP_ERROR S104InputPoint::getSectorHandle(TMWSCTR** ppSectorHdle)
{
    checkNotNull(mp_db,"No sector database");

    return mp_db->getSectorHandle(ppSectorHdle);
}


/*****************MSP Class definitions****************************************/
S104MSP::S104MSP(S104InputPoint::Config& conf) : S104InputPoint(S104InputPoint::MSP, conf.vpointID)
{
    m_data.commonElements                 = conf.pointAttributes;
    m_data.value                          = TMWDEFS_FALSE;
}


S104MSP::~S104MSP()
{

}


void S104MSP::update(PointIdStr pointID, PointData* pointDataPtr)
{
    LU_UNUSED(pointID);

    DBG_INFO("%s MSP IOA=%lu updated. id:[%s]", __AT__, m_data.commonElements.ioa, pointID.toString().c_str());

    /* Update time*/
    TimeManager::TimeStr    evtime;

    // Fill in event time stamp
    pointDataPtr->getTime(evtime);
    convertTime(&evtime, &(m_data.commonElements.dtime));

    /* update VALUE */

    if((lu_uint8_t)(*pointDataPtr) == I14DEF_SIQ_OFF)
    {
        LU_SETBITMASKVALUE(m_data.value,I14DEF_SIQ_MASK, I14DEF_SIQ_OFF, 0);
    }
    else if((lu_uint8_t)(*pointDataPtr) == I14DEF_SIQ_ON)
    {
        LU_SETBITMASKVALUE(m_data.value,I14DEF_SIQ_MASK, I14DEF_SIQ_ON, 0);
    }
    /* Update FLAGS */

    /*Update Not Topical flag*/
    LU_SETALLMASKBITS(m_data.value, I14DEF_QUALITY_NT,
                         (pointDataPtr->getQuality() == POINT_QUALITY_OFF_LINE)? 1 : 0);
    /*Update Invalid flag*/
    LU_SETALLMASKBITS(m_data.value, I14DEF_QUALITY_IV,
                         (pointDataPtr->getInvalidFlag() == true)? 1 : 0);
    /*Update Blocked flag*/
    LU_SETALLMASKBITS(m_data.value, I14DEF_QUALITY_BL,
                         (pointDataPtr->getChatterFlag() == true)? 1 : 0);


    // Generate event only if spontaneous flag is enabled
    if( (m_data.commonElements.spontaneousEvent) && !(pointDataPtr->getInitialFlag()) )
    {
        g_s14EventProc.addMSPEvent(*this, *pointDataPtr);
    }

}


/*****************MDP Class definitions****************************************/
S104MDP::S104MDP(S104InputPoint::Config& conf):S104InputPoint(S104InputPoint::MDP, conf.vpointID)
{
    m_data.commonElements           = conf.pointAttributes;
    m_data.value                    = TMWDEFS_DPI_INTERMEDIATE;
}


S104MDP::~S104MDP()
{}


void S104MDP::update(PointIdStr pointID, PointData* pointDataPtr)
{
    LU_UNUSED(pointID);

    DBG_INFO("%s MDP IOA=%lu updated. id:[%s]", __AT__, m_data.commonElements.ioa, pointID.toString().c_str());

    /* Update time*/
    TimeManager::TimeStr    evtime;

    // Fill in event time stamp
    pointDataPtr->getTime(evtime);
    convertTime(&evtime, &(m_data.commonElements.dtime));

      /* update VALUE */
      if((lu_uint8_t)(*pointDataPtr) == I14DEF_DIQ_INTERMEDIATE)
      {
          LU_SETBITMASKVALUE(m_data.value,I14DEF_DIQ_MASK,I14DEF_DIQ_INTERMEDIATE,0);
      }

      else if((lu_uint8_t)(*pointDataPtr) == I14DEF_DIQ_OFF)
      {
          LU_SETBITMASKVALUE(m_data.value,I14DEF_DIQ_MASK,I14DEF_DIQ_OFF,0);
      }
      else if((lu_uint8_t)(*pointDataPtr) == I14DEF_DIQ_ON)
      {
          LU_SETBITMASKVALUE(m_data.value,I14DEF_DIQ_MASK,I14DEF_DIQ_ON,0);
      }

      else if((lu_uint8_t)(*pointDataPtr) == I14DEF_DIQ_INDETERMINATE)
      {
          LU_SETBITMASKVALUE(m_data.value,I14DEF_DIQ_MASK,I14DEF_DIQ_INDETERMINATE,0);
      }

      /* Update FLAGS */

      /*Update Not Topical flag*/
      LU_SETALLMASKBITS(m_data.value, I14DEF_QUALITY_NT,
                           (pointDataPtr->getQuality() == POINT_QUALITY_OFF_LINE)? 1 : 0);
      /*Update Invalid flag*/
      LU_SETALLMASKBITS(m_data.value, I14DEF_QUALITY_IV,
                           (pointDataPtr->getInvalidFlag() == true)? 1 : 0);
      /*Update Blocked flag*/
      LU_SETALLMASKBITS(m_data.value, I14DEF_QUALITY_BL,
                           (pointDataPtr->getChatterFlag() == true)? 1 : 0);

      // Generate event only if spontaneous flag is enabled
      if( (m_data.commonElements.spontaneousEvent) && !(pointDataPtr->getInitialFlag()) )
      {
          g_s14EventProc.addMDPEvent(*this, *pointDataPtr);
      }
}


/*****************Analog Class definitions****************************************/
S104Analog::S104Analog(ProtPointType type,S104InputPoint::Config& conf):S104InputPoint(type,conf.vpointID)
{
    m_data.commonElements             = conf.pointAttributes;
    m_data.commonElements.flags       = 0;

    if(type == S104InputPoint::MMENC )
    {
        m_data.value.type                 = TMWTYPES_ANALOG_TYPE_SFLOAT;
        m_data.value.value.fval           = 0;
    }
    else if(type == S104InputPoint::MMENB)
    {
        m_data.value.type                 = TMWTYPES_ANALOG_TYPE_SHORT;
        m_data.value.value.sval          = 0;
    }
    else if(type == S104InputPoint::MMENA)
    {
        m_data.value.type                 = TMWTYPES_ANALOG_TYPE_SHORT;
        m_data.value.value.sval          = 0;
    }
}


S104Analog::~S104Analog()
{}


void S104Analog::update(PointIdStr pointID, PointData* pointDataPtr)
{
    //DBG_INFO("%s Analogue IOA=%lu updated. id:%s", __AT__, m_data.commonElements.ioa, pointID.toString().c_str());

    PointDataFloat32 min;
    PointDataFloat32 max;

    /* Update time*/
    TimeManager::TimeStr    evtime;

    // Fill in event time stamp
    pointDataPtr->getTime(evtime);
    convertTime(&evtime, &(m_data.commonElements.dtime));

    TMWTYPES_UCHAR lastFlags = m_data.commonElements.flags;  //Store previous flag state
    bool isCounter; //States if the source value comes from a counter VP instead
    isCounter = mp_db->getSector().getSession().getChannel().getG3Database().getPointType(pointID) == POINT_TYPE_COUNTER;

    switch(m_type)
    {
        case S104InputPoint::MMENC :

            m_data.value.value.fval = (lu_float32_t)(*pointDataPtr);

        break;

        case S104InputPoint::MMENB :
            /*get the minimum and maximum of the particular point ID*/
            mp_db->getSector().getSession().getChannel().getG3Database().getPointRange(pointID, &min,&max);
            m_data.value.value.sval = IEC870Scaled(*pointDataPtr, min, max);
        break;

        case S104InputPoint::MMENA:
            /*get the minimum and maximum of the particular point ID*/
            mp_db->getSector().getSession().getChannel().getG3Database().getPointRange(pointID, &min,&max);
            //Store Normalized value [-1, 1) into 16-bit
            m_data.value.value.sval = (TMWTYPES_SHORT)(NORMALIZE_SCALING_FACTOR * IEC870Normalized(*pointDataPtr, min, max));
        break;

        default :
            DBG_INFO("%s Invalid analog type of ID %s", __AT__, pointID.toString().c_str());
        break;

    }

    /* Update FLAGS common to analog values*/

    if(!isCounter)
    {
        /*Update Overflow flag*/
        LU_SETALLMASKBITS(m_data.commonElements.flags, I14DEF_QDS_OV,
                             (pointDataPtr->getOverflowFlag() == true)? 1 : 0);
    }

    /*Update Invalid flag*/
    LU_SETALLMASKBITS(m_data.commonElements.flags, I14DEF_QUALITY_IV,
                         (pointDataPtr->getInvalidFlag() == true)? 1 : 0);

    /*Update Not Topical flag*/
    LU_SETALLMASKBITS(m_data.commonElements.flags, I14DEF_QUALITY_NT,
                         (pointDataPtr->getQuality() == POINT_QUALITY_OFF_LINE)? 1 : 0);

    /* Report points to Master only when flag changes, or filter says it (includes sanity checks)*/
    if( (mp_db->getSector().getSession().getChannel().getProtocol() != NULL) &&
         m_data.commonElements.spontaneousEvent &&        //event only if spontaneous flag is enabled
         ( (lastFlags != m_data.commonElements.flags) ||  //change in flags
           isCounter ||                                   //Source is counter
           ( (!isCounter) &&                              //Source is analogue and filter applied
             (pointDataPtr->getAFilterStatus() == AFILTER_RESULT_OUT_LIMIT_THR)
           )
         )
         && !(pointDataPtr->getInitialFlag())             //Not the initial value
      )
    {
        g_s14EventProc.addAnalogEvent(*this, *pointDataPtr);
    }
}


/*****************MIT Class definitions****************************************/
S104MIT::S104MIT(S104InputPoint::Config& conf) :
                                                S104InputPoint(S104InputPoint::MIT,conf.vpointID),
                                                m_type(S104MIT::COUNTER_TYPE_FROZEN),
                                                m_seqNum(0)
{
    m_data.commonElements = conf.pointAttributes;
    m_data.value = 0;
    m_data.mitFreeze = NULL;
    m_data.pG3DB = NULL;
    m_data.vPoint.group = conf.vpointID.group;
    m_data.vPoint.ID = conf.vpointID.ID;
}


S104MIT::~S104MIT()
{
}


void S104MIT::update(PointIdStr pointID, PointData* pointDataPtr)
{
    LU_UNUSED(pointID);

    DBG_INFO("%s Integrated totals IOA=%lu updated. id:%s", __AT__, m_data.commonElements.ioa, pointID.toString().c_str());

    if( ((pointDataPtr->getEdge() == EDGE_TYPE_POSITIVE) &&
         (m_type == S104MIT::COUNTER_TYPE_FROZEN)) || //Current count event for frozen 104 point
        ((pointDataPtr->getEdge() != EDGE_TYPE_POSITIVE) &&
         (m_type == S104MIT::COUNTER_TYPE_CURRENT)) //Frozen count event for nonfrozen 104 point
        )
    {
        //Discard point event
        return;
    }
    /* Update time*/
    TimeManager::TimeStr    evtime;

    // Fill in event time stamp
    pointDataPtr->getTime(evtime);
    convertTime(&evtime, &(m_data.commonElements.dtime));

    /* update VALUE */
    m_data.value = (lu_uint32_t)(*pointDataPtr);

    /* update Flags */
    /*Update overflow flag*/
    LU_SETALLMASKBITS(m_data.commonElements.flags, I14DEF_BCR_CY,
                         (pointDataPtr->getOverflowFlag() == true)? 1 : 0);

    /*Update Invalid flag*/
    LU_SETALLMASKBITS(m_data.commonElements.flags, I14DEF_BCR_IV,
                         (pointDataPtr->getInvalidFlag() == true)? 1 : 0);

    /*Update "counter adjusted" flag*/
    LU_SETALLMASKBITS(m_data.commonElements.flags, I14DEF_BCR_CA,
                         (pointDataPtr->getInitialFlag() == true)? 1 : 0);

    /* Update Binary Counter sequence number */
    LU_SETBITMASKVALUE(m_data.commonElements.flags, I14DEF_BCR_SEQ_MASK, m_seqNum, 0);
    m_seqNum = (m_seqNum + 1) % I14DEF_BCR_SEQ_MASK;

    // Generate event only if spontaneous flag is enabled
    if( (m_data.commonElements.spontaneousEvent) && !(pointDataPtr->getInitialFlag()) )
    {
        g_s14EventProc.addMITEvent(*this, *pointDataPtr);
    }
}


/*******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
