/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/
/* file: s101mem.c
 * description:  Implementation of Slave 101 specific utility functions.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/utils/tmwtarg.h"
#include "tmwscl/i870/i870dia1.h"

#include "tmwscl/i870/s101mem.h"
#include "tmwscl/i870/s101sesn.h"
#include "tmwscl/i870/s101sctr.h"
#include "tmwscl/i870/s14diag.h"

typedef struct{
  TMWMEM_HEADER     header;
  S101SESN          data;
} S101MEM_SESN;

typedef struct{
  TMWMEM_HEADER     header;
  S101SCTR           data;
} S101MEM_SCTR;

static const TMWTYPES_CHAR *_nameTable[S101MEM_ALLOC_TYPE_MAX] = {
  "S101SESN", 
  "S101SCTR"
};

#if !TMWCNFG_USE_DYNAMIC_MEMORY
/* Use static allocated array of memory instead of dynamic memory */
static S101MEM_SESN          s101mem_sesns[S101CNFG_NUMALLOC_SESNS];
static S101MEM_SCTR          s101mem_sctrs[S101CNFG_NUMALLOC_SCTRS];
#endif

static TMWMEM_POOL_STRUCT _s101allocTable[S101MEM_ALLOC_TYPE_MAX];

#if TMWCNFG_USE_DYNAMIC_MEMORY
static void TMWDEFS_LOCAL _initConfig(
  S101MEM_CONFIG *pConfig)
{
  pConfig->numSessions = S101CNFG_NUMALLOC_SESNS;
  pConfig->numSectors  = S101CNFG_NUMALLOC_SCTRS;
}

void TMWDEFS_GLOBAL s101mem_initConfig(
  S101MEM_CONFIG  *pS101Config,
  S14MEM_CONFIG   *pS14MemConfig,
  I870MEM1_CONFIG *pI870Mem1Config,
  I870MEM_CONFIG  *pI870MemConfig,
  TMWMEM_CONFIG   *pTmwConfig)
{
  tmwmem_initConfig(pTmwConfig);
  i870mem_initConfig(pI870MemConfig);
  i870mem1_initConfig(pI870Mem1Config);
  s14mem_initConfig(pS14MemConfig);
  _initConfig(pS101Config);
}

/* function: s101mem_initMemory */
TMWTYPES_BOOL TMWDEFS_GLOBAL s101mem_initMemory(
  S101MEM_CONFIG  *pS101Config,
  S14MEM_CONFIG   *pS14MemConfig,
  I870MEM1_CONFIG *pI870Mem1Config,
  I870MEM_CONFIG  *pI870MemConfig,
  TMWMEM_CONFIG   *pTmwConfig)
{
  /* Initialize memory management and diagnostics for
   * 101 if not yet done 
   */ 
  if(!tmwmem_init(pTmwConfig))
    return TMWDEFS_FALSE;

  if(!tmwappl_getInitialized(TMWAPPL_INIT_S101))
  { 
    if(!s101mem_init(pS101Config))
      return TMWDEFS_FALSE;

    tmwappl_setInitialized(TMWAPPL_INIT_S101);

    if(!tmwappl_getInitialized(TMWAPPL_INIT_S104))
    { 
      if(!s14mem_init(pS14MemConfig))
        return TMWDEFS_FALSE;

#if TMWCNFG_SUPPORT_DIAG
      s14diag_init();
#endif
    }

    if(!tmwappl_getInitialized(TMWAPPL_INIT_I870))
    { 
      if(!i870mem_init(pI870MemConfig))
        return TMWDEFS_FALSE;

#if TMWCNFG_SUPPORT_DIAG
      i870diag_init();
#endif
      tmwappl_setInitialized(TMWAPPL_INIT_I870);
    }

    if(!tmwappl_getInitialized(TMWAPPL_INIT_FT12))
    { 
      if(!i870mem1_init(pI870Mem1Config))
        return TMWDEFS_FALSE;

#if TMWCNFG_SUPPORT_DIAG
      i870dia1_init();
#endif
      tmwappl_setInitialized(TMWAPPL_INIT_FT12);
    }
  }  
  
  return TMWDEFS_TRUE;
}
#endif

/* routine: s101mem_init */
TMWTYPES_BOOL TMWDEFS_GLOBAL s101mem_init(
  S101MEM_CONFIG  *pConfig)
{
#if TMWCNFG_USE_DYNAMIC_MEMORY
  /* dynamic memory allocation supported */
  S101MEM_CONFIG  config; 

  /* If caller has not specified memory pool configuration, use the
   * default compile time values 
   */
  if(pConfig == TMWDEFS_NULL)
  {
    pConfig = &config;
    _initConfig(pConfig);
  }

  if(!tmwmem_lowInit(_s101allocTable, S101MEM_SESN_TYPE, pConfig->numSessions, sizeof(S101MEM_SESN), TMWDEFS_NULL))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_s101allocTable, S101MEM_SCTR_TYPE, pConfig->numSectors, sizeof(S101MEM_SCTR), TMWDEFS_NULL))
    return TMWDEFS_FALSE;
#else
  /* static memory allocation supported */  
  TMWTARG_UNUSED_PARAM(pConfig);  
  if(!tmwmem_lowInit(_s101allocTable, S101MEM_SESN_TYPE, S101CNFG_NUMALLOC_SESNS, sizeof(S101MEM_SESN), (TMWTYPES_UCHAR*)s101mem_sesns))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_s101allocTable, S101MEM_SCTR_TYPE, S101CNFG_NUMALLOC_SCTRS, sizeof(S101MEM_SCTR), (TMWTYPES_UCHAR*)s101mem_sctrs))
    return TMWDEFS_FALSE;
#endif  
  
  return TMWDEFS_TRUE;
}

/* function: s101mem_alloc */
void * TMWDEFS_GLOBAL s101mem_alloc(
  S101MEM_ALLOC_TYPE type)
{
  if(type >= S101MEM_ALLOC_TYPE_MAX)
  {
    return(TMWDEFS_NULL);
  }

  return(tmwmem_lowAlloc(&_s101allocTable[type]));
}

/* function: s101mem_free */
void TMWDEFS_GLOBAL s101mem_free(
  void *pBuf)
{    
  TMWMEM_HEADER *pHeader = TMWMEM_GETHEADER(pBuf);
  TMWTYPES_UCHAR   type = TMWMEM_GETTYPE(pHeader);

  if(type >= S101MEM_ALLOC_TYPE_MAX)
  {
    return;
  }

  tmwmem_lowFree(&_s101allocTable[type], pHeader);
}

/* function: s101mem_getUsage */
TMWTYPES_BOOL TMWDEFS_GLOBAL s101mem_getUsage(
  TMWTYPES_UCHAR index,
  const TMWTYPES_CHAR **name,
  TMWMEM_POOL_STRUCT *pStruct)
{
  if(index >= S101MEM_ALLOC_TYPE_MAX)
  {
    return(TMWDEFS_FALSE);
  }

  *name = _nameTable[index];
  *pStruct = _s101allocTable[index];
  return(TMWDEFS_TRUE);
}


