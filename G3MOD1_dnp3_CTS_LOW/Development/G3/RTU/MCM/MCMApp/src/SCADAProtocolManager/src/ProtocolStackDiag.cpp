/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:ProtocolStackDiag.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   9 Dec 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "ProtocolStackDiag.h"
#include "tmwscl/utils/tmwdiag.h"
#include "tmwscl/utils/tmwtargp.h"
#include "Logger.h"


#include "Debug.h"

#if (DEBUG_PROTOCOLSTACK_TRAFFIC == 0)
#include "DebugOff.h"
#endif


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
static void lucy_put_diag_string_func(const TMWDIAG_ANLZ_ID *pAnlzId,
        const TMWTYPES_CHAR *pString)
{
    LU_UNUSED(pAnlzId);

	// Output TMW diagnostics to Logger
    static Logger& log = Logger::getLogger(SUBSYSTEM_ID_PS_TRAFFIC);
    log.debug(pString);

	// Output TMW diagnostics to Console
    DBG_RAW("%s",pString);
}

void tmwdiag_initialize()
{
    tmwtargp_registerPutDiagStringFunc(lucy_put_diag_string_func);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
