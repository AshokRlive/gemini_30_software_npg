/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14diag.h
 * description: This file is intended for internal SCL use only.
 *  IEC 60870-5-101/104 Slave Diagnostics
 */
#ifndef S14DIAG_DEFINED
#define S14DIAG_DEFINED

#include "tmwscl/utils/tmwcnfg.h"
#include "tmwscl/i870/s14sesn.h"
#include "tmwscl/i870/s14sctr.h"

/* Define error numbers used by slave 101/104 protocols */
typedef enum {  
  S14DIAG_ALLOC_CYCLIC,
  S14DIAG_ALLOC_BACKGROUND,
  S14DIAG_INVALID_SIZE,
  S14DIAG_UNKNOWN_ASDU,
  S14DIAG_ALLOC_UNKNOWN,
  S14DIAG_UNKNOWN_SECTOR,
  S14DIAG_ALLOC_EVENT,
  S14DIAG_CMD_DELAYED,

#ifdef TMW_SUPPORT_MONITOR
  S14DIAG_MON_UNKNOWN_ASDU,
#endif

  /* This must be last entry */
  S14DIAG_ERROR_ENUM_MAX

} S14DIAG_ERROR_ENUM; 


#if !TMWCNFG_SUPPORT_DIAG

#define S14DIAG_ERROR(pSession, pSector, errorNumber) \
  TMWTARG_UNUSED_PARAM(pSession); TMWTARG_UNUSED_PARAM(pSector); TMWTARG_UNUSED_PARAM(errorNumber);

#else

#define S14DIAG_ERROR(pSession, pSector, errorNumber) \
  s14diag_error(pSession, pSector, errorNumber)

#ifdef __cplusplus
extern "C" {
#endif

  /* routine: s14diag_init
   * purpose: internal diagnostic init function
   * arguments:
   *  void
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL s14diag_init(void);
  
  /* routine: s14diag_validateErrorTable
   * purpose: Called only to verify if error message table is correct.
   *  This is intended for test purposes only.
   * arguments:
   *  void
   * returns:
   *  TMWDEFS_TRUE if formatted correctly
   *  TMWDEFS_FALSE if there is an error in the table.
   */
  TMWTYPES_BOOL s14diag_validateErrorTable(void);

  /* function: s14diag_error
   * purpose: Display error message
   * arguments:
   *  pChannel - channel from which this message originated
   *  pSession - session from which this message originated
   *  errorNumber - enum indicating what error message to display
   *  pExtraTextMsg - pointer to additional text to display with error msg
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL s14diag_error(
    TMWSESN *pSession,  
    TMWSCTR *pSector,
    S14DIAG_ERROR_ENUM errorNumber);

  /* function: s14diag_errorMsgEnable
   * purpose: Enable/Disable specific error message output
   * arguments:
   *  errorNumber - enum indicating what error message
   *  enabled - TMWDEFS_TRUE if error message should be enabled
   *            TMWDEFS_FALSE if error message should be disabled
   * returns:
   *  void
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14diag_errorMsgEnable(
    S14DIAG_ERROR_ENUM errorNumber,
    TMWTYPES_BOOL enabled);

#ifdef __cplusplus
}
#endif
#endif /* TMWCNFG_SUPPORT_DIAG */
#endif /* S14DIAG_DEFINED */
