/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i870sesn.c
 * description: Generic IEC 60870-5 session
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/i870sesn.h"
#include "tmwscl/i870/i870sctr.h"
#include "tmwscl/i870/i870util.h"
#include "tmwscl/i870/i870chnl.h"
#include "tmwscl/utils/tmwtarg.h"
 
/* function: _beforeTxCallback */
static void TMWDEFS_CALLBACK _beforeTxCallback(
  TMWSESN_TX_DATA *pTxData)
{
  /* Call beforeTxCallback function */
  /* This is not currently set anywhere, it is never called */
  if(pTxData->pBeforeTxCallback != TMWDEFS_NULL)
  {
    pTxData->pBeforeTxCallback(pTxData->pCallbackData, pTxData);
  }

  if((pTxData->txFlags & TMWSESN_TXFLAGS_STORE_16BIT_TIME) != 0)
  {
    TMWDTIME currentTime;
    TMWTYPES_UCHAR buf[2];

    /* Get current time */
    tmwdtime_getDateTime(pTxData->pSession, &currentTime);
    tmwtarg_store16(&currentTime.mSecsAndSecs, buf);

    /* Update last 2 bytes of message with time */
    pTxData->pSession->pChannel->pLink->pLinkUpdateMsg(
      pTxData->pSession->pChannel->pLinkContext,
      (TMWTYPES_USHORT)(pTxData->msgLength - 2), buf, 2);
  }
  else if((pTxData->txFlags & TMWSESN_TXFLAGS_STORE_56BIT_TIME) != 0)
  {
    I870SESN *pI870Session;
    TMWDTIME currentTime;
    TMWTYPES_UCHAR buf[7];

    /* Get current time */
    pI870Session = (I870SESN *)pTxData->pSession;
    tmwdtime_getDateTime((TMWSESN *)pI870Session, &currentTime);
    i870util_store56BitTime(buf, &currentTime, TMWDEFS_FALSE, pI870Session->useDayOfWeek);

    /* Update last 7 bytes of message with time */
    pTxData->pSession->pChannel->pLink->pLinkUpdateMsg(
      pTxData->pSession->pChannel->pLinkContext,
      (TMWTYPES_USHORT)(pTxData->msgLength - 7), buf, 7);
  }
}

/* function: _afterTxCallback */
static void TMWDEFS_CALLBACK _afterTxCallback(
  TMWSESN_TX_DATA *pTxData)
{
  /* Call afterTxCallback function */
  if(pTxData->pAfterTxCallback != TMWDEFS_NULL)
  {
    pTxData->pAfterTxCallback(pTxData->pCallbackData, pTxData);
  }

  if((pTxData->txFlags & TMWSESN_TXFLAGS_NO_RESPONSE) != 0)
  {
    i870chnl_cleanupMessage(pTxData, TMWDEFS_NULL, I870CHNL_RESP_STATUS_SUCCESS, 0);
  }
}

/* function: _failedTxCallback */ 
static void TMWDEFS_CALLBACK _failedTxCallback(
  TMWSESN_TX_DATA *pTxData)
{
  I870CHNL_TX_DATA *pI870ChnlTxData = (I870CHNL_TX_DATA *)pTxData;

  if((pI870ChnlTxData->state != I870CHNL_REDUNDANT_SWITCH) && (pTxData->pFailedTxCallback != TMWDEFS_NULL))
  {
    pTxData->pFailedTxCallback(pTxData->pCallbackData, pTxData);
  }

  if((pI870ChnlTxData->state != I870CHNL_CANCELED) && (pI870ChnlTxData->state != I870CHNL_REDUNDANT_SWITCH))
    i870chnl_cleanupMessage(pTxData, TMWDEFS_NULL, I870CHNL_RESP_STATUS_FAILURE, 0);
}


/* function: i870sesn_openSession */
TMWTYPES_BOOL TMWDEFS_GLOBAL i870sesn_openSession(
  TMWCHNL *pChannel,
  TMWSESN *pSession,
  void *pConfig,
  TMWSESN_STAT_CALLBACK pStatCallback,
  void *pStatCallbackParam,
  TMWTYPES_PROTOCOL protocol,
  TMWTYPES_SESSION_TYPE type)
{
  TMWTYPES_BOOL status;
  I870SESN *pI870Session = (I870SESN *)pSession;

  /* Open TMW Session */
  if(tmwsesn_openSession(pChannel, pSession, pStatCallback, pStatCallbackParam, protocol, type) == TMWDEFS_FALSE)
    return(TMWDEFS_FALSE);

  /* Tx Callback functions */
  pI870Session->pBeforeTxCallback = _beforeTxCallback;
  pI870Session->pAfterTxCallback = _afterTxCallback;
  pI870Session->pFailedTxCallback = _failedTxCallback;

  tmwdlist_initialize(&pI870Session->sectorList);

  /* Lock channel */
  TMWTARG_LOCK_SECTION(&pChannel->lock);

  /* Open Link Layer Session */
  status = pChannel->pLink->pLinkOpenSession(pChannel->pLinkContext, pSession, pConfig);
  
  /* Unlock channel */
  TMWTARG_UNLOCK_SECTION(&pChannel->lock);

  return(status);
}

/* function: i870sesn_closeSession */
TMWTYPES_BOOL TMWDEFS_GLOBAL i870sesn_closeSession(TMWSESN *pSession)
{
  I870SESN *pI870Session = (I870SESN *)pSession;

  /* Check for NULL since this would be a common error */
  if(pSession == TMWDEFS_NULL)
  {
    return(TMWDEFS_FALSE);
  }

  /* If there are any open sectors return false. 
   * User must manage open sectors, if we close them on 
   * their behalf, they would not know
   */
  if(tmwdlist_size(&pI870Session->sectorList) != 0)
  {
    return(TMWDEFS_FALSE);
  }

  /* Lock channel */
  TMWTARG_LOCK_SECTION(&pSession->pChannel->lock);

  /* Close Link Layer Session */
  pSession->pChannel->pLink->pLinkCloseSession(pSession->pChannel->pLinkContext, pSession);

  /* Close TMW Session */
  tmwsesn_closeSession(pSession);

  /* Unlock channel */
  TMWTARG_UNLOCK_SECTION(&pSession->pChannel->lock);

  return(TMWDEFS_TRUE);
}

/* function: i870sesn_lookupSector */
TMWSCTR * TMWDEFS_GLOBAL i870sesn_lookupSector(TMWSESN *pSession, TMWTYPES_USHORT sectorAddress)
{
  I870SESN *pI870Session = (I870SESN *)pSession;
  TMWSCTR *pSector = TMWDEFS_NULL;

  while((pSector = (TMWSCTR *)tmwdlist_getAfter(
    &pI870Session->sectorList, (TMWDLIST_MEMBER *)pSector)) != TMWDEFS_NULL)
  {
    /* If address matches this sectors address */
    if(((I870SCTR *)pSector)->asduAddress == sectorAddress)
    {
      return(pSector);
    }
  }

  return(TMWDEFS_NULL);
}

/* function: i870sesn_cancelMessage */
void TMWDEFS_GLOBAL i870sesn_cancelMessage(
  TMWSESN *pSession,
  TMWSESN_TX_DATA *pTxData)
{
  TMWTARG_UNUSED_PARAM(pSession);

  /* Send cancel to channel */
  i870chnl_cancelMessage(pTxData);
}
