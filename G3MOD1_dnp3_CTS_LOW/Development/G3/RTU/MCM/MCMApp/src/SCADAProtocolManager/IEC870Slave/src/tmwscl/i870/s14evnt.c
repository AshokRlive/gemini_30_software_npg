/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14evnt.c
 * description: This file is intended for internal SCL use only.
 *   Base class for IEC 60870-5-101/104 slave events types
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14evnt.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14mem.h"
#include "tmwscl/i870/s14ccsna.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/s14diag.h"
#include "tmwscl/i870/i870chnl.h"

#include "tmwscl/utils/tmwdlist.h"

/* XXX - Changed by Lucy */
#include "lucy/lucy_s14data.h"

/* function: _eventsConfirmed  
 * purpose: remove all events that have been sent from this queue 
 * arguments: 
 *  pCallbackParam - pointer to the event list
 *  pTxData - unused
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _eventsConfirmed(
  void *pCallbackParam,
  TMWSESN_TX_DATA *pTxData)
{
  TMWDLIST *pEventQueue = (TMWDLIST *)pCallbackParam;
  S14EVNT *pEvent;

  /* remove all of the events from the queue that have been marked as sent. */
  pEvent = (S14EVNT *)tmwdlist_getFirst(pEventQueue);
  while(pEvent != TMWDEFS_NULL)
  {
    /* Get next event now in case we delete this one */
    S14EVNT *pNextEvent = (S14EVNT *)tmwdlist_getNext((TMWDLIST_MEMBER *)pEvent);

    if(pEvent->eventSent && (pEvent->pTxData == pTxData))
    {
#ifdef TMWCNFG_SUPPORT_STATS
      {
      TMWSCTR_STAT_EVENT_STRUCT eventData;
      eventData.typeId = pEvent->typeId;
      eventData.ioa = pEvent->ioa;
      TMWSCTR_STAT_CALLBACK_FUNC(pTxData->pSector, TMWSCTR_STAT_EVENT_CONFIRM, &eventData);
      }
#endif

      /* XXX - Changed by Lucy */
// TODO - SKA - Call callback to delete event from Event Manager!
//
      // XXX - Changed by Lucy
      I870DBHandleStr *dbPtr = (I870DBHandleStr*) pTxData->pSector->pUserData;
      if(dbPtr->pIEC870DB)
      {
        /* We need to send the sessionID to the deleteEventID() method.  This is so
         * the relevant 'session bit' can be cleared against this event.
         */
        dbPtr->deleteEventID(pEvent->timeStamp.lucyEventID, dbPtr->pIEC870DB);
      }

      tmwdlist_removeEntry(pEventQueue, (TMWDLIST_MEMBER *)pEvent);

      s14mem_free(pEvent);
    }

    pEvent = pNextEvent;
  }
}

/* function: _eventsNotConfirmed  
 * purpose: mark all events in the queue as not sent since the message
 *  was not confirmed by the master
 * arguments: 
 *  pCallbackParam - pointer to the event list
 *  pTxData - unused
 * returns:
 *  void
 */
static void TMWDEFS_CALLBACK _eventsNotConfirmed(
  void *pCallbackParam,
  TMWSESN_TX_DATA *pTxData)
{
  TMWDLIST *pEventQueue = (TMWDLIST *)pCallbackParam;
  S14EVNT *pEvent;

  /* Mark the event as not sent. */
  pEvent = (S14EVNT *)tmwdlist_getFirst(pEventQueue);
  while(pEvent != TMWDEFS_NULL)
  {
    if(pEvent->eventSent && (pEvent->pTxData == pTxData))
    {
      pEvent->eventSent = TMWDEFS_FALSE;
    }
    pEvent = (S14EVNT *)tmwdlist_getNext((TMWDLIST_MEMBER *)pEvent);
  }
}

/* function: s14evnt_scanForChanges */
TMWTYPES_BOOL TMWDEFS_GLOBAL s14evnt_scanForChanges(
  TMWSCTR *pSector,
  S14EVNT_DESC *pDesc)
{
  S14SCTR *p14Sector;
  TMWTYPES_USHORT index = 0;
  TMWDTIME timeStamp;
  void *pPoint;
  TMWTYPES_BOOL eventsAdded = TMWDEFS_FALSE;

  /* If scanning not enabled on this sector return */
  if(!pDesc->scanEnabled) 
    return(TMWDEFS_FALSE);

  /* Initialize time to current time in case pChanged does not
   * update it.
   */
  s14util_getDateTime(pSector, &timeStamp);

  /* Scan all points of the specified type to see if they have changed */
  p14Sector = (S14SCTR *)pSector;
  while((pPoint = pDesc->pGetPointFunc(p14Sector->i870.pDbHandle, index)) != TMWDEFS_NULL)
  {
    if(pDesc->pChangedFunc(pSector, pPoint, &timeStamp))
    {
      eventsAdded = TMWDEFS_TRUE;

      /* Reset time for next event */
      s14util_getDateTime(pSector, &timeStamp);
    }

    index += 1;
  }

  return(eventsAdded);
}
 
/* function: s14evnt_addEvent */
S14EVNT * TMWDEFS_GLOBAL s14evnt_addEvent(
  TMWSCTR *pSector,
  TMWTYPES_UCHAR cot,
  TMWTYPES_ULONG ioa, 
  TMWTYPES_UCHAR quality,
  TMWDTIME *pTimeStamp,
  S14EVNT_DESC *pDesc,
  void **pDoubleTransEvent
  )
{
  int numberOfEventsToAdd = 1;
  S14EVNT *pEvent;
  TMWTARG_UNUSED_PARAM(pDoubleTransEvent);

  /* If mode is most recent, remove previous events from this point */
  if(pDesc->eventMode == TMWDEFS_EVENT_MODE_MOST_RECENT)
  {
    pEvent = (S14EVNT *)tmwdlist_getFirst(pDesc->pEventList);
    while(pEvent != TMWDEFS_NULL)
    {
      /* See if this event is for the same point as the new one */
      if(pEvent->ioa == ioa)
      {
        /* Yep, delete it */
        tmwdlist_removeEntry(pDesc->pEventList, (TMWDLIST_MEMBER *)pEvent);
#ifdef TMWCNFG_SUPPORT_STATS
        {
        TMWSCTR_STAT_EVENT_STRUCT eventData;
        eventData.typeId = pEvent->typeId;
        eventData.ioa = pEvent->ioa;
        TMWSCTR_STAT_CALLBACK_FUNC(pSector, TMWSCTR_STAT_EVENT_REMOVED, &eventData);
        }
#endif
        s14mem_free(pEvent); 
        
#if S14DATA_SUPPORT_DOUBLE_TRANS
        if(pDesc->doubleTransmission)
        { 
          pEvent = (S14EVNT *)tmwdlist_getFirst(pDesc->pEventList);
          while(pEvent != TMWDEFS_NULL)
          {
            /* See if this event is for the same point as the new one */
            if(pEvent->ioa == ioa)
            {
              /* Yep, delete it */
              tmwdlist_removeEntry(pDesc->pEventList, (TMWDLIST_MEMBER *)pEvent);
#ifdef TMWCNFG_SUPPORT_STATS
              {
              TMWSCTR_STAT_EVENT_STRUCT eventData;
              eventData.typeId = pEvent->typeId;
              eventData.ioa = pEvent->ioa;
              TMWSCTR_STAT_CALLBACK_FUNC(pSector, TMWSCTR_STAT_EVENT_REMOVED, &eventData);
              }
#endif
              s14mem_free(pEvent); 
              break;
            }
            pEvent = (S14EVNT *)tmwdlist_getNext((TMWDLIST_MEMBER *)pEvent);
          }
        }
#endif
        /* found a matching event, don't look for any more */
        break;
      }

      pEvent = (S14EVNT *)tmwdlist_getNext((TMWDLIST_MEMBER *)pEvent);
    }
  }

  /* Try to allocate new event */
  pEvent = (S14EVNT *)s14mem_alloc(pDesc->eventMemType);

#if S14DATA_SUPPORT_DOUBLE_TRANS
  if(pDoubleTransEvent != TMWDEFS_NULL)
    *pDoubleTransEvent = TMWDEFS_NULL;
  if(pDesc->doubleTransmission)
    numberOfEventsToAdd = 2;
#endif 
 
  /* Make sure event allocation ok and event limit not reached */
  if((pEvent == TMWDEFS_NULL) 
    || ((pDesc->maxEvents != 0)
    && ((tmwdlist_size(pDesc->pEventList) + numberOfEventsToAdd) > pDesc->maxEvents)))
  {
    S14EVNT *pRemoveEvent;
    S14SCTR *p14Sector = (S14SCTR *)pSector;

    /* Update stats */
    TMWSCTR_STAT_CALLBACK_FUNC(pSector, TMWSCTR_STAT_EVENT_OVERFLOW, &pDesc->woTimeTypeId);
    *pDesc->pEventsOverflowedFlag = TMWDEFS_TRUE;

    /* determine which event to delete, based on the configuration.
     */
    if(p14Sector->deleteOldestEvent) 
    {
      pRemoveEvent = (S14EVNT *)tmwdlist_getFirst(pDesc->pEventList);
      if(pRemoveEvent != TMWDEFS_NULL)
      {
        tmwdlist_removeEntry(pDesc->pEventList, (TMWDLIST_MEMBER *)pRemoveEvent);
#ifdef TMWCNFG_SUPPORT_STATS
        {
        TMWSCTR_STAT_EVENT_STRUCT eventData;
        eventData.typeId = pRemoveEvent->typeId;
        eventData.ioa = pRemoveEvent->ioa;
        TMWSCTR_STAT_CALLBACK_FUNC(pSector, TMWSCTR_STAT_EVENT_REMOVED, &eventData);
        }
#endif
        s14mem_free(pRemoveEvent);
      }
#if S14DATA_SUPPORT_DOUBLE_TRANS
      if(pDesc->doubleTransmission)
      {
        /* May need to remove another event to make room for double event transmission */
        if((pEvent == TMWDEFS_NULL) 
         || ((pDesc->maxEvents != 0)
         && ((tmwdlist_size(pDesc->pEventList) + numberOfEventsToAdd) > pDesc->maxEvents)))
        {
          pRemoveEvent = (S14EVNT *)tmwdlist_getFirst(pDesc->pEventList);
          if(pRemoveEvent != TMWDEFS_NULL)
          {
            tmwdlist_removeEntry(pDesc->pEventList, (TMWDLIST_MEMBER *)pRemoveEvent);
#ifdef TMWCNFG_SUPPORT_STATS
            {
            TMWSCTR_STAT_EVENT_STRUCT eventData;
            eventData.typeId = pRemoveEvent->typeId;
            eventData.ioa = pRemoveEvent->ioa;
            TMWSCTR_STAT_CALLBACK_FUNC(pSector, TMWSCTR_STAT_EVENT_REMOVED, &eventData);
            }
#endif
            s14mem_free(pRemoveEvent);
          }
        }
      }
#endif
    }
    else
    {
      /* If configured for delete newest event, just don't add this new event */
      if(pEvent!= TMWDEFS_NULL)
      {
        s14mem_free(pEvent);
      }
      return(TMWDEFS_NULL);
    }
   
    /* Allocate new event if previous allocation failed */
    if(pEvent == TMWDEFS_NULL)
    {
      pEvent = (S14EVNT *)s14mem_alloc(pDesc->eventMemType);

      if(pEvent == TMWDEFS_NULL)
      {
        S14DIAG_ERROR(pSector->pSession, pSector, S14DIAG_ALLOC_EVENT);
        return(TMWDEFS_NULL);
      }
    }
  }

  /* Initialize new event */
  pEvent->ioa = ioa;
  pEvent->typeId = pDesc->woTimeTypeId;
  pEvent->cot = cot;
  pEvent->quality = quality;
#if S14DATA_SUPPORT_CCSNA
  pEvent->ccsnaRespNumber = s14ccsna_getRespNumber(pSector);
#endif
#if S14DATA_SUPPORT_DOUBLE_TRANS
  pEvent->doubleTransmission = pDesc->doubleTransmission;
#endif
  if(pTimeStamp != TMWDEFS_NULL)
  {
    pEvent->timeSpecified = TMWDEFS_TRUE;
    pEvent->timeStamp = *pTimeStamp;
  }
  else
  {
    /* Allow no time stamp to be specified */
    pEvent->timeSpecified = TMWDEFS_FALSE;
  }
  pEvent->eventSent = TMWDEFS_FALSE;
  
#if S14DATA_SUPPORT_DOUBLE_TRANS
  if(pEvent->doubleTransmission)
  {
    /* put this before all events that are not double transmission */
    S14EVNT *pEvent2 = (S14EVNT *)tmwdlist_getFirst(pDesc->pEventList);
    while(pEvent2 != TMWDEFS_NULL)
    {
      if(!pEvent2->doubleTransmission)
      { 
        tmwdlist_insertEntryBefore(pDesc->pEventList, (TMWDLIST_MEMBER *)pEvent2, (TMWDLIST_MEMBER *)pEvent); 
        break;
      }
      pEvent2 = (S14EVNT *)tmwdlist_getNext((TMWDLIST_MEMBER *)pEvent2);
    }
    if(pEvent2 == TMWDEFS_NULL)
    { 
      tmwdlist_addEntry(pDesc->pEventList, (TMWDLIST_MEMBER *)pEvent);
    }

    /* And add another copy of the event at the end of the queue */
    pEvent2 = (S14EVNT *)s14mem_alloc(pDesc->eventMemType);
    if(pEvent2 == TMWDEFS_NULL)
    {
      S14DIAG_ERROR(pSector->pSession, pSector, S14DIAG_ALLOC_EVENT); 
    }
    else
    {
      *pEvent2 = *pEvent; 
      pEvent2->doubleTransmission = TMWDEFS_FALSE;
      if(pDoubleTransEvent != TMWDEFS_NULL)
        *pDoubleTransEvent = pEvent2;
      tmwdlist_addEntry(pDesc->pEventList, (TMWDLIST_MEMBER *)pEvent2);

#ifdef TMWCNFG_SUPPORT_STATS
      {
      TMWSCTR_STAT_EVENT_STRUCT eventData;
      eventData.typeId = pDesc->woTimeTypeId;
      eventData.ioa = pEvent->ioa;
      TMWSCTR_STAT_CALLBACK_FUNC(pSector, TMWSCTR_STAT_EVENT_ADDED, &eventData);
      }
#endif
    }
  }
  else
#endif
  {
    tmwdlist_addEntry(pDesc->pEventList, (TMWDLIST_MEMBER *)pEvent);
  }

#ifdef TMWCNFG_SUPPORT_STATS
  {
  TMWSCTR_STAT_EVENT_STRUCT eventData;
  eventData.typeId = pDesc->woTimeTypeId;
  eventData.ioa = pEvent->ioa;
  TMWSCTR_STAT_CALLBACK_FUNC(pSector, TMWSCTR_STAT_EVENT_ADDED, &eventData);
  }
#endif

  return(pEvent);
}

/* function: s14evnt_countEvents */
TMWTYPES_USHORT TMWDEFS_GLOBAL s14evnt_countEvents(
  TMWSCTR *pSector,
  TMWDLIST *pEventList)
{
  S14EVNT *pEvent;
  TMWTYPES_USHORT numSent = 0;
  TMWTARG_UNUSED_PARAM(pSector);

  /* Loop through all of the events, counting those that have not yet been sent */
  pEvent = (S14EVNT *)tmwdlist_getFirst(pEventList);
  while(pEvent != TMWDEFS_NULL)
  {
    if(!pEvent->eventSent)
      break;

    numSent++;
    pEvent = (S14EVNT *)tmwdlist_getNext((TMWDLIST_MEMBER *)pEvent);
  }

  return((TMWTYPES_USHORT)(tmwdlist_size(pEventList) - numSent));
}

/* function: s14evnt_reasonToCOT */
TMWTYPES_UCHAR TMWDEFS_GLOBAL s14evnt_reasonToCOT(TMWDEFS_CHANGE_REASON reason)
{
  switch(reason)
  {
  case TMWDEFS_CHANGE_LOCAL_OP:
    return(I14DEF_COT_RETURN_LOCAL);
  case TMWDEFS_CHANGE_REMOTE_OP:
    return(I14DEF_COT_RETURN_REMOTE);
  default:
    return(I14DEF_COT_SPONTANEOUS);
  }
}

/* function: s14evnt_processEvents */
S14EVNT_STATUS TMWDEFS_GLOBAL s14evnt_processEvents(
  TMWSCTR *pSector,
  S14EVNT_DESC *pDesc,
  TMWTYPES_UCHAR dataSize, 
  TMWDTIME *pEventTime,
  TMWTYPES_BOOL onlyDoubleTrans) 
{
  S14EVNT_STATUS eventStatus = S14EVNT_NOT_SENT;
  
#if !S14DATA_SUPPORT_DOUBLE_TRANS
  TMWTARG_UNUSED_PARAM(onlyDoubleTrans);
#endif

  /* See if there are events pending */
  if(tmwdlist_size(pDesc->pEventList) > 0)
  {
    S14SESN *pS14Session = (S14SESN *)pSector->pSession;
    S14SCTR *p14Sector = (S14SCTR *)pSector;
    TMWTYPES_UCHAR ioaSize = pS14Session->i870.infoObjAddrSize;
    S14EVNT *pEvent = TMWDEFS_NULL;
    TMWTYPES_UCHAR typeId;
    TMWTYPES_UCHAR timeSize = 0;
    TMWTYPES_UCHAR numSent = 0;
    TMWTYPES_UCHAR cot;
    TMWDEFS_TIME_FORMAT timeFormat;
    TMWSESN_TX_DATA *pTxData; 
    
    /* Special code for looking to look for I14DEF_COT_RETURN_REMOTE or LOCAL 
     * If event is found, send all events before that one to keep time order correct
     * If event is not found, return NOT SENT
     */
    if(pDesc->cotSpecified!=0)
    {
      pEvent = (S14EVNT *)tmwdlist_getFirst(pDesc->pEventList);
      while(pEvent->eventSent || (pEvent->cot != pDesc->cotSpecified)) 
      { 
        pEvent = (S14EVNT *)tmwdlist_getNext((TMWDLIST_MEMBER *)pEvent);
        if(pEvent == TMWDEFS_NULL)
        {
          return(S14EVNT_NOT_SENT);
        }
      } 
    }

    /* Get the first event that has not been sent yet so we know the 
     * cause of transmission for message header. 
     * Also, since we allow user to call s14xxx_addEvent with pTimeStamp
     * == TMWDEFS_NULL, indicating this event should be sent using ASDU
     * with no timestamp, see whether this first event has timeStamp or not.
     */
    pEvent = (S14EVNT *)tmwdlist_getFirst(pDesc->pEventList);
    while(pEvent->eventSent)
    {
      pEvent = (S14EVNT *)tmwdlist_getNext((TMWDLIST_MEMBER *)pEvent);
      if(pEvent == TMWDEFS_NULL)
      {
        return(S14EVNT_NOT_SENT);
      }
    }

#if S14DATA_SUPPORT_DOUBLE_TRANS
    if(onlyDoubleTrans && (!pEvent->doubleTransmission))
    {
      return(S14EVNT_NOT_SENT);
    }
#endif

    if((pTxData = i870chnl_newTxData(pSector->pSession->pChannel, 
      pSector->pSession, pSector, pS14Session->maxASDUSize)) == TMWDEFS_NULL)
    {
      return(S14EVNT_NOT_SENT);
    }

#if TMWCNFG_SUPPORT_DIAG
    pTxData->pMsgDescription = "Event Response";
#endif
    /* Don't set a timeout value. Timing out some event responses 
     * could result in points being out of time order 
     */
    pTxData->txFlags = TMWSESN_TXFLAGS_NO_RESPONSE;

    cot = pEvent->cot;

#if S14DATA_SUPPORT_DOUBLE_TRANS
    if(pEvent->doubleTransmission)
    {
      timeFormat = TMWDEFS_TIME_FORMAT_NONE;
      typeId = pDesc->woTimeTypeId;
    }
    else 
#endif
    if(pEvent->timeSpecified)
    {
      timeFormat = pDesc->timeFormat;
      if(timeFormat == TMWDEFS_TIME_FORMAT_24) timeSize = 3;
      if(timeFormat == TMWDEFS_TIME_FORMAT_56) timeSize = 7;

      typeId = pDesc->typeId;
    }
    else
    {
      timeFormat = TMWDEFS_TIME_FORMAT_NONE;
      typeId = pDesc->woTimeTypeId;
    }

    /* Initialize message header */
    i870util_buildMessageHeader(pSector->pSession,
      pTxData, typeId, cot, 0, p14Sector->i870.asduAddress);

    /* While more events, and enough room in message buffer for at least
      * one more event, process events
      */
    while((pEvent != TMWDEFS_NULL)
      && ((pTxData->msgLength + ioaSize + dataSize + timeSize) <= pTxData->maxLength))
    { 
      TMWDEFS_TIME_FORMAT eventTimeFormat;

      /* If cause of transmission does not match, send it in next ASDU */
      if(pEvent->cot != cot)
      {
        break;
      }
      
#if S14DATA_SUPPORT_DOUBLE_TRANS 
      if(pEvent->doubleTransmission) 
        eventTimeFormat = TMWDEFS_TIME_FORMAT_NONE;
      else
#endif
      if(!pEvent->timeSpecified)
        eventTimeFormat = TMWDEFS_TIME_FORMAT_NONE;
      else
        eventTimeFormat = pDesc->timeFormat;

      /* If time format does not match, send it in next ASDU */
      if(timeFormat != eventTimeFormat)
      {
        break;
      }

#if S14DATA_SUPPORT_CCSNA
      /* If a response to a clock sync request is required, 
       * before this event is sent don't send this event now.
       * Check this first, to make sure all events that should be sent
       * before the response are sent. 
       */
      if(s14ccsna_responseRequired(pSector, pEvent->ccsnaRespNumber))
      {
        eventStatus = S14EVNT_CCSNA_RESPONSE_REQUIRED;
        break;
      }
#endif

#if S14DATA_SUPPORT_DOUBLE_TRANS
      if(!pEvent->doubleTransmission)
#endif
      {
        /* If this sector is configured to send clock sync events and the time of
         * this event is not within the range of the last clock sync we need to
         * go ahead and send any events in the message and let the RBE code generate
         * a clock sync.
         */
        if(p14Sector->sendClockSyncEvents)
        {
          if(pEvent->timeSpecified &&
            ((pEvent->timeStamp.hour != p14Sector->lastClockSyncTime.hour)
            || (pEvent->timeStamp.dayOfMonth != p14Sector->lastClockSyncTime.dayOfMonth)
            || (pEvent->timeStamp.month != p14Sector->lastClockSyncTime.month)
            || (pEvent->timeStamp.year != p14Sector->lastClockSyncTime.year)
            || (pEvent->timeStamp.dstInEffect != p14Sector->lastClockSyncTime.dstInEffect)))
          {
            eventStatus = S14EVNT_CCSNA_SPONT_REQUIRED;
            *pEventTime = pEvent->timeStamp;
            break;
          }
        }
      }

      /* Store information object address */
      i870util_storeInfoObjAddr(pSector->pSession, pTxData, pEvent->ioa);

      /* Store event data */
      pDesc->pEventDataFunc(pTxData, pEvent);

      /* Store time as required */
      if(timeFormat == TMWDEFS_TIME_FORMAT_24)
        i870util_write24BitTime(pTxData, &pEvent->timeStamp, S14DATA_SUPPORT_GENUINE_TIME);
      else if(timeFormat == TMWDEFS_TIME_FORMAT_56)
        i870util_write56BitTime(pTxData, &pEvent->timeStamp, S14DATA_SUPPORT_GENUINE_TIME);

      /* Keep track of how many events have been sent */
      numSent += 1;

      /* mark event as sent. 
       * It will be deleted after the message is successfully sent 
       * In balanced mode, if this is not confirmed, the event will be kept in
       * queue to be resent
       */
      pEvent->eventSent = TMWDEFS_TRUE;

#ifdef TMWCNFG_SUPPORT_STATS
      {
      TMWSCTR_STAT_EVENT_STRUCT eventData;
      eventData.typeId = pDesc->woTimeTypeId;
      eventData.ioa = pEvent->ioa;
      TMWSCTR_STAT_CALLBACK_FUNC(pSector, TMWSCTR_STAT_EVENT_SENT, &eventData);
      }
#endif

      pEvent->pTxData = pTxData;
     
      pEvent = (S14EVNT *)tmwdlist_getNext((TMWDLIST_MEMBER *)pEvent); 
    }

    if(numSent > 0)
    {
      /* Update quantity in response */
      pTxData->pMsgBuf[1] = (TMWTYPES_UCHAR)numSent;

      /* If this queue is in the overflowed state, indicate it is no
       * longer full, and clear the overflowed flag.
       */
      if(*pDesc->pEventsOverflowedFlag == TMWDEFS_TRUE)
      { 
        TMWSCTR_STAT_CALLBACK_FUNC(pSector, TMWSCTR_STAT_EVENT_NOTFULL, &pDesc->woTimeTypeId);
        *pDesc->pEventsOverflowedFlag = TMWDEFS_FALSE;
      }
     
      /* Marking events as sent, and then removing them after they are confirmed  */
      pTxData->pAfterTxCallback = _eventsConfirmed;
      pTxData->pFailedTxCallback = _eventsNotConfirmed;
       
      pTxData->pCallbackData = pDesc->pEventList;
      pTxData->txFlags |= TMWSESN_TXFLAGS_CONTAINS_EVENTS;

      /* Send message */
      i870chnl_sendMessage(pTxData);
      return(S14EVNT_SENT);
    }
    else
    {
      i870chnl_freeTxData(pTxData);
    }
  }

  return(eventStatus);
}

/* function: s14event_linkDataReady */
void TMWDEFS_GLOBAL s14event_linkDataReady(
  TMWSCTR *pSector)
{ 
  /* Tell link layer we have data to send */
  pSector->pSession->pChannel->pLink->pLinkDataReady(
    pSector->pSession->pChannel->pLinkContext, pSector->pSession);
}


