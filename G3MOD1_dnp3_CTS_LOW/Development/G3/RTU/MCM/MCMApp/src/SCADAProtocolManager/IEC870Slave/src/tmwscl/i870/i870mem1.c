/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: i870mem1.c
 * description: IEC 60870-5 FT12 Link Layer Memory Implementation
 */

#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/i870mem1.h"
#include "tmwscl/i870/i870lnk1.h"
#include "tmwscl/i870/i870ft12.h"

typedef struct{
  TMWMEM_HEADER           header;
  I870LNK1_SESSION_INFO   data;
} I870MEM1_ALLOC_SESN;

typedef struct{
  TMWMEM_HEADER           header;
  I870LNK1_CONTEXT        data;
} I870MEM1_ALLOC_CONTEXT;

static const TMWTYPES_CHAR *_nameTable[I870MEM1_ALLOC_TYPE_MAX] = {
  "I870LNK1_SESSION_INFO", 
  "I870LNK1_CONTEXT"
};
 
#if !TMWCNFG_USE_DYNAMIC_MEMORY
/* Use static allocated memory instead of dynamic memory */
static I870MEM1_ALLOC_SESN      i870mem1Sesns[I870MEM1_NUMALLOC_SESNS];
static I870MEM1_ALLOC_CONTEXT   i870mem1Contexts[I870MEM1_NUMALLOC_CONTEXTS];
#endif

static TMWMEM_POOL_STRUCT _i870mem1AllocTable[I870MEM1_ALLOC_TYPE_MAX];

#if TMWCNFG_USE_DYNAMIC_MEMORY
void TMWDEFS_GLOBAL i870mem1_initConfig(
  I870MEM1_CONFIG   *pConfig)
{
  pConfig->numSessions = I870MEM1_NUMALLOC_SESNS;
  pConfig->numContexts = I870MEM1_NUMALLOC_CONTEXTS;
}
#endif

/* routine: i870mem1_init */
TMWTYPES_BOOL TMWDEFS_GLOBAL i870mem1_init(
 I870MEM1_CONFIG *pConfig)
{
#if TMWCNFG_USE_DYNAMIC_MEMORY
  /* dynamic memory allocation supported */
  
  I870MEM1_CONFIG  config; 

  /* If caller has not specified memory pool configuration, use the
   * default compile time values 
   */
  if(pConfig == TMWDEFS_NULL)
  {
    pConfig = &config;
    i870mem1_initConfig(pConfig);
  }

  if(!tmwmem_lowInit(_i870mem1AllocTable, I870MEM1_SESSION_INFO_TYPE, pConfig->numSessions,    sizeof(I870MEM1_ALLOC_SESN),    TMWDEFS_NULL))    
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_i870mem1AllocTable, I870MEM1_CONTEXT_TYPE,      pConfig->numContexts, sizeof(I870MEM1_ALLOC_CONTEXT), TMWDEFS_NULL))
    return TMWDEFS_FALSE;

#else
  /* static memory allocation supported */
  TMWTARG_UNUSED_PARAM(pConfig);  
  if(!tmwmem_lowInit(_i870mem1AllocTable, I870MEM1_SESSION_INFO_TYPE, I870MEM1_NUMALLOC_SESNS,    sizeof(I870MEM1_ALLOC_SESN),    (TMWTYPES_UCHAR*)i870mem1Sesns))
    return TMWDEFS_FALSE;
  if(!tmwmem_lowInit(_i870mem1AllocTable, I870MEM1_CONTEXT_TYPE,      I870MEM1_NUMALLOC_CONTEXTS, sizeof(I870MEM1_ALLOC_CONTEXT), (TMWTYPES_UCHAR*)i870mem1Contexts))
    return TMWDEFS_FALSE;
#endif   
    
  return TMWDEFS_TRUE;
}
/* function: i870mem1_alloc */
void * TMWDEFS_GLOBAL i870mem1_alloc(
  I870MEM1_ALLOC_TYPE type)
{
  if(type >= I870MEM1_ALLOC_TYPE_MAX)
  {
    return(TMWDEFS_NULL);
  }

  return(tmwmem_lowAlloc(&_i870mem1AllocTable[type]));
}

/* function: i870mem1_free */
void TMWDEFS_GLOBAL i870mem1_free(
  void *pBuf)
{    
  TMWMEM_HEADER *pHeader = TMWMEM_GETHEADER(pBuf);
  TMWTYPES_UCHAR type = TMWMEM_GETTYPE(pHeader);

  if(type >= I870MEM1_ALLOC_TYPE_MAX)
  {
    return;
  }

  tmwmem_lowFree(&_i870mem1AllocTable[type], pHeader);
}

/* function: i870mem1_getUsage */
TMWTYPES_BOOL TMWDEFS_GLOBAL i870mem1_getUsage(
  TMWTYPES_UCHAR index,
  const TMWTYPES_CHAR **pName,
  TMWMEM_POOL_STRUCT *pStruct)
{
  if(index >= I870MEM1_ALLOC_TYPE_MAX)
  {
    return(TMWDEFS_FALSE);
  }

  *pName = _nameTable[index];
  *pStruct = _i870mem1AllocTable[index];
  return(TMWDEFS_TRUE);
}


