/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14pmenc.c
 * description: IEC 60870-5-101/104 slave PMENC (Parameter of Measured Value,
 *  Short Floating Point Value) functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14pmenc.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/i870chnl.h"
#include "tmwscl/utils/tmwtarg.h"

#if S14DATA_SUPPORT_PMENC

/* function: s14pmenc_processRequest */
void TMWDEFS_CALLBACK s14pmenc_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  void *pPoint;

  /* Store originator address */
  p14Sector->pmencOriginator = pMsg->origAddress;
 
  /* Parse Information Object Address */
  i870util_readInfoObjAddr(pSector->pSession, pMsg, &p14Sector->pmencIOA);

  /* Parse FVA */
  tmwtarg_get32(&pMsg->pRxData->pMsgBuf[pMsg->offset], (TMWTYPES_ULONG *)&p14Sector->pmencFVA);
  pMsg->offset += 4;

  /* Parse QPM */
  p14Sector->pmencQPM = pMsg->pRxData->pMsgBuf[pMsg->offset++];

  /* Parse received cause of transmission */
  if(pMsg->cot == I14DEF_COT_ACTIVATION)
  {
    p14Sector->pmencCOT = I14DEF_COT_ACTCON;
  }
  else
  {
    p14Sector->pmencCOT = I14DEF_COT_UNKNOWN_COT | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* Get database point */
  pPoint = s14data_pmencLookupPoint(p14Sector->i870.pDbHandle, p14Sector->pmencIOA);
  if(pPoint == TMWDEFS_NULL)
  {
    p14Sector->pmencCOT = I14DEF_COT_UNKNOWN_IOA | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* Store New Value */
  if(!s14data_pmencStore(pPoint, &p14Sector->pmencFVA, &p14Sector->pmencQPM))
    p14Sector->pmencCOT |= I14DEF_COT_NEGATIVE_CONFIRM;
}

/* function: s14pmenc_buildResponse */
TMWTYPES_BOOL TMWDEFS_CALLBACK s14pmenc_buildResponse(
  TMWSCTR *pSector, 
  TMWTYPES_BOOL buildResponse)
{
  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  if(p14Sector->pmencCOT != 0)
  {
    if(buildResponse)
    {
      /* Activation confirmation or Deactivation */
      TMWSESN_TX_DATA *pTxData;
      S14SESN *pS14Session = (S14SESN *)pSector->pSession;

      if((pTxData = i870chnl_newTxData(pSector->pSession->pChannel, 
        pSector->pSession, pSector, pS14Session->maxASDUSize)) == TMWDEFS_NULL)
      {
        return(TMWDEFS_FALSE);
      }

#if TMWCNFG_SUPPORT_DIAG
      pTxData->pMsgDescription = "Parameter of Measured Value, Short Floating Point Value Response";
#endif
      pTxData->txFlags = TMWSESN_TXFLAGS_NO_RESPONSE;
      pTxData->responseTimeout = p14Sector->defaultResponseTimeout;

      /* Build response */
      i870util_buildMessageHeader(pSector->pSession, 
        pTxData, I14DEF_TYPE_PMENC1, p14Sector->pmencCOT, 
        p14Sector->pmencOriginator, p14Sector->i870.asduAddress);

      /* Store Information Object Address */
      i870util_storeInfoObjAddr(pSector->pSession, pTxData, p14Sector->pmencIOA);

      /* Store NVA */
      tmwtarg_store32((TMWTYPES_ULONG *)&p14Sector->pmencFVA, &pTxData->pMsgBuf[pTxData->msgLength]);
      pTxData->msgLength += 4;

      /* Store Qualifier of Parameter of Measured Value */
      pTxData->pMsgBuf[pTxData->msgLength++] = p14Sector->pmencQPM;

      /* Request is complete */
      p14Sector->pmencCOT = 0;

      /* Send the response */
      i870chnl_sendMessage(pTxData);
    }

    return(TMWDEFS_TRUE);
  }

  return(TMWDEFS_FALSE);
}

/* function: _storeInResponse */
static void TMWDEFS_CALLBACK _storeInResponse(
  TMWSESN_TX_DATA *pTxData, 
  void *pPoint,
  TMWDEFS_TIME_FORMAT timeFormat)
{
  TMWTYPES_SFLOAT value = s14data_pmencGetValue(pPoint);
  TMWTARG_UNUSED_PARAM(timeFormat);

  tmwtarg_store32((TMWTYPES_ULONG *)&value, &pTxData->pMsgBuf[pTxData->msgLength]);
  pTxData->msgLength += 4;

  pTxData->pMsgBuf[pTxData->msgLength++] = s14data_pmencGetQualifier(pPoint);
}

/* function: s14pmenc_readIntoResponse */
S14DBAS_GROUP_STATUS TMWDEFS_CALLBACK s14pmenc_readIntoResponse(
  TMWSESN_TX_DATA *pTxData, 
  TMWSCTR *pSector, 
  TMWTYPES_UCHAR cot, 
  TMWDEFS_GROUP_MASK groupMask, 
  TMWTYPES_ULONG ioa,
  TMWTYPES_USHORT *pPointIndex)
{
  return(s14util_readIntoResponse(pTxData, pSector, cot, 
    groupMask, ioa, I14DEF_TYPE_PMENC1, TMWDEFS_TIME_FORMAT_NONE, 5, pPointIndex, s14data_pmencGetPoint, 
    s14data_pmencGetGroupMask, s14data_pmencGetInfoObjAddr, s14util_alwaysUseIndexed, s14data_getTimeFormat, 
    _storeInResponse));
}

#endif /* S14DATA_SUPPORT_PMENC */
