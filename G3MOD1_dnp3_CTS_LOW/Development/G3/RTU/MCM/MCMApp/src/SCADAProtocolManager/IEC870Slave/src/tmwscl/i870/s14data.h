/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14data.h
 * description: This file defines the interface between the Triangle 
 *  MicroWorks, Inc. IEC 60870-5-101/104 slave source code library 
 *  and the target database. The corresponding implementations in 
 *  s14data.c should be modified to interface with the target 
 *  database.
 */
#ifndef S14DATA_DEFINED
#define S14DATA_DEFINED

#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/utils/tmwcnfg.h"
#include "tmwscl/utils/tmwdtime.h"
#include "tmwscl/utils/tmwsctr.h" 
 
/* The following macros define support for the different ASDU types. 
 * Setting any of these macros to TMWDEFS_FALSE will remove functionality
 * required to support the corresponding ASDU type.
 */
#define S14DATA_SUPPORT_MSP    TMWDEFS_TRUE  /* Single Point Information */
#define S14DATA_SUPPORT_MDP    TMWDEFS_TRUE  /* Double Point Information */
/* XXX Changed by Lucy */
#define S14DATA_SUPPORT_MST    TMWDEFS_FALSE  /* Step Position Information */

/* XXX Changed by Lucy */
#define S14DATA_SUPPORT_MBO    TMWDEFS_FALSE  /* Bitstring Of 32 Bit */

#define S14DATA_SUPPORT_MME_A  TMWDEFS_TRUE  /* Measured Value, Normalized Value */
#define S14DATA_SUPPORT_MME_B  TMWDEFS_TRUE  /* Measured Value, Scaled Value */
#define S14DATA_SUPPORT_MME_C  TMWCNFG_SUPPORT_FLOAT
                                             /* Measured Value, Short Floating Point Value */
#define S14DATA_SUPPORT_MIT    TMWDEFS_TRUE  /* Integrated Totals */
/* XXX Changed by Lucy */
#define S14DATA_SUPPORT_MPS    TMWDEFS_FALSE  /* Packed Single Point Information With Status Change Detection */

/* XXX Changed by Lucy */
#define S14DATA_SUPPORT_MMEND  TMWDEFS_FALSE  /* Measured Value, Normalized Value Without Quality Descriptor */

/* XXX Changed by Lucy */
#define S14DATA_SUPPORT_MEPTA  TMWDEFS_FALSE  /* Event of Protection Equipment  */

/* XXX Changed by Lucy */
#define S14DATA_SUPPORT_MEPTB  TMWDEFS_FALSE  /* Packed Start Events of Protection Equipment */

/* XXX Changed by Lucy */
#define S14DATA_SUPPORT_MEPTC  TMWDEFS_FALSE  /* Packed Output Circuit Information of Protection Equipment */

#define S14DATA_SUPPORT_CSC    TMWDEFS_TRUE  /* Single Point Command */
#define S14DATA_SUPPORT_CDC    TMWDEFS_TRUE  /* Double Point Command */

/* XXX Changed by Lucy */
#define S14DATA_SUPPORT_CRC    TMWDEFS_FALSE  /* Regulating Step Command */

/* XXX Changed by Lucy */
#define S14DATA_SUPPORT_CSE_A  TMWDEFS_FALSE  /* Set Point Command, Normalized Value */

/* XXX Changed by Lucy */
#define S14DATA_SUPPORT_CSE_B  TMWDEFS_FALSE  /* Set Point Command, Scaled Value */

#define S14DATA_SUPPORT_CSE_C  TMWCNFG_SUPPORT_FLOAT
                                             /* Set Point Command, Floating Point Value */
/* XXX Changed by Lucy */
#define S14DATA_SUPPORT_CBO    TMWDEFS_FALSE  /* Bitstring Command */     

#define S14DATA_SUPPORT_CCSNA  TMWDEFS_TRUE  /* Clock Synchronization Command */

/* XXX Changed by Lucy */
#define S14DATA_SUPPORT_CCD    TMWDEFS_FALSE  /* Load Delay Command */

/* XXX Changed by Lucy */
#define S14DATA_SUPPORT_CRDNA  TMWDEFS_TRUE /* Read Command */

/* XXX Changed by Lucy */
#define S14DATA_SUPPORT_CTS    TMWDEFS_TRUE  /* Test Command with or without time */

/* XXX Changed by Lucy */
#define S14DATA_SUPPORT_CRPNA  TMWDEFS_TRUE  /* Reset Process Command */

/* XXX Changed by Lucy */
#define S14DATA_SUPPORT_PMENA  TMWDEFS_FALSE  /* Parameter of Measured Value, Normalized Value */

/* XXX Changed by Lucy */
#define S14DATA_SUPPORT_PMENB  TMWDEFS_FALSE  /* Parameter of Measured Value, Scaled Value */

/* XXX Changed by Lucy */
#define S14DATA_SUPPORT_PMENC  TMWDEFS_FALSE
                                             /* Parameter of Measured Value, Short Floating Point Value */
/* XXX Changed by Lucy */
#define S14DATA_SUPPORT_PACNA  TMWDEFS_FALSE  /* Parameter Activation */

#define S14DATA_SUPPORT_CICNA  TMWDEFS_TRUE  /* Interrogation Command */
#define S14DATA_SUPPORT_CCINA  TMWDEFS_TRUE  /* Counter Interrogation Command */

/* XXX Changed by Lucy */
#define S14DATA_SUPPORT_FILE   TMWDEFS_FALSE  /* File Transfer Commands */

#ifndef S14DATA_SUPPORT_FSCNB
#define S14DATA_SUPPORT_FSCNB  TMWDEFS_FALSE /* File Transfer, Query Log, added in 104 Ed 2*/ 
#endif

/* This is not required for 60870-5-104 and normally should be left TMWDEFS_FALSE
 *   Setting this to TMWDEFS_TRUE will allow 104 slave to respond to CTSNA request 
 *   because some customers require it. CTSTA should be used in 104 instead.
 */
#ifndef S14DATA_SUPPORT_104CTSNA
#define S14DATA_SUPPORT_104CTSNA TMWDEFS_FALSE /* Test Command without time in 104*/
#endif

/* The following are custom ASDUs as specified by Gasunie
 * and are not defined in the IEC 60870-5-101/4 specifications.  
 */
#ifndef S14DATA_SUPPORT_MITC
#define S14DATA_SUPPORT_MITC  TMWDEFS_FALSE /* Integrated Total BCD             */
#define S14DATA_SUPPORT_CSE_Z TMWDEFS_FALSE /* Set Integrated Total Command, BCD*/
#define S14DATA_SUPPORT_MCTNA TMWDEFS_FALSE /* Configuration Table Information  */
#define S14DATA_SUPPORT_CCTNA TMWDEFS_FALSE /* Set Configuration Table Command  */
#endif

/* When a GI with broadcast Common ASDU Address is received, 
 *  send I14DEF_COT_ACTCON from RTU first, 
 *  followed by I14DEF_COT_ACTCON from Logical Units,
 *  followed by I14DEF_COT_INTG_GEN (or other data) from Logical Units,
 *  followed by I14DEF_COT_ACTTERM from Logical Units, 
 *  followed by I14DEF_COT_ACTTERM from RTU  
 * This behavior is not specified by 101 and 104 specs, this should be set to   
 * TMWDEFS_TRUE only if this special behavior is required 
 */
#define S14DATA_SUPPORT_RTU_CICNA TMWDEFS_FALSE /* Special CICNA behavior       */

/* Support for special behavior to prevent cyclic data from being sent to master
 * before a first CICNA completes. 
 * cyclicWaitCICNAComplete in S101/S104SESN_CONFIG should also be set
 * This behavior is not specified by the 101 and 104 specs. This should be set to
 * TMWDEFS_TRUE only if this special behavior is required by the master.
 */
#ifndef S14DATA_SUPPORT_CICNAWAIT
#define S14DATA_SUPPORT_CICNAWAIT TMWDEFS_FALSE /* Special Cyclic behavior       */
#endif

/* Setting the following to TMWDEFS_FALSE will disable support for 
 * timetag commands
 */
#define S14DATA_SUPPORT_TIMETAG_COMMANDS  TMWDEFS_TRUE

/* Setting the following to TMWDEFS_TRUE will enable support for setting RES1 in 
 * CP56Time2a Seven Octet Binary Time and CP24Time2a Three Octet Binary Time
 * to indicate Genuine or Substituted time defined in 60870-5-101 Second 
 * Edition 2003-02 7.2.6.18.
 * This may be set in the monitor direction only to indicate whether the time tag 
 * was acquired by the RTU or substituted by intermediate equipment such as 
 * concentrator stations. 
 *
 * NOTE: This is set to TMWDEFS_FALSE by default to avoid problems for existing 
 * SCL customers who may not initialize the new field properly.
 * 
 * If support is enabled, make sure that TMWDTIME genuineTime is initialized
 * or set properly, or else this bit may not be set properly in messages sent.
 * Verify that your implementation of tmwtarg_getDateTime() 
 * sets genuineTime = TMWDEFS_TRUE; and that any place your application fills
 * in a TMWDTIME structure this field gets set appropriately.
 */
#ifndef S14DATA_SUPPORT_GENUINE_TIME
#define S14DATA_SUPPORT_GENUINE_TIME      TMWDEFS_FALSE  
#endif

/* Specifies whether to compile in code to support Double Transmission mode 
 * for sending events. A single call to s14mxx_addEvent will cause the SCL 
 * to send a high priority spontaneous message without time, followed by a lower 
 * priority spontaneous message with time. 
 *  This is rarely used and would normally be left set to TMWDEFS_FALSE
 */
#ifndef S14DATA_SUPPORT_DOUBLE_TRANS
#define S14DATA_SUPPORT_DOUBLE_TRANS TMWDEFS_FALSE
#endif

/* Specifies whether to compile in code to support multiple simultaneous commands 
 * for the same ASDU type id on a single sector.
 */
#ifndef S14DATA_SUPPORT_MULTICMDS
#define S14DATA_SUPPORT_MULTICMDS TMWDEFS_FALSE
#endif
/* Some data types */
typedef TMWTYPES_ULONG S14DATA_GROUP_MASK;

typedef TMWTYPES_UCHAR  S14DATA_TRANSMISSION_MODE;
#define S14DATA_TRANSMISSION_SINGLE    0  
#define S14DATA_TRANSMISSION_DOUBLE    1
#define S14DATA_TRANSMISSION_PERPOINT  2

#ifdef __cplusplus
extern "C" {
#endif

  /* function: s14data_init
   * purpose: Initialize target database for specified sector.
   * arguments:
   *  pSector - Pointer to TMW sector structure
   *  pUserHandle - handle passed to the open sector function from
   *   user. This can be used to identify the sector from a users
   *   perspective.
   * returns:
   *  pointer to a handle which will be used to identify this
   *   database in all future calls.
   */
  void * TMWDEFS_GLOBAL s14data_init(
    TMWSCTR *pSector, 
    void *pUserHandle);

  /* function: s14data_close
   * purpose: Close target database. After this call the database
   *  handle will be invalid.
   * arguments:
   *  pHandle - pointer to handle returned from s14data_init.
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL s14data_close(
    void *pHandle);

  /* function: s14data_dataReady
   * purpose: Determine if the data for the particular type request is 
   *  ready. This allows user code to hold off response until data is ready.
   *  For example, it could be gathered from other devices.
   * arguments:
   *  pHandle - pointer to handle returned from s14data_init.
   *  type -  ASDU type indicating what data is being requested
   *    I14DEF_TYPE_CICNA1
   *    I14DEF_TYPE_CCINA1
   *    I14DEF_TYPE_CRDNA1
   * returns:
   *  TMWDEFS_TRUE if the data is ready to be read
   *  TMWDEFS_FALSE otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_dataReady(
    void *pHandle,
    TMWTYPES_UCHAR type);
 
  /* function: s14data_hasSectorReset   
   * purpose: Determine if sector has been reset and if so
   *  specify the Cause of Initialization value to be sent in 
   *  End of Initialization (MEINA) ASDU.
   * arguments:
   *  pHandle - pointer to handle returned from s14data_init.
   *  pResetCoi -  pointer to location into which to store Cause of
   *   initialization as defined in 7.2.6.21.
   *     I14DEF_COI_POWER_ON     - local power switch on
   *     I14DEF_COI_MANUAL_RESET - local manual reset
   *     I14DEF_COI_REMOTE_RESET - remote reset
   *     3..31  - compatible range
   *     32-127 - private range
   *   The above values can be or'ed with bit 8  
   *     0 - unchanged local parameters
   *     I14DEF_COI_LOCAL_PARAMS_CHANGED - change of local parameters
   * returns:
   *  TMWDEFS_TRUE if the sector has been reset
   *  TMWDEFS_FALSE otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_hasSectorReset(
    void *pHandle,
    TMWTYPES_UCHAR *pResetCOI);

  /* function: s14data_clearSectorReset 
   * purpose: Clear the reset state for this sector, MEINA has been sent.
   * arguments:
   *  pHandle - pointer to handle returned from s14data_init.
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL s14data_clearSectorReset(
    void *pHandle);
   
  /* function: s14data_ccsnaSetTime 
   * purpose: Set time because Clock sync (CCSNA) has been received. 
   *  The time received from the master has already been adjusted for
   *  processing and propagation delay.  
   *  
   *  Default behavior is to set the clock. Target implementations
   *  may only want 1 sector/session on a device to set the time. 
   *  Otherwise multiple masters might set the time differently.
   *
   *  Another use for this would be multiple masters in separate
   *  timezones requiring that time to be kept per sector. The 
   *  recommended method however, is for all devices to use GMT.
   *
   * arguments:
   *  pHandle - pointer to handle returned from s14data_init.
   *  pNewTime - pointer to time to set.
   * returns:
   *  TMWDEFS_TRUE if you want ACT CON positive to be sent back to the master
   *  TMWDEFS_FALSE if you want ACT CON negative to be sent back to the master
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_ccsnaSetTime(
    void *pHandle,
    TMWDTIME *pNewTime);
  
  /* function: s14data_ccsnaGetTime   
   * purpose: Get time for this sector. Typically this would just return
   *  the clock time which would be common for all sectors.
   * arguments:
   *  pHandle - pointer to handle returned from s14data_init.
   *  pTime - pointer location to put retrieved time.
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL s14data_ccsnaGetTime(
    void *pHandle,
    TMWDTIME *pTime);

  /* function: s14data_convertQOItoGroupMask  
   * purpose: Convert the received QOI that was not in the range
   *  global to group 16. If this QOI value is supported indicate 
   *  what bit to use in the return from the s14data_xxxGetGroupMask() 
   *  functions.  
   *  Note: Most customers will not need to change the default implementation.
   *   The default implementation of this function only needs to be modified
   *   if the target supports private range groups for General Interrogation (CICNA).
   *   
   * arguments:
   *  pHandle - pointer to handle returned from s14data_init. 
   *  qoi - Qualifier of Interrogation received, defined in 7.2.6.22
   * returns:
   *  TMWDEFS_GROUP_MASK indicating what bit will be set in the return from
   *   s14data_xxxGetGroupMask() for points that should be sent in response
   *   to this QOI from the private range.
   *  TMWDEFS_GROUP_MASK_PRIVATE_1 or
   *  TMWDEFS_GROUP_MASK_PRIVATE_2 or
   *  TMWDEFS_GROUP_MASK_PRIVATE_3 or
   *  TMWDEFS_GROUP_MASK_PRIVATE_4 or
   *  TMWDEFS_GROUP_MASK_PRIVATE_5
   */
  TMWDEFS_GROUP_MASK TMWDEFS_GLOBAL s14data_convertQOItoGroupMask(
    void *pHandle,
    TMWTYPES_UCHAR qoi);
   
  /* function: s14data_getTimeFormat 
   * purpose: Return the time format to be used for the specified point.
   *  This will only be called if the slave sector is opened with 
   *  readTimeFormat, readMsrndTimeFormat, or cicnaTimeFormat set to 
   *  TMWDEFS_TIME_FORMAT_UNKNOWN.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  time format to use for this point in response. 
   *    TMWDEFS_TIME_FORMAT_NONE, 
   *    TMWDEFS_TIME_FORMAT_24,
   *    TMWDEFS_TIME_FORMAT_56
   */
  TMWDEFS_TIME_FORMAT TMWDEFS_CALLBACK s14data_getTimeFormat(
    void *pPoint);

  /* Monitored Single Points */

  /* function: s14data_mspGetPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  index - which point to return, 0 to n-1, where n is the number of single points
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_mspGetPoint(
    void *pHandle, 
    TMWTYPES_USHORT index);

#if S14DATA_SUPPORT_DOUBLE_TRANS
  /* function: s14data_mspLookupPoint 
   * purpose: Return a handle to a specific data point. This handle is
   *  used by s14data_mspGetTransmissionMode to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  ioa - ioa number of point to return
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_mspLookupPoint(
    void *pHandle,
    TMWTYPES_ULONG ioa);  

  /* function: s14data_mspGetTransmissionMode 
   * purpose: Return event transmission mode for this point
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *   S14DATA_TRANSMISSION_SINGLE or S14DATA_TRANSMISSION_DOUBLE 
   */
  S14DATA_TRANSMISSION_MODE TMWDEFS_CALLBACK s14data_mspGetTransmissionMode(
    void *pPoint);
#endif

  /* function: s14data_mspGetInfoObjAddr
   * purpose: Return the Information Object Address for this point
   *  NOTE: IOA 0 is not allowed by the 101/104 protocol.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Information Object Address
   */
  TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_mspGetInfoObjAddr(
    void *pPoint);

  /* function: s14data_mspGetGroupMask
   * purpose: Return the group mask indicating what groups this point 
   *  belongs to
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Group Mask indicating what groups this point belongs to.
   *  Returning TMWDEFS_GROUP_MASK_GENERAL indicates this point should be
   *   sent back in response to a GI when the QOI defined in 7.2.6.22 is 20
   *  Returning TMWDEFS_GROUP_MASK_7 indicates this point should be sent back in 
   *   response to a GI when the QOI is 27
   *  Returning both of those bits set indicates this point should be included
   *   in responses to a GI with a QOI of either 20 or 27
   *  Returning TMWDEFS_GROUP_MASK_PRIVATE_x indicates this point should be sent
   *   back in response to a GI when the QOI y in the private range is received. 
   *   This value should also be returned back from s14data_convertQOItoGroupMask() 
   *   for QOI y. For example QOI 64 could be mapped to TMWDEFS_GROUP_MASK_PRIVATE_1
   *   and QOI 100 could be mapped to TMWDEFS_GROUP_MASK_PRIVATE_2.
   *  Returning TMWDEFS_GROUP_MASK_CYCLIC indicates this point should be sent as 
   *   cyclic data
   *  Returning TMWDEFS_GROUP_MASK_BACKGROUND indicates this point should be sent as 
   *   the result of a background scan
   */
  TMWDEFS_GROUP_MASK TMWDEFS_CALLBACK s14data_mspGetGroupMask(
    void *pPoint);

  /* function: s14data_mspGetIndexed
   * purpose: Return whether this point should be sent using indexed or 
   *  sequential addressing
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  TMWDEFS_TRUE if this point should use indexed addressing
   *  TMWDEFS_FALSE if this point should use sequential addressing
   */
  TMWTYPES_BOOL TMWDEFS_CALLBACK s14data_mspGetIndexed(
    void *pPoint);

  /* function: s14data_mspGetFlagsAndValue
   * purpose: Return the flags and value for this monitored single point
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   * returns:
   *  single point information with quality descriptor as defined in 7.2.6.1
   *   The return value contains a status indication and the current state of the point.
   *   The following values (or OR'd combinations) are valid for this type:
   *    I14DEF_SIQ_OFF    - Single Point Information value (Off)
   *    I14DEF_SIQ_ON     - Single Point Information value (On)
   *    I14DEF_QUALITY_BL - Blocked
   *    I14DEF_QUALITY_SB - Substituted
   *    I14DEF_QUALITY_NT - Topical
   *    I14DEF_QUALITY_IV - Invalid
   */
  TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mspGetFlagsAndValue(
    void *pPoint);
  
  /* function: s14data_mspGetFlagsValueTime
   * purpose: Return the flags, value and timestamp for this monitored single point
   *  This function will only be called if the sector is configured to return typeId with
   *  timestamp in response to CRDNA (or CICNA which is not in the specification).
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   *  pTime - pointer to location to return timestamp for this value.
   * returns:
   *  single point information with quality descriptor as defined in 7.2.6.1
   *   The return value contains a status indication and the current state of the point.
   *   The following values (or OR'd combinations) are valid for this type:
   *    I14DEF_SIQ_OFF    - Single Point Information value (Off)
   *    I14DEF_SIQ_ON     - Single Point Information value (On)
   *    I14DEF_QUALITY_BL - Blocked
   *    I14DEF_QUALITY_SB - Substituted
   *    I14DEF_QUALITY_NT - Topical
   *    I14DEF_QUALITY_IV - Invalid
   */
  TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mspGetFlagsValueTime(
    void *pPoint,
    TMWDTIME *pTime);

  /* function: s14data_mspChanged
   * purpose: Determine if the specified point has changed and if so
   *  return the new value. The SCL will use the current time as the time
   *  the event occurred. Use s14msp_addEvent to add events and specify
   *  a time of occurrence.
   *  This function is used to scan for events on each data point.
   *  It will be called if S101SCTR_CONFIG or S104SCTR_CONFIG 
   *  rbeScanPeriod is nonzero and mspScanEnabled == TMWDEFS_TRUE
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   *  pSIQ - pointer to location into which to store single point information
   *   with quality descriptor as defined in 7.2.6.1
   *   The value contains a status indication and the current state of the point.
   *   The following values (or OR'd combinations) are valid for the quality
   *   descriptor for this type:
   *    I14DEF_SIQ_OFF    - Single Point Information value (Off)
   *    I14DEF_SIQ_ON     - Single Point Information value (On)
   *    I14DEF_QUALITY_BL - Blocked
   *    I14DEF_QUALITY_SB - Substituted
   *    I14DEF_QUALITY_NT - Topical
   *    I14DEF_QUALITY_IV - Invalid
   *  pReason - pointer to location into which to store reason for change
   * returns:
   *  TMWDEFS_TRUE if the point has changed, else TMWDEFS_FALSE
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_mspChanged(
    void *pPoint, 
    TMWTYPES_UCHAR *pSIQ,
    TMWDEFS_CHANGE_REASON *pReason);

  /* Monitored Double Points */
  
  /* function: s14data_mdpGetPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  index - which point to return, 0 to n-1, where n is the number of double points
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_mdpGetPoint(
    void *pHandle, 
    TMWTYPES_USHORT index);
    
#if S14DATA_SUPPORT_DOUBLE_TRANS
  /* function: s14data_mdpLookupPoint 
   * purpose: Return a handle to a specific data point. This handle is
   *  used by s14data_mdpGetTransmissionMode to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  ioa - ioa number of point to return
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_mdpLookupPoint(
    void *pHandle,
    TMWTYPES_ULONG ioa);  

  /* function: s14data_mdpGetTransmissionMode 
   * purpose: Return event transmission mode for this point
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *   S14DATA_TRANSMISSION_SINGLE or S14DATA_TRANSMISSION_DOUBLE 
   */
  S14DATA_TRANSMISSION_MODE TMWDEFS_CALLBACK s14data_mdpGetTransmissionMode(
    void *pPoint);
#endif

  /* function: s14data_mdpGetInfoObjAddr
   * purpose: Return the Information Object Address for this point
   *  NOTE: IOA 0 is not allowed by the 101/104 protocol.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Information Object Address
   */
  TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_mdpGetInfoObjAddr(
    void *pPoint);

  /* function: s14data_mdpGetGroupMask
   * purpose: Return the group mask indicating what groups this point 
   *  belongs to
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Group Mask indicating what groups this point belongs to.
   *  Returning TMWDEFS_GROUP_MASK_GENERAL indicates this point should be
   *   sent back in response to a GI when the QOI defined in 7.2.6.22 is 20
   *  Returning TMWDEFS_GROUP_MASK_7 indicates this point should be sent back in 
   *   response to a GI when the QOI is 27
   *  Returning both of those bits set indicates this point should be included
   *   in responses to a GI with a QOI of either 20 or 27   
   *  Returning TMWDEFS_GROUP_MASK_PRIVATE_x indicates this point should be sent
   *   back in response to a GI when the QOI y in the private range is received. 
   *   This value should also be returned back from s14data_convertQOItoGroupMask() 
   *   for QOI y. For example QOI 64 could be mapped to TMWDEFS_GROUP_MASK_PRIVATE_1
   *   and QOI 100 could be mapped to TMWDEFS_GROUP_MASK_PRIVATE_2.
   *  Returning TMWDEFS_GROUP_MASK_CYCLIC indicates this point should be sent as 
   *   cyclic data
   *  Returning TMWDEFS_GROUP_MASK_BACKGROUND indicates this point should be sent as 
   *   the result of a background scan
   */
  TMWDEFS_GROUP_MASK TMWDEFS_CALLBACK s14data_mdpGetGroupMask(
    void *pPoint);

  /* function: s14data_mdpGetIndexed
   * purpose: Return whether this point should be sent using indexed or 
   *  sequential addressing
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  TMWDEFS_TRUE if this point should use indexed addressing
   *  TMWDEFS_FALSE if this point should use sequential addressing
   */
  TMWTYPES_BOOL TMWDEFS_CALLBACK s14data_mdpGetIndexed(
    void *pPoint);
 
  /* function: s14data_mdpGetFlagsAndValue
   * purpose: Return the flags and value for this monitored double point
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   * returns:
   *  double point information with quality descriptor as defined in 7.2.6.2
   *   The return value contains a status indication and the current state of the point.
   *   The following values (or OR'd combinations) are valid for this type:
   *    I14DEF_DIQ_INTERMEDIATE  - Double Point Information value (Intermediate)
   *    I14DEF_DIQ_OFF           - Double Point Information value (Off)
   *    I14DEF_DIQ_ON            - Double Point Information value (On)
   *    I14DEF_DIQ_INDETERMINATE - Double Point Information value (Indeterminate)
   *    I14DEF_QUALITY_BL        - Blocked
   *    I14DEF_QUALITY_SB        - Substituted
   *    I14DEF_QUALITY_NT        - Topical
   *    I14DEF_QUALITY_IV        - Invalid
   */
  TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mdpGetFlagsAndValue(
    void *pPoint);
 
  /* function: s14data_mdpGetFlagsValueTime
   * purpose: Return the flags and value for this monitored double point
   *  This function will only be called if the sector is configured to return typeId with
   *  timestamp in response to CRDNA (or CICNA which is not in the specification).
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   *  pTime - pointer to location to return timestamp for this value.
   * returns:
   *  double point information with quality descriptor as defined in 7.2.6.2
   *   The return value contains a status indication and the current state of the point.
   *   The following values (or OR'd combinations) are valid for this type:
   *    I14DEF_DIQ_INTERMEDIATE  - Double Point Information value (Intermediate)
   *    I14DEF_DIQ_OFF           - Double Point Information value (Off)
   *    I14DEF_DIQ_ON            - Double Point Information value (On)
   *    I14DEF_DIQ_INDETERMINATE - Double Point Information value (Indeterminate)
   *    I14DEF_QUALITY_BL        - Blocked
   *    I14DEF_QUALITY_SB        - Substituted
   *    I14DEF_QUALITY_NT        - Topical
   *    I14DEF_QUALITY_IV        - Invalid
   */
  TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mdpGetFlagsValueTime(
    void *pPoint,
    TMWDTIME *pTime);

  /* function: s14data_mdpChanged
   * purpose: Determine if the specified point has changed and if so
   *  return the new value. The SCL will use the current time as the time
   *  the event occurred. Use s14mdp_addEvent to add events and specify
   *  a time of occurrence.
   *  This function is used to scan for events on each data point.  
   *  It will be called if S101SCTR_CONFIG or S104SCTR_CONFIG 
   *  rbeScanPeriod is nonzero and mdpScanEnabled == TMWDEFS_TRUE
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   *  pDIQ - pointer to location into which to store double point information
   *   with quality descriptor as defined in 7.2.6.2
   *   The value contains a status indication and the current state of the point.
   *   The following values (or OR'd combinations) are valid for this type:
   *   I14DEF_DIQ_INTERMEDIATE  - Double Point Information value (Intermediate)
   *   I14DEF_DIQ_OFF           - Double Point Information value (Off)
   *   I14DEF_DIQ_ON            - Double Point Information value (On)
   *   I14DEF_DIQ_INDETERMINATE - Double Point Information value (Indeterminate)
   *   I14DEF_QUALITY_BL        - Blocked
   *   I14DEF_QUALITY_SB        - Substituted
   *   I14DEF_QUALITY_NT        - Topical
   *   I14DEF_QUALITY_IV        - Invalid
   *  pReason - pointer to location into which to store reason for change
   * returns:
   *  TMWDEFS_TRUE if the point has changed, else TMWDEFS_FALSE
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_mdpChanged(
    void *pPoint, 
    TMWTYPES_UCHAR *pDIQ,
    TMWDEFS_CHANGE_REASON *pReason);

  /* Step Position Information */ 
  
  /* function: s14data_mstGetPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  index - which point to return, 0 to n-1, where n is the number of mst points
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_mstGetPoint(
    void *pHandle, 
    TMWTYPES_USHORT index);

#if S14DATA_SUPPORT_DOUBLE_TRANS
  /* function: s14data_mstLookupPoint 
   * purpose: Return a handle to a specific data point. This handle is
   *  used by s14data_mstGetTransmissionMode to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  ioa - ioa number of point to return
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_mstLookupPoint(
    void *pHandle,
    TMWTYPES_ULONG ioa);  

  /* function: s14data_mstGetTransmissionMode 
   * purpose: Return event transmission mode for this point
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *   S14DATA_TRANSMISSION_SINGLE or S14DATA_TRANSMISSION_DOUBLE 
   */
  S14DATA_TRANSMISSION_MODE TMWDEFS_CALLBACK s14data_mstGetTransmissionMode(
    void *pPoint);
#endif

  /* function: s14data_mstGetInfoObjAddr
   * purpose: Return the Information Object Address for this point
   *  NOTE: IOA 0 is not allowed by the 101/104 protocol.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Information Object Address
   */
  TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_mstGetInfoObjAddr(
    void *pPoint);

  /* function: s14data_mstGetGroupMask
   * purpose: Return the group mask indicating what groups this point 
   *  belongs to
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Group Mask indicating what groups this point belongs to.
   *  Returning TMWDEFS_GROUP_MASK_GENERAL indicates this point should be
   *   sent back in response to a GI when the QOI defined in 7.2.6.22 is 20
   *  Returning TMWDEFS_GROUP_MASK_7 indicates this point should be sent back in 
   *   response to a GI when the QOI is 27
   *  Returning both of those bits set indicates this point should be included
   *   in responses to a GI with a QOI of either 20 or 27 
   *  Returning TMWDEFS_GROUP_MASK_PRIVATE_x indicates this point should be sent
   *   back in response to a GI when the QOI y in the private range is received. 
   *   This value should also be returned back from s14data_convertQOItoGroupMask() 
   *   for QOI y. For example QOI 64 could be mapped to TMWDEFS_GROUP_MASK_PRIVATE_1
   *   and QOI 100 could be mapped to TMWDEFS_GROUP_MASK_PRIVATE_2.
   *  Returning TMWDEFS_GROUP_MASK_CYCLIC indicates this point should be sent as 
   *   cyclic data
   *  Returning TMWDEFS_GROUP_MASK_BACKGROUND indicates this point should be sent as 
   *   the result of a background scan
   */
  TMWDEFS_GROUP_MASK TMWDEFS_CALLBACK s14data_mstGetGroupMask(
    void *pPoint);

  /* function: s14data_mstGetIndexed
   * purpose: Return whether this point should be sent using indexed or 
   *  sequential addressing
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  TMWDEFS_TRUE if this point should use indexed addressing
   *  TMWDEFS_FALSE if this point should use sequential addressing
   */
  TMWTYPES_BOOL TMWDEFS_CALLBACK s14data_mstGetIndexed(
    void *pPoint);
  
  /* function: s14data_mstGetFlags
   * purpose: Return the flags for this monitored step position
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   * returns:
   *  quality descriptor as defined in 7.2.6.3
   *   The return value contains a status indication of the point.
   *   The following values (or OR'd combinations) are valid for this type:
   *    I14DEF_QUALITY_OV - Overflow
   *    I14DEF_QUALITY_BL - Blocked
   *    I14DEF_QUALITY_SB - Substituted
   *    I14DEF_QUALITY_NT - Topical
   *    I14DEF_QUALITY_IV - Invalid
   */
  TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mstGetFlags(
    void *pPoint);
  
  /* function: s14data_mstGetValueAndState
   * purpose: Return the value and state for this monitored step position
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   * returns:
   *  value with transient state indication as defined in 7.2.6.5
   *  value + I14DEF_VTI_TRANSIENT_STATE
   */
  TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mstGetValueAndState(
    void *pPoint);
  
  /* function: s14data_mstGetValueStateFlagsTime
   * purpose: Return the value, state and timestamp for this monitored step position
   *  This function will only be called if the sector is configured to return typeId with
   *  timestamp in response to CRDNA (or CICNA which is not in the specification).
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   *  pFlags - pointer to location to return flags 
   *   The following values (or OR'd combinations) are valid for this type:
   *    I14DEF_QUALITY_OV - Overflow
   *    I14DEF_QUALITY_BL - Blocked
   *    I14DEF_QUALITY_SB - Substituted
   *    I14DEF_QUALITY_NT - Topical
   *    I14DEF_QUALITY_IV - Invalid
   *  pTime - pointer to location to return timestamp for this value.
   * returns:
   *  value with transient state indication as defined in 7.2.6.5
   *   The value contains a status indication and the current state of the point.
   */
  TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mstGetValueStateFlagsTime(
    void *pPoint,
    TMWTYPES_UCHAR *pFlags,
    TMWDTIME *pTime);
 
  /* function: s14data_mstChanged
   * purpose: Determine if the specified point has changed and if so
   *  return the new value. The SCL will use the current time as the time
   *  the event occurred. Use s14mst_addEvent to add events and specify
   *  a time of occurrence.
   *  This function is used to scan for events on each data point.
   *  It will be called if S101SCTR_CONFIG or S104SCTR_CONFIG 
   *  rbeScanPeriod is nonzero and mstScanEnabled == TMWDEFS_TRUE
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   *  pVTI - pointer to location into which to store the
   *   transient state indication as defined in 7.2.6.5
   *   The value contains a status indication and the current state of the point.
   *   The following values (or OR'd combinations) are valid for this type:
   *    I14DEF_VTI_TRANSIENT_STATE - Equipment is in a transient state  
   *  pQDS - pointer to location into which to store quality descriptor 
   *   as defined in 7.2.6.3
   *   The following values (or OR'd combinations) are valid for this type:
   *    I14DEF_QUALITY_OV - Overflow
   *    I14DEF_QUALITY_BL - Blocked
   *    I14DEF_QUALITY_SB - Substituted
   *    I14DEF_QUALITY_NT - Topical
   *    I14DEF_QUALITY_IV - Invalid
   *  pReason - pointer to location into which to store reason for change
   * returns:
   *  TMWDEFS_TRUE if the point has changed, else TMWDEFS_FALSE
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_mstChanged(
    void *pPoint, 
    TMWTYPES_UCHAR *pVTI,
    TMWTYPES_UCHAR *pQDS,
    TMWDEFS_CHANGE_REASON *pReason);

  /* Bitstring of 32 bit */
  
  /* function: s14data_mboGetPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  index - which point to return, 0 to n-1, where n is the number of mbo points
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_mboGetPoint(
    void *pHandle, 
    TMWTYPES_USHORT index);

#if S14DATA_SUPPORT_DOUBLE_TRANS
  /* function: s14data_mboLookupPoint 
   * purpose: Return a handle to a specific data point. This handle is
   *  used by s14data_mboGetTransmissionMode to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  ioa - ioa number of point to return
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_mboLookupPoint(
    void *pHandle,
    TMWTYPES_ULONG ioa);  

  /* function: s14data_mboGetTransmissionMode 
   * purpose: Return event transmission mode for this point
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *   S14DATA_TRANSMISSION_SINGLE or S14DATA_TRANSMISSION_DOUBLE 
   */
  S14DATA_TRANSMISSION_MODE TMWDEFS_CALLBACK s14data_mboGetTransmissionMode(
    void *pPoint);
#endif

  /* function: s14data_mboGetInfoObjAddr
   * purpose: Return the Information Object Address for this point
   *  NOTE: IOA 0 is not allowed by the 101/104 protocol.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Information Object Address
   */
  TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_mboGetInfoObjAddr(
    void *pPoint);

  /* function: s14data_mboGetGroupMask
   * purpose: Return the group mask indicating what groups this point 
   *  belongs to
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Group Mask indicating what groups this point belongs to.
   *  Returning TMWDEFS_GROUP_MASK_GENERAL indicates this point should be
   *   sent back in response to a GI when the QOI defined in 7.2.6.22 is 20
   *  Returning TMWDEFS_GROUP_MASK_7 indicates this point should be sent back in 
   *   response to a GI when the QOI is 27
   *  Returning both of those bits set indicates this point should be included
   *   in responses to a GI with a QOI of either 20 or 27 
   *  Returning TMWDEFS_GROUP_MASK_PRIVATE_x indicates this point should be sent
   *   back in response to a GI when the QOI y in the private range is received. 
   *   This value should also be returned back from s14data_convertQOItoGroupMask() 
   *   for QOI y. For example QOI 64 could be mapped to TMWDEFS_GROUP_MASK_PRIVATE_1
   *   and QOI 100 could be mapped to TMWDEFS_GROUP_MASK_PRIVATE_2.
   *  Returning TMWDEFS_GROUP_MASK_CYCLIC indicates this point should be sent as 
   *   cyclic data
   *  Returning TMWDEFS_GROUP_MASK_BACKGROUND indicates this point should be sent as 
   *   the result of a background scan
   */
  TMWDEFS_GROUP_MASK TMWDEFS_CALLBACK s14data_mboGetGroupMask(
    void *pPoint);

  /* function: s14data_mboGetIndexed
   * purpose: Return whether this point should be sent using indexed or 
   *  sequential addressing
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  TMWDEFS_TRUE if this point should use indexed addressing
   *  TMWDEFS_FALSE if this point should use sequential addressing
   */
  TMWTYPES_BOOL TMWDEFS_CALLBACK s14data_mboGetIndexed(
    void *pPoint);
 
  /* function: s14data_mboGetFlags
   * purpose: Return the flags for this monitored bitstring
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   * returns:
   *  quality descriptor as defined in 7.2.6.3
   *   The following values (or OR'd combinations) are valid for this type:
   *    I14DEF_QUALITY_OV - Overflow
   *    I14DEF_QUALITY_BL - Blocked
   *    I14DEF_QUALITY_SB - Substituted
   *    I14DEF_QUALITY_NT - Topical
   *    I14DEF_QUALITY_IV - Invalid
   */
  TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mboGetFlags(
    void *pPoint);
 
  /* function: s14data_mboGetValue
   * purpose: Return the value for this monitored bitstring
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   * returns:
   *  binary state information as defined in 7.2.6.13
   */
  TMWTYPES_ULONG TMWDEFS_GLOBAL s14data_mboGetValue(
    void *pPoint);

  /* function: s14data_mboGetValueFlagsTime
   * purpose: Return the value for this monitored bitstring
   *  This function will only be called if the sector is configured to return typeId with
   *  timestamp in response to CRDNA (or CICNA which is not in the specification).
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   *  pFlags - pointer to location to return flags 
   *  pTime - pointer to location to return timestamp for this value.
   * returns:
   *  binary state information as defined in 7.2.6.13
   */
  TMWTYPES_ULONG TMWDEFS_GLOBAL s14data_mboGetValueFlagsTime(
    void *pPoint,
    TMWTYPES_UCHAR *pFlags,
    TMWDTIME *pTime);
 
  /* function: s14data_mboChanged
   * purpose: Determine if the specified point has changed and if so
   *  return the new value. The SCL will use the current time as the time
   *  the event occurred. Use s14mbo_addEvent to add events and specify
   *  a time of occurrence.
   *  This function is used to scan for events on each data point.
   *  It will be called if S101SCTR_CONFIG or S104SCTR_CONFIG 
   *  rbeScanPeriod is nonzero and mboScanEnabled == TMWDEFS_TRUE
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   *  pBSI - pointer to location into which to store the
   *   binary state information as defined in 7.2.6.13
   *  pQDS - pointer to location into which to store the quality descriptor 
   *   as defined in 7.2.6.3
   *   The following values (or OR'd combinations) are valid for this type:
   *    I14DEF_QUALITY_OV - Overflow
   *    I14DEF_QUALITY_BL - Blocked
   *    I14DEF_QUALITY_SB - Substituted
   *    I14DEF_QUALITY_NT - Topical
   *    I14DEF_QUALITY_IV - Invalid
   *  pReason - pointer to location into which to store reason for change
   * returns:
   *  TMWDEFS_TRUE if the point has changed, else TMWDEFS_FALSE
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_mboChanged(
    void *pPoint, 
    TMWTYPES_ULONG *pBSI,
    TMWTYPES_UCHAR *pQDS,
    TMWDEFS_CHANGE_REASON *pReason);

  /* Normalized Measurands */
  
  /* function: s14data_mmenaGetPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  index - which point to return, 0 to n-1, where n is the number of mmena points
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_mmenaGetPoint(
    void *pHandle, 
    TMWTYPES_USHORT index);

#if S14DATA_SUPPORT_DOUBLE_TRANS
  /* function: s14data_mmenaLookupPoint 
   * purpose: Return a handle to a specific data point. This handle is
   *  used by s14data_mmenaGetTransmissionMode to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  ioa - ioa number of point to return
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_mmenaLookupPoint(
    void *pHandle,
    TMWTYPES_ULONG ioa);  

  /* function: s14data_mmenaGetTransmissionMode 
   * purpose: Return event transmission mode for this point
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *   S14DATA_TRANSMISSION_SINGLE or S14DATA_TRANSMISSION_DOUBLE 
   */
  S14DATA_TRANSMISSION_MODE TMWDEFS_CALLBACK s14data_mmenaGetTransmissionMode(
    void *pPoint);
#endif

  /* function: s14data_mmenaGetInfoObjAddr
   * purpose: Return the Information Object Address for this point
   *  NOTE: IOA 0 is not allowed by the 101/104 protocol.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Information Object Address
   */
  TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_mmenaGetInfoObjAddr(
    void *pPoint);

  /* function: s14data_mmenaGetGroupMask
   * purpose: Return the group mask indicating what groups this point 
   *  belongs to
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Group Mask indicating what groups this point belongs to.
   *  Returning TMWDEFS_GROUP_MASK_GENERAL indicates this point should be
   *   sent back in response to a GI when the QOI defined in 7.2.6.22 is 20
   *  Returning TMWDEFS_GROUP_MASK_7 indicates this point should be sent back in 
   *   response to a GI when the QOI is 27
   *  Returning both of those bits set indicates this point should be included
   *   in responses to a GI with a QOI of either 20 or 27 
   *  Returning TMWDEFS_GROUP_MASK_PRIVATE_x indicates this point should be sent
   *   back in response to a GI when the QOI y in the private range is received. 
   *   This value should also be returned back from s14data_convertQOItoGroupMask() 
   *   for QOI y. For example QOI 64 could be mapped to TMWDEFS_GROUP_MASK_PRIVATE_1
   *   and QOI 100 could be mapped to TMWDEFS_GROUP_MASK_PRIVATE_2.
   *  Returning TMWDEFS_GROUP_MASK_CYCLIC indicates this point should be sent as 
   *   cyclic data
   *  Returning TMWDEFS_GROUP_MASK_BACKGROUND indicates this point should be sent as 
   *   the result of a background scan
   */
  TMWDEFS_GROUP_MASK TMWDEFS_CALLBACK s14data_mmenaGetGroupMask(
    void *pPoint);

  /* function: s14data_mmenaGetIndexed
   * purpose: Return whether this point should be sent using indexed or 
   *  sequential addressing
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  TMWDEFS_TRUE if this point should use indexed addressing
   *  TMWDEFS_FALSE if this point should use sequential addressing
   */
  TMWTYPES_BOOL TMWDEFS_CALLBACK s14data_mmenaGetIndexed(
    void *pPoint);

  /* function: s14data_mmenaGetFlags
   * purpose: Return the flags for this measured value, normalized value
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   * returns:
   *  quality descriptor as defined in 7.2.6.3
   *   The following values (or OR'd combinations) are valid for this type:
   *    I14DEF_QUALITY_OV - Overflow
   *    I14DEF_QUALITY_BL - Blocked
   *    I14DEF_QUALITY_SB - Substituted
   *    I14DEF_QUALITY_NT - Topical
   *    I14DEF_QUALITY_IV - Invalid
   */
  TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mmenaGetFlags(
    void *pPoint);
 
  /* function: s14data_mmenaGetValue
   * purpose: Return the value for this measured value, normalized value
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   * returns:
   *  Normalized value as defined in 7.2.6.6
   *   A normalized value is a 16-bit value with a range of
   *   -1 to (1 - (2^(-15)))
   */
  TMWTYPES_SHORT TMWDEFS_GLOBAL s14data_mmenaGetValue(
    void *pPoint);

  /* function: s14data_mmenaGetValueFlagsTime
   * purpose: Return the value for this measured value, normalized value
   *  This function will only be called if the sector is configured to return typeId with
   *  timestamp in response to CRDNA (or CICNA which is not in the specification).
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   *  pFlags - pointer to location to return flags for this point.
   *  pTime - pointer to location to return timestamp for this value.
   * returns:
   *  Normalized value as defined in 7.2.6.6
   *   A normalized value is a 16-bit value with a range of
   *   -1 to (1 - (2^(-15)))
   */
  TMWTYPES_SHORT TMWDEFS_GLOBAL s14data_mmenaGetValueFlagsTime(
    void *pPoint, 
    TMWTYPES_UCHAR *pFlags,
    TMWDTIME *pTime);

  /* function: s14data_mmenaChanged
   * purpose: Determine if the specified point has changed and if so
   *  return the new value. The SCL will use the current time as the time
   *  the event occurred. Use s14mmena_addEvent to add events and specify
   *  a time of occurrence.
   *  This function is used to scan for events on each data point.
   *  It will be called if S101SCTR_CONFIG or S104SCTR_CONFIG 
   *  rbeScanPeriod is nonzero and mmenaScanEnabled == TMWDEFS_TRUE
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   *  pNVA - pointer to location into which to store the 
   *   normalized value as defined in 7.2.6.6
   *   A normalized value is a 16-bit value with a range of
   *   -1 to (1 - (2^(-15)))
   *  pQDS - pointer to location into which to store the quality descriptor
   *   as defined in 7.2.6.3
   *   The following values (or OR'd combinations) are valid for the quality
   *   descriptor for this type:
   *    I14DEF_QUALITY_OV - Overflow
   *    I14DEF_QUALITY_BL - Blocked
   *    I14DEF_QUALITY_SB - Substituted
   *    I14DEF_QUALITY_NT - Topical
   *    I14DEF_QUALITY_IV - Invalid
   *  pReason - pointer to location into which to store reason for change
   * returns:
   *  TMWDEFS_TRUE if the point has changed, else TMWDEFS_FALSE
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_mmenaChanged(
    void *pPoint, 
    TMWTYPES_SHORT *pNVA,
    TMWTYPES_UCHAR *pQDS,
    TMWDEFS_CHANGE_REASON *pReason);

  /* Scaled Measurands */
  
  /* function: s14data_mmenbGetPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  index - which point to return, 0 to n-1, where n is the number of mmenb points
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_mmenbGetPoint(
    void *pHandle, 
    TMWTYPES_USHORT index);

#if S14DATA_SUPPORT_DOUBLE_TRANS
  /* function: s14data_mmenbLookupPoint 
   * purpose: Return a handle to a specific data point. This handle is
   *  used by s14data_mmenbGetTransmissionMode to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  ioa - ioa number of point to return
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_mmenbLookupPoint(
    void *pHandle,
    TMWTYPES_ULONG ioa);  

  /* function: s14data_mmenbGetTransmissionMode 
   * purpose: Return event transmission mode for this point
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *   S14DATA_TRANSMISSION_SINGLE or S14DATA_TRANSMISSION_DOUBLE 
   */
  S14DATA_TRANSMISSION_MODE TMWDEFS_CALLBACK s14data_mmenbGetTransmissionMode(
    void *pPoint);
#endif
 
  /* function: s14data_mmenbGetInfoObjAddr
   * purpose: Return the Information Object Address for this point
   *  NOTE: IOA 0 is not allowed by the 101/104 protocol.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Information Object Address
   */
  TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_mmenbGetInfoObjAddr(
    void *pPoint);

  /* function: s14data_mmenbGetGroupMask
   * purpose: Return the group mask indicating what groups this point 
   *  belongs to
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Group Mask indicating what groups this point belongs to.
   *  Returning TMWDEFS_GROUP_MASK_GENERAL indicates this point should be
   *   sent back in response to a GI when the QOI defined in 7.2.6.22 is 20
   *  Returning TMWDEFS_GROUP_MASK_7 indicates this point should be sent back in 
   *   response to a GI when the QOI is 27
   *  Returning both of those bits set indicates this point should be included
   *   in responses to a GI with a QOI of either 20 or 27 
   *  Returning TMWDEFS_GROUP_MASK_PRIVATE_x indicates this point should be sent
   *   back in response to a GI when the QOI y in the private range is received. 
   *   This value should also be returned back from s14data_convertQOItoGroupMask() 
   *   for QOI y. For example QOI 64 could be mapped to TMWDEFS_GROUP_MASK_PRIVATE_1
   *   and QOI 100 could be mapped to TMWDEFS_GROUP_MASK_PRIVATE_2.
   *  Returning TMWDEFS_GROUP_MASK_CYCLIC indicates this point should be sent as 
   *   cyclic data
   *  Returning TMWDEFS_GROUP_MASK_BACKGROUND indicates this point should be sent as 
   *   the result of a background scan
   */
  TMWDEFS_GROUP_MASK TMWDEFS_CALLBACK s14data_mmenbGetGroupMask(
    void *pPoint);

  /* function: s14data_mmenbGetIndexed
   * purpose: Return whether this point should be sent using indexed or 
   *  sequential addressing
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  TMWDEFS_TRUE if this point should use indexed addressing
   *  TMWDEFS_FALSE if this point should use sequential addressing
   */
  TMWTYPES_BOOL TMWDEFS_CALLBACK s14data_mmenbGetIndexed(
    void *pPoint);

  /* function: s14data_mmenbGetFlags
   * purpose: Return the flags for this measured value, scaled value
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   * returns:
   *  quality descriptor as defined in 7.2.6.3
   *   The following values (or OR'd combinations) are valid for this type:
   *    I14DEF_QUALITY_OV - Overflow
   *    I14DEF_QUALITY_BL - Blocked
   *    I14DEF_QUALITY_SB - Substituted
   *    I14DEF_QUALITY_NT - Topical
   *    I14DEF_QUALITY_IV - Invalid
   */
  TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mmenbGetFlags(
    void *pPoint);

  /* function: s14data_mmenbGetValue
   * purpose: Return the value for this measured value, scaled value
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   * returns:
   *  scaled value as defined in 7.2.6.7
   *   A scaled value is a 16-bit value with a range of
   *   -(2^15) to ((2^15) - 1)
   */
  TMWTYPES_SHORT TMWDEFS_GLOBAL s14data_mmenbGetValue(
    void *pPoint);

  /* function: s14data_mmenbGetValueFlagsTime
   * purpose: Return the value for this measured value, scaled value
   *  This function will only be called if the sector is configured to return typeId with
   *  timestamp in response to CRDNA (or CICNA which is not in the specification).
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   *  pFlags - pointer to location to return flags for this point.
   *  pTime - pointer to location to return timestamp for this value.
   * returns:
   *  scaled value as defined in 7.2.6.7
   *   A scaled value is a 16-bit value with a range of
   *   -(2^15) to ((2^15) - 1)
   */
  TMWTYPES_SHORT TMWDEFS_GLOBAL s14data_mmenbGetValueFlagsTime(
    void *pPoint,
    TMWTYPES_UCHAR *pFlags,
    TMWDTIME *pTime);

  /* function: s14data_mmenbChanged
   * purpose: Determine if the specified point has changed and if so
   *  return the new value. The SCL will use the current time as the time
   *  the event occurred. Use s14mmenb_addEvent to add events and specify
   *  a time of occurrence.
   *  This function is used to scan for events on each data point.
   *  It will be called if S101SCTR_CONFIG or S104SCTR_CONFIG 
   *  rbeScanPeriod is nonzero and mmenbScanEnabled == TMWDEFS_TRUE
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   *  pSVA - pointer to location into which to store the
   *   scaled value as defined in 7.2.6.7
   *   A scaled value is a 16-bit value with a range of
   *   -(2^15) to (2^(15) - 1)
   *  pQDS - pointer to location into which to store the quality descriptor
   *   as defined in 7.2.6.3
   *   The following values (or OR'd combinations) are valid for the quality
   *   descriptor for this type:
   *    I14DEF_QUALITY_OV - Overflow
   *    I14DEF_QUALITY_BL - Blocked
   *    I14DEF_QUALITY_SB - Substituted
   *    I14DEF_QUALITY_NT - Topical
   *    I14DEF_QUALITY_IV - Invalid
   *  pReason - pointer to location into which to store reason for change
   * returns:
   *  TMWDEFS_TRUE if the point has changed, else TMWDEFS_FALSE
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_mmenbChanged(
    void *pPoint, 
    TMWTYPES_SHORT *pSVA,
    TMWTYPES_UCHAR *pQDS,
    TMWDEFS_CHANGE_REASON *pReason);

  /* Floating Point Measurands */
  
  /* function: s14data_mmencGetPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  index - which point to return, 0 to n-1, where n is the number of mmenc points
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_mmencGetPoint(
    void *pHandle, 
    TMWTYPES_USHORT index);
 
#if S14DATA_SUPPORT_DOUBLE_TRANS
  /* function: s14data_mmencLookupPoint 
   * purpose: Return a handle to a specific data point. This handle is
   *  used by s14data_mmencGetTransmissionMode to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  ioa - ioa number of point to return
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_mmencLookupPoint(
    void *pHandle,
    TMWTYPES_ULONG ioa);  

  /* function: s14data_mmencGetTransmissionMode 
   * purpose: Return event transmission mode for this point
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *   S14DATA_TRANSMISSION_SINGLE or S14DATA_TRANSMISSION_DOUBLE 
   */
  S14DATA_TRANSMISSION_MODE TMWDEFS_CALLBACK s14data_mmencGetTransmissionMode(
    void *pPoint);
#endif

  /* function: s14data_mmencGetInfoObjAddr
   * purpose: Return the Information Object Address for this point
   *  NOTE: IOA 0 is not allowed by the 101/104 protocol.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Information Object Address
   */
  TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_mmencGetInfoObjAddr(
    void *pPoint);

  /* function: s14data_mmencGetGroupMask
   * purpose: Return the group mask indicating what groups this point 
   *  belongs to
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Group Mask indicating what groups this point belongs to.
   *  Returning TMWDEFS_GROUP_MASK_GENERAL indicates this point should be
   *   sent back in response to a GI when the QOI defined in 7.2.6.22 is 20
   *  Returning TMWDEFS_GROUP_MASK_7 indicates this point should be sent back in 
   *   response to a GI when the QOI is 27
   *  Returning both of those bits set indicates this point should be included
   *   in responses to a GI with a QOI of either 20 or 27 
   *  Returning TMWDEFS_GROUP_MASK_PRIVATE_x indicates this point should be sent
   *   back in response to a GI when the QOI y in the private range is received. 
   *   This value should also be returned back from s14data_convertQOItoGroupMask() 
   *   for QOI y. For example QOI 64 could be mapped to TMWDEFS_GROUP_MASK_PRIVATE_1
   *   and QOI 100 could be mapped to TMWDEFS_GROUP_MASK_PRIVATE_2.
   *  Returning TMWDEFS_GROUP_MASK_CYCLIC indicates this point should be sent as 
   *   cyclic data
   *  Returning TMWDEFS_GROUP_MASK_BACKGROUND indicates this point should be sent as 
   *   the result of a background scan
   */
  TMWDEFS_GROUP_MASK TMWDEFS_CALLBACK s14data_mmencGetGroupMask(
    void *pPoint);

  /* function: s14data_mmencGetIndexed
   * purpose: Return whether this point should be sent using indexed or 
   *  sequential addressing
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  TMWDEFS_TRUE if this point should use indexed addressing
   *  TMWDEFS_FALSE if this point should use sequential addressing
   */
  TMWTYPES_BOOL TMWDEFS_CALLBACK s14data_mmencGetIndexed(
    void *pPoint);

  /* function: s14data_mmencGetFlags
   * purpose: Return the flags for this measured value, short floating 
   *  point
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   * returns:
   *  quality descriptor as defined in 7.2.6.3
   *   The following values (or OR'd combinations) are valid for this type:
   *    I14DEF_QUALITY_OV - Overflow
   *    I14DEF_QUALITY_BL - Blocked
   *    I14DEF_QUALITY_SB - Substituted
   *    I14DEF_QUALITY_NT - Topical
   *    I14DEF_QUALITY_IV - Invalid
   */
  TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mmencGetFlags(
    void *pPoint);

#if S14DATA_SUPPORT_MME_C
  /* function: s14data_mmencGetValue
   * purpose: Return the value for this measured value, short floating 
   *  point 
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   * returns:
   *  short floating point value as defined in 7.2.6.8
   *   A float is defined according to IEEE STD 754
   */
  TMWTYPES_SFLOAT TMWDEFS_GLOBAL s14data_mmencGetValue(
    void *pPoint);

  /* function: s14data_mmencGetValueFlagsTime
   * purpose: Return the value, flags and timestamp for this measured value, 
   *  short floating
   *  This function will only be called if the sector is configured to return typeId with
   *  timestamp in response to CRDNA (or CICNA which is not in the specification). 
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   *  pFlags - pointer to location to return flag
   *   quality descriptor as defined in 7.2.6.3
   *    The following values (or OR'd combinations) are valid for this type:
   *     I14DEF_QUALITY_OV - Overflow
   *     I14DEF_QUALITY_BL - Blocked
   *     I14DEF_QUALITY_SB - Substituted
   *     I14DEF_QUALITY_NT - Topical
   *     I14DEF_QUALITY_IV - Invalid
   *  pTime - pointer to location to return timestamp for this value.
   * returns:
   *  short floating point value as defined in 7.2.6.8
   *   A float is defined according to IEEE STD 754
   */
  TMWTYPES_SFLOAT TMWDEFS_GLOBAL s14data_mmencGetValueFlagsTime(
    void *pPoint,
    TMWTYPES_UCHAR *pFlags,
    TMWDTIME *pTime);

  /* function: s14data_mmencChanged
   * purpose: Determine if the specified point has changed and if so
   *  return the new value. The SCL will use the current time as the time
   *  the event occurred. Use s14mmenc_addEvent to add events and specify
   *  a time of occurrence.
   *  This function is used to scan for events on each data point.
   *  It will be called if S101SCTR_CONFIG or S104SCTR_CONFIG 
   *  rbeScanPeriod is nonzero and mmencScanEnabled == TMWDEFS_TRUE
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   *  pFVA - pointer to location into which to store the
   *   short flating point number as defined in 7.2.6.8
   *   A float is defined according to IEEE STD 754
   *  pQDS - pointer to location into which to store the quality descriptor
   *   as defined in 7.2.6.3
   *   The following values (or OR'd combinations) are valid for quality
   *   descriptor for this type:
   *    I14DEF_QUALITY_OV - Overflow
   *    I14DEF_QUALITY_BL - Blocked
   *    I14DEF_QUALITY_SB - Substituted
   *    I14DEF_QUALITY_NT - Topical
   *    I14DEF_QUALITY_IV - Invalid
   *  pReason - pointer to location into which to store reason for change
   * returns:
   *  TMWDEFS_TRUE if the point has changed, else TMWDEFS_FALSE
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_mmencChanged(
    void *pPoint, 
    TMWTYPES_SFLOAT *pFVA,
    TMWTYPES_UCHAR *pQDS,
    TMWDEFS_CHANGE_REASON *pReason);
#endif

  /* Integrated Totals */
  
  /* function: s14data_mitGetPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  index - which point to return, 0 to n-1, where n is the number of mit points
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_mitGetPoint(
    void *pHandle, 
    TMWTYPES_USHORT index);
 
#if S14DATA_SUPPORT_DOUBLE_TRANS
  /* function: s14data_mitLookupPoint 
   * purpose: Return a handle to a specific data point. This handle is
   *  used by s14data_mitGetTransmissionMode to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  ioa - ioa number of point to return
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_mitLookupPoint(
    void *pHandle,
    TMWTYPES_ULONG ioa);  

  /* function: s14data_mitGetTransmissionMode 
   * purpose: Return event transmission mode for this point
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *   S14DATA_TRANSMISSION_SINGLE or S14DATA_TRANSMISSION_DOUBLE 
   */
  S14DATA_TRANSMISSION_MODE TMWDEFS_CALLBACK s14data_mitGetTransmissionMode(
    void *pPoint);
#endif

  /* function: s14data_mitGetInfoObjAddr
   * purpose: Return the Information Object Address for this point
   *  NOTE: IOA 0 is not allowed by the 101/104 protocol.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Information Object Address
   */
  TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_mitGetInfoObjAddr(
    void *pPoint);

  /* function: s14data_mitGetGroupMask
   * purpose: Return the group mask indicating what groups this point 
   *  belongs to.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Group Mask indicating what groups this point belongs to.
   *  Returning TMWDEFS_GROUP_MASK_GENERAL indicates this point should be
   *   sent back in response to a CCINA when the RQT defined in 7.2.6.23 is 5
   *   indicating General Request Counter.
   *  Returning TMWDEFS_GROUP_MASK_1 indicates this point should be sent back in response
   *   to a CCINA when the RQT defined is 1 indicating counter group 1.
   *  Returning both of those bits would indicate this point would be included
   *  in responses to a CCINA with RQT of 5 or 1.
   */
  TMWDEFS_GROUP_MASK TMWDEFS_CALLBACK s14data_mitGetGroupMask(
    void *pPoint);

  /* function: s14data_mitGetIndexed
   * purpose: Return whether this point should be sent using indexed or 
   *  sequential addressing
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  TMWDEFS_TRUE if this point should use indexed addressing
   *  TMWDEFS_FALSE if this point should use sequential addressing
   */
  TMWTYPES_BOOL TMWDEFS_CALLBACK s14data_mitGetIndexed(
    void *pPoint);
 
  /* function: s14data_mitGetFlags
   * purpose: Return the flags for this integrated totals point 
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   * returns:
   *  last byte of binary counter reading as defined in 7.2.6.9
   *   The following values (or OR'd combinations) are valid for this type:
   *    0 through 31  - Counter sequence number
   *    I14DEF_BCR_CY - Carry (Counter Overflow)
   *    I14DEF_BCR_CA - Counter was adjusted since last reading
   *    I14DEF_BCR_IV - Counter reading is invalid
   */
  TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mitGetFlags(
    void *pPoint);

  /* function: s14data_mitGetValue
   * purpose: Return the frozen value for this integrated totals point
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   * returns:
   *  first 32 bits of binary counter reading value as defined in 7.2.6.9
   */
  TMWTYPES_ULONG TMWDEFS_GLOBAL s14data_mitGetValue(
    void *pPoint);

  /* function: s14data_mitGetValueFlagsTime
   * purpose: Return the frozen value, flags, and timestamp for this integrated 
   *  totals point.
   *  This function will only be called if the sector is configured to return 
   *  typeId with timestamp in response to CCINA which is not in the specification. 
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   *  pFlags - pointer to location to return flags for this point.
   *  pTime - pointer to location to return timestamp for this value.
   * returns:
   *  first 32 bits of binary counter reading value as defined in 7.2.6.9
   *   The following values (or OR'd combinations) are valid for this type:
   *    0 through 31  - Counter sequence number
   *    I14DEF_BCR_CY - Carry (Counter Overflow)
   *    I14DEF_BCR_CA - Counter was adjusted since last reading
   *    I14DEF_BCR_IV - Counter reading is invalid
   */
  TMWTYPES_ULONG TMWDEFS_GLOBAL s14data_mitGetValueFlagsTime(
    void *pPoint,
    TMWTYPES_UCHAR *pFlags,
    TMWDTIME *pTime);

  /* function: s14data_mitFreeze
   * purpose: Freeze this counter
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   *  qcc - qualifier of counter interrogation command as defined in 7.2.6.23
   *   The following values are valid for the RQT (request) field:
   *    I14DEF_QCC_RQT_GROUP1     - Request counter group 1
   *    I14DEF_QCC_RQT_GROUP2     - Request counter group 2
   *    I14DEF_QCC_RQT_GROUP3     - Request counter group 3
   *    I14DEF_QCC_RQT_GROUP4     - Request counter group 4
   *    I14DEF_QCC_RQT_GENERAL    - General request
   *   The following values are valid for the FRZ (Freeze) field:
   *    I14DEF_QCC_FRZ_READ_ONLY  - Read (no freeze or reset)
   *    I14DEF_QCC_FRZ_NO_RESET   - Counter freeze without reset
   *    I14DEF_QCC_FRZ_RESET      - Counter freeze with reset
   *    I14DEF_QCC_FRZ_RESET_ONLY - Counter reset
   * returns:
   *  TMWDEFS_TRUE if counter was frozen
   *  TMWDEFS_FALSE otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_mitFreeze(
    void *pPoint, 
    TMWTYPES_UCHAR qcc);

/* Private ASDU Integrated Totals BCD */
 /* function: s14data_mitcGetPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  index - which point to return, 0 to n-1, where n is the number of bcd points
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_mitcGetPoint(
    void *pHandle, 
    TMWTYPES_USHORT index);
  
#if S14DATA_SUPPORT_DOUBLE_TRANS
  /* function: s14data_mitcLookupPoint 
   * purpose: Return a handle to a specific data point. This handle is
   *  used by s14data_mitcGetTransmissionMode to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  ioa - ioa number of point to return
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_mitcLookupPoint(
    void *pHandle,
    TMWTYPES_ULONG ioa);  

  /* function: s14data_mitcGetTransmissionMode 
   * purpose: Return event transmission mode for this point
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *   S14DATA_TRANSMISSION_SINGLE or S14DATA_TRANSMISSION_DOUBLE 
   */
  S14DATA_TRANSMISSION_MODE TMWDEFS_CALLBACK s14data_mitcGetTransmissionMode(
    void *pPoint);
#endif

/* function: s14data_mitcLookupPoint */
void * TMWDEFS_CALLBACK s14data_mitcLookupPoint(
  void *pHandle,
  TMWTYPES_ULONG ioa);
 
  /* function: s14data_mitcGetInfoObjAddr
   * purpose: Return the Information Object Address for this point
   *  NOTE: IOA 0 is not allowed by the 101/104 protocol.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Information Object Address
   */
  TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_mitcGetInfoObjAddr(
    void *pPoint);

  /* function: s14data_mitcGetGroupMask
   * purpose: Return the group mask indicating what groups this point 
   *  belongs to.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Group Mask indicating what groups this point belongs to.
   *  Returning TMWDEFS_GROUP_MASK_GENERAL indicates this point should be
   *   sent back in response to a CCINA when the RQT defined in 7.2.6.23 is 5
   *   indicating General Request Counter.
   *  Returning TMWDEFS_GROUP_MASK_1 indicates this point should be sent back in response
   *   to a CCINA when the RQT defined is 1 indicating counter group 1.
   *  Returning both of those bits would indicate this point would be included
   *  in responses to a CCINA with RQT of 5 or 1.
   */
  TMWDEFS_GROUP_MASK TMWDEFS_CALLBACK s14data_mitcGetGroupMask(
    void *pPoint);
 
  /* function: s14data_mitcGetFlags
   * purpose: Return the flags for this integrated totals point 
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   * returns:
   *  last byte of binary counter reading as defined in 7.2.6.9
   *   The following values (or OR'd combinations) are valid for this type:
   *    0 through 31  - Counter sequence number
   *    I14DEF_BCR_CY - Carry (Counter Overflow)
   *    I14DEF_BCR_CA - Counter was adjusted since last reading
   *    I14DEF_BCR_IV - Counter reading is invalid
   */
  TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mitcGetFlags(
    void *pPoint);

  /* function: s14data_mitcGetValue
   * purpose: Return the frozen value for this integrated totals point
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   *  pBCD - pointer to location to copy 6 byte Binary Coded Decimal value. 
   *   Least significant byte should be in pBCD[0]. Most significant byte 
   *   is in pBCD[5]. Should be padded with 0 values. 
   *   Value 123 decimal would be 0x23,0x01,0x00,0x00,0x00,0x00
   * returns:
   */
  void TMWDEFS_GLOBAL s14data_mitcGetValue(
    void *pPoint,
    TMWTYPES_UCHAR *pBCD);

  /* function: s14data_mitcGetValueFlagsTime
   * purpose: Return the value, flags and time for this integrated totals point
   *  This function will only be called if the sector is configured to return 
   *  typeId with timestamp in response to CRDNA or CCINA. 
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   *  pBCD - pointer to location to copy 6 byte Binary Coded Decimal value. 
   *   Least significant byte should be in pBCD[0]. Most significant byte 
   *   is in pBCD[5]. Should be padded with 0 values. 
   *   Value 123 decimal would be 0x23,0x01,0x00,0x00,0x00,0x00
   *  pFlags - pointer to location to return flags
   *           last byte of binary counter reading as defined in 7.2.6.9
   *  pTime - pointer to location to return timestamp for this value.
   * returns:
   */
  void TMWDEFS_GLOBAL s14data_mitcGetValueFlagsTime(
    void *pPoint,
    TMWTYPES_UCHAR *pBCD,
    TMWTYPES_UCHAR *pFlags,
    TMWDTIME *pTime);
 
  /* Monitored Single Point Information With Status Change Detection*/
  
  /* function: s14data_mpsnaGetPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  index - which point to return, 0 to n-1, where n is the number of mspna points
   * returns:
   *   void * - reference into database for this point or TMWDEFS_NULL 
   */
  void * TMWDEFS_CALLBACK s14data_mpsnaGetPoint(
    void *pHandle, 
    TMWTYPES_USHORT index);
 
  /* function: s14data_mpsnaGetInfoObjAddr
   * purpose: Return the Information Object Address for this point
   *  NOTE: IOA 0 is not allowed by the 101/104 protocol.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Information Object Address
   */
  TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_mpsnaGetInfoObjAddr(
    void *pPoint);

  /* function: s14data_mpsnaGetGroupMask
   * purpose: Return the group mask indicating what groups this point 
   *  belongs to
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Group Mask indicating what groups this point belongs to.
   *  Returning TMWDEFS_GROUP_MASK_GENERAL indicates this point should be
   *   sent back in response to a GI when the QOI defined in 7.2.6.22 is 20
   *  Returning TMWDEFS_GROUP_MASK_7 indicates this point should be sent back in 
   *   response to a GI when the QOI is 27
   *  Returning both of those bits set indicates this point should be included
   *   in responses to a GI with a QOI of either 20 or 27 
   *  Returning TMWDEFS_GROUP_MASK_PRIVATE_x indicates this point should be sent
   *   back in response to a GI when the QOI y in the private range is received. 
   *   This value should also be returned back from s14data_convertQOItoGroupMask() 
   *   for QOI y. For example QOI 64 could be mapped to TMWDEFS_GROUP_MASK_PRIVATE_1
   *   and QOI 100 could be mapped to TMWDEFS_GROUP_MASK_PRIVATE_2.
   *  Returning TMWDEFS_GROUP_MASK_CYCLIC indicates this point should be sent as 
   *   cyclic data
   *  Returning TMWDEFS_GROUP_MASK_BACKGROUND indicates this point should be sent as 
   *   the result of a background scan
   */
  TMWDEFS_GROUP_MASK TMWDEFS_CALLBACK s14data_mpsnaGetGroupMask(
    void *pPoint);

  /* function: s14data_mpsnaGetIndexed
   * purpose: Return whether this point should be sent using indexed or 
   *  sequential addressing
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  TMWDEFS_TRUE if this point should use indexed addressing
   *  TMWDEFS_FALSE if this point should use sequential addressing
   */
  TMWTYPES_BOOL TMWDEFS_CALLBACK s14data_mpsnaGetIndexed(
    void *pPoint);

  /* function: s14data_mpsnaGetFlags 
   * purpose: Return the flags for this packed single point 
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   * returns:
   *  quality descriptor as defined in 7.2.6.3
   *   The following values (or OR'd combinations) are valid for this type:
   *    I14DEF_QUALITY_OV - Overflow
   *    I14DEF_QUALITY_BL - Blocked
   *    I14DEF_QUALITY_SB - Substituted
   *    I14DEF_QUALITY_NT - Topical
   *    I14DEF_QUALITY_IV - Invalid
   */
  TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mpsnaGetFlags(
    void *pPoint);

  /* function: s14data_mpsnaGetValue   
   * purpose: Return the value for this packed single point
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   * returns:
   *  status and change detection, 32 bit as defined in 7.2.6.40
   */
  TMWTYPES_ULONG TMWDEFS_GLOBAL s14data_mpsnaGetValue(
    void *pPoint);
 
  /* function: s14data_mpsnaChanged
   * purpose: Determine if the specified point has changed and if so
   *  return the new value. The SCL will use the current time as the time
   *  the event occurred. Use s14mpsna_addEvent to add events and specify
   *  a time of occurrence.
   *  This function is used to scan for events on each data point.
   *  It will be called if S101SCTR_CONFIG or S104SCTR_CONFIG 
   *  rbeScanPeriod is nonzero and mpsnaScanEnabled == TMWDEFS_TRUE
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   *  pSCD - pointer to location into which to store value with 
   *   status and change dectection, 32 bit as defined in 7.2.6.40
   *  pQDS - pointer to location into which to store the quality descriptor
   *   as defined in 7.2.6.3
   *   The following values (or OR'd combinations) are valid for the quality
   *   descriptor for this type:
   *    I14DEF_QUALITY_OV - Overflow
   *    I14DEF_QUALITY_BL - Blocked
   *    I14DEF_QUALITY_SB - Substituted
   *    I14DEF_QUALITY_NT - Topical
   *    I14DEF_QUALITY_IV - Invalid
   *  pReason - pointer to location into which to store reason for change
   * returns:
   *  TMWDEFS_TRUE if the point has changed, else TMWDEFS_FALSE
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_mpsnaChanged(
    void *pPoint, 
    TMWTYPES_ULONG *pSCD,
    TMWTYPES_UCHAR *pQDS,
    TMWDEFS_CHANGE_REASON *pReason);

  /* Normalized Measurands Without Quality Descriptor */
  
  /* function: s14data_mmendGetPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  index - which point to return, 0 to n-1, where n is the number of mmend points
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_mmendGetPoint(
    void *pHandle, 
    TMWTYPES_USHORT index);

  /* function: s14data_mmendGetInfoObjAddr
   * purpose: Return the Information Object Address for this point
   *  NOTE: IOA 0 is not allowed by the 101/104 protocol.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Information Object Address
   */
  TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_mmendGetInfoObjAddr(
    void *pPoint);

  /* function: s14data_mmendGetGroupMask
   * purpose: Return the group mask indicating what groups this point 
   *  belongs to
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Group Mask indicating what groups this point belongs to.
   *  Returning TMWDEFS_GROUP_MASK_GENERAL indicates this point should be
   *   sent back in response to a GI when the QOI defined in 7.2.6.22 is 20
   *  Returning TMWDEFS_GROUP_MASK_7 indicates this point should be sent back in 
   *   response to a GI when the QOI is 27
   *  Returning both of those bits set indicates this point should be included
   *   in responses to a GI with a QOI of either 20 or 27 
   *  Returning TMWDEFS_GROUP_MASK_PRIVATE_x indicates this point should be sent
   *   back in response to a GI when the QOI y in the private range is received. 
   *   This value should also be returned back from s14data_convertQOItoGroupMask() 
   *   for QOI y. For example QOI 64 could be mapped to TMWDEFS_GROUP_MASK_PRIVATE_1
   *   and QOI 100 could be mapped to TMWDEFS_GROUP_MASK_PRIVATE_2.
   *  Returning TMWDEFS_GROUP_MASK_CYCLIC indicates this point should be sent as 
   *   cyclic data
   *  Returning TMWDEFS_GROUP_MASK_BACKGROUND indicates this point should be sent as 
   *   the result of a background scan
   */
  TMWDEFS_GROUP_MASK TMWDEFS_CALLBACK s14data_mmendGetGroupMask(
    void *pPoint);

  /* function: s14data_mmendGetIndexed
   * purpose: Return whether this point should be sent using indexed or 
   *  sequential addressing
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  TMWDEFS_TRUE if this point should use indexed addressing
   *  TMWDEFS_FALSE if this point should use sequential addressing
   */
  TMWTYPES_BOOL TMWDEFS_CALLBACK s14data_mmendGetIndexed(
    void *pPoint);

  /* function: s14data_mmendGetValue
   * purpose: Return the value for this Normalized Measurand 
   *  Without Quality Descriptor
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   * returns:
   *  normalized value as defined in 7.2.6.6
   *   A normalized value is a 16-bit value with a range of
   *   -1 to (1 - (2^(-15)))
   */
  TMWTYPES_SHORT TMWDEFS_GLOBAL s14data_mmendGetValue(
    void *pPoint);
 
  /* function: s14data_mmendChanged
   * purpose: Determine if the specified point has changed and if so
   *  return the new value. The SCL will use the current time as the time
   *  the event occurred. Use s14mmend_addEvent to add events and specify
   *  a time of occurrence.
   *  This function is used to scan for events on each data point.
   *  It will be called if S101SCTR_CONFIG or S104SCTR_CONFIG 
   *  rbeScanPeriod is nonzero and mmendScanEnabled == TMWDEFS_TRUE
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   *  pNVA - pointer to location into which to store normalized value 
   *   as defined in 7.2.6.6
   *   A normalized value is a 16-bit value with a range of
   *   -1 to (1 - (2^(-15)))
   *  pReason - pointer to location into which to store reason for change
   * returns:
   *  TMWDEFS_TRUE if the point has changed, else TMWDEFS_FALSE
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_mmendChanged(
    void *pPoint, 
    TMWTYPES_SHORT *pNVA,
    TMWDEFS_CHANGE_REASON *pReason);

  /* Event of protection equipment with time tag */
  
  /* function: s14data_meptaGetPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  index - which point to return, 0 to n-1, where n is the number of mepta points
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_meptaGetPoint(
    void *pHandle, 
    TMWTYPES_USHORT index);

  /* function: s14data_meptaGetInfoObjAddr
   * purpose: Return the Information Object Address for this point
   *  NOTE: IOA 0 is not allowed by the 101/104 protocol.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Information Object Address
   */
  TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_meptaGetInfoObjAddr(
    void *pPoint);
 
  /* function: s14data_meptaChanged
   * purpose: Determine if the specified point has changed and if so
   *  return the new value. The SCL will use the current time as the time
   *  the event occurred. Use s14mepta_addEvent to add events and specify
   *  a time of occurrence.
   *  This function is used to scan for events on each data point.
   *  It will be called if S101SCTR_CONFIG or S104SCTR_CONFIG 
   *  rbeScanPeriod is nonzero and meptaScanEnabled == TMWDEFS_TRUE
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   *  pSEP - pointer to location into which to store the single event of protection 
   *   equipment as defined in 7.2.6.10
   *   The following values (or OR'd combinations) are valid for this type:
   *    I14DEF_DIQ_INTERMEDIATE  - Indeterminate event state
   *    I14DEF_DIQ_OFF           - OFF
   *    I14DEF_DIQ_ON            - ON
   *    I14DEF_DIQ_INDETERMINATE - Indeterminate event state
   *    I14DEF_QUALITY_EI        - Elapsed time invalid
   *    I14DEF_QUALITY_BL        - Blocked
   *    I14DEF_QUALITY_SB        - Substituted
   *    I14DEF_QUALITY_NT        - Topical
   *    I14DEF_QUALITY_IV        - Invalid
   *  pElapsedTime - pointer to location into which to store the elapsed time
   *   as defined in 7.2.6.20
   *   The value is a 2 octet value representing an elapsed time, such as
   *   "Relay operating time" or "Relay duration time." The range is:
   *     0 to 59,999 milliseconds
   *  pReason - pointer to location into which to store reason for change
   * returns:
   *  TMWDEFS_TRUE if the point has changed, else TMWDEFS_FALSE
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_meptaChanged(
    void *pPoint, 
    TMWTYPES_UCHAR *pSEP,
    TMWTYPES_USHORT *pElapsedTime);

  /* Packed start events of protection equipment with time tag */

  /* function: s14data_meptbGetPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  index - which point to return, 0 to n-1, where n is the number of meptb points
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_meptbGetPoint(
    void *pHandle, 
    TMWTYPES_USHORT index);

  /* function: s14data_meptbGetInfoObjAddr
   * purpose: Return the Information Object Address for this point
   *  NOTE: IOA 0 is not allowed by the 101/104 protocol.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Information Object Address
   */
  TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_meptbGetInfoObjAddr(
    void *pPoint);

   /* function: s14data_meptbChanged
   * purpose: Determine if the specified point has changed and if so
   *  return the new value. The SCL will use the current time as the time
   *  the event occurred. Use s14meptb_addEvent to add events and specify
   *  a time of occurrence.
   *  This function is used to scan for events on each data point.
   *  It will be called if S101SCTR_CONFIG or S104SCTR_CONFIG 
   *  rbeScanPeriod is nonzero and meptbScanEnabled == TMWDEFS_TRUE
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   *  pSPE - pointer to location into which to store the start event of protection 
   *   equipment as defined in 7.2.6.11.
   *   Valid values are:
   *    I14DEF_SPE_GS  - General start of operation
   *    I14DEF_SPE_SL1 - Start of operation phase L1
   *    I14DEF_SPE_SL2 - Start of operation phase L2
   *    I14DEF_SPE_SL3 - Start of operation phase L3
   *    I14DEF_SPE_SIE - Start of operation IE (Earth current)
   *    I14DEF_SPE_SRD - Start of operation in reverse direction
   *  pQDP - pointer to location into which to store the quality descriptor of
   *   protection equipment as defined in 7.2.6.4
   *  pRelayDuration - pointer to location into which to store the 
   *   relay duration time as defined in 7.2.6.20
   *   The value is a 2 octet value representing an elapsed time, such as
   *   "Relay operating time" or "Relay duration time." The range is:
   *     0 to 59,999 milliseconds
   *  pReason - pointer to location into which to store reason for change
   * returns:
   *  TMWDEFS_TRUE if the point has changed, else TMWDEFS_FALSE
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_meptbChanged(
    void *pPoint,
    TMWTYPES_UCHAR *pSPE,
    TMWTYPES_UCHAR *pQDP,
    TMWTYPES_USHORT *pRelayDuration);

  /* Packed output circuit information of protection equipment with time tag */
  
  /* function: s14data_meptcGetPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  index - which point to return, 0 to n-1, where n is the number of meptc points
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_meptcGetPoint(
    void *pHandle, 
    TMWTYPES_USHORT index);
 
  /* function: s14data_meptcGetInfoObjAddr
   * purpose: Return the Information Object Address for this point
   *  NOTE: IOA 0 is not allowed by the 101/104 protocol.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Information Object Address
   */
  TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_meptcGetInfoObjAddr(
    void *pPoint);
 
  /* function: s14data_meptcChanged
   * purpose: Determine if the specified point has changed and if so
   *  return the new value. The SCL will use the current time as the time
   *  the event occurred. Use s14meptc_addEvent to add events and specify
   *  a time of occurrence.
   *  This function is used to scan for events on each data point.
   *  It will be called if S101SCTR_CONFIG or S104SCTR_CONFIG 
   *  rbeScanPeriod is nonzero and meptcScanEnabled == TMWDEFS_TRUE
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   *  pOCI - pointer to location into which to store the output circuit command 
   *   of event protection equipment as defined in 7.2.6.12
   *   Valid values are:
   *    I14DEF_OCI_GC  - General command to output circuit
   *    I14DEF_OCI_CL1 - Command to output circuit phase L1
   *    I14DEF_OCI_CL2 - Command to output circuit phase L2
   *    I14DEF_OCI_CL3 - Command to output circuit phase L3
   *  pQDP - pointer to location into which to store the quality descriptor of
   *   protection equipment as defined in 7.2.6.4. Valid values are:
   *    I14DEF_QUALITY_OV - Value has overflowed
   *    I14DEF_QUALITY_EI - Elapsed time invalid
   *    I14DEF_QUALITY_BL - Value is blocked for transmission
   *    I14DEF_QUALITY_NT - Not topical (not updated successfully)
   *    I14DEF_QUALITY_IV - Invalid, corresponding value is incorrect and
   *                          cannot be used
   *  pRealyOperating - pointer to location into which to store the 
   *   relay operating time as defined in 7.2.6.20
   *   The value is a 2 octet value representing an elapsed time, such as
   *   "Relay operating time" or "Relay duration time." The range is:
   *     0 to 59,999 milliseconds
   *  pReason - pointer to location into which to store reason for change
   * returns:
   *  TMWDEFS_TRUE if the point has changed, else TMWDEFS_FALSE
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_meptcChanged(
    void *pPoint,
    TMWTYPES_UCHAR *pOCI,
    TMWTYPES_UCHAR *pQDP,
    TMWTYPES_USHORT *pRelayOperating);

  /* Single Commands 
   *
   * Command Processing
   * Details are presented here using Single Point Commands (CSC) as an example.
   *  The exact same description applies to 
   *  Double Point Commands(CDC), and
   *  Regulating Step Commands (CRC). 
   *
   * SINGLE POINT COMMANDS
   * When a single point direct execute command is received from the master 
   * s14data_cscSelectRequired() will be called to verify that a select is not 
   * required before the execute. This should return TMWDEF_FALSE if a select 
   * is not required before the execute. If the select takes time to complete, it 
   * may return TMWDEFS_CMD_STAT_SELECTING.
   *
   * If it returned SELECTING, the function s14data_cscStatus() will then be 
   * called periodically. When the select completes, this status function 
   * should return TMWDEFS_CMD_STAT_SUCCESS or TMWDEFS_CMD_STAT_FAILED.
   *
   * s14data_cscExecute() will be called to perform the execute.
   * If the command can be performed immediately, the function should return 
   * TMWDEFS_CMD_STAT_SUCCESS or if it fails, it should return 
   * TMWDEFS_CMD_STAT_FAILED. If the command takes time, it may 
   * return TMWDEFS_CMD_STAT_EXECUTING.
   * 
   * If it returned SUCCESS or EXECUTING, the function s14data_cscStatus() will
   * then be called. If the command is still executing it should return
   * TMWDEFS_CMD_STAT_EXECUTING until the execute completes. At that time this
   * function should return TMWDEFS_CMD_STAT_SUCCESS or TMWDEFS_CMD_STAT_FAILED.
   * 
   * If the execute command or status command while executing returns FAILED an 
   * error (ACT CON Negative) will be sent back to the master.
   *
   * If the execute or status function returns TMWDEFS_CMD_STAT_SUCCESS or 
   * TMWDEFS_CMD_STAT_MONITORING, s14data_cscStatus() will be called periodically. 
   * As long as this returns MONITORING the SCL will continue to call
   * s14data_cscStatus() until it returns SUCCESS. At that time the SCL will call 
   * s14data_cscGetMonitoredPoint to determine if the SCL should read a monitored 
   * feedback point to generate a change event. 
   * A failure while monitoring will need to be returned via the flag bits in the 
   * monitored data. The protocol does not allow for an ACT TERM Negative to be sent.
   *
   * If you do NOT want the SCL to automatically generate a change event with the 
   * current value, s14data_cscGetMonitoredPoint should return TMWDEFS_NULL.
   * If for example the value of the point did not change, returning TMWDEFS_NULL
   * will prevent an event from being generated. If this function returns non-NULL 
   * and the SCL generates a change event, your application should not normally 
   * also generate a change event for this point. You can choose to return 
   * TMWDEFS_NULL and generate your own change event, but determining what COT to
   * use, spontaneous, remote or local will require your code to determine if the 
   * change was the result of this command or another spontaneous reason.
   *
   * Note:
   * If select before execute is required, a call to s14data_cscSelect() will 
   * precede the call to s14data_cscExecute() This can return 
   * TMWDEFS_CMD_STAT_SELECTING, TMWDEFS_CMD_STAT_FAILED or TMWDEFS_CMD_STAT_SUCCESS. 
   * If it returned SELECTING, s14data_cscStatus() will be called until it returns 
   * TMWDEFS_CMD_STAT_SUCCESS or TMWDEFS_CMD_STAT_FAILED. If it returned SUCCESS, 
   * s14data_cscExecute() will be called.
   */
  
  /* function: s14data_cscGetPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  index - which point to return, 0 to n-1, where n is the number of csc points
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_cscGetPoint(
    void *pHandle, 
    TMWTYPES_USHORT index);
 
  /* function: s14data_cscLookupPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  ioa - Information Object Address of point to return
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_GLOBAL s14data_cscLookupPoint(
    void *pHandle, 
    TMWTYPES_ULONG ioa);
 
  /* function: s14data_cscGetInfoObjAddr
   * purpose: Return the Information Object Address for this point
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Information Object Address
   */
  TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_cscGetInfoObjAddr(
    void *pPoint);

  /* function: s14data_cscSelectRequired
   * purpose: Determine whether a select is required before 
   *  execute for this point
   *  Normally this would return false and allow the master to 
   *  decide whether to issue the select before execute. If this
   *  returns true, then this slave requires a select before execute
   *  for this particular point.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  TMWDEFS_TRUE if a select is required
   *  TMWDEFS_FALSE if not required
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_cscSelectRequired(
    void *pPoint);
 
  /* function: s14data_cscGetMonitoredPoint
   * purpose:  Return a handle to a monitored feedback point 
   *  if the requested point is being monitored. This handle is
   *  used by other s14data_mspGetxxx routines to access to this monitored point.
   * arguments:
   *  pPoint - reference into database for this point
   * returns: 
   *  void * - reference into database for the monitored point or TMWDEFS_NULL
   */
  void * TMWDEFS_GLOBAL s14data_cscGetMonitoredPoint(
    void *pPoint);

  /* function: s14data_cscSelect
   * purpose:Issue a select for this point. See above for description of 
   *  Single Point Command processing.
   * arguments:
   *  pPoint - reference into database for this point
   *  cot - cause of transmission as defined in 7.2.3
   *   I14DEF_COT_ACTIVATION   
   *  sco - single command as defined in 7.2.6.15. 
   *    This contains single command state (SCS) and Qualifier of Command QOC
   *      combined into a single byte
   *    Valid SCS values in bit I14DEF_SCS_MASK are:
   *      I14DEF_SCS_OFF            - Single command state = OFF
   *      I14DEF_SCS_ON             - Single command state = ON
   *    Valid QOC QU values in bits I14DEF_QOC_QU_MASK are:
   *      I14DEF_QOC_QU_USE_DEFAULT - No additional definition      
   *      I14DEF_QOC_QU_SHORT_PULSE - Short pulse duration (circuit-breaker)
   *      I14DEF_QOC_QU_LONG_PULSE  - Long pulse duration
   *      I14DEF_QOC_QU_PERSISTENT  - Persistent output  
   *    Valid QOC QU Select/Execute values in bit I14DEF_QOC_SE_MASK are:
   *      I14DEF_QOC_SE_SELECT      - Select               
   * returns:
   *  status of command 
   *   TMWDEFS_CMD_STAT_SELECTING
   *   TMWDEFS_CMD_STAT_FAILED
   *   TMWDEFS_CMD_STAT_SUCCESS
   */
  TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_cscSelect(
    void *pPoint, 
    TMWTYPES_UCHAR cot, 
    TMWTYPES_UCHAR sco);

  /* XXX added by Lucy */
  TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s4data_cscSelect(
    void *pPoint,
    TMWTYPES_UCHAR cot,
    TMWTYPES_UCHAR sco);

  /* function: s14data_cscExecute
   * purpose:Issue an execute for this point. See above for description of 
   *  Single Point Command processing.
   * arguments:
   *  pPoint - reference into database for this point
   *  cot - cause of transmission as defined in 7.2.3 
   *   always I14DEF_COT_ACTIVATION   - execute command
   *  sco - single command as defined in 7.2.6.15. 
   *    This contains single command state (SCS) and Qualifier of Command QOC
   *      combined into a single byte
   *    Valid SCS values in bit I14DEF_SCS_MASK are:
   *      I14DEF_SCS_OFF            - Single command state = OFF
   *      I14DEF_SCS_ON             - Single command state = ON
   *    Valid QOC QU values in bits I14DEF_QOC_QU_MASK are:
   *      I14DEF_QOC_QU_USE_DEFAULT - No additional definition      
   *      I14DEF_QOC_QU_SHORT_PULSE - Short pulse duration (circuit-breaker)
   *      I14DEF_QOC_QU_LONG_PULSE  - Long pulse duration
   *      I14DEF_QOC_QU_PERSISTENT  - Persistent output  
   *    Valid QOC QU Select/Execute values in bit I14DEF_QOC_SE_MASK are:
   *      I14DEF_QOC_SE_EXECUTE     - Execute        
   * returns:
   *  status of command 
   *   TMWDEFS_CMD_STAT_EXECUTING
   *   TMWDEFS_CMD_STAT_FAILED
   *   TMWDEFS_CMD_STAT_SUCCESS
   */
  TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_cscExecute(
    void *pPoint, 
    TMWTYPES_UCHAR cot, 
    TMWTYPES_UCHAR sco);

  /* XXX added by Lucy */
  TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s4data_cscExecute(
    void *pPoint,
    TMWTYPES_UCHAR cot,
    TMWTYPES_UCHAR sco);
 
  /* function: s14data_cscStatus
   * purpose:Get current status for this point. See above for description of 
   *  Single Point Command processing.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  status of command 
   *   TMWDEFS_CMD_STAT_SELECTING
   *   TMWDEFS_CMD_STAT_EXECUTING
   *   TMWDEFS_CMD_STAT_MONITORING
   *   TMWDEFS_CMD_STAT_FAILED
   *   TMWDEFS_CMD_STAT_SUCCESS
   */
  TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_cscStatus(
    void *pPoint);

  /* Double Commands 
   *
   * Command Processing
   * Details are the same as the Single Point Commands (CSC) description above.
   *  
   */

  /* function: s14data_cdcGetPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  index - which point to return, 0 to n-1, where n is the number of cdc points
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_cdcGetPoint(
    void *pHandle, 
    TMWTYPES_USHORT index);

  /* function: s14data_cdcLookupPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  ioa - Information Object Address of point to return
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_GLOBAL s14data_cdcLookupPoint(
    void *pHandle, 
    TMWTYPES_ULONG ioa);
 
  /* function: s14data_cdcGetInfoObjAddr
   * purpose: Return the Information Object Address for this point
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Information Object Address
   */
  TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_cdcGetInfoObjAddr(
    void *pPoint);

  /* function: s14data_cdcSelectRequired
   * purpose: Determine whether a select is required before 
   *  execute for this point.
   *  Normally this would return false and allow the master to 
   *  decide whether to issue the select before execute. If this
   *  returns true, then this slave requires a select before execute
   *  for this particular point.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  TMWDEFS_TRUE if a select is required
   *  TMWDEFS_FALSE if not required
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_cdcSelectRequired(
    void *pPoint);
 
  /* function: s14data_cdcGetMonitoredPoint
   * purpose:  Return a handle to a monitored feedback point 
   *  if the requested point is being monitored. This handle is
   *  used by other s14data_mdpGetxxx routines to access to this monitored point.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  void * - reference into database for the monitored point or TMWDEFS_NULL
   */
  void * TMWDEFS_GLOBAL s14data_cdcGetMonitoredPoint(
    void *pPoint);

  /* function: s14data_cdcSelect
   * purpose:Issue a select for this point. See above for description of 
   *  Double Point Command processing.
   * arguments:
   *  pPoint - reference into database for this point
   *  cot - cause of transmission as defined in 7.2.3
   *   I14DEF_COT_ACTIVATION   
   *  dco - double command as defined in 7.2.6.16. 
   *    This contains double command state (DCS) and Qualifier of Command QOC
   *      combined into a single byte
   *    Valid SCS values in bits I14DEF_DCS_MASK are:
   *      I14DEF_DCS_OFF            - Double command state = OFF
   *      I14DEF_DCS_ON             - Double command state = ON
   *    Valid QOS QU values in bits I14DEF_QOC_QU_MASK are:
   *      I14DEF_QOC_QU_USE_DEFAULT - No additional definition      
   *      I14DEF_QOC_QU_SHORT_PULSE - Short pulse duration (circuit-breaker)
   *      I14DEF_QOC_QU_LONG_PULSE  - Long pulse duration
   *      I14DEF_QOC_QU_PERSISTENT  - Persistent output  
   *    Valid QOS QU Select/Execute values in bit I14DEF_QOC_SE_MASK are:
   *      I14DEF_QOC_SE_SELECT      - Select      
   * returns:
   *  status of command 
   *   TMWDEFS_CMD_STAT_SELECTING 
   *   TMWDEFS_CMD_STAT_FAILED
   *   TMWDEFS_CMD_STAT_SUCCESS
   */
  TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_cdcSelect(
    void *pPoint, 
    TMWTYPES_UCHAR cot, 
    TMWTYPES_UCHAR dco);

  /* XXX added by Lucy */
  TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s4data_cdcSelect(
    void *pPoint,
    TMWTYPES_UCHAR cot,
    TMWTYPES_UCHAR dco);
 
  /* function: s14data_cdcExecute
   * purpose:Issue an execute for this point. See above for description of 
   *  Double Point Command processing.
   * arguments:
   *  pPoint - reference into database for this point
   *  cot - cause of transmission as defined in 7.2.3 
   *   always I14DEF_COT_ACTIVATION   - execute command
   *  dco - double command as defined in 7.2.6.16. 
   *    This contains double command state (DCS) and Qualifier of Command QOC
   *      combined into a single byte
   *    Valid SCS values in bits I14DEF_DCS_MASK are:
   *      I14DEF_DCS_OFF            - Double command state = OFF
   *      I14DEF_DCS_ON             - Double command state = ON
   *    Valid QOS QU values in bits I14DEF_QOC_QU_MASK are:
   *      I14DEF_QOC_QU_USE_DEFAULT - No additional definition      
   *      I14DEF_QOC_QU_SHORT_PULSE - Short pulse duration (circuit-breaker)
   *      I14DEF_QOC_QU_LONG_PULSE  - Long pulse duration
   *      I14DEF_QOC_QU_PERSISTENT  - Persistent output  
   *    Valid QOS QU Select/Execute values in bit I14DEF_QOC_SE_MASK are:
   *      I14DEF_QOC_SE_EXECUTE     - Execute              
   * returns:
   *  status of command 
   *   TMWDEFS_CMD_STAT_EXECUTING
   *   TMWDEFS_CMD_STAT_FAILED
   *   TMWDEFS_CMD_STAT_SUCCESS
   */
  TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_cdcExecute(
    void *pPoint, 
    TMWTYPES_UCHAR cot, 
    TMWTYPES_UCHAR dco);

  /* XXX added by Lucy */
  TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s4data_cdcExecute(
    void *pPoint,
    TMWTYPES_UCHAR cot,
    TMWTYPES_UCHAR dco);

  /* function: s14data_cdcStatus
   * purpose:Get current status for this point. See above for description of 
   *  Double Point Command processing.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  status of command 
   *   TMWDEFS_CMD_STAT_SELECTING
   *   TMWDEFS_CMD_STAT_EXECUTING
   *   TMWDEFS_CMD_STAT_MONITORING
   *   TMWDEFS_CMD_STAT_FAILED
   *   TMWDEFS_CMD_STAT_SUCCESS
   */
  TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_cdcStatus(
    void *pPoint);

  /* Regulating Step Commands
   *
   * Command Processing
   * Details are the same as the Single Point Commands (CSC) description above.
   *  
   */

  /* function: s14data_crcGetPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  index - which point to return, 0 to n-1, where n is the number of crc points
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_crcGetPoint(
    void *pHandle, 
    TMWTYPES_USHORT index);

  /* function: s14data_crcLookupPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  ioa - Information Object Address of point to return
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_GLOBAL s14data_crcLookupPoint(
    void *pHandle, 
    TMWTYPES_ULONG ioa);
 
  /* function: s14data_crcGetInfoObjAddr
   * purpose: Return the Information Object Address for this point
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Information Object Address
   */
  TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_crcGetInfoObjAddr(
    void *pPoint);

  /* function: s14data_crcSelectRequired
   * purpose: Determine whether a select is required before 
   *  execute for this point
   *  Normally this would return false and allow the master to 
   *  decide whether to issue the select before execute. If this
   *  returns true, then this slave requires a select before execute
   *  for this particular point.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  TMWDEFS_TRUE if a select is required
   *  TMWDEFS_FALSE if not required
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_crcSelectRequired(
    void *pPoint);
 
   /* function: s14data_crcGetMonitoredPoint
   * purpose:  Return a handle to a monitored feedback point 
   *  if the requested point is being monitored. This handle is
   *  used by other s14data_mstGetxxx routines to access to this monitored point.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  void * - reference into database for the monitored point or TMWDEFS_NULL
   */
  void * TMWDEFS_GLOBAL s14data_crcGetMonitoredPoint(
    void *pPoint);
 
  /* function: s14data_crcSelect
   * purpose:Issue a select for this point. See above for description of 
   *  Regulating Step Command processing.
   * arguments:
   *  pPoint - reference into database for this point
   *  cot - cause of transmission as defined in 7.2.3
   *   I14DEF_COT_ACTIVATION   
   *  rco - regulating step command as defined in 7.2.6.17. 
   *    This contains regulating step command (RCO) and Qualifier of Command QOC
   *      combined into a single byte
   *    Valid RCO values in bits I14DEF_RCS_MASK are:
   *      I14DEF_RCS_STEP_LOWER     - next step LOWER
   *      I14DEF_RCS_STEP_HIGHER    - next step HIGHER 
   *    Valid QOS QU values in bits I14DEF_QOC_QU_MASK are:
   *      I14DEF_QOC_QU_USE_DEFAULT - No additional definition      
   *      I14DEF_QOC_QU_SHORT_PULSE - Short pulse duration (circuit-breaker)
   *      I14DEF_QOC_QU_LONG_PULSE  - Long pulse duration
   *      I14DEF_QOC_QU_PERSISTENT  - Persistent output  
   *    Valid QOS QU Select/Execute values in bit I14DEF_QOC_SE_MASK are:
   *      I14DEF_QOC_SE_SELECT      - Select         
   * returns:
   *  status of command 
   *   TMWDEFS_CMD_STAT_SELECTING
   *   TMWDEFS_CMD_STAT_FAILED
   *   TMWDEFS_CMD_STAT_SUCCESS
   */
  TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_crcSelect(
    void *pPoint, 
    TMWTYPES_UCHAR cot, 
    TMWTYPES_UCHAR rco);

  /* function: s14data_crcExecute
   * purpose:Issue an execute for this point. See above for description of 
   *  Regulating Step Command processing.
   * arguments:
   *  pPoint - reference into database for this point
   *  cot - cause of transmission as defined in 7.2.3 
   *   always I14DEF_COT_ACTIVATION   - execute command
   *  rco - regulating step command as defined in 7.2.6.17. 
   *    This contains regulating step command (RCO) and Qualifier of Command QOC
   *      combined into a single byte
   *    Valid RCO values in bits I14DEF_RCS_MASK are:
   *      I14DEF_RCS_STEP_LOWER     - next step LOWER
   *      I14DEF_RCS_STEP_HIGHER    - next step HIGHER 
   *    Valid QOS QU values in bits I14DEF_QOC_QU_MASK are:
   *      I14DEF_QOC_QU_USE_DEFAULT - No additional definition      
   *      I14DEF_QOC_QU_SHORT_PULSE - Short pulse duration (circuit-breaker)
   *      I14DEF_QOC_QU_LONG_PULSE  - Long pulse duration
   *      I14DEF_QOC_QU_PERSISTENT  - Persistent output  
   *    Valid QOS QU Select/Execute values in bit I14DEF_QOC_SE_MASK are:
   *      I14DEF_QOC_SE_EXECUTE     - Execute              
   * returns:
   *  status of command 
   *   TMWDEFS_CMD_STAT_EXECUTING
   *   TMWDEFS_CMD_STAT_FAILED
   *   TMWDEFS_CMD_STAT_SUCCESS
   */
  TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_crcExecute(
    void *pPoint, 
    TMWTYPES_UCHAR cot, 
    TMWTYPES_UCHAR rco);

 /* function: s14data_crcStatus
   * purpose:Get current status for this point. See above for description of 
   *  Regulating Step Command processing.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  status of command 
   *   TMWDEFS_CMD_STAT_SELECTING
   *   TMWDEFS_CMD_STAT_EXECUTING
   *   TMWDEFS_CMD_STAT_MONITORING
   *   TMWDEFS_CMD_STAT_FAILED
   *   TMWDEFS_CMD_STAT_SUCCESS
   */
  TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_crcStatus(
    void *pPoint);

  /* Bitstring Commands 
   *
   * Command Processing
   *   
   * Neither select before execute nor monitoring applies.
   *
   * When a bitstring point direct execute command is received from the master 
   * s14data_cboExecute() will be called to perform the execute.
   * This function should return TMWDEFS_CMD_STAT_SUCCESS or if it fails,
   * it should return TMWDEFS_CMD_STAT_FAILED. 
   */
  
  /* function: s14data_cboGetPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  index - which point to return, 0 to n-1, where n is the number of cbo points
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_cboGetPoint(
    void *pHandle, 
    TMWTYPES_USHORT index);

  /* function: s14data_cboLookupPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  ioa - Information Object Address of point to return
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_GLOBAL s14data_cboLookupPoint(
    void *pHandle, 
    TMWTYPES_ULONG ioa);
 
  /* function: s14data_cboExecute
   * purpose:Issue an execute for this point. See above for description of 
   *  BitString Command processing.
   * arguments:
   *  pPoint - reference into database for this point
   *  value - binary state information as defined in 7.2.6.13
   *    Binary state information consists of a 32-bit value, representing
   *    32 1-bit binary states
   * returns:
   *  status of command 
   *   TMWDEFS_CMD_STAT_FAILED
   *   TMWDEFS_CMD_STAT_SUCCESS
   *   NOTE:
   *    TMWDEFS_CMD_STAT_EXECUTING would be considered a failure by the SCL
   */
  TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_cboExecute(
    void *pPoint, 
    TMWTYPES_ULONG value);

  /* Normalized Measurand Commands
   *
   * Command Processing
   * Details are presented here using Normalized Commands (CSENA) as an example.
   *  The exact same description applies to 
   *  Scaled Commands (CSENB) and 
   *  Floating Commands (CSENC).
   *  
   * NORMALIZED COMMANDS
   * When a Normalized direct execute command is received from the master 
   * s14data_csenaSelectRequired() will be called to verify that a select is not
   * required before the execute. This should return TMWDEF_FALSE if a select 
   * is not required before the execute.
   *
   * s14data_csenaExecute() will be called to perform the execute.
   * If the command can be performed immediately,the function should return 
   * TMWDEFS_CMD_STAT_SUCCESS or if it fails, it should return 
   * TMWDEFS_CMD_STAT_FAILED. If the command takes time to complete, it should 
   * return TMWDEFS_CMD_STAT_EXECUTING. 
   * 
   * If it returned EXECUTING, the function s14data_csenaStatus() will then be 
   * called periodically. When the execute completes, this status function 
   * should return TMWDEFS_CMD_STAT_SUCCESS or TMWDEFS_CMD_STAT_FAILED.
   *
   * If the execute or status command returns FAILED at any time an error 
   * (ACT CON Negative) will be sent back to the master.
   *
   * MONITORING mode does not apply to the CSENA CSENB or CSENC commands.
   *
   * Note:
   * If select before execute is required, a call to s14data_csenaSelect() will 
   * precede the call to s14data_csenaExecute() This can return 
   * TMWDEFS_CMD_STAT_SELECTING, TMWDEFS_CMD_STAT_FAILED or TMWDEFS_CMD_STAT_SUCCESS. 
   * If it returned SELECTING, s14data_csenaStatus() will be called until it 
   * returns TMWDEFS_CMD_STAT_SUCCESS or TMWDEFS_CMD_STAT_FAILED. If it returned 
   * SUCCESS, s14data_csenaExecute() will be called.
   */

  /* function: s14data_csenaGetPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  index - which point to return, 0 to n-1, where n is the number of csena points
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_csenaGetPoint(
    void *pHandle, 
    TMWTYPES_USHORT index);

  /* function: s14data_csenaLookupPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  ioa - Information Object Address of point to return
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_GLOBAL s14data_csenaLookupPoint(
    void *pHandle, 
    TMWTYPES_ULONG ioa);
 
  /* function: s14data_csenaGetInfoObjAddr
   * purpose: Return the Information Object Address for this point
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Information Object Address
   */
  TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_csenaGetInfoObjAddr(
    void *pPoint);
    
  /* function: s14data_csenaSelectRequired
   * purpose: Determine whether a select is required before 
   *  execute for this point
   *  Normally this would return false and allow the master to 
   *  decide whether to issue the select before execute. If this
   *  returns true, then this slave requires a select before execute
   *  for this particular point.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  TMWDEFS_TRUE if a select is required
   *  TMWDEFS_FALSE if not required
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_csenaSelectRequired(
    void *pPoint);

  /* function: s14data_csenaSelect
   * purpose:Issue a select for this point. See above for description of 
   *  Normalized Command processing.
   * arguments:
   *  pPoint - reference into database for this point
   *  cot - cause of transmission as defined in 7.2.3
   *   I14DEF_COT_ACTIVATION   
   *  value - normalized value as defined in 7.2.6.6
   *   A normalized value is a 16-bit value with a range of
   *   -1 to (1 - (2^(-15)))
   *  qos - quality of set-point command as defined in 7.2.6.39
   *    The following values (or OR'd combinations of I14DEF_QOS_QL and
   *    I14DEF_QOS_SE fields) are valid for this type:
   *      I14DEF_QOS_QL_USE_DEFAULT - Default 
   *      I14DEF_QOS_SE_EXECUTE     - Execute
   *      I14DEF_QOS_SE_SELECT      - Select
   * returns:
   *  status of command 
   *   TMWDEFS_CMD_STAT_SELECTING
   *   TMWDEFS_CMD_STAT_FAILED
   *   TMWDEFS_CMD_STAT_SUCCESS
   */
  TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_csenaSelect(
    void *pPoint, 
    TMWTYPES_UCHAR cot, 
    TMWTYPES_SHORT value, 
    TMWTYPES_UCHAR qos);

  /* function: s14data_csenaExecute
   * purpose:Issue an execute for this point. See above for description of 
   *  Normalized Command processing.
   * arguments:
   *  pPoint - reference into database for this point
   *  cot - cause of transmission as defined in 7.2.3 
   *   always I14DEF_COT_ACTIVATION   - execute command
   *  value - normalized value as defined in 7.2.6.6
   *   A normalized value is a 16-bit value with a range of
   *   -1 to (1 - (2^(-15)))
   *  qos - quality of set-point command as defined in 7.2.6.39
   *    The following values (or OR'd combinations of I14DEF_QOS_QL and
   *    I14DEF_QOS_SE fields) are valid for this type:
   *      I14DEF_QOS_QL_USE_DEFAULT - Default 
   *      I14DEF_QOS_SE_EXECUTE     - Execute
   * returns:
   *  status of command 
   *   TMWDEFS_CMD_STAT_EXECUTING
   *   TMWDEFS_CMD_STAT_FAILED
   *   TMWDEFS_CMD_STAT_SUCCESS
   */
  TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_csenaExecute(
    void *pPoint, 
    TMWTYPES_UCHAR cot, 
    TMWTYPES_SHORT value, 
    TMWTYPES_UCHAR qos);
  
  /* function: s14data_csenaStatus
   * purpose:Get current status for this point. See above for description of 
   *  Normalized Command processing.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  status of command 
   *   TMWDEFS_CMD_STAT_SELECTING
   *   TMWDEFS_CMD_STAT_EXECUTING
   *   TMWDEFS_CMD_STAT_FAILED
   *   TMWDEFS_CMD_STAT_SUCCESS
   */
  TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_csenaStatus(
    void *pPoint);

  /* Scaled Measurand Commands
   *
   * Command Processing
   * Details are the same as the Normalized Measurand Commands (CSENA)
   * description above.
   *
   */
  
  /* function: s14data_csenbGetPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  index - which point to return, 0 to n-1, where n is the number of csenb points
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_csenbGetPoint(
    void *pHandle, 
    TMWTYPES_USHORT index);

  /* function: s14data_csenbLookupPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  ioa - Information Object Address of point to return
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_GLOBAL s14data_csenbLookupPoint(
    void *pHandle, 
    TMWTYPES_ULONG ioa);
 
  /* function: s14data_csenbGetInfoObjAddr
   * purpose: Return the Information Object Address for this point
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Information Object Address
   */
  TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_csenbGetInfoObjAddr(
    void *pPoint);

  /* function: s14data_csenbSelectRequired
   * purpose: Determine whether a select is required before 
   *  execute for this point
   *  Normally this would return false and allow the master to 
   *  decide whether to issue the select before execute. If this
   *  returns true, then this slave requires a select before execute
   *  for this particular point.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  TMWDEFS_TRUE if a select is required
   *  TMWDEFS_FALSE if not required
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_csenbSelectRequired(
    void *pPoint);

  /* function: s14data_csenbSelect
   * purpose:Issue a select for this point. See above for description of 
   *  Scaled Command processing.
   * arguments:
   *  pPoint - reference into database for this point
   *  cot - cause of transmission as defined in 7.2.3
   *   I14DEF_COT_ACTIVATION   
   *  value - scaled value as defined in 7.2.6.7
   *   A scaled value is a 16-bit value with a range of
   *   -(2^15) to ((2^15) - 1)
   *  qos - quality of set-point command as defined in 7.2.6.39
   *    The following values (or OR'd combinations of I14DEF_QOS_QL and
   *    I14DEF_QOS_SE fields) are valid for this type:
   *      I14DEF_QOS_QL_USE_DEFAULT - Default 
   *      I14DEF_QOS_SE_SELECT      - Select
   * returns:
   *  status of command 
   *   TMWDEFS_CMD_STAT_SELECTING 
   *   TMWDEFS_CMD_STAT_FAILED
   *   TMWDEFS_CMD_STAT_SUCCESS
   */
  TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_csenbSelect(
    void *pPoint, 
    TMWTYPES_UCHAR cot, 
    TMWTYPES_SHORT value, 
    TMWTYPES_UCHAR qos);

  /* function: s14data_csenbExecute
   * purpose:Issue an execute for this point. See above for description of 
   *  Scaled Command processing.
   * arguments:
   *  pPoint - reference into database for this point
   *  cot - cause of transmission as defined in 7.2.3 
   *   always I14DEF_COT_ACTIVATION   - execute command
   *  value - scaled value as defined in 7.2.6.7
   *   A scaled value is a 16-bit value with a range of
   *   -(2^15) to ((2^15) - 1)
   *  qos - quality of set-point command as defined in 7.2.6.39
   *    The following values (or OR'd combinations of I14DEF_QOS_QL and
   *    I14DEF_QOS_SE fields) are valid for this type:
   *      I14DEF_QOS_QL_USE_DEFAULT - Default 
   *      I14DEF_QOS_SE_EXECUTE     - Execute
   * returns:
   *  status of command 
   *   TMWDEFS_CMD_STAT_EXECUTING
   *   TMWDEFS_CMD_STAT_FAILED
   *   TMWDEFS_CMD_STAT_SUCCESS
   */
  TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_csenbExecute(
    void *pPoint, 
    TMWTYPES_UCHAR cot, 
    TMWTYPES_SHORT value, 
    TMWTYPES_UCHAR qos);
 
  /* function: s14data_csenbStatus
   * purpose:Get current status for this point. See above for description of 
   *  Scaled Command processing.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  status of command 
   *   TMWDEFS_CMD_STAT_SELECTING
   *   TMWDEFS_CMD_STAT_EXECUTING
   *   TMWDEFS_CMD_STAT_FAILED
   *   TMWDEFS_CMD_STAT_SUCCESS
   */
  TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_csenbStatus(
    void *pPoint);

  /* Floating Point Measurand Commands    
   *
   * Command Processing
   * Details are the same as the Normalized Measurand Commands (CSENA)
   * description above.
   *
   */

  /* function: s14data_csencGetPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  index - which point to return, 0 to n-1, where n is the number of csenc points
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_csencGetPoint(
    void *pHandle, 
    TMWTYPES_USHORT index);

  /* function: s14data_csencLookupPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  ioa - Information Object Address of point to return
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_GLOBAL s14data_csencLookupPoint(
    void *pHandle, 
    TMWTYPES_ULONG ioa);
 
  /* function: s14data_csencGetInfoObjAddr
   * purpose: Return the Information Object Address for this point
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Information Object Address
   */
  TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_csencGetInfoObjAddr(
    void *pPoint);

  /* function: s14data_csencSelectRequired
   * purpose: Determine whether a select is required before 
   *  execute for this point
   *  Normally this would return false and allow the master to 
   *  decide whether to issue the select before execute. If this
   *  returns true, then this slave requires a select before execute
   *  for this particular point.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  TMWDEFS_TRUE if a select is required
   *  TMWDEFS_FALSE if not required
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_csencSelectRequired(
    void *pPoint);

#if S14DATA_SUPPORT_CSE_C
  /* function: s14data_csencSelect
   * purpose:Issue a select for this point. See above for description of 
   *  Floating Point Command processing.
   * arguments:
   *  pPoint - reference into database for this point
   *  cot - cause of transmission as defined in 7.2.3
   *   I14DEF_COT_ACTIVATION   
   *  value - short floating point number as defined in 7.2.6.8
   *   A float is defined according to IEEE STD 754
   *  qos - quality of set-point command as defined in 7.2.6.39
   *    The following values (or OR'd combinations of I14DEF_QOS_QL and
   *    I14DEF_QOS_SE fields) are valid for this type:
   *      I14DEF_QOS_QL_USE_DEFAULT - Default 
   *      I14DEF_QOS_SE_SELECT      - Select
   * returns:
   *  status of command 
   *   TMWDEFS_CMD_STAT_SELECTING
   *   TMWDEFS_CMD_STAT_FAILED
   *   TMWDEFS_CMD_STAT_SUCCESS
   */
  TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_csencSelect(
    void *pPoint, 
    TMWTYPES_UCHAR cot, 
    TMWTYPES_SFLOAT value, 
    TMWTYPES_UCHAR qos);
 
  /* function: s14data_csencExecute
   * purpose:Issue an execute for this point. See above for description of 
   *  Floating Point Command processing.
   * arguments:
   *  pPoint - reference into database for this point
   *  cot - cause of transmission as defined in 7.2.3 
   *   always I14DEF_COT_ACTIVATION   - execute command
   *  value - short floating point number as defined in 7.2.6.8
   *   A float is defined according to IEEE STD 754
   *  qos - quality of set-point command as defined in 7.2.6.39
   *    The following values (or OR'd combinations of I14DEF_QOS_QL and
   *    I14DEF_QOS_SE fields) are valid for this type:
   *      I14DEF_QOS_QL_USE_DEFAULT - Default 
   *      I14DEF_QOS_SE_EXECUTE     - Execute
   * returns:
   *  status of command 
   *   TMWDEFS_CMD_STAT_EXECUTING
   *   TMWDEFS_CMD_STAT_FAILED
   *   TMWDEFS_CMD_STAT_SUCCESS
   */
  TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_csencExecute(
    void *pPoint, 
    TMWTYPES_UCHAR cot, 
    TMWTYPES_SFLOAT value, 
    TMWTYPES_UCHAR qos);
#endif

  /* function: s14data_csencStatus
   * purpose:Get current status for this point. See above for description of 
   *  Floating Point Command processing.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  status of command 
   *   TMWDEFS_CMD_STAT_SELECTING
   *   TMWDEFS_CMD_STAT_EXECUTING
   *   TMWDEFS_CMD_STAT_FAILED
   *   TMWDEFS_CMD_STAT_SUCCESS
   */
  TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_csencStatus(
    void *pPoint);

  /* Parameter of Measured Value, Normalized Value */ 
  
  /* function: s14data_pmenaGetPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  index - which point to return, 0 to n-1, where n is the number of pmena points
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_pmenaGetPoint(
    void *pHandle, 
    TMWTYPES_USHORT index);

  /* function: s14data_pmenaLookupPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  ioa - Information Object Address of point to return
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_GLOBAL s14data_pmenaLookupPoint(
    void *pHandle, 
    TMWTYPES_ULONG ioa);
 
  /* function: s14data_pmenaGetInfoObjAddr
   * purpose: Return the Information Object Address for this point
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Information Object Address
   */
  TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_pmenaGetInfoObjAddr(
    void *pPoint);
 
  /* function: s14data_pmenaGetGroupMask
   * purpose: Return the group mask indicating what groups this point 
   *  belongs to
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Group Mask indicating what groups this point belongs to.
   *  Returning TMWDEFS_GROUP_MASK_GENERAL indicates this point should be
   *   sent back in response to a GI when the QOI defined in 7.2.6.22 is 20
   *  Returning TMWDEFS_GROUP_MASK_7 indicates this point should be sent back in 
   *   response to a GI when the QOI is 27
   *  Returning both of those bits set indicates this point should be included
   *   in responses to a GI with a QOI of either 20 or 27 
   *  Returning TMWDEFS_GROUP_MASK_PRIVATE_x indicates this point should be sent
   *   back in response to a GI when the QOI y in the private range is received. 
   *   This value should also be returned back from s14data_convertQOItoGroupMask() 
   *   for QOI y. For example QOI 64 could be mapped to TMWDEFS_GROUP_MASK_PRIVATE_1
   *   and QOI 100 could be mapped to TMWDEFS_GROUP_MASK_PRIVATE_2.
   *  Returning TMWDEFS_GROUP_MASK_CYCLIC indicates this point should be sent as 
   *   cyclic data
   *  Returning TMWDEFS_GROUP_MASK_BACKGROUND indicates this point should be sent as 
   *   the result of a background scan
   */
  TMWDEFS_GROUP_MASK TMWDEFS_CALLBACK s14data_pmenaGetGroupMask(
    void *pPoint);

  /* function: s14data_pmenaGetValue
   * purpose: Return the value for parameter of measured values
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   * returns:
   *  normalized value as defined in 7.2.6.6
   *   A normalized value is a 16-bit value with a range of
   *   -1 to (1 - (2^(-15)))
   */
  TMWTYPES_SHORT TMWDEFS_GLOBAL s14data_pmenaGetValue(
    void *pPoint);
  
  /* function: s14data_pmenaGetQualifier
   * purpose: Return the qualifier of parameter of measured values
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   * returns:
   *  qualifier of parameter of measured values as defined in 7.2.6.24
   *   Valid values are:
   *    I14DEF_QPM_KPA_THRESHOLD  - Threshold value
   *    I14DEF_QPM_KPA_SMOOTHING  - Smoothing factor (filter time constant)
   *    I14DEF_QPM_KPA_LOW_LIMIT  - Low limit for transmission of measured
   *                                values
   *    I14DEF_QPM_KPA_HIGH_LIMIT - High limit for transmission of measured
   *                                 values
   */
  TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_pmenaGetQualifier(
    void *pPoint);
  
  /* function: s14data_pmenaStore
   * purpose: Attempt to store the parameter of measured values
   * arguments:
   *  pPoint - reference into database for this point
   *  pValue - pointer to normalized value to store as defined in 7.2.6.6
   *   A normalized value is a 16-bit value with a range of
   *   -1 to (1 - (2^(-15)))
   *    On return pValue should point to the value of the parameter in operation.
   *     If this was a successful store the value is already filled in. If this 
   *     function failed, the old unchanged value should be put in *pValue.
   *  pQualifier - pointer to qualifier of parameter of measured values 
   *   as defined in 7.2.6.24. Valid values are:
   *    I14DEF_QPM_KPA_THRESHOLD  - Threshold value
   *    I14DEF_QPM_KPA_SMOOTHING  - Smoothing factor (filter time constant)
   *    I14DEF_QPM_KPA_LOW_LIMIT  - Low limit for transmission of measured
   *                                values
   *    I14DEF_QPM_KPA_HIGH_LIMIT - High limit for transmission of measured
   *                                 values
   * returns:
   *  TMWDEFS_TRUE if this store was successful 
   *  TMWDEFS_FALSE otherwise 
   *    NOTE: if this returns TMWDEFS_FALSE *pValue should set to the current 
   *     value.
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_pmenaStore(
    void *pPoint, 
    TMWTYPES_SHORT *pValue, 
    TMWTYPES_UCHAR *pQualifier);

  /* Parameter of Measured Value, Scaled Value */ 
  
  /* function: s14data_pmenbGetPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  index - which point to return, 0 to n-1, where n is the number of pmenb points
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_pmenbGetPoint(
    void *pHandle, 
    TMWTYPES_USHORT index);

  /* function: s14data_pmenbLookupPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  ioa - Information Object Address of point to return
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_GLOBAL s14data_pmenbLookupPoint(
    void *pHandle, 
    TMWTYPES_ULONG ioa);
 
  /* function: s14data_pmenbGetInfoObjAddr
   * purpose: Return the Information Object Address for this point
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Information Object Address
   */
  TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_pmenbGetInfoObjAddr(
    void *pPoint);

  /* function: s14data_pmenbGetGroupMask
   * purpose: Return the group mask indicating what groups this point 
   *  belongs to
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Group Mask indicating what groups this point belongs to.
   *  Returning TMWDEFS_GROUP_MASK_GENERAL indicates this point should be
   *   sent back in response to a GI when the QOI defined in 7.2.6.22 is 20
   *  Returning TMWDEFS_GROUP_MASK_7 indicates this point should be sent back in 
   *   response to a GI when the QOI is 27
   *  Returning both of those bits set indicates this point should be included
   *   in responses to a GI with a QOI of either 20 or 27 
   *  Returning TMWDEFS_GROUP_MASK_PRIVATE_x indicates this point should be sent
   *   back in response to a GI when the QOI y in the private range is received. 
   *   This value should also be returned back from s14data_convertQOItoGroupMask() 
   *   for QOI y. For example QOI 64 could be mapped to TMWDEFS_GROUP_MASK_PRIVATE_1
   *   and QOI 100 could be mapped to TMWDEFS_GROUP_MASK_PRIVATE_2.
   *  Returning TMWDEFS_GROUP_MASK_CYCLIC indicates this point should be sent as 
   *   cyclic data
   *  Returning TMWDEFS_GROUP_MASK_BACKGROUND indicates this point should be sent as 
   *   the result of a background scan
   */
  TMWDEFS_GROUP_MASK TMWDEFS_CALLBACK s14data_pmenbGetGroupMask(
    void *pPoint);

  /* function: s14data_pmenbGetValue
   * purpose: Return the value for parameter of measured values
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   * returns:
   *  scaled value as defined in 7.2.6.7
   *   A scaled value is a 16-bit value with a range of
   *   -(2^15) to ((2^15) - 1)
   */
  TMWTYPES_SHORT TMWDEFS_GLOBAL s14data_pmenbGetValue(
    void *pPoint);
 
  /* function: s14data_pmenbGetQualifier
   * purpose: Return the qualifier of parameter of measured values
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   * returns:
   *  qualifier of parameter of measured values as defined in 7.2.6.24
   *   Valid values are:
   *    I14DEF_QPM_KPA_UNUSED     - Not used
   *    I14DEF_QPM_KPA_THRESHOLD  - Threshold value
   *    I14DEF_QPM_KPA_SMOOTHING  - Smoothing factor (filter time constant)
   *    I14DEF_QPM_KPA_LOW_LIMIT  - Low limit for transmission of measured
   *                                values
   *    I14DEF_QPM_KPA_HIGH_LIMIT - High limit for transmission of measured
   *                                 values
   *    I14DEF_QPM_LPC            - Local parameter change
   *    I14DEF_QPM_POP            - Parameter not in operation
   */
  TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_pmenbGetQualifier(
    void *pPoint);
  
  /* function: s14data_pmenbStore
   * purpose: Attempt to store the parameter of measured values
   * arguments:
   *  pPoint - reference into database for this point
   *  pValue - pointer to scaled value to store as defined in 7.2.7  
   *   On return pValue should point to the value of the parameter in operation.
   *     If this was a successful store the value is already filled in. If this 
   *     function failed, the old unchanged value should be put in *pValue.
   *  pQualifier - pointer to qualifier of parameter of measured values 
   *  as defined in 7.2.6.24 Valid values are:
   *    I14DEF_QPM_KPA_UNUSED     - Not used
   *    I14DEF_QPM_KPA_THRESHOLD  - Threshold value
   *    I14DEF_QPM_KPA_SMOOTHING  - Smoothing factor (filter time constant)
   *    I14DEF_QPM_KPA_LOW_LIMIT  - Low limit for transmission of measured
   *                                values
   *    I14DEF_QPM_KPA_HIGH_LIMIT - High limit for transmission of measured
   *                                 values
   *    I14DEF_QPM_LPC            - Local parameter change
   *    I14DEF_QPM_POP            - Parameter not in operation
   * returns:
   *  TMWDEFS_TRUE if this store was successful 
   *  TMWDEFS_FALSE otherwise
   *    NOTE: if this returns TMWDEFS_FALSE *pValue should set to the current 
   *     value.
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_pmenbStore(
    void *pPoint, 
    TMWTYPES_SHORT *pValue, 
    TMWTYPES_UCHAR *pQualifier);

  /* Parameter of Measured Value, Short Floating Point Value */
  
  /* function: s14data_pmencGetPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  index - which point to return, 0 to n-1, where n is the number of pmenc points
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_pmencGetPoint(
    void *pHandle, 
    TMWTYPES_USHORT index);

  /* function: s14data_pmencLookupPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  ioa - Information Object Address of point to return
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_GLOBAL s14data_pmencLookupPoint(
    void *pHandle, 
    TMWTYPES_ULONG ioa);
 
  /* function: s14data_pmencGetInfoObjAddr
   * purpose: Return the Information Object Address for this point
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Information Object Address
   */
  TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_pmencGetInfoObjAddr(
    void *pPoint);
 
  /* function: s14data_pmencGetGroupMask
   * purpose: Return the group mask indicating what groups this point 
   *  belongs to
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Group Mask indicating what groups this point belongs to.
   *  Returning TMWDEFS_GROUP_MASK_GENERAL indicates this point should be
   *   sent back in response to a GI when the QOI defined in 7.2.6.22 is 20
   *  Returning TMWDEFS_GROUP_MASK_7 indicates this point should be sent back in 
   *   response to a GI when the QOI is 27
   *  Returning both of those bits set indicates this point should be included
   *   in responses to a GI with a QOI of either 20 or 27 
   *  Returning TMWDEFS_GROUP_MASK_PRIVATE_x indicates this point should be sent
   *   back in response to a GI when the QOI y in the private range is received. 
   *   This value should also be returned back from s14data_convertQOItoGroupMask() 
   *   for QOI y. For example QOI 64 could be mapped to TMWDEFS_GROUP_MASK_PRIVATE_1
   *   and QOI 100 could be mapped to TMWDEFS_GROUP_MASK_PRIVATE_2.
   *  Returning TMWDEFS_GROUP_MASK_CYCLIC indicates this point should be sent as 
   *   cyclic data
   *  Returning TMWDEFS_GROUP_MASK_BACKGROUND indicates this point should be sent as 
   *   the result of a background scan
   */
  TMWDEFS_GROUP_MASK TMWDEFS_CALLBACK s14data_pmencGetGroupMask(
    void *pPoint);

#if S14DATA_SUPPORT_PMENC
  /* function: s14data_pmencGetValue
   * purpose: Return the value for parameter of measured values
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   * returns:
   *  short floating point number as defined in 7.2.6.8
   *   A float is defined according to IEEE STD 754
   */
  TMWTYPES_SFLOAT TMWDEFS_GLOBAL s14data_pmencGetValue(
    void *pPoint);

  /* function: s14data_pmencGetQualifier
   * purpose: Return the qualifier of parameter of measured values
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   * returns:
   *  qualifier of parameter of measured values as defined in 7.2.6.24
   *   Valid values are:
   *    I14DEF_QPM_KPA_UNUSED     - Not used
   *    I14DEF_QPM_KPA_THRESHOLD  - Threshold value
   *    I14DEF_QPM_KPA_SMOOTHING  - Smoothing factor (filter time constant)
   *    I14DEF_QPM_KPA_LOW_LIMIT  - Low limit for transmission of measured
   *                                values
   *    I14DEF_QPM_KPA_HIGH_LIMIT - High limit for transmission of measured
   *                                 values
   *    I14DEF_QPM_LPC            - Local parameter change
   *    I14DEF_QPM_POP            - Parameter not in operation
   */
  TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_pmencGetQualifier(
    void *pPoint);
 
  /* function: s14data_pmencStore
   * purpose: Attempt to store the parameter of measured values
   * arguments:
   *  pPoint - reference into database for this point
   *  pValue - pointer to short floating point number to store as defined in 7.2.8
   *   A float is defined according to IEEE STD 754
   *   On return pValue should point to the value of the parameter in operation.
   *     If this was a successful store the value is already filled in. If this 
   *     function failed, the old unchanged value should be put in *pValue.
   *  pQualifier - pointer to qualifier of parameter of measured values 
   *   as defined in 7.2.6.24 Valid values are:
   *    I14DEF_QPM_KPA_UNUSED     - Not used
   *    I14DEF_QPM_KPA_THRESHOLD  - Threshold value
   *    I14DEF_QPM_KPA_SMOOTHING  - Smoothing factor (filter time constant)
   *    I14DEF_QPM_KPA_LOW_LIMIT  - Low limit for transmission of measured
   *                                values
   *    I14DEF_QPM_KPA_HIGH_LIMIT - High limit for transmission of measured
   *                                 values
   *    I14DEF_QPM_LPC            - Local parameter change
   *    I14DEF_QPM_POP            - Parameter not in operation
   * returns:
   *  TMWDEFS_TRUE if this store was successful 
   *  TMWDEFS_FALSE otherwise
   *    NOTE: if this returns TMWDEFS_FALSE *pValue should set to the current 
   *     value.
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_pmencStore(
    void *pPoint, 
    TMWTYPES_SFLOAT *pValue, 
    TMWTYPES_UCHAR *pQualifier);
#endif

  /* Parameter Activation */
 
  /* function: s14data_pacnaLookupPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  ioa - Information Object Address of point to return
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_GLOBAL s14data_pacnaLookupPoint(
    void *pHandle, 
    TMWTYPES_ULONG ioa);

  /* function: s14data_pacnaStore
   * purpose: Attempt to perform the requested parameter activation
   *  (or deactivation)
   * arguments:
   *  pPoint - reference into database for this point
   *  cot - cause of transmission as defined in 7.2.3
   *    See I14DEF_COT_xxx definitions in i14def.h for allowable
   *    values
   *  qpa - qualifier of parameter activation as defined in 7.2.6.25
   *   Valid values are:
   *    I14DEF_QPA_LOADED_PARAMS    - Act/deact of the previously loaded
   *                                    parameters (NOT USED)
   *    I14DEF_QPA_ADDRESSED_PARAMS - Act/deact of the parameter of the
   *                                    addressed object (NOT USED)
   *    I14DEF_QPA_CYCLIC_PARAMS    - Act/deact of persistent cyclic or
   *                                    periodic transmission of the
   *                                    addressed object
   *   Activate/Deactivate is defined by the Cause of Transmission (COT)
   * returns:
   *  TMWDEFS_TRUE if this activation/deactivation was successful 
   *  TMWDEFS_FALSE otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_pacnaStore(
    void *pPoint, 
    TMWTYPES_UCHAR cot, 
    TMWTYPES_UCHAR qpa);

  /* Reset Process */ 

  /* function: s14data_crpnaSupport
   * purpose: Determine whether reset process of type qrp 
   *  is supported for this sector
   * arguments:
   *  pSector - Pointer to TMW sector structure
   *  qrp - Qualifier of reset process command as defined in 7.2.6.27
   *   Valid values are:
   *    I14DEF_QRP_RESET_GENERAL - General reset of process
   *    I14DEF_QRP_RESET_EVENTS  - Reset of pending information with time tag
   *                                of the event buffer
   *   I14DEF_QRP_RESET_EVENTS will be handled internally by the SCL.
   *   Other values will not be handled internally by the SCL and will be
   *    passed to the user for processing.
   * returns:
   *  TMWDEFS_TRUE if a reset is supported
   *  TMWDEFS_FALSE otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_crpnaSupport(
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR qrp);

  /* function: s14data_crpnaExecute
   * purpose: Reset process according to qrp
   * arguments:
   *  pSector - Pointer to TMW sector structure
   *  qrp - Qualifier of reset process command as defined in 7.2.6.27
   *   Valid values are:
   *    I14DEF_QRP_RESET_GENERAL - General reset of process
   *    I14DEF_QRP_RESET_EVENTS  - Reset of pending information with time tag
   *                                of the event buffer
   *   I14DEF_QRP_RESET_EVENTS will be handled internally by the SCL. It will
   *    also be passed through this function to notify the user that it
   *    occured in case additional action is desired by the user.
   *   I14DEF_QRP_RESET_GENERAL and other reserved values are passed through
   *    this function and need to be handled as appropriate
   * returns:
   *  void 
   * NOTE: This function cannot reset the process by closing and reopening the
   *  sector. This function may need to set an event and process the reset after 
   *  returning from this function. 
   */
  void TMWDEFS_GLOBAL s14data_crpnaExecute(
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR qrp);

  /* File Transfer */

  /* function: s14data_fileCallFileResp
   * purpose: Get value for Select and Call Qualifier to put in
   *  FSCNA to be sent back to master in response to FFRNA
   * arguments:
   *  pHandle - pointer to handle returned from s14data_init  
   *  ioa - Information Object Address for requested file.
   *  fileName - name of file
   *  fileLength - length of file
   *  frq - file ready qualifier received in FFRNA
   *  pSfq - pointer to Select and Call Qualifier to be filled in. Caller will
   *    initialize to Call/Request File.
   *    As defined in 7.2.6.30. The following values (or OR'd combinations of I14DEF_SCQ_ and
   *    I14DEF_FILE_XFER_ERR_ fields) are valid for this type:
   *      I14DEF_SCQ_SELECT_FILE        - Select file
   *      I14DEF_SCQ_CALL_FILE          - Request file
   *      I14DEF_SCQ_DEACT_FILE         - Deactivate file
   *      I14DEF_SCQ_DELETE_FILE        - Delete file
   *      I14DEF_SCQ_SELECT_SECTION     - Select section
   *      I14DEF_SCQ_CALL_SECTION       - Request section 
   *      I14DEF_SCQ_DEACT_SECTION      - Deactivate section
   *      I14DEF_FILE_XFER_ERR_MASK     - Mask for File Transfer Error
   *      I14DEF_FILE_XFER_ERR_NO_MEM   - Requested memory space not available
   *      I14DEF_FILE_XFER_ERR_BAD_CHS  - Checksum failed
   *      I14DEF_FILE_XFER_ERR_NO_COMM  - Unexpected communication service
   *      I14DEF_FILE_XFER_ERR_BAD_FILE - Unexpected name of file
   *      I14DEF_FILE_XFER_ERR_BAD_SECT - Unexpected name of section
   * returns:
   *  TMWDEFS_TRUE  if *pScq is filled in or to be left Call/Request File
   *  TMWDEFS_FALSE if otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_fileCallFileResp(
    void *pHandle, 
    TMWTYPES_ULONG  ioa,
    TMWTYPES_USHORT fileName,
    TMWTYPES_ULONG  fileLength,
    TMWTYPES_UCHAR  frq,
    TMWTYPES_UCHAR *pScq);

  /* function: s14data_fileCallSectionResp
   * purpose: Get value for Select and Call Qualifier to put in
   *  FSCNA to be sent back to master in response to FSRNA
   * arguments:
   *  pHandle - pointer to handle returned from s14data_init  
   *  ioa - Information Object Address for requested file.
   *  fileName - name of file
   *  sectionName - name of section
   *  sectionLength - length of section
   *  srq - section ready qualifier received in FSRNA 
   *  pSfq - pointer to Select and Call Qualifier to be filled in. Caller will
   *    initialize to Call Section.
   *    As defined in 7.2.6.30. The following values (or OR'd combinations of I14DEF_SCQ_ and
   *    I14DEF_FILE_XFER_ERR_ fields) are valid for this type:
   *      I14DEF_SCQ_SELECT_FILE        - Select file
   *      I14DEF_SCQ_CALL_FILE          - Request file
   *      I14DEF_SCQ_DEACT_FILE         - Deactivate file
   *      I14DEF_SCQ_DELETE_FILE        - Delete file
   *      I14DEF_SCQ_SELECT_SECTION     - Select section
   *      I14DEF_SCQ_CALL_SECTION       - Request section 
   *      I14DEF_SCQ_DEACT_SECTION      - Deactivate section
   *      I14DEF_FILE_XFER_ERR_MASK     - Mask for File Transfer Error
   *      I14DEF_FILE_XFER_ERR_NO_MEM   - Requested memory space not available
   *      I14DEF_FILE_XFER_ERR_BAD_CHS  - Checksum failed
   *      I14DEF_FILE_XFER_ERR_NO_COMM  - Unexpected communication service
   *      I14DEF_FILE_XFER_ERR_BAD_FILE - Unexpected name of file
   *      I14DEF_FILE_XFER_ERR_BAD_SECT - Unexpected name of section
   * returns:
   *  TMWDEFS_TRUE  if *pScq is filled in or to be left Call Section
   *  TMWDEFS_FALSE otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_fileCallSectionResp(
    void *pHandle, 
    TMWTYPES_ULONG  ioa,
    TMWTYPES_USHORT fileName,
    TMWTYPES_UCHAR  sectionName,
    TMWTYPES_ULONG  sectionLength,
    TMWTYPES_UCHAR  srq, 
    TMWTYPES_UCHAR *pScq);
 
  /* function: s14data_fileAckLastSectOrSegResp
   * purpose: Get ACK file or section qualifier to be put in FAFNA APDU
   *  to be sent back to master in response to FLSNA.
   * arguments:
   *  pHandle - pointer to handle returned from s14data_init  
   *  ioa - Information Object Address for requested file.
   *  fileName - name of file
   *  sectionName - name of section
   *  lsq - Last section or segment qualifier received in FLSNA
   *  checksumGood - indicates whether checksum received FLSNA matches
   *   checksum calculated on received bytes.
   *  pAfq - pointer to Ack Section Qualifier to be filled in. Caller will
   *    initialize to Positive Ack of section or file transfer. 
   *    As defined in 7.2.6.32. The following values are valid for this type:
   *     I14DEF_AFQ_FILE_XFER_POS_ACT - Positive acknowledge of file transfer
   *     I14DEF_AFQ_FILE_XFER_NEG_ACT - Negative acknowledge of file transfer
   *     I14DEF_AFQ_SECT_XFER_POS_ACT - Positive acknowledge of section
   *                                     transfer
   *     I14DEF_AFQ_SECT_XFER_NEG_ACT - Negative acknowledge of section
   *                                     transfer 
   * returns:
   *  TMWDEFS_TRUE  if *pAfq is filled in or to be left to Positive Ack
   *  TMWDEFS_FALSE otherwise 
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_fileAckLastSectOrSegResp(
    void *pHandle, 
    TMWTYPES_ULONG  ioa,
    TMWTYPES_USHORT fileName,
    TMWTYPES_UCHAR  sectionName,
    TMWTYPES_UCHAR  lsq,
    TMWTYPES_BOOL   checksumGood,
    TMWTYPES_UCHAR  *pAfq);

  /* function: s14data_fileGetLastSegmentResp
   * purpose: Get Last Segment Qualifier and Checksum to be put in FLSNA APDU
   *  to be sent in response to FAFNA received from master.
   * arguments:
   *  pHandle - pointer to handle returned from s14data_init  
   *  ioa - Information Object Address for requested file.
   *  fileName - name of file
   *  sectionName - name of section
   *  pLsq - pointer to Last segment qualifier to be filled in. Caller will 
   *    initialize to Section Transfer with No Deactivate.
   * returns
   *  TMWDEFS_TRUE  if parameters are filled in
   *  TMWDEFS_FALSE otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_fileGetLastSegmentResp(
    void *pHandle, 
    TMWTYPES_ULONG  ioa,
    TMWTYPES_USHORT fileName,
    TMWTYPES_UCHAR  sectionName,
    TMWTYPES_UCHAR  *pLsq);

  /* function: s14data_fileGetLastSectionResp
   * purpose: Get Last Section Qualifier and Checksum to be put in FLSNA APDU
   *  to be sent in response to FAFNA received from master.
   * arguments:
   *  pHandle - pointer to handle returned from s14data_init  
   *  ioa - Information Object Address for requested file.
   *  fileName - name of file
   *  pLsq - pointer to Last segment qualifier to be filled in. Caller will 
   *    initialize to File Transfer with No Deactivate.
   * returns
   *  TMWDEFS_TRUE  if parameters are filled in
   *  TMWDEFS_FALSE otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_fileGetLastSectionResp(
    void *pHandle, 
    TMWTYPES_ULONG  ioa,
    TMWTYPES_USHORT fileName,
    TMWTYPES_UCHAR  *pLsq);

  /* function: s14data_fileAckFile
   * purpose:  ACK File PDU (FAFNA with afq == FILE POSITIVE ACT)
   *  was received. Slave database can now delete the file and remove the 
   *  directory entry. It may also cause an event to spontaneously
   *  send the updated directory if it chooses.
   * arguments:
   *  pHandle - pointer to handle returned from s14data_init  
   *  ioa - Information Object Address for requested file.
   *  fileName - name of file
   * returns
   *  TMWDEFS_TRUE  if successful
   *  TMWDEFS_FALSE otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_fileAckFile(
    void *pHandle, 
    TMWTYPES_ULONG  ioa,
    TMWTYPES_USHORT fileName);

  /* function: s14data_fileGetSectionReady
   * purpose: Get section length and Section Ready Qualifier for the 
   *  designated file and section to be put in FSRNA to be sent back to
   *  master in response to FAFNA
   *  or in response to FSCNA with SCQ Deactivate Section.
   *  or in response to FSCNA with SCQ Call/Request file
   * arguments:
   *  pHandle - pointer to handle returned from s14data_init  
   *  ioa - Information Object Address for requested file.
   *  fileName - name of file
   *  sectionName - name of section
   *  pSectionLength - pointer to length of section to be filled in
   *  pSrq - pointer to Section ready qualifier to be filled in. Caller will
   *    initialize to Section Ready.
   * returns
   *  TMWDEFS_TRUE  if section length and *pSrq are filled in
   *  TMWDEFS_FALSE otherwise 
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_fileGetSectionReady(
    void *pHandle, 
    TMWTYPES_ULONG  ioa,
    TMWTYPES_USHORT fileName,
    TMWTYPES_UCHAR  sectionName,
    TMWTYPES_ULONG *pSectionLength,
    TMWTYPES_UCHAR *pSrq);
  
  /* function: s14data_fileGetDirectoryEntry 
   * purpose:  Retrieve directory entry information for the specified entry
   *  to be put in FDRTA ASDU to be sent to master. 
   * arguments:
   *  pHandle - pointer to handle returned from s14data_init 
   *  index - index of entry to ge gotten
   *  pIoa - pointer to Information Object Address to be filled in
   *  pFileName - pointer to name of file to be filled in
   *  pFileLength - pointer to length of file to be filled in
   *  pFileTime - pointer to time structure to be filled in
   *  pSof - pointer to Status of File to be filled in
   *    as defined in 7.2.6.38. Valid values are:
   *     I14DEF_SOF_LFD_MORE_FILES - Additional file of the directory follows
   *     I14DEF_SOF_LFD_LAST_FILE  - Last file of the directory
   *     I14DEF_SOF_FOR_MASK       - Mask for FOR (name) field
   *     I14DEF_SOF_FOR_IS_FILE    - Name defines file
   *     I14DEF_SOF_FOR_IS_DIR     - Name defines subdirectory 
   *    I14DEF_SOF_LFD_LAST_FILE indicates whether there are additional files.
   *    This bit must be set in the last entry to indicate that there are no
   *     more files.
   * returns:
   *  TMWDEFS_TRUE  if entry number index was found and parameters filled in.
   *  TMWDEFS_FALSE otherwise 
     */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_fileGetDirectoryEntry(
    void *pHandle, 
    TMWTYPES_UCHAR   index,
    TMWTYPES_ULONG  *pIoa,
    TMWTYPES_USHORT *pFileName,
    TMWTYPES_ULONG  *pFileLength,
    TMWDTIME       *pFileTime,
    TMWTYPES_UCHAR  *pSof);

  /* function: s14data_fileSelectFile
   * purpose: Get length and File Ready Qualifier of requested file 
   *  to be sent to the master in a FFRNA APDU in response to the
   *  FSCNA ADPU received with SCQ of select file
   * arguments:
   *  pHandle - pointer to handle returned from s14data_init  
   *  ioa - Information Object Address for requested file.
   *  fileName - Name of requested file
   *  pFileLength - pointer to File Length to be filled in
   *  pFrq - pointer to File Ready Qualifier to be filled in. Caller will
   *    initialize to Positive Confirm.
   * returns:
   *  TMWDEFS_TRUE  if parameters were filled in
   *  TMWDEFS_FALSE otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_fileSelectFile(
    void *pHandle, 
    TMWTYPES_ULONG   ioa,
    TMWTYPES_USHORT  fileName,
    TMWTYPES_ULONG  *pFileLength,
    TMWTYPES_UCHAR  *pFrq);

#if S14DATA_SUPPORT_FSCNB
  /* function: s14data_fileQueryLog
   * purpose: Get length and File Ready Qualifier of requested file 
   *  to be sent to the master in a FFRNA APDU in response to the
   *  FSCNB ADPU received. (Query Log added in IEC60870-5-104 Ed. 2)   
   *  FSCNB is very similar to FSCNA Select File, but specifies a 
   *  start and stop time for log entries to be retrieved.
   * arguments:
   *  pHandle - pointer to handle returned from s14data_init  
   *  ioa - Information Object Address for requested file.
   *  fileName - Name of requested file
   *  pRangeStartTime - pointer to start time of range of log records to be returned 
   *  pRangeStopTime - Pointer to stop time of range of log records to be returned
   *  pFileLength - pointer to File Length to be filled in
   *  pFrq - pointer to File Ready Qualifier to be filled in. Caller will
   *    initialize to Positive Confirm.
   * returns:
   *  TMWDEFS_TRUE  if parameters were filled in
   *  TMWDEFS_FALSE otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_fileQueryLog(
    void *pHandle, 
    TMWTYPES_ULONG  ioa,
    TMWTYPES_USHORT fileName,
    TMWDTIME *pRangeStartTime,
    TMWDTIME *pRangeStopTime,
    TMWTYPES_ULONG  *pFileLength,
    TMWTYPES_UCHAR  *pFrq); 
#endif

  /* function: s14data_fileSegmentStore
   * purpose: Store the segment data that was received from master in a
   *  FSGNA APDU to the file and section indicated
   * arguments:
   *  pHandle - pointer to handle returned from s14data_init  
   *  ioa - Information Object Address for requested file.
   *  fileName - name of file
   *  sectionName - name of section 
   *  segmentLength - length of data in segment to be stored
   *  pSegmentData - pointer to segment data to be stored
   * returns:
   *  TMWDEFS_TRUE  if successful
   *  TMWDEFS_FALSE if not successful
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_fileSegmentStore(
    void *pHandle, 
    TMWTYPES_ULONG  ioa,
    TMWTYPES_USHORT fileName,
    TMWTYPES_UCHAR  sectionName,
    TMWTYPES_ULONG  segmentLength,
    TMWTYPES_UCHAR *pSegmentData);

  /* function: s14data_getSegment
   * purpose: Get segment length and segment data bytes to be sent for this
   *  file and section in FSGNA APDU. 
   * arguments:
   *  pHandle - pointer to handle returned from s14data_init
   *  ioa - Information Object Address for requested file.
   *  fileName - name of requested file
   *  sectionName - name of requested section
   *  maxLength - maximum number of segment data bytes that can be copied in
   *  pSegmentLength - pointer to segment length to be filled in with 
   *   number of segment data bytes copied into pSegmentData. This should
   *   return 0 when all bytes have been read from specified section.
   *  pSegmentData - pointer to segment data to be filled in
   * returns:
   *  TMWDEFS_TRUE  if parameters were filled in 
   *    NOTE: this should be returned with *pSegmentLength set to 0 when all
   *          bytes have been read from specified section.
   *  TMWDEFS_FALSE if file and section could not be found
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_fileGetSegment(
    void *pHandle, 
    TMWTYPES_ULONG  ioa,
    TMWTYPES_USHORT fileName,
    TMWTYPES_UCHAR  sectionName,
    TMWTYPES_UCHAR  maxLength,
    TMWTYPES_UCHAR *pSegmentLength,
    TMWTYPES_UCHAR *pSegmentData);

  /* function: s14data_fileDeleteResp
   * purpose: Delete file specified and get response to send back to master
   * arguments:
   *  pHandle - pointer to handle returned from s14data_init
   *  ioa - Information Object Address of file to be deleted
   *  fileName - name of file to be deleted
   *  pFrq -  pointer to File Ready Qualifier to be filled in. Caller will
   *    initialize to Positive Confirm. If this function  
   *    returns TMWDEFS_FALSE *pFrq will be set to Negative Confirm.
   * returns:
   *  TMWDEFS_TRUE  if *pFrq was filled in
   *  TMWDEFS_FALSE otherwise 
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_fileDeleteResp(
    void *pHandle, 
    TMWTYPES_ULONG  ioa,
    TMWTYPES_USHORT fileName,
    TMWTYPES_UCHAR *pFrq);

  /* function: s14data_fileDirectoryChanged
   * purpose: Determine whether the file directory for file transfer has changed
   *  It will be called if S101SCTR_CONFIG or S104SCTR_CONFIG 
   *  rbeScanPeriod is nonzero and fdrtaScanEnabled == TMWDEFS_TRUE
   * arguments:
   *  pHandle - pointer to handle returned from s14data_init
   * returns:
   *  TMWDEFS_TRUE  if file directory changed
   *  TMWDEFS_FALSE otherwise 
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_fileDirectoryChanged(
    void *pHandle);
 

  /* The following functions are for diagnostic purposes only and 
   * only need to be implemented if the diagnostic show database functionality 
   * and file transfer are supported 
   */

  /* function: s14data_fileGetFile
   *  NOTE: only necessary for diagnostic show database functionality
   * purpose: Get a handle for a file from the slave database, using index to specify
   *  which file.
   * arguments:
   *  pHandle - pointer to handle returned from s14data_init
   *  index - the index of the file in the database
   * returns:
   *  pointer to file handle to be passed to s14data_fileDiagShow()
   */
  void * TMWDEFS_GLOBAL s14data_fileGetFile(
    void *pHandle, 
    TMWTYPES_USHORT index);

  /* function: s14data_fileDiagShow
   *  NOTE: only necessary for diagnostic show database functionality
   * purpose: Show summary information about this file 
   * arguments:
   *  pHandle - pointer to handle returned from s14data_init
   * returns:
   *  void 
   */
  void TMWDEFS_GLOBAL s14data_fileDiagShow(
    TMWSCTR *pSector,
    void *pFile);


 /* THE FOLLOWING SECTIONS CSENZ, MCTNA, CCTNA and s14data_CICNAResponseReady()
  * ARE ONLY NECESSARY IF GASUNIE PRIVATE ASDU SUPPORT IS REQUIRED 
  */
#if S14DATA_SUPPORT_CSE_Z
  /* Set Integrated Totals BCD Commands    
   *
   * Command Processing
   * Details are the same as the Normalized Measurand Commands (CSENA)
   * description above.
   *
   */

  /* function: s14data_csenzGetPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  index - which point to return, 0 to n-1, where n is the number of csenz points
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_CALLBACK s14data_csenzGetPoint(
    void *pHandle, 
    TMWTYPES_USHORT index);

  /* function: s14data_csenzLookupPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access to this point.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  ioa - Information Object Address of point to return
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_GLOBAL s14data_csenzLookupPoint(
    void *pHandle, 
    TMWTYPES_ULONG ioa);
 
  /* function: s14data_csenzGetInfoObjAddr
   * purpose: Return the Information Object Address for this point
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  Information Object Address
   */
  TMWTYPES_ULONG TMWDEFS_CALLBACK s14data_csenzGetInfoObjAddr(
    void *pPoint);

  /* function: s14data_csenzSelectRequired
   * purpose: Determine whether a select is required before 
   *  execute for this point
   *  Normally this would return false and allow the master to 
   *  decide whether to issue the select before execute. If this
   *  returns true, then this slave requires a select before execute
   *  for this particular point.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  TMWDEFS_TRUE if a select is required
   *  TMWDEFS_FALSE if not required
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_csenzSelectRequired(
    void *pPoint);

  /* function: s14data_csenzSelect
   * purpose:Issue a select for this point. See above for description of 
   *  Floating Point Command processing.
   * arguments:
   *  pPoint - reference into database for this point
   *  cot - cause of transmission as defined in 7.2.3
   *   I14DEF_COT_ACTIVATION   
   *  pBCD - pointer to 6 byte Binary Coded Decimal value. Least significant byte
   *   is in pBCD[0]. Most significant byte is in pBCD[5]. Should be padded with 
   *   0 values. Value 123 decimal would be 0x23,0x01,0x00,0x00,0x00,0x00
   *  qos - quality of set-point command as defined in 7.2.6.39
   *    The following values (or OR'd combinations of I14DEF_QOS_QL and
   *    I14DEF_QOS_SE fields) are valid for this type:
   *      I14DEF_QOS_QL_USE_DEFAULT - Default 
   *      I14DEF_QOS_SE_SELECT      - Select
   * returns:
   *  status of command 
   *   TMWDEFS_CMD_STAT_SELECTING
   *   TMWDEFS_CMD_STAT_FAILED
   *   TMWDEFS_CMD_STAT_SUCCESS
   */
  TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_csenzSelect(
    void *pPoint, 
    TMWTYPES_UCHAR cot, 
    TMWTYPES_UCHAR *pBCD, 
    TMWTYPES_UCHAR qos);
 
  /* function: s14data_csenzExecute
   * purpose:Issue an execute for this point. See above for description of 
   *  Floating Point Command processing.
   * arguments:
   *  pPoint - reference into database for this point
   *  cot - cause of transmission as defined in 7.2.3 
   *   always I14DEF_COT_ACTIVATION   - execute command
   *  pBCD - pointer to 6 byte Binary Coded Decimal value. Least significant byte
   *   is in pBCD[0]. Most significant byte is in pBCD[5]. Should be padded with 
   *   0 values. Value 123 decimal would be 0x23,0x01,0x00,0x00,0x00,0x00
   *  qos - quality of set-point command as defined in 7.2.6.39
   *    The following values (or OR'd combinations of I14DEF_QOS_QL and
   *    I14DEF_QOS_SE fields) are valid for this type:
   *      I14DEF_QOS_QL_USE_DEFAULT - Default 
   *      I14DEF_QOS_SE_EXECUTE     - Execute
   * returns:
   *  status of command 
   *   TMWDEFS_CMD_STAT_EXECUTING
   *   TMWDEFS_CMD_STAT_FAILED
   *   TMWDEFS_CMD_STAT_SUCCESS
   */
  TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_csenzExecute(
    void *pPoint, 
    TMWTYPES_UCHAR cot, 
    TMWTYPES_UCHAR *pBCD, 
    TMWTYPES_UCHAR qos);

  /* function: s14data_csenzStatus
   * purpose:Get current status for this point. See above for description of 
   *  Floating Point Command processing.
   * arguments:
   *  pPoint - reference into database for this point
   * returns:
   *  status of command 
   *   TMWDEFS_CMD_STAT_SELECTING
   *   TMWDEFS_CMD_STAT_EXECUTING
   *   TMWDEFS_CMD_STAT_FAILED
   *   TMWDEFS_CMD_STAT_SUCCESS
   */
  TMWDEFS_COMMAND_STATUS TMWDEFS_GLOBAL s14data_csenzStatus(
    void *pPoint);
#endif

#if S14DATA_SUPPORT_MCTNA
  /* function: s14data_mctnaLookupPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access this configuration table.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  ioa - Information Object Address of point to return
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_GLOBAL s14data_mctnaLookupPoint(
    void *pHandle, 
    TMWTYPES_ULONG ioa);

  /* function: s14data_mctnaGetValues
   * purpose: Return the values for this configuration table
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   *  maxQuantity - maximum number of cti values to be read
   *  pCtiValues - pointer to location to copy up to maxQuantity 1 byte values
   * returns:
   *  number of cti values copied into pCtiValues
   */
  TMWTYPES_UCHAR TMWDEFS_GLOBAL s14data_mctnaGetValues(
    void *pPoint,
    TMWTYPES_UCHAR maxQuantity,
    TMWTYPES_UCHAR *pCtiValues);
#endif

#if S14DATA_SUPPORT_CCTNA
  /* function: s14data_cctnaLookupPoint
   * purpose: Return a handle to a specific data point. This handle is
   *  used by the routines below to access this configuration table.
   * arguments:
   *  pHandle - handle to database returned from s14data_init
   *  ioa - Information Object Address of point to return
   * returns:
   *  void * - reference into database for this point or TMWDEFS_NULL
   */
  void * TMWDEFS_GLOBAL s14data_cctnaLookupPoint(
    void *pHandle, 
    TMWTYPES_ULONG ioa);

  /* function: s14data_cctnaSetValues
   * purpose: Set the values for this configuration table
   * arguments:
   *  pPoint - handle to data point returned from 'getPoint' function.
   *  pCtiValues - pointer to array of 1 byte CTI values
   *  quantity - number of cti values to be set
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL s14data_cctnaSetValues(
    void *pPoint,
    TMWTYPES_UCHAR *pCtiValues,
    TMWTYPES_UCHAR quantity);
#endif
  /* function: s14data_CICNAResponseReady
   *
   * This function is ONLY REQUIRED IF S14DATA_SUPPORT_RTU_CICNA is TMWDEFS_TRUE
   *
   * purpose: Determine if this response to the CICNA request should be sent for
   *  this sector.
   *  This provides support for the behavior required by Gasunie IEC-104 PID
   *   when a GI with broadcast Common ASDU Address is received.
   * 
   *   RTU sector contains no data points
   *   Logical Unit sectors contain data points.
   *
   *   When a GI with broadcast Common ASDU Address is received, 
   *    send I14DEF_COT_ACTCON from RTU first, 
   *    followed by I14DEF_COT_ACTCON from Logical Units, 
   *    followed by I14DEF_COT_INTG_GEN (or other data) from Logical Units,
   *    followed by I14DEF_COT_ACTTERM from Logical Units, 
   *    followed by I14DEF_COT_ACTTERM from RTU  
   *
   *   This behavior is not specified by 101 and 104 specs. This function
   *    will only be called if S14DATA_SUPPORT_RTU_CICNA is TMWDEFS_TRUE
   *
   *   SUGGESTED ALGORITHM:
   *   When this function is called with cot ACTCON for RTU sector, 
   *     this should ALWAYS return TMWDEFS_TRUE.
   *   When this function is called with cot ACTON for LU sector, 
   *     if this function has NOT yet returned TMWDEFS_TRUE for ACTCON on 
   *      RTU sector this should return TMWDEFS_FALSE. 
   *     if this function HAS already returned TMWDEFS_TRUE for ACTCON on 
   *      RTU sector this should return TMWDEFS_TRUE.
   *   When this function is called with cot ACTTERM for LU sector, 
   *     this should ALWAYS return TMWDEFS_TRUE.
   *   When this function is called with cot ACTTERM for RTU sector, 
   *     if this function has NOT yet returned TMWDEFS_TRUE for ACTTERM on 
   *      ALL LU sectors this should return TMWDEFS_FALSE.
   *     if this function HAS already returned TMWDEFS_TRUE for ACTTERM on 
   *      ALL LU sectors this should return TMWDEFS_TRUE.
   *   NOTE: to simplify the algorithm, this function will be called with 
   *    cot ACTTERM for all monitored data with COTs 20-36, since this will 
   *    follow the same rule as ACTTERM.
   *
   * arguments:
   *  pHandle - pointer to handle returned from s14data_init.
   *  cot - I14DEF_COT_ACTCON
   *     or I14DEF_COT_ACTTERM  
   * returns:
   *  TMWDEFS_TRUE if the response should be sent for this sector now
   *  TMWDEFS_FALSE otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14data_CICNAResponseReady(
    void *pHandle,
    TMWTYPES_UCHAR cot);

#ifdef __cplusplus
}
#endif
#endif /* S14DATA_DEFINED */
