/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/


/* file: s14fsrna.c
 * description: IEC 60870-5-101 slave FSRNA (File Transfer -Section Ready)
 *  functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s14fsrna.h"
#include "tmwscl/i870/s14file.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/i14diag.h"
#include "tmwscl/i870/s14util.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/utils/tmwtarg.h"

#if S14DATA_SUPPORT_FILE

/* function: s14fsrna_processRequest */
void TMWDEFS_CALLBACK s14fsrna_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  TMWTYPES_ULONG  ioa;
  TMWTYPES_USHORT fileName;
  TMWTYPES_UCHAR  sectionName;
  TMWTYPES_ULONG  sectionLength;
  TMWTYPES_UCHAR  srq;

  /* Get pointer to application specific sector info */
  S14SCTR *p14Sector = (S14SCTR *)pSector;

  /* Store originator address */
  p14Sector->fileOriginator = pMsg->origAddress;

  /* Diagnostics */
  I14DIAG_SHOW_STORE_TYPE(pSector, pMsg);
   
  if(pMsg->cot != I14DEF_COT_FILE_XFER)
  { 
    /* Build and send a Negative Activation Confirmation */
    s14sesn_sendNegActCon(pSector, pMsg, I14DEF_COT_UNKNOWN_COT);
    return;
  }
  
  /* Parse Information Object Address */
  i870util_readInfoObjAddr(pSector->pSession, pMsg, &ioa);
  
  /* Read name of file */
  tmwtarg_get16(&pMsg->pRxData->pMsgBuf[pMsg->offset], &fileName);
  pMsg->offset += 2;

  /* Read name of section */
  sectionName = pMsg->pRxData->pMsgBuf[pMsg->offset++];

  /* Read length of section */
  sectionLength = 0;
  tmwtarg_get24(&pMsg->pRxData->pMsgBuf[pMsg->offset], &sectionLength);
  pMsg->offset += 3;

  /* Read srq */
  srq = pMsg->pRxData->pMsgBuf[pMsg->offset++];
 
  /* Diagnostics */
  I14DIAG_SHOW_FSRNA(pSector, ioa, fileName, sectionName, sectionLength, srq);

  p14Sector->fileState         = S14FILE_RCV_SECTION_READY_STATE;
  p14Sector->fileIOA           = ioa;
  p14Sector->fileName          = fileName;
  p14Sector->fileSectionName   = sectionName;
  p14Sector->fileSectionLength = sectionLength;
  p14Sector->fileSRQ           = srq;
}

#endif /* S14DATA_SUPPORT_FILE */
