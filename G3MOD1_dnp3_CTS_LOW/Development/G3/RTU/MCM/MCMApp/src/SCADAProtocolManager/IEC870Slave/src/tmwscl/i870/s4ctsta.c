/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s4ctsta.c
 * description: IEC 60870-5-104 slave CTSTA (Test Command, with time)
 *  functionality.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s4ctsta.h"
#include "tmwscl/i870/s4util.h"
#include "tmwscl/i870/s14data.h"
#include "tmwscl/i870/s104sctr.h"
#include "tmwscl/i870/s14diag.h"
#include "tmwscl/i870/i14def.h"
#include "tmwscl/i870/i14diag.h"
#include "tmwscl/i870/i870chnl.h"
#include "tmwscl/utils/tmwtarg.h"

#if (S14DATA_SUPPORT_CTS && S14DATA_SUPPORT_TIMETAG_COMMANDS)

/* function: s4ctsta_processRequest */
void TMWDEFS_CALLBACK s4ctsta_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  S104SCTR *p104Sector = (S104SCTR *)pSector;

  /* Store originator address */
  p104Sector->ctstaOriginator = pMsg->origAddress;
 
  /* Parse Information Object Address */
  i870util_readInfoObjAddr(pSector->pSession, pMsg, &p104Sector->ctstaIOA);

  /* Parse sequence number */
  tmwtarg_get16(&pMsg->pRxData->pMsgBuf[pMsg->offset], &p104Sector->testSequenceNumber);
  pMsg->offset += 2;

  /* Check command age */
  i870util_read56BitTime(pMsg, &p104Sector->ctstaRequestTime);
  I14DIAG_SHOW_TIME(pSector, &p104Sector->ctstaRequestTime);

  /* Parse received cause of transmission */
  if(pMsg->cot == I14DEF_COT_ACTIVATION)
  {
    p104Sector->ctstaCOT = I14DEF_COT_ACTCON;
  }
  else
  {
    p104Sector->ctstaCOT = I14DEF_COT_UNKNOWN_COT | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  if(p104Sector->ctstaIOA != 0)
  {
    p104Sector->ctstaCOT = I14DEF_COT_UNKNOWN_IOA | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  if(s4util_checkCommandAge(p104Sector, &p104Sector->ctstaRequestTime) == TMWDEFS_FALSE)
  {
    /* 104 Edition 2 says there should be no response sent */
    p104Sector->ctstaCOT = 0;
    S14DIAG_ERROR(p104Sector->s14.i870.tmw.pSession, (TMWSCTR *)p104Sector, S14DIAG_CMD_DELAYED);
    return;
  }
}

/* function: s4ctsta_buildResponse */
TMWTYPES_BOOL TMWDEFS_CALLBACK s4ctsta_buildResponse(
  TMWSCTR *pSector, 
  TMWTYPES_BOOL buildResponse)
{
  S104SCTR *p104Sector = (S104SCTR *)pSector;

  if(p104Sector->ctstaCOT != 0)
  {
    if(buildResponse)
    {
      /* Activation confirmation or Deactivation */
      TMWSESN_TX_DATA *pTxData;
      S14SESN *pS14Session = (S14SESN *)pSector->pSession;

      if((pTxData = i870chnl_newTxData(pSector->pSession->pChannel, pSector->pSession, pSector,
        pS14Session->maxASDUSize)) == TMWDEFS_NULL)
      {
        return(TMWDEFS_FALSE);
      }

#if TMWCNFG_SUPPORT_DIAG
      pTxData->pMsgDescription = "Test Command Response";
#endif
      pTxData->txFlags = TMWSESN_TXFLAGS_NO_RESPONSE;
      pTxData->responseTimeout = p104Sector->s14.defaultResponseTimeout;

      /* Build response */
      i870util_buildMessageHeader(pSector->pSession, 
        pTxData, I14DEF_TYPE_CTSTA1, p104Sector->ctstaCOT, 
        p104Sector->ctstaOriginator, p104Sector->s14.i870.asduAddress);

      /* Store point number, should always 0, unless it was a bad IOA */
      i870util_storeInfoObjAddr(pSector->pSession, pTxData, p104Sector->ctstaIOA);

      /* Store sequence number */
      tmwtarg_store16(&p104Sector->testSequenceNumber, &pTxData->pMsgBuf[pTxData->msgLength]);
      pTxData->msgLength += 2;

      /* Store time */
      i870util_write56BitTime(pTxData, &p104Sector->ctstaRequestTime, TMWDEFS_FALSE);

      /* Request is complete */
      p104Sector->ctstaCOT = 0;

      /* Send the response */
      i870chnl_sendMessage(pTxData);
    }

    return(TMWDEFS_TRUE);
  }

  return(TMWDEFS_FALSE);
}

#endif /* S14DATA_SUPPORT_CTS && S14DATA_SUPPORT_TIMETAG_COMMANDS */

#if S14DATA_SUPPORT_104CTSNA
/* CTSNA, Test command without time is not required for 60870-5-104. 
 *   We make this function available to the 104 SCL because some customers require it 
 */
/* function: s4ctsna_processRequest */
void TMWDEFS_CALLBACK s4ctsna_processRequest(
  TMWSCTR *pSector, 
  I870UTIL_MESSAGE *pMsg)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  S104SCTR *p104Sector = (S104SCTR *)p14Sector;

  /* Store originator address */
  p104Sector->ctstaOriginator = pMsg->origAddress;  

  /* Parse Information Object Address */
  i870util_readInfoObjAddr(pSector->pSession, pMsg, &p104Sector->ctstaIOA);

  /* Parse test pattern */
  tmwtarg_get16(&pMsg->pRxData->pMsgBuf[pMsg->offset], &p104Sector->pattern);
  pMsg->offset += 2;

  /* Parse received cause of transmission */
  if(pMsg->cot == I14DEF_COT_ACTIVATION)
  {
    p104Sector->ctsnaCOT = I14DEF_COT_ACTCON;
  }
  else
  {
    p104Sector->ctsnaCOT = I14DEF_COT_UNKNOWN_COT | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  if(p104Sector->ctstaIOA != 0)
  {
    p104Sector->ctsnaCOT = I14DEF_COT_UNKNOWN_IOA | I14DEF_COT_NEGATIVE_CONFIRM;
    return;
  }

  /* Validate pattern */
  if(p104Sector->pattern != 0x55aa)
    p104Sector->ctsnaCOT |= I14DEF_COT_NEGATIVE_CONFIRM;
}

/* function: s4ctsna_buildResponse */
TMWTYPES_BOOL TMWDEFS_CALLBACK s4ctsna_buildResponse(
  TMWSCTR *pSector, 
  TMWTYPES_BOOL buildResponse)
{
  S14SCTR *p14Sector = (S14SCTR *)pSector;
  S104SCTR *p104Sector = (S104SCTR *)p14Sector;

  if(p104Sector->ctsnaCOT != 0)
  {
    if(buildResponse)
    {
      /* Activation confirmation or Deactivation */
      TMWSESN_TX_DATA *pTxData;
      S14SESN *pS14Session = (S14SESN *)pSector->pSession;

      if((pTxData = i870chnl_newTxData(pSector->pSession->pChannel, pSector->pSession, pSector,
        pS14Session->maxASDUSize)) == TMWDEFS_NULL)
      {
        return(TMWDEFS_FALSE);
      }

#if TMWCNFG_SUPPORT_DIAG
      pTxData->pMsgDescription = "Test Command Response";
#endif
      pTxData->txFlags = TMWSESN_TXFLAGS_NO_RESPONSE;
      pTxData->responseTimeout = p14Sector->defaultResponseTimeout;

      /* Build response */
      i870util_buildMessageHeader(pSector->pSession, 
        pTxData, I14DEF_TYPE_CTSNA1, p104Sector->ctsnaCOT, 
        p104Sector->ctstaOriginator, p14Sector->i870.asduAddress);

      /* Store point number, always 0 */
      i870util_storeInfoObjAddr(pSector->pSession, pTxData, p104Sector->ctstaIOA);

      /* Store test pattern */
      tmwtarg_store16(&p104Sector->pattern, &pTxData->pMsgBuf[pTxData->msgLength]);
      pTxData->msgLength += 2;

      /* Request is complete */
      p104Sector->ctsnaCOT = 0;

      /* Send the response */
      i870chnl_sendMessage(pTxData);
    }

    return(TMWDEFS_TRUE);
  }

  return(TMWDEFS_FALSE);
}

#endif /* S14DATA_SUPPORT_104CTSNA */
