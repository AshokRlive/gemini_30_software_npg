/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s14msp.h
 * description: This file is intended for internal SCL use only.
 *  IEC 60870-5-101 slave monitored single point functionality.
 */
#ifndef S14MSP_DEFINED
#define S14MSP_DEFINED

#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/utils/tmwdtime.h"
#include "tmwscl/utils/tmwsesn.h"
#include "tmwscl/i870/s14sesn.h"
#include "tmwscl/i870/s14sctr.h"
#include "tmwscl/i870/s14dbas.h"
#include "tmwscl/i870/s14evnt.h"

/* Structure used to store single point events */
typedef struct S14MSPEventStruct {
  S14EVNT s14Event;
} S14MSP_EVENT;

#ifdef __cplusplus
extern "C" {
#endif
   
  /* function: s14msp_init
   * purpose: Initialize MSP events
   * arguments:
   *  pSector - identifies sector
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL s14msp_init(
    TMWSCTR *pSector);

  /* function: s14msp_close 
   * purpose: Close MSP event processing
   * arguments:
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   * returns:
   *  void
   */
  void TMWDEFS_GLOBAL s14msp_close(
    TMWSCTR *pSector);

  /* function: s14msp_internalAddEvent 
   * purpose: Internal function called by SCL to add an event to the queue.
   *  This will not tell the link layer to ask the application for a message
   *  to send. 
   * arguments:
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   *  cot - cause of transmission
   *  ioa - Information object address
   *  siq - Single point information with quality descriptor, defined in 7.2.6.1
   *  pTimeStamp - pointer to time structures
   * returns:
   *  pointer to event structure if event was added, TMWDEFS_NULL otherwise
   */
  void * TMWDEFS_GLOBAL s14msp_internalAddEvent(
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot,
    TMWTYPES_ULONG ioa, 
    TMWTYPES_UCHAR siq, 
    TMWDTIME *pTimeStamp);

  /* function: s14msp_addEvent 
   * purpose: Exported function called by user to add an event to the queue.
   * arguments:
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   *  cot - cause of transmission
   *   normally I14DEF_COT_SPONTANEOUS,I14DEF_COT_RETURN_LOCAL or 
   *    I14DEF_COT_RETURN_REMOTE
   *  ioa - Information object address
   *  siq - Single point information with quality descriptor, defined in 7.2.6.1
   *   SIQ is the value of the 4 flags or�d together with the value bit.
   *  I14DEF_QUALITY_BL    0x10  value is blocked for transmission   
   *  I14DEF_QUALITY_SB    0x20  substituted by operator or an       
   *                               automatic source                    
   *  I14DEF_QUALITY_NT    0x40  not topical (not updated successfully)                       
   *  I14DEF_QUALITY_IV    0x80  invalid, corresponding value is     
   *                               incorrect and cannot be used        
   *  I14DEF_SIQ_OFF       0x00  Value off
   *  I14DEF_SIQ_ON        0x01  Value on
   *  pTimeStamp - time of event
   *   if pTimeStamp != TMWDEFS_NULL Event will be sent using the ASDU specified 
   *   by sector config mspTimeFormat
   *    To set RES1 bit to indicate Substituted time, 
   *     set pTimeStamp->genuineTime to TMWEFS_FALSE and make sure
   *     S14DATA_SUPPORT_GENUINE_TIME is defined in s14data.h
   *   if pTimeStamp == TMWDEFS_NULL the event will be sent using ASDU 1 MSPNA
   * returns:
   *  void * - non-NULL value indicates success; TMWDEFS_NULL indicates failure
   */
  TMWDEFS_SCL_API void * TMWDEFS_GLOBAL s14msp_addEvent(
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot,
    TMWTYPES_ULONG ioa, 
    TMWTYPES_UCHAR siq, 
    TMWDTIME *pTimeStamp);

  /* function: s14msp_countEvents
   * purpose: count MSP events
   * arguments:
   *  pSector - identifies sector
   * returns:
   *  number of events in queue
   */
  TMWTYPES_USHORT TMWDEFS_GLOBAL s14msp_countEvents(
    TMWSCTR *pSector);

  /* function: s14msp_scanForChanges
   * purpose: Scans slave database for changes to MSP data points. If scanning
   *  is enabled and an MSP point has changed an event will be added to the MSP event 
   *  queue.
   * arguments:
   *  pSector - identifies sector
   * returns:
   *  TMWDEFS_TRUE if change(s) have been found and event(s) have been added to queue
   *  TMWDEFS_FALSE otherwise
   */
  TMWTYPES_BOOL TMWDEFS_GLOBAL s14msp_scanForChanges(
    TMWSCTR *pSector); 

  /* function: s14msp_processEvents
   * purpose: Process MSP events into a message
   * arguments:
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   *  pEventTime - returns time of earliest event if a clock sync is required
   *  cotSpecified - this will be 0 or I14DEF_COT_RETURN_REMOTE. If not zero, only an event 
   *   with this COT will be sent. This allows COT REMOTE to be sent during command transmission
   *   before the ACT TERM but no other events to be sent, maintaining the priority order from
   *   the 101 spec.
   * returns:
   *  S14EVNT_SENT                - ASDU was sent
   *  S14EVNT_CLOCK_SYNC_REQUIRED - Spontaneous CCSNA is required
   *  S14EVNT_NOT_SENT            - No ASDU was sent
   */
  S14EVNT_STATUS TMWDEFS_GLOBAL s14msp_processEvents(
    TMWSCTR *pSector,
    TMWDTIME *pEventTime,
    TMWTYPES_UCHAR cotSpecified); 

  /* function: s14msp_processDblTransEvents
   * purpose: Process Double Transmission high priority events into a message
   * arguments:
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   * returns:
   *  S14EVNT_SENT                - ASDU was sent
   *  S14EVNT_NOT_SENT            - No ASDU was sent
   */
  S14EVNT_STATUS TMWDEFS_GLOBAL s14msp_processDblTransEvents(
    TMWSCTR *pSector);
 
  /* function: s14msp_readIntoResponse 
   * purpose: Read the MSP values for the point specified and store in response
   *  message to be sent to master
   * arguments:
   *  pTxData - pointer
   *  pSector - pointer to sector structure returned by s14sctr_openSector
   *  cot - cause of tranmission
   *  groupMask - mask specifying what group(s) points should be in
   *  ioa - information object address
   *  pPointIndex - pointer to index of point to be read. This should be updated
   *   by this function.
   * returns:
   *   status
   */
  S14DBAS_GROUP_STATUS TMWDEFS_CALLBACK s14msp_readIntoResponse(
    TMWSESN_TX_DATA *pTxData, 
    TMWSCTR *pSector, 
    TMWTYPES_UCHAR cot, 
    TMWDEFS_GROUP_MASK groupMask, 
    TMWTYPES_ULONG ioa,
    TMWTYPES_USHORT *pPointIndex);

#ifdef __cplusplus
}
#endif
#endif /* S14MSP_DEFINED */
