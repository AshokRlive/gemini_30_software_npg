/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: lnk4rdcy.h
 * description: IEC 60870-5-104 profile redundancy implementation
 */
#ifndef LNK4RDCY_DEFINED
#define LNK4RDCY_DEFINED

#include "tmwscl/utils/tmwlink.h"
#include "tmwscl/i870/i870lnk4.h"
#include "tmwscl/i870/i870chnl.h"

/* Define configuration parameters supported by this redundancy group
 * implementation.
 */
typedef struct lnk4RdcyConfig {

  /* Incremental application layer timeout. This is the maximum time in 
   * milliseconds that this device will wait for some type of response 
   * from the remote device, if a command is outstanding. Once any response  
   * is received on this channel this period is restarted.
   */
  TMWTYPES_MILLISECONDS incrementalTimeout;
  
  /* If TRUE this side determines which link to make ACTIVE etc.                  
   */   
  TMWTYPES_BOOL   isControlling; 

  /* routine to call when events occur on this channel,
   *   can be used to track statistics on this channel.
   */
  TMWCHNL_STAT_CALLBACK  pStatCallback;

  /* pCallbackParam - user callback parameter to pass to pCallback 
   */
  void                  *pStatCallbackParam;

} LNK4RDCY_CONFIG;

/* State of redundancy group and individual connections/channels */
typedef enum {

  /* Offline, not communicating, initial state */
  LNK4RDCY_STATE_OFFLINE,

  /* For redundant connection online received, but this is not the active 
   *  connection 
   * For redundancy group at least one connection is online, but START DT has not
   *  been sent/received.
   */
  LNK4RDCY_STATE_INACTIVE,

  /* For redundant connection, START DT has been sent/received, 
   *  this is active connection
   * For redundancy group a START DT has been sent/received, there is an active 
   *  connection
   */
  LNK4RDCY_STATE_ACTIVE,

} LNK4RDCY_STATE;

/* Forward declarations*/

typedef struct lnk4RdcyGroupContext {
  /* Generic channel info, must be first  */
  TMWCHNL                         tmw;    

  /* state of this redundancy group       */
  LNK4RDCY_STATE                 state;    

  /* Pointer to session opened on this redundancy group */
  TMWSESN                       *pSession;

  /* pointer to the connection/channel  
   * currently being used by this rdcy group  
   */
  struct lnk4RdcyConnectContext *pCurrentLink; 

  /* List of redundant link layer connections/channels    */
  TMWDLIST                       rdntChannels;      

  /* If true this side determines which link is active 
   * and sends START DT and STOP DT etc
   */
  TMWTYPES_BOOL                  isControlling;

  TMWTIMER                       delayTimer;

} LNK4RDCY_GROUP_CONTEXT;

typedef struct lnk4RdcyConnectContext {
  /* List Member, must be first entry so this can be put in linked list */
  TMWDLIST_MEMBER           listMember;

  /* state of this redundant connection/channel */
  LNK4RDCY_STATE            state;         

  /* Pointer to channel containing link and phys */
  TMWCHNL                  *pLnk4Channel;  

} LNK4RDCY_CONNECT_CONTEXT;

#ifdef __cplusplus
extern "C" {
#endif
 
  /* function: lnk4rdcy_initConfig
   * purpose: initialize redundancy configuration
   * arguments:  
   *  pConfig - pointer to lnk4rdcy configuration information
   * returns:
   *  void
   */
  TMWDEFS_SCL_API void TMWDEFS_GLOBAL lnk4rdcy_initConfig(
    LNK4RDCY_CONFIG *pConfig);

  /* function: lnk4rdcy_initRdcyGroup
   * purpose: initialize a new redundancy group.
   * arguments:  
   *  pConfig - pointer to lnk4rdcy configuration information
   * returns:
   *  TMWTYPES_BOOL - TMWDEFS_TRUE if successful
   *                 TMWDEFS_FALSE otherwise
   */
  TMWDEFS_SCL_API TMWCHNL * TMWDEFS_GLOBAL lnk4rdcy_initRdcyGroup(
    const LNK4RDCY_CONFIG *pRdcyConfig);

  /* function: lnk4rdcy_deleteRdcyGroup
   * purpose: delete this redundancy group, freeing all allocated
   *  memory and releasing resources.
   * arguments:
   *  pChannel - pointer to TMWCHNL structure
   * returns:
   *  void
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL lnk4rdcy_deleteRdcyGroup(
    TMWCHNL *pChannel);

  /* function: lnk4rdcy_initConfig
   * purpose: initialize a redundancy onfiguration structure
   * arguments:
   *  pConfig - pointer to lnk4rdcy_config configuration information
   *      to be filled in
   * returns:
   *  void
   */
  TMWDEFS_SCL_API void TMWDEFS_GLOBAL lnk4rdcy_initConfig(LNK4RDCY_CONFIG *pConfig);

  /* function: lnk4rdcy_openRedundantChannel
   * purpose: Open a 104 link layer channel and adds it to this redundancy group.
   * arguments:
   *  pApplContext - application context to add channel to
   *  pRdcyGroup - pointer to redundancy group channel to add channel to
   *  pChnlConfig - pointer to channel configuration
   *  pLinkConfig - pointer to link layer configuration
   *  pPhysConfig - pointer to physical layer configuration
   *  pIOConfig - pointer to target I/O configuration data structure
   *   which is passed directly to the target routines implemented
   *   in tmwtarg.h/c
   *  pTmwTargConfig - TMW specific IO configuration information 
   *   which will be passed to low level IO routines in tmwtarg.h.
   * returns:
   *  pointer to new channel, this pointer is used to reference this
   *  channel in other calls to the SCL.
   */
  TMWDEFS_SCL_API TMWCHNL * TMWDEFS_GLOBAL lnk4rdcy_openRedundantChannel(
    TMWAPPL               *pApplContext,
    TMWCHNL               *pRdcyGroup,
    const I870CHNL_CONFIG *pChnlConfig,
    I870LNK4_CONFIG       *pLinkConfig,
    const TMWPHYS_CONFIG  *pPhysConfig,
    const void            *pIOConfig,
    struct TMWTargConfigStruct *pTmwTargConfig);

  /* function: lnk4rdcy_closeRedundantChannel
   * purpose: Closes and removes a 104 link layer channel from this redundancy 
   *  group.
   * arguments:
   *  pRdcyGroupChannel - pointer to TMWCHNL structure containing
   *     redundancy group
   *  pRdntChannel - pointer to TMWCHNL structure containing 104 link layer
   * returns:
   *  TMWDEFS_TRUE if successful, else TMWDEFS_FALSE
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL lnk4rdcy_closeRedundantChannel(
    TMWCHNL *pRdcyGroupChannel,
    TMWCHNL *pRdntChannel);

  /* function: lnk4rdcy_manualSwitchOver
   * purpose: Perform manual switch to specified redundant channel. 
   * arguments:
   *  pRdcyGroupChannel - pointer to TMWCHNL structure containing
   *   redundancy group
   *  pRdntChannel - pointer to TMWCHNL structure containing 104 link layer
   *   to make active
   * returns:
   *  TMWDEFS_TRUE if successful, else TMWDEFS_FALSE
   */
  TMWDEFS_SCL_API TMWTYPES_BOOL TMWDEFS_GLOBAL lnk4rdcy_manualSwitchOver(
    TMWCHNL *pRdcyGroupChannel,
    TMWCHNL *pRdntChannel);

#ifdef __cplusplus
}
#endif
#endif
