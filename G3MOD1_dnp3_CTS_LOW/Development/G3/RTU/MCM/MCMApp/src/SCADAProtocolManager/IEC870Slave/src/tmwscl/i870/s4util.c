/*****************************************************************************/
/* Triangle MicroWorks, Inc.                         Copyright (c) 1997-2014 */
/*****************************************************************************/
/*                                                                           */
/* This file is the property of:                                             */
/*                                                                           */
/*                       Triangle MicroWorks, Inc.                           */
/*                      Raleigh, North Carolina USA                          */
/*                       www.TriangleMicroWorks.com                          */
/*                          (919) 870-6615                                   */
/*                                                                           */
/* This Source Code and the associated Documentation contain proprietary     */
/* information of Triangle MicroWorks, Inc. and may not be copied or         */
/* distributed in any form without the written permission of Triangle        */
/* MicroWorks, Inc.  Copies of the source code may be made only for backup   */
/* purposes.                                                                 */
/*                                                                           */
/* Your License agreement may limit the installation of this source code to  */
/* specific products.  Before installing this source code on a new           */
/* application, check your license agreement to ensure it allows use on the  */
/* product in question.  Contact Triangle MicroWorks for information about   */
/* extending the number of products that may use this source code library or */
/* obtaining the newest revision.                                            */
/*                                                                           */
/*****************************************************************************/

/* file: s4util.c
 * description: IEC 60870-5-104 slave utilities.
 */
#include "tmwscl/i870/i870diag.h"
#include "tmwscl/i870/s4util.h"
#include "tmwscl/utils/tmwtarg.h"
#include "tmwscl/i870/i14diag.h"
 
/* function: s4util_checkCommandAge */
TMWTYPES_BOOL TMWDEFS_GLOBAL s4util_checkCommandAge(
  S104SCTR *p104Sector,
  TMWDTIME *pDateTime)
{
  TMWDTIME currentTime;
  TMWDTIME messageTime;
  TMWDTIME tempMsgTime;

  /* Store the received time so it does not get changed below */
  messageTime = *pDateTime;

  /* Get current time */
  s14data_ccsnaGetTime(p104Sector->s14.i870.pDbHandle, &currentTime); 
  
  I14DIAG_SHOW_TIME((TMWSCTR*)p104Sector, pDateTime);

  /* XXX Changed by Lucy */
  /* WARNING: Lucy disabled the following lines since they are correcting the
   * summer time (DST) meanwhile in the Gemini3 RTU there is only UTC time. This
   * correction code makes a conflict since it corrects the incoming time for
   * summer time while the RTU does not support summer time, making the offset
   * too big in summer time.
   * Note that a Master Session properly configured for an UTC slave will not
   * send a time stamp with DST and therefore not causing any problem.
  if(currentTime.dstInEffect != messageTime.dstInEffect)
  {
    if(messageTime.dstInEffect)
    {
      tmwdtime_decrementHour(&messageTime);
    }
    else
    {
      tmwdtime_decrementHour(&currentTime);
    }
  }
  */

  /* Make sure command is within maxCommandAge of the current time 
   * There is a conformance test that says time in the future 
   * by one hour also fails. We will only allow maxCommandFuture
   * in the future, though the spec does not address this.
   */
  
  /* Add maximum age to time from command message */
  tempMsgTime = messageTime;
  tmwdtime_addOffset(&tempMsgTime, p104Sector->maxCommandAge);

  /* Is command not too old? */
  if (tmwdtime_checkTimeOrder(&currentTime, &tempMsgTime))
  {
    /* Add max age to current time */
    tmwdtime_addOffset(&currentTime, p104Sector->maxCommandFuture);

    /* Is the time not too far in the future? */
    if (tmwdtime_checkTimeOrder(&messageTime, &currentTime)) 
    {
      /* message time is within range */
      return(TMWDEFS_TRUE);
    }
  }

  /* message time is outside of range */
  return(TMWDEFS_FALSE);
}
