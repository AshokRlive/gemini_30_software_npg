/*! \files
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:DialupModemSM.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   22 Aug 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "LockingMutex.h"

#include "DialupModemSM.h"
#include "DialupModem.h"
#include "DialupModem_fsm.h"
#include "CommsDeviceDebug.h"
#include "ModemCommon.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define TRACE_INFO DBG_TRACK("[function called]\"%s\" >> %s",name(),__FUNCTION__)

const static lu_uint32_t DIAL_STR_NUM = 2;

#define RETRY_INTERVAL_NUM DialupModem::DialupModemConfig::RETRY_INTERVAL_NUM


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
extern int initPaknet (int commsPortFD, const DialupModem::DialupModemConfig& conf);

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
DialupModemSM::DialupModemSM(DialupModem& modem):
                log(Logger::getLogger(SUBSYSTEM_ID_COMMS_DEVICE)),
                mp_fsm(NULL),
                m_fsmOpMutex(),
                m_owner(modem),
                m_connRequested(false),
                m_connRetries(0),
                m_dialStrIndex(0),
                m_lastPingTicks(0),
                m_initState(INIT_LAST),
                m_pingState(PING_LAST),
                m_dialState(DIAL_STATE_LAST),
                m_portFD(-1),
                m_paknetModem(NULL)
{
    mp_fsm = new DialupModem_fsm(*this);
    startTimer(TIMER_MODEM_INACTIVITY);
    startTimer(TIMER_REGULAR_CALL);
    m_owner.getSniffer().setRcvDataTimer(&m_timers[TIMER_MODEM_INACTIVITY]);

    //Enable state machine debugging
    mp_fsm -> setDebugFlag(false);
}


DialupModemSM::~DialupModemSM()
{
    delete m_paknetModem;
    const DialupModem::DialupModemConfig& conf = m_owner.getConfig();
    conf.port.closePort();
    delete mp_fsm;
}

const char* DialupModemSM::name()
{
    return m_owner.getConfig().name.c_str();
}

/************************************/
/* State Machine Operation API       */
/************************************/

bool DialupModemSM::connect()
{
    //TRACE_INFO;

    LockingMutex locker(m_fsmOpMutex);
    mp_fsm->EVT_CONNECT_REQUESTED();
    return m_connRequested;
}

void DialupModemSM::disconnect()
{
    TRACE_INFO;

    LockingMutex locker(m_fsmOpMutex);
    mp_fsm->EVT_DISCONNECT();
}


void DialupModemSM::enterStartState()
{
    TRACE_INFO;

    LockingMutex locker(m_fsmOpMutex);
    mp_fsm->enterStartState();
}


void DialupModemSM::tickEvent(lu_uint32_t ticks)
{
    LockingMutex locker(m_fsmOpMutex);


    // Check if it is needed to ping
    if((ticks - m_lastPingTicks) >= PING_INTERVAL_TICKS)
    {
        DBG_TRACK("Requesting Ping");
        mp_fsm->EVT_PING_REQUESTED();
        m_lastPingTicks = ticks;
    }

    // Check if it is needed to connect
    if(m_connRequested)
    {
        mp_fsm->EVT_CONNECT();
    }


    mp_fsm->EVT_TICK(ticks);
}

void DialupModemSM::checkTimer()
{
    LockingMutex locker(m_fsmOpMutex);

    m_timers[TIMER_CONNECT_INACTIVITY].reset(m_owner.getSniffer().getLastRcvTS());

    for (lu_uint32_t i = 0; i < TIMER_LAST; ++i) {
        if(m_timers[i].isTimeout())
        {
            switch (i)
            {
                case TIMER_WAIT_RETRY:
                    mp_fsm->EVT_TIMER_RETRY();
                    break;

                case TIMER_DROP_DTR  :
                    log.debug("DialupModemSM: Drop DTR");
                    mp_fsm->EVT_TIMER_DROP_DTR();
                    break;

                case TIMER_RAISE_DTR :
                    log.debug("DialupModemSM: Raise DTR");
                    mp_fsm->EVT_TIMER_RAISE_DTR();
                    break;

                case TIMER_WAIT_ANSWER:
                    mp_fsm->EVT_TIMER_WAIT_ANSWER();
                    break;

                case TIMER_POWER_CYCLE:
                    log.debug("DialupModemSM: Power Cycle");
                    mp_fsm->EVT_TIMER_POWER_CYCLE();
                    break;

                case TIMER_MODEM_INACTIVITY:
                    log.debug("DialupModemSM: Inactivity");
                    mp_fsm->EVT_TIMER_MODEM_INACTIVITY();
                    break;

                case TIMER_CONNECT_INACTIVITY:
                    mp_fsm->EVT_TIMER_CONNECT_INACTIVITY();
                    break;

                case TIMER_REGULAR_CALL:
                    log.debug("DialupModemSM: Regular Call");
                    connect();
                    resetTimer(TIMER_REGULAR_CALL);
                    break;

                case TIMER_CHECK_MODEM_RESPONSE:
                    mp_fsm->EVT_TIMER_CHECK_MODEM_RESPONSE();
                    break;

                default:
                    DBG_ERR("Unrecognised timer:%i",i);
                    break;
            }
        }
    }

}



/************************************/
/* State Machine Actions API        */
/************************************/


void DialupModemSM::setPowerOn()
{
    // Nothing to do here, the PSM Comms Supply is always
    // powered (if fitted).
    TRACE_INFO;
}

void DialupModemSM::setPowerOff()
{
    // The PSM Comms Supply is always powered, we can tell it to turn
    // off for a specific period, whereupon it will turn back on
    TRACE_INFO;

    IOM_ERROR ret = IOM_ERROR_GENERIC;
    PowerSupplyCANChannel *PowerSupplyChan;

    PowerSupplyChan = (PowerSupplyCANChannel *) m_owner.getPowerSupplyChan();

    if (PowerSupplyChan == NULL)
    {
        log.error("DialupModemSM: Cannot reset Power as no PSM channel found");
        return;
    }

    //Send command to the channel
    ret = PowerSupplyChan->operate( 1,          //Ignored by the PSM
                                    LU_FALSE,   //Ignored by the PSM
                                    TIMEOUT_POWER_CYCLE / 1000 //duration
                                    );
    if(ret != IOM_ERROR_NONE)
    {
        ret = PowerSupplyChan->cancel();
    }
}

void DialupModemSM::raiseDTR()
{
    TRACE_INFO;
    m_owner.setDTR(true);
}

void DialupModemSM::dropDTR()
{
    TRACE_INFO;
    m_owner.setDTR(false);
}

void DialupModemSM::startTimer(TIMER timerID)
{
    if(timerID >=0 && timerID < TIMER_LAST)
    {
        lu_uint32_t timeout;
        switch (timerID)
        {
            case TIMER_WAIT_RETRY:
                timeout = getRetryDelayMS();
                log.debug("DialupModemSM: Retry in %d ms", timeout);
                break;
            case TIMER_DROP_DTR:
                timeout = TIMEOUT_DROP_DTR;
                break;
            case TIMER_RAISE_DTR:
                timeout = TIMEOUT_RAISE_DTR;
                break;
            case TIMER_WAIT_ANSWER:
                timeout = m_owner.getConfig().connectTimeoutMs;
                break;
            case TIMER_POWER_CYCLE:
                timeout = TIMEOUT_POWER_CYCLE;
                break;
            case TIMER_MODEM_INACTIVITY:
                timeout = m_owner.getConfig().modemInactivityTimeoutMins*60000;
                break;
            case TIMER_CONNECT_INACTIVITY:
                timeout = m_owner.getConfig().connectionInactivityTimeoutSecs*1000;
                break;
            case TIMER_REGULAR_CALL:
                timeout = m_owner.getConfig().regularCallIntervalMins*60000;
                break;
            case TIMER_CHECK_MODEM_RESPONSE:
                timeout = TIMEOUT_CHECK_MODEM_RESPONSE;
                break;
            default:
                log.error("Timeout is not specified for timer:%i. %s",timerID,__AT__);
                timeout = 3000;
                break;
        }

        startTimer(timerID,timeout);
    }
    else
    {
        log.error("Start timer error. Invalid timer ID:%i %s",timerID,__AT__);
    }
}

void DialupModemSM::startTimer(TIMER timerID,lu_uint32_t timeoutMs)
{
    DBG_TRACK("Start timer:%i timeout:%u ms",timerID,timeoutMs);
    m_timers[timerID].start(timeoutMs);
}

void DialupModemSM::stopTimer (TIMER timerID)
{
    DBG_TRACK("Stop timer:%i",timerID);
    m_timers[timerID].stop();
}

void DialupModemSM::resetTimer (TIMER timerID)
{
    DBG_TRACK("Reset timer:%i",timerID);
    m_timers[timerID].reset();

    if(timerID == TIMER_MODEM_INACTIVITY)
    {
        DBG_INFO("Reset Inactivity timer!!!");
    }
}

void DialupModemSM::dial()
{
    TRACE_INFO;

    if(m_dialStrIndex >= DIAL_STR_NUM)
    {
        /* Dial string not available*/
        m_dialState = DIAL_STATE_WAIT_RETRY;
        return;
    }

    std::string dialString = getDialString(m_dialStrIndex);

    /* #3387 Soft reset when retry backup dial str.*/
    if(m_dialStrIndex != 0)
    {
        dropDTR();
        lucy_usleep(TIMEOUT_DROP_DTR*1000);

        raiseDTR();
        lucy_usleep(TIMEOUT_RAISE_DTR*1000);
    }

    m_dialStrIndex ++;

    log.info("\"%s\" Dialling %s ...",name(),dialString.c_str());
    DBG_INFO("\"%s\" Dialling %s ...",name(),dialString.c_str());

    lu_int32_t nbytes=write(m_portFD, dialString.c_str(), dialString.size());
    if(nbytes < 0)
    {
        log.error("write error! fd:%i bytes:%d error:%s %s",
                        m_portFD,nbytes,strerror(errno),__AT__);
    }
    else
    {
        DBG_INFO("\"%s\" dialling string write succeeded",name());
        m_dialState = DIAL_STATE_WAIT_ANSWER;
    }
}

void DialupModemSM::checkForAnswer()
{
    TRACE_INFO;

    lu_uint32_t nbytes;
    char buf[RX_BUFF_SIZE];
    SNIFF_STATE state;

    if((nbytes = read(m_portFD, buf, RX_BUFF_SIZE)) > 0)
    {
        DBG_INFO("checkForAnswer >> attempt to sniff data:[%d] [%s]",nbytes, buf);
        state = m_owner.getSniffer().sniffRcvData(buf, nbytes);

        switch (state)
        {
            case SNIFF_STATE_NEW_CONNECTION:
                log.info("Dialling succeeded!");
                DBG_INFO("checkForAnswer >> Dialling succeeded!");
                m_dialState = DIAL_STATE_FINISHED;
            break;

            default:
                DBG_INFO("checkForAnswer >> sniff result state:%d",state);
                break;
        }
    }
}


void DialupModemSM::initialise()
{
    TRACE_INFO;

    lu_int32_t ret;

    // 1. Access Port and open port
    const DialupModem::DialupModemConfig& conf = m_owner.getConfig();
    m_portFD = conf.port.openPort();
    if(m_portFD < 0)
    {
        log.error("\"%s\" Port open failed!",name());
        m_initState = INIT_FAILURE;
        return;
    }

    // Raise DTR
    conf.port.setDTR(LU_TRUE);

    // 2. Call init func
        /* TODO: pueyos_a - DialupModem should be independent of the Paknet modem */
    if(m_paknetModem == NULL)
    {
        m_paknetModem = new PaknetModem(m_portFD, conf);
    }
    ret = m_paknetModem->init();
//    ret = initPaknet(m_portFD, conf, m_rateModeIdx);

    // 3. Update initState m_initState;
    switch(ret)
    {
        case PAKNET_INIT_OK     :
            m_initState  = INIT_OK;
            break;

        case PAKNET_INIT_FAIL   :
            log.error("Paknet initialisation failed!");
            m_initState  = INIT_FAILURE;
            break;

        case PAKNET_INIT_RETRY  :
            m_initState  = INIT_RETRY;
            break;

        default:
            m_initState  = INIT_FAILURE;
            break;
    }
}

void DialupModemSM::ping()
{
    TRACE_INFO;
    m_pingState = PING_FAILURE;

    int nbytes;
    nbytes=write(m_portFD, "\r", 1);
    if(nbytes < 0)
    {
        DBG_ERR("Write error! fd:%i bytes:%d error:%s", m_portFD,nbytes,strerror(errno));
    }
    else
    {
        checkModemResponse();
    }

    DBG_INFO("Ping result state: %d", m_pingState);
}

void DialupModemSM::checkModemResponse()
{
    int nbytes;
    char buf[RX_BUFF_SIZE];
    char c;

    lucy_usleep(50000);

    // Only read one byte at a time that will add up into the sniffer buffer
//    while ((nbytes = read(m_portFD, bufPos, (RX_BUFF_SIZE - totalBytes))) > 0 && (totalBytes < RX_BUFF_SIZE))
    while (((nbytes = read(m_portFD, &c, 1)) > 0) && (m_pingState != PING_INCOMING_CALL))
    {
//        bufPos += nbytes;
//        totalBytes += nbytes;

        //if (totalBytes > 0)
        {
            SNIFF_STATE state;
//            state = m_owner.getSniffer().sniffRcvData(buf, totalBytes);
            state = m_owner.getSniffer().sniffRcvData(&c, 1);
            DBG_INFO("ping::sniff state:%i",state);
            switch(state)
            {
                case SNIFF_STATE_PING_OK:
                    log.info("Ping response OK");
                    m_pingState = PING_OK;
                    break;

                case SNIFF_STATE_NEW_CONNECTION:
                    log.info("New incoming call");
                    m_pingState = PING_INCOMING_CALL;

                    resetTimer(TIMER_MODEM_INACTIVITY);

                    // SKA - Added to speed up state transition
                    notifyStateChange(CONNECTED);
                    break;

                case SNIFF_STATE_PROTOCOL_DATA:
                    break;  // Stop sniffing

                default:
                    m_pingFails ++;
                    if(m_pingFails > 3)
                    {
                        m_pingState = PING_FAILURE;
                        log.debug("Ping FAILURE!!!");
                    }
            }
        }
    }
}

void DialupModemSM::resetPingState()
{
    TRACE_INFO;

    m_pingFails = 0;
    m_pingState = PING_LAST;
}

void DialupModemSM::notifyStateChange(CONNECT_STATE newState)
{
    TRACE_INFO;

    bool connected = (newState == CONNECTED);
    lu_int32_t fd = connected? m_portFD : -1;

    /* Notify all observer connected state*/
    std::vector<ICommsDeviceObserver*> obs = m_owner.getAllObservers();
    for(std::vector<ICommsDeviceObserver*>::iterator it = obs.begin();
                    it != obs.end();
                    ++it
        )
    {
        (*it)->notifyConnectionState(connected,fd);
    }
}


bool DialupModemSM::moreRetries()
{
    return (m_connRetries < RETRY_INTERVAL_NUM  &&
            m_owner.getConfig().dialRetryIntervalSecs[m_connRetries] != 0);
}


lu_uint32_t DialupModemSM::getRetryDelayMS()
{
    if (m_connRetries < RETRY_INTERVAL_NUM)
        return m_owner.getConfig().dialRetryIntervalSecs[m_connRetries]*1000;
    else
    {
        log.error("Invalid m_connRetries:%i %s!",m_connRetries,__AT__);
        return 0;
    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
std::string DialupModemSM::getDialString(lu_uint32_t index)
{
    std::string ret;

    switch(index)
    {
    case 0:
        ret = m_owner.getConfig().dialString1;
//        ret = "23538680386784";     // TODO FOR TEST
//        ret = "23538680577887";     // TODO FOR TEST
//        ret = "23533420116850";  // UKPN Test NUA1
        break;

    case 1:
        ret = m_owner.getConfig().dialString2;
//        ret = "23538680386785";     // TODO FOR TEST
//        ret = "23533420116851";  // UKPN Test NUA1
        break;

    default:
        log.error("dial string not available at index:%i %s",index,__AT__);
        break;
    }

    ret += "\r";


    return ret;
}


/*
 *********************** End of file ******************************************
 */
