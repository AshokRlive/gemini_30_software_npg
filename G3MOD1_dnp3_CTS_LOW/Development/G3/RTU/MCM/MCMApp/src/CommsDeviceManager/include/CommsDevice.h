/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:CommsDevice.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   21 Aug 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef COMMSDEVICE_H_
#define COMMSDEVICE_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "ObserveableObj.h"
#include "Logger.h"
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
class ICommsDeviceObserver; // Forward declaration


class CommsDevice:public ObserveableObj<ICommsDeviceObserver>
{
public:
    CommsDevice(lu_uint32_t deviceId);
    virtual ~CommsDevice();

    /**
     * Initialises this Comms Device.
     */
    virtual void init() = 0;

    virtual void tickEvent(lu_uint32_t ticks) = 0;

    virtual void checkTimer() = 0;

public:
    const lu_uint32_t deviceID;

protected:
    Logger& log;

};



class ICommsDeviceObserver
{
public:
    ICommsDeviceObserver()
    {
    }
    ;
    virtual ~ ICommsDeviceObserver()
    {
    }
    ;

    virtual void notifyConnectionState(bool connected, lu_int32_t commDevFD) = 0;

};

#endif /* COMMSDEVICE_H_ */

/*
 *********************** End of file ******************************************
 */
