/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:CommsTrafficSniffer.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   21 Aug 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include "time.h"

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "DialupModem.h"
#include "TimeManager.h"
#include "CommsTrafficSniffer.h"
#include "CommsDeviceDebug.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
// <NUA> = Network User Address whcih is a unique 14 digit number
static const char connectString[]        = " COM";
static const char connectClearedString[] = "CLR";
static const char pingResponse[]         = "*\r";


// Array of states together with the strings to look for in the 'sniffed' data
static const StateStringStr sniffRxToState[] = { { SNIFF_STATE_NEW_CONNECTION, connectString       },
                                                 { SNIFF_STATE_CLR,            connectClearedString },
                                                 { SNIFF_STATE_PING_OK,        pingResponse        },
                                                 { SNIFF_STATE_LAST,           NULL                },
                                               };

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
CommsTrafficSniffer::CommsTrafficSniffer():
                m_lastRcvTS(),
                sniffBuffPos(0),
                mp_rcvDataTimer(NULL)
{
    clock_gettime( CLOCK_MONOTONIC, &m_lastRcvTS);

    // Clear sniffbuff
    memset(sniffBuff, '\0', SNIFFBUFFSIZE);
}

CommsTrafficSniffer::~CommsTrafficSniffer()
{

}

void CommsTrafficSniffer::setRcvDataTimer(SMTimer* smtimer)
{
    if (mp_rcvDataTimer == NULL)
    {
        mp_rcvDataTimer = smtimer;
    }
    else
    {
        DBG_ERR("setRcvDataTimer error. %s",__AT__);
    }
}


SNIFF_STATE CommsTrafficSniffer::sniffBuffer(const lu_int8_t* dataPtr, lu_uint32_t len)
{
    SNIFF_STATE returnState = SNIFF_STATE_LAST;

    const lu_int8_t *p;
    lu_uint32_t needleLen;

    for (int needle = 0; sniffRxToState[needle].stateString != NULL; needle++)
    {
        needleLen = strlen(sniffRxToState[needle].stateString);
        for (lu_uint32_t i = 0; (i < (len - needleLen) && (len > needleLen)); i++)
        {
            p = dataPtr+i;
            if (memcmp(p, sniffRxToState[needle].stateString, LU_MIN(len, needleLen)) == 0)
            {
                returnState =  sniffRxToState[needle].state;
                DBG_INFO("Found [%s] - state [%d]",sniffRxToState[needle].stateString, returnState);
            }
        }
    }

//    if(returnState == SNIFF_STATE_PROTOCOL_DATA && mp_rcvDataTimer != NULL)
//    {
//        mp_rcvDataTimer->reset();
//    }

    return returnState;
}

SNIFF_STATE CommsTrafficSniffer::sniffRcvData(const lu_int8_t* dataPtr, lu_uint32_t len)
{
    SNIFF_STATE returnState = SNIFF_STATE_LAST;
    lu_uint32_t numBytes;
    char* start;

    if ((dataPtr == NULL))
    {
        return SNIFF_STATE_ERROR;
    }

    // If there are a 'few' bytes in the incoming buffer, parse them.
    // If we find nothing, append the data to our buffer
    if (len > 2)
    {
        if ((returnState = sniffBuffer(dataPtr, len)) != SNIFF_STATE_LAST)
        {
            // Empty our buffer
            sniffBuffPos = 0;
            memset(sniffBuff, '\0', SNIFFBUFFSIZE);
            return returnState;
        }
    }

    // Append incoming data to our buffer and parse it
    start = sniffBuff + sniffBuffPos;
    numBytes = LU_MIN(len, (SNIFFBUFFSIZE - sniffBuffPos));

    memcpy((void *)start, (const void *)dataPtr, numBytes);
    sniffBuffPos = (sniffBuffPos + numBytes) % SNIFFBUFFSIZE;
    sniffBuff[SNIFFBUFFSIZE] = '\0';

    returnState = sniffBuffer(sniffBuff, sniffBuffPos);

    // Clear sniff buffer
    if (returnState != SNIFF_STATE_LAST)
    {
        DBG_INFO("%s Emptying buffer and returning state:%d", __AT__, returnState);
        sniffBuffPos = 0;
        memset(sniffBuff, '\0', SNIFFBUFFSIZE);
        return returnState;
    }

    // If no recognised string is found, assume it's protocol data
    clock_gettime( CLOCK_MONOTONIC, &m_lastRcvTS);
    //DBG_INFO("%s returning state [%d]",__AT__, SNIFF_STATE_PROTOCOL_DATA);
    return SNIFF_STATE_PROTOCOL_DATA;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
