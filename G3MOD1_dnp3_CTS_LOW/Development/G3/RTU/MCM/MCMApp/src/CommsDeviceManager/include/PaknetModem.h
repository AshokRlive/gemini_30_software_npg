/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: PaknetModem.h 24 Nov 2015 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/CommsDeviceManager/include/PaknetModem.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 24 Nov 2015 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24 Nov 2015   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef PAKNETMODEM_H__INCLUDED
#define PAKNETMODEM_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "ModemCommon.h"
#include "Logger.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define PAKNET_INIT_OK 0
#define PAKNET_INIT_RETRY 1
#define PAKNET_INIT_FAIL -1

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Paknet initialisation specialised class
 *
 * This class is intended for initialisation of the Paknet modem.
 * The implementation tries to resolve a problem that make other instances of the
 * SCADA protocol (even TCP-based) to stuck when the Paknet modem was trying to
 * set the modem speed by setting baud, write request, wait for response.
 * The object is intended to allow to call init several times until connection,
 * releasing the thread to the caller after a little try and keeping control of
 * the internal state so it can resume the trying activities.
 * This implementation is far from ideal -- not even good, but should do for now.
 */
class PaknetModem
{
public:
    typedef enum
    {
        MODE7E1,
        MODE8N1
    } ModeDefs;

public:
    PaknetModem(const lu_int32_t commsPortFD, const DialupModemCfg& conf);
    virtual ~PaknetModem();

    lu_int32_t init();

private:
    int try_attention(const char *attn_str, const char *ok_str);

    void set_baud_and_parity(const int baud, const PaknetModem::ModeDefs mode);

    int findBaudSetting();

    void send_init(const char *init_str);

private:
    Logger& log;
    const lu_uint32_t num_rate_modes;   //Max amount of rate modes
    const lu_uint32_t MAXTRIES; //Max number of attention tries before trying another rate
    const DialupModemCfg& m_config;
    lu_int32_t m_fileDesc;      //file descriptor
    lu_int32_t m_rateModeIdx;   //rate_mode[] index
    bool m_changedRate;         //Indicates that the rate has been changed
    lu_uint32_t m_tries;        //Number of attention tries so far
    lu_uint32_t m_triedRates;   //Human-apdapted conversion from index
                                // Since the index can start from any value, we
                                // show it to the user as the 1st rate to try.
    lu_uint64_t m_elapsedWrite_ms;  //Time elapsed since last write try for attention
};


#endif /* PAKNETMODEM_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
