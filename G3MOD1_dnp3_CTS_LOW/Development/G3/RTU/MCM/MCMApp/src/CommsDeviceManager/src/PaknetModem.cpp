/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: PaknetModem.cpp 24 Nov 2015 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/CommsDeviceManager/src/PaknetModem.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 24 Nov 2015 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24 Nov 2015   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdio.h>   /* Standard input/output definitions */
#include <string.h> /* String function definitions */
#include <unistd.h> /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <time.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "PaknetModem.h"
#include "FileUtil.h"
#include "dbg.h"
#include "timeOperations.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define PAKNET_ATTENTION     "\r"
#define PAKNET_OK            "*"
#define NUM_ATTN_TRIES       3
#define MAX_INIT_STRING_SIZE 100 /* Max length of initialisation string */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
struct RateMode
{
    int         rate;
    PaknetModem::ModeDefs    mode;
    const char* rate_name;
    const char* mode_name;
};

struct BaudSetting
{
    int rate;
    int setting;
};

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static RateMode rate_modes[] = {
    { B4800,   PaknetModem::MODE7E1, "4800",   "7E1" }, // Default for a new PAD
    { B4800,   PaknetModem::MODE8N1, "4800",   "8N1" },
    { B9600,   PaknetModem::MODE8N1, "9600",   "8N1" }, // Default for UKPN
    { B9600,   PaknetModem::MODE7E1, "9600",   "7E1" },
    { B19200,  PaknetModem::MODE8N1, "19200",  "8N1" },
    { B38400,  PaknetModem::MODE8N1, "38400",  "8N1" },
    { B57600,  PaknetModem::MODE8N1, "57600",  "8N1" },
    { B115200, PaknetModem::MODE8N1, "115200", "8N1" }
};

/*
 * Baud Rates to Paknet Parameter 11 (Data Rate) values
 */
static BaudSetting baud_settings[] = {
    { B300,     2 },
    { B1200,    3 },
    { B600,     4 },
    { B150,     6 },
    { B2400,   12 },
    { B4800,   13 },
    { B9600,   14 },
    { B19200,  15 },
    { B38400,  16 },
    { B57600,  17 },
    { B115200, 18 },
    { -1     , -1 }  /* End marker */
};

/*
 * Initialisation format string for Paknet PAD
 * Please note that:
 *      parameter 11 is baud rate (by paknet index number)
 *      parameter 21 is framing cfg (being 0 for 7E1 and 2 for 8N1)
 */
static const char initFormatStr[] = "SET 0:0, 1:0, 2:0, 3:0, 4:2, 5:0, 6:5, 11:%d, 12:0, 13:0, 15:0, 21:%d\r";


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

PaknetModem::PaknetModem(const lu_int32_t commsPortFD, const DialupModemCfg& conf) :
                                 log(Logger::getLogger(SUBSYSTEM_ID_COMMS_DEVICE)),
                                 num_rate_modes(sizeof(rate_modes)/sizeof(RateMode)),
                                 MAXTRIES(3),
                                 m_config(conf),
                                 m_fileDesc(commsPortFD),
                                 m_rateModeIdx(0),
                                 m_changedRate(true),
                                 m_tries(0),
                                 m_triedRates(0)
{
    /* Initialise rate mode to the first similar to the config before scanning */
    for(lu_uint32_t i = 0; i < num_rate_modes; ++i)
    {
        if(rate_modes[i].rate == conf.port.serialConf.serBaudrate)
        {
            //It is easier to find the first baud rate match and try from there
            m_rateModeIdx = i;  //1st baud speed match
            break;
        }
    }
}

PaknetModem::~PaknetModem()
{}

lu_int32_t PaknetModem::init()
{
    lu_int32_t ret = PAKNET_INIT_RETRY;
    const char* rate_name;
    const char* mode_name;
    char initString[MAX_INIT_STRING_SIZE];
    int rate;
    ModeDefs mode;
    bool connected = false;
    bool newRate = false;

    if(m_fileDesc < 0)
    {
        log.error("%s Invalid file descriptor (%i) for port %s",
                    __AT__, m_fileDesc, m_config.name.c_str());
        return m_fileDesc;
    }

    rate = rate_modes[m_rateModeIdx].rate;
    mode = rate_modes[m_rateModeIdx].mode;
    rate_name = rate_modes[m_rateModeIdx].rate_name;
    mode_name = rate_modes[m_rateModeIdx].mode_name;

    if(m_changedRate)
    {
        log.debug("%s Try %d/%d: %s@%s (rate %d/%d)",
                    __AT__,
                    m_tries+1, PaknetModem::MAXTRIES,
                    mode_name, rate_name,
                    m_triedRates+1, num_rate_modes);
        DBG_INFO("%s Try %d/%d: %s@%s (rate %d/%d) index %i",
                    __AT__, m_tries+1, PaknetModem::MAXTRIES,
                    mode_name, rate_name,
                    m_triedRates+1, num_rate_modes,
                    m_rateModeIdx);

        set_baud_and_parity(rate, mode);
    }


    switch(try_attention(PAKNET_ATTENTION, PAKNET_OK))
    {
        case 0: //OK
        {
            m_changedRate = false;

            //Get the Paknet config from the Device Config

            log.info("%s Connected at %s %s - sending init string",__AT__,rate_name,mode_name);
            DBG_INFO("%s Connected at %s %s - sending init string",__AT__,rate_name,mode_name);

            /* Build the Paknet modem initialisation string */
            bool isDataBits7 = (m_config.port.serialConf.serDataBits == LIN232_DATA_BITS_7);
            int framing = (isDataBits7)? 0 : 2;
            ModeDefs setMode = (isDataBits7)? MODE7E1 : MODE8N1;
            lu_uint32_t setBaud = findBaudSetting();
            snprintf(initString, MAX_INIT_STRING_SIZE, initFormatStr, setBaud, framing);
            send_init(initString);

            /* Set the baud rate of the port to the new rate to match the modem */
            set_baud_and_parity(m_config.port.serialConf.serBaudrate, setMode);

            connected = true;
        }
            break;

        case 2: //Read timed out
            return ret;
            break;

        case -1: //Read failed
        case 3: //Write timed out
            m_tries++;
            if(m_tries >= PaknetModem::MAXTRIES)
            {
                newRate = true;
            }
        default:

            break;
    }

    if(connected)
    {
        // now connected OK
        log.info("%s Got attention at desired rate/mode", __AT__);
        DBG_INFO("%s Got attention at desired rate/mode", __AT__);

        ret = PAKNET_INIT_OK;
    }

    if(newRate)
    {
        log.info("%s Failed to connect at %s %s - tried mode %i of %i",
                        __AT__, rate_name, mode_name, m_triedRates+1, num_rate_modes);
        DBG_INFO("%s Failed to connect at %s %s - tried mode %i of %i",
                        __AT__, rate_name, mode_name, m_triedRates+1, num_rate_modes);

        m_changedRate = true;
        m_rateModeIdx = (m_rateModeIdx + 1) % num_rate_modes;    //Try another rate mode next time
        if((m_triedRates+1) >= num_rate_modes)
        {
            log.error("%s Failed to get attention at any rate/mode in table",__AT__);
            ret = PAKNET_INIT_FAIL;
        }
        m_triedRates = (m_triedRates + 1) % num_rate_modes;
        m_tries = 0;
    }

    return ret;

}
/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
int PaknetModem::try_attention(const char* attn_str, const char* ok_str)
{
    const lu_uint64_t ReadTimeout_ms = 50;   //Time in ms for trying to read a reply from the write
    const lu_uint64_t WriteTimeout_ms = 100; //Time in ms for issue another write

    int ret;
    int attn_len = strlen(attn_str);
    int ok_len = strlen(ok_str);
    char buffer[255];   /* Input buffer */
    char *bufptr;       /* Current char in buffer */
    int nbytes;      /* Number of bytes read */
    timespec initial;
    timespec final;
    lu_uint64_t elapsed;

    if(m_changedRate)
    {
//        DBG_INFO("XXXXXX Rate changed to index %i", m_rateModeIdx);
        m_changedRate = false;
        fileIgnore(m_fileDesc); /* Flush read buffer */
        ret = write(m_fileDesc, attn_str, attn_len); /* send an attention command */
        if(ret < 1)
        {
            //Write failed
            DBG_INFO("%s - Write failed!!!", __AT__);
            return -1;
        }
        m_elapsedWrite_ms = 0;
    }
    clock_gettime(CLOCK_MONOTONIC, &initial);
    FDVector fds;
    FDVector result;
    fds.push_back(m_fileDesc);
    timeval timeout = ms_to_timeval(ReadTimeout_ms);
    do
    {
//        DBG_INFO("Listening for %llu ms", timeval_to_ms(&timeout));
        if(fileSelect(timeout, fds, result) > 0)
        {
            /* read characters into our string buffer until we get a NL */
            bufptr = buffer;
            while ((nbytes = read(m_fileDesc, bufptr, buffer + sizeof(buffer) - bufptr - 1)) > 0)
            {
                bufptr += nbytes;
                if (*(bufptr-1) == '\n')
                {
                    //Last char received was a \n
                    break;  //All the line has been received
                }
            }

            /* Add null termination to the C string and see if we got an OK response */
            *bufptr = '\0';

            if (strncmp(buffer, ok_str, ok_len) == 0)
            {
                DBG_INFO("%s Got OK string",__AT__);

                fileIgnore(m_fileDesc); /* Flush read buffer */
                return 0;   //OK
            }

            return -1;   //Response failed
        }
        clock_gettime(CLOCK_MONOTONIC, &final);
        elapsed = timespec_elapsed_ms(&initial, &final);
        m_elapsedWrite_ms += elapsed;

        if(m_elapsedWrite_ms > WriteTimeout_ms)
        {
//            DBG_INFO("############### WRITE Timed Out after %llu/%llu ms", m_elapsedWrite_ms, WriteTimeout_ms);

            ret = write(m_fileDesc, attn_str, attn_len); /* send an attention command */
            if(ret < 1)
            {
                //Write failed
                DBG_INFO("%s - Write failed!!!", __AT__);
                return -1;
            }
            m_elapsedWrite_ms = 0;
            return 3;   //Write timed out
        }

    } while(elapsed < ReadTimeout_ms);

//    DBG_INFO("############### READ Timed Out after %llu/%llu ms", elapsed, ReadTimeout_ms);
    return 2;   //Read timed out
}


void PaknetModem::set_baud_and_parity(const int baud, const ModeDefs mode)
{
    //DBG_INFO("%s set_baud_and_parity",TITLE);

    struct termios options;

//  tcgetattr(m_fileDesc, &options);

    memset(&options, 0, sizeof(options));

    cfsetispeed(&options,baud);
    cfsetospeed(&options,baud);

    if (mode == MODE7E1)
    {
        DBG_INFO("%s Setting 7E1 (@%d)", __AT__, mode);

        options.c_cflag |= PARENB;
        options.c_cflag &= ~PARODD;
        options.c_cflag &= ~CSTOPB;
        options.c_cflag &= ~CSIZE;
        options.c_cflag |= CS7;
        options.c_iflag |= (INPCK | ISTRIP);

    }
    else if (mode == MODE8N1)
    {
        DBG_INFO("%s Setting 8N1 (@%d)", __AT__, mode);

        options.c_cflag &= ~PARENB;
        options.c_cflag &= ~CSTOPB;
        options.c_cflag &= ~CSIZE;
        options.c_cflag |= CS8;
    }

    options.c_cflag |= CRTSCTS;         // Hardware flow

    options.c_cc[VTIME] = 10;           //VTIME is the time to wait for chars to be available to read
                                        //It is a byte in 0.1 secs interval (255 = 25.5 secs)

    tcsetattr(m_fileDesc, TCSANOW, &options);
//    tcsetattr(m_fileDesc, TCSAFLUSH, &options);

    /* Discard any data in IO buffers */
    //tcflush(m_fileDesc, TCIOFLUSH);
}


int PaknetModem::findBaudSetting()
{
    int i;
    int setBaud = 14; /* Default to 9600 */

    for (i = 0; baud_settings[i].rate > 0; i++)
    {
        if (baud_settings[i].rate == m_config.port.serialConf.serBaudrate)
        {
            setBaud = baud_settings[i].setting;
            break;
        }
    }
    return setBaud;
}


void PaknetModem::send_init(const char *init_str)
{
    Logger& log = Logger::getLogger(SUBSYSTEM_ID_COMMS_DEVICE);

    int len;
    char buffer[255];   /* Input buffer */
    int nbytes;

    // flush input
    fileIgnore(m_fileDesc);

    log.info("Sending init string [%s]", init_str);
    DBG_INFO("Sending init string [%s]", init_str);

    len = strlen(init_str);
    write(m_fileDesc, init_str, len);

    buffer[0] = '\0';

    /* TODO: pueyos_a - do select before read */
    lucy_usleep(100000);

    // read response
    nbytes = read(m_fileDesc, buffer, sizeof(buffer));

    log.debug("init string response [%d: %s]\n", nbytes, buffer);
}


/*
 *********************** End of file ******************************************
 */
