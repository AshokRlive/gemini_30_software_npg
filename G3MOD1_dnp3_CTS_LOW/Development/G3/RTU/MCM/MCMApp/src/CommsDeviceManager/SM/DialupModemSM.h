/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:DialupModemSM.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   22 Aug 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef DIALUPMODEMSM_H_
#define DIALUPMODEMSM_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "Mutex.h"
#include "Logger.h"
#include "CommsTrafficSniffer.h"
#include "PSMIOModule.h"
#include "SMTimer.h"
#include "PaknetModem.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define TIMEOUT_DROP_DTR                3000
#define TIMEOUT_RAISE_DTR               3000
#define TIMEOUT_POWER_CYCLE             5000
#define TIMEOUT_CHECK_MODEM_RESPONSE    2000
#define RX_BUFF_SIZE                    100

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
class DialupModem_fsm; // Forward declaration
class DialupModem;

class IDialupModemSM
{
public:
    enum INIT_STATE
    {
        INIT_OK = 0,
        INIT_FAILURE,
        INIT_RETRY,
        INIT_LAST
    };

    enum PING_STATE
    {
        PING_OK = 0 ,
        PING_FAILURE,
        PING_INCOMING_CALL,
        PING_LAST
    };

    enum CONNECT_STATE
    {
        CONNECTED = 0  ,
        DISCONNECTED
    };

    enum DIAL_STATE
    {
        DIAL_STATE_DIALING = 0,
        DIAL_STATE_WAIT_ANSWER,
        DIAL_STATE_WAIT_RETRY,
        DIAL_STATE_FINISHED,
        DIAL_STATE_LAST
    };

    enum TIMER
    {
        TIMER_WAIT_RETRY = 0,
        TIMER_DROP_DTR,
        TIMER_RAISE_DTR,
        TIMER_WAIT_ANSWER,
        TIMER_POWER_CYCLE,
        TIMER_MODEM_INACTIVITY,
        TIMER_CONNECT_INACTIVITY,
        TIMER_REGULAR_CALL,
        TIMER_CHECK_MODEM_RESPONSE,
        TIMER_LAST
    };


    IDialupModemSM(){}
    virtual ~IDialupModemSM(){}

    /************************************/
    /* State Machine Operation API        */
    /************************************/
    virtual void enterStartState()                  = 0;
    virtual bool connect()                          = 0;
    virtual void disconnect()                       = 0;
    virtual void tickEvent(lu_uint32_t ticks)       = 0;
    virtual void checkTimer()                       = 0;

};

class DialupModemSM:public IDialupModemSM
{
public:
    DialupModemSM(DialupModem& modem);
    virtual ~DialupModemSM();

    const char* name();

    /************************************/
    /* State Machine Operation API       */
    /************************************/
    virtual void enterStartState()            ;
    virtual bool connect()                    ; // Request connection
    virtual void disconnect()                 ;
    virtual void tickEvent(lu_uint32_t ticks) ;
    virtual void checkTimer()                 ;

    /************************************/
    /* State Machine Actions API        */
    /************************************/
    void            initialise();
    INIT_STATE      getInitState(){ return m_initState;}
    void            resetInitState(){ m_initState = INIT_LAST;}

    void setPowerOn();
    void setPowerOff();

    void raiseDTR();
    void dropDTR();

    void startTimer (TIMER timerID);
    void stopTimer  (TIMER timerID);
    void resetTimer (TIMER timerID);

    void increaseRetries()      {m_connRetries++;}
    bool moreRetries();

    void resetConnRetries()     {m_connRetries = 0;}
    void resetDialStrIndex()    {m_dialStrIndex = 0;}
    void resetPingState();

    void            dial();
    void            setDialState(DIAL_STATE newstate){this->m_dialState = newstate;}
    DIAL_STATE      getDialState()                   {return m_dialState;          }

    void            ping();
    void            checkModemResponse();
    PING_STATE      getPingState(){ return m_pingState;}

    void            checkForAnswer();

    void            setFlagConnRequested(bool value){ m_connRequested = value;}

    void            notifyStateChange(CONNECT_STATE newState);

private:
    void startTimer(TIMER timerID, lu_uint32_t timeoutMs);

    lu_uint32_t getRetryDelayMS();

    std::string getDialString(lu_uint32_t index);

private:
    Logger&             log;

    DialupModem_fsm*    mp_fsm;
    Mutex               m_fsmOpMutex; // FSM operation Mutex

    DialupModem&        m_owner;

    bool                m_connRequested;
    lu_uint32_t         m_connRetries;
    lu_uint32_t         m_pingFails;
    lu_uint32_t         m_dialStrIndex;
    lu_uint32_t         m_lastPingTicks;//TODO change to TIMER

    INIT_STATE          m_initState;
    PING_STATE          m_pingState;
    DIAL_STATE          m_dialState;

    lu_int32_t          m_portFD;

    SMTimer             m_timers[TIMER_LAST];

    /* TODO: pueyos_a - DialupModem should be independent of the Paknet modem */
    PaknetModem*        m_paknetModem;  //Object for Paknet Modem initialisation

    const static lu_uint32_t PING_INTERVAL_TICKS = 200; // Ping every 200 ticks
};

#endif /* DIALUPMODEMSM_H_ */

/*
 *********************** End of file ******************************************
 */
