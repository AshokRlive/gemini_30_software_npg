/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:CommsTrafficSniffer.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   21 Aug 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef COMMSTRAFFICSNIFFER_H_
#define COMMSTRAFFICSNIFFER_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <string.h>  // strstr()

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "SMTimer.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
// Define return codes for sniffRcvData()
enum SNIFF_STATE
{
    SNIFF_STATE_NEW_CONNECTION = 0, // Connection detected
    SNIFF_STATE_CLR,                // Failed to connect
    SNIFF_STATE_PING_OK,            // A 'ping' response detected
    SNIFF_STATE_PROTOCOL_DATA,      // Protocol data detected - ignore
    SNIFF_STATE_ERROR,              // An error occurred
    SNIFF_STATE_LAST
};

#define SNIFFBUFFSIZE   1024
/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

// Structure to hold the the different state strings
struct StateStringStr
{
    SNIFF_STATE state;
    const char* stateString;

};



/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
class DialupModem; // Forward declaration

class CommsTrafficSniffer
{
public:
    CommsTrafficSniffer();

    virtual ~CommsTrafficSniffer();

    /**
     * \brief Search the supplied buffer for MODEM commands
     *
     * \param dataPtr  Pointer to data buffer
     * \param len      Length of supplied data buffer
     *
     * \return State
     */
    SNIFF_STATE sniffBuffer(const lu_int8_t* dataPtr, lu_uint32_t len);

    /**
     * \brief Search the supplied buffer for MODEM commands
     *
     * \detail If the supplied buffer has no commands in it, append
     *         the data to sniffbuff[] and sniff again
     *
     * \param dataPtr  Pointer to data buffer
     * \param len      Length of supplied data buffer
     *
     * \return State
     */
    SNIFF_STATE sniffRcvData(const lu_int8_t* dataPtr, lu_uint32_t len);

    timespec& getLastRcvTS(){ return m_lastRcvTS; };

    void setRcvDataTimer(SMTimer* smtimer);

private:
    struct timespec m_lastRcvTS;//Time stamp of last time sniffing received data.


    lu_char_t sniffBuff[SNIFFBUFFSIZE + 1];
    lu_uint8_t sniffBuffPos;

    SMTimer* mp_rcvDataTimer;


    //const static lu_uint32_t RCV_DATA_TIMEOUT_MS = 30000;
};

#endif /* COMMSTRAFFICSNIFFER_H_ */

/*
 *********************** End of file ******************************************
 */
