/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:CommsDeviceManager.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   21 Aug 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "CommsDeviceManager.h"
#include "CommsDeviceDebug.h"
#include "GlobalDefs.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
CommsDeviceManager::CommsDeviceManager():
       Thread(SCHED_TYPE_FIFO, COMMSDEV_MANAGER_THREAD, Thread::TYPE_DETACHED, "CommsDevManagerTh"),
       log(Logger::getLogger(SUBSYSTEM_ID_COMMS_DEVICE))

{

}

CommsDeviceManager::~CommsDeviceManager()
{
    stopDeviceManager();

    /* Delete comms devices */
    for (std::vector<CommsDevice*>::iterator it = commsDevices.begin();
                  it != commsDevices.end();
                  ++it
       )
    {
      delete (*it);
    }
}

lu_int32_t CommsDeviceManager::addCommsDevice(CommsDevice *commsDevicePtr)
{
    if (commsDevicePtr == NULL)
    {
        return -1;
    }

    commsDevices.push_back(commsDevicePtr);

    return 0;
}

void CommsDeviceManager::startDeviceManger()
{
    Thread::start();
}

void CommsDeviceManager::stopDeviceManager()
{
    Thread::stop();
}

DialupModem* CommsDeviceManager::getDialupModem(lu_uint32_t deviceId)
{
    // Loop over commsDevices to find deviceID
    for (CommsDeviceVector::iterator itModem = commsDevices.begin();
                                     itModem != commsDevices.end();
                                     ++itModem)
    {
        if ((*itModem)->deviceID == deviceId)
        {
            return dynamic_cast<DialupModem *>(*itModem);
        }
    }

    return NULL;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void CommsDeviceManager::threadBody()
{
    DBG_INFO("Enter CommsDeviceManager thread body");
    DBG_INFO("Comms Device Num: %i",commsDevices.size());
    
    /* Initialise comms devices */
    for (std::vector<CommsDevice*>::iterator it = commsDevices.begin();
                    it != commsDevices.end();
                    ++it
         )
    {
        (*it)->init();
    }


    lu_uint32_t ticks = 0;

    while(1)
    {

        //TODO mutext required?

        /* Generate tick event */
        for(std::vector<CommsDevice*>::iterator it = commsDevices.begin();
                        it!= commsDevices.end();
                        ++it
            )
        {
            (*it)->checkTimer();


            (*it)->tickEvent(ticks); // TODO review ticks generator
        }

        lucy_usleep(100000); // sleep 100 ms
        ticks++;
    }
}

/*
 *********************** End of file ******************************************
 */
