/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ModemCommon.h 25 Nov 2015 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/CommsDeviceManager/include/ModemCommon.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 25 Nov 2015 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25 Nov 2015 pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef MODEMCOMMON_H__INCLUDED
#define MODEMCOMMON_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <string>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "Port.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
/**
 * \brief Common dialup modem configuration
 */
struct DialupModemCfg
{
public:
    const static lu_uint32_t RETRY_INTERVAL_NUM = 10;

public:
    std::string name;               // Comms Device Name
    lu_uint32_t id;                 // Unique CommsDevice ID

    Port&       port;               // Reference to port the MODEM connected to

    std::string initString;         // MODEM initialisation String
    std::string dialString1;        // Primary dial String
    std::string dialString2;        // Backup dial String
    std::string connectString;      // String returned by MODEM on connect
    std::string hangupString;       // String used to hangup MODEM
    std::string promptString;       // Standard MODEM prompt (e.g. 'OK')

    lu_uint32_t connectTimeoutMs;   // Connection Timeout in milliseconds
    lu_uint32_t regularCallIntervalMins;
    lu_uint32_t modemInactivityTimeoutMins;
    lu_uint32_t connectionInactivityTimeoutSecs;
    lu_uint32_t dialRetryIntervalSecs[RETRY_INTERVAL_NUM];

public:
    DialupModemCfg(Port& x) : port(x)
    {
        // Set retry intervals to 0
        memset(dialRetryIntervalSecs, 0, sizeof(lu_uint32_t)*RETRY_INTERVAL_NUM);
    };
};

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */


#endif /* MODEMCOMMON_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
