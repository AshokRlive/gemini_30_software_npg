/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: User.cpp 15-Aug-2013 16:12:47 pueyos_a $
 *               $HeadURL: \\10.11.0.7\pueyos_a\workspace\app_refactor\G3\RTU\MCM\MCMApp\src\..\..\common\UserManager\src\User.cpp $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       User header module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 15-Aug-2013 16:12:47	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   15-Aug-2013 16:12:47  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_10F11414_F693_463c_AEE6_CCE5653ECAFA__INCLUDED_)
#define EA_10F11414_F693_463c_AEE6_CCE5653ECAFA__INCLUDED_



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <string>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "G3ConfigProtocol.h"   //USER_LEVEL definition
#include "CTHMessage.h"
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * Class User
 */
class User
{
public:
    /* Hash size to use */
    static const lu_uint32_t HASH_BUF_SIZE = CTMsgPL_C_UserLogin::HASH_BUF_SIZE;

    struct PasswordHashArray
    {
        lu_uint8_t hash[HASH_BUF_SIZE]; //type for MD5 array
        lu_uint8_t len;
    };

    enum USERDATA_ERROR
    {
        USERDATA_ERROR_NONE,
        USERDATA_ERROR_NOUSER = -1,
        USERDATA_ERROR_NOPASSWORD = -2,
        USERDATA_ERROR_LAST
    };

public:
    /**
     * \brief Custom constructor
     *
     * \param name User name
     * \param password User password in hash format
     * \param userLevel User access level
     *
     * \return
     */
    User(std::string userName, std::string password, USER_LEVEL userLevel, lu_uint32_t _loginAttemptThreshold);
	//User(std::string userName, PasswordHashArray password, USER_LEVEL userLevel, lu_uint32_t _loginAttemptThreshold);
	virtual ~User();

    /**
     * \brief Returns the user name.
     *
     * \param name String where to return the user name.
     */
	std::string getName();

    /**
     * \brief Returns the user access level.
     *
     * \param access Where to return the user access level.
     */
	USER_LEVEL getAccessLevel();

	void setMDAlgorithm(std::string newMdAlgorithm)
	{
	    mdAlgorithm = newMdAlgorithm;
	}

	std::string getMDAlgorithm()
	{
	    return mdAlgorithm;
	}

    /**
     * \brief Compare a given hash codes with the one registered for this user.
     *
     * \param passwordHash Password in 128-bit (16-bytes) format.
     *
     * \return Error code if they doesn't match.
     */
    USERDATA_ERROR checkPassword(std::string passwordHash);
    USERDATA_ERROR checkPassword(PasswordHashArray passwordHash);

    lu_bool_t shouldBeLocked()
    {
    	return loginAttemptsCounter == loginAttemptsThreshold
    			&& loginAttemptsThreshold != 0;
    }

private:
	std::string name;
	PasswordHashArray password;
	USER_LEVEL accessLevel;
	std::string mdAlgorithm;
public:
	lu_uint32_t loginAttemptsCounter;
	lu_uint32_t loginAttemptsThreshold;
	lu_uint32_t* loginAttemptsTimestampArray;

private:
    /**
     * \brief Extract the hash code from an already-coded password string.
     *
     * This converts a coded password string to 128-bit format.
     *
     * \param source Password string to be converted.
     * \param result Converted password to 128-bit format.
     */
	void convertHash(const std::string source, PasswordHashArray& result);
};


#endif // !defined(EA_10F11414_F693_463c_AEE6_CCE5653ECAFA__INCLUDED_)

/*
 *********************** End of file ******************************************
 */

