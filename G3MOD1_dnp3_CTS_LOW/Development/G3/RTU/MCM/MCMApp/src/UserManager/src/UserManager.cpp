/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: UserManager.cpp 15-Aug-2013 16:12:51 pueyos_a $
 *               $HeadURL: \\10.11.0.7\pueyos_a\workspace\app_refactor\G3\RTU\MCM\MCMApp\src\..\..\common\UserManager\src\UserManager.cpp $
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       UserManager module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 15-Aug-2013 16:12:51	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   15-Aug-2013 16:12:51  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "UserManager.h"
#include "Logger.h"
#include "LogLevelDef.h"
#include "TimeManager.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static void clearAttemptRecords(User& user);
static void addAttemptRecord(User& user, lu_uint32_t curTimeSecs);
static lu_bool_t checkAttemptedTooManyTimes(User& user,
		lu_uint32_t curTimeSecs, const lu_uint32_t lockoutDurationSecs);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

UserManager::UserManager()
{
    userList.clear();
    accountLockoutDurationSecs = 300;
    invalidLogonAttemptThreshold = 3;
}


UserManager::~UserManager()
{}


UserManager::USERLIST_ERROR UserManager::userAdd(User userData)
{
    /* check valid parameters */
    if(userData.getAccessLevel() >= USER_LEVEL_LAST)
    {
        return USERLIST_ERROR_ACCESS_LEVEL;
    }
    /* check for duplicate entries */
    for (lu_uint32_t index = 0; index < userList.size(); ++index)
    {
        if( userList[index].getName() == userData.getName() )
        {
            return USERLIST_ERROR_ALREADY_EXISTS;
        }
    }
    //push_back is guaranteed to end in a stable state
    userList.push_back(userData);
    return USERLIST_ERROR_NONE;
}

UserManager::USERLIST_ERROR UserManager::userLogin(std::string userName, std::string password, USER_LEVEL *userLevel)
{
	static Logger& log = Logger::getLogger(SUBSYSTEM_ID_CT_HANDLER);

    lu_uint32_t listSize = userList.size();
    *userLevel = USER_LEVEL_OPERATOR;   //lowest level by default (should not be used when resulted in error
    if(listSize == 0)
    {
        *userLevel = USER_LEVEL_ADMIN;  //If there is no user in the list, give max access level!
        return USERLIST_ERROR_NONE;
    }

    TimeManager::TimeStr curTime;
    TimeManager::getInstance().getTime(curTime);

    for (lu_uint32_t index = 0; index < listSize; ++index)
    {
        if(userList[index].getName() == userName)
        {
        	if(checkAttemptedTooManyTimes(userList[index],curTime.relatime.tv_sec, getAccountLockoutDurationSecs()))
			  {
        		log.warn("Login failed too many times from user:%s [%d times]",
        				userName.c_str(), userList[index].loginAttemptsCounter);
				return USERLIST_ERROR_TOO_MANY_ATTEMPTS;
			  }


            if(userList[index].checkPassword(password) == User::USERDATA_ERROR_NONE)
            {
                *userLevel = userList[index].getAccessLevel();
                /* TODO: pueyos_a - log user interaction in a separate logger that always logs */
                log.warn("user %s logged in", userName.c_str());
            	clearAttemptRecords(userList[index]);
                return USERLIST_ERROR_NONE;
            }
            else
            {
            	addAttemptRecord(userList[index],curTime.relatime.tv_sec);
            	log.warn("Login attempted from user:%s [%d times]", userName.c_str(), userList[index].loginAttemptsCounter);
            	if(userList[index].loginAttemptsCounter == userList[index].loginAttemptsThreshold)
				{
					log.warn("User \"%s\" has been locked for %d seconds",
							userName.c_str(),accountLockoutDurationSecs);
				}
                return USERLIST_ERROR_NOLOGIN;
            }
        }
    }

    log.warn("Login user name not found: %s.",userName.c_str());
    return USERLIST_ERROR_NOTFOUND;
}

std::string UserManager::getMDAlgorithm(std::string userName)
{
    lu_uint32_t listSize = userList.size();
    for (lu_uint32_t index = 0; index < listSize; ++index)
        {
            if(userList[index].getName() == userName)
            {
                return userList[index].getMDAlgorithm();
            }
        }

    return "";
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
static TimeManager::TimeStr curtime;

static void clearAttemptRecords(User& user)
{

	if(user.loginAttemptsTimestampArray != NULL)
	{
		memset(user.loginAttemptsTimestampArray, 0, sizeof(lu_uint32_t)*(user.loginAttemptsThreshold));
		user.loginAttemptsCounter = 0;
	}
}

static void addAttemptRecord(User& user, lu_uint32_t curTimeSecs)
{

	if(user.loginAttemptsCounter >= user.loginAttemptsThreshold)
	{
		return;
	}

	lu_uint32_t i;
	for (i = 0; i < user.loginAttemptsThreshold; ++i)
	{
		if(user.loginAttemptsTimestampArray[i] == 0)
		{
			user.loginAttemptsTimestampArray[i] = curTimeSecs;
			user.loginAttemptsCounter ++;
			break;
		}
	}
}

static lu_bool_t checkAttemptedTooManyTimes(User& user, lu_uint32_t curTimeSecs, const lu_uint32_t lockoutDurationSecs)
{
	// Init timestamp array
	if(user.loginAttemptsTimestampArray == NULL)
	{
		user.loginAttemptsTimestampArray = new lu_uint32_t[user.loginAttemptsThreshold];
		clearAttemptRecords(user);
	}

	// TODO Clear attempts that are too old (e.g. time that is older than 1 day).


	// Currently locked
	if(user.shouldBeLocked())
	{

		// Find the timestamp of the latest attempt.
		lu_uint32_t i;
		lu_uint32_t latest = 0;
		for (i = 0; i < user.loginAttemptsThreshold; ++i)
		{
			if(user.loginAttemptsTimestampArray[i] > latest)
				latest = user.loginAttemptsTimestampArray[i];
		}

		// Check if it is time to unlock user
		if((curTimeSecs - latest) > lockoutDurationSecs)
		{
			clearAttemptRecords(user);
        	Logger::getLogger(SUBSYSTEM_ID_CT_HANDLER)
        			.warn("The user has been unlocked: %s",user.getName().c_str());

		}
	}

	return user.shouldBeLocked();
}


/*
 *********************** End of file ******************************************
 */

