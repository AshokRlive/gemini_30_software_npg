/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: UserManager.cpp 15-Aug-2013 16:12:51 pueyos_a $
 *               $HeadURL: \\10.11.0.7\pueyos_a\workspace\app_refactor\G3\RTU\MCM\MCMApp\src\..\..\common\UserManager\src\UserManager.cpp $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       UserManager header module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 15-Aug-2013 16:12:51	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   15-Aug-2013 16:12:51  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_2999C3AD_B810_48db_9AB8_587A7493347B__INCLUDED_)
#define EA_2999C3AD_B810_48db_9AB8_587A7493347B__INCLUDED_



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <vector>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "User.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * Class UserManager
 *
 * This class manages a list of users and checks the validity and access level
 * of any user.
 */
class UserManager
{
public:
    /**
     * \brief User list error codes.
     */
    enum USERLIST_ERROR
    {
        USERLIST_ERROR_NONE,
        USERLIST_ERROR_NOTFOUND,       //User not found
        USERLIST_ERROR_NOLOGIN,        //Invalid password
        USERLIST_ERROR_ALREADY_EXISTS, //adding a user name that already exists
        USERLIST_ERROR_ACCESS_LEVEL,   //unsupported access level
		USERLIST_ERROR_TOO_MANY_ATTEMPTS,
        USERLIST_ERROR_LAST
    };

	UserManager();
	virtual ~UserManager();

	/**
	 * \brief Add a user to the user list.
	 *
	 * \param userData User-specific data.
	 */
	UserManager::USERLIST_ERROR userAdd(User userData);

    /**
     * \brief Check if a given user is valid and therefore can log in.
     *
     * Checks if the given user is present in the list of users and matches the
     * provided password with the registered one. If it does not match, an
     * error is returned.
     * CAUTION: If the list of users is empty, ANY user/password get access
     *
     * \param userName Name of the user.
     * \param password Password provided by the user.
     * \param userLevel Returns here the access level granted for this user.
     *
     * \return Success/error code.
     */
	USERLIST_ERROR userLogin(std::string userName,
                            std::string password,
                            USER_LEVEL *userLevel
                            );

	std::string getMDAlgorithm(std::string username);

	void setAccountLockoutDurationSecs(lu_uint32_t _accountLockoutDurationSecs)
	{
		this->accountLockoutDurationSecs = _accountLockoutDurationSecs;
	}

	void setInvalidLogonAttemptThreshold(lu_uint32_t _invalidLogonAttemptThreshold)
	{
		this->invalidLogonAttemptThreshold = _invalidLogonAttemptThreshold;
	}

	lu_uint32_t getAccountLockoutDurationSecs(){return accountLockoutDurationSecs;}
	lu_uint32_t getInvalidLogonAttemptThreshold(){return invalidLogonAttemptThreshold;}

private:
	std::vector<User> userList;
	lu_uint32_t accountLockoutDurationSecs;
	lu_uint32_t invalidLogonAttemptThreshold;
};


#endif // !defined(EA_2999C3AD_B810_48db_9AB8_587A7493347B__INCLUDED_)

/*
 *********************** End of file ******************************************
 */

