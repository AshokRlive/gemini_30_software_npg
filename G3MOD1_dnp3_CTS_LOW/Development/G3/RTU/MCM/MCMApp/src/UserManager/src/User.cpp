/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: User.cpp 15-Aug-2013 16:12:47 pueyos_a $
 *               $HeadURL: \\10.11.0.7\pueyos_a\workspace\app_refactor\G3\RTU\MCM\MCMApp\src\..\..\common\UserManager\src\User.cpp $
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       User module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 15-Aug-2013 16:12:47	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   15-Aug-2013 16:12:47  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdio.h>  //use of sscanf()

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "User.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

//User::User(std::string userName, PasswordHashArray userPassword, USER_LEVEL userLevel) :
//    name(userName),
//    password(userPassword), //  To fix: password not deep copied
//    accessLevel(userLevel),
//    mdAlgorithm("")
//{}

User::User(std::string userName, std::string userPassword, USER_LEVEL userLevel, lu_uint32_t _loginAttemptThreshold) :
    name(userName),
    accessLevel(userLevel),
    mdAlgorithm(""),
	loginAttemptsCounter(0),
	loginAttemptsThreshold(_loginAttemptThreshold),
	loginAttemptsTimestampArray(NULL)
{
	if(loginAttemptsThreshold > 10)
		loginAttemptsThreshold = 10;
    convertHash(userPassword, password);
}

User::~User()
{
	delete []loginAttemptsTimestampArray;
	loginAttemptsTimestampArray = NULL;
}


std::string User::getName()
{
    return name;
}


USER_LEVEL User::getAccessLevel()
{
    return accessLevel;
}


User::USERDATA_ERROR User::checkPassword(std::string passwordHash)
{
    PasswordHashArray res;
    convertHash(passwordHash, res);
    return checkPassword(res);
}

User::USERDATA_ERROR User::checkPassword(PasswordHashArray passwordHash)
{
    if(passwordHash.len != this->password.len)
        return USERDATA_ERROR_NOPASSWORD;

    for(lu_uint32_t pos=0; pos < passwordHash.len; pos+=2)
    {
        if(passwordHash.hash[pos] != password.hash[pos])  // atoi(password.substr(pos*2, 2).c_str()) )
        {
            return USERDATA_ERROR_NOPASSWORD;
        }
    }
    return USERDATA_ERROR_NONE; //match found.
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void User::convertHash(const std::string source, PasswordHashArray& result)
{

//    if(source.size() > HASH_BUF_SIZE)
//        return;

    lu_uint32_t value;
    lu_uint32_t bytes = source.size()/2;
    for(lu_uint32_t pos=0; pos < HASH_BUF_SIZE && pos < bytes; pos++)
    {
        sscanf(source.substr(pos*2, 2).c_str(), "%x", &value);
        result.hash[pos] = value;
        result.len = bytes;
    }
}


/*
 *********************** End of file ******************************************
 */

