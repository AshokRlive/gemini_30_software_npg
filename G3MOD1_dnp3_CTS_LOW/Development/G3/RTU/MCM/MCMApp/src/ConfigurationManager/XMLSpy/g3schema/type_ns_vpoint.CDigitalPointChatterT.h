#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CDigitalPointChatterT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CDigitalPointChatterT



namespace g3schema
{

namespace ns_vpoint
{	

class CDigitalPointChatterT : public TypeBase
{
public:
	g3schema_EXPORT CDigitalPointChatterT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CDigitalPointChatterT(CDigitalPointChatterT const& init);
	void operator=(CDigitalPointChatterT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CDigitalPointChatterT); }

	MemberAttribute<unsigned __int64,_altova_mi_ns_vpoint_altova_CDigitalPointChatterT_altova_chatterNo, 0, 0> chatterNo;	// chatterNo CnonNegativeInteger

	MemberAttribute<unsigned __int64,_altova_mi_ns_vpoint_altova_CDigitalPointChatterT_altova_chatterTime, 0, 0> chatterTime;	// chatterTime CnonNegativeInteger
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CDigitalPointChatterT
