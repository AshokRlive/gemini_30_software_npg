#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CBaseDigitalPointT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CBaseDigitalPointT

#include "type_ns_vpoint.CVirtualPointT.h"


namespace g3schema
{

namespace ns_vpoint
{	

class CBaseDigitalPointT : public ::g3schema::ns_vpoint::CVirtualPointT
{
public:
	g3schema_EXPORT CBaseDigitalPointT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CBaseDigitalPointT(CBaseDigitalPointT const& init);
	void operator=(CBaseDigitalPointT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CBaseDigitalPointT); }
	MemberElement<ns_vpoint::CDigitalPointChatterT, _altova_mi_ns_vpoint_altova_CBaseDigitalPointT_altova_chatter> chatter;
	struct chatter { typedef Iterator<ns_vpoint::CDigitalPointChatterT> iterator; };
	MemberElement<ns_vpoint::CDelayedReportT, _altova_mi_ns_vpoint_altova_CBaseDigitalPointT_altova_delayedReport> delayedReport;
	struct delayedReport { typedef Iterator<ns_vpoint::CDelayedReportT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CBaseDigitalPointT
