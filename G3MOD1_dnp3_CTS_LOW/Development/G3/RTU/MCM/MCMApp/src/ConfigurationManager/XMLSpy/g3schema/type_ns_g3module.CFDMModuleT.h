#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CFDMModuleT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CFDMModuleT

#include "type_ns_g3module.CBaseModuleT.h"


namespace g3schema
{

namespace ns_g3module
{	

class CFDMModuleT : public ::g3schema::ns_g3module::CBaseModuleT
{
public:
	g3schema_EXPORT CFDMModuleT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CFDMModuleT(CFDMModuleT const& init);
	void operator=(CFDMModuleT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_g3module_altova_CFDMModuleT); }
	MemberElement<ns_g3module::CFDMChannelsT, _altova_mi_ns_g3module_altova_CFDMModuleT_altova_channels> channels;
	struct channels { typedef Iterator<ns_g3module::CFDMChannelsT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_g3module

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CFDMModuleT
