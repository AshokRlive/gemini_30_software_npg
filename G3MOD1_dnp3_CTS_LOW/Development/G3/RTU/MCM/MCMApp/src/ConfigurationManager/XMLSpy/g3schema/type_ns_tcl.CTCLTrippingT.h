#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_tcl_ALTOVA_CTCLTrippingT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_tcl_ALTOVA_CTCLTrippingT



namespace g3schema
{

namespace ns_tcl
{	

class CTCLTrippingT : public TypeBase
{
public:
	g3schema_EXPORT CTCLTrippingT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CTCLTrippingT(CTCLTrippingT const& init);
	void operator=(CTCLTrippingT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_tcl_altova_CTCLTrippingT); }

	MemberAttribute<bool,_altova_mi_ns_tcl_altova_CTCLTrippingT_altova_cbTripEnable, 0, 0> cbTripEnable;	// cbTripEnable Cboolean

	MemberAttribute<bool,_altova_mi_ns_tcl_altova_CTCLTrippingT_altova_commsFailMonitorEnable, 0, 0> commsFailMonitorEnable;	// commsFailMonitorEnable Cboolean

	MemberAttribute<unsigned,_altova_mi_ns_tcl_altova_CTCLTrippingT_altova_tripSignalTimer, 0, 0> tripSignalTimer;	// tripSignalTimer CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_tcl_altova_CTCLTrippingT_altova_commsFailTimer, 0, 0> commsFailTimer;	// commsFailTimer CunsignedInt

	MemberAttribute<int,_altova_mi_ns_tcl_altova_CTCLTrippingT_altova_cbCurrentThreshold, 0, 0> cbCurrentThreshold;	// cbCurrentThreshold Cint
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_tcl

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_tcl_ALTOVA_CTCLTrippingT
