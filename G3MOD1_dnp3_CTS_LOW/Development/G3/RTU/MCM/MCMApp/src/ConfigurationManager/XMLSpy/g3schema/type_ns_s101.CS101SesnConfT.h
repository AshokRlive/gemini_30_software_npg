#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s101_ALTOVA_CS101SesnConfT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s101_ALTOVA_CS101SesnConfT

#include "type_ns_iec870.CIEC870SesnConfT.h"


namespace g3schema
{

namespace ns_s101
{	

class CS101SesnConfT : public ::g3schema::ns_iec870::CIEC870SesnConfT
{
public:
	g3schema_EXPORT CS101SesnConfT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CS101SesnConfT(CS101SesnConfT const& init);
	void operator=(CS101SesnConfT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_s101_altova_CS101SesnConfT); }

	MemberAttribute<__int64,_altova_mi_ns_s101_altova_CS101SesnConfT_altova_maxPollDelay, 0, 0> maxPollDelay;	// maxPollDelay Clong

	MemberAttribute<int,_altova_mi_ns_s101_altova_CS101SesnConfT_altova_linkAddress, 0, 0> linkAddress;	// linkAddress Cshort

	MemberAttribute<int,_altova_mi_ns_s101_altova_CS101SesnConfT_altova_maxFrameSize, 0, 0> maxFrameSize;	// maxFrameSize Cshort

	MemberAttribute<int,_altova_mi_ns_s101_altova_CS101SesnConfT_altova_asduAddrSize, 0, 0> asduAddrSize;	// asduAddrSize Cshort

	MemberAttribute<int,_altova_mi_ns_s101_altova_CS101SesnConfT_altova_infoObjAddrSize, 0, 0> infoObjAddrSize;	// infoObjAddrSize Cshort

	MemberAttribute<unsigned,_altova_mi_ns_s101_altova_CS101SesnConfT_altova_cotSize, 0, 0> cotSize;	// cotSize CunsignedByte
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_s101

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s101_ALTOVA_CS101SesnConfT
