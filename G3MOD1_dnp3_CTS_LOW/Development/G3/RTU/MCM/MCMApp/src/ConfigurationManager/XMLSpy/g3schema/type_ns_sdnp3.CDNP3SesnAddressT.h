#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CDNP3SesnAddressT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CDNP3SesnAddressT



namespace g3schema
{

namespace ns_sdnp3
{	

class CDNP3SesnAddressT : public TypeBase
{
public:
	g3schema_EXPORT CDNP3SesnAddressT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CDNP3SesnAddressT(CDNP3SesnAddressT const& init);
	void operator=(CDNP3SesnAddressT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_sdnp3_altova_CDNP3SesnAddressT); }

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CDNP3SesnAddressT_altova_source, 0, 0> source;	// source CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CDNP3SesnAddressT_altova_destination, 0, 0> destination;	// destination CunsignedInt
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_sdnp3

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CDNP3SesnAddressT
