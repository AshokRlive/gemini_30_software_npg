#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CSwitchOutChnlT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CSwitchOutChnlT

#include "type_ns_g3module.CDigitalOutputChnlT.h"


namespace g3schema
{

namespace ns_g3module
{	

class CSwitchOutChnlT : public ::g3schema::ns_g3module::CDigitalOutputChnlT
{
public:
	g3schema_EXPORT CSwitchOutChnlT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSwitchOutChnlT(CSwitchOutChnlT const& init);
	void operator=(CSwitchOutChnlT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_g3module_altova_CSwitchOutChnlT); }

	MemberAttribute<unsigned,_altova_mi_ns_g3module_altova_CSwitchOutChnlT_altova_overrunMS, 0, 0> overrunMS;	// overrunMS CunsignedShort

	MemberAttribute<unsigned,_altova_mi_ns_g3module_altova_CSwitchOutChnlT_altova_opTimeoutS, 0, 0> opTimeoutS;	// opTimeoutS CunsignedShort

	MemberAttribute<unsigned,_altova_mi_ns_g3module_altova_CSwitchOutChnlT_altova_inhibit, 0, 0> inhibit;	// inhibit CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_g3module_altova_CSwitchOutChnlT_altova_polarity, 0, 0> polarity;	// polarity CunsignedByte

	MemberAttribute<unsigned,_altova_mi_ns_g3module_altova_CSwitchOutChnlT_altova_motorOverDriveMS, 0, 0> motorOverDriveMS;	// motorOverDriveMS CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_g3module_altova_CSwitchOutChnlT_altova_motorReverseDriveMS, 0, 0> motorReverseDriveMS;	// motorReverseDriveMS CunsignedInt
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_g3module

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CSwitchOutChnlT
