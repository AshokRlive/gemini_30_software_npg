#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CDefaultDebounceT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CDefaultDebounceT



namespace g3schema
{

namespace ns_vpoint
{	

class CDefaultDebounceT : public TypeBase
{
public:
	g3schema_EXPORT CDefaultDebounceT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CDefaultDebounceT(CDefaultDebounceT const& init);
	void operator=(CDefaultDebounceT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CDefaultDebounceT); }

	MemberAttribute<unsigned __int64,_altova_mi_ns_vpoint_altova_CDefaultDebounceT_altova_debounce, 0, 0> debounce;	// debounce CnonNegativeInteger
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CDefaultDebounceT
