/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:XMLParser.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       Implements functions for accessing XML point-specific configurations.
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   9 Jul 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef XMLPARSER_POINTS_H_
#define XMLPARSER_POINTS_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <limits>   //For max_float32 value (FLOAT_ULTRAMAX)
#include <typeinfo>
#include <algorithm>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "XMLParser.h"

#include "ConfigurationManagerError.h"
#include "IPoint.h"
#include "DigitalPoint.h"
#include "AnaloguePoint.h"
#include "CounterPoint.h"
#include "DigitalChangeEvent.h"
#include "LabelPool.h"

using namespace ns_common;
using namespace ns_vpoint;
using namespace ns_clogic;

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 * In this file you could found parsing functions mainly for:
 * - Obtain a Point ID related with a configuration element
 * - Obtain a Channel reference
 * - Configure a Digital Change Event
 * - General configuration of a Virtual Point (Analogue, Digital, and Counter)
 */

/**
 * NOTE:
 * The following functions are for extracting Channel and Point-specific
 * configurations from XML fields.
 */
static inline ChannelRef getXMLChannelConf(const CChannelRefT& xmlElem) throw(ParserException);
static inline PointIdStr getXMLPointIDConf(const CVirtualPointT& xmlElem) throw(ParserException);
static inline PointIdStr getXMLVirtualPointRef(const CVirtualPointRefT& xmlElem) throw(ParserException);
static inline PointIdStr getXMLInputPointIDConf(CCLogicInputsT xmlElem) throw(ParserException);
static inline DigitalChangeEvent::Config getXMLInputDigitalModeConf(CInputModeT xmlElem) throw(ParserException);

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/**
 * \brief Parse a Channel Configuration from XML config file, storing it.
 *
 * \param xmlElem XML structure to parse.
 *
 * \return Struct containing the obtained data.
 *
 * \throw Exception when failed to get all of the mandatory elements.
 */
static inline ChannelRef getXMLChannelConf(const CChannelRefT& xmlElem) throw(ParserException)
{
    ChannelRef config;
    config.moduleType = getXMLAttrEnum<MODULE>(xmlElem.moduleType, MODULE_VALUEfromINDEX);
    config.moduleID = getXMLAttrEnum<MODULE_ID>(xmlElem.moduleID, MODULE_ID_VALUEfromINDEX);
    config.channelID = getXMLAttr(xmlElem.channelID);
    return config;
}


/**
 * \brief Parse a Point ID from XML config file, storing it.
 *
 * \param xmlElem XML structure to parse.
 *
 * \return obtained point ID.
 *
 * \throw Exception when failed to get all of the mandatory elements.
 */
static inline PointIdStr getXMLPointIDConf(const CVirtualPointT& xmlElem) throw(ParserException)
{
    PointIdStr config;
    config.group = getXMLAttr(xmlElem.group);
    config.ID    = getXMLAttr(xmlElem.id);
    return config;
}


/**
 * \brief Parse a Point Reference from XML config file, storing it.
 *
 * \param xmlElem XML structure to parse.
 *
 * \return obtained point ID.
 *
 * \throw Exception when failed to get all of the mandatory elements.
 */
static inline PointIdStr getXMLVirtualPointRef(const CVirtualPointRefT& xmlElem) throw(ParserException)
{
    PointIdStr config;
    config.group = getXMLAttr(xmlElem.pointGroup);
    config.ID    = getXMLAttr(xmlElem.pointID);
    return config;
}


/**
 * \brief Parse a Point ID from input entry of XML config file, storing it.
 *
 * \param xmlElem XML structure to parse.
 *
 * \return obtained point ID.
 *
 * \throw Exception when failed to get all of the mandatory elements.
 */
static inline PointIdStr getXMLInputPointIDConf(CCLogicInputsT xmlElem) throw(ParserException)
{
    if( !xmlElem.inputPoint.exists() )
    {
        throw ParserException(CONFMGR_ERROR_NO_POINT, "No input points");
    }
    return getXMLVirtualPointRef(xmlElem.inputPoint.first());
}


/**
 * \brief Parse an input mode configuration from XML config file, storing it.
 *
 * \param xmlElem XML structure to parse.
 *
 * \return Struct containing the obtained data.
 *
 * \throw Exception when failed to get all of the mandatory elements.
 */
static inline DigitalChangeEvent::Config getXMLInputDigitalModeConf(CInputModeT xmlElem) throw(ParserException)
{
    DigitalChangeEvent::Config config;
    try
    {
        /* First try to configure it as a pulse-driven */
        checkXMLElement(xmlElem.pulseDriven);
        config.pulseWidth_ms = getXMLAttr(xmlElem.pulseDriven.first().pulseWidth);
        if(config.pulseWidth_ms == 0)
        {
            throw ParserException(CONFMGR_ERROR_NONE, "No pulse configured");
        }
    }
    catch (const ParserException& e)
    {
        /* No pulse config. Try edge-driven */
        checkXMLElement(xmlElem.edgeDriven);
        lu_uint32_t edge;
        edge = getXMLAttr(xmlElem.edgeDriven.first().edgeDrivenMode);
        config.edge = static_cast<EDGE_DRIVEN_MODE>(edge);
    }
    /* Note: If no pulse/edge config is given, it throws an exception for no edge configuration */
    return config;
}


/**
 * \brief Parse all point labels from XML config file for the given point.
 *
 * \param configuration XML configuration storage.
 * \param vPoint XML Virtual Point structure to parse.
 * \param config Where to store the obtained data.
 *
 * \throw Exception when failed to get all of the mandatory elements.
 */
template <typename XMLElement, typename Config>
void addVPointLabels(Cg3schema& configuration, XMLElement& vPoint, Config& config)
{
    try
    {
        checkXMLElement(configuration.configuration);
        CVirtualPointsT vPoints = getXMLElement<CVirtualPointsT>(configuration.configuration.first().virtualPoints);
        CCustomLabelsT labelSets = getXMLElement<CCustomLabelsT>(vPoints.customLabels);
        checkXMLElement(labelSets.labelSet);
        checkXMLElement(vPoint.customLabel);
        std::string labelSetName = getXMLAttr(vPoint.customLabel.first().labelSetName);
        for(Iterator<CCustomLabelSetT> labelIt = labelSets.labelSet.all(); labelIt; ++labelIt)
        {
            if(labelSetName.compare(labelIt.name) == 0)
            {
                //Label Set found: Apply all custom labels
                for(Iterator<CCustomLabelEntryT> it=labelIt.labelEntry.all(); it; ++it)
                {
                    lu_uint32_t value = getXMLAttr(it.value2);
                    std::string label = getXMLAttr(it.label);
                    config.addLabel(value, label);
                }
            }
        }
    }
    catch(const ParserException& e)
    {}//No custom labels: nothing to do!
}


/**
 * \brief Parse a Digital Point Configuration from XML config file, storing it.
 *
 * \param groupID Point group ID.
 * \param id Point ID number.
 * \param configuration XML configuration storage.
 * \param xmlElem XML Virtual Point structure to parse.
 * \param config Where to store the obtained data.
 *
 * \throw Exception when failed to get all of the mandatory elements.
 */
template <typename XMLElement>
void getXMLDigitalPointConf(Cg3schema& configuration,
                            XMLElement xmlElem,
                            DigitalPoint::Config& config
                           ) throw(ParserException)
{
    config.ID = getXMLPointIDConf(xmlElem); //Get Point ID from config

//    getXMLOptAttr(xmlElem.uniqueID, config.uniqueID);
//    getXMLOptAttr(xmlElem.persistent, config.persistent); //Note:persistent=false by default

    checkXMLElement(xmlElem.chatter);
    config.chatterNo = getXMLAttr(xmlElem.chatter.first().chatterNo);
    config.chatterTime = getXMLAttr(xmlElem.chatter.first().chatterTime);

    addVPointLabels(configuration, xmlElem, config);

    /* Get delayed report configuration */
    if(xmlElem.delayedReport.exists())
    {
        if(xmlElem.delayedReport.first().enabled)
        {
            for(Iterator<CDelayedEventEntryT> delayIt = xmlElem.delayedReport.first().entry.all(); delayIt; ++delayIt)
            {
                DigitalPoint::Config::Delay delay;
                try
                {
                    //Final value is kind of mandatory
                    delay.eventVal = getXMLAttr(delayIt.eventValue);
                }
                catch (const ParserException& e)
                {
                    //No event val, means this entry is invalid => ignore it
                    continue;
                }
                //Add delay -- when not present, is 0 (not delayed)
                getXMLOptAttr(delayIt.delayPeriodMs, delay.delay_ms);
                config.addDelay(delay); //Add it to the configuration
            }
        }
    }
}


/**
 * \brief Parse an Analogue Threshold Filter configuration from XML config file, storing it.
 *
 * \param xmlElem XML Virtual Point structure to parse.
 * \param config Where to store the obtained data.
 *
 * \throw Exception when failed to get all of the mandatory elements.
 */
template <typename XMLElement, typename ConfigType>
void parseXMLThresholdFilterConf(const XMLElement& xmlElem,
                                 ConfigType& config
                                ) throw(ParserException)
{
    config.limit[HIHILOLOFilter::LOLO].threshold  = getXMLAttr(xmlElem.loloThreshold);
    config.limit[HIHILOLOFilter::LOLO].hysteresis = getXMLAttr(xmlElem.loloHysteresis);
    config.limit[HIHILOLOFilter::LOLO].active     = getXMLAttr(xmlElem.loloEnabled);
    config.limit[HIHILOLOFilter::LO  ].threshold  = getXMLAttr(xmlElem.loThreshold);   ;
    config.limit[HIHILOLOFilter::LO  ].hysteresis = getXMLAttr(xmlElem.loHysteresis);
    config.limit[HIHILOLOFilter::LO  ].active     = getXMLAttr(xmlElem.loEnabled);
    config.limit[HIHILOLOFilter::HI  ].threshold  = getXMLAttr(xmlElem.hiThreshold);
    config.limit[HIHILOLOFilter::HI  ].hysteresis = getXMLAttr(xmlElem.hiHysteresis);
    config.limit[HIHILOLOFilter::HI  ].active     = getXMLAttr(xmlElem.hiEnabled);
    config.limit[HIHILOLOFilter::HIHI].threshold  = getXMLAttr(xmlElem.hihiThreshold); ;
    config.limit[HIHILOLOFilter::HIHI].hysteresis = getXMLAttr(xmlElem.hihiHysteresis);
    config.limit[HIHILOLOFilter::HIHI].active     = getXMLAttr(xmlElem.hihiEnabled);
    if( !config.isValid() )
    {
        /* Check boundaries correctness */
        throw ParserException(CONFMGR_ERROR_CONFIG, "invalid Threshold Limits");
    }
}


/**
 * \brief Parse an Analogue Point Configuration from XML config file, storing it.
 *
 * \param groupID Point group ID.
 * \param id Point ID number.
 * \param configuration XML configuration storage.
 * \param xmlElem XML Virtual Point structure to parse.
 * \param config Where to store the obtained data.
 *
 * \throw Exception when failed to get all of the mandatory elements.
 */
template <typename XMLElement>
void getXMLAnaloguePointConf( Cg3schema& configuration,
                              XMLElement xmlElem,
                              AnaloguePoint::Config& config
                             ) throw(ParserException)
{
    config.ID = getXMLPointIDConf(xmlElem);

//    getXMLOptAttr(xmlElem.uniqueID, config.uniqueID);
//    getXMLOptAttr(xmlElem.persistent, config.persistent); //Note:persistent=false by default

    checkXMLElement(xmlElem.scaling);
    config.scale  = getXMLAttr(xmlElem.scaling.first().scalingFactor);
    config.offset = getXMLAttr(xmlElem.scaling.first().offset);

    /* Convert Range from raw scale to engineering units full range */
    lu_float32_t minValue = getXMLAttr(xmlElem.scaling.first().rawMin);
    lu_float32_t maxValue = getXMLAttr(xmlElem.scaling.first().rawMax);
    config.fullRange.minValue = (minValue * config.scale) + config.offset;
    config.fullRange.maxValue = (maxValue * config.scale) + config.offset;

    /* Over Range parameters. Level and hysteresis are given in engineering units */
    COverRangeT overRange = getXMLElement<COverRangeT>(xmlElem.overRange);
    config.overflow.threshold   = getXMLAttr(overRange.overflow);
    config.overflow.hysteresis  = getXMLAttr(overRange.overflowHysteresis);
    config.overflow.active      = getXMLAttr(overRange.overflowEnabled);
    config.underflow.threshold  = getXMLAttr(overRange.underflow);
    config.underflow.hysteresis = getXMLAttr(overRange.underflowHysteresis);
    config.underflow.active     = getXMLAttr(overRange.underflowEnabled);

    addVPointLabels(configuration, xmlElem, config);

    /* Check boundaries correctness */
    if( !config.isValid() )
    {
        //Unforgivable error: wrong Over Range configuration
        throw ParserException(CONFMGR_ERROR_CONFIG, "invalid Over Range limits");
    }

    config.filter.filterType = AFILTER_TYPE_NONE;   //Default is no filter
    if( xmlElem.filter.exists() )
    {
        CAnalogueFilterT filter = xmlElem.filter.first();
        if(filter.deadBand.exists()  == true)
        {
            CDeadBandFilterT dbFilter = filter.deadBand.first();
            config.filter.filterType = AFILTER_TYPE_DEADBAND;
            config.filter.DBFilter.deadband = getXMLAttr(dbFilter.deadBand);
            config.filter.DBFilter.useNominal = getXMLAttr(dbFilter.initialNominalEnabled);
            config.filter.DBFilter.nominal = getXMLAttr(dbFilter.initialNominal);
        }
        else if(filter.lowerUpperLimit.exists() == true)
        {
            CLowerUpperLimitFilterT LUFilter = filter.lowerUpperLimit.first();
            config.filter.filterType = AFILTER_TYPE_LOWER_UPPER_LIMIT;
            bool wrong = false;
            LowerUpperLimitFilter::Config& lulConf = config.filter.LULFilter;
            try
            {
                lulConf.lower.active = getXMLAttr(LUFilter.lowerEnabled);
                lulConf.lower.threshold = getXMLAttr(LUFilter.lowerLimit);
                lulConf.lower.hysteresis = getXMLAttr(LUFilter.lowerHysteresis);
            }
            catch (const ParserException& e)
            {
                wrong = true;
                lulConf.lower.active = false;
            }
            try
            {
                lulConf.upper.active    = getXMLAttr(LUFilter.upperEnabled);
                lulConf.upper.threshold = getXMLAttr(LUFilter.upperLimit);
                lulConf.upper.hysteresis= getXMLAttr(LUFilter.upperHysteresis);
            }
            catch (const ParserException& e)
            {
                wrong = true;
                lulConf.upper.active = false;
            }
            /* Check boundaries correctness */
            if(wrong || !lulConf.isValid())
            {
                //Unforgivable error: wrong filter configuration
                std::stringstream ss;
                ss << "invalid "
                   << filter.lowerUpperLimit.info().GetLocalName().c_str()
                   << " Filter Limits";
                throw ParserException(CONFMGR_ERROR_CONFIG, ss.str().c_str());
            }
        }
        else if(filter.highHighLowLow.exists() == true)
        {
            CHighHighLowLowFilterT HLFilter = filter.highHighLowLow.first();
            config.filter.filterType = AFILTER_TYPE_HIHILOLO;
            HIHILOLOFilter::Config& hlConfig = config.filter.HHLLFilter;
            parseXMLThresholdFilterConf(HLFilter, hlConfig);
        }
    }
}

/**
 * \brief Singleton Class for providing limited unique IDs to Counters
 */
class CounterIdxHandler
{
public:
    static CounterIdxHandler& getInstance()
    {
        static CounterIdxHandler instance;
        return instance;
    };
    VPointUID getUID()
    {
        VPointUID ret = persistentIDx;
        if (persistentIDx > 0)
        {
            --persistentIDx;    //Decrease counting for next ID
        }
        return ret;
    };
private:
    /**
     * \brief Singleton: Private constructor, destructor, and copy constructor
     */
    CounterIdxHandler() : persistentIDx(CounterPoint::Config::MAX_PERSISTENT)
    {};
    virtual ~CounterIdxHandler()
    {};
    CounterIdxHandler(const CounterIdxHandler& p) { LU_UNUSED(p); };
private:
    VPointUID persistentIDx;
};


/**
 * \brief Parse a Counter Point Configuration from XML config file, storing it.
 *
 * \param groupID Point group ID.
 * \param id Point ID number.
 * \param configuration XML configuration storage.
 * \param xmlElem XML Virtual Point structure to parse.
 * \param config Where to store the obtained data.
 *
 * \throw Exception when failed to get all of the mandatory elements.
 */
template <typename Config>
void getXMLCounterPointConf(Cg3schema& configuration,
                            CBaseCounterPointT xmlElem,
                            Config& config
                           ) throw(ParserException)
{
    config.ID = getXMLPointIDConf(xmlElem);

    config.persistent = getXMLAttr(xmlElem.persistent);
    if(config.persistent)
    {
        /* Persistent counters are limited */
        config.uniqueID = CounterIdxHandler::getInstance().getUID();
        if(config.uniqueID == 0)
        {
            config.persistent = false;
        }
    }
    config.clearOffline = getXMLAttr(xmlElem.autoReset);
    CCounterValuesT values = getXMLElement<CCounterValuesT>(xmlElem.counterValues);
    config.resetValue = getXMLAttr(values.resetValue);
    config.rolloverValue = getXMLAttr(values.rolloverValue);
    config.eventStepCount = getXMLAttr(values.eventStepValue);

    addVPointLabels(configuration, xmlElem, config);

    /* Counter Freezing configuration */
    config.freezable = LU_FALSE;
    if(xmlElem.freezing.exists())
    {
    	CFreezingConfT freeze = xmlElem.freezing.first();
        config.freezable = getXMLAttr(freeze.enabled);
        if(freeze.enabled)
        {
            /* Read freezing configuration for freezable counters */
            bool freezeAndClear;
            freezeAndClear = getXMLAttr(freeze.freezeThenReset);
            config.freezeType = (freezeAndClear)? CounterPoint::Config::FREEZE_TYPE_CLEAR :
                                                  CounterPoint::Config::FREEZE_TYPE_ONLY;
            if(freeze.freezingClockTime.exists())
            {
                config.freezeTimeType = CounterPoint::Config::FREEZE_TIME_OCLOCK;
                config.freezeTime = getXMLElement<lu_uint32_t>(freeze.freezingClockTime);
                config.freezeTime *= 60;   //convert minutes to seconds
            }
            else if(freeze.freezingInterval.exists())
            {
                config.freezeTimeType = CounterPoint::Config::FREEZE_TIME_INTERVAL;
                config.freezeTime = getXMLElement<lu_uint32_t>(freeze.freezingInterval);
                config.freezeTime *= 60;   //convert minutes to seconds
            }
            else
            {
                throw ParserException(CONFMGR_ERROR_CONFIG, "No valid freeze time type found.");
            }
        }
    }
    /* Check validity of given values */
    if( (config.rolloverValue < config.resetValue) && (config.rolloverValue > 0) )
    {
        throw ParserException(CONFMGR_ERROR_CONFIG, "Reset value can't be bigger than the Rollover value.");
    }
    if( (config.freezable == LU_FALSE) &&
        (values.rolloverValue < values.eventStepValue) &&
        (config.rolloverValue > 0)
        )
    {
        throw ParserException(CONFMGR_ERROR_CONFIG, "Event step value can't be bigger than the Rollover value.");
    }
}


#endif /* XMLPARSER_POINTS_H_ */

/*
 *********************** End of file ******************************************
 */
