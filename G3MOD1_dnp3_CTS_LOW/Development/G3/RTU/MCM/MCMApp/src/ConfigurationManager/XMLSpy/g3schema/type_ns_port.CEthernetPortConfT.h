#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_port_ALTOVA_CEthernetPortConfT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_port_ALTOVA_CEthernetPortConfT



namespace g3schema
{

namespace ns_port
{	

class CEthernetPortConfT : public TypeBase
{
public:
	g3schema_EXPORT CEthernetPortConfT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CEthernetPortConfT(CEthernetPortConfT const& init);
	void operator=(CEthernetPortConfT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_port_altova_CEthernetPortConfT); }

	MemberAttribute<bool,_altova_mi_ns_port_altova_CEthernetPortConfT_altova_enabled, 0, 0> enabled;	// enabled Cboolean
	MemberAttribute<string_type,_altova_mi_ns_port_altova_CEthernetPortConfT_altova_portName, 0, 2> portName;	// portName CETHERNET_PORT
	MemberElement<ns_port::CIpv4SettingT, _altova_mi_ns_port_altova_CEthernetPortConfT_altova_ipv4Setting> ipv4Setting;
	struct ipv4Setting { typedef Iterator<ns_port::CIpv4SettingT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_port

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_port_ALTOVA_CEthernetPortConfT
