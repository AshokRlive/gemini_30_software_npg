#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_pstack_ALTOVA_CSesnEventConfT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_pstack_ALTOVA_CSesnEventConfT



namespace g3schema
{

namespace ns_pstack
{	

class CSesnEventConfT : public TypeBase
{
public:
	g3schema_EXPORT CSesnEventConfT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSesnEventConfT(CSesnEventConfT const& init);
	void operator=(CSesnEventConfT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_pstack_altova_CSesnEventConfT); }

	MemberAttribute<unsigned,_altova_mi_ns_pstack_altova_CSesnEventConfT_altova_maxEvents, 0, 0> maxEvents;	// maxEvents CunsignedInt
	MemberAttribute<string_type,_altova_mi_ns_pstack_altova_CSesnEventConfT_altova_eventMode, 0, 4> eventMode;	// eventMode CLU_EVENT_MODE
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_pstack

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_pstack_ALTOVA_CSesnEventConfT
