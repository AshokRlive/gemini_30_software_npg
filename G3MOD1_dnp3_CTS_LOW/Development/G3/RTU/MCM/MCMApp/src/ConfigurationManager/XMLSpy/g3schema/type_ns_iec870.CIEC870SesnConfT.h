#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_iec870_ALTOVA_CIEC870SesnConfT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_iec870_ALTOVA_CIEC870SesnConfT

#include "type_ns_pstack.CSessionConfT.h"


namespace g3schema
{

namespace ns_iec870
{	

class CIEC870SesnConfT : public ::g3schema::ns_pstack::CSessionConfT
{
public:
	g3schema_EXPORT CIEC870SesnConfT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CIEC870SesnConfT(CIEC870SesnConfT const& init);
	void operator=(CIEC870SesnConfT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_iec870_altova_CIEC870SesnConfT); }

	MemberAttribute<bool,_altova_mi_ns_iec870_altova_CIEC870SesnConfT_altova_useDayOfWeek, 0, 0> useDayOfWeek;	// useDayOfWeek Cboolean

	MemberAttribute<unsigned,_altova_mi_ns_iec870_altova_CIEC870SesnConfT_altova_asduAddress, 0, 0> asduAddress;	// asduAddress CunsignedShort

	MemberAttribute<unsigned,_altova_mi_ns_iec870_altova_CIEC870SesnConfT_altova_cyclicPeriodMs, 0, 0> cyclicPeriodMs;	// cyclicPeriodMs CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_iec870_altova_CIEC870SesnConfT_altova_cyclicFirstPeriodMs, 0, 0> cyclicFirstPeriodMs;	// cyclicFirstPeriodMs CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_iec870_altova_CIEC870SesnConfT_altova_backgroundPeriodMs, 0, 0> backgroundPeriodMs;	// backgroundPeriodMs CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_iec870_altova_CIEC870SesnConfT_altova_selectTimeoutMs, 0, 0> selectTimeoutMs;	// selectTimeoutMs CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_iec870_altova_CIEC870SesnConfT_altova_defResponseTimeoutMs, 0, 0> defResponseTimeoutMs;	// defResponseTimeoutMs CunsignedInt

	MemberAttribute<bool,_altova_mi_ns_iec870_altova_CIEC870SesnConfT_altova_cseUseActTerm, 0, 0> cseUseActTerm;	// cseUseActTerm Cboolean

	MemberAttribute<bool,_altova_mi_ns_iec870_altova_CIEC870SesnConfT_altova_cmdUseActTerm, 0, 0> cmdUseActTerm;	// cmdUseActTerm Cboolean

	MemberAttribute<bool,_altova_mi_ns_iec870_altova_CIEC870SesnConfT_altova_multiplePMEN, 0, 0> multiplePMEN;	// multiplePMEN Cboolean

	MemberAttribute<bool,_altova_mi_ns_iec870_altova_CIEC870SesnConfT_altova_sendClockSyncEvents, 0, 0> sendClockSyncEvents;	// sendClockSyncEvents Cboolean

	MemberAttribute<bool,_altova_mi_ns_iec870_altova_CIEC870SesnConfT_altova_deleteOldestEvent, 0, 0> deleteOldestEvent;	// deleteOldestEvent Cboolean

	MemberAttribute<unsigned,_altova_mi_ns_iec870_altova_CIEC870SesnConfT_altova_cmdTimetagMode, 0, 0> cmdTimetagMode;	// cmdTimetagMode CunsignedByte
	MemberAttribute<string_type,_altova_mi_ns_iec870_altova_CIEC870SesnConfT_altova_cicnaTimeFormat, 0, 3> cicnaTimeFormat;	// cicnaTimeFormat CLU_TIME_FORMAT
	MemberAttribute<string_type,_altova_mi_ns_iec870_altova_CIEC870SesnConfT_altova_readTimeFormat, 0, 3> readTimeFormat;	// readTimeFormat CLU_TIME_FORMAT
	MemberAttribute<string_type,_altova_mi_ns_iec870_altova_CIEC870SesnConfT_altova_readMsrndTimeFormat, 0, 3> readMsrndTimeFormat;	// readMsrndTimeFormat CLU_TIME_FORMAT
	MemberElement<ns_iec870::CIEC870EventConfigT, _altova_mi_ns_iec870_altova_CIEC870SesnConfT_altova_eventConfig> eventConfig;
	struct eventConfig { typedef Iterator<ns_iec870::CIEC870EventConfigT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_iec870

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_iec870_ALTOVA_CIEC870SesnConfT
