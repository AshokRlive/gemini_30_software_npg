#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CConfigVersionT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CConfigVersionT



namespace g3schema
{

class CConfigVersionT : public TypeBase
{
public:
	g3schema_EXPORT CConfigVersionT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CConfigVersionT(CConfigVersionT const& init);
	void operator=(CConfigVersionT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_altova_CConfigVersionT); }

	MemberAttribute<string_type,_altova_mi_altova_CConfigVersionT_altova_configReleaseType, 0, 0> configReleaseType;	// configReleaseType Cstring

	MemberAttribute<unsigned,_altova_mi_altova_CConfigVersionT_altova_configVersionMinor, 0, 0> configVersionMinor;	// configVersionMinor CunsignedInt

	MemberAttribute<unsigned,_altova_mi_altova_CConfigVersionT_altova_configVersionMajor, 0, 0> configVersionMajor;	// configVersionMajor CunsignedInt

	MemberAttribute<unsigned,_altova_mi_altova_CConfigVersionT_altova_configVersionPatch, 0, 0> configVersionPatch;	// configVersionPatch CunsignedInt

	MemberAttribute<string_type,_altova_mi_altova_CConfigVersionT_altova_configFileDescription, 0, 0> configFileDescription;	// configFileDescription Cstring

	MemberAttribute<string_type,_altova_mi_altova_CConfigVersionT_altova_modificationDate, 0, 0> modificationDate;	// modificationDate CFileModificationDateT
	g3schema_EXPORT void SetXsiType();
};


}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CConfigVersionT
