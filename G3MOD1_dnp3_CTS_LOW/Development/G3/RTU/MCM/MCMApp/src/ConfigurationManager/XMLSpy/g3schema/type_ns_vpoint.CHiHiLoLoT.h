#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CHiHiLoLoT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CHiHiLoLoT



namespace g3schema
{

namespace ns_vpoint
{	

class CHiHiLoLoT : public TypeBase
{
public:
	g3schema_EXPORT CHiHiLoLoT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CHiHiLoLoT(CHiHiLoLoT const& init);
	void operator=(CHiHiLoLoT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CHiHiLoLoT); }

	MemberAttribute<bool,_altova_mi_ns_vpoint_altova_CHiHiLoLoT_altova_hihiEnabled, 0, 0> hihiEnabled;	// hihiEnabled Cboolean

	MemberAttribute<double,_altova_mi_ns_vpoint_altova_CHiHiLoLoT_altova_hihiThreshold, 0, 0> hihiThreshold;	// hihiThreshold Cfloat

	MemberAttribute<double,_altova_mi_ns_vpoint_altova_CHiHiLoLoT_altova_hihiHysteresis, 0, 0> hihiHysteresis;	// hihiHysteresis Cfloat

	MemberAttribute<bool,_altova_mi_ns_vpoint_altova_CHiHiLoLoT_altova_hiEnabled, 0, 0> hiEnabled;	// hiEnabled Cboolean

	MemberAttribute<double,_altova_mi_ns_vpoint_altova_CHiHiLoLoT_altova_hiThreshold, 0, 0> hiThreshold;	// hiThreshold Cfloat

	MemberAttribute<double,_altova_mi_ns_vpoint_altova_CHiHiLoLoT_altova_hiHysteresis, 0, 0> hiHysteresis;	// hiHysteresis Cfloat

	MemberAttribute<bool,_altova_mi_ns_vpoint_altova_CHiHiLoLoT_altova_loEnabled, 0, 0> loEnabled;	// loEnabled Cboolean

	MemberAttribute<double,_altova_mi_ns_vpoint_altova_CHiHiLoLoT_altova_loThreshold, 0, 0> loThreshold;	// loThreshold Cfloat

	MemberAttribute<double,_altova_mi_ns_vpoint_altova_CHiHiLoLoT_altova_loHysteresis, 0, 0> loHysteresis;	// loHysteresis Cfloat

	MemberAttribute<bool,_altova_mi_ns_vpoint_altova_CHiHiLoLoT_altova_loloEnabled, 0, 0> loloEnabled;	// loloEnabled Cboolean

	MemberAttribute<double,_altova_mi_ns_vpoint_altova_CHiHiLoLoT_altova_loloThreshold, 0, 0> loloThreshold;	// loloThreshold Cfloat

	MemberAttribute<double,_altova_mi_ns_vpoint_altova_CHiHiLoLoT_altova_loloHysteresis, 0, 0> loloHysteresis;	// loloHysteresis Cfloat
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CHiHiLoLoT
