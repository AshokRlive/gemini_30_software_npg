#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CHeaderT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CHeaderT



namespace g3schema
{

class CHeaderT : public TypeBase
{
public:
	g3schema_EXPORT CHeaderT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CHeaderT(CHeaderT const& init);
	void operator=(CHeaderT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_altova_CHeaderT); }
	MemberElement<CConfigVersionT, _altova_mi_altova_CHeaderT_altova_configVersion> configVersion;
	struct configVersion { typedef Iterator<CConfigVersionT> iterator; };
	MemberElement<CToolVersionT, _altova_mi_altova_CHeaderT_altova_toolVersion> toolVersion;
	struct toolVersion { typedef Iterator<CToolVersionT> iterator; };
	g3schema_EXPORT void SetXsiType();
};


}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CHeaderT
