#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_COutgoingConnectionsT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_COutgoingConnectionsT



namespace g3schema
{

namespace ns_sdnp3
{	

class COutgoingConnectionsT : public TypeBase
{
public:
	g3schema_EXPORT COutgoingConnectionsT(xercesc::DOMNode* const& init);
	g3schema_EXPORT COutgoingConnectionsT(COutgoingConnectionsT const& init);
	void operator=(COutgoingConnectionsT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_sdnp3_altova_COutgoingConnectionsT); }

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_COutgoingConnectionsT_altova_retries, 0, 0> retries;	// retries CunsignedInt
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_sdnp3

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_COutgoingConnectionsT
