#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CCounterPointT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CCounterPointT

#include "type_ns_vpoint.CBaseCounterPointT.h"


namespace g3schema
{

namespace ns_vpoint
{	

class CCounterPointT : public ::g3schema::ns_vpoint::CBaseCounterPointT
{
public:
	g3schema_EXPORT CCounterPointT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CCounterPointT(CCounterPointT const& init);
	void operator=(CCounterPointT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CCounterPointT); }
	MemberElement<ns_common::CVirtualPointRefT, _altova_mi_ns_vpoint_altova_CCounterPointT_altova_inputSource> inputSource;
	struct inputSource { typedef Iterator<ns_common::CVirtualPointRefT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CCounterPointT
