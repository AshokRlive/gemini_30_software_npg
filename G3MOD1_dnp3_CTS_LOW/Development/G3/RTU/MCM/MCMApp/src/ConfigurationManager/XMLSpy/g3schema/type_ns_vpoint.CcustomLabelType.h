#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CcustomLabelType
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CcustomLabelType



namespace g3schema
{

namespace ns_vpoint
{	

class CcustomLabelType : public TypeBase
{
public:
	g3schema_EXPORT CcustomLabelType(xercesc::DOMNode* const& init);
	g3schema_EXPORT CcustomLabelType(CcustomLabelType const& init);
	void operator=(CcustomLabelType const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CcustomLabelType); }

	MemberAttribute<string_type,_altova_mi_ns_vpoint_altova_CcustomLabelType_altova_labelSetName, 0, 0> labelSetName;	// labelSetName Cstring
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CcustomLabelType
