#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CScadaTemplateCommsDevT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CScadaTemplateCommsDevT



namespace g3schema
{

namespace ns_template
{	

class CScadaTemplateCommsDevT : public TypeBase
{
public:
	g3schema_EXPORT CScadaTemplateCommsDevT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CScadaTemplateCommsDevT(CScadaTemplateCommsDevT const& init);
	void operator=(CScadaTemplateCommsDevT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_template_altova_CScadaTemplateCommsDevT); }

	MemberAttribute<string_type,_altova_mi_ns_template_altova_CScadaTemplateCommsDevT_altova_masterAddress, 0, 0> masterAddress;	// masterAddress Cstring
	MemberElement<ns_sdnp3::CSDNP3CommsDeviceConfT, _altova_mi_ns_template_altova_CScadaTemplateCommsDevT_altova_commsDevice> commsDevice;
	struct commsDevice { typedef Iterator<ns_sdnp3::CSDNP3CommsDeviceConfT> iterator; };
	MemberElement<ns_template::CCommsDeviceConfigT, _altova_mi_ns_template_altova_CScadaTemplateCommsDevT_altova_commsDeviceConfig> commsDeviceConfig;
	struct commsDeviceConfig { typedef Iterator<ns_template::CCommsDeviceConfigT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_template

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CScadaTemplateCommsDevT
