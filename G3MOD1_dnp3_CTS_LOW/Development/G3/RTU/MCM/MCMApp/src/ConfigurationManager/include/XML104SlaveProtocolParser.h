/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:XMLss104ProtocolParser.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24 Nov 2014     shetty_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef XMLss104PROTOCOLPARSER_H_
#define XMLss104PROTOCOLPARSER_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <iostream>
#include <string>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
//#include "Thread.h"
#include "ConfigurationManagerError.h"
//#include "MCMConfigEnum.h"
//#include "MainAppEnum.h"
#include "PortManager.h"
#include "ModuleManager.h"
#include "CommsDeviceManager.h"
//#include "S104InputPoint.h"
#include "S104Protocol.h"

#include "XMLParser.h"

#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>

#include "Altova/Altova.h"
#include "Altova/SchemaTypes.h"
#include "Altova/AltovaException.h"

#include "AltovaXML/XmlException.h"
#include "AltovaXML/Node.h"

#include "g3schema/g3schema.h"

using namespace g3schema;
using namespace altova;
using namespace ns_iec870;
using namespace ns_s104;

/*
 *******************************CConnectionSettingsT***********************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class XMLSlave104ProtocolParser
{
public:
    /**
     * \brief Custom constructor
     *
     * The initParser method is automatically called
     *
     * \param configuration         Configuration file parser handler
     * \param commsDeviceManager    Comms Device Manager (MODEMs)
     * \param portManager           Port Manager (Serial ports etc)
     * \param modulemanager         Module Manager (modules, channels etc)
     */
    XMLSlave104ProtocolParser(Cg3schema&          configuration,
                               CommsDeviceManager& commsDeviceManager,
                               PortManager&        portManager,
                               ModuleManager&      moduleManager);

    virtual ~XMLSlave104ProtocolParser();

    /**
     * \brief Create a new protocol stack instance
     *
     * \param GDatabase Gemini database reference
     *
     * \return pointer to the new protocol stack. NULL in case of error
     */
    virtual S104Protocol *newProtocolManager(GeminiDatabase&   GDatabase);

private:
    /**
     * \brief Initialize the parser
     *
     * \return Error code
     */
    CONFMGR_ERROR initParser();

    /**
     * \brief Allocate all the configured analogue points
     *
     * The analogue point configuration section is parsed and the
     * points/observers database is created and initialised
     *
     * \param GDatabase Reference to the gemini database.
     * \param sessionParserPtr Pointer to the session parser config
     * \param sectorPtr Pointer to the sector (which holds the protocol point db too)
     */
    void parseAnalogConfig( GeminiDatabase           &GDatabase,
                              CS104SessionT         *sessionParserPtr,
                              S104InputPoint::ProtPointType analogType,
                              S104Sector *sectorPtr);


    void parseCommonAnalogAttributes(CIEC870InputPointT*,
                                     lu_uint8_t,
                                     S104InputPoint::Config*
                                     ) throw(ParserException);


    /**
     * \brief Allocate all the configured binary points
     *
     * The binary point configuration section is parsed and the
     * points/observers database is created and initialised
     *
     * \param GDatabase Reference to the gemini database.
     * \param sessionParserPtr Pointer to the session parser config
     * \param sectorPtr Pointer to the sector (which holds the protocol point db too)
     */

    void parseBinaryConfig( GeminiDatabase           &GDatabase,
                            CS104SessionT           *sessionParserPtr,
                            S104Sector *sectorPtr);

    /**
     * \brief Allocate all the configured double binary points
     *
     * The double binary point configuration section is parsed and the
     * points/observers database is created and initialised
     *
     * \param GDatabase Reference to the gemini database.
     * \param sessionParserPtr Pointer to the session parser config
     * \param sectorPtr Pointer to the sector (which holds the protocol point db too)
     */
    void parseDBinaryConfig( GeminiDatabase           &GDatabase,
                             CS104SessionT           *sessionParserPtr,
                             S104Sector *sectorPtr);

    /**
     * \brief Allocate all the configured binary output points
     *
     * The binary output point configuration section is parsed and the
     * points database is created and initialised
     *
     * \param s104 reference to the s104 slave section of the XML parser
     * \param  db reference to the protocol stack database to initialise
     */
    void parseBinaryOutputConfig( CS104SessionT           *sessionParserPtr,
                                  S104Sector *sectorPtr);

    /**
     * \brief Allocate all the configured double binary output points
     *
     * The double binary output point configuration section is parsed and the
     * points database is created and initialised
     *
     * \param s104 reference to the s104 slave section of the XML parser
     */
    void parseDBinaryOutputConfig(CS104SessionT           *sessionParserPtr,
                                  S104Sector *sectorPtr);

    /**
     * \brief Allocate all the configured Integrated Total Points
     *
     * The Integrated Totals point configuration section is parsed and the
     * points database is created and initialised
     *
     * \param s104 reference to the s104 slave section of the XML parser
     * \param  db reference to the protocol stack database to initialise
     */
    void parseIntegratedTotsConfig(GeminiDatabase           &GDatabase,
                                   CS104SessionT           *sessionParserPtr,
                                   S104Sector *sectorPtr);
    /**
     * \brief parse channel config
     *
     * \param channelConfig reference to the channelConfig of the XML parser
     * \param  reference to the structure of the 104Channel::S104ChannelConfig
     */
    void parseChannelConfig(CS104ChannelT&  channelConfig,
                             S104Channel::S104ChannelConfig& channelConfigStr) throw(ParserException);

    /**
     * \brief parse session config
     *
     * \param  sessionConfig reference to the session field of the XML parser
     * \param  reference to the S104SESN_CONFIG structure to fill
     *
     * \return Max APDU size
     */
    lu_uint32_t parseSessionConfig(CS104SessionT&  sessionConfig,
                                   S104SESN_CONFIG& sessionConfigStr) throw(ParserException);

    /**
     * \brief parse sector config
     *
     * \param sessionConfig reference to the session field of the XML parser
     * \param  reference to the S104SCTR_CONFIG structure to fill
     */
    void parseSectorConfig( CS104SessionT&  sessionConfig,
                            S104SCTR_CONFIG& sectorConfigStr) throw(ParserException);

private:
    Cg3schema&           configuration;
    CommsDeviceManager&  commsDeviceManager;
    PortManager&         portManager;
    ModuleManager&       moduleManager;
    Logger&              log;
};


#endif /* XMLss104PROTOCOLPARSER_H_ */

/*
 *********************** End of file ******************************************
 */
