#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CBaseCounterPointT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CBaseCounterPointT

#include "type_ns_vpoint.CVirtualPointT.h"


namespace g3schema
{

namespace ns_vpoint
{	

class CBaseCounterPointT : public ::g3schema::ns_vpoint::CVirtualPointT
{
public:
	g3schema_EXPORT CBaseCounterPointT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CBaseCounterPointT(CBaseCounterPointT const& init);
	void operator=(CBaseCounterPointT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CBaseCounterPointT); }

	MemberAttribute<bool,_altova_mi_ns_vpoint_altova_CBaseCounterPointT_altova_persistent, 0, 0> persistent;	// persistent Cboolean

	MemberAttribute<bool,_altova_mi_ns_vpoint_altova_CBaseCounterPointT_altova_autoReset, 0, 0> autoReset;	// autoReset Cboolean
	MemberElement<ns_vpoint::CInputModeT, _altova_mi_ns_vpoint_altova_CBaseCounterPointT_altova_inputMode> inputMode;
	struct inputMode { typedef Iterator<ns_vpoint::CInputModeT> iterator; };
	MemberElement<ns_vpoint::CFreezingConfT, _altova_mi_ns_vpoint_altova_CBaseCounterPointT_altova_freezing> freezing;
	struct freezing { typedef Iterator<ns_vpoint::CFreezingConfT> iterator; };
	MemberElement<ns_vpoint::CCounterValuesT, _altova_mi_ns_vpoint_altova_CBaseCounterPointT_altova_counterValues> counterValues;
	struct counterValues { typedef Iterator<ns_vpoint::CCounterValuesT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CBaseCounterPointT
