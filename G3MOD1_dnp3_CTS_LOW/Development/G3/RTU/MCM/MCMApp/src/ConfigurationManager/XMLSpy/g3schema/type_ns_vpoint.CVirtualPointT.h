#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CVirtualPointT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CVirtualPointT



namespace g3schema
{

namespace ns_vpoint
{	

class CVirtualPointT : public TypeBase
{
public:
	g3schema_EXPORT CVirtualPointT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CVirtualPointT(CVirtualPointT const& init);
	void operator=(CVirtualPointT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CVirtualPointT); }

	MemberAttribute<unsigned,_altova_mi_ns_vpoint_altova_CVirtualPointT_altova_group, 0, 0> group;	// group CVirtualPointGroupT

	MemberAttribute<unsigned,_altova_mi_ns_vpoint_altova_CVirtualPointT_altova_id, 0, 0> id;	// id CVirtualPointIdT

	MemberAttribute<string_type,_altova_mi_ns_vpoint_altova_CVirtualPointT_altova_description, 0, 0> description;	// description Cstring

	MemberAttribute<bool,_altova_mi_ns_vpoint_altova_CVirtualPointT_altova_customDescriptionEnabled, 0, 0> customDescriptionEnabled;	// customDescriptionEnabled Cboolean
	MemberElement<ns_vpoint::CcustomLabelType, _altova_mi_ns_vpoint_altova_CVirtualPointT_altova_customLabel> customLabel;
	struct customLabel { typedef Iterator<ns_vpoint::CcustomLabelType> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CVirtualPointT
