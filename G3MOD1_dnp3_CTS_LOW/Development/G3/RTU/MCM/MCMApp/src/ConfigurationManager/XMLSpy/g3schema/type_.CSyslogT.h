#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CSyslogT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CSyslogT



namespace g3schema
{

class CSyslogT : public TypeBase
{
public:
	g3schema_EXPORT CSyslogT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSyslogT(CSyslogT const& init);
	void operator=(CSyslogT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_altova_CSyslogT); }

	MemberAttribute<bool,_altova_mi_altova_CSyslogT_altova_enabled, 0, 0> enabled;	// enabled Cboolean

	MemberAttribute<string_type,_altova_mi_altova_CSyslogT_altova_syslogconf, 0, 0> syslogconf;	// syslogconf Cstring
	g3schema_EXPORT void SetXsiType();
};


}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CSyslogT
