#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CBinariesT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CBinariesT



namespace g3schema
{

namespace ns_vpoint
{	

class CBinariesT : public TypeBase
{
public:
	g3schema_EXPORT CBinariesT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CBinariesT(CBinariesT const& init);
	void operator=(CBinariesT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CBinariesT); }
	MemberElement<ns_vpoint::CBinaryPointT, _altova_mi_ns_vpoint_altova_CBinariesT_altova_binary> binary;
	struct binary { typedef Iterator<ns_vpoint::CBinaryPointT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CBinariesT
