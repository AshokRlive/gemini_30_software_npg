#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CFPIChannelT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CFPIChannelT



namespace g3schema
{

namespace ns_g3module
{	

class CFPIChannelT : public TypeBase
{
public:
	g3schema_EXPORT CFPIChannelT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CFPIChannelT(CFPIChannelT const& init);
	void operator=(CFPIChannelT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_g3module_altova_CFPIChannelT); }

	MemberAttribute<unsigned,_altova_mi_ns_g3module_altova_CFPIChannelT_altova_selfResetMs, 0, 0> selfResetMs;	// selfResetMs CunsignedInt

	MemberAttribute<bool,_altova_mi_ns_g3module_altova_CFPIChannelT_altova_enabled, 0, 0> enabled;	// enabled Cboolean

	MemberAttribute<unsigned,_altova_mi_ns_g3module_altova_CFPIChannelT_altova_CTRatioDividend, 0, 0> CTRatioDividend;	// CTRatioDividend CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_g3module_altova_CFPIChannelT_altova_CTRatioDivisor, 0, 0> CTRatioDivisor;	// CTRatioDivisor CunsignedInt
	MemberElement<ns_g3module::CFPIPhaseFaultT, _altova_mi_ns_g3module_altova_CFPIChannelT_altova_phaseFault> phaseFault;
	struct phaseFault { typedef Iterator<ns_g3module::CFPIPhaseFaultT> iterator; };
	MemberElement<ns_g3module::CFPIFaultT, _altova_mi_ns_g3module_altova_CFPIChannelT_altova_earthFault> earthFault;
	struct earthFault { typedef Iterator<ns_g3module::CFPIFaultT> iterator; };
	MemberElement<ns_g3module::CFPIFaultT, _altova_mi_ns_g3module_altova_CFPIChannelT_altova_sensitiveEarthFault> sensitiveEarthFault;
	struct sensitiveEarthFault { typedef Iterator<ns_g3module::CFPIFaultT> iterator; };
	MemberElement<ns_g3module::CFPIFaultT, _altova_mi_ns_g3module_altova_CFPIChannelT_altova_currentPresence> currentPresence;
	struct currentPresence { typedef Iterator<ns_g3module::CFPIFaultT> iterator; };
	MemberElement<ns_g3module::CFPIFaultT, _altova_mi_ns_g3module_altova_CFPIChannelT_altova_currentAbsence> currentAbsence;
	struct currentAbsence { typedef Iterator<ns_g3module::CFPIFaultT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_g3module

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CFPIChannelT
