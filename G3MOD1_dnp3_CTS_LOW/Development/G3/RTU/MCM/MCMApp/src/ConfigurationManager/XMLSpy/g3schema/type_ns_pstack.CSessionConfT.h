#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_pstack_ALTOVA_CSessionConfT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_pstack_ALTOVA_CSessionConfT



namespace g3schema
{

namespace ns_pstack
{	

class CSessionConfT : public TypeBase
{
public:
	g3schema_EXPORT CSessionConfT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSessionConfT(CSessionConfT const& init);
	void operator=(CSessionConfT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_pstack_altova_CSessionConfT); }

	MemberAttribute<string_type,_altova_mi_ns_pstack_altova_CSessionConfT_altova_sessionName, 0, 0> sessionName;	// sessionName Cstring

	MemberAttribute<bool,_altova_mi_ns_pstack_altova_CSessionConfT_altova_enabled, 0, 0> enabled;	// enabled Cboolean

	MemberAttribute<string_type,_altova_mi_ns_pstack_altova_CSessionConfT_altova_uuid, 0, 0> uuid;	// uuid Cstring
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_pstack

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_pstack_ALTOVA_CSessionConfT
