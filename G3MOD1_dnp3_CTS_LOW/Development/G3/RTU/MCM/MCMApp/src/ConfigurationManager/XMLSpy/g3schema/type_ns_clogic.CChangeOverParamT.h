#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CChangeOverParamT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CChangeOverParamT

#include "type_ns_clogic.CCLogicParametersBaseT.h"


namespace g3schema
{

namespace ns_clogic
{	

class CChangeOverParamT : public ::g3schema::ns_clogic::CCLogicParametersBaseT
{
public:
	g3schema_EXPORT CChangeOverParamT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CChangeOverParamT(CChangeOverParamT const& init);
	void operator=(CChangeOverParamT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_clogic_altova_CChangeOverParamT); }

	MemberAttribute<bool,_altova_mi_ns_clogic_altova_CChangeOverParamT_altova_defaultEnable, 0, 0> defaultEnable;	// defaultEnable Cboolean

	MemberAttribute<unsigned,_altova_mi_ns_clogic_altova_CChangeOverParamT_altova_swithcOffDelay, 0, 0> swithcOffDelay;	// swithcOffDelay CunsignedShort

	MemberAttribute<unsigned,_altova_mi_ns_clogic_altova_CChangeOverParamT_altova_switchChangeDelay, 0, 0> switchChangeDelay;	// switchChangeDelay CunsignedShort
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_clogic

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CChangeOverParamT
