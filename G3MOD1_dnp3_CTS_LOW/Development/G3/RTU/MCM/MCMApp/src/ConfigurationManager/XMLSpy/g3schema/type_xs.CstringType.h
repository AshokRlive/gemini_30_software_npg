#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_xs_ALTOVA_CstringType
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_xs_ALTOVA_CstringType



namespace g3schema
{

namespace xs
{	

class CstringType : public TypeBase
{
public:
	g3schema_EXPORT CstringType(xercesc::DOMNode* const& init);
	g3schema_EXPORT CstringType(CstringType const& init);
	void operator=(CstringType const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_xs_altova_CstringType); }
	g3schema_EXPORT void operator=(const string_type& value);
	g3schema_EXPORT operator string_type();
	g3schema_EXPORT void SetXsiType();
};



} // namespace xs

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_xs_ALTOVA_CstringType
