/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:XMLCommsDeviceConfigParser.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   30 Jun 2014     andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "XMLCommsDeviceConfigParser.h"
#include "CommsDeviceDebug.h"

using namespace ns_comms;

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

XMLCommsDeviceConfigParser::XMLCommsDeviceConfigParser(Cg3schema&          configuration,
                                                       CommsDeviceManager& commsDeviceManager,
                                                       PortManager&        portManager,
                                                       IIOModuleManager&   moduleManager) :
                                                           configuration(configuration),
                                                           commsDeviceManager(commsDeviceManager),
                                                           portManager(portManager),
                                                           moduleManager(moduleManager),
                                                           log(Logger::getLogger(SUBSYSTEM_ID_CONFMGR))
{
    initParser();
}

XMLCommsDeviceConfigParser::~XMLCommsDeviceConfigParser()
{
}


CONFMGR_ERROR XMLCommsDeviceConfigParser::initParser()
{

    try
    {
        checkXMLElement(configuration.configuration);
        CCommDevicesT CommsDevices = getXMLElement<CCommDevicesT>(configuration.configuration.first().commsDevices);

        if (CommsDevices.GPRSModem.exists())
        {
            // Iterate through the GPRS MODEMs
            for (Iterator<CGPRSModemT> itCommsDeviceGPRS = CommsDevices.GPRSModem.all();
                 itCommsDeviceGPRS;
                 ++itCommsDeviceGPRS)
            {
                // TODO - Not yet supported!!
            }
        }

        if (CommsDevices.PSTNModem.exists())
        {
            // Iterate through the Standard MODEMs
            for (Iterator<CPSTNModemT> itCommsDeviceGPRS = CommsDevices.PSTNModem.all();
                 itCommsDeviceGPRS;
                 ++itCommsDeviceGPRS)
            {
                // TODO - Not yet supported!!
            }
        }

        if (CommsDevices.PAKNETModem.exists())
        {
            // Iterate through the Paknet MODEMs
            for (Iterator<CPAKNETModemT> itCommsDevicePaknet = CommsDevices.PAKNETModem.all();
                      itCommsDevicePaknet;
                    ++itCommsDevicePaknet)
            {
                if(itCommsDevicePaknet.disabled.exists())
                {
                    lu_bool_t disabled = itCommsDevicePaknet.disabled;
                    if(disabled == LU_TRUE)
                    {
                        log.warn("comms device[PAKNET] disabled!\n");
                        continue;
                    }
                }

                Port *port = portManager.getPort(itCommsDevicePaknet.serialPortName);

                if (port != NULL)
                {
                    DialupModem::DialupModemConfig dialupModemConfig(*port);
                    dialupModemConfig.id           = getXMLAttr(itCommsDevicePaknet.id);
                    dialupModemConfig.name         = getXMLAttr(itCommsDevicePaknet.name);
                    dialupModemConfig.promptString = getXMLAttr(itCommsDevicePaknet.promptString);
                    dialupModemConfig.dialString1  = getXMLAttr(itCommsDevicePaknet.dialString);
                    getXMLOptAttr(itCommsDevicePaknet.dialString2, dialupModemConfig.dialString2);
                    dialupModemConfig.modemInactivityTimeoutMins      = getXMLAttr(itCommsDevicePaknet.modemInactivityTimeoutMins);
                    dialupModemConfig.connectionInactivityTimeoutSecs = getXMLAttr(itCommsDevicePaknet.connectionInactivityTimeoutSecs);
                    dialupModemConfig.connectTimeoutMs                = getXMLAttr(itCommsDevicePaknet.connectTimeoutMs);
                    dialupModemConfig.regularCallIntervalMins         = getXMLAttr(itCommsDevicePaknet.regularCallIntervalMins);

                    if (itCommsDevicePaknet.dialRetryIntervalSecs.exists())
                    {
                        lu_uint32_t retryIndex = 0;

                        for (lu_uint32_t retryInt = 0; retryInt < itCommsDevicePaknet.dialRetryIntervalSecs.count(); retryInt++)
                        {
                            if (retryIndex < DialupModem::DialupModemConfig::RETRY_INTERVAL_NUM)
                            {
                                dialupModemConfig.dialRetryIntervalSecs[retryIndex] = (lu_uint32_t)(itCommsDevicePaknet.dialRetryIntervalSecs[retryInt]);
                                DBG_INFO("Added retry interval [%i]", dialupModemConfig.dialRetryIntervalSecs[retryIndex]);
                                retryIndex++;
                            }
                        }
                    }

                    //  These settings are not yet used but are present in the configuration
//                    dialupModemConfig.hangupString  = getXMLAttr(itCommsDeviceModem.hangupString);
//                    dialupModemConfig.connectString = getXMLAttr(itCommsDeviceModem.connectString);

                    DialupModem* dialupModem = new DialupModem(dialupModemConfig);

                    // If a PSM is present, retrieve the COMMS1 channel to allow for
                    // power cycling of the MODEM
                    IChannel *channelPtr;

                    IIOModule* modulePtr = moduleManager.getModule(MODULE_PSM, MODULE_ID_0);
                    if(modulePtr != NULL)
                    {
                        /* Valid board found. Get the channel */
                        if ((channelPtr = modulePtr->getChannel(CHANNEL_TYPE_PSUPPLY, PSM_CH_PSUPPLY_COMMS_PRIM)) != NULL)
                        {
                            dialupModem->setPowerSupplyChan(channelPtr);
                        }
                    }

                    DBG_INFO("Adding Modem Device [%s]...\n", dialupModemConfig.name.c_str());

                    // Add the MODEM to the commsDeviceManager
                    if (commsDeviceManager.addCommsDevice(dialupModem) != 0)
                    {
                        log.error("XMLPortConfigParser: Unable to add CommsDevice [%s]", dialupModemConfig.name.c_str());
                        return CONFMGR_ERROR_CONFIG;
                    }
                }
                else
                {
                    std::string portName = getXMLAttr(itCommsDevicePaknet.serialPortName);

                    log.error("XMLCommsDeviceConfigParser: Unable to add CommsDevice attached to port [%s]", portName.c_str());
                    return CONFMGR_ERROR_CONFIG;
                }
            }
        }

        return CONFMGR_ERROR_NONE;
    }
    catch (const ParserException& e)
    {
        log.error("CommsDeviceConfigParser error: %s", e.what());
    }
    catch(...)
    {
        log.error("XMLCommsDeviceConfigParser: Exception raised");
    }

    return CONFMGR_ERROR_CONFIG;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
