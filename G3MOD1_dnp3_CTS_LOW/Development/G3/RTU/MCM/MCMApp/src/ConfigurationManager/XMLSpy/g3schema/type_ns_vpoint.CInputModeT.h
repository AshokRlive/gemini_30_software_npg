#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CInputModeT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CInputModeT



namespace g3schema
{

namespace ns_vpoint
{	

class CInputModeT : public TypeBase
{
public:
	g3schema_EXPORT CInputModeT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CInputModeT(CInputModeT const& init);
	void operator=(CInputModeT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CInputModeT); }
	MemberElement<ns_vpoint::CEdgeDrivenT, _altova_mi_ns_vpoint_altova_CInputModeT_altova_edgeDriven> edgeDriven;
	struct edgeDriven { typedef Iterator<ns_vpoint::CEdgeDrivenT> iterator; };
	MemberElement<ns_vpoint::CPulseDrivenT, _altova_mi_ns_vpoint_altova_CInputModeT_altova_pulseDriven> pulseDriven;
	struct pulseDriven { typedef Iterator<ns_vpoint::CPulseDrivenT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CInputModeT
