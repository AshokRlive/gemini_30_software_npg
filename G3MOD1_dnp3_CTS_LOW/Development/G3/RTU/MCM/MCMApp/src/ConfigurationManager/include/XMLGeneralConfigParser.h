/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       XML based G3 general configuration parser
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15-Oct-2012   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#if !defined(EA_8CB13480_F5FE_44ff_B98B_9F0FCF582A42__INCLUDED_)
#define EA_8CB13480_F5FE_44ff_B98B_9F0FCF582A42__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "XMLParser.h"
#include "IStatusManager.h"
#include "IIOModuleManager.h"
#include "IEventLogManager.h"
#include "UserManager.h"
#include "ConfigurationManagerError.h"
#include "Logger.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief XML Parser for general configuration items
 *
 * Parses the XML Configuration file for general configuration items.
 */
class XMLGeneralConfigParser
{

public:
    /**
     * \brief Custom constructor
     *
     * \param statusMgr Reference to the Status Manager
     * \param MManager Reference to the Module Manager
     * \param Cg3schema XML configuration
     */
	XMLGeneralConfigParser(Cg3schema &configuration);
	virtual ~XMLGeneralConfigParser();

	CONFMGR_ERROR initParser();
	/* \brief Parses user list */
	CONFMGR_ERROR parse(UserManager &userManager);
	/* \brief Parses misc options */
	CONFMGR_ERROR parse(IStatusManager& statusManager,
	                    IIOModuleManager::OLRButtonConfig& cfgOLR
                        );

private:
    Cg3schema &configuration;
    Logger& log;
    IEventLogManager* eventLog;
};


#endif // !defined(EA_8CB13480_F5FE_44ff_B98B_9F0FCF582A42__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
