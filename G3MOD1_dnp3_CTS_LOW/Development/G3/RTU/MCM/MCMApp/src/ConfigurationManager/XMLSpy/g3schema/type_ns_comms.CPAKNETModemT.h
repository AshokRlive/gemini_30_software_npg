#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_comms_ALTOVA_CPAKNETModemT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_comms_ALTOVA_CPAKNETModemT

#include "type_ns_comms.CBaseModemT.h"


namespace g3schema
{

namespace ns_comms
{	

class CPAKNETModemT : public ::g3schema::ns_comms::CBaseModemT
{
public:
	g3schema_EXPORT CPAKNETModemT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CPAKNETModemT(CPAKNETModemT const& init);
	void operator=(CPAKNETModemT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_comms_altova_CPAKNETModemT); }

	MemberAttribute<string_type,_altova_mi_ns_comms_altova_CPAKNETModemT_altova_promptString, 0, 0> promptString;	// promptString CanySimpleType

	MemberAttribute<string_type,_altova_mi_ns_comms_altova_CPAKNETModemT_altova_dialString, 0, 0> dialString;	// dialString Cstring

	MemberAttribute<string_type,_altova_mi_ns_comms_altova_CPAKNETModemT_altova_dialString2, 0, 0> dialString2;	// dialString2 Cstring

	MemberAttribute<unsigned,_altova_mi_ns_comms_altova_CPAKNETModemT_altova_regularCallIntervalMins, 0, 0> regularCallIntervalMins;	// regularCallIntervalMins CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_comms_altova_CPAKNETModemT_altova_modemInactivityTimeoutMins, 0, 0> modemInactivityTimeoutMins;	// modemInactivityTimeoutMins CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_comms_altova_CPAKNETModemT_altova_connectionInactivityTimeoutSecs, 0, 0> connectionInactivityTimeoutSecs;	// connectionInactivityTimeoutSecs CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_comms_altova_CPAKNETModemT_altova_connectTimeoutMs, 0, 0> connectTimeoutMs;	// connectTimeoutMs CunsignedInt

	MemberAttribute<string_type,_altova_mi_ns_comms_altova_CPAKNETModemT_altova_mainNUA, 0, 0> mainNUA;	// mainNUA CNUA

	MemberAttribute<string_type,_altova_mi_ns_comms_altova_CPAKNETModemT_altova_standyNUA, 0, 0> standyNUA;	// standyNUA CNUA
	MemberElement<xs::CunsignedIntType, _altova_mi_ns_comms_altova_CPAKNETModemT_altova_dialRetryIntervalSecs> dialRetryIntervalSecs;
	struct dialRetryIntervalSecs { typedef Iterator<xs::CunsignedIntType> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_comms

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_comms_ALTOVA_CPAKNETModemT
