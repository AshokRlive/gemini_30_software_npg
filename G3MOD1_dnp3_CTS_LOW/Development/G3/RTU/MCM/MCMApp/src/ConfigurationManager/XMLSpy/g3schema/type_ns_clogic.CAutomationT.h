#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CAutomationT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CAutomationT

#include "type_ns_clogic.CCLogicParametersBaseT.h"


namespace g3schema
{

namespace ns_clogic
{	

class CAutomationT : public ::g3schema::ns_clogic::CCLogicParametersBaseT
{
public:
	g3schema_EXPORT CAutomationT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CAutomationT(CAutomationT const& init);
	void operator=(CAutomationT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_clogic_altova_CAutomationT); }

	MemberAttribute<bool,_altova_mi_ns_clogic_altova_CAutomationT_altova_autoStart, 0, 0> autoStart;	// autoStart Cboolean

	MemberAttribute<string_type,_altova_mi_ns_clogic_altova_CAutomationT_altova_sharedLibraryFile, 0, 0> sharedLibraryFile;	// sharedLibraryFile Cstring

	MemberAttribute<int,_altova_mi_ns_clogic_altova_CAutomationT_altova_autoSchemeType, 0, 0> autoSchemeType;	// autoSchemeType Cint

	MemberAttribute<unsigned,_altova_mi_ns_clogic_altova_CAutomationT_altova_autoSchemeVersionMajor, 0, 0> autoSchemeVersionMajor;	// autoSchemeVersionMajor CunsignedByte

	MemberAttribute<unsigned,_altova_mi_ns_clogic_altova_CAutomationT_altova_autoSchemeVersionMinor, 0, 0> autoSchemeVersionMinor;	// autoSchemeVersionMinor CunsignedByte

	MemberAttribute<string_type,_altova_mi_ns_clogic_altova_CAutomationT_altova_autoResName, 0, 0> autoResName;	// autoResName Cstring
	MemberElement<ns_clogic::CAutomationConstantsT, _altova_mi_ns_clogic_altova_CAutomationT_altova_constants> constants;
	struct constants { typedef Iterator<ns_clogic::CAutomationConstantsT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_clogic

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CAutomationT
