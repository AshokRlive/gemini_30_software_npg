#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CSwitchChannelTemplateT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CSwitchChannelTemplateT



namespace g3schema
{

namespace ns_template
{	

class CSwitchChannelTemplateT : public TypeBase
{
public:
	g3schema_EXPORT CSwitchChannelTemplateT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSwitchChannelTemplateT(CSwitchChannelTemplateT const& init);
	void operator=(CSwitchChannelTemplateT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_template_altova_CSwitchChannelTemplateT); }

	MemberAttribute<unsigned,_altova_mi_ns_template_altova_CSwitchChannelTemplateT_altova_overrunMS, 0, 0> overrunMS;	// overrunMS CunsignedShort

	MemberAttribute<unsigned,_altova_mi_ns_template_altova_CSwitchChannelTemplateT_altova_opTimeoutS, 0, 0> opTimeoutS;	// opTimeoutS CunsignedShort

	MemberAttribute<unsigned,_altova_mi_ns_template_altova_CSwitchChannelTemplateT_altova_polarity, 0, 0> polarity;	// polarity CunsignedByte

	MemberAttribute<unsigned,_altova_mi_ns_template_altova_CSwitchChannelTemplateT_altova_pulseLength, 0, 0> pulseLength;	// pulseLength CunsignedShort
	MemberElement<ns_template::CParameterT, _altova_mi_ns_template_altova_CSwitchChannelTemplateT_altova_parameter> parameter;
	struct parameter { typedef Iterator<ns_template::CParameterT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_template

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CSwitchChannelTemplateT
