#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CAnalogueInputChnlT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CAnalogueInputChnlT



namespace g3schema
{

namespace ns_g3module
{	

class CAnalogueInputChnlT : public TypeBase
{
public:
	g3schema_EXPORT CAnalogueInputChnlT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CAnalogueInputChnlT(CAnalogueInputChnlT const& init);
	void operator=(CAnalogueInputChnlT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_g3module_altova_CAnalogueInputChnlT); }

	MemberAttribute<unsigned,_altova_mi_ns_g3module_altova_CAnalogueInputChnlT_altova_channelID, 0, 0> channelID;	// channelID CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_g3module_altova_CAnalogueInputChnlT_altova_eventMs, 0, 0> eventMs;	// eventMs CunsignedInt

	MemberAttribute<bool,_altova_mi_ns_g3module_altova_CAnalogueInputChnlT_altova_eventEnable, 0, 0> eventEnable;	// eventEnable Cboolean

	MemberAttribute<bool,_altova_mi_ns_g3module_altova_CAnalogueInputChnlT_altova_enable, 0, 0> enable;	// enable Cboolean
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_g3module

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CAnalogueInputChnlT
