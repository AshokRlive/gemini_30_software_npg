#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CDelayedEventEntryT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CDelayedEventEntryT



namespace g3schema
{

namespace ns_vpoint
{	

class CDelayedEventEntryT : public TypeBase
{
public:
	g3schema_EXPORT CDelayedEventEntryT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CDelayedEventEntryT(CDelayedEventEntryT const& init);
	void operator=(CDelayedEventEntryT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CDelayedEventEntryT); }

	MemberAttribute<int,_altova_mi_ns_vpoint_altova_CDelayedEventEntryT_altova_eventValue, 0, 0> eventValue;	// eventValue Cint

	MemberAttribute<unsigned,_altova_mi_ns_vpoint_altova_CDelayedEventEntryT_altova_delayPeriodMs, 0, 0> delayPeriodMs;	// delayPeriodMs CunsignedInt
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CDelayedEventEntryT
