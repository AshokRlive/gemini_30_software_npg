#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CSCMMK2ModuleT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CSCMMK2ModuleT

#include "type_ns_g3module.CBaseModuleT.h"


namespace g3schema
{

namespace ns_g3module
{	

class CSCMMK2ModuleT : public ::g3schema::ns_g3module::CBaseModuleT
{
public:
	g3schema_EXPORT CSCMMK2ModuleT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSCMMK2ModuleT(CSCMMK2ModuleT const& init);
	void operator=(CSCMMK2ModuleT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_g3module_altova_CSCMMK2ModuleT); }
	MemberAttribute<string_type,_altova_mi_ns_g3module_altova_CSCMMK2ModuleT_altova_openColour, 0, 2> openColour;	// openColour CLED_COLOUR

	MemberAttribute<unsigned,_altova_mi_ns_g3module_altova_CSCMMK2ModuleT_altova_motorMode, 0, 0> motorMode;	// motorMode CunsignedByte

	MemberAttribute<bool,_altova_mi_ns_g3module_altova_CSCMMK2ModuleT_altova_allowForcedOperation, 0, 0> allowForcedOperation;	// allowForcedOperation Cboolean
	MemberElement<ns_g3module::CSCMChannelsT, _altova_mi_ns_g3module_altova_CSCMMK2ModuleT_altova_channels> channels;
	struct channels { typedef Iterator<ns_g3module::CSCMChannelsT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_g3module

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CSCMMK2ModuleT
