#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CSectionaliserParamT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CSectionaliserParamT

#include "type_ns_clogic.CCLogicParametersBaseT.h"


namespace g3schema
{

namespace ns_clogic
{	

class CSectionaliserParamT : public ::g3schema::ns_clogic::CCLogicParametersBaseT
{
public:
	g3schema_EXPORT CSectionaliserParamT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSectionaliserParamT(CSectionaliserParamT const& init);
	void operator=(CSectionaliserParamT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_clogic_altova_CSectionaliserParamT); }

	MemberAttribute<bool,_altova_mi_ns_clogic_altova_CSectionaliserParamT_altova_defaultEnable, 0, 0> defaultEnable;	// defaultEnable Cboolean

	MemberAttribute<unsigned,_altova_mi_ns_clogic_altova_CSectionaliserParamT_altova_shortReclaimTime, 0, 0> shortReclaimTime;	// shortReclaimTime CunsignedShort

	MemberAttribute<unsigned,_altova_mi_ns_clogic_altova_CSectionaliserParamT_altova_longReclaimTime, 0, 0> longReclaimTime;	// longReclaimTime CunsignedShort

	MemberAttribute<unsigned,_altova_mi_ns_clogic_altova_CSectionaliserParamT_altova_faultCounterThreshold, 0, 0> faultCounterThreshold;	// faultCounterThreshold CunsignedShort
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_clogic

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CSectionaliserParamT
