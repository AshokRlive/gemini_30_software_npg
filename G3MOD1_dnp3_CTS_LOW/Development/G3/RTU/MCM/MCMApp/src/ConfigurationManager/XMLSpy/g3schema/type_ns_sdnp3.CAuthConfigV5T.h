#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CAuthConfigV5T
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CAuthConfigV5T



namespace g3schema
{

namespace ns_sdnp3
{	

class CAuthConfigV5T : public TypeBase
{
public:
	g3schema_EXPORT CAuthConfigV5T(xercesc::DOMNode* const& init);
	g3schema_EXPORT CAuthConfigV5T(CAuthConfigV5T const& init);
	void operator=(CAuthConfigV5T const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_sdnp3_altova_CAuthConfigV5T); }

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CAuthConfigV5T_altova_randomChallengeDataLength, 0, 0> randomChallengeDataLength;	// randomChallengeDataLength CunsignedShort

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CAuthConfigV5T_altova_maxSessionKeyStatusCount, 0, 0> maxSessionKeyStatusCount;	// maxSessionKeyStatusCount CunsignedShort

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CAuthConfigV5T_altova_maxAuthenticationFailures, 0, 0> maxAuthenticationFailures;	// maxAuthenticationFailures CunsignedShort

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CAuthConfigV5T_altova_maxReplyTimeouts, 0, 0> maxReplyTimeouts;	// maxReplyTimeouts CunsignedShort

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CAuthConfigV5T_altova_maxAuthenticationRekeys, 0, 0> maxAuthenticationRekeys;	// maxAuthenticationRekeys CunsignedShort

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CAuthConfigV5T_altova_maxErrorMessagesSent, 0, 0> maxErrorMessagesSent;	// maxErrorMessagesSent CunsignedShort
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_sdnp3

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CAuthConfigV5T
