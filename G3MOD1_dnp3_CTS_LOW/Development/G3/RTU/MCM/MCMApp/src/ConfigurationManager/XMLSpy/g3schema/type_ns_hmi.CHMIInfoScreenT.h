#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CHMIInfoScreenT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CHMIInfoScreenT



namespace g3schema
{

namespace ns_hmi
{	

class CHMIInfoScreenT : public TypeBase
{
public:
	g3schema_EXPORT CHMIInfoScreenT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CHMIInfoScreenT(CHMIInfoScreenT const& init);
	void operator=(CHMIInfoScreenT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_hmi_altova_CHMIInfoScreenT); }

	MemberAttribute<int,_altova_mi_ns_hmi_altova_CHMIInfoScreenT_altova_screenID, 0, 0> screenID;	// screenID Cint

	MemberAttribute<string_type,_altova_mi_ns_hmi_altova_CHMIInfoScreenT_altova_screenTitle, 0, 0> screenTitle;	// screenTitle Cstring

	MemberAttribute<unsigned,_altova_mi_ns_hmi_altova_CHMIInfoScreenT_altova_type, 0, 0> type;	// type CunsignedByte
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_hmi

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CHMIInfoScreenT
