#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen_ALTOVA_CLU_MBLINK_TYPE
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen_ALTOVA_CLU_MBLINK_TYPE



namespace g3schema
{

namespace ns_gen
{	

class CLU_MBLINK_TYPE : public TypeBase
{
public:
	g3schema_EXPORT CLU_MBLINK_TYPE(xercesc::DOMNode* const& init);
	g3schema_EXPORT CLU_MBLINK_TYPE(CLU_MBLINK_TYPE const& init);
	void operator=(CLU_MBLINK_TYPE const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen_altova_CLU_MBLINK_TYPE); }

	enum EnumValues {
		Invalid = -1,
		k_LU_MBLINK_TYPE_TCP = 0, // LU_MBLINK_TYPE_TCP
		k_LU_MBLINK_TYPE_ASCII = 1, // LU_MBLINK_TYPE_ASCII
		k_LU_MBLINK_TYPE_RTU = 2, // LU_MBLINK_TYPE_RTU
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen_ALTOVA_CLU_MBLINK_TYPE
