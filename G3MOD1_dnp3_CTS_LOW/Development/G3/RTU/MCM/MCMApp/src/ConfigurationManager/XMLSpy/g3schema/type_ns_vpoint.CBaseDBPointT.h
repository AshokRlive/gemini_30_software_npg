#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CBaseDBPointT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CBaseDBPointT

#include "type_ns_vpoint.CBaseDigitalPointT.h"


namespace g3schema
{

namespace ns_vpoint
{	

class CBaseDBPointT : public ::g3schema::ns_vpoint::CBaseDigitalPointT
{
public:
	g3schema_EXPORT CBaseDBPointT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CBaseDBPointT(CBaseDBPointT const& init);
	void operator=(CBaseDBPointT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CBaseDBPointT); }
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CBaseDBPointT
