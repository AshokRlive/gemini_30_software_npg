#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen2_ALTOVA_CLU_LINTCPUDP_MODE
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen2_ALTOVA_CLU_LINTCPUDP_MODE



namespace g3schema
{

namespace ns_gen2
{	

class CLU_LINTCPUDP_MODE : public TypeBase
{
public:
	g3schema_EXPORT CLU_LINTCPUDP_MODE(xercesc::DOMNode* const& init);
	g3schema_EXPORT CLU_LINTCPUDP_MODE(CLU_LINTCPUDP_MODE const& init);
	void operator=(CLU_LINTCPUDP_MODE const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen2_altova_CLU_LINTCPUDP_MODE); }

	enum EnumValues {
		Invalid = -1,
		k_LU_LINTCPUDP_MODE_SERVER = 0, // LU_LINTCPUDP_MODE_SERVER
		k_LU_LINTCPUDP_MODE_CLIENT = 1, // LU_LINTCPUDP_MODE_CLIENT
		k_LU_LINTCPUDP_MODE_DUAL_ENDPOINT = 2, // LU_LINTCPUDP_MODE_DUAL_ENDPOINT
		k_LU_LINTCPUDP_MODE_UDP = 3, // LU_LINTCPUDP_MODE_UDP
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen2

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen2_ALTOVA_CLU_LINTCPUDP_MODE
