#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CFPMModuleT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CFPMModuleT

#include "type_ns_g3module.CBaseModuleT.h"


namespace g3schema
{

namespace ns_g3module
{	

class CFPMModuleT : public ::g3schema::ns_g3module::CBaseModuleT
{
public:
	g3schema_EXPORT CFPMModuleT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CFPMModuleT(CFPMModuleT const& init);
	void operator=(CFPMModuleT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_g3module_altova_CFPMModuleT); }
	MemberElement<ns_g3module::CFPMChannelsT, _altova_mi_ns_g3module_altova_CFPMModuleT_altova_channels> channels;
	struct channels { typedef Iterator<ns_g3module::CFPMChannelsT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_g3module

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CFPMModuleT
