#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CHMIDataEntryRef
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CHMIDataEntryRef



namespace g3schema
{

namespace ns_hmi
{	

class CHMIDataEntryRef : public TypeBase
{
public:
	g3schema_EXPORT CHMIDataEntryRef(xercesc::DOMNode* const& init);
	g3schema_EXPORT CHMIDataEntryRef(CHMIDataEntryRef const& init);
	void operator=(CHMIDataEntryRef const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_hmi_altova_CHMIDataEntryRef); }

	MemberAttribute<unsigned,_altova_mi_ns_hmi_altova_CHMIDataEntryRef_altova_entryID, 0, 0> entryID;	// entryID CunsignedShort
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_hmi

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CHMIDataEntryRef
