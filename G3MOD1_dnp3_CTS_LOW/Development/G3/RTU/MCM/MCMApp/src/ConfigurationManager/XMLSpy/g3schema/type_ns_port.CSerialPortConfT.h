#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_port_ALTOVA_CSerialPortConfT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_port_ALTOVA_CSerialPortConfT



namespace g3schema
{

namespace ns_port
{	

class CSerialPortConfT : public TypeBase
{
public:
	g3schema_EXPORT CSerialPortConfT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSerialPortConfT(CSerialPortConfT const& init);
	void operator=(CSerialPortConfT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_port_altova_CSerialPortConfT); }

	MemberAttribute<bool,_altova_mi_ns_port_altova_CSerialPortConfT_altova_enabled, 0, 0> enabled;	// enabled Cboolean

	MemberAttribute<string_type,_altova_mi_ns_port_altova_CSerialPortConfT_altova_portName, 0, 0> portName;	// portName Cstring

	MemberAttribute<string_type,_altova_mi_ns_port_altova_CSerialPortConfT_altova_portPath, 0, 0> portPath;	// portPath Cstring
	MemberAttribute<string_type,_altova_mi_ns_port_altova_CSerialPortConfT_altova_baudRate, 0, 15> baudRate;	// baudRate CLU_LIN232_BAUD_RATE
	MemberAttribute<string_type,_altova_mi_ns_port_altova_CSerialPortConfT_altova_numDataBits, 0, 2> numDataBits;	// numDataBits CLU_LIN232_DATA_BITS
	MemberAttribute<string_type,_altova_mi_ns_port_altova_CSerialPortConfT_altova_numStopBits, 0, 2> numStopBits;	// numStopBits CLU_LIN232_STOP_BITS
	MemberAttribute<string_type,_altova_mi_ns_port_altova_CSerialPortConfT_altova_parity, 0, 3> parity;	// parity CLU_LIN232_PARITY
	MemberAttribute<string_type,_altova_mi_ns_port_altova_CSerialPortConfT_altova_portMode, 0, 2> portMode;	// portMode CLU_LIN232_PORT_MODE

	MemberAttribute<bool,_altova_mi_ns_port_altova_CSerialPortConfT_altova_ctLowDiscardMsg, 0, 0> ctLowDiscardMsg;	// ctLowDiscardMsg Cboolean
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_port

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_port_ALTOVA_CSerialPortConfT
