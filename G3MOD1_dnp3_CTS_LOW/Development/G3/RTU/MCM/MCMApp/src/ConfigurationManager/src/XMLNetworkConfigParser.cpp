/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       XML based G3 general configuration parser implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15-Oct-2012   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <sstream>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "XMLNetworkConfigParser.h"
#include "versions.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

XMLNetworkConfigParser::XMLNetworkConfigParser(Cg3schema& configuration) :
                                            configuration(configuration),
                                            log(Logger::getLogger(SUBSYSTEM_ID_CONFMGR))
{}

XMLNetworkConfigParser::~XMLNetworkConfigParser()
{}


CONFMGR_ERROR XMLNetworkConfigParser::initParser()
{
//
//    try
//    {
//        /* Get a reference to the main configuration */
//        CConfigurationType configurationRoot = configuration.configuration.first();
//
//        // Stop all PPPD threads
//        system("killall -q pppd");
//
//        /* XXX: PW - remove it when added directly in the JFFS2 filesystem (to be removed) */
//        system("mkdir -p /etc/ppp/peers");
//
//        // Delete previous peer scripts
//        system("rm /etc/ppp/peers/*");
//
//        for(Iterator<CPPPType2> ppp = configurationRoot.network.first().PPP.all();
//                                ppp;
//                                ++ppp
//                              )
//        {
//        	lu_bool_t writeScript = false;
//
//        	/*Write PPP script file from configuration to linux system*/
//		   for(Iterator<CScriptFileType> scriptFile = ppp.ScriptFile.all();
//										scriptFile;
//										++scriptFile
//									  )
//			{
//
//		       	//Check fields
//				if(!scriptFile.filePath.exists() || !scriptFile.content.exists())
//				{
//					log.error("PPP script ignored: invalid script configuration.");
//					continue;
//				}
//
//				FILE * fp;
//				std::string filepath;
//				filepath = scriptFile.filePath;
//				filepath = "/etc/ppp/" + filepath;
//
//				// Write script file
//				fp = fopen(filepath.c_str(),"w");
//				if(fp != NULL)
//				{
//					std::string content;
//					content = scriptFile.content.first();
//					fwrite(content.c_str(),content.size(), 1, fp);
//					fclose(fp);
//					writeScript = true;
//				}
//				else
//				{
//					log.error("Fail to write chat script file: %s", filepath.c_str());
//				}
//			}
//
//		   // Start PPPD threads
//			if(writeScript)
//			{
//				std::string sysComman;
//				std::string sysCommand;
//				sysCommand = "for script in `ls /etc/ppp/peers/`; do pppd call $script; echo Starting $script; done;echo PPP threads started!";
//				system(sysCommand.c_str());
//			}
//        }
//
//    }
//    catch (...)
//    {
//        log.error("XMLNetworkConfigParser::initParser Exception raised");
//        return CONFMGR_ERROR_INITPARSER;
//    }
//
    return CONFMGR_ERROR_NONE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */

