#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen0_ALTOVA_CMODULE_BOARD_STATUS
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen0_ALTOVA_CMODULE_BOARD_STATUS



namespace g3schema
{

namespace ns_gen0
{	

class CMODULE_BOARD_STATUS : public TypeBase
{
public:
	g3schema_EXPORT CMODULE_BOARD_STATUS(xercesc::DOMNode* const& init);
	g3schema_EXPORT CMODULE_BOARD_STATUS(CMODULE_BOARD_STATUS const& init);
	void operator=(CMODULE_BOARD_STATUS const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen0_altova_CMODULE_BOARD_STATUS); }

	enum EnumValues {
		Invalid = -1,
		k_MODULE_BOARD_STATUS_INVALID = 0, // MODULE_BOARD_STATUS_INVALID
		k_MODULE_BOARD_STATUS_WAIT_INIT = 1, // MODULE_BOARD_STATUS_WAIT_INIT
		k_MODULE_BOARD_STATUS_STARTING = 2, // MODULE_BOARD_STATUS_STARTING
		k_MODULE_BOARD_STATUS_ON_LINE = 3, // MODULE_BOARD_STATUS_ON_LINE
		k_MODULE_BOARD_STATUS_BOOT_POST = 4, // MODULE_BOARD_STATUS_BOOT_POST
		k_MODULE_BOARD_STATUS_BOOT_READY = 5, // MODULE_BOARD_STATUS_BOOT_READY
		k_MODULE_BOARD_STATUS_APP_BOOTL_INIT = 6, // MODULE_BOARD_STATUS_APP_BOOTL_INIT
		k_MODULE_BOARD_STATUS_APP_BOOTL_PROG = 7, // MODULE_BOARD_STATUS_APP_BOOTL_PROG
		k_MODULE_BOARD_STATUS_APP_BOOTL_OK = 8, // MODULE_BOARD_STATUS_APP_BOOTL_OK
		k_MODULE_BOARD_STATUS_APP_BOOTL_FAIL = 9, // MODULE_BOARD_STATUS_APP_BOOTL_FAIL
		k_MODULE_BOARD_STATUS_ERROR = 10, // MODULE_BOARD_STATUS_ERROR
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen0

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen0_ALTOVA_CMODULE_BOARD_STATUS
