#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CFailoverGroupT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CFailoverGroupT



namespace g3schema
{

namespace ns_sdnp3
{	

class CFailoverGroupT : public TypeBase
{
public:
	g3schema_EXPORT CFailoverGroupT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CFailoverGroupT(CFailoverGroupT const& init);
	void operator=(CFailoverGroupT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_sdnp3_altova_CFailoverGroupT); }

	MemberAttribute<string_type,_altova_mi_ns_sdnp3_altova_CFailoverGroupT_altova_ipAddress, 0, 0> ipAddress;	// ipAddress Cstring

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CFailoverGroupT_altova_port, 0, 0> port;	// port CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CFailoverGroupT_altova_masterAddress, 0, 0> masterAddress;	// masterAddress CunsignedInt
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_sdnp3

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CFailoverGroupT
