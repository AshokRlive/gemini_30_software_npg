#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s101_ALTOVA_CS101SessionT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s101_ALTOVA_CS101SessionT



namespace g3schema
{

namespace ns_s101
{	

class CS101SessionT : public TypeBase
{
public:
	g3schema_EXPORT CS101SessionT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CS101SessionT(CS101SessionT const& init);
	void operator=(CS101SessionT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_s101_altova_CS101SessionT); }
	MemberElement<ns_s101::CS101SesnConfT, _altova_mi_ns_s101_altova_CS101SessionT_altova_config> config;
	struct config { typedef Iterator<ns_s101::CS101SesnConfT> iterator; };
	MemberElement<ns_iec870::CIEC870PointsT, _altova_mi_ns_s101_altova_CS101SessionT_altova_iomap> iomap;
	struct iomap { typedef Iterator<ns_iec870::CIEC870PointsT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_s101

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s101_ALTOVA_CS101SessionT
