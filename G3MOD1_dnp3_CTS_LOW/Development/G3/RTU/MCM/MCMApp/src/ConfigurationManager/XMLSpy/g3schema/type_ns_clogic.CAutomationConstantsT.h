#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CAutomationConstantsT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CAutomationConstantsT



namespace g3schema
{

namespace ns_clogic
{	

class CAutomationConstantsT : public TypeBase
{
public:
	g3schema_EXPORT CAutomationConstantsT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CAutomationConstantsT(CAutomationConstantsT const& init);
	void operator=(CAutomationConstantsT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_clogic_altova_CAutomationConstantsT); }
	MemberElement<ns_clogic::CAutomationConstantT, _altova_mi_ns_clogic_altova_CAutomationConstantsT_altova_constant> constant;
	struct constant { typedef Iterator<ns_clogic::CAutomationConstantT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_clogic

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CAutomationConstantsT
