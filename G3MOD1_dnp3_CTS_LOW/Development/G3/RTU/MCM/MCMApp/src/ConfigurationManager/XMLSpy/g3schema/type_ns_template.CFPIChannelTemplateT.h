#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CFPIChannelTemplateT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CFPIChannelTemplateT

#include "type_ns_g3module.CFPIChannelT.h"


namespace g3schema
{

namespace ns_template
{	

class CFPIChannelTemplateT : public ::g3schema::ns_g3module::CFPIChannelT
{
public:
	g3schema_EXPORT CFPIChannelTemplateT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CFPIChannelTemplateT(CFPIChannelTemplateT const& init);
	void operator=(CFPIChannelTemplateT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_template_altova_CFPIChannelTemplateT); }

	MemberAttribute<string_type,_altova_mi_ns_template_altova_CFPIChannelTemplateT_altova_profileName, 0, 0> profileName;	// profileName Cstring

	MemberAttribute<string_type,_altova_mi_ns_template_altova_CFPIChannelTemplateT_altova_uuid, 0, 0> uuid;	// uuid Cstring

	MemberAttribute<bool,_altova_mi_ns_template_altova_CFPIChannelTemplateT_altova_resetFPIOnLVRestored, 0, 0> resetFPIOnLVRestored;	// resetFPIOnLVRestored Cboolean

	MemberAttribute<bool,_altova_mi_ns_template_altova_CFPIChannelTemplateT_altova_resetFPIOnLVLost, 0, 0> resetFPIOnLVLost;	// resetFPIOnLVLost Cboolean
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_template

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CFPIChannelTemplateT
