#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen2_ALTOVA_CLU_LIN232_BAUD_RATE
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen2_ALTOVA_CLU_LIN232_BAUD_RATE



namespace g3schema
{

namespace ns_gen2
{	

class CLU_LIN232_BAUD_RATE : public TypeBase
{
public:
	g3schema_EXPORT CLU_LIN232_BAUD_RATE(xercesc::DOMNode* const& init);
	g3schema_EXPORT CLU_LIN232_BAUD_RATE(CLU_LIN232_BAUD_RATE const& init);
	void operator=(CLU_LIN232_BAUD_RATE const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen2_altova_CLU_LIN232_BAUD_RATE); }

	enum EnumValues {
		Invalid = -1,
		k_LU_LIN232_BAUD_110 = 0, // LU_LIN232_BAUD_110
		k_LU_LIN232_BAUD_300 = 1, // LU_LIN232_BAUD_300
		k_LU_LIN232_BAUD_600 = 2, // LU_LIN232_BAUD_600
		k_LU_LIN232_BAUD_1200 = 3, // LU_LIN232_BAUD_1200
		k_LU_LIN232_BAUD_2400 = 4, // LU_LIN232_BAUD_2400
		k_LU_LIN232_BAUD_4800 = 5, // LU_LIN232_BAUD_4800
		k_LU_LIN232_BAUD_9600 = 6, // LU_LIN232_BAUD_9600
		k_LU_LIN232_BAUD_19200 = 7, // LU_LIN232_BAUD_19200
		k_LU_LIN232_BAUD_38400 = 8, // LU_LIN232_BAUD_38400
		k_LU_LIN232_BAUD_57600 = 9, // LU_LIN232_BAUD_57600
		k_LU_LIN232_BAUD_115200 = 10, // LU_LIN232_BAUD_115200
		k_LU_LIN232_BAUD_230400 = 11, // LU_LIN232_BAUD_230400
		k_LU_LIN232_BAUD_576000 = 12, // LU_LIN232_BAUD_576000
		k_LU_LIN232_BAUD_921600 = 13, // LU_LIN232_BAUD_921600
		k_LU_LIN232_BAUD_1152000 = 14, // LU_LIN232_BAUD_1152000
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen2

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen2_ALTOVA_CLU_LIN232_BAUD_RATE
