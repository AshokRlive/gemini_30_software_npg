#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CDSMModuleT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CDSMModuleT

#include "type_ns_g3module.CBaseModuleT.h"


namespace g3schema
{

namespace ns_g3module
{	

class CDSMModuleT : public ::g3schema::ns_g3module::CBaseModuleT
{
public:
	g3schema_EXPORT CDSMModuleT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CDSMModuleT(CDSMModuleT const& init);
	void operator=(CDSMModuleT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_g3module_altova_CDSMModuleT); }
	MemberAttribute<string_type,_altova_mi_ns_g3module_altova_CDSMModuleT_altova_openColour, 0, 2> openColour;	// openColour CLED_COLOUR

	MemberAttribute<unsigned,_altova_mi_ns_g3module_altova_CDSMModuleT_altova_polarityA, 0, 0> polarityA;	// polarityA CunsignedByte

	MemberAttribute<unsigned,_altova_mi_ns_g3module_altova_CDSMModuleT_altova_polarityB, 0, 0> polarityB;	// polarityB CunsignedByte

	MemberAttribute<bool,_altova_mi_ns_g3module_altova_CDSMModuleT_altova_supplyEnable24V, 0, 0> supplyEnable24V;	// supplyEnable24V Cboolean

	MemberAttribute<bool,_altova_mi_ns_g3module_altova_CDSMModuleT_altova_allowForcedOperationA, 0, 0> allowForcedOperationA;	// allowForcedOperationA Cboolean

	MemberAttribute<bool,_altova_mi_ns_g3module_altova_CDSMModuleT_altova_allowForcedOperationB, 0, 0> allowForcedOperationB;	// allowForcedOperationB Cboolean
	MemberElement<ns_g3module::CSCMChannelsT, _altova_mi_ns_g3module_altova_CDSMModuleT_altova_channels> channels;
	struct channels { typedef Iterator<ns_g3module::CSCMChannelsT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_g3module

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CDSMModuleT
