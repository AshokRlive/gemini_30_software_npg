#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen10_ALTOVA_CLINKCNFM
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen10_ALTOVA_CLINKCNFM



namespace g3schema
{

namespace ns_gen10
{	

class CLINKCNFM : public TypeBase
{
public:
	g3schema_EXPORT CLINKCNFM(xercesc::DOMNode* const& init);
	g3schema_EXPORT CLINKCNFM(CLINKCNFM const& init);
	void operator=(CLINKCNFM const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen10_altova_CLINKCNFM); }

	enum EnumValues {
		Invalid = -1,
		k_LINKCNFM_NEVER = 0, // LINKCNFM_NEVER
		k_LINKCNFM_ALWAYS = 1, // LINKCNFM_ALWAYS
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen10

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen10_ALTOVA_CLINKCNFM
