#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CDummySwitchParamT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CDummySwitchParamT

#include "type_ns_clogic.CCLogicParametersBaseT.h"


namespace g3schema
{

namespace ns_clogic
{	

class CDummySwitchParamT : public ::g3schema::ns_clogic::CCLogicParametersBaseT
{
public:
	g3schema_EXPORT CDummySwitchParamT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CDummySwitchParamT(CDummySwitchParamT const& init);
	void operator=(CDummySwitchParamT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_clogic_altova_CDummySwitchParamT); }

	MemberAttribute<string_type,_altova_mi_ns_clogic_altova_CDummySwitchParamT_altova_openColour, 0, 0> openColour;	// openColour Cstring

	MemberAttribute<unsigned,_altova_mi_ns_clogic_altova_CDummySwitchParamT_altova_interval, 0, 0> interval;	// interval CunsignedInt

	MemberAttribute<bool,_altova_mi_ns_clogic_altova_CDummySwitchParamT_altova_allowForcedOperation, 0, 0> allowForcedOperation;	// allowForcedOperation Cboolean
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_clogic

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CDummySwitchParamT
