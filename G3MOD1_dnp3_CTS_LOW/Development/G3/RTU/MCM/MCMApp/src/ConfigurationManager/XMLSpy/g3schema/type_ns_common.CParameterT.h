#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_CParameterT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_CParameterT



namespace g3schema
{

namespace ns_common
{	

class CParameterT : public TypeBase
{
public:
	g3schema_EXPORT CParameterT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CParameterT(CParameterT const& init);
	void operator=(CParameterT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_common_altova_CParameterT); }

	MemberAttribute<string_type,_altova_mi_ns_common_altova_CParameterT_altova_parameterName, 0, 0> parameterName;	// parameterName Cstring

	MemberAttribute<string_type,_altova_mi_ns_common_altova_CParameterT_altova_parameterValue, 0, 0> parameterValue;	// parameterValue CanySimpleType
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_common

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_CParameterT
