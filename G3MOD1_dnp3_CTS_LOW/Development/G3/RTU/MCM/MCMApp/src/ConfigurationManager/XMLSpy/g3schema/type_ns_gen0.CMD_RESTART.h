#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen0_ALTOVA_CMD_RESTART
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen0_ALTOVA_CMD_RESTART



namespace g3schema
{

namespace ns_gen0
{	

class CMD_RESTART : public TypeBase
{
public:
	g3schema_EXPORT CMD_RESTART(xercesc::DOMNode* const& init);
	g3schema_EXPORT CMD_RESTART(CMD_RESTART const& init);
	void operator=(CMD_RESTART const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen0_altova_CMD_RESTART); }

	enum EnumValues {
		Invalid = -1,
		k_MD_RESTART_WARM = 0, // MD_RESTART_WARM
		k_MD_RESTART_COLD = 1, // MD_RESTART_COLD
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen0

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen0_ALTOVA_CMD_RESTART
