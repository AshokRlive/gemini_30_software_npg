#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_port_ALTOVA_CIpv4SettingT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_port_ALTOVA_CIpv4SettingT



namespace g3schema
{

namespace ns_port
{	

class CIpv4SettingT : public TypeBase
{
public:
	g3schema_EXPORT CIpv4SettingT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CIpv4SettingT(CIpv4SettingT const& init);
	void operator=(CIpv4SettingT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_port_altova_CIpv4SettingT); }
	MemberElement<ns_port::CStaticIpConfT, _altova_mi_ns_port_altova_CIpv4SettingT_altova_staticIP> staticIP;
	struct staticIP { typedef Iterator<ns_port::CStaticIpConfT> iterator; };
	MemberElement<ns_port::CDynamicIpConfT, _altova_mi_ns_port_altova_CIpv4SettingT_altova_dynamicIP> dynamicIP;
	struct dynamicIP { typedef Iterator<ns_port::CDynamicIpConfT> iterator; };
	MemberElement<ns_port::CDhcpServerT, _altova_mi_ns_port_altova_CIpv4SettingT_altova_dhcpServer> dhcpServer;
	struct dhcpServer { typedef Iterator<ns_port::CDhcpServerT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_port

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_port_ALTOVA_CIpv4SettingT
