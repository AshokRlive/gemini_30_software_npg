#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_mmb_ALTOVA_CMMBDeviceChnlAnalogT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_mmb_ALTOVA_CMMBDeviceChnlAnalogT

#include "type_ns_mmb.CMMBDeviceChnlT.h"


namespace g3schema
{

namespace ns_mmb
{	

class CMMBDeviceChnlAnalogT : public ::g3schema::ns_mmb::CMMBDeviceChnlT
{
public:
	g3schema_EXPORT CMMBDeviceChnlAnalogT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CMMBDeviceChnlAnalogT(CMMBDeviceChnlAnalogT const& init);
	void operator=(CMMBDeviceChnlAnalogT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_mmb_altova_CMMBDeviceChnlAnalogT); }

	MemberAttribute<double,_altova_mi_ns_mmb_altova_CMMBDeviceChnlAnalogT_altova_scalingFactor, 0, 0> scalingFactor;	// scalingFactor Cdouble

	MemberAttribute<double,_altova_mi_ns_mmb_altova_CMMBDeviceChnlAnalogT_altova_offset, 0, 0> offset;	// offset Cdouble

	MemberAttribute<string_type,_altova_mi_ns_mmb_altova_CMMBDeviceChnlAnalogT_altova_unit, 0, 0> unit;	// unit Cstring
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_mmb

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_mmb_ALTOVA_CMMBDeviceChnlAnalogT
