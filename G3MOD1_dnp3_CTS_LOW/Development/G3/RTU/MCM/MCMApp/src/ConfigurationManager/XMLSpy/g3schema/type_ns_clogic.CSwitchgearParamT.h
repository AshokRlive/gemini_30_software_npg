#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CSwitchgearParamT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CSwitchgearParamT

#include "type_ns_clogic.CCLogicParametersBaseT.h"


namespace g3schema
{

namespace ns_clogic
{	

class CSwitchgearParamT : public ::g3schema::ns_clogic::CCLogicParametersBaseT
{
public:
	g3schema_EXPORT CSwitchgearParamT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSwitchgearParamT(CSwitchgearParamT const& init);
	void operator=(CSwitchgearParamT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_clogic_altova_CSwitchgearParamT); }

	MemberAttribute<unsigned,_altova_mi_ns_clogic_altova_CSwitchgearParamT_altova_remoteDelay, 0, 0> remoteDelay;	// remoteDelay CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_clogic_altova_CSwitchgearParamT_altova_localDelay, 0, 0> localDelay;	// localDelay CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_clogic_altova_CSwitchgearParamT_altova_motorCurrentThreshold, 0, 0> motorCurrentThreshold;	// motorCurrentThreshold CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_clogic_altova_CSwitchgearParamT_altova_motorOvercurrentTime, 0, 0> motorOvercurrentTime;	// motorOvercurrentTime CunsignedInt

	MemberAttribute<int,_altova_mi_ns_clogic_altova_CSwitchgearParamT_altova_preOperationTime, 0, 0> preOperationTime;	// preOperationTime Cint

	MemberAttribute<int,_altova_mi_ns_clogic_altova_CSwitchgearParamT_altova_postOperationTime, 0, 0> postOperationTime;	// postOperationTime Cint

	MemberAttribute<bool,_altova_mi_ns_clogic_altova_CSwitchgearParamT_altova_isCircuitBreaker, 0, 0> isCircuitBreaker;	// isCircuitBreaker Cboolean

	MemberAttribute<bool,_altova_mi_ns_clogic_altova_CSwitchgearParamT_altova_showOpenWhenEarthed, 0, 0> showOpenWhenEarthed;	// showOpenWhenEarthed Cboolean
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_clogic

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CSwitchgearParamT
