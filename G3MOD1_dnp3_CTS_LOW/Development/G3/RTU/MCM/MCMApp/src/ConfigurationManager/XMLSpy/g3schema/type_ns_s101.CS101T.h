#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s101_ALTOVA_CS101T
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s101_ALTOVA_CS101T



namespace g3schema
{

namespace ns_s101
{	

class CS101T : public TypeBase
{
public:
	g3schema_EXPORT CS101T(xercesc::DOMNode* const& init);
	g3schema_EXPORT CS101T(CS101T const& init);
	void operator=(CS101T const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_s101_altova_CS101T); }
	MemberElement<ns_s101::CS101ChannelT, _altova_mi_ns_s101_altova_CS101T_altova_channel> channel;
	struct channel { typedef Iterator<ns_s101::CS101ChannelT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_s101

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s101_ALTOVA_CS101T
