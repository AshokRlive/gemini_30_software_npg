/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       XML based G3 control logic factory interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/09/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_3DB8E67B_75CC_4be2_BC8C_383B73EDC4B9__INCLUDED_)
#define EA_3DB8E67B_75CC_4be2_BC8C_383B73EDC4B9__INCLUDED_D

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <iostream>
#include <string>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "MCMConfigEnum.h"
#include "ConfigurationManagerError.h"
#include "IStatusManager.h"
#include "IMemoryManager.h"
#include "IIOModuleManager.h"
#include "IControlLogicFactory.h"
#include "XMLParser_Points.h"
#include "../../Automation/include/AutomationLogic.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class XMLControlLogicFactory : public IControlLogicFactory
{

public:

    /**
     * \brief Custom constructor
     *
     * The initParser method is automatically called
     *
     * \param statusMgr Reference to the Status Manager
     * \param memMgr Reference to the Memory Manager
     * \param MManager Reference to the Module Manager
     * \param configuration Configuration file parser handler
     */
    XMLControlLogicFactory(GeminiDatabase& g3database,
                           IIOModuleManager& MManager,
                           IStatusManager& statusMgr,
                           IMemoryManager& memMgr,
                           Cg3schema& configuration
                          );

    virtual ~XMLControlLogicFactory();

    /* == Inherited from IControlLogicFactory == */
    virtual void setMutexLock(Mutex& mutex);
    virtual lu_uint16_t getCLogicNumber();
    virtual ControlLogic* newCLogic(lu_uint16_t group);

private:
    /* \brief generic function pointer for XXX_INPUT_isMandatory() calls */
    typedef lu_bool_t (*IsMandatoryFuncPtr)(const lu_int32_t enumValue);

private:
    /**
     * \brief Initialise the parser
     *
     * This function must be called before the getPointNumber and
     * newPoint methods
     *
     * \return Error code
     */
    CONFMGR_ERROR initParser();

    /**
     * \brief Parse the correct Off/Local/Remote control logic block
     *
     * \param cLogic Control logic XML field containing the configuration
     * \param group Control logic group ID
     *
     * \return New Control Logic Object, or NULL if it failed.
     */
    ControlLogic* parseOLRConfig(CControlLogicT& cLogic, const lu_uint16_t group);

    /**
     * \brief Parse the correct Logic Gate control logic block
     *
     * \param cLogic Control logic XML field containing the configuration
     * \param group Control logic group ID
     *
     * \return New Control Logic Object, or NULL if it failed.
     */
    ControlLogic* parseLGTConfig(CControlLogicT& cLogic, const lu_uint16_t group);

    /**
     * \brief Parse the correct Switch Gear control logic block
     *
     * \param group Control logic group ID to search
     * \param SGLConf Buffer where the XML configuration is saved
     *
     * \return New Control Logic Object, or NULL if it failed.
     */
    ControlLogic* parseSGLConfig(CControlLogicT& cLogic, const lu_uint16_t group);

    /**
     * \brief Parse the correct Dummy Switch control logic block
     *
     * \param group Control logic group ID to search
     * \param DSLConf Buffer where the XML configuration is saved
     *
     * \return New Control Logic Object, or NULL if it failed.
     */
    ControlLogic* parseDummySwConfig(CControlLogicT& cLogic, const lu_uint16_t group);

    /**
     * \brief Parse the correct Digital Output control logic block
     *
     * \param group Control logic group ID to search
     * \param DOLConf Buffer where the XML configuration is saved
     *
     * \return New Control Logic Object, or NULL if it failed.
     */
    ControlLogic* parseDOLConfig(CControlLogicT& cLogic, const lu_uint16_t group);

    /**
     * \brief Parse the correct Battery Charger control logic block
     *
     * \param group Control logic group ID to search
     * \param BTCConf Buffer where the XML configuration is saved
     *
     * \return New Control Logic Object, or NULL if it failed.
     */
    ControlLogic* parseBatteryChargerConfig(CControlLogicT& cLogic, const lu_uint16_t group);

    /**
     * \brief Parse the correct Fan Test control logic block
     *
     * \param group Control logic group ID to search
     * \param FTConf Buffer where the XML configuration is saved
     *
     * \return New Control Logic Object, or NULL if it failed.
     */
    ControlLogic* parseFanTestConfig(CControlLogicT& cLogic, const lu_uint16_t group);

    /**
     * \brief Parse the correct Max/Min/Average control logic block
     *
     * \param group Control logic group ID to search
     * \param config Buffer where the XML configuration is saved
     *
     * \return New Control Logic Object, or NULL if it failed.
     */
    ControlLogic* parseMMAvgConfig(CControlLogicT& cLogic, const lu_uint16_t group);

    /**
     * \brief Parse the correct Pseudo Clock control logic block
     *
     * \param group Control logic group ID to search
     * \param config Buffer where the XML configuration is saved
     *
     * \return New Control Logic Object, or NULL if it failed.
     */
    ControlLogic* parsePCLKConfig(CControlLogicT& cLogic, const lu_uint16_t group);

    /**
     * \brief Parse the correct Parasitic Load Trip Unit control logic block
     *
     * \param group Control logic group ID to search
     * \param config Buffer where the XML configuration is saved
     *
     * \return New Control Logic Object, or NULL if it failed.
     */
    ControlLogic* parsePLTUConfig(CControlLogicT& cLogic, const lu_uint16_t group);

    /**
     * \brief Parse the correct FPI Test control logic block
     *
     * \param group Control logic group ID to search
     * \param config Buffer where the XML configuration is saved
     *
     * \return New Control Logic Object, or NULL if it failed.
     */
    ControlLogic* parseFPITestConfig(CControlLogicT& cLogic, const lu_uint16_t group);

    /**
     * \brief Parse the correct FPI Reset control logic block
     *
     * \param group Control logic group ID to search
     * \param config Buffer where the XML configuration is saved
     *
     * \return New Control Logic Object, or NULL if it failed.
     */
    ControlLogic* parseFPIResetConfig(CControlLogicT& cLogic, const lu_uint16_t group);

    /**
     * \brief Parse the correct Digital/Analogue Control Point control logic block
     *
     * \param group Control logic group ID to search
     * \param config Buffer where the XML configuration is saved
     *
     * \return New Control Logic Object, or NULL if it failed.
     */
    ControlLogic* parseDCPConfig(CControlLogicT& cLogic, const lu_uint16_t group);
    ControlLogic* parseACPConfig(CControlLogicT& cLogic, const lu_uint16_t group);

    /**
     * \brief Parse the configuration and create Threshold Control Logic.
     *
     * \param cLogic Control logic XML field containing the configuration
     * \param group Control logic group ID to search
     *
     * \return New Control Logic Object, or NULL if it failed.
     */
    ControlLogic* parseThresholdConfig(CControlLogicT& cLogic, const lu_uint16_t group);

    /**
     * \brief Parse the configuration and create Action Control Logic.
     *
     * \param cLogic Control logic XML field containing the configuration
     * \param group Control logic group ID to search
     *
     * \return New Control Logic Object, or NULL if it failed.
     */
    ControlLogic* parseActionConfig(CControlLogicT& cLogic, const lu_uint16_t group);

    /**
     * \brief Parse the configuration and create Counter Command Control Logic.
     *
     * \param cLogic Control logic XML field containing the configuration
     * \param group Control logic group ID to search
     *
     * \return New Control Logic Object, or NULL if it failed.
     */
    ControlLogic* parseCounterCommandConfig(CControlLogicT& cLogic, const lu_uint16_t group);

    /**
     * \brief Parse the configuration and create Double-to-Single Binary Point Control Logic.
     *
     * \param cLogic Control logic XML field containing the configuration
     * \param group Control logic group ID to search
     *
     * \return New Control Logic Object, or NULL if it failed.
     */
    ControlLogic* parseDBtoSBConfig(CControlLogicT& cLogic, const lu_uint16_t group);

    /**
     * \brief Parse the configuration and create Binary-to-Analogue Binary (bitfield) Control Logic.
     *
     * \param cLogic Control logic XML field containing the configuration
     * \param group Control logic group ID to search
     *
     * \return New Control Logic Object, or NULL if it failed.
     */
    ControlLogic* parseAnaBitfieldConfig(CControlLogicT& cLogic, const lu_uint16_t group);

    /**
     * \brief Parse the configuration and create Power Supply Controller Logic.
     *
     * \param cLogic Control logic XML field containing the configuration
     * \param group Control logic group ID to search
     *
     * \return New Control Logic Object, or NULL if it failed.
     */
    ControlLogic* parsePowerSupplyConfig(CControlLogicT& cLogic, const lu_uint16_t group);

    /**
     * \brief Parse the configuration and create Motor Supply Controller Logic.
     *
     * \param cLogic Control logic XML field containing the configuration
     * \param group Control logic group ID to search
     *
     * \return New Control Logic Object, or NULL if it failed.
     */
    ControlLogic* parseMotorSupplyConfig(CControlLogicT& cLogic, const lu_uint16_t group);

    /**
     * \brief Parse the configuration and create Automation Control Logic.
     *
     * \param cLogic Control logic XML field containing the configuration
     * \param group Control logic group ID to search
     *
     * \return New Control Logic Object, or NULL if it failed.
     */
    ControlLogic* parseAutomationConfig(CControlLogicT& cLogic, const lu_uint16_t group);

    /**
     * \brief support function used to obtain the Open & Close channels.
     *
     * This function obtains 2 channels from the module manager, or throws an
     * exception otherwise.
     *
     * \param group Control logic group
     * \param moduleID ID of the module
     * \param moduleSwitch ID of the switch of the Module (ignored for 1-switch modules)
     * \param channelOpen  Store here the retrieved Open channel (pointer is modified!)
     * \param channelClose Store here the retrieved Close channel (pointer is modified!)
     *
     * \throw Always a ParserException with an appropiate message.
     */
    void getOpenCloseChannels(const lu_uint16_t group,
                                    const IOModuleIDStr moduleID,
                                    const lu_uint32_t moduleSwitch,
                                    IChannel*& channelOpen,
                                    IChannel*& channelClose
                                    ) throw(ParserException);

    /**
     * \brief support function used to retrieve a channel.
     *
     * This function obtains a channel from the module manager, or NULL otherwise.
     * It logs an error if the channel is not found.
     *
     * \param group Control logic group
     * \param chType Channel type
     * \param channelRef Reference of the channel, being Module type & ID, and Channel Number
     *
     * \return Pointer to obtained channel, or NULL when not found
     */
    IChannel* retrieveChannel(lu_uint16_t group,
                              CHANNEL_TYPE chType,
                              ChannelRef channelRef
                             );

    /**
     * \brief support function used to get Control Logic's input points.
     *
     * \param cLogic Control logic XML field containing the configuration.
     * \param dInput Point array where to store point config.
     * \param size Size of the given Point array
     *
     * \throw Always a ParserException with an appropiate message.
     */
    void getInputVPoints(CControlLogicT& cLogic,
                         PointIdStr dInput[],
                         const lu_uint32_t size,
                         IsMandatoryFuncPtr isMandatory) throw(ParserException);

    /**
     * \brief Fills and issues a ParserException for a missing mandatory field.
     *
     * \param pointID Point ID
     *
     * \throw Always a ParserException with a "missing mandatory input" message.
     */
    void mandatoryInputFailMsg(const PointIdStr pointID) throw(ParserException);

    /**
     * \brief Log a Control Logic Configuration error
     *
     * \param group Control Logic group number
     * \param cLogicType Control Logic type
     * \param text Error text to display
     */
    void logConfigError(const lu_uint16_t group, const lu_uint16_t cLogicType, const char* text);

private:
    GeminiDatabase& database;
    IIOModuleManager& MManager;
    IStatusManager& statusManager;
    IMemoryManager& memoryManager;
    Logger& log;
    Cg3schema &configuration;

    lu_uint16_t cLogicNum;    //Total amount of control logics
};


#endif // !defined(EA_3DB8E67B_75CC_4be2_BC8C_383B73EDC4B9__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
