#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CSCMChannelsT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CSCMChannelsT



namespace g3schema
{

namespace ns_g3module
{	

class CSCMChannelsT : public TypeBase
{
public:
	g3schema_EXPORT CSCMChannelsT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSCMChannelsT(CSCMChannelsT const& init);
	void operator=(CSCMChannelsT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_g3module_altova_CSCMChannelsT); }
	MemberElement<ns_g3module::CDigitalInputChnlT, _altova_mi_ns_g3module_altova_CSCMChannelsT_altova_digitalInput> digitalInput;
	struct digitalInput { typedef Iterator<ns_g3module::CDigitalInputChnlT> iterator; };
	MemberElement<ns_g3module::CAnalogueInputChnlT, _altova_mi_ns_g3module_altova_CSCMChannelsT_altova_analogueInput> analogueInput;
	struct analogueInput { typedef Iterator<ns_g3module::CAnalogueInputChnlT> iterator; };
	MemberElement<ns_g3module::CDigitalOutputChnlT, _altova_mi_ns_g3module_altova_CSCMChannelsT_altova_digitalOutput> digitalOutput;
	struct digitalOutput { typedef Iterator<ns_g3module::CDigitalOutputChnlT> iterator; };
	MemberElement<ns_g3module::CSwitchOutChnlT, _altova_mi_ns_g3module_altova_CSCMChannelsT_altova_switchOut> switchOut;
	struct switchOut { typedef Iterator<ns_g3module::CSwitchOutChnlT> iterator; };
	MemberElement<ns_g3module::CFPIChannelT, _altova_mi_ns_g3module_altova_CSCMChannelsT_altova_fpi> fpi;
	struct fpi { typedef Iterator<ns_g3module::CFPIChannelT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_g3module

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CSCMChannelsT
