#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CCommsDeviceConfigT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CCommsDeviceConfigT



namespace g3schema
{

namespace ns_template
{	

class CCommsDeviceConfigT : public TypeBase
{
public:
	g3schema_EXPORT CCommsDeviceConfigT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CCommsDeviceConfigT(CCommsDeviceConfigT const& init);
	void operator=(CCommsDeviceConfigT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_template_altova_CCommsDeviceConfigT); }
	MemberElement<ns_comms::CGPRSModemT, _altova_mi_ns_template_altova_CCommsDeviceConfigT_altova_GPRSModem> GPRSModem;
	struct GPRSModem { typedef Iterator<ns_comms::CGPRSModemT> iterator; };
	MemberElement<ns_comms::CPSTNModemT, _altova_mi_ns_template_altova_CCommsDeviceConfigT_altova_PSTNModem> PSTNModem;
	struct PSTNModem { typedef Iterator<ns_comms::CPSTNModemT> iterator; };
	MemberElement<ns_comms::CPAKNETModemT, _altova_mi_ns_template_altova_CCommsDeviceConfigT_altova_PAKNETModem> PAKNETModem;
	struct PAKNETModem { typedef Iterator<ns_comms::CPAKNETModemT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_template

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CCommsDeviceConfigT
