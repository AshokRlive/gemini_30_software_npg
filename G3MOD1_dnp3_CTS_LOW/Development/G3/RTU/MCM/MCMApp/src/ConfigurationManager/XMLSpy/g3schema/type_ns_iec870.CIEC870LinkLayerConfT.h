#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_iec870_ALTOVA_CIEC870LinkLayerConfT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_iec870_ALTOVA_CIEC870LinkLayerConfT



namespace g3schema
{

namespace ns_iec870
{	

class CIEC870LinkLayerConfT : public TypeBase
{
public:
	g3schema_EXPORT CIEC870LinkLayerConfT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CIEC870LinkLayerConfT(CIEC870LinkLayerConfT const& init);
	void operator=(CIEC870LinkLayerConfT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_iec870_altova_CIEC870LinkLayerConfT); }

	MemberAttribute<unsigned,_altova_mi_ns_iec870_altova_CIEC870LinkLayerConfT_altova_rxFrameSize, 0, 0> rxFrameSize;	// rxFrameSize CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_iec870_altova_CIEC870LinkLayerConfT_altova_txFrameSize, 0, 0> txFrameSize;	// txFrameSize CunsignedInt
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_iec870

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_iec870_ALTOVA_CIEC870LinkLayerConfT
