#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CPowerCycleInhibitPointT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CPowerCycleInhibitPointT



namespace g3schema
{

namespace ns_sdnp3
{	

class CPowerCycleInhibitPointT : public TypeBase
{
public:
	g3schema_EXPORT CPowerCycleInhibitPointT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CPowerCycleInhibitPointT(CPowerCycleInhibitPointT const& init);
	void operator=(CPowerCycleInhibitPointT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_sdnp3_altova_CPowerCycleInhibitPointT); }

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CPowerCycleInhibitPointT_altova_group, 0, 0> group;	// group CunsignedShort

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CPowerCycleInhibitPointT_altova_id, 0, 0> id;	// id CunsignedShort
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_sdnp3

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CPowerCycleInhibitPointT
