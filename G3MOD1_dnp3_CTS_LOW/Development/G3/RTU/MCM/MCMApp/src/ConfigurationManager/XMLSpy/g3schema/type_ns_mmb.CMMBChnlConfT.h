#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_mmb_ALTOVA_CMMBChnlConfT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_mmb_ALTOVA_CMMBChnlConfT

#include "type_ns_pstack.CChannelConfT.h"


namespace g3schema
{

namespace ns_mmb
{	

class CMMBChnlConfT : public ::g3schema::ns_pstack::CChannelConfT
{
public:
	g3schema_EXPORT CMMBChnlConfT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CMMBChnlConfT(CMMBChnlConfT const& init);
	void operator=(CMMBChnlConfT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_mmb_altova_CMMBChnlConfT); }
	MemberAttribute<string_type,_altova_mi_ns_mmb_altova_CMMBChnlConfT_altova_mbType, 0, 3> mbType;	// mbType CLU_MBLINK_TYPE

	MemberAttribute<unsigned,_altova_mi_ns_mmb_altova_CMMBChnlConfT_altova_maxQueueSize, 0, 0> maxQueueSize;	// maxQueueSize CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_mmb_altova_CMMBChnlConfT_altova_rxFrameTimeout, 0, 0> rxFrameTimeout;	// rxFrameTimeout CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_mmb_altova_CMMBChnlConfT_altova_channelResponseTimeout, 0, 0> channelResponseTimeout;	// channelResponseTimeout CunsignedInt
	MemberElement<ns_pstack::CSerialConfT, _altova_mi_ns_mmb_altova_CMMBChnlConfT_altova_serial> serial;
	struct serial { typedef Iterator<ns_pstack::CSerialConfT> iterator; };
	MemberElement<ns_pstack::CTCPConfT, _altova_mi_ns_mmb_altova_CMMBChnlConfT_altova_tcp> tcp;
	struct tcp { typedef Iterator<ns_pstack::CTCPConfT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_mmb

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_mmb_ALTOVA_CMMBChnlConfT
