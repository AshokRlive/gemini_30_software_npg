/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <vector>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "bitOperations.h"
#include "StringUtil.h"
#include "XMLDNP3SlaveProtocolParser.h"
#include "DNP3SlaveProtocol.h"
#include "SDNP3Debug.h"
#include "XMLParser.h"
#include "XMLParser_Points.h"
#include "ProtocolStackCommonEnum.h"

using namespace ns_common;
using namespace ns_pstack;
using namespace ns_sdnp3;
using namespace ns_vpoint;

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
/* Debug */
#define DEBUG_DNPXMLCFG     0

#if DEBUG_DNPXMLCFG
#define DBG(M,...)      DBG_INFO("DNP3::" M,##__VA_ARGS__)
#define DBG_INF(M,...)  DBG_INFO("DNP3::=>>"M,##__VA_ARGS__)
#else
#define DBG(M,...)
#define DBG_INF(M,...)
#endif

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/**
 * \brief Types of retries
 *
 * This enums mimics the one present in DNP3Enum.xml since we can not include it
 * directly.
 */
typedef enum
{
    UNSOLICITED_RETRY_MODE_NO_RETRY        =0x00,   //No Retry
    UNSOLICITED_RETRY_MODE_SIMPLE_RETRY    =0x01,   //Simple Retry
    UNSOLICITED_RETRY_MODE_WITH_OFFLINE    =0x02,   //Retry with Offline Time
    UNSOLICITED_RETRY_MODE_UNLIMITED       =0x03    //Unlimited Retry
} UNSOLICITED_RETRY_MODE;

/**
 * \brief Types of Commands accepted
 *
 * This enums mimics the one present in DNP3Enum.xml since we can not include it
 * directly.
 */
typedef enum
{
    ACCEPTED_COMMAND_SBO    =0x01,   //Select Before Operate Only (SBO)
    ACCEPTED_COMMAND_DO     =0x02,   //Direct Operate Only (DO)
    ACCEPTED_COMMAND_ANY    =0x03    //Any(SBO and DO)
} ACCEPTED_COMMAND;


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
template <typename XMLElementT>
lu_uint32_t getProtocolPoint(const XMLElementT& elementXML) throw(ParserException)
{
    lu_uint32_t protocolID;
    protocolID = getXMLAttr(elementXML.protocolID);
    if(!getXMLAttr(elementXML.enable))
    {
        throw ParserException(CONFMGR_ERROR_NONE, "Protocol Point disabled");
    }
    return protocolID;
}


template <typename XMLElementT>
lu_uint32_t getProtocolPointRef(XMLElementT& elementXML, PointIdStr& pointID) throw(ParserException)
{
    lu_uint32_t protocolID;
    protocolID = getProtocolPoint(elementXML);
    pointID = getXMLVirtualPointRef(elementXML.vpoint.first());
    if(!pointID.isValid())
    {
        throw ParserException(CONFMGR_ERROR_NO_POINT, "Invalid Virtual Point reference");
    }
    return protocolID;
}


template <typename XMLElementT>
lu_uint32_t maxValidDNPPoints(const XMLElementT& elementXML) throw(ParserException)
{
    /* TODO: pueyos_a - Improve lists of DNP TMWInputs/observers by not indexing them directly by DNP3 ID (probably using two lookup tables DNP3<->VPoint, or a hash table)
     * Note that these lists stores pointers (usually 4 Bytes in G3 MCM) and we can have up to 65536 DNP Points -- 65535*4Bytes*2pointers = 262144 * 2 pointers = 524288 Bytes, 512KiB
     * Please have in mind that defining only a DNP point in the 65535 index, reserves an entire block of 512KiB of memory unnecessarily.
     */

    /* Count number of DNP3 points and get max DNP3 id */
    lu_uint32_t dnpPointNum = 0;
    bool validFound = false; //Watches if any valid point found
    PointIdStr pointID;
    lu_uint32_t protocolID;
    for(XMLElementT itConfig = elementXML; itConfig; ++itConfig)
    {
        try
        {
            /* Skip points without valid protocol ID, disabled or with no related G3 point */
            protocolID = getProtocolPointRef(itConfig, pointID);
            validFound = true;  //At least one found
            dnpPointNum = LU_MAX(protocolID, dnpPointNum);   //get the highest protocol ID
        }
        catch(const ParserException& e)
        {}  //Ignore cases: we are only checking for counting
    }
    if(validFound)
    {
        dnpPointNum++;   //correct from 0-based index to size
    }
    return dnpPointNum;
}


template <typename VPointT, typename XMLIteratorT, typename VPointListT>
IPointObserver** preparePointConfig(const XMLIteratorT& iteratorXML, VPointListT& pointDBListPtr, PointObserverList& pointObsList) throw(ParserException)
{
    /* Count number of DNP3 points and get max DNP3 id */
    lu_uint32_t dnpPointNum = maxValidDNPPoints(iteratorXML);
    if(dnpPointNum == 0)
    {
        return NULL;
    }

    /* Allocate analogue binary input DNP db */
    pointDBListPtr.size = dnpPointNum;
    pointDBListPtr.list = (VPointT*)calloc(dnpPointNum, sizeof(VPointT));

    /* Allocate analogue input observer list */
    IPointObserver** observerList;
    observerList = (IPointObserver**)calloc(dnpPointNum, sizeof(IPointObserver*));

    /* Initialise both lists in order to ignore gaps */
    for(lu_uint32_t i = 0; i < dnpPointNum; i++)
    {
        pointDBListPtr.list[i].enabled = LU_FALSE;
        observerList[i] = NULL;
    }

    pointObsList.setTable(observerList, dnpPointNum);

    return observerList;
}


template <typename ProtPointT, typename XMLIteratorT>
void parsePointConfig(const XMLIteratorT& itXMLConfig, ProtPointT& point) throw(ParserException)
{
    point.eventOnlyWhenOnline = getXMLAttr(itXMLConfig.eventOnlyWhenConnected);
    lu_uint32_t defaultVariation;
    defaultVariation = getXMLAttr(itXMLConfig.defaultVariation);
    if(defaultVariation != 0)
    {
        point.defaultVariation = defaultVariation;
    }
    defaultVariation = 0;
    defaultVariation = getXMLAttr(itXMLConfig.eventDefaultVariation);
    if(defaultVariation != 0)
    {
        point.eventDefaultVariation = defaultVariation;
    }
    point.enableClass0 = getXMLAttr(itXMLConfig.enableClass0);

    /* Read point eventClass bit field */
    TMWDEFS_CLASS_MASK eventClass;
    eventClass = getXMLAttr(itXMLConfig.eventClass);
    point.eventClass = 0;
    LU_SETBIT_BS(point.eventClass, eventClass - 1);
    point.eventClass &= TMWDEFS_CLASS_MASK_ALL;
}


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

XMLDNP3SlaveProtocolParser::XMLDNP3SlaveProtocolParser(Cg3schema&          configuration,
                                                       CommsDeviceManager& commsDeviceManager,
                                                       PortManager&        portManager,
                                                       ModuleManager&      moduleManager) :
                                                       configuration(configuration),
                                                       commsDeviceManager(commsDeviceManager),
                                                       portManager(portManager),
                                                       moduleManager(moduleManager),
                                                       log(Logger::getLogger(SUBSYSTEM_ID_CONFMGR))
{
    initParser();
}

XMLDNP3SlaveProtocolParser::~XMLDNP3SlaveProtocolParser()
{
}


DNP3SlaveProtocol* XMLDNP3SlaveProtocolParser::newProtocolManager(
                                                     GeminiDatabase& GDatabase,
                                                     IMCMApplication& mcmApplication,
                                                     IIOModuleManager& moduleManager
                                                    )
{
    static lu_uint8_t sessionID = 0;
    DNP3SlaveProtocol* protocolManager = NULL;

    // The vector of channels will also store the session configurations
    // and be passed to the new DNP3SlaveProtocolManager
    std::vector<DNP3SlaveProtocolChannel> DNP3SlaveChannelsConfig;

    try
    {
        /* Get the DNP3 Slave Object (there should only be 0 or 1 of these) */
        CConfigurationT cfgRoot = getXMLElement<CConfigurationT>(configuration.configuration);
        checkXMLElement(cfgRoot.scadaProtocol);
        CSDNP3T sdnp3 = getXMLElement<CSDNP3T>(cfgRoot.scadaProtocol.first().sDNP3);
        checkXMLElement(sdnp3.channel);

        /* Get common attributes */
        CMiscellaneousT misc = getXMLElement<CMiscellaneousT>(cfgRoot.miscellaneous);
        bool invalidEventtimeCorrection = getXMLAttr(misc.invalidEventTimeCorrection);
        protocolManager = new DNP3SlaveProtocol(GDatabase, mcmApplication, invalidEventtimeCorrection);

        // Loop over channels, adding a new channelsConfig element and initialising it
        for (Iterator<CSDNP3ChnlT> itChannelConfig = sdnp3.channel.all();
             itChannelConfig;
             ++itChannelConfig)
        {

            lu_bool_t isDNP3chlEnabled = true; // flag which is set to false if
            //optional attribute(enabled) is present

            getXMLOptAttr(itChannelConfig.config.first().enabled, isDNP3chlEnabled);

            if (isDNP3chlEnabled == false)
            {
                std::string chnlName = itChannelConfig.config.first().chnlName;
                DBG_INFO("DNP3Slave Channel : %s is disabled",chnlName.c_str());
                continue;
            }

            DNP3SlaveProtocolChannel* channelPtr;
            DNP3SlaveProtocolChannel::Config channelConfig;

            /* TODO: pueyos_a - enableBroadcast to be a bit field in the session cfg */
            // Temp variable to get the enableBroadcast setting from the
            // current Schema (Channel Config) to set TMW DNP3 3.17
            // library settings in the Session Config
            bool enableBroadcast = false;

            // Initialise the TMW Structures
            liniotarg_initConfig(&(channelConfig.IOConfig));

            tmwtarg_initConfig(&(channelConfig.targConfig));

            dnpchnl_initConfig(&(channelConfig.channelConfig),
                               &(channelConfig.tprtConfig),
                               &(channelConfig.linkConfig),
                               &(channelConfig.physConfig));

            /* Load the LINK LAYER parameters */
            CSDNP3ChnlConfT dnp3ChnlConf = getXMLElement<CSDNP3ChnlConfT>(itChannelConfig.config);
            getXMLOptAttr(dnp3ChnlConf.chnlName, channelConfig.channelName);
            channelConfig.linkConfig.rxFrameSize         = getXMLAttr(dnp3ChnlConf.rxFrameSize);
            channelConfig.linkConfig.txFrameSize         = getXMLAttr(dnp3ChnlConf.txFrameSize);
            channelConfig.linkConfig.rxFrameTimeout      = getXMLAttr(dnp3ChnlConf.rxFrameTimeout);
            channelConfig.linkConfig.offlinePollPeriod   = getXMLAttr(dnp3ChnlConf.offlinePollPeriodMs);
            channelConfig.linkConfig.confirmMode = getXMLAttrEnum<TMWDEFS_LINKCNFM>(dnp3ChnlConf.confirmMode, LU_LINKCNFM_VALUEfromINDEX);
            channelConfig.linkConfig.confirmTimeout = getXMLAttr(dnp3ChnlConf.confirmTimeout);
            channelConfig.linkConfig.maxRetries     = getXMLAttr(dnp3ChnlConf.maxRetries);

            enableBroadcast = getXMLAttr(dnp3ChnlConf.broadcast);
            
            channelConfig.channelConfig.rxFragmentSize = getXMLAttr(dnp3ChnlConf.rxFragmentSize);
            channelConfig.channelConfig.txFragmentSize = getXMLAttr(dnp3ChnlConf.txFragmentSize);

            /* Load the IO CONFIG parameters */

            // Set the network type in the LinIo Library
            if (!dnp3ChnlConf.networkType.exists())
            {
                log.error("Invalid network type configured for Slave DNP3 Channel [%s]", dnp3ChnlConf.chnlName);
                continue;
            }

            channelConfig.IOConfig.lin232.bModemFitted = TMWDEFS_FALSE;

            /* Extract the network type from the config and translate it to enum */
            NETWORK_TYPE netTypeConfig;
            checkXMLElement(dnp3ChnlConf.networkType);
            netTypeConfig = static_cast<NETWORK_TYPE>(dnp3ChnlConf.networkType.GetEnumerationValue());
            switch(netTypeConfig)
            {
                case NETWORK_TYPE_SERIAL:
                    channelConfig.linkConfig.networkType = DNPLINK_NETWORK_NO_IP;
                    break;
                case NETWORK_TYPE_COMMSDEVICE:
                    channelConfig.linkConfig.networkType = DNPLINK_NETWORK_NO_IP;
                    channelConfig.IOConfig.lin232.bModemFitted = TMWDEFS_TRUE;
                    break;
                case NETWORK_TYPE_TCP_ONLY:
                    channelConfig.linkConfig.networkType = DNPLINK_NETWORK_TCP_ONLY;
                    break;
                case NETWORK_TYPE_TCP_UDP:
                    channelConfig.linkConfig.networkType = DNPLINK_NETWORK_TCP_UDP;
                    break;
                case NETWORK_TYPE_UDP_ONLY:
                    channelConfig.linkConfig.networkType = DNPLINK_NETWORK_UDP_ONLY;
                    break;
                default:
                    log.error("Invalid network type configured for Slave DNP3 Channel [%s]", dnp3ChnlConf.chnlName);
                    continue;
            }

            CDNP3LinuxIoT linuxIo = getXMLElement<CDNP3LinuxIoT>(dnp3ChnlConf.linuxIo);
            channelConfig.IOConfig.type = (channelConfig.linkConfig.networkType == DNPLINK_NETWORK_NO_IP) ? LINIO_TYPE_232 : LINIO_TYPE_TCP;
            if (channelConfig.IOConfig.type == LINIO_TYPE_232)
            {
                DialupModem *modemPtr = NULL;
                SCADAConnectionManager::SCADAConnManConfig config;
                /* Parse the serial & CommsDevice config */
                parseChannelSerialConfig(linuxIo, commsDeviceManager, channelConfig, &modemPtr, &(config.m_connectionSettingsConfig.connectOnEventClassMask));

                // Create the Protocol Channel
                channelPtr = new DNP3SlaveProtocolChannel(GDatabase, protocolManager, channelConfig);
                channelPtr->setModem(modemPtr);
                channelPtr->init();

                channelPtr->setConnectionManagerConfig(config);
            }
            else
            {
                // Use TCP Config
                channelConfig.IOConfig.linTCP.validateUDPAddress = TMWDEFS_FALSE; //MG

                /* Read TCP/IP configuration */
                CSDNP3ChnlTCPConfT TCPConfigStr = getXMLElement<CSDNP3ChnlTCPConfT>(linuxIo.tcp);
                std::string chnlName = getXMLAttr(dnp3ChnlConf.chnlName);
                stringToCString(chnlName, channelConfig.IOConfig.linTCP.chnlName, LINIOCNFG_MAX_NAME_LEN);
                std::string ipAddress = getXMLAttr(TCPConfigStr.ipAddress);
                stringToCString(ipAddress, channelConfig.IOConfig.linTCP.ipAddress, 32);
                channelConfig.IOConfig.linTCP.dualEndPointIpPort = getXMLAttr(TCPConfigStr.dualEndPointIpPort);
                channelConfig.IOConfig.linTCP.ipPort             = getXMLAttr(TCPConfigStr.ipPort            );
                channelConfig.IOConfig.linTCP.ipConnectTimeout   = getXMLAttr(TCPConfigStr.ipConnectTimeout  );
                channelConfig.IOConfig.linTCP.localUDPPort       = getXMLAttr(TCPConfigStr.localUDPPort      );
                channelConfig.IOConfig.linTCP.destUDPPort        = getXMLAttr(TCPConfigStr.destUDPPort       );
                channelConfig.IOConfig.linTCP.initUnsolUDPPort   = getXMLAttr(TCPConfigStr.initUnsolUDPPort  );
                channelConfig.IOConfig.linTCP.mode = getXMLAttrEnum<LINTCP_MODE>(TCPConfigStr.mode, LU_LINTCP_MODE_VALUEfromINDEX);
                channelConfig.IOConfig.linTCP.disconnectOnNewSyn = TMWDEFS_FALSE;   //Default
                getXMLOptAttr(TCPConfigStr.disconnectOnNewSyn, channelConfig.IOConfig.linTCP.disconnectOnNewSyn);   //Optional attribute

                // We are ALWAYS an OUTSTATION
                channelConfig.IOConfig.linTCP.role = LINTCP_ROLE_OUTSTATION;

                // Do a quick sanity check on the TCP configuration
                if ((channelConfig.IOConfig.linTCP.mode != LINTCP_MODE_SERVER) &&
                    (strcmp(channelConfig.IOConfig.linTCP.ipAddress, "*.*.*.*") == 0))
                {
                    log.error("DNP3 Channel [%s] Cannot be a CLIENT or DUAL ENDPOINT with ipAddress set to [%s]!\n",
                                    channelConfig.IOConfig.linTCP.chnlName,
                                    channelConfig.IOConfig.linTCP.ipAddress);
                }

                channelConfig.channelName = channelConfig.IOConfig.linTCP.chnlName;

                channelPtr = new DNP3SlaveProtocolChannel(GDatabase, protocolManager, channelConfig);

                if (TCPConfigStr.connectionManager.exists())
                {
                    channelPtr->setConnectionManagerEnabled(true);

                    // Parse the Connection Group settings
                    CConnectionManagerT connMgr = getXMLElement<CConnectionManagerT>(TCPConfigStr.connectionManager);

                    /* Get the masterAddress for the primary master */
                    channelConfig.masterAddress = getXMLAttr(TCPConfigStr.masterAddress);

                    /* Parse Connection Group Settings  - this group includes ALL channel configurations! */
                    parseFailoverGroupConfig(connMgr, &channelConfig, channelPtr);
                }

                // Need to initialise the channel before adding any Connectivity Check Configuration
                channelPtr->init();

                if (TCPConfigStr.connectionManager.exists())
                {
                    SCADAConnectionManager::SCADAConnManConfig config;

                    CConnectionManagerT connMgr = TCPConfigStr.connectionManager.first();

                    if (connMgr.commsPowerCycle.exists())
                    {
                        CCommsPowerCycleT commsPowerCycle = connMgr.commsPowerCycle.first();

                        if (commsPowerCycle.commsPowerSupply.exists())
                        {
                            parseCommsPowerSupplyConfig(commsPowerCycle, moduleManager, config);
                        }

                        if (commsPowerCycle.ipConnectivityCheck.exists())
                        {
                            CIpConnectivityCheckT ipConnectivityCheck = commsPowerCycle.ipConnectivityCheck.first();

                            parseConnectivityCheckConfig(ipConnectivityCheck, config);
                        }

                        if (commsPowerCycle.outgoingConnections.exists())
                        {
                            COutgoingConnectionsT outgoingConnections = commsPowerCycle.outgoingConnections.first();

                            parseOutgoingConnectionsConfig(outgoingConnections, config);
                        }

                        if (commsPowerCycle.incomingConnections.exists())
                        {
                            CIncomingConnectionsT incomingConnections = commsPowerCycle.incomingConnections.first();

                            parseIncomingConnectionsConfig(incomingConnections, config);
                        }
                    }

					parseConnectionSettingsConfig(connMgr, config);

                    // Get the validate master IP setting
                    config.m_validateMasterIP = getXMLAttr(TCPConfigStr.validateMasterIP);

                    channelPtr->setConnectionManagerConfig(config);
                }
            }

            // Iterate through the configured sessions
            for (Iterator<CSDNP3SesnT> itSessionConfig = itChannelConfig.session.all();
                 itSessionConfig;
                 ++itSessionConfig)
            {

                lu_bool_t isDNP3seshEnabled = true ;// flag which is set to false if
                //optional attribute(enabled) is present

                getXMLOptAttr(itSessionConfig.config.first().enabled,isDNP3seshEnabled);

                if (isDNP3seshEnabled == false)
                {
                    std::string seshName = itSessionConfig.config.first().sessionName;
                    DBG_INFO("DNP3Slave Session : %s is disabled", seshName.c_str());
                    continue;
                }

                DNP3SlaveProtocolSession::Config sessionConfig;

                /* Parse the Session config */
                parseSessionConfig(itSessionConfig, sessionConfig, enableBroadcast);

// TODO - SKA - Get the sessionID from the XML
                DNP3SlaveProtocolSession *sessionPtr = new DNP3SlaveProtocolSession(GDatabase, channelPtr, sessionConfig, sessionID++);

                /* Parse Analogue input points */
                parseAnalogueConfig(GDatabase, &(itSessionConfig), sessionPtr);

                /* Parse Binary input points */
                parseBinaryConfig(GDatabase, &(itSessionConfig), sessionPtr);

                /* Parse Double Binary input points */
                parseDBinaryConfig(GDatabase, &(itSessionConfig), sessionPtr);

                /* Parse Counter points */
                parseCounterConfig(GDatabase, &(itSessionConfig), sessionPtr);

                /* Parse Binary output points */
                parseBinaryOutputConfig(&(itSessionConfig), sessionPtr);

                /* Parse Analogue output points */
                parseAnalogOutputConfig(GDatabase, &(itSessionConfig), sessionPtr);

                // Add the session to the vector of sessions
                channelPtr->addSession(*sessionPtr);
                protocolManager->addSession(*sessionPtr);    //also add it to the manager list
            }

            // Add the channel to the vector of channels
            protocolManager->addChannel(*channelPtr);
        }
    }
    catch (...)
    {
        log.error("XMLDNP3SlaveProtocolParser: Exception raised");
    }

    return protocolManager;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


CONFMGR_ERROR XMLDNP3SlaveProtocolParser::initParser()
{
    try
    {
        CConfigurationT configurationRoot = getXMLElement<CConfigurationT>(configuration.configuration);
        CScadaProtocolT scadaProtocol = getXMLElement<CScadaProtocolT>(configurationRoot.scadaProtocol);

        /* Check if DNP3 protocol is present */
        if(scadaProtocol.sDNP3.exists())
        {
            // Maximum of one instance of DNP3 Slave
            if (scadaProtocol.sDNP3.count() <= 1)
            {
                return CONFMGR_ERROR_NONE;
            }
        }
    }
    catch (...)
    {
        log.error("XMLDNP3SlaveProtocolParser::initParser Exception raised");
    }

    return CONFMGR_ERROR_INITPARSER;
}


void XMLDNP3SlaveProtocolParser::parseAnalogueConfig(GeminiDatabase           &GDatabase,
                                                     CSDNP3SesnT           *sessionParserPtr,
                                                     DNP3SlaveProtocolSession *sessionPtr)
{
    IPointObserver** observerList;
    try
    {
        CSDNP3PointsT sessionMap = getXMLElement<CSDNP3PointsT>(sessionParserPtr->iomap);
        if(!sessionMap.analogInput.exists())
        {
            log.info("%s No analogue points configured", __AT__);
            return;
        }
        observerList = preparePointConfig<TMWSDNPAInputStr>(sessionMap.analogInput.all(),
                                                            sessionPtr->m_db->pointDB->analogueInput,
                                                            sessionPtr->m_db->AIObserverList);
        PointIdStr pointID;
        lu_uint32_t protocolID;
        for(Iterator<CSDNP3InputPointT> itConfig = sessionMap.analogInput.all();
            itConfig;
            ++itConfig
           )
        {
            try
            {
                /* Skip points without valid protocol ID, disabled or with no related G3 point */
                protocolID = getProtocolPointRef(itConfig, pointID);
            }
            catch(ParserException& e)
            {
                if(e.getID() == CONFMGR_ERROR_NONE)
                {
                    log.debug("DNP3 Protocol analogue point %d skipped: %s", protocolID, e.what());
                }
                else
                {
                    log.warn("DNP3 Protocol analogue point %d skipped: %s", protocolID, e.what());
                }
                continue;   //skip this point
            }
            TMWSDNPAInputStr* pointPtr;
            pointPtr = &(sessionPtr->m_db->pointDB->analogueInput.list[protocolID]);
            if(pointPtr == NULL)
            {
                throw ParserException(CONFMGR_ERROR_NOT_INITIALIZED, "Database Point not available");
            }

            /* The Point's own Default Variation is used by default.
             * If the Point's Default Variation is set to 0 the Session Default Variation for the Point is used.
             * This means that a Point's Default Variation has a HIGHER priority than the Session's Default Variation.
             * The default behaviour of the TMW library gives the Session's Default Variation a higher priority that the Point's.
             * */
            checkXMLElement(sessionParserPtr->config);
            CObjDefaultVariationsT objDefaultVariations = sessionParserPtr->config.first().objDefaultVariations.first();

            /* Set defaults first. This may be overridden by parsePointConfig() */
            pointPtr->defaultVariation = objDefaultVariations.obj30;
            pointPtr->eventDefaultVariation = objDefaultVariations.obj32;
            pointPtr->eventLogStore = LU_TRUE;      //Store event by default
            getXMLOptAttr(itConfig.enableStoreEvent, pointPtr->eventLogStore);

            parsePointConfig(itConfig, *pointPtr);
            pointPtr->flags = DNPDEFS_DBAS_FLAG_OFF_LINE | DNPDEFS_DBAS_FLAG_RESTART;
            pointPtr->value.type = TMWTYPES_ANALOG_TYPE_SFLOAT;
            pointPtr->value.value.fval = 0;

            /* Create observers */
            observerList[protocolID] = new DNP3SlaveAIObserver(pointID, protocolID, sessionPtr->m_db->pointDB);
            if (GDatabase.attach(pointID, sessionPtr->m_db->AIObserverList[protocolID]) == GDB_ERROR_NONE)
            {
                pointPtr->enabled = LU_TRUE;
            }
            else
            {
                /* Invalid virtual point */
                //delete db.AIObserverList[protocolID];
                //observerList[protocolID] = NULL;

                //FIX: APD: instead of deleting, create an always-offline point
                pointPtr->enabled = LU_TRUE;
                pointPtr->flags &= ~DNPDEFS_DBAS_FLAG_ON_LINE;
            }
        }
    }
    catch (const ParserException& e)
    {
        log.error("Error reading DNP3 analogue point configuration: %s", e.what());
    }
}


void XMLDNP3SlaveProtocolParser::parseCounterConfig(GeminiDatabase           &GDatabase,
                                                     CSDNP3SesnT           *sessionParserPtr,
                                                     DNP3SlaveProtocolSession *sessionPtr)
{
    IPointObserver** observerList;
    try
    {
        CSDNP3PointsT sessionMap = getXMLElement<CSDNP3PointsT>(sessionParserPtr->iomap);
        if(!sessionMap.counter.exists())
        {
            log.info("%s No counter points configured", __AT__);
            return;
        }
        observerList = preparePointConfig<TMWSDNPCInputStr>(sessionMap.counter.all(),
                                                            sessionPtr->m_db->pointDB->counterInput,
                                                            sessionPtr->m_db->CObserverList);
        PointIdStr pointID;
        lu_uint32_t protocolID;
        for(Iterator<CSDNP3CounterPointT> itConfig = sessionMap.counter.all();
            itConfig;
            ++itConfig
           )
        {
            try
            {
                /* Skip points without valid protocol ID, disabled or with no related G3 point */
                protocolID = getProtocolPointRef(itConfig, pointID);
            }
            catch(ParserException& e)
            {
                if(e.getID() == CONFMGR_ERROR_NONE)
                {
                    log.debug("DNP3 Protocol counter point %d skipped: %s", protocolID, e.what());
                }
                else
                {
                    log.warn("DNP3 Protocol counter point %d skipped: %s", protocolID, e.what());
                }
                continue;   //skip this point
            }
            TMWSDNPCInputStr* pointPtr;
            pointPtr = &(sessionPtr->m_db->pointDB->counterInput.list[protocolID]);
            if(pointPtr == NULL)
            {
                throw ParserException(CONFMGR_ERROR_NOT_INITIALIZED, "Database Point not available");
            }

            /* The Point's own Default Variation is used by default.
             * If the Point's Default Variation is set to 0 the Session Default Variation for the Point is used.
             * This means that a Point's Default Variation has a HIGHER priority than the Session's Default Variation.
             * The default behaviour of the TMW library gives the Session's Default Variation a higher priority that the Point's.
             * */
            checkXMLElement(sessionParserPtr->config);

            /* Set defaults first. This may be overridden by parsePointConfig() */
            CObjDefaultVariationsT objDefaultVariations = sessionParserPtr->config.first().objDefaultVariations.first();
            pointPtr->defaultVariation = objDefaultVariations.obj20;            //obj 20: counter
            pointPtr->eventDefaultVariation = objDefaultVariations.obj22;       //obj 22: counter event
            pointPtr->frzDefaultVariation = objDefaultVariations.obj21;         //obj 21: frozen counter
            pointPtr->frzEventDefaultVariation = objDefaultVariations.obj23;    //obj 23: frozen counter event
            pointPtr->frzEventClass = TMWDEFS_CLASS_MASK_NONE;
            pointPtr->frzEnableClass0 = TMWDEFS_FALSE;
            pointPtr->vPointRef.group = pointID.group;
            pointPtr->vPointRef.ID = pointID.ID;
            pointPtr->eventLogStore = LU_TRUE;      //Store event by default
            getXMLOptAttr(itConfig.enableStoreEvent, pointPtr->eventLogStore);

            parsePointConfig(itConfig, *pointPtr);

            bool isFrozen = getXMLAttr(itConfig.frozenFlag);
            pointPtr->frozenFlag = (isFrozen)? TMWDEFS_TRUE : TMWDEFS_FALSE;
            if(isFrozen)
            {
                CFrozenCounterT frzCounter = getXMLElement<CFrozenCounterT>(itConfig.frozenCounter);
                lu_uint32_t variation = 0;
                variation = getXMLAttr(frzCounter.defaultVariation);
                if(variation != 0)
                {
                    pointPtr->frzDefaultVariation = variation;
                }
                variation = 0;
                variation = getXMLAttr(frzCounter.eventDefaultVariation);
                if(variation != 0)
                {
                    pointPtr->frzEventDefaultVariation = variation;
                }
                pointPtr->frzEventClass = getXMLAttr(frzCounter.eventClass);
                pointPtr->frzEnableClass0 = getXMLAttr(frzCounter.enableClass0);
            }

            /* Initialise callback structure */
            pointPtr->callbacks = &(sessionPtr->m_db->pointDB->callbacks);

            /* Set initial values */
            pointPtr->flags = DNPDEFS_DBAS_FLAG_OFF_LINE | DNPDEFS_DBAS_FLAG_RESTART;
            pointPtr->value = 0;
            pointPtr->frzValue = 0;
            pointPtr->useRolloverFlag = getXMLAttr(itConfig.useRolloverFlag);

            /* Counters, like the Output points, have a pointer to the session object which allows them
             * to see the SRC ADDR of the Master trying to Freeze them.  This allows
             * the Counter to disallow controls from a Broadcast Address as well.
             */
            pointPtr->sclSessionPtrPtr = sessionPtr->getSCLSessionPtr();

            /* Create observers */
            observerList[protocolID] = new DNP3SlaveCounterObserver(pointID, protocolID, sessionPtr->m_db->pointDB);
            if (GDatabase.attach(pointID, sessionPtr->m_db->CObserverList[protocolID]) == GDB_ERROR_NONE)
            {
                pointPtr->enabled = LU_TRUE;
            }
            else
            {
                /* Invalid virtual point */
                //FIX: pueyos_a: instead of deleting, create an always-offline point
                pointPtr->enabled = LU_TRUE;
                pointPtr->flags &= ~DNPDEFS_DBAS_FLAG_ON_LINE;
            }
        }
    }
    catch (const ParserException& e)
    {
        log.error("Error reading DNP3 counter point configuration: %s", e.what());
    }
}


void XMLDNP3SlaveProtocolParser::parseBinaryConfig(GeminiDatabase           &GDatabase,
                                                     CSDNP3SesnT           *sessionParserPtr,
                                                     DNP3SlaveProtocolSession *sessionPtr)
{
    IPointObserver** observerList;
    try
    {
        CSDNP3PointsT sessionMap = getXMLElement<CSDNP3PointsT>(sessionParserPtr->iomap);
        if(!sessionMap.binaryInput.exists())
        {
            log.info("%s No binary points configured", __AT__);
            return;
        }
        observerList = preparePointConfig<TMWSDNPBInputStr>(sessionMap.binaryInput.all(),
                                                            sessionPtr->m_db->pointDB->binaryInput,
                                                            sessionPtr->m_db->BIObserverList);
        PointIdStr pointID;
        lu_uint32_t protocolID;
        for(Iterator<CSDNP3InputPointT> itConfig = sessionMap.binaryInput.all();
            itConfig;
            ++itConfig
           )
        {
            try
            {
                /* Skip points without valid protocol ID, disabled or with no related G3 point */
                protocolID = getProtocolPointRef(itConfig, pointID);
            }
            catch(ParserException& e)
            {
                if(e.getID() == CONFMGR_ERROR_NONE)
                {
                    log.debug("DNP3 Protocol binary point %d skipped: %s", protocolID, e.what());
                }
                else
                {
                    log.warn("DNP3 Protocol binary point %d skipped: %s", protocolID, e.what());
                }
                continue;   //skip this point
            }
            TMWSDNPBInputStr* pointPtr;
            pointPtr = &(sessionPtr->m_db->pointDB->binaryInput.list[protocolID]);
            if(pointPtr == NULL)
            {
                throw ParserException(CONFMGR_ERROR_NOT_INITIALIZED, "Database Point not available");
            }

            /* The Point's own Default Variation is used by default.
             * If the Point's Default Variation is set to 0 the Session Default Variation for the Point is used.
             * This means that a Point's Default Variation has a HIGHER priority than the Session's Default Variation.
             * The default behaviour of the TMW library gives the Session's Default Variation a higher priority that the Point's.
             * */
            checkXMLElement(sessionParserPtr->config);
            CObjDefaultVariationsT objDefaultVariations = sessionParserPtr->config.first().objDefaultVariations.first();

            /* Set defaults first. This may be overridden by parsePointConfig() */
            pointPtr->defaultVariation = objDefaultVariations.obj01;
            pointPtr->eventDefaultVariation = objDefaultVariations.obj02;
            pointPtr->eventLogStore = LU_TRUE;      //Store event by default
            getXMLOptAttr(itConfig.enableStoreEvent, pointPtr->eventLogStore);

            parsePointConfig(itConfig, *pointPtr);

            pointPtr->value = DNPDEFS_DBAS_FLAG_OFF_LINE | DNPDEFS_DBAS_FLAG_RESTART;

            /* Create observers */
            observerList[protocolID] = new DNP3SlaveBIObserver(pointID, protocolID, sessionPtr->m_db->pointDB);
            if (GDatabase.attach(pointID, sessionPtr->m_db->BIObserverList[protocolID]) == GDB_ERROR_NONE)
            {
                pointPtr->enabled = LU_TRUE;
            }
            else
            {
                /* Invalid virtual point */
                //delete db.AIObserverList[protocolID];
                //observerList[protocolID] = NULL;

                //FIX: APD: instead of deleting, create an always-offline point
                pointPtr->enabled = LU_TRUE;
                pointPtr->value &= ~DNPDEFS_DBAS_FLAG_ON_LINE;
            }
        }
    }
    catch (const ParserException& e)
    {
        log.error("Error reading DNP3 binary point configuration: %s", e.what());
    }
}


void XMLDNP3SlaveProtocolParser::parseDBinaryConfig(GeminiDatabase           &GDatabase,
                                                     CSDNP3SesnT           *sessionParserPtr,
                                                     DNP3SlaveProtocolSession *sessionPtr)
{
    IPointObserver** observerList;
    try
    {
        CSDNP3PointsT sessionMap = getXMLElement<CSDNP3PointsT>(sessionParserPtr->iomap);
        if(!sessionMap.doubleBinaryInput.exists())
        {
            log.info("%s No double binary points configured", __AT__);
            return;
        }
        observerList = preparePointConfig<TMWSDNPDBInputStr>(sessionMap.doubleBinaryInput.all(),
                                                            sessionPtr->m_db->pointDB->dbinaryInput,
                                                            sessionPtr->m_db->DBIObserverList);
        PointIdStr pointID;
        lu_uint32_t protocolID;
        for(Iterator<CSDNP3InputPointT> itConfig = sessionMap.doubleBinaryInput.all();
            itConfig;
            ++itConfig
           )
        {
            try
            {
                /* Skip points without valid protocol ID, disabled or with no related G3 point */
                protocolID = getProtocolPointRef(itConfig, pointID);
            }
            catch(ParserException& e)
            {
                if(e.getID() == CONFMGR_ERROR_NONE)
                {
                    log.debug("DNP3 Protocol double binary point %d skipped: %s", protocolID, e.what());
                }
                else
                {
                    log.warn("DNP3 Protocol double binary point %d skipped: %s", protocolID, e.what());
                }
                continue;   //skip this point
            }

            TMWSDNPDBInputStr* pointPtr;
            pointPtr = &(sessionPtr->m_db->pointDB->dbinaryInput.list[protocolID]);
            if(pointPtr == NULL)
            {
                throw ParserException(CONFMGR_ERROR_NOT_INITIALIZED, "Database Point not available");
            }

            /* The Point's own Default Variation is used by default.
             * If the Point's Default Variation is set to 0 the Session Default Variation for the Point is used.
             * This means that a Point's Default Variation has a HIGHER priority than the Session's Default Variation.
             * The default behaviour of the TMW library gives the Session's Default Variation a higher priority that the Point's.
             * */
            checkXMLElement(sessionParserPtr->config);
            CObjDefaultVariationsT objDefaultVariations = sessionParserPtr->config.first().objDefaultVariations.first();

            /* Set defaults first. This may be overridden by parsePointConfig() */
            pointPtr->defaultVariation = objDefaultVariations.obj03;
            pointPtr->eventDefaultVariation = objDefaultVariations.obj04;
            pointPtr->eventLogStore = LU_TRUE;      //Store event by default
            getXMLOptAttr(itConfig.enableStoreEvent, pointPtr->eventLogStore);

            parsePointConfig(itConfig, *pointPtr);

            pointPtr->value = DNPDEFS_DBAS_FLAG_OFF_LINE | DNPDEFS_DBAS_FLAG_RESTART;

            /* Create observers */
            observerList[protocolID] = new DNP3SlaveDBIObserver(pointID, protocolID, sessionPtr->m_db->pointDB);
            if (GDatabase.attach(pointID, sessionPtr->m_db->DBIObserverList[protocolID]) == GDB_ERROR_NONE)
            {
                pointPtr->enabled = LU_TRUE;
            }
            else
            {
                /* Invalid virtual point */
                //delete db.AIObserverList[protocolID];
                //observerList[protocolID] = NULL;

                //FIX: APD: instead of deleting, create an always-offline point
                pointPtr->enabled = LU_TRUE;
                pointPtr->value &= ~DNPDEFS_DBAS_FLAG_ON_LINE;
            }
        }
    }
    catch (const ParserException& e)
    {
        log.error("Error reading DNP3 binary point configuration: %s", e.what());
    }
}


void XMLDNP3SlaveProtocolParser::parseBinaryOutputConfig(//GeminiDatabase           &GDatabase,
                                                     CSDNP3SesnT           *sessionParserPtr,
                                                     DNP3SlaveProtocolSession *sessionPtr)
{
    try
    {

        CSDNP3PointsT sessionMap = getXMLElement<CSDNP3PointsT>(sessionParserPtr->iomap);
        if(!sessionMap.binaryOutput.exists())
        {
            log.info("No binary output points configured");
            return;
        }

        /* Count number of DNP3 points and get max DNP3 id */
        Iterator<CSDNP3OutputPointT> itConfig = sessionMap.binaryOutput.all();
        /* TODO: pueyos_a - Improve lists of DNP TMWInputs/observers by not indexing them directly by DNP3 ID (probably using two lookup tables DNP3<->VPoint, or a hash table)
         * Note that these lists stores pointers (usually 4 Bytes in G3 MCM) and we can have up to 65536 DNP Points -- 262144 * 2 = 524288 Bytes, 512KiB
         * Please have in mind that defining only a DNP point in the 65535 index, reserves an entire block of 512KiB of memory unnecessarily.
         */

        /* Count number of DNP3 points and get max DNP3 id */
        lu_uint32_t dnpPointNum = 0;
        bool validFound = false; //Watches if any valid point found
        lu_uint32_t protocolID;
        for(Iterator<CSDNP3OutputPointT> itConfig = sessionMap.binaryOutput.all(); itConfig; ++itConfig)
        {
            try
            {
                /* Skip points without valid protocol ID, disabled or with no related G3 point */
                protocolID = getProtocolPoint(itConfig);
                validFound = true;  //At least one found
                dnpPointNum = LU_MAX(protocolID, dnpPointNum);   //get the highest protocol ID
            }
            catch(const ParserException& e)
            {}  //Ignore cases: we are only checking for counting
        }
        if(!validFound)
        {
            log.info("No binary output points configured");
            return;
        }
        dnpPointNum++;   //correct from 0-based index to size

        TMWSDNPBOutputListStr* pointDBListPtr = &(sessionPtr->m_db->pointDB->binaryOutput);
        /* Allocate analogue binary output DNP db */
        pointDBListPtr->size = dnpPointNum;
        pointDBListPtr->list = (TMWSDNPBOutputStr*)calloc(dnpPointNum, sizeof(TMWSDNPBOutputStr));

        /* Initialise list in order to ignore gaps */
        for(lu_uint32_t i = 0; i < dnpPointNum; i++)
        {
            pointDBListPtr->list[i].enabled = LU_FALSE;
        }

        /* Initialise binary output database */
        for(Iterator<CSDNP3OutputPointT> itConfig = sessionMap.binaryOutput.all();
            itConfig;
            ++itConfig
           )
        {
            TMWSDNPBOutputStr* pointPtr;
            lu_uint32_t protocolID = 0;
            lu_uint16_t cLogicGroup = PointIdStr::invalidGroup;
            try
            {
                /* Skip points without valid protocol ID, disabled or with no related G3 out point */
                protocolID = getXMLAttr(itConfig.protocolID);
                cLogicGroup = getXMLAttr(itConfig.controlLogicGroup);
                if(!getXMLAttr(itConfig.enable))
                {
                    throw ParserException(CONFMGR_ERROR_NONE, "Protocol Point disabled");
                }
                pointPtr = &(pointDBListPtr->list[protocolID]);
                if(pointPtr == NULL)
                {
                    throw ParserException(CONFMGR_ERROR_NOT_INITIALIZED, "Database Point not available");
                }
                /* Valid protocol point. Read the configuration */
                pointPtr->switchID = cLogicGroup;

                /* enable class 0 from configuration */
                pointPtr->enableClass0 = TMWDEFS_FALSE; //default
                pointPtr->defaultVariation = 2;         //default
                if(itConfig.outputEvent.exists())
                {
                    pointPtr->enableClass0 = getXMLAttr(itConfig.outputEvent.first().enableClass0);
                    pointPtr->defaultVariation = getXMLAttr(itConfig.outputEvent.first().defaultVariation);
                }

                /* SBO vs Direct Operate */
                lu_uint8_t accCommands = ACCEPTED_COMMAND_ANY;      //Default
                getXMLOptAttr(itConfig.acceptedCommand, accCommands);
                pointPtr->acceptSBO = TMWDEFS_FALSE;
                pointPtr->acceptDO = TMWDEFS_FALSE;
                switch (accCommands)
                {
                    case ACCEPTED_COMMAND_SBO:
                        pointPtr->acceptSBO = TMWDEFS_TRUE;
                        break;
                    case ACCEPTED_COMMAND_DO:
                        pointPtr->acceptDO = TMWDEFS_TRUE;
                        break;
                    case ACCEPTED_COMMAND_ANY:
                        pointPtr->acceptSBO = TMWDEFS_TRUE;
                        pointPtr->acceptDO = TMWDEFS_TRUE;
                        break;
                    default:
                        throw ParserException(CONFMGR_ERROR_PARAM, "Invalid command ");
                        break;
                }

                //Default: value 0 with online
                pointPtr->value = DNPDEFS_DBAS_FLAG_ON_LINE;

                /* Initialise callback structure */
                pointPtr->callbacks = &(sessionPtr->m_db->pointDB->callbacks);

                /* Point fully configured. We can enable it */
                pointPtr->enabled = LU_TRUE;

                /* Output points have a pointer to the session object which allows them
                 * to see the SRC ADDR of the Master trying to Select/Operate/Direct Operate them.  This allows
                 * the Output Point to disallow controls from a Broadcast Address.
                 */
                pointPtr->sclSessionPtrPtr = sessionPtr->getSCLSessionPtr();
            }
            catch(const ParserException& e)
            {
                if(e.getID() == CONFMGR_ERROR_NONE)
                {
                    log.debug("DNP3 Protocol binary output point %d skipped: %s", protocolID, e.what());
                }
                else
                {
                    log.warn("DNP3 Protocol binary output point %d skipped: %s", protocolID, e.what());
                    continue;
                }
            }
        }
    }
    catch (const ParserException& e)
    {
        log.error("Error reading DNP3 binary output point configuration: %s", e.what());
    }
}

void XMLDNP3SlaveProtocolParser::parseAnalogOutputConfig(GeminiDatabase           &GDatabase,
                                                     CSDNP3SesnT           *sessionParserPtr,
                                                     DNP3SlaveProtocolSession *sessionPtr)
{
    PointIdStr pointID;

    try
    {

        CSDNP3PointsT sessionMap = getXMLElement<CSDNP3PointsT>(sessionParserPtr->iomap);
        if(!sessionMap.analogueOutput.exists())
        {
            log.info("No analogue output points configured");
            return;
        }

        /* Allocate analogue binary input DNP db */
        lu_uint32_t dnpPointNum = sessionMap.analogueOutput.count();
        sessionPtr->m_db->pointDB->analogueOutput.size = dnpPointNum;
        sessionPtr->m_db->pointDB->analogueOutput.list = (TMWSDNPAOutputStr*)calloc(dnpPointNum, sizeof(TMWSDNPAOutputStr));

		/* Allocate analogue input observer list */
		IPointObserver** observerList;
		observerList = (IPointObserver**)calloc(dnpPointNum, sizeof(IPointObserver*));

		/* Initialise both lists in order to ignore gaps */
		for(lu_uint32_t i = 0; i < dnpPointNum; i++)
		{
			sessionPtr->m_db->pointDB->analogueOutput.list[i].enabled = LU_FALSE;
			observerList[i] = NULL;
		}
		sessionPtr->m_db->AOObserverList.setTable(observerList, dnpPointNum);


        /* Count number of DNP3 points and get max DNP3 id */
        Iterator<CSDNP3OutputPointT> itConfig = sessionMap.analogueOutput.all();
        /* TODO: pueyos_a - Improve lists of DNP TMWInputs/observers by not indexing them directly by DNP3 ID (probably using two lookup tables DNP3<->VPoint, or a hash table)
         * Note that these lists stores pointers (usually 4 Bytes in G3 MCM) and we can have up to 65536 DNP Points -- 262144 * 2 = 524288 Bytes, 512KiB
         * Please have in mind that defining only a DNP point in the 65535 index, reserves an entire block of 512KiB of memory unnecessarily.
         */

        /* Count number of DNP3 points and get max DNP3 id */
        bool validFound = false; //Watches if any valid point found
        lu_uint32_t protocolID;
        for(Iterator<CSDNP3OutputPointT> itConfig = sessionMap.analogueOutput.all(); itConfig; ++itConfig)
        {
            try
            {
                /* Skip points without valid protocol ID, disabled or with no related G3 point */
                protocolID = getProtocolPoint(itConfig);
                validFound = true;  //At least one found
                dnpPointNum = LU_MAX(protocolID, dnpPointNum);   //get the highest protocol ID
            }
            catch(const ParserException& e)
            {}  //Ignore cases: we are only checking for counting
        }
        if(!validFound)
        {
            log.info("No analogue output points configured");
            return;
        }
        dnpPointNum++;   //correct from 0-based index to size

        TMWSDNPAOutputListStr* pointDBListPtr = &(sessionPtr->m_db->pointDB->analogueOutput);
        /* Allocate analogue output DNP db */
        pointDBListPtr->size = dnpPointNum;
        pointDBListPtr->list = (TMWSDNPAOutputStr*)calloc(dnpPointNum, sizeof(TMWSDNPAOutputStr));

        /* Initialise list in order to ignore gaps */
        for(lu_uint32_t i = 0; i < dnpPointNum; i++)
        {
            pointDBListPtr->list[i].enabled = LU_FALSE;
        }

        /* Initialise analogue output database */
        for(Iterator<CSDNP3OutputPointT> itConfig = sessionMap.analogueOutput.all();
            itConfig;
            ++itConfig
           )
        {
            TMWSDNPAOutputStr* pointPtr;
            lu_uint32_t protocolID = 0;
            lu_uint16_t cLogicGroup = PointIdStr::invalidGroup;
            try
            {
                /* Skip points without valid protocol ID, disabled or with no related G3 out point */
                protocolID = getXMLAttr(itConfig.protocolID);
                cLogicGroup = getXMLAttr(itConfig.controlLogicGroup);
                if(!getXMLAttr(itConfig.enable))
                {
                    throw ParserException(CONFMGR_ERROR_NONE, "Protocol Point disabled");
                }
                pointPtr = &(pointDBListPtr->list[protocolID]);
                if(pointPtr == NULL)
                {
                    throw ParserException(CONFMGR_ERROR_NOT_INITIALIZED, "Database Point not available");
                }
                /* Valid protocol point. Read the configuration */
                pointPtr->logicGroup = cLogicGroup;
                pointPtr->dnp3Id = protocolID;
                /* FIXME: pueyos_a - Analogue Output should be initialised with the current output value & flags, whatever they are (?) */
                pointPtr->value.value.fval = 0;
                pointPtr->value.type = TMWTYPES_ANALOG_TYPE_SFLOAT;
                pointPtr->flags = DNPDEFS_DBAS_FLAG_ON_LINE;

                /* The Point's own Default Variation is used by default.
                 * If the Point's Default Variation is set to 0 the Session Default Variation for the Point is used.
                 * This means that a Point's Default Variation has a HIGHER priority than the Session's Default Variation.
                 * The default behaviour of the TMW library gives the Session's Default Variation a higher priority that the Point's.
                 * */
                checkXMLElement(sessionParserPtr->config);
                CObjDefaultVariationsT objDefaultVariations = sessionParserPtr->config.first().objDefaultVariations.first();


                /* TODO: pueyos_a - enable Analogue Output's default variation cfg */
                /* Set defaults first. This may be overridden by parsePointConfig() */
                pointPtr->defaultVariation = objDefaultVariations.obj40;
                pointPtr->eventDefaultVariation = objDefaultVariations.obj42;
                checkXMLElement(itConfig.outputEvent);
                pointPtr->eventLogStore = LU_TRUE;      //Store event by default
                getXMLOptAttr(itConfig.enableStoreEvent, pointPtr->eventLogStore);

                parsePointConfig(itConfig.outputEvent.first(), *pointPtr);

                /* Initialise callback structure */
                pointPtr->callbacks = &(sessionPtr->m_db->pointDB->callbacks);

                /* Point fully configured. We can enable it */
                pointPtr->enabled = LU_TRUE;

                /* Output points have a pointer to the session object which allows them
                 * to see the SRC ADDR of the Master trying to Select/Operate/Direct Operate them.  This allows
                 * the Output Point to disallow controls from a Broadcast Address.
                 */
                pointPtr->sclSessionPtrPtr = sessionPtr->getSCLSessionPtr();

                /* Create observers */
                pointID.group = cLogicGroup;
                pointID.ID = 0; // pseudo analogue point owned by the logic
				observerList[protocolID] = new DNP3SlaveAOObserver(pointID, protocolID, sessionPtr->m_db->pointDB);
				if (GDatabase.attach(pointID, sessionPtr->m_db->AOObserverList[protocolID])
						!= GDB_ERROR_NONE) {
					//create an always-offline point
					pointPtr->flags &= ~DNPDEFS_DBAS_FLAG_ON_LINE;
				}
            }
            catch(const ParserException& e)
            {
                if(e.getID() == CONFMGR_ERROR_NONE)
                {
                    log.debug("DNP3 Protocol analogue output point %d skipped: %s", protocolID, e.what());
                }
                else
                {
                    log.warn("DNP3 Protocol analogue output point %d skipped: %s", protocolID, e.what());
                    continue;
                }
            }
        }
    }
    catch (const ParserException& e)
    {
        log.error("Error reading DNP3 analogue output point configuration: %s", e.what());
    }
}


void XMLDNP3SlaveProtocolParser::parseFailoverGroupConfig(CConnectionManagerT&     conGroup,
                                                           DNP3SlaveProtocolChannel::Config *channelConfigPtr,
                                                           DNP3SlaveProtocolChannel  *channelPtr)
{
    if ((channelPtr == NULL) || (channelConfigPtr == NULL))
    {
        return;
    }

    ISlaveProtocolChannel::ScadaAddress scadaAddress;

    // Add the IP Address/Port etc as the first entry in the failover group
    if (channelConfigPtr->IOConfig.linTCP.mode == LINTCP_MODE_DUAL_ENDPOINT)
    {
        scadaAddress.port = channelConfigPtr->IOConfig.linTCP.dualEndPointIpPort;
    }
    else
    {
        scadaAddress.port = channelConfigPtr->IOConfig.linTCP.ipPort;
    }
    scadaAddress.ip = channelConfigPtr->IOConfig.linTCP.ipAddress;
    scadaAddress.masterAddress = channelConfigPtr->masterAddress;

    channelPtr->addScadaAddress(scadaAddress);

    DBG_INF("Primary SCADA [%s:%d] MA[%d]", scadaAddress.ip.c_str(), scadaAddress.port, scadaAddress.masterAddress);

    try
    {
        for (Iterator<CFailoverGroupT> itFailover = conGroup.failoverGroup.all();
                                          itFailover;
                                          ++itFailover)
        {
            scadaAddress.ip            = getXMLAttr(itFailover.ipAddress);
            scadaAddress.port          = getXMLAttr(itFailover.port);
            scadaAddress.masterAddress = getXMLAttr(itFailover.masterAddress);

            channelPtr->addScadaAddress(scadaAddress);

            DBG_INF("Failover SCADA [%s:%d] MA[%d]", scadaAddress.ip.c_str(), scadaAddress.port, scadaAddress.masterAddress);
        }
    }
    catch (const ParserException& e)
    {
        log.error("Error reading DNP3 Failover configuration: %s", e.what());
    }
}



void XMLDNP3SlaveProtocolParser::parseCommsPowerSupplyConfig(CCommsPowerCycleT&     commsPowerCycleXML,
                                                              IIOModuleManager&         moduleManager,
                                                              SCADAConnectionManager::SCADAConnManConfig& config)
{
    try
    {
        CChannelRefT powerSupplyChan = getXMLElement<CChannelRefT>(commsPowerCycleXML.commsPowerSupply);
        SCADAConnectionManager::CommsPowerSupplyConfig commsPowerSupplyConfig;

        ChannelRef channel = getXMLChannelConf(powerSupplyChan);

        IIOModule* modulePtr = moduleManager.getModule(channel.moduleType, channel.moduleID);
        if (modulePtr == NULL)
        {
            IOModuleIDStr moduleID(channel.moduleType, channel.moduleID);
            std::string errMsg;
            errMsg = "module " + moduleID.toString() + " not available";
            throw ParserException(CONFMGR_ERROR_NOT_INITIALIZED, errMsg.c_str());
        }
        commsPowerSupplyConfig.powerSupplyChanPtr = modulePtr->getChannel(CHANNEL_TYPE_PSUPPLY, channel.channelID);
        DBG_INF("Power Supply Channel [%s]", commsPowerSupplyConfig.powerSupplyChanPtr->getName());

        if(commsPowerCycleXML.powerCycleInhibitPoint.exists())
        {
            PointIdStr& inhibit = commsPowerSupplyConfig.powerCycleInhibit;
            inhibit.group = getXMLAttr(commsPowerCycleXML.powerCycleInhibitPoint.first().group);
            inhibit.ID = getXMLAttr(commsPowerCycleXML.powerCycleInhibitPoint.first().id);
            DBG_INF("Power Cycle Inhibit Point:%s", inhibit.toString().c_str());
        }


        commsPowerSupplyConfig.durationSecs = getXMLAttr(commsPowerCycleXML.durationSecs);
        commsPowerSupplyConfig.initTimeSecs = getXMLAttr(commsPowerCycleXML.initTimeSecs);

        config.m_commsPowerSupplyConfig = commsPowerSupplyConfig;
    }
    catch(const ParserException& e)
    {
        log.error("Error reading DNP3 Comms supply configuration: %s", e.what());
    }
}


void XMLDNP3SlaveProtocolParser::parseConnectivityCheckConfig(CIpConnectivityCheckT& conCheckXML,
                                                               SCADAConnectionManager::SCADAConnManConfig& config)
{
    try
    {
        SCADAConnectionManager::IPConnectivityCheckConfig conCheckConfig;
        conCheckConfig.enabled = LU_TRUE;
        conCheckConfig.checkIPPort = getXMLAttr(conCheckXML.port);
        conCheckConfig.periodMin   = getXMLAttr(conCheckXML.periodMins);
        conCheckConfig.retries     = getXMLAttr(conCheckXML.retries);
        config.m_connectivityCheckConfig = conCheckConfig;
    }
    catch(const ParserException& e)
    {
        log.error("Error reading DNP3 Connectivity check configuration: %s", e.what());
    }
}


void XMLDNP3SlaveProtocolParser::parseOutgoingConnectionsConfig(COutgoingConnectionsT& outgoingConnectionsXML,
                                                                 SCADAConnectionManager::SCADAConnManConfig& config)
{
    try
    {
        SCADAConnectionManager::OutgoingConnectionsConfig outgoingConnectionsConfig;
        outgoingConnectionsConfig.enabled = LU_TRUE;
        outgoingConnectionsConfig.retries = getXMLAttr(outgoingConnectionsXML.retries);
        config.m_outgoingConConfig = outgoingConnectionsConfig;
    }
    catch(const ParserException& e)
    {
        log.error("Error reading DNP3 Outgoing connections configuration: %s", e.what());
    }
}


void XMLDNP3SlaveProtocolParser::parseIncomingConnectionsConfig(CIncomingConnectionsT& incomingConnectionsXML,
                                                                 SCADAConnectionManager::SCADAConnManConfig& config)
{
    try
    {
        SCADAConnectionManager::IncomingConnectionsConfig incomingConnectionsConfig;
        incomingConnectionsConfig.enabled         = LU_TRUE;
        incomingConnectionsConfig.notReceivedHrs = getXMLAttr(incomingConnectionsXML.notReceivedHours);
        config.m_incomingConConfig = incomingConnectionsConfig;
    }
    catch(const ParserException& e)
    {
        log.error("Error reading DNP3 Incoming connections configuration: %s", e.what());
    }
}


void XMLDNP3SlaveProtocolParser::parseConnectionSettingsConfig(CConnectionManagerT& connectionSettingsXML,
                                                                SCADAConnectionManager::SCADAConnManConfig& config)
{
    try
    {
        SCADAConnectionManager::ConnectionSettingsConfig connectionSettingsConfig;
        connectionSettingsConfig.inactivityTimeSecs       = getXMLAttr(connectionSettingsXML.inactivityTimeoutSecs);
        connectionSettingsConfig.connectOnEventClassMask  = getXMLAttr(connectionSettingsXML.connectionClass);
        connectionSettingsConfig.connectionRetryDelaySecs = getXMLAttr(connectionSettingsXML.connectRetryDelaySecs);
        config.m_connectionSettingsConfig = connectionSettingsConfig;
    }
    catch(const ParserException& e)
    {
        log.error("Error reading DNP3 Connection settings configuration: %s", e.what());
    }
}

/*
 * Initialise secure authentication
 * TODO read all configuration from XML.
 */
#if SDNPDATA_SUPPORT_OBJ120
static void parseAuthentication(CAuthenticationT xml, DNP3SlaveProtocolSession::Config& config)
{
  SDNPSESN_CONFIG& sesnConfig = config.sessionConfig;
  SDNPSESN_AUTH_CONFIG *pAuthCnfg = &sesnConfig.authConfig;
  g3schema::ns_sdnp3::CAuthUsersT xmlUsers = xml.users.first();

  config.sessionConfig .authenticationEnabled   = getXMLAttr(xml.authenticationEnabled);
  config.sessionConfig .authSecStatMaxEvents    = getXMLAttr(xml.authSecStatMaxEvents);
  config.sessionConfig .authSecStatEventMode    = getXMLAttrEnum<TMWDEFS_EVENT_MODE>(xml.authSecStatEventMode, LU_EVENT_MODE_VALUEfromINDEX);
  sesnConfig.authConfig.assocId                 = getXMLAttr(xml.authConfig.first().assocId);
  sesnConfig.authConfig.MACAlgorithm            = getXMLAttr(xml.authConfig.first().MACAlgorithm);
  sesnConfig.authConfig.maxApplTimeoutCount     = getXMLAttr(xml.authConfig.first().maxApplTimeoutCount);
  sesnConfig.authConfig.replyTimeout            = getXMLAttr(xml.authConfig.first().replyTimeout);
  sesnConfig.authConfig.keyChangeInterval       = getXMLAttr(xml.authConfig.first().keyChangeInterval);
  sesnConfig.authConfig.maxKeyChangeCount       = getXMLAttr(xml.authConfig.first().maxKeyChangeCount);
  sesnConfig.authConfig.aggressiveModeSupport   = getXMLAttr(xml.authConfig.first().aggressiveModeSupport);
  sesnConfig.authConfig.disallowSHA1            = getXMLAttr(xml.authConfig.first().disallowSHA1);
  sesnConfig.authConfig.extraDiags              = getXMLAttr(xml.authConfig.first().extraDiags);
  sesnConfig.authConfig.operateInV2Mode         = getXMLAttr(xml.authConfig.first().operateInV2Mode);

  DBG_INFO("Session authentication config:\n"
                  "authenticationEnabled   :%d\n"
                  "authSecStatMaxEvents    :%d\n"
                  "authSecStatEventMode    :%d\n"
                  "assocId                 :%d\n"
                  "MACAlgorithm            :%d\n"
                  "maxApplTimeoutCount     :%d\n"
                  "replyTimeout            :%lu\n"
                  "keyChangeInterval       :%lu\n"
                  "maxKeyChangeCount       :%d\n"
                  "aggressiveModeSupport   :%d\n"
                  "disallowSHA1            :%d\n"
                  "extraDiags              :%d\n"
                  "operateInV2Mode         :%d\n",
                  config.sessionConfig .authenticationEnabled,
                  config.sessionConfig .authSecStatMaxEvents ,
                  config.sessionConfig .authSecStatEventMode ,
                  sesnConfig.authConfig.assocId              ,
                  sesnConfig.authConfig.MACAlgorithm         ,
                  sesnConfig.authConfig.maxApplTimeoutCount  ,
                  sesnConfig.authConfig.replyTimeout         ,
                  sesnConfig.authConfig.keyChangeInterval    ,
                  sesnConfig.authConfig.maxKeyChangeCount    ,
                  sesnConfig.authConfig.aggressiveModeSupport,
                  sesnConfig.authConfig.disallowSHA1         ,
                  sesnConfig.authConfig.extraDiags           ,
                  sesnConfig.authConfig.operateInV2Mode      );

#if SDNPCNFG_SUPPORT_SA_VERSION2
  sesnConfig.authConfig.maxErrorCount = getXMLAttr(xml.authConfigV2.first().maxErrorCount);
  sesnConfig.authConfig.operateInV2Mode = TMWDEFS_TRUE;

  {
      /* Configure V2 users numbers*/
      lu_uint32_t i;
      lu_uint32_t usersNumber = xml.users.first().user.count();
      for (i = 0; i < usersNumber && i < DNPCNFG_AUTHV2_MAX_NUMBER_USERS; ++ i)
      {
        sesnConfig.authConfig.authUsers[i].userNumber = getXMLAttr(xmlUsers.user[i].userNumber);
      }
  }
#endif

#if SDNPCNFG_SUPPORT_SA_VERSION5
  sesnConfig.authConfig.randomChallengeDataLength = getXMLAttr(xml.authConfigV5.first().randomChallengeDataLength);
  sesnConfig.authConfig.maxSessionKeyStatusCount = getXMLAttr(xml.authConfigV5.first().maxSessionKeyStatusCount);
  sesnConfig.authConfig.maxAuthenticationFailures = getXMLAttr(xml.authConfigV5.first().maxAuthenticationFailures);
  sesnConfig.authConfig.maxReplyTimeouts = getXMLAttr(xml.authConfigV5.first().maxReplyTimeouts);
  sesnConfig.authConfig.maxAuthenticationRekeys = getXMLAttr(xml.authConfigV5.first().maxAuthenticationRekeys);
  sesnConfig.authConfig.maxErrorMessagesSent = getXMLAttr(xml.authConfigV5.first().maxErrorMessagesSent);
#endif

  /* Read users database configuration*/
  config.usersNumber = xml.users.first().user.count();
  if(config.usersNumber > LUCYCRYPTO_MAX_USERS)
      config.usersNumber = LUCYCRYPTO_MAX_USERS;

  lu_uint32_t i;
  std::vector<unsigned char> updateKey;
  for (i = 0; i < config.usersNumber; ++ i)
  {
      updateKey = xmlUsers.user[i].updateKey;

      memcpy(config.users[i].updateKey, &(updateKey[0]), updateKey.size());

      config.users[i].updateKeyLen = updateKey.size();
      config.users[i].userNumber   = getXMLAttr(xmlUsers.user[i].userNumber);
      config.users[i].userRole     = getXMLAttr(xmlUsers.user[i].userRole);

      DBG_INFO("Configuring users database. User Number:%d User Key Length: %d",
                      config.users[i].userNumber,
                      config.users[i].updateKeyLen
                      );

#ifndef NDEBUG
        printf("Update key: ");
        for (int j = 0; j < config.users[i].updateKeyLen; ++j)
        {
            printf("0x%02x ", config.users[i].updateKey[j]);
        }
        printf("\n");
#endif
  }


  DBG_INFO("Using SAv%c", pAuthCnfg->operateInV2Mode?'2':'5');
}
#endif

void XMLDNP3SlaveProtocolParser::parseSessionConfig(CSDNP3SesnT&                                           sessionConfig,
                                                     DNP3SlaveProtocolSession::Config& sessionConfigStr,
                                                     bool                                                      enableBroadcast)
{
    try
    {
        // Initialise the TMW Structures
        sdnpsesn_initConfig(&(sessionConfigStr.sessionConfig));
        CSDNP3SesnConfT sessionDNP3 = getXMLElement<CSDNP3SesnConfT>(sessionConfig.config);

        /* Override default configuration using the XML configuration */

        //Force clockValidPeriod to be 0 to do not use Triangle's time synch auto-expiration
        sessionConfigStr.sessionConfig.clockValidPeriod      = 0;

        sessionConfigStr.sessionConfig.coldRestartDelay       = 60000; //Approx 60sec - guidance value only
        sessionConfigStr.sessionConfig.warmRestartDelay       = 20000; //Approx 15sec - guidance value only
        sessionConfigStr.sessionConfig.source                 = getXMLAttr(sessionDNP3.address.first().source);
        sessionConfigStr.sessionConfig.destination            = getXMLAttr(sessionDNP3.address.first().destination);
        sessionConfigStr.sessionConfig.enableSelfAddress      = getXMLAttr(sessionDNP3.selfAddress);
        sessionConfigStr.sessionConfig.validateSourceAddress  = getXMLAttr(sessionDNP3.validateSourceAddress);
        sessionConfigStr.sessionConfig.linkStatusPeriod       = getXMLAttr(sessionDNP3.linkStatusPeriod);
        sessionConfigStr.sessionConfig.multiFragRespAllowed   = getXMLAttr(sessionDNP3.multiFragRespAllowed);
        sessionConfigStr.sessionConfig.multiFragConfirm       = getXMLAttr(sessionDNP3.multiFragConfirm);
        sessionConfigStr.sessionConfig.respondNeedTime        = getXMLAttr(sessionDNP3.respondNeedTime);
        sessionConfigStr.sessionConfig.applConfirmTimeout     = getXMLAttr(sessionDNP3.applConfirmTimeout);
        sessionConfigStr.sessionConfig.selectTimeout          = getXMLAttr(sessionDNP3.selectTimeout);
        sessionConfigStr.sessionConfig.unsolAllowed           = getXMLAttr(sessionDNP3.unsolAllowed);
        sessionConfigStr.sessionConfig.unsolSendIdenticalRetry= getXMLAttr(sessionDNP3.identicalRetriesAllowed);
        sessionConfigStr.sessionConfig.unsolClassMask         = getXMLAttr(sessionDNP3.unsolClassMask);
        sessionConfigStr.sessionConfig.unsolClass1MaxEvents   = getXMLAttr(sessionDNP3.unsolClassMaxEvents.first().class1);
        sessionConfigStr.sessionConfig.unsolClass2MaxEvents   = getXMLAttr(sessionDNP3.unsolClassMaxEvents.first().class2);
        sessionConfigStr.sessionConfig.unsolClass3MaxEvents   = getXMLAttr(sessionDNP3.unsolClassMaxEvents.first().class3);
        sessionConfigStr.sessionConfig.unsolClass1MaxDelay    = getXMLAttr(sessionDNP3.unsolClassMaxDelays.first().class1);
        sessionConfigStr.sessionConfig.unsolClass2MaxDelay    = getXMLAttr(sessionDNP3.unsolClassMaxDelays.first().class2);
        sessionConfigStr.sessionConfig.unsolClass3MaxDelay    = getXMLAttr(sessionDNP3.unsolClassMaxDelays.first().class3);
        /* Set retry mode for unsolicited events */
        lu_int32_t unsolRetryMode = UNSOLICITED_RETRY_MODE_WITH_OFFLINE;
        getXMLOptAttr(sessionDNP3.unsolRetryMode, unsolRetryMode);
        switch(unsolRetryMode)
        {
            case UNSOLICITED_RETRY_MODE_NO_RETRY:
            {
                sessionConfigStr.sessionConfig.unsolMaxRetries        = 0;
                sessionConfigStr.sessionConfig.unsolRetryDelay        = 0;
                sessionConfigStr.sessionConfig.unsolOfflineRetryDelay = UNSOL_NORETRY;
                TMWTYPES_MILLISECONDS unsolConfirmTimeout = getXMLAttr(sessionDNP3.unsolConfirmTimeout);
                if(unsolConfirmTimeout != 0)
                {
                    sessionConfigStr.sessionConfig.unsolConfirmTimeout    = unsolConfirmTimeout;
                }
                //else use default sessionConfigStr.sessionConfig.unsolConfirmTimeout value
                break;
            }
            case UNSOLICITED_RETRY_MODE_SIMPLE_RETRY:
                sessionConfigStr.sessionConfig.unsolMaxRetries        = getXMLAttr(sessionDNP3.unsolMaxRetries);
                sessionConfigStr.sessionConfig.unsolRetryDelay        = getXMLAttr(sessionDNP3.unsolRetryDelay);
                sessionConfigStr.sessionConfig.unsolOfflineRetryDelay = UNSOL_NORETRY;
                sessionConfigStr.sessionConfig.unsolConfirmTimeout    = getXMLAttr(sessionDNP3.unsolConfirmTimeout);
                break;
            case UNSOLICITED_RETRY_MODE_UNLIMITED:
                //Note that in TMW, setting unsolOfflineRetryDelay == unsolRetryDelay repeats the same retry
                sessionConfigStr.sessionConfig.unsolMaxRetries        = 1;
                sessionConfigStr.sessionConfig.unsolRetryDelay        = getXMLAttr(sessionDNP3.unsolRetryDelay);
                sessionConfigStr.sessionConfig.unsolOfflineRetryDelay = sessionConfigStr.sessionConfig.unsolRetryDelay;
                sessionConfigStr.sessionConfig.unsolConfirmTimeout    = getXMLAttr(sessionDNP3.unsolConfirmTimeout);
                break;
            default:
                //Default case is UNSOLICITED_RETRY_MODE_WITH_OFFLINE:
                sessionConfigStr.sessionConfig.unsolMaxRetries        = getXMLAttr(sessionDNP3.unsolMaxRetries);
                sessionConfigStr.sessionConfig.unsolRetryDelay        = getXMLAttr(sessionDNP3.unsolRetryDelay);
                sessionConfigStr.sessionConfig.unsolOfflineRetryDelay = getXMLAttr(sessionDNP3.unsolOfflineRetryDelay);
                sessionConfigStr.sessionConfig.unsolConfirmTimeout    = getXMLAttr(sessionDNP3.unsolConfirmTimeout);
                break;
        }

        // The TMW library will use these Session Default Variations for all points if set to non-zero.
        // Setting them to zero enables per-point Default Variations to be used which is more
        // intuitive to the User.  Config Tools can be used to apply Session Default Variations to points.
        sessionConfigStr.sessionConfig.obj01DefaultVariation = 0; //objDefaultVariations.obj01;
        sessionConfigStr.sessionConfig.obj02DefaultVariation = 0; //objDefaultVariations.obj02;
        sessionConfigStr.sessionConfig.obj03DefaultVariation = 0; //objDefaultVariations.obj03;
        sessionConfigStr.sessionConfig.obj04DefaultVariation = 0; //objDefaultVariations.obj04;
        sessionConfigStr.sessionConfig.obj20DefaultVariation = 0; //objDefaultVariations.obj20;
        sessionConfigStr.sessionConfig.obj21DefaultVariation = 0; //objDefaultVariations.obj21;
        sessionConfigStr.sessionConfig.obj22DefaultVariation = 0; //objDefaultVariations.obj22;
        sessionConfigStr.sessionConfig.obj23DefaultVariation = 0; //objDefaultVariations.obj23;
        sessionConfigStr.sessionConfig.obj30DefaultVariation = 0; //objDefaultVariations.obj30;
        sessionConfigStr.sessionConfig.obj32DefaultVariation = 0; //objDefaultVariations.obj32;
        sessionConfigStr.sessionConfig.obj40DefaultVariation = 0; //objDefaultVariations.obj40;
        sessionConfigStr.sessionConfig.obj42DefaultVariation = 0; //objDefaultVariations.obj42;

        CSesnEventConfT bInputEv = getXMLElement<CSesnEventConfT>(sessionDNP3.binaryInputEvent);
        CSesnEventConfT dInputEv = getXMLElement<CSesnEventConfT>(sessionDNP3.doubleInputEvent);
        CSesnEventConfT aInputEv = getXMLElement<CSesnEventConfT>(sessionDNP3.analogInputEvent);
        CSesnEventConfT cInputEv = getXMLElement<CSesnEventConfT>(sessionDNP3.counterEvent);
        CSesnEventConfT fInputEv = getXMLElement<CSesnEventConfT>(sessionDNP3.frozenCounterEvent);
        CSesnEventConfT aOutputEv = getXMLElement<CSesnEventConfT>(sessionDNP3.analogOutputEvent);

        sessionConfigStr.sessionConfig.binaryInputMaxEvents = getXMLAttr(bInputEv.maxEvents);
        sessionConfigStr.sessionConfig.binaryInputEventMode = getXMLAttrEnum<TMWDEFS_EVENT_MODE>(bInputEv.eventMode, LU_EVENT_MODE_VALUEfromINDEX);
        sessionConfigStr.sessionConfig.doubleInputMaxEvents = getXMLAttr(dInputEv.maxEvents);
        sessionConfigStr.sessionConfig.doubleInputEventMode = getXMLAttrEnum<TMWDEFS_EVENT_MODE>(dInputEv.eventMode, LU_EVENT_MODE_VALUEfromINDEX);
        sessionConfigStr.sessionConfig.analogInputMaxEvents = getXMLAttr(aInputEv.maxEvents);
        sessionConfigStr.sessionConfig.analogInputEventMode = getXMLAttrEnum<TMWDEFS_EVENT_MODE>(aInputEv.eventMode, LU_EVENT_MODE_VALUEfromINDEX);
        sessionConfigStr.sessionConfig.binaryCounterMaxEvents = getXMLAttr(cInputEv.maxEvents);
        sessionConfigStr.sessionConfig.binaryCounterEventMode = getXMLAttrEnum<TMWDEFS_EVENT_MODE>(cInputEv.eventMode, LU_EVENT_MODE_VALUEfromINDEX);
        sessionConfigStr.sessionConfig.frozenCounterMaxEvents = getXMLAttr(fInputEv.maxEvents);
        sessionConfigStr.sessionConfig.frozenCounterEventMode = getXMLAttrEnum<TMWDEFS_EVENT_MODE>(fInputEv.eventMode, LU_EVENT_MODE_VALUEfromINDEX);
        sessionConfigStr.sessionConfig.analogOutputMaxEvents = getXMLAttr(aOutputEv.maxEvents);
        sessionConfigStr.sessionConfig.analogOutputEventMode = getXMLAttrEnum<TMWDEFS_EVENT_MODE>(aOutputEv.eventMode, LU_EVENT_MODE_VALUEfromINDEX);

        //Note that this defaults to FALSE in the TMW Stack, while we want it to default to TRUE.
        sessionConfigStr.sessionConfig.deleteOldestEvent = TMWDEFS_TRUE;    //Default value
        lu_uint32_t evtBehaviour = 0;
        getXMLOptAttr(sessionDNP3.evtBufOverflowBehaviour, sessionConfigStr.sessionConfig.deleteOldestEvent);
        sessionConfigStr.sessionConfig.deleteOldestEvent = (evtBehaviour == 0)? TMWDEFS_TRUE : TMWDEFS_FALSE;

        // DNP 3.17 Session Config
        // By default we will NOT allow ANY Function Codes on the Broadcast Address
        sessionConfigStr.sessionConfig.enabledBroadcastFCs = 0;
        sessionConfigStr.sessionConfig.enabledBroadcastWrites = 0;

        // By default we will ONLY allow writing of the Clock on the RTU to the Broadcast Address
        if (enableBroadcast == true)
        {
            // Enable/Disable Controls using the DNP3 Broadcast Address
            // This is in the Schema but it has been decided (SW Meeting 2014/08/13) that this feature should
            // always be disabled
        	// TODO - SKA - will be replaced with a broadcast controls mask
            bool allowBcastCtrls = false;
            getXMLOptAttr(sessionDNP3.allowControlsOnSelfAddress, allowBcastCtrls);
            sessionConfigStr.lucy_allowControlOnBrdCstAddr = allowBcastCtrls;
            sessionConfigStr.sessionConfig.enabledBroadcastFCs =
                            SDNPSESN_ENABLE_FC_WRITE;

            if(allowBcastCtrls)
            {
            	log.info("Enabled controls over broadcast.");
            	/* NOTE: (pueyos_a) OPERATE & DIRECT_OP are required for DNP3 conformance
            	 * 		tests in the 8.6.5 group. The rest is meant to be in a wider
            	 * 		configuration where the user selects the FCs to be available for
            	 * 		responding to broadcast.
				 */
            	sessionConfigStr.sessionConfig.enabledBroadcastFCs |=
                            SDNPSESN_ENABLE_FC_OPERATE           |
                            SDNPSESN_ENABLE_FC_DIRECT_OP         |
                            SDNPSESN_ENABLE_FC_DIRECT_OP_NOACK;	 //|
//                            SDNPSESN_ENABLE_FC_FRZ               |
//                            SDNPSESN_ENABLE_FC_FRZ_NOACK         |
//                            SDNPSESN_ENABLE_FC_FRZ_CLEAR         |
//                            SDNPSESN_ENABLE_FC_FRZ_CLEAR_NOACK   |
//                            SDNPSESN_ENABLE_FC_FRZ_TIME          |
//                            SDNPSESN_ENABLE_FC_FRZ_TIME_NOACK    |
//                            SDNPSESN_ENABLE_FC_COLD_RESTART      |
//                            SDNPSESN_ENABLE_FC_WARM_RESTART      |
//                            SDNPSESN_ENABLE_FC_INIT_DATA         |
//                            SDNPSESN_ENABLE_FC_INIT_APP          |
//                            SDNPSESN_ENABLE_FC_START_APP         |
//                            SDNPSESN_ENABLE_FC_STOP_APP          |
//                            SDNPSESN_ENABLE_FC_SAVE_CONFIG       |
//                            SDNPSESN_ENABLE_FC_ENABLE_UNSOL      |
//                            SDNPSESN_ENABLE_FC_DISABLE_UNSOL     |
//                            SDNPSESN_ENABLE_FC_ASSIGN_CLASS      |
//                            SDNPSESN_ENABLE_FC_DELAY_MEASURE     |
//                            SDNPSESN_ENABLE_FC_RECORD_CTIME      |
//                            SDNPSESN_ENABLE_FC_ACTIVATE_CONFIG;
            }

            sessionConfigStr.sessionConfig.enabledBroadcastWrites =
                        SDNPSESN_ENABLE_WRITE_CLOCK          |
                        SDNPSESN_ENABLE_WRITE_LRTIME         |
                        SDNPSESN_ENABLE_WRITE_CRESTART       |
                        SDNPSESN_ENABLE_WRITE_OTHER;
        }


        #if SDNPDATA_SUPPORT_OBJ120
        if(sessionConfig.config.first().authentication.exists())
        {
         parseAuthentication(sessionConfig.config.first().authentication.first(), sessionConfigStr);
        }
        #endif
    }
    catch(const ParserException& e)
    {
        log.error("DNP3 protocol session configuration Error: %s", e.what());
    }
}


void XMLDNP3SlaveProtocolParser::parseChannelSerialConfig(CDNP3LinuxIoT&                     linuxIo,
                                                           CommsDeviceManager&               commsDeviceManager,
                                                           DNP3SlaveProtocolChannel::Config& channelConfigStr,
                                                           DialupModem **modemPtrPtr,
                                                           lu_uint8_t   *connectionClassPtr)
{
    try
    {
        if (channelConfigStr.IOConfig.lin232.bModemFitted == TMWDEFS_TRUE)
        {
            /* Read commsDevice configuration */
            CSDNP3CommsDeviceConfT commsDev = getXMLElement<CSDNP3CommsDeviceConfT>(linuxIo.commsDevice);

            if ((*modemPtrPtr = commsDeviceManager.getDialupModem((lu_uint32_t) commsDev.commsDeviceID)) == NULL)
            {
                log.error("CommsDevice [%d] not found for Slave DNP3 Channel [%s]\n", commsDev.commsDeviceID, channelConfigStr.channelName.c_str());
            }

            *connectionClassPtr = getXMLAttr(commsDev.connectionClass);
        }
        else
        {
            /* Read serial configuration */
            CSerialConfT SerialConfigStr = getXMLElement<CSerialConfT>(linuxIo.serial);

            // Lookup the Serial Port Name and retrieve a Port object from the Port Manager!
            // Apply the settings from this Port object to the LinIo Library

            Port* portObj;
            // Copy the Port String
            std::string portString = getXMLAttr(SerialConfigStr.serialPortName);

            if ((portObj = portManager.getPort(portString)) == NULL)
            {
                std::string errMsg;
                errMsg = "Port [" + portString + "] not found";
                throw ParserException(CONFMGR_ERROR_CONFIG, errMsg.c_str());
            }

            // Use the settings from the Port Object
            channelConfigStr.IOConfig.lin232.baudRate    = portObj->serialConf.serBaudrate;
            channelConfigStr.IOConfig.lin232.numDataBits = portObj->serialConf.serDataBits;
            channelConfigStr.IOConfig.lin232.numStopBits = portObj->serialConf.serStopBits;
            channelConfigStr.IOConfig.lin232.parity      = portObj->serialConf.serParity;
            channelConfigStr.IOConfig.lin232.portMode    = portObj->serialConf.serPortMode;
            channelConfigStr.IOConfig.lin232.ctsLowCheck = portObj->serialConf.serCTSlowCheck;
            
            stringToCString(channelConfigStr.channelName, channelConfigStr.IOConfig.lin232.chnlName, LINIOCNFG_MAX_NAME_LEN);
            stringToCString(portObj->getPortPath(), channelConfigStr.IOConfig.lin232.portName, LINIOCNFG_MAX_NAME_LEN);

            // Not relevant to DNP3 Serial, force to FALSE
            channelConfigStr.IOConfig.lin232.bModbusRTU   = TMWDEFS_FALSE;
            channelConfigStr.IOConfig.lin232.bModemFitted = TMWDEFS_FALSE;
        }
    }
    catch(const ParserException& e)
    {
        log.error("Error reading DNP3 Channel serial configuration: %s", e.what());
    }

}

/*
 *********************** End of file ******************************************
 */
