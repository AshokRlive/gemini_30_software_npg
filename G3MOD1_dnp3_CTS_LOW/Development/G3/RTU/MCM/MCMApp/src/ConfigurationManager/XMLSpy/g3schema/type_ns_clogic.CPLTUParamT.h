#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CPLTUParamT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CPLTUParamT

#include "type_ns_clogic.CCLogicParametersBaseT.h"


namespace g3schema
{

namespace ns_clogic
{	

class CPLTUParamT : public ::g3schema::ns_clogic::CCLogicParametersBaseT
{
public:
	g3schema_EXPORT CPLTUParamT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CPLTUParamT(CPLTUParamT const& init);
	void operator=(CPLTUParamT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_clogic_altova_CPLTUParamT); }

	MemberAttribute<unsigned,_altova_mi_ns_clogic_altova_CPLTUParamT_altova_voltageMismatchDelaySec, 0, 0> voltageMismatchDelaySec;	// voltageMismatchDelaySec CunsignedInt
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_clogic

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CPLTUParamT
