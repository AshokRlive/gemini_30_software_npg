#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen1_ALTOVA_CVARIATION_GROUP1
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen1_ALTOVA_CVARIATION_GROUP1



namespace g3schema
{

namespace ns_gen1
{	

class CVARIATION_GROUP1 : public TypeBase
{
public:
	g3schema_EXPORT CVARIATION_GROUP1(xercesc::DOMNode* const& init);
	g3schema_EXPORT CVARIATION_GROUP1(CVARIATION_GROUP1 const& init);
	void operator=(CVARIATION_GROUP1 const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen1_altova_CVARIATION_GROUP1); }

	enum EnumValues {
		Invalid = -1,
		k_PACKED_FORMAT = 0, // PACKED_FORMAT
		k_WITH_FLAGS = 1, // WITH_FLAGS
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen1

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen1_ALTOVA_CVARIATION_GROUP1
