#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CCommsPowerCycleT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CCommsPowerCycleT



namespace g3schema
{

namespace ns_sdnp3
{	

class CCommsPowerCycleT : public TypeBase
{
public:
	g3schema_EXPORT CCommsPowerCycleT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CCommsPowerCycleT(CCommsPowerCycleT const& init);
	void operator=(CCommsPowerCycleT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_sdnp3_altova_CCommsPowerCycleT); }

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CCommsPowerCycleT_altova_durationSecs, 0, 0> durationSecs;	// durationSecs CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CCommsPowerCycleT_altova_initTimeSecs, 0, 0> initTimeSecs;	// initTimeSecs CunsignedInt
	MemberElement<ns_common::CChannelRefT, _altova_mi_ns_sdnp3_altova_CCommsPowerCycleT_altova_commsPowerSupply> commsPowerSupply;
	struct commsPowerSupply { typedef Iterator<ns_common::CChannelRefT> iterator; };
	MemberElement<ns_sdnp3::CIpConnectivityCheckT, _altova_mi_ns_sdnp3_altova_CCommsPowerCycleT_altova_ipConnectivityCheck> ipConnectivityCheck;
	struct ipConnectivityCheck { typedef Iterator<ns_sdnp3::CIpConnectivityCheckT> iterator; };
	MemberElement<ns_sdnp3::COutgoingConnectionsT, _altova_mi_ns_sdnp3_altova_CCommsPowerCycleT_altova_outgoingConnections> outgoingConnections;
	struct outgoingConnections { typedef Iterator<ns_sdnp3::COutgoingConnectionsT> iterator; };
	MemberElement<ns_sdnp3::CIncomingConnectionsT, _altova_mi_ns_sdnp3_altova_CCommsPowerCycleT_altova_incomingConnections> incomingConnections;
	struct incomingConnections { typedef Iterator<ns_sdnp3::CIncomingConnectionsT> iterator; };
	MemberElement<ns_sdnp3::CPowerCycleInhibitPointT, _altova_mi_ns_sdnp3_altova_CCommsPowerCycleT_altova_powerCycleInhibitPoint> powerCycleInhibitPoint;
	struct powerCycleInhibitPoint { typedef Iterator<ns_sdnp3::CPowerCycleInhibitPointT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_sdnp3

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CCommsPowerCycleT
