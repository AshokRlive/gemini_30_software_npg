#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen10_ALTOVA_CLINK_MODE
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen10_ALTOVA_CLINK_MODE



namespace g3schema
{

namespace ns_gen10
{	

class CLINK_MODE : public TypeBase
{
public:
	g3schema_EXPORT CLINK_MODE(xercesc::DOMNode* const& init);
	g3schema_EXPORT CLINK_MODE(CLINK_MODE const& init);
	void operator=(CLINK_MODE const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen10_altova_CLINK_MODE); }

	enum EnumValues {
		Invalid = -1,
		k_LINK_MODE_BALANCED = 0, // LINK_MODE_BALANCED
		k_LINK_MODE_UNBALANCED = 1, // LINK_MODE_UNBALANCED
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen10

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen10_ALTOVA_CLINK_MODE
