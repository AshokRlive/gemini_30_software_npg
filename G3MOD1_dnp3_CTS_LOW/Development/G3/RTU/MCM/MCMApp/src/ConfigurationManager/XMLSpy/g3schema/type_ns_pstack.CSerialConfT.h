#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_pstack_ALTOVA_CSerialConfT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_pstack_ALTOVA_CSerialConfT



namespace g3schema
{

namespace ns_pstack
{	

class CSerialConfT : public TypeBase
{
public:
	g3schema_EXPORT CSerialConfT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSerialConfT(CSerialConfT const& init);
	void operator=(CSerialConfT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_pstack_altova_CSerialConfT); }

	MemberAttribute<string_type,_altova_mi_ns_pstack_altova_CSerialConfT_altova_serialPortName, 0, 0> serialPortName;	// serialPortName Cstring
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_pstack

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_pstack_ALTOVA_CSerialConfT
