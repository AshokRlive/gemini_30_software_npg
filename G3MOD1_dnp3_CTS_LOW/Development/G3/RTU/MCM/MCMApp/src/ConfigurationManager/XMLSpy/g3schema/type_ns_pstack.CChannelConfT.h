#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_pstack_ALTOVA_CChannelConfT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_pstack_ALTOVA_CChannelConfT



namespace g3schema
{

namespace ns_pstack
{	

class CChannelConfT : public TypeBase
{
public:
	g3schema_EXPORT CChannelConfT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CChannelConfT(CChannelConfT const& init);
	void operator=(CChannelConfT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_pstack_altova_CChannelConfT); }

	MemberAttribute<string_type,_altova_mi_ns_pstack_altova_CChannelConfT_altova_chnlName, 0, 0> chnlName;	// chnlName CChannelNameT

	MemberAttribute<bool,_altova_mi_ns_pstack_altova_CChannelConfT_altova_enabled, 0, 0> enabled;	// enabled Cboolean

	MemberAttribute<string_type,_altova_mi_ns_pstack_altova_CChannelConfT_altova_uuid, 0, 0> uuid;	// uuid Cstring
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_pstack

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_pstack_ALTOVA_CChannelConfT
