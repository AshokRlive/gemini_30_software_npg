#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CSCMModuleT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CSCMModuleT

#include "type_ns_g3module.CBaseModuleT.h"


namespace g3schema
{

namespace ns_g3module
{	

class CSCMModuleT : public ::g3schema::ns_g3module::CBaseModuleT
{
public:
	g3schema_EXPORT CSCMModuleT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSCMModuleT(CSCMModuleT const& init);
	void operator=(CSCMModuleT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_g3module_altova_CSCMModuleT); }
	MemberAttribute<string_type,_altova_mi_ns_g3module_altova_CSCMModuleT_altova_openColour, 0, 2> openColour;	// openColour CLED_COLOUR

	MemberAttribute<bool,_altova_mi_ns_g3module_altova_CSCMModuleT_altova_allowForcedOperation, 0, 0> allowForcedOperation;	// allowForcedOperation Cboolean
	MemberElement<ns_g3module::CSCMChannelsT, _altova_mi_ns_g3module_altova_CSCMModuleT_altova_channels> channels;
	struct channels { typedef Iterator<ns_g3module::CSCMChannelsT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_g3module

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CSCMModuleT
