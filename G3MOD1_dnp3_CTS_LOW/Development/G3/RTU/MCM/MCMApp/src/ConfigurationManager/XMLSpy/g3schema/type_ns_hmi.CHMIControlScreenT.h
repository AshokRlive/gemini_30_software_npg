#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CHMIControlScreenT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CHMIControlScreenT



namespace g3schema
{

namespace ns_hmi
{	

class CHMIControlScreenT : public TypeBase
{
public:
	g3schema_EXPORT CHMIControlScreenT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CHMIControlScreenT(CHMIControlScreenT const& init);
	void operator=(CHMIControlScreenT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_hmi_altova_CHMIControlScreenT); }

	MemberAttribute<int,_altova_mi_ns_hmi_altova_CHMIControlScreenT_altova_screenID, 0, 0> screenID;	// screenID Cint

	MemberAttribute<string_type,_altova_mi_ns_hmi_altova_CHMIControlScreenT_altova_screenTitle, 0, 0> screenTitle;	// screenTitle Cstring

	MemberAttribute<unsigned,_altova_mi_ns_hmi_altova_CHMIControlScreenT_altova_controlLogicID, 0, 0> controlLogicID;	// controlLogicID CVirtualPointGroupT
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_hmi

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CHMIControlScreenT
