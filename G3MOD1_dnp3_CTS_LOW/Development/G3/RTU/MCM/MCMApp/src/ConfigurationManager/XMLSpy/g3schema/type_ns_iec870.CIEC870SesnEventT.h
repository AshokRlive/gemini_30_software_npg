#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_iec870_ALTOVA_CIEC870SesnEventT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_iec870_ALTOVA_CIEC870SesnEventT

#include "type_ns_pstack.CSesnEventConfT.h"


namespace g3schema
{

namespace ns_iec870
{	

class CIEC870SesnEventT : public ::g3schema::ns_pstack::CSesnEventConfT
{
public:
	g3schema_EXPORT CIEC870SesnEventT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CIEC870SesnEventT(CIEC870SesnEventT const& init);
	void operator=(CIEC870SesnEventT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_iec870_altova_CIEC870SesnEventT); }
	MemberAttribute<string_type,_altova_mi_ns_iec870_altova_CIEC870SesnEventT_altova_timeFormat, 0, 3> timeFormat;	// timeFormat CLU_TIME_FORMAT
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_iec870

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_iec870_ALTOVA_CIEC870SesnEventT
