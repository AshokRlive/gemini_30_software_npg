#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_iec870_ALTOVA_CIEC870OutputPointT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_iec870_ALTOVA_CIEC870OutputPointT

#include "type_ns_pstack.CPointT.h"


namespace g3schema
{

namespace ns_iec870
{	

class CIEC870OutputPointT : public ::g3schema::ns_pstack::CPointT
{
public:
	g3schema_EXPORT CIEC870OutputPointT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CIEC870OutputPointT(CIEC870OutputPointT const& init);
	void operator=(CIEC870OutputPointT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_iec870_altova_CIEC870OutputPointT); }

	MemberAttribute<unsigned,_altova_mi_ns_iec870_altova_CIEC870OutputPointT_altova_controlLogicGroup, 0, 0> controlLogicGroup;	// controlLogicGroup CVirtualPointGroupT

	MemberAttribute<bool,_altova_mi_ns_iec870_altova_CIEC870OutputPointT_altova_selectRequired, 0, 0> selectRequired;	// selectRequired Cboolean
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_iec870

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_iec870_ALTOVA_CIEC870OutputPointT
