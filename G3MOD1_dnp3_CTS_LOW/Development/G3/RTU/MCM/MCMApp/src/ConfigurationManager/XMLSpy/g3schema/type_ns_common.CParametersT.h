#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_CParametersT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_CParametersT



namespace g3schema
{

namespace ns_common
{	

class CParametersT : public TypeBase
{
public:
	g3schema_EXPORT CParametersT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CParametersT(CParametersT const& init);
	void operator=(CParametersT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_common_altova_CParametersT); }
	MemberElement<ns_common::CParameterT, _altova_mi_ns_common_altova_CParametersT_altova_parameter> parameter;
	struct parameter { typedef Iterator<ns_common::CParameterT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_common

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_CParametersT
