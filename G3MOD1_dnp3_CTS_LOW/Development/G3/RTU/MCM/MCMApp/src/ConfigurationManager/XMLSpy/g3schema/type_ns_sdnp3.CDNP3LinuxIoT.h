#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CDNP3LinuxIoT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CDNP3LinuxIoT



namespace g3schema
{

namespace ns_sdnp3
{	

class CDNP3LinuxIoT : public TypeBase
{
public:
	g3schema_EXPORT CDNP3LinuxIoT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CDNP3LinuxIoT(CDNP3LinuxIoT const& init);
	void operator=(CDNP3LinuxIoT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_sdnp3_altova_CDNP3LinuxIoT); }
	MemberElement<ns_sdnp3::CSDNP3ChnlTCPConfT, _altova_mi_ns_sdnp3_altova_CDNP3LinuxIoT_altova_tcp> tcp;
	struct tcp { typedef Iterator<ns_sdnp3::CSDNP3ChnlTCPConfT> iterator; };
	MemberElement<ns_sdnp3::CSDNP3CommsDeviceConfT, _altova_mi_ns_sdnp3_altova_CDNP3LinuxIoT_altova_commsDevice> commsDevice;
	struct commsDevice { typedef Iterator<ns_sdnp3::CSDNP3CommsDeviceConfT> iterator; };
	MemberElement<ns_pstack::CSerialConfT, _altova_mi_ns_sdnp3_altova_CDNP3LinuxIoT_altova_serial> serial;
	struct serial { typedef Iterator<ns_pstack::CSerialConfT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_sdnp3

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CDNP3LinuxIoT
