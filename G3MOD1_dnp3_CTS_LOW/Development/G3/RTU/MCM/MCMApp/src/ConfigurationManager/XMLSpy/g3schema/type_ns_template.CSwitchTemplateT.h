#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CSwitchTemplateT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CSwitchTemplateT

#include "type_ns_clogic.CSwitchgearParamT.h"


namespace g3schema
{

namespace ns_template
{	

class CSwitchTemplateT : public ::g3schema::ns_clogic::CSwitchgearParamT
{
public:
	g3schema_EXPORT CSwitchTemplateT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSwitchTemplateT(CSwitchTemplateT const& init);
	void operator=(CSwitchTemplateT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_template_altova_CSwitchTemplateT); }

	MemberAttribute<string_type,_altova_mi_ns_template_altova_CSwitchTemplateT_altova_switchName, 0, 0> switchName;	// switchName Cstring

	MemberAttribute<bool,_altova_mi_ns_template_altova_CSwitchTemplateT_altova_allowForcedOperation, 0, 0> allowForcedOperation;	// allowForcedOperation Cboolean

	MemberAttribute<string_type,_altova_mi_ns_template_altova_CSwitchTemplateT_altova_uuid, 0, 0> uuid;	// uuid Cstring
	MemberElement<ns_template::CSwitchChannelTemplateT, _altova_mi_ns_template_altova_CSwitchTemplateT_altova_open> open;
	struct open { typedef Iterator<ns_template::CSwitchChannelTemplateT> iterator; };
	MemberElement<ns_template::CSwitchChannelTemplateT, _altova_mi_ns_template_altova_CSwitchTemplateT_altova_close> close;
	struct close { typedef Iterator<ns_template::CSwitchChannelTemplateT> iterator; };
	MemberElement<ns_template::CSwitchTemplateParamT, _altova_mi_ns_template_altova_CSwitchTemplateT_altova_parameters> parameters;
	struct parameters { typedef Iterator<ns_template::CSwitchTemplateParamT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_template

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CSwitchTemplateT
