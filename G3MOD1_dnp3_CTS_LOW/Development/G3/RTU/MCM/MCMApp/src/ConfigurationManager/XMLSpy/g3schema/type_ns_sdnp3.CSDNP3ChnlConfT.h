#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3ChnlConfT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3ChnlConfT

#include "type_ns_pstack.CChannelConfT.h"


namespace g3schema
{

namespace ns_sdnp3
{	

class CSDNP3ChnlConfT : public ::g3schema::ns_pstack::CChannelConfT
{
public:
	g3schema_EXPORT CSDNP3ChnlConfT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSDNP3ChnlConfT(CSDNP3ChnlConfT const& init);
	void operator=(CSDNP3ChnlConfT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_sdnp3_altova_CSDNP3ChnlConfT); }
	MemberAttribute<string_type,_altova_mi_ns_sdnp3_altova_CSDNP3ChnlConfT_altova_networkType, 0, 5> networkType;	// networkType CDNPLINK_NETWORK_TYPE

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CSDNP3ChnlConfT_altova_rxFrameSize, 0, 0> rxFrameSize;	// rxFrameSize CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CSDNP3ChnlConfT_altova_txFrameSize, 0, 0> txFrameSize;	// txFrameSize CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CSDNP3ChnlConfT_altova_rxFrameTimeout, 0, 0> rxFrameTimeout;	// rxFrameTimeout CunsignedInt
	MemberAttribute<string_type,_altova_mi_ns_sdnp3_altova_CSDNP3ChnlConfT_altova_confirmMode, 0, 3> confirmMode;	// confirmMode CLU_LINKCNFM

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CSDNP3ChnlConfT_altova_confirmTimeout, 0, 0> confirmTimeout;	// confirmTimeout CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CSDNP3ChnlConfT_altova_maxRetries, 0, 0> maxRetries;	// maxRetries CunsignedByte

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CSDNP3ChnlConfT_altova_rxFragmentSize, 0, 0> rxFragmentSize;	// rxFragmentSize CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CSDNP3ChnlConfT_altova_txFragmentSize, 0, 0> txFragmentSize;	// txFragmentSize CunsignedInt

	MemberAttribute<bool,_altova_mi_ns_sdnp3_altova_CSDNP3ChnlConfT_altova_broadcast, 0, 0> broadcast;	// broadcast Cboolean

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CSDNP3ChnlConfT_altova_offlinePollPeriodMs, 0, 0> offlinePollPeriodMs;	// offlinePollPeriodMs CunsignedInt
	MemberElement<ns_sdnp3::CDNP3LinuxIoT, _altova_mi_ns_sdnp3_altova_CSDNP3ChnlConfT_altova_linuxIo> linuxIo;
	struct linuxIo { typedef Iterator<ns_sdnp3::CDNP3LinuxIoT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_sdnp3

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3ChnlConfT
