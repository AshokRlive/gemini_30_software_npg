#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CCLogicParametersT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CCLogicParametersT



namespace g3schema
{

namespace ns_clogic
{	

class CCLogicParametersT : public TypeBase
{
public:
	g3schema_EXPORT CCLogicParametersT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CCLogicParametersT(CCLogicParametersT const& init);
	void operator=(CCLogicParametersT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_clogic_altova_CCLogicParametersT); }
	MemberElement<ns_clogic::CLogicGateParamT, _altova_mi_ns_clogic_altova_CCLogicParametersT_altova_logicGateParam> logicGateParam;
	struct logicGateParam { typedef Iterator<ns_clogic::CLogicGateParamT> iterator; };
	MemberElement<ns_clogic::CSwitchgearParamT, _altova_mi_ns_clogic_altova_CCLogicParametersT_altova_switchgearParam> switchgearParam;
	struct switchgearParam { typedef Iterator<ns_clogic::CSwitchgearParamT> iterator; };
	MemberElement<ns_clogic::CFanTestParamT, _altova_mi_ns_clogic_altova_CCLogicParametersT_altova_fanTestParam> fanTestParam;
	struct fanTestParam { typedef Iterator<ns_clogic::CFanTestParamT> iterator; };
	MemberElement<ns_clogic::CActionParamT, _altova_mi_ns_clogic_altova_CCLogicParametersT_altova_actionParam> actionParam;
	struct actionParam { typedef Iterator<ns_clogic::CActionParamT> iterator; };
	MemberElement<ns_clogic::CDummySwitchParamT, _altova_mi_ns_clogic_altova_CCLogicParametersT_altova_dummySwitchParam> dummySwitchParam;
	struct dummySwitchParam { typedef Iterator<ns_clogic::CDummySwitchParamT> iterator; };
	MemberElement<ns_clogic::CBatteryTestParamT, _altova_mi_ns_clogic_altova_CCLogicParametersT_altova_batteryTestParam> batteryTestParam;
	struct batteryTestParam { typedef Iterator<ns_clogic::CBatteryTestParamT> iterator; };
	MemberElement<ns_clogic::CSectionaliserParamT, _altova_mi_ns_clogic_altova_CCLogicParametersT_altova_sectionaliserParam> sectionaliserParam;
	struct sectionaliserParam { typedef Iterator<ns_clogic::CSectionaliserParamT> iterator; };
	MemberElement<ns_clogic::CChangeOverParamT, _altova_mi_ns_clogic_altova_CCLogicParametersT_altova_changeOverParam> changeOverParam;
	struct changeOverParam { typedef Iterator<ns_clogic::CChangeOverParamT> iterator; };
	MemberElement<ns_clogic::CPLTUParamT, _altova_mi_ns_clogic_altova_CCLogicParametersT_altova_pltuParam> pltuParam;
	struct pltuParam { typedef Iterator<ns_clogic::CPLTUParamT> iterator; };
	MemberElement<ns_clogic::CMinMaxAvgParamT, _altova_mi_ns_clogic_altova_CCLogicParametersT_altova_minMaxAvgParam> minMaxAvgParam;
	struct minMaxAvgParam { typedef Iterator<ns_clogic::CMinMaxAvgParamT> iterator; };
	MemberElement<ns_clogic::CFPIResetParamT, _altova_mi_ns_clogic_altova_CCLogicParametersT_altova_fpiResetParam> fpiResetParam;
	struct fpiResetParam { typedef Iterator<ns_clogic::CFPIResetParamT> iterator; };
	MemberElement<ns_clogic::CDigitalControlParamT, _altova_mi_ns_clogic_altova_CCLogicParametersT_altova_digCtrlParam> digCtrlParam;
	struct digCtrlParam { typedef Iterator<ns_clogic::CDigitalControlParamT> iterator; };
	MemberElement<ns_clogic::CThresholdParamT, _altova_mi_ns_clogic_altova_CCLogicParametersT_altova_thresholdParam> thresholdParam;
	struct thresholdParam { typedef Iterator<ns_clogic::CThresholdParamT> iterator; };
	MemberElement<ns_clogic::CAutomationT, _altova_mi_ns_clogic_altova_CCLogicParametersT_altova_automation> automation;
	struct automation { typedef Iterator<ns_clogic::CAutomationT> iterator; };
	MemberElement<ns_clogic::CCounterCommandParamT, _altova_mi_ns_clogic_altova_CCLogicParametersT_altova_counterCommand> counterCommand;
	struct counterCommand { typedef Iterator<ns_clogic::CCounterCommandParamT> iterator; };
	MemberElement<ns_clogic::CPseudoClockT, _altova_mi_ns_clogic_altova_CCLogicParametersT_altova_pseudoClock> pseudoClock;
	struct pseudoClock { typedef Iterator<ns_clogic::CPseudoClockT> iterator; };
	MemberElement<ns_clogic::CCLogicParametersBaseT, _altova_mi_ns_clogic_altova_CCLogicParametersT_altova_generic> generic;
	struct generic { typedef Iterator<ns_clogic::CCLogicParametersBaseT> iterator; };
	MemberElement<ns_tcl::CTCLSensingT, _altova_mi_ns_clogic_altova_CCLogicParametersT_altova_tclSensing> tclSensing;
	struct tclSensing { typedef Iterator<ns_tcl::CTCLSensingT> iterator; };
	MemberElement<ns_tcl::CTCLTrippingT, _altova_mi_ns_clogic_altova_CCLogicParametersT_altova_tclTripping> tclTripping;
	struct tclTripping { typedef Iterator<ns_tcl::CTCLTrippingT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_clogic

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CCLogicParametersT
