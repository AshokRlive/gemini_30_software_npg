#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_tcl_ALTOVA_CTimeSlotT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_tcl_ALTOVA_CTimeSlotT



namespace g3schema
{

namespace ns_tcl
{	

class CTimeSlotT : public TypeBase
{
public:
	g3schema_EXPORT CTimeSlotT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CTimeSlotT(CTimeSlotT const& init);
	void operator=(CTimeSlotT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_tcl_altova_CTimeSlotT); }

	MemberAttribute<bool,_altova_mi_ns_tcl_altova_CTimeSlotT_altova_enabled, 0, 0> enabled;	// enabled Cboolean

	MemberAttribute<string_type,_altova_mi_ns_tcl_altova_CTimeSlotT_altova_startTime, 0, 0> startTime;	// startTime Cstring

	MemberAttribute<string_type,_altova_mi_ns_tcl_altova_CTimeSlotT_altova_endTime, 0, 0> endTime;	// endTime Cstring

	MemberAttribute<double,_altova_mi_ns_tcl_altova_CTimeSlotT_altova_powerValue, 0, 0> powerValue;	// powerValue Cdouble

	MemberAttribute<unsigned,_altova_mi_ns_tcl_altova_CTimeSlotT_altova_powerDirection, 0, 0> powerDirection;	// powerDirection CunsignedByte
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_tcl

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_tcl_ALTOVA_CTimeSlotT
