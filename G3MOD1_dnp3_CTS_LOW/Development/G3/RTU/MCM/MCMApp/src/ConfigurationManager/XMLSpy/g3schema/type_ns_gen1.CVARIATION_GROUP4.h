#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen1_ALTOVA_CVARIATION_GROUP4
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen1_ALTOVA_CVARIATION_GROUP4



namespace g3schema
{

namespace ns_gen1
{	

class CVARIATION_GROUP4 : public TypeBase
{
public:
	g3schema_EXPORT CVARIATION_GROUP4(xercesc::DOMNode* const& init);
	g3schema_EXPORT CVARIATION_GROUP4(CVARIATION_GROUP4 const& init);
	void operator=(CVARIATION_GROUP4 const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen1_altova_CVARIATION_GROUP4); }

	enum EnumValues {
		Invalid = -1,
		k_WITHOUT_TIME = 0, // WITHOUT_TIME
		k_WITH_ABSOLUTE_TIME = 1, // WITH_ABSOLUTE_TIME
		k_WITHOUT_RELATIVE_TIME = 2, // WITHOUT_RELATIVE_TIME
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen1

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen1_ALTOVA_CVARIATION_GROUP4
