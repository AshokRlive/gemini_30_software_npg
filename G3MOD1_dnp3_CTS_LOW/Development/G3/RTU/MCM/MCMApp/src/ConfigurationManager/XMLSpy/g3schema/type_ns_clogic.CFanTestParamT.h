#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CFanTestParamT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CFanTestParamT

#include "type_ns_clogic.CCLogicParametersBaseT.h"


namespace g3schema
{

namespace ns_clogic
{	

class CFanTestParamT : public ::g3schema::ns_clogic::CCLogicParametersBaseT
{
public:
	g3schema_EXPORT CFanTestParamT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CFanTestParamT(CFanTestParamT const& init);
	void operator=(CFanTestParamT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_clogic_altova_CFanTestParamT); }

	MemberAttribute<unsigned,_altova_mi_ns_clogic_altova_CFanTestParamT_altova_duration, 0, 0> duration;	// duration CunsignedInt
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_clogic

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CFanTestParamT
