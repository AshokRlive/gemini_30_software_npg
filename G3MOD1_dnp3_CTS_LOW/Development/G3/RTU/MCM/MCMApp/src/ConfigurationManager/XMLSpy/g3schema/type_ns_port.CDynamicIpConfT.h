#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_port_ALTOVA_CDynamicIpConfT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_port_ALTOVA_CDynamicIpConfT



namespace g3schema
{

namespace ns_port
{	

class CDynamicIpConfT : public TypeBase
{
public:
	g3schema_EXPORT CDynamicIpConfT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CDynamicIpConfT(CDynamicIpConfT const& init);
	void operator=(CDynamicIpConfT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_port_altova_CDynamicIpConfT); }
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_port

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_port_ALTOVA_CDynamicIpConfT
