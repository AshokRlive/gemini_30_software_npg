#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CHMIPhaseScreenT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CHMIPhaseScreenT



namespace g3schema
{

namespace ns_hmi
{	

class CHMIPhaseScreenT : public TypeBase
{
public:
	g3schema_EXPORT CHMIPhaseScreenT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CHMIPhaseScreenT(CHMIPhaseScreenT const& init);
	void operator=(CHMIPhaseScreenT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_hmi_altova_CHMIPhaseScreenT); }

	MemberAttribute<int,_altova_mi_ns_hmi_altova_CHMIPhaseScreenT_altova_screenID, 0, 0> screenID;	// screenID Cint

	MemberAttribute<string_type,_altova_mi_ns_hmi_altova_CHMIPhaseScreenT_altova_screenTitle, 0, 0> screenTitle;	// screenTitle Cstring

	MemberAttribute<string_type,_altova_mi_ns_hmi_altova_CHMIPhaseScreenT_altova_currentFooter, 0, 0> currentFooter;	// currentFooter Cstring

	MemberAttribute<string_type,_altova_mi_ns_hmi_altova_CHMIPhaseScreenT_altova_voltageFooter, 0, 0> voltageFooter;	// voltageFooter Cstring
	MemberElement<ns_common::CVirtualPointRefT, _altova_mi_ns_hmi_altova_CHMIPhaseScreenT_altova_virtualPointEntry> virtualPointEntry;
	struct virtualPointEntry { typedef Iterator<ns_common::CVirtualPointRefT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_hmi

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CHMIPhaseScreenT
