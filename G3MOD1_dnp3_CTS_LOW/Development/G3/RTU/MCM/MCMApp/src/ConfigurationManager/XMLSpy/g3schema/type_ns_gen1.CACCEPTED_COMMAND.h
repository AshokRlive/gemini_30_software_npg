#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen1_ALTOVA_CACCEPTED_COMMAND
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen1_ALTOVA_CACCEPTED_COMMAND



namespace g3schema
{

namespace ns_gen1
{	

class CACCEPTED_COMMAND : public TypeBase
{
public:
	g3schema_EXPORT CACCEPTED_COMMAND(xercesc::DOMNode* const& init);
	g3schema_EXPORT CACCEPTED_COMMAND(CACCEPTED_COMMAND const& init);
	void operator=(CACCEPTED_COMMAND const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen1_altova_CACCEPTED_COMMAND); }

	enum EnumValues {
		Invalid = -1,
		k_ACCEPTED_COMMAND_SBO = 0, // ACCEPTED_COMMAND_SBO
		k_ACCEPTED_COMMAND_DO = 1, // ACCEPTED_COMMAND_DO
		k_ACCEPTED_COMMAND_ANY = 2, // ACCEPTED_COMMAND_ANY
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen1

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen1_ALTOVA_CACCEPTED_COMMAND
