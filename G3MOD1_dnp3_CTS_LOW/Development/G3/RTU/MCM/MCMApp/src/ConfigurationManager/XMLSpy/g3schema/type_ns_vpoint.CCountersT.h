#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CCountersT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CCountersT



namespace g3schema
{

namespace ns_vpoint
{	

class CCountersT : public TypeBase
{
public:
	g3schema_EXPORT CCountersT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CCountersT(CCountersT const& init);
	void operator=(CCountersT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CCountersT); }
	MemberElement<ns_vpoint::CCounterPointT, _altova_mi_ns_vpoint_altova_CCountersT_altova_counter> counter;
	struct counter { typedef Iterator<ns_vpoint::CCounterPointT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CCountersT
