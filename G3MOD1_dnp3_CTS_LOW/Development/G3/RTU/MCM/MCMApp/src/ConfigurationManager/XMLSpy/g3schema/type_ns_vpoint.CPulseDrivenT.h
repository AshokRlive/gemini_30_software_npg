#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CPulseDrivenT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CPulseDrivenT



namespace g3schema
{

namespace ns_vpoint
{	

class CPulseDrivenT : public TypeBase
{
public:
	g3schema_EXPORT CPulseDrivenT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CPulseDrivenT(CPulseDrivenT const& init);
	void operator=(CPulseDrivenT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CPulseDrivenT); }

	MemberAttribute<unsigned,_altova_mi_ns_vpoint_altova_CPulseDrivenT_altova_pulseWidth, 0, 0> pulseWidth;	// pulseWidth CunsignedInt
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CPulseDrivenT
