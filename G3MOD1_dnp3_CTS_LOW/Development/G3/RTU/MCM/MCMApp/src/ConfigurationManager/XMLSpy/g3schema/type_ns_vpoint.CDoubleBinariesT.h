#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CDoubleBinariesT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CDoubleBinariesT



namespace g3schema
{

namespace ns_vpoint
{	

class CDoubleBinariesT : public TypeBase
{
public:
	g3schema_EXPORT CDoubleBinariesT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CDoubleBinariesT(CDoubleBinariesT const& init);
	void operator=(CDoubleBinariesT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CDoubleBinariesT); }
	MemberElement<ns_vpoint::CDoubleBinaryPointT, _altova_mi_ns_vpoint_altova_CDoubleBinariesT_altova_doubleBinary> doubleBinary;
	struct doubleBinary { typedef Iterator<ns_vpoint::CDoubleBinaryPointT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CDoubleBinariesT
