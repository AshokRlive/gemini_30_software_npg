#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s101_ALTOVA_CS101LinuxIoT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s101_ALTOVA_CS101LinuxIoT



namespace g3schema
{

namespace ns_s101
{	

class CS101LinuxIoT : public TypeBase
{
public:
	g3schema_EXPORT CS101LinuxIoT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CS101LinuxIoT(CS101LinuxIoT const& init);
	void operator=(CS101LinuxIoT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_s101_altova_CS101LinuxIoT); }

	MemberAttribute<unsigned,_altova_mi_ns_s101_altova_CS101LinuxIoT_altova_firstCharWait, 0, 0> firstCharWait;	// firstCharWait CunsignedShort
	MemberElement<ns_pstack::CSerialConfT, _altova_mi_ns_s101_altova_CS101LinuxIoT_altova_serial> serial;
	struct serial { typedef Iterator<ns_pstack::CSerialConfT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_s101

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s101_ALTOVA_CS101LinuxIoT
