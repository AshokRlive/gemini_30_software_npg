#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen11_ALTOVA_CSERIAL_PORT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen11_ALTOVA_CSERIAL_PORT



namespace g3schema
{

namespace ns_gen11
{	

class CSERIAL_PORT : public TypeBase
{
public:
	g3schema_EXPORT CSERIAL_PORT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSERIAL_PORT(CSERIAL_PORT const& init);
	void operator=(CSERIAL_PORT const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen11_altova_CSERIAL_PORT); }

	enum EnumValues {
		Invalid = -1,
		k_SERIAL_PORT_CONTROL = 0, // SERIAL_PORT_CONTROL
		k_SERIAL_PORT_CONFIG_ = 1, // SERIAL_PORT_CONFIG 
		k_SERIAL_PORT_RS485__ = 2, // SERIAL_PORT_RS485  
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen11

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen11_ALTOVA_CSERIAL_PORT
