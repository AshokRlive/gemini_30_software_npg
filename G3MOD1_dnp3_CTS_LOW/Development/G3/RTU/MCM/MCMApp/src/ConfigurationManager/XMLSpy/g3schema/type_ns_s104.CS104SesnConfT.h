#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s104_ALTOVA_CS104SesnConfT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s104_ALTOVA_CS104SesnConfT

#include "type_ns_iec870.CIEC870SesnConfT.h"


namespace g3schema
{

namespace ns_s104
{	

class CS104SesnConfT : public ::g3schema::ns_iec870::CIEC870SesnConfT
{
public:
	g3schema_EXPORT CS104SesnConfT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CS104SesnConfT(CS104SesnConfT const& init);
	void operator=(CS104SesnConfT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_s104_altova_CS104SesnConfT); }

	MemberAttribute<unsigned,_altova_mi_ns_s104_altova_CS104SesnConfT_altova_maxCommandFuture, 0, 0> maxCommandFuture;	// maxCommandFuture CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_s104_altova_CS104SesnConfT_altova_maxCommandAge, 0, 0> maxCommandAge;	// maxCommandAge CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_s104_altova_CS104SesnConfT_altova_maxAPDUSize, 0, 0> maxAPDUSize;	// maxAPDUSize CunsignedByte

	MemberAttribute<int,_altova_mi_ns_s104_altova_CS104SesnConfT_altova_infoObjAddrSize, 0, 0> infoObjAddrSize;	// infoObjAddrSize Cshort
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_s104

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s104_ALTOVA_CS104SesnConfT
