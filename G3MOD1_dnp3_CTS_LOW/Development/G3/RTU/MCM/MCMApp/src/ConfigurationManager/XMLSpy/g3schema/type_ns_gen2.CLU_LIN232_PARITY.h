#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen2_ALTOVA_CLU_LIN232_PARITY
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen2_ALTOVA_CLU_LIN232_PARITY



namespace g3schema
{

namespace ns_gen2
{	

class CLU_LIN232_PARITY : public TypeBase
{
public:
	g3schema_EXPORT CLU_LIN232_PARITY(xercesc::DOMNode* const& init);
	g3schema_EXPORT CLU_LIN232_PARITY(CLU_LIN232_PARITY const& init);
	void operator=(CLU_LIN232_PARITY const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen2_altova_CLU_LIN232_PARITY); }

	enum EnumValues {
		Invalid = -1,
		k_LU_LIN232_PARITY_NONE = 0, // LU_LIN232_PARITY_NONE
		k_LU_LIN232_PARITY_EVEN = 1, // LU_LIN232_PARITY_EVEN
		k_LU_LIN232_PARITY_ODD = 2, // LU_LIN232_PARITY_ODD
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen2

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen2_ALTOVA_CLU_LIN232_PARITY
