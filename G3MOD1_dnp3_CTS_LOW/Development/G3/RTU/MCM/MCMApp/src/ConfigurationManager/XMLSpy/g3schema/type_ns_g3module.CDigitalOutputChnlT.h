#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CDigitalOutputChnlT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CDigitalOutputChnlT

#include "type_ns_g3module.CBaseChannelT.h"


namespace g3schema
{

namespace ns_g3module
{	

class CDigitalOutputChnlT : public ::g3schema::ns_g3module::CBaseChannelT
{
public:
	g3schema_EXPORT CDigitalOutputChnlT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CDigitalOutputChnlT(CDigitalOutputChnlT const& init);
	void operator=(CDigitalOutputChnlT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_g3module_altova_CDigitalOutputChnlT); }

	MemberAttribute<unsigned,_altova_mi_ns_g3module_altova_CDigitalOutputChnlT_altova_pulseLength, 0, 0> pulseLength;	// pulseLength CunsignedShort
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_g3module

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CDigitalOutputChnlT
