#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CScadaAddressT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CScadaAddressT



namespace g3schema
{

namespace ns_template
{	

class CScadaAddressT : public TypeBase
{
public:
	g3schema_EXPORT CScadaAddressT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CScadaAddressT(CScadaAddressT const& init);
	void operator=(CScadaAddressT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_template_altova_CScadaAddressT); }

	MemberAttribute<string_type,_altova_mi_ns_template_altova_CScadaAddressT_altova_ipaddress, 0, 0> ipaddress;	// ipaddress Cstring

	MemberAttribute<string_type,_altova_mi_ns_template_altova_CScadaAddressT_altova_masterAddress, 0, 0> masterAddress;	// masterAddress Cstring
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_template

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CScadaAddressT
