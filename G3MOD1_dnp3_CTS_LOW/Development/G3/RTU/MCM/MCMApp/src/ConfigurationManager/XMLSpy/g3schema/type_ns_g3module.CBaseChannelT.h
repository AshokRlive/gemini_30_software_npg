#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CBaseChannelT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CBaseChannelT



namespace g3schema
{

namespace ns_g3module
{	

class CBaseChannelT : public TypeBase
{
public:
	g3schema_EXPORT CBaseChannelT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CBaseChannelT(CBaseChannelT const& init);
	void operator=(CBaseChannelT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_g3module_altova_CBaseChannelT); }

	MemberAttribute<unsigned,_altova_mi_ns_g3module_altova_CBaseChannelT_altova_channelID, 0, 0> channelID;	// channelID CunsignedInt

	MemberAttribute<bool,_altova_mi_ns_g3module_altova_CBaseChannelT_altova_enable, 0, 0> enable;	// enable Cboolean
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_g3module

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CBaseChannelT
