/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:XML101SlaveProtocolParser.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief Slave IEC-101 Protocol Configuration Parser
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11 Jul 2016     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <vector>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "XML101SlaveProtocolParser.h"
#include "bitOperations.h"
#include "StringUtil.h"
#include "XMLParser_Points.h"
#include "IEC870Enum.h"
#include "ProtocolStackCommonEnum.h"
#include "S101Protocol.h"
#include "S101Debug.h"

using namespace ns_common;
using namespace ns_pstack;
using namespace ns_iec870;
using namespace ns_s101;
using namespace ns_vpoint;

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


#define S101_CHECK_POINT_FIELD(xmlProtocolID, xmlEnable, xmlVPoint)\
{\
    if(!xmlProtocolID.exists())\
    {\
        continue;   /* Skip points without protocol ID */\
    }\
    if(xmlEnable.exists())\
    {\
        if(xmlEnable == LU_FALSE)\
        {\
            continue;   /* Skip disabled points */\
        }\
    }\
    lu_uint32_t protocolID = xmlProtocolID;\
    if(!xmlVPoint.exists())\
    {\
        log.warn("S101_CHECK_POINT_FIELD: Protocol Point %i, doesn't have a linked virtual point. Skipping.",\
                  protocolID\
                  );\
        continue;   /* Skip points not linked with the Gemini DB */\
    }\
}\


#if !DEBUG_IEC_101_SLAVE
#define DBG_DUMP_CHANNEL101(a)    do {} while(0)
#define DBG_DUMP_SESSION101(a,b)    do {} while(0)
#define DBG_DUMP_SECTOR101()    do {} while(0)
#else
#define DBG_DUMP_CHANNEL101(channelConfig)          \
    do {                                            \
        DBG_INFO("%s Channel:\n"                    \
                    "incrementalTimeout=%lu\n"      \
                    "rxFrameSize       =%d\n"       \
                    "txFrameSize       =%d\n"       \
                    "offlinePollPeriod     =%lu\n"  \
                    "linkAddressSize       =%d\n"   \
                    "oneCharAckAllowed     =%d\n"   \
                    "oneCharResponseAllowed=%d\n"   \
                    "rxFrameTimeout        =%lu\n"  \
                    "confirmTimeout        =%lu\n"  \
                    "maxRetries  =%d\n"             \
                    "testFramePeriod=%lu\n"         \
                    "linkMode   =%s\n"              \
                    "confirmMode=%s\n"              \
                    "chnlName   =%s\n"              \
                    "portName   =%s\n"              \
                    "dataBits   =%s\n"              \
                    "stopBits   =%s\n"              \
                    "parity     =%s\n",             \
            TITLE,                                                   \
            channelConfig.pChnlConfig.incrementalTimeout        ,    \
            channelConfig.pLinkConfig.rxFrameSize               ,    \
            channelConfig.pLinkConfig.txFrameSize               ,    \
            channelConfig.pLinkConfig.offlinePollPeriod         ,    \
            channelConfig.pLinkConfig.linkAddressSize           ,    \
            channelConfig.pLinkConfig.oneCharAckAllowed         ,    \
            channelConfig.pLinkConfig.oneCharResponseAllowed    ,    \
            channelConfig.pLinkConfig.rxFrameTimeout            ,    \
            channelConfig.pLinkConfig.confirmTimeout            ,    \
            channelConfig.pLinkConfig.maxRetries                ,    \
            channelConfig.pLinkConfig.testFramePeriod           ,    \
            (channelConfig.pLinkConfig.linkMode==TMWDEFS_LINK_MODE_UNBALANCED)? "Unbalanced" :  \
                (channelConfig.pLinkConfig.linkMode==TMWDEFS_LINK_MODE_BALANCED)? "Balanced" :  \
                                                                                    "INVALID",  \
            (channelConfig.pLinkConfig.confirmMode==TMWDEFS_LINKCNFM_NEVER)? "Never" :          \
                (channelConfig.pLinkConfig.confirmMode==TMWDEFS_LINKCNFM_SOMETIMES)? "Sometimes" :  \
                    (channelConfig.pLinkConfig.confirmMode==TMWDEFS_LINKCNFM_ALWAYS)? "Always" :    \
                                                                                    "INVALID",      \
            channelConfig.pIOConfig.lin232.chnlName   ,    \
            channelConfig.pIOConfig.lin232.portName   ,    \
            (channelConfig.pIOConfig.lin232.numDataBits==LIN232_DATA_BITS_7)? "7 bits" :        \
                (channelConfig.pIOConfig.lin232.numDataBits==LIN232_DATA_BITS_8)? "8 bits" :    \
                                                                                "INVALID",      \
            (channelConfig.pIOConfig.lin232.numStopBits==LIN232_STOP_BITS_1)? "1 bit" :         \
                (channelConfig.pIOConfig.lin232.numStopBits==LIN232_STOP_BITS_2)? "2 bits" :    \
                                                                                "INVALID",      \
            (channelConfig.pIOConfig.lin232.parity==LIN232_PARITY_NONE)? "None" :           \
                (channelConfig.pIOConfig.lin232.parity==LIN232_PARITY_EVEN)? "Even" :       \
                    (channelConfig.pIOConfig.lin232.parity==LIN232_PARITY_ODD )? "Odd" :    \
                                                                            "INVALID"       \
            );                                                       \
    } while(0)


#define DBG_DUMP_SESSION101(maxFrameLength, s101sessionConfig) \
    do {                                                    \
        DBG_INFO("%s Session:\n"                            \
                    "Frame Length=%d\n"                     \
                    "Link Address=%d\n"                     \
                    "max ASDU address=%d\n"                 \
                    "ASDU size=%d\n"                        \
                    "IOA size=%d\n"                         \
                    "maxPollDelay=%lu\n"                    \
                    "useDayOfWeek=%d\n",                    \
                TITLE,                                      \
                maxFrameLength                   ,          \
                s101sessionConfig.linkAddress    ,          \
                s101sessionConfig.maxASDUSize    ,          \
                s101sessionConfig.asduAddrSize   ,          \
                s101sessionConfig.infoObjAddrSize,          \
                s101sessionConfig.maxPollDelay   ,          \
                s101sessionConfig.useDayOfWeek);            \
    } while(0)

#define DBG_MMX_TIME_FORMAT(timeFormat)                                     \
        (timeFormat==TMWDEFS_TIME_FORMAT_NONE)? "None" :                    \
            (timeFormat==TMWDEFS_TIME_FORMAT_24)? "24bit" :                 \
                (timeFormat==TMWDEFS_TIME_FORMAT_56)? "56bit" :             \
                    (timeFormat==TMWDEFS_TIME_FORMAT_UNKNOWN)? "UNK (Default)" : "INVALID"

#define DBG_MMX_EVENT_MODE(eventMode)                                       \
        (eventMode==TMWDEFS_EVENT_MODE_SOE)? "SOE" :                        \
            (eventMode==TMWDEFS_EVENT_MODE_MOST_RECENT)? "MostRecent" : "INVALID"

#define DBG_DUMP_SECTOR101()                                \
    do {                                                    \
        DBG_INFO("%s Sector:\n"                             \
                    "ASDUaddress=%d\n"                      \
                    "cyclicPeriod=%lu\n"                    \
                    "cyclicFirstPeriod=%lu\n"               \
                    "backgroundPeriod=%lu\n"                \
                    "selectTimeout=%lu\n"                   \
                    "defaultResponseTimeout=%lu\n"          \
                    "cseUseActTerm=%d\n"                    \
                    "cmdUseActTerm=%d\n"                    \
                    "readTimeFormat=%s\n"                   \
                    "readMsrndTimeFormat=%s\n"              \
                    "cicnaTimeFormat=%s\n"                  \
                    "sendClockSyncEvents=%d\n"              \
                    "deleteOldestEvent=%d\n"                \
                    "multiplePMEN=%d\n",                    \
                TITLE,                                      \
                sectorConfigStr.asduAddress,                \
                sectorConfigStr.cyclicPeriod,               \
                sectorConfigStr.cyclicFirstPeriod,          \
                sectorConfigStr.backgroundPeriod,           \
                sectorConfigStr.selectTimeout,              \
                sectorConfigStr.defaultResponseTimeout,     \
                sectorConfigStr.cseUseActTerm,              \
                sectorConfigStr.cmdUseActTerm,              \
                DBG_MMX_TIME_FORMAT(sectorConfigStr.readTimeFormat),         \
                DBG_MMX_TIME_FORMAT(sectorConfigStr.readMsrndTimeFormat),    \
                DBG_MMX_TIME_FORMAT(sectorConfigStr.cicnaTimeFormat),        \
                sectorConfigStr.sendClockSyncEvents,        \
                sectorConfigStr.deleteOldestEvent,          \
                multiplePMEN);                              \
        DBG_INFO("%s Sector Events:\n"                      \
                    "mspMaxEvents =%d\n"                    \
                    "mspEventMode =%s\n"                    \
                    "mspTimeFormat=%s\n"                    \
                    "mdpMaxEvents =%d\n"                    \
                    "mdpEventMode =%s\n"                    \
                    "mdpTimeFormat=%s\n"                    \
                    "mitMaxEvents =%d\n"                    \
                    "mitEventMode =%s\n"                    \
                    "mitTimeFormat=%s\n"                    \
                    "mmenaMaxEvents =%d\n"                  \
                    "mmenaEventMode =%s\n"                  \
                    "mmenaTimeFormat=%s\n"                  \
                    "mmenbMaxEvents =%d\n"                  \
                    "mmenbEventMode =%s\n"                  \
                    "mmenbTimeFormat=%s\n"                  \
                    "mmencMaxEvents =%d\n"                  \
                    "mmencEventMode =%s\n"                  \
                    "mmencTimeFormat=%s\n",                 \
                TITLE,                                      \
                sectorConfigStr.mspMaxEvents,               \
                DBG_MMX_EVENT_MODE(sectorConfigStr.mspEventMode),       \
                DBG_MMX_TIME_FORMAT(sectorConfigStr.mspTimeFormat),     \
                sectorConfigStr.mdpMaxEvents,                           \
                DBG_MMX_EVENT_MODE(sectorConfigStr.mdpEventMode),       \
                DBG_MMX_TIME_FORMAT(sectorConfigStr.mdpTimeFormat),     \
                sectorConfigStr.mitMaxEvents,                           \
                DBG_MMX_EVENT_MODE(sectorConfigStr.mitEventMode),       \
                DBG_MMX_TIME_FORMAT(sectorConfigStr.mitTimeFormat),     \
                sectorConfigStr.mmenaMaxEvents,                         \
                DBG_MMX_EVENT_MODE(sectorConfigStr.mmenaEventMode),     \
                DBG_MMX_TIME_FORMAT(sectorConfigStr.mmenaTimeFormat),   \
                sectorConfigStr.mmenbMaxEvents,                         \
                DBG_MMX_EVENT_MODE(sectorConfigStr.mmenbEventMode),     \
                DBG_MMX_TIME_FORMAT(sectorConfigStr.mmenbTimeFormat),   \
                sectorConfigStr.mmencMaxEvents,                         \
                DBG_MMX_EVENT_MODE(sectorConfigStr.mmencEventMode),     \
                DBG_MMX_TIME_FORMAT(sectorConfigStr.mmencTimeFormat)    \
                );                                                      \
    } while(0)
#endif


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

template <typename XMLSource, typename Dest>
void getSessionTimeFormat(XMLSource sourceXML, Dest& dest) throw(ParserException)
{
    dest = TMWDEFS_TIME_FORMAT_56;  //Default value as set by TMW
    CIEC870SesnEventT event = getXMLElement<CIEC870SesnEventT>(sourceXML);
    getXMLOptAttrEnum(sourceXML.first().timeFormat, LU_TIME_FORMAT_VALUEfromINDEX, dest);
}


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
XML101SlaveProtocolParser::XML101SlaveProtocolParser(Cg3schema&          configuration,
                                                       CommsDeviceManager& commsDeviceManager,
                                                       PortManager&        portManager,
                                                       ModuleManager&      moduleManager) :
                                                       configuration(configuration),
                                                       commsDeviceManager(commsDeviceManager),
                                                       portManager(portManager),
                                                       moduleManager(moduleManager),
                                                       log(Logger::getLogger(SUBSYSTEM_ID_CONFMGR))
{
    initParser();
}

XML101SlaveProtocolParser::~XML101SlaveProtocolParser()
{}

S101Protocol* XML101SlaveProtocolParser::newProtocolManager(GeminiDatabase& GDatabase)
{
    try
    {
        checkXMLElement(configuration.configuration);
        checkXMLElement(configuration.configuration.first().scadaProtocol);
        /* Get the 101 Slave Object (there should only be 0 or 1 of these) */
        checkXMLElement(configuration.configuration.first().scadaProtocol.first().sIEC101);
    }
    catch (const ParserException &e)
    {
        return NULL;    //No IEC101 Config present
    }
    S101Protocol* s101protocolManager = new S101Protocol(GDatabase);
    try
    {
        // Loop over channels, adding a new channelConfig element and initialising it
        CS101T s101 = getXMLElement<CS101T>(configuration.configuration.first().scadaProtocol.first().sIEC101);

        // Iterate through the configured channels
        for (Iterator<CS101ChannelT> itChannelConfig = s101.channel.all();
             itChannelConfig;
             ++itChannelConfig)
        {
            checkXMLElement(itChannelConfig.config);

            lu_bool_t is101chlEnabled = true;   //Flag which is set to false if
                                                // optional attribute(enabled) is present
            getXMLOptAttr(itChannelConfig.config.first().enabled, is101chlEnabled);
            if (is101chlEnabled == false)
            {
                std::string chnlName = itChannelConfig.config.first().chnlName;
                DBG_INFO("101Slave Channel %s is disabled",chnlName.c_str());
                continue;
            }
            S101Channel::S101ChannelConfig s101ChnlConf;

            parseChannelConfig(itChannelConfig, s101ChnlConf);

            DBG_DUMP_CHANNEL101(s101ChnlConf);

            S101Channel *channelPtr = new S101Channel(GDatabase, s101protocolManager);

            channelPtr->configure(s101ChnlConf);
            // Iterate through the configured sessions
            for (Iterator<CS101SessionT> itSessionConfig = itChannelConfig.session.all();
                 itSessionConfig;
                 ++itSessionConfig)
            {

                lu_bool_t is101sesEnabled = true ;// flag which is set to false if
                //optional attribute(enabled) is present

                getXMLOptAttr(itSessionConfig.config.first().enabled,is101sesEnabled);

                if (is101sesEnabled == false)
                {
                    std::string seshName = itSessionConfig.config.first().sessionName;
                    DBG_INFO("101Slave Session : %s is disabled", seshName.c_str());
                    continue;
                }

                S101SESN_CONFIG s101sessionConfig;
                S101SCTR_CONFIG s101sectorConfig;

                /* Parse the Session config */
                lu_uint32_t maxFrameLength;
                maxFrameLength = parseSessionConfig(itSessionConfig, s101sessionConfig);
                //Convert from Frame Length to ASDU size (ASDU = FL - 1 control field - Link Addr size)
                s101sessionConfig.maxASDUSize = maxFrameLength - 1 - s101ChnlConf.pLinkConfig.linkAddressSize;

                DBG_DUMP_SESSION101(maxFrameLength, s101sessionConfig);

                /* Parse the Sector config */
                parseSectorConfig(itSessionConfig, s101sectorConfig);


 //TODO - SKA - Get the sessionID from the XML
                S101Session *sessionPtr = new S101Session(*channelPtr);

                S101Sector *sectorPtr   = new S101Sector(*sessionPtr);

                /* Parse Analogue input points */
                if (itSessionConfig->iomap.first().mmea.exists())
                {
                    parseAnalogConfig(GDatabase, &(itSessionConfig),S101InputPoint::MMENA, sectorPtr);
                }
                if(itSessionConfig->iomap.first().mmeb.exists())
                {
                    parseAnalogConfig(GDatabase, &(itSessionConfig),S101InputPoint::MMENB, sectorPtr);
                }
                if(itSessionConfig->iomap.first().mmec.exists())
                {
                    parseAnalogConfig(GDatabase, &(itSessionConfig),S101InputPoint::MMENC, sectorPtr);
                }


                /* Parse Binary input points */
                parseBinaryConfig(GDatabase, &(itSessionConfig), sectorPtr);

                /* Parse Double Binary input points */
                parseDBinaryConfig(GDatabase, &(itSessionConfig), sectorPtr);

                /* Parse Binary output points */
                parseBinaryOutputConfig(&(itSessionConfig), sectorPtr);

                /* Parse Binary output points */
                parseDBinaryOutputConfig(&(itSessionConfig), sectorPtr);

                /*Parse Integrated Totals points*/
                parseIntegratedTotsConfig(GDatabase, &(itSessionConfig), sectorPtr);

                sectorPtr->configure(s101sectorConfig);

                // Add the sector to the vector of sectors
                sessionPtr->addSector(sectorPtr);

                sessionPtr->configure(s101sessionConfig);

                // Add the session to the vector of sessions
                channelPtr->addSession(sessionPtr);
            }
            // Add the channel to the vector of channels
            s101protocolManager->addChannel(channelPtr);
        }
    }
    catch (const ParserException &e)
    {
        DBG_ERR("%s",e.what());
        log.error("%s in %s",e.what(), __AT__);
    }
    catch (...)
    {
        log.error("An unknown error occured  at %s", __AT__);
    }

    return s101protocolManager;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
CONFMGR_ERROR XML101SlaveProtocolParser::initParser()
{
    try
    {
        checkXMLElement(configuration.configuration);
        CConfigurationT configurationRoot = getXMLElement<CConfigurationT>(configuration.configuration);
        CScadaProtocolT scadaProtocol =  getXMLElement<CScadaProtocolT>(configurationRoot.scadaProtocol);

        /* Check if 101 protocol is present */
        if(scadaProtocol.sIEC101.exists())
        {
            // Maximum of one instance of this 101 Slave Session
            if (scadaProtocol.sIEC101.count() <= 1)
            {
                return CONFMGR_ERROR_NONE;
            }
        }
        else
        {
            DBG_INFO("IEC-101 protocol is not added");
        }
    }
    catch (const ParserException &e)
    {
        DBG_ERR("%s",e.what());
        log.error("%s in %s",e.what(), __AT__);
        return CONFMGR_ERROR_NOT_INITIALIZED;
    }
    return CONFMGR_ERROR_INITPARSER;
}


void XML101SlaveProtocolParser::parseAnalogConfig( GeminiDatabase           &GDatabase,
                                                     CS101SessionT      *sessionParserPtr,
                                                     S101InputPoint::ProtPointType analogType,
                                                     S101Sector *sectorPtr)
{
    /* count for number of analog input points*/
    lu_uint8_t mmeaNos = 0;
    lu_uint8_t mmebNos = 0;
    lu_uint8_t mmecNos = 0;

    S101InputPoint::Config* mmeaPointConf = NULL;
    S101Analog** mmeaPoint = NULL;
    S101InputPoint::Config* mmebPointConf = NULL;
    S101Analog** mmebPoint = NULL;
    S101InputPoint::Config* mmecPointConf = NULL;
    S101Analog** mmecPoint = NULL;

    try
    {
        switch(analogType)
        {
            case S101InputPoint::MMENA :

                /*Count number of analog input points*/
                for(Iterator<CIEC870InputPointT> itAnalogConfig = sessionParserPtr->iomap.first().mmea.all();
                        itAnalogConfig; ++itAnalogConfig)
                {
                    /* Skip points without valid protocol ID, disabled or with no related G3 point */
                    S101_CHECK_POINT_FIELD(itAnalogConfig.protocolID, itAnalogConfig.enable, itAnalogConfig.vpoint);
                    mmeaNos++;   // Count no of analog input points
                }

                mmeaPointConf = new S101InputPoint::Config[mmeaNos];
                mmeaPoint = new S101Analog*[mmeaNos];

                for(Iterator<CIEC870InputPointT> itAnalogConfig = sessionParserPtr->iomap.first().mmea.all();
                        itAnalogConfig; ++itAnalogConfig)
                {
                	lu_uint8_t pointIndex = 0;

                    /* Skip points without valid protocol ID, disabled or with no related G3 point */
                    S101_CHECK_POINT_FIELD(itAnalogConfig.protocolID, itAnalogConfig.enable, itAnalogConfig.vpoint);

                    /* Get time format */
                    lu_uint32_t timeFormat = TMWDEFS_TIME_FORMAT_56; //default
                    getSessionTimeFormat(sessionParserPtr->config.first().eventConfig.first().mmea, timeFormat);
                    mmeaPointConf[pointIndex].pointAttributes.timeformat = (TMWDEFS_TIME_FORMAT)(timeFormat);

                    parseCommonAnalogAttributes(&itAnalogConfig ,pointIndex, mmeaPointConf);

                    mmeaPoint[pointIndex]= new S101Analog(S101InputPoint::MMENA,mmeaPointConf[pointIndex]);
                    sectorPtr->getS101Database().addInputPoint(S101InputPoint::MMENA,*(&mmeaPoint[pointIndex]));
                    GDatabase.attach(mmeaPointConf[pointIndex].vpointID, *(&mmeaPoint[pointIndex]));

                    pointIndex++;
                 }

            break;

            case S101InputPoint::MMENB :

                /*Count number of analog input points*/
                for(Iterator<CIEC870InputPointT> itAnalogConfig = sessionParserPtr->iomap.first().mmeb.all();
                    itAnalogConfig; ++itAnalogConfig)
                {
                    /* Skip points without valid protocol ID, disabled or with no related G3 point */
                    S101_CHECK_POINT_FIELD(itAnalogConfig.protocolID, itAnalogConfig.enable, itAnalogConfig.vpoint);
                    mmebNos++;   // Count no of analog input points
                }

                mmebPointConf = new S101InputPoint::Config[mmebNos];
                mmebPoint = new S101Analog*[mmebNos];

                for(Iterator<CIEC870InputPointT> itAnalogConfig = sessionParserPtr->iomap.first().mmeb.all();
                        itAnalogConfig; ++itAnalogConfig)
                {
                    lu_uint8_t pointIndex = 0;

                    /* Skip points without valid protocol ID, disabled or with no related G3 point */
                    S101_CHECK_POINT_FIELD(itAnalogConfig.protocolID, itAnalogConfig.enable, itAnalogConfig.vpoint);

                    /* Get time format */
                    lu_uint32_t timeFormat = TMWDEFS_TIME_FORMAT_56; //default
                    getSessionTimeFormat(sessionParserPtr->config.first().eventConfig.first().mmeb, timeFormat);
                    mmebPointConf[pointIndex].pointAttributes.timeformat = (TMWDEFS_TIME_FORMAT)(timeFormat);

                    parseCommonAnalogAttributes(&itAnalogConfig ,pointIndex, mmebPointConf);

                    mmebPoint[pointIndex]= new S101Analog(S101InputPoint::MMENB,mmebPointConf[pointIndex]);
                    sectorPtr->getS101Database().addInputPoint(S101InputPoint::MMENB,*(&mmebPoint[pointIndex]));
                    GDatabase.attach(mmebPointConf[pointIndex].vpointID, *(&mmebPoint[pointIndex]));

                    pointIndex++;
                 }

            break;

            case S101InputPoint::MMENC :

                /*Count number of analog input points*/
                for(Iterator<CIEC870InputPointT> itAnalogConfig = sessionParserPtr->iomap.first().mmec.all();
                        itAnalogConfig; ++itAnalogConfig)
                {
                    /* Skip points without valid protocol ID, disabled or with no related G3 point */
                    S101_CHECK_POINT_FIELD(itAnalogConfig.protocolID, itAnalogConfig.enable, itAnalogConfig.vpoint);
                    mmecNos++;   // Count no of analog input points
                }

                mmecPointConf = new S101InputPoint::Config[mmecNos];
                mmecPoint = new S101Analog*[mmecNos];

                for(Iterator<CIEC870InputPointT> itAnalogConfig = sessionParserPtr->iomap.first().mmec.all();
                        itAnalogConfig; ++itAnalogConfig)
                {
                    lu_uint8_t pointIndex = 0;

                    /* Skip points without valid protocol ID, disabled or with no related G3 point */
                    S101_CHECK_POINT_FIELD(itAnalogConfig.protocolID, itAnalogConfig.enable, itAnalogConfig.vpoint);

                    /* Get time format */
                    lu_uint32_t timeFormat = TMWDEFS_TIME_FORMAT_56; //default
                    getSessionTimeFormat(sessionParserPtr->config.first().eventConfig.first().mmec, timeFormat);
                    mmecPointConf[pointIndex].pointAttributes.timeformat = (TMWDEFS_TIME_FORMAT)(timeFormat);

                    parseCommonAnalogAttributes(&itAnalogConfig ,pointIndex, mmecPointConf);

                    mmecPoint[pointIndex]= new S101Analog(S101InputPoint::MMENC,mmecPointConf[pointIndex]);
                    sectorPtr->getS101Database().addInputPoint(S101InputPoint::MMENC,*(&mmecPoint[pointIndex]));
                    GDatabase.attach(mmecPointConf[pointIndex].vpointID, *(&mmecPoint[pointIndex]));

                    pointIndex++;
                 }
            break;

            default :
                DBG_ERR("Invalid Analog type at %s",__AT__);
        }
    }
    catch (const ParserException &e)
    {
        DBG_ERR("%s",e.what());
        log.error("%s in %s", e.what(), __AT__);
    }
    catch (...)
    {
        log.error("%s Exception raised", __AT__);
    }
}


void XML101SlaveProtocolParser::parseCommonAnalogAttributes(CIEC870InputPointT* analogPoint,
                                                            lu_uint8_t pIndex,
                                                            S101InputPoint::Config* pointConf
                                                            ) throw(ParserException)
{
    pointConf[pIndex].pointAttributes.ioa = getXMLAttr(analogPoint->protocolID);

    /*applying group masks*/
    pointConf[pIndex].pointAttributes.groupMask = getXMLAttr(analogPoint->groupMask);

    if(getXMLAttr(analogPoint->enableCyclic))
    {
        pointConf[pIndex].pointAttributes.groupMask |= TMWDEFS_GROUP_MASK_CYCLIC;
    }

    if(getXMLAttr(analogPoint->enableBackground))
    {
        pointConf[pIndex].pointAttributes.groupMask |= TMWDEFS_GROUP_MASK_BACKGROUND;
    }

    if(getXMLAttr(analogPoint->enableGeneralInterrogation))
    {
        pointConf[pIndex].pointAttributes.groupMask |= TMWDEFS_GROUP_MASK_GENERAL;
    }

    getXMLOptAttr(analogPoint->spontaneousEvent, pointConf[pIndex].pointAttributes.spontaneousEvent);

    pointConf[pIndex].pointAttributes.enable = getXMLAttr(analogPoint->enable);

    if(pointConf[pIndex].pointAttributes.enable)
    {
        getXMLOptAttrEnum(analogPoint->timeformat, LU_TIME_FORMAT_VALUEfromINDEX, pointConf[pIndex].pointAttributes.timeformat);
        pointConf[pIndex].vpointID = getXMLVirtualPointRef(analogPoint->vpoint.first());
    }

    pointConf[pIndex].pointAttributes.eventOnlyWhenOnline = getXMLAttr(analogPoint->eventOnlyWhenConnected);
    pointConf[pIndex].pointAttributes.eventLogStore = TMWDEFS_TRUE;
    getXMLOptAttr(analogPoint->enableStoreEvent, pointConf[pIndex].pointAttributes.eventLogStore);
}


void XML101SlaveProtocolParser::parseBinaryConfig(GeminiDatabase           &GDatabase,
                                                  CS101SessionT         *sessionParserPtr,
                                                  S101Sector *sectorPtr)
{
    try
    {
        /* Parse Binary input points */
        if (sessionParserPtr->iomap.first().msp.exists())
        {
            /* count for number of Binary input points*/
            static lu_uint8_t BIPointNum = 0;


            /*Count number of BI points*/
            for(Iterator<CIEC870InputPointT> itBIConfig = sessionParserPtr->iomap.first().msp.all();
                    itBIConfig; ++itBIConfig)
            {
                /* Skip points without valid protocol ID, disabled or with no related G3 point */
                S101_CHECK_POINT_FIELD(itBIConfig.protocolID, itBIConfig.enable, itBIConfig.vpoint);
                BIPointNum++;   // Count no of BI points
            }

            S101InputPoint::Config* pointconf = new S101InputPoint::Config[BIPointNum];

            S101MSP ** singlePoint = new S101MSP*[BIPointNum];

            for(Iterator<CIEC870InputPointT> itBIConfig = sessionParserPtr->iomap.first().msp.all();
                    itBIConfig; ++itBIConfig)
            {
                lu_uint8_t pointIndex = 0;
                /* Skip points without valid protocol ID, disabled or with no related G3 point */
                S101_CHECK_POINT_FIELD(itBIConfig.protocolID, itBIConfig.enable, itBIConfig.vpoint);

                pointconf[pointIndex].pointAttributes.ioa = getXMLAttr(itBIConfig.protocolID);
                /*applying group masks*/
                pointconf[pointIndex].pointAttributes.groupMask = getXMLAttr(itBIConfig.groupMask);

                if(getXMLAttr(itBIConfig.enableCyclic))
                {
                    pointconf[pointIndex].pointAttributes.groupMask |= TMWDEFS_GROUP_MASK_CYCLIC;
                }
                if(getXMLAttr(itBIConfig.enableBackground))
                {
                    pointconf[pointIndex].pointAttributes.groupMask |= TMWDEFS_GROUP_MASK_BACKGROUND;
                }
                if(getXMLAttr(itBIConfig.enableGeneralInterrogation))
                {
                    pointconf[pointIndex].pointAttributes.groupMask |= TMWDEFS_GROUP_MASK_GENERAL;
                }

                getXMLOptAttr(itBIConfig.spontaneousEvent, pointconf[pointIndex].pointAttributes.spontaneousEvent);
                pointconf[pointIndex].pointAttributes.eventOnlyWhenOnline = getXMLAttr(itBIConfig.eventOnlyWhenConnected);
                pointconf[pointIndex].pointAttributes.eventLogStore = TMWDEFS_TRUE;
                getXMLOptAttr(itBIConfig.enableStoreEvent, pointconf[pointIndex].pointAttributes.eventLogStore);

                /* Get time format */
                /* Note: we need to assign to a int instead to the enum since XMLSpy takes wrong numbers as enum */
                lu_uint32_t timeFormat = TMWDEFS_TIME_FORMAT_56; //default
                getSessionTimeFormat(sessionParserPtr->config.first().eventConfig.first().msp, timeFormat);
                getXMLOptAttrEnum(itBIConfig.timeformat, LU_TIME_FORMAT_VALUEfromINDEX, timeFormat);
                pointconf[pointIndex].pointAttributes.timeformat = (TMWDEFS_TIME_FORMAT)(timeFormat);

                pointconf[pointIndex].vpointID = getXMLVirtualPointRef(itBIConfig.vpoint.first());

                pointconf[pointIndex].pointAttributes.enable = getXMLAttr(itBIConfig.enable);

                if(pointconf[pointIndex].pointAttributes.enable)
                {
                    singlePoint[pointIndex]= new S101MSP(pointconf[pointIndex]);
                    sectorPtr->getS101Database().addInputPoint(S101InputPoint::MSP,*(&singlePoint[pointIndex]));
                    GDatabase.attach(pointconf[pointIndex].vpointID, *(&singlePoint[pointIndex]));
                }
                pointIndex++;
            }
        }
        else
        {
            log.info("%s No binary points configured", __AT__);
        }
    }
    catch (ParserException &e)
    {
        DBG_ERR("%s",e.what());
        log.error("%s in %s",e.what(), __AT__);
    }
    catch (...)
    {
        log.error("%s Exception raised", __AT__);
    }

}


void XML101SlaveProtocolParser::parseDBinaryConfig(GeminiDatabase           &GDatabase,
                                                  CS101SessionT         *sessionParserPtr,
                                                  S101Sector *sectorPtr)
{
    try
    {
        /* Parse double binary input points */
        if (sessionParserPtr->iomap.first().mdp.exists())
        {
            /* count for number of double binary input points*/
            static lu_uint8_t DBIPointNum = 0;


            /*Count number of BI points*/
            for(Iterator<CIEC870InputPointT> itDBIConfig = sessionParserPtr->iomap.first().mdp.all();
                    itDBIConfig; ++itDBIConfig)
            {
                /* Skip points without valid protocol ID, disabled or with no related G3 point */
                S101_CHECK_POINT_FIELD(itDBIConfig.protocolID, itDBIConfig.enable, itDBIConfig.vpoint);
                DBIPointNum++;   // Count no of DBI points
            }

            S101InputPoint::Config *pointconf = new S101InputPoint::Config[DBIPointNum];

            S101MDP ** doublePoint = new S101MDP*[DBIPointNum];

            for(Iterator<CIEC870InputPointT> itDBIConfig = sessionParserPtr->iomap.first().mdp.all();
                    itDBIConfig; ++itDBIConfig)
            {
                lu_uint8_t pointIndex = 0;
                /* Skip points without valid protocol ID, disabled or with no related G3 point */
                S101_CHECK_POINT_FIELD(itDBIConfig.protocolID, itDBIConfig.enable, itDBIConfig.vpoint);

                pointconf[pointIndex].pointAttributes.ioa = getXMLAttr(itDBIConfig.protocolID);

                /*applying group masks*/
                pointconf[pointIndex].pointAttributes.groupMask = getXMLAttr(itDBIConfig.groupMask);

                if(getXMLAttr (itDBIConfig.enableCyclic))
                {
                    pointconf[pointIndex].pointAttributes.groupMask |= TMWDEFS_GROUP_MASK_CYCLIC;
                }
                if(getXMLAttr (itDBIConfig.enableBackground))
                {
                    pointconf[pointIndex].pointAttributes.groupMask |= TMWDEFS_GROUP_MASK_BACKGROUND;
                }
                if(getXMLAttr (itDBIConfig.enableGeneralInterrogation))
                {
                    pointconf[pointIndex].pointAttributes.groupMask |= TMWDEFS_GROUP_MASK_GENERAL;
                }

                getXMLOptAttr(itDBIConfig.spontaneousEvent, pointconf[pointIndex].pointAttributes.spontaneousEvent);
                pointconf[pointIndex].pointAttributes.eventOnlyWhenOnline = getXMLAttr(itDBIConfig.eventOnlyWhenConnected);
                pointconf[pointIndex].pointAttributes.eventLogStore = TMWDEFS_TRUE;
                getXMLOptAttr(itDBIConfig.enableStoreEvent, pointconf[pointIndex].pointAttributes.eventLogStore);

                /* Get time format */
                lu_uint32_t timeFormat = TMWDEFS_TIME_FORMAT_56; //default
                getSessionTimeFormat(sessionParserPtr->config.first().eventConfig.first().mdp, timeFormat);
                getXMLOptAttrEnum(itDBIConfig.timeformat, LU_TIME_FORMAT_VALUEfromINDEX, timeFormat);
                pointconf[pointIndex].pointAttributes.timeformat = (TMWDEFS_TIME_FORMAT)(timeFormat);

                pointconf[pointIndex].vpointID = getXMLVirtualPointRef(itDBIConfig.vpoint.first());

                pointconf[pointIndex].pointAttributes.enable = getXMLAttr(itDBIConfig.enable);

                if(pointconf[pointIndex].pointAttributes.enable)
                {
                    doublePoint[pointIndex]= new S101MDP(pointconf[pointIndex]);
                    sectorPtr->getS101Database().addInputPoint(S101InputPoint::MDP,*(&doublePoint[pointIndex]));
                    GDatabase.attach(pointconf[pointIndex].vpointID, *(&doublePoint[pointIndex]));
                }
                pointIndex++;
            }
        }
        else
        {
            log.info("%s No double binary points configured", __AT__);
        }
    }
    catch (ParserException &e)
    {
        DBG_ERR("%s",e.what());
        log.error("%s in %s",e.what(), __AT__);
    }
    catch (...)
    {
        log.error("%s Exception raised", __AT__);
    }
}


void XML101SlaveProtocolParser::parseBinaryOutputConfig(CS101SessionT           *sessionParserPtr,
                                                        S101Sector *sectorPtr)
{
    try
    {
        lu_uint8_t timeTagMode;
        getXMLOptAttr(sessionParserPtr->config.first().cmdTimetagMode, timeTagMode);

        /* Parse binary output  points */
        if (sessionParserPtr->iomap.first().csc.exists())
        {
            /* count for number of binary output  points*/
            static lu_uint8_t BOPointNum = 0;

            /*Count number of BO points*/
            for(Iterator<CIEC870OutputPointT> itBOConfig = sessionParserPtr->iomap.first().csc.all();
                    itBOConfig; ++itBOConfig)
            {
                /* Skip points without valid protocol ID, disabled or with no related G3 point */
                S101_CHECK_POINT_FIELD(itBOConfig.protocolID, itBOConfig.enable, itBOConfig.controlLogicGroup);
                BOPointNum++;   // Count no of Binary output points
            }

            I870OutputPointConf *pointconf = new I870OutputPointConf[BOPointNum];

            S101OutputPoint ** bopoint = new S101OutputPoint*[BOPointNum];

            for(Iterator<CIEC870OutputPointT> itBOConfig = sessionParserPtr->iomap.first().csc.all();
                    itBOConfig; ++itBOConfig)
            {
                lu_uint8_t pointIndex = 0;
                /* Skip points without valid protocol ID, disabled or with no related G3 point */
                S101_CHECK_POINT_FIELD(itBOConfig.protocolID, itBOConfig.enable, itBOConfig.controlLogicGroup);

                pointconf[pointIndex].ioa = getXMLAttr(itBOConfig.protocolID);
                pointconf[pointIndex].groupID =  getXMLAttr(itBOConfig.controlLogicGroup);//!!!!group id!
                pointconf[pointIndex].selectRequired = getXMLAttr(itBOConfig.selectRequired);
                pointconf[pointIndex].enable = getXMLAttr(itBOConfig.enable);
                pointconf[pointIndex].timeTag = (TIME_TAG)timeTagMode;

                if(pointconf[pointIndex].enable)
                {
                    bopoint[pointIndex]= new S101OutputPoint(pointconf[pointIndex]);
                    sectorPtr->getS101Database().addOutputPoint(S101OutputPoint::SCO,*(&bopoint[pointIndex]));
                }
                pointIndex++;
            }
        }
        else
        {
            log.info("%s No double binary output points configured", __AT__);
        }
    }
    catch (const ParserException &e)
    {
        DBG_ERR("%s",e.what());
        log.error("%s in %s",e.what(), __AT__);
    }
    catch (...)
    {
        log.error("%s Exception raised", __AT__);
    }

}


void XML101SlaveProtocolParser::parseDBinaryOutputConfig(CS101SessionT           *sessionParserPtr,
                                                        S101Sector *sectorPtr)
{
    try
    {
        lu_uint8_t timeTagMode;
        getXMLOptAttr(sessionParserPtr->config.first().cmdTimetagMode, timeTagMode);

        /* Parse double binary input points */
        if (sessionParserPtr->iomap.first().cdc.exists())
        {
            /* count for number of double binary input points*/
            static lu_uint8_t DBOPointNum = 0;


            /*Count number of BI points*/
            for(Iterator<CIEC870OutputPointT> itDBOConfig = sessionParserPtr->iomap.first().cdc.all();
                    itDBOConfig; ++itDBOConfig)
            {
                /* Skip points without valid protocol ID, disabled or with no related G3 point */
                S101_CHECK_POINT_FIELD(itDBOConfig.protocolID, itDBOConfig.enable, itDBOConfig.controlLogicGroup);
                DBOPointNum++;   // Count no of Binary output points
            }

            I870OutputPointConf *pointconf = new I870OutputPointConf[DBOPointNum];

            S101OutputPoint ** dbopoint = new S101OutputPoint*[DBOPointNum];

            for(Iterator<CIEC870OutputPointT> itDBOConfig = sessionParserPtr->iomap.first().cdc.all();
                    itDBOConfig; ++itDBOConfig)
            {
                lu_uint8_t pointIndex = 0;
                /* Skip points without valid protocol ID, disabled or with no related G3 point */
                S101_CHECK_POINT_FIELD(itDBOConfig.protocolID, itDBOConfig.enable, itDBOConfig.controlLogicGroup);

                pointconf[pointIndex].ioa = getXMLAttr(itDBOConfig.protocolID);
                pointconf[pointIndex].groupID =  getXMLAttr(itDBOConfig.controlLogicGroup);
                pointconf[pointIndex].selectRequired = getXMLAttr(itDBOConfig.selectRequired);
                pointconf[pointIndex].enable = getXMLAttr(itDBOConfig.enable);
                pointconf[pointIndex].timeTag = (TIME_TAG)timeTagMode;

                if(pointconf[pointIndex].enable)
                {
                    dbopoint[pointIndex]= new S101OutputPoint(pointconf[pointIndex]);
                    sectorPtr->getS101Database().addOutputPoint(S101OutputPoint::DCO,*(&dbopoint[pointIndex]));
                }
                pointIndex++;
            }
        }
        else
        {
            log.info("%s No double binary output points configured", __AT__);
        }
    }
    catch (const ParserException &e)
    {
        DBG_ERR("%s",e.what());
        log.error("%s in %s",e.what(), __AT__);
    }
    catch (...)
    {
        log.error("%s Exception raised", __AT__);
    }
}


void XML101SlaveProtocolParser::parseIntegratedTotsConfig(GeminiDatabase           &GDatabase,
                                                        CS101SessionT           *sessionParserPtr,
                                                        S101Sector *sectorPtr)
{
    try
    {
        /* Parse double binary input points */
        if (sessionParserPtr->iomap.first().mit.exists())
        {
            /* count for number of double binary input points*/
            static lu_uint8_t mitPointNum = 0;


            /*Count number of BI points*/
            for(Iterator<CIEC870CounterPointT> itMITConfig = sessionParserPtr->iomap.first().mit.all();
                    itMITConfig; ++itMITConfig)
            {
                /* Skip points without valid protocol ID, disabled or with no related G3 point */
                S101_CHECK_POINT_FIELD(itMITConfig.protocolID, itMITConfig.enable, itMITConfig.vpoint);
                mitPointNum++;   // Count no of DBI points
            }

            S101InputPoint::Config *pointconf = new S101InputPoint::Config[mitPointNum];

            S101MIT** mitPoint = new S101MIT*[mitPointNum];

            for(Iterator<CIEC870CounterPointT> itMITConfig = sessionParserPtr->iomap.first().mit.all();
                    itMITConfig; ++itMITConfig)
            {
                lu_uint8_t pointIndex = 0;
                /* Skip points without valid protocol ID, disabled or with no related G3 point */
                S101_CHECK_POINT_FIELD(itMITConfig.protocolID, itMITConfig.enable, itMITConfig.vpoint);

                pointconf[pointIndex].pointAttributes.ioa = getXMLAttr(itMITConfig.protocolID);

                /*applying group masks*/
                pointconf[pointIndex].pointAttributes.groupMask = getXMLAttr(itMITConfig.groupMask);

                if(itMITConfig.enableCyclic)
                {
                    pointconf[pointIndex].pointAttributes.groupMask |= TMWDEFS_GROUP_MASK_CYCLIC;
                }
                if(itMITConfig.enableBackground)
                {
                    pointconf[pointIndex].pointAttributes.groupMask |= TMWDEFS_GROUP_MASK_BACKGROUND;
                }
                if(itMITConfig.enableGeneralInterrogation)
                {
                    pointconf[pointIndex].pointAttributes.groupMask |= TMWDEFS_GROUP_MASK_GENERAL;
                }
                getXMLOptAttr(itMITConfig.spontaneousEvent, pointconf[pointIndex].pointAttributes.spontaneousEvent);
                pointconf[pointIndex].pointAttributes.eventOnlyWhenOnline = getXMLAttr(itMITConfig.eventOnlyWhenConnected);
                pointconf[pointIndex].pointAttributes.eventLogStore = TMWDEFS_TRUE;
                getXMLOptAttr(itMITConfig.enableStoreEvent, pointconf[pointIndex].pointAttributes.eventLogStore);

                /* Get time format */
                lu_uint32_t timeFormat = TMWDEFS_TIME_FORMAT_56; //default
                getSessionTimeFormat(sessionParserPtr->config.first().eventConfig.first().mit, timeFormat);
                getXMLOptAttrEnum(itMITConfig.timeformat, LU_TIME_FORMAT_VALUEfromINDEX, timeFormat);
                pointconf[pointIndex].pointAttributes.timeformat = (TMWDEFS_TIME_FORMAT)(timeFormat);

                pointconf[pointIndex].vpointID = getXMLVirtualPointRef(itMITConfig.vpoint.first());

                pointconf[pointIndex].pointAttributes.enable = getXMLAttr(itMITConfig.enable);

                if(pointconf[pointIndex].pointAttributes.enable)
                {
                    mitPoint[pointIndex] = new S101MIT(pointconf[pointIndex]);
                    /* TODO: pueyos_a - [s101] set counter value to use (current/frozen) from configuration */
                    mitPoint[pointIndex]->setType(S101MIT::COUNTER_TYPE_FROZEN);    //Default to frozen for now

                    sectorPtr->getS101Database().addInputPoint(S101InputPoint::MIT, *(&mitPoint[pointIndex]));
                    GDatabase.attach(pointconf[pointIndex].vpointID, *(&mitPoint[pointIndex]));
                }
                pointIndex++;
            }
        }
        else
        {
            log.info("%s No integrated totals configured", __AT__);
        }
    }
    catch (const ParserException &e)
    {
        DBG_ERR("%s",e.what());
        log.error("%s in %s",e.what(), __AT__);
    }
    catch (...)
    {
        log.error("%s Exception raised", __AT__);
    }
}


void XML101SlaveProtocolParser::parseChannelConfig(CS101ChannelT&  itChannelConfig,
                                                   S101Channel::S101ChannelConfig& channelConfig) throw(ParserException)
{
    // Initialise the TMW Structures
    liniotarg_initConfig(&(channelConfig.pIOConfig));

    tmwtarg_initConfig(&(channelConfig.pTMWTargConfig));

    ft12chnl_initConfig(&(channelConfig.pChnlConfig),
                       &(channelConfig.pLinkConfig),
                       &(channelConfig.pPhysConfig));

    CS101ChlConfT chCfg = getXMLElement<CS101ChlConfT>(itChannelConfig.config);
    channelConfig.pChnlConfig.incrementalTimeout = getXMLAttr(chCfg.incrementalTimeoutMs);
    //channelConfig.pChnlConfig.maxQueueSize = getXMLAttr(chCfg..maxQueueSize);

    /* Parse the LINK LAYER parameters */
    CS101LinkLayerConfT s101LinkConfigStr = getXMLElement<CS101LinkLayerConfT>(chCfg.linkLayer);
    channelConfig.pLinkConfig.rxFrameSize = getXMLAttr(s101LinkConfigStr.rxFrameSize);
    channelConfig.pLinkConfig.txFrameSize = getXMLAttr(s101LinkConfigStr.txFrameSize);
    channelConfig.pLinkConfig.offlinePollPeriod     = getXMLAttr(s101LinkConfigStr.offlinePollPeriod     );
    channelConfig.pLinkConfig.linkAddressSize       = getXMLAttr(s101LinkConfigStr.linkAddressSize       );
    channelConfig.pLinkConfig.oneCharAckAllowed     = getXMLAttr(s101LinkConfigStr.oneCharAckAllowed     );
    channelConfig.pLinkConfig.oneCharResponseAllowed= getXMLAttr(s101LinkConfigStr.oneCharResponseAllowed);
    channelConfig.pLinkConfig.rxFrameTimeout        = getXMLAttr(s101LinkConfigStr.rxFrameTimeout        );
    channelConfig.pLinkConfig.confirmTimeout        = getXMLAttr(s101LinkConfigStr.confirmTimeout        );
    channelConfig.pLinkConfig.maxRetries            = getXMLAttr(s101LinkConfigStr.maxRetries            );
    channelConfig.pLinkConfig.testFramePeriod       = getXMLAttr(s101LinkConfigStr.testFramePeriod       );

    lu_int32_t linkModeNum;
    if (LINK_MODE_VALUEfromINDEX(getXMLAttr(s101LinkConfigStr.linkMode), &linkModeNum) == LU_TRUE)
    {
        channelConfig.pLinkConfig.linkMode = (TMWDEFS_LINK_MODE)linkModeNum;
    }
    lu_int32_t confirmModeNum;
    if (LINKCNFM_VALUEfromINDEX(getXMLAttr(s101LinkConfigStr.confirmMode), &confirmModeNum) == LU_TRUE)
    {
        //Note that LINKCNFM does not match values with TMWDEFS_LINKCNFM
        switch (confirmModeNum)
        {
            case LINKCNFM_NEVER:
                channelConfig.pLinkConfig.confirmMode = TMWDEFS_LINKCNFM_NEVER;
                break;
            case LINKCNFM_ALWAYS:
                channelConfig.pLinkConfig.confirmMode = TMWDEFS_LINKCNFM_ALWAYS;
                break;
            default:
                std::string errMsg;
                errMsg = "Invalid value " + to_string(confirmModeNum) + " for Confirm Mode";
                throw ParserException(CONFMGR_ERROR_CONFIG, errMsg.c_str());
                break;
        }
    }

    //channelConfig.pLinkConfig.isControlling = TMWDEFS_FALSE; //by default because its slave

    /* Read serial configuration */
    itChannelConfig.config.first().linuxIO.first().serial;
    CS101LinuxIoT linuxIo = getXMLElement<CS101LinuxIoT>(chCfg.linuxIO);
    CSerialConfT SerialConfigStr = getXMLElement<CSerialConfT>(linuxIo.serial);

    Port* portObj;
    // Copy the Port String
    std::string portString = getXMLAttr(SerialConfigStr.serialPortName);
    if ((portObj = portManager.getPort(portString)) == NULL)
    {
        std::string errMsg;
        errMsg = "Port [" + portString + "] not found";
        throw ParserException(CONFMGR_ERROR_CONFIG, errMsg.c_str());
    }
    channelConfig.pIOConfig.lin232.baudRate    = portObj->serialConf.serBaudrate;
    channelConfig.pIOConfig.lin232.numDataBits = portObj->serialConf.serDataBits;
    channelConfig.pIOConfig.lin232.numStopBits = portObj->serialConf.serStopBits;
    channelConfig.pIOConfig.lin232.parity      = portObj->serialConf.serParity;
    channelConfig.pIOConfig.lin232.portMode    = portObj->serialConf.serPortMode;
    std::string tmp_chnlName = getXMLAttr(chCfg.chnlName);
    stringToCString(tmp_chnlName, channelConfig.pIOConfig.lin232.chnlName, LINIOCNFG_MAX_NAME_LEN);
    stringToCString(portObj->getPortPath(), channelConfig.pIOConfig.lin232.portName, LINIOCNFG_MAX_NAME_LEN);

    // Not relevant to Lucy, force to FALSE
    channelConfig.pIOConfig.lin232.bModbusRTU   = TMWDEFS_FALSE;
    channelConfig.pIOConfig.lin232.bModemFitted = TMWDEFS_FALSE;
}


lu_uint32_t XML101SlaveProtocolParser::parseSessionConfig(CS101SessionT&  sessionConfig,
                                                          S101SESN_CONFIG& sessionConfigStr) throw(ParserException)
{
    s101sesn_initConfig(&(sessionConfigStr));    // Initialise the TMW Structures

    CS101SesnConfT session = getXMLElement<CS101SesnConfT>(sessionConfig.config);
    lu_uint32_t maxFrameLength; //Note that ASDU length has to be calculated by the caller
    maxFrameLength = getXMLAttr(session.maxFrameSize);
    sessionConfigStr.linkAddress = getXMLAttr(session.linkAddress);
    sessionConfigStr.asduAddrSize = getXMLAttr(session.asduAddrSize);
    sessionConfigStr.infoObjAddrSize = getXMLAttr(session.infoObjAddrSize);
    sessionConfigStr.maxPollDelay = getXMLAttr(session.maxPollDelay);
    sessionConfigStr.useDayOfWeek = getXMLAttr(session.useDayOfWeek);

    lu_uint32_t cotSize = getXMLAttr(session.cotSize);
    if (cotSize < 3)
    {
        sessionConfigStr.cotSize = cotSize;
    }
    else
    {
        std::string errMsg;
        errMsg = "invalid COT size " + to_string(cotSize);
        throw ParserException(CONFMGR_ERROR_CONFIG, errMsg.c_str());
    }

    return maxFrameLength;
}


void XML101SlaveProtocolParser::parseSectorConfig(CS101SessionT&  sessionConfig,
                                                  S101SCTR_CONFIG& sectorConfigStr) throw(ParserException)
{
    // Initialise the TMW Structures
    s101sctr_initConfig(&(sectorConfigStr));

    CS101SesnConfT session = getXMLElement<CS101SesnConfT>(sessionConfig.config);
    sectorConfigStr.asduAddress = getXMLAttr(session.asduAddress);
    sectorConfigStr.cyclicPeriod = getXMLAttr(session.cyclicPeriodMs);
    sectorConfigStr.cyclicFirstPeriod = getXMLAttr(session.cyclicFirstPeriodMs);
    sectorConfigStr.backgroundPeriod = getXMLAttr(session.backgroundPeriodMs);
    //sectorConfigStr.rbeScanPeriod  = getXMLAttr(session.rbeScanPeriodMs);
    sectorConfigStr.selectTimeout = getXMLAttr(session.selectTimeoutMs);
    sectorConfigStr.defaultResponseTimeout = getXMLAttr(session.defResponseTimeoutMs);
    sectorConfigStr.cseUseActTerm = getXMLAttr(session.cseUseActTerm);
    sectorConfigStr.cmdUseActTerm = getXMLAttr(session.cmdUseActTerm);
    sectorConfigStr.sendClockSyncEvents = getXMLAttr(session.sendClockSyncEvents);
    sectorConfigStr.deleteOldestEvent = getXMLAttr(session.deleteOldestEvent);

    bool multiplePMEN = getXMLAttr(session.multiplePMEN); //Get for composing bit field
    if(!multiplePMEN)
    {
        sectorConfigStr.strictAdherence |= S14SCTR_STRICT_PMENABC;
    }

    /*we set the below members as TMWDEFS_TIME_FORMAT_UNKNOWN by default if these
     * are not present in order to use the per-point time formats for:
     *      - READ commands (analogue points and the rest)
     *      - General Interrogation commands
     */
    sectorConfigStr.readTimeFormat = TMWDEFS_TIME_FORMAT_UNKNOWN;
    sectorConfigStr.readMsrndTimeFormat = TMWDEFS_TIME_FORMAT_UNKNOWN;
    sectorConfigStr.cicnaTimeFormat = TMWDEFS_TIME_FORMAT_UNKNOWN;

    getXMLOptAttrEnum(session.readTimeFormat,  LU_TIME_FORMAT_VALUEfromINDEX, sectorConfigStr.readTimeFormat);
    getXMLOptAttrEnum(session.readMsrndTimeFormat,  LU_TIME_FORMAT_VALUEfromINDEX, sectorConfigStr.readMsrndTimeFormat );
    getXMLOptAttrEnum(session.cicnaTimeFormat, LU_TIME_FORMAT_VALUEfromINDEX, sectorConfigStr.cicnaTimeFormat);

    /* === Eventing configuration === */
    checkXMLElement(sessionConfig.config);
    CIEC870EventConfigT eventCfg = getXMLElement<CIEC870EventConfigT>(session.eventConfig);

    /*binary input Events*/
    CIEC870SesnEventT mspEvents = getXMLElement<CIEC870SesnEventT>(eventCfg.msp);
    sectorConfigStr.mspMaxEvents = getXMLAttr(mspEvents.maxEvents);
    sectorConfigStr.mspEventMode = getXMLAttrEnum<TMWDEFS_EVENT_MODE>(mspEvents.eventMode, LU_EVENT_MODE_VALUEfromINDEX);
    getXMLOptAttrEnum(mspEvents.timeFormat, LU_TIME_FORMAT_VALUEfromINDEX, sectorConfigStr.mspTimeFormat );

    /*double binary input Events*/
    CIEC870SesnEventT mdpEvents = getXMLElement<CIEC870SesnEventT>(eventCfg.mdp);
    sectorConfigStr.mdpMaxEvents = getXMLAttr(mdpEvents.maxEvents);
    sectorConfigStr.mdpEventMode = getXMLAttrEnum<TMWDEFS_EVENT_MODE>(mdpEvents.eventMode, LU_EVENT_MODE_VALUEfromINDEX);
    getXMLOptAttrEnum(mdpEvents.timeFormat, LU_TIME_FORMAT_VALUEfromINDEX, sectorConfigStr.mdpTimeFormat );

    /*Integrated Totals (counter) input Events*/
    CIEC870SesnEventT mitEvents = getXMLElement<CIEC870SesnEventT>(eventCfg.mit);
    sectorConfigStr.mitMaxEvents = getXMLAttr(mitEvents.maxEvents);
    sectorConfigStr.mitEventMode = getXMLAttrEnum<TMWDEFS_EVENT_MODE>(mitEvents.eventMode, LU_EVENT_MODE_VALUEfromINDEX);
    getXMLOptAttrEnum(mitEvents.timeFormat, LU_TIME_FORMAT_VALUEfromINDEX, sectorConfigStr.mitTimeFormat );

    /*Floating point Analog Events*/
    CIEC870SesnEventT mmencEvents = getXMLElement<CIEC870SesnEventT>(eventCfg.mmec);
    sectorConfigStr.mmencMaxEvents = getXMLAttr(mmencEvents.maxEvents);
    sectorConfigStr.mmencEventMode = getXMLAttrEnum<TMWDEFS_EVENT_MODE>(mmencEvents.eventMode, LU_EVENT_MODE_VALUEfromINDEX);
    getXMLOptAttrEnum(mmencEvents.timeFormat, LU_TIME_FORMAT_VALUEfromINDEX, sectorConfigStr.mmencTimeFormat);

    /*Scaled Analog Events*/
    CIEC870SesnEventT mmenbEvents = getXMLElement<CIEC870SesnEventT>(eventCfg.mmeb);
    sectorConfigStr.mmenbMaxEvents = getXMLAttr(mmenbEvents.maxEvents);
    sectorConfigStr.mmenbEventMode = getXMLAttrEnum<TMWDEFS_EVENT_MODE>(mmenbEvents.eventMode, LU_EVENT_MODE_VALUEfromINDEX);
    getXMLOptAttrEnum(mmenbEvents.timeFormat, LU_TIME_FORMAT_VALUEfromINDEX, sectorConfigStr.mmenbTimeFormat);

    /*Normalized Analog Events*/
    CIEC870SesnEventT mmenaEvents = getXMLElement<CIEC870SesnEventT>(eventCfg.mmea);
    sectorConfigStr.mmenaMaxEvents = getXMLAttr(mmenaEvents.maxEvents);
    sectorConfigStr.mmenaEventMode = getXMLAttrEnum<TMWDEFS_EVENT_MODE>(mmenaEvents.eventMode, LU_EVENT_MODE_VALUEfromINDEX);
    getXMLOptAttrEnum(mmenaEvents.timeFormat, LU_TIME_FORMAT_VALUEfromINDEX, sectorConfigStr.mmenaTimeFormat);

    DBG_DUMP_SECTOR101();
}


/*
 *********************** End of file ******************************************
 */
