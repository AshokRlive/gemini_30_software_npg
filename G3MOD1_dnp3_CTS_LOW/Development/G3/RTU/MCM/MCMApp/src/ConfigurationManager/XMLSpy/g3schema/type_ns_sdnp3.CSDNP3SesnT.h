#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3SesnT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3SesnT



namespace g3schema
{

namespace ns_sdnp3
{	

class CSDNP3SesnT : public TypeBase
{
public:
	g3schema_EXPORT CSDNP3SesnT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSDNP3SesnT(CSDNP3SesnT const& init);
	void operator=(CSDNP3SesnT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_sdnp3_altova_CSDNP3SesnT); }
	MemberElement<ns_sdnp3::CSDNP3SesnConfT, _altova_mi_ns_sdnp3_altova_CSDNP3SesnT_altova_config> config;
	struct config { typedef Iterator<ns_sdnp3::CSDNP3SesnConfT> iterator; };
	MemberElement<ns_sdnp3::CSDNP3PointsT, _altova_mi_ns_sdnp3_altova_CSDNP3SesnT_altova_iomap> iomap;
	struct iomap { typedef Iterator<ns_sdnp3::CSDNP3PointsT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_sdnp3

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3SesnT
