#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CConfigurationT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CConfigurationT



namespace g3schema
{

class CConfigurationT : public TypeBase
{
public:
	g3schema_EXPORT CConfigurationT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CConfigurationT(CConfigurationT const& init);
	void operator=(CConfigurationT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_altova_CConfigurationT); }

	MemberAttribute<unsigned,_altova_mi_altova_CConfigurationT_altova_schemaVersionMajor, 0, 0> schemaVersionMajor;	// schemaVersionMajor CunsignedByte

	MemberAttribute<unsigned,_altova_mi_altova_CConfigurationT_altova_schemaVersionMinor, 0, 0> schemaVersionMinor;	// schemaVersionMinor CunsignedByte

	MemberAttribute<string_type,_altova_mi_altova_CConfigurationT_altova_siteName, 0, 0> siteName;	// siteName Cstring

	MemberAttribute<string_type,_altova_mi_altova_CConfigurationT_altova_modificationDate, 0, 0> modificationDate;	// modificationDate CFileModificationDateT

	MemberAttribute<string_type,_altova_mi_altova_CConfigurationT_altova_configReleaseType, 0, 0> configReleaseType;	// configReleaseType Cstring

	MemberAttribute<unsigned,_altova_mi_altova_CConfigurationT_altova_configVersionMinor, 0, 0> configVersionMinor;	// configVersionMinor CunsignedInt

	MemberAttribute<unsigned,_altova_mi_altova_CConfigurationT_altova_configVersionMajor, 0, 0> configVersionMajor;	// configVersionMajor CunsignedInt

	MemberAttribute<unsigned,_altova_mi_altova_CConfigurationT_altova_configVersionPatch, 0, 0> configVersionPatch;	// configVersionPatch CunsignedInt

	MemberAttribute<string_type,_altova_mi_altova_CConfigurationT_altova_configFileDescription, 0, 0> configFileDescription;	// configFileDescription Cstring
	MemberElement<CHeaderT, _altova_mi_altova_CConfigurationT_altova_header> header;
	struct header { typedef Iterator<CHeaderT> iterator; };
	MemberElement<ns_g3module::CModulesT, _altova_mi_altova_CConfigurationT_altova_modules> modules;
	struct modules { typedef Iterator<ns_g3module::CModulesT> iterator; };
	MemberElement<ns_vpoint::CVirtualPointsT, _altova_mi_altova_CConfigurationT_altova_virtualPoints> virtualPoints;
	struct virtualPoints { typedef Iterator<ns_vpoint::CVirtualPointsT> iterator; };
	MemberElement<ns_clogic::CControlLogicsT, _altova_mi_altova_CConfigurationT_altova_controlLogic> controlLogic;
	struct controlLogic { typedef Iterator<ns_clogic::CControlLogicsT> iterator; };
	MemberElement<ns_port::CPortsT, _altova_mi_altova_CConfigurationT_altova_ports> ports;
	struct ports { typedef Iterator<ns_port::CPortsT> iterator; };
	MemberElement<CScadaProtocolT, _altova_mi_altova_CConfigurationT_altova_scadaProtocol> scadaProtocol;
	struct scadaProtocol { typedef Iterator<CScadaProtocolT> iterator; };
	MemberElement<CUserSettingT, _altova_mi_altova_CConfigurationT_altova_userSetting> userSetting;
	struct userSetting { typedef Iterator<CUserSettingT> iterator; };
	MemberElement<CFieldDevicesT, _altova_mi_altova_CConfigurationT_altova_fieldDevices> fieldDevices;
	struct fieldDevices { typedef Iterator<CFieldDevicesT> iterator; };
	MemberElement<ns_comms::CCommDevicesT, _altova_mi_altova_CConfigurationT_altova_commsDevices> commsDevices;
	struct commsDevices { typedef Iterator<ns_comms::CCommDevicesT> iterator; };
	MemberElement<CMiscellaneousT, _altova_mi_altova_CConfigurationT_altova_miscellaneous> miscellaneous;
	struct miscellaneous { typedef Iterator<CMiscellaneousT> iterator; };
	MemberElement<ns_template::CTemplateT, _altova_mi_altova_CConfigurationT_altova_template2> template2;
	struct template2 { typedef Iterator<ns_template::CTemplateT> iterator; };
	g3schema_EXPORT void SetXsiType();
};


}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CConfigurationT
