#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_tcl_ALTOVA_CTimeMonitoringModeT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_tcl_ALTOVA_CTimeMonitoringModeT



namespace g3schema
{

namespace ns_tcl
{	

class CTimeMonitoringModeT : public TypeBase
{
public:
	g3schema_EXPORT CTimeMonitoringModeT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CTimeMonitoringModeT(CTimeMonitoringModeT const& init);
	void operator=(CTimeMonitoringModeT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_tcl_altova_CTimeMonitoringModeT); }

	MemberAttribute<string_type,_altova_mi_ns_tcl_altova_CTimeMonitoringModeT_altova_summerStartDate, 0, 0> summerStartDate;	// summerStartDate Cstring

	MemberAttribute<string_type,_altova_mi_ns_tcl_altova_CTimeMonitoringModeT_altova_winterStartDate, 0, 0> winterStartDate;	// winterStartDate Cstring
	MemberElement<ns_tcl::CTimeSlotT, _altova_mi_ns_tcl_altova_CTimeMonitoringModeT_altova_summerWeekdays> summerWeekdays;
	struct summerWeekdays { typedef Iterator<ns_tcl::CTimeSlotT> iterator; };
	MemberElement<ns_tcl::CTimeSlotT, _altova_mi_ns_tcl_altova_CTimeMonitoringModeT_altova_summerWeekends> summerWeekends;
	struct summerWeekends { typedef Iterator<ns_tcl::CTimeSlotT> iterator; };
	MemberElement<ns_tcl::CTimeSlotT, _altova_mi_ns_tcl_altova_CTimeMonitoringModeT_altova_winterWeekdays> winterWeekdays;
	struct winterWeekdays { typedef Iterator<ns_tcl::CTimeSlotT> iterator; };
	MemberElement<ns_tcl::CTimeSlotT, _altova_mi_ns_tcl_altova_CTimeMonitoringModeT_altova_winterWeekends> winterWeekends;
	struct winterWeekends { typedef Iterator<ns_tcl::CTimeSlotT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_tcl

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_tcl_ALTOVA_CTimeMonitoringModeT
