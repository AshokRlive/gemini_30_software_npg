#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen1_ALTOVA_CDNPLINK_NETWORK_TYPE
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen1_ALTOVA_CDNPLINK_NETWORK_TYPE



namespace g3schema
{

namespace ns_gen1
{	

class CDNPLINK_NETWORK_TYPE : public TypeBase
{
public:
	g3schema_EXPORT CDNPLINK_NETWORK_TYPE(xercesc::DOMNode* const& init);
	g3schema_EXPORT CDNPLINK_NETWORK_TYPE(CDNPLINK_NETWORK_TYPE const& init);
	void operator=(CDNPLINK_NETWORK_TYPE const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen1_altova_CDNPLINK_NETWORK_TYPE); }

	enum EnumValues {
		Invalid = -1,
		k_DNPLINK_NETWORK_SERIAL = 0, // DNPLINK_NETWORK_SERIAL
		k_DNPLINK_NETWORK_COMMS_DEVICE = 1, // DNPLINK_NETWORK_COMMS_DEVICE
		k_DNPLINK_NETWORK_TCP_ONLY = 2, // DNPLINK_NETWORK_TCP_ONLY
		k_DNPLINK_NETWORK_TCP_UDP = 3, // DNPLINK_NETWORK_TCP_UDP
		k_DNPLINK_NETWORK_UDP_ONLY = 4, // DNPLINK_NETWORK_UDP_ONLY
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen1

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen1_ALTOVA_CDNPLINK_NETWORK_TYPE
