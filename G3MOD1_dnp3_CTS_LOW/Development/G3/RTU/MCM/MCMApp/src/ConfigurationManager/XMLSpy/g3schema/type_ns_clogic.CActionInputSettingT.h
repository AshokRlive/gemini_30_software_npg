#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CActionInputSettingT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CActionInputSettingT



namespace g3schema
{

namespace ns_clogic
{	

class CActionInputSettingT : public TypeBase
{
public:
	g3schema_EXPORT CActionInputSettingT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CActionInputSettingT(CActionInputSettingT const& init);
	void operator=(CActionInputSettingT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_clogic_altova_CActionInputSettingT); }

	MemberAttribute<unsigned,_altova_mi_ns_clogic_altova_CActionInputSettingT_altova_command, 0, 0> command;	// command CunsignedByte
	MemberElement<ns_vpoint::CInputModeT, _altova_mi_ns_clogic_altova_CActionInputSettingT_altova_inputMode> inputMode;
	struct inputMode { typedef Iterator<ns_vpoint::CInputModeT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_clogic

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CActionInputSettingT
