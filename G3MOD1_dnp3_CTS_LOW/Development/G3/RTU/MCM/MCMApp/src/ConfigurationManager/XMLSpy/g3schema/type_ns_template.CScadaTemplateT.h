#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CScadaTemplateT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CScadaTemplateT



namespace g3schema
{

namespace ns_template
{	

class CScadaTemplateT : public TypeBase
{
public:
	g3schema_EXPORT CScadaTemplateT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CScadaTemplateT(CScadaTemplateT const& init);
	void operator=(CScadaTemplateT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_template_altova_CScadaTemplateT); }

	MemberAttribute<string_type,_altova_mi_ns_template_altova_CScadaTemplateT_altova_profileName, 0, 0> profileName;	// profileName Cstring

	MemberAttribute<string_type,_altova_mi_ns_template_altova_CScadaTemplateT_altova_uuid, 0, 0> uuid;	// uuid Cstring

	MemberAttribute<string_type,_altova_mi_ns_template_altova_CScadaTemplateT_altova_classifier, 0, 0> classifier;	// classifier Cstring
	MemberElement<ns_template::CScadaTemplateCommsDevT, _altova_mi_ns_template_altova_CScadaTemplateT_altova_commsDevice> commsDevice;
	struct commsDevice { typedef Iterator<ns_template::CScadaTemplateCommsDevT> iterator; };
	MemberElement<ns_template::CScadaTemplateTcpT, _altova_mi_ns_template_altova_CScadaTemplateT_altova_tcpip> tcpip;
	struct tcpip { typedef Iterator<ns_template::CScadaTemplateTcpT> iterator; };
	MemberElement<ns_pstack::CSerialConfT, _altova_mi_ns_template_altova_CScadaTemplateT_altova_serial> serial;
	struct serial { typedef Iterator<ns_pstack::CSerialConfT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_template

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CScadaTemplateT
