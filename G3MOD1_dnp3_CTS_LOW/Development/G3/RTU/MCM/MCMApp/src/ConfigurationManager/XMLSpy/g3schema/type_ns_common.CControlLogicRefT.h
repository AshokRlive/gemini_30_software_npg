#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_CControlLogicRefT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_CControlLogicRefT

#include "type_ns_common.CVirtualPointGroupTType.h"


namespace g3schema
{

namespace ns_common
{	

class CControlLogicRefT : public ::g3schema::ns_common::CVirtualPointGroupTType
{
public:
	g3schema_EXPORT CControlLogicRefT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CControlLogicRefT(CControlLogicRefT const& init);
	void operator=(CControlLogicRefT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_common_altova_CControlLogicRefT); }
	g3schema_EXPORT void operator=(const unsigned& value);
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_common

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_CControlLogicRefT
