#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CEFH_OPERATOR
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CEFH_OPERATOR



namespace g3schema
{

namespace ns_clogic
{	

class CEFH_OPERATOR : public TypeBase
{
public:
	g3schema_EXPORT CEFH_OPERATOR(xercesc::DOMNode* const& init);
	g3schema_EXPORT CEFH_OPERATOR(CEFH_OPERATOR const& init);
	void operator=(CEFH_OPERATOR const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_clogic_altova_CEFH_OPERATOR); }

	enum EnumValues {
		Invalid = -1,
		k_AND = 0, // AND
		k_OR = 1, // OR
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_clogic

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CEFH_OPERATOR
