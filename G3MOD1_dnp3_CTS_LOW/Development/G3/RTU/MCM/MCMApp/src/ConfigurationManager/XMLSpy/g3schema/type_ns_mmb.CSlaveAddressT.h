#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_mmb_ALTOVA_CSlaveAddressT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_mmb_ALTOVA_CSlaveAddressT



namespace g3schema
{

namespace ns_mmb
{	

class CSlaveAddressT : public TypeBase
{
public:
	g3schema_EXPORT CSlaveAddressT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSlaveAddressT(CSlaveAddressT const& init);
	void operator=(CSlaveAddressT const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_mmb_altova_CSlaveAddressT); }
	void operator= (const unsigned& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::IntegerFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator unsigned()
	{
		return CastAs<unsigned >::Do(GetNode(), 0);
	}
};



} // namespace ns_mmb

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_mmb_ALTOVA_CSlaveAddressT
