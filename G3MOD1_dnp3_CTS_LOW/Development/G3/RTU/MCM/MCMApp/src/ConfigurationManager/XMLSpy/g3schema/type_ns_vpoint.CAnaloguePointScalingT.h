#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CAnaloguePointScalingT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CAnaloguePointScalingT



namespace g3schema
{

namespace ns_vpoint
{	

class CAnaloguePointScalingT : public TypeBase
{
public:
	g3schema_EXPORT CAnaloguePointScalingT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CAnaloguePointScalingT(CAnaloguePointScalingT const& init);
	void operator=(CAnaloguePointScalingT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CAnaloguePointScalingT); }

	MemberAttribute<double,_altova_mi_ns_vpoint_altova_CAnaloguePointScalingT_altova_rawMin, 0, 0> rawMin;	// rawMin Cfloat

	MemberAttribute<double,_altova_mi_ns_vpoint_altova_CAnaloguePointScalingT_altova_rawMax, 0, 0> rawMax;	// rawMax Cfloat

	MemberAttribute<double,_altova_mi_ns_vpoint_altova_CAnaloguePointScalingT_altova_scalingFactor, 0, 0> scalingFactor;	// scalingFactor Cfloat

	MemberAttribute<double,_altova_mi_ns_vpoint_altova_CAnaloguePointScalingT_altova_offset, 0, 0> offset;	// offset Cfloat
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CAnaloguePointScalingT
