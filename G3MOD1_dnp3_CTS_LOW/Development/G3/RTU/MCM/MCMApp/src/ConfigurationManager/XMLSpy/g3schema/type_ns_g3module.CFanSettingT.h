#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CFanSettingT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CFanSettingT



namespace g3schema
{

namespace ns_g3module
{	

class CFanSettingT : public TypeBase
{
public:
	g3schema_EXPORT CFanSettingT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CFanSettingT(CFanSettingT const& init);
	void operator=(CFanSettingT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_g3module_altova_CFanSettingT); }

	MemberAttribute<bool,_altova_mi_ns_g3module_altova_CFanSettingT_altova_fanFitted, 0, 0> fanFitted;	// fanFitted Cboolean

	MemberAttribute<int,_altova_mi_ns_g3module_altova_CFanSettingT_altova_fanTempThreshold, 0, 0> fanTempThreshold;	// fanTempThreshold Cint

	MemberAttribute<int,_altova_mi_ns_g3module_altova_CFanSettingT_altova_fanTempHysteresis, 0, 0> fanTempHysteresis;	// fanTempHysteresis Cint

	MemberAttribute<int,_altova_mi_ns_g3module_altova_CFanSettingT_altova_faultHysteresis, 0, 0> faultHysteresis;	// faultHysteresis Cint

	MemberAttribute<bool,_altova_mi_ns_g3module_altova_CFanSettingT_altova_fanSpdSensorFitted, 0, 0> fanSpdSensorFitted;	// fanSpdSensorFitted Cboolean

	MemberAttribute<bool,_altova_mi_ns_g3module_altova_CFanSettingT_altova_fanOutAsLED, 0, 0> fanOutAsLED;	// fanOutAsLED Cboolean

	MemberAttribute<bool,_altova_mi_ns_g3module_altova_CFanSettingT_altova_fanOutAsDO, 0, 0> fanOutAsDO;	// fanOutAsDO Cboolean
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_g3module

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CFanSettingT
