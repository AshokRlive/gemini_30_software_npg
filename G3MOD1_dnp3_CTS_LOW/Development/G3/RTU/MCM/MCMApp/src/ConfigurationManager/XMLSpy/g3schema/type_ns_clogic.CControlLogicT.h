#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CControlLogicT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CControlLogicT



namespace g3schema
{

namespace ns_clogic
{	

class CControlLogicT : public TypeBase
{
public:
	g3schema_EXPORT CControlLogicT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CControlLogicT(CControlLogicT const& init);
	void operator=(CControlLogicT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_clogic_altova_CControlLogicT); }

	MemberAttribute<unsigned,_altova_mi_ns_clogic_altova_CControlLogicT_altova_group, 0, 0> group;	// group CVirtualPointGroupT

	MemberAttribute<string_type,_altova_mi_ns_clogic_altova_CControlLogicT_altova_name, 0, 0> name;	// name Cstring

	MemberAttribute<unsigned,_altova_mi_ns_clogic_altova_CControlLogicT_altova_logicType, 0, 0> logicType;	// logicType CunsignedByte

	MemberAttribute<bool,_altova_mi_ns_clogic_altova_CControlLogicT_altova_enabled, 0, 0> enabled;	// enabled Cboolean

	MemberAttribute<string_type,_altova_mi_ns_clogic_altova_CControlLogicT_altova_uuid, 0, 0> uuid;	// uuid Cstring
	MemberElement<ns_clogic::CCLogicBinaryPointT, _altova_mi_ns_clogic_altova_CControlLogicT_altova_binaryPoints> binaryPoints;
	struct binaryPoints { typedef Iterator<ns_clogic::CCLogicBinaryPointT> iterator; };
	MemberElement<ns_clogic::CCLogicDoubleBinaryPointT, _altova_mi_ns_clogic_altova_CControlLogicT_altova_dbinaryPoints> dbinaryPoints;
	struct dbinaryPoints { typedef Iterator<ns_clogic::CCLogicDoubleBinaryPointT> iterator; };
	MemberElement<ns_clogic::CCLogicAnalogPointT, _altova_mi_ns_clogic_altova_CControlLogicT_altova_analoguePoints> analoguePoints;
	struct analoguePoints { typedef Iterator<ns_clogic::CCLogicAnalogPointT> iterator; };
	MemberElement<ns_clogic::CCLogicCounterPointT, _altova_mi_ns_clogic_altova_CControlLogicT_altova_counterPoints> counterPoints;
	struct counterPoints { typedef Iterator<ns_clogic::CCLogicCounterPointT> iterator; };
	MemberElement<ns_clogic::CCLogicInputsT, _altova_mi_ns_clogic_altova_CControlLogicT_altova_inputs> inputs;
	struct inputs { typedef Iterator<ns_clogic::CCLogicInputsT> iterator; };
	MemberElement<ns_clogic::CCLogicOutputsT, _altova_mi_ns_clogic_altova_CControlLogicT_altova_outputs> outputs;
	struct outputs { typedef Iterator<ns_clogic::CCLogicOutputsT> iterator; };
	MemberElement<ns_clogic::CCLogicParametersT, _altova_mi_ns_clogic_altova_CControlLogicT_altova_parameters> parameters;
	struct parameters { typedef Iterator<ns_clogic::CCLogicParametersT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_clogic

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CControlLogicT
