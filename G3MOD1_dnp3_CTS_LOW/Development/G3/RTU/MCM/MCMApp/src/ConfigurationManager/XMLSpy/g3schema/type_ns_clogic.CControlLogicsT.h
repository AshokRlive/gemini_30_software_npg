#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CControlLogicsT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CControlLogicsT



namespace g3schema
{

namespace ns_clogic
{	

class CControlLogicsT : public TypeBase
{
public:
	g3schema_EXPORT CControlLogicsT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CControlLogicsT(CControlLogicsT const& init);
	void operator=(CControlLogicsT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_clogic_altova_CControlLogicsT); }
	MemberElement<ns_clogic::CControlLogicT, _altova_mi_ns_clogic_altova_CControlLogicsT_altova_clogic> clogic;
	struct clogic { typedef Iterator<ns_clogic::CControlLogicT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_clogic

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CControlLogicsT
