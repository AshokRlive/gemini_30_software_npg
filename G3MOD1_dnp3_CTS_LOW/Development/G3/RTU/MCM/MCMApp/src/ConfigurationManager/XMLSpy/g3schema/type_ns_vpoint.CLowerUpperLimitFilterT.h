#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CLowerUpperLimitFilterT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CLowerUpperLimitFilterT



namespace g3schema
{

namespace ns_vpoint
{	

class CLowerUpperLimitFilterT : public TypeBase
{
public:
	g3schema_EXPORT CLowerUpperLimitFilterT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CLowerUpperLimitFilterT(CLowerUpperLimitFilterT const& init);
	void operator=(CLowerUpperLimitFilterT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CLowerUpperLimitFilterT); }

	MemberAttribute<bool,_altova_mi_ns_vpoint_altova_CLowerUpperLimitFilterT_altova_upperEnabled, 0, 0> upperEnabled;	// upperEnabled Cboolean

	MemberAttribute<bool,_altova_mi_ns_vpoint_altova_CLowerUpperLimitFilterT_altova_lowerEnabled, 0, 0> lowerEnabled;	// lowerEnabled Cboolean

	MemberAttribute<double,_altova_mi_ns_vpoint_altova_CLowerUpperLimitFilterT_altova_upperHysteresis, 0, 0> upperHysteresis;	// upperHysteresis Cfloat

	MemberAttribute<double,_altova_mi_ns_vpoint_altova_CLowerUpperLimitFilterT_altova_lowerLimit, 0, 0> lowerLimit;	// lowerLimit Cfloat

	MemberAttribute<double,_altova_mi_ns_vpoint_altova_CLowerUpperLimitFilterT_altova_upperLimit, 0, 0> upperLimit;	// upperLimit Cfloat

	MemberAttribute<double,_altova_mi_ns_vpoint_altova_CLowerUpperLimitFilterT_altova_lowerHysteresis, 0, 0> lowerHysteresis;	// lowerHysteresis Cfloat
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CLowerUpperLimitFilterT
