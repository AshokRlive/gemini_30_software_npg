/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       XML based G3 general configuration parser
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15-Oct-2012   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#if !defined(EA_8CB13480_F5FE_44ff_B98B_9F0FCF582A42__INCLUDED_ABC)
#define EA_8CB13480_F5FE_44ff_B98B_9F0FCF582A42__INCLUDED_ABC

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

//#include <iostream>
//#include <string>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "IStatusManager.h"
#include "IIOModuleManager.h"
#include "ConfigurationManagerError.h"
#include "Logger.h"

#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>

#include "Altova/Altova.h"
#include "Altova/SchemaTypes.h"
#include "Altova/AltovaException.h"

#include "AltovaXML/XmlException.h"
#include "AltovaXML/Node.h"

#include "g3schema/g3schema.h"

using namespace g3schema;
using namespace altova;

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */


/**
 * \brief XML Parser for general configuration items
 *
 * Parses the XML Configuration file for general configuration items.
 */
class XMLNetworkConfigParser
{

public:
    /**
     * \brief Custom constructor
     *
     * \param Cg3schema XML configuration
     */
	XMLNetworkConfigParser(Cg3schema &configuration);
	virtual ~XMLNetworkConfigParser();

	CONFMGR_ERROR initParser();

private:
    Cg3schema &configuration;
    Logger& log;
};
#endif // !defined(EA_8CB13480_F5FE_44ff_B98B_9F0FCF582A42__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
