#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s101_ALTOVA_CS101ChannelT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s101_ALTOVA_CS101ChannelT



namespace g3schema
{

namespace ns_s101
{	

class CS101ChannelT : public TypeBase
{
public:
	g3schema_EXPORT CS101ChannelT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CS101ChannelT(CS101ChannelT const& init);
	void operator=(CS101ChannelT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_s101_altova_CS101ChannelT); }
	MemberElement<ns_s101::CS101ChlConfT, _altova_mi_ns_s101_altova_CS101ChannelT_altova_config> config;
	struct config { typedef Iterator<ns_s101::CS101ChlConfT> iterator; };
	MemberElement<ns_s101::CS101SessionT, _altova_mi_ns_s101_altova_CS101ChannelT_altova_session> session;
	struct session { typedef Iterator<ns_s101::CS101SessionT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_s101

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s101_ALTOVA_CS101ChannelT
