#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CVirtualPointsT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CVirtualPointsT



namespace g3schema
{

namespace ns_vpoint
{	

class CVirtualPointsT : public TypeBase
{
public:
	g3schema_EXPORT CVirtualPointsT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CVirtualPointsT(CVirtualPointsT const& init);
	void operator=(CVirtualPointsT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CVirtualPointsT); }
	MemberElement<ns_vpoint::CAnaloguesT, _altova_mi_ns_vpoint_altova_CVirtualPointsT_altova_analogues> analogues;
	struct analogues { typedef Iterator<ns_vpoint::CAnaloguesT> iterator; };
	MemberElement<ns_vpoint::CCountersT, _altova_mi_ns_vpoint_altova_CVirtualPointsT_altova_counters> counters;
	struct counters { typedef Iterator<ns_vpoint::CCountersT> iterator; };
	MemberElement<ns_vpoint::CDoubleBinariesT, _altova_mi_ns_vpoint_altova_CVirtualPointsT_altova_doubleBinaries> doubleBinaries;
	struct doubleBinaries { typedef Iterator<ns_vpoint::CDoubleBinariesT> iterator; };
	MemberElement<ns_vpoint::CBinariesT, _altova_mi_ns_vpoint_altova_CVirtualPointsT_altova_binaries> binaries;
	struct binaries { typedef Iterator<ns_vpoint::CBinariesT> iterator; };
	MemberElement<ns_vpoint::CCustomLabelsT, _altova_mi_ns_vpoint_altova_CVirtualPointsT_altova_customLabels> customLabels;
	struct customLabels { typedef Iterator<ns_vpoint::CCustomLabelsT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CVirtualPointsT
