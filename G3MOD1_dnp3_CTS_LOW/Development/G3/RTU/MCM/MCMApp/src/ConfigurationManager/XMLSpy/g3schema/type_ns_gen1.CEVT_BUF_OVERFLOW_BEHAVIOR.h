#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen1_ALTOVA_CEVT_BUF_OVERFLOW_BEHAVIOR
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen1_ALTOVA_CEVT_BUF_OVERFLOW_BEHAVIOR



namespace g3schema
{

namespace ns_gen1
{	

class CEVT_BUF_OVERFLOW_BEHAVIOR : public TypeBase
{
public:
	g3schema_EXPORT CEVT_BUF_OVERFLOW_BEHAVIOR(xercesc::DOMNode* const& init);
	g3schema_EXPORT CEVT_BUF_OVERFLOW_BEHAVIOR(CEVT_BUF_OVERFLOW_BEHAVIOR const& init);
	void operator=(CEVT_BUF_OVERFLOW_BEHAVIOR const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen1_altova_CEVT_BUF_OVERFLOW_BEHAVIOR); }

	enum EnumValues {
		Invalid = -1,
		k_EVT_BUF_OVERFLOW_BEHAVIOR_DEL_OLDEST = 0, // EVT_BUF_OVERFLOW_BEHAVIOR_DEL_OLDEST
		k_EVT_BUF_OVERFLOW_BEHAVIOR_DEL_LATEST = 1, // EVT_BUF_OVERFLOW_BEHAVIOR_DEL_LATEST
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen1

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen1_ALTOVA_CEVT_BUF_OVERFLOW_BEHAVIOR
