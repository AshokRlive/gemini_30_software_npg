#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_COverRangeT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_COverRangeT



namespace g3schema
{

namespace ns_vpoint
{	

class COverRangeT : public TypeBase
{
public:
	g3schema_EXPORT COverRangeT(xercesc::DOMNode* const& init);
	g3schema_EXPORT COverRangeT(COverRangeT const& init);
	void operator=(COverRangeT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_COverRangeT); }

	MemberAttribute<bool,_altova_mi_ns_vpoint_altova_COverRangeT_altova_overflowEnabled, 0, 0> overflowEnabled;	// overflowEnabled Cboolean

	MemberAttribute<double,_altova_mi_ns_vpoint_altova_COverRangeT_altova_overflow, 0, 0> overflow;	// overflow Cfloat

	MemberAttribute<double,_altova_mi_ns_vpoint_altova_COverRangeT_altova_overflowHysteresis, 0, 0> overflowHysteresis;	// overflowHysteresis Cfloat

	MemberAttribute<bool,_altova_mi_ns_vpoint_altova_COverRangeT_altova_underflowEnabled, 0, 0> underflowEnabled;	// underflowEnabled Cboolean

	MemberAttribute<double,_altova_mi_ns_vpoint_altova_COverRangeT_altova_underflow, 0, 0> underflow;	// underflow Cfloat

	MemberAttribute<double,_altova_mi_ns_vpoint_altova_COverRangeT_altova_underflowHysteresis, 0, 0> underflowHysteresis;	// underflowHysteresis Cfloat
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_COverRangeT
