#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CDigitalInputChnlT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CDigitalInputChnlT

#include "type_ns_g3module.CBaseChannelT.h"


namespace g3schema
{

namespace ns_g3module
{	

class CDigitalInputChnlT : public ::g3schema::ns_g3module::CBaseChannelT
{
public:
	g3schema_EXPORT CDigitalInputChnlT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CDigitalInputChnlT(CDigitalInputChnlT const& init);
	void operator=(CDigitalInputChnlT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_g3module_altova_CDigitalInputChnlT); }

	MemberAttribute<bool,_altova_mi_ns_g3module_altova_CDigitalInputChnlT_altova_extEquipInvert, 0, 0> extEquipInvert;	// extEquipInvert Cboolean

	MemberAttribute<bool,_altova_mi_ns_g3module_altova_CDigitalInputChnlT_altova_eventEnable, 0, 0> eventEnable;	// eventEnable Cboolean

	MemberAttribute<unsigned,_altova_mi_ns_g3module_altova_CDigitalInputChnlT_altova_dbHigh2LowMs, 0, 0> dbHigh2LowMs;	// dbHigh2LowMs CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_g3module_altova_CDigitalInputChnlT_altova_dbLow2HighMs, 0, 0> dbLow2HighMs;	// dbLow2HighMs CunsignedInt
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_g3module

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CDigitalInputChnlT
