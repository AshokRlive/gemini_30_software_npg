#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_comms_ALTOVA_CScriptFileT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_comms_ALTOVA_CScriptFileT



namespace g3schema
{

namespace ns_comms
{	

class CScriptFileT : public TypeBase
{
public:
	g3schema_EXPORT CScriptFileT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CScriptFileT(CScriptFileT const& init);
	void operator=(CScriptFileT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_comms_altova_CScriptFileT); }

	MemberAttribute<string_type,_altova_mi_ns_comms_altova_CScriptFileT_altova_filePath, 0, 0> filePath;	// filePath Cstring
	MemberElement<xs::CstringType, _altova_mi_ns_comms_altova_CScriptFileT_altova_content> content;
	struct content { typedef Iterator<xs::CstringType> iterator; };
	MemberElement<xs::CstringType, _altova_mi_ns_comms_altova_CScriptFileT_altova_template2> template2;
	struct template2 { typedef Iterator<xs::CstringType> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_comms

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_comms_ALTOVA_CScriptFileT
