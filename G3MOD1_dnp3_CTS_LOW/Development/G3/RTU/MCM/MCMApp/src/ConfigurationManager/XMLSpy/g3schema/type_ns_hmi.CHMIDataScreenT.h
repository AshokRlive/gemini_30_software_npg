#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CHMIDataScreenT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CHMIDataScreenT



namespace g3schema
{

namespace ns_hmi
{	

class CHMIDataScreenT : public TypeBase
{
public:
	g3schema_EXPORT CHMIDataScreenT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CHMIDataScreenT(CHMIDataScreenT const& init);
	void operator=(CHMIDataScreenT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_hmi_altova_CHMIDataScreenT); }

	MemberAttribute<int,_altova_mi_ns_hmi_altova_CHMIDataScreenT_altova_screenID, 0, 0> screenID;	// screenID Cint

	MemberAttribute<string_type,_altova_mi_ns_hmi_altova_CHMIDataScreenT_altova_screenTitle, 0, 0> screenTitle;	// screenTitle Cstring
	MemberElement<ns_hmi::CHMIDataEntryRef, _altova_mi_ns_hmi_altova_CHMIDataScreenT_altova_entries> entries;
	struct entries { typedef Iterator<ns_hmi::CHMIDataEntryRef> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_hmi

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CHMIDataScreenT
