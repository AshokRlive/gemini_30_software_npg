#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s104_ALTOVA_CS104LinuxIoT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s104_ALTOVA_CS104LinuxIoT



namespace g3schema
{

namespace ns_s104
{	

class CS104LinuxIoT : public TypeBase
{
public:
	g3schema_EXPORT CS104LinuxIoT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CS104LinuxIoT(CS104LinuxIoT const& init);
	void operator=(CS104LinuxIoT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_s104_altova_CS104LinuxIoT); }
	MemberElement<ns_s104::CS104ChnlTCPConfT, _altova_mi_ns_s104_altova_CS104LinuxIoT_altova_tcp> tcp;
	struct tcp { typedef Iterator<ns_s104::CS104ChnlTCPConfT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_s104

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s104_ALTOVA_CS104LinuxIoT
