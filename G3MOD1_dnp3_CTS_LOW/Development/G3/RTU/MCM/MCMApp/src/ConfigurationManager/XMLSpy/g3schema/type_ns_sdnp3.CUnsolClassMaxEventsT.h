#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CUnsolClassMaxEventsT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CUnsolClassMaxEventsT



namespace g3schema
{

namespace ns_sdnp3
{	

class CUnsolClassMaxEventsT : public TypeBase
{
public:
	g3schema_EXPORT CUnsolClassMaxEventsT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CUnsolClassMaxEventsT(CUnsolClassMaxEventsT const& init);
	void operator=(CUnsolClassMaxEventsT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_sdnp3_altova_CUnsolClassMaxEventsT); }

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CUnsolClassMaxEventsT_altova_class1, 0, 0> class1;	// class1 CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CUnsolClassMaxEventsT_altova_class2, 0, 0> class2;	// class2 CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CUnsolClassMaxEventsT_altova_class3, 0, 0> class3;	// class3 CunsignedInt
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_sdnp3

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CUnsolClassMaxEventsT
