#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CDoubleBinaryValueMapT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CDoubleBinaryValueMapT



namespace g3schema
{

namespace ns_vpoint
{	

class CDoubleBinaryValueMapT : public TypeBase
{
public:
	g3schema_EXPORT CDoubleBinaryValueMapT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CDoubleBinaryValueMapT(CDoubleBinaryValueMapT const& init);
	void operator=(CDoubleBinaryValueMapT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CDoubleBinaryValueMapT); }

	MemberAttribute<int,_altova_mi_ns_vpoint_altova_CDoubleBinaryValueMapT_altova_value00, 0, 0> value00;	// value00 Cint

	MemberAttribute<int,_altova_mi_ns_vpoint_altova_CDoubleBinaryValueMapT_altova_value01, 0, 0> value01;	// value01 Cint

	MemberAttribute<int,_altova_mi_ns_vpoint_altova_CDoubleBinaryValueMapT_altova_value10, 0, 0> value10;	// value10 Cint

	MemberAttribute<int,_altova_mi_ns_vpoint_altova_CDoubleBinaryValueMapT_altova_value11, 0, 0> value11;	// value11 Cint
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CDoubleBinaryValueMapT
