#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3T
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3T



namespace g3schema
{

namespace ns_sdnp3
{	

class CSDNP3T : public TypeBase
{
public:
	g3schema_EXPORT CSDNP3T(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSDNP3T(CSDNP3T const& init);
	void operator=(CSDNP3T const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_sdnp3_altova_CSDNP3T); }
	MemberElement<ns_sdnp3::CSDNP3ChnlT, _altova_mi_ns_sdnp3_altova_CSDNP3T_altova_channel> channel;
	struct channel { typedef Iterator<ns_sdnp3::CSDNP3ChnlT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_sdnp3

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3T
