#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s104_ALTOVA_CRedundancyChnlT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s104_ALTOVA_CRedundancyChnlT



namespace g3schema
{

namespace ns_s104
{	

class CRedundancyChnlT : public TypeBase
{
public:
	g3schema_EXPORT CRedundancyChnlT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CRedundancyChnlT(CRedundancyChnlT const& init);
	void operator=(CRedundancyChnlT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_s104_altova_CRedundancyChnlT); }

	MemberAttribute<string_type,_altova_mi_ns_s104_altova_CRedundancyChnlT_altova_ipAddress, 0, 0> ipAddress;	// ipAddress Cstring

	MemberAttribute<unsigned,_altova_mi_ns_s104_altova_CRedundancyChnlT_altova_ipPort, 0, 0> ipPort;	// ipPort CunsignedInt
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_s104

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s104_ALTOVA_CRedundancyChnlT
