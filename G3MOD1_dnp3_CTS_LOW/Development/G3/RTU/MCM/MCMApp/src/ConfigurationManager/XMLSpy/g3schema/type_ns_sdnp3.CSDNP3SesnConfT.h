#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3SesnConfT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3SesnConfT

#include "type_ns_pstack.CSessionConfT.h"


namespace g3schema
{

namespace ns_sdnp3
{	

class CSDNP3SesnConfT : public ::g3schema::ns_pstack::CSessionConfT
{
public:
	g3schema_EXPORT CSDNP3SesnConfT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSDNP3SesnConfT(CSDNP3SesnConfT const& init);
	void operator=(CSDNP3SesnConfT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_sdnp3_altova_CSDNP3SesnConfT); }

	MemberAttribute<bool,_altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_selfAddress, 0, 0> selfAddress;	// selfAddress Cboolean

	MemberAttribute<bool,_altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_allowControlsOnSelfAddress, 0, 0> allowControlsOnSelfAddress;	// allowControlsOnSelfAddress Cboolean

	MemberAttribute<bool,_altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_validateSourceAddress, 0, 0> validateSourceAddress;	// validateSourceAddress Cboolean

	MemberAttribute<bool,_altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_multiFragRespAllowed, 0, 0> multiFragRespAllowed;	// multiFragRespAllowed Cboolean

	MemberAttribute<bool,_altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_multiFragConfirm, 0, 0> multiFragConfirm;	// multiFragConfirm Cboolean

	MemberAttribute<bool,_altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_respondNeedTime, 0, 0> respondNeedTime;	// respondNeedTime Cboolean

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_applConfirmTimeout, 0, 0> applConfirmTimeout;	// applConfirmTimeout CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_selectTimeout, 0, 0> selectTimeout;	// selectTimeout CunsignedInt

	MemberAttribute<bool,_altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_unsolAllowed, 0, 0> unsolAllowed;	// unsolAllowed Cboolean

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_unsolConfirmTimeout, 0, 0> unsolConfirmTimeout;	// unsolConfirmTimeout CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_unsolClassMask, 0, 0> unsolClassMask;	// unsolClassMask CunsignedByte

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_unsolMaxRetries, 0, 0> unsolMaxRetries;	// unsolMaxRetries CunsignedByte

	MemberAttribute<int,_altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_unsolRetryMode, 0, 0> unsolRetryMode;	// unsolRetryMode Cbyte

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_unsolRetryDelay, 0, 0> unsolRetryDelay;	// unsolRetryDelay CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_unsolOfflineRetryDelay, 0, 0> unsolOfflineRetryDelay;	// unsolOfflineRetryDelay CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_linkStatusPeriod, 0, 0> linkStatusPeriod;	// linkStatusPeriod CunsignedInt

	MemberAttribute<int,_altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_evtBufOverflowBehaviour, 0, 0> evtBufOverflowBehaviour;	// evtBufOverflowBehaviour Cbyte

	MemberAttribute<bool,_altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_identicalRetriesAllowed, 0, 0> identicalRetriesAllowed;	// identicalRetriesAllowed Cboolean
	MemberElement<ns_sdnp3::CDNP3SesnAddressT, _altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_address> address;
	struct address { typedef Iterator<ns_sdnp3::CDNP3SesnAddressT> iterator; };
	MemberElement<ns_sdnp3::CUnsolClassMaxEventsT, _altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_unsolClassMaxEvents> unsolClassMaxEvents;
	struct unsolClassMaxEvents { typedef Iterator<ns_sdnp3::CUnsolClassMaxEventsT> iterator; };
	MemberElement<ns_sdnp3::CUnsolClassMaxDelaysT, _altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_unsolClassMaxDelays> unsolClassMaxDelays;
	struct unsolClassMaxDelays { typedef Iterator<ns_sdnp3::CUnsolClassMaxDelaysT> iterator; };
	MemberElement<ns_sdnp3::CObjDefaultVariationsT, _altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_objDefaultVariations> objDefaultVariations;
	struct objDefaultVariations { typedef Iterator<ns_sdnp3::CObjDefaultVariationsT> iterator; };
	MemberElement<ns_pstack::CSesnEventConfT, _altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_doubleInputEvent> doubleInputEvent;
	struct doubleInputEvent { typedef Iterator<ns_pstack::CSesnEventConfT> iterator; };
	MemberElement<ns_pstack::CSesnEventConfT, _altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_analogInputEvent> analogInputEvent;
	struct analogInputEvent { typedef Iterator<ns_pstack::CSesnEventConfT> iterator; };
	MemberElement<ns_pstack::CSesnEventConfT, _altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_binaryInputEvent> binaryInputEvent;
	struct binaryInputEvent { typedef Iterator<ns_pstack::CSesnEventConfT> iterator; };
	MemberElement<ns_pstack::CSesnEventConfT, _altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_counterEvent> counterEvent;
	struct counterEvent { typedef Iterator<ns_pstack::CSesnEventConfT> iterator; };
	MemberElement<ns_pstack::CSesnEventConfT, _altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_frozenCounterEvent> frozenCounterEvent;
	struct frozenCounterEvent { typedef Iterator<ns_pstack::CSesnEventConfT> iterator; };
	MemberElement<ns_pstack::CSesnEventConfT, _altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_analogOutputEvent> analogOutputEvent;
	struct analogOutputEvent { typedef Iterator<ns_pstack::CSesnEventConfT> iterator; };
	MemberElement<ns_sdnp3::CAuthenticationT, _altova_mi_ns_sdnp3_altova_CSDNP3SesnConfT_altova_authentication> authentication;
	struct authentication { typedef Iterator<ns_sdnp3::CAuthenticationT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_sdnp3

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3SesnConfT
