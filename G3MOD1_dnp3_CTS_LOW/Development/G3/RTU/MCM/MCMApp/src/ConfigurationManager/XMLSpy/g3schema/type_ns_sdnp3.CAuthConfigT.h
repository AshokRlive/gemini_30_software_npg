#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CAuthConfigT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CAuthConfigT



namespace g3schema
{

namespace ns_sdnp3
{	

class CAuthConfigT : public TypeBase
{
public:
	g3schema_EXPORT CAuthConfigT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CAuthConfigT(CAuthConfigT const& init);
	void operator=(CAuthConfigT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_sdnp3_altova_CAuthConfigT); }

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CAuthConfigT_altova_assocId, 0, 0> assocId;	// assocId CunsignedShort

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CAuthConfigT_altova_MACAlgorithm, 0, 0> MACAlgorithm;	// MACAlgorithm CunsignedByte

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CAuthConfigT_altova_maxApplTimeoutCount, 0, 0> maxApplTimeoutCount;	// maxApplTimeoutCount CunsignedByte

	MemberAttribute<__int64,_altova_mi_ns_sdnp3_altova_CAuthConfigT_altova_replyTimeout, 0, 0> replyTimeout;	// replyTimeout Clong

	MemberAttribute<__int64,_altova_mi_ns_sdnp3_altova_CAuthConfigT_altova_keyChangeInterval, 0, 0> keyChangeInterval;	// keyChangeInterval Clong

	MemberAttribute<__int64,_altova_mi_ns_sdnp3_altova_CAuthConfigT_altova_maxKeyChangeCount, 0, 0> maxKeyChangeCount;	// maxKeyChangeCount Clong

	MemberAttribute<bool,_altova_mi_ns_sdnp3_altova_CAuthConfigT_altova_aggressiveModeSupport, 0, 0> aggressiveModeSupport;	// aggressiveModeSupport Cboolean

	MemberAttribute<bool,_altova_mi_ns_sdnp3_altova_CAuthConfigT_altova_disallowSHA1, 0, 0> disallowSHA1;	// disallowSHA1 Cboolean

	MemberAttribute<bool,_altova_mi_ns_sdnp3_altova_CAuthConfigT_altova_extraDiags, 0, 0> extraDiags;	// extraDiags Cboolean

	MemberAttribute<bool,_altova_mi_ns_sdnp3_altova_CAuthConfigT_altova_operateInV2Mode, 0, 0> operateInV2Mode;	// operateInV2Mode Cboolean
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_sdnp3

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CAuthConfigT
