#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen0_ALTOVA_CMODULE_ID
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen0_ALTOVA_CMODULE_ID



namespace g3schema
{

namespace ns_gen0
{	

class CMODULE_ID : public TypeBase
{
public:
	g3schema_EXPORT CMODULE_ID(xercesc::DOMNode* const& init);
	g3schema_EXPORT CMODULE_ID(CMODULE_ID const& init);
	void operator=(CMODULE_ID const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen0_altova_CMODULE_ID); }

	enum EnumValues {
		Invalid = -1,
		k_MODULE_ID_0 = 0, // MODULE_ID_0
		k_MODULE_ID_1 = 1, // MODULE_ID_1
		k_MODULE_ID_2 = 2, // MODULE_ID_2
		k_MODULE_ID_3 = 3, // MODULE_ID_3
		k_MODULE_ID_4 = 4, // MODULE_ID_4
		k_MODULE_ID_5 = 5, // MODULE_ID_5
		k_MODULE_ID_6 = 6, // MODULE_ID_6
		k_MODULE_ID_BRD = 7, // MODULE_ID_BRD
		k_MODULE_ID_7_ = 8, // MODULE_ID_7 
		k_MODULE_ID_8_ = 9, // MODULE_ID_8 
		k_MODULE_ID_9_ = 10, // MODULE_ID_9 
		k_MODULE_ID_10 = 11, // MODULE_ID_10
		k_MODULE_ID_11 = 12, // MODULE_ID_11
		k_MODULE_ID_12 = 13, // MODULE_ID_12
		k_MODULE_ID_13 = 14, // MODULE_ID_13
		k_MODULE_ID_14 = 15, // MODULE_ID_14
		k_MODULE_ID_15 = 16, // MODULE_ID_15
		k_MODULE_ID_16 = 17, // MODULE_ID_16
		k_MODULE_ID_17 = 18, // MODULE_ID_17
		k_MODULE_ID_18 = 19, // MODULE_ID_18
		k_MODULE_ID_19 = 20, // MODULE_ID_19
		k_MODULE_ID_20 = 21, // MODULE_ID_20
		k_MODULE_ID_21 = 22, // MODULE_ID_21
		k_MODULE_ID_22 = 23, // MODULE_ID_22
		k_MODULE_ID_23 = 24, // MODULE_ID_23
		k_MODULE_ID_24 = 25, // MODULE_ID_24
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen0

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen0_ALTOVA_CMODULE_ID
