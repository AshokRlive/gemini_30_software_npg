#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3CommsDeviceConfT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3CommsDeviceConfT



namespace g3schema
{

namespace ns_sdnp3
{	

class CSDNP3CommsDeviceConfT : public TypeBase
{
public:
	g3schema_EXPORT CSDNP3CommsDeviceConfT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSDNP3CommsDeviceConfT(CSDNP3CommsDeviceConfT const& init);
	void operator=(CSDNP3CommsDeviceConfT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_sdnp3_altova_CSDNP3CommsDeviceConfT); }

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CSDNP3CommsDeviceConfT_altova_commsDeviceID, 0, 0> commsDeviceID;	// commsDeviceID CunsignedInt

	MemberAttribute<int,_altova_mi_ns_sdnp3_altova_CSDNP3CommsDeviceConfT_altova_connectionClass, 0, 0> connectionClass;	// connectionClass Cbyte
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_sdnp3

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3CommsDeviceConfT
