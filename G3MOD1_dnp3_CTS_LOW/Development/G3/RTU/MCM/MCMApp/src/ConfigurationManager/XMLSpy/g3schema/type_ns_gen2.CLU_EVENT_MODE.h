#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen2_ALTOVA_CLU_EVENT_MODE
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen2_ALTOVA_CLU_EVENT_MODE



namespace g3schema
{

namespace ns_gen2
{	

class CLU_EVENT_MODE : public TypeBase
{
public:
	g3schema_EXPORT CLU_EVENT_MODE(xercesc::DOMNode* const& init);
	g3schema_EXPORT CLU_EVENT_MODE(CLU_EVENT_MODE const& init);
	void operator=(CLU_EVENT_MODE const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen2_altova_CLU_EVENT_MODE); }

	enum EnumValues {
		Invalid = -1,
		k_LU_EVENT_MODE_SOE = 0, // LU_EVENT_MODE_SOE
		k_LU_EVENT_MODE_MOST_RECENT = 1, // LU_EVENT_MODE_MOST_RECENT
		k_LU_EVENT_MODE_CURRENT = 2, // LU_EVENT_MODE_CURRENT
		k_LU_EVENT_MODE_PER_POINT = 3, // LU_EVENT_MODE_PER_POINT
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen2

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen2_ALTOVA_CLU_EVENT_MODE
