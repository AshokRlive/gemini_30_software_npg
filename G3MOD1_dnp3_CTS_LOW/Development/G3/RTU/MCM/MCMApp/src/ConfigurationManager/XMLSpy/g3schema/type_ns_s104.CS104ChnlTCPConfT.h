#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s104_ALTOVA_CS104ChnlTCPConfT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s104_ALTOVA_CS104ChnlTCPConfT

#include "type_ns_pstack.CTCPConfT.h"


namespace g3schema
{

namespace ns_s104
{	

class CS104ChnlTCPConfT : public ::g3schema::ns_pstack::CTCPConfT
{
public:
	g3schema_EXPORT CS104ChnlTCPConfT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CS104ChnlTCPConfT(CS104ChnlTCPConfT const& init);
	void operator=(CS104ChnlTCPConfT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_s104_altova_CS104ChnlTCPConfT); }

	MemberAttribute<bool,_altova_mi_ns_s104_altova_CS104ChnlTCPConfT_altova_disconnectOnNewSyn, 0, 0> disconnectOnNewSyn;	// disconnectOnNewSyn Cboolean
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_s104

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s104_ALTOVA_CS104ChnlTCPConfT
