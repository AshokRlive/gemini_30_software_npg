#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CIpConnectivityCheckT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CIpConnectivityCheckT



namespace g3schema
{

namespace ns_sdnp3
{	

class CIpConnectivityCheckT : public TypeBase
{
public:
	g3schema_EXPORT CIpConnectivityCheckT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CIpConnectivityCheckT(CIpConnectivityCheckT const& init);
	void operator=(CIpConnectivityCheckT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_sdnp3_altova_CIpConnectivityCheckT); }

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CIpConnectivityCheckT_altova_periodMins, 0, 0> periodMins;	// periodMins CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CIpConnectivityCheckT_altova_port, 0, 0> port;	// port CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CIpConnectivityCheckT_altova_retries, 0, 0> retries;	// retries CunsignedInt
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_sdnp3

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CIpConnectivityCheckT
