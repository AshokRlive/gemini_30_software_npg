#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3PointsT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3PointsT



namespace g3schema
{

namespace ns_sdnp3
{	

class CSDNP3PointsT : public TypeBase
{
public:
	g3schema_EXPORT CSDNP3PointsT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSDNP3PointsT(CSDNP3PointsT const& init);
	void operator=(CSDNP3PointsT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_sdnp3_altova_CSDNP3PointsT); }
	MemberElement<ns_sdnp3::CSDNP3InputPointT, _altova_mi_ns_sdnp3_altova_CSDNP3PointsT_altova_analogInput> analogInput;
	struct analogInput { typedef Iterator<ns_sdnp3::CSDNP3InputPointT> iterator; };
	MemberElement<ns_sdnp3::CSDNP3InputPointT, _altova_mi_ns_sdnp3_altova_CSDNP3PointsT_altova_binaryInput> binaryInput;
	struct binaryInput { typedef Iterator<ns_sdnp3::CSDNP3InputPointT> iterator; };
	MemberElement<ns_sdnp3::CSDNP3InputPointT, _altova_mi_ns_sdnp3_altova_CSDNP3PointsT_altova_doubleBinaryInput> doubleBinaryInput;
	struct doubleBinaryInput { typedef Iterator<ns_sdnp3::CSDNP3InputPointT> iterator; };
	MemberElement<ns_sdnp3::CSDNP3OutputPointT, _altova_mi_ns_sdnp3_altova_CSDNP3PointsT_altova_binaryOutput> binaryOutput;
	struct binaryOutput { typedef Iterator<ns_sdnp3::CSDNP3OutputPointT> iterator; };
	MemberElement<ns_sdnp3::CSDNP3CounterPointT, _altova_mi_ns_sdnp3_altova_CSDNP3PointsT_altova_counter> counter;
	struct counter { typedef Iterator<ns_sdnp3::CSDNP3CounterPointT> iterator; };
	MemberElement<ns_sdnp3::CSDNP3OutputPointT, _altova_mi_ns_sdnp3_altova_CSDNP3PointsT_altova_analogueOutput> analogueOutput;
	struct analogueOutput { typedef Iterator<ns_sdnp3::CSDNP3OutputPointT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_sdnp3

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3PointsT
