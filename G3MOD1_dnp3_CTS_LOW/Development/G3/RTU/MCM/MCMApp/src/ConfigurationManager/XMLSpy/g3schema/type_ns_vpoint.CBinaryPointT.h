#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CBinaryPointT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CBinaryPointT

#include "type_ns_vpoint.CBaseBPointT.h"


namespace g3schema
{

namespace ns_vpoint
{	

class CBinaryPointT : public ::g3schema::ns_vpoint::CBaseBPointT
{
public:
	g3schema_EXPORT CBinaryPointT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CBinaryPointT(CBinaryPointT const& init);
	void operator=(CBinaryPointT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CBinaryPointT); }
	MemberElement<ns_common::CChannelRefT, _altova_mi_ns_vpoint_altova_CBinaryPointT_altova_channel> channel;
	struct channel { typedef Iterator<ns_common::CChannelRefT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CBinaryPointT
