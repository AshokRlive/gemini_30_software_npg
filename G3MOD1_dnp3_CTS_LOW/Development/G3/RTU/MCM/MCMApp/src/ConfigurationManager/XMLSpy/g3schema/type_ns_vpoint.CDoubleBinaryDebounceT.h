#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CDoubleBinaryDebounceT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CDoubleBinaryDebounceT



namespace g3schema
{

namespace ns_vpoint
{	

class CDoubleBinaryDebounceT : public TypeBase
{
public:
	g3schema_EXPORT CDoubleBinaryDebounceT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CDoubleBinaryDebounceT(CDoubleBinaryDebounceT const& init);
	void operator=(CDoubleBinaryDebounceT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CDoubleBinaryDebounceT); }
	MemberElement<ns_vpoint::CDefaultDebounceT, _altova_mi_ns_vpoint_altova_CDoubleBinaryDebounceT_altova_default2> default2;
	struct default2 { typedef Iterator<ns_vpoint::CDefaultDebounceT> iterator; };
	MemberElement<ns_vpoint::CCustomDebounceT, _altova_mi_ns_vpoint_altova_CDoubleBinaryDebounceT_altova_custom> custom;
	struct custom { typedef Iterator<ns_vpoint::CCustomDebounceT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CDoubleBinaryDebounceT
