/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       HMI XML factory interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   01/03/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_574B5A1F_FC96_49fb_971A_FD001657F172__INCLUDED_)
#define EA_574B5A1F_FC96_49fb_971A_FD001657F172__INCLUDED_


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "Logger.h"
#include "Screen.h"
#include "DataEntry.h"
#include "ConfigurationManagerError.h"
#include "IIOModuleManager.h"
#include "IStatusManager.h"
#include "GeminiDatabase.h"

#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>

#include "Altova/Altova.h"
#include "Altova/SchemaTypes.h"
#include "Altova/AltovaException.h"

#include "AltovaXML/XmlException.h"
#include "AltovaXML/Node.h"

#include "g3schema/g3schema.h"

using namespace g3schema;
using namespace altova;
using namespace ns_hmi;

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class XMLHMIFactory : public IHMIFactory
{

public:
    /**
     * \brief Custom constructor
     *
     * \param configuration Configuration file parser handler
     *
     * \return none
     */
    XMLHMIFactory(Cg3schema& configuration,
                  GeminiDatabase& database,
                  IIOModuleManager& moduleMgr,
                  IStatusManager& statusMgr
                  );
    virtual ~XMLHMIFactory();

    /**
     * \brief Get the root screen
     *
     * All the sub screens are also created
     *
     * \return Pointer to the root screen.
     */
    virtual Screen* getScreen( ScreenManager &mediator,
                               lu_uint8_t rows,
                               lu_uint8_t columns
                             );

    virtual Screen* getLogonScreen( ScreenManager &mediator,
                                  lu_uint8_t rows,
                                  lu_uint8_t columns
                                );

private:
    Cg3schema& configuration;
    GeminiDatabase& database;
    IIOModuleManager& moduleManager;
    IStatusManager& statusManager;
    lu_uint64_t actPressDelay_ms; /* Time required to keep pressed ACT + OPEN/CLOSE buttons to start an action */
    Logger& log;

private:
    Screen *getScreen( CScreensT &screens,
                       lu_uint16_t id,
                       ScreenManager &mediator,
                       lu_uint8_t rows,
                       lu_uint8_t columns
                     );

    Screen *createMenuScreen( CScreensT &screens,
                              CHMIMenuScreenT &config,
                              ScreenManager &mediator,
                              lu_uint8_t rows,
                              lu_uint8_t columns
                            );

    Screen *createControlScreen( CHMIControlScreenT &config,
                               ScreenManager &mediator,
                               lu_uint8_t rows,
                               lu_uint8_t columns
                             );

    Screen *createPhaseScreen( CHMIPhaseScreenT &config,
                               ScreenManager &mediator,
                               lu_uint8_t rows,
                               lu_uint8_t columns
                             );

    Screen *createDataScreen( CHMIDataScreenT &config,
                              CScreensT &entries,
                              ScreenManager &mediator,
                              lu_uint8_t rows,
                              lu_uint8_t columns
                            );

    Screen*createInfoScreen( CHMIInfoScreenT &config,
                              ScreenManager &mediator,
                              lu_uint8_t rows,
                              lu_uint8_t columns
                             );

    DataEntry *getDataEntry(CScreensT &entries, lu_uint16_t id);
};

#endif // !defined(EA_574B5A1F_FC96_49fb_971A_FD001657F172__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
