#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CAuthenticationT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CAuthenticationT



namespace g3schema
{

namespace ns_sdnp3
{	

class CAuthenticationT : public TypeBase
{
public:
	g3schema_EXPORT CAuthenticationT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CAuthenticationT(CAuthenticationT const& init);
	void operator=(CAuthenticationT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_sdnp3_altova_CAuthenticationT); }

	MemberAttribute<bool,_altova_mi_ns_sdnp3_altova_CAuthenticationT_altova_authenticationEnabled, 0, 0> authenticationEnabled;	// authenticationEnabled Cboolean

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CAuthenticationT_altova_authSecStatMaxEvents, 0, 0> authSecStatMaxEvents;	// authSecStatMaxEvents CunsignedShort
	MemberAttribute<string_type,_altova_mi_ns_sdnp3_altova_CAuthenticationT_altova_authSecStatEventMode, 0, 4> authSecStatEventMode;	// authSecStatEventMode CLU_EVENT_MODE
	MemberElement<ns_sdnp3::CAuthConfigT, _altova_mi_ns_sdnp3_altova_CAuthenticationT_altova_authConfig> authConfig;
	struct authConfig { typedef Iterator<ns_sdnp3::CAuthConfigT> iterator; };
	MemberElement<ns_sdnp3::CAuthConfigV2T, _altova_mi_ns_sdnp3_altova_CAuthenticationT_altova_authConfigV2> authConfigV2;
	struct authConfigV2 { typedef Iterator<ns_sdnp3::CAuthConfigV2T> iterator; };
	MemberElement<ns_sdnp3::CAuthConfigV5T, _altova_mi_ns_sdnp3_altova_CAuthenticationT_altova_authConfigV5> authConfigV5;
	struct authConfigV5 { typedef Iterator<ns_sdnp3::CAuthConfigV5T> iterator; };
	MemberElement<ns_sdnp3::CAuthUsersT, _altova_mi_ns_sdnp3_altova_CAuthenticationT_altova_users> users;
	struct users { typedef Iterator<ns_sdnp3::CAuthUsersT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_sdnp3

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CAuthenticationT
