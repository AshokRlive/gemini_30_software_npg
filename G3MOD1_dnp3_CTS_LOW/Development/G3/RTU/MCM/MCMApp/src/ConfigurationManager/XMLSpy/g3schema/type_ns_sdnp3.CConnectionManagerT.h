#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CConnectionManagerT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CConnectionManagerT



namespace g3schema
{

namespace ns_sdnp3
{	

class CConnectionManagerT : public TypeBase
{
public:
	g3schema_EXPORT CConnectionManagerT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CConnectionManagerT(CConnectionManagerT const& init);
	void operator=(CConnectionManagerT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_sdnp3_altova_CConnectionManagerT); }

	MemberAttribute<int,_altova_mi_ns_sdnp3_altova_CConnectionManagerT_altova_connectionClass, 0, 0> connectionClass;	// connectionClass Cbyte

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CConnectionManagerT_altova_inactivityTimeoutSecs, 0, 0> inactivityTimeoutSecs;	// inactivityTimeoutSecs CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CConnectionManagerT_altova_connectRetryDelaySecs, 0, 0> connectRetryDelaySecs;	// connectRetryDelaySecs CunsignedInt

	MemberAttribute<bool,_altova_mi_ns_sdnp3_altova_CConnectionManagerT_altova_enabled, 0, 0> enabled;	// enabled Cboolean

	MemberAttribute<int,_altova_mi_ns_sdnp3_altova_CConnectionManagerT_altova_commsFailureOutputLogicGroup, 0, 0> commsFailureOutputLogicGroup;	// commsFailureOutputLogicGroup Cint
	MemberElement<ns_sdnp3::CFailoverGroupT, _altova_mi_ns_sdnp3_altova_CConnectionManagerT_altova_failoverGroup> failoverGroup;
	struct failoverGroup { typedef Iterator<ns_sdnp3::CFailoverGroupT> iterator; };
	MemberElement<ns_sdnp3::CCommsPowerCycleT, _altova_mi_ns_sdnp3_altova_CConnectionManagerT_altova_commsPowerCycle> commsPowerCycle;
	struct commsPowerCycle { typedef Iterator<ns_sdnp3::CCommsPowerCycleT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_sdnp3

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CConnectionManagerT
