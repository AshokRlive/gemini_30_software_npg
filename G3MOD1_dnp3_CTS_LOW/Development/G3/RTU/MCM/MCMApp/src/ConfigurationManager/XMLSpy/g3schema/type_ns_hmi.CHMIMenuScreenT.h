#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CHMIMenuScreenT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CHMIMenuScreenT



namespace g3schema
{

namespace ns_hmi
{	

class CHMIMenuScreenT : public TypeBase
{
public:
	g3schema_EXPORT CHMIMenuScreenT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CHMIMenuScreenT(CHMIMenuScreenT const& init);
	void operator=(CHMIMenuScreenT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_hmi_altova_CHMIMenuScreenT); }

	MemberAttribute<int,_altova_mi_ns_hmi_altova_CHMIMenuScreenT_altova_screenID, 0, 0> screenID;	// screenID Cint

	MemberAttribute<string_type,_altova_mi_ns_hmi_altova_CHMIMenuScreenT_altova_screenTitle, 0, 0> screenTitle;	// screenTitle Cstring
	MemberElement<ns_hmi::CHMIScreenRef, _altova_mi_ns_hmi_altova_CHMIMenuScreenT_altova_childList> childList;
	struct childList { typedef Iterator<ns_hmi::CHMIScreenRef> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_hmi

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CHMIMenuScreenT
