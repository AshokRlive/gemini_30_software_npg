#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s104_ALTOVA_CRedundancyT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s104_ALTOVA_CRedundancyT



namespace g3schema
{

namespace ns_s104
{	

class CRedundancyT : public TypeBase
{
public:
	g3schema_EXPORT CRedundancyT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CRedundancyT(CRedundancyT const& init);
	void operator=(CRedundancyT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_s104_altova_CRedundancyT); }

	MemberAttribute<bool,_altova_mi_ns_s104_altova_CRedundancyT_altova_enabled, 0, 0> enabled;	// enabled Cboolean
	MemberElement<ns_s104::CRedundancyChnlT, _altova_mi_ns_s104_altova_CRedundancyT_altova_channel> channel;
	struct channel { typedef Iterator<ns_s104::CRedundancyChnlT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_s104

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s104_ALTOVA_CRedundancyT
