#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_iec870_ALTOVA_CIEC870InputPointT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_iec870_ALTOVA_CIEC870InputPointT

#include "type_ns_pstack.CPointT.h"


namespace g3schema
{

namespace ns_iec870
{	

class CIEC870InputPointT : public ::g3schema::ns_pstack::CPointT
{
public:
	g3schema_EXPORT CIEC870InputPointT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CIEC870InputPointT(CIEC870InputPointT const& init);
	void operator=(CIEC870InputPointT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_iec870_altova_CIEC870InputPointT); }

	MemberAttribute<bool,_altova_mi_ns_iec870_altova_CIEC870InputPointT_altova_enableCyclic, 0, 0> enableCyclic;	// enableCyclic Cboolean

	MemberAttribute<bool,_altova_mi_ns_iec870_altova_CIEC870InputPointT_altova_enableBackground, 0, 0> enableBackground;	// enableBackground Cboolean

	MemberAttribute<bool,_altova_mi_ns_iec870_altova_CIEC870InputPointT_altova_enableGeneralInterrogation, 0, 0> enableGeneralInterrogation;	// enableGeneralInterrogation Cboolean

	MemberAttribute<unsigned,_altova_mi_ns_iec870_altova_CIEC870InputPointT_altova_groupMask, 0, 0> groupMask;	// groupMask CunsignedInt

	MemberAttribute<bool,_altova_mi_ns_iec870_altova_CIEC870InputPointT_altova_eventOnlyWhenConnected, 0, 0> eventOnlyWhenConnected;	// eventOnlyWhenConnected Cboolean
	MemberAttribute<string_type,_altova_mi_ns_iec870_altova_CIEC870InputPointT_altova_timeformat, 0, 3> timeformat;	// timeformat CLU_TIME_FORMAT
	MemberAttribute<string_type,_altova_mi_ns_iec870_altova_CIEC870InputPointT_altova_ioaDisplayPattern, 0, 4> ioaDisplayPattern;	// ioaDisplayPattern CLU_IOA_FORMAT

	MemberAttribute<bool,_altova_mi_ns_iec870_altova_CIEC870InputPointT_altova_spontaneousEvent, 0, 0> spontaneousEvent;	// spontaneousEvent Cboolean
	MemberElement<ns_common::CVirtualPointRefT, _altova_mi_ns_iec870_altova_CIEC870InputPointT_altova_vpoint> vpoint;
	struct vpoint { typedef Iterator<ns_common::CVirtualPointRefT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_iec870

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_iec870_ALTOVA_CIEC870InputPointT
