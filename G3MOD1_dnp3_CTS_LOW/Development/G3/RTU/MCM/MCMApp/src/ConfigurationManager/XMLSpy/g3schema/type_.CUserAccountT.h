#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CUserAccountT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CUserAccountT



namespace g3schema
{

class CUserAccountT : public TypeBase
{
public:
	g3schema_EXPORT CUserAccountT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CUserAccountT(CUserAccountT const& init);
	void operator=(CUserAccountT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_altova_CUserAccountT); }

	MemberAttribute<string_type,_altova_mi_altova_CUserAccountT_altova_name, 0, 0> name;	// name Cstring

	MemberAttribute<string_type,_altova_mi_altova_CUserAccountT_altova_password, 0, 0> password;	// password Cstring

	MemberAttribute<unsigned,_altova_mi_altova_CUserAccountT_altova_group, 0, 0> group;	// group CunsignedByte

	MemberAttribute<string_type,_altova_mi_altova_CUserAccountT_altova_mdAlgorithm, 0, 0> mdAlgorithm;	// mdAlgorithm Cstring

	MemberAttribute<string_type,_altova_mi_altova_CUserAccountT_altova_pin, 0, 0> pin;	// pin Cstring
	g3schema_EXPORT void SetXsiType();
};


}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CUserAccountT
