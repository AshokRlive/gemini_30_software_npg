#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_mmb_ALTOVA_CMMBDeviceChnlT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_mmb_ALTOVA_CMMBDeviceChnlT



namespace g3schema
{

namespace ns_mmb
{	

class CMMBDeviceChnlT : public TypeBase
{
public:
	g3schema_EXPORT CMMBDeviceChnlT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CMMBDeviceChnlT(CMMBDeviceChnlT const& init);
	void operator=(CMMBDeviceChnlT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_mmb_altova_CMMBDeviceChnlT); }

	MemberAttribute<unsigned,_altova_mi_ns_mmb_altova_CMMBDeviceChnlT_altova_channelID, 0, 0> channelID;	// channelID CunsignedShort

	MemberAttribute<unsigned,_altova_mi_ns_mmb_altova_CMMBDeviceChnlT_altova_regAddress, 0, 0> regAddress;	// regAddress CunsignedInt

	MemberAttribute<int,_altova_mi_ns_mmb_altova_CMMBDeviceChnlT_altova_regType, 0, 0> regType;	// regType Cbyte

	MemberAttribute<int,_altova_mi_ns_mmb_altova_CMMBDeviceChnlT_altova_dataType, 0, 0> dataType;	// dataType Cbyte

	MemberAttribute<unsigned,_altova_mi_ns_mmb_altova_CMMBDeviceChnlT_altova_pollRate, 0, 0> pollRate;	// pollRate CunsignedInt

	MemberAttribute<int,_altova_mi_ns_mmb_altova_CMMBDeviceChnlT_altova_bitMask, 0, 0> bitMask;	// bitMask Cshort

	MemberAttribute<string_type,_altova_mi_ns_mmb_altova_CMMBDeviceChnlT_altova_description, 0, 0> description;	// description CanySimpleType
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_mmb

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_mmb_ALTOVA_CMMBDeviceChnlT
