/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Configuration Manager module implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/09/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <string.h>
#include <zlib.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "IIOModuleManager.h"
#include "GeminiDatabase.h"
#include "SCADAProtocolManager.h"


#include "ConfigurationManager.h"
#include "XMLPointFactory.h"
#include "XMLGeneralConfigParser.h"
#include "XMLNetworkConfigParser.h"
#include "XMLControlLogicFactory.h"
#include "XMLSCADAProtocolParser.h"
#include "XMLPortConfigParser.h"
#include "XMLCommsDeviceConfigParser.h"

#include "XMLHMIFactory.h"
#include "ICANChannelConfigurationFactory.h"

#if MODBUS_MASTER_SUPPORT
#include "XMLModbusConfigParser.h"
#include "DummyMBConfigRepo.h"
#include "MBDeviceFactory.h"
#endif

#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>

#include "Altova/Altova.h"
#include "Altova/SchemaTypes.h"
#include "Altova/AltovaException.h"

#include "AltovaXML/XmlException.h"
#include "AltovaXML/Node.h"

#include "g3schema/g3schema.h"

using namespace g3schema;

#include "XMLParser.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define FILE_READ_CHUNK_SIZE 1024   //Max size of the chunk to read from GZip file

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
static XMLGeneralConfigParser *generalCfgParser = NULL;

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
ConfigurationManager::ConfigurationManager() :
                                    configuration(NULL),
                                    log(Logger::getLogger(SUBSYSTEM_ID_CONFMGR))
{
    /* Initialise XML xercesc parser */
    xercesc::XMLPlatformUtils::Initialize();
}


ConfigurationManager::~ConfigurationManager()
{
    releaseParser();
}


CONFMGR_ERROR ConfigurationManager::initParser(const lu_char_t* configName)
{
    const lu_char_t *FTITLE = "ConfigurationManager::initParser:";
    std::string configString;   //stores the complete config file for parsing


    if(configName == NULL)
    {
        return CONFMGR_ERROR_PARAM;
    }

    if(configuration != NULL)
    {
        return CONFMGR_ERROR_INITIALIZED;
    }

    try
    {
        log.info("%s Initialising configuration parser (%s)", FTITLE, configName);

        /* Load the G3Config Gzip configuration file */
        if (readConfigGzFile(configName, configString) != CONFMGR_ERROR_NONE)
        {
            log.error("%s Error reading (%s)", FTITLE, configName);
            return CONFMGR_ERROR_UNPACK;
        }
        configuration = new Cg3schema(Cg3schema::LoadFromString(configString));

        if( !(configuration->configuration.exists()) )
        {
            log.error("%s Invalid configuration", FTITLE);
            return CONFMGR_ERROR_INITPARSER;
        }

        /* Start the General Config parser */
        generalCfgParser = new XMLGeneralConfigParser(*configuration);
        if(generalCfgParser->initParser() != CONFMGR_ERROR_NONE)
        {
            /* Failed to read version: discard */
            delete generalCfgParser;
            generalCfgParser = NULL;
            return CONFMGR_ERROR_INITPARSER;
        }
    }
    catch (CXmlException& e)
    {
        log.error("%s %s", FTITLE, e.GetInfo().c_str());
        return CONFMGR_ERROR_INITPARSER;
    }
    catch (xercesc::XMLException& e)
    {
        log.error("%s Xerces XMLException: %s: %s", FTITLE, e.getSrcFile(), e.getMessage());
        return CONFMGR_ERROR_INITPARSER;
    }
    catch (xercesc::DOMException& e)
    {
        log.error("%s Xerces DOMException %i: %s", FTITLE, e.code, e.msg);
        return CONFMGR_ERROR_INITPARSER;
    }
    catch (altova::Exception& exception)
    {
        log.error("%s Alt_Exception:  %s", FTITLE, exception.message().c_str());
        return CONFMGR_ERROR_INITPARSER;
    }
    catch (altova::Error& exception)
    {
        log.error("%s Alt_Error:  %s", FTITLE, exception.message().c_str());
        return CONFMGR_ERROR_INITPARSER;
    }
    catch (std::exception& e)
    {
        log.error("%s Exception:  %s", FTITLE, e.what());
        return CONFMGR_ERROR_INITPARSER;
    }
    catch (...)
    {
        log.error("%s Unknown error", FTITLE);
        return CONFMGR_ERROR_INITPARSER;
    }

    return  CONFMGR_ERROR_NONE;
}


void ConfigurationManager::initModules()
{
    /* Update CAN Channel factory */
    moduleFactory.setConfiguration(configuration);
}


CONFMGR_ERROR ConfigurationManager::init(IStatusManager& statusManager,
                                        IIOModuleManager::OLRButtonConfig& cfgOLR)
{
    CONFMGR_ERROR ret = CONFMGR_ERROR_NONE;

    if(generalCfgParser == NULL)
    {
        return CONFMGR_ERROR_NOT_INITIALIZED;
    }
    /* Initialise parsers */
    if(generalCfgParser->parse(statusManager, cfgOLR) != CONFMGR_ERROR_NONE)
    {
        ret = CONFMGR_ERROR_NOT_INITIALIZED;
    }
    return ret;
}

CONFMGR_ERROR ConfigurationManager::init( IStatusManager& statusManager,
                                          IMemoryManager& memoryManager,
                                          IIOModuleManager& MManager,
                                          GeminiDatabase& database,
                                          UserManager& userManager
                                        )
{
    CONFMGR_ERROR ret = CONFMGR_ERROR_NONE;

    if(configuration == NULL)
    {
        return CONFMGR_ERROR_NOT_INITIALIZED;
    }

    if(generalCfgParser == NULL)
    {
        return CONFMGR_ERROR_NOT_INITIALIZED;
    }
    /* Initialise parsers */
    if(generalCfgParser->parse(userManager) != CONFMGR_ERROR_NONE)
    {
        ret = CONFMGR_ERROR_NOT_INITIALIZED;
    }
    XMLPointFactory pointFactory(database, MManager, *configuration);
    XMLControlLogicFactory clogicFactory(database,
                                         MManager,
                                         statusManager,
                                         memoryManager,
                                         *configuration
                                        );

    /* Initialise database */
    if(database.initialize(pointFactory, clogicFactory) != GDB_ERROR_NONE)
    {
        ret = CONFMGR_ERROR_NOT_INITIALIZED;
    }

    /* TODO: pueyos_a - Parse PPP config???
    XMLNetworkConfigParser networkCfg(*configuration);
    if(networkCfg.initParser() != CONFMGR_ERROR_NONE)
    {
        ret = CONFMGR_ERROR_NOT_INITIALIZED;
    }
    */
    return ret;
}


CONFMGR_ERROR ConfigurationManager::init( PortManager& portManager)
{
    if(configuration == NULL)
    {
        return CONFMGR_ERROR_NOT_INITIALIZED;
    }

    XMLPortConfigParser PortConfigParser(*configuration,
                                         portManager);

    return CONFMGR_ERROR_NONE;
}

CONFMGR_ERROR ConfigurationManager::init( CommsDeviceManager& commsDeviceManager,
                                          PortManager&        portManager,
                                          IIOModuleManager&   moduleManager)
{
    if(configuration == NULL)
    {
        return CONFMGR_ERROR_NOT_INITIALIZED;
    }

    XMLCommsDeviceConfigParser CommsDeviceConfigParser(*configuration,
                                                        commsDeviceManager,
                                                        portManager,
                                                        moduleManager);

    return CONFMGR_ERROR_NONE;
}


CONFMGR_ERROR ConfigurationManager::init( SCADAProtocolManager& SCADAManager,
                                          CommsDeviceManager&   commsDeviceManager,
                                          PortManager&          portManager,
                                          ModuleManager&        moduleManager,
                                          GeminiDatabase&       database,
                                          IMCMApplication&      mcmApplication
                                        )
{

    if(configuration == NULL)
    {
        return CONFMGR_ERROR_NOT_INITIALIZED;
    }

    /* Initialise parsers */
    XMLSCADAProtocolParser SCADAProtocolParser(*configuration,
                                                SCADAManager,
                                                commsDeviceManager,
                                                portManager,
                                                moduleManager,
                                                database,
                                                mcmApplication);

    return  CONFMGR_ERROR_NONE;
}


CONFMGR_ERROR ConfigurationManager::init( HMIManager &HMI,
                                          GeminiDatabase &database,
                                          IIOModuleManager& moduleManager,
                                          IStatusManager& statusManager
                                        )
{
    if(configuration == NULL)
        return CONFMGR_ERROR_NOT_INITIALIZED;

    XMLHMIFactory factory(*configuration, database, moduleManager, statusManager);
    HMI.initialize(factory);

    return CONFMGR_ERROR_NONE;
}


CONFMGR_ERROR ConfigurationManager::createAllModules(IIOModuleManager &moduleManager)
{
    //call XMLCANModuleConfigurationFactory::setAllModules()
    if(moduleFactory.setAllModules(moduleManager) == LU_FALSE)
    {
        return CONFMGR_ERROR_INITIALIZED;
    }
    return CONFMGR_ERROR_NONE;
}

#if MODBUS_MASTER_SUPPORT
CONFMGR_ERROR ConfigurationManager::createAllMBDevice(mbm::MBDeviceManager& mbdeviceManager,
                                                    PortManager& portManager)
{
    if(configuration != NULL)
    {
    //    mbm::DummyMBConfigRepo repo;
        XMLModbusConfigParser parser(*configuration,portManager);

        mbm::MBDeviceFactory factory(parser);
        mbdeviceManager.init(factory);

        return CONFMGR_ERROR_NONE;
    }
    else
    {
        DBG_ERR("ModBus Configuration not available!!");
        return CONFMGR_ERROR_INITPARSER;
    }
}
#endif


CONFMGR_ERROR ConfigurationManager::releaseParser()
{
    if(configuration != NULL)
    {
        /* Destroy the configuration file handler */
        configuration->DestroyDocument();

        /* Close the xercesc parser */
        xercesc::XMLPlatformUtils::Terminate();

        /* Free the general configuration parser */
        delete generalCfgParser;

        /* Free configuration handler */
        delete configuration;
        configuration = NULL;

        /* Update CAN Channel factory */
        moduleFactory.setConfiguration(configuration);
    }

    return  CONFMGR_ERROR_NONE;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

CONFMGR_ERROR ConfigurationManager::readConfigGzFile(const lu_char_t* configName, std::string &configString)
{
    gzFile gzFile;
    char   buff[FILE_READ_CHUNK_SIZE];
    int    chunkReadSize;


    if(configuration != NULL)
    {
        return CONFMGR_ERROR_INITIALIZED;
    }

    if ((gzFile = gzopen(configName, "rb")) == NULL)
    {
        return CONFMGR_ERROR_PARAM;
    }

    while((chunkReadSize = gzread(gzFile, buff, FILE_READ_CHUNK_SIZE)) > 0)
    {
        configString.append(buff, chunkReadSize);
    }

    gzclose(gzFile);

    // Read error
    if (chunkReadSize < 0)
    {
        return CONFMGR_ERROR_INITPARSER;
    }

    return CONFMGR_ERROR_NONE;
}




/*
 *********************** End of file ******************************************
 */
