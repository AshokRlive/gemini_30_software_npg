/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       XML based G3 database point factory public interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14/09/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_5DCC5A46_DBF1_4daa_A39F_D7139E374885__INCLUDED_)
#define EA_5DCC5A46_DBF1_4daa_A39F_D7139E374885__INCLUDED_


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IPointFactory.h"
#include "IIOModuleManager.h"
#include "ConfigurationManagerError.h"
#include "XMLParser_Points.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Virtual Point configuration parsing and generation
 *
 * This class is in charge of parsing the configuration from the XML file and
 * generate the Virtual Point with its proper configuration.
 */
class XMLPointFactory : public IPointFactory
{

public:
    /**
     * \brief Custom constructor
     *
     * The initParser method is automatically called
     *
     * \param database Reference to the database used in the point constructor
     * \param MManager Reference to the Module Manager
     * \param configuration Configuration file parser handler
     *
     * \return none
     */
    XMLPointFactory(GeminiDatabase& database, IIOModuleManager &MManager, Cg3schema &configuration);

    virtual ~XMLPointFactory();

    /* == Inherited from IPointFactory == */
    void setMutexLock(Mutex& mutex);

    /**
	 * \brief Return the total number of points the factory can create
	 * 
	 * \return Points number
	 */
    virtual lu_uint16_t getPointNumber();

    /**
     * \brief Create a new virtual point
     *
     * \param mutex Reference to the mutex used in the virtual point constructor
     * \param pointID Virtual point point ID
     *
     * \return Pointer to the new virtual point. NULL in case of error.
     */
    virtual IPoint* newPoint(PointIdStr pointID);

private:
    /**
     * \brief Initialise the parser
     *
     * This function must be called before the getPointNumber and
     * newPoint methods
     *
     * \return Error code
     */
    CONFMGR_ERROR initParser();

    /**
     * \brief Parse analogue point configuration from the XML configuration file
     *
     * \param vPoint XML field containing the configuration
     * \param pointID ID of the point to configure
     *
     * \return New Virtual Point Object, or NULL if it failed.
     */
    IPoint* parseAnalogueConfig(CAnaloguePointT vPoint, const PointIdStr& pointID);

    /**
     * \brief Parse single binary point configuration from the XML configuration file
     *
     * \param vPoint XML field containing the configuration
     * \param pointID ID of the point to configure
     *
     * \return New Virtual Point Object, or NULL if it failed.
     */
    IPoint* parseBinaryConfig(CBinaryPointT vPoint, const PointIdStr& pointID);

    /**
     * \brief Parse double binary point configuration from the XML configuration file
     *
     * \param vPoint XML field containing the configuration
     * \param pointID ID of the point to configure
     *
     * \return New Virtual Point Object, or NULL if it failed.
     */
    IPoint* parseDBinaryConfig(CDoubleBinaryPointT vPoint, const PointIdStr& pointID);

    /**
     * \brief Parse counter point configuration from the XML configuration file
     *
     * \param vPoint XML field containing the configuration
     * \param pointID ID of the point to configure
     *
     * \return New Virtual Point Object, or NULL if it failed.
     */
    IPoint* parseCounterConfig(CCounterPointT vPoint, const PointIdStr& pointID);


    /**
     * \brief Get a channel reference from the registered module manager
     *
     * \param moduleType Module type of the channel to search
     * \param moduleID Module ID of the channel to search
     * \param type Type of the channel to search
     * \param channelID channel ID
     *
     * \return Reference to the channel. NULL if no channel found
     */
    IChannel* getChannel(ChannelRef& channel, const CHANNEL_TYPE type);

private:
    GeminiDatabase& database;
    IIOModuleManager& MManager;
    Logger& log;
    Cg3schema& configuration;

    lu_uint16_t analoguePointNum;
    lu_uint16_t binaryPointNum;
    lu_uint16_t dbinaryPointNum;
    lu_uint16_t counterPointNum;
    lu_uint16_t pointNum;

};


#endif // !defined(EA_5DCC5A46_DBF1_4daa_A39F_D7139E374885__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
