#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CAnalogueFilterT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CAnalogueFilterT



namespace g3schema
{

namespace ns_vpoint
{	

class CAnalogueFilterT : public TypeBase
{
public:
	g3schema_EXPORT CAnalogueFilterT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CAnalogueFilterT(CAnalogueFilterT const& init);
	void operator=(CAnalogueFilterT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CAnalogueFilterT); }
	MemberElement<ns_vpoint::CDeadBandFilterT, _altova_mi_ns_vpoint_altova_CAnalogueFilterT_altova_deadBand> deadBand;
	struct deadBand { typedef Iterator<ns_vpoint::CDeadBandFilterT> iterator; };
	MemberElement<ns_vpoint::CLowerUpperLimitFilterT, _altova_mi_ns_vpoint_altova_CAnalogueFilterT_altova_lowerUpperLimit> lowerUpperLimit;
	struct lowerUpperLimit { typedef Iterator<ns_vpoint::CLowerUpperLimitFilterT> iterator; };
	MemberElement<ns_vpoint::CHighHighLowLowFilterT, _altova_mi_ns_vpoint_altova_CAnalogueFilterT_altova_highHighLowLow> highHighLowLow;
	struct highHighLowLow { typedef Iterator<ns_vpoint::CHighHighLowLowFilterT> iterator; };
	MemberElement<xs::CanyType, _altova_mi_ns_vpoint_altova_CAnalogueFilterT_altova_none> none;
	struct none { typedef Iterator<xs::CanyType> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CAnalogueFilterT
