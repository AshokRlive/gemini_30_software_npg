#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen10_ALTOVA_CLU_TIMETGA_MODE
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen10_ALTOVA_CLU_TIMETGA_MODE



namespace g3schema
{

namespace ns_gen10
{	

class CLU_TIMETGA_MODE : public TypeBase
{
public:
	g3schema_EXPORT CLU_TIMETGA_MODE(xercesc::DOMNode* const& init);
	g3schema_EXPORT CLU_TIMETGA_MODE(CLU_TIMETGA_MODE const& init);
	void operator=(CLU_TIMETGA_MODE const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen10_altova_CLU_TIMETGA_MODE); }

	enum EnumValues {
		Invalid = -1,
		k_LU_TIMETGA_MODE_WITH_TIMETAG = 0, // LU_TIMETGA_MODE_WITH_TIMETAG
		k_LU_TIMETGA_MODE_WITHOUT_TIMETAG = 1, // LU_TIMETGA_MODE_WITHOUT_TIMETAG
		k_LU_TIMETGA_MODE_BOTH = 2, // LU_TIMETGA_MODE_BOTH
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen10

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen10_ALTOVA_CLU_TIMETGA_MODE
