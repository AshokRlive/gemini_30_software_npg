#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CFileModificationDateT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CFileModificationDateT



namespace g3schema
{

class CFileModificationDateT : public TypeBase
{
public:
	g3schema_EXPORT CFileModificationDateT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CFileModificationDateT(CFileModificationDateT const& init);
	void operator=(CFileModificationDateT const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_altova_CFileModificationDateT); }
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};


}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CFileModificationDateT
