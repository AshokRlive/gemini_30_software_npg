#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen10_ALTOVA_CLU_IOA_FORMAT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen10_ALTOVA_CLU_IOA_FORMAT



namespace g3schema
{

namespace ns_gen10
{	

class CLU_IOA_FORMAT : public TypeBase
{
public:
	g3schema_EXPORT CLU_IOA_FORMAT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CLU_IOA_FORMAT(CLU_IOA_FORMAT const& init);
	void operator=(CLU_IOA_FORMAT const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen10_altova_CLU_IOA_FORMAT); }

	enum EnumValues {
		Invalid = -1,
		k_LU_IOA_FORMAT_8_16 = 0, // LU_IOA_FORMAT_8_16
		k_LU_IOA_FORMAT_16_8 = 1, // LU_IOA_FORMAT_16_8
		k_LU_IOA_FORMAT_8_8_8 = 2, // LU_IOA_FORMAT_8_8_8
		k_LU_IOA_FORMAT_NONE = 3, // LU_IOA_FORMAT_NONE
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen10

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen10_ALTOVA_CLU_IOA_FORMAT
