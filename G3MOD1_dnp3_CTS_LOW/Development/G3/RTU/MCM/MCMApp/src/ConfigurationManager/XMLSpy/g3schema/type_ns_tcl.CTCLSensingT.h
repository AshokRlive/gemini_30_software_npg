#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_tcl_ALTOVA_CTCLSensingT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_tcl_ALTOVA_CTCLSensingT



namespace g3schema
{

namespace ns_tcl
{	

class CTCLSensingT : public TypeBase
{
public:
	g3schema_EXPORT CTCLSensingT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CTCLSensingT(CTCLSensingT const& init);
	void operator=(CTCLSensingT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_tcl_altova_CTCLSensingT); }

	MemberAttribute<unsigned,_altova_mi_ns_tcl_altova_CTCLSensingT_altova_triggerTimer, 0, 0> triggerTimer;	// triggerTimer CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_tcl_altova_CTCLSensingT_altova_badIoTimer, 0, 0> badIoTimer;	// badIoTimer CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_tcl_altova_CTCLSensingT_altova_trippingMode, 0, 0> trippingMode;	// trippingMode CunsignedByte
	MemberElement<ns_tcl::CAnalogueMonitoringModeT, _altova_mi_ns_tcl_altova_CTCLSensingT_altova_analogueMonitoringMode> analogueMonitoringMode;
	struct analogueMonitoringMode { typedef Iterator<ns_tcl::CAnalogueMonitoringModeT> iterator; };
	MemberElement<ns_tcl::CTimeMonitoringModeT, _altova_mi_ns_tcl_altova_CTCLSensingT_altova_timeMonitoringMode> timeMonitoringMode;
	struct timeMonitoringMode { typedef Iterator<ns_tcl::CTimeMonitoringModeT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_tcl

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_tcl_ALTOVA_CTCLSensingT
