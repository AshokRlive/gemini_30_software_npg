#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CScadaProtocolT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CScadaProtocolT



namespace g3schema
{

class CScadaProtocolT : public TypeBase
{
public:
	g3schema_EXPORT CScadaProtocolT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CScadaProtocolT(CScadaProtocolT const& init);
	void operator=(CScadaProtocolT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_altova_CScadaProtocolT); }
	MemberElement<ns_sdnp3::CSDNP3T, _altova_mi_altova_CScadaProtocolT_altova_sDNP3> sDNP3;
	struct sDNP3 { typedef Iterator<ns_sdnp3::CSDNP3T> iterator; };
	MemberElement<ns_s104::CS104T, _altova_mi_altova_CScadaProtocolT_altova_sIEC104> sIEC104;
	struct sIEC104 { typedef Iterator<ns_s104::CS104T> iterator; };
	MemberElement<ns_s101::CS101T, _altova_mi_altova_CScadaProtocolT_altova_sIEC101> sIEC101;
	struct sIEC101 { typedef Iterator<ns_s101::CS101T> iterator; };
	g3schema_EXPORT void SetXsiType();
};


}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CScadaProtocolT
