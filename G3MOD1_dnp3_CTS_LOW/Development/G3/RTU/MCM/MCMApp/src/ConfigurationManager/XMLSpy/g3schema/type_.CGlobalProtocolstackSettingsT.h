#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CGlobalProtocolstackSettingsT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CGlobalProtocolstackSettingsT



namespace g3schema
{

class CGlobalProtocolstackSettingsT : public TypeBase
{
public:
	g3schema_EXPORT CGlobalProtocolstackSettingsT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CGlobalProtocolstackSettingsT(CGlobalProtocolstackSettingsT const& init);
	void operator=(CGlobalProtocolstackSettingsT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_altova_CGlobalProtocolstackSettingsT); }

	MemberAttribute<unsigned,_altova_mi_altova_CGlobalProtocolstackSettingsT_altova_slaveProtocolStackDelay, 0, 0> slaveProtocolStackDelay;	// slaveProtocolStackDelay CunsignedInt
	g3schema_EXPORT void SetXsiType();
};


}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CGlobalProtocolstackSettingsT
