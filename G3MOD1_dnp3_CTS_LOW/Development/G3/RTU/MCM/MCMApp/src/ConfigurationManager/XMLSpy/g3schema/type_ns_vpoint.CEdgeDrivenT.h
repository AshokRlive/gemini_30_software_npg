#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CEdgeDrivenT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CEdgeDrivenT



namespace g3schema
{

namespace ns_vpoint
{	

class CEdgeDrivenT : public TypeBase
{
public:
	g3schema_EXPORT CEdgeDrivenT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CEdgeDrivenT(CEdgeDrivenT const& init);
	void operator=(CEdgeDrivenT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CEdgeDrivenT); }

	MemberAttribute<unsigned,_altova_mi_ns_vpoint_altova_CEdgeDrivenT_altova_edgeDrivenMode, 0, 0> edgeDrivenMode;	// edgeDrivenMode CunsignedByte
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CEdgeDrivenT
