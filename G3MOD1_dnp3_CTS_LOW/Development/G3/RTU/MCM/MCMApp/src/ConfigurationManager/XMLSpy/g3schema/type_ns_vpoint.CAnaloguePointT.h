#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CAnaloguePointT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CAnaloguePointT

#include "type_ns_vpoint.CBaseAPointT.h"


namespace g3schema
{

namespace ns_vpoint
{	

class CAnaloguePointT : public ::g3schema::ns_vpoint::CBaseAPointT
{
public:
	g3schema_EXPORT CAnaloguePointT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CAnaloguePointT(CAnaloguePointT const& init);
	void operator=(CAnaloguePointT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CAnaloguePointT); }
	MemberElement<ns_common::CChannelRefT, _altova_mi_ns_vpoint_altova_CAnaloguePointT_altova_channel> channel;
	struct channel { typedef Iterator<ns_common::CChannelRefT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CAnaloguePointT
