#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CPseudoClockT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CPseudoClockT

#include "type_ns_clogic.CCLogicParametersBaseT.h"


namespace g3schema
{

namespace ns_clogic
{	

class CPseudoClockT : public ::g3schema::ns_clogic::CCLogicParametersBaseT
{
public:
	g3schema_EXPORT CPseudoClockT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CPseudoClockT(CPseudoClockT const& init);
	void operator=(CPseudoClockT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_clogic_altova_CPseudoClockT); }

	MemberAttribute<bool,_altova_mi_ns_clogic_altova_CPseudoClockT_altova_autoStartEnabled, 0, 0> autoStartEnabled;	// autoStartEnabled Cboolean

	MemberAttribute<bool,_altova_mi_ns_clogic_altova_CPseudoClockT_altova_outputToggleEnabled, 0, 0> outputToggleEnabled;	// outputToggleEnabled Cboolean

	MemberAttribute<unsigned,_altova_mi_ns_clogic_altova_CPseudoClockT_altova_outputPulseWidth, 0, 0> outputPulseWidth;	// outputPulseWidth CunsignedInt

	MemberAttribute<bool,_altova_mi_ns_clogic_altova_CPseudoClockT_altova_periodicIntervalEnabled, 0, 0> periodicIntervalEnabled;	// periodicIntervalEnabled Cboolean

	MemberAttribute<unsigned,_altova_mi_ns_clogic_altova_CPseudoClockT_altova_periodicIntervalMs, 0, 0> periodicIntervalMs;	// periodicIntervalMs CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_clogic_altova_CPseudoClockT_altova_fromOClockMins, 0, 0> fromOClockMins;	// fromOClockMins CunsignedInt
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_clogic

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CPseudoClockT
