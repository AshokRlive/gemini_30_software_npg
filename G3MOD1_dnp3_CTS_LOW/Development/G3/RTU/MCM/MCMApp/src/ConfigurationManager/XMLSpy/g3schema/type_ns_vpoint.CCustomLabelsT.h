#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CCustomLabelsT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CCustomLabelsT



namespace g3schema
{

namespace ns_vpoint
{	

class CCustomLabelsT : public TypeBase
{
public:
	g3schema_EXPORT CCustomLabelsT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CCustomLabelsT(CCustomLabelsT const& init);
	void operator=(CCustomLabelsT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CCustomLabelsT); }
	MemberElement<ns_vpoint::CCustomLabelSetT, _altova_mi_ns_vpoint_altova_CCustomLabelsT_altova_labelSet> labelSet;
	struct labelSet { typedef Iterator<ns_vpoint::CCustomLabelSetT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CCustomLabelsT
