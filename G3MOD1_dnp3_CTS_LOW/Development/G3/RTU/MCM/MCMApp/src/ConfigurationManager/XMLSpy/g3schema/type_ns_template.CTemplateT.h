#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CTemplateT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CTemplateT



namespace g3schema
{

namespace ns_template
{	

class CTemplateT : public TypeBase
{
public:
	g3schema_EXPORT CTemplateT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CTemplateT(CTemplateT const& init);
	void operator=(CTemplateT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_template_altova_CTemplateT); }
	MemberElement<ns_template::CSwitchTemplateT, _altova_mi_ns_template_altova_CTemplateT_altova_switchTemplate> switchTemplate;
	struct switchTemplate { typedef Iterator<ns_template::CSwitchTemplateT> iterator; };
	MemberElement<ns_template::CModbusTemplateT, _altova_mi_ns_template_altova_CTemplateT_altova_modbusTemplate> modbusTemplate;
	struct modbusTemplate { typedef Iterator<ns_template::CModbusTemplateT> iterator; };
	MemberElement<ns_template::CScadaTemplateT, _altova_mi_ns_template_altova_CTemplateT_altova_scadaTemplate> scadaTemplate;
	struct scadaTemplate { typedef Iterator<ns_template::CScadaTemplateT> iterator; };
	MemberElement<ns_template::CFPIChannelTemplateT, _altova_mi_ns_template_altova_CTemplateT_altova_fpiTemplate> fpiTemplate;
	struct fpiTemplate { typedef Iterator<ns_template::CFPIChannelTemplateT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_template

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CTemplateT
