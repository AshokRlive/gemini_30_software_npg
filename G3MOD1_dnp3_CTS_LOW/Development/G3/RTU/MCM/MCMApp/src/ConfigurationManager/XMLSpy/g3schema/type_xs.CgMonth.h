#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_xs_ALTOVA_CgMonth
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_xs_ALTOVA_CgMonth



namespace g3schema
{

namespace xs
{	

class CgMonth : public TypeBase
{
public:
	g3schema_EXPORT CgMonth(xercesc::DOMNode* const& init);
	g3schema_EXPORT CgMonth(CgMonth const& init);
	void operator=(CgMonth const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_xs_altova_CgMonth); }
	void operator= (const altova::DateTime& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::GMonthFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator altova::DateTime()
	{
		return CastAs<altova::DateTime >::Do(GetNode(), 0);
	}
};



} // namespace xs

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_xs_ALTOVA_CgMonth
