#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CfanSettingType
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CfanSettingType

#include "type_ns_g3module.CFanSettingT.h"


namespace g3schema
{

namespace ns_g3module
{	

class CfanSettingType : public ::g3schema::ns_g3module::CFanSettingT
{
public:
	g3schema_EXPORT CfanSettingType(xercesc::DOMNode* const& init);
	g3schema_EXPORT CfanSettingType(CfanSettingType const& init);
	void operator=(CfanSettingType const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_g3module_altova_CfanSettingType); }
};



} // namespace ns_g3module

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CfanSettingType
