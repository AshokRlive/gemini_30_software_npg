#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CHMIModuleT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CHMIModuleT

#include "type_ns_g3module.CBaseModuleT.h"


namespace g3schema
{

namespace ns_g3module
{	

class CHMIModuleT : public ::g3schema::ns_g3module::CBaseModuleT
{
public:
	g3schema_EXPORT CHMIModuleT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CHMIModuleT(CHMIModuleT const& init);
	void operator=(CHMIModuleT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_g3module_altova_CHMIModuleT); }
	MemberElement<ns_g3module::CHMIChannelsT, _altova_mi_ns_g3module_altova_CHMIModuleT_altova_channels> channels;
	struct channels { typedef Iterator<ns_g3module::CHMIChannelsT> iterator; };
	MemberElement<ns_hmi::CScreensT, _altova_mi_ns_g3module_altova_CHMIModuleT_altova_screens> screens;
	struct screens { typedef Iterator<ns_hmi::CScreensT> iterator; };
	MemberElement<ns_g3module::CHMIGeneralConfT, _altova_mi_ns_g3module_altova_CHMIModuleT_altova_generalConfig> generalConfig;
	struct generalConfig { typedef Iterator<ns_g3module::CHMIGeneralConfT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_g3module

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CHMIModuleT
