/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:XMLModbusConfigRepo.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   12 Jun 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef XMLMODBUSCONFIGREPO_H_
#define XMLMODBUSCONFIGREPO_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include "LinIoTarg/liniodefs.h"
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IModbusConfigRepository.h"
#include "ConfigurationManagerError.h"
#include "Logger.h"
#include "XMLParser.h"
#include "PortManager.h"
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */


using namespace g3schema;
using namespace altova;

/**
 * This structure contains all configuration for one device.
 */
struct DevConfWrapper
{
    DevConfWrapper()
    {
        aichs = NULL;
        dichs = NULL;
        dochs = NULL;
    }

    ~DevConfWrapper()
    {
        delete[]   aichs;
        delete[]   dichs;
        delete[]   dochs;
    }
    mbm::DeviceConf    devconf;
    mbm::ChannelConf*  aichs;
    mbm::ChannelConf*  dichs;
    mbm::ChannelConf*  dochs;
};

class XMLModbusConfigParser:public mbm::IModbusConfigRepository
{
public:
    XMLModbusConfigParser(Cg3schema& configuration,PortManager &portManager);
    virtual ~XMLModbusConfigParser();


    lu_uint8_t getDeviceNum();

    void getDeviceConf(const lu_uint8_t deviceIdx, mbm::DeviceConf* conf);

    lu_int32_t getChannelNum(const lu_uint8_t deviceIdx, CHANNEL_TYPE type);

    void getChannelConf(const mbm::ChannelRef& ch, mbm::ChannelConf* conf);

private:
    CONFMGR_ERROR initParser();

    bool checkDeviceId(const lu_uint8_t deviceIdx);

    bool checkChannelRef(const mbm::ChannelRef& ch);
private:
    Logger&            log;
    Cg3schema&         m_configuration;
    lu_uint8_t         m_devNum;
    DevConfWrapper*    mp_devconf;
    PortManager&       m_portManager;
};

#endif /* XMLMODBUSCONFIGREPO_H_ */

/*
 *********************** End of file ******************************************
 */
