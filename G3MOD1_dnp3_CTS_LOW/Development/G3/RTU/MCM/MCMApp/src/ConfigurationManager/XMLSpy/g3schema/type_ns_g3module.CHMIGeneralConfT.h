#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CHMIGeneralConfT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CHMIGeneralConfT



namespace g3schema
{

namespace ns_g3module
{	

class CHMIGeneralConfT : public TypeBase
{
public:
	g3schema_EXPORT CHMIGeneralConfT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CHMIGeneralConfT(CHMIGeneralConfT const& init);
	void operator=(CHMIGeneralConfT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_g3module_altova_CHMIGeneralConfT); }

	MemberAttribute<bool,_altova_mi_ns_g3module_altova_CHMIGeneralConfT_altova_buttonClick, 0, 0> buttonClick;	// buttonClick Cboolean

	MemberAttribute<bool,_altova_mi_ns_g3module_altova_CHMIGeneralConfT_altova_requestOLR, 0, 0> requestOLR;	// requestOLR Cboolean

	MemberAttribute<unsigned,_altova_mi_ns_g3module_altova_CHMIGeneralConfT_altova_openClosePressTimeMs, 0, 0> openClosePressTimeMs;	// openClosePressTimeMs CunsignedInt
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_g3module

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CHMIGeneralConfT
