#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CToolVersionT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CToolVersionT



namespace g3schema
{

class CToolVersionT : public TypeBase
{
public:
	g3schema_EXPORT CToolVersionT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CToolVersionT(CToolVersionT const& init);
	void operator=(CToolVersionT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_altova_CToolVersionT); }

	MemberAttribute<string_type,_altova_mi_altova_CToolVersionT_altova_revision, 0, 0> revision;	// revision CnormalizedString

	MemberAttribute<string_type,_altova_mi_altova_CToolVersionT_altova_version, 0, 0> version;	// version CnormalizedString
	g3schema_EXPORT void SetXsiType();
};


}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CToolVersionT
