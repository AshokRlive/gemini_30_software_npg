#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s104_ALTOVA_CS104LinkLayerConfT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s104_ALTOVA_CS104LinkLayerConfT

#include "type_ns_iec870.CIEC870LinkLayerConfT.h"


namespace g3schema
{

namespace ns_s104
{	

class CS104LinkLayerConfT : public ::g3schema::ns_iec870::CIEC870LinkLayerConfT
{
public:
	g3schema_EXPORT CS104LinkLayerConfT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CS104LinkLayerConfT(CS104LinkLayerConfT const& init);
	void operator=(CS104LinkLayerConfT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_s104_altova_CS104LinkLayerConfT); }

	MemberAttribute<unsigned,_altova_mi_ns_s104_altova_CS104LinkLayerConfT_altova_t1AckPeriodMs, 0, 0> t1AckPeriodMs;	// t1AckPeriodMs CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_s104_altova_CS104LinkLayerConfT_altova_t2SFramePeriodMs, 0, 0> t2SFramePeriodMs;	// t2SFramePeriodMs CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_s104_altova_CS104LinkLayerConfT_altova_t3TestPeriodMs, 0, 0> t3TestPeriodMs;	// t3TestPeriodMs CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_s104_altova_CS104LinkLayerConfT_altova_offlinePollPeriodMs, 0, 0> offlinePollPeriodMs;	// offlinePollPeriodMs CunsignedInt

	MemberAttribute<bool,_altova_mi_ns_s104_altova_CS104LinkLayerConfT_altova_discardIFramesOnDisconnect, 0, 0> discardIFramesOnDisconnect;	// discardIFramesOnDisconnect Cboolean

	MemberAttribute<unsigned,_altova_mi_ns_s104_altova_CS104LinkLayerConfT_altova_kValue, 0, 0> kValue;	// kValue CunsignedShort

	MemberAttribute<unsigned,_altova_mi_ns_s104_altova_CS104LinkLayerConfT_altova_wValue, 0, 0> wValue;	// wValue CunsignedShort
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_s104

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s104_ALTOVA_CS104LinkLayerConfT
