/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Configuration Manager error code
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/09/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_18911469_1E4D_47aa_AD7C_440C1DDE23D4__INCLUDED_)
#define EA_18911469_1E4D_47aa_AD7C_440C1DDE23D4__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/**
 * \brief Configuration Manager module error codes
 */
enum CONFMGR_ERROR
{
    CONFMGR_ERROR_NONE        = 0,
    CONFMGR_ERROR_PARAM          ,
    CONFMGR_ERROR_NOT_INITIALIZED,
    CONFMGR_ERROR_INITIALIZED    ,
    CONFMGR_ERROR_INITPARSER     ,
    CONFMGR_ERROR_CONFIG         ,
    CONFMGR_ERROR_NO_POINT       ,
    CONFMGR_ERROR_UNPACK         ,


    CONFMGR_ERROR_LAST
};

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */



#endif // !defined(EA_18911469_1E4D_47aa_AD7C_440C1DDE23D4__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
