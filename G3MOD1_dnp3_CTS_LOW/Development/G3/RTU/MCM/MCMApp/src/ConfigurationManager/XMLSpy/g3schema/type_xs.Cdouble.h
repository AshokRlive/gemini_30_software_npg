#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_xs_ALTOVA_Cdouble
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_xs_ALTOVA_Cdouble



namespace g3schema
{

namespace xs
{	

class Cdouble : public TypeBase
{
public:
	g3schema_EXPORT Cdouble(xercesc::DOMNode* const& init);
	g3schema_EXPORT Cdouble(Cdouble const& init);
	void operator=(Cdouble const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_xs_altova_Cdouble); }
	void operator= (const double& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::DoubleFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator double()
	{
		return CastAs<double >::Do(GetNode(), 0);
	}
};



} // namespace xs

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_xs_ALTOVA_Cdouble
