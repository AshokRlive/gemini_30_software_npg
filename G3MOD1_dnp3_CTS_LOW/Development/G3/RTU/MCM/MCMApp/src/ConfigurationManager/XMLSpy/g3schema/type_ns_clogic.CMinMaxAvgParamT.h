#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CMinMaxAvgParamT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CMinMaxAvgParamT

#include "type_ns_clogic.CCLogicParametersBaseT.h"


namespace g3schema
{

namespace ns_clogic
{	

class CMinMaxAvgParamT : public ::g3schema::ns_clogic::CCLogicParametersBaseT
{
public:
	g3schema_EXPORT CMinMaxAvgParamT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CMinMaxAvgParamT(CMinMaxAvgParamT const& init);
	void operator=(CMinMaxAvgParamT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_clogic_altova_CMinMaxAvgParamT); }

	MemberAttribute<int,_altova_mi_ns_clogic_altova_CMinMaxAvgParamT_altova_startDelay, 0, 0> startDelay;	// startDelay Cint

	MemberAttribute<unsigned,_altova_mi_ns_clogic_altova_CMinMaxAvgParamT_altova_clocktime, 0, 0> clocktime;	// clocktime CunsignedByte

	MemberAttribute<unsigned,_altova_mi_ns_clogic_altova_CMinMaxAvgParamT_altova_period, 0, 0> period;	// period CunsignedByte

	MemberAttribute<bool,_altova_mi_ns_clogic_altova_CMinMaxAvgParamT_altova_enableCounter, 0, 0> enableCounter;	// enableCounter Cboolean

	MemberAttribute<double,_altova_mi_ns_clogic_altova_CMinMaxAvgParamT_altova_counterScalingFactor, 0, 0> counterScalingFactor;	// counterScalingFactor Cfloat
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_clogic

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CMinMaxAvgParamT
