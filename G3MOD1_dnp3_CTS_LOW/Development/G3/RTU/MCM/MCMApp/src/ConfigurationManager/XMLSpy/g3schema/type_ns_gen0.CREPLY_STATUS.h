#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen0_ALTOVA_CREPLY_STATUS
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen0_ALTOVA_CREPLY_STATUS



namespace g3schema
{

namespace ns_gen0
{	

class CREPLY_STATUS : public TypeBase
{
public:
	g3schema_EXPORT CREPLY_STATUS(xercesc::DOMNode* const& init);
	g3schema_EXPORT CREPLY_STATUS(CREPLY_STATUS const& init);
	void operator=(CREPLY_STATUS const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen0_altova_CREPLY_STATUS); }

	enum EnumValues {
		Invalid = -1,
		k_REPLY_STATUS_OKAY________ = 0, // REPLY_STATUS_OKAY        
		k_REPLY_STATUS_ERROR_____ = 1, // REPLY_STATUS_ERROR     
		k_REPLY_STATUS_SELECT_ERROR___ = 2, // REPLY_STATUS_SELECT_ERROR   
		k_REPLY_STATUS_OPERATE_ERROR___ = 3, // REPLY_STATUS_OPERATE_ERROR   
		k_REPLY_STATUS_CANCEL_ERROR___ = 4, // REPLY_STATUS_CANCEL_ERROR   
		k_REPLY_STATUS_LOCAL_REMOTE_ERROR__ = 5, // REPLY_STATUS_LOCAL_REMOTE_ERROR  
		k_REPLY_STATUS_ALREADY_OPERATING_ERROR = 6, // REPLY_STATUS_ALREADY_OPERATING_ERROR
		k_REPLY_STATUS_OPERATION_IN_PROGRESS_ = 7, // REPLY_STATUS_OPERATION_IN_PROGRESS 
		k_REPLY_STATUS_BATTERY_SUPPLY_ERROR_ = 8, // REPLY_STATUS_BATTERY_SUPPLY_ERROR 
		k_REPLY_STATUS_PARAM_ERROR___ = 9, // REPLY_STATUS_PARAM_ERROR   
		k_REPLY_STATUS_INHIBIT_ERROR___ = 10, // REPLY_STATUS_INHIBIT_ERROR   
		k_REPLY_STATUS_RELAY_ERROR___ = 11, // REPLY_STATUS_RELAY_ERROR   
		k_REPLY_STATUS_VMOTOR_ERROR___ = 12, // REPLY_STATUS_VMOTOR_ERROR   
		k_REPLY_STATUS_TIMEOUT_ERROR___ = 13, // REPLY_STATUS_TIMEOUT_ERROR   
		k_REPLY_STATUS_IOMANAGER_ERROR__ = 14, // REPLY_STATUS_IOMANAGER_ERROR  
		k_REPLY_STATUS_CONFIG_ERROR___ = 15, // REPLY_STATUS_CONFIG_ERROR   
		k_REPLY_STATUS_COMPLETE_ERROR___ = 16, // REPLY_STATUS_COMPLETE_ERROR   
		k_REPLY_STATUS_NOT_ONLINE_ERROR_______ = 17, // REPLY_STATUS_NOT_ONLINE_ERROR       
		k_REPLY_STATUS_NO_APPLICATION_________ = 18, // REPLY_STATUS_NO_APPLICATION         
		k_REPLY_STATUS_WRITE_ERROR____________ = 19, // REPLY_STATUS_WRITE_ERROR            
		k_REPLY_STATUS_NO_EVENTS______________ = 20, // REPLY_STATUS_NO_EVENTS              
		k_REPLY_STATUS_OPERATE_REJECT_ERROR_ = 21, // REPLY_STATUS_OPERATE_REJECT_ERROR 
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen0

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen0_ALTOVA_CREPLY_STATUS
