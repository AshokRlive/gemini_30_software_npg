#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen0_ALTOVA_CMODULE
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen0_ALTOVA_CMODULE



namespace g3schema
{

namespace ns_gen0
{	

class CMODULE : public TypeBase
{
public:
	g3schema_EXPORT CMODULE(xercesc::DOMNode* const& init);
	g3schema_EXPORT CMODULE(CMODULE const& init);
	void operator=(CMODULE const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen0_altova_CMODULE); }

	enum EnumValues {
		Invalid = -1,
		k_MODULE_PSM = 0, // MODULE_PSM
		k_MODULE_MCM = 1, // MODULE_MCM
		k_MODULE_SCM = 2, // MODULE_SCM
		k_MODULE_FDM = 3, // MODULE_FDM
		k_MODULE_HMI = 4, // MODULE_HMI
		k_MODULE_IOM = 5, // MODULE_IOM
		k_MODULE_DSM = 6, // MODULE_DSM
		k_MODULE_FPM = 7, // MODULE_FPM
		k_MODULE_SSM = 8, // MODULE_SSM
		k_MODULE_SCM_MK2 = 9, // MODULE_SCM_MK2
		k_MODULE_AMM = 10, // MODULE_AMM
		k_MODULE_UNKNOWN_4 = 11, // MODULE_UNKNOWN_4
		k_MODULE_UNKNOWN_5 = 12, // MODULE_UNKNOWN_5
		k_MODULE_UNKNOWN_6 = 13, // MODULE_UNKNOWN_6
		k_MODULE_UNKNOWN_7 = 14, // MODULE_UNKNOWN_7
		k_MODULE_UNKNOWN_8 = 15, // MODULE_UNKNOWN_8
		k_MODULE_UNKNOWN_9 = 16, // MODULE_UNKNOWN_9
		k_MODULE_UNKNOWN_10 = 17, // MODULE_UNKNOWN_10
		k_MODULE_UNKNOWN_11 = 18, // MODULE_UNKNOWN_11
		k_MODULE_UNKNOWN_12 = 19, // MODULE_UNKNOWN_12
		k_MODULE_UNKNOWN_13 = 20, // MODULE_UNKNOWN_13
		k_MODULE_UNKNOWN_14 = 21, // MODULE_UNKNOWN_14
		k_MODULE_UNKNOWN_15 = 22, // MODULE_UNKNOWN_15
		k_MODULE_UNKNOWN_16 = 23, // MODULE_UNKNOWN_16
		k_MODULE_UNKNOWN_17 = 24, // MODULE_UNKNOWN_17
		k_MODULE_UNKNOWN_18 = 25, // MODULE_UNKNOWN_18
		k_MODULE_UNKNOWN_19 = 26, // MODULE_UNKNOWN_19
		k_MODULE_UNKNOWN_20 = 27, // MODULE_UNKNOWN_20
		k_MODULE_MODEM = 28, // MODULE_MODEM
		k_MODULE_MBDEVICE = 29, // MODULE_MBDEVICE
		k_MODULE_TEST_MCM = 30, // MODULE_TEST_MCM
		k_MODULE_BRD = 31, // MODULE_BRD
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen0

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen0_ALTOVA_CMODULE
