#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CFieldDevicesT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CFieldDevicesT



namespace g3schema
{

class CFieldDevicesT : public TypeBase
{
public:
	g3schema_EXPORT CFieldDevicesT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CFieldDevicesT(CFieldDevicesT const& init);
	void operator=(CFieldDevicesT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_altova_CFieldDevicesT); }
	MemberElement<ns_mmb::CModbusMasterT, _altova_mi_altova_CFieldDevicesT_altova_ModbusMaster> ModbusMaster;
	struct ModbusMaster { typedef Iterator<ns_mmb::CModbusMasterT> iterator; };
	g3schema_EXPORT void SetXsiType();
};


}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CFieldDevicesT
