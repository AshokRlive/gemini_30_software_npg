#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CDigitalControlParamT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CDigitalControlParamT

#include "type_ns_clogic.CCLogicParametersBaseT.h"


namespace g3schema
{

namespace ns_clogic
{	

class CDigitalControlParamT : public ::g3schema::ns_clogic::CCLogicParametersBaseT
{
public:
	g3schema_EXPORT CDigitalControlParamT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CDigitalControlParamT(CDigitalControlParamT const& init);
	void operator=(CDigitalControlParamT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_clogic_altova_CDigitalControlParamT); }

	MemberAttribute<bool,_altova_mi_ns_clogic_altova_CDigitalControlParamT_altova_pulseEnabled, 0, 0> pulseEnabled;	// pulseEnabled Cboolean

	MemberAttribute<unsigned,_altova_mi_ns_clogic_altova_CDigitalControlParamT_altova_pulseDuration, 0, 0> pulseDuration;	// pulseDuration CunsignedShort

	MemberAttribute<int,_altova_mi_ns_clogic_altova_CDigitalControlParamT_altova_trigger, 0, 0> trigger;	// trigger Cint
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_clogic

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CDigitalControlParamT
