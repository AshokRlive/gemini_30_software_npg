#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CDelayedReportT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CDelayedReportT



namespace g3schema
{

namespace ns_vpoint
{	

class CDelayedReportT : public TypeBase
{
public:
	g3schema_EXPORT CDelayedReportT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CDelayedReportT(CDelayedReportT const& init);
	void operator=(CDelayedReportT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CDelayedReportT); }

	MemberAttribute<bool,_altova_mi_ns_vpoint_altova_CDelayedReportT_altova_enabled, 0, 0> enabled;	// enabled Cboolean

	MemberAttribute<unsigned,_altova_mi_ns_vpoint_altova_CDelayedReportT_altova_delay, 0, 0> delay;	// delay CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_vpoint_altova_CDelayedReportT_altova_edgeDrivenMode, 0, 0> edgeDrivenMode;	// edgeDrivenMode CunsignedByte
	MemberElement<ns_vpoint::CDelayedEventEntryT, _altova_mi_ns_vpoint_altova_CDelayedReportT_altova_entry> entry;
	struct entry { typedef Iterator<ns_vpoint::CDelayedEventEntryT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CDelayedReportT
