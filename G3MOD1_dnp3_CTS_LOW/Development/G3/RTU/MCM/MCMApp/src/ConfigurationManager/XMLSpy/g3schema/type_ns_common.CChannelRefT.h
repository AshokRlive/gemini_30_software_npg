#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_CChannelRefT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_CChannelRefT



namespace g3schema
{

namespace ns_common
{	

class CChannelRefT : public TypeBase
{
public:
	g3schema_EXPORT CChannelRefT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CChannelRefT(CChannelRefT const& init);
	void operator=(CChannelRefT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_common_altova_CChannelRefT); }
	MemberAttribute<string_type,_altova_mi_ns_common_altova_CChannelRefT_altova_moduleType, 0, 32> moduleType;	// moduleType CMODULE
	MemberAttribute<string_type,_altova_mi_ns_common_altova_CChannelRefT_altova_moduleID, 0, 26> moduleID;	// moduleID CMODULE_ID

	MemberAttribute<unsigned,_altova_mi_ns_common_altova_CChannelRefT_altova_channelID, 0, 0> channelID;	// channelID CunsignedInt
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_common

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_CChannelRefT
