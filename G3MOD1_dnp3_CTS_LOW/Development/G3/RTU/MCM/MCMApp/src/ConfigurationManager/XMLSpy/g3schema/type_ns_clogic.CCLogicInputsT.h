#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CCLogicInputsT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CCLogicInputsT



namespace g3schema
{

namespace ns_clogic
{	

class CCLogicInputsT : public TypeBase
{
public:
	g3schema_EXPORT CCLogicInputsT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CCLogicInputsT(CCLogicInputsT const& init);
	void operator=(CCLogicInputsT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_clogic_altova_CCLogicInputsT); }

	MemberAttribute<string_type,_altova_mi_ns_clogic_altova_CCLogicInputsT_altova_name, 0, 0> name;	// name Cstring

	MemberAttribute<int,_altova_mi_ns_clogic_altova_CCLogicInputsT_altova_type, 0, 0> type;	// type Cint
	MemberElement<ns_common::CVirtualPointRefT, _altova_mi_ns_clogic_altova_CCLogicInputsT_altova_inputPoint> inputPoint;
	struct inputPoint { typedef Iterator<ns_common::CVirtualPointRefT> iterator; };
	MemberElement<xs::CanyType, _altova_mi_ns_clogic_altova_CCLogicInputsT_altova_none> none;
	struct none { typedef Iterator<xs::CanyType> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_clogic

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CCLogicInputsT
