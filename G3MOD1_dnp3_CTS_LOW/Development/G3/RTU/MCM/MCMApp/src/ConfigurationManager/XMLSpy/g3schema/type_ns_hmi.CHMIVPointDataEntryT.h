#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CHMIVPointDataEntryT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CHMIVPointDataEntryT



namespace g3schema
{

namespace ns_hmi
{	

class CHMIVPointDataEntryT : public TypeBase
{
public:
	g3schema_EXPORT CHMIVPointDataEntryT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CHMIVPointDataEntryT(CHMIVPointDataEntryT const& init);
	void operator=(CHMIVPointDataEntryT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_hmi_altova_CHMIVPointDataEntryT); }

	MemberAttribute<unsigned,_altova_mi_ns_hmi_altova_CHMIVPointDataEntryT_altova_entryID, 0, 0> entryID;	// entryID CunsignedShort

	MemberAttribute<string_type,_altova_mi_ns_hmi_altova_CHMIVPointDataEntryT_altova_header, 0, 0> header;	// header Cstring

	MemberAttribute<string_type,_altova_mi_ns_hmi_altova_CHMIVPointDataEntryT_altova_footer, 0, 0> footer;	// footer Cstring

	MemberAttribute<unsigned,_altova_mi_ns_hmi_altova_CHMIVPointDataEntryT_altova_pointGroup, 0, 0> pointGroup;	// pointGroup CVirtualPointGroupT

	MemberAttribute<unsigned,_altova_mi_ns_hmi_altova_CHMIVPointDataEntryT_altova_pointID, 0, 0> pointID;	// pointID CVirtualPointIdT

	MemberAttribute<unsigned,_altova_mi_ns_hmi_altova_CHMIVPointDataEntryT_altova_analogValuePrecision, 0, 0> analogValuePrecision;	// analogValuePrecision CunsignedByte

	MemberAttribute<unsigned,_altova_mi_ns_hmi_altova_CHMIVPointDataEntryT_altova_analogValueRoundingMode, 0, 0> analogValueRoundingMode;	// analogValueRoundingMode CunsignedByte
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_hmi

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CHMIVPointDataEntryT
