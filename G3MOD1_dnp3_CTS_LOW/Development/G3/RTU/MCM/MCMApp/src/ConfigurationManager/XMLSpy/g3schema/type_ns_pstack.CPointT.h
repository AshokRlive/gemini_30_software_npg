#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_pstack_ALTOVA_CPointT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_pstack_ALTOVA_CPointT



namespace g3schema
{

namespace ns_pstack
{	

class CPointT : public TypeBase
{
public:
	g3schema_EXPORT CPointT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CPointT(CPointT const& init);
	void operator=(CPointT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_pstack_altova_CPointT); }

	MemberAttribute<unsigned,_altova_mi_ns_pstack_altova_CPointT_altova_protocolID, 0, 0> protocolID;	// protocolID CunsignedInt

	MemberAttribute<bool,_altova_mi_ns_pstack_altova_CPointT_altova_enable, 0, 0> enable;	// enable Cboolean

	MemberAttribute<string_type,_altova_mi_ns_pstack_altova_CPointT_altova_description, 0, 0> description;	// description Cstring

	MemberAttribute<bool,_altova_mi_ns_pstack_altova_CPointT_altova_enableStoreEvent, 0, 0> enableStoreEvent;	// enableStoreEvent Cboolean
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_pstack

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_pstack_ALTOVA_CPointT
