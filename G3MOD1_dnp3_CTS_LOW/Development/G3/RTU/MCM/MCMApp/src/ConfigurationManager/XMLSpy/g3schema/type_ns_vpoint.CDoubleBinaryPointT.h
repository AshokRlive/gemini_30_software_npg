#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CDoubleBinaryPointT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CDoubleBinaryPointT

#include "type_ns_vpoint.CBaseDBPointT.h"


namespace g3schema
{

namespace ns_vpoint
{	

class CDoubleBinaryPointT : public ::g3schema::ns_vpoint::CBaseDBPointT
{
public:
	g3schema_EXPORT CDoubleBinaryPointT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CDoubleBinaryPointT(CDoubleBinaryPointT const& init);
	void operator=(CDoubleBinaryPointT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CDoubleBinaryPointT); }
	MemberElement<ns_vpoint::CDoubleBinaryValueMapT, _altova_mi_ns_vpoint_altova_CDoubleBinaryPointT_altova_valueMap> valueMap;
	struct valueMap { typedef Iterator<ns_vpoint::CDoubleBinaryValueMapT> iterator; };
	MemberElement<ns_vpoint::CDoubleBinaryDebounceT, _altova_mi_ns_vpoint_altova_CDoubleBinaryPointT_altova_debounce> debounce;
	struct debounce { typedef Iterator<ns_vpoint::CDoubleBinaryDebounceT> iterator; };
	MemberElement<ns_common::CChannelRefT, _altova_mi_ns_vpoint_altova_CDoubleBinaryPointT_altova_channel0> channel0;
	struct channel0 { typedef Iterator<ns_common::CChannelRefT> iterator; };
	MemberElement<ns_common::CChannelRefT, _altova_mi_ns_vpoint_altova_CDoubleBinaryPointT_altova_channel1> channel1;
	struct channel1 { typedef Iterator<ns_common::CChannelRefT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CDoubleBinaryPointT
