/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       XML based G3 general configuration parser implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15-Oct-2012   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <fstream>
#include <iostream>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "XMLGeneralConfigParser.h"
#include "EventLogManager.h"
#include "versions.h"
#include "TimeManager.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
/* NTP configuration script calls */
#define NTP_SCRIPT_CALL "scripts/ntp_config.sh "
#define TEMP_NTP_CFG_FILE "/tmp/ntpconfig.tmp"
#define NTP_NOCFG_OPT "--delete"

#define NTP_SCRIPT NTP_SCRIPT_CALL TEMP_NTP_CFG_FILE
#define NTP_NOSCRIPT NTP_SCRIPT_CALL NTP_NOCFG_OPT

/* Time zone configuration script: update softlink */
//Example: ln -sf zoneinfo/Etc/GMT-1 localtime
#define LOCALE_SCRIPT "ln -sf zoneinfo/"
#define LOCALE_LINK_NAME "localtime"

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

XMLGeneralConfigParser::XMLGeneralConfigParser(Cg3schema& configuration) :
                                            configuration(configuration),
                                            log(Logger::getLogger(SUBSYSTEM_ID_CONFMGR))
{
    eventLog = EventLogManager::getInstance();
}

XMLGeneralConfigParser::~XMLGeneralConfigParser()
{}

CONFMGR_ERROR XMLGeneralConfigParser::initParser()
{
    /* ===Get Mandatory fields=== */
    try
    {
        /* Get a reference to the main configuration */
        CConfigurationT configurationRoot = getXMLElement<CConfigurationT>(configuration.configuration);

        /* Check Schema version compatibility */
        checkXMLElement(configurationRoot.schemaVersionMajor);
        checkXMLElement(configurationRoot.schemaVersionMinor);
        lu_uint32_t schemaVersion_M = configurationRoot.schemaVersionMajor;
        lu_uint32_t schemaVersion_m = configurationRoot.schemaVersionMinor;

        //Note: "(signed)(lu_int64_t)" added to avoid compiler warning when comparing < 0 in case of SCHEMA_MINOR==0
        if( (schemaVersion_M != SCHEMA_MAJOR) ||
            ( (schemaVersion_M == SCHEMA_MAJOR) &&
                ( (signed)(lu_int64_t)schemaVersion_m > (signed)(lu_int64_t)SCHEMA_MINOR)
                )
            )
        {
            /* XML Config file Schema version does not match with supported one: do not continue! */
            log.error("%s Configuration file version %d.%d is not supported! (supported: %d.%d).",
                            __AT__,
                        schemaVersion_M, schemaVersion_m, SCHEMA_MAJOR, SCHEMA_MINOR
                      );
            /* TODO: pueyos_a - Report read XML error due to version mismatch */
            throw ParserException(CONFMGR_ERROR_PARAM, "Config file version mismatch");
        }

        if(schemaVersion_m != SCHEMA_MINOR)
        {
            /* Using a previous (probably compatible) Configuration file version */
            log.warn("%s Using configuration file version %d.%d but supported version is %d.%d.",
                            __AT__,
                        schemaVersion_M, schemaVersion_m, SCHEMA_MAJOR, SCHEMA_MINOR
                      );
            /* TODO: pueyos_a - Report read XML warning due to version mismatch */
            //Report Event
            CTEventSystemStr event;
            event.eventType = EVENT_TYPE_SYSTEM_READ_CONFIG;
            event.value = EVENT_READ_CONFIG_VALUE_ERROR; //WARNING_VERSION_PREVIOUS;
            eventLog->logEvent(event);
        }
    }
    catch(ParserException& e)
    {
        if(e.getID() == CONFMGR_ERROR_PARAM)
        {
            //Report Event
            CTEventSystemStr event;
            event.eventType = EVENT_TYPE_SYSTEM_READ_CONFIG;
            event.value = EVENT_READ_CONFIG_VALUE_ERROR; //_VERSION_UNSUPPORTED;
            eventLog->logEvent(event);
            return CONFMGR_ERROR_CONFIG;    //error in file version: stop reading it.
        }
        log.error("%s Exception raised: %s", __AT__, e.what());
        return CONFMGR_ERROR_INITPARSER;
    }

    return CONFMGR_ERROR_NONE;
}


CONFMGR_ERROR XMLGeneralConfigParser::parse(UserManager &userManager)
{
    try
    {
        /* Get a reference to the main configuration */
        CConfigurationT configurationRoot = getXMLElement<CConfigurationT>(configuration.configuration);

        /* Get User list */
        CUserSettingT userList = getXMLElement<CUserSettingT>(configurationRoot.userSetting);

        if(userList.securityPolicy.exists())
        {
        	if(userList.securityPolicy.first().accountLockoutDurationSecs.exists())
        		userManager.setAccountLockoutDurationSecs(
        				getXMLAttr(userList.securityPolicy.first().accountLockoutDurationSecs));

        	if(userList.securityPolicy.first().invalidLogonAttemptsThreshold.exists())
        		userManager.setInvalidLogonAttemptThreshold(
        				getXMLAttr(userList.securityPolicy.first().invalidLogonAttemptsThreshold));
        }

        if(userList.userAccounts.exists())
        {
            try
            {
                //fill user list
                for( Iterator<CUserAccountT> itUser = userList.userAccounts.all();
                     itUser;
                     ++itUser
                   )
                {
                    std::string name = getXMLAttr(itUser.name);
                    std::string password = getXMLAttr(itUser.password);
                    lu_uint32_t userlevel = getXMLAttr(itUser.group);
                    USER_LEVEL accessLevel = static_cast<USER_LEVEL>(userlevel);

                    if(accessLevel > USER_LEVEL_LAST)
                    {
                        accessLevel = USER_LEVEL_OPERATOR;
                    }
                    User user(name, password, accessLevel, userManager.getInvalidLogonAttemptThreshold());

                    if(itUser.mdAlgorithm.exists())
                    {
                        std::string md = getXMLAttr(itUser.mdAlgorithm);
                        user.setMDAlgorithm(md);
                    }

                    switch(userManager.userAdd(user))
                    {
                        case UserManager::USERLIST_ERROR_NONE:
                            //user added successfully
                            break;
                        case UserManager::USERLIST_ERROR_ALREADY_EXISTS:
                            log.warn("%s user %s is duplicated.", __AT__, name.c_str());
                            break;
                        case UserManager::USERLIST_ERROR_ACCESS_LEVEL:
                            log.warn("%s user has an unsupported access level %i.", __AT__,
                                        name.c_str(), accessLevel
                                      );
                            break;
                        default:
                            log.warn("%s error adding user %s: skipped.", __AT__, name.c_str());
                            break;
                    }
                }
            }
            catch(const ParserException& e)
            {
                log.error("%s exception raised reading user list: %s", __AT__, e.what());
                return CONFMGR_ERROR_CONFIG;
            }
        }
        else
        {
            //No user registered
            log.warn("%s No users in the user list.", __AT__);
        }
    }
    catch(const ParserException& e)
    {
        //Report Event
        CTEventSystemStr event;
        event.eventType = EVENT_TYPE_SYSTEM_READ_CONFIG;
        event.value = EVENT_READ_CONFIG_VALUE_ERROR;
        eventLog->logEvent(event);

        log.error("%s Configuration file error: %s", __AT__, e.what());
        return CONFMGR_ERROR_INITPARSER;
    }
    return CONFMGR_ERROR_NONE;
}


CONFMGR_ERROR XMLGeneralConfigParser::parse(IStatusManager& statusManager,
                                            IIOModuleManager::OLRButtonConfig& cfgOLR
                                            )
{
    try
    {
        /* Get a reference to the main configuration */
        CConfigurationT configurationRoot = getXMLElement<CConfigurationT>(configuration.configuration);

        /* Get site name */
        checkXMLElement(configurationRoot.siteName);
        string_type siteNameRTU = configurationRoot.siteName;
        statusManager.setSiteNameRTU(siteNameRTU);

        /* Get Config File modification date */
        checkXMLElement(configurationRoot.modificationDate);
        string_type configFileDate = configurationRoot.modificationDate;
        statusManager.setConfigFileTimestamp(configFileDate);

        string_type configFileDesc = configurationRoot.configFileDescription;
        statusManager.setConfigFileDesc(configFileDesc);

        /* Get Config File version */
        BasicVersionStr cfgVersion = {0, 0};    //default value
        getXMLOptAttr(configurationRoot.configVersionMajor, cfgVersion.major);
        getXMLOptAttr(configurationRoot.configVersionMinor, cfgVersion.minor);
        statusManager.setConfigFileVersion(cfgVersion);

        if( configurationRoot.miscellaneous.exists() )
        {
            CMiscellaneousT misc = getXMLElement<CMiscellaneousT>(configurationRoot.miscellaneous);

            /* Get synchronisation timeout */
            lu_uint64_t timeSyncTimeout_ms = 0;
            timeSyncTimeout_ms = getXMLAttr(misc.timeSyncTimeoutSec);
            timeSyncTimeout_ms *= 1000; //Convert to milliseconds
            TimeManager::getInstance().setSynchTimeoutTime(timeSyncTimeout_ms);

            /* Process Local time zone config */
            std::string localeFile;
            if(misc.timezone.exists())
            {
                localeFile = getXMLAttr(misc.timezone); //Point locale to the given file
            }
            else
            {
                /* FIXME: pueyos_a - For UKPN's TCL special case, we need to set the local time but tell the Time Manager to keep UTC */
                localeFile = "UTC";                     //Point locale back to UTC
            }
            //Store the UTC/locale usage
            bool useUTC =   (localeFile.compare("UTC") == 0) ||
                            (localeFile.compare("Etc/UTC") == 0) ||
                            (localeFile.compare("Etc/UCT") == 0) ||
                            (localeFile.compare("Etc/Universal") == 0) ||
                            (localeFile.compare("Etc/Zulu") == 0) ||
                            (localeFile.compare("Etc/GMT") == 0) ||
                            (localeFile.compare("Etc/GMT0") == 0) ||
                            (localeFile.compare("Etc/GMT+0") == 0) ||
                            (localeFile.compare("Etc/GMT-0") == 0) ||
                            (localeFile.compare("Etc/Greenwich") == 0);
            TimeManager::getInstance().setTimezoneUTC(useUTC);
            TimeManager::getInstance().setTimezoneName(localeFile);
            //Set time zone on the system
            std::stringstream localeCommand;
            localeCommand << LOCALE_SCRIPT << localeFile << " " << LOCALE_LINK_NAME;
            if(system(localeCommand.str().c_str()) != 0)
            {
                std::stringstream ss;
                ss << "Unable to set RTU locale to " << localeFile;
                throw ParserException(CONFMGR_ERROR_CONFIG, ss.str().c_str());
            }

            /* Get power saving time delay */
            lu_uint64_t powerSavingDelay_ms = 0;
            powerSavingDelay_ms = getXMLAttr(misc.powerSavingDelaySec);
            powerSavingDelay_ms *= 1000; //Convert to milliseconds
            statusManager.setPowerSavingDelay(powerSavingDelay_ms);

            /* Get MODE button config */
            checkXMLElement(misc.offLocalButtonSetting);
            cfgOLR.toggleTimeLR_ms = getXMLAttr(misc.offLocalButtonSetting.first().localRemoteToggleTime);
            cfgOLR.toggleTimeOFF_ms = getXMLAttr(misc.offLocalButtonSetting.first().offToggleTime);

            /* Get Protocol Stack init delay */
            lu_uint32_t protocolInitDelay = 0;  //default
            if(misc.protocolstack.exists())
            {
                getXMLOptAttr(misc.protocolstack.first().slaveProtocolStackDelay, protocolInitDelay);
                statusManager.setProtocolStackDelay(protocolInitDelay);
            }

            /* Get NTP-related config */
            bool ntpEnabled = false;
            if(misc.ntp.exists())
            {
                //Get NTP config
                CNTPConfigT ntp = getXMLElement<CNTPConfigT>(misc.ntp);
                getXMLOptAttr(ntp.enabled, ntpEnabled);
                if(ntpEnabled)
                {
                    //Check NTP config
                    std::string ntpConfig = getXMLAttr(ntp.script);
                    if(ntpConfig.length() > 2)
                    {
                        //Save file and apply config if not empty
                        std::ofstream ntpConfigFile(TEMP_NTP_CFG_FILE);
                        ntpConfigFile << ntpConfig;
                        ntpConfigFile.close();  //flush writing
                        if(system(NTP_SCRIPT) != 0)
                        {
                            throw ParserException(CONFMGR_ERROR_CONFIG, "Unable to set NTP configuration");
                        }
                    }

                    //Add synchronisation source: NTP
                    TimeManager::getInstance().addSyncSource(SYNC_SOURCE_NTP);
                }
                else
                {
                    //Outcome is ignored for not throwing error when repeatedly called
                    system(NTP_NOSCRIPT);
                }
            }

            /* Parse Syslog config */
           bool syslogEnabled = false;
           if(misc.syslog.exists())
           {
               //Get Syslog config
               CSyslogT syslog = getXMLElement<CSyslogT>(misc.syslog);
               getXMLOptAttr(syslog.enabled, syslogEnabled);
               if(syslogEnabled && syslog.syslogconf.exists())
               {
                   //Check Syslog config
                   std::string syslogConf = getXMLAttr(syslog.syslogconf);
                   if(syslogConf.length() > 2)
                   {
                       //Save file and apply config if not empty
                       std::ofstream syslogConfigFile("config/syslog.conf");
                       syslogConfigFile << syslogConf;
                       syslogConfigFile.close();  //flush writing
                   }
               }
           }

        }
    }
    catch(const ParserException& e)
    {
        //Report Event
        CTEventSystemStr event;
        event.eventType = EVENT_TYPE_SYSTEM_READ_CONFIG;
        event.value = EVENT_READ_CONFIG_VALUE_ERROR;
        eventLog->logEvent(event);

        log.error("%s Configuration file error: %s", __AT__, e.what());
        return CONFMGR_ERROR_INITPARSER;
    }

    return CONFMGR_ERROR_NONE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */

