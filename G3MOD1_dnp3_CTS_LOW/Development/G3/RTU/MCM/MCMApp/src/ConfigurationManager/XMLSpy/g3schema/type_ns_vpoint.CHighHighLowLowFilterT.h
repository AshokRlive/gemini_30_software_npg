#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CHighHighLowLowFilterT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CHighHighLowLowFilterT

#include "type_ns_vpoint.CHiHiLoLoT.h"


namespace g3schema
{

namespace ns_vpoint
{	

class CHighHighLowLowFilterT : public ::g3schema::ns_vpoint::CHiHiLoLoT
{
public:
	g3schema_EXPORT CHighHighLowLowFilterT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CHighHighLowLowFilterT(CHighHighLowLowFilterT const& init);
	void operator=(CHighHighLowLowFilterT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CHighHighLowLowFilterT); }
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CHighHighLowLowFilterT
