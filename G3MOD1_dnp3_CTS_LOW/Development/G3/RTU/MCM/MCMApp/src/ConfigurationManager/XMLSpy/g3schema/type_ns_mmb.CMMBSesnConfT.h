#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_mmb_ALTOVA_CMMBSesnConfT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_mmb_ALTOVA_CMMBSesnConfT



namespace g3schema
{

namespace ns_mmb
{	

class CMMBSesnConfT : public TypeBase
{
public:
	g3schema_EXPORT CMMBSesnConfT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CMMBSesnConfT(CMMBSesnConfT const& init);
	void operator=(CMMBSesnConfT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_mmb_altova_CMMBSesnConfT); }

	MemberAttribute<string_type,_altova_mi_ns_mmb_altova_CMMBSesnConfT_altova_sessionName, 0, 0> sessionName;	// sessionName Cstring

	MemberAttribute<unsigned,_altova_mi_ns_mmb_altova_CMMBSesnConfT_altova_slaveAddress, 0, 0> slaveAddress;	// slaveAddress CSlaveAddressT

	MemberAttribute<unsigned,_altova_mi_ns_mmb_altova_CMMBSesnConfT_altova_defaultResponseTimeout, 0, 0> defaultResponseTimeout;	// defaultResponseTimeout CunsignedInt
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_mmb

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_mmb_ALTOVA_CMMBSesnConfT
