#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CRootScreenT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CRootScreenT



namespace g3schema
{

namespace ns_hmi
{	

class CRootScreenT : public TypeBase
{
public:
	g3schema_EXPORT CRootScreenT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CRootScreenT(CRootScreenT const& init);
	void operator=(CRootScreenT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_hmi_altova_CRootScreenT); }

	MemberAttribute<int,_altova_mi_ns_hmi_altova_CRootScreenT_altova_rootScreenID, 0, 0> rootScreenID;	// rootScreenID Cint
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_hmi

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CRootScreenT
