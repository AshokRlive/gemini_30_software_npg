#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3ChnlTCPConfT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3ChnlTCPConfT

#include "type_ns_pstack.CTCPConfT.h"


namespace g3schema
{

namespace ns_sdnp3
{	

class CSDNP3ChnlTCPConfT : public ::g3schema::ns_pstack::CTCPConfT
{
public:
	g3schema_EXPORT CSDNP3ChnlTCPConfT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSDNP3ChnlTCPConfT(CSDNP3ChnlTCPConfT const& init);
	void operator=(CSDNP3ChnlTCPConfT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_sdnp3_altova_CSDNP3ChnlTCPConfT); }

	MemberAttribute<int,_altova_mi_ns_sdnp3_altova_CSDNP3ChnlTCPConfT_altova_masterAddress, 0, 0> masterAddress;	// masterAddress Cint

	MemberAttribute<bool,_altova_mi_ns_sdnp3_altova_CSDNP3ChnlTCPConfT_altova_validateMasterIP, 0, 0> validateMasterIP;	// validateMasterIP Cboolean

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CSDNP3ChnlTCPConfT_altova_dualEndPointIpPort, 0, 0> dualEndPointIpPort;	// dualEndPointIpPort CunsignedShort

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CSDNP3ChnlTCPConfT_altova_localUDPPort, 0, 0> localUDPPort;	// localUDPPort CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CSDNP3ChnlTCPConfT_altova_destUDPPort, 0, 0> destUDPPort;	// destUDPPort CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CSDNP3ChnlTCPConfT_altova_initUnsolUDPPort, 0, 0> initUnsolUDPPort;	// initUnsolUDPPort CunsignedInt

	MemberAttribute<bool,_altova_mi_ns_sdnp3_altova_CSDNP3ChnlTCPConfT_altova_disconnectOnNewSyn, 0, 0> disconnectOnNewSyn;	// disconnectOnNewSyn Cboolean
	MemberElement<ns_sdnp3::CConnectionManagerT, _altova_mi_ns_sdnp3_altova_CSDNP3ChnlTCPConfT_altova_connectionManager> connectionManager;
	struct connectionManager { typedef Iterator<ns_sdnp3::CConnectionManagerT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_sdnp3

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3ChnlTCPConfT
