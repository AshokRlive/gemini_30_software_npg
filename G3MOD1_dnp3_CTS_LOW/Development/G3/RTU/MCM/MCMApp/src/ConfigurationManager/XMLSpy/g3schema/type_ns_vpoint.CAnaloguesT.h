#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CAnaloguesT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CAnaloguesT



namespace g3schema
{

namespace ns_vpoint
{	

class CAnaloguesT : public TypeBase
{
public:
	g3schema_EXPORT CAnaloguesT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CAnaloguesT(CAnaloguesT const& init);
	void operator=(CAnaloguesT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CAnaloguesT); }
	MemberElement<ns_vpoint::CAnaloguePointT, _altova_mi_ns_vpoint_altova_CAnaloguesT_altova_analogue> analogue;
	struct analogue { typedef Iterator<ns_vpoint::CAnaloguePointT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CAnaloguesT
