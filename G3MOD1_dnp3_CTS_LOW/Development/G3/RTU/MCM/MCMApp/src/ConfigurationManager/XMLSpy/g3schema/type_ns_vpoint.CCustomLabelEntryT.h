#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CCustomLabelEntryT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CCustomLabelEntryT



namespace g3schema
{

namespace ns_vpoint
{	

class CCustomLabelEntryT : public TypeBase
{
public:
	g3schema_EXPORT CCustomLabelEntryT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CCustomLabelEntryT(CCustomLabelEntryT const& init);
	void operator=(CCustomLabelEntryT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CCustomLabelEntryT); }

	MemberAttribute<double,_altova_mi_ns_vpoint_altova_CCustomLabelEntryT_altova_value2, 0, 0> value2;	// value Cdecimal

	MemberAttribute<string_type,_altova_mi_ns_vpoint_altova_CCustomLabelEntryT_altova_label, 0, 0> label;	// label Cstring
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CCustomLabelEntryT
