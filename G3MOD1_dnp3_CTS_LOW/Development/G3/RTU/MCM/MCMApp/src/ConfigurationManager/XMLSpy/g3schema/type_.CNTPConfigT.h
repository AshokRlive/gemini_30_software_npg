#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CNTPConfigT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CNTPConfigT



namespace g3schema
{

class CNTPConfigT : public TypeBase
{
public:
	g3schema_EXPORT CNTPConfigT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CNTPConfigT(CNTPConfigT const& init);
	void operator=(CNTPConfigT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_altova_CNTPConfigT); }

	MemberAttribute<bool,_altova_mi_altova_CNTPConfigT_altova_enabled, 0, 0> enabled;	// enabled Cboolean

	MemberAttribute<string_type,_altova_mi_altova_CNTPConfigT_altova_script, 0, 0> script;	// script Cstring
	g3schema_EXPORT void SetXsiType();
};


}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CNTPConfigT
