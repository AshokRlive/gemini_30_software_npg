#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CCustomDebounceT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CCustomDebounceT



namespace g3schema
{

namespace ns_vpoint
{	

class CCustomDebounceT : public TypeBase
{
public:
	g3schema_EXPORT CCustomDebounceT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CCustomDebounceT(CCustomDebounceT const& init);
	void operator=(CCustomDebounceT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CCustomDebounceT); }

	MemberAttribute<unsigned __int64,_altova_mi_ns_vpoint_altova_CCustomDebounceT_altova_debounce0, 0, 0> debounce0;	// debounce0 CnonNegativeInteger

	MemberAttribute<unsigned __int64,_altova_mi_ns_vpoint_altova_CCustomDebounceT_altova_debounce1, 0, 0> debounce1;	// debounce1 CnonNegativeInteger

	MemberAttribute<unsigned __int64,_altova_mi_ns_vpoint_altova_CCustomDebounceT_altova_debounce2, 0, 0> debounce2;	// debounce2 CnonNegativeInteger

	MemberAttribute<unsigned __int64,_altova_mi_ns_vpoint_altova_CCustomDebounceT_altova_debounce3, 0, 0> debounce3;	// debounce3 CnonNegativeInteger
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CCustomDebounceT
