#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen11_ALTOVA_CEDGE_DRIVEN_MODE
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen11_ALTOVA_CEDGE_DRIVEN_MODE



namespace g3schema
{

namespace ns_gen11
{	

class CEDGE_DRIVEN_MODE : public TypeBase
{
public:
	g3schema_EXPORT CEDGE_DRIVEN_MODE(xercesc::DOMNode* const& init);
	g3schema_EXPORT CEDGE_DRIVEN_MODE(CEDGE_DRIVEN_MODE const& init);
	void operator=(CEDGE_DRIVEN_MODE const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen11_altova_CEDGE_DRIVEN_MODE); }

	enum EnumValues {
		Invalid = -1,
		k_EDGE_DRIVEN_MODE_RISE = 0, // EDGE_DRIVEN_MODE_RISE
		k_EDGE_DRIVEN_MODE_FALL = 1, // EDGE_DRIVEN_MODE_FALL
		k_EDGE_DRIVEN_MODE_BOTH = 2, // EDGE_DRIVEN_MODE_BOTH
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen11

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen11_ALTOVA_CEDGE_DRIVEN_MODE
