/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:XMLPortsConfigParser.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   30 Jun 2014     andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "PortManagerDebug.h"
#include "XMLParser.h"
#include "XMLPortConfigParser.h"
#include "ProtocolStackCommonEnum.h"

using namespace ns_port;

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
// Define a lookup table for the Baud Rates; the enum is not sequential)
static const lu_int32_t ConfToLinBaud[] = { LIN232_BAUD_110,
                                            LIN232_BAUD_300,
                                            LIN232_BAUD_600,
                                            LIN232_BAUD_1200,
                                            LIN232_BAUD_2400,
                                            LIN232_BAUD_4800,
                                            LIN232_BAUD_9600,
                                            LIN232_BAUD_19200,
                                            LIN232_BAUD_38400,
                                            LIN232_BAUD_57600,
                                            LIN232_BAUD_115200,
                                            LIN232_BAUD_230400,
                                            LIN232_BAUD_576000,
                                            LIN232_BAUD_921600,
                                            LIN232_BAUD_1152000 };

#define CONFTOLINBAUD_SIZE ((lu_int32_t)(sizeof(ConfToLinBaud) / sizeof(lu_int32_t)))

struct EthPortsStr
{
    std::string EthPortName;
    std::string EthPortIface;
};

static const EthPortsStr ConfToEthName[] = { { "ETHERNET_PORT_CONFIG",  "eth1" },
                                             { "ETHERNET_PORT_CONTROL", "eth0" } };

#define CONFTOETHNAME_SIZE   2

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

XMLPortConfigParser::XMLPortConfigParser(Cg3schema& configuration,
                                         PortManager& portManager) :
                                            configuration(configuration),
                                            portManager(portManager),
                                            log(Logger::getLogger(SUBSYSTEM_ID_CONFMGR))
{
    initParser();

    // Apply the port configuration changes to the OS
    portManager.applyConfig();
}

XMLPortConfigParser::~XMLPortConfigParser()
{}


CONFMGR_ERROR XMLPortConfigParser::initParser()
{
    Port *port;

    try
    {
        checkXMLElement(configuration.configuration);
        checkXMLElement(configuration.configuration.first().ports);

        CPortsT ports = configuration.configuration.first().ports.first();

        if (ports.serial.exists())
        {
            // Iterate through the serial ports
            for (Iterator<CSerialPortConfT> itPort = ports.serial.all();
                 itPort;
                 ++itPort)
            {
                port = new Port();
                port->portType = Port::PORT_TYPE_SERIAL;

                std::string settingStr = getXMLAttr(itPort.portPath);
                port->setPortPath(settingStr);

                settingStr = getXMLAttr(itPort.portName);
                port->setPortName(settingStr);

                // Get the baud rate from the LookUp Array as the LINIO Enum is not sequential
                if ((itPort.baudRate.GetEnumerationValue() >= 0) && (itPort.baudRate.GetEnumerationValue() < CONFTOLINBAUD_SIZE))
                {
                    port->serialConf.serBaudrate = (LIN232_BAUD_RATE) ConfToLinBaud[itPort.baudRate.GetEnumerationValue()];
                }

                port->serialConf.serDataBits = getXMLAttrEnum<LIN232_DATA_BITS>(itPort.numDataBits, LU_LIN232_DATA_BITS_VALUEfromINDEX);
                port->serialConf.serStopBits = getXMLAttrEnum<LIN232_STOP_BITS>(itPort.numStopBits, LU_LIN232_STOP_BITS_VALUEfromINDEX);
                port->serialConf.serParity   = getXMLAttrEnum<LIN232_PARITY>(itPort.parity,         LU_LIN232_PARITY_VALUEfromINDEX);
                port->serialConf.serPortMode = getXMLAttrEnum<LIN232_PORT_MODE>(itPort.portMode,    LU_LIN232_PORT_MODE_VALUEfromINDEX);
                port->serialConf.serCTSlowCheck = getXMLAttr(itPort.ctLowDiscardMsg);

                // Add the port to the portManager
                if (portManager.addPort(port) != 0)
                {
                    log.error("XMLPortConfigParser: Unable to add port [%s]", port->getPortName().c_str());
                    return CONFMGR_ERROR_CONFIG;
                }
            }
        }

        if (ports.ethernet.exists())
        {
            // Iterate through the Ethernet ports
            for (Iterator<CEthernetPortConfT> itPort = ports.ethernet.all();
                 itPort;
                 ++itPort)
            {
                port = new Port();
                port->portType = Port::PORT_TYPE_ETHERNET;

                std::string settingStr = getXMLAttr(itPort.portName);
                port->setPortName(settingStr);

                // Lookup the portName to get the interface name (e.g. eth0)
                for (lu_uint32_t i = 0; i < CONFTOETHNAME_SIZE; i++)
                {
                    if (ConfToEthName[i].EthPortName == port->getPortName())
                    {
                        port->setPortPath(ConfToEthName[i].EthPortIface);
                    }
                }

                // If the ipDHCPClient node exists then we are a DHCP client
                port->ethernetConf.ipDHCPClient = LU_FALSE;

                checkXMLElement(itPort.ipv4Setting);
				CIpv4SettingT ipv4 = itPort.ipv4Setting.first();


                if (ipv4.dynamicIP.exists())
                {
                    port->ethernetConf.ipDHCPClient = LU_TRUE;
                    DBG_INFO("Ethernet Port [%s] is a DHCP Client\n", port->getPortName().c_str());
                }
                else
                {
                    // Get the Static IP Settings
                    if (ipv4.staticIP.exists())
                    {
                        port->ethernetConf.ipAddress   = getXMLAttr(ipv4.staticIP.first().ipaddress);
                        port->ethernetConf.ipMask      = getXMLAttr(ipv4.staticIP.first().submask);
                        port->ethernetConf.ipGateway   = getXMLAttr(ipv4.staticIP.first().gateway);
                        port->ethernetConf.ipBroadcast = getXMLAttr(ipv4.staticIP.first().broadcast);
                        port->ethernetConf.ipDNS1      = getXMLAttr(ipv4.staticIP.first().DNSServer);

                        // DNS Server2 is optional
                        if (ipv4.staticIP.first().DNSServer2.exists())
                        {
                            port->ethernetConf.ipDNS2 = ipv4.staticIP.first().DNSServer2;
                        }
                    }
                }

                // Get the DHCP Server Settings
                port->ethernetConf.dhcpServer.enabled = LU_FALSE;

                if (ipv4.dhcpServer.exists())
                {
                    port->ethernetConf.dhcpServer.enabled = LU_TRUE;


                    DBG_INFO("Ethernet Port [%s] is a DHCP Server\n", port->getPortName().c_str());

                    port->ethernetConf.dhcpServer.startAddress = getXMLAttr(ipv4.dhcpServer.first().startIP);
                    port->ethernetConf.dhcpServer.endAddress   = getXMLAttr(ipv4.dhcpServer.first().endIP);
                    port->ethernetConf.dhcpServer.subnet       = getXMLAttr(ipv4.dhcpServer.first().submask);
                    port->ethernetConf.dhcpServer.router       = getXMLAttr(ipv4.dhcpServer.first().router);
                }

                // TODO - SKA - Add the Ethernet Config + DHCPServer as required
                if (portManager.addPort(port) != 0)
                {
                    log.error("XMLPortConfigParser: Unable to add port [%s]", port->getPortName().c_str());
                    return CONFMGR_ERROR_CONFIG;
                }
            }
        }

        return CONFMGR_ERROR_NONE;
    }
    catch (const ParserException& e)
    {
        log.error("PortConfigParser error: %s", e.what());
    }
    catch(...)
    {
        log.error("XMLPortConfigParser: Exception raised");
    }

    return CONFMGR_ERROR_CONFIG;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
