#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_xs_ALTOVA_Cboolean
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_xs_ALTOVA_Cboolean



namespace g3schema
{

namespace xs
{	

class Cboolean : public TypeBase
{
public:
	g3schema_EXPORT Cboolean(xercesc::DOMNode* const& init);
	g3schema_EXPORT Cboolean(Cboolean const& init);
	void operator=(Cboolean const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_xs_altova_Cboolean); }
	void operator= (const bool& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator bool()
	{
		return CastAs<bool >::Do(GetNode(), 0);
	}
};



} // namespace xs

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_xs_ALTOVA_Cboolean
