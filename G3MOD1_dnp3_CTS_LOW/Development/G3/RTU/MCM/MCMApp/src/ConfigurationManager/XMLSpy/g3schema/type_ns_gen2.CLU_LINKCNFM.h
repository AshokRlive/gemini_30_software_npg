#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen2_ALTOVA_CLU_LINKCNFM
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen2_ALTOVA_CLU_LINKCNFM



namespace g3schema
{

namespace ns_gen2
{	

class CLU_LINKCNFM : public TypeBase
{
public:
	g3schema_EXPORT CLU_LINKCNFM(xercesc::DOMNode* const& init);
	g3schema_EXPORT CLU_LINKCNFM(CLU_LINKCNFM const& init);
	void operator=(CLU_LINKCNFM const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen2_altova_CLU_LINKCNFM); }

	enum EnumValues {
		Invalid = -1,
		k_LU_LINKCNFM_NEVER = 0, // LU_LINKCNFM_NEVER
		k_LU_LINKCNFM_SOMETIMES = 1, // LU_LINKCNFM_SOMETIMES
		k_LU_LINKCNFM_ALWAYS = 2, // LU_LINKCNFM_ALWAYS
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen2

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen2_ALTOVA_CLU_LINKCNFM
