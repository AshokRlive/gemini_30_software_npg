#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_mmb_ALTOVA_CMMBSesnT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_mmb_ALTOVA_CMMBSesnT



namespace g3schema
{

namespace ns_mmb
{	

class CMMBSesnT : public TypeBase
{
public:
	g3schema_EXPORT CMMBSesnT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CMMBSesnT(CMMBSesnT const& init);
	void operator=(CMMBSesnT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_mmb_altova_CMMBSesnT); }

	MemberAttribute<string_type,_altova_mi_ns_mmb_altova_CMMBSesnT_altova_deviceName, 0, 0> deviceName;	// deviceName Cstring

	MemberAttribute<string_type,_altova_mi_ns_mmb_altova_CMMBSesnT_altova_deviceModel, 0, 0> deviceModel;	// deviceModel Cstring

	MemberAttribute<unsigned,_altova_mi_ns_mmb_altova_CMMBSesnT_altova_deviceId, 0, 0> deviceId;	// deviceId CunsignedByte

	MemberAttribute<bool,_altova_mi_ns_mmb_altova_CMMBSesnT_altova_byteswap, 0, 0> byteswap;	// byteswap Cboolean

	MemberAttribute<bool,_altova_mi_ns_mmb_altova_CMMBSesnT_altova_wordswap, 0, 0> wordswap;	// wordswap Cboolean

	MemberAttribute<int,_altova_mi_ns_mmb_altova_CMMBSesnT_altova_offlinePollPeriod, 0, 0> offlinePollPeriod;	// offlinePollPeriod Cint

	MemberAttribute<unsigned,_altova_mi_ns_mmb_altova_CMMBSesnT_altova_numberOfRetries, 0, 0> numberOfRetries;	// numberOfRetries CunsignedByte

	MemberAttribute<string_type,_altova_mi_ns_mmb_altova_CMMBSesnT_altova_classifier, 0, 0> classifier;	// classifier Cstring

	MemberAttribute<bool,_altova_mi_ns_mmb_altova_CMMBSesnT_altova_enabled, 0, 0> enabled;	// enabled Cboolean

	MemberAttribute<string_type,_altova_mi_ns_mmb_altova_CMMBSesnT_altova_uuid, 0, 0> uuid;	// uuid Cstring
	MemberElement<ns_mmb::CMMBSesnConfT, _altova_mi_ns_mmb_altova_CMMBSesnT_altova_config> config;
	struct config { typedef Iterator<ns_mmb::CMMBSesnConfT> iterator; };
	MemberElement<ns_mmb::CMMBIOMapT, _altova_mi_ns_mmb_altova_CMMBSesnT_altova_iomap> iomap;
	struct iomap { typedef Iterator<ns_mmb::CMMBIOMapT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_mmb

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_mmb_ALTOVA_CMMBSesnT
