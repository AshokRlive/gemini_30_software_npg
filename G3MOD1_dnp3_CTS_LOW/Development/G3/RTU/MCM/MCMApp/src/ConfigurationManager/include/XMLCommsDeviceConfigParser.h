/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:XMLCommsDeviceConfigParser.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   30 Jun 2014     andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef XMLCOMMSDEVICECONFIGPARSER_H_
#define XMLCOMMSDEVICECONFIGPARSER_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ********************************** ********************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "ConfigurationManagerError.h"
#include "CommsDeviceManager.h"
#include "DialupModem.h"
#include "GPRSModem.h"

#include "PortManager.h"
#include "XMLParser.h"

#include "g3schema/g3schema.h"

using namespace g3schema;
using namespace altova;


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief XML Parser for general configuration items
 *
 * Parses the XML Configuration file for general configuration items.
 */
class XMLCommsDeviceConfigParser
{

public:
    /**
     * \brief Custom constructor
     *
     * \param configuration     Ref to Cg3schema XML configuration
     * \param commDeviceManager Ref to CommDeviceManager
     * \param portManager       Ref to Port manager
     * \param moduleManager     Ref to Module manager
     */
    XMLCommsDeviceConfigParser(Cg3schema&          configuration,
                               CommsDeviceManager& commsDeviceManager,
                               PortManager&        portManager,
                               IIOModuleManager&   moduleManager);


    virtual ~XMLCommsDeviceConfigParser();

    CONFMGR_ERROR initParser();

private:
    Cg3schema&          configuration;
    CommsDeviceManager& commsDeviceManager;
    PortManager&        portManager;
    IIOModuleManager&   moduleManager;

    Logger&             log;
};



#endif /* XMLCOMMSDEVICECONFIGPARSER_H_ */

/*
 *********************** End of file ******************************************
 */
