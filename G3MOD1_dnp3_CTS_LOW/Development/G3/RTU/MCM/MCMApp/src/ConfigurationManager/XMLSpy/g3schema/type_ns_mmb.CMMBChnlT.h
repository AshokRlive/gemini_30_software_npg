#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_mmb_ALTOVA_CMMBChnlT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_mmb_ALTOVA_CMMBChnlT



namespace g3schema
{

namespace ns_mmb
{	

class CMMBChnlT : public TypeBase
{
public:
	g3schema_EXPORT CMMBChnlT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CMMBChnlT(CMMBChnlT const& init);
	void operator=(CMMBChnlT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_mmb_altova_CMMBChnlT); }
	MemberElement<ns_mmb::CMMBChnlConfT, _altova_mi_ns_mmb_altova_CMMBChnlT_altova_config> config;
	struct config { typedef Iterator<ns_mmb::CMMBChnlConfT> iterator; };
	MemberElement<ns_mmb::CMMBSesnT, _altova_mi_ns_mmb_altova_CMMBChnlT_altova_session> session;
	struct session { typedef Iterator<ns_mmb::CMMBSesnT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_mmb

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_mmb_ALTOVA_CMMBChnlT
