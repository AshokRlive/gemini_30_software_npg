#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CSwitchTemplateParamT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CSwitchTemplateParamT



namespace g3schema
{

namespace ns_template
{	

class CSwitchTemplateParamT : public TypeBase
{
public:
	g3schema_EXPORT CSwitchTemplateParamT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSwitchTemplateParamT(CSwitchTemplateParamT const& init);
	void operator=(CSwitchTemplateParamT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_template_altova_CSwitchTemplateParamT); }
	MemberElement<ns_template::CParameterT, _altova_mi_ns_template_altova_CSwitchTemplateParamT_altova_parameter> parameter;
	struct parameter { typedef Iterator<ns_template::CParameterT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_template

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CSwitchTemplateParamT
