#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen2_ALTOVA_CLU_LINUDP_MODE
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen2_ALTOVA_CLU_LINUDP_MODE



namespace g3schema
{

namespace ns_gen2
{	

class CLU_LINUDP_MODE : public TypeBase
{
public:
	g3schema_EXPORT CLU_LINUDP_MODE(xercesc::DOMNode* const& init);
	g3schema_EXPORT CLU_LINUDP_MODE(CLU_LINUDP_MODE const& init);
	void operator=(CLU_LINUDP_MODE const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen2_altova_CLU_LINUDP_MODE); }

	enum EnumValues {
		Invalid = -1,
		k_LU_LINUDP_MODE_UDP = 0, // LU_LINUDP_MODE_UDP
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen2

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen2_ALTOVA_CLU_LINUDP_MODE
