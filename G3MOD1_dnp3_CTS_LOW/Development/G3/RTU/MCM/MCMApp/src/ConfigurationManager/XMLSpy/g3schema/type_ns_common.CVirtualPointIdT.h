#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_CVirtualPointIdT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_CVirtualPointIdT



namespace g3schema
{

namespace ns_common
{	

class CVirtualPointIdT : public TypeBase
{
public:
	g3schema_EXPORT CVirtualPointIdT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CVirtualPointIdT(CVirtualPointIdT const& init);
	void operator=(CVirtualPointIdT const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_common_altova_CVirtualPointIdT); }
	void operator= (const unsigned& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::IntegerFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator unsigned()
	{
		return CastAs<unsigned >::Do(GetNode(), 0);
	}
};



} // namespace ns_common

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_CVirtualPointIdT
