#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CIncomingConnectionsT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CIncomingConnectionsT



namespace g3schema
{

namespace ns_sdnp3
{	

class CIncomingConnectionsT : public TypeBase
{
public:
	g3schema_EXPORT CIncomingConnectionsT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CIncomingConnectionsT(CIncomingConnectionsT const& init);
	void operator=(CIncomingConnectionsT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_sdnp3_altova_CIncomingConnectionsT); }

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CIncomingConnectionsT_altova_notReceivedHours, 0, 0> notReceivedHours;	// notReceivedHours CunsignedInt
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_sdnp3

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CIncomingConnectionsT
