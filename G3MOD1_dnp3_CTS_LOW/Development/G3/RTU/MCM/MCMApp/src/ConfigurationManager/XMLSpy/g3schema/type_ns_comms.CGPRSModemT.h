#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_comms_ALTOVA_CGPRSModemT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_comms_ALTOVA_CGPRSModemT

#include "type_ns_comms.CBaseModemT.h"


namespace g3schema
{

namespace ns_comms
{	

class CGPRSModemT : public ::g3schema::ns_comms::CBaseModemT
{
public:
	g3schema_EXPORT CGPRSModemT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CGPRSModemT(CGPRSModemT const& init);
	void operator=(CGPRSModemT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_comms_altova_CGPRSModemT); }

	MemberAttribute<string_type,_altova_mi_ns_comms_altova_CGPRSModemT_altova_apn, 0, 0> apn;	// apn Cstring

	MemberAttribute<string_type,_altova_mi_ns_comms_altova_CGPRSModemT_altova_user, 0, 0> user;	// user Cstring

	MemberAttribute<string_type,_altova_mi_ns_comms_altova_CGPRSModemT_altova_password, 0, 0> password;	// password Cstring
	MemberElement<ns_comms::CScriptFileT, _altova_mi_ns_comms_altova_CGPRSModemT_altova_ScriptFile> ScriptFile;
	struct ScriptFile { typedef Iterator<ns_comms::CScriptFileT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_comms

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_comms_ALTOVA_CGPRSModemT
