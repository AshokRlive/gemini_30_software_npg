/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:XMLModbusConfigParser.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   12 Jun 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <string.h>
#include <assert.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "XMLModbusConfigParser.h"
#include "MBDebug.h"
#include "ProtocolStackCommonEnum.h"
#include "ModBusEnum.h"

using namespace ns_pstack;
using namespace ns_mmb;

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static inline void getLinTCPConf(mbm::LinTCPConf& tcpconf, const CTCPConfT& xml_tcp) throw(ParserException);
static inline void getLin232Conf(mbm::Lin232Conf& serialconf, const Port& portObj);
static inline void getMBLinkConf(mbm::MBLinkConf& mblinkconf, const CMMBChnlConfT& xml_chnl_conf) throw(ParserException);
static inline void getMBDeviceChConf( mbm::ChannelConf& ch, const CMMBDeviceChnlT& xml_ch) throw(ParserException);
static inline void getMBDeviceChConf(mbm::ChannelConf& ch, const CMMBDeviceChnlT& xml_ch,const CMMBDeviceChnlAnalogT& xml_ana_ch ) throw(ParserException);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
XMLModbusConfigParser::XMLModbusConfigParser(Cg3schema& configuration,PortManager& portMgr):
                                        log(Logger::getLogger(SUBSYSTEM_ID_CONFMGR)),
                                        m_configuration(configuration),
                                        m_devNum(0),
                                        mp_devconf(NULL),
                                        m_portManager(portMgr)


{
    DBG_CTOR();

    initParser();
}

XMLModbusConfigParser::~XMLModbusConfigParser()
{
    DBG_DTOR();
    delete[] mp_devconf;
}

union MBChannelIOConf
{
    mbm::SerialDeviceConf serialDevConf;
    mbm::TCPDeviceConf tcpDevConf;
};

CONFMGR_ERROR XMLModbusConfigParser::initParser()
{
    if(mp_devconf != NULL)
        return CONFMGR_ERROR_INITIALIZED; // Already initialised

    lu_int16_t totalSesnNum = 0;

    try
    {
        checkXMLElement(m_configuration.configuration);
        checkXMLElement(m_configuration.configuration.first().fieldDevices);
        checkXMLElement(m_configuration.configuration.first().fieldDevices.first().ModbusMaster);
    }
    catch(...)
    {
        //No Field Devices nor Modbus configuration found: nothing to do
        return CONFMGR_ERROR_NONE;
    };

    try
    {
        CConfigurationT xml_config  = m_configuration.configuration.first();
        CFieldDevicesT xml_fdev = xml_config.fieldDevices.first();

        /* Count channel and session number */
        CModbusMasterT xml_mbm = xml_fdev.ModbusMaster.first();
        const lu_int16_t chnlNum = xml_mbm.channel.count();
        for (lu_int16_t i = 0; i < chnlNum; ++i) {
            totalSesnNum += xml_mbm.channel[i].session.count();
        }
        DevConfWrapper* alldev = new DevConfWrapper[totalSesnNum];
        DBG_INFO("ModBus Master channel num:%i, session num:%i",chnlNum,totalSesnNum);

        /*
         * Parse all channel and session fields.
         */
        lu_int16_t              devIdx = 0;

        mbm::MMBSessionConf     sesnconf;
        mbm::MBLinkConf         mblinkconf;
        mbm::Lin232Conf         serialconf;
        mbm::LinTCPConf         tcpconf;

        memset(&sesnconf,   0,sizeof(sesnconf));
        memset(&mblinkconf, 0,sizeof(mblinkconf));
        memset(&serialconf, 0,sizeof(serialconf));
        memset(&tcpconf,    0,sizeof(tcpconf));



        /*Parse MB Channel*/
        for (int mbchlIdx = 0; mbchlIdx < chnlNum; ++mbchlIdx)
        {
            CMMBChnlT     xml_chnl = xml_mbm.channel[mbchlIdx];
            CMMBChnlConfT  xml_chnl_conf = xml_chnl.config.first();

            getMBLinkConf(mblinkconf, xml_chnl_conf);

            std::string chnlName = xml_chnl_conf.chnlName;
            lu_uint8_t deviceType = 0;
            lu_bool_t channelActive = xml_chnl_conf.enabled;

            /* Parse Serial*/
            if(xml_chnl_conf.serial.exists())
            {
               CSerialConfT xml_seril = xml_chnl_conf.serial.first();

               chnlName.copy(serialconf.chnlName, chnlName.size(), 0);
               deviceType = mbm::DEVICE_TYPE_SERIAL;

               /* Apply the settings from this Port object to the LinIo Library*/
               Port* portObj;
               std::string portName(xml_seril.serialPortName);
               if ((portObj = m_portManager.getPort(portName)) != NULL)
               {
                   // Use the settings from the Port Object
                   getLin232Conf(serialconf, *portObj);
               }
               else
               {
                   DBG_ERR("Port [%s] not found\n", portName.c_str());
               }
            }

            /* Parse TCP config*/
            if(xml_chnl_conf.tcp.exists())
            {
                CTCPConfT xml_tcp = xml_chnl_conf.tcp.first();

                deviceType = mbm::DEVICE_TYPE_TCP;
                chnlName.copy(tcpconf.chnlName, chnlName.size(), 0);

                getLinTCPConf(tcpconf, xml_tcp);
            }

            /* Parse session*/
            const lu_uint16_t sesncount = xml_chnl.session.count();
            for (int mbSesnIdx = 0; mbSesnIdx < sesncount; ++mbSesnIdx)
             {

                CMMBSesnT xml_sesn = xml_chnl.session[mbSesnIdx];
                mbm::MMBSessionConf* sesnconf = NULL;

                assert(devIdx < totalSesnNum);

                mbm::DeviceConf& dev = alldev[devIdx].devconf;
                dev.ref.deviceId = devIdx;
                dev.ref.deviceType = deviceType;

                if(xml_chnl_conf.serial.exists())
                {
                    dev.conf.serialDevConf.mblinkconf = mblinkconf ;
                    dev.conf.serialDevConf.serialconf = serialconf;
                    sesnconf = &(dev.conf.serialDevConf.sesnconf);
                }

                if(xml_chnl_conf.tcp.exists())
                {
                    dev.conf.tcpDevConf.mblinkconf  = mblinkconf ;
                    dev.conf.tcpDevConf.tcpconf     = tcpconf;
                    sesnconf = &(dev.conf.tcpDevConf.sesnconf);
                }

                // Parse session conf
                CMMBSesnConfT xml_sesn_conf       = xml_sesn.config.first();
                sesnconf->defaultResponseTimeout = xml_sesn_conf.defaultResponseTimeout;
                sesnconf->slaveAddress           = xml_sesn_conf.slaveAddress;

                // Parse session iomap
                CMMBIOMapT xml_sesn_iomap = xml_sesn.iomap.first();
                dev.aichlNum = xml_sesn_iomap.AIChs.count();
                dev.dichlNum = xml_sesn_iomap.DIChs.count();
                dev.dochlNum = xml_sesn_iomap.DOChs.count();
                dev.numOfRetries = getXMLAttr(xml_sesn.numberOfRetries);
                dev.offLinePollingRate = getXMLAttr(xml_sesn.offlinePollPeriod);
                //for the session to be active the channel needs to be active
                dev.active = getXMLAttr(xml_sesn.enabled) && channelActive;
                dev.byteswap = getXMLAttr(xml_sesn.byteswap);
                dev.wordswap = getXMLAttr(xml_sesn.wordswap);

                alldev[devIdx].aichs = new mbm::ChannelConf[dev.aichlNum];
                alldev[devIdx].dichs = new mbm::ChannelConf[dev.dichlNum];
                alldev[devIdx].dochs = new mbm::ChannelConf[dev.dochlNum];

                memset(alldev[devIdx].aichs, 0, sizeof(mbm::ChannelConf)*dev.aichlNum);
                memset(alldev[devIdx].dichs, 0, sizeof(mbm::ChannelConf)*dev.dichlNum);
                memset(alldev[devIdx].dochs, 0, sizeof(mbm::ChannelConf)*dev.dochlNum);

                // Parse AI channels
                for (lu_uint16_t chIdx = 0; chIdx < dev.aichlNum; ++chIdx)
                {
                    CMMBDeviceChnlT xml_ch = xml_sesn_iomap.AIChs[chIdx];
                    CMMBDeviceChnlAnalogT xml_ana_ch = xml_sesn_iomap.AIChs[chIdx];

                    mbm::ChannelConf& ch = alldev[devIdx].aichs[chIdx];

                    ch.channel.channelId = chIdx;
                    ch.channel.channelType = CHANNEL_TYPE_AINPUT;
                    ch.channel.device = dev.ref;

                    getMBDeviceChConf(ch, xml_ch, xml_ana_ch);
                }

                // Parse DI channels
                for (lu_uint16_t chIdx = 0; chIdx < dev.dichlNum; ++chIdx)
                {
                    CMMBDeviceChnlT xml_ch = xml_sesn_iomap.DIChs[chIdx];
                    mbm::ChannelConf& ch = alldev[devIdx].dichs[chIdx];

                    ch.channel.channelId = chIdx;
                    ch.channel.channelType = CHANNEL_TYPE_DINPUT;
                    ch.channel.device = dev.ref;

                    getMBDeviceChConf(ch, xml_ch);
                }

                // Parse DO channels
                for (lu_uint16_t chIdx = 0; chIdx < dev.dochlNum; ++chIdx)
                {
                    CMMBDeviceChnlT xml_ch = xml_sesn_iomap.DOChs[chIdx];
                    mbm::ChannelConf& ch = alldev[devIdx].dochs[chIdx];

                    ch.channel.channelId = chIdx;
                    ch.channel.channelType = CHANNEL_TYPE_DOUTPUT;
                    ch.channel.device = dev.ref;

                    getMBDeviceChConf(ch, xml_ch);
                }

                devIdx ++;
            }
        }

        this->m_devNum = totalSesnNum;
        this->mp_devconf = alldev;
    }
    catch (...)
    {
        log.error("%s Exception raised", __AT__);
        return CONFMGR_ERROR_INITIALIZED;
    }

    return CONFMGR_ERROR_NONE;
}


lu_uint8_t XMLModbusConfigParser::getDeviceNum()
{
    return m_devNum;
}


 void XMLModbusConfigParser::getDeviceConf(const lu_uint8_t deviceIdx,
                                           mbm::DeviceConf* conf)
{
    if (checkDeviceId(deviceIdx))
        *conf = mp_devconf[deviceIdx].devconf;
    else
    {
        DBG_ERR("Invalid device index:%i", deviceIdx);
    }
}


lu_int32_t XMLModbusConfigParser::getChannelNum(const lu_uint8_t deviceIdx, CHANNEL_TYPE type)
{
     if(checkDeviceId(deviceIdx))
     {
         /* TODO: pueyos_a - why is there no NULL checking in mp_devConf[].devconf??? */
         if(type == CHANNEL_TYPE_AINPUT)
             return  mp_devconf[deviceIdx].devconf.aichlNum;
         else if(type == CHANNEL_TYPE_DINPUT)
             return  mp_devconf[deviceIdx].devconf.dichlNum;
         else if(type == CHANNEL_TYPE_DOUTPUT)
             return  mp_devconf[deviceIdx].devconf.dochlNum;
         else
         {
             DBG_ERR("Unsupported channel type:%i",type);
         }
     }

     return 0;
}


void XMLModbusConfigParser::getChannelConf(const mbm::ChannelRef& ch, mbm::ChannelConf* conf)
{
    memset(conf, 0, sizeof(*conf));
    conf->channel = ch;

    if (checkDeviceId(ch.device.deviceId) == false || checkChannelRef(ch) == false)
    {
        return;
    }

    if (ch.channelType == CHANNEL_TYPE_AINPUT)
        *conf = mp_devconf[ch.device.deviceId].aichs[ch.channelId];
    else if (ch.channelType == CHANNEL_TYPE_DINPUT)
        *conf = mp_devconf[ch.device.deviceId].dichs[ch.channelId];
    else if (ch.channelType == CHANNEL_TYPE_DOUTPUT)
        *conf = mp_devconf[ch.device.deviceId].dochs[ch.channelId];
    else
    {
        DBG_ERR("Unsupported channel type:%i",ch.channelType);
    }

}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
bool XMLModbusConfigParser::checkDeviceId(const lu_uint8_t deviceIdx)
{

    if(deviceIdx >= m_devNum)
    {
        DBG_ERR("Invalid device idx: %i",deviceIdx);
        return false;
    }

    return true;
}

bool XMLModbusConfigParser::checkChannelRef(const mbm::ChannelRef& ch)
{

    lu_uint8_t deviceIdx = ch.device.deviceId;
    if(checkDeviceId(deviceIdx) == false)
    {
        return false;
    }

    const lu_uint16_t chId = ch.channelId;
    const lu_uint8_t chType = ch.channelType;

    if(chType == CHANNEL_TYPE_DINPUT)
    {
        if(chId < mp_devconf[deviceIdx].devconf.dichlNum)
            return true;
    }

    else if(chType == CHANNEL_TYPE_AINPUT)
    {
        if(chId < mp_devconf[deviceIdx].devconf.aichlNum)
            return true;
    }

    else if(chType == CHANNEL_TYPE_DOUTPUT)
    {
        if(chId < mp_devconf[deviceIdx].devconf.dochlNum)
            return true;
    }

    else
    {
        DBG_ERR("Invalid channel idx: %i type:%i",chId,chType);
    }
    return false;
}

static inline void getMBLinkConf(mbm::MBLinkConf& mblinkconf, const CMMBChnlConfT& xml_chnl_conf) throw(ParserException)
{
    mblinkconf.channelResponseTimeout = getXMLAttr(xml_chnl_conf.channelResponseTimeout);
    mblinkconf.type = getXMLAttrEnum<LU_MBLINK_TYPE>(xml_chnl_conf.mbType, LU_MBLINK_TYPE_VALUEfromINDEX);
    mblinkconf.maxQueueSize = getXMLAttr(xml_chnl_conf.maxQueueSize);
    mblinkconf.rxFrameTimeout = getXMLAttr(xml_chnl_conf.rxFrameTimeout);
}


static inline void getLin232Conf(mbm::Lin232Conf& serialconf, const Port& portObj)
{
    serialconf.baudRate = portObj.serialConf.serBaudrate;
    serialconf.numDataBits = portObj.serialConf.serDataBits;
    serialconf.numStopBits = portObj.serialConf.serStopBits;
    serialconf.parity = portObj.serialConf.serParity;
    serialconf.portMode = portObj.serialConf.serPortMode;
    serialconf.toPortName(portObj.getPortPath());

    /* Hard-coded configuration fields */
    serialconf.bModbusRTU = TMWDEFS_TRUE;
}

static inline void getLinTCPConf(mbm::LinTCPConf& tcpconf, const CTCPConfT& xml_tcp) throw(ParserException)
{
    tcpconf.ipConnectTimeout = getXMLAttr(xml_tcp.ipConnectTimeout);
    tcpconf.ipPort = getXMLAttr(xml_tcp.ipPort);
    tcpconf.mode = getXMLAttrEnum<LU_LINTCP_MODE>(xml_tcp.mode, LU_LINTCP_MODE_VALUEfromINDEX);
    //tcpconf.role = getXMLAttrEnum<LU_LINTCP_ROLE>(xml_tcp.mode, LU_LINTCP_ROLE_VALUEfromINDEX); //role has been removed from config and it shall be forced by MCM
    tcpconf.toIPAddress(getXMLAttr(xml_tcp.ipAddress));
}

static inline void getMBDeviceChConf(mbm::ChannelConf& ch, const CMMBDeviceChnlT& xml_ch) throw(ParserException)
{
    ch.datatype = getXMLAttr(xml_ch.dataType);
    ch.pollingRate = getXMLAttr(xml_ch.pollRate);
    ch.reg = getXMLAttr(xml_ch.regAddress);
    ch.regType = getXMLAttr(xml_ch.regType);
    ch.bitmask = getXMLAttr(xml_ch.bitMask);
}

/*Overloaded function for parsing analog channel parameters(scaling, offset and units)*/
static inline void getMBDeviceChConf(mbm::ChannelConf& ch, const CMMBDeviceChnlT& xml_ch,const CMMBDeviceChnlAnalogT& xml_ana_ch ) throw(ParserException)
{
    std::string unit = getXMLAttr(xml_ana_ch.unit);
    ch.datatype = getXMLAttr(xml_ch.dataType);
    ch.pollingRate = getXMLAttr(xml_ch.pollRate);
    ch.reg = getXMLAttr(xml_ch.regAddress);
    ch.regType = getXMLAttr(xml_ch.regType);
    ch.bitmask = getXMLAttr(xml_ch.bitMask);

    strcpy(ch.unit,unit.c_str());
    ch.scalingFactor = getXMLAttr(xml_ana_ch.scalingFactor);
    ch.offset = getXMLAttr(xml_ana_ch.offset);
    strcpy(ch.unit,unit.c_str());
}

/*
 *********************** End of file ******************************************
 */
