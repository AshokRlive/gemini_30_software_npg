#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CCLogicBinaryPointT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CCLogicBinaryPointT

#include "type_ns_vpoint.CBaseBPointT.h"


namespace g3schema
{

namespace ns_clogic
{	

class CCLogicBinaryPointT : public ::g3schema::ns_vpoint::CBaseBPointT
{
public:
	g3schema_EXPORT CCLogicBinaryPointT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CCLogicBinaryPointT(CCLogicBinaryPointT const& init);
	void operator=(CCLogicBinaryPointT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_clogic_altova_CCLogicBinaryPointT); }
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_clogic

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CCLogicBinaryPointT
