#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CModbusTemplateT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CModbusTemplateT

#include "type_ns_mmb.CMMBSesnT.h"


namespace g3schema
{

namespace ns_template
{	

class CModbusTemplateT : public ::g3schema::ns_mmb::CMMBSesnT
{
public:
	g3schema_EXPORT CModbusTemplateT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CModbusTemplateT(CModbusTemplateT const& init);
	void operator=(CModbusTemplateT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_template_altova_CModbusTemplateT); }
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_template

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CModbusTemplateT
