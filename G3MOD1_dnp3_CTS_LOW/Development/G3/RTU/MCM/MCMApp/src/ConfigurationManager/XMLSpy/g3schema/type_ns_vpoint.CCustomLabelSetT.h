#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CCustomLabelSetT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CCustomLabelSetT



namespace g3schema
{

namespace ns_vpoint
{	

class CCustomLabelSetT : public TypeBase
{
public:
	g3schema_EXPORT CCustomLabelSetT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CCustomLabelSetT(CCustomLabelSetT const& init);
	void operator=(CCustomLabelSetT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CCustomLabelSetT); }

	MemberAttribute<string_type,_altova_mi_ns_vpoint_altova_CCustomLabelSetT_altova_name, 0, 0> name;	// name Cstring
	MemberElement<ns_vpoint::CCustomLabelEntryT, _altova_mi_ns_vpoint_altova_CCustomLabelSetT_altova_labelEntry> labelEntry;
	struct labelEntry { typedef Iterator<ns_vpoint::CCustomLabelEntryT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CCustomLabelSetT
