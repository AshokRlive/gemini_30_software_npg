#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CCLogicCounterPointT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CCLogicCounterPointT

#include "type_ns_vpoint.CBaseCounterPointT.h"


namespace g3schema
{

namespace ns_clogic
{	

class CCLogicCounterPointT : public ::g3schema::ns_vpoint::CBaseCounterPointT
{
public:
	g3schema_EXPORT CCLogicCounterPointT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CCLogicCounterPointT(CCLogicCounterPointT const& init);
	void operator=(CCLogicCounterPointT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_clogic_altova_CCLogicCounterPointT); }
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_clogic

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CCLogicCounterPointT
