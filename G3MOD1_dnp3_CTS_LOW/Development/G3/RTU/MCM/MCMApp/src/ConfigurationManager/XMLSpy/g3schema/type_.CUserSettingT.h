#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CUserSettingT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CUserSettingT



namespace g3schema
{

class CUserSettingT : public TypeBase
{
public:
	g3schema_EXPORT CUserSettingT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CUserSettingT(CUserSettingT const& init);
	void operator=(CUserSettingT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_altova_CUserSettingT); }
	MemberElement<CSecurityPolicyT, _altova_mi_altova_CUserSettingT_altova_securityPolicy> securityPolicy;
	struct securityPolicy { typedef Iterator<CSecurityPolicyT> iterator; };
	MemberElement<CUserAccountT, _altova_mi_altova_CUserSettingT_altova_userAccounts> userAccounts;
	struct userAccounts { typedef Iterator<CUserAccountT> iterator; };
	g3schema_EXPORT void SetXsiType();
};


}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CUserSettingT
