#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_iec870_ALTOVA_CIEC870EventConfigT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_iec870_ALTOVA_CIEC870EventConfigT



namespace g3schema
{

namespace ns_iec870
{	

class CIEC870EventConfigT : public TypeBase
{
public:
	g3schema_EXPORT CIEC870EventConfigT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CIEC870EventConfigT(CIEC870EventConfigT const& init);
	void operator=(CIEC870EventConfigT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_iec870_altova_CIEC870EventConfigT); }
	MemberElement<ns_iec870::CIEC870SesnEventT, _altova_mi_ns_iec870_altova_CIEC870EventConfigT_altova_mmea> mmea;
	struct mmea { typedef Iterator<ns_iec870::CIEC870SesnEventT> iterator; };
	MemberElement<ns_iec870::CIEC870SesnEventT, _altova_mi_ns_iec870_altova_CIEC870EventConfigT_altova_mmeb> mmeb;
	struct mmeb { typedef Iterator<ns_iec870::CIEC870SesnEventT> iterator; };
	MemberElement<ns_iec870::CIEC870SesnEventT, _altova_mi_ns_iec870_altova_CIEC870EventConfigT_altova_mmec> mmec;
	struct mmec { typedef Iterator<ns_iec870::CIEC870SesnEventT> iterator; };
	MemberElement<ns_iec870::CIEC870SesnEventT, _altova_mi_ns_iec870_altova_CIEC870EventConfigT_altova_mit> mit;
	struct mit { typedef Iterator<ns_iec870::CIEC870SesnEventT> iterator; };
	MemberElement<ns_iec870::CIEC870SesnEventT, _altova_mi_ns_iec870_altova_CIEC870EventConfigT_altova_mdp> mdp;
	struct mdp { typedef Iterator<ns_iec870::CIEC870SesnEventT> iterator; };
	MemberElement<ns_iec870::CIEC870SesnEventT, _altova_mi_ns_iec870_altova_CIEC870EventConfigT_altova_msp> msp;
	struct msp { typedef Iterator<ns_iec870::CIEC870SesnEventT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_iec870

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_iec870_ALTOVA_CIEC870EventConfigT
