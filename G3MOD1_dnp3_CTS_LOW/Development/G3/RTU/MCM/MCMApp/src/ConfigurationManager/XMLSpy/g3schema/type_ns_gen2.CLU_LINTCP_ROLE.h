#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen2_ALTOVA_CLU_LINTCP_ROLE
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen2_ALTOVA_CLU_LINTCP_ROLE



namespace g3schema
{

namespace ns_gen2
{	

class CLU_LINTCP_ROLE : public TypeBase
{
public:
	g3schema_EXPORT CLU_LINTCP_ROLE(xercesc::DOMNode* const& init);
	g3schema_EXPORT CLU_LINTCP_ROLE(CLU_LINTCP_ROLE const& init);
	void operator=(CLU_LINTCP_ROLE const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen2_altova_CLU_LINTCP_ROLE); }

	enum EnumValues {
		Invalid = -1,
		k_LU_LINTCP_ROLE_MASTER = 0, // LU_LINTCP_ROLE_MASTER
		k_LU_LINTCP_ROLE_OUTSTATION = 1, // LU_LINTCP_ROLE_OUTSTATION
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen2

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen2_ALTOVA_CLU_LINTCP_ROLE
