/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       HMI XML factory implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   01/03/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "XMLHMIFactory.h"
#include "XMLParser.h"
#include "MenuScreen.h"
#include "SwitchGearScreen.h"
#include "DataEntryScreen.h"
#include "VPointDataEntry.h"
#include "ControlScreen.h"
#include "PhaseScreen.h"
#include "InfoScreen.h"
#include "AlarmScreen.h"
#include "LogonScreen.h"

using namespace ns_common;
using namespace ns_hmi;
using namespace ns_g3module;
using namespace ns_vpoint;

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

void fillPhasePoint( CHMIPhaseScreenT &config,
                     PointIdStr *phase,
                     PhaseScreen::PHASE idx
                   );

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

XMLHMIFactory::XMLHMIFactory( Cg3schema &configuration,
                              GeminiDatabase &database,
                              IIOModuleManager& moduleMgr,
                              IStatusManager& statusMgr
                            ) : configuration(configuration),
                                database(database),
                                moduleManager(moduleMgr),
                                statusManager(statusMgr),
                                actPressDelay_ms(2000),
                                log(Logger::getLogger(SUBSYSTEM_ID_CONFMGR))
{}

XMLHMIFactory::~XMLHMIFactory() {}


Screen* XMLHMIFactory::getScreen( ScreenManager &mediator,
                                  lu_uint8_t rows,
                                  lu_uint8_t columns
                                )
{
    try
    {
        CConfigurationT configurationRoot = getXMLElement<CConfigurationT>(configuration.configuration);
        CModulesT modules = getXMLElement<CModulesT>(configurationRoot.modules);
        CHMIModuleT HMI = getXMLElement<CHMIModuleT>(modules.HMI);

        /* Get time required to keep pressed ACT + OPEN/CLOSE buttons to start an action */
        if(HMI.channels.exists())
        {
            if(HMI.generalConfig.exists())
            {
                //Look in all the channels until found
				CHMIGeneralConfT generalConf = HMI.generalConfig.first();
                {
                    /* Channel found. Parse configuration */
                    if(generalConf.openClosePressTimeMs.exists())
                    {
                        //found
                        actPressDelay_ms = generalConf.openClosePressTimeMs;
                    }
                }
            }
        }

        /* Get HMI screens & menus */
        CScreensT screens = getXMLElement<CScreensT>(HMI.screens);
        checkXMLElement(screens.rootScreen);
        lu_uint16_t rootID = screens.rootScreen.first().rootScreenID;

        return getScreen(screens, rootID, mediator, rows, columns);
    }
    catch (const ParserException& e)
    {
        log.info("%s: HMI configuration not found: %s", __AT__, e.what());
    }
    return NULL;
}

Screen* XMLHMIFactory::getLogonScreen( ScreenManager &mediator,
                                  lu_uint8_t rows,
                                  lu_uint8_t columns
                                )
{
   try
   {
       /* Get a reference to the main configuration */
       CConfigurationT configurationRoot = getXMLElement<CConfigurationT>(configuration.configuration);

       /* Get User list */
       CUserSettingT userList = getXMLElement<CUserSettingT>(configurationRoot.userSetting);
       std::vector<std::string> pinList;
       std::string pin;
       if(userList.userAccounts.exists())
       {
           //fill user list
           for( Iterator<CUserAccountT> itUser = userList.userAccounts.all();
                itUser;
                ++itUser
              )
           {
               if(itUser.pin.exists())
               {
                   pin = getXMLAttr(itUser.pin);
                   if(pin.length() <= PIN_HASH_LENGTH_MAX)
                   {
                       pinList.push_back(pin);
                   }
               }
           }
       }


       if(pinList.size() > 0)
       {
           LogonScreen* screen = new LogonScreen(mediator, rows,columns);
           screen->setPinList(pinList);
           return screen;
       }
   }
   catch(const ParserException& e)
   {
       log.error("%s: HMI PIN code parse error: %s", __AT__, e.what());
   }

    return NULL;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


Screen *XMLHMIFactory::getScreen( CScreensT &screens,
                                  lu_uint16_t id,
                                  ScreenManager &mediator,
                                  lu_uint8_t rows,
                                  lu_uint8_t columns
                                )
{
    /* Search the ID */

    /* MenuScreen */
    if(screens.menuScreens.exists())
    {
        for( Iterator<CHMIMenuScreenT> itMenuScreen = screens.menuScreens.all();
             itMenuScreen;
             ++itMenuScreen
           )
        {
            if( (itMenuScreen.screenID.exists()) &&
                (itMenuScreen.screenID == id)
              )
            {
                /* Screen found */
                return createMenuScreen(screens, itMenuScreen, mediator, rows, columns);
            }
        }
    }

    /* ControlScreen */
    if(screens.controlScreens.exists())
    {
        for( Iterator<CHMIControlScreenT> itControlScreen = screens.controlScreens.all();
             itControlScreen;
             ++itControlScreen
           )
        {
            if( (itControlScreen.screenID.exists()) &&
                (itControlScreen.screenID == id)
              )
            {
                /* Screen found */
                return createControlScreen(itControlScreen, mediator, rows, columns);
            }
        }
    }

    /* Phase screen */
    if(screens.phaseScreens.exists())
    {
        for( Iterator<CHMIPhaseScreenT> itPhaseScreen = screens.phaseScreens.all();
             itPhaseScreen;
             ++itPhaseScreen
           )
        {
            if( (itPhaseScreen.screenID.exists()) &&
                (itPhaseScreen.screenID == id)
              )
            {
                /* Screen found */
                return createPhaseScreen(itPhaseScreen, mediator, rows, columns);
            }
        }
    }

    /* Data Screen */
    if( (screens.dataScreens.exists()) &&
        (screens.dataEntries.exists())
        )
    {
        for( Iterator<CHMIDataScreenT> itDataScreen = screens.dataScreens.all();
                itDataScreen;
             ++itDataScreen
           )
        {
            if( (itDataScreen.screenID.exists()) &&
                (itDataScreen.screenID == id)
              )
            {
                /* Screen found */
                //CHMIDataEntryRef entries; // = screens.dataEntries.first();
                return createDataScreen(itDataScreen, screens, mediator, rows, columns);
            }
        }
    }

    /* ControlScreen */
    if(screens.infoScreens.exists())
    {
        for( Iterator<CHMIInfoScreenT> itInfoScreen = screens.infoScreens.all();
             itInfoScreen;
             ++itInfoScreen
           )
        {
            if( (itInfoScreen.screenID.exists()) &&
                (itInfoScreen.screenID == id)
              )
            {
                /* Screen found */
                return createInfoScreen(itInfoScreen, mediator, rows, columns);
            }
        }
    }

    /* Screen not found */
    return NULL;
}

Screen *XMLHMIFactory::createMenuScreen( CScreensT &screens,
                                         CHMIMenuScreenT &config,
                                         ScreenManager &mediator,
                                         lu_uint8_t rows,
                                         lu_uint8_t columns
                                       )
{
    if(!config.screenTitle.exists())
    {
        return NULL;
    }

    MenuScreen *menu = new MenuScreen( mediator,
                                       0,
                                       tstring(config.screenTitle).c_str(),
                                       rows,
                                       columns
                                     );
    if(menu != NULL)
    {
        /* Add children */
        for( Iterator<CHMIScreenRef> itChildList = config.childList.all();
             itChildList;
             ++itChildList
           )
        {
            if(itChildList.screenID.exists())
            {
                Screen *child = getScreen(screens, itChildList.screenID, mediator, rows, columns);

                if(child != NULL)
                {
                    menu->add(child);
                }
            }
        }
    }
    return menu;
}


Screen *XMLHMIFactory::createControlScreen( CHMIControlScreenT &config,
                                           ScreenManager &mediator,
                                           lu_uint8_t rows,
                                           lu_uint8_t columns
                                         )
{
    if( (!config.screenTitle.exists())   ||
        (!config.controlLogicID.exists())
      )
    {
        return NULL;
    }

    /* When a Control Logic is added, ensure that is updated in the switch
     * statement (if is operatable) and then put the number of Control Logics
     * (straight a number, not the constant!) and the last one in the ASSERT.
     * E.g.:    CHECK_CONTROL_LOGIC(28, CONTROL_LOGIC_TYPE_BYPASS)
     */
    LU_STATIC_ASSERT(CHECK_CONTROL_LOGIC(29, CONTROL_LOGIC_TYPE_MOTOR_SUPPLY), Control_Logic_amount_mismatch);
    switch(database.getType(config.controlLogicID))
    {
    case CONTROL_LOGIC_TYPE_SGL:
    case CONTROL_LOGIC_TYPE_DUMMYSW:
    case CONTROL_LOGIC_TYPE_DOL:
    case CONTROL_LOGIC_TYPE_DIGCTRLPOINT:
    {
        SwitchGearScreen* menu = new SwitchGearScreen( mediator,
                                                       0,
                                                       tstring(config.screenTitle).c_str(),
                                                       rows,
                                                       columns,
                                                       database,
                                                       config.controlLogicID,
                                                       actPressDelay_ms
                                                     );
        return menu;
        break;
    }
    case CONTROL_LOGIC_TYPE_FAN_TEST:
    case CONTROL_LOGIC_TYPE_BATTERY:
    case CONTROL_LOGIC_TYPE_PCLK:
    case CONTROL_LOGIC_TYPE_FPI_TEST:
    case CONTROL_LOGIC_TYPE_FPI_RESET:
    case CONTROL_LOGIC_TYPE_POWER_SUPPLY:
    case CONTROL_LOGIC_TYPE_MOTOR_SUPPLY:
    {
        ControlScreen* menu = new ControlScreen( mediator,
                                               0,
                                               tstring(config.screenTitle).c_str(),
                                               rows,
                                               columns,
                                               database,
                                               config.controlLogicID
                                             );
        return menu;
        break;
    }
    /* TODO: pueyos_a - add HMI menu for start/stop automation?
        case CONTROL_LOGIC_TYPE_IEC61499:
             break;
     */
    default:
        break;
    }
    return NULL;
}


Screen *XMLHMIFactory::createPhaseScreen( CHMIPhaseScreenT &config,
                                          ScreenManager &mediator,
                                          lu_uint8_t rows,
                                          lu_uint8_t columns
                                        )
{
    if( (!config.screenTitle.exists())
      )
    {
        return NULL;
    }

    const lu_char_t *currentFooter = NULL;
    lu_char_t currentBuffer[Screen::maxFooterSize];
    if(config.currentFooter.exists())
    {
        currentFooter = tstring(config.currentFooter).c_str();
        strncpy(currentBuffer, currentFooter, sizeof(currentBuffer));
        currentBuffer[sizeof(currentBuffer)-1] = '\0';
        currentFooter = currentBuffer;
    }

    const lu_char_t *voltageFooter= NULL;
    lu_char_t voltageBuffer[Screen::maxFooterSize];
    if(config.voltageFooter.exists())
    {
        voltageFooter = tstring(config.voltageFooter).c_str();
        strncpy(voltageBuffer, voltageFooter, sizeof(voltageBuffer));
        voltageBuffer[sizeof(voltageBuffer)-1] = '\0';
        voltageFooter = voltageBuffer;
    }

    PointIdStr phases[PhaseScreen::PHASE_LAST];
    for(lu_uint32_t i = PhaseScreen::PHASE_I_A; i <= PhaseScreen::PHASE_I_N; ++i)
    {
        fillPhasePoint(config, &phases[i], static_cast<PhaseScreen::PHASE>(i));
    }

    PhaseScreen *phaseScreen = new PhaseScreen( mediator,
                                                0,
                                               tstring(config.screenTitle).c_str(),
                                               rows,
                                               columns,
                                               database,
                                               phases,
                                               currentFooter,
                                               voltageFooter
                                              );
   return phaseScreen;
}


Screen *XMLHMIFactory::createDataScreen( CHMIDataScreenT &config,
                                         CScreensT &entries,
                                         ScreenManager &mediator,
                                         lu_uint8_t rows,
                                         lu_uint8_t columns
                                       )
{

    if(!config.screenTitle.exists())
    {
        return NULL;
    }

    DataEntryScreen *dataScreen = new DataEntryScreen( mediator,
                                                       0,
                                                       tstring(config.screenTitle).c_str(),
                                                       rows,
                                                       columns
                                                     );

    if(dataScreen != NULL)
    {
        for( Iterator<CHMIDataEntryRef> itDataEntry = config.entries.all();
             itDataEntry;
             ++itDataEntry
           )
        {
            if(itDataEntry.entryID.exists())
            {
                DataEntry *entry = getDataEntry(entries, itDataEntry.entryID);
                if(entry != NULL)
                {
                    dataScreen->addEntry(entry);
                }
            }
        }
    }
    return dataScreen;
}


Screen* XMLHMIFactory::createInfoScreen( CHMIInfoScreenT &config,
                                         ScreenManager &mediator,
                                         lu_uint8_t rows,
                                         lu_uint8_t columns
                                         )
{
    if( (!config.screenTitle.exists())   ||
        (!config.type.exists())
      )
    {
        return NULL;
    }

    if(config.type == HMI_INFO_SCREEN_TYPE_ALARMS)
    {
        return new AlarmScreen(mediator,
                               0,
                               tstring(config.screenTitle).c_str(),
                               rows,
                               columns,
                               moduleManager,
                               statusManager,
                               config.type
                             );
    }
    return new InfoScreen( mediator,
                           0,
                           tstring(config.screenTitle).c_str(),
                           rows,
                           columns,
                           moduleManager,
                           statusManager,
                           config.type
                         );
}


DataEntry *XMLHMIFactory::getDataEntry(CScreensT &entries, lu_uint16_t id)
{
    lu_char_t headerBuffer[Screen::maxHeaderSize];
    lu_char_t footerBuffer[Screen::maxFooterSize];

    if(!entries.dataEntries.exists())
    {
        return NULL;
    }

    for( Iterator<CHMIVPointDataEntryT> itDataEntry = entries.dataEntries.all();
         itDataEntry;
         ++itDataEntry
       )
    {
        if( (itDataEntry.entryID.exists()) &&
            (itDataEntry.entryID == id) &&
            (itDataEntry.pointGroup.exists()) &&
            (itDataEntry.pointID.exists())
          )
        {
            /* Valid Entry found */
            const lu_char_t *header = NULL;
            const lu_char_t *footer = NULL;
            PointIdStr point;

            point.group = itDataEntry.pointGroup;
            point.ID = itDataEntry.pointID;

            if(itDataEntry.header.exists())
            {
                header = tstring(itDataEntry.header).c_str();
                strncpy(headerBuffer, header, sizeof(headerBuffer));
                headerBuffer[sizeof(headerBuffer)-1] = '\0';
                header = headerBuffer;
            }

            if(itDataEntry.footer.exists())
            {
                footer = tstring(itDataEntry.footer).c_str();
                strncpy(footerBuffer, footer, sizeof(footerBuffer));
                footerBuffer[sizeof(footerBuffer)-1] = '\0';
                footer = footerBuffer;
            }

            VPointDataEntry *entry = new VPointDataEntry( header,
                                                          footer,
                                                          database,
                                                          point
                                                        );
            return entry;
        }
    }
    return NULL;
}


void fillPhasePoint( CHMIPhaseScreenT &config,
                     PointIdStr *phase,
                     PhaseScreen::PHASE idx
                   )
{
    switch(idx)
    {
        case PhaseScreen::PHASE_I_A:
        case PhaseScreen::PHASE_V_A:
        case PhaseScreen::PHASE_I_B:
        case PhaseScreen::PHASE_V_B:
        case PhaseScreen::PHASE_I_C:
        case PhaseScreen::PHASE_V_C:
        case PhaseScreen::PHASE_I_N:
            if(config.virtualPointEntry.exists())
            {
                CVirtualPointRefT point = config.virtualPointEntry.first();
                if( (point.pointGroup.exists()) &&
                    (point.pointID.exists())
                  )
                {
                    phase->group = point.pointGroup;
                    phase->ID    = point.pointID;
                }
            }
        break;

        default:
            phase->group = PointIdStr::invalidGroup;
            phase->ID    = PointIdStr::invalidID;
        break;
    }
}


/*
 *********************** End of file ******************************************
 */
