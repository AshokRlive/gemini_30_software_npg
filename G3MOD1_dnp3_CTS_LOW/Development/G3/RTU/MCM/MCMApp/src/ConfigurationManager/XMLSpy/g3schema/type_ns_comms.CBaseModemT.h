#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_comms_ALTOVA_CBaseModemT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_comms_ALTOVA_CBaseModemT

#include "type_ns_comms.CCommsDeviceT.h"


namespace g3schema
{

namespace ns_comms
{	

class CBaseModemT : public ::g3schema::ns_comms::CCommsDeviceT
{
public:
	g3schema_EXPORT CBaseModemT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CBaseModemT(CBaseModemT const& init);
	void operator=(CBaseModemT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_comms_altova_CBaseModemT); }

	MemberAttribute<string_type,_altova_mi_ns_comms_altova_CBaseModemT_altova_serialPortName, 0, 0> serialPortName;	// serialPortName Cstring
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_comms

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_comms_ALTOVA_CBaseModemT
