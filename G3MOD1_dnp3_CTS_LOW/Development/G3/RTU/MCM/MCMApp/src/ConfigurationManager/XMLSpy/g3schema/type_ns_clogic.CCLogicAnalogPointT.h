#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CCLogicAnalogPointT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CCLogicAnalogPointT

#include "type_ns_vpoint.CBaseAPointT.h"


namespace g3schema
{

namespace ns_clogic
{	

class CCLogicAnalogPointT : public ::g3schema::ns_vpoint::CBaseAPointT
{
public:
	g3schema_EXPORT CCLogicAnalogPointT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CCLogicAnalogPointT(CCLogicAnalogPointT const& init);
	void operator=(CCLogicAnalogPointT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_clogic_altova_CCLogicAnalogPointT); }
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_clogic

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CCLogicAnalogPointT
