#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CHMIScreenRef
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CHMIScreenRef



namespace g3schema
{

namespace ns_hmi
{	

class CHMIScreenRef : public TypeBase
{
public:
	g3schema_EXPORT CHMIScreenRef(xercesc::DOMNode* const& init);
	g3schema_EXPORT CHMIScreenRef(CHMIScreenRef const& init);
	void operator=(CHMIScreenRef const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_hmi_altova_CHMIScreenRef); }

	MemberAttribute<int,_altova_mi_ns_hmi_altova_CHMIScreenRef_altova_screenID, 0, 0> screenID;	// screenID Cint
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_hmi

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CHMIScreenRef
