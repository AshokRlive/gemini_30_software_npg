#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CFreezingConfT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CFreezingConfT



namespace g3schema
{

namespace ns_vpoint
{	

class CFreezingConfT : public TypeBase
{
public:
	g3schema_EXPORT CFreezingConfT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CFreezingConfT(CFreezingConfT const& init);
	void operator=(CFreezingConfT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CFreezingConfT); }

	MemberAttribute<bool,_altova_mi_ns_vpoint_altova_CFreezingConfT_altova_enabled, 0, 0> enabled;	// enabled Cboolean

	MemberAttribute<bool,_altova_mi_ns_vpoint_altova_CFreezingConfT_altova_freezeThenReset, 0, 0> freezeThenReset;	// freezeThenReset Cboolean
	MemberElement<xs::CunsignedIntType, _altova_mi_ns_vpoint_altova_CFreezingConfT_altova_freezingInterval> freezingInterval;
	struct freezingInterval { typedef Iterator<xs::CunsignedIntType> iterator; };
	MemberElement<xs::CunsignedIntType, _altova_mi_ns_vpoint_altova_CFreezingConfT_altova_freezingClockTime> freezingClockTime;
	struct freezingClockTime { typedef Iterator<xs::CunsignedIntType> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CFreezingConfT
