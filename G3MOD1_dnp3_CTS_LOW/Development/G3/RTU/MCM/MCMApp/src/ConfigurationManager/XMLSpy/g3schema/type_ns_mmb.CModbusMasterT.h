#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_mmb_ALTOVA_CModbusMasterT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_mmb_ALTOVA_CModbusMasterT



namespace g3schema
{

namespace ns_mmb
{	

class CModbusMasterT : public TypeBase
{
public:
	g3schema_EXPORT CModbusMasterT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CModbusMasterT(CModbusMasterT const& init);
	void operator=(CModbusMasterT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_mmb_altova_CModbusMasterT); }
	MemberElement<ns_mmb::CMMBChnlT, _altova_mi_ns_mmb_altova_CModbusMasterT_altova_channel> channel;
	struct channel { typedef Iterator<ns_mmb::CMMBChnlT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_mmb

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_mmb_ALTOVA_CModbusMasterT
