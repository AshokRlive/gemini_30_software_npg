#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s104_ALTOVA_CS104T
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s104_ALTOVA_CS104T



namespace g3schema
{

namespace ns_s104
{	

class CS104T : public TypeBase
{
public:
	g3schema_EXPORT CS104T(xercesc::DOMNode* const& init);
	g3schema_EXPORT CS104T(CS104T const& init);
	void operator=(CS104T const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_s104_altova_CS104T); }
	MemberElement<ns_s104::CS104ChannelT, _altova_mi_ns_s104_altova_CS104T_altova_channel> channel;
	struct channel { typedef Iterator<ns_s104::CS104ChannelT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_s104

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s104_ALTOVA_CS104T
