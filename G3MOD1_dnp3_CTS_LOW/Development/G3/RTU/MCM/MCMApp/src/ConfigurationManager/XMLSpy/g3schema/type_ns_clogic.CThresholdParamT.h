#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CThresholdParamT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CThresholdParamT

#include "type_ns_vpoint.CHiHiLoLoT.h"


namespace g3schema
{

namespace ns_clogic
{	

class CThresholdParamT : public ::g3schema::ns_vpoint::CHiHiLoLoT
{
public:
	g3schema_EXPORT CThresholdParamT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CThresholdParamT(CThresholdParamT const& init);
	void operator=(CThresholdParamT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_clogic_altova_CThresholdParamT); }
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_clogic

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CThresholdParamT
