#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CParameterT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CParameterT



namespace g3schema
{

namespace ns_template
{	

class CParameterT : public TypeBase
{
public:
	g3schema_EXPORT CParameterT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CParameterT(CParameterT const& init);
	void operator=(CParameterT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_template_altova_CParameterT); }

	MemberAttribute<string_type,_altova_mi_ns_template_altova_CParameterT_altova_name, 0, 0> name;	// name Cstring

	MemberAttribute<string_type,_altova_mi_ns_template_altova_CParameterT_altova_value2, 0, 0> value2;	// value CanySimpleType
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_template

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CParameterT
