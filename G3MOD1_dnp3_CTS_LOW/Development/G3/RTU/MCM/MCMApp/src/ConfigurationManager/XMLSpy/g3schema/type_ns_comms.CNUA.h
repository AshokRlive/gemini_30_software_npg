#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_comms_ALTOVA_CNUA
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_comms_ALTOVA_CNUA



namespace g3schema
{

namespace ns_comms
{	

class CNUA : public TypeBase
{
public:
	g3schema_EXPORT CNUA(xercesc::DOMNode* const& init);
	g3schema_EXPORT CNUA(CNUA const& init);
	void operator=(CNUA const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_comms_altova_CNUA); }
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_comms

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_comms_ALTOVA_CNUA
