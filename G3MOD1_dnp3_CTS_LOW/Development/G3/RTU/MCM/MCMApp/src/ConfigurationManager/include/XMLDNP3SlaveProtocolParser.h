/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_39B84EED_3828_4a13_BF9B_0CB605CDA73E__INCLUDED_)
#define EA_39B84EED_3828_4a13_BF9B_0CB605CDA73E__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <iostream>
#include <string>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Thread.h"
#include "ConfigurationManagerError.h"
#include "Logger.h"
#include "MCMConfigEnum.h"
#include "MainAppEnum.h"
#include "DNP3SlaveProtocol.h"
#include "PortManager.h"
#include "ModuleManager.h"
#include "CommsDeviceManager.h"

#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>

#include "Altova/Altova.h"
#include "Altova/SchemaTypes.h"
#include "Altova/AltovaException.h"

#include "AltovaXML/XmlException.h"
#include "AltovaXML/Node.h"

#include "g3schema/g3schema.h"

using namespace g3schema;
using namespace altova;
using namespace ns_sdnp3;
using namespace ns_comms;

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
typedef enum
{
    NETWORK_TYPE_SERIAL       = 0,
    NETWORK_TYPE_COMMSDEVICE,
    NETWORK_TYPE_TCP_ONLY,
    NETWORK_TYPE_TCP_UDP,
    NETWORK_TYPE_UDP_ONLY,

    NETWORK_TYPE_LAST
} NETWORK_TYPE;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class XMLDNP3SlaveProtocolParser
{
public:
    /**
     * \brief Custom constructor
     *
     * The initParser method is automatically called
     *
     * \param configuration         Configuration file parser handler
     * \param commsDeviceManager    Comms Device Manager (MODEMs)
     * \param portManager           Port Manager (Serial ports etc)
     * \param modulemanager         Module Manager (modules, channels etc)
     *
     * \return none
     */
    XMLDNP3SlaveProtocolParser(Cg3schema&          configuration,
                               CommsDeviceManager& commsDeviceManager,
                               PortManager&        portManager,
                               ModuleManager&      moduleManager);

    virtual ~XMLDNP3SlaveProtocolParser();

    /**
     * \brief Create a new protocol stack instance
     *
     * \param GDatabase Gemini database reference
     *
     * \return pointer to the new protocol stack. NULL in case of error
     */
    virtual DNP3SlaveProtocol* newProtocolManager(GeminiDatabase&   GDatabase,
                                                  IMCMApplication&  mcmApplication,
                                                  IIOModuleManager& moduleManager);

private:
    /**
     * \brief Initialize the parser
     *
     * This function must be called before the getPointNumber and
     * newPoint methods
     *
     * \return Error code
     */
    CONFMGR_ERROR initParser();

    /**
     * \brief Allocate all the configured analogue points
     *
     * The analogue point configuration section is parsed and the
     * points/observers database is created and initialised
     *
     * \param GDatabase Reference to the gemini database. It is used to register
     * the protocol points observers
     * \param sessionParserPtr Pointer to the session parser config
     * \param sessionPtr Pointer to the session (which holds the protocol point db too)
     *
     * \return None
     */
    void parseAnalogueConfig( GeminiDatabase           &GDatabase,
                              CSDNP3SesnT              *sessionParserPtr,
                              DNP3SlaveProtocolSession *sessionPtr);

    /**
     * \brief Allocate all the configured binary points
     *
     * The binary point configuration section is parsed and the
     * points/observers database is created and initialised
     *
     * \param GDatabase Reference to the gemini database. It is used to register
     * the protocol points observers
     * \param sessionParserPtr Pointer to the session parser config
     * \param sessionPtr Pointer to the session (which holds the protocol point db too)
     *
     * \return None
     */
    void parseBinaryConfig( GeminiDatabase           &GDatabase,
                            CSDNP3SesnT              *sessionParserPtr,
                            DNP3SlaveProtocolSession *sessionPtr);

    /**
     * \brief Allocate all the configured double binary points
     *
     * The double binary point configuration section is parsed and the
     * points/observers database is created and initialised
     *
     * \param GDatabase Reference to the gemini database. It is used to register
     * the protocol points observers
     * \param DNP3 reference to the DNP3 slave section of the XML parser
     * \param  db reference to the protocol stack database to initialise
     *
     * \return None
     */
    void parseDBinaryConfig( GeminiDatabase           &GDatabase,
                             CSDNP3SesnT              *sessionParserPtr,
                             DNP3SlaveProtocolSession *sessionPtr);

    /**
     * \brief Allocate all the configured Counter points
     *
     * The Counter point configuration section is parsed and the
     * points/observers database is created and initialised
     *
     * \param GDatabase Reference to the Gemini database. It is used to register
     * the protocol points observers
     * \param sessionParserPtr Pointer to the session parser config
     * \param sessionPtr Pointer to the session (which holds the protocol point db too)
     *
     * \return None
     */
    void parseCounterConfig( GeminiDatabase           &GDatabase,
                             CSDNP3SesnT              *sessionParserPtr,
                             DNP3SlaveProtocolSession *sessionPtr);

    /**
     * \brief Allocate all the configured binary output points
     *
     * The binary output point configuration section is parsed and the
     * points database is created and initialised
     *
     * the protocol points observers
     * \param DNP3 reference to the DNP3 slave section of the XML parser
     * \param  db reference to the protocol stack database to initialise
     *
     * \return None
     */
    void parseBinaryOutputConfig(CSDNP3SesnT              *sessionParserPtr,
                                 DNP3SlaveProtocolSession *sessionPtr);

    void parseAnalogOutputConfig(GeminiDatabase           &GDatabase,
    							 CSDNP3SesnT              *sessionParserPtr,
                                 DNP3SlaveProtocolSession *sessionPtr);

    void parseFailoverGroupConfig(CConnectionManagerT&     conGroup,
                                  DNP3SlaveProtocolChannel::Config *channelConfigPtr,
                                  DNP3SlaveProtocolChannel  *channelPtr);

    void parseCommsPowerSupplyConfig(CCommsPowerCycleT&     commsPowerCycleXML,
                                     IIOModuleManager&         moduleManager,
                                     SCADAConnectionManager::SCADAConnManConfig& config);

    void parseConnectivityCheckConfig(CIpConnectivityCheckT& conCheckConfig,
                                      SCADAConnectionManager::SCADAConnManConfig& config);

    void parseOutgoingConnectionsConfig(COutgoingConnectionsT& outgoingConnectionsXML,
                                        SCADAConnectionManager::SCADAConnManConfig& config);

    void parseIncomingConnectionsConfig(CIncomingConnectionsT& incomingConnectionsXML,
                                        SCADAConnectionManager::SCADAConnManConfig& config);

    void parseConnectionSettingsConfig(CConnectionManagerT& connectionSettingsXML,
                                       SCADAConnectionManager::SCADAConnManConfig& config);

    void parseSessionConfig(CSDNP3SesnT&    sessionConfig,
                            DNP3SlaveProtocolSession::Config& csessionConfigStr,
                            bool enableBroadcast);

    void parseChannelSerialConfig(CDNP3LinuxIoT&                    linuxIo,
                                  CommsDeviceManager&               commsDeviceManager,
                                  DNP3SlaveProtocolChannel::Config& channelConfigStr,
                                  DialupModem**                     modemPtrPtr,
                                  lu_uint8_t*                       connectionClassPtr);

private:
    Cg3schema&           configuration;
    CommsDeviceManager&  commsDeviceManager;
    PortManager&         portManager;
    ModuleManager&       moduleManager;
    Logger&              log;
};


#endif // !defined(EA_39B84EED_3828_4a13_BF9B_0CB605CDA73E__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
