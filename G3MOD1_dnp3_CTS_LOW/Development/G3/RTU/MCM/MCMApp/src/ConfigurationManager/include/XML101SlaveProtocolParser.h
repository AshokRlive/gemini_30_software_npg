/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:XML101SlaveProtocolParser.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief Slave IEC-101 Protocol Configuration Parser
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11 Jul 2016     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef XML101SLAVEPROTOCOLPARSER_H_
#define XML101SLAVEPROTOCOLPARSER_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "ConfigurationManagerError.h"
#include "PortManager.h"
#include "ModuleManager.h"
#include "CommsDeviceManager.h"
//#include "S101InputPoint.h"
#include "S101Protocol.h"

#include "XMLParser.h"

#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>

#include "Altova/Altova.h"
#include "Altova/SchemaTypes.h"
#include "Altova/AltovaException.h"

#include "AltovaXML/XmlException.h"
#include "AltovaXML/Node.h"

#include "g3schema/g3schema.h"

using namespace g3schema;
using namespace altova;
using namespace ns_iec870;
using namespace ns_s101;

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Slave IEC-101 Protocol Configuration Parser
 *
 *   \warning This code has been copied straight from XMLSlave104ProtocolParser
 *              and therefore not optimised.
 ******************************************************************************
 */
class XML101SlaveProtocolParser
{
public:
    /**
     * \brief Custom constructor
     *
     * The initParser method is automatically called
     *
     * \param configuration         Configuration file parser handler
     * \param commsDeviceManager    Comms Device Manager (MODEMs)
     * \param portManager           Port Manager (Serial ports etc)
     * \param modulemanager         Module Manager (modules, channels etc)
     */
    XML101SlaveProtocolParser(  Cg3schema&          configuration,
                                CommsDeviceManager& commsDeviceManager,
                                PortManager&        portManager,
                                ModuleManager&      moduleManager
                                );
    virtual ~XML101SlaveProtocolParser();

    /**
     * \brief Create a new protocol stack instance
     *
     * \param GDatabase Gemini database reference
     *
     * \return pointer to the new protocol stack. NULL in case of error
     */
    virtual S101Protocol *newProtocolManager(GeminiDatabase&   GDatabase);

private:
    /**
     * \brief Initialize the parser
     *
     *
     * \return Error code
     */
    CONFMGR_ERROR initParser();

    /**
     * \brief Allocate all the configured analogue points
     *
     * The analogue point configuration section is parsed and the
     * points/observers database is created and initialised
     *
     * \param GDatabase Reference to the gemini database.
     * \param sessionParserPtr Pointer to the session parser config
     * \param sectorPtr Pointer to the sector (which holds the protocol point db too)
     */
    void parseAnalogConfig( GeminiDatabase           &GDatabase,
                              CS101SessionT         *sessionParserPtr,
                              S101InputPoint::ProtPointType analogType,
                              S101Sector *sectorPtr);


    void parseCommonAnalogAttributes(CIEC870InputPointT*,
                                     lu_uint8_t,
                                     S101InputPoint::Config*
                                     ) throw(ParserException);


    /**
     * \brief Allocate all the configured binary points
     *
     * The binary point configuration section is parsed and the
     * points/observers database is created and initialised
     *
     * \param GDatabase Reference to the gemini database.
     * \param sessionParserPtr Pointer to the session parser config
     * \param sectorPtr Pointer to the sector (which holds the protocol point db too)
     */

    void parseBinaryConfig( GeminiDatabase           &GDatabase,
                            CS101SessionT           *sessionParserPtr,
                            S101Sector *sectorPtr);

    /**
     * \brief Allocate all the configured double binary points
     *
     * The double binary point configuration section is parsed and the
     * points/observers database is created and initialised
     *
     * \param GDatabase Reference to the gemini database.
     * \param sessionParserPtr Pointer to the session parser config
     * \param sectorPtr Pointer to the sector (which holds the protocol point db too)
     */
    void parseDBinaryConfig( GeminiDatabase           &GDatabase,
                             CS101SessionT           *sessionParserPtr,
                             S101Sector *sectorPtr);

    /**
     * \brief Allocate all the configured binary output points
     *
     * The binary output point configuration section is parsed and the
     * points database is created and initialised
     *
     * \param s101 reference to the s101 slave section of the XML parser
     * \param  db reference to the protocol stack database to initialise
     */
    void parseBinaryOutputConfig( CS101SessionT           *sessionParserPtr,
                                  S101Sector *sectorPtr);

    /**
     * \brief Allocate all the configured double binary output points
     *
     * The double binary output point configuration section is parsed and the
     * points database is created and initialised
     *
     * \param s101 reference to the s101 slave section of the XML parser
     */
    void parseDBinaryOutputConfig(CS101SessionT           *sessionParserPtr,
                                  S101Sector *sectorPtr);

    /**
     * \brief Allocate all the configured Integrated Total Points
     *
     * The Integrated Totals point configuration section is parsed and the
     * points database is created and initialised
     *
     * \param s101 reference to the s101 slave section of the XML parser
     * \param  db reference to the protocol stack database to initialise
     */
    void parseIntegratedTotsConfig(GeminiDatabase           &GDatabase,
                                   CS101SessionT           *sessionParserPtr,
                                   S101Sector *sectorPtr);
    /**
     * \brief parse channel config
     *
     * \param channelConfig reference to the channelConfig of the XML parser
     * \param  reference to the structure of the 101Channel::S101ChannelConfig
     */
    void parseChannelConfig(CS101ChannelT&  channelConfig,
                            S101Channel::S101ChannelConfig& channelConfigStr) throw(ParserException);

    /**
     * \brief parse session config
     *
     * \param  sessionConfig reference to the session field of the XML parser
     * \param  reference to the S101SESN_CONFIG structure to fill
     *
     * \return Max Frame Length (max size of frame)
     */
    lu_uint32_t parseSessionConfig(CS101SessionT&  sessionConfig,
                                    S101SESN_CONFIG& sessionConfigStr) throw(ParserException);

    /**
     * \brief parse sector config
     *
     * \param sessionConfig reference to the session field of the XML parser
     * \param  reference to the S101SCTR_CONFIG structure to fill
     */
    void parseSectorConfig( CS101SessionT&  sessionConfig,
                            S101SCTR_CONFIG& sectorConfigStr) throw(ParserException);

private:
    Cg3schema&           configuration;
    CommsDeviceManager&  commsDeviceManager;
    PortManager&         portManager;
    ModuleManager&       moduleManager;
    Logger&              log;
};


#endif /* XML101SLAVEPROTOCOLPARSER_H_ */

/*
 *********************** End of file ******************************************
 */
