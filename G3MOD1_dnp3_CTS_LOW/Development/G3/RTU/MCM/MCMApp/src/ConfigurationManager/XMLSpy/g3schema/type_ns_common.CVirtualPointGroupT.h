#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_CVirtualPointGroupT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_CVirtualPointGroupT



namespace g3schema
{

namespace ns_common
{	

class CVirtualPointGroupT : public TypeBase
{
public:
	g3schema_EXPORT CVirtualPointGroupT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CVirtualPointGroupT(CVirtualPointGroupT const& init);
	void operator=(CVirtualPointGroupT const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_common_altova_CVirtualPointGroupT); }
	void operator= (const unsigned& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::IntegerFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator unsigned()
	{
		return CastAs<unsigned >::Do(GetNode(), 0);
	}
};



} // namespace ns_common

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_CVirtualPointGroupT
