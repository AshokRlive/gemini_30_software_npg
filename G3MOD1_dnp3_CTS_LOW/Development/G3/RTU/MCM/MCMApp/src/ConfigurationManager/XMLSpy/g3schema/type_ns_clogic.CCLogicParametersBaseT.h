#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CCLogicParametersBaseT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CCLogicParametersBaseT

#include "type_ns_common.CParametersT.h"


namespace g3schema
{

namespace ns_clogic
{	

class CCLogicParametersBaseT : public ::g3schema::ns_common::CParametersT
{
public:
	g3schema_EXPORT CCLogicParametersBaseT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CCLogicParametersBaseT(CCLogicParametersBaseT const& init);
	void operator=(CCLogicParametersBaseT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_clogic_altova_CCLogicParametersBaseT); }
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_clogic

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CCLogicParametersBaseT
