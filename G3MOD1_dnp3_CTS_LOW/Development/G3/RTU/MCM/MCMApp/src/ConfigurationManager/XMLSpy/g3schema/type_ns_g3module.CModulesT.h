#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CModulesT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CModulesT



namespace g3schema
{

namespace ns_g3module
{	

class CModulesT : public TypeBase
{
public:
	g3schema_EXPORT CModulesT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CModulesT(CModulesT const& init);
	void operator=(CModulesT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_g3module_altova_CModulesT); }

	MemberAttribute<unsigned,_altova_mi_ns_g3module_altova_CModulesT_altova_backplaneSlotNum, 0, 0> backplaneSlotNum;	// backplaneSlotNum CunsignedByte
	MemberElement<ns_g3module::CPSMModuleT, _altova_mi_ns_g3module_altova_CModulesT_altova_PSM> PSM;
	struct PSM { typedef Iterator<ns_g3module::CPSMModuleT> iterator; };
	MemberElement<ns_g3module::CSCMModuleT, _altova_mi_ns_g3module_altova_CModulesT_altova_SCM> SCM;
	struct SCM { typedef Iterator<ns_g3module::CSCMModuleT> iterator; };
	MemberElement<ns_g3module::CDSMModuleT, _altova_mi_ns_g3module_altova_CModulesT_altova_DSM> DSM;
	struct DSM { typedef Iterator<ns_g3module::CDSMModuleT> iterator; };
	MemberElement<ns_g3module::CFDMModuleT, _altova_mi_ns_g3module_altova_CModulesT_altova_FDM> FDM;
	struct FDM { typedef Iterator<ns_g3module::CFDMModuleT> iterator; };
	MemberElement<ns_g3module::CFPMModuleT, _altova_mi_ns_g3module_altova_CModulesT_altova_FPM> FPM;
	struct FPM { typedef Iterator<ns_g3module::CFPMModuleT> iterator; };
	MemberElement<ns_g3module::CHMIModuleT, _altova_mi_ns_g3module_altova_CModulesT_altova_HMI> HMI;
	struct HMI { typedef Iterator<ns_g3module::CHMIModuleT> iterator; };
	MemberElement<ns_g3module::CIOMModuleT, _altova_mi_ns_g3module_altova_CModulesT_altova_IOM> IOM;
	struct IOM { typedef Iterator<ns_g3module::CIOMModuleT> iterator; };
	MemberElement<ns_g3module::CMCMModuleT, _altova_mi_ns_g3module_altova_CModulesT_altova_MCM> MCM;
	struct MCM { typedef Iterator<ns_g3module::CMCMModuleT> iterator; };
	MemberElement<ns_g3module::CSCMMK2ModuleT, _altova_mi_ns_g3module_altova_CModulesT_altova_SCM_MK2> SCM_MK2;
	struct SCM_MK2 { typedef Iterator<ns_g3module::CSCMMK2ModuleT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_g3module

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CModulesT
