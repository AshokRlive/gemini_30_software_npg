#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_iec870_ALTOVA_CIEC870PointsT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_iec870_ALTOVA_CIEC870PointsT



namespace g3schema
{

namespace ns_iec870
{	

class CIEC870PointsT : public TypeBase
{
public:
	g3schema_EXPORT CIEC870PointsT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CIEC870PointsT(CIEC870PointsT const& init);
	void operator=(CIEC870PointsT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_iec870_altova_CIEC870PointsT); }
	MemberElement<ns_iec870::CIEC870InputPointT, _altova_mi_ns_iec870_altova_CIEC870PointsT_altova_mmea> mmea;
	struct mmea { typedef Iterator<ns_iec870::CIEC870InputPointT> iterator; };
	MemberElement<ns_iec870::CIEC870InputPointT, _altova_mi_ns_iec870_altova_CIEC870PointsT_altova_mmeb> mmeb;
	struct mmeb { typedef Iterator<ns_iec870::CIEC870InputPointT> iterator; };
	MemberElement<ns_iec870::CIEC870InputPointT, _altova_mi_ns_iec870_altova_CIEC870PointsT_altova_mmec> mmec;
	struct mmec { typedef Iterator<ns_iec870::CIEC870InputPointT> iterator; };
	MemberElement<ns_iec870::CIEC870InputPointT, _altova_mi_ns_iec870_altova_CIEC870PointsT_altova_msp> msp;
	struct msp { typedef Iterator<ns_iec870::CIEC870InputPointT> iterator; };
	MemberElement<ns_iec870::CIEC870InputPointT, _altova_mi_ns_iec870_altova_CIEC870PointsT_altova_mdp> mdp;
	struct mdp { typedef Iterator<ns_iec870::CIEC870InputPointT> iterator; };
	MemberElement<ns_iec870::CIEC870OutputPointT, _altova_mi_ns_iec870_altova_CIEC870PointsT_altova_csc> csc;
	struct csc { typedef Iterator<ns_iec870::CIEC870OutputPointT> iterator; };
	MemberElement<ns_iec870::CIEC870OutputPointT, _altova_mi_ns_iec870_altova_CIEC870PointsT_altova_cdc> cdc;
	struct cdc { typedef Iterator<ns_iec870::CIEC870OutputPointT> iterator; };
	MemberElement<ns_iec870::CIEC870CounterPointT, _altova_mi_ns_iec870_altova_CIEC870PointsT_altova_mit> mit;
	struct mit { typedef Iterator<ns_iec870::CIEC870CounterPointT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_iec870

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_iec870_ALTOVA_CIEC870PointsT
