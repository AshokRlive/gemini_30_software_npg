#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen11_ALTOVA_CMOTOR_MODE
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen11_ALTOVA_CMOTOR_MODE



namespace g3schema
{

namespace ns_gen11
{	

class CMOTOR_MODE : public TypeBase
{
public:
	g3schema_EXPORT CMOTOR_MODE(xercesc::DOMNode* const& init);
	g3schema_EXPORT CMOTOR_MODE(CMOTOR_MODE const& init);
	void operator=(CMOTOR_MODE const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen11_altova_CMOTOR_MODE); }

	enum EnumValues {
		Invalid = -1,
		k_MOTOR_MODE_1 = 0, // MOTOR_MODE_1
		k_MOTOR_MODE_2 = 1, // MOTOR_MODE_2
		k_MOTOR_MODE_3 = 2, // MOTOR_MODE_3
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen11

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen11_ALTOVA_CMOTOR_MODE
