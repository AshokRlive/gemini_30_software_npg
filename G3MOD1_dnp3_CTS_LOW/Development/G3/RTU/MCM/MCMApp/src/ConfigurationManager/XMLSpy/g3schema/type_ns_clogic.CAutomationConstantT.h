#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CAutomationConstantT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CAutomationConstantT



namespace g3schema
{

namespace ns_clogic
{	

class CAutomationConstantT : public TypeBase
{
public:
	g3schema_EXPORT CAutomationConstantT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CAutomationConstantT(CAutomationConstantT const& init);
	void operator=(CAutomationConstantT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_clogic_altova_CAutomationConstantT); }

	MemberAttribute<string_type,_altova_mi_ns_clogic_altova_CAutomationConstantT_altova_name, 0, 0> name;	// name Cstring

	MemberAttribute<string_type,_altova_mi_ns_clogic_altova_CAutomationConstantT_altova_value2, 0, 0> value2;	// value CanySimpleType

	MemberAttribute<string_type,_altova_mi_ns_clogic_altova_CAutomationConstantT_altova_valueType, 0, 0> valueType;	// valueType Cstring

	MemberAttribute<string_type,_altova_mi_ns_clogic_altova_CAutomationConstantT_altova_unit, 0, 0> unit;	// unit Cstring
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_clogic

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CAutomationConstantT
