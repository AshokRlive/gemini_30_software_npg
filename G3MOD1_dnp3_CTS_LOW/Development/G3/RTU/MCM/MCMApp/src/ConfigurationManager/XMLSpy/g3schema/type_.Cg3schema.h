#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_Cg3schema
#define _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_Cg3schema



namespace g3schema
{

class Cg3schema : public TypeBase
{
public:
	g3schema_EXPORT Cg3schema(xercesc::DOMNode* const& init);
	g3schema_EXPORT Cg3schema(Cg3schema const& init);
	void operator=(Cg3schema const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_altova_Cg3schema); }
	MemberElement<ns_vpoint::CVirtualPointsT, _altova_mi_altova_Cg3schema_altova_virtualPoints> virtualPoints;
	struct virtualPoints { typedef Iterator<ns_vpoint::CVirtualPointsT> iterator; };
	MemberElement<CConfigurationT, _altova_mi_altova_Cg3schema_altova_configuration> configuration;
	struct configuration { typedef Iterator<CConfigurationT> iterator; };
	g3schema_EXPORT void SetXsiType();

	// document functions
	g3schema_EXPORT static Cg3schema LoadFromFile(const string_type& fileName);
	g3schema_EXPORT static Cg3schema LoadFromString(const string_type& xml);
	g3schema_EXPORT static Cg3schema LoadFromBinary(const std::vector<unsigned char>& data);
	g3schema_EXPORT void SaveToFile( const string_type& fileName, bool prettyPrint );
	g3schema_EXPORT void SaveToFile( const string_type& fileName, bool prettyPrint, const string_type& encoding );
	g3schema_EXPORT void SaveToFile( const string_type& fileName, bool prettyPrint, const string_type& encoding, bool bBigEndian, bool bBOM );
	g3schema_EXPORT string_type SaveToString(bool prettyPrint);
	g3schema_EXPORT std::vector<unsigned char> SaveToBinary(bool prettyPrint);
	g3schema_EXPORT std::vector<unsigned char> SaveToBinary(bool prettyPrint, const string_type& encoding);
	g3schema_EXPORT std::vector<unsigned char> SaveToBinary(bool prettyPrint, const string_type& encoding, bool bBigEndian, bool bBOM);
 	g3schema_EXPORT static Cg3schema CreateDocument();
	g3schema_EXPORT void DestroyDocument();
	g3schema_EXPORT void SetDTDLocation(const string_type& dtdLocation);
	g3schema_EXPORT void SetSchemaLocation(const string_type& schemaLocation);
protected:
	XercesTreeOperations::DocumentType GetDocumentNode() { return (XercesTreeOperations::DocumentType)m_node; }
};


}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_Cg3schema
