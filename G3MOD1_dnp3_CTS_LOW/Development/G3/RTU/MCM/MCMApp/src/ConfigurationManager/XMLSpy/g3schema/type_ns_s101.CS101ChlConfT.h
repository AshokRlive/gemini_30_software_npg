#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s101_ALTOVA_CS101ChlConfT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s101_ALTOVA_CS101ChlConfT

#include "type_ns_iec870.CIEC870ChnlConfT.h"


namespace g3schema
{

namespace ns_s101
{	

class CS101ChlConfT : public ::g3schema::ns_iec870::CIEC870ChnlConfT
{
public:
	g3schema_EXPORT CS101ChlConfT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CS101ChlConfT(CS101ChlConfT const& init);
	void operator=(CS101ChlConfT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_s101_altova_CS101ChlConfT); }
	MemberElement<ns_s101::CS101LinkLayerConfT, _altova_mi_ns_s101_altova_CS101ChlConfT_altova_linkLayer> linkLayer;
	struct linkLayer { typedef Iterator<ns_s101::CS101LinkLayerConfT> iterator; };
	MemberElement<ns_s101::CS101LinuxIoT, _altova_mi_ns_s101_altova_CS101ChlConfT_altova_linuxIO> linuxIO;
	struct linuxIO { typedef Iterator<ns_s101::CS101LinuxIoT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_s101

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s101_ALTOVA_CS101ChlConfT
