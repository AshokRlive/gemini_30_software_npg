#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CDeadBandFilterT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CDeadBandFilterT



namespace g3schema
{

namespace ns_vpoint
{	

class CDeadBandFilterT : public TypeBase
{
public:
	g3schema_EXPORT CDeadBandFilterT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CDeadBandFilterT(CDeadBandFilterT const& init);
	void operator=(CDeadBandFilterT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CDeadBandFilterT); }

	MemberAttribute<double,_altova_mi_ns_vpoint_altova_CDeadBandFilterT_altova_deadBand, 0, 0> deadBand;	// deadBand Cdouble

	MemberAttribute<double,_altova_mi_ns_vpoint_altova_CDeadBandFilterT_altova_initialNominal, 0, 0> initialNominal;	// initialNominal Cdouble

	MemberAttribute<bool,_altova_mi_ns_vpoint_altova_CDeadBandFilterT_altova_initialNominalEnabled, 0, 0> initialNominalEnabled;	// initialNominalEnabled Cboolean
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CDeadBandFilterT
