#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_pstack_ALTOVA_CTCPConfT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_pstack_ALTOVA_CTCPConfT



namespace g3schema
{

namespace ns_pstack
{	

class CTCPConfT : public TypeBase
{
public:
	g3schema_EXPORT CTCPConfT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CTCPConfT(CTCPConfT const& init);
	void operator=(CTCPConfT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_pstack_altova_CTCPConfT); }

	MemberAttribute<string_type,_altova_mi_ns_pstack_altova_CTCPConfT_altova_ipAddress, 0, 0> ipAddress;	// ipAddress Cstring

	MemberAttribute<unsigned,_altova_mi_ns_pstack_altova_CTCPConfT_altova_ipPort, 0, 0> ipPort;	// ipPort CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_pstack_altova_CTCPConfT_altova_ipConnectTimeout, 0, 0> ipConnectTimeout;	// ipConnectTimeout CunsignedInt
	MemberAttribute<string_type,_altova_mi_ns_pstack_altova_CTCPConfT_altova_mode, 0, 3> mode;	// mode CLU_LINTCP_MODE
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_pstack

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_pstack_ALTOVA_CTCPConfT
