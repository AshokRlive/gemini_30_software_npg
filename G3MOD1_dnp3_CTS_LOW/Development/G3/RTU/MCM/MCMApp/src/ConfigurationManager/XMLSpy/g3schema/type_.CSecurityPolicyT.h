#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CSecurityPolicyT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CSecurityPolicyT



namespace g3schema
{

class CSecurityPolicyT : public TypeBase
{
public:
	g3schema_EXPORT CSecurityPolicyT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSecurityPolicyT(CSecurityPolicyT const& init);
	void operator=(CSecurityPolicyT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_altova_CSecurityPolicyT); }

	MemberAttribute<unsigned,_altova_mi_altova_CSecurityPolicyT_altova_invalidLogonAttemptsThreshold, 0, 0> invalidLogonAttemptsThreshold;	// invalidLogonAttemptsThreshold CunsignedInt

	MemberAttribute<unsigned,_altova_mi_altova_CSecurityPolicyT_altova_accountLockoutDurationSecs, 0, 0> accountLockoutDurationSecs;	// accountLockoutDurationSecs CunsignedInt
	g3schema_EXPORT void SetXsiType();
};


}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_CSecurityPolicyT
