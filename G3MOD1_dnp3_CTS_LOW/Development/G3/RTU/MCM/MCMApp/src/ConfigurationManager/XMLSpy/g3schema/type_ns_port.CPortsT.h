#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_port_ALTOVA_CPortsT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_port_ALTOVA_CPortsT



namespace g3schema
{

namespace ns_port
{	

class CPortsT : public TypeBase
{
public:
	g3schema_EXPORT CPortsT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CPortsT(CPortsT const& init);
	void operator=(CPortsT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_port_altova_CPortsT); }
	MemberElement<ns_port::CEthernetPortConfT, _altova_mi_ns_port_altova_CPortsT_altova_ethernet> ethernet;
	struct ethernet { typedef Iterator<ns_port::CEthernetPortConfT> iterator; };
	MemberElement<ns_port::CSerialPortConfT, _altova_mi_ns_port_altova_CPortsT_altova_serial> serial;
	struct serial { typedef Iterator<ns_port::CSerialPortConfT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_port

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_port_ALTOVA_CPortsT
