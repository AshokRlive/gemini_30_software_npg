#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen2_ALTOVA_CLU_LIN232_DATA_BITS
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen2_ALTOVA_CLU_LIN232_DATA_BITS



namespace g3schema
{

namespace ns_gen2
{	

class CLU_LIN232_DATA_BITS : public TypeBase
{
public:
	g3schema_EXPORT CLU_LIN232_DATA_BITS(xercesc::DOMNode* const& init);
	g3schema_EXPORT CLU_LIN232_DATA_BITS(CLU_LIN232_DATA_BITS const& init);
	void operator=(CLU_LIN232_DATA_BITS const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen2_altova_CLU_LIN232_DATA_BITS); }

	enum EnumValues {
		Invalid = -1,
		k_LU_LIN232_DATA_BITS_7 = 0, // LU_LIN232_DATA_BITS_7
		k_LU_LIN232_DATA_BITS_8 = 1, // LU_LIN232_DATA_BITS_8
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen2

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen2_ALTOVA_CLU_LIN232_DATA_BITS
