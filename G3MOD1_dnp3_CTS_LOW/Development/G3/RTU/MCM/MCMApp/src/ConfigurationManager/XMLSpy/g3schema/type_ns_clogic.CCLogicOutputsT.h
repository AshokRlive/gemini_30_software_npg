#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CCLogicOutputsT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CCLogicOutputsT



namespace g3schema
{

namespace ns_clogic
{	

class CCLogicOutputsT : public TypeBase
{
public:
	g3schema_EXPORT CCLogicOutputsT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CCLogicOutputsT(CCLogicOutputsT const& init);
	void operator=(CCLogicOutputsT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_clogic_altova_CCLogicOutputsT); }

	MemberAttribute<string_type,_altova_mi_ns_clogic_altova_CCLogicOutputsT_altova_name, 0, 0> name;	// name Cstring
	MemberElement<ns_common::CControlLogicRefT, _altova_mi_ns_clogic_altova_CCLogicOutputsT_altova_outputLogic> outputLogic;
	struct outputLogic { typedef Iterator<ns_common::CControlLogicRefT> iterator; };
	MemberElement<ns_common::COutputModuleRefT, _altova_mi_ns_clogic_altova_CCLogicOutputsT_altova_outputModule> outputModule;
	struct outputModule { typedef Iterator<ns_common::COutputModuleRefT> iterator; };
	MemberElement<ns_common::COutputChannelRefT, _altova_mi_ns_clogic_altova_CCLogicOutputsT_altova_outputChannel> outputChannel;
	struct outputChannel { typedef Iterator<ns_common::COutputChannelRefT> iterator; };
	MemberElement<xs::CanyType, _altova_mi_ns_clogic_altova_CCLogicOutputsT_altova_none> none;
	struct none { typedef Iterator<xs::CanyType> iterator; };
	MemberElement<ns_common::CVirtualPointRefT, _altova_mi_ns_clogic_altova_CCLogicOutputsT_altova_outputPoint> outputPoint;
	struct outputPoint { typedef Iterator<ns_common::CVirtualPointRefT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_clogic

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CCLogicOutputsT
