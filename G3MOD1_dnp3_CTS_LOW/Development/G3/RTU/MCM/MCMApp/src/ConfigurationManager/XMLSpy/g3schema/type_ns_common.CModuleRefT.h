#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_CModuleRefT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_CModuleRefT



namespace g3schema
{

namespace ns_common
{	

class CModuleRefT : public TypeBase
{
public:
	g3schema_EXPORT CModuleRefT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CModuleRefT(CModuleRefT const& init);
	void operator=(CModuleRefT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_common_altova_CModuleRefT); }
	MemberAttribute<string_type,_altova_mi_ns_common_altova_CModuleRefT_altova_moduleType, 0, 32> moduleType;	// moduleType CMODULE
	MemberAttribute<string_type,_altova_mi_ns_common_altova_CModuleRefT_altova_moduleID, 0, 26> moduleID;	// moduleID CMODULE_ID
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_common

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_CModuleRefT
