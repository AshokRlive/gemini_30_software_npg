#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3OutputPointT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3OutputPointT

#include "type_ns_sdnp3.CSDNP3PointT.h"


namespace g3schema
{

namespace ns_sdnp3
{	

class CSDNP3OutputPointT : public ::g3schema::ns_sdnp3::CSDNP3PointT
{
public:
	g3schema_EXPORT CSDNP3OutputPointT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSDNP3OutputPointT(CSDNP3OutputPointT const& init);
	void operator=(CSDNP3OutputPointT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_sdnp3_altova_CSDNP3OutputPointT); }

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CSDNP3OutputPointT_altova_controlLogicGroup, 0, 0> controlLogicGroup;	// controlLogicGroup CVirtualPointGroupT

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CSDNP3OutputPointT_altova_acceptedCommand, 0, 0> acceptedCommand;	// acceptedCommand CunsignedByte
	MemberElement<ns_sdnp3::CSDNP3EventConfigT, _altova_mi_ns_sdnp3_altova_CSDNP3OutputPointT_altova_outputEvent> outputEvent;
	struct outputEvent { typedef Iterator<ns_sdnp3::CSDNP3EventConfigT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_sdnp3

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3OutputPointT
