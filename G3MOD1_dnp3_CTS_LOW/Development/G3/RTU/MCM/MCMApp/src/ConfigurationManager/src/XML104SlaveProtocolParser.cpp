/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:XML104SlaveProtocolParser.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief Slave IEC-104 Protocol Configuration Parser
 *       
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <vector>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "XML104SlaveProtocolParser.h"
#include "bitOperations.h"
#include "StringUtil.h"
#include "XMLParser_Points.h"
#include "IEC870Enum.h"
#include "ProtocolStackCommonEnum.h"
#include "Logger.h"
#include "S104Protocol.h"
#include "S104Debug.h"

using namespace ns_common;
using namespace ns_pstack;
using namespace ns_iec870;
using namespace ns_s104;
using namespace ns_vpoint;

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*NEEEED*/
#define S104_CHECK_POINT_FIELD(xmlProtocolID, xmlEnable, xmlVPoint)\
{\
    if(!xmlProtocolID.exists())\
    {\
        continue;   /* Skip points without protocol ID */\
    }\
    if(xmlEnable.exists())\
    {\
        if(xmlEnable == LU_FALSE)\
        {\
            continue;   /* Skip disabled points */\
        }\
    }\
    lu_uint32_t protocolID = xmlProtocolID;\
    if(!xmlVPoint.exists())\
    {\
        log.warn("S104_CHECK_POINT_FIELD: Protocol Point %i, doesn't have a linked virtual point. Skipping.",\
                  protocolID\
                  );\
        continue;   /* Skip points not linked with the Gemini DB */\
    }\
}

#if !DEBUG_IEC_104_SLAVE
#define DBG_DUMP_CHANNEL104(a)    do {} while(0)
#define DBG_DUMP_SESSION104(a,b)    do {} while(0)
#define DBG_DUMP_SECTOR104()    do {} while(0)
#else
#define DBG_DUMP_CHANNEL104(channelConfig)                  \
    do {                                                    \
        DBG_INFO("%s Channel:\n"                            \
                    "incrementalTimeout=%lu\n"              \
                    "rxFrameSize       =%d\n"               \
                    "txFrameSize       =%d\n"               \
                    "t0=%lu\n"                              \
                    "t1=%lu\n"                              \
                    "t2=%lu\n"                              \
                    "t3=%lu\n"                              \
                    "k =%d\n"                               \
                    "w =%d\n"                               \
                    "discardIFramesOnDisconnect  =%d\n"     \
                    "disconnectOnNewSyn=%d\n"               \
                    "validateUDPAddress=%d\n"               \
                    "chnlName          =%s\n"               \
                    "ipAddress         =%s\n"               \
                    "ipPort            =%d\n"               \
                    "ipConnectTimeout  =%lu\n"              \
                    "mode              =%s\n"               \
                    "type              =%s\n"               \
                    "redundancySize    =%d\n",              \
            TITLE,                                          \
            channelConfig.pChnlConfig.incrementalTimeout        ,    \
            channelConfig.pLinkConfig.rxFrameSize               ,    \
            channelConfig.pLinkConfig.txFrameSize               ,    \
            channelConfig.pLinkConfig.offlinePollPeriod         ,    \
            channelConfig.pLinkConfig.t1AckPeriod               ,    \
            channelConfig.pLinkConfig.t2SFramePeriod            ,    \
            channelConfig.pLinkConfig.t3TestPeriod              ,    \
            channelConfig.pLinkConfig.kValue                    ,    \
            channelConfig.pLinkConfig.wValue                    ,    \
            channelConfig.pLinkConfig.discardIFramesOnDisconnect,    \
            channelConfig.pIOConfig.linTCP.disconnectOnNewSyn   ,    \
            channelConfig.pIOConfig.linTCP.validateUDPAddress   ,    \
            channelConfig.pIOConfig.linTCP.chnlName             ,    \
            channelConfig.pIOConfig.linTCP.ipAddress            ,    \
            channelConfig.pIOConfig.linTCP.ipPort               ,    \
            channelConfig.pIOConfig.linTCP.ipConnectTimeout     ,    \
            (channelConfig.pIOConfig.linTCP.mode==LINTCP_MODE_SERVER)? "Server" :               \
                (channelConfig.pIOConfig.linTCP.mode==LINTCP_MODE_CLIENT)? "Client" :           \
                    (channelConfig.pIOConfig.linTCP.mode==LINTCP_MODE_DUAL_ENDPOINT)? "Dual" :  \
                                                            "UDP",              \
            (channelConfig.pIOConfig.type==LINIO_TYPE_TCP)? "TCP":"INCORRECT",  \
            channelConfig.rdcySize );                                \
        for (lu_uint16_t i = 0; i < channelConfig.rdcySize; i++)     \
        {                                                            \
            DBG_INFO("\t%s Redundancy %d: IP=%s:%d",                 \
                    TITLE, i+1,                                      \
                    channelConfig.rdcyConfig[i]->ipAddress,          \
                    channelConfig.rdcyConfig[i]->ipPort );           \
        }                                                            \
    } while(0)


#define DBG_DUMP_SESSION104(maxAPDUSize, sessionConfigStr)  \
    do {                                                    \
        DBG_INFO("%s Session:\n"                            \
                    "maxAPDUsize=%d\n"                      \
                    "ASDUsize=%d\n"                         \
                    "useDayOfWeek=%d\n",                    \
                TITLE,                                      \
                maxAPDUSize,                                \
                sessionConfigStr.maxASDUSize,               \
                sessionConfigStr.useDayOfWeek);             \
    } while(0)

#define DBG_MMX_TIME_FORMAT(timeFormat)                                     \
        (timeFormat==TMWDEFS_TIME_FORMAT_NONE)? "None" :                    \
            (timeFormat==TMWDEFS_TIME_FORMAT_24)? "24bit" :                 \
                (timeFormat==TMWDEFS_TIME_FORMAT_56)? "56bit" :             \
                    (timeFormat==TMWDEFS_TIME_FORMAT_UNKNOWN)? "UNK (Default)" : "INVALID"

#define DBG_MMX_EVENT_MODE(eventMode)                                       \
        (eventMode==TMWDEFS_EVENT_MODE_SOE)? "SOE" :                        \
            (eventMode==TMWDEFS_EVENT_MODE_MOST_RECENT)? "MostRecent" : "INVALID"

#define DBG_DUMP_SECTOR104()                                \
    do {                                                    \
        DBG_INFO("%s Sector:\n"                             \
                    "ASDUaddress=%d\n"                      \
                    "cyclicPeriod=%lu\n"                    \
                    "cyclicFirstPeriod=%lu\n"               \
                    "backgroundPeriod=%lu\n"                \
                    "selectTimeout=%lu\n"                   \
                    "defaultResponseTimeout=%lu\n"          \
                    "cseUseActTerm=%d\n"                    \
                    "cmdUseActTerm=%d\n"                    \
                    "maxCommandAge=%lu\n"                   \
                    "maxCommandFuture=%lu\n"                \
                    "readTimeFormat=%s\n"                   \
                    "readMsrndTimeFormat=%s\n"              \
                    "cicnaTimeFormat=%s\n"                  \
                    "sendClockSyncEvents=%d\n"              \
                    "deleteOldestEvent=%d\n"                \
                    "multiplePMEN=%d\n",                    \
                TITLE,                                      \
                sectorConfigStr.asduAddress,                \
                sectorConfigStr.cyclicPeriod,               \
                sectorConfigStr.cyclicFirstPeriod,          \
                sectorConfigStr.backgroundPeriod,           \
                sectorConfigStr.selectTimeout,              \
                sectorConfigStr.defaultResponseTimeout,     \
                sectorConfigStr.cseUseActTerm,              \
                sectorConfigStr.cmdUseActTerm,              \
                sectorConfigStr.maxCommandAge,              \
                sectorConfigStr.maxCommandFuture,           \
                DBG_MMX_TIME_FORMAT(sectorConfigStr.readTimeFormat),         \
                DBG_MMX_TIME_FORMAT(sectorConfigStr.readMsrndTimeFormat),    \
                DBG_MMX_TIME_FORMAT(sectorConfigStr.cicnaTimeFormat),        \
                sectorConfigStr.sendClockSyncEvents,        \
                sectorConfigStr.deleteOldestEvent,          \
                multiplePMEN);                              \
        DBG_INFO("%s Sector Events:\n"                      \
                    "mspMaxEvents =%d\n"                    \
                    "mspEventMode =%s\n"                    \
                    "mspTimeFormat=%s\n"                    \
                    "mdpMaxEvents =%d\n"                    \
                    "mdpEventMode =%s\n"                    \
                    "mdpTimeFormat=%s\n"                    \
                    "mitMaxEvents =%d\n"                    \
                    "mitEventMode =%s\n"                    \
                    "mitTimeFormat=%s\n"                    \
                    "mmenaMaxEvents =%d\n"                  \
                    "mmenaEventMode =%s\n"                  \
                    "mmenaTimeFormat=%s\n"                  \
                    "mmenbMaxEvents =%d\n"                  \
                    "mmenbEventMode =%s\n"                  \
                    "mmenbTimeFormat=%s\n"                  \
                    "mmencMaxEvents =%d\n"                  \
                    "mmencEventMode =%s\n"                  \
                    "mmencTimeFormat=%s\n",                 \
                TITLE,                                      \
                sectorConfigStr.mspMaxEvents,               \
                DBG_MMX_EVENT_MODE(sectorConfigStr.mspEventMode),       \
                DBG_MMX_TIME_FORMAT(sectorConfigStr.mspTimeFormat),     \
                sectorConfigStr.mdpMaxEvents,                           \
                DBG_MMX_EVENT_MODE(sectorConfigStr.mdpEventMode),       \
                DBG_MMX_TIME_FORMAT(sectorConfigStr.mdpTimeFormat),     \
                sectorConfigStr.mitMaxEvents,                           \
                DBG_MMX_EVENT_MODE(sectorConfigStr.mitEventMode),       \
                DBG_MMX_TIME_FORMAT(sectorConfigStr.mitTimeFormat),     \
                sectorConfigStr.mmenaMaxEvents,                         \
                DBG_MMX_EVENT_MODE(sectorConfigStr.mmenaEventMode),     \
                DBG_MMX_TIME_FORMAT(sectorConfigStr.mmenaTimeFormat),   \
                sectorConfigStr.mmenbMaxEvents,                         \
                DBG_MMX_EVENT_MODE(sectorConfigStr.mmenbEventMode),     \
                DBG_MMX_TIME_FORMAT(sectorConfigStr.mmenbTimeFormat),   \
                sectorConfigStr.mmencMaxEvents,                         \
                DBG_MMX_EVENT_MODE(sectorConfigStr.mmencEventMode),     \
                DBG_MMX_TIME_FORMAT(sectorConfigStr.mmencTimeFormat)    \
                );                                                      \
    } while(0)
#endif


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

template <typename XMLSource, typename Dest>
void getSessionTimeFormat(XMLSource sourceXML, Dest& dest) throw(ParserException)
{
    dest = TMWDEFS_TIME_FORMAT_56;  //Default value as set by TMW
    CIEC870SesnEventT event = getXMLElement<CIEC870SesnEventT>(sourceXML);
    getXMLOptAttrEnum(sourceXML.first().timeFormat, LU_TIME_FORMAT_VALUEfromINDEX, dest);
}


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

XMLSlave104ProtocolParser::XMLSlave104ProtocolParser(Cg3schema&          configuration,
                                                       CommsDeviceManager& commsDeviceManager,
                                                       PortManager&        portManager,
                                                       ModuleManager&      moduleManager) :
                                                       configuration(configuration),
                                                       commsDeviceManager(commsDeviceManager),
                                                       portManager(portManager),
                                                       moduleManager(moduleManager),
                                                       log(Logger::getLogger(SUBSYSTEM_ID_CONFMGR))
{
    initParser();
}

XMLSlave104ProtocolParser::~XMLSlave104ProtocolParser()
{}

S104Protocol* XMLSlave104ProtocolParser::newProtocolManager(GeminiDatabase& GDatabase )

{
    try
    {
        checkXMLElement(configuration.configuration);
        checkXMLElement(configuration.configuration.first().scadaProtocol);
        /* Get the 104 Slave Object (there should only be 0 or 1 of these) */
        checkXMLElement(configuration.configuration.first().scadaProtocol.first().sIEC104);
    }
    catch (const ParserException &e)
    {
        return NULL;    //No IEC104 Config present
    }
    S104Protocol *s104protocolManager = new S104Protocol(GDatabase);
    try
    {
        // Loop over channels, adding a new channelConfig element and initialising it
        CS104T s104 = getXMLElement<CS104T>(configuration.configuration.first().scadaProtocol.first().sIEC104);

        // Iterate through the configured channels
        for (Iterator<CS104ChannelT> itChannelConfig = s104.channel.all();
             itChannelConfig;
             ++itChannelConfig)
        {
            checkXMLElement(itChannelConfig.config);

            lu_bool_t is104chlEnabled = true;   //Flag which is set to false if
                                                // optional attribute(enabled) is present

            getXMLOptAttr(itChannelConfig.config.first().enabled,is104chlEnabled);
            if (is104chlEnabled == false)
            {
                std::string chnlName = itChannelConfig.config.first().chnlName;
                DBG_INFO("104Slave Channel %s is disabled",chnlName.c_str());
                continue;
            }


            S104Channel::S104ChannelConfig s104ChnlConf;

            parseChannelConfig(itChannelConfig, s104ChnlConf);

            DBG_DUMP_CHANNEL104(s104ChnlConf);

            S104Channel *channelPtr = new S104Channel(GDatabase, s104protocolManager);

            channelPtr->configure(s104ChnlConf);

            // Iterate through the configured sessions
            for (Iterator<CS104SessionT> itSessionConfig = itChannelConfig.session.all();
                 itSessionConfig;
                 ++itSessionConfig)
            {

                lu_bool_t is104sesEnabled = true ;// flag which is set to false if
                //optional attribute(enabled) is present

                getXMLOptAttr(itSessionConfig.config.first().enabled,is104sesEnabled);

                if (is104sesEnabled == false)
                {
                    std::string seshName = itSessionConfig.config.first().sessionName;
                    DBG_INFO("104Slave Session : %s is disabled", seshName.c_str());
                    continue;
                }

                S104SESN_CONFIG s104sessionConfig;
                S104SCTR_CONFIG s104sectorConfig;

                /* Parse the Session config */
                lu_uint32_t maxAPDUSize;
                maxAPDUSize = parseSessionConfig(itSessionConfig, s104sessionConfig);
                //Convert from APDU size to ASDU size (ASDU = APDU - 4 control fields)
                s104sessionConfig.maxASDUSize = maxAPDUSize - 4;

                DBG_DUMP_SESSION104(maxAPDUSize, s104sessionConfig);

                /* Parse the Sector config */
                parseSectorConfig(itSessionConfig, s104sectorConfig);


 //TODO - SKA - Get the sessionID from the XML
                S104Session *sessionPtr = new S104Session(*channelPtr);

                S104Sector *sectorPtr   = new S104Sector(*sessionPtr);

                /* Parse Analogue input points */
                if (itSessionConfig->iomap.first().mmea.exists())
                {
                    parseAnalogConfig(GDatabase, &(itSessionConfig),S104InputPoint::MMENA, sectorPtr);
                }
                if(itSessionConfig->iomap.first().mmeb.exists())
                {
                    parseAnalogConfig(GDatabase, &(itSessionConfig),S104InputPoint::MMENB, sectorPtr);
                }
                if(itSessionConfig->iomap.first().mmec.exists())
                {
                    parseAnalogConfig(GDatabase, &(itSessionConfig),S104InputPoint::MMENC, sectorPtr);
                }


                /* Parse Binary input points */
                parseBinaryConfig(GDatabase, &(itSessionConfig), sectorPtr);

                /* Parse Double Binary input points */
                parseDBinaryConfig(GDatabase, &(itSessionConfig), sectorPtr);

                /* Parse Binary output points */
                parseBinaryOutputConfig(&(itSessionConfig), sectorPtr);

                /* Parse Binary output points */
                parseDBinaryOutputConfig(&(itSessionConfig), sectorPtr);

                /*Parse Integrated Totals points*/
                parseIntegratedTotsConfig(GDatabase, &(itSessionConfig), sectorPtr);

                sectorPtr->configure(s104sectorConfig);

                // Add the sector to the vector of sectors
                sessionPtr->addSector(sectorPtr);

                sessionPtr->configure(s104sessionConfig);

                // Add the session to the vector of sessions
                channelPtr->addSession(sessionPtr);
            }
            // Add the channel to the vector of channels
            s104protocolManager->addChannel(channelPtr);
        }
    }
    catch (const ParserException &e)
    {
        DBG_ERR("%s",e.what());
        log.error("%s in %s",e.what(), __AT__);
    }
    catch (...)
    {
        log.error("An unknown error occured  at %s", __AT__);
    }

    return s104protocolManager;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
CONFMGR_ERROR XMLSlave104ProtocolParser::initParser()
{
    try
    {
        checkXMLElement(configuration.configuration);
        CConfigurationT configurationRoot = getXMLElement<CConfigurationT>(configuration.configuration);
        CScadaProtocolT scadaProtocol =  getXMLElement<CScadaProtocolT>(configurationRoot.scadaProtocol);

        /* Check if 104 protocol is present */
        if(scadaProtocol.sIEC104.exists())
        {
            // Maximum of one instance of this 104 Slave Session
            if (scadaProtocol.sIEC104.count() <= 1)
            {
                return CONFMGR_ERROR_NONE;
            }
        }
        else
        {
            DBG_INFO("IEC-104 protocol is not added");
        }
    }
    catch (const ParserException &e)
    {
        DBG_ERR("%s",e.what());
        log.error("%s in %s",e.what(), __AT__);
        return CONFMGR_ERROR_NOT_INITIALIZED;
    }
    return CONFMGR_ERROR_INITPARSER;
}


void XMLSlave104ProtocolParser::parseAnalogConfig( GeminiDatabase           &GDatabase,
                                                     CS104SessionT      *sessionParserPtr,
                                                     S104InputPoint::ProtPointType analogType,
                                                     S104Sector *sectorPtr)
{
    /* count for number of analog input points*/
    lu_uint8_t mmeaNos = 0;
    lu_uint8_t mmebNos = 0;
    lu_uint8_t mmecNos = 0;

    S104InputPoint::Config* mmeaPointConf = NULL;
    S104Analog** mmeaPoint = NULL;
    S104InputPoint::Config* mmebPointConf = NULL;
    S104Analog** mmebPoint = NULL;
    S104InputPoint::Config* mmecPointConf = NULL;
    S104Analog** mmecPoint = NULL;

    try
    {
        switch(analogType)
        {
            case S104InputPoint::MMENA :

                /*Count number of analog input points*/
                for(Iterator<CIEC870InputPointT> itAnalogConfig = sessionParserPtr->iomap.first().mmea.all();
                        itAnalogConfig; ++itAnalogConfig)
                {
                    /* Skip points without valid protocol ID, disabled or with no related G3 point */
                    S104_CHECK_POINT_FIELD(itAnalogConfig.protocolID, itAnalogConfig.enable, itAnalogConfig.vpoint);
                    mmeaNos++;   // Count no of analog input points
                }

                mmeaPointConf = new S104InputPoint::Config[mmeaNos];
                mmeaPoint = new S104Analog*[mmeaNos];

                for(Iterator<CIEC870InputPointT> itAnalogConfig = sessionParserPtr->iomap.first().mmea.all();
                        itAnalogConfig; ++itAnalogConfig)
                {
                    lu_uint8_t pointIndex = 0;

                    /* Skip points without valid protocol ID, disabled or with no related G3 point */
                    S104_CHECK_POINT_FIELD(itAnalogConfig.protocolID, itAnalogConfig.enable, itAnalogConfig.vpoint);

                    /* Get time format */
                    lu_uint32_t timeFormat = TMWDEFS_TIME_FORMAT_56; //default
                    getSessionTimeFormat(sessionParserPtr->config.first().eventConfig.first().mmea, timeFormat);
                    mmeaPointConf[pointIndex].pointAttributes.timeformat = (TMWDEFS_TIME_FORMAT)(timeFormat);

                    parseCommonAnalogAttributes(&itAnalogConfig ,pointIndex, mmeaPointConf);

                    mmeaPoint[pointIndex]= new S104Analog(S104InputPoint::MMENA,mmeaPointConf[pointIndex]);
                    sectorPtr->getS104Database().addInputPoint(S104InputPoint::MMENA,*(&mmeaPoint[pointIndex]));
                    GDatabase.attach(mmeaPointConf[pointIndex].vpointID, *(&mmeaPoint[pointIndex]));

                    pointIndex++;
                 }

            break;

            case S104InputPoint::MMENB :

                /*Count number of analog input points*/
                for(Iterator<CIEC870InputPointT> itAnalogConfig = sessionParserPtr->iomap.first().mmeb.all();
                    itAnalogConfig; ++itAnalogConfig)
                {
                    /* Skip points without valid protocol ID, disabled or with no related G3 point */
                    S104_CHECK_POINT_FIELD(itAnalogConfig.protocolID, itAnalogConfig.enable, itAnalogConfig.vpoint);
                    mmebNos++;   // Count no of analog input points
                }

                mmebPointConf = new S104InputPoint::Config[mmebNos];
                mmebPoint = new S104Analog*[mmebNos];

                for(Iterator<CIEC870InputPointT> itAnalogConfig = sessionParserPtr->iomap.first().mmeb.all();
                        itAnalogConfig; ++itAnalogConfig)
                {
                    lu_uint8_t pointIndex = 0;

                    /* Skip points without valid protocol ID, disabled or with no related G3 point */
                    S104_CHECK_POINT_FIELD(itAnalogConfig.protocolID, itAnalogConfig.enable, itAnalogConfig.vpoint);

                    /* Get time format */
                    lu_uint32_t timeFormat = TMWDEFS_TIME_FORMAT_56; //default
                    getSessionTimeFormat(sessionParserPtr->config.first().eventConfig.first().mmeb, timeFormat);
                    mmebPointConf[pointIndex].pointAttributes.timeformat = (TMWDEFS_TIME_FORMAT)(timeFormat);

                    parseCommonAnalogAttributes(&itAnalogConfig ,pointIndex, mmebPointConf);

                    mmebPoint[pointIndex]= new S104Analog(S104InputPoint::MMENB,mmebPointConf[pointIndex]);
                    sectorPtr->getS104Database().addInputPoint(S104InputPoint::MMENB,*(&mmebPoint[pointIndex]));
                    GDatabase.attach(mmebPointConf[pointIndex].vpointID, *(&mmebPoint[pointIndex]));

                    pointIndex++;
                 }

            break;

            case S104InputPoint::MMENC :

                /*Count number of analog input points*/
                for(Iterator<CIEC870InputPointT> itAnalogConfig = sessionParserPtr->iomap.first().mmec.all();
                        itAnalogConfig; ++itAnalogConfig)
                {
                    /* Skip points without valid protocol ID, disabled or with no related G3 point */
                    S104_CHECK_POINT_FIELD(itAnalogConfig.protocolID, itAnalogConfig.enable, itAnalogConfig.vpoint);
                    mmecNos++;   // Count no of analog input points
                }

                mmecPointConf = new S104InputPoint::Config[mmecNos];
                mmecPoint = new S104Analog*[mmecNos];

                for(Iterator<CIEC870InputPointT> itAnalogConfig = sessionParserPtr->iomap.first().mmec.all();
                        itAnalogConfig; ++itAnalogConfig)
                {
                    lu_uint8_t pointIndex = 0;

                    /* Skip points without valid protocol ID, disabled or with no related G3 point */
                    S104_CHECK_POINT_FIELD(itAnalogConfig.protocolID, itAnalogConfig.enable, itAnalogConfig.vpoint);

                    /* Get time format */
                    lu_uint32_t timeFormat = TMWDEFS_TIME_FORMAT_56; //default
                    getSessionTimeFormat(sessionParserPtr->config.first().eventConfig.first().mmec, timeFormat);
                    mmecPointConf[pointIndex].pointAttributes.timeformat = (TMWDEFS_TIME_FORMAT)(timeFormat);

                    parseCommonAnalogAttributes(&itAnalogConfig ,pointIndex, mmecPointConf);

                    mmecPoint[pointIndex]= new S104Analog(S104InputPoint::MMENC,mmecPointConf[pointIndex]);
                    sectorPtr->getS104Database().addInputPoint(S104InputPoint::MMENC,*(&mmecPoint[pointIndex]));
                    GDatabase.attach(mmecPointConf[pointIndex].vpointID, *(&mmecPoint[pointIndex]));

                    pointIndex++;
                 }
            break;

            default :
                DBG_ERR("Invalid Analog type at %s",__AT__);
        }
    }
    catch (const ParserException &e)
    {
        DBG_ERR("%s",e.what());
        log.error("%s in %s", e.what(), __AT__);
    }
    catch (...)
    {
        log.error("%s Exception raised", __AT__);
    }
}


void XMLSlave104ProtocolParser::parseCommonAnalogAttributes(CIEC870InputPointT* analogPoint,
                                                            lu_uint8_t pIndex,
                                                            S104InputPoint::Config* pointConf
                                                            ) throw(ParserException)
{
    pointConf[pIndex].pointAttributes.ioa = getXMLAttr(analogPoint->protocolID);

    /*applying group masks*/
    pointConf[pIndex].pointAttributes.groupMask = getXMLAttr(analogPoint->groupMask);

    if(getXMLAttr(analogPoint->enableCyclic))
    {
        pointConf[pIndex].pointAttributes.groupMask |= TMWDEFS_GROUP_MASK_CYCLIC;
    }

    if(getXMLAttr(analogPoint->enableBackground))
    {
        pointConf[pIndex].pointAttributes.groupMask |= TMWDEFS_GROUP_MASK_BACKGROUND;
    }

    if(getXMLAttr(analogPoint->enableGeneralInterrogation))
    {
        pointConf[pIndex].pointAttributes.groupMask |= TMWDEFS_GROUP_MASK_GENERAL;
    }

    getXMLOptAttr(analogPoint->spontaneousEvent, pointConf[pIndex].pointAttributes.spontaneousEvent);

    pointConf[pIndex].pointAttributes.enable = getXMLAttr(analogPoint->enable);

    if(pointConf[pIndex].pointAttributes.enable)
    {
        getXMLOptAttrEnum(analogPoint->timeformat, LU_TIME_FORMAT_VALUEfromINDEX, pointConf[pIndex].pointAttributes.timeformat);
        pointConf[pIndex].vpointID = getXMLVirtualPointRef(analogPoint->vpoint.first());
    }

    pointConf[pIndex].pointAttributes.eventOnlyWhenOnline = getXMLAttr(analogPoint->eventOnlyWhenConnected);
    pointConf[pIndex].pointAttributes.eventLogStore = TMWDEFS_TRUE;
    getXMLOptAttr(analogPoint->enableStoreEvent, pointConf[pIndex].pointAttributes.eventLogStore);
}


void XMLSlave104ProtocolParser::parseBinaryConfig(GeminiDatabase           &GDatabase,
                                                  CS104SessionT         *sessionParserPtr,
                                                  S104Sector *sectorPtr)
{
    try
    {
        /* Parse Binary input points */
        if (sessionParserPtr->iomap.first().msp.exists())
        {
            /* count for number of Binary input points*/
            static lu_uint8_t BIPointNum = 0;


            /*Count number of BI points*/
            for(Iterator<CIEC870InputPointT> itBIConfig = sessionParserPtr->iomap.first().msp.all();
                    itBIConfig; ++itBIConfig)
            {
                /* Skip points without valid protocol ID, disabled or with no related G3 point */
                S104_CHECK_POINT_FIELD(itBIConfig.protocolID, itBIConfig.enable, itBIConfig.vpoint);
                BIPointNum++;   // Count no of BI points
            }

            S104InputPoint::Config* pointconf = new S104InputPoint::Config[BIPointNum];

            S104MSP ** singlePoint = new S104MSP*[BIPointNum];

            for(Iterator<CIEC870InputPointT> itBIConfig = sessionParserPtr->iomap.first().msp.all();
                    itBIConfig; ++itBIConfig)
            {
                lu_uint8_t pointIndex = 0;
                /* Skip points without valid protocol ID, disabled or with no related G3 point */
                S104_CHECK_POINT_FIELD(itBIConfig.protocolID, itBIConfig.enable, itBIConfig.vpoint);

                pointconf[pointIndex].pointAttributes.ioa = getXMLAttr(itBIConfig.protocolID);
                /*applying group masks*/
                pointconf[pointIndex].pointAttributes.groupMask = getXMLAttr(itBIConfig.groupMask);

                if(getXMLAttr(itBIConfig.enableCyclic))
                {
                    pointconf[pointIndex].pointAttributes.groupMask |= TMWDEFS_GROUP_MASK_CYCLIC;
                }
                if(getXMLAttr(itBIConfig.enableBackground))
                {
                    pointconf[pointIndex].pointAttributes.groupMask |= TMWDEFS_GROUP_MASK_BACKGROUND;
                }
                if(getXMLAttr(itBIConfig.enableGeneralInterrogation))
                {
                    pointconf[pointIndex].pointAttributes.groupMask |= TMWDEFS_GROUP_MASK_GENERAL;
                }

                getXMLOptAttr(itBIConfig.spontaneousEvent, pointconf[pointIndex].pointAttributes.spontaneousEvent);
                pointconf[pointIndex].pointAttributes.eventOnlyWhenOnline = getXMLAttr(itBIConfig.eventOnlyWhenConnected);
                pointconf[pointIndex].pointAttributes.eventLogStore = TMWDEFS_TRUE;
                getXMLOptAttr(itBIConfig.enableStoreEvent, pointconf[pointIndex].pointAttributes.eventLogStore);

                /* Get time format */
                /* Note: we need to assign to a int instead to the enum since XMLSpy takes wrong numbers as enum */
                lu_uint32_t timeFormat = TMWDEFS_TIME_FORMAT_56; //default
                getSessionTimeFormat(sessionParserPtr->config.first().eventConfig.first().msp, timeFormat);
                getXMLOptAttrEnum(itBIConfig.timeformat, LU_TIME_FORMAT_VALUEfromINDEX, timeFormat);
                pointconf[pointIndex].pointAttributes.timeformat = (TMWDEFS_TIME_FORMAT)(timeFormat);

                pointconf[pointIndex].vpointID = getXMLVirtualPointRef(itBIConfig.vpoint.first());

                pointconf[pointIndex].pointAttributes.enable = getXMLAttr(itBIConfig.enable);

                if(pointconf[pointIndex].pointAttributes.enable)
                {
                    singlePoint[pointIndex]= new S104MSP(pointconf[pointIndex]);
                    sectorPtr->getS104Database().addInputPoint(S104InputPoint::MSP,*(&singlePoint[pointIndex]));
                    GDatabase.attach(pointconf[pointIndex].vpointID, *(&singlePoint[pointIndex]));
                }
                pointIndex++;
            }
        }
        else
        {
            log.info("%s No binary points configured", __AT__);
        }
    }
    catch (ParserException &e)
    {
        DBG_ERR("%s",e.what());
        log.error("%s in %s",e.what(), __AT__);
    }
    catch (...)
    {
        log.error("%s Exception raised", __AT__);
    }

}


void XMLSlave104ProtocolParser::parseDBinaryConfig(GeminiDatabase           &GDatabase,
                                                  CS104SessionT         *sessionParserPtr,
                                                  S104Sector *sectorPtr)
{
    try
    {
        /* Parse double binary input points */
        if (sessionParserPtr->iomap.first().mdp.exists())
        {
            /* count for number of double binary input points*/
            static lu_uint8_t DBIPointNum = 0;


            /*Count number of BI points*/
            for(Iterator<CIEC870InputPointT> itDBIConfig = sessionParserPtr->iomap.first().mdp.all();
                    itDBIConfig; ++itDBIConfig)
            {
                /* Skip points without valid protocol ID, disabled or with no related G3 point */
                S104_CHECK_POINT_FIELD(itDBIConfig.protocolID, itDBIConfig.enable, itDBIConfig.vpoint);
                DBIPointNum++;   // Count no of DBI points
            }

            S104InputPoint::Config *pointconf = new S104InputPoint::Config[DBIPointNum];

            S104MDP ** doublePoint = new S104MDP*[DBIPointNum];

            for(Iterator<CIEC870InputPointT> itDBIConfig = sessionParserPtr->iomap.first().mdp.all();
                    itDBIConfig; ++itDBIConfig)
            {
                lu_uint8_t pointIndex = 0;
                /* Skip points without valid protocol ID, disabled or with no related G3 point */
                S104_CHECK_POINT_FIELD(itDBIConfig.protocolID, itDBIConfig.enable, itDBIConfig.vpoint);

                pointconf[pointIndex].pointAttributes.ioa = getXMLAttr(itDBIConfig.protocolID);

                /*applying group masks*/
                pointconf[pointIndex].pointAttributes.groupMask = getXMLAttr(itDBIConfig.groupMask);

                if(getXMLAttr (itDBIConfig.enableCyclic))
                {
                    pointconf[pointIndex].pointAttributes.groupMask |= TMWDEFS_GROUP_MASK_CYCLIC;
                }
                if(getXMLAttr (itDBIConfig.enableBackground))
                {
                    pointconf[pointIndex].pointAttributes.groupMask |= TMWDEFS_GROUP_MASK_BACKGROUND;
                }
                if(getXMLAttr (itDBIConfig.enableGeneralInterrogation))
                {
                    pointconf[pointIndex].pointAttributes.groupMask |= TMWDEFS_GROUP_MASK_GENERAL;
                }

                getXMLOptAttr(itDBIConfig.spontaneousEvent, pointconf[pointIndex].pointAttributes.spontaneousEvent);
                pointconf[pointIndex].pointAttributes.eventOnlyWhenOnline = getXMLAttr(itDBIConfig.eventOnlyWhenConnected);
                pointconf[pointIndex].pointAttributes.eventLogStore = TMWDEFS_TRUE;
                getXMLOptAttr(itDBIConfig.enableStoreEvent, pointconf[pointIndex].pointAttributes.eventLogStore);

                /* Get time format */
                lu_uint32_t timeFormat = TMWDEFS_TIME_FORMAT_56; //default
                getSessionTimeFormat(sessionParserPtr->config.first().eventConfig.first().mdp, timeFormat);
                getXMLOptAttrEnum(itDBIConfig.timeformat, LU_TIME_FORMAT_VALUEfromINDEX, timeFormat);
                pointconf[pointIndex].pointAttributes.timeformat = (TMWDEFS_TIME_FORMAT)(timeFormat);

                pointconf[pointIndex].vpointID = getXMLVirtualPointRef(itDBIConfig.vpoint.first());

                pointconf[pointIndex].pointAttributes.enable = getXMLAttr(itDBIConfig.enable);

                if(pointconf[pointIndex].pointAttributes.enable)
                {
                    doublePoint[pointIndex]= new S104MDP(pointconf[pointIndex]);
                    sectorPtr->getS104Database().addInputPoint(S104InputPoint::MDP,*(&doublePoint[pointIndex]));
                    GDatabase.attach(pointconf[pointIndex].vpointID, *(&doublePoint[pointIndex]));
                }
                pointIndex++;
            }
        }
        else
        {
            log.info("%s No double binary points configured", __AT__);
        }
    }
    catch (ParserException &e)
    {
        DBG_ERR("%s",e.what());
        log.error("%s in %s",e.what(), __AT__);
    }
    catch (...)
    {
        log.error("%s Exception raised", __AT__);
    }
}


void XMLSlave104ProtocolParser::parseBinaryOutputConfig(CS104SessionT           *sessionParserPtr,
                                                        S104Sector *sectorPtr)
{
    try
    {
        lu_uint8_t timeTagMode;
        getXMLOptAttr(sessionParserPtr->config.first().cmdTimetagMode, timeTagMode);

        /* Parse binary output  points */
        if (sessionParserPtr->iomap.first().csc.exists())
        {
            /* count for number of binary output  points*/
            static lu_uint8_t BOPointNum = 0;

            /*Count number of BO points*/
            for(Iterator<CIEC870OutputPointT> itBOConfig = sessionParserPtr->iomap.first().csc.all();
                    itBOConfig; ++itBOConfig)
            {
                /* Skip points without valid protocol ID, disabled or with no related G3 point */
                S104_CHECK_POINT_FIELD(itBOConfig.protocolID, itBOConfig.enable, itBOConfig.controlLogicGroup);
                BOPointNum++;   // Count no of Binary output points
            }

            I870OutputPointConf *pointconf = new I870OutputPointConf[BOPointNum];

            S104OutputPoint ** bopoint = new S104OutputPoint*[BOPointNum];

            for(Iterator<CIEC870OutputPointT> itBOConfig = sessionParserPtr->iomap.first().csc.all();
                    itBOConfig; ++itBOConfig)
            {
                lu_uint8_t pointIndex = 0;
                /* Skip points without valid protocol ID, disabled or with no related G3 point */
                S104_CHECK_POINT_FIELD(itBOConfig.protocolID, itBOConfig.enable, itBOConfig.controlLogicGroup);

                pointconf[pointIndex].ioa = getXMLAttr(itBOConfig.protocolID);
                pointconf[pointIndex].groupID =  getXMLAttr(itBOConfig.controlLogicGroup);//!!!!group id!
                pointconf[pointIndex].selectRequired = getXMLAttr(itBOConfig.selectRequired);
                pointconf[pointIndex].enable = getXMLAttr(itBOConfig.enable);
                pointconf[pointIndex].timeTag = (TIME_TAG)timeTagMode;


                if(pointconf[pointIndex].enable)
                {
                    bopoint[pointIndex]= new S104OutputPoint(pointconf[pointIndex]);
                    sectorPtr->getS104Database().addOutputPoint(S104OutputPoint::SCO,*(&bopoint[pointIndex]));
                }
                pointIndex++;
            }
        }
        else
        {
            log.info("%s No double binary output points configured", __AT__);
        }
    }
    catch (const ParserException &e)
    {
        DBG_ERR("%s",e.what());
        log.error("%s in %s",e.what(), __AT__);
    }
    catch (...)
    {
        log.error("%s Exception raised", __AT__);
    }

}


void XMLSlave104ProtocolParser::parseDBinaryOutputConfig(CS104SessionT           *sessionParserPtr,
                                                        S104Sector *sectorPtr)
{
    try
    {
        lu_uint8_t timeTagMode;
        getXMLOptAttr(sessionParserPtr->config.first().cmdTimetagMode, timeTagMode);

        /* Parse double binary input points */
        if (sessionParserPtr->iomap.first().cdc.exists())
        {
            /* count for number of double binary input points*/
            static lu_uint8_t DBOPointNum = 0;


            /*Count number of BI points*/
            for(Iterator<CIEC870OutputPointT> itDBOConfig = sessionParserPtr->iomap.first().cdc.all();
                    itDBOConfig; ++itDBOConfig)
            {
                /* Skip points without valid protocol ID, disabled or with no related G3 point */
                S104_CHECK_POINT_FIELD(itDBOConfig.protocolID, itDBOConfig.enable, itDBOConfig.controlLogicGroup);
                DBOPointNum++;   // Count no of Binary output points
            }

            I870OutputPointConf *pointconf = new I870OutputPointConf[DBOPointNum];

            S104OutputPoint ** dbopoint = new S104OutputPoint*[DBOPointNum];

            for(Iterator<CIEC870OutputPointT> itDBOConfig = sessionParserPtr->iomap.first().cdc.all();
                    itDBOConfig; ++itDBOConfig)
            {
                lu_uint8_t pointIndex = 0;
                /* Skip points without valid protocol ID, disabled or with no related G3 point */
                S104_CHECK_POINT_FIELD(itDBOConfig.protocolID, itDBOConfig.enable, itDBOConfig.controlLogicGroup);

                pointconf[pointIndex].ioa = getXMLAttr(itDBOConfig.protocolID);
                pointconf[pointIndex].groupID =  getXMLAttr(itDBOConfig.controlLogicGroup);
                pointconf[pointIndex].selectRequired = getXMLAttr(itDBOConfig.selectRequired);
                pointconf[pointIndex].enable = getXMLAttr(itDBOConfig.enable);
                pointconf[pointIndex].timeTag = (TIME_TAG)timeTagMode;

                if(pointconf[pointIndex].enable)
                {
                    dbopoint[pointIndex]= new S104OutputPoint(pointconf[pointIndex]);
                    sectorPtr->getS104Database().addOutputPoint(S104OutputPoint::DCO,*(&dbopoint[pointIndex]));
                }
                pointIndex++;
            }
        }
        else
        {
            log.info("%s No double binary output points configured", __AT__);
        }
    }
    catch (const ParserException &e)
    {
        DBG_ERR("%s",e.what());
        log.error("%s in %s",e.what(), __AT__);
    }
    catch (...)
    {
        log.error("%s Exception raised", __AT__);
    }
}


void XMLSlave104ProtocolParser::parseIntegratedTotsConfig(GeminiDatabase           &GDatabase,
                                                        CS104SessionT           *sessionParserPtr,
                                                        S104Sector *sectorPtr)
{
    try
    {
        /* Parse double binary input points */
        if (sessionParserPtr->iomap.first().mit.exists())
        {
            /* count for number of double binary input points*/
            static lu_uint8_t mitPointNum = 0;


            /*Count number of BI points*/
            for(Iterator<CIEC870CounterPointT> itMITConfig = sessionParserPtr->iomap.first().mit.all();
                    itMITConfig; ++itMITConfig)
            {
                /* Skip points without valid protocol ID, disabled or with no related G3 point */
                S104_CHECK_POINT_FIELD(itMITConfig.protocolID, itMITConfig.enable, itMITConfig.vpoint);
                mitPointNum++;   // Count no of DBI points
            }

            S104InputPoint::Config *pointconf = new S104InputPoint::Config[mitPointNum];

            S104MIT** mitPoint = new S104MIT*[mitPointNum];

            for(Iterator<CIEC870CounterPointT> itMITConfig = sessionParserPtr->iomap.first().mit.all();
                    itMITConfig; ++itMITConfig)
            {
                lu_uint8_t pointIndex = 0;
                /* Skip points without valid protocol ID, disabled or with no related G3 point */
                S104_CHECK_POINT_FIELD(itMITConfig.protocolID, itMITConfig.enable, itMITConfig.vpoint);

                pointconf[pointIndex].pointAttributes.ioa = getXMLAttr(itMITConfig.protocolID);

                /*applying group masks*/
                pointconf[pointIndex].pointAttributes.groupMask = getXMLAttr(itMITConfig.groupMask);

                if(itMITConfig.enableCyclic)
                {
                    pointconf[pointIndex].pointAttributes.groupMask |= TMWDEFS_GROUP_MASK_CYCLIC;
                }
                if(itMITConfig.enableBackground)
                {
                    pointconf[pointIndex].pointAttributes.groupMask |= TMWDEFS_GROUP_MASK_BACKGROUND;
                }
                if(itMITConfig.enableGeneralInterrogation)
                {
                    pointconf[pointIndex].pointAttributes.groupMask |= TMWDEFS_GROUP_MASK_GENERAL;
                }
                getXMLOptAttr(itMITConfig.spontaneousEvent, pointconf[pointIndex].pointAttributes.spontaneousEvent);
                pointconf[pointIndex].pointAttributes.eventOnlyWhenOnline = getXMLAttr(itMITConfig.eventOnlyWhenConnected);
                pointconf[pointIndex].pointAttributes.eventLogStore = TMWDEFS_TRUE;
                getXMLOptAttr(itMITConfig.enableStoreEvent, pointconf[pointIndex].pointAttributes.eventLogStore);

                /* Get time format */
                lu_uint32_t timeFormat = TMWDEFS_TIME_FORMAT_56; //default
                getSessionTimeFormat(sessionParserPtr->config.first().eventConfig.first().mit, timeFormat);
                getXMLOptAttrEnum(itMITConfig.timeformat, LU_TIME_FORMAT_VALUEfromINDEX, timeFormat);
                pointconf[pointIndex].pointAttributes.timeformat = (TMWDEFS_TIME_FORMAT)(timeFormat);

                pointconf[pointIndex].vpointID = getXMLVirtualPointRef(itMITConfig.vpoint.first());

                pointconf[pointIndex].pointAttributes.enable = getXMLAttr(itMITConfig.enable);

                if(pointconf[pointIndex].pointAttributes.enable)
                {
                    mitPoint[pointIndex] = new S104MIT(pointconf[pointIndex]);
                    /* TODO: pueyos_a - [s104] set counter value to use (current/frozen) from configuration */
                    mitPoint[pointIndex]->setType(S104MIT::COUNTER_TYPE_FROZEN);    //Default to frozen for now

                    sectorPtr->getS104Database().addInputPoint(S104InputPoint::MIT, *(&mitPoint[pointIndex]));
                    GDatabase.attach(pointconf[pointIndex].vpointID, *(&mitPoint[pointIndex]));
                }
                pointIndex++;
            }
        }
        else
        {
            log.info("%s No integrated totals configured", __AT__);
        }
    }
    catch (const ParserException &e)
    {
        DBG_ERR("%s",e.what());
        log.error("%s in %s",e.what(), __AT__);
    }
    catch (...)
    {
        log.error("%s Exception raised", __AT__);
    }
}


void XMLSlave104ProtocolParser::parseChannelConfig(CS104ChannelT&  itChannelConfig,
                                                   S104Channel::S104ChannelConfig& channelConfig) throw(ParserException)
{
    // Initialise the TMW Structures
    liniotarg_initConfig(&(channelConfig.pIOConfig));

    tmwtarg_initConfig(&(channelConfig.pTMWTargConfig));

    i104chnl_initConfig(&(channelConfig.pChnlConfig),
                       &(channelConfig.pLinkConfig),
                       &(channelConfig.pPhysConfig));

    channelConfig.pChnlConfig.incrementalTimeout = getXMLAttr(itChannelConfig.config.first().incrementalTimeoutMs);
    //channelConfig.pChnlConfig.maxQueueSize = getXMLAttr(itChannelConfig.config.first().maxQueueSize);

    /* Parse the LINK LAYER parameters */
    CS104LinkLayerConfT s104LinkConfigStr = getXMLElement<CS104LinkLayerConfT>(itChannelConfig.config.first().linkLayer);

    channelConfig.pLinkConfig.rxFrameSize = getXMLAttr(s104LinkConfigStr.rxFrameSize);
    channelConfig.pLinkConfig.txFrameSize = getXMLAttr(s104LinkConfigStr.txFrameSize);
    channelConfig.pLinkConfig.offlinePollPeriod = getXMLAttr(s104LinkConfigStr.offlinePollPeriodMs);    //t0
    channelConfig.pLinkConfig.t1AckPeriod = getXMLAttr(s104LinkConfigStr.t1AckPeriodMs);
    channelConfig.pLinkConfig.t2SFramePeriod = getXMLAttr(s104LinkConfigStr.t2SFramePeriodMs);
    channelConfig.pLinkConfig.t3TestPeriod = getXMLAttr(s104LinkConfigStr.t3TestPeriodMs);
    channelConfig.pLinkConfig.kValue   = getXMLAttr(s104LinkConfigStr.kValue);
    channelConfig.pLinkConfig.wValue   = getXMLAttr(s104LinkConfigStr.wValue);

    channelConfig.pLinkConfig.isControlling = TMWDEFS_FALSE; //by default because RTU is a slave

    /* Read TCP/IP configuration */
    checkXMLElement(itChannelConfig.config.first().linuxIO);
    CS104ChnlTCPConfT TCPConfigStr = getXMLElement<CS104ChnlTCPConfT>(itChannelConfig.config.first().linuxIO.first().tcp);

    std::string tmp_chnlName = getXMLAttr(itChannelConfig.config.first().chnlName);
    stringToCString(tmp_chnlName, channelConfig.pIOConfig.linTCP.chnlName, LINIOCNFG_MAX_NAME_LEN);

    std::string tmp_ipaddress = getXMLAttr(TCPConfigStr.ipAddress);
    stringToCString(tmp_ipaddress, channelConfig.pIOConfig.linTCP.ipAddress, 32);


    // Use TCP Config
    channelConfig.pIOConfig.linTCP.ipPort    = getXMLAttr(TCPConfigStr.ipPort);
    channelConfig.pIOConfig.linTCP.ipConnectTimeout = getXMLAttr(TCPConfigStr.ipConnectTimeout);
    channelConfig.pIOConfig.linTCP.mode   = getXMLAttrEnum<LINTCP_MODE>(TCPConfigStr.mode, LU_LINTCP_MODE_VALUEfromINDEX);
    channelConfig.pIOConfig.linTCP.validateUDPAddress = TMWDEFS_FALSE;
    channelConfig.pIOConfig.linTCP.disconnectOnNewSyn = TMWDEFS_FALSE;   //Default
    getXMLOptAttr(TCPConfigStr.disconnectOnNewSyn, channelConfig.pIOConfig.linTCP.disconnectOnNewSyn);   //Optional attribute

    channelConfig.pIOConfig.type = LINIO_TYPE_TCP;
    // Do a quick sanity check on the TCP configuration
    if ((channelConfig.pIOConfig.linTCP.mode != LINTCP_MODE_SERVER) &&
        (strcmp(channelConfig.pIOConfig.linTCP.ipAddress, "*.*.*.*") == 0))
    {
        log.error("104 Channel [%s] Cannot be a CLIENT or DUAL ENDPOINT with ipAddress set to [%s]!\n",
                        channelConfig.pIOConfig.linTCP.chnlName,
                        channelConfig.pIOConfig.linTCP.ipAddress);
    }

    // Parse redundancies
    if(itChannelConfig.redundancy.exists() &&
       itChannelConfig.redundancy.first().enabled == true)
    {
        /* Disable "Discard Information Frames on Disconnect" when REDUNDANCY is enabled */
        channelConfig.pLinkConfig.discardIFramesOnDisconnect = TMWDEFS_FALSE;
        channelConfig.rdcySize = itChannelConfig.redundancy.first().channel.count();
        channelConfig.rdcyConfig = new S104Channel::RedundancyConfig*[channelConfig.rdcySize];

        lu_uint16_t i;
        for (i = 0; i < channelConfig.rdcySize; i ++)
        {
            channelConfig.rdcyConfig[i] = new S104Channel::RedundancyConfig();
            std::string tmp_ipaddress = getXMLAttr(itChannelConfig.redundancy.first().channel[i].ipAddress);
            stringToCString(tmp_ipaddress, channelConfig.rdcyConfig[i]->ipAddress, 32);
            channelConfig.rdcyConfig[i]->ipPort = getXMLAttr(itChannelConfig.redundancy.first().channel[i].ipPort);
        }
    }
    else
    {
        channelConfig.rdcySize = 0;
        channelConfig.rdcyConfig = NULL;
        /* Get "Discard Information Frames on Disconnect" from user configuration */
        channelConfig.pLinkConfig.discardIFramesOnDisconnect = getXMLAttr(s104LinkConfigStr.discardIFramesOnDisconnect);
    }
}


lu_uint32_t XMLSlave104ProtocolParser::parseSessionConfig(CS104SessionT&  sessionConfig,
                                                          S104SESN_CONFIG& sessionConfigStr) throw(ParserException)

{
    s104sesn_initConfig(&(sessionConfigStr));    // Initialise the TMW Structures
    CS104SesnConfT session = getXMLElement<CS104SesnConfT>(sessionConfig.config);
    sessionConfigStr.infoObjAddrSize = getXMLAttr(session.infoObjAddrSize);
#define BLU   "\x1B[34m"
#define RESET "\x1B[0m"
    printf(BLU"\r\nsessionConfigStr.infoObjAddrSize =%d\n",sessionConfigStr.infoObjAddrSize,RESET);
    lu_uint32_t maxAPDUSize;    //Note that ASDU length has to be calculated by the caller
    maxAPDUSize = getXMLAttr(sessionConfig.config.first().maxAPDUSize);
    sessionConfigStr.useDayOfWeek = getXMLAttr(sessionConfig.config.first().useDayOfWeek);

    return maxAPDUSize;
}


void XMLSlave104ProtocolParser::parseSectorConfig(CS104SessionT&  sessionConfig,
                                                  S104SCTR_CONFIG& sectorConfigStr) throw(ParserException)
{
    // Initialise the TMW Structures
    s104sctr_initConfig(&(sectorConfigStr));

    CS104SesnConfT session = getXMLElement<CS104SesnConfT>(sessionConfig.config);
    sectorConfigStr.asduAddress = getXMLAttr(session.asduAddress);
    sectorConfigStr.cyclicPeriod = getXMLAttr(session.cyclicPeriodMs);
    sectorConfigStr.cyclicFirstPeriod = getXMLAttr(session.cyclicFirstPeriodMs);
    sectorConfigStr.backgroundPeriod = getXMLAttr(session.backgroundPeriodMs);
    //sectorConfigStr.rbeScanPeriod  = getXMLAttr(session.rbeScanPeriodMs);
    sectorConfigStr.selectTimeout = getXMLAttr(session.selectTimeoutMs);
    sectorConfigStr.defaultResponseTimeout = getXMLAttr(session.defResponseTimeoutMs);
    sectorConfigStr.cseUseActTerm = getXMLAttr(session.cseUseActTerm);
    sectorConfigStr.cmdUseActTerm = getXMLAttr(session.cmdUseActTerm);
    sectorConfigStr.sendClockSyncEvents = getXMLAttr(session.sendClockSyncEvents);
    sectorConfigStr.deleteOldestEvent = getXMLAttr(session.deleteOldestEvent);

    sectorConfigStr.maxCommandAge = getXMLAttr(session.maxCommandAge);
    sectorConfigStr.maxCommandFuture = getXMLAttr(session.maxCommandFuture);

    bool multiplePMEN = getXMLAttr(session.multiplePMEN); //Get for composing bit field
    if(!multiplePMEN)
    {
        sectorConfigStr.strictAdherence |= S14SCTR_STRICT_PMENABC;
    }

    /*we set the below members as TMWDEFS_TIME_FORMAT_UNKNOWN by default if these
     * are not present in order to use the per-point time formats for:
     *      - READ commands (analogue points and the rest)
     *      - General Interrogation commands
     */
    sectorConfigStr.readTimeFormat = TMWDEFS_TIME_FORMAT_UNKNOWN;
    sectorConfigStr.readMsrndTimeFormat = TMWDEFS_TIME_FORMAT_UNKNOWN;
    sectorConfigStr.cicnaTimeFormat = TMWDEFS_TIME_FORMAT_UNKNOWN;

    getXMLOptAttrEnum(session.readTimeFormat,  LU_TIME_FORMAT_VALUEfromINDEX, sectorConfigStr.readTimeFormat);
    getXMLOptAttrEnum(session.readMsrndTimeFormat,  LU_TIME_FORMAT_VALUEfromINDEX, sectorConfigStr.readMsrndTimeFormat );
    getXMLOptAttrEnum(session.cicnaTimeFormat, LU_TIME_FORMAT_VALUEfromINDEX, sectorConfigStr.cicnaTimeFormat);

    /* === Eventing configuration === */
    checkXMLElement(sessionConfig.config);
    CIEC870EventConfigT eventCfg = getXMLElement<CIEC870EventConfigT>(session.eventConfig);

    /*binary input Events*/
    CIEC870SesnEventT mspEvents = getXMLElement<CIEC870SesnEventT>(eventCfg.msp);
    sectorConfigStr.mspMaxEvents = getXMLAttr(mspEvents.maxEvents);
    sectorConfigStr.mspEventMode = getXMLAttrEnum<TMWDEFS_EVENT_MODE>(mspEvents.eventMode, LU_EVENT_MODE_VALUEfromINDEX);
    getXMLOptAttrEnum(mspEvents.timeFormat, LU_TIME_FORMAT_VALUEfromINDEX, sectorConfigStr.mspTimeFormat );

    /*double binary input Events*/
    CIEC870SesnEventT mdpEvents = getXMLElement<CIEC870SesnEventT>(eventCfg.mdp);
    sectorConfigStr.mdpMaxEvents = getXMLAttr(mdpEvents.maxEvents);
    sectorConfigStr.mdpEventMode = getXMLAttrEnum<TMWDEFS_EVENT_MODE>(mdpEvents.eventMode, LU_EVENT_MODE_VALUEfromINDEX);
    getXMLOptAttrEnum(mdpEvents.timeFormat, LU_TIME_FORMAT_VALUEfromINDEX, sectorConfigStr.mdpTimeFormat );

    /*Integrated Totals (counter) input Events*/
    CIEC870SesnEventT mitEvents = getXMLElement<CIEC870SesnEventT>(eventCfg.mit);
    sectorConfigStr.mitMaxEvents = getXMLAttr(mitEvents.maxEvents);
    sectorConfigStr.mitEventMode = getXMLAttrEnum<TMWDEFS_EVENT_MODE>(mitEvents.eventMode, LU_EVENT_MODE_VALUEfromINDEX);
    getXMLOptAttrEnum(mitEvents.timeFormat, LU_TIME_FORMAT_VALUEfromINDEX, sectorConfigStr.mitTimeFormat );

    /*Floating point Analog Events*/
    CIEC870SesnEventT mmencEvents = getXMLElement<CIEC870SesnEventT>(eventCfg.mmec);
    sectorConfigStr.mmencMaxEvents = getXMLAttr(mmencEvents.maxEvents);
    sectorConfigStr.mmencEventMode = getXMLAttrEnum<TMWDEFS_EVENT_MODE>(mmencEvents.eventMode, LU_EVENT_MODE_VALUEfromINDEX);
    getXMLOptAttrEnum(mmencEvents.timeFormat, LU_TIME_FORMAT_VALUEfromINDEX, sectorConfigStr.mmencTimeFormat);

    /*Scaled Analog Events*/
    CIEC870SesnEventT mmenbEvents = getXMLElement<CIEC870SesnEventT>(eventCfg.mmeb);
    sectorConfigStr.mmenbMaxEvents = getXMLAttr(mmenbEvents.maxEvents);
    sectorConfigStr.mmenbEventMode = getXMLAttrEnum<TMWDEFS_EVENT_MODE>(mmenbEvents.eventMode, LU_EVENT_MODE_VALUEfromINDEX);
    getXMLOptAttrEnum(mmenbEvents.timeFormat, LU_TIME_FORMAT_VALUEfromINDEX, sectorConfigStr.mmenbTimeFormat);

    /*Normalized Analog Events*/
    CIEC870SesnEventT mmenaEvents = getXMLElement<CIEC870SesnEventT>(eventCfg.mmea);
    sectorConfigStr.mmenaMaxEvents = getXMLAttr(mmenaEvents.maxEvents);
    sectorConfigStr.mmenaEventMode = getXMLAttrEnum<TMWDEFS_EVENT_MODE>(mmenaEvents.eventMode, LU_EVENT_MODE_VALUEfromINDEX);
    getXMLOptAttrEnum(mmenaEvents.timeFormat, LU_TIME_FORMAT_VALUEfromINDEX, sectorConfigStr.mmenaTimeFormat);

    DBG_DUMP_SECTOR104();
}


/*
 *********************** End of file ******************************************
 */
