#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_COffLocalButtonSettingT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_COffLocalButtonSettingT



namespace g3schema
{

class COffLocalButtonSettingT : public TypeBase
{
public:
	g3schema_EXPORT COffLocalButtonSettingT(xercesc::DOMNode* const& init);
	g3schema_EXPORT COffLocalButtonSettingT(COffLocalButtonSettingT const& init);
	void operator=(COffLocalButtonSettingT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_altova_COffLocalButtonSettingT); }

	MemberAttribute<unsigned,_altova_mi_altova_COffLocalButtonSettingT_altova_localRemoteToggleTime, 0, 0> localRemoteToggleTime;	// localRemoteToggleTime CunsignedShort

	MemberAttribute<unsigned,_altova_mi_altova_COffLocalButtonSettingT_altova_offToggleTime, 0, 0> offToggleTime;	// offToggleTime CunsignedShort
	g3schema_EXPORT void SetXsiType();
};


}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA__ALTOVA_COffLocalButtonSettingT
