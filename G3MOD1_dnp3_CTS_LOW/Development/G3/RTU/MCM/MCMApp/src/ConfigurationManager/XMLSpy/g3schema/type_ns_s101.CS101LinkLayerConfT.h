#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s101_ALTOVA_CS101LinkLayerConfT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s101_ALTOVA_CS101LinkLayerConfT

#include "type_ns_iec870.CIEC870LinkLayerConfT.h"


namespace g3schema
{

namespace ns_s101
{	

class CS101LinkLayerConfT : public ::g3schema::ns_iec870::CIEC870LinkLayerConfT
{
public:
	g3schema_EXPORT CS101LinkLayerConfT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CS101LinkLayerConfT(CS101LinkLayerConfT const& init);
	void operator=(CS101LinkLayerConfT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_s101_altova_CS101LinkLayerConfT); }

	MemberAttribute<unsigned,_altova_mi_ns_s101_altova_CS101LinkLayerConfT_altova_linkAddressSize, 0, 0> linkAddressSize;	// linkAddressSize CunsignedByte

	MemberAttribute<unsigned,_altova_mi_ns_s101_altova_CS101LinkLayerConfT_altova_linkMode, 0, 0> linkMode;	// linkMode CunsignedByte

	MemberAttribute<bool,_altova_mi_ns_s101_altova_CS101LinkLayerConfT_altova_oneCharResponseAllowed, 0, 0> oneCharResponseAllowed;	// oneCharResponseAllowed Cboolean

	MemberAttribute<bool,_altova_mi_ns_s101_altova_CS101LinkLayerConfT_altova_oneCharAckAllowed, 0, 0> oneCharAckAllowed;	// oneCharAckAllowed Cboolean

	MemberAttribute<__int64,_altova_mi_ns_s101_altova_CS101LinkLayerConfT_altova_rxFrameTimeout, 0, 0> rxFrameTimeout;	// rxFrameTimeout Clong

	MemberAttribute<unsigned,_altova_mi_ns_s101_altova_CS101LinkLayerConfT_altova_confirmMode, 0, 0> confirmMode;	// confirmMode CunsignedByte

	MemberAttribute<__int64,_altova_mi_ns_s101_altova_CS101LinkLayerConfT_altova_confirmTimeout, 0, 0> confirmTimeout;	// confirmTimeout Clong

	MemberAttribute<unsigned,_altova_mi_ns_s101_altova_CS101LinkLayerConfT_altova_maxRetries, 0, 0> maxRetries;	// maxRetries CunsignedByte

	MemberAttribute<__int64,_altova_mi_ns_s101_altova_CS101LinkLayerConfT_altova_testFramePeriod, 0, 0> testFramePeriod;	// testFramePeriod Clong

	MemberAttribute<__int64,_altova_mi_ns_s101_altova_CS101LinkLayerConfT_altova_offlinePollPeriod, 0, 0> offlinePollPeriod;	// offlinePollPeriod Clong
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_s101

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s101_ALTOVA_CS101LinkLayerConfT
