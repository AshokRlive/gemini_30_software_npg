#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_comms_ALTOVA_CPSTNModemT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_comms_ALTOVA_CPSTNModemT

#include "type_ns_comms.CBaseModemT.h"


namespace g3schema
{

namespace ns_comms
{	

class CPSTNModemT : public ::g3schema::ns_comms::CBaseModemT
{
public:
	g3schema_EXPORT CPSTNModemT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CPSTNModemT(CPSTNModemT const& init);
	void operator=(CPSTNModemT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_comms_altova_CPSTNModemT); }

	MemberAttribute<string_type,_altova_mi_ns_comms_altova_CPSTNModemT_altova_initStr, 0, 0> initStr;	// initStr Cstring

	MemberAttribute<string_type,_altova_mi_ns_comms_altova_CPSTNModemT_altova_hangupString, 0, 0> hangupString;	// hangupString Cstring

	MemberAttribute<string_type,_altova_mi_ns_comms_altova_CPSTNModemT_altova_connectString, 0, 0> connectString;	// connectString Cstring

	MemberAttribute<string_type,_altova_mi_ns_comms_altova_CPSTNModemT_altova_promptString, 0, 0> promptString;	// promptString CanySimpleType

	MemberAttribute<string_type,_altova_mi_ns_comms_altova_CPSTNModemT_altova_dialString, 0, 0> dialString;	// dialString Cstring

	MemberAttribute<string_type,_altova_mi_ns_comms_altova_CPSTNModemT_altova_dialString2, 0, 0> dialString2;	// dialString2 Cstring
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_comms

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_comms_ALTOVA_CPSTNModemT
