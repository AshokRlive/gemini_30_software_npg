#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_CVirtualPointRefT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_CVirtualPointRefT



namespace g3schema
{

namespace ns_common
{	

class CVirtualPointRefT : public TypeBase
{
public:
	g3schema_EXPORT CVirtualPointRefT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CVirtualPointRefT(CVirtualPointRefT const& init);
	void operator=(CVirtualPointRefT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_common_altova_CVirtualPointRefT); }

	MemberAttribute<unsigned,_altova_mi_ns_common_altova_CVirtualPointRefT_altova_pointGroup, 0, 0> pointGroup;	// pointGroup CVirtualPointGroupT

	MemberAttribute<unsigned,_altova_mi_ns_common_altova_CVirtualPointRefT_altova_pointID, 0, 0> pointID;	// pointID CVirtualPointIdT
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_common

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_CVirtualPointRefT
