/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:XMLSCADAProtocolParser.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   1 Jul 2014     andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef XMLSCADAPROTOCOLPARSER_H_
#define XMLSCADAPROTOCOLPARSER_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "IMCMApplication.h"
#include "ConfigurationManagerError.h"
#include "SCADAProtocolManager.h"
#include "PortManager.h"
#include "CommsDeviceManager.h"
#include "Logger.h"

// Supported Slave Protocol Parsers
#include "XMLDNP3SlaveProtocolParser.h"
#include "XML104SlaveProtocolParser.h"
#include "XML101SlaveProtocolParser.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */


class XMLSCADAProtocolParser
{
public:
    XMLSCADAProtocolParser(g3schema::Cg3schema&     configuration,
                           SCADAProtocolManager&    SCADAManager,
                           CommsDeviceManager&      commsDeviceManager,
                           PortManager&             portManager,
                           ModuleManager&           moduleManager,
                           GeminiDatabase&          database,
                           IMCMApplication&         mcmApplication);

    virtual ~XMLSCADAProtocolParser();

    CONFMGR_ERROR initParser();

private:
    g3schema::Cg3schema&    configuration;
    SCADAProtocolManager&   SCADAManager;
    PortManager&            portManager;
    ModuleManager&          moduleManager;
    CommsDeviceManager&     commsDeviceManager;
    GeminiDatabase&         database;
    IMCMApplication&        mcmApplication;

    Logger&                 log;
};




#endif /* XMLSCADAPROTOCOLPARSER_H_ */

/*
 *********************** End of file ******************************************
 */
