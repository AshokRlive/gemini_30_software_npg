#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CAuthUserT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CAuthUserT



namespace g3schema
{

namespace ns_sdnp3
{	

class CAuthUserT : public TypeBase
{
public:
	g3schema_EXPORT CAuthUserT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CAuthUserT(CAuthUserT const& init);
	void operator=(CAuthUserT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_sdnp3_altova_CAuthUserT); }

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CAuthUserT_altova_userNumber, 0, 0> userNumber;	// userNumber CunsignedShort

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CAuthUserT_altova_userRole, 0, 0> userRole;	// userRole CunsignedShort

	MemberAttribute<std::vector<unsigned char>,_altova_mi_ns_sdnp3_altova_CAuthUserT_altova_updateKey, 0, 0> updateKey;	// updateKey Cbase64Binary

	MemberAttribute<string_type,_altova_mi_ns_sdnp3_altova_CAuthUserT_altova_userName, 0, 0> userName;	// userName Cstring
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_sdnp3

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CAuthUserT
