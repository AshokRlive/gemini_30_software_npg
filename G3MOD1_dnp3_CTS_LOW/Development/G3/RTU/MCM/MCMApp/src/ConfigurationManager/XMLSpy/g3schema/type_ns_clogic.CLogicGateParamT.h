#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CLogicGateParamT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CLogicGateParamT

#include "type_ns_clogic.CCLogicParametersBaseT.h"


namespace g3schema
{

namespace ns_clogic
{	

class CLogicGateParamT : public ::g3schema::ns_clogic::CCLogicParametersBaseT
{
public:
	g3schema_EXPORT CLogicGateParamT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CLogicGateParamT(CLogicGateParamT const& init);
	void operator=(CLogicGateParamT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_clogic_altova_CLogicGateParamT); }
	MemberAttribute<string_type,_altova_mi_ns_clogic_altova_CLogicGateParamT_altova_logicOperate, 0, 2> logicOperate;	// logicOperate CEFH_OPERATOR

	MemberAttribute<int,_altova_mi_ns_clogic_altova_CLogicGateParamT_altova_offlineBehaviour, 0, 0> offlineBehaviour;	// offlineBehaviour Cbyte
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_clogic

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CLogicGateParamT
