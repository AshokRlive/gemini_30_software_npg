#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3PointT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3PointT

#include "type_ns_pstack.CPointT.h"


namespace g3schema
{

namespace ns_sdnp3
{	

class CSDNP3PointT : public ::g3schema::ns_pstack::CPointT
{
public:
	g3schema_EXPORT CSDNP3PointT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSDNP3PointT(CSDNP3PointT const& init);
	void operator=(CSDNP3PointT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_sdnp3_altova_CSDNP3PointT); }
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_sdnp3

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3PointT
