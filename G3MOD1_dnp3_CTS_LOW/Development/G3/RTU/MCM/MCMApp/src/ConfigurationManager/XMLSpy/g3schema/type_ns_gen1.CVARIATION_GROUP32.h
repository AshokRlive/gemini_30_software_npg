#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen1_ALTOVA_CVARIATION_GROUP32
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen1_ALTOVA_CVARIATION_GROUP32



namespace g3schema
{

namespace ns_gen1
{	

class CVARIATION_GROUP32 : public TypeBase
{
public:
	g3schema_EXPORT CVARIATION_GROUP32(xercesc::DOMNode* const& init);
	g3schema_EXPORT CVARIATION_GROUP32(CVARIATION_GROUP32 const& init);
	void operator=(CVARIATION_GROUP32 const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen1_altova_CVARIATION_GROUP32); }

	enum EnumValues {
		Invalid = -1,
		k_BIT32_WITHOUT_TIME = 0, // BIT32_WITHOUT_TIME
		k_BIT16_WITHOUT_TIME = 1, // BIT16_WITHOUT_TIME
		k_BIT32_WITH_TIME = 2, // BIT32_WITH_TIME
		k_BIT16_WITH_TIME = 3, // BIT16_WITH_TIME
		k_SINGLE_PREC_FLT_PT_WITHOUT_TIME = 4, // SINGLE_PREC_FLT_PT_WITHOUT_TIME
		k_SINGLE_PREC_FLT_PT_WITH_TIME = 5, // SINGLE_PREC_FLT_PT_WITH_TIME
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen1

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen1_ALTOVA_CVARIATION_GROUP32
