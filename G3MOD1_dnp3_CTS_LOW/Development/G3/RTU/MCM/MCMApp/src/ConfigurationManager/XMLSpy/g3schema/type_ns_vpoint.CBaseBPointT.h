#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CBaseBPointT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CBaseBPointT

#include "type_ns_vpoint.CBaseDigitalPointT.h"


namespace g3schema
{

namespace ns_vpoint
{	

class CBaseBPointT : public ::g3schema::ns_vpoint::CBaseDigitalPointT
{
public:
	g3schema_EXPORT CBaseBPointT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CBaseBPointT(CBaseBPointT const& init);
	void operator=(CBaseBPointT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_vpoint_altova_CBaseBPointT); }

	MemberAttribute<bool,_altova_mi_ns_vpoint_altova_CBaseBPointT_altova_invert, 0, 0> invert;	// invert Cboolean
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_vpoint

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_vpoint_ALTOVA_CBaseBPointT
