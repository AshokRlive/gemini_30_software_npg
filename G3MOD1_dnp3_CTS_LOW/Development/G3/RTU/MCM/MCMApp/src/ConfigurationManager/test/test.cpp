#include <iostream>
#include <string>

#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>

#include "Altova/Altova.h"
#include "Altova/SchemaTypes.h"
#include "Altova/AltovaException.h"

#include "AltovaXML/XmlException.h"
#include "AltovaXML/Node.h"

#include "g3schema/g3schema.h"

using namespace g3schema;
using namespace altova;
using namespace std;


#include "g3schema/g3schema.h"

int main()
{
    try
    {
        xercesc::XMLPlatformUtils::Initialize();

        Cg3schema config = Cg3schema::LoadFromFile(_T("configTest02.xml"));

        CConfigurationType configuration = config.configuration.first();

        CModulesType modules = configuration.modules.first();

        CModuleMCMType MCM = modules.MCM.first();

        CVirtualPointsType virtualPoints = MCM.VirtualPoints.first();

        CbinaryPointsType BPoints = virtualPoints.binaryPoints.first();

        for(Iterator<CBinaryPointType> itBPoint = BPoints.Point.all(); itBPoint; ++itBPoint)
        {
            unsigned int group = itBPoint->ID.first().Group.first();
            unsigned int id = itBPoint->ID.first().ID.first();
            bool invert = itBPoint->Invert.first();
            unsigned int chatterNo = itBPoint->ChatterNo.first();
            unsigned int chatterTime = itBPoint->ChatterTime.first();
            int moduleType = itBPoint->Channel.first().moduleType.first().GetEnumerationValue();
            int ModuleID =  itBPoint->Channel.first().moduleID.first();
            int channelID =  itBPoint->Channel.first().channelID.first();

            printf("Binary point: %i:%i Invert: %i chatterNo: %i chatterTime: %i channel: %i:%i:%i\n", group, id, invert,chatterNo, chatterTime, moduleType, ModuleID, channelID);
        }

        config.DestroyDocument();

        xercesc::XMLPlatformUtils::Terminate();

        tcout << _T("OK") << endl;
        return 0;
    }
    catch (CXmlException& e)
    {
        tcerr << _T("Error: ") << e.GetInfo().c_str() << endl;
        return 1;
    }
    catch (xercesc::XMLException& e)
    {
        tcerr << _T("Xerces XMLException: ") << e.getSrcFile() << _T("(")
              << e.getSrcLine() << _T("): ") << e.getMessage() << endl;
        return 1;
    }
    catch (xercesc::DOMException& e)
    {
        tcerr << _T("Xerces DOMException ") << e.code;
        if (e.msg)
            tcerr << _T(": ") << e.msg;
        tcerr << endl;
        return 1;
    }
    catch (altova::Exception& exception)
    {
        tcerr << "Exception: " << exception.message() << endl;
        return 1;
    }
    catch (altova::Error& exception)
    {
        tcerr << "Error: " << exception.message() << endl;
        return 1;
    }
    catch (std::exception& e)
    {
        cerr << "Exception: " << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        tcerr << _T("Unknown error") << endl;
        return 1;
    }
}

