#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CFPIFaultT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CFPIFaultT



namespace g3schema
{

namespace ns_g3module
{	

class CFPIFaultT : public TypeBase
{
public:
	g3schema_EXPORT CFPIFaultT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CFPIFaultT(CFPIFaultT const& init);
	void operator=(CFPIFaultT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_g3module_altova_CFPIFaultT); }

	MemberAttribute<unsigned,_altova_mi_ns_g3module_altova_CFPIFaultT_altova_minFaultDurationMs, 0, 0> minFaultDurationMs;	// minFaultDurationMs CunsignedShort

	MemberAttribute<unsigned,_altova_mi_ns_g3module_altova_CFPIFaultT_altova_timedFaultCurrent, 0, 0> timedFaultCurrent;	// timedFaultCurrent CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_g3module_altova_CFPIFaultT_altova_InstantFaultCurrent, 0, 0> InstantFaultCurrent;	// InstantFaultCurrent CunsignedInt
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_g3module

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CFPIFaultT
