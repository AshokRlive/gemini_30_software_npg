#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_COutputChannelRefT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_COutputChannelRefT

#include "type_ns_common.CChannelRefT.h"


namespace g3schema
{

namespace ns_common
{	

class COutputChannelRefT : public ::g3schema::ns_common::CChannelRefT
{
public:
	g3schema_EXPORT COutputChannelRefT(xercesc::DOMNode* const& init);
	g3schema_EXPORT COutputChannelRefT(COutputChannelRefT const& init);
	void operator=(COutputChannelRefT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_common_altova_COutputChannelRefT); }

	MemberAttribute<unsigned,_altova_mi_ns_common_altova_COutputChannelRefT_altova_channelType, 0, 0> channelType;	// channelType CunsignedByte
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_common

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_COutputChannelRefT
