/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       XML based G3 control logic factory implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/09/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdio.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "XMLControlLogicFactory.h"
#include "SwitchCANChannel.h"
#include "SwitchCANChannelDSM.h"
#include "ModuleProtocol/ModuleProtocolEnumMCMInternal.h"
#include "SCMIOModule.h"
#include "DSMIOModule.h"
#include "SCMMK2IOModule.h"

/* Control Logic Types */
#include "LocalRemoteControlLogic.h"
#include "LogicGateControlLogic.h"
#include "FanTestControlLogic.h"
#include "SwitchGearLogic.h"
#include "DummySwitchLogic.h"
#include "DigitalOutputLogic.h"
#include "BatteryChargerTestLogic.h"
#include "PLTULogic.h"
#include "PseudoClockLogic.h"
#include "MaxMinAverageLogic.h"
#include "FPITestLogic.h"
#include "FPIResetLogic.h"
#include "DigitalControlPointLogic.h"
#include "ThresholdLogic.h"
#include "ActionControlLogic.h"
#ifdef IEC61131
#include "../../Automation/IEC61131/IEC61131Logic.h"
#endif
#ifdef IEC61499
#include "../../Automation/IEC61499/IEC61499Logic.h"
#include "../../Automation/IEC61499/IEC61499Manager.h"
#endif

#include "AnalogControlPointLogic.h"
#include "CounterCommandLogic.h"
#include "DBinSplitterLogic.h"
#include "AnalogBitmaskLogic.h"
#include "PowerSupplyLogic.h"
#include "MotorSupplyLogic.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define ARRAYSIZE(array) (lu_uint32_t)(sizeof(array)/sizeof(* array))

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/**
 * \brief Gets the configuration of the Channel to be used as Control Logic Output
 *
 * When a Control Logic has a configuration to operate a Channel, this function
 * checks the fields and obtains the Channel configuration.
 *
 * \param type Type of channel expected to be present as Control Logic output.
 * \param xmlField XML structure to parse.
 * \param config Where to store the obtained data.
 *
 * \throw Exception when failed to get all of the elements.
 */
static ChannelRef getXMLOutChannelConf(const OUTPUT_CHANNEL_TYPE type, CControlLogicT& xmlField) throw(ParserException)
{
    if( !xmlField.outputs.exists() )
    {
        throw ParserException(CONFMGR_ERROR_NO_POINT, "No output channel");
    }
    if( !xmlField.outputs.first().outputChannel.exists() )
    {
        throw ParserException(CONFMGR_ERROR_NO_POINT, "No output channel");
    }
    COutputChannelRefT xmlChannelAttr = xmlField.outputs.first().outputChannel.first();
    /* Check proper channel type */
    lu_uint32_t chType = getXMLAttr(xmlChannelAttr.channelType);
    if(chType != type)
    {
        throw ParserException(CONFMGR_ERROR_NO_POINT, "Wrong output channel type");
    }

    /* Get configuration */
    return getXMLChannelConf(xmlChannelAttr);
}


/**
 * \brief Gets the Control Logic ID to be used as Control Logic Output
 *
 * When a Control Logic has a configuration to operate another Control Logic,
 * the ID is given by this field. This function checks the field and gets the
 * ID.
 *
 * \param xmlField XML structure to parse.
 * \param logicID Where to store the obtained data.
 *
 * \throw Exception when failed to get all of the elements.
 */
static lu_uint16_t getXMLOutLogicConf(CControlLogicT& xmlField) throw(ParserException)
{
    if( !xmlField.outputs.exists() )
    {
        throw ParserException(CONFMGR_ERROR_NO_POINT, "No output channel");
    }
    return getXMLElement<lu_uint16_t>(xmlField.outputs.first().outputLogic);
}


/**
 * \brief Gets a value for a given property name
 *
 * When a Control Logic has a configuration expressed in generic properties
 * (given as property name and value), this function finds the property and
 * extracts the value.
 *
 * \param xmlField XML structure to parse, containing "basic" element and a list of properties.
 * \param name Name of the property to find.
 *
 * \return Property value
 *
 * \throw Exception when failed to get the value (Not found/not valid).
 *
 * \example config.count = getXMLValueByPropertyName<lu_uint32_t>(params.generic, "count");
 */
template <typename T, typename XMLFieldType>
T getXMLValueByPropertyName(XMLFieldType& xmlField,
                            const std::string sName,
                            const bool optional = false,
                            const T defaultValue = 0
                            ) throw(ParserException)
{
    bool found = false;

    /* Find property by name */
    checkXMLElement(xmlField);
    Iterator<CParameterT> itParams = xmlField.first().parameter.all();
    for(; itParams; ++itParams)
    {
        if(itParams.parameterName.exists() && itParams.parameterValue.exists())
        {
            if(sName.compare(itParams.parameterName) == 0)
            {
                //Property found
                found = true;
                break;
            }
        }
    }
    if(!found)
    {
        if(optional)
        {
            return defaultValue;
        }
        std::stringstream ss;
        ss << "Property " << sName << " not present";
        throw ParserException(CONFMGR_ERROR_PARAM, ss.str().c_str());
    }

    /* Extract value */
    std::stringstream sValue(itParams.parameterValue);
    T ret;
    sValue >> ret;  //Try to convert to the destination type
    if(sValue.fail())
    {
        std::stringstream ss;
        ss << "Incorrect value '" << sValue << "' for Property " << sName;
        throw ParserException(CONFMGR_ERROR_PARAM, ss.str().c_str());
    }
    return ret;
}


/**
 * \brief Fetch specific Open & close parameters from given Channels
 *
 * \param channelOpen Reference of the Switch Open Channel
 * \param channelClose Reference of the Switch Close Channel
 * \param config Where to store the fetched parameters
 *
 * \example In the case of the DSM, a call looks like this:
 *      getSwitchParameters<SwitchCANChannelDSM::SwitchCANChannelDSMConf,
 *                          SwitchCANChannelDSM>(
 *                                  config.outChannel[OUTCHANNELIDX_OPEN],
 *                                  config.outChannel[OUTCHANNELIDX_CLOSE],
 *                                  config.operationCfg
 *                          );
 */
template <typename SwitchConf, typename SwitchChannel>
static void getSwitchParameters(IChannel* channelOpen,
                                IChannel* channelClose,
                                SwitchGearLogic::SGLOperationCfg& config
                                )
{
    SwitchConf configuration;
    SwitchChannel* tmpChannelPtr;
    tmpChannelPtr = dynamic_cast<SwitchChannel*>(channelOpen);
    tmpChannelPtr->getConfiguration(configuration);
    config.overrunOpenMs = configuration.overrunMS;
    config.timeoutOpenS = configuration.opTimeoutS;

    tmpChannelPtr = dynamic_cast<SwitchChannel*>(channelClose);
    tmpChannelPtr->getConfiguration(configuration);
    config.overrunCloseMs = configuration.overrunMS;
    config.timeoutCloseS = configuration.opTimeoutS;
}




/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static Mutex* globalMutex = NULL;

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

XMLControlLogicFactory::XMLControlLogicFactory(GeminiDatabase& g3database,
                                               IIOModuleManager& MManager,
                                               IStatusManager& statusMgr,
                                               IMemoryManager& memMgr,
                                               Cg3schema& configuration
                                               ) :
                                                database(g3database),
                                                MManager(MManager),
                                                statusManager(statusMgr),
                                                memoryManager(memMgr),
                                                log(Logger::getLogger(SUBSYSTEM_ID_CONFMGR)),
                                                configuration(configuration),
                                                cLogicNum(0)
{
    initParser();
}


XMLControlLogicFactory::~XMLControlLogicFactory()
{}


void XMLControlLogicFactory::setMutexLock(Mutex& mutex)
{
    if(globalMutex == NULL)
    {
        globalMutex = &mutex;    //we assume it is always the same DB mutex!!
    }
}

ControlLogic* XMLControlLogicFactory::newCLogic(lu_uint16_t group)
{
    /* Check if the id is valid */
    if( (group == 0        ) ||
        (group > cLogicNum)
      )
    {
        log.error("%s invalid logic ID %i. Max ID %i", __AT__, group, cLogicNum);
        return NULL;
    }

    CConfigurationT configurationRoot = configuration.configuration.first();
    CControlLogicsT controlLogic = configurationRoot.controlLogic.first();
    /* FIXME: pueyos_a - we are using here XMLdoc index as Control Logic group and is not totally right */
    CControlLogicT cLogic = controlLogic.clogic[group-1];
    if(cLogic.group != group)
    {
        log.error("%s No Control Logic %i found in Configuration.", __AT__, group);
        return NULL;
    }

    switch (cLogic.logicType)
    {
        case CONTROL_LOGIC_TYPE_OLR:
            return parseOLRConfig(cLogic, group);
            break;
        case CONTROL_LOGIC_TYPE_LGT:
            return parseLGTConfig(cLogic, group);
            break;
        case CONTROL_LOGIC_TYPE_DUMMYSW:
            return parseDummySwConfig(cLogic, group);
            break;
        case CONTROL_LOGIC_TYPE_SGL:
            return parseSGLConfig(cLogic, group);
            break;
        case CONTROL_LOGIC_TYPE_DOL:
            return parseDOLConfig(cLogic, group);
            break;
        case CONTROL_LOGIC_TYPE_FPI_TEST:
            return parseFPITestConfig(cLogic, group);
            break;
        case CONTROL_LOGIC_TYPE_FPI_RESET:
            return parseFPIResetConfig(cLogic, group);
            break;
        case CONTROL_LOGIC_TYPE_DIGCTRLPOINT:
            return parseDCPConfig(cLogic, group);
            break;
        case CONTROL_LOGIC_TYPE_ANALOGCTRLPOINT:
            return parseACPConfig(cLogic, group);
            break;
        case CONTROL_LOGIC_TYPE_BATTERY:
            return parseBatteryChargerConfig(cLogic, group);
            break;
        case CONTROL_LOGIC_TYPE_FAN_TEST:
            return parseFanTestConfig(cLogic, group);
            break;
        case CONTROL_LOGIC_TYPE_ACTION:
            return parseActionConfig(cLogic, group);
            break;
        case CONTROL_LOGIC_TYPE_MMAVG:
            return parseMMAvgConfig(cLogic, group);
            break;
        case CONTROL_LOGIC_TYPE_PCLK:
            return parsePCLKConfig(cLogic, group);
            break;
        case CONTROL_LOGIC_TYPE_PLTU:
            return parsePLTUConfig(cLogic, group);
            break;
        case CONTROL_LOGIC_TYPE_THR:
            return parseThresholdConfig(cLogic, group);
            break;
        case CONTROL_LOGIC_TYPE_COUNTER_COMMAND:
            return parseCounterCommandConfig(cLogic, group);
            break;
        case CONTROL_LOGIC_TYPE_DTS:
            return parseDBtoSBConfig(cLogic, group);
            break;
        case CONTROL_LOGIC_TYPE_BTA:
            return parseAnaBitfieldConfig(cLogic, group);
            break;
        case CONTROL_LOGIC_TYPE_POWER_SUPPLY:
            return parsePowerSupplyConfig(cLogic, group);
            break;
        case CONTROL_LOGIC_TYPE_MOTOR_SUPPLY:
            return parseMotorSupplyConfig(cLogic, group);
            break;
        case CONTROL_LOGIC_TYPE_IEC61131:
        case CONTROL_LOGIC_TYPE_IEC61499:
        	return parseAutomationConfig(cLogic, group);
            break;
        default:
            log.error("Cannot create control logic. Unsupported logic type:%i",cLogic.logicType);
            break;
    }

    return NULL;
}


lu_uint16_t XMLControlLogicFactory::getCLogicNumber()
{
    return cLogicNum;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

CONFMGR_ERROR XMLControlLogicFactory::initParser()
{
    try
    {
        /* Get a reference to the control logic */
        CConfigurationT configurationRoot = getXMLElement<CConfigurationT>(configuration.configuration);

        /* Get the amount of Control Logics */
        if(configurationRoot.controlLogic.exists())
        {
            CControlLogicsT controlLogic = configurationRoot.controlLogic.first();
            if(controlLogic.clogic.exists())
            {
                cLogicNum = controlLogic.clogic.count();
            }
        }
        return CONFMGR_ERROR_NONE;
    }
    catch (const std::exception& e)
    {
        log.error("Control Logic Parser init configuration error: %s", e.what());
        return CONFMGR_ERROR_INITPARSER;
    }
}


ControlLogic* XMLControlLogicFactory::parseOLRConfig(CControlLogicT& cLogic,
                                                     const lu_uint16_t group)
{
    ControlLogic* ret = NULL;
    LocalRemoteControlLogic::Config config;

    //NOTE: there can be only one instance of this Control Logic
    try
    {
        config.groupID = group;
        config.enabled = cLogic.enabled;    //Note that is forced to be enabled inside the CLogic itself

        /* === Add mandatory Control Logic points === */
        checkXMLArraySize(cLogic.binaryPoints, LocalRemoteControlLogic::Config::BINARYPOINT_NUM, "Missing Control Logic's Binary points");
        for (lu_uint32_t i = 0; i < LocalRemoteControlLogic::Config::BINARYPOINT_NUM; ++i)
        {
            getXMLDigitalPointConf(configuration, cLogic.binaryPoints[i], config.bPoint[i]);
            config.bPoint[i].invert = getXMLAttr(cLogic.binaryPoints[i].invert);
        }
        checkXMLArraySize(cLogic.dbinaryPoints, LocalRemoteControlLogic::Config::DBINARYPOINT_NUM, "Missing Control Logic's Double Binary points");
        for (lu_uint32_t i = 0; i < LocalRemoteControlLogic::Config::DBINARYPOINT_NUM; ++i)
        {
            getXMLDigitalPointConf(configuration, cLogic.dbinaryPoints[i], config.dbPoint[i]);
        }

        /* === Configure Channels === */
        IIOModule* mcm = MManager.getModule(MODULE_MCM, MODULE_ID_0);
        config.getOLRChannel = mcm->getChannel(CHANNEL_TYPE_INTERNAL_DDINPUT, MCM_CH_INTERNAL_DDINPUT_OLR);
        config.setOLRChannel = mcm->getChannel(CHANNEL_TYPE_INTERNAL_DDOUTPUT, MCM_CH_INTERNAL_DDOUTPUT_OLR);

        ret = new LocalRemoteControlLogic(globalMutex, database, memoryManager, config);
    }
    catch (const std::exception& e)
    {
        logConfigError(group, cLogic.logicType, e.what());
    }
    return ret;
}


ControlLogic* XMLControlLogicFactory::parseLGTConfig(CControlLogicT& cLogic,
                                                     const lu_uint16_t group)
{
    ControlLogic* ret = NULL;
    LogicGateControlLogic::Config config;

    try
    {
        config.groupID = group;
        config.enabled = cLogic.enabled;

        checkXMLElement(cLogic.inputs);

        /* === Get mandatory attributes === */
        CCLogicParametersT params = getXMLElement<CCLogicParametersT>(cLogic.parameters);
        CLogicGateParamT param = getXMLElement<CLogicGateParamT>(params.logicGateParam);
        config.logic = getXMLAttrEnum<LOGIC_GATE_OPERATE>(param.logicOperate, LOGIC_GATE_OPERATE_VALUEfromINDEX);
        /* TODO: pueyos_a - [CLOGIC] enable Logic Gate Control Logic's online behaviour */
        //config.onlineBehaviour = getXMLAttr(param.offlineBehaviour);

        /* === Add mandatory Control Logic points === */
        checkXMLArraySize(cLogic.binaryPoints, LogicGateControlLogic::Config::BINARYPOINT_NUM, "Missing Control Logic's Binary points");
        for (lu_uint32_t i = 0; i < LogicGateControlLogic::Config::BINARYPOINT_NUM; ++i)
        {
            getXMLDigitalPointConf(configuration, cLogic.binaryPoints[i], config.bPoint[i]);
            config.bPoint[i].invert = getXMLAttr(cLogic.binaryPoints[i].invert);
        }

        /* Create Control Logic */
        ret = new LogicGateControlLogic(globalMutex, database, config);

        /* === Configure Inputs after creation === */
        PointIdStr vPointID;
        for( Iterator<CCLogicInputsT> itPoint = cLogic.inputs.all();
            itPoint;
            ++itPoint
           )
        {
            vPointID = getXMLInputPointIDConf(itPoint);
            dynamic_cast<LogicGateControlLogic*>(ret)->addPoint(vPointID);
        }
    }
    catch (const std::exception& e)
    {
        logConfigError(group, cLogic.logicType, e.what());
        if(ret != NULL)
        {
            //Remove object in case of already being created
            delete ret;
            ret = NULL;
        }
    }
    return ret;
}

static lu_bool_t AUTO_INPUT_isMandatory(const lu_int32_t value)
{
    LU_UNUSED(value);
    return LU_FALSE;
}


ControlLogic* XMLControlLogicFactory::parseSGLConfig(CControlLogicT& cLogic,
                                                     const lu_uint16_t group)
{
    ControlLogic* ret = NULL;
    SwitchGearLogic::Config config;

    try
    {
        config.groupID = group;
        config.enabled = cLogic.enabled;

        /* === Get mandatory attributes === */
        checkXMLElement(cLogic.parameters);
        CSwitchgearParamT param = getXMLElement<CSwitchgearParamT>(cLogic.parameters.first().switchgearParam);
        SwitchGearLogic::SGLOperationCfg& opParam = config.operationCfg;
        opParam.delayedOperationLocal  = getXMLAttr(param.localDelay);
        opParam.delayedOperationRemote = getXMLAttr(param.remoteDelay);
        opParam.currentThreshold       = getXMLAttr(param.motorCurrentThreshold);
        opParam.motorOvercurrentTime   = getXMLAttr(param.motorOvercurrentTime);
        opParam.preOperationTime_ms    = getXMLAttr(param.preOperationTime);
        opParam.postOperationTime_ms   = getXMLAttr(param.postOperationTime);

        /* === Get optional attributes === */
        opParam.isCBreaker = LU_FALSE;
        getXMLOptAttr(param.isCircuitBreaker, opParam.isCBreaker);
        opParam.openWhenEarthed = LU_FALSE;
        getXMLOptAttr(param.showOpenWhenEarthed, opParam.openWhenEarthed);

        /* === Add input CL virtual points === */
        getInputVPoints(cLogic, config.dInput, ARRAYSIZE(config.dInput), SGL_INPUT_isMandatory);

        /* === Add Control Logic points === */
        checkXMLArraySize(cLogic.binaryPoints, SwitchGearLogic::Config::BINARYPOINT_NUM, "Missing Control Logic's Binary points");
        for (lu_uint32_t i = 0; i < SwitchGearLogic::Config::BINARYPOINT_NUM; ++i)
        {
            getXMLDigitalPointConf(configuration, cLogic.binaryPoints[i], config.bPoint[i]);
            config.bPoint[i].invert = getXMLAttr(cLogic.binaryPoints[i].invert);
        }
        checkXMLArraySize(cLogic.dbinaryPoints, SwitchGearLogic::Config::DBINARYPOINT_NUM, "Missing Control Logic's Double Binary points");
        for (lu_uint32_t i = 0; i < SwitchGearLogic::Config::DBINARYPOINT_NUM; ++i)
        {
            getXMLDigitalPointConf(configuration, cLogic.dbinaryPoints[i], config.dbPoint[i]);
        }
        checkXMLArraySize(cLogic.analoguePoints, SwitchGearLogic::Config::ANALOGUEPOINT_NUM, "Missing Control Logic's Analogue points");
        for (lu_uint32_t i = 0; i < SwitchGearLogic::Config::ANALOGUEPOINT_NUM; ++i)
        {
            getXMLAnaloguePointConf(configuration, cLogic.analoguePoints[i], config.aPoint[i]);
        }

        /* === Get ID of module used for switching === */
        checkXMLElement(cLogic.outputs);
        COutputModuleRefT swRef = getXMLElement<COutputModuleRefT>(cLogic.outputs.first().outputModule);
        config.operationCfg.moduleSwitch.type = getXMLAttrEnum<MODULE>(swRef.moduleType, MODULE_VALUEfromINDEX);
        config.operationCfg.moduleSwitch.id   = getXMLAttrEnum<MODULE_ID>(swRef.moduleID, MODULE_ID_VALUEfromINDEX);
        config.operationCfg.switchID          = getXMLAttr(swRef.switchID);

        IIOModule* module = MManager.getModule(config.operationCfg.moduleSwitch.type,
                                               config.operationCfg.moduleSwitch.id);
        if(module == NULL)
        {
            std::string msgErr;
            IOModuleIDStr moduleID(config.operationCfg.moduleSwitch.type,
                                   config.operationCfg.moduleSwitch.id);
            msgErr = "Module " + moduleID.toString() + " unavailable";
            throw ParserException(CONFMGR_ERROR_NOT_INITIALIZED, msgErr.c_str());
        }
        bool swPosFail = false; //Failure to get the switch position configuration
        switch(config.operationCfg.moduleSwitch.type)
        {
            case MODULE_SCM:
            {
                SCMConfigStr confSCM;
                SCMIOModule* scm = dynamic_cast<SCMIOModule*>(module);
                swPosFail = (scm->getConfiguration(confSCM) != IOM_ERROR_NONE);
                config.operationCfg.checkPosition = (confSCM.doNotCheckSwitchPosition == 0);
            }
            break;
#if SCM_MK2_SUPPORT
            case MODULE_SCM_MK2:
            {
                SCMMK2ConfigStr confSCM2;
                SCMMK2IOModule* scm2 = dynamic_cast<SCMMK2IOModule*>(module);
                swPosFail = (scm2->getConfiguration(confSCM2) != IOM_ERROR_NONE);
                config.operationCfg.checkPosition = (confSCM2.doNotCheckSwitchPosition == 0);
            }
            break;
#endif
            case MODULE_DSM:
            {
                DSMConfigStr confDSM;
                DSMIOModule* dsm = dynamic_cast<DSMIOModule*>(module);
                swPosFail = (dsm->getConfiguration(confDSM) != IOM_ERROR_NONE);
                switch(config.operationCfg.switchID)
                {
                    case 0:
                        config.operationCfg.checkPosition = (confDSM.doNotCheckSwitchPositionA == 0);
                        break;
                    case 1:
                        config.operationCfg.checkPosition = (confDSM.doNotCheckSwitchPositionB == 0);
                        break;
                    default:
                        logConfigError(group, cLogic.logicType, "Invalid associated switch ID");
                        return NULL;    //invalid switch ID
                        break;
                }
            }
            break;
            default:
                logConfigError(group, cLogic.logicType, "Invalid associated module type");
                return NULL;  //invalid module type
                break;
        }
        if(swPosFail)
        {
            logConfigError(group, cLogic.logicType, "Error getting switch position configuration: "
                                                    "using default (do check switch position)");
        }

        /* === Configure channels === */
        ChannelRef channelRef;
        /* Get PSM Motor Supply channel */
        channelRef.moduleType = MODULE_PSM;
        channelRef.moduleID = MODULE_ID_0;
        channelRef.channelID = PSM_CH_PSUPPLY_MOTOR;
        config.outChannel[SwitchGearLogic::Config::OUTCHANNELIDX_MSUPPLY] = retrieveChannel(group, CHANNEL_TYPE_PSUPPLY, channelRef);
        /* Get PSM Motor Current channel */
        channelRef.channelID = PSM_CH_AINPUT_MS_CURRENT;
        config.inChannel[SwitchGearLogic::Config::INCHANNELIDX_MOTORCURRENT] = retrieveChannel(group, CHANNEL_TYPE_AINPUT, channelRef);
        /* Get PSM Motor HW OverCurrent channel */
        channelRef.channelID = PSM_CH_DINPUT_MS_OVERCURRENT;
        config.inChannel[SwitchGearLogic::Config::INCHANNELIDX_OVERCURRENT] = retrieveChannel(group, CHANNEL_TYPE_DINPUT, channelRef);

        /* Get Open/Close channels */
        getOpenCloseChannels(group,
                            config.operationCfg.moduleSwitch,
                            config.operationCfg.switchID,
                            config.outChannel[SwitchGearLogic::Config::OUTCHANNELIDX_OPEN],
                            config.outChannel[SwitchGearLogic::Config::OUTCHANNELIDX_CLOSE]);
        //Note that the channels are not NULL at this point, otherwise an exception was thrown

        /* Get Overrun & timeout parameters */
        switch (config.operationCfg.moduleSwitch.type)
        {
            case MODULE_SCM:
            {
                getSwitchParameters<SwitchCANChannelConf, SwitchCANChannel>(
                                config.outChannel[SwitchGearLogic::Config::OUTCHANNELIDX_OPEN],
                                config.outChannel[SwitchGearLogic::Config::OUTCHANNELIDX_CLOSE],
                                config.operationCfg
                                );
                break;
            }

#if SCM_MK2_SUPPORT
            case MODULE_SCM_MK2:
            {
                getSwitchParameters<SwitchCANChannelSCM2::SwitchConf, SwitchCANChannelSCM2>(
                                config.outChannel[SwitchGearLogic::Config::OUTCHANNELIDX_OPEN],
                                config.outChannel[SwitchGearLogic::Config::OUTCHANNELIDX_CLOSE],
                                config.operationCfg
                                );
                break;
            }
#endif
            case MODULE_DSM:
            {
                getSwitchParameters<SwitchCANChannelDSM::SwitchCANChannelDSMConf, SwitchCANChannelDSM>(
                                config.outChannel[SwitchGearLogic::Config::OUTCHANNELIDX_OPEN],
                                config.outChannel[SwitchGearLogic::Config::OUTCHANNELIDX_CLOSE],
                                config.operationCfg
                                );
                break;
            }

            default:
                break;
        }
        ret = new SwitchGearLogic(globalMutex, database, config, MManager);
    }
    catch (const std::exception& e)
    {
        logConfigError(group, cLogic.logicType, e.what());
    }
    return ret;
}

static lu_uint16_t getXMLOutLogicConf(CControlLogicT& xmlField, int index) throw(ParserException)
{
    checkXMLArraySize(xmlField.outputs, index+1, "output channel not found");
    return getXMLElement<lu_uint16_t>(xmlField.outputs[index].outputLogic);
}

ControlLogic* XMLControlLogicFactory::parseAutomationConfig(CControlLogicT& cLogic,
                                                     const lu_uint16_t group)
{
    ControlLogic* ret = NULL;

    try
    {

        /* === Get mandatory attributes === */
        checkXMLElement(cLogic.parameters);
        CAutomationT autom = getXMLElement<CAutomationT>(cLogic.parameters.first().automation);

        AutomationLogic::Config config(
        		autom.constants.first().constant.count(),
        		cLogic.inputs.count(),
        		cLogic.binaryPoints.count()  ,
        		cLogic.dbinaryPoints.count() ,
        		cLogic.analoguePoints.count(),
        		cLogic.counterPoints.count() ,
        		cLogic.outputs.count()
        		);

        config.groupID = group;
        config.enabled = cLogic.enabled;
        config.autoStart = autom.autoStart.exists() ? autom.autoStart : false;

        /* === Get lib name === */
        std::string libSchemeFile;
        if(autom.sharedLibraryFile.exists())
            libSchemeFile = autom.sharedLibraryFile;

        /* === Get resource name === */
        std::string resName;
        if(autom.autoResName.exists())
            resName = autom.autoResName;
        snprintf(config.resName, sizeof(config.resName),"%s", resName.c_str());

        /* === Get scheme id === */
        config.schemeID = -1;
        getXMLOptAttr(autom.autoSchemeType, config.schemeID);

        /* === Add constants === */
        if(config.constantsNum > 0)
        {
        	checkXMLElement(autom.constants);
        	checkXMLElement(autom.constants.first().constant);
			lu_uint32_t i = 0;
        	for(Iterator<CAutomationConstantT> itConst = autom.constants.first().constant.all();
        			itConst && (i<config.constantsNum);
        			++itConst, ++i)
			{
				std::string valStr = itConst.value2;
				std::string valType = itConst.valueType;
				lu_int32_t val;
				if(valType.compare("Boolean") == 0)
				{
				   if(valStr.compare("true") == 0)
				       val = 1;
				   else
				       val = 0;
				}
				else if(valType.compare("Float") == 0)
				{
				    float fval = atof(valStr.c_str());
				    val = *((lu_int32_t*)(&fval));
				}
				else
				{
				    val = atoi(valStr.c_str());
				}

        		config.addConstant(i, val);
			}
        }

        /* === Add input CL virtual points === */
        if(config.inputsNum > 0)
        {
        	getInputVPoints(cLogic, config.inputs, config.inputsNum, AUTO_INPUT_isMandatory);
        }

        /* === Add Control Logic points === */
        checkXMLArraySize(cLogic.binaryPoints, config.bPointNum, "Missing Control Logic's Binary points");
        for (lu_uint32_t i = 0; i < config.bPointNum; ++i)
        {
            getXMLDigitalPointConf(configuration, cLogic.binaryPoints[i], config.bPoint[i]);
            config.bPoint[i].invert = getXMLAttr(cLogic.binaryPoints[i].invert);
        }
        checkXMLArraySize(cLogic.dbinaryPoints, config.dbPointNum, "Missing Control Logic's Double Binary points");
        for (lu_uint32_t i = 0; i < config.dbPointNum; ++i)
        {
            getXMLDigitalPointConf(configuration, cLogic.dbinaryPoints[i], config.dbPoint[i]);
        }
        checkXMLArraySize(cLogic.analoguePoints, config.aPointNum, "Missing Control Logic's Analogue points");
        for (lu_uint32_t i = 0; i < config.aPointNum; ++i)
        {
            getXMLAnaloguePointConf(configuration, cLogic.analoguePoints[i], config.aPoint[i]);
        }
        checkXMLArraySize(cLogic.counterPoints, config.cPointNum, "Missing Control Logic's Counter points");
        for (lu_uint32_t i = 0; i < config.cPointNum; ++i)
        {
            getXMLCounterPointConf(configuration, cLogic.counterPoints[i], config.cPoint[i]);
        }

        /* === Configure Output Control Logic === */
        for (lu_uint32_t i = 0; i < config.outputsNum; ++i)
        {
        	config.outputs[i] = getXMLOutLogicConf(cLogic,i);
        }

#ifdef IEC61131
        if(cLogic.logicType == CONTROL_LOGIC_TYPE_IEC61131)
        {
            snprintf(config.libSchemeFile, sizeof(config.libSchemeFile),"%s", libSchemeFile.c_str());
            ret = new IEC61131Logic(globalMutex, database, config);
        }
#endif

#ifdef IEC61499
        if(cLogic.logicType == CONTROL_LOGIC_TYPE_IEC61499)
        {
            IEC61499Manager::writeForteBootFile(libSchemeFile);
            ret = new IEC61499Logic(globalMutex, database, config);
        }
#endif
    }
    catch (const std::exception& e)
    {
        logConfigError(group, cLogic.logicType, e.what());
    }

    if(ret == NULL)
        log.error("Automation Logic not created!");
    return ret;
}


ControlLogic* XMLControlLogicFactory::parseDummySwConfig(CControlLogicT& cLogic,
                                                         const lu_uint16_t group)
{
    ControlLogic* ret = NULL;
    DummySwitchLogic::Config config;

    try
    {
        config.groupID = group;
        config.enabled = cLogic.enabled;

        /* === Add input CL virtual points === */
        PointIdStr dInput[DSL_INPUT_LAST];
        getInputVPoints(cLogic, dInput, ARRAYSIZE(dInput), DSL_INPUT_isMandatory);
        config.useLocalRemote = (dInput[DSL_INPUT_OLR].isValid());
        config.inhibitPoint.pointID = dInput[DSL_INPUT_INHIBIT];

        /* === Add Control Logic points === */
        checkXMLArraySize(cLogic.binaryPoints, DummySwitchLogic::Config::BINARYPOINT_NUM, "Missing Control Logic's Binary points");
        for (lu_uint32_t i = 0; i < DummySwitchLogic::Config::BINARYPOINT_NUM; ++i)
        {
            getXMLDigitalPointConf(configuration, cLogic.binaryPoints[i], config.bPoint[i]);
            config.bPoint[i].invert = getXMLAttr(cLogic.binaryPoints[i].invert);
        }
        checkXMLArraySize(cLogic.dbinaryPoints, DummySwitchLogic::Config::DBINARYPOINT_NUM, "Missing Control Logic's Double Binary points");
        for (lu_uint32_t i = 0; i < DummySwitchLogic::Config::DBINARYPOINT_NUM; ++i)
        {
            getXMLDigitalPointConf(configuration, cLogic.dbinaryPoints[i], config.dbPoint[i]);
        }

        /* === Get mandatory attributes === */
        checkXMLElement(cLogic.parameters);
        CDummySwitchParamT params = getXMLElement<CDummySwitchParamT>(cLogic.parameters.first().dummySwitchParam);
        /* Extract open colour since it is not an enum, but a string only */
        config.opParams.openColour = LED_COLOUR_RED;    //default
        std::string colour = getXMLAttr(params.openColour);
        if(colour.compare("LED_COLOUR_GREEN") == 0)
        {
            config.opParams.openColour = LED_COLOUR_GREEN;
        }
        config.opParams.interval   = getXMLAttr(params.interval);
        config.opParams.allowForced = getXMLAttr(params.allowForcedOperation);

        ret = new DummySwitchLogic(globalMutex, database, memoryManager, config);
    }
    catch (const std::exception& e)
    {
        logConfigError(group, cLogic.logicType, e.what());
    }
    return ret;
}


ControlLogic* XMLControlLogicFactory::parseDOLConfig(CControlLogicT& cLogic,
                                                     const lu_uint16_t group)
{
    ControlLogic* ret = NULL;
    DigitalOutputLogic::Config config;

    try
    {
        config.groupID = group;
        config.enabled = cLogic.enabled;

        /* === Add Control Logic points === */
        checkXMLArraySize(cLogic.binaryPoints, DigitalOutputLogic::Config::BINARYPOINT_NUM, "Missing Control Logic's Binary points");
        for (lu_uint32_t i = 0; i < DigitalOutputLogic::Config::BINARYPOINT_NUM; ++i)
        {
            getXMLDigitalPointConf(configuration, cLogic.binaryPoints[i], config.bPoint[i]);
            config.bPoint[i].invert = getXMLAttr(cLogic.binaryPoints[i].invert);
        }

        /* === Add input CL virtual points === */
        /* Optional input points */
        PointIdStr dInput[DOL_INPUT_LAST];
        getInputVPoints(cLogic, dInput, ARRAYSIZE(dInput), DOL_INPUT_isMandatory);
        config.useLocalRemote = (dInput[DOL_INPUT_OLR].isValid());
        config.inhibitPoint.pointID = dInput[DOL_INPUT_INHIBIT];

        /* === Get mandatory attributes === */
        /* === Configure Channels === */
        ChannelRef channel = getXMLOutChannelConf(OUTPUT_CHANNEL_TYPE_DOUTPUT, cLogic);
        config.channel = retrieveChannel(group, CHANNEL_TYPE_DOUTPUT, channel);

        ret = new DigitalOutputLogic(globalMutex, database, config);
    }
    catch (const std::exception& e)
    {
        logConfigError(group, cLogic.logicType, e.what());
    }
    return ret;
}


ControlLogic* XMLControlLogicFactory::parseBatteryChargerConfig(CControlLogicT& cLogic,
                                                                const lu_uint16_t group)
{
    ControlLogic* ret = NULL;
    BatteryChargerTestLogic::Config config;

    try
    {
        config.groupID = group;
        config.enabled = cLogic.enabled;

        /* === Add Control Logic points === */
        /* === Get mandatory attributes === */
        checkXMLElement(cLogic.parameters);
        CBatteryTestParamT params = getXMLElement<CBatteryTestParamT>(cLogic.parameters.first().batteryTestParam);
        lu_uint32_t duration = getXMLAttr(params.duration);

        /* === Add mandatory input virtual points === */
        /* === Add input CL virtual points === */
        PointIdStr dInput[BATTERY_CHARGER_INPUT_LAST];
        getInputVPoints(cLogic, dInput, ARRAYSIZE(dInput), BATTERY_CHARGER_INPUT_isMandatory);
        config.useLocalRemote = (dInput[BATTERY_CHARGER_INPUT_OLR].isValid());
        config.inhibitPoint.pointID = dInput[BATTERY_CHARGER_INPUT_INHIBIT];

        /* === Configure Channels === */
		ChannelRef channelRef = getXMLOutChannelConf(OUTPUT_CHANNEL_TYPE_BCHARGER, cLogic);
        config.channelRef = channelRef;
        config.channel = retrieveChannel(group, CHANNEL_TYPE_BCHARGER, channelRef);

        /* in the PSM's Battery test channel, the duration in secs must be converted to minutes */
        if( (channelRef.moduleType == MODULE_PSM) && (channelRef.channelID == PSM_CH_BCHARGER_BATTERY_TEST) )
        {
            config.opParams.duration = duration / 60; //Convert from seconds to minutes
        }
        else
        {
            config.opParams.duration = duration; //Leave it as it is
        }
        
        ret = new BatteryChargerTestLogic(globalMutex, database, config);
    }
    catch (const std::exception& e)
    {
        logConfigError(group, cLogic.logicType, e.what());
    }
    return ret;
}


ControlLogic* XMLControlLogicFactory::parseFanTestConfig(CControlLogicT& cLogic,
                                                         const lu_uint16_t group)
{
    ControlLogic* ret = NULL;
    FanTestControlLogic::Config config;

    try
    {
        config.groupID = group;
        config.enabled = cLogic.enabled;

        /* === Add Control Logic points === */
        /* === Get mandatory attributes === */
        checkXMLElement(cLogic.parameters);
        CFanTestParamT params = getXMLElement<CFanTestParamT>(cLogic.parameters.first().fanTestParam);
        config.operationParameters.duration = getXMLAttr(params.duration);

        /* === Add input CL virtual points === */
        PointIdStr dInput[FAN_INPUT_LAST];
        getInputVPoints(cLogic, dInput, ARRAYSIZE(dInput), FAN_INPUT_isMandatory);
        config.useLocalRemote = (dInput[FAN_INPUT_OLR].isValid());
        config.inhibitPoint.pointID = dInput[FAN_INPUT_INHIBIT];

        /* === Configure Channels === */
        ChannelRef channel = getXMLOutChannelConf(OUTPUT_CHANNEL_TYPE_FAN, cLogic);
        config.channel = retrieveChannel(group, CHANNEL_TYPE_FAN, channel);

        ret = new FanTestControlLogic(globalMutex, database, config);
    }
    catch (const std::exception& e)
    {
        logConfigError(group, cLogic.logicType, e.what());
    }
    return ret;
}


ControlLogic* XMLControlLogicFactory::parseMMAvgConfig(CControlLogicT& cLogic,
                                                       const lu_uint16_t group)
{
    ControlLogic* ret = NULL;
    MaxMinAverageLogic::Config config;

    try
    {
        config.groupID = group;
        config.enabled = cLogic.enabled;

        /* === Get mandatory attributes === */
        checkXMLElement(cLogic.parameters);
        CMinMaxAvgParamT params = getXMLElement<CMinMaxAvgParamT>(cLogic.parameters.first().minMaxAvgParam);
        config.operationParams.startDelay_s = getXMLAttr(params.startDelay);

        /* If the o'clock time is present, use it. Otherwise use interval time */
        if(params.clocktime.exists())
        {
            config.operationParams.periodTime_s = getXMLAttr(params.clocktime);
            config.operationParams.intervalType = MaxMinAverageLogic::OperationParams::INTERVAL_OCLOCK;
        }
        else
        {
            config.operationParams.periodTime_s = getXMLAttr(params.period);
            config.operationParams.intervalType = MaxMinAverageLogic::OperationParams::INTERVAL_PERIOD;
        }
        config.operationParams.periodTime_s *= 60; //Convert minutes to seconds

        /* Optional Counter point parameters */
        bool counterEnabled = false;
        getXMLOptAttr(params.enableCounter, counterEnabled);
        config.operationParams.supportCounter = counterEnabled;
        config.allPointNum = (!counterEnabled)? MaxMinAverageLogic::Config::ANALOGUEPOINT_NUM :
                                              (MaxMinAverageLogic::Config::COUNTERPOINT_NUM + MaxMinAverageLogic::Config::ANALOGUEPOINT_NUM);

        if(counterEnabled)
        {
            config.operationParams.scale = getXMLAttr(params.counterScalingFactor);
        }

        /* === Add mandatory input CL virtual points === */
        checkXMLElement(cLogic.inputs);
        config.aInput[MMAVG_INPUT_VALUE] = getXMLInputPointIDConf(cLogic.inputs.first());

        /* === Add Control Logic points === */
        checkXMLArraySize(cLogic.analoguePoints, MaxMinAverageLogic::Config::ANALOGUEPOINT_NUM, "Missing Control Logic's Analogue points");
        for (lu_uint32_t i = 0; i < MaxMinAverageLogic::Config::ANALOGUEPOINT_NUM; ++i)
        {
            getXMLAnaloguePointConf(configuration, cLogic.analoguePoints[i], config.aPoint[i]);
        }

        if(counterEnabled)
        {
            /* Add Counter pseudo points */
            checkXMLArraySize(cLogic.counterPoints, MaxMinAverageLogic::Config::COUNTERPOINT_NUM, "Missing Control Logic's Counter points");
            for (lu_uint32_t i = 0; i < MaxMinAverageLogic::Config::COUNTERPOINT_NUM; ++i)
            {
                getXMLCounterPointConf(configuration, cLogic.counterPoints[i], config.cPoint[i]);
            }
        }

        ret = new MaxMinAverageLogic(globalMutex, database, config);
    }
    catch (const std::exception& e)
    {
        logConfigError(group, cLogic.logicType, e.what());
    }
    return ret;
}


ControlLogic* XMLControlLogicFactory::parsePCLKConfig(CControlLogicT& cLogic,
                                                      const lu_uint16_t group)
{
    ControlLogic* ret = NULL;
    PseudoClockLogic::Config config;

    try
    {
        config.groupID = group;
        config.enabled = cLogic.enabled;

        /* === Get mandatory attributes === */
        /* === Get optional attributes === */
        checkXMLElement(cLogic.parameters);
        CPseudoClockT params = getXMLElement<CPseudoClockT>(cLogic.parameters.first().pseudoClock);
        getXMLOptAttr(params.autoStartEnabled, config.startupEnabled);
        bool toggleIt = true;
        getXMLOptAttr(params.outputToggleEnabled, toggleIt);
        if(toggleIt)
        {
            config.pulseWidth_ms = 0;   //No pulse
        }
        else
        {
            getXMLOptAttr(params.outputPulseWidth, config.pulseWidth_ms);
        }
        bool periodic = (config.timingType == config.TIMING_INTERVAL);
        getXMLOptAttr(params.periodicIntervalEnabled, periodic);
        if(periodic)
        {
            config.timingType = config.TIMING_INTERVAL;
            getXMLOptAttr(params.periodicIntervalMs, config.tickTime_ms);
        }
        else
        {
            config.timingType = config.TIMING_OCLOCK;
            lu_uint32_t tickTime_mins = config.tickTime_ms / 60000; //ms to minutes
            getXMLOptAttr(params.fromOClockMins, tickTime_mins);
            config.tickTime_ms = tickTime_mins * 60000; //minutes to ms
        }

        /* === Add input CL virtual points === */
        PointIdStr dInput[PCLK_INPUT_LAST];
        getInputVPoints(cLogic, dInput, ARRAYSIZE(dInput), PCLK_INPUT_isMandatory);
        config.useLocalRemote = (dInput[PCLK_INPUT_OLR].isValid());
        config.inhibitPoint.pointID = dInput[PCLK_INPUT_INHIBIT];

        /* === Add Control Logic points === */
        checkXMLArraySize(cLogic.binaryPoints, PseudoClockLogic::Config::BINARYPOINT_NUM, "Missing Control Logic's Binary points");
        for (lu_uint32_t i = 0; i < PseudoClockLogic::Config::BINARYPOINT_NUM; ++i)
        {
            getXMLDigitalPointConf(configuration, cLogic.binaryPoints[i], config.bPoint[i]);
            config.bPoint[i].invert = getXMLAttr(cLogic.binaryPoints[i].invert);
        }

        if(!config.validate())
        {
            std::stringstream ss;
            ss << "Invalid configuration.";
            if(config.pulseWidth_ms >= config.tickTime_ms)
            {
               ss << " The pulse width "
                  << static_cast<lu_uint32_t>(config.pulseWidth_ms)
                  << " is bigger than the time for ticking ("
                  << static_cast<lu_uint32_t>(config.tickTime_ms)
                  << ") specified.";
            }
            throw ParserException(CONFMGR_ERROR_PARAM, ss.str().c_str());
        }
        ret = new PseudoClockLogic(globalMutex, database, config);
    }
    catch (const std::exception& e)
    {
        logConfigError(group, cLogic.logicType, e.what());
    }
    return ret;
}


ControlLogic* XMLControlLogicFactory::parsePLTUConfig(CControlLogicT& cLogic,
                                                      const lu_uint16_t group)
{
    ControlLogic* ret = NULL;
    PLTULogic::Config config;

    try
    {
        config.groupID = group;
        config.enabled = cLogic.enabled;

        /* === Get mandatory attributes === */
        checkXMLElement(cLogic.parameters);
        CPLTUParamT params = getXMLElement<CPLTUParamT>(cLogic.parameters.first().pltuParam);
        config.operationParameters.voltageDelay = getXMLAttr(params.voltageMismatchDelaySec);

        /* === Add input CL virtual points === */
        getInputVPoints(cLogic, config.dInput, ARRAYSIZE(config.dInput), PLTU_INPUT_isMandatory);

        /* === Add Control Logic points === */
        checkXMLArraySize(cLogic.binaryPoints, PLTULogic::Config::BINARYPOINT_NUM, "Missing Control Logic's Binary points");
        for (lu_uint32_t i = 0; i < PLTULogic::Config::BINARYPOINT_NUM; ++i)
        {
            getXMLDigitalPointConf(configuration, cLogic.binaryPoints[i], config.bPoint[i]);
            config.bPoint[i].invert = getXMLAttr(cLogic.binaryPoints[i].invert);
        }

        /* === Configure CB Control Logic === */
        config.switchCBID = getXMLOutLogicConf(cLogic);

        ret = new PLTULogic(globalMutex, database, config);
    }
    catch (const std::exception& e)
    {
        logConfigError(group, cLogic.logicType, e.what());
    }
    return ret;
}


ControlLogic* XMLControlLogicFactory::parseFPITestConfig(CControlLogicT& cLogic,
                                                         const lu_uint16_t group)
{
    ControlLogic* ret = NULL;
    FPITestLogic::Config config;

    try
    {
        config.groupID = group;
        config.enabled = cLogic.enabled;

        /* === Get mandatory attributes === */
        /* === Add input CL virtual points === */
        PointIdStr dInput[FPITEST_INPUT_LAST];
        getInputVPoints(cLogic, dInput, ARRAYSIZE(dInput), FPITEST_INPUT_isMandatory);

        /* === Add Control Logic points === */
        /* === Configure Output Channel === */
        ChannelRef channel = getXMLOutChannelConf(OUTPUT_CHANNEL_TYPE_FPI, cLogic);
        config.channel = retrieveChannel(group, CHANNEL_TYPE_FPI, channel);

        ret = new FPITestLogic(globalMutex, database, config);
    }
    catch (const std::exception& e)
    {
        logConfigError(group, cLogic.logicType, e.what());
    }
    return ret;
}


ControlLogic* XMLControlLogicFactory::parseFPIResetConfig(CControlLogicT& cLogic,
                                                          const lu_uint16_t group)
{
    ControlLogic* ret = NULL;
    FPIResetLogic::Config config;

    try
    {
        config.groupID = group;
        config.enabled = cLogic.enabled;

        /* === Get mandatory attributes === */
        checkXMLElement(cLogic.parameters);
        CFPIResetParamT params = getXMLElement<CFPIResetParamT>(cLogic.parameters.first().fpiResetParam);
        config.dependsLVRestore = getXMLAttr(params.resetFPIOnLVRestored);
        config.dependsLVLoss    = getXMLAttr(params.resetFPIOnLVLost);

        /* === Add input CL virtual points === */
        getInputVPoints(cLogic, config.dInput, ARRAYSIZE(config.dInput), FPIRESET_INPUT_isMandatory);

        /* === Add Control Logic points === */
        /* === Configure Output Channel === */
        ChannelRef channel = getXMLOutChannelConf(OUTPUT_CHANNEL_TYPE_FPI, cLogic);
        config.channel = retrieveChannel(group, CHANNEL_TYPE_FPI, channel);

        ret = new FPIResetLogic(globalMutex, database, config);
    }
    catch (const std::exception& e)
    {
        logConfigError(group, cLogic.logicType, e.what());
    }
    return ret;
}


ControlLogic* XMLControlLogicFactory::parseDCPConfig(CControlLogicT& cLogic,
                                                     const lu_uint16_t group)
{
    ControlLogic* ret = NULL;
    DigitalControlPointLogic::Config config;

    try
    {
        config.groupID = group;
        config.enabled = cLogic.enabled;

        /* === Get mandatory attributes === */
        checkXMLElement(cLogic.parameters);
        CDigitalControlParamT params = getXMLElement<CDigitalControlParamT>(cLogic.parameters.first().digCtrlParam);
        config.pulseEnabled = getXMLAttr(params.pulseEnabled);
        config.pulseDuration_ms = getXMLAttr(params.pulseDuration);
        lu_uint32_t trigger = getXMLAttr(params.trigger);
        config.trigger = static_cast<EDGE_DRIVEN_MODE>(trigger);

        /* === Add input CL virtual points === */
        PointIdStr dInput[DIGCTRL_INPUT_LAST];
        getInputVPoints(cLogic, dInput, ARRAYSIZE(dInput), DIGCTRL_INPUT_isMandatory);
        config.useLocalRemote = (dInput[DIGCTRL_INPUT_OLR].isValid());
        config.inhibitPoint.pointID = dInput[DIGCTRL_INPUT_INHIBIT];
        config.inhibitPoint.index = DIGCTRL_INPUT_INHIBIT;

        /* === Add Control Logic points === */
        checkXMLArraySize(cLogic.binaryPoints, DigitalControlPointLogic::Config::BINARYPOINT_NUM, "Missing Control Logic's Binary points");
        for (lu_uint32_t i = 0; i < DigitalControlPointLogic::Config::BINARYPOINT_NUM; ++i)
        {
            getXMLDigitalPointConf(configuration, cLogic.binaryPoints[i], config.bPoint[i]);
            config.bPoint[i].invert = getXMLAttr(cLogic.binaryPoints[i].invert);
        }

        ret = new DigitalControlPointLogic(globalMutex, database, config);
    }
    catch (const std::exception& e)
    {
        logConfigError(group, cLogic.logicType, e.what());
    }
    return ret;
}

ControlLogic* XMLControlLogicFactory::parseACPConfig(CControlLogicT& cLogic,
                                                     const lu_uint16_t group)
{
    ControlLogic* ret = NULL;
    AnalogControlPointLogic::Config config;

    try
    {
        config.groupID = group;
        config.enabled = cLogic.enabled;

        /* === Get mandatory attributes === */

        /* === Add input CL virtual points === */
        PointIdStr aInput[ANALOGCTRL_INPUT_LAST];
        getInputVPoints(cLogic, aInput, ARRAYSIZE(aInput), ANALOGCTRL_INPUT_isMandatory);
        config.useLocalRemote = (aInput[ANALOGCTRL_INPUT_OLR].isValid());
        config.inhibitPoint.pointID = aInput[ANALOGCTRL_INPUT_INHIBIT];
        config.inhibitPoint.index = ANALOGCTRL_INPUT_INHIBIT;



        /* === Add Control Logic points === */
        checkXMLArraySize(cLogic.analoguePoints, AnalogControlPointLogic::Config::ANALOGPOINT_NUM, "Missing Control Logic's Analogue points");
        for (lu_uint32_t i = 0; i < AnalogControlPointLogic::Config::ANALOGPOINT_NUM; ++i)
        {
            getXMLAnaloguePointConf(configuration, cLogic.analoguePoints[i], config.aPoint[i]);
        }

        ret = new AnalogControlPointLogic(globalMutex, database, config);
    }
    catch (const std::exception& e)
    {
        logConfigError(group, cLogic.logicType, e.what());
    }
    return ret;
}

ControlLogic* XMLControlLogicFactory::parseThresholdConfig(CControlLogicT& cLogic,
                                                           const lu_uint16_t group)
{
    ControlLogic* ret = NULL;
    ThresholdLogic::Config config;

    try
    {
        config.groupID = group;
        config.enabled = cLogic.enabled;

        /* === Get mandatory attributes === */
        checkXMLElement(cLogic.parameters);
        CThresholdParamT params = getXMLElement<CThresholdParamT>(cLogic.parameters.first().thresholdParam);
		parseXMLThresholdFilterConf(params, config.filterConf);

		//Please note that all params fields have been already checked by parseXMLThresholdFilterConf()
		config.binOutEnabled[THR_POINT_HIHI] =  params.hihiEnabled;
		config.binOutEnabled[THR_POINT_HI] =    params.hiEnabled;
		config.binOutEnabled[THR_POINT_LO] =    params.loEnabled;
		config.binOutEnabled[THR_POINT_LOLO] =  params.loloEnabled;

        /* === Add input CL virtual points === */
        checkXMLElement(cLogic.inputs);
        config.aInput = getXMLInputPointIDConf(cLogic.inputs.first());

        /* === Add Control Logic points === */
        checkXMLArraySize(cLogic.binaryPoints, ThresholdLogic::Config::BINARYPOINT_NUM, "Missing Control Logic's Binary points");
        for (lu_uint32_t i = 0; i < ThresholdLogic::Config::BINARYPOINT_NUM; ++i)
        {
            getXMLDigitalPointConf(configuration, cLogic.binaryPoints[i], config.bPoint[i]);
            config.bPoint[i].invert = getXMLAttr(cLogic.binaryPoints[i].invert);
        }

        ret = new ThresholdLogic(globalMutex, database, config);
    }
    catch (const std::exception& e)
    {
        logConfigError(group, cLogic.logicType, e.what());
    }
    return ret;
}



ControlLogic* XMLControlLogicFactory::parseActionConfig(CControlLogicT& cLogic,
                                                         const lu_uint16_t group)
{
    ControlLogic* ret = NULL;
    ActionControlLogic::Config config;


    try
    {
        config.groupID = group;
        config.enabled = cLogic.enabled;

        checkXMLElement(cLogic.inputs);

        /* === Get mandatory attributes === */
        checkXMLElement(cLogic.parameters);
        CActionParamT params = getXMLElement<CActionParamT>(cLogic.parameters.first().actionParam);
        config.delay = getXMLAttr(params.delay);

        /* === Configure output Control Logic === */
        config.controlLogicID = getXMLOutLogicConf(cLogic);

        ret = new ActionControlLogic(globalMutex, database, config);

        /* === Configure Inputs after creation === */
        PointIdStr vPointID;
        DigitalChangeEvent::Config eventCfg;
        ACTION_CLOGIC_CMD action;
        ACTION_CLOGIC_OLR olr = ACTION_CLOGIC_OLR_UNUSED;
        Iterator<CCLogicInputsT> itPoint = cLogic.inputs.all();
        Iterator<CActionInputSettingT> itSetting = params.inputSetting.all();
        for(;
            itPoint && itSetting;
            ++itPoint, ++itSetting
           )
        {
            checkXMLElement(itPoint->inputPoint);
            vPointID = getXMLVirtualPointRef(itPoint->inputPoint.first());
            checkXMLElement(itSetting->inputMode);
            eventCfg = getXMLInputDigitalModeConf(itSetting->inputMode.first());
            checkXMLElement(itSetting.command);
            lu_uint32_t actionCommand;
            actionCommand = getXMLAttr(itSetting.command);
            action = static_cast<ACTION_CLOGIC_CMD>(actionCommand);
            /* TODO: pueyos_a - add Action OLR dependency */
            //olr = getXMLAttrEnum<ACTION_CLOGIC_OLR>(itSetting->actionCommand.first().localRemote, ACTION_CLOGIC_OLR_VALUEfromINDEX);
            dynamic_cast<ActionControlLogic*>(ret)->addPoint(vPointID, eventCfg, action, olr);
        }
    }
    catch (const std::exception& e)
    {
        logConfigError(group, cLogic.logicType, e.what());
        if(ret != NULL)
        {
            //Remove object in case of already being created
            delete ret;
            ret = NULL;
        }
    }
    return ret;
}


ControlLogic* XMLControlLogicFactory::parseCounterCommandConfig(CControlLogicT& cLogic,
                                                                 const lu_uint16_t group)
{
    ControlLogic* ret = NULL;
    CounterCommandLogic::Config config;

    try
    {
        config.groupID = group;
        config.enabled = cLogic.enabled;

        /* === Get mandatory attributes === */
        CCLogicParametersT params = getXMLElement<CCLogicParametersT>(cLogic.parameters);
        CCounterCommandParamT param = getXMLElement<CCounterCommandParamT>(params.counterCommand);
        lu_uint8_t action = getXMLAttr(param.action);
        if(action >= COUNTER_COMMAND_ACTION_LAST)
        {
            std::stringstream ss;
            ss << "Invalid action (" << static_cast<lu_uint32_t>(action)
               << ") specified";
            throw ParserException(CONFMGR_ERROR_PARAM, ss.str().c_str());
        }
        config.action = static_cast<COUNTER_COMMAND_ACTION>(action);

        /* === Add mandatory Control Logic output === */
        checkXMLElement(cLogic.outputs);
        CVirtualPointRefT pointRef = getXMLElement<CVirtualPointRefT>(cLogic.outputs.first().outputPoint);
        config.cPointID = getXMLVirtualPointRef(pointRef);    //Counter to be operated

        /* Create Control Logic */
        ret = new CounterCommandLogic(globalMutex, database, config);
    }
    catch (const std::exception& e)
    {
        logConfigError(group, cLogic.logicType, e.what());
    }
    return ret;
}


ControlLogic* XMLControlLogicFactory::parseDBtoSBConfig(CControlLogicT& cLogic,
                                                        const lu_uint16_t group)
{
    ControlLogic* ret = NULL;
    DBinSplitterLogic::Config config;

    try
    {
        config.groupID = group;
        config.enabled = cLogic.enabled;

        /* === Add input CL virtual points === */
        checkXMLElement(cLogic.inputs);
        config.dbInput = getXMLInputPointIDConf(cLogic.inputs.first());

        /* === Add Control Logic points === */
        checkXMLArraySize(cLogic.binaryPoints, DBinSplitterLogic::Config::SBPOINT_NUM, "Missing Control Logic's Binary points");
        for (lu_uint32_t i = 0; i < DBinSplitterLogic::Config::SBPOINT_NUM; ++i)
        {
            getXMLDigitalPointConf(configuration, cLogic.binaryPoints[i], config.bPoint[i]);
            config.bPoint[i].invert = getXMLAttr(cLogic.binaryPoints[i].invert);
        }

        /* Create Control Logic */
        ret = new DBinSplitterLogic(globalMutex, database, config);
    }
    catch (const std::exception& e)
    {
        logConfigError(group, cLogic.logicType, e.what());
    }
    return ret;
}


ControlLogic* XMLControlLogicFactory::parseAnaBitfieldConfig(CControlLogicT& cLogic,
                                                            const lu_uint16_t group)
{
    ControlLogic* ret = NULL;
    AnalogBitmaskLogic::Config config;

    try
    {
        config.groupID = group;
        config.enabled = cLogic.enabled;

        /* === Get mandatory attributes === */
        CCLogicParametersT params = getXMLElement<CCLogicParametersT>(cLogic.parameters);
        config.debounce_ms = getXMLValueByPropertyName<lu_uint32_t>(params.generic, "debounce");

        /* === Add Control Logic points === */
        checkXMLArraySize(cLogic.analoguePoints, AnalogBitmaskLogic::Config::AIPOINT_NUM, "Missing Control Logic's Binary points");
        getXMLAnaloguePointConf(configuration, cLogic.analoguePoints[0], config.aPoint);

        checkXMLElement(cLogic.inputs);
        config.numDigInputs = cLogic.inputs.count();

        /* Create Control Logic */
        ret = new AnalogBitmaskLogic(globalMutex, database, config);

        /* === Configure Inputs after creation === */
        PointIdStr vPointID;
        GDB_ERROR res;
        lu_uint32_t i = 0;
        for(Iterator<CCLogicInputsT> itPoint = cLogic.inputs.all(); itPoint; ++itPoint, ++i)
        {
            if(itPoint.inputPoint.exists())
            {
                vPointID = getXMLVirtualPointRef(itPoint.inputPoint.first());
            }
            else
            {
                vPointID = PointIdStr();    //Invalidate
            }
            res = (dynamic_cast<AnalogBitmaskLogic*>(ret))->addPoint(vPointID);
            if(res != GDB_ERROR_NONE)
            {
                std::stringstream ss;
                ss << "Failed to add input " << i << "error: " << GDB_ERROR_ToSTRING(res);
                throw ParserException(CONFMGR_ERROR_CONFIG, ss.str().c_str());
            }
        }
    }
    catch (const std::exception& e)
    {
        logConfigError(group, cLogic.logicType, e.what());
    }
    return ret;
}


ControlLogic* XMLControlLogicFactory::parsePowerSupplyConfig(CControlLogicT& cLogic,
                                                                const lu_uint16_t group)
{
    ControlLogic* ret = NULL;
    PowerSupplyLogic::Config config;

    try
    {
        config.groupID = group;
        config.enabled = cLogic.enabled;

        /* === Add input CL virtual points === */
        PointIdStr dInput[POWER_SUPPLY_INPUT_LAST];
        getInputVPoints(cLogic, dInput, ARRAYSIZE(dInput), POWER_SUPPLY_INPUT_isMandatory);
        config.useLocalRemote = (dInput[POWER_SUPPLY_INPUT_OLR].isValid());
        config.inhibitPoint.pointID = dInput[POWER_SUPPLY_INPUT_INHIBIT];

        /* === Add Control Logic points === */

        /* === Get optional attributes === */
        CCLogicParametersT params = getXMLElement<CCLogicParametersT>(cLogic.parameters);
        config.duration_s = getXMLValueByPropertyName<lu_uint32_t>(params.generic,
                                                                    "duration",
                                                                    true,
                                                                    config.duration_s);

        /* === Configure Channels === */
        ChannelRef channelRef = getXMLOutChannelConf(OUTPUT_CHANNEL_TYPE_PSUPPLY, cLogic);
        config.channelRef = channelRef;
        config.channel = retrieveChannel(group, CHANNEL_TYPE_PSUPPLY, channelRef);

        /* Create Control Logic */
        ret = new PowerSupplyLogic(globalMutex, database, config);
    }
    catch (const std::exception& e)
    {
        logConfigError(group, cLogic.logicType, e.what());
    }
    return ret;
}


ControlLogic* XMLControlLogicFactory::parseMotorSupplyConfig(CControlLogicT& cLogic,
                                                                const lu_uint16_t group)
{
    ControlLogic* ret = NULL;
    MotorSupplyLogic::Config config;

    try
    {
        config.groupID = group;
        config.enabled = cLogic.enabled;

        /* === Add input CL virtual points === */
        PointIdStr dInput[MOTOR_SUPPLY_INPUT_LAST];
        getInputVPoints(cLogic, dInput, ARRAYSIZE(dInput), MOTOR_SUPPLY_INPUT_isMandatory);
        config.useLocalRemote = (dInput[MOTOR_SUPPLY_INPUT_OLR].isValid());
        config.inhibitPoint.pointID = dInput[MOTOR_SUPPLY_INPUT_INHIBIT];
        config.controlInput = dInput[MOTOR_SUPPLY_INPUT_CONTROL];

        /* === Add Control Logic points === */

        /* === Get optional attributes === */
        CCLogicParametersT params = getXMLElement<CCLogicParametersT>(cLogic.parameters);
        config.duration_s = getXMLValueByPropertyName<lu_uint32_t>(params.generic,
                                                                    "timeout",
                                                                    true,
                                                                    config.duration_s);

        /* === Get ID of module used for switching === */
        checkXMLElement(cLogic.outputs);
        COutputModuleRefT swRef = getXMLElement<COutputModuleRefT>(cLogic.outputs.first().outputModule);
        config.moduleSwitch.type = getXMLAttrEnum<MODULE>(swRef.moduleType, MODULE_VALUEfromINDEX);
        config.moduleSwitch.id   = getXMLAttrEnum<MODULE_ID>(swRef.moduleID, MODULE_ID_VALUEfromINDEX);
        config.switchID          = getXMLAttr(swRef.switchID);


        /* === Configure Channels === */
        ChannelRef channelRef;
        /* Get PSM Motor Supply channel */
        channelRef.moduleType = MODULE_PSM;
        channelRef.moduleID = MODULE_ID_0;
        channelRef.channelID = PSM_CH_PSUPPLY_MOTOR;
        config.motorChannel = retrieveChannel(group, CHANNEL_TYPE_PSUPPLY, channelRef);

        /* Get Open/Close channels */
        getOpenCloseChannels(group,
                            config.moduleSwitch,
                            config.switchID,
                            config.switchChannel,   //Use the same is OK, we only need either close or open
                            config.switchChannel);
        //Note that the channels must be not NULL at this point, otherwise an exception was thrown

        /* Create Control Logic */
        ret = new MotorSupplyLogic(globalMutex, database, config);
    }
    catch (const std::exception& e)
    {
        logConfigError(group, cLogic.logicType, e.what());
    }
    return ret;
}


void XMLControlLogicFactory::getInputVPoints(CControlLogicT& cLogic,
                                             PointIdStr dInput[],
                                             const lu_uint32_t size,
                                             IsMandatoryFuncPtr isMandatory) throw(ParserException)
{
    checkXMLElement(cLogic.inputs);
    lu_uint16_t group = getXMLAttr(cLogic.group);

    /* Search for all input points */
    lu_uint32_t i = 0;
    for(Iterator<CCLogicInputsT> itPoint=cLogic.inputs.all(); itPoint && (i<size); ++itPoint, ++i)
    {
        try
        {
            dInput[i] = getXMLInputPointIDConf(itPoint);
        }
        catch (ParserException& e)
        {
            //Not valid configuration:
            log.debug("CLogic Input point %s not configured", PointIdStr(group, i).toString().c_str());

            /* abort configuration if any field is mandatory but never found */
            if(isMandatory(i))
            {
                mandatoryInputFailMsg(PointIdStr(group, i));   //this throws a ParserException
            }
        }
    }
    if(i < size)
    {
        /* Check the rest of inputs for mandatory inputs not present in the config */
        for(lu_uint32_t idx = i; (idx < size); ++idx)
        {
            log.debug("CL Input point %s not present!", PointIdStr(group, i).toString().c_str());

            /* abort configuration if any field is mandatory but never found */
            if(isMandatory(i))
            {
                mandatoryInputFailMsg(PointIdStr(group, i));   //this throws a ParserException
            }
        }
    }
}


void XMLControlLogicFactory::mandatoryInputFailMsg(const PointIdStr pointID) throw(ParserException)
{
    //Mandatory inputs invalid/not present
    std::stringstream ss;
    ss << "Mandatory input [" << static_cast<lu_uint32_t>(pointID.group)
       << ", " << static_cast<lu_uint32_t>(pointID.ID)
       << "] not present in configuration";
    throw ParserException(CONFMGR_ERROR_NO_POINT, ss.str().c_str()); //send to the upper exception catch
}


void XMLControlLogicFactory::logConfigError(const lu_uint16_t group, const lu_uint16_t clogicType, const char* text)
{
    log.error("Control Logic %i (%s) configuration error: %s",
              group, CONTROL_LOGIC_TYPE_ToSTRING(clogicType), text
              );
}


void XMLControlLogicFactory::getOpenCloseChannels(const lu_uint16_t group,
                                                  const IOModuleIDStr module,
                                                  const lu_uint32_t moduleSwitch,
                                                  IChannel*& channelOpen,
                                                  IChannel*& channelClose
                                                  ) throw(ParserException)
{
    /* Get Open/Close channels */
    ChannelRef channelRef;
    lu_uint32_t openChannel;
    lu_uint32_t closeChannel;
    channelOpen = NULL;
    channelClose = NULL;

    switch(module.type)
    {
        case MODULE_SCM:
        {
            openChannel = SCM_CH_SWOUT_OPEN_SWITCH;
            closeChannel = SCM_CH_SWOUT_CLOSE_SWITCH;
        }
        break;
#if SCM_MK2_SUPPORT
        case MODULE_SCM_MK2:
        {
            openChannel = SCM_MK2_CH_SWOUT_OPEN_SWITCH;
            closeChannel = SCM_MK2_CH_SWOUT_CLOSE_SWITCH;
        }
        break;
#endif
        case MODULE_DSM:
        {
            switch(moduleSwitch)
            {
                case 0:
                    openChannel = DSM_CH_SWOUT_OPEN_SWITCH_A;
                    closeChannel = DSM_CH_SWOUT_CLOSE_SWITCH_A;
                    break;
                case 1:
                    openChannel = DSM_CH_SWOUT_OPEN_SWITCH_B;
                    closeChannel = DSM_CH_SWOUT_CLOSE_SWITCH_B;
                    break;
                default:
                    //invalid module ID for DSM
                    std::stringstream ss;
                    ss << "Unsupported Switch ID " << (lu_uint32_t)moduleSwitch
                       << " on DSM";
                    throw ParserException(CONFMGR_ERROR_PARAM, ss.str().c_str());
                    break;
            }
        }
        break;
        default:
            //invalid module type
            std::stringstream ss;
            ss << "Module type " << (lu_uint32_t)module.type
               << " not supported";
            throw ParserException(CONFMGR_ERROR_PARAM, ss.str().c_str());
            break;
    }
    channelRef.moduleType = module.type;
    channelRef.moduleID = module.id;
    channelRef.channelID = openChannel;
    channelOpen = retrieveChannel(group, CHANNEL_TYPE_SW_OUT, channelRef);
    channelRef.channelID = closeChannel;
    channelClose = retrieveChannel(group, CHANNEL_TYPE_SW_OUT, channelRef);
    if( (channelOpen == NULL) || (channelClose == NULL) )
    {
        //invalid channel
        std::stringstream ss;
        if(channelOpen == NULL)
        {
            ss << "OPEN";
        }
        else
        {
            ss << "CLOSE";
        }
        ss << " channel not found!";
        throw ParserException(CONFMGR_ERROR_PARAM, ss.str().c_str());
    }
}


IChannel* XMLControlLogicFactory::retrieveChannel(lu_uint16_t group, CHANNEL_TYPE chType, ChannelRef chRef)
{
    IChannel* ret = NULL;
    ret = MManager.getChannel(chRef.moduleType, chRef.moduleID, chType, chRef.channelID);
    if(ret == NULL)
    {
        std::string typeName;
        switch(chType)
        {
            case CHANNEL_TYPE_DINPUT:
                typeName = "DI";
                break;
            case CHANNEL_TYPE_DOUTPUT:
                typeName = "DO";
                break;
            case CHANNEL_TYPE_AINPUT:
                typeName = "AI";
                break;
            case CHANNEL_TYPE_AOUTPUT:
                typeName = "AO";
                break;
            case CHANNEL_TYPE_PSUPPLY:
                typeName = "PS";
                break;
            case CHANNEL_TYPE_BCHARGER:
                typeName = "BC";
                break;
            case CHANNEL_TYPE_FAN:
                typeName = "FN";
                break;
            case CHANNEL_TYPE_HMI:
                typeName = "HM";
                break;
            case CHANNEL_TYPE_SW_OUT:
                typeName = "SO";
                break;
            case CHANNEL_TYPE_AUXSW_OUT:
                typeName = "AU";
                break;
            case CHANNEL_TYPE_FPI:
                typeName = "FP";
                break;
            default:
                lu_char_t ctypeName[20];
                snprintf(ctypeName, 20, "%d", chType);
                typeName = std::string(ctypeName);
                break;
        }
        log.error("%s group %i invalid board/channel %s%i/%s:%i",
                     __AT__, group,
                     MODULE_ToSTRING(chRef.moduleType), chRef.moduleID + 1,
                     typeName.c_str(), chRef.channelID
                  );
    }
    return ret;
}


/*
 *********************** End of file ******************************************
 */
