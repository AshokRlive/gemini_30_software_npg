#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_pstack_ALTOVA_CChannelNameT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_pstack_ALTOVA_CChannelNameT



namespace g3schema
{

namespace ns_pstack
{	

class CChannelNameT : public TypeBase
{
public:
	g3schema_EXPORT CChannelNameT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CChannelNameT(CChannelNameT const& init);
	void operator=(CChannelNameT const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_pstack_altova_CChannelNameT); }
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_pstack

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_pstack_ALTOVA_CChannelNameT
