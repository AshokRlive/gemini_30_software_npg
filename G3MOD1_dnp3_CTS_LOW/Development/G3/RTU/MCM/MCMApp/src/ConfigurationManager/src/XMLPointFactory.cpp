/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       XML based G3 database point factory implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14/09/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "XMLPointFactory.h"
#include "MCMConfigEnum.h"
#include "StandardAnaloguePoint.h"
#include "BinaryDigitalPoint.h"
#include "DoubleBinaryDigitalPoint.h"
#include "StandardCounterPoint.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static Mutex* globalMutex = NULL;

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
XMLPointFactory::XMLPointFactory(GeminiDatabase& g3database,
                                 IIOModuleManager& MManager,
                                 Cg3schema& configuration
                                 ) :
                                     database(g3database),
                                     MManager(MManager) ,
                                     log(Logger::getLogger(SUBSYSTEM_ID_CONFMGR)),
                                     configuration(configuration),
                                     analoguePointNum(0),
                                     binaryPointNum(0)  ,
                                     dbinaryPointNum(0),
                                     counterPointNum(0),
                                     pointNum(0)
{
    initParser();
}


XMLPointFactory::~XMLPointFactory()
{}


void XMLPointFactory::setMutexLock(Mutex& mutex)
{
    if(globalMutex == NULL)
    {
        globalMutex = &mutex;    //we assume it is always the same DB mutex!!
    }
}


lu_uint16_t XMLPointFactory::getPointNumber()
{
    return pointNum;
}


IPoint* XMLPointFactory::newPoint(PointIdStr pointID)
{
    /* Check if the point id is valid */
    if( (pointID.group != 0       ) ||
        (pointID.ID    >= pointNum)
      )
    {
        log.error("XMLPointFactory::newPoint invalid point %i:%i. Max id %i",
                    pointID.group, pointID.ID, pointNum
                  );
        return NULL;
    }

    try
    {
        checkXMLElement(configuration.configuration.first().virtualPoints);
        CVirtualPointsT virtualPoint = configuration.configuration.first().virtualPoints.first();

        if(pointID.ID < analoguePointNum)
        {
            checkXMLElement(virtualPoint.analogues);
            return parseAnalogueConfig(virtualPoint.analogues.first().analogue[pointID.ID], pointID);
        }
        else if( pointID.ID < (analoguePointNum + binaryPointNum) )
        {
            checkXMLElement(virtualPoint.binaries);
            lu_uint32_t pos = pointID.ID - analoguePointNum;
            return parseBinaryConfig(virtualPoint.binaries.first().binary[pos], pointID);
        }
        else if( pointID.ID < (analoguePointNum + binaryPointNum + dbinaryPointNum) )
        {
            checkXMLElement(virtualPoint.doubleBinaries);
            lu_uint32_t pos = pointID.ID - analoguePointNum - binaryPointNum;
            return parseDBinaryConfig(virtualPoint.doubleBinaries.first().doubleBinary[pos], pointID);
        }
        else if( pointID.ID < (analoguePointNum + binaryPointNum + dbinaryPointNum + counterPointNum) )
        {
            checkXMLElement(virtualPoint.counters);
            lu_uint32_t pos = pointID.ID - analoguePointNum - binaryPointNum - dbinaryPointNum;
            return parseCounterConfig(virtualPoint.counters.first().counter[pos], pointID);
        }
        else
        {
            throw ParserException(CONFMGR_ERROR_NO_POINT, "");
        }
    }
    catch (...)
    {
        log.error("%s No Virtual Point %s found in Configuration.", __AT__, pointID.toString().c_str());
    }
    return NULL;


}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

CONFMGR_ERROR XMLPointFactory::initParser()
{
    try
    {
        /* Get a reference to the virtual points DB */
        CConfigurationT configurationRoot = getXMLElement<CConfigurationT>(configuration.configuration);
        CVirtualPointsT virtualPoints = getXMLElement<CVirtualPointsT>(configurationRoot.virtualPoints);

        /* Initialise points number */
        if(virtualPoints.analogues.exists())
        {
            analoguePointNum = virtualPoints.analogues.first().analogue.count();
        }

        if(virtualPoints.binaries.exists())
        {
            binaryPointNum   = virtualPoints.binaries.first().binary.count();
        }

        if(virtualPoints.doubleBinaries.exists())
        {
            dbinaryPointNum  = virtualPoints.doubleBinaries.first().doubleBinary.count();
        }

        if(virtualPoints.counters.exists())
        {
            counterPointNum  = virtualPoints.counters.first().counter.count();
        }

        pointNum = analoguePointNum + binaryPointNum + dbinaryPointNum + counterPointNum;
    }
    catch (ParserException& e)
    {
        log.error("%s No Virtual Points in Configuration: %s.", __AT__, e.what());
        return CONFMGR_ERROR_INITPARSER;
    }
    catch (...)
    {
        log.error("XMLPointFactory::initParser Exception raised");
        return CONFMGR_ERROR_INITPARSER;
    }

    return  CONFMGR_ERROR_NONE;
}


IPoint* XMLPointFactory::parseAnalogueConfig(CAnaloguePointT vPoint, const PointIdStr& pointID)
{
    StandardAnaloguePoint::StdConfig config;
    try
    {
        getXMLAnaloguePointConf(configuration, vPoint, config);

        /* Configure channel */
        checkXMLElement(vPoint.channel);
        ChannelRef channel = getXMLChannelConf(vPoint.channel.first());
        config.channelRef = getChannel(channel, CHANNEL_TYPE_AINPUT);
        if(config.channelRef == NULL)
        {
            log.error("Analogue Virtual Point %i:%i - invalid board/channel %s%i/%s:%i",
                       pointID.group, pointID.ID,
                       MODULE_ToSTRING(channel.moduleType), channel.moduleID + 1,
                       "AI", channel.channelID
                     );
            return NULL;
        }

        return new StandardAnaloguePoint(globalMutex, database, config);
    }
    catch (const std::exception& e)
    {
        log.error("Analogue Virtual Point %s configuration error: %s", pointID.toString().c_str(), e.what());
    }
    return NULL;
}


IPoint* XMLPointFactory::parseBinaryConfig(CBinaryPointT vPoint, const PointIdStr& pointID)
{
    BinaryDigitalPoint::BDConfig config;
    try
    {
        getXMLDigitalPointConf(configuration, vPoint, config);
        config.invert = getXMLAttr(vPoint.invert);
        
        /* Configure channel */
        checkXMLElement(vPoint.channel);
        ChannelRef channel = getXMLChannelConf(vPoint.channel.first());
        config.channelRef = getChannel(channel, CHANNEL_TYPE_DINPUT);
        if(config.channelRef == NULL)
        {
            log.error("Digital Virtual Point %i:%i - invalid board/channel %s%i/%s:%i",
                       pointID.group, pointID.ID,
                       MODULE_ToSTRING(channel.moduleType), channel.moduleID + 1,
                       "DI", channel.channelID
                     );
            return NULL;
        }
        return new BinaryDigitalPoint(globalMutex, database, config);
    }
    catch (const std::exception& e)
    {
        log.error("Single Binary Virtual Point %s configuration error: %s", pointID.toString().c_str(), e.what());
    }
    return NULL;
}


IPoint* XMLPointFactory::parseDBinaryConfig(CDoubleBinaryPointT vPoint, const PointIdStr& pointID)
{
    DoubleBinaryDigitalPoint::DBDConfig config;
    try
    {
        getXMLDigitalPointConf(configuration, vPoint, config);

        /* Double Binary custom value map config */
        try
        {
            CDoubleBinaryValueMapT map = getXMLElement<CDoubleBinaryValueMapT>(vPoint.valueMap);
            config.map[0] = LU_MIN(getXMLAttr(map.value00), 3);
            config.map[1] = LU_MIN(getXMLAttr(map.value01), 3);
            config.map[2] = LU_MIN(getXMLAttr(map.value10), 3);
            config.map[3] = LU_MIN(getXMLAttr(map.value11), 3);
        }
        catch (const std::exception& e)
        {
            //Mapping not configured: revert to default
            config.init();
        }

        /* Get debounce parameter */
        config.debounce = 0;
        if(vPoint.debounce.exists())
        {
            if(vPoint.debounce.first().default2.exists())
            {
                config.debounce = getXMLAttr(vPoint.debounce.first().default2.first().debounce);
            }
        }

        config.invert = LU_FALSE;       //Force to do not invert in this type of point

        /* Configure channels */
        checkXMLElement(vPoint.channel0);
        checkXMLElement(vPoint.channel1);
        CChannelRefT xmlChannel = vPoint.channel0.first();
        for (lu_uint32_t i = 0; i < 2; ++i)
        {
            if(i>0)
            {
                xmlChannel = vPoint.channel1.first();
            }
            ChannelRef channel = getXMLChannelConf(xmlChannel);
            config.channelRef[i] = getChannel(channel, CHANNEL_TYPE_DINPUT);
            if(config.channelRef[i] == NULL)
            {
                log.error("Digital Virtual Point %i:%i - invalid board/channel %s%i/%s:%i",
                           pointID.group, pointID.ID,
                           MODULE_ToSTRING(channel.moduleType), channel.moduleID + 1,
                           "DI", channel.channelID
                         );
                return NULL;
            }
        }

        return new DoubleBinaryDigitalPoint(globalMutex, database, config);
    }
    catch (const std::exception& e)
    {
        log.error("Double Binary Virtual Point %s configuration error: %s", pointID.toString().c_str(), e.what());
    }
    return NULL;
}


IPoint* XMLPointFactory::parseCounterConfig(CCounterPointT vPoint, const PointIdStr& pointID)
{
    StandardCounterPoint::StdConfig config;
    try
    {
        getXMLCounterPointConf(configuration, vPoint, config);

        /* Get input point that acts as source */
        checkXMLElement(vPoint.inputSource);
        config.IDRef = getXMLVirtualPointRef(vPoint.inputSource.first());

        /* Configure Counter input mode (pulse/edge) */
        checkXMLElement(vPoint.inputMode);
        config.eventCfg = getXMLInputDigitalModeConf(vPoint.inputMode.first());

        return new StandardCounterPoint(globalMutex, database, config);
    }
    catch (const std::exception& e)
    {
        log.error("Counter Virtual Point %s configuration error: %s", pointID.toString().c_str(), e.what());
    }
    return NULL;
}


IChannel* XMLPointFactory::getChannel(ChannelRef& channel, const CHANNEL_TYPE type)
{
    IChannel* channelPtr = NULL;

    IIOModule* modulePtr = MManager.getModule(channel.moduleType, channel.moduleID);
    if(modulePtr != NULL)
    {
        /* Valid board found. Get the channel */
        channelPtr = modulePtr->getChannel(type, channel.channelID);
    }
    return channelPtr;
}


/*
 *********************** End of file ******************************************
 */
