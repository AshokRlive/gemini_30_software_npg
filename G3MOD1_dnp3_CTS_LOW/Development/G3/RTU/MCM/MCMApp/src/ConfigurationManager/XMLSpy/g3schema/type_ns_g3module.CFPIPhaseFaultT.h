#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CFPIPhaseFaultT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CFPIPhaseFaultT

#include "type_ns_g3module.CFPIFaultT.h"


namespace g3schema
{

namespace ns_g3module
{	

class CFPIPhaseFaultT : public ::g3schema::ns_g3module::CFPIFaultT
{
public:
	g3schema_EXPORT CFPIPhaseFaultT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CFPIPhaseFaultT(CFPIPhaseFaultT const& init);
	void operator=(CFPIPhaseFaultT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_g3module_altova_CFPIPhaseFaultT); }

	MemberAttribute<unsigned,_altova_mi_ns_g3module_altova_CFPIPhaseFaultT_altova_minOverloadDurationMs, 0, 0> minOverloadDurationMs;	// minOverloadDurationMs CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_g3module_altova_CFPIPhaseFaultT_altova_overloadCurrent, 0, 0> overloadCurrent;	// overloadCurrent CunsignedInt
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_g3module

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_g3module_ALTOVA_CFPIPhaseFaultT
