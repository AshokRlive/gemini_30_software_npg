#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_iec870_ALTOVA_CIEC870ChnlConfT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_iec870_ALTOVA_CIEC870ChnlConfT

#include "type_ns_pstack.CChannelConfT.h"


namespace g3schema
{

namespace ns_iec870
{	

class CIEC870ChnlConfT : public ::g3schema::ns_pstack::CChannelConfT
{
public:
	g3schema_EXPORT CIEC870ChnlConfT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CIEC870ChnlConfT(CIEC870ChnlConfT const& init);
	void operator=(CIEC870ChnlConfT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_iec870_altova_CIEC870ChnlConfT); }

	MemberAttribute<unsigned,_altova_mi_ns_iec870_altova_CIEC870ChnlConfT_altova_incrementalTimeoutMs, 0, 0> incrementalTimeoutMs;	// incrementalTimeoutMs CunsignedInt
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_iec870

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_iec870_ALTOVA_CIEC870ChnlConfT
