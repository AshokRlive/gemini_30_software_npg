#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen11_ALTOVA_CETHERNET_PORT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen11_ALTOVA_CETHERNET_PORT



namespace g3schema
{

namespace ns_gen11
{	

class CETHERNET_PORT : public TypeBase
{
public:
	g3schema_EXPORT CETHERNET_PORT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CETHERNET_PORT(CETHERNET_PORT const& init);
	void operator=(CETHERNET_PORT const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen11_altova_CETHERNET_PORT); }

	enum EnumValues {
		Invalid = -1,
		k_ETHERNET_PORT_CONTROL = 0, // ETHERNET_PORT_CONTROL
		k_ETHERNET_PORT_CONFIG = 1, // ETHERNET_PORT_CONFIG
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen11

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen11_ALTOVA_CETHERNET_PORT
