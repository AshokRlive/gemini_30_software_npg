#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen0_ALTOVA_CSLAVE_IMAGE_TYPE
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen0_ALTOVA_CSLAVE_IMAGE_TYPE



namespace g3schema
{

namespace ns_gen0
{	

class CSLAVE_IMAGE_TYPE : public TypeBase
{
public:
	g3schema_EXPORT CSLAVE_IMAGE_TYPE(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSLAVE_IMAGE_TYPE(CSLAVE_IMAGE_TYPE const& init);
	void operator=(CSLAVE_IMAGE_TYPE const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen0_altova_CSLAVE_IMAGE_TYPE); }

	enum EnumValues {
		Invalid = -1,
		k_SLAVE_IMAGE_TYPE_BOOT_LOADER = 0, // SLAVE_IMAGE_TYPE_BOOT_LOADER
		k_SLAVE_IMAGE_TYPE_APPLICATION = 1, // SLAVE_IMAGE_TYPE_APPLICATION
		k_SLAVE_IMAGE_TYPE_APP_BOOT_PROGRAM = 2, // SLAVE_IMAGE_TYPE_APP_BOOT_PROGRAM
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen0

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen0_ALTOVA_CSLAVE_IMAGE_TYPE
