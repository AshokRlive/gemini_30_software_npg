#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CScadaTemplateTcpT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CScadaTemplateTcpT



namespace g3schema
{

namespace ns_template
{	

class CScadaTemplateTcpT : public TypeBase
{
public:
	g3schema_EXPORT CScadaTemplateTcpT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CScadaTemplateTcpT(CScadaTemplateTcpT const& init);
	void operator=(CScadaTemplateTcpT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_template_altova_CScadaTemplateTcpT); }
	MemberElement<ns_template::CScadaAddressT, _altova_mi_ns_template_altova_CScadaTemplateTcpT_altova_scadaAddress> scadaAddress;
	struct scadaAddress { typedef Iterator<ns_template::CScadaAddressT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_template

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_template_ALTOVA_CScadaTemplateTcpT
