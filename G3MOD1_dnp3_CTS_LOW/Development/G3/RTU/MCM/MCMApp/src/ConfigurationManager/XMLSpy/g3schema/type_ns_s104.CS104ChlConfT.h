#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s104_ALTOVA_CS104ChlConfT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s104_ALTOVA_CS104ChlConfT

#include "type_ns_iec870.CIEC870ChnlConfT.h"


namespace g3schema
{

namespace ns_s104
{	

class CS104ChlConfT : public ::g3schema::ns_iec870::CIEC870ChnlConfT
{
public:
	g3schema_EXPORT CS104ChlConfT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CS104ChlConfT(CS104ChlConfT const& init);
	void operator=(CS104ChlConfT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_s104_altova_CS104ChlConfT); }
	MemberElement<ns_s104::CS104LinkLayerConfT, _altova_mi_ns_s104_altova_CS104ChlConfT_altova_linkLayer> linkLayer;
	struct linkLayer { typedef Iterator<ns_s104::CS104LinkLayerConfT> iterator; };
	MemberElement<ns_s104::CS104LinuxIoT, _altova_mi_ns_s104_altova_CS104ChlConfT_altova_linuxIO> linuxIO;
	struct linuxIO { typedef Iterator<ns_s104::CS104LinuxIoT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_s104

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s104_ALTOVA_CS104ChlConfT
