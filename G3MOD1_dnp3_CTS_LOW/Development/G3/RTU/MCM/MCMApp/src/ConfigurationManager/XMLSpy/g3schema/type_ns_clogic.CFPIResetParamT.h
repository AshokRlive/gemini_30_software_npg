#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CFPIResetParamT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CFPIResetParamT

#include "type_ns_clogic.CCLogicParametersBaseT.h"


namespace g3schema
{

namespace ns_clogic
{	

class CFPIResetParamT : public ::g3schema::ns_clogic::CCLogicParametersBaseT
{
public:
	g3schema_EXPORT CFPIResetParamT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CFPIResetParamT(CFPIResetParamT const& init);
	void operator=(CFPIResetParamT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_clogic_altova_CFPIResetParamT); }

	MemberAttribute<bool,_altova_mi_ns_clogic_altova_CFPIResetParamT_altova_resetFPIOnLVLost, 0, 0> resetFPIOnLVLost;	// resetFPIOnLVLost Cboolean

	MemberAttribute<bool,_altova_mi_ns_clogic_altova_CFPIResetParamT_altova_resetFPIOnLVRestored, 0, 0> resetFPIOnLVRestored;	// resetFPIOnLVRestored Cboolean
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_clogic

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CFPIResetParamT
