#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen2_ALTOVA_CLU_TIME_FORMAT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen2_ALTOVA_CLU_TIME_FORMAT



namespace g3schema
{

namespace ns_gen2
{	

class CLU_TIME_FORMAT : public TypeBase
{
public:
	g3schema_EXPORT CLU_TIME_FORMAT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CLU_TIME_FORMAT(CLU_TIME_FORMAT const& init);
	void operator=(CLU_TIME_FORMAT const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen2_altova_CLU_TIME_FORMAT); }

	enum EnumValues {
		Invalid = -1,
		k_LU_TIME_FORMAT_NONE = 0, // LU_TIME_FORMAT_NONE
		k_LU_TIME_FORMAT_24 = 1, // LU_TIME_FORMAT_24
		k_LU_TIME_FORMAT_56 = 2, // LU_TIME_FORMAT_56
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen2

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen2_ALTOVA_CLU_TIME_FORMAT
