#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3InputPointT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3InputPointT

#include "type_ns_sdnp3.CSDNP3PointT.h"


namespace g3schema
{

namespace ns_sdnp3
{	

class CSDNP3InputPointT : public ::g3schema::ns_sdnp3::CSDNP3PointT
{
public:
	g3schema_EXPORT CSDNP3InputPointT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSDNP3InputPointT(CSDNP3InputPointT const& init);
	void operator=(CSDNP3InputPointT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_sdnp3_altova_CSDNP3InputPointT); }

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CSDNP3InputPointT_altova_defaultVariation, 0, 0> defaultVariation;	// defaultVariation CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CSDNP3InputPointT_altova_eventDefaultVariation, 0, 0> eventDefaultVariation;	// eventDefaultVariation CunsignedInt

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CSDNP3InputPointT_altova_eventClass, 0, 0> eventClass;	// eventClass CunsignedByte

	MemberAttribute<bool,_altova_mi_ns_sdnp3_altova_CSDNP3InputPointT_altova_enableClass0, 0, 0> enableClass0;	// enableClass0 Cboolean

	MemberAttribute<bool,_altova_mi_ns_sdnp3_altova_CSDNP3InputPointT_altova_customVariation, 0, 0> customVariation;	// customVariation Cboolean

	MemberAttribute<bool,_altova_mi_ns_sdnp3_altova_CSDNP3InputPointT_altova_eventOnlyWhenConnected, 0, 0> eventOnlyWhenConnected;	// eventOnlyWhenConnected Cboolean
	MemberElement<ns_common::CVirtualPointRefT, _altova_mi_ns_sdnp3_altova_CSDNP3InputPointT_altova_vpoint> vpoint;
	struct vpoint { typedef Iterator<ns_common::CVirtualPointRefT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_sdnp3

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3InputPointT
