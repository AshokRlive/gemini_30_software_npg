/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:XMLSCADAProtocolParser.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   1 Jul 2014     andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "XMLSCADAProtocolParser.h"

#if IEC_104_SLAVE_SUPPORT
#include "S104Protocol.h"
#include "DummyS104Factory.h"
#endif

#if IEC_101_SLAVE_SUPPORT
#include "S101Protocol.h"
#endif

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define USE_DUMMY_104_CONFIG 0


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

XMLSCADAProtocolParser::XMLSCADAProtocolParser(g3schema::Cg3schema&     configuration,
                                               SCADAProtocolManager&    SCADAManager,
                                               CommsDeviceManager&      commsDeviceManager,
                                               PortManager&             portManager,
                                               ModuleManager&           moduleManager,
                                               GeminiDatabase&          database,
                                               IMCMApplication&         mcmApplication)
                                               : configuration(configuration),
                                                 SCADAManager(SCADAManager),
                                                 portManager(portManager),
                                                 moduleManager(moduleManager),
                                                 commsDeviceManager(commsDeviceManager),
                                                 database(database),
                                                 mcmApplication(mcmApplication),
                                                 log(Logger::getLogger(SUBSYSTEM_ID_CONFMGR))
{
    initParser();
}

XMLSCADAProtocolParser::~XMLSCADAProtocolParser()
{}


CONFMGR_ERROR XMLSCADAProtocolParser::initParser()
{
    try
    {
        CScadaProtocolT SCADAProtocols = configuration.configuration.first().scadaProtocol.first();

        // We only allow a single instance of each SCADA Protocol
        if (SCADAProtocols.sDNP3.exists())
        {
            // Create a DNP3Slave Parser
            XMLDNP3SlaveProtocolParser DNP3Parser(configuration, commsDeviceManager, portManager, moduleManager);

            // Parse the DNP3 Slave XML configuration and create a new DNP3SlaveProtocolManager
            DNP3SlaveProtocol *DNP3SCADAProtocol = DNP3Parser.newProtocolManager(database, mcmApplication, moduleManager);

            if (DNP3SCADAProtocol != NULL)
            {
                // Add the DNP3 Slave to the list of protocol stacks in the SCADAManager
                if (SCADAManager.addProtocolStack(*DNP3SCADAProtocol) != SCADAP_ERROR_NONE)
                {
                    log.error("XMLSCADAProtocolParser: Unable to add DNP3 slave protocol stack");
                    return CONFMGR_ERROR_CONFIG;
                }
            }
        }

#if IEC_104_SLAVE_SUPPORT

    #if USE_DUMMY_104_CONFIG
        S104Protocol* s104= newS104Protocol(database);
        if(SCADAManager.addProtocolStack(*s104) != SCADAP_ERROR_NONE)
            DBG_ERR("Fail to add S104 protocol!");
    #endif

    #if !USE_DUMMY_104_CONFIG

        // We only allow a single instance of each SCADA Protocol
        if (SCADAProtocols.sIEC104.exists())
        {
            // Create a 104Slave Parser
            XMLSlave104ProtocolParser s104Parser(configuration, commsDeviceManager, portManager, moduleManager);

            // Parse the 104 Slave XML configuration and create a new s104ProtocolManager
            S104Protocol *s104protocolManager = s104Parser.newProtocolManager(database);

            if (s104protocolManager != NULL)
            {
                // Add the 104 Slave to the list of protocol stacks in the SCADAManager
                if(SCADAManager.addProtocolStack(*s104protocolManager) != SCADAP_ERROR_NONE)
                {
                    DBG_ERR  ("Fail to add S104 protocol!");
                    log.error("XMLSCADAProtocolParser: Unable to add IEC-104 slave protocol stack");
                    return CONFMGR_ERROR_CONFIG;
                 }
            }

        }

    #endif

#endif

#if IEC_101_SLAVE_SUPPORT
        // We only allow a single instance of each SCADA Protocol
        if (SCADAProtocols.sIEC101.exists())
        {
            // Create a 101Slave Parser
            XML101SlaveProtocolParser s101Parser(configuration, commsDeviceManager, portManager, moduleManager);

            // Parse the 101 Slave XML configuration and create a new s101ProtocolManager
            S101Protocol *s101protocolManager = s101Parser.newProtocolManager(database);

            if (s101protocolManager != NULL)
            {
                // Add the 101 Slave to the list of protocol stacks in the SCADAManager
                if(SCADAManager.addProtocolStack(*s101protocolManager) != SCADAP_ERROR_NONE)
                {
                    DBG_ERR  ("Fail to add S101 protocol!");
                    log.error("XMLSCADAProtocolParser: Unable to add IEC-101 slave protocol stack");
                    return CONFMGR_ERROR_CONFIG;
                 }
            }

        }
#endif

        return CONFMGR_ERROR_NONE;
    }
    catch(...)
    {
        log.error("XMLSCADAProtocolParser: Exception raised");
    }

    return CONFMGR_ERROR_CONFIG;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
