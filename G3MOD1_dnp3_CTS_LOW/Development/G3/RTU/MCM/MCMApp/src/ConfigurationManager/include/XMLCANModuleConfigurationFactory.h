/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       XML CAN channel configuration factory public interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   28/10/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_5CD7AD44_683B_4cea_80EE_96063E5DFF5A__INCLUDED_)
#define EA_5CD7AD44_683B_4cea_80EE_96063E5DFF5A__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "ICANModuleConfigurationFactory.h"
#include "IIOModuleManager.h"
#include "Logger.h"

/* Forward declaration */

class ModuleRegistrationManager;

namespace g3schema
{
    class Cg3schema;

    class CModulesType;
}

using namespace g3schema;

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class XMLCANModuleConfigurationFactory : public ICANModuleConfigurationFactory
{

public:
    XMLCANModuleConfigurationFactory();
    virtual ~XMLCANModuleConfigurationFactory();

    void setConfiguration(Cg3schema *configuration);

    /**
     * \brief Check if the module is configured
     *
     * \param module Module type
     * \param moduleId Module ID
     *
     * \return LU_TRUE if the module is configured
     */
    virtual lu_bool_t isConfigured(MODULE module, MODULE_ID moduleID);

    /**
     * \brief Check if the module is registered
     *
     * Check if the module is in the module registration database
     *
     * \param module Module type
     * \param moduleId Module ID
     * \param moduleUID Module unique ID (serial) number
     *
     * \return LU_TRUE if the module is registered
     */
    virtual lu_bool_t isRegistered( MODULE module,
                                    MODULE_ID moduleID,
                                    ModuleUID moduleUID
                                  );

    /**
     * \brief Force the creation of all the modules present in the configuration
     *
     * Creates the modules even if they are not connected
     *
     * \param moduleManager Reference to the Module Manager
     * \param iFace Interface where the module is supposed to be located.
     *
     * \return result of the operation. LU_TRUE if it was successful
     */
    virtual lu_bool_t setAllModules(IIOModuleManager &moduleManager);

    /**
     * \brief Get input digital channel configuration
     *
     * \param module Module type
     * \param moduleID Module ID
     * \param channeID Channel ID
     * \param conf reference to the buffer where the configuration will be saved
     *
     * \return Error Code
     */
    virtual IOM_ERROR getIDigitalConf( MODULE module,
                                       MODULE_ID moduleID,
                                       lu_uint32_t channeID,
                                       InputDigitalCANChannelConf &conf
                                     );

    /**
     * \brief Get input analogue channel configuration
     *
     * \param module Module type
     * \param moduleID Module ID
     * \param channeID Channel ID
     * \param conf reference to the buffer where the configuration will be saved
     *
     * \return Error Code
     */
    virtual IOM_ERROR getIAnalogueConf( MODULE module,
                                        MODULE_ID moduleID,
                                        lu_uint32_t channeID,
                                        InputAnalogueCANChannelConf &conf
                                      );

    /**
     * \brief Get switch out channel configuration
     *
     * \param module Module type
     * \param moduleID Module ID
     * \param channeID Channel ID
     * \param conf reference to the buffer where the configuration will be saved
     *
     * \return Error Code
     */
    virtual IOM_ERROR getSwitchOutConf( MODULE module,
                                        MODULE_ID moduleID,
                                        lu_uint32_t channeID,
                                        SwitchCANChannelConf &conf
                                       );

    /**
     * \brief Get DSM-specific switch out channel configuration
     *
     * \param module Module type
     * \param moduleID Module ID
     * \param channeID Channel ID
     * \param conf reference to the buffer where the configuration will be saved
     *
     * \return Error Code
     */
    virtual IOM_ERROR getDSMSwitchOutConf( MODULE module,
                                           MODULE_ID moduleID,
                                           lu_uint32_t channeID,
                                           SwitchCANChannelDSM::SwitchCANChannelDSMConf &conf
                                          );

    /**
     * \brief Get DSM-specific switch out channel configuration
     *
     * \param module Module type
     * \param moduleID Module ID
     * \param channeID Channel ID
     * \param conf reference to the buffer where the configuration will be saved
     *
     * \return Error Code
     */
    virtual IOM_ERROR getSCM2SwitchOutConf( MODULE module,
                                           MODULE_ID moduleID,
                                           lu_uint32_t channeID,
                                           SwitchCANChannelSCM2::SwitchConf &conf
                                          );

    /**
     * \brief Get FPI Output channel configuration
     *
     * \param module Module type
     * \param moduleID Module ID
     * \param channeID Channel ID
     * \param conf reference to the buffer where the configuration will be saved
     *
     * \return Error Code
     */
    virtual IOM_ERROR getFPMConf(MODULE module,
                                    MODULE_ID moduleID,
                                    lu_uint32_t channeID,
                                    FPMConfigStr& conf
                                    );

    /**
     * \brief Get HMIt channel configuration
     *
     * \param module Module type
     * \param moduleID Module ID
     * \param channeID Channel ID
     * \param conf reference to the buffer where the configuration will be saved
     *
     * \return Error Code
     */
    virtual IOM_ERROR getHMIConf( MODULE module,
                                  MODULE_ID moduleID,
                                  lu_uint32_t channeID,
                                  HMIConfigStr &conf
                                 );

    /**
     * \brief Get Digital Output channel configuration
     *
     * \param module Module type
     * \param moduleID Module ID
     * \param channeID Channel ID
     * \param conf reference to the buffer where the configuration will be saved
     *
     * \return Error Code
     */
    virtual IOM_ERROR getDigitalOutConf( MODULE module,
                                           MODULE_ID moduleID,
                                           lu_uint32_t channeID,
                                           OutputDigitalCANChannelConf &conf
                                         );


private:

    /**
     * \brief Check if a module is present in the configuration file
     *
     * If the module is not present an error is returned.
     *
     * \param it Module iterator
     * \param moduleId ID of the module we are searching
     *
     * \return Error Code
     */
    template<typename ModuleType> IOM_ERROR getModule( ModuleType it,
                                                       MODULE_ID moduleID
                                                      );

    /**
     * \brief Get module-only configuration
     *
     * Gets module-wide specific configuration (per-module configuration fields).
     * NOTE: It is guaranteed that conf is never pointing to NULL.
     *
     * \param module Module type
     * \param moduleID Module ID
     * \param conf Where to store module-specific configuration
     *
     * \return Error Code
     */
    IOM_ERROR getModuleConfig(MODULE module, MODULE_ID moduleID, void* conf);

    /**
     * \brief Get the configuration for a digital input channel
     *
     * Search the boards/channels in the configuration file. If the channel
     * is configured the configuration is parsed otherwise an error is returned
     *
     * \param it Module iterator
     * \param moduleId ID of the module we are searching
     * \param channelID ID of the channel we are searching
     * \param conf Reference to the buffer where the configuration is saved
     *
     * \return Error Code
     */
    template<typename ModuleType> IOM_ERROR getDigitalChConf( ModuleType it,
                                                              MODULE_ID moduleID,
                                                              lu_uint32_t channelID,
                                                              InputDigitalCANChannelConf &conf
                                                             );

    /**
     * \brief Get the configuration for an analogue input channel
     *
     * Search the boards/channels in the configuration file. If the channel
     * is configured the configuration is parsed otherwise an error is returned
     *
     * \param it Module iterator
     * \param moduleId ID of the module we are searching
     * \param channelID ID of the channel we are searching
     * \param conf Reference to the buffer where the configuration is saved
     *
     * \return Error Code
     */
    template<typename ModuleType> IOM_ERROR getAnalogueChConf( ModuleType it,
                                                               MODULE_ID moduleID,
                                                               lu_uint32_t channelID,
                                                               InputAnalogueCANChannelConf &conf
                                                             );

    /**
     * \brief Get the configuration for a switch out channel
     *
     * Search the boards/channels in the configuration file. If the channel
     * is configured the configuration is parsed otherwise an error is returned
     *
     * \param it Module iterator
     * \param moduleId ID of the module we are searching
     * \param channelID ID of the channel we are searching
     * \param conf Reference to the buffer where the configuration is saved
     *
     * \return Error Code
     */
    template<typename ModuleType> IOM_ERROR getSwitchChConf( ModuleType it,
                                                             MODULE_ID moduleID,
                                                             lu_uint32_t channelID,
                                                             SwitchCANChannelConf &conf
                                                           );

    /**
     * \brief Get the configuration for a DSM-specific switch out channel
     *
     * Search the boards/channels in the configuration file. If the channel
     * is configured the configuration is parsed otherwise an error is returned
     *
     * \param it Module iterator
     * \param moduleType Module Type
     * \param moduleId ID of the module we are searching
     * \param channelID ID of the channel we are searching
     * \param conf Reference to the buffer where the configuration is saved
     *
     * \return Error Code
     */
    template<typename ModuleType> IOM_ERROR getDSMSwitchChConf( ModuleType it,
                                                                MODULE moduleType,
                                                                MODULE_ID moduleID,
                                                                lu_uint32_t channelID,
                                                                SwitchCANChannelDSM::SwitchCANChannelDSMConf& conf
                                                              );

    /**
     * \brief Get the configuration for a SCM_MK2-specific switch out channel
     *
     * Search the boards/channels in the configuration file. If the channel
     * is configured the configuration is parsed otherwise an error is returned
     *
     * \param it Module iterator
     * \param moduleType Module Type
     * \param moduleId ID of the module we are searching
     * \param channelID ID of the channel we are searching
     * \param conf Reference to the buffer where the configuration is saved
     *
     * \return Error Code
     */
    template<typename ModuleType> IOM_ERROR getSCM2SwitchChConf(ModuleType it,
                                                                MODULE moduleType,
                                                                MODULE_ID moduleID,
                                                                lu_uint32_t channelID,
                                                                SwitchCANChannelSCM2::SwitchConf& conf
                                                              );

    /**
     * \brief Get the configuration for a digital out channel
     *
     * Search the boards/channels in the configuration file. If the channel
     * is configured the configuration is parsed otherwise an error is returned
     *
     * \param it Module iterator
     * \param moduleId ID of the module we are searching
     * \param channelID ID of the channel we are searching
     * \param conf Reference to the buffer where the configuration is saved
     *
     * \return Error Code
     */
    template<typename ModuleType> IOM_ERROR getDigitalOutChConf( ModuleType it,
                                                                 MODULE_ID moduleID,
                                                                 lu_uint32_t channelID,
                                                                 OutputDigitalCANChannelConf& conf
                                                               );

    /**
     * \brief Get the configuration for an FPI out channel
     *
     * Search the boards/channels in the configuration file. If the channel
     * is configured the configuration is parsed otherwise an error is returned
     *
     * \param it Module iterator
     * \param moduleId ID of the module we are searching
     * \param channelID ID of the channel we are searching
     * \param conf Reference to the buffer where the configuration is saved
     *
     * \return Error Code
     */
    template<typename ModuleType> IOM_ERROR getFPMConf( ModuleType it,
                                                             MODULE_ID moduleID,
                                                             lu_uint32_t channelID,
                                                             FPMConfigStr& conf
                                                           );

    /**
     * \brief Get the configuration for a HMI channel
     *
     * Search the boards/channels in the configuration file. If the channel
     * is configured the configuration is parsed otherwise an error is returned
     *
     * \param it Module iterator
     * \param moduleId ID of the module we are searching
     * \param channelID ID of the channel we are searching
     * \param conf Reference to the buffer where the configuration is saved
     *
     * \return Error Code
     */
    template<typename ModuleType> IOM_ERROR getHmiChConf( ModuleType it,
                                                          MODULE_ID moduleID,
                                                          lu_uint32_t channelID,
                                                          HMIConfigStr &conf
                                                        );

    /**
     * \brief Resets the counters for all the module types
     */
    void countReset();


    /**
     * \brief Checks if a channel is reserved
     *
     * \ return LU_TRUE when the channel is a reserved one
     */
    lu_bool_t checkReserved(MODULE moduleType,
                            MODULE_ID moduleID,
                            CHANNEL_TYPE channelType,
                            lu_uint32_t channelID);

private:
    Logger& log;
    Cg3schema *configuration;
    lu_uint8_t PSMCount;
    lu_uint8_t SCMCount;
    lu_uint8_t DSMCount;
    lu_uint8_t FDMCount;
    lu_uint8_t FPMCount;
    lu_uint8_t HMICount;
    lu_uint8_t IOMCount;
    lu_uint8_t SCMMK2Count;
};

#endif // !defined(EA_5CD7AD44_683B_4cea_80EE_96063E5DFF5A__INCLUDED_)


/*
 *********************** End of file ******************************************
 */
