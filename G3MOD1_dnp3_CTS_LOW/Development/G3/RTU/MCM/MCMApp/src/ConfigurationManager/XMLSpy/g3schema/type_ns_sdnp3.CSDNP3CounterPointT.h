#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3CounterPointT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3CounterPointT

#include "type_ns_sdnp3.CSDNP3InputPointT.h"


namespace g3schema
{

namespace ns_sdnp3
{	

class CSDNP3CounterPointT : public ::g3schema::ns_sdnp3::CSDNP3InputPointT
{
public:
	g3schema_EXPORT CSDNP3CounterPointT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSDNP3CounterPointT(CSDNP3CounterPointT const& init);
	void operator=(CSDNP3CounterPointT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_sdnp3_altova_CSDNP3CounterPointT); }

	MemberAttribute<bool,_altova_mi_ns_sdnp3_altova_CSDNP3CounterPointT_altova_useRolloverFlag, 0, 0> useRolloverFlag;	// useRolloverFlag Cboolean

	MemberAttribute<bool,_altova_mi_ns_sdnp3_altova_CSDNP3CounterPointT_altova_frozenFlag, 0, 0> frozenFlag;	// frozenFlag Cboolean
	MemberElement<ns_sdnp3::CFrozenCounterT, _altova_mi_ns_sdnp3_altova_CSDNP3CounterPointT_altova_frozenCounter> frozenCounter;
	struct frozenCounter { typedef Iterator<ns_sdnp3::CFrozenCounterT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_sdnp3

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3CounterPointT
