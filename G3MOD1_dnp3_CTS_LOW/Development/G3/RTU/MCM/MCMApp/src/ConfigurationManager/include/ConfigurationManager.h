/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Configuration Manager interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/09/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_24E7C8AC_6B92_4f0b_B34D_76BE509D7164__INCLUDED_)
#define EA_24E7C8AC_6B92_4f0b_B34D_76BE509D7164__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <iostream>
#include <string>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "ConfigurationManagerError.h"
#include "XMLCANModuleConfigurationFactory.h"
#include "HMIManager.h"
#include "IProcess.h"

#include "IMemoryManager.h"
#include "IMCMApplication.h"
#include "IStatusManager.h"
#include "UserManager.h"
#include "IIOModuleManager.h"
#if MODBUS_MASTER_SUPPORT
#include "MBDeviceManager.h"
#endif
class IIOModuleManager;
class GeminiDatabase;
class ModuleManager;
class PortManager;
class CommsDeviceManager;
class SCADAProtocolManager;
class ICANChannelConfigurationFactory;
namespace g3schema
{
    class Cg3schema;
}

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class ConfigurationManager
{
public:
    ConfigurationManager();
    virtual ~ConfigurationManager();

    /**
     * \brief Initialise the parser
     *
     * This function must be called before configuring the system
     *
     * \param configName Pointer to the configuration file name
     *
     * \return Error code
     */
    CONFMGR_ERROR initParser(const lu_char_t* configName);

    /**
     * \brief Initialise the slave modules
     *
     * This function must be called before configuring the modules
     */
    void initModules();

    /**
     * \brief Configure the misc data
     *
     * \param database Reference to the gemini database to configure
     *
     * \return Error Code
     */
    CONFMGR_ERROR init( IStatusManager &statusManager,
                        IIOModuleManager::OLRButtonConfig& cfgOLR
                        );

    /**
     * \brief Configure the gemini database
     *
     * \param database Reference to the gemini database to configure
     *
     * \return Error Code
     */
    CONFMGR_ERROR init( IStatusManager &statusManager,
                        IMemoryManager &memoryManager,
                        IIOModuleManager &MManager,
                        GeminiDatabase &database,
                        UserManager &userManager
                        );

    /**
     * \brief Configure the gemini HMI
     *
     * \param HMI reference to the system to initialise
     * \param database Reference to the gemini database to configure
     *
     * \return Error Code
     */
    CONFMGR_ERROR init( HMIManager &HMI,
                        GeminiDatabase &database,
                        IIOModuleManager& moduleMgr,
                        IStatusManager& statusMgr
                        );

    /**
     * \brief Configure Port Objects
     *
     * \param portManager Reference to the portManager to initialise
     *
     * \return Error Code
     */
    CONFMGR_ERROR init( PortManager&          portManager
                       );

    /**
     * \brief Configure CommDevice Objects
     *
     * \param commDeviceManager Reference to the CommDeviceManager to initialise
     * \param portManager       Reference to the Port manager (used to get port settings)
     * \param moduleManager     Reference to the Module manager (used to get PSM Comms1 Output Chan)
     *
     * \return Error Code
     */
    CONFMGR_ERROR init( CommsDeviceManager& commsDeviceManager,
                        PortManager&        portManager,
                        IIOModuleManager&   moduleManager
                        );

    /**
     * \brief Configure Protocol stack
     *
     * \param PManager Reference to the protocol stack manager to initialise
     * \param commDeviceManager Reference to the Comm Device Manager (used to get modem Settings)
     * \param portManager Reference to the Port manager (used to get port settings)
     * \param moduleManager Reference to the Module manager (used to get channels)
     * \param database Reference to the gemini database
     * \param mcmApplication Reference to the mcmAppliction
     *
     * \return Error Code
     */
    CONFMGR_ERROR init( SCADAProtocolManager& PManager,
                        CommsDeviceManager&   commsDeviceManager,
                        PortManager&          portManager,
                        ModuleManager&        moduleManager,
                        GeminiDatabase&       database,
                        IMCMApplication&      mcmApplication
                       );

    /**
     * \brief Force the creation of all the modules present in the configuration
     *
     * Creates the modules even if they are not connected
     *
     * \param moduleManager Module Manager
     *
     * \return error code
     */
    CONFMGR_ERROR createAllModules(IIOModuleManager& moduleManager);

#if MODBUS_MASTER_SUPPORT
    CONFMGR_ERROR createAllMBDevice(mbm::MBDeviceManager& mbdeviceManager, PortManager& portManager);
#endif
    /**
     * \brief Get CAN channel configuration factory
     *
     * \return Reference to CAN channel configuration factory
     */
    ICANModuleConfigurationFactory& getCANChannelFactory() {return moduleFactory;};

    /**
     * \brief Release the parser resources
     *
     * This method is automatically called when the class is destroyed
     *
     * \return Error code
     */
    CONFMGR_ERROR releaseParser();

private:
    /**
     * \brief Read configuration directly from a GZipped file
     *
     * \param configName Name of the GZip file that contains the configuration file
     * \param configString Where to store the configuration once it is read from file
     *
     * \return Error code
     */
    CONFMGR_ERROR readConfigGzFile(const lu_char_t* configName, std::string &configString);

private:
    g3schema::Cg3schema* configuration;
    XMLCANModuleConfigurationFactory moduleFactory;
    Logger& log;
};


#endif // !defined(EA_24E7C8AC_6B92_4f0b_B34D_76BE509D7164__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
