#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen0_ALTOVA_CLED_COLOUR
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen0_ALTOVA_CLED_COLOUR



namespace g3schema
{

namespace ns_gen0
{	

class CLED_COLOUR : public TypeBase
{
public:
	g3schema_EXPORT CLED_COLOUR(xercesc::DOMNode* const& init);
	g3schema_EXPORT CLED_COLOUR(CLED_COLOUR const& init);
	void operator=(CLED_COLOUR const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen0_altova_CLED_COLOUR); }

	enum EnumValues {
		Invalid = -1,
		k_LED_COLOUR_RED = 0, // LED_COLOUR_RED
		k_LED_COLOUR_GREEN = 1, // LED_COLOUR_GREEN
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen0

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen0_ALTOVA_CLED_COLOUR
