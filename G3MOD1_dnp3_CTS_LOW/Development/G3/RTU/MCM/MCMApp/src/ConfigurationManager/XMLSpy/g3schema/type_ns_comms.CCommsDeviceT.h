#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_comms_ALTOVA_CCommsDeviceT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_comms_ALTOVA_CCommsDeviceT



namespace g3schema
{

namespace ns_comms
{	

class CCommsDeviceT : public TypeBase
{
public:
	g3schema_EXPORT CCommsDeviceT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CCommsDeviceT(CCommsDeviceT const& init);
	void operator=(CCommsDeviceT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_comms_altova_CCommsDeviceT); }

	MemberAttribute<string_type,_altova_mi_ns_comms_altova_CCommsDeviceT_altova_name, 0, 0> name;	// name Cstring

	MemberAttribute<unsigned,_altova_mi_ns_comms_altova_CCommsDeviceT_altova_id, 0, 0> id;	// id CunsignedInt

	MemberAttribute<bool,_altova_mi_ns_comms_altova_CCommsDeviceT_altova_disabled, 0, 0> disabled;	// disabled Cboolean
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_comms

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_comms_ALTOVA_CCommsDeviceT
