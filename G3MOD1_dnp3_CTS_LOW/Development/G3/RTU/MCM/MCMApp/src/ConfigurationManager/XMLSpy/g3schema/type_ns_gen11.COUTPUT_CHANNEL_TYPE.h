#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen11_ALTOVA_COUTPUT_CHANNEL_TYPE
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen11_ALTOVA_COUTPUT_CHANNEL_TYPE



namespace g3schema
{

namespace ns_gen11
{	

class COUTPUT_CHANNEL_TYPE : public TypeBase
{
public:
	g3schema_EXPORT COUTPUT_CHANNEL_TYPE(xercesc::DOMNode* const& init);
	g3schema_EXPORT COUTPUT_CHANNEL_TYPE(COUTPUT_CHANNEL_TYPE const& init);
	void operator=(COUTPUT_CHANNEL_TYPE const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen11_altova_COUTPUT_CHANNEL_TYPE); }

	enum EnumValues {
		Invalid = -1,
		k_OUTPUT_CHANNEL_TYPE_DOUTPUT__ = 0, // OUTPUT_CHANNEL_TYPE_DOUTPUT  
		k_OUTPUT_CHANNEL_TYPE_AOUTPUT__ = 1, // OUTPUT_CHANNEL_TYPE_AOUTPUT  
		k_OUTPUT_CHANNEL_TYPE_PSUPPLY__ = 2, // OUTPUT_CHANNEL_TYPE_PSUPPLY  
		k_OUTPUT_CHANNEL_TYPE_BCHARGER_ = 3, // OUTPUT_CHANNEL_TYPE_BCHARGER 
		k_OUTPUT_CHANNEL_TYPE_FPI______ = 4, // OUTPUT_CHANNEL_TYPE_FPI      
		k_OUTPUT_CHANNEL_TYPE_FAN______ = 5, // OUTPUT_CHANNEL_TYPE_FAN      
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen11

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen11_ALTOVA_COUTPUT_CHANNEL_TYPE
