#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3ChnlT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3ChnlT



namespace g3schema
{

namespace ns_sdnp3
{	

class CSDNP3ChnlT : public TypeBase
{
public:
	g3schema_EXPORT CSDNP3ChnlT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CSDNP3ChnlT(CSDNP3ChnlT const& init);
	void operator=(CSDNP3ChnlT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_sdnp3_altova_CSDNP3ChnlT); }
	MemberElement<ns_sdnp3::CSDNP3ChnlConfT, _altova_mi_ns_sdnp3_altova_CSDNP3ChnlT_altova_config> config;
	struct config { typedef Iterator<ns_sdnp3::CSDNP3ChnlConfT> iterator; };
	MemberElement<ns_sdnp3::CSDNP3SesnT, _altova_mi_ns_sdnp3_altova_CSDNP3ChnlT_altova_session> session;
	struct session { typedef Iterator<ns_sdnp3::CSDNP3SesnT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_sdnp3

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CSDNP3ChnlT
