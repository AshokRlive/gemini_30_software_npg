#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_mmb_ALTOVA_CMMBIOMapT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_mmb_ALTOVA_CMMBIOMapT



namespace g3schema
{

namespace ns_mmb
{	

class CMMBIOMapT : public TypeBase
{
public:
	g3schema_EXPORT CMMBIOMapT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CMMBIOMapT(CMMBIOMapT const& init);
	void operator=(CMMBIOMapT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_mmb_altova_CMMBIOMapT); }
	MemberElement<ns_mmb::CMMBDeviceChnlAnalogT, _altova_mi_ns_mmb_altova_CMMBIOMapT_altova_AIChs> AIChs;
	struct AIChs { typedef Iterator<ns_mmb::CMMBDeviceChnlAnalogT> iterator; };
	MemberElement<ns_mmb::CMMBDeviceChnlT, _altova_mi_ns_mmb_altova_CMMBIOMapT_altova_DIChs> DIChs;
	struct DIChs { typedef Iterator<ns_mmb::CMMBDeviceChnlT> iterator; };
	MemberElement<ns_mmb::CMMBDeviceChnlT, _altova_mi_ns_mmb_altova_CMMBIOMapT_altova_DOChs> DOChs;
	struct DOChs { typedef Iterator<ns_mmb::CMMBDeviceChnlT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_mmb

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_mmb_ALTOVA_CMMBIOMapT
