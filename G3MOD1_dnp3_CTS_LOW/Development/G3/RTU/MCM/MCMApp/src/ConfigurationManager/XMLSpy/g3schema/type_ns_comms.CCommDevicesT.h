#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_comms_ALTOVA_CCommDevicesT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_comms_ALTOVA_CCommDevicesT



namespace g3schema
{

namespace ns_comms
{	

class CCommDevicesT : public TypeBase
{
public:
	g3schema_EXPORT CCommDevicesT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CCommDevicesT(CCommDevicesT const& init);
	void operator=(CCommDevicesT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_comms_altova_CCommDevicesT); }
	MemberElement<ns_comms::CGPRSModemT, _altova_mi_ns_comms_altova_CCommDevicesT_altova_GPRSModem> GPRSModem;
	struct GPRSModem { typedef Iterator<ns_comms::CGPRSModemT> iterator; };
	MemberElement<ns_comms::CPSTNModemT, _altova_mi_ns_comms_altova_CCommDevicesT_altova_PSTNModem> PSTNModem;
	struct PSTNModem { typedef Iterator<ns_comms::CPSTNModemT> iterator; };
	MemberElement<ns_comms::CPAKNETModemT, _altova_mi_ns_comms_altova_CCommDevicesT_altova_PAKNETModem> PAKNETModem;
	struct PAKNETModem { typedef Iterator<ns_comms::CPAKNETModemT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_comms

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_comms_ALTOVA_CCommDevicesT
