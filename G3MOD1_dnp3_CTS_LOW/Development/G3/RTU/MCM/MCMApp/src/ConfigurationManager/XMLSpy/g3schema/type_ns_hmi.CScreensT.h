#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CScreensT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CScreensT



namespace g3schema
{

namespace ns_hmi
{	

class CScreensT : public TypeBase
{
public:
	g3schema_EXPORT CScreensT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CScreensT(CScreensT const& init);
	void operator=(CScreensT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_hmi_altova_CScreensT); }
	MemberElement<ns_hmi::CRootScreenT, _altova_mi_ns_hmi_altova_CScreensT_altova_rootScreen> rootScreen;
	struct rootScreen { typedef Iterator<ns_hmi::CRootScreenT> iterator; };
	MemberElement<ns_hmi::CHMIMenuScreenT, _altova_mi_ns_hmi_altova_CScreensT_altova_menuScreens> menuScreens;
	struct menuScreens { typedef Iterator<ns_hmi::CHMIMenuScreenT> iterator; };
	MemberElement<ns_hmi::CHMIControlScreenT, _altova_mi_ns_hmi_altova_CScreensT_altova_controlScreens> controlScreens;
	struct controlScreens { typedef Iterator<ns_hmi::CHMIControlScreenT> iterator; };
	MemberElement<ns_hmi::CHMIPhaseScreenT, _altova_mi_ns_hmi_altova_CScreensT_altova_phaseScreens> phaseScreens;
	struct phaseScreens { typedef Iterator<ns_hmi::CHMIPhaseScreenT> iterator; };
	MemberElement<ns_hmi::CHMIDataScreenT, _altova_mi_ns_hmi_altova_CScreensT_altova_dataScreens> dataScreens;
	struct dataScreens { typedef Iterator<ns_hmi::CHMIDataScreenT> iterator; };
	MemberElement<ns_hmi::CHMIVPointDataEntryT, _altova_mi_ns_hmi_altova_CScreensT_altova_dataEntries> dataEntries;
	struct dataEntries { typedef Iterator<ns_hmi::CHMIVPointDataEntryT> iterator; };
	MemberElement<ns_hmi::CHMIInfoScreenT, _altova_mi_ns_hmi_altova_CScreensT_altova_infoScreens> infoScreens;
	struct infoScreens { typedef Iterator<ns_hmi::CHMIInfoScreenT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_hmi

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_hmi_ALTOVA_CScreensT
