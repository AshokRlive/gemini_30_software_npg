#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen1_ALTOVA_CUNSOLICITED_RETRY_MODE
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen1_ALTOVA_CUNSOLICITED_RETRY_MODE



namespace g3schema
{

namespace ns_gen1
{	

class CUNSOLICITED_RETRY_MODE : public TypeBase
{
public:
	g3schema_EXPORT CUNSOLICITED_RETRY_MODE(xercesc::DOMNode* const& init);
	g3schema_EXPORT CUNSOLICITED_RETRY_MODE(CUNSOLICITED_RETRY_MODE const& init);
	void operator=(CUNSOLICITED_RETRY_MODE const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_ns_gen1_altova_CUNSOLICITED_RETRY_MODE); }

	enum EnumValues {
		Invalid = -1,
		k_UNSOLICITED_RETRY_MODE_NO_RETRY = 0, // UNSOLICITED_RETRY_MODE_NO_RETRY
		k_UNSOLICITED_RETRY_MODE_SIMPLE_RETRY = 1, // UNSOLICITED_RETRY_MODE_SIMPLE_RETRY
		k_UNSOLICITED_RETRY_MODE_WITH_OFFLINE = 2, // UNSOLICITED_RETRY_MODE_WITH_OFFLINE
		k_UNSOLICITED_RETRY_MODE_UNLIMITED = 3, // UNSOLICITED_RETRY_MODE_UNLIMITED
		EnumValueCount
	};
	void operator= (const string_type& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::AnySimpleTypeFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator string_type()
	{
		return CastAs<string_type >::Do(GetNode(), 0);
	}
};



} // namespace ns_gen1

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_gen1_ALTOVA_CUNSOLICITED_RETRY_MODE
