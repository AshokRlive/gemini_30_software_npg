#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_COutputModuleRefT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_COutputModuleRefT

#include "type_ns_common.CModuleRefT.h"


namespace g3schema
{

namespace ns_common
{	

class COutputModuleRefT : public ::g3schema::ns_common::CModuleRefT
{
public:
	g3schema_EXPORT COutputModuleRefT(xercesc::DOMNode* const& init);
	g3schema_EXPORT COutputModuleRefT(COutputModuleRefT const& init);
	void operator=(COutputModuleRefT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_common_altova_COutputModuleRefT); }

	MemberAttribute<unsigned,_altova_mi_ns_common_altova_COutputModuleRefT_altova_switchID, 0, 0> switchID;	// switchID CunsignedByte
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_common

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_common_ALTOVA_COutputModuleRefT
