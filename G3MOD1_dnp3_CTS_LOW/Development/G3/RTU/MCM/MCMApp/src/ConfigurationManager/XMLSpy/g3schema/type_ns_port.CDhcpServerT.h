#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_port_ALTOVA_CDhcpServerT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_port_ALTOVA_CDhcpServerT



namespace g3schema
{

namespace ns_port
{	

class CDhcpServerT : public TypeBase
{
public:
	g3schema_EXPORT CDhcpServerT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CDhcpServerT(CDhcpServerT const& init);
	void operator=(CDhcpServerT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_port_altova_CDhcpServerT); }

	MemberAttribute<string_type,_altova_mi_ns_port_altova_CDhcpServerT_altova_startIP, 0, 0> startIP;	// startIP CanySimpleType

	MemberAttribute<string_type,_altova_mi_ns_port_altova_CDhcpServerT_altova_endIP, 0, 0> endIP;	// endIP CanySimpleType

	MemberAttribute<string_type,_altova_mi_ns_port_altova_CDhcpServerT_altova_submask, 0, 0> submask;	// submask CanySimpleType

	MemberAttribute<string_type,_altova_mi_ns_port_altova_CDhcpServerT_altova_router, 0, 0> router;	// router CanySimpleType
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_port

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_port_ALTOVA_CDhcpServerT
