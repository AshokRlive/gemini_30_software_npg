#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s104_ALTOVA_CS104SessionT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s104_ALTOVA_CS104SessionT



namespace g3schema
{

namespace ns_s104
{	

class CS104SessionT : public TypeBase
{
public:
	g3schema_EXPORT CS104SessionT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CS104SessionT(CS104SessionT const& init);
	void operator=(CS104SessionT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_s104_altova_CS104SessionT); }
	MemberElement<ns_s104::CS104SesnConfT, _altova_mi_ns_s104_altova_CS104SessionT_altova_config> config;
	struct config { typedef Iterator<ns_s104::CS104SesnConfT> iterator; };
	MemberElement<ns_iec870::CIEC870PointsT, _altova_mi_ns_s104_altova_CS104SessionT_altova_iomap> iomap;
	struct iomap { typedef Iterator<ns_iec870::CIEC870PointsT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_s104

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_s104_ALTOVA_CS104SessionT
