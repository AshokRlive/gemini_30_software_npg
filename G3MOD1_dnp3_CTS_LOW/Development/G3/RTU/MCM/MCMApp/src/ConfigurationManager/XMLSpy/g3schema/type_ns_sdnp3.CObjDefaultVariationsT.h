#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CObjDefaultVariationsT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CObjDefaultVariationsT



namespace g3schema
{

namespace ns_sdnp3
{	

class CObjDefaultVariationsT : public TypeBase
{
public:
	g3schema_EXPORT CObjDefaultVariationsT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CObjDefaultVariationsT(CObjDefaultVariationsT const& init);
	void operator=(CObjDefaultVariationsT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_sdnp3_altova_CObjDefaultVariationsT); }

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CObjDefaultVariationsT_altova_obj01, 0, 0> obj01;	// obj01 CunsignedByte

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CObjDefaultVariationsT_altova_obj02, 0, 0> obj02;	// obj02 CunsignedByte

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CObjDefaultVariationsT_altova_obj03, 0, 0> obj03;	// obj03 CunsignedByte

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CObjDefaultVariationsT_altova_obj04, 0, 0> obj04;	// obj04 CunsignedByte

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CObjDefaultVariationsT_altova_obj30, 0, 0> obj30;	// obj30 CunsignedByte

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CObjDefaultVariationsT_altova_obj32, 0, 0> obj32;	// obj32 CunsignedByte

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CObjDefaultVariationsT_altova_obj20, 0, 0> obj20;	// obj20 CunsignedByte

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CObjDefaultVariationsT_altova_obj21, 0, 0> obj21;	// obj21 CunsignedByte

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CObjDefaultVariationsT_altova_obj22, 0, 0> obj22;	// obj22 CunsignedByte

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CObjDefaultVariationsT_altova_obj23, 0, 0> obj23;	// obj23 CunsignedByte

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CObjDefaultVariationsT_altova_obj40, 0, 0> obj40;	// obj40 CunsignedByte

	MemberAttribute<unsigned,_altova_mi_ns_sdnp3_altova_CObjDefaultVariationsT_altova_obj42, 0, 0> obj42;	// obj42 CunsignedByte
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_sdnp3

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_sdnp3_ALTOVA_CObjDefaultVariationsT
