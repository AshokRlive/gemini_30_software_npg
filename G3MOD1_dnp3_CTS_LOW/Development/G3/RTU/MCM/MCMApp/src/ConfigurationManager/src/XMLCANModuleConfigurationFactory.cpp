/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <iostream>
#include <string>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "Table.h"
#include "GlobalDefs.h"

#include "MCMConfigEnum.h"
#include "XMLCANModuleConfigurationFactory.h"
#include "ModuleRegistrationManager.h"
#include "PSMIOModule.h"

#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>

#include "Altova/Altova.h"
#include "Altova/SchemaTypes.h"
#include "Altova/AltovaException.h"

#include "AltovaXML/XmlException.h"
#include "AltovaXML/Node.h"

#include "g3schema/g3schema.h"

using namespace g3schema;
using namespace altova;

#include "XMLParser.h"

using namespace ns_common;
using namespace ns_g3module;

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define LOG_DEF_CHNL_CONF(moduleType, moduleID, channelType, channelID,iom_err) \
            log.warn("%s %i:%i:%i not found. Using default configuration. Error %i: %s",\
            channelType, module, moduleID + 1, channelID, iom_err, IOM_ERROR_ToSTRING(iom_err)\
                );\
            ret = IOM_ERROR_NONE;\

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/**
 * \brief Create a module and log errors
 *
 * \param module Module Type
 * \param modID Module ID
 * \param moduleManager Instance of the Module Manager
 */
static void createModule(MODULE module, MODULE_ID modID, IIOModuleManager& moduleManager);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/** Reserved channels arrays
 * These arrays contains the ID of the Channels that should be forced to event
 * even if the Configuration File says otherwise.
 */
static ChannelRef reservedAIChannels[] = {
                {MODULE_PSM, MODULE_ID_0, PSM_CH_AINPUT_MS_CURRENT}
};
static ChannelRef reservedDIChannels[] = {
                {MODULE_PSM, MODULE_ID_0, PSM_CH_DINPUT_MS_OVERCURRENT},
                {MODULE_PSM, MODULE_ID_0, PSM_CH_DINPUT_BT_TEST_FAIL},
                {MODULE_PSM, MODULE_ID_0, PSM_CH_DINPUT_BT_TEST_IN_PROGRESS}
};

Table <ChannelRef>reservedAIChannel(reservedAIChannels, sizeof(reservedAIChannels) / sizeof(ChannelRef));
Table <ChannelRef>reservedDIChannel(reservedDIChannels, sizeof(reservedDIChannels) / sizeof(ChannelRef));

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

XMLCANModuleConfigurationFactory::XMLCANModuleConfigurationFactory() :
                                                log(Logger::getLogger(SUBSYSTEM_ID_CONFMGR)),
                                                configuration(NULL),
                                                PSMCount(0),
                                                SCMCount(0),
                                                DSMCount(0),
                                                FDMCount(0),
                                                FPMCount(0),
                                                HMICount(0),
                                                IOMCount(0),
                                                SCMMK2Count(0)
{}

XMLCANModuleConfigurationFactory::~XMLCANModuleConfigurationFactory(){}


void XMLCANModuleConfigurationFactory::setConfiguration(Cg3schema *configuration)
{
    this->configuration = configuration;

    /* If there is no configuration reset boards counter */
    if(configuration == NULL)
    {
        countReset();
        return;
    }

    try
    {
        /* Get a reference to the configuration */
        CConfigurationT configurationRoot = getXMLElement<CConfigurationT>(configuration->configuration);

        /* TODO: pueyos_a - module presence shouldn't be optional? */
        /* Get a reference to the modules */
        CModulesT modules = getXMLElement<CModulesT>(configurationRoot.modules);

        /* Get CAN Modules counts */
        PSMCount = (modules.PSM.exists())? modules.PSM.count() : 0;
        SCMCount = (modules.SCM.exists())? modules.SCM.count() : 0;
        DSMCount = (modules.DSM.exists())? modules.DSM.count() : 0;
        FDMCount = (modules.FDM.exists())? modules.FDM.count() : 0;
        FPMCount = (modules.FPM.exists())? modules.FPM.count() : 0;
        HMICount = (modules.HMI.exists())? modules.HMI.count() : 0;
        IOMCount = (modules.IOM.exists())? modules.IOM.count() : 0;
#if SCM_MK2_SUPPORT
        SCMMK2Count = (modules.SCM_MK2.exists())? modules.SCM_MK2.count() : 0;
#endif

        log.debug("%s - Modules found in config: SCM=%u SCM_MK2=%u DSM=%u FDM=%u FPM=%u HMI=%u IOM=%u",
                        __AT__,
                        PSMCount,
                        SCMCount,
                        SCMMK2Count,
                        DSMCount,
                        FDMCount,
                        FPMCount,
                        HMICount,
                        IOMCount
                        );
    }
    catch(const ParserException& e)
    {
        log.error("%s: Error in CAN module Configuration: %s", __AT__, e.what());
        this->configuration = NULL;
        countReset();
    }
    catch(...)
    {
        log.error("%s Exception raised", __AT__);
        this->configuration = NULL;
        countReset();
    }
}


lu_bool_t XMLCANModuleConfigurationFactory::isConfigured( MODULE module,
                                                          MODULE_ID moduleID
                                                        )
{
    IOM_ERROR ret = IOM_ERROR_NOT_FOUND;

    /* Check if the configuration is available */
    if(configuration == NULL)
    {
        return LU_FALSE;
    }

    /* Get a reference to the configuration */
    CConfigurationT configurationRoot = configuration->configuration.first();

    /* Get a reference to the modules */
    CModulesT modules = configurationRoot.modules.first();

    switch(module)
    {
        case MODULE_PSM:
            if(PSMCount > 0)
            {
                ret = getModule(modules.PSM.all(), moduleID);
            }
        break;

        case MODULE_SCM:
            if(SCMCount > 0)
            {
                ret = getModule(modules.SCM.all(), moduleID);
            }
        break;

#if SCM_MK2_SUPPORT
        case MODULE_SCM_MK2:
            if(SCMMK2Count > 0)
            {
                ret = getModule(modules.SCM_MK2.all(), moduleID);
            }
        break;
#endif

        case MODULE_DSM:
            if(DSMCount > 0)
            {
                ret = getModule(modules.DSM.all(), moduleID);
            }
        break;

        case MODULE_FDM:
            if(FDMCount > 0)
            {
                ret = getModule(modules.FDM.all(), moduleID);
            }
        break;

        case MODULE_FPM:
            if(FPMCount > 0)
            {
                ret = getModule(modules.FPM.all(), moduleID);
            }
        break;

        case MODULE_HMI:
            if(HMICount > 0)
            {
                ret = getModule(modules.HMI.all(), moduleID);
            }
        break;

        case MODULE_IOM:
            if(IOMCount > 0)
            {
                ret = getModule(modules.IOM.all(), moduleID);
            }
        break;

        default:
            ret = IOM_ERROR_NOT_FOUND;
        break;
    }

    return (ret == IOM_ERROR_NONE)? LU_TRUE : LU_FALSE;
}


lu_bool_t XMLCANModuleConfigurationFactory::isRegistered( MODULE module,
                                                          MODULE_ID moduleID,
                                                          ModuleUID moduleUID
                                                        )
{
    if( ModuleRegistrationManager::getInstance()->validateModule( module,
                                                                  moduleID,
                                                                  moduleUID
                                                                ) != MRDB_ERROR_NONE
      )
    {
        return LU_FALSE;
    }
    else
    {
        return LU_TRUE;
    }
}


lu_bool_t XMLCANModuleConfigurationFactory::setAllModules(IIOModuleManager &moduleManager)
{
    /* Check if the configuration is available */
    if(configuration == NULL)
    {
        return LU_FALSE;
    }

    try
    {
        checkXMLElement(configuration->configuration);
        checkXMLElement(configuration->configuration.first().modules);
    }
    catch (ParserException& e)
    {
        log.error("Configuration file, error: %s", e.what());
        return LU_FALSE;
    }

    /* Get a reference to the configuration */
    CConfigurationT configurationRoot = configuration->configuration.first();

    /* Get a reference to the modules */
    CModulesT modules = configurationRoot.modules.first();

    MODULE_ID modID;
    if(PSMCount > 0)
    {
        for(Iterator<CPSMModuleT> itModule = modules.PSM.all(); itModule; ++itModule)
        {
            if(itModule.moduleID.exists())
            {
                modID = static_cast<MODULE_ID>(itModule.moduleID.GetEnumerationValue());
                createModule(MODULE_PSM, modID, moduleManager);
            }
        }
    }
    if(SCMCount > 0)
    {
        for(Iterator<CSCMModuleT> itModule = modules.SCM.all(); itModule; ++itModule)
        {
            if(itModule.moduleID.exists())
            {
                modID = static_cast<MODULE_ID>(itModule.moduleID.GetEnumerationValue());
                createModule(MODULE_SCM, modID, moduleManager);
            }
        }
    }
#if SCM_MK2_SUPPORT
    if(SCMMK2Count > 0)
    {
        for(Iterator<CSCMMK2ModuleT> itModule = modules.SCM_MK2.all(); itModule; ++itModule)
        {
            if(itModule.moduleID.exists())
            {
                modID = static_cast<MODULE_ID>(itModule.moduleID.GetEnumerationValue());
                createModule(MODULE_SCM_MK2, modID, moduleManager);
            }
        }
    }
#endif
    if(DSMCount > 0)
    {
        for(Iterator<CDSMModuleT> itModule = modules.DSM.all(); itModule; ++itModule)
        {
            if(itModule.moduleID.exists())
            {
                modID = static_cast<MODULE_ID>(itModule.moduleID.GetEnumerationValue());
                createModule(MODULE_DSM, modID, moduleManager);
            }
        }
    }
    if(FDMCount > 0)
    {
        for(Iterator<CFDMModuleT> itModule = modules.FDM.all(); itModule; ++itModule)
        {
            if(itModule.moduleID.exists())
            {
                modID = static_cast<MODULE_ID>(itModule.moduleID.GetEnumerationValue());
                createModule(MODULE_FDM, modID, moduleManager);
            }
        }
    }
    if(FPMCount > 0)
    {
        for(Iterator<CFPMModuleT> itModule = modules.FPM.all(); itModule; ++itModule)
        {
            if(itModule.moduleID.exists())
            {
                modID = static_cast<MODULE_ID>(itModule.moduleID.GetEnumerationValue());
                createModule(MODULE_FPM, modID, moduleManager);
            }
        }
    }
    if(HMICount > 0)
    {
        for(Iterator<CHMIModuleT> itModule = modules.HMI.all(); itModule; ++itModule)
        {
            if(itModule.moduleID.exists())
            {
                modID = static_cast<MODULE_ID>(itModule.moduleID.GetEnumerationValue());
                createModule(MODULE_HMI, modID, moduleManager);
            }
        }
    }
    if(IOMCount > 0)
    {
        for(Iterator<CIOMModuleT> itModule = modules.IOM.all(); itModule; ++itModule)
        {
            if(itModule.moduleID.exists())
            {
                modID = static_cast<MODULE_ID>(itModule.moduleID.GetEnumerationValue());
                moduleManager.createModule(MODULE_IOM, modID);
            }
        }
    }

    return LU_TRUE;
}


IOM_ERROR XMLCANModuleConfigurationFactory::getIDigitalConf( MODULE module,
                                                              MODULE_ID moduleID,
                                                              lu_uint32_t channelID,
                                                              InputDigitalCANChannelConf &conf
                                                            )
{
    IOM_ERROR ret = IOM_ERROR_NONE;

    /* Check if the configuration is available */
    if(configuration == NULL)
    {
        ret = IOM_ERROR_NOT_RUNNING;
    }
    else
    {
        try
        {
            checkXMLElement(configuration->configuration);
            checkXMLElement(configuration->configuration.first().modules);
        }
        catch (ParserException& e)
        {
            log.error("Configuration file, error: %s", e.what());
            return IOM_ERROR_NOT_FOUND;
        }
        /* Get a reference to the configuration */
        CConfigurationT configurationRoot = configuration->configuration.first();

        /* Get a reference to the modules */
        CModulesT modules = configurationRoot.modules.first();

        switch(module)
        {
            case MODULE_MCM:
                ret = getDigitalChConf(modules.MCM.all(), moduleID, channelID, conf);
            break;

            case MODULE_PSM:
                if(PSMCount > 0)
                {
                    ret = getDigitalChConf(modules.PSM.all(), moduleID, channelID, conf);
                }
            break;
            case MODULE_SCM:
                if(SCMCount > 0)
                {
                    ret = getDigitalChConf(modules.SCM.all(), moduleID, channelID, conf);
                }
            break;
#if SCM_MK2_SUPPORT
            case MODULE_SCM_MK2:
                if(SCMMK2Count > 0)
                {
                    ret = getDigitalChConf(modules.SCM_MK2.all(), moduleID, channelID, conf);
                }
            break;
#endif
            case MODULE_DSM:
                if(DSMCount > 0)
                {
                    ret = getDigitalChConf(modules.DSM.all(), moduleID, channelID, conf);
                }
            break;
            case MODULE_FDM:
                if(FDMCount > 0)
                {
                    ret = getDigitalChConf(modules.FDM.all(), moduleID, channelID, conf);
                }
            break;
            case MODULE_FPM:
                if(FPMCount > 0)
                {
                    ret = getDigitalChConf(modules.FPM.all(), moduleID, channelID, conf);
                }
            break;
            case MODULE_IOM:
                if(IOMCount > 0)
                {
                    ret = getDigitalChConf(modules.IOM.all(), moduleID, channelID, conf);
                }
            break;

            default:
                ret = IOM_ERROR_NOT_FOUND;
            break;
        }
    }

    if(ret != IOM_ERROR_NONE)
    {

        /* In case of error or not configured
         * channel use default configuration
         */
        conf.enable = LU_FALSE;
        conf.eventEnable = LU_FALSE;
        conf.extEquipInvert = LU_FALSE;
        conf.dbHigh2LowMs = 100;
        conf.dbLow2HighMs = 100;

        LOG_DEF_CHNL_CONF(module,moduleID,"Digital Input Channel",channelID,ret);
    }
    else
    {
        if(checkReserved(module, moduleID, CHANNEL_TYPE_DINPUT, channelID) == LU_TRUE)
        {
            //Reserved: force eventing
            conf.eventEnable = LU_TRUE;
        }
    }
    return ret;
}


IOM_ERROR XMLCANModuleConfigurationFactory::getIAnalogueConf( MODULE module,
                                                              MODULE_ID moduleID,
                                                              lu_uint32_t channelID,
                                                              InputAnalogueCANChannelConf &conf
                                                            )
{
    IOM_ERROR ret = IOM_ERROR_NONE;

    if(configuration == NULL)
    {
        ret = IOM_ERROR_NOT_RUNNING;
    }
    else
    {
        try
        {
            checkXMLElement(configuration->configuration);
            checkXMLElement(configuration->configuration.first().modules);
        }
        catch (ParserException& e)
        {
            log.error("Configuration file, error: %s", e.what());
            return IOM_ERROR_NOT_FOUND;
        }

        /* Get a reference to the configuration */
        CConfigurationT configurationRoot = configuration->configuration.first();

        /* Get a reference to the modules */
        CModulesT modules = configurationRoot.modules.first();

        ret = IOM_ERROR_NOT_FOUND;

        switch(module)
        {
            case MODULE_MCM:
                ret = getAnalogueChConf(modules.MCM.all(), moduleID, channelID, conf);
            break;

            case MODULE_PSM:
                if(PSMCount > 0)
                {
                    ret = getAnalogueChConf(modules.PSM.all(), moduleID, channelID, conf);
                }
            break;
            case MODULE_SCM:
                if(SCMCount > 0)
                {
                    ret = getAnalogueChConf(modules.SCM.all(), moduleID, channelID, conf);
                }
            break;
#if SCM_MK2_SUPPORT
            case MODULE_SCM_MK2:
                if(SCMMK2Count > 0)
                {
                    ret = getAnalogueChConf(modules.SCM_MK2.all(), moduleID, channelID, conf);
                }
#endif
            break;
            case MODULE_DSM:
                if(DSMCount > 0)
                {
                    ret = getAnalogueChConf(modules.DSM.all(), moduleID, channelID, conf);
                }
            break;
            case MODULE_FDM:
                if(FDMCount > 0)
                {
                    ret = getAnalogueChConf(modules.FDM.all(), moduleID, channelID, conf);
                }
            break;
            case MODULE_FPM:
                if(FPMCount > 0)
                {
                    ret = getAnalogueChConf(modules.FPM.all(), moduleID, channelID, conf);
                }
            break;
            case MODULE_HMI:
                if(HMICount > 0)
                {
                    ret = getAnalogueChConf(modules.HMI.all(), moduleID, channelID, conf);
                }
            break;

            case MODULE_IOM:
                if(IOMCount > 0)
                {
                    ret = getAnalogueChConf(modules.IOM.all(), moduleID, channelID, conf);
                }
            break;

            default:
                ret = IOM_ERROR_NOT_FOUND;
            break;
        }
    }

    if(ret != IOM_ERROR_NONE)
    {

        /* In case of error or not configured
         * channel use default configuration
         */
        conf.enable = LU_FALSE;
        conf.eventEnable = LU_FALSE;
        conf.eventMs = 0;

        LOG_DEF_CHNL_CONF(module,moduleID,"Analogue Input Channel",channelID,ret);
    }
    else
    {
        if(checkReserved(module, moduleID, CHANNEL_TYPE_AINPUT, channelID) == LU_TRUE)
        {
            //Reserved: force eventing
            conf.eventEnable = LU_TRUE;
            conf.eventMs = 100;
        }
    }

    return ret;
}

IOM_ERROR XMLCANModuleConfigurationFactory::getSwitchOutConf( MODULE module,
                                                               MODULE_ID moduleID,
                                                               lu_uint32_t channelID,
                                                               SwitchCANChannelConf &conf
                                                             )
{
    IOM_ERROR ret = IOM_ERROR_NONE;

    if(configuration == NULL)
    {
        ret = IOM_ERROR_NOT_RUNNING;
    }
    else
    {
        /* Get a reference to the configuration */
        CConfigurationT configurationRoot = configuration->configuration.first();

        /* Get a reference to the modules */
        CModulesT modules = configurationRoot.modules.first();

        ret = IOM_ERROR_NOT_FOUND;

        if(module == MODULE_SCM)
        {
            if(SCMCount > 0)
            {
                ret = getSwitchChConf(modules.SCM.all(), moduleID, channelID, conf);
            }
        }
    }

    if(ret != IOM_ERROR_NONE)
    {
        /* In case of error or not configured
         * channel use default configuration
         */
        conf.enable        = LU_FALSE;
        conf.polarity      = 0;
        conf.pulseLengthMs = 0;
        conf.inhibit       = 0;

        LOG_DEF_CHNL_CONF(module,moduleID,"Switch Out Channel",channelID,ret);
    }

    return ret;
}


IOM_ERROR XMLCANModuleConfigurationFactory::getDSMSwitchOutConf(MODULE module,
                                                                MODULE_ID moduleID,
                                                                lu_uint32_t channelID,
                                                                SwitchCANChannelDSM::SwitchCANChannelDSMConf &conf
                                                                )
{
    IOM_ERROR ret = IOM_ERROR_NONE;

    if(configuration == NULL)
    {
        ret = IOM_ERROR_NOT_RUNNING;
    }
    else
    {
        /* Get a reference to the configuration */
        CConfigurationT configurationRoot = configuration->configuration.first();

        /* Get a reference to the modules */
        CModulesT modules = configurationRoot.modules.first();

        ret = IOM_ERROR_NOT_FOUND;

        if(module == MODULE_DSM)
        {
            if(DSMCount > 0)
            {
                ret = getDSMSwitchChConf(modules.DSM.all(), MODULE_DSM, moduleID, channelID, conf);
            }
        }
    }

    if(ret != IOM_ERROR_NONE)
    {
        //In case of error or not configured channel use default configuration
        conf.reset();

        LOG_DEF_CHNL_CONF(module,moduleID,"DSM Switch Out Channel",channelID,ret);
    }
    return ret;
}


IOM_ERROR XMLCANModuleConfigurationFactory::getSCM2SwitchOutConf(MODULE module,
                                                                MODULE_ID moduleID,
                                                                lu_uint32_t channelID,
                                                                SwitchCANChannelSCM2::SwitchConf &conf
                                                                )
{
    IOM_ERROR ret = IOM_ERROR_NONE;

    if(configuration == NULL)
    {
        ret = IOM_ERROR_NOT_RUNNING;
    }
    else
    {
        /* Get a reference to the configuration */
        CConfigurationT configurationRoot = configuration->configuration.first();

        /* Get a reference to the modules */
        CModulesT modules = configurationRoot.modules.first();

        ret = IOM_ERROR_NOT_FOUND;

        if(module == MODULE_SCM_MK2)
        {
            if(SCMMK2Count > 0)
            {
                ret = getSCM2SwitchChConf(modules.SCM_MK2.all(), MODULE_SCM_MK2, moduleID, channelID, conf);
            }
        }
    }

    if(ret != IOM_ERROR_NONE)
    {
        //In case of error or not configured channel use default configuration
        conf.reset();

        LOG_DEF_CHNL_CONF(module,moduleID,"SCM Switch Out Channel",channelID,ret);
    }
    return ret;
}


IOM_ERROR XMLCANModuleConfigurationFactory::getDigitalOutConf( MODULE module,
                                                               MODULE_ID moduleID,
                                                               lu_uint32_t channelID,
                                                               OutputDigitalCANChannelConf &conf
                                                             )
{
    IOM_ERROR ret = IOM_ERROR_NONE;

    if(configuration == NULL)
    {
        ret = IOM_ERROR_NOT_RUNNING;
    }
    else
    {
        /* Get a reference to the configuration */
        CConfigurationT configurationRoot = configuration->configuration.first();

        /* Get a reference to the modules */
        CModulesT modules = configurationRoot.modules.first();

        ret = IOM_ERROR_NOT_FOUND;

        switch(module)
        {
            case MODULE_IOM:
                if(IOMCount > 0)
                {
                    ret = getDigitalOutChConf(modules.IOM.all(), moduleID, channelID, conf);
                }
            break;

            case MODULE_SCM:
                if(SCMCount > 0)
                {
                    ret = getDigitalOutChConf(modules.SCM.all(), moduleID, channelID, conf);
                }
            break;

#if SCM_MK2_SUPPORT
            case MODULE_SCM_MK2:
                if(SCMMK2Count > 0)
                {
                    ret = getDigitalOutChConf(modules.SCM_MK2.all(), moduleID, channelID, conf);
                }
            break;
#endif

            case MODULE_DSM:
                if(DSMCount > 0)
                {
                    ret = getDigitalOutChConf(modules.DSM.all(), moduleID, channelID, conf);
                }
            break;

            case MODULE_PSM:
                if (PSMCount > 0)
                {
                    ret = getDigitalOutChConf(modules.PSM.all(), moduleID, channelID, conf);
                }
            break;

            default:
                ret = IOM_ERROR_NOT_FOUND;
            break;
        }
    }

    if(ret != IOM_ERROR_NONE)
    {
        /* In case of error or not configured
         * channel use default configuration
         */
        conf.enable        = LU_FALSE;
        conf.pulseLengthMs = 0;

        LOG_DEF_CHNL_CONF(module,moduleID,"Digital Out Channel",channelID,ret);

    }

    return ret;
}


IOM_ERROR XMLCANModuleConfigurationFactory::getFPMConf(MODULE module,
                                                          MODULE_ID moduleID,
                                                          lu_uint32_t channelID,
                                                          FPMConfigStr& conf
                                                        )
{
    IOM_ERROR ret = IOM_ERROR_NONE;

    if(configuration == NULL)
    {
        ret = IOM_ERROR_NOT_RUNNING;
    }
    else
    {
        /* Get a reference to the configuration */
        CConfigurationT configurationRoot = configuration->configuration.first();

        /* Get a reference to the modules */
        CModulesT modules = configurationRoot.modules.first();

        ret = IOM_ERROR_NOT_FOUND;

        if(module == MODULE_FPM)
        {
            if(FPMCount > 0)
            {
                ret = getFPMConf(modules.FPM.all(), moduleID, channelID, conf);
            }
        }
    }

    if(ret != IOM_ERROR_NONE)
    {
        /* In case of error or not configured
         * channel use default configuration
         */
        conf.enabled = LU_FALSE;

        LOG_DEF_CHNL_CONF(module,moduleID,"FPM FPI Channel",channelID,ret);
    }

    return ret;
}



IOM_ERROR XMLCANModuleConfigurationFactory::getHMIConf( MODULE module,
                                                        MODULE_ID moduleID,
                                                        lu_uint32_t channelID,
                                                        HMIConfigStr &conf
                                                      )
{
    IOM_ERROR ret = IOM_ERROR_NONE;

    if(configuration == NULL)
    {
        ret = IOM_ERROR_NOT_RUNNING;
    }
    else
    {
        /* Get a reference to the configuration */
        CConfigurationT configurationRoot = configuration->configuration.first();

        /* Get a reference to the modules */
        CModulesT modules = configurationRoot.modules.first();

        ret = IOM_ERROR_NOT_FOUND;

        switch(module)
        {
            case MODULE_HMI:
                if(HMICount > 0)
                {
                    ret = getHmiChConf(modules.HMI.all(), moduleID, channelID, conf);
                }
            break;

            default:
                ret = IOM_ERROR_NOT_FOUND;
            break;
        }
    }

    if(ret != IOM_ERROR_NONE)
    {
        /* In case of error or not configured
         * channel use default configuration
         */
        conf.buttonClickEnable  = LU_FALSE;
        conf.reqOLREnable       = LU_FALSE;
        conf.timeModeOffMs      = 0;
        conf.timeModeToggleLRMs = 0;

        LOG_DEF_CHNL_CONF(module,moduleID,"Switch Out Channel",channelID,ret);
    }

    return ret;
}
/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

template<typename ModuleType> IOM_ERROR XMLCANModuleConfigurationFactory::getModule
                                                            (
                                                              ModuleType it,
                                                              MODULE_ID moduleID
                                                             )
{
    try
    {
        /* Scan all the boards */
        for(; it; ++it)
        {
            if(it.moduleID.GetEnumerationValue() == moduleID)
            {
                return IOM_ERROR_NONE;
            }
        }
    }
    catch(...)
    {
        log.error("XMLCANChannelConfigurationFactory::getModule Exception raised");
    }

    /* Not found */
    return IOM_ERROR_NOT_FOUND;
}


IOM_ERROR XMLCANModuleConfigurationFactory::getModuleConfig(MODULE module,
                                                          MODULE_ID moduleID,
                                                          void* conf
                                                          )
{
    /* Check if the configuration is available */
    if(configuration == NULL)
    {
        return IOM_ERROR_NOT_RUNNING;
    }

    /* Get a reference to the configuration */
    CConfigurationT configurationRoot = configuration->configuration.first();

    /* Get a reference to the modules */
    CModulesT modules = configurationRoot.modules.first();

    try
    {
        switch(module)
        {
            case MODULE_SCM:
                if(SCMCount > 0)
                {
                    Iterator<CSCMModuleT> it = modules.SCM.all();
                    /* Scan all the boards */
                    for(; it; ++it)
                    {
                        SCMConfigStr* cfgSCM = (SCMConfigStr*)conf;
                        if(it.moduleID.GetEnumerationValue() == moduleID)
                        {
                            /* Board found. Get configuration fields */
                            /* Mandatory Fields */
                            cfgSCM->greenLEDPolarity = getXMLAttrEnum<LED_COLOUR>(it.openColour, LED_COLOUR_VALUEfromINDEX);
                            cfgSCM->doNotCheckSwitchPosition = getXMLAttr(it.allowForcedOperation);

                            /* Optional Fields */

                            return IOM_ERROR_NONE;
                        }
                    }
                }
                break;

            case MODULE_DSM:
                if(DSMCount > 0)
                {
                    Iterator<CDSMModuleT> it = modules.DSM.all();
                    /* Scan all the boards */
                    for(; it; ++it)
                    {
                        DSMConfigStr* cfgDSM = (DSMConfigStr*)conf;
                        if(it.moduleID.GetEnumerationValue() == moduleID)
                        {
                            /* Board found. Get configuration fields */
                            /* Mandatory Fields */
                            cfgDSM->greenLEDPolarity = getXMLAttrEnum<LED_COLOUR>(it.openColour, LED_COLOUR_VALUEfromINDEX);
                            cfgDSM->polarityA = getXMLAttr(it.polarityA);
                            cfgDSM->polarityB = getXMLAttr(it.polarityB);
                            cfgDSM->supplyEnable24V = getXMLAttr(it.supplyEnable24V);

                            cfgDSM->doNotCheckSwitchPositionA = getXMLAttr(it.allowForcedOperationA);
                            cfgDSM->doNotCheckSwitchPositionB = getXMLAttr(it.allowForcedOperationB);

                            /* Optional Fields */

                            return IOM_ERROR_NONE;
                        }
                    }
                }
                break;

#if SCM_MK2_SUPPORT
            case MODULE_SCM_MK2:
                if(SCMMK2Count > 0)
                {
                    Iterator<CSCMMK2ModuleT> it = modules.SCM_MK2.all();
                    /* Scan all the boards */
                    for(; it; ++it)
                    {
                        SCMMK2ConfigStr* cfgSCM2 = (SCMMK2ConfigStr*)conf;
                        if(it.moduleID.GetEnumerationValue() == moduleID)
                        {
                            /* Board found. Get configuration fields */
                            /* Mandatory Fields */
                            cfgSCM2->greenLEDPolarity = getXMLAttrEnum<LED_COLOUR>(it.openColour, LED_COLOUR_VALUEfromINDEX);
                            cfgSCM2->doNotCheckSwitchPosition = getXMLAttr(it.allowForcedOperation);
                            lu_uint8_t motorMode = getXMLAttr(it.motorMode);
                            switch(motorMode)
                            {
                                case MOTOR_MODE_1:
                                    cfgSCM2->actuatorLimitSwitchEnable = 0;
                                    cfgSCM2->hardwareMotorLimitEnable = 1;
                                    break;
                                case MOTOR_MODE_2:
                                    cfgSCM2->actuatorLimitSwitchEnable = 0;
                                    cfgSCM2->hardwareMotorLimitEnable = 0;
                                    break;
                                case MOTOR_MODE_3:
                                    cfgSCM2->actuatorLimitSwitchEnable = 1;
                                    cfgSCM2->hardwareMotorLimitEnable = 0;
                                    break;
                                default:
                                    std::stringstream ss;
                                    ss << "'" << it.motorMode.info().GetLocalName() << "' ";
                                    ss << "Invalid motor mode " << (lu_uint32_t)motorMode;
                                    throw ParserException(CONFMGR_ERROR_CONFIG, ss.str().c_str());
                                    break;
                            }
                            return IOM_ERROR_NONE;
                        }
                    }
                }
                break;
#endif

            case MODULE_PSM:
                if(PSMCount > 0)
                {
                    Iterator<CPSMModuleT> it = modules.PSM.all();
                    /* Scan all the boards */
                    for(; it; ++it)
                    {
                        if(it.moduleID.GetEnumerationValue() == moduleID)
                        {
                            /* Board found. Get configuration fields */
                            PSMIOModule::PSMConfigStr* cfgPSM = (PSMIOModule::PSMConfigStr*)conf;
                            /* Optional Fields */
                        	if(it.fanSetting.exists())
                        	{
                                CFanSettingT fan = it.fanSetting.first();
                                if(fan.fanFitted.exists())
                                {
                                    /* Mandatory Fields */
                                    cfgPSM->fanConfig.fanFitted = getXMLAttr(fan.fanFitted);
                                    cfgPSM->fanConfig.fanSpeedSensorFitted = getXMLAttr(fan.fanSpdSensorFitted);
                                    cfgPSM->fanConfig.fanTempThreshold = getXMLAttr(fan.fanTempThreshold);
                                    cfgPSM->fanConfig.fanTempHysteresis = getXMLAttr(fan.fanTempHysteresis);
                                    cfgPSM->fanConfig.fanFaultHysteresisMs = getXMLAttr(fan.faultHysteresis);
                                }
                                if(fan.fanOutAsDO.exists()) 
							    { 
								     cfgPSM->fanConfig.fanDigitalOutput = getXMLAttr(fan.fanOutAsDO); 
							    } 
								if(fan.fanOutAsLED.exists()) 
								{ 
								     cfgPSM->fanConfig.fanExternalSupply = getXMLAttr(fan.fanOutAsLED); 
							    } 
                        	}
             
                            bool present = false;
                            getXMLOptAttr(it.cyclicBatteryTest.first().enabled, present);
                            if(present)
                            {
                                /* Note that cyclic test is disabled by default
                                 * (batteryTestPeriodHours == 0)
                                 */
                                cfgPSM->chargerCfg.batteryTestPeriodHours = getXMLAttr(it.cyclicBatteryTest.first().schedulePeriodHrs);
                                cfgPSM->chargerCfg.batteryTestDurationSecs = getXMLAttr(it.cyclicBatteryTest.first().testDurationMins) * 60;
                            }

                            return IOM_ERROR_NONE;
                        }
                    }
                }
                break;
            default:
                break;
        }
    }
    catch(const std::exception& e)
    {
       log.error("%s: %s", __AT__, e.what());
    }

    /* Not found */
    return IOM_ERROR_NOT_FOUND;
}


template<typename ModuleType> IOM_ERROR XMLCANModuleConfigurationFactory::getDigitalChConf
                                              (
                                                ModuleType it,
                                                MODULE_ID moduleID,
                                                lu_uint32_t channelID,
                                                InputDigitalCANChannelConf &conf
                                               )
{
    try
    {
        /* Scan all the boards */
        for(; it; ++it)
        {
            if(it.moduleID.GetEnumerationValue() == moduleID)
            {
                /* Board found. Now scan all channels */
                checkXMLElement(it.channels);
                checkXMLElement(it.channels.first().digitalInput);
                for(Iterator<CDigitalInputChnlT> itDCH = it.channels.first().digitalInput.all(); itDCH; ++itDCH)
                {
                    if(getXMLAttr(itDCH.channelID) == channelID)
                    {
                        /* Channel found. Parse configuration */
                        conf.eventEnable = getXMLAttr(itDCH.eventEnable);
                        conf.extEquipInvert = getXMLAttr(itDCH.extEquipInvert);
                        conf.dbHigh2LowMs = getXMLAttr(itDCH.dbHigh2LowMs);
                        conf.dbLow2HighMs = getXMLAttr(itDCH.dbLow2HighMs);
                        conf.enable = getXMLAttr(itDCH.enable);
                        return IOM_ERROR_NONE;
                    }
                }
            }
        }
    }
    catch(const ParserException& e)
    {
        log.error("%s: Error in CAN module Digital channel Configuration: %s", __AT__, e.what());
    }
    catch(...)
    {
        log.error("XMLCANChannelConfigurationFactory::getDigitalChConf Exception raised");
    }

    /* Not found */
    return IOM_ERROR_NOT_FOUND;
}


template<typename ModuleType> IOM_ERROR XMLCANModuleConfigurationFactory::getAnalogueChConf
                                             (
                                               ModuleType it,
                                               MODULE_ID moduleID,
                                               lu_uint32_t channelID,
                                               InputAnalogueCANChannelConf &conf
                                             )
{
    try
    {
        /* Scan all the boards */
        for(; it; ++it)
        {
            if(it.moduleID.GetEnumerationValue() == moduleID)
            {
                /* Board found. Now scan all channels */
                checkXMLElement(it.channels);
                checkXMLElement(it.channels.first().analogueInput);
                for(Iterator<CAnalogueInputChnlT> itACH = it.channels.first().analogueInput.all(); itACH; ++itACH)
                {
                    if(getXMLAttr(itACH.channelID) == channelID)
                    {
                        /* Channel found. Parse configuration */
                        conf.eventMs = getXMLAttr(itACH.eventMs);
                        conf.eventEnable = getXMLAttr(itACH.eventEnable);
                        conf.enable = getXMLAttr(itACH.enable);
                        return IOM_ERROR_NONE;
                    }
                }
            }
        }
    }
    catch(const ParserException& e)
    {
        log.error("%s: Error in CAN module Analogue channel configuration: %s", __AT__, e.what());
    }
    catch(...)
    {
        log.error("XMLCANChannelConfigurationFactory::getAnalogueChConf Exception raised");
    }

    /* Not found */
    return IOM_ERROR_NOT_FOUND;
}


template<typename ModuleType> IOM_ERROR XMLCANModuleConfigurationFactory::getSwitchChConf
                                                    (
                                                      ModuleType it,
                                                      MODULE_ID moduleID,
                                                      lu_uint32_t channelID,
                                                      SwitchCANChannelConf &conf
                                                    )
{
    try
    {
        /* Scan all the boards */
        for(; it; ++it)
        {
            if(it.moduleID.GetEnumerationValue() == moduleID)
            {
                /* Board found. Now scan all channels */
                checkXMLElement(it.channels);
                checkXMLElement(it.channels.first().switchOut);
                for(Iterator<CSwitchOutChnlT> itCH = it.channels.first().switchOut.all(); itCH; ++itCH)
                {
                    if(getXMLAttr(itCH.channelID) == channelID)
                    {
                        /* Channel found. Parse configuration */
                        conf.polarity = getXMLAttr(itCH.polarity);
                        conf.pulseLengthMs = getXMLAttr(itCH.pulseLength);
                        conf.inhibit = getXMLAttr(itCH.inhibit);
                        conf.overrunMS = getXMLAttr(itCH.overrunMS);
                        conf.opTimeoutS = getXMLAttr(itCH.opTimeoutS);
                        conf.enable = LU_TRUE;  //Output channels are always enabled!
                        return IOM_ERROR_NONE;
                    }
                }
            }
        }
    }
    catch(const ParserException& e)
    {
        log.error("%s: Error in CAN module Switch channel configuration: %s", __AT__, e.what());
    }
    catch(...)
    {
        log.error("XMLCANChannelConfigurationFactory::getSwitchChConf Exception raised");
    }

    /* Not found */
    return IOM_ERROR_NOT_FOUND;
}


template<typename ModuleType>
IOM_ERROR XMLCANModuleConfigurationFactory::getDSMSwitchChConf(
                                              ModuleType it,
                                              MODULE moduleType,
                                              MODULE_ID moduleID,
                                              lu_uint32_t channelID,
                                              SwitchCANChannelDSM::SwitchCANChannelDSMConf &conf
                                            )
{
    try
    {
        /* Scan all the boards */
        for(; it; ++it)
        {
            if(it.moduleID.GetEnumerationValue() == moduleID)
            {
                /* Board found. Now scan all channels */
                checkXMLElement(it.channels);
                checkXMLElement(it.channels.first().switchOut);
                for(Iterator<CSwitchOutChnlT> itCH = it.channels.first().switchOut.all(); itCH; ++itCH)
                {
                    if(getXMLAttr(itCH.channelID) == channelID)
                    {
                        /* Channel found. Parse configuration */
                        conf.pulseLengthMs = 0;
                        getXMLOptAttr(itCH.pulseLength, conf.pulseLengthMs);
                        conf.inhibit = getXMLAttr(itCH.inhibit);
                        conf.overrunMS = getXMLAttr(itCH.overrunMS);
                        conf.opTimeoutS = getXMLAttr(itCH.opTimeoutS);
                        conf.enable = LU_TRUE;  //Output channels are always enabled!
                        return IOM_ERROR_NONE;
                    }
                }
            }
        }
    }
    catch(const ParserException& e)
    {
        log.error("%s: Error in %s%d module Switch channel configuration: %s",
                    __AT__, MODULE_ToSTRING(moduleType), moduleID+1, e.what());
    }
    catch(...)
    {
        log.error("XMLCANChannelConfigurationFactory::getSwitchChConf Exception raised");
    }

    /* Not found */
    return IOM_ERROR_NOT_FOUND;
}


template<typename ModuleType>
IOM_ERROR XMLCANModuleConfigurationFactory::getSCM2SwitchChConf(
                                              ModuleType it,
                                              MODULE moduleType,
                                              MODULE_ID moduleID,
                                              lu_uint32_t channelID,
                                              SwitchCANChannelSCM2::SwitchConf &conf
                                            )
{
    try
    {
        /* Scan all the boards */
        for(; it; ++it)
        {
            if(it.moduleID.GetEnumerationValue() == moduleID)
            {
                /* Board found. Now scan all channels */
                checkXMLElement(it.channels);
                checkXMLElement(it.channels.first().switchOut);
                for(Iterator<CSwitchOutChnlT> itCH = it.channels.first().switchOut.all(); itCH; ++itCH)
                {
                    if(getXMLAttr(itCH.channelID) == channelID)
                    {
                        /* Channel found. Parse configuration */
                        conf.inhibit = getXMLAttr(itCH.inhibit);
                        conf.overrunMS = getXMLAttr(itCH.overrunMS);
                        conf.opTimeoutS = getXMLAttr(itCH.opTimeoutS);
                        conf.motorOverDrive_ms = getXMLAttr(itCH.motorOverDriveMS);
                        conf.motorReverseDrive_ms = getXMLAttr(itCH.motorReverseDriveMS);
                        conf.enable = LU_TRUE;  //Output channels are always enabled!
                        return IOM_ERROR_NONE;
                    }
                }
            }
        }
    }
    catch(const ParserException& e)
    {
        log.error("%s: Error in %s%d module Switch channel configuration: %s",
                    __AT__, MODULE_ToSTRING(moduleType), moduleID+1, e.what());
    }
    catch(...)
    {
        log.error("XMLCANChannelConfigurationFactory::getSwitchChConf Exception raised");
    }

    /* Not found */
    return IOM_ERROR_NOT_FOUND;
}


template<typename ModuleType> IOM_ERROR XMLCANModuleConfigurationFactory::getDigitalOutChConf
                                                    (
                                                      ModuleType it,
                                                      MODULE_ID moduleID,
                                                      lu_uint32_t channelID,
                                                      OutputDigitalCANChannelConf& conf
                                                    )
{
    try
    {
        /* Scan all the boards */
        for(; it; ++it)
        {
            if(it.moduleID.GetEnumerationValue() == moduleID)
            {
                /* Board found. Now scan all channels */
                checkXMLElement(it.channels);
                checkXMLElement(it.channels.first().digitalOutput);

                for(Iterator<CDigitalOutputChnlT> itCH = it.channels.first().digitalOutput.all(); itCH; ++itCH)
                {
                    if(getXMLAttr(itCH.channelID) == channelID)
                    {
                        /* Channel found. Parse configuration */
                        conf.pulseLengthMs = getXMLAttr(itCH.pulseLength);
                        conf.enable = LU_TRUE;  //Output channels are always enabled!
                        return IOM_ERROR_NONE;
                    }
                }
            }
        }
    }
    catch(const ParserException& e)
    {
        log.error("%s: Error in CAN module Digital Out channel configuration: %s", __AT__, e.what());
    }
    catch(...)
    {
        log.error("XMLCANChannelConfigurationFactory::getDigitalOutChConf Exception raised");
    }

    /* Not found */
    return IOM_ERROR_NOT_FOUND;
}


template<typename ModuleType> IOM_ERROR XMLCANModuleConfigurationFactory::getFPMConf
                                                    (
                                                      ModuleType it,
                                                      MODULE_ID moduleID,
                                                      lu_uint32_t channelID,
                                                      FPMConfigStr& conf
                                                    )
{
    try
    {
        /* Scan all the boards */
        for(; it; ++it)
        {
            if(it.moduleID.GetEnumerationValue() == moduleID)
            {
                /* Board found. Now scan all channels */
                checkXMLElement(it.channels);
                checkXMLElement(it.channels.first().fpi);

                if(channelID >= it.channels.first().fpi.count())
                {
                    return IOM_ERROR_CONFIG; // invalid channel id
                }
                CFPIChannelT itCH = it.channels.first().fpi[channelID];
                {
                    /* Channel found. Parse configuration */
                    conf.fpiConfigChannel = channelID;
                    checkXMLElement(itCH.earthFault);
                    conf.earthFault.InstantFaultCurrent   = getXMLAttr(itCH.earthFault.first().InstantFaultCurrent);
                    conf.earthFault.minFaultDurationMs    = getXMLAttr(itCH.earthFault.first().minFaultDurationMs);
                    conf.earthFault.timedFaultCurrent     = getXMLAttr(itCH.earthFault.first().timedFaultCurrent);
                    checkXMLElement(itCH.phaseFault);
                    conf.phaseFault.InstantFaultCurrent   = getXMLAttr(itCH.phaseFault.first().InstantFaultCurrent);
                    conf.phaseFault.minFaultDurationMs    = getXMLAttr(itCH.phaseFault.first().minFaultDurationMs);
                    conf.phaseFault.timedFaultCurrent     = getXMLAttr(itCH.phaseFault.first().timedFaultCurrent);
                    conf.phaseFault.overloadCurrent       = getXMLAttr(itCH.phaseFault.first().overloadCurrent);
                    conf.phaseFault.minOverloadDurationMs = getXMLAttr(itCH.phaseFault.first().minOverloadDurationMs);

                    /* Optional sensitive Earth Fault config */
                    if(itCH.sensitiveEarthFault.exists())
                    {
                            const CFPIFaultT& senEarth = itCH.sensitiveEarthFault.first();
                            conf.sensitiveEarthFault.InstantFaultCurrent = getXMLAttr(senEarth.InstantFaultCurrent);
                                conf.sensitiveEarthFault.minFaultDurationMs  = getXMLAttr(senEarth.minFaultDurationMs);
                                conf.sensitiveEarthFault.timedFaultCurrent   = getXMLAttr(senEarth.timedFaultCurrent);
                    }
                    else
                    {
                            conf.sensitiveEarthFault.minFaultDurationMs  = 15000;
                            conf.sensitiveEarthFault.timedFaultCurrent   = 8;
                                conf.sensitiveEarthFault.InstantFaultCurrent = 0;
                    }

                    /* Optional current config */
                    if(itCH.currentAbsence.exists())
                    {
                        const CFPIFaultT& xml = itCH.currentAbsence.first();
                        conf.absenceIndication.minAbsenceDurationMs = getXMLAttr(xml.minFaultDurationMs);
                        conf.absenceIndication.timedAbsenceCurrent  = getXMLAttr(xml.timedFaultCurrent);
                    }
                    else
                    {
                        conf.absenceIndication.minAbsenceDurationMs  = 0;
                        conf.absenceIndication.timedAbsenceCurrent = 0;
                    }

                    /* Optional current config */
                    if(itCH.currentPresence.exists())
                    {
                        const CFPIFaultT& xml = itCH.currentPresence.first();
                        conf.absenceIndication.minPresenceDurationMs = getXMLAttr(xml.minFaultDurationMs);
                        conf.absenceIndication.timedPresenceCurrent  = getXMLAttr(xml.timedFaultCurrent);
                    }
                    else
                    {
                        conf.absenceIndication.minPresenceDurationMs  = 0;
                        conf.absenceIndication.timedPresenceCurrent   = 0;
                    }


                    conf.ctRatio.ctRatioDividend = getXMLAttr(itCH.CTRatioDividend);
                    conf.ctRatio.ctRatioDivisor = getXMLAttr(itCH.CTRatioDivisor);
                    conf.selfResetMs = getXMLAttr(itCH.selfResetMs);
                    conf.enabled = getXMLAttr(itCH.enabled);
                    return IOM_ERROR_NONE;
                }
            }
        }
    }
    catch(const ParserException& e)
    {
        log.error("%s: Error in CAN module Configuration: %s", __AT__, e.what());
        this->configuration = NULL;
        countReset();
    }
    catch(...)
    {
        log.error("XMLCANChannelConfigurationFactory::getDigitalOutChConf Exception raised");
    }

    /* Not found */
    return IOM_ERROR_NOT_FOUND;
}


template<typename ModuleType> IOM_ERROR XMLCANModuleConfigurationFactory::getHmiChConf( ModuleType it,
                                                                                        MODULE_ID moduleID,
                                                                                        lu_uint32_t channelID,
                                                                                        HMIConfigStr &conf
                                                                                      )
{
    LU_UNUSED(channelID);

    /* Set default values */
    conf.timeModeToggleLRMs = 500;
    conf.timeModeOffMs = 1000;

    try
    {
        /* Get MODE button config */
        CConfigurationT configurationRoot = getXMLElement<CConfigurationT>(configuration->configuration);
        if(configurationRoot.miscellaneous.exists() )
        {
            CMiscellaneousT misc = configurationRoot.miscellaneous.first();
            checkXMLElement(misc.offLocalButtonSetting);

            conf.timeModeToggleLRMs = getXMLAttr(misc.offLocalButtonSetting.first().localRemoteToggleTime);
            conf.timeModeOffMs = getXMLAttr(misc.offLocalButtonSetting.first().offToggleTime);
        }

        /* Scan all the boards */
        for(; it; ++it)
        {
            if(it.moduleID.GetEnumerationValue() == moduleID)
            {
                /* Board found. Now scan all channels */
                CHMIGeneralConfT xml = getXMLElement<CHMIGeneralConfT>(it.generalConfig);
                conf.buttonClickEnable = getXMLAttr(xml.buttonClick);
                conf.reqOLREnable = getXMLAttr(xml.requestOLR);
                return IOM_ERROR_NONE;
            }
        }
    }
    catch(const ParserException& e)
    {
        log.error("%s Error getting HMI config: %s", __AT__, e.what());
    }

    /* Not found */
    return IOM_ERROR_NOT_FOUND;
}


void XMLCANModuleConfigurationFactory::countReset()
{
    PSMCount = 0;
    SCMCount = 0;
    DSMCount = 0;
    FDMCount = 0;
    FPMCount = 0;
    HMICount = 0;
    IOMCount = 0;
    SCMMK2Count = 0;
}


lu_bool_t XMLCANModuleConfigurationFactory::checkReserved(MODULE moduleType, MODULE_ID moduleID, CHANNEL_TYPE channelType, lu_uint32_t channelID)
{
    ChannelRef* reserved = NULL;
    lu_uint32_t amount = 0;

    switch(channelType)
    {
        case CHANNEL_TYPE_AINPUT:
            reserved = reservedAIChannel.getTable();
            amount = reservedAIChannel.getEntries();
            break;
        case CHANNEL_TYPE_DINPUT:
            reserved = reservedDIChannel.getTable();
            amount = reservedDIChannel.getEntries();
            break;
        default:
            return LU_FALSE;
    }
    for (lu_uint32_t i = 0; i < amount; ++i)
    {
        if( (moduleType == reserved[i].moduleType) &&
            (moduleID == reserved[i].moduleID) &&
            (channelID == reserved[i].channelID)
            )
        {
            return LU_TRUE; //found: it is a reserved one
        }
    }
    return LU_FALSE;    //not found
}


void createModule(MODULE module, MODULE_ID modID, IIOModuleManager& moduleManager)
{
    IIOModule* mptr = moduleManager.createModule(module, modID);
    if(mptr == NULL)
    {
        /* The DB can not return/generate an appropriate module */
        Logger::getLogger(SUBSYSTEM_ID_CONFMGR).error("%s ModuleDB creation error. "
                                                    "CAN Module %i - CAN ID: %i",
                                                    __AT__,module, modID);
    }
}


/*
 *********************** End of file ******************************************
 */
