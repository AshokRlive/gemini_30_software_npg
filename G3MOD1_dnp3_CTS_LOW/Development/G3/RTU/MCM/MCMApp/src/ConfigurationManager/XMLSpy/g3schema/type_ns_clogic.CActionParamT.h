#ifndef _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CActionParamT
#define _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CActionParamT

#include "type_ns_clogic.CCLogicParametersBaseT.h"


namespace g3schema
{

namespace ns_clogic
{	

class CActionParamT : public ::g3schema::ns_clogic::CCLogicParametersBaseT
{
public:
	g3schema_EXPORT CActionParamT(xercesc::DOMNode* const& init);
	g3schema_EXPORT CActionParamT(CActionParamT const& init);
	void operator=(CActionParamT const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_ns_clogic_altova_CActionParamT); }

	MemberAttribute<unsigned,_altova_mi_ns_clogic_altova_CActionParamT_altova_delay, 0, 0> delay;	// delay CunsignedInt
	MemberElement<ns_clogic::CActionInputSettingT, _altova_mi_ns_clogic_altova_CActionParamT_altova_inputSetting> inputSetting;
	struct inputSetting { typedef Iterator<ns_clogic::CActionInputSettingT> iterator; };
	g3schema_EXPORT void SetXsiType();
};



} // namespace ns_clogic

}	// namespace g3schema

#endif // _ALTOVA_INCLUDED_g3schema_ALTOVA_ns_clogic_ALTOVA_CActionParamT
