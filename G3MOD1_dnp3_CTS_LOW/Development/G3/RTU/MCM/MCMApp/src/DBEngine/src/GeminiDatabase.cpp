/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stddef.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "GeminiDatabase.h"
#include "LockingMutex.h"
#include "ControlLogic.h"
#include "OperatableControlLogic.h"
#include "IControlLogicFactory.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
#ifdef G3DBSERVER
#include "ZmqContext.h"
#include "G3DBServer.h"

namespace g3db
{
    class ReadPointHandler:public IRequestHandler
    {
    public:
        ReadPointHandler(){};
        virtual ~ReadPointHandler(){};

        virtual lu_bool_t handle(DBQuery& query);
    };
}

/*Global instance of G3DBServer*/
g3db::G3DBServer g_dbserver(*ZmqContext::getInstance());

#endif //G3DBSERVER
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
static GeminiDatabase *INSTANCE = NULL;

GeminiDatabase* GeminiDatabase::getInstance()
{
    return INSTANCE;
}

GeminiDatabase::GeminiDatabase( SCHED_TYPE schedType,
                                lu_uint32_t priority
                              ) : bigDBLock(LU_TRUE),
                                  scheduler(schedType, priority),
                                  points(),
                                  controlLogic(),
                                  initialised(false),
                                  log(Logger::getLogger(SUBSYSTEM_ID_G3DB)),
                                  groupOLR(PointIdStr::invalidGroup),
                                  m_EOIFlag(LU_FALSE)
{
    INSTANCE = this;
#ifdef G3DBSERVER
    g_dbserver.registerHandler(new g3db::ReadPointHandler());
    g_dbserver.start();
#endif
}

GeminiDatabase::~GeminiDatabase()
{
#ifdef G3DBSERVER
    g_dbserver.stop();
#endif
    scheduler.stop();
}

GDB_ERROR GeminiDatabase::initialize( IPointFactory &pointFactory,
                                      IControlLogicFactory &CLogicFactory
                                    )
{
    /* Avoid to initialise the G3DB more than once */
    if(initialised)
        return GDB_ERROR_INITIALIZED;

    initialised = true;

    const lu_char_t *FTITLE = "GeminiDatabase::initialize:";
    GDB_ERROR ret;
    PointIdStr pointIDTmp;

    /* Get the number of points */
    lu_uint16_t sizeVP = pointFactory.getPointNumber();     //Amount of Virtual Points
    lu_uint16_t sizeCL = CLogicFactory.getCLogicNumber();   //Amount of Control Logics

    /* == Create virtual point database == */
    ret = points.init(sizeVP);
    if(ret != GDB_ERROR_NONE)
    {
        log.error("%s - Initialise DB error: %s", FTITLE, GDB_ERROR_ToSTRING(ret));
    }
    else
    {
        /* Fill the virtual point database using the factory method */
        pointFactory.setMutexLock(bigDBLock);

        for( pointIDTmp.ID = 0, pointIDTmp.group = 0;
             pointIDTmp.ID < sizeVP                   ;
             pointIDTmp.ID++
           )
        {
            GDB_ERROR res;
            res = points.addPoint(pointFactory.newPoint(pointIDTmp));
            if(res != GDB_ERROR_NONE)
            {
                log.info("%s Add point %i:%i error: %s", FTITLE,
                         pointIDTmp.group, pointIDTmp.ID, GDB_ERROR_ToSTRING(res)
                        );
                ret = GDB_ERROR_NOT_INITIALIZED;
            }
            else
            {
                log.info("%s Virtual Point %i:%i added to the internal database",
                            FTITLE, pointIDTmp.group, pointIDTmp.ID
                          );
            }
        }
    }

    /* == Create control logic blocks database == */
    GDB_ERROR ret2 = controlLogic.init(sizeCL);
    if(ret2 != GDB_ERROR_NONE)
    {
        log.error("%s Initialise Control Logic DB error: %s", FTITLE, GDB_ERROR_ToSTRING(ret2));
        ret = GDB_ERROR_NOT_INITIALIZED;
    }
    else
    {
        /* Fill the control logic database using the factory method */
        CLogicFactory.setMutexLock(bigDBLock);

        for(lu_uint16_t group = 0; group < sizeCL; ++group)
        {
            /* Group 0 is reserved (it's the virtual point DB). We create
             * control logic blocks  starting from ID 1
             */
            GDB_ERROR res;
            ControlLogic* cLogic = CLogicFactory.newCLogic(group + 1);
            res = controlLogic.addCLogic(cLogic);
            if(res != GDB_ERROR_NONE)
            {
                log.warn("%s Add Control logic %i error: %s", FTITLE,
                                group + 1, GDB_ERROR_ToSTRING(res)
                              );
                ret = GDB_ERROR_NOT_INITIALIZED;
            }
            else
            {
                if(cLogic != NULL)
                {
                    if(cLogic->getType() == CONTROL_LOGIC_TYPE_OLR)
                    {
                        groupOLR = group + 1;   //Store OLR control logic group
                    }
                }
                log.info("%s Control logic %i added to the internal database",
                          FTITLE, group + 1
                          );
            }
        }
    }

    /* == Start Scheduler == */
    scheduler.start();

    /* == Initialise virtual points == */
    IPoint* point;
    ControlLogic* ctrlLogic;
    for( pointIDTmp.ID = 0, pointIDTmp.group = 0;
         pointIDTmp.ID < sizeVP                   ;
         pointIDTmp.ID++
       )
    {
        point = points.getPoint(pointIDTmp.ID);
        if(point != NULL)
        {
            ret2 = point->init();
            switch (ret2)
            {
                case GDB_ERROR_NONE:        //OK
                case GDB_ERROR_NOT_ENABLED: //Disabled
                    //continue
                    break;
                case GDB_ERROR_INITIALIZED: //already initialised: not fatal but shouldn't happen
                    log.error("G3DB Warning: Tried to initialise again Point %s",
                                point->getID().toString().c_str());
                    break;
                default:
                    ret = ret2;     //keep (last) error
                    break;
            }
        }
    }

    /* == Initialise control logic blocks == */
    {//Mutex Section
        CLogicFactory.setMutexLock(bigDBLock);

        for(lu_uint16_t group = 0; group < sizeCL; ++group)
        {
            //Note: Group 0 is reserved (it's the virtual point DB)
            ctrlLogic = controlLogic.getCLogic(group+1);
            if(ctrlLogic != NULL)
            {
                ret2 = ctrlLogic->init();
                switch (ret2)
                {
                    case GDB_ERROR_NONE:        //OK
                    case GDB_ERROR_NOT_ENABLED: //Disabled
                        //continue
                        break;
                    case GDB_ERROR_INITIALIZED: //already initialised: not fatal but shouldn't happen
                        log.error("G3DB Warning: Tried to initialise again Control Logic %s",
                                    ctrlLogic->toString().c_str());
                        break;
                    default:
                        ret = ret2;     //keep (last) error
                        break;
                }
            }
        }
    }

    if(ret != GDB_ERROR_NONE)
    {
        log.error("Failed to fully initialise G3 Database: %s", GDB_ERROR_ToSTRING(ret));
    }

    return ret;
}


lu_uint32_t GeminiDatabase::getVPointNum(lu_uint16_t group)
{
    lu_uint32_t pointNum = 0;
    if(group == 0)  //raw Virtual Points
    {
        pointNum = points.getSize();
    }
    else
    {
        if( group <= getCLogicNum() )
        {
            ControlLogic* clPtr = controlLogic.getCLogic(group);
            if(clPtr != NULL)
            {
                pointNum = clPtr->getPointNum();
            }
        }
    }
    return pointNum;
}

lu_uint32_t GeminiDatabase::getCLogicNum()
{
    return controlLogic.getSize();
}

POINT_TYPE GeminiDatabase::getPointType(const PointIdStr pointID)
{
    POINT_TYPE ret = POINT_TYPE_INVALID;

    IPoint* pointPtr = getPoint(pointID);
    if(pointPtr != NULL)
    {
        /* Valid point - Read its type back */
        ret = pointPtr->getType();
    }
    return ret;
}


GDB_ERROR GeminiDatabase::getValue(const PointIdStr pointID, PointData *valuePtr)
{
    GDB_ERROR ret = GDB_ERROR_NOPOINT;

    if(valuePtr == NULL)
    {
        return GDB_ERROR_PARAM;
    }

    IPoint* pointPtr = getPoint(pointID);
    if(pointPtr != NULL)
    {
        /* Valid point - Read the value back */
        ret = pointPtr->getValue(valuePtr);
    }
    return ret;
}


GDB_ERROR GeminiDatabase::getRAWValue(const PointIdStr pointID, PointData* valuePtr)
{
    GDB_ERROR ret = GDB_ERROR_NOPOINT;

    if(valuePtr == NULL)
    {
        return GDB_ERROR_PARAM;
    }

    IPoint* pointPtr = getPoint(pointID);
    if(pointPtr != NULL)
    {
        /* Valid point - Read the value back */
        ret = pointPtr->getRAWValue(valuePtr);
    }
    return ret;
}


GDB_ERROR GeminiDatabase::getFrozenValue(const PointIdStr pointID, PointData* valuePtr)
{
    GDB_ERROR ret = GDB_ERROR_NOPOINT;

    if(valuePtr == NULL)
    {
        return GDB_ERROR_PARAM;
    }

    IPoint* pointPtr = getPoint(pointID);
    if(pointPtr != NULL)
    {
        /* Valid point - Read the value back */
        ret = pointPtr->getFrozenValue(valuePtr);
    }
    return ret;
}


GDB_ERROR GeminiDatabase::getInputValue(const PointIdStr pointID, PointData *valuePtr)
{
    GDB_ERROR ret = GDB_ERROR_NOPOINT;

    if(valuePtr == NULL)
    {
        return GDB_ERROR_PARAM;
    }

    /* Stand alone virtual point or Control logic virtual point ? */
    if(pointID.group == PointDB::POINT_DB_GROUP)
    {
        /* Feature not supported for virtual points */
        ret = GDB_ERROR_NOT_SUPPORTED;
    }
    else
    {
        /* Get the Control logic virtual point from the database */
        ControlLogic *CLogicPtr = controlLogic.getCLogic(pointID.group);
        if(CLogicPtr != NULL)
        {
            /* Valid point - Read the value back */
            ret = CLogicPtr->getInputValue(pointID.ID, valuePtr);
            if(ret != GDB_ERROR_NONE)
            {
                valuePtr->setQuality(POINT_QUALITY_INVALID);
            }
        }
    }

    /* The point doesn't exist */
    if(ret == GDB_ERROR_NOPOINT)
    {
        log.info("%s: Point %s not found", __AT__, pointID.toString().c_str());
    }

    return ret;
}


GDB_ERROR GeminiDatabase::getOutputValue(const PointIdStr pointID, PointData *valuePtr)
{
    GDB_ERROR ret = GDB_ERROR_NOPOINT;

    if(valuePtr == NULL)
    {
        return GDB_ERROR_PARAM;
    }

    /* Stand alone virtual point or Control logic virtual point ? */
    if(pointID.group == PointDB::POINT_DB_GROUP)
    {
        /* Feature not supported for virtual points */
        ret = GDB_ERROR_NOT_SUPPORTED;
    }
    else
    {
        /* Ask directly to the Control logic */
        ControlLogic *CLogicPtr = controlLogic.getCLogic(pointID.group);
        if(CLogicPtr != NULL)
        {
            /* Valid logic item - Read the value & status back */
            ret = CLogicPtr->getOutputValue(valuePtr);
            if(ret != GDB_ERROR_NONE)
            {
                valuePtr->setQuality(POINT_QUALITY_INVALID);
            }
        }
    }

    /* The point doesn't exist */
    if(ret == GDB_ERROR_NOPOINT)
    {
        log.info("%s: Point %s not found", __AT__, pointID.toString().c_str());
    }

    return ret;
}


const lu_char_t* GeminiDatabase::getValueLabel(const PointIdStr pointID, PointData* valuePtr)
{
    if( (valuePtr != NULL) && pointID.isValid() )
    {
        IPoint* pointPtr = getPoint(pointID);
        if(pointPtr != NULL)
        {
            return pointPtr->getLabel(valuePtr).c_str();
        }
        else
        {
            log.debug("%s Point %i:%i not found", pointID.toString().c_str());
        }
    }
    return "";
}


GDB_ERROR GeminiDatabase::getPointRange(const PointIdStr pointID, PointData* minValuePtr, PointData* maxValuePtr)
{
    GDB_ERROR ret = GDB_ERROR_NOPOINT;
    if( !pointID.isValid() || (minValuePtr == NULL) || (maxValuePtr == NULL) )
    {
        return GDB_ERROR_PARAM;
    }
    IPoint* pointPtr = getPoint(pointID);
    if(pointPtr != NULL)
    {
        /* Valid point - Read the value back */
        ret = pointPtr->getRange(minValuePtr, maxValuePtr);
    }
    return ret;
}


GDB_ERROR GeminiDatabase::attach(const PointIdStr pointID, IPointObserver *observer)
{
    GDB_ERROR ret = GDB_ERROR_NOPOINT;

    if(observer == NULL)
    {
        return GDB_ERROR_PARAM;
    }

    IPoint* pointPtr = getPoint(pointID);
    if(pointPtr != NULL)
    {
        /* Valid point - attach the observer */
        ret = pointPtr->attach(observer);
    }
    return ret;
}


GDB_ERROR GeminiDatabase::detach(const PointIdStr pointID, IPointObserver* observer)
{
    GDB_ERROR ret = GDB_ERROR_NOPOINT;

    IPoint* pointPtr = getPoint(pointID);
    if(pointPtr != NULL)
    {
        /* Valid point - attach the observer */
        ret = pointPtr->detach(observer);
    }
    return ret;
}


CONTROL_LOGIC_TYPE GeminiDatabase::getType(const lu_uint16_t groupID)
{
    CONTROL_LOGIC_TYPE ret = CONTROL_LOGIC_TYPE_INVALID;
    ControlLogic *CLogicPtr;

    /* Check if this is Virtual Point's group */
    if(groupID == 0)
    {
        return CONTROL_LOGIC_TYPE_DB;
    }

    /* It's not VP group. Search for a control logic */
    CLogicPtr = controlLogic.getCLogic(groupID);

    if(CLogicPtr != NULL)
    {
        /* Valid block - Get type */
        ret = CLogicPtr->getType();
    }

    return ret;
}


const lu_char_t* GeminiDatabase::getTypeName(const lu_uint16_t groupID)
{
    CONTROL_LOGIC_TYPE type = getType(groupID);

    if(type < CONTROL_LOGIC_TYPE_LAST)
    {
        return CONTROL_LOGIC_TYPE_ToSTRING(type);
    }
    else
    {
        return CONTROL_LOGIC_TYPE_ToSTRING(CONTROL_LOGIC_TYPE_INVALID);   //unsupported type;
    }
}


lu_bool_t GeminiDatabase::isOperatable(const lu_uint16_t groupID)
{
    lu_bool_t ret = LU_FALSE;
    ControlLogic *CLogicPtr;

    CLogicPtr = controlLogic.getCLogic(groupID);
    if(CLogicPtr != NULL)
    {
        /* Valid block - Get type */
        ret = (CLogicPtr->isOperatable())? LU_TRUE : LU_FALSE;
    }
    return ret;
}


GDB_ERROR GeminiDatabase::getLocalRemote(OLR_STATE& status)
{
    GDB_ERROR ret = GDB_ERROR_NOPOINT;
    status = OLR_STATE_OFF;

    IPoint* olrPoint = getPoint(PointIdStr(groupOLR, OLR_POINT_OFFLOCALREMOTE));
    if(olrPoint != NULL)
    {
        /* Valid point - Get the status */
        PointDataUint8 olr;
        olr = OLR_STATE_OFF;
        ret = olrPoint->getValue(&olr);
        status = static_cast<OLR_STATE>(static_cast<lu_uint8_t>(olr));
    }
    return ret;
}


GDB_ERROR GeminiDatabase::setLocalRemote(const OLR_STATE status, const OLR_SOURCE source)
{
    GDB_ERROR ret;
    ControlLogic *CLogicPtr;

    CLogicPtr = controlLogic.getCLogic(groupOLR);
    if(CLogicPtr != NULL)
    {
        /* Valid point - Send the command */
        ret = CLogicPtr->setLocalRemote(status, source);
    }
    else
    {
        /* The point doesn't exist */
        ret = GDB_ERROR_NOPOINT;
    }

    return ret;
}


GDB_ERROR GeminiDatabase::startOperation(const lu_uint16_t groupID,
                                         SwitchLogicOperation &operation
                                       )
{
    GDB_ERROR ret = GDB_ERROR_NOPOINT;
    ControlLogic *CLogicPtr;

    CLogicPtr = controlLogic.getCLogic(groupID);

    if(CLogicPtr != NULL)
    {
        /* Valid point - Send the command */
        ret = CLogicPtr->startOperation(operation);
    }

    return ret;
}

GDB_ERROR GeminiDatabase::writeAnalogOperation(const lu_uint16_t groupID, AnalogueOutputOperation &value)
{
    GDB_ERROR ret;
    ControlLogic *clogic;

    clogic = controlLogic.getCLogic(groupID);

    if(clogic != NULL)
    {
        if(clogic-> isOperatable())
            ret = ((OperatableControlLogic*)clogic)->writeOutputValue(value);
        else
            ret = GDB_ERROR_NOT_SUPPORTED;
    }
    else
    {
        ret = GDB_ERROR_NOPOINT;
    }

    return ret;
}


GDB_ERROR GeminiDatabase::freeze(const PointIdStr pointID, const lu_bool_t andClear)
{
    GDB_ERROR ret = GDB_ERROR_NOPOINT;

    IPoint* pointPtr = getPoint(pointID);
    if(pointPtr != NULL)
    {
        /* Point exists: try to freeze it */
        ret = pointPtr->freeze(andClear == LU_TRUE);
    }
    return ret;
}


GDB_ERROR GeminiDatabase::clear(const PointIdStr pointID)
{
    GDB_ERROR ret = GDB_ERROR_NOPOINT;

    IPoint* pointPtr = getPoint(pointID);
    if(pointPtr != NULL)
    {
        /* Point exists: try to reset it */
        ret = pointPtr->clear();
    }
    return ret;
}


GDB_ERROR GeminiDatabase::stop()
{
    /* Stop all the points */
    points.stop();
    controlLogic.stop();

    return GDB_ERROR_NONE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
IPoint* GeminiDatabase::getPoint(const PointIdStr pointID)
{
    IPoint* pointPtr = NULL;
    if(pointID.isValid())
    {
        /* Stand alone virtual point or Control logic virtual point ? */
        if(pointID.group == PointDB::POINT_DB_GROUP)
        {
            /* Get the point from the database */
            pointPtr = points.getPoint(pointID.ID);
        }
        else
        {
            /* Get the Control logic virtual point from the database */
            ControlLogic *CLogicPtr = controlLogic.getCLogic(pointID.group);
            if(CLogicPtr != NULL)
            {
                /* Valid point - Read the value back */
                pointPtr = CLogicPtr->getPoint(pointID);
            }
        }
    }
    return pointPtr;
}


#ifdef G3DBSERVER
namespace g3db
{
lu_bool_t ReadPointHandler::handle(DBQuery& query)
{
    static PointIdStr point;       //temporal pointID for get value from G3DB
    static POINT_TYPE type;
    static TimeManager::TimeStr ts;

    PointData* data = NULL;
    point.ID = query.ref.id;
    point.group = query.ref.group;

    if(query.reqId != DB_REQUEST_ID_READ_POINT)
        return false;

    type = INSTANCE->getPointType(point);

    switch(type)
    {
        case POINT_TYPE_ANALOGUE:
            data = new PointDataFloat32();
            break;

        case POINT_TYPE_BINARY:
        case POINT_TYPE_DBINARY:
            data = new PointDataUint8();
            break;

        case POINT_TYPE_COUNTER:
            data = new PointDataCounter32();
            break;

        default:
            query.reqStatus = DB_REQUEST_STATUS_INVALID_REQUEST;
            query.value.type = VALUE_TYPE_INVALID;
            return true;
    }

    if(data != NULL)
    {
        INSTANCE->getValue(point, data);
        if(type == POINT_TYPE_ANALOGUE)
        {
            query.value.type =  VALUE_TYPE_ANALOGUE;
            query.value.analogue = *data;
        }
        else
        {
            query.value.type =  VALUE_TYPE_DIGITAL;
            query.value.digital = *data;
        }

        query.quality.online = data->getFlags().getQuality() == POINT_QUALITY_ON_LINE;
        data->getTime(ts);
        query.timestamp.value = ts.time;

        delete data;
    }

    return true;
}

}// End of namespace g3db

#endif // G3DBSERVER
/*
 *********************** End of file ******************************************
 */
