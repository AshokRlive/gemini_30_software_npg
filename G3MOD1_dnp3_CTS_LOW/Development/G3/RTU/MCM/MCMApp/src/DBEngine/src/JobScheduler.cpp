/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Job scheduler module implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "timeOperations.h"
#include "JobScheduler.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

JobScheduler::JobScheduler(SCHED_TYPE schedType, lu_uint32_t priority) :
                                            Thread(schedType, priority, LU_FALSE, "G3DBJobScheduler"),
                                            log(Logger::getLogger(SUBSYSTEM_ID_G3DB))
{}

JobScheduler::~JobScheduler()
{
    stop();
}

THREAD_ERR JobScheduler::join()
{
    return stop();
}

THREAD_ERR JobScheduler::stop()
{
    /* Note: all jobs in the event queue are destroyed when deleting the event queue */
    THREAD_ERR result;
    result = Thread::stop();
    result = Thread::join();

    /* TODO: pueyos_a - log error message */

    return result;
}


GDB_ERROR JobScheduler::addJob(IJob* jobPtr)
{
    GDB_ERROR ret;

    if(jobPtr == NULL)
    {
        return GDB_ERROR_PARAM;
    }

    /* Add the job to the queue */
    if(jobs.push(std::auto_ptr<IJob>(jobPtr)) == LU_TRUE)
    {
        ret= GDB_ERROR_NONE;
    }
    else
    {
        ret = GDB_ERROR_ADDJOB;
    }

    return ret;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void JobScheduler::threadBody()
{
    const lu_char_t *FTITLE = "JobScheduler::threadBody:";

    struct timeval timeout = {0, 100000};
    lu_int32_t result;

    log.info("%s Job scheduler running...", FTITLE);

    while(isRunning() == LU_TRUE)
    {
        { //Scope of jobPtr defined here to force jobPtr to be deleted if sudden exit
            /* Wait for a new job */
            std::auto_ptr<IJob> jobPtr = jobs.popLock(timeout, result);

            if(isInterrupting() == LU_TRUE)
                continue;   //exit thread

            switch(result)
            {
                case OBJECTQUEUERESULT_OK:
                    if(jobPtr.get() != NULL)
                    {
                        /* Trace when we execute a job */
                        log.debug("%s run job @ %p", FTITLE, jobPtr.get());

                        /* Run the job */
                        jobPtr.get()->run();
                        /* Note: if the loop is interrupted at this point, jobPtr
                         *      will be deleted automatically anyway */
                    }
                    break;
                default:
                    break;
            }
        }//End of jobPtr scope: Job is already deleted in the jobPtr destructor
    }
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
