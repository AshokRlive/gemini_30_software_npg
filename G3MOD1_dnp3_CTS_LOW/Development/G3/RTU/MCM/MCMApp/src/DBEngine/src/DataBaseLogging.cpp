/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Status Manager - Database Logging module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   09/05/12      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "TimeManager.h"
#include "DataBaseLogging.h"
#include "Logger.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

class printPointObserver : public IPointObserver
{
public:
    /**
     * \brief Custom constructor
     */
    printPointObserver(GeminiDatabase &g3database) :
                        log(Logger::getLogger(SUBSYSTEM_ID_VPOINTVAL)),
                        database(g3database)
    {}

    /**
     * \brief log a change in a point when it updates its content
     *
     * \param pointID Point ID
     * \param pointDataPtr reference to the Point data
     *
     * \return None
     */
    virtual void update(PointIdStr pointID, PointData *pointDataPtr)
    {
        if(pointDataPtr == NULL)
            return;

        TimeManager::TimeStr timestamp;
        pointDataPtr->getTime(timestamp);
        POINT_TYPE type = database.getPointType(pointID);
        switch(type)
        {
            case POINT_TYPE_BINARY:
            case POINT_TYPE_DBINARY:
            {
                std::string edgeStr;
                switch (pointDataPtr->getEdge())
                {
                   case EDGE_TYPE_POSITIVE:
                       edgeStr = "P";
                   break;

                   case EDGE_TYPE_NEGATIVE:
                       edgeStr = "N";
                   break;

                   default:
                       edgeStr = "NA";
                   break;
                }

                log.info("DPoint(%s)=%u %s Edge:%s @ %s",
                            pointID.toString().c_str(),
                            (DigitalPointValue)(*pointDataPtr),
                            pointDataPtr->getFlags().toString().c_str(),
                            edgeStr.c_str(),
                            timestamp.toString(true).c_str()
                          );
            break;
            }
            case POINT_TYPE_ANALOGUE:
            {
                AFILTER_RESULT afResult = pointDataPtr->getAFilterStatus();
                if( (afResult == AFILTER_RESULT_IN_LIMIT_THR) ||
                    (afResult == AFILTER_RESULT_OUT_LIMIT_THR)
                   )
                {
                    log.info("APoint(%s)=%f %s Filt:%s @ %s",
                                pointID.toString().c_str(),
                                (AnaloguePointValue)(*pointDataPtr),
                                pointDataPtr->getFlags().toString().c_str(),
                                (afResult == AFILTER_RESULT_IN_LIMIT_THR)? "In" : "Out",
                                timestamp.toString(true).c_str()
                              );
                }
                break;
            }
            case POINT_TYPE_COUNTER:
            {
                PointDataCounter32 current;
                PointDataCounter32 frozen;
                database.getRAWValue(pointID, &current);
                database.getFrozenValue(pointID, &frozen);
                log.info("CPoint(%s)=%lu %s (Curr=%lu %s|Froz=%lu %s) @ %s",
                            pointID.toString().c_str(),
                            (CounterPointValue)(*pointDataPtr),
                            pointDataPtr->getFlags().toString().c_str(),
                            (CounterPointValue)(current), current.getFlags().toString().c_str(),
                            (CounterPointValue)(frozen), frozen.getFlags().toString().c_str(),
                            timestamp.toString(true).c_str()
                          );
                break;
            }
            default:
                break;
        }
        return;
    }

    virtual PointIdStr getPointID()
    {
        return pointID;
    }

private:
    Logger& log;
    GeminiDatabase &database;
    PointIdStr pointID;
};


DataBaseLogging::DataBaseLogging(GeminiDatabase &database)
{
	PointIdStr pointIdx;

	for( pointIdx.group = 0;
		 pointIdx.group <= database.getCLogicNum();
		 ++pointIdx.group
	   )
	{
        for( pointIdx.ID = 0;
             pointIdx.ID < database.getVPointNum(pointIdx.group);
             ++pointIdx.ID
           )
        {
            printPointObserver *printPointObserverPtr = new printPointObserver(database);
            if(database.attach(pointIdx, printPointObserverPtr) != GDB_ERROR_NONE)
            {
                delete printPointObserverPtr;
            }
        }
	}
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */

