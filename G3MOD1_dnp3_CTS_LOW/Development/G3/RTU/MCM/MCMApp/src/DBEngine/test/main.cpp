#include <cstdlib>
#include <cstring>
#include <unistd.h>

#include <pthread.h>

#include "RAWModuleMessage.h"
#include "CANFraming.h"
//#include "SysLogManagerPrintf.h"
#include "CANIOModuleManager.h"
#include "CANIOModuleFactory.h"
#include "IOData.h"
#include "GeminiDatabase.h"
#include "ConfigurationManager.h"
#include "LocalRemoteControlLogic.h"

#include "ModuleManager.h"

#include "SCADAProtocolManager.h"
#include "SlaveProtocolManagerFactory.h"
#include "DummyDNP3SlaveProtocolManagerFactoy.h"

#include "ConfigurationToolHandler.h"
#include "TimeManager.h"


class printPointObserver : public IPointObserver
{
public:
    virtual void update(PointIdStr pointID, PointData *pointDataPtr)
    {
        lu_char_t buffer[50];
        TimeManager::TimeStr timestamp;
        AFILTER_RESULT afResult;
        pointDataPtr->getTime(timestamp);

        afResult = pointDataPtr->getAFilterStatus();

        if( (afResult == AFILTER_RESULT_NA)           ||
            (afResult == AFILTER_RESULT_IN_LIMIT_THR) ||
            (afResult == AFILTER_RESULT_OUT_LIMIT_THR)
           )
        {
//            SysLogFactory::getInstance()->getSysLogManager(SUBSYSTEM_ID_VPOINTVAL)->logMsg(
//                                        LOG_LEVEL_INFO, SUBSYSTEM_ID_VPOINTVAL,
//                                        "Point %i:%i observer changed: %i - Value: %i(%f) - Chatter: %i - Edge: %i - Overflow: %i - Filter: %i - Synch: %i: Time: %s",
//                                        pointID.group, pointID.ID,
//                                        pointDataPtr->getInitialFlag(),
//                                        (lu_uint32_t)(*pointDataPtr),
//                                        (lu_float32_t)(*pointDataPtr),
//                                        pointDataPtr->getChatterFlag(),
//                                        pointDataPtr->getEdge(),
//                                        pointDataPtr->getOverflowFlag(),
//                                        pointDataPtr->getAFilterStatus(),
//                                        timestamp.synchronized,
//                                        timestamp.toString().c_str()
//                                      );
        }

        return;
    }

    virtual PointIdStr getPointID()
    {
        return pointID;
    }

private:

    PointIdStr pointID;
};


int main()
{
    SIG_BlockAllExceptTerm();

//    /* Initialize syslog */
//    SysLogManagerPrintf::getInstance()->setLogLevel(LOG_LEVEL_INFO);

    /* Start configuration manager */
    ConfigurationManager configurationManager("configTest02.xml");

    /* Initialize Module manager and start discovery */
    ModuleManager moduleManager( SCHED_TYPE_FIFO,
                                 40,
                                 CANIOModuleFactory::getInstance(),
                                 configurationManager,
                                 MODULE_MANAGER_SIGNAL
                                );
    moduleManager.discover();

    /* wait for the a PSM Module */
//    SysLogManagerPrintf::getInstance()->logMsg( LOG_LEVEL_INFO,
//                                                SUBSYSTEM_ID_TEST,
//                                                "Wait for a PSM board..."
//                                              );
    IIOModule *psmPtr;
    while( (psmPtr = moduleManager.getModule(MODULE_PSM, MODULE_ID_0)) == NULL)
    {
        usleep(100000);
    }

    /* Wait for all the boards to be initialised */
    usleep(1500000);

    /* Initialize gemini database */
    GeminiDatabase database(SCHED_TYPE_FIFO, 30, GEMINI_DB_SIGNAL);

    /* Configure gemini database */
    configurationManager.init(moduleManager, database);

    /* Configure protocol manager */
    SCADAProtocolManager PManager;
    configurationManager.init(PManager, SCHED_TYPE_FIFO, 20, database);

    /* Release configuration manager resources */
    configurationManager.releaseParser();

    //printPointObserver pointObserver;

    PointIdStr pointIdx;
    for( pointIdx.ID = 0, pointIdx.group = 0;
         pointIdx.ID < database.getVPointNum();
         ++pointIdx.ID
       )
    {
        database.attach(pointIdx, new printPointObserver());
    }

    /* Off/Local/Remote vpoints observer */
#if 1 //MG

    pointIdx.group = 1;
    pointIdx.ID = OLR_DPOINT_OFF;
    database.attach(pointIdx, new printPointObserver());
    pointIdx.ID = OLR_DPOINT_LOCAL;
    database.attach(pointIdx, new printPointObserver());
    pointIdx.ID = OLR_DPOINT_REMOTE;
    database.attach(pointIdx, new printPointObserver());
    pointIdx.ID = OLR_DPOINT_OFFLOCALREMOTE;
    database.attach(pointIdx, new printPointObserver());


    //pointIdx.group = 2;
    //pointIdx.ID = 0;
    //database.attach(pointIdx, new printPointObserver());

    //pointIdx.group = 3;
    //pointIdx.ID = 0;
    //database.attach(pointIdx, new printPointObserver());
#endif

    /* Switch gear logic vpoints observer */
#if 1 //MG
    pointIdx.group = 2;
    for(lu_uint32_t i = 0; i < 3; ++i)//MG
    {
        pointIdx.ID = i;
        database.attach(pointIdx, new printPointObserver());
    }
#endif

    /* Test ConfigurationToolHandler */
    ConfigurationToolHandler configTool(moduleManager, database);

    /* Start protocol stack */
    PManager.startProtocolStack();

#if 0
    while(LU_TRUE)
    {
#if 0
        IOM_ERROR ret;
        TimeManager::TimeStr absTime;

        if(module != NULL)
        {
//            SysLogManagerPrintf::getInstance()->logMsg(
//                                        LOG_LEVEL_INFO,
//                                        SUBSYSTEM_ID_TEST,
//                                        "Ping result: %i",
//                                        module->ping(100, 100)
//                                                       );


            ret = module->getTime(&absTime, 100);
            if(ret == IOM_ERROR_NONE)
            {
//                SysLogManagerPrintf::getInstance()->logMsg(
//                                                    LOG_LEVEL_INFO,
//                                                    SUBSYSTEM_ID_TEST,
//                                                    &absTime.time,
//                                                    "Get time. Synch: %s",
//                                                    (absTime.synchronized == LU_TRUE) ? "Yes" : "No"
//                                                          );
            }
            else
            {
//                SysLogManagerPrintf::getInstance()->logMsg(
//                                                       LOG_LEVEL_INFO,
//                                                       SUBSYSTEM_ID_TEST,
//                                                       "Get time error: %i",
//                                                       ret
//                                                          );
            }
        }
#endif

        struct timeval timeout = {0  , 100000};
        //database.startOperation(3, &timeout, 2);
#if 0
        psmPtr->getChannel(CHANNEL_TYPE_PSUPPLY, PSM_CH_PSUPPLY_MOTOR)->select(&timeout, 10, LU_FALSE);
        psmPtr->getChannel(CHANNEL_TYPE_PSUPPLY, PSM_CH_PSUPPLY_MOTOR)->operate(&timeout, 10, 100, LU_FALSE);
        psmPtr->getChannel(CHANNEL_TYPE_PSUPPLY, PSM_CH_PSUPPLY_MOTOR)->cancel(&timeout, LU_TRUE);
        psmPtr->getChannel(CHANNEL_TYPE_PSUPPLY, PSM_CH_PSUPPLY_MOTOR)->select(&timeout, 10, LU_TRUE);
        psmPtr->getChannel(CHANNEL_TYPE_PSUPPLY, PSM_CH_PSUPPLY_MOTOR)->select(&timeout, 10, LU_TRUE);
        psmPtr->getChannel(CHANNEL_TYPE_PSUPPLY, PSM_CH_PSUPPLY_MOTOR)->operate(&timeout, 10, 100, LU_TRUE);
        psmPtr->getChannel(CHANNEL_TYPE_PSUPPLY, PSM_CH_PSUPPLY_MOTOR)->select(&timeout, 10, LU_TRUE);
        psmPtr->getChannel(CHANNEL_TYPE_PSUPPLY, PSM_CH_PSUPPLY_MOTOR)->operate(&timeout, 10, 100, LU_TRUE);
        sleep(5);
        psmPtr->getChannel(CHANNEL_TYPE_PSUPPLY, PSM_CH_PSUPPLY_MOTOR)->select(&timeout, 10, LU_TRUE);
        psmPtr->getChannel(CHANNEL_TYPE_PSUPPLY, PSM_CH_PSUPPLY_MOTOR)->operate(&timeout, 10, 100, LU_TRUE);
        sleep(6);
        psmPtr->getChannel(CHANNEL_TYPE_PSUPPLY, PSM_CH_PSUPPLY_MOTOR)->select(&timeout, 10, LU_TRUE);
        psmPtr->getChannel(CHANNEL_TYPE_PSUPPLY, PSM_CH_PSUPPLY_MOTOR)->operate(&timeout, 10, 100, LU_TRUE);
        psmPtr->getChannel(CHANNEL_TYPE_PSUPPLY, PSM_CH_PSUPPLY_MOTOR)->cancel(&timeout, LU_TRUE);
        psmPtr->getChannel(CHANNEL_TYPE_PSUPPLY, PSM_CH_PSUPPLY_MOTOR)->operate(&timeout, 10, 100, LU_TRUE);
        psmPtr->getChannel(CHANNEL_TYPE_PSUPPLY, PSM_CH_PSUPPLY_MOTOR)->operate(&timeout, 10, 100, LU_TRUE);
#endif
        //psmPtr->getChannel(CHANNEL_TYPE_PSUPPLY, PSM_CH_PSUPPLY_MOTOR)->select(&timeout, 3, LU_TRUE);
        //psmPtr->getChannel(CHANNEL_TYPE_PSUPPLY, PSM_CH_PSUPPLY_MOTOR)->operate(&timeout, 3, 100, LU_TRUE);

        //static OLR_State olrStatus = OLR_STATE_OFF;
        //database.setLocalRemote(1, olrStatus);
        //olrStatus = static_cast<OLR_State>((static_cast<int>(olrStatus) + 1) % OLR_STATE_LAST);

        //psmPtr->getChannel(CHANNEL_TYPE_PSUPPLY, PSM_CH_PSUPPLY_MOTOR)->select(&timeout, 10, LU_FALSE);
        //sleep(5);
        //psmPtr->getChannel(CHANNEL_TYPE_PSUPPLY, PSM_CH_PSUPPLY_MOTOR)->operate(&timeout, 100, 100, LU_FALSE);
        //sleep(30);
        //psmPtr->getChannel(CHANNEL_TYPE_PSUPPLY, PSM_CH_PSUPPLY_MOTOR)->cancel(&timeout, LU_FALSE);
        //sleep(30);

        //psmPtr->getChannel(CHANNEL_TYPE_BCHARGER, PSM_CH_BCHARGER_BATTERY_TEST)->select(&timeout, 10, LU_TRUE);
        //sleep(5);
        //psmPtr->getChannel(CHANNEL_TYPE_BCHARGER, PSM_CH_BCHARGER_BATTERY_TEST)->operate(&timeout, 100, 0, LU_TRUE);
        //sleep(30);
        //psmPtr->getChannel(CHANNEL_TYPE_BCHARGER, PSM_CH_BCHARGER_BATTERY_TEST)->cancel(&timeout, LU_TRUE);
        //sleep(30);

        //psmPtr->getChannel(CHANNEL_TYPE_BCHARGER, PSM_CH_BCHARGER_BATTERY_TEST)->select(&timeout, 2, LU_TRUE);
        //sleep(5);
        //psmPtr->getChannel(CHANNEL_TYPE_BCHARGER, PSM_CH_BCHARGER_BATTERY_TEST)->operate(&timeout, 100, 0, LU_TRUE);
        //sleep(30);
        //psmPtr->getChannel(CHANNEL_TYPE_BCHARGER, PSM_CH_BCHARGER_BATTERY_TEST)->cancel(&timeout, LU_TRUE);

        sleep(10);

        SwitchLogicOperation operation;
        operation.operation = SWITCH_OPERATION_OPEN;
        operation.local = LU_TRUE;
        database.startOperation(2, operation);
        sleep(30);
    }
#else
    while(LU_TRUE)
    {
#define HELP_S "h"
#define HELP_L "help"

#define SET_OFF_LOCAL_REMOTE_S "olr"
#define OPERATE_SWITCH_S "os"
#define CONTROL_LOGIC_LIST "clist"
#define MODULE_LIST "mlist"
#define MODULE_FTEST "mftest"

#define CHK_COMMAND(buffer, command) (strncmp (buffer, command, strlen(buffer)) == 0)

        lu_char_t buf[255];

        /* Wait command */
        fprintf(stdout, "\nWait for command...\n");
        fscanf(stdin, "%255s", buf);

        /* Decode command */
        if( (CHK_COMMAND(buf, HELP_S)) ||
            (CHK_COMMAND(buf, HELP_L))
          )
        {
            printf("\nHelp screen:\n");
            printf("\t%s (%s) : This screen\n", HELP_S, HELP_L);
            printf("\t%s : Set Off/Local/Remote status\n", SET_OFF_LOCAL_REMOTE_S);
            printf("\t%s : Operate Switch\n", OPERATE_SWITCH_S);
            printf("\t%s : Modules List\n", MODULE_LIST);
            printf("\t%s : Control Logic Blocks List\n", CONTROL_LOGIC_LIST);
            printf("\t%s : PSM Module Fan Test\n", MODULE_FTEST);

            printf("\n");
        }
        else if(CHK_COMMAND(buf, MODULE_LIST))
        {
            std::vector<IIOModule*> moduleList;
            moduleList = moduleManager.getAllModules();
            printf("Modules %i:\n", moduleList.size());
            for(lu_uint32_t i = 0; i < moduleList.size(); ++i)
            {
                IOModuleInfoStr version;
                moduleList[i]->getInfo(&version);
                printf("\tType: %i - ID: %i - serial: %s online: %i - FW version: %i.%i.%i\n",
                        version.type, version.id, version.moduleUID.toString().c_str(), version.online,
                        version.softwareVersion.major,
                        version.softwareVersion.minor,
                        version.patch
                        );
            }

            printf("\n");
        }
        else if(CHK_COMMAND(buf, CONTROL_LOGIC_LIST))
        {
            lu_uint32_t cLogicNum = database.getCLogicNum();

            printf("Control logic blocks %i:\n", cLogicNum);
            for(lu_uint32_t i = 0; i <= cLogicNum; ++i)
            {
                printf("\tIndex %i %s\n", i, database.getTypeName(i));
            }

            printf("\n");
        }
        else if(CHK_COMMAND(buf, OPERATE_SWITCH_S))
        {
            lu_uint32_t clogic, operation, local;

            lu_uint32_t cLogicNum = database.getCLogicNum();

            fprintf(stdout, "\tCLogic block (");
            for(lu_uint32_t i = 0; i <= cLogicNum; ++i)
            {
                if(database.getType(i) == CONTROL_LOGIC_TYPE_SGL)
                {
                    printf(" %i -", i);
                }
            }
            printf("): ");

            fscanf(stdin, "%u", &clogic);
            fprintf(stdout, "\tOperation(0 open - 1 close): ");
            fscanf(stdin, "%u", &operation);
            fprintf(stdout, "\tLocal(0 remote - 1 local): ");
            fscanf(stdin, "%u", &local);

            SwitchLogicOperation sglOperation;
            sglOperation.operation = (operation == 0) ? SWITCH_OPERATION_OPEN : SWITCH_OPERATION_CLOSE;
            sglOperation.local = local;

            fprintf(stdout, "Starting operation. clogic: %i - operation: %i - local: %i\n", clogic, operation, local);
            GDB_ERROR ret = database.startOperation(clogic, sglOperation);
            if(ret == GDB_ERROR_NONE)
            {
                fprintf(stdout, "Operation started...\n");
            }
            else
            {
                fprintf(stdout, "Operation error: %i\n", ret);
            }
        }
        else if(CHK_COMMAND(buf, SET_OFF_LOCAL_REMOTE_S))
        {
            lu_uint32_t olr;
            lu_uint32_t i;


            lu_uint32_t cLogicNum = database.getCLogicNum();

            for(i = 0; i <= cLogicNum; ++i)
            {
                if(database.getType(i) == CONTROL_LOGIC_TYPE_OLR)
                {
                    fprintf(stdout, "\tSet Off/Local/Remote (Off(0), Local(1), Remote(2): ");
                    fscanf(stdin, "%u", &olr);

                    GDB_ERROR ret = database.setLocalRemote(i, static_cast<OLR_State>(olr));
                    if(ret == GDB_ERROR_NONE)
                    {
                        fprintf(stdout, "status set correctly\n");
                    }
                    else
                    {
                        fprintf(stdout, "Operation error: %i\n", ret);
                    }

                    break;
                }
            }

            if(i >= cLogicNum)
            {
                fprintf(stdout, "Off/Local/Remote Control Logic NOT found\n");
            }

        }
        else if(CHK_COMMAND(buf, MODULE_FTEST))
        {
            struct timeval timeout = {0, 100000};
            printf("Start Module fan test");

            moduleManager.getModule(MODULE_PSM, MODULE_ID_0)->getChannel(CHANNEL_TYPE_FAN, PSM_CH_FAN_FAN_TEST)->startOperation(&timeout, 5);
        }
        else
        {
            fprintf(stdout, "Unsupported command: %s\n", buf);
        }
    }
#endif

    sleep(10);

    return 0;

}
