/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Timed scheduler implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <algorithm> // std::find

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "TimedJobScheduler.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */


TimedJob::TimedJob(lu_uint32_t timer, lu_uint32_t timerInterval) :
                                                   timerInitial(timer + TimedJobScheduler::JobSchedulerTick),
                                                   timerInterval(timerInterval),
                                                   counter(timerInitial)
{}


TIMED_JOB_ERROR TimedJob::run(lu_uint32_t dTime, TimeManager::TimeStr* timePtr)
{
    TIMED_JOB_ERROR ret = TIMED_JOB_ERROR_NONE;

    if(timePtr == NULL)
    {
        return TIMED_JOB_ERROR_PARAM;
    }

    /* Is the timer expired ? */
    if(counter >= dTime)
    {
        /* No - Update timer */
        counter -= dTime;
    }
    else
    {
        /* Yes - run the custom job */
        job(timePtr);

        /* Update timer */
        if(timerInterval != 0)
        {
            /* Periodic task. Restart the internal timer */
            counter = timerInterval;
        }
        else
        {
            /* One shoot task. remove from the queue */
            ret = TIMED_JOB_ERROR_REMOVE;
        }
    }

    return ret;
}


TimedJobScheduler::TimedJobScheduler( SCHED_TYPE schedType,
                                      lu_uint32_t priority
                                    ) :
                              Thread(schedType, priority, LU_FALSE, "G3DBTimedJobScheduler"),
                              tickTimer(0, JobSchedulerTick * 1000, Timer::TIMER_TYPE_PERIODIC),
                              log(Logger::getLogger(SUBSYSTEM_ID_G3DB)),
                              rqCancelPipe(Pipe::PIPE_TYPE_NONBLOCKING)
{}


TimedJobScheduler::~TimedJobScheduler()
{
    this->stop();
}

THREAD_ERR TimedJobScheduler::join()
{
    return this->stop();
}

THREAD_ERR TimedJobScheduler::stop()
{
    THREAD_ERR result;
    result = Thread::stop();
    result = Thread::join();

    /* TODO: pueyos_a - log error message */

    /* Delete all jobs in the queue */
    TimedJob *jobPtr;
    TimedJob *jobNextPtr;
    jobPtr = jobs.getFirst();
    while(jobPtr != NULL)
    {
        jobNextPtr = jobs.getNext(jobPtr);
        jobs.dequeue(jobPtr);
        delete jobPtr;

        jobPtr = jobNextPtr;
    }
    return result;
}


GDB_ERROR TimedJobScheduler::addJob(TimedJob* jobPtr)
{
    GDB_ERROR ret;

     if(jobPtr == NULL)
     {
         return GDB_ERROR_PARAM;
     }

     /* Add the job to the queue */
     if(jobs.enqueue(jobPtr) == LU_TRUE)
     {
         ret = GDB_ERROR_NONE;
     }
     else
     {
         ret = GDB_ERROR_ADDJOB;
     }

     return ret;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void TimedJobScheduler::threadBody()
{
    const lu_char_t *FTITLE = "TimedJobScheduler::threadBody:";
    TimedJob *jobPtr;
    TimeManager::TimeStr currentTime;

    log.info("%s Timed Job scheduler running...", FTITLE);

    /* Start the tick timer */
    lu_int32_t res;
    res = tickTimer.start();
    if(res < 0)
    {
        log.fatal("Timer Job Scheduler Timer unavailable");
        return;
    }

    while(isRunning() == LU_TRUE)
    {
        /* Wait for the timer tick */
        tickTimer.wait();

        if(isInterrupting() == LU_TRUE)
            continue;   //exit thread

        /* Get current time */
        TimeManager::getInstance().getTime(currentTime);

        /* Log the tick */
        //log.debug("%s Timed Job scheduler tick", FTITLE);

        /* Get the first job */
        jobPtr = jobs.getFirst();

        /* Go through job queue to run them */
        while(jobPtr != NULL)
        {
            if(isInterrupting() == LU_TRUE)
            {
                jobPtr= NULL;  //exit thread
                continue;
            }

            /* Check if the current Job has been requested to be cancelled */
            getCancelRequests();
            TIMED_JOB_ERROR res = TIMED_JOB_ERROR_NONE;
            for(JobVector::iterator it = cancelList.begin(), end = cancelList.end();
                            it != end; it++)
            {
                if(*it == jobPtr)
                {
                    cancelList.erase(it);           //remove request
                    res = TIMED_JOB_ERROR_REMOVE;   //do not run Job and remove it
                    break;
                }
            }

            /* Run Job (if it was not cancelled/removed) */
            if(res != TIMED_JOB_ERROR_REMOVE)
            {
                res = jobPtr->run(JobSchedulerTick, &currentTime);
            }

            /* Check Job outcome */
            if(res == TIMED_JOB_ERROR_REMOVE)
            {
                /* Job cancelled or finished run */

                TimedJob *removeJob = jobPtr;
                jobPtr = jobs.getNext(jobPtr); /* Get the next job */
                /* Remove the old job from the scheduler queue */
                jobs.dequeue(removeJob);
                delete removeJob;   /* Delete removed the job */
            }
            else
            {
                /* Simply get the next job */
                jobPtr = jobs.getNext(jobPtr);
            }
        } //Endwhile Job launching loop

        /* Review all pending cancel requests */
        getCancelRequests();
        jobPtr = jobs.getFirst();
        while(jobPtr != NULL)
        {
            /* Read through queue, checking for each job being in the vector */
            if(isInterrupting() == LU_TRUE)
            {
                /* Note: If we are interrupting, the destructor will empty the
                 *      job queue (cancel all), and what's left in the cancel
                 *      list is not important anymore.
                 */
                jobPtr= NULL;  //exit thread
                continue;
            }
            std::vector<TimedJob*>::iterator it;
            it = std::find(cancelList.begin(), cancelList.end(), jobPtr);
            if(it != cancelList.end())
            {
                //Found: remove from queue
                TimedJob *removeJob = jobPtr;
                jobPtr = jobs.getNext(jobPtr);  //Get the next job
                jobs.dequeue(removeJob);
                delete removeJob;
            }
            jobPtr = jobs.getNext(jobPtr);
        }
        cancelList.clear(); //The rest is discarded (not really in the job queue)
    }
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void TimedJobScheduler::getCancelRequests()
{
    TimedJob* jobPtr = NULL;
    lu_uint8_t* pointerPtr = (lu_uint8_t*)&jobPtr;
    lu_int32_t ret;
    do
    {
        /* Please note that it is storing pointers and not objects */
        ret = rqCancelPipe.readP(pointerPtr, sizeof(jobPtr));
        if(ret >= 0)
        {
            cancelList.push_back(jobPtr);
        }
    } while (ret >= 0);
}




/*
 *********************** End of file ******************************************
 */
