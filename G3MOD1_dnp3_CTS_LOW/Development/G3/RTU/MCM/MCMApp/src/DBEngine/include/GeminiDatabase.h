/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_27FC4164_92A8_4ced_87A3_649FCE429249__INCLUDED_)
#define EA_27FC4164_92A8_4ced_87A3_649FCE429249__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Mutex.h"
#include "MainAppEnum.h"
#include "IPoint.h"
#include "IPointFactory.h"
#include "GDBScheduler.h"
#include "PointDB.h"
#include "ControlLogic.h"
#include "ControlLogicDB.h"
#include "Logger.h"

/* Forward declaration */
class IControlLogicFactory;

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/**
 * IJob and timedJob are "disposable" objects. They are dynamically created in the
 * application and then executed and removed from the system by the scheduler.
 * Once added to the scheduler the application shouldn't use these objects anymore.
 * The scheduler is in complete control of the object.
 */
class GeminiDatabase
{

public:
    /**
	 * \brief Private Constructor
	 * 
	 * \param schedType Scheduler type used by the internal thread
	 * \param priority thread priority
	 */
    GeminiDatabase(SCHED_TYPE schedType, lu_uint32_t priority);

    virtual ~GeminiDatabase();

    /**
	 * \brief Initialise the database
	 * 
	 * A database of the provided size is created. The database is filled using the
	 * factory class provided
	 * 
	 * \param pointFactory Virtual point factory class
	 * \param CLogicFactory Control Logic block factory class
	 * 
	 * \return Error code
	 */
    GDB_ERROR initialize( IPointFactory &pointFactory,
                          IControlLogicFactory &CLogicFactory
                        );

    /**
     * \brief Return the number of configured virtual points for a group
     *
     * \param groupID Point group.
     *
     * \return Virtual points number
     */
    lu_uint32_t getVPointNum(const lu_uint16_t groupID);

    /**
     * \brief Return the number of control logic blocks
     *
     * \return Control logic blocks number
     */
    lu_uint32_t getCLogicNum();

    /**
	 * \brief Get the type of the Virtual Point
	 * 
	 * \param PointID Point ID
	 * 
	 * \return Point type, POINT_TYPE_INVALID if not available
	 */
    POINT_TYPE getPointType(const PointIdStr pointID);

    /**
     * \brief Read the value of a virtual point
     *
     * \param PointID Point ID
     * \param valuePtr Pointer to the object where the vpoint value is saved
     *
     * \return Error code
     */
    GDB_ERROR getValue(const PointIdStr pointID, PointData* valuePtr);

    /**
     * \brief Read the RAW value of a virtual point
     *
     * \param PointID Point ID
     * \param valuePtr Pointer to the object where the vpoint value is saved
     *
     * \return Error code
     */
    GDB_ERROR getRAWValue(const PointIdStr pointID, PointData *valuePtr);

    /**
     * \brief Read frozen value of the point
     *
     * When the point is configured as freezable, then every specific time
     * the point stores a copy of its value, "frozening" it; this method gets
     * this frozen value. When the point is not freezable, it simply gives 
     * the current counting value.
     *
     * \param PointID Point ID.
     * \param valuePtr where the data is saved.
     *
     * \return Error code.
     */
    GDB_ERROR getFrozenValue(const PointIdStr pointID, PointData *valuePtr);

    /**
     * \brief Read the value & status of an input virtual point of a control logic
     *
     * \param PointID Point ID.
     * \param valuePtr Pointer to the object where the VPoint value is saved
     *
     * \return Error code
     */
    GDB_ERROR getInputValue(const PointIdStr pointID, PointData *valuePtr);

    /**
     * \brief Read the value & status of an output virtual point of a control logic
     *
     * \param PointID Point ID.
     * \param valuePtr Pointer to the object where the VPoint value is saved
     *
     * \return Error code
     */
    GDB_ERROR getOutputValue(const PointIdStr pointID, PointData *valuePtr);

    /**
     * \brief Read the value label of a virtual point
     *
     * \param PointID Point ID.
     * \param valuePtr Pointer to the object containing the value to match.
     *
     * \return C string with the label text, or empty if error or not found.
     */
    const lu_char_t* getValueLabel(const PointIdStr pointID, PointData* valuePtr);

    /**
     * \brief Get the range (max and min) values of a virtual point
     *
     * Note that these values are in Engineer units and can be exceeded by the
     * Virtual Point without any warning.
     *
     * \param pointID Point ID
     * \param minValuePtr Object to be used to store the minimum range value
     * \param maxValuePtr Object to be used to store the maximum range value
     *
     * \return Error code
     */
    GDB_ERROR getPointRange(const PointIdStr pointID,
                            PointData* minValuePtr,
                            PointData* maxValuePtr
                           );
    /**
	 * \brief Register an observer to a virtual point
	 * 
	 * \param pointID Point ID
	 * \param observer Pointer to the observer to register
	 * 
	 * \return Error code
	 */
    GDB_ERROR attach(const PointIdStr pointID, IPointObserver *observer);

    /**
	 * \brief Remove an observer from a virtual point
	 * 
	 * \param pointID Point ID
	 * \param observer Observer to remove
	 * 
	 * \return Error code
	 */
    GDB_ERROR detach(const PointIdStr pointID, IPointObserver* observer);

    /**
	 * \brief Add job to the scheduler queue.
	 * 
	 * Once executed the job is deleted
	 * 
	 * \param jobPtr Pointer to the job
	 * 
	 * \return Error code
	 */
    GDB_ERROR addJob(IJob* jobPtr)
    {
        return scheduler.addJob(jobPtr);
    };

    /**
	 * \brief Add a delayed/periodic job to the scheduler queue.
	 * 
	 * One shot jobs are removed and deleted after execution
	 * 
	 * \param jobPtr Pointer to the job
	 * 
	 * \return Error code
	 */
    GDB_ERROR addJob(TimedJob* jobPtr)
    {
       return scheduler.addJob(jobPtr);
    }

    /**
     * \brief Remove a delayed/periodic job from the scheduler queue.
     *
     * Job is removed and deleted before execution.
     * This method ensures a job will not be deleted while running.
     *
     * \param jobPtr Pointer to the job
     */
    template <typename TJob>
    void cancelJob(TJob*& jobPtr)
    {
        scheduler.cancelJob(jobPtr);
    }

    /**
     * \brief Get Control logic type
     *
     * \param groupID Point group.
     *
     * \return Control logic type
     */
    CONTROL_LOGIC_TYPE getType(const lu_uint16_t groupID);

    /**
     * \brief Get Control logic name
     *
     * \param groupID Point group.
     *
     * \return Pointer to the Control logic name
     */
    const lu_char_t* getTypeName(const lu_uint16_t groupID);

    /**
     * \brief Check if a Control logic can be operated
     *
     * \param groupID Point group.
     *
     * \return operatability of the Control Logic
     */
    lu_bool_t isOperatable(const lu_uint16_t groupID);

    /**
     * \brief Get the off/local/remote status of the system
     *
     * \param status Where to store the off/local/remote status.
     *
     * \return Error Code
     */
    GDB_ERROR getLocalRemote(OLR_STATE& status);

	/**
	 * \brief Change off/local/remote status of the system
	 * 
	 * \param status New status
     * \param source
     * \param force
	 * 
	 * \return Error Code
	 */
    GDB_ERROR setLocalRemote(const OLR_STATE status, const OLR_SOURCE source);

	 /**
      * \brief Start an element's operation
      *
      * Activates the main operation associated to an element, usually
      * being a Control Logic.
      * If the preliminary checks are passed the operation is started in the
      * background and the function returns immediately with an error code.
      *
      * \param groupID Group ID, usually a Control logic group ID
      * \param operation Operation to perform
      *
      * \return Error Code
      */
    GDB_ERROR startOperation(const lu_uint16_t groupID, SwitchLogicOperation &operation);

    /**
     * \brief Writes an output value with control logic.
     */
    GDB_ERROR writeAnalogOperation(const lu_uint16_t groupID, AnalogueOutputOperation &val);

    /**
     * \brief Freeze a point value
     *
     * This function freezes a point value immediately, while keeping the current
     * value update in the background. When andClear set to LU_TRUE, the point 
     * will be freezed, its internal value being reset afterwards.
     *
     * \param pointID ID of the point.
     * \param andClear Set to LU_TRUE to reset the value after freezing
     *
     * \return Error code
     */
    GDB_ERROR freeze(const PointIdStr pointID, const lu_bool_t andClear);

    /**
     * \brief Clear (reset) a point value
     *
     * This function resets a point value immediately to its default value, if
     * any configured -- and only if the point supports it.
     *
     * \param pointID ID of the point.
     *
     * \return Error code
     */
    GDB_ERROR clear(const PointIdStr pointID);

    /**
     * \brief sets EOI(End Of Initialization) Flag data member
     *
     * \param flagValue EOI Flag value, defaults to LU_True
     *
     * \return void
     */
    void setEOIFlag(lu_bool_t flagValue = LU_TRUE){m_EOIFlag = flagValue;}

    /**
     * \brief gets EOI(End Of Initialization) Flag
     *
     * \param void
     *
     * \return value of EOIFlag
     */
    lu_bool_t getEOIFlag(){return m_EOIFlag;}


    /**
     * \brief Stop the Database
     *
     * Stops the points being updated and detaches any observers
     *
     * \param none
     *
     * \return Error Code
     */
    GDB_ERROR stop();

private:
    /**
     * \brief Get point from database
     *
     * \param PointID Point ID.
     *
     * \return Pointer to the Virtual Point, or NULL when error.
     */
    IPoint* getPoint(const PointIdStr pointID);

private:
    Mutex bigDBLock;
    GDBScheduler scheduler;
    PointDB points;
    ControlLogicDB controlLogic;
    bool initialised;
    Logger &log;
    lu_uint16_t groupOLR;   //ID of the OLR control Logic
    lu_bool_t m_EOIFlag;


public:
    /**
     * Temporary solution for accessing database instance.
     */
    static GeminiDatabase* getInstance();
};

#endif // !defined(EA_27FC4164_92A8_4ced_87A3_649FCE429249__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
