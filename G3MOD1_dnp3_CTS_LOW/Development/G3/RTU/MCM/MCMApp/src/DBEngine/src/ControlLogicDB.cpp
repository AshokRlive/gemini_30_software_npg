/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Control Logic database implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   26/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <cstring>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "ControlLogicDB.h"
#include "ControlLogic.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

static inline lu_uint16_t groupID2Idx(lu_uint16_t groupID)
{
    return groupID - (PointDB::POINT_DB_GROUP + 1);
}

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

ControlLogicDB::ControlLogicDB() : mutex(LU_TRUE),
                                   dbSize(-1),
                                   db(NULL)
{

}


ControlLogicDB::~ControlLogicDB()
{
    ControlLogic *cLogicPtr;

    if(db != NULL)
    {
        /* Deallocate all the points */
        for(lu_int32_t i = 0; i < dbSize; ++i)
        {
            cLogicPtr = db[i];

            if(cLogicPtr != NULL)
            {
                delete cLogicPtr;
                db[i] = NULL;   //prevent further use of this point while deleting
            }
        }

        /* Deallocate the database */
        delete db;
        db = NULL;
    }
}


GDB_ERROR ControlLogicDB::init(lu_int16_t size)
{
    GDB_ERROR ret = GDB_ERROR_NONE;

    /* Enter critical region */
    LockingMutex lMutex(mutex);

    if(db == NULL)
    {
        /* Create the database */
        db = new ControlLogic*[size];

        /* Save db size */
        dbSize = size;

        /* Initialize the database */
        memset(db, 0, sizeof(ControlLogic*) * dbSize);
    }
    else
    {
        ret = GDB_ERROR_INITIALIZED;
    }

    return ret;
}

lu_uint32_t ControlLogicDB::getSize()
{
    if(db == NULL)
    {
        return 0;
    }
    else
    {
        return dbSize;
    }
}

GDB_ERROR ControlLogicDB::addCLogic(ControlLogic* logicPtr)
{
    lu_uint16_t groupIdx;
    GDB_ERROR ret = GDB_ERROR_NONE;

    if(logicPtr == NULL)
    {
        return GDB_ERROR_PARAM;
    }

    /* Enter critical region */
    LockingMutex lMutex(mutex);

    if(db == NULL)
    {
        return GDB_ERROR_NOT_INITIALIZED;
    }

    /* Get point ID */
    groupIdx = groupID2Idx(logicPtr->getID());

    /* check groupID (used as an index in the db)
     * against the db size.
     * The groupID must be smaller then the db size
     */
    if( (groupIdx >= dbSize) )
    {
        ret = GDB_ERROR_PARAM;
    }

    /* Add the point to the db. Generate an error if the
     * slot is not empty
     */
    if(ret == GDB_ERROR_NONE)
    {
        if(db[groupIdx] == NULL)
        {
            db[groupIdx] = logicPtr;
        }
        else
        {
            ret = GDB_ERROR_POINT_EXIST;
        }
    }

    return ret;
}

ControlLogic* ControlLogicDB::getCLogic(lu_uint16_t CLogicID)
{
    if(CLogicID == 0)
    {
        return NULL;
    }
    lu_uint16_t groupIdx = groupID2Idx(CLogicID);

    /* Probably unnecessary */
    LockingMutex lMutex(mutex);

    if( (db       == NULL  ) ||
        (groupIdx >= dbSize)
      )
    {
        return NULL;
    }

    /* Return a reference to the virtual point */
    return  db[groupIdx];
}


lu_uint32_t ControlLogicDB::getPointNum(lu_uint16_t CLogicID)
{
    lu_uint32_t ret = 0;
    lu_uint16_t groupIdx = groupID2Idx(CLogicID);

    if( (db != NULL) && (groupIdx < dbSize) )
    {
        ControlLogic* clPtr = db[groupIdx];
        if(clPtr != NULL)
        {
            ret = clPtr->getPointNum();
        }
    }
    return ret;
}

void ControlLogicDB::stop()
{
    lu_int32_t i;

    if (db != NULL)
    {
        /* Loop around the points, stopping them */
        for (i = 0; i < dbSize; i++)
        {
            if (db[i] != NULL)
            {
                db[i]->stopUpdate();
            }
        }
    }
}
/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
