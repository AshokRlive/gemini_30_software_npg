/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       G3 job scheduler interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_63777F33_5740_465a_A9C3_C65BCF6A2FA5__INCLUDED_)
#define EA_63777F33_5740_465a_A9C3_C65BCF6A2FA5__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "JobScheduler.h"
#include "TimedJobScheduler.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class GDBScheduler
{
public:
    /**
     * \brief Constructor
     *
     * \param schedType Scheduler type used by the internal thread
     * \param priority thread priority
     */
    GDBScheduler( SCHED_TYPE schedType   ,
                  lu_uint32_t priority
                );

    virtual ~GDBScheduler();

    /**
     * \brief Start scheduler
     *
     * \return Error code
     */
    void start();

    /**
     * \brief Stop scheduler
     */
    void stop();

	/**
	 * \brief Add job to the scheduler queue.
	 * 
	 * One executed the job is deleted
	 * 
	 * \param jobPtr Pointer to the job
	 * 
	 * \return Error code
	 */
    GDB_ERROR addJob(IJob *jobPtr)
    {
        return jobs.addJob(jobPtr);
    }

	/**
	 * \brief Add a delayed/periodic job to the scheduler queue.
	 * 
	 * One shot jobs are removed and deleted after execution
	 * 
	 * \param jobPtr Pointer to the job
	 * 
	 * \return Error code
	 */
    GDB_ERROR addJob(TimedJob *jobPtr)
    {
        return timedJobs.addJob(jobPtr);
    }

    /**
     * \brief Remove a delayed/periodic job from the scheduler queue.
     *
     * Job is removed and deleted before execution.
     * This method ensures a job will not be deleted while running.
     *
     * \param jobPtr Pointer to the job
     */
    template <typename TJob>
    void cancelJob(TJob*& jobPtr)
    {
        timedJobs.cancelJob(jobPtr);
    }

private:
    JobScheduler jobs;
    TimedJobScheduler timedJobs;
};


#endif // !defined(EA_63777F33_5740_465a_A9C3_C65BCF6A2FA5__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
