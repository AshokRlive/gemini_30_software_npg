/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Control Logic database interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   26/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_D4EEAEA6_6816_494f_89D1_084A14062236__INCLUDED_)
#define EA_D4EEAEA6_6816_494f_89D1_084A14062236__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Mutex.h"
#include "MainAppEnum.h"

/* Forward declaration */
class ControlLogic;

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * Control logic database. A virtual factory class with a parametrized factory
 * method is used to build the control logic instances.
 * The groupID is used as an index to find the control logic instances in the
 * database.
 * Maximum number of supported instances: 65536
 */
class ControlLogicDB
{

public:
    ControlLogicDB();
    virtual ~ControlLogicDB();

    /**
     * \brief Initialize database
     *
     * \param size Database size
     *
     * \return Error Code
     */
    GDB_ERROR init(lu_int16_t size);

    /**
     * \brief Return the amount of Control Logics present in the database
     *
     * \return Database size (amount of Control Logics)
     */
    lu_uint32_t getSize();

    /**
     * \brief Add a new control logic block to the DB
     *
     * \param pointPtr Pointer to the control logic block
     *
     * \return Error code
     */
    GDB_ERROR addCLogic(ControlLogic* logicPtr);

    /**
     * \brief get a Control Logic Block from the DB
     *
     * \param CLogicID Control Logic Block ID
     *
     * \return Pointer to the Control Logic Block. NULL if the
     * Control Logic Block doesn't exist
     */
    ControlLogic* getCLogic(lu_uint16_t CLogicID);

    /**
     * \brief get the number of configured points in a Control Logic
     *
     * \return Number of Control Logic points configured
     */
    lu_uint32_t getPointNum(lu_uint16_t CLogicID);

    /**
     * \brief Stop all points
     *
     * \param none
     *
     * \return none
     */
    void stop();

private:
    Mutex mutex;
    lu_uint16_t dbSize;
    ControlLogic **db;
};
#endif // !defined(EA_D4EEAEA6_6816_494f_89D1_084A14062236__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
