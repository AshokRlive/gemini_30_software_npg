/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Virtual point database interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_FC986AE2_EC12_4142_9A98_34D8634998EF__INCLUDED_)
#define EA_FC986AE2_EC12_4142_9A98_34D8634998EF__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "Mutex.h"
#include "MainAppEnum.h"
#include "IPoint.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * Virtual point database. A virtual factory class with a  parametrized factory
 * method is used to build the points.
 * The point ID is used as an index to find the points in the database.
 * Maximum number of supported points: 65536
 */
class PointDB
{

public:

    /**
	 * \brief constructor
	 * 
	 * \param size Database size (number of points)
	 * \param factoryPtr Pointer to the factory class used to create virtual points
	 * 
	 * \return None
	 */
    PointDB();

    virtual ~PointDB();

    /**
     * \brief Initialize database
     *
     * \param size Database size
     *
     * \return Error Code
     */
    GDB_ERROR init(lu_uint16_t size);

    /**
     * \brief Return the database size
     *
     * \return Database size
     */
    lu_uint32_t getSize();

    /**
     * \brief Add a new point to the DB
     *
     * \param pointPtr Point
     *
     * \return Error code
     */
    GDB_ERROR addPoint(IPoint* pointPtr);

    /**
	 * \brief get a point from the DB
	 * 
	 * \param ID Point ID
	 * 
	 * \return Pointer to the virtual point. NULL if the point doesn't exist
	 */
    IPoint* getPoint(lu_uint16_t ID);

    /**
     * \brief Stop all points
     *
     * \param none
     *
     * \return E
     */
    GDB_ERROR stop();

    static const lu_uint16_t POINT_DB_GROUP = 0;
private:
    Mutex mutex;
    lu_int32_t dbSize;
    IPoint **db;
};
#endif // !defined(EA_FC986AE2_EC12_4142_9A98_34D8634998EF__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
