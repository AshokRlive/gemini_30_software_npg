/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Job scheduler module definition
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_28FAC409_44EE_4d78_A6FF_CA29D0050203__INCLUDED_)
#define EA_28FAC409_44EE_4d78_A6FF_CA29D0050203__INCLUDED_


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Thread.h"
#include "ObjectQueue.h"
#include "QueueObj.h"
#include "MainAppEnum.h"
#include "Logger.h"

/* Forward declaration */
class IJob;

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


//typedef QueueMgr<IJob> JobQueue;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */


class IJob : public QueueObj<IJob>
{
public:
    IJob() {};
    virtual ~IJob() {};

	/**
	 * \brief Method executed by the job scheduler
	 * 
	 * \return none
	 */
    virtual void run() = 0;
};

/**
 * All the jobs queued are executed by the internal thread
 * in a FIFO order. As soon as a job is terminated the object is
 * deleted
 */
class JobScheduler : public Thread
{

public:
    /**
     * \brief Constructor
     *
     * \param schedType Scheduler type used by the internal thread
     * \param priority thread priority
     *
     * \return none
     */
    JobScheduler(SCHED_TYPE schedType, lu_uint32_t priority);

    virtual ~JobScheduler();

    /**
     * \brief Add a new job
     *
     * The jobs are queued in a FIFO queue
     *
     * \param jobPtr Pointer to the job to add
     *
     * \return Error Code
     */
    GDB_ERROR addJob(IJob* jobPtr);

    THREAD_ERR stop();
    THREAD_ERR join();

protected:
    /**
     * \brief Thread main loop
     *
     * This function is called automatically when a thread is started
     */
    virtual void threadBody();

private:
    ObjectQueue<IJob> jobs;
    Logger& log;
};

#endif // !defined(EA_28FAC409_44EE_4d78_A6FF_CA29D0050203__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
