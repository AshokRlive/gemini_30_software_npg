/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ModuleAlarmStanding.h 14 Apr 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/IOModule/include/moduleAlarm/ModuleAlarmStanding.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Module Alarm that goes inactive only when acknowledged when it has been
 *       deactivated previously.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 14 Apr 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14 Apr 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MODULEALARMSTANDING_H__INCLUDED
#define MODULEALARMSTANDING_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "ModuleAlarm.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief
 *
 * Module Alarm that goes inactive only when acknowledged when it has been
 * deactivated previously. For that it keeps an internal state to remember if
 * it was activated or deactivated.
 */
class ModuleAlarmStanding: public ModuleAlarm
{
public:
    ModuleAlarmStanding() : ModuleAlarm(), activated(LU_FALSE)
    {};
    ModuleAlarmStanding(const ModuleAlarm::AlarmIDStr id) : ModuleAlarm(id),
                                                            activated(LU_FALSE)
    {};
    virtual ~ModuleAlarmStanding();

    /* ==Overridden from ModuleAlarm== */
    virtual ModuleAlarmStanding* clone() const { return new ModuleAlarmStanding(*this); };
    virtual void activate();
    virtual void deactivate();
    virtual void acknowledge();

private:
    lu_bool_t activated;    //Activation/deactivation state.
};

#endif /* MODULEALARMSTANDING_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
