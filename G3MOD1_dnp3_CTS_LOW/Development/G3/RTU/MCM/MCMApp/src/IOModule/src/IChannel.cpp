/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id: IChannel.cpp 12 Feb 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/CANModule_StateMachine/G3/RTU/MCM/MCMApp/src/IOModule/src/IChannel.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Implementation of default methods of the Channel Interface.
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 12 Feb 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   12 Feb 2014 pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "IChannel.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
IChannel::ChannelIDStr::ChannelIDStr(CHANNEL_TYPE channelType, lu_uint32_t channelID) :
                                                type(channelType),
                                                id(channelID)
{}

std::string IChannel::ChannelIDStr::toString() const
{
    std::ostringstream ss;
    ss << CHANNEL_TYPE_ToSTRING(type) << " Channel:" << id;
    return ss.str();
}


IChannel::FlagsStr::FlagsStr() :
                            online(LU_FALSE),
                            alarm(LU_FALSE),
                            restart(LU_TRUE),   //Not updated yet at creation time
                            outRange(LU_FALSE),
                            filtered(LU_FALSE),
                            simulated(LU_FALSE)
{}

bool IChannel::FlagsStr::operator==(const IChannel::FlagsStr flags)
{
    return ( (online == flags.online) &&
             (alarm == flags.alarm) &&
             (restart == flags.restart) &&
             (outRange == flags.outRange) &&
             (filtered == flags.filtered) &&
             (simulated == flags.simulated)
             );
}

IChannel::FlagsStr IChannel::FlagsStr::operator&(const IChannel::FlagsStr flags)
{
    FlagsStr result;
    result.online =   (online && flags.online); //only when both are set
    result.alarm =    (alarm || flags.alarm);
    result.restart =  (restart || flags.restart);
    result.outRange = (outRange || flags.outRange);
    result.filtered = (filtered || flags.filtered);
    result.simulated= (simulated || flags.simulated);
    return result;
}

const std::string IChannel::FlagsStr::toString() const
{
    std::string s("|");
    s.append((online == LU_TRUE)? "Online|" : "Offline|");
    s.append((alarm   == LU_TRUE)? "Alarm|" : "");
    s.append((restart == LU_TRUE)? "Init|" : "");
    s.append((outRange == LU_TRUE)? "OutRange|" : "");
    s.append((filtered == LU_TRUE)? "Filt|" : "");
    s.append((simulated == LU_TRUE)? "Sim|" : "");

    return s;
}


IChannel::IChannel(CHANNEL_TYPE type, lu_uint32_t id) :
                                            ch_id(type,id),
                                            mutex(LU_TRUE),  //priority inheritance enabled
                                            name(NULL)
{
    updateName();
}

IChannel::~IChannel()
{
    this->stopUpdate();
    if(name != NULL)
    {
        delete[] name;
    }
}


void IChannel::setActive(const lu_bool_t onlineStatus)
{
    LockingMutex lMutex(mutex); //protect flag change
    if(flags.online != onlineStatus)
    {
        flags.online = onlineStatus;
        updateObservers();
    }
}

IOM_ERROR IChannel::selOperate(lu_uint16_t value)
{
    /* Default implementation */
    LU_UNUSED(value);
    return IOM_ERROR_NOT_SUPPORTED;
}

IOM_ERROR IChannel::selOperate(const lu_uint16_t value, const lu_bool_t local)
{
    /* Default implementation */
    LU_UNUSED(value);
    LU_UNUSED(local);
    return IOM_ERROR_NOT_SUPPORTED;
}

IOM_ERROR IChannel::selOperate(const lu_uint16_t value, const lu_bool_t local, const lu_int32_t auxValue)
{
    /* Default implementation */
    LU_UNUSED(value);
    LU_UNUSED(local);
    LU_UNUSED(auxValue);
    return IOM_ERROR_NOT_SUPPORTED;
}


IOM_ERROR IChannel::select(const lu_uint8_t selectTimeout, const lu_bool_t local)
{
    LU_UNUSED(selectTimeout);
    LU_UNUSED(local);

   return IOM_ERROR_NOT_SUPPORTED;
}


IOM_ERROR IChannel::operate(const lu_bool_t value, const lu_bool_t local, const lu_uint8_t delay)
{
    LU_UNUSED(value);
    LU_UNUSED(local);
    LU_UNUSED(delay);

    return IOM_ERROR_NOT_SUPPORTED;
}


IOM_ERROR IChannel::operate(const timeval *timeoutPtr, const lu_uint16_t duration, const lu_int32_t limit, const lu_bool_t local)
{
    LU_UNUSED(timeoutPtr);
    LU_UNUSED(duration);
    LU_UNUSED(limit);
    LU_UNUSED(local);

    return IOM_ERROR_NOT_SUPPORTED;
}

IOM_ERROR IChannel::cancel(const lu_bool_t local)
{
    LU_UNUSED(local);

    return IOM_ERROR_NOT_SUPPORTED;
}


IOM_ERROR IChannel::attach(ChannelObserver *observer)
{
    if(observer != NULL)
    {
        // Protect the observer queue
        MasterLockingMutex mMutex(queueMutex, LU_TRUE);

        /* Add the observer to the observer list */
        if(observers.enqueue(observer) == LU_TRUE)
        {
            LockingMutex lMutex(mutex);
            observer->update(this);     //Immediately call the observer
            return IOM_ERROR_NONE;
        }
    }
    return IOM_ERROR_PARAM;
}


void IChannel::detach(ChannelObserver *observer)
{
    if(observer != NULL)
    {
        // Protect the observer queue
        MasterLockingMutex mMutex(queueMutex, LU_TRUE);
        observers.dequeue(observer);
    }
}


void IChannel::stopUpdate()
{
    // Protect the observer queue
    MasterLockingMutex mMutex(queueMutex, LU_TRUE);

    /* Delete all observers */
    observers.empty();
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
void IChannel::updateName()
{
    if(name != NULL)
    {
        delete[] name;
        name = NULL;
    }
    std::string s = getID().toString();
    name = new lu_char_t[s.size() + 1];
    strcpy(name,s.c_str());
}


void IChannel::updateObservers()
{
    ChannelObserver *observerPtr;

    // Protect the observer queue
    MasterLockingMutex mMutex(queueMutex);

    // Protect the data to ensure all observers update with same data
    LockingMutex lMutex(mutex);

    /* Notify observer. Get first observer */
    observerPtr = observers.getFirst();
    while(observerPtr != NULL)
    {
        observerPtr->update(this);

        /* Get next observer */
        observerPtr = observers.getNext(observerPtr);
    }
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
