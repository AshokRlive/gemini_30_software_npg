/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ModuleAlarmFactory.h 14 Apr 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/IOModule/include/ModuleAlarmFactory.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 14 Apr 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14 Apr 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MODULEALARMFACTORY_H__INCLUDED
#define MODULEALARMFACTORY_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "ModuleAlarm.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Factory for getting alarms initialised
 *
 * This class creates Module alarms objects as solicited, depending on the type.
 * The class uses a function template pointer.
 * Gives away MCM-only alarms (NOT the ones from Slave Modules), and set its
 * default severity.
 */
class ModuleAlarmFactory
{
public:
    ModuleAlarmFactory() {};
    virtual ~ModuleAlarmFactory() {};

    /**
     * \brief Creates a new ModuleAlarm object of the given type.
     *
     * \param id Module Alarm ID
     * \param subsystem Module Alarm subsystem
     * \param alarmCode Module Alarm code
     *
     * \return pointer to the newly created object.
     */
    static ModuleAlarm* createAlarm(const ModuleAlarm::AlarmIDStr id);
    static ModuleAlarm* createAlarm(const SYS_ALARM_SUBSYSTEM subSystem,
                                    const lu_uint32_t alarmCode
                                    );
};


#endif /* MODULEALARMFACTORY_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
