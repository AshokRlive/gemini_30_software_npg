/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ModuleAlarmLister.cpp 27 May 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/IOModule/src/ModuleAlarmLister.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 27 May 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   27 May 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "ModuleAlarmLister.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

ModuleAlarmLister::ModuleAlarmLister()
{}


ModuleAlarmLister::~ModuleAlarmLister()
{
    MasterLockingMutex lMutex(accessMutex, LU_TRUE);
    for(ModuleAlarmList::iterator it = alarmList.begin(); it != alarmList.end(); ++it)
    {
        delete *it; //remove object
    }
    alarmList.clear();  //remove pointers
}


void ModuleAlarmLister::add(ModuleAlarm& mAlarm)
{
    MasterLockingMutex lMutex(accessMutex, LU_TRUE);
    alarmList.push_back(mAlarm.clone());    //store a pointer to alarm copy
}


void ModuleAlarmLister::addList(ModuleAlarmList& inList)
{
    MasterLockingMutex lMutex(accessMutex, LU_TRUE);
    for(ModuleAlarmList::iterator it = inList.begin(), end = inList.end(); it != end; ++it)
    {
        alarmList.push_back((*it)->clone());    //store a pointer to alarm copy
    }
}


size_t ModuleAlarmLister::size()
{
    MasterLockingMutex lMutex(accessMutex);
    return alarmList.size();
}


ModuleAlarm& ModuleAlarmLister::operator[](lu_uint32_t pos)
{
    MasterLockingMutex lMutex(accessMutex);
    if(pos >= alarmList.size())
    {
        return *(alarmList[alarmList.size()-1]);
    }
    else
    {
        return *(alarmList[pos]);
    }
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
