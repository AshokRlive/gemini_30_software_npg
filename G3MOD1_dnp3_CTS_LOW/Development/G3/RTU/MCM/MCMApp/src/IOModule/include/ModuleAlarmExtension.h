/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ModuleAlarmExtension.h 11 Apr 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/IOModule/include/ModuleAlarmExtension.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 11 Apr 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11 Apr 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MODULEALARMEXTENSION_H__INCLUDED
#define MODULEALARMEXTENSION_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "ModuleAlarm.h"
#include "ModuleAlarmLister.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief This class provides module alarm handling capabilities to a module.
 *
 * Functionality provided is related to a list of module alarms maintained here.
 *
 */
class ModuleAlarmExtension
{
public:
    ModuleAlarmExtension();
    virtual ~ModuleAlarmExtension();

    /**
     * \brief Get the status of the Module Alarms
     *
     * Returns if there are active alarms in a bit field that uses a bit per
     * alarm severity.
     *
     * \return Bit field stating the alarms active by severity (MODULE_BOARD_ERROR_ALARM_*)
     */
    virtual lu_uint8_t alarmStatus();

    /**
     * \brief Get a copy of an specific alarm from the Alarm List.
     *
     * \return Copy of one alarm, or an invalid one when not found
     */
    virtual ModuleAlarm getAlarm(const ModuleAlarm::AlarmIDStr id);

    /**
     * \brief Get a copy of the module alarm list.
     *
     * \return Copy of the list of module alarms.
     */
    virtual ModuleAlarmLister getAlarms();

public:

    /**
     * \brief Get a module alarm list copy, filtered by alarm severity.
     *
     * \param severity Severity of the alarms to be retrieved.
     *
     * \return List of module alarms that had this severity level.
     */
    virtual ModuleAlarmLister getAlarms(const SYS_ALARM_SEVERITY severity);


    /* === Available actions === */

    /**
     * \brief Notify to an specific alarm that the stimulus is present.
     *
     * This method tells the alarm that the condition that sets the alarm as
     * active is currently present.
     * The alarm is added (or updated its status) in the alarms list.
     *
     * \param id Complete alarm ID to be stored in the alarms list.
     * \param subSystem Alarm subsystem ID.
     * \param alarmCode Alarm code ID.
     * \param severity (New) severity of the alarm.
     * \param parameter Alarm parameter, if applicable.
     *
     * \return Error code
     */
    virtual IOM_ERROR activateAlarm(const ModuleAlarm::AlarmIDStr id);
    virtual IOM_ERROR activateAlarm(const SYS_ALARM_SUBSYSTEM subSystem,
                                    const lu_uint32_t alarmCode);
    virtual IOM_ERROR activateAlarm(const ModuleAlarm::AlarmIDStr id,
                                    const SYS_ALARM_SEVERITY  severity,
                                    const lu_uint16_t         parameter = 0);
    virtual IOM_ERROR activateAlarm(const SYS_ALARM_SUBSYSTEM subSystem,
                                    const lu_uint32_t alarmCode,
                                    const SYS_ALARM_SEVERITY  severity,
                                    const lu_uint16_t         parameter = 0);

    /**
     * \brief Notify to an specific alarm that the stimulus is absent.
     *
     * This method tells the alarm that the condition that sets the alarm as
     * active is currently absent.
     * If the alarm is not present in the list, it is NOT added.
     *
     * \param id Alarm ID to be modified in the alarms list.
     * \param subSystem Alarm subsystem ID.
     * \param alarmCode Alarm code ID.
     */
    virtual void deactivateAlarm(const ModuleAlarm::AlarmIDStr id);
    virtual void deactivateAlarm(const SYS_ALARM_SUBSYSTEM subSystem,
                                 const lu_uint32_t alarmCode);

    /**
     * \brief Acknowledge all the alarms in the module alarm list.
     *
     * \return Error code
     */
    virtual IOM_ERROR ackAlarms();

    /**
     * \brief Acknowledge a specific alarm of the module alarm list.
     *
     * \param id ID of the alarm to be acknowledged.
     *
     * \return Error code
     */
    virtual IOM_ERROR ackAlarm(const ModuleAlarm::AlarmIDStr id);

    /**
     * \brief Clears a specific alarm of the module alarm list.
     *
     * Clearing effectively removes the alarm from the alarm list instead of
     * deactivate it.
     *
     * \param id ID of the alarm to be cleared.
     *
     * \return Error code
     */
    virtual IOM_ERROR clearAlarm(const ModuleAlarm::AlarmIDStr id);

    /**
     * \brief Inhibit alarm publishing
     *
     * Inhibit alarms, allowing them to be updated, but it does not inform
     * about them.
     */
    virtual void inhibitAlarms();

    /**
     * \brief Grant alarm publishing
     *
     * Removes inhibition of any active alarm, effectively allowing the
     * alarms to be published.
     * In addition, any INACTIVE alarm is REMOVED.
     */
    virtual void grantAlarms();

protected:
    /**
     * \brief Get a module alarm list filtered by alarm severity.
     *
     * Please note that it gives away a copy of the pointers, so alarms are the
     * same.
     *
     * \param severity Severity of the alarms to be retrieved.
     *
     * \return List of pointers of module alarms that had this severity level.
     */
    virtual ModuleAlarmList getAlarmList(const SYS_ALARM_SEVERITY severity);

    /**
     * \brief Review the module alarm list to remove invalid ones.
     */
    virtual void packAlarms();

protected:
    MasterMutex alarmMutex;     //Mutex to control access to the internal alarm list
    ModuleAlarmList alarmList;  //Internal list of module alarms
    lu_bool_t alarmsInhibit;    //Alarm publishing inhibition
};

#endif /* MODULEALARMEXTENSION_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
