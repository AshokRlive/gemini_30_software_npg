/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ModuleAlarm.cpp 22 Jan 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/IOModule/src/ModuleAlarm.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 22 Jan 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   22 Jan 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <sstream>
#include <vector>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "ModuleAlarm.h"
#include "SysAlarm/SysAlarmAuxSwitchControllerEnum.h"
#include "SysAlarm/SysAlarmBatteryChargerEnum.h"
#include "SysAlarm/SysAlarmCalibrationEnum.h"
#include "SysAlarm/SysAlarmDigitalOutControllerEnum.h"
#include "SysAlarm/SysAlarmFanControllerEnum.h"
#include "SysAlarm/SysAlarmHmiControllerEnum.h"
#include "SysAlarm/SysAlarmIoManagerEnum.h"
#include "SysAlarm/SysAlarmLcdDriverEnum.h"
#include "SysAlarm/SysAlarmMCMEnum.h"
#include "SysAlarm/SysAlarmNvramEnum.h"
#include "SysAlarm/SysAlarmSerialProtocolEnum.h"
#include "SysAlarm/SysAlarmStatusManagerEnum.h"
#include "SysAlarm/SysAlarmSupplyControllerEnum.h"
#include "SysAlarm/SysAlarmSwitchControllerEnum.h"
#include "SysAlarm/SysAlarmSystemEnum.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/**
 * \brief Macro for defining and entry in the AlarmInit definition Array
 *
 * This macro helps to define entries in this array in order to simplify entry
 * additions. It depends on the AlarmArrayStr definition.
 *
 * \param subSystem SubSystem enum value
 * \param alCodeSymbol Type name of the alarm code enum related to this subSystem.
 *          This mangled name is directly the Enum definition (type) name.
 *
 * example: ALARMARRAYENTRY(SYS_ALARM_SUBSYSTEM_SYSTEM, SYSALC_SYSTEM)
 *          generates an entry prepared for an initialisation array, like:
 *          { SYS_ALARM_SUBSYSTEM_SYSTEM, &SYSALC_SYSTEM_ToSTRING, SYSALC_SYSTEM_LAST }
 */
#define ALARMARRAYENTRY(subSystem, alCodeSymbol) {subSystem, &alCodeSymbol##_ToSTRING, alCodeSymbol##_LAST }

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
typedef std::vector<const lu_char_t*> NameRow;

/**
 * Internal structure to help to initialise an alarm table depending on the
 * alarm subSystem.
 */
struct AlarmArrayStr
{
    SYS_ALARM_SUBSYSTEM subSystem;  //alarm subsystem
    const lu_char_t* (*ToSTRINGPtr)(lu_int32_t);   //function to get alarm name by the code
    lu_uint32_t size;           //Amount of alarm codes in this subsystem
};

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
/* Vector of alarmCode-related names, oner per alarm subsystem */
static std::vector<NameRow> alarmName;
static lu_bool_t initialised = LU_FALSE;    //apply initialisation once

/* Initialisation array to get MCM alarm strings. It relates subsystems with
 * all the depending alarm codes and their names.
 * Used mainly for ToString() method.
 */
static const AlarmArrayStr AlarmInit[] = {
    ALARMARRAYENTRY(SYS_ALARM_SUBSYSTEM_SYSTEM, SYSALC_SYSTEM),
    ALARMARRAYENTRY(SYS_ALARM_SUBSYSTEM_IOMANAGER, SYSALC_IOMAN),
    ALARMARRAYENTRY(SYS_ALARM_SUBSYSTEM_LCD_DRIVER, SYSALC_LCD_DRIVER),
    ALARMARRAYENTRY(SYS_ALARM_SUBSYSTEM_CALIBRATION, SYSALC_CALIBRATION),
    ALARMARRAYENTRY(SYS_ALARM_SUBSYSTEM_DIGITAL_OUT_CON, SYSALC_DIGITAL_OUT_CONTROL),
    ALARMARRAYENTRY(SYS_ALARM_SUBSYSTEM_FAN_CONTROLLER, SYSALC_FAN_CONTROL),
    ALARMARRAYENTRY(SYS_ALARM_SUBSYSTEM_HMI_CONTROLLER, SYSALC_HMI_CONTROL),
    ALARMARRAYENTRY(SYS_ALARM_SUBSYSTEM_NVRAM, SYSALC_NVRAM),
    ALARMARRAYENTRY(SYS_ALARM_SUBSYSTEM_SERIAL_PROTOCOL, SYSALC_SERIAL_PROTOCOL),
    ALARMARRAYENTRY(SYS_ALARM_SUBSYSTEM_STATUS_MANAGER, SYSALC_STATUS_MANAGER),
    ALARMARRAYENTRY(SYS_ALARM_SUBSYSTEM_SUPPLY_CONTROLLER, SYSALC_SUPPLY_CONTROL),
    ALARMARRAYENTRY(SYS_ALARM_SUBSYSTEM_AUX_SWITCH_CONTROLLER, SYSALC_AUX_SWITCH_CONTROL),
    ALARMARRAYENTRY(SYS_ALARM_SUBSYSTEM_SWITCH_CONTROLLER, SYSALC_SWITCH_CONTROLLER),
    ALARMARRAYENTRY(SYS_ALARM_SUBSYSTEM_BATTERY_CHARGER, SYSALC_BATTERY_CHARGER),
    ALARMARRAYENTRY(SYS_ALARM_SUBSYSTEM_MCM, SYSALC_MCM),
};
//Number of elements in the initialisation array
static const lu_uint32_t sizeAlarmInit = sizeof(AlarmInit) / sizeof(AlarmArrayStr);

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
ModuleAlarm::AlarmIDStr& ModuleAlarm::AlarmIDStr::operator=(const ModuleAlarm& moduleAlarm)
{
    subSystem = moduleAlarm.subSystem;
    alarmCode = moduleAlarm.alarmCode;
    return *this;
}


lu_bool_t ModuleAlarm::AlarmIDStr::isValid() const
{
    return (subSystem < SYS_ALARM_SUBSYSTEM_LAST)? LU_TRUE : LU_FALSE;
}


const lu_char_t* ModuleAlarm::AlarmIDStr::toCString() const
{
    if(isValid() == LU_TRUE)
        return alarmName[subSystem][alarmCode];
    else
        return "[Invalid]";
}



ModuleAlarm::ModuleAlarm(): subSystem(SYS_ALARM_SUBSYSTEM_LAST),
                            alarmCode(0)
{
    init();
}

ModuleAlarm::ModuleAlarm(const SYS_ALARM_SUBSYSTEM subSys, const lu_uint32_t alCode) :
                                                            subSystem(subSys),
                                                            alarmCode(alCode),
                                                            valid(LU_TRUE)
{
    init();
}

ModuleAlarm::ModuleAlarm(const ModuleAlarm::AlarmIDStr id) : subSystem(id.subSystem),
                                                             alarmCode(id.alarmCode),
                                                             valid(LU_TRUE)
{
    init();
}


ModuleAlarm::ModuleAlarm(SYS_ALARM_SUBSYSTEM subSys,
                        lu_uint32_t codeAlarm,
                        AlarmState stateAlarm,
                        SYS_ALARM_SEVERITY  severityAlarm,
                        lu_uint16_t         alarmParameter) :
                            subSystem(subSys),
                            alarmCode(codeAlarm),
                            valid(LU_TRUE)
{
    init();
    //Override init values
    severity = severityAlarm;
    parameter = alarmParameter;
    state = stateAlarm;
}

ModuleAlarm::~ModuleAlarm()
{}


ModuleAlarm& ModuleAlarm::operator=(const ModuleAlarm::AlarmIDStr& alarmID)
{
    subSystem = alarmID.subSystem;
    alarmCode = alarmID.alarmCode;
    return *this;
}

ModuleAlarm::AlarmIDStr ModuleAlarm::getID()
{
    ModuleAlarm::AlarmIDStr alarmRef(subSystem, alarmCode);
    return alarmRef;
}

lu_bool_t ModuleAlarm::checkID(const ModuleAlarm::AlarmIDStr& alarmID)
{
    return ( (alarmID.subSystem == subSystem) && (alarmID.alarmCode == alarmCode) );
}

lu_bool_t ModuleAlarm::checkID(const ModuleAlarm& alarmToCompare)
{
    return ( (alarmToCompare.subSystem == subSystem) && (alarmToCompare.alarmCode == alarmCode) );
}


lu_bool_t ModuleAlarm::isValid()
{
    return valid && (subSystem < SYS_ALARM_SUBSYSTEM_LAST);
}


void ModuleAlarm::invalidate()
{
    valid = LU_FALSE;
}


lu_bool_t ModuleAlarm::isActive()
{
    return ( isValid() && ( (state == SYS_ALARM_STATE_ACTIVE) ||
                            (state == SYS_ALARM_STATE_REACTIVATE_LATCHED) ) );
}


lu_bool_t ModuleAlarm::isSevere()
{
    return ((severity == SYS_ALARM_SEVERITY_CRITICAL) ||
            (severity == SYS_ALARM_SEVERITY_ERROR) ||
            (severity == SYS_ALARM_SEVERITY_WARNING)
            );
}


lu_bool_t ModuleAlarm::isImportant()
{
    return ( (isSevere() == LU_TRUE) && (isActive() == LU_TRUE) );
}


ModuleAlarm::AlarmState ModuleAlarm::getState()
{
    return state;
}


void ModuleAlarm::activate()
{
    //nothing to do by default
}


void ModuleAlarm::deactivate()
{
    //nothing to do by default
}


void ModuleAlarm::acknowledge()
{
    //Acknowledged only from Active or Reactivate by default
    state = (state == STATE_INACTIVE)?
                    STATE_INACTIVE :
                    (AlarmState)SYS_ALARM_STATE_INACTIVE_LATCHED;
}


void ModuleAlarm::clear()
{
    state = STATE_INACTIVE; //back to inactive by default
    invalidate();           //mark it to be cleared
}


std::string ModuleAlarm::toString()
{
    std::stringstream ss;
    ss  << "(" << subSystem << "," << alarmCode << "," << severity << "," << state << "," << parameter << "): ";
    if( (isValid() == LU_FALSE) || (alarmCode >= alarmName[subSystem].size()) )
    {
        ss << "[Invalid alarm data]";
    }
    else
    {
        ss  << SYS_ALARM_SUBSYSTEM_ToSTRING(subSystem) << ", "
            << alarmName[subSystem][alarmCode] << ", "
            << SYS_ALARM_SEVERITY_ToSTRING(severity) << ", "
            << SYS_ALARM_STATE_ToSTRING(state) << ", ("
            << parameter << ")";
    }
    return ss.str();
}


std::string ModuleAlarm::toShortString()
{
    std::stringstream ss;
    lu_char_t severityCritical = '!';
    const lu_char_t* severityChar = SYS_ALARM_SEVERITY_ToSTRING(severity);
    const lu_char_t* stateChar = SYS_ALARM_STATE_ToSTRING(state);
    if( (isValid() == LU_FALSE) || (alarmCode >= alarmName[subSystem].size()) ||
        (severityChar == NULL) || (stateChar == NULL)
        )
    {
        ss << "[Invalid alarm data]";
    }
    else
    {
        if(severity != SYS_ALARM_SEVERITY_CRITICAL)
            severityCritical = severityChar[0]; //get first letter (leaving critical as '!')

        ss  << severityCritical  << "|"
            << stateChar[0] << "|"
            << alarmName[subSystem][alarmCode] << "("
            << parameter << ")";
    }
    return ss.str();
}


std::string ModuleAlarm::stateToString()
{
    std::stringstream ss;
    if(isValid() == LU_FALSE)
    {
        ss << "[Invalid alarm data]";
    }
    else
    {
        ss  << SYS_ALARM_STATE_ToSTRING(state);
    }
    return ss.str();
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void ModuleAlarm::init()
{
    state = STATE_INACTIVE;
    severity = SYS_ALARM_SEVERITY_INFO;
    parameter = 0;

    if(initialised == LU_FALSE)
    {
        initialised = LU_TRUE;
        /* Initialise alarm names for ToString method. */
        for (lu_uint32_t subSys = 0; subSys < SYS_ALARM_SUBSYSTEM_LAST; ++subSys)
        {
            for (lu_uint32_t initIdx = 0; initIdx < sizeAlarmInit; ++initIdx)
            {
                if(AlarmInit[initIdx].subSystem == subSys)
                {
                    alarmName.push_back(std::vector<const lu_char_t*>());    //create empty row
                    for (lu_uint32_t alCode = 0; alCode < AlarmInit[initIdx].size; ++alCode)
                    {
                        alarmName[subSys].push_back( AlarmInit[initIdx].ToSTRINGPtr(alCode) );
                    }
                }
            }
        }
    }
}


/*
 *********************** End of file ******************************************
 */
