/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ModuleAlarmPersistent.cpp 14 Apr 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/IOModule/src/moduleAlarm/ModuleAlarmPersistent.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Module Alarm that never goes inactive.
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 14 Apr 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14 Apr 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "ModuleAlarmPersistent.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
ModuleAlarmPersistent::~ModuleAlarmPersistent()
{}


void ModuleAlarmPersistent::activate()
{
    ModuleAlarm::AlarmState newState = STATE_INACTIVE;
    switch(state)
    {
        /* Note: SYS_ALARM_STATE_INACTIVE_LATCHED is an invalid state in this type of alarm */
        case ModuleAlarm::STATE_INACTIVE:
            newState = SYS_ALARM_STATE_ACTIVE;
            break;
        default:
            newState = state;   //if already active, leave as it was
            break;
    }
    state = newState;
}


void ModuleAlarmPersistent::acknowledge()
{
    //must be always active or reactivated
    state = (state == STATE_INACTIVE)?
                    STATE_INACTIVE :
                    (AlarmState)SYS_ALARM_STATE_REACTIVATE_LATCHED;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
