/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ModuleAlarmAutoClear.h 23 Apr 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/IOModule/include/moduleAlarm/ModuleAlarmAutoClear.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 23 Apr 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   23 Apr 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MODULEALARMAUTOCLEAR_H__INCLUDED
#define MODULEALARMAUTOCLEAR_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "ModuleAlarm.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Module Alarm that is automatically cleared when deactivated
 *
 * To clear and effectively being removed from the alarm list, it is
 * invalidated. This requires that the handler of the list checks the validity
 * of the alarm after alarm deactivation and remove the alarm from the list in
 * case it becomes invalid.
 */
class ModuleAlarmAutoClear: public ModuleAlarm
{
public:
    ModuleAlarmAutoClear() : ModuleAlarm() {};
    ModuleAlarmAutoClear(const ModuleAlarm::AlarmIDStr id) : ModuleAlarm(id) {};
    virtual ~ModuleAlarmAutoClear();

    /* ==Overridden from ModuleAlarm== */
    virtual ModuleAlarmAutoClear* clone() const { return new ModuleAlarmAutoClear(*this); };
    virtual void activate();
    virtual void deactivate();
    virtual void acknowledge();
};

#endif /* MODULEALARMAUTOCLEAR_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
