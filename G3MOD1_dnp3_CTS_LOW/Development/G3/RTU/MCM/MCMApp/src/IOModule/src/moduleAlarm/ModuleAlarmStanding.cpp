/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ModuleAlarmStanding.cpp 14 Apr 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/IOModule/src/moduleAlarm/ModuleAlarmStanding.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Module Alarm that goes inactive only when acknowledged when it has been
 *       deactivated previously.
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 14 Apr 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14 Apr 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "ModuleAlarmStanding.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
ModuleAlarmStanding::~ModuleAlarmStanding()
{}


void ModuleAlarmStanding::activate()
{
    switch(state)
    {
        case STATE_INACTIVE:
            state = SYS_ALARM_STATE_ACTIVE;
            break;
        case SYS_ALARM_STATE_INACTIVE_LATCHED:
            state = SYS_ALARM_STATE_REACTIVATE_LATCHED;
            break;
        default:
            //leave as it was
            break;
    }
    activated = LU_TRUE;
}


void ModuleAlarmStanding::deactivate()
{
    activated = LU_FALSE;
}


void ModuleAlarmStanding::acknowledge()
{
    //Acknowledged depending on the activation/deactivation state
    switch(state)
    {
        case SYS_ALARM_STATE_ACTIVE:
            if(activated == LU_TRUE)
            {
                state = SYS_ALARM_STATE_REACTIVATE_LATCHED;
            }
            else
            {
                state = SYS_ALARM_STATE_INACTIVE_LATCHED;
            }
            break;
        case SYS_ALARM_STATE_REACTIVATE_LATCHED:
            if(activated == LU_FALSE)
            {
                state = SYS_ALARM_STATE_INACTIVE_LATCHED;
            }
            break;
        default:
            //leave as it was
            break;
    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
