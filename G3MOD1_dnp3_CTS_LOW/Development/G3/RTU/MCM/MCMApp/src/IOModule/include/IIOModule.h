/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_D3100DEC_3501_4cbe_B01C_7B9F2DC90573__INCLUDED_)
#define EA_D3100DEC_3501_4cbe_B01C_7B9F2DC90573__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <vector>
#include <string.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Table.h"
#include "TimeManager.h"

#include "IOModuleCommon.h"
#include "IChannel.h"
#include "ModuleMessage.h"
#include "ModuleProtocol.h"
#include "ModuleAlarm.h"
#include "ModuleAlarmExtension.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
class IIOModule;//Forward declaration

typedef Table<IIOModule*> IOModuleTable;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class IIOModule : public ModuleAlarmExtension
{
public:
    /* TODO: pueyos_a - move sendReplyStr to proper place such as CAN-related module */
    struct sendReplyStr
    {
        /** Message type */
        MODULE_MSG_TYPE  messageType;
        /** Message ID */
        MODULE_MSG_ID    messageID;
        /** Payload */
        PayloadRAW      *messagePtr;
        /** Expected message type */
        MODULE_MSG_TYPE  rMessageType;
        /** Expected message id */
        MODULE_MSG_ID    rMessageID;
        /** where the reply message is saved */
        ModuleMessage   *rMessagePtr;
        /** Reply timeout */
        struct timeval timeout;
    };

public:
    IIOModule(MODULE type, MODULE_ID id) : mid(type,id)
    {
        IOModuleIDStr m(type,id);
        std::string mstr = m.toString();
        name = new lu_char_t[mstr.size() + 1];
        strcpy(name,mstr.c_str());
    };

    virtual ~IIOModule(){delete[]name;}

    /**
     * \brief Get Module ID
     *
     * \return Module ID (type and address)
     */
    const IOModuleIDStr getID() { return mid;}

	/**
	 * \brief Return a channel reference
	 * 
	 * \param type Channel type
	 * \param number Channel number
	 * 
	 * \return Channel pointer. NULL if channel is not present
	 */
	virtual IChannel* getChannel(CHANNEL_TYPE type, lu_uint32_t number) = 0;
	virtual IChannel* getChannel(IChannel::ChannelIDStr channel) { return getChannel(channel.type, channel.id); }
	/**
	 * \brief Send a message to the module
	 * 
	 * \param messagePtr Message to send
	 * 
	 * \return Error code
	 */
	virtual IOM_ERROR sendMessage(ModuleMessage *messagePtr, lu_uint32_t retries= 0) = 0;

    /**
      * \brief Send a message to the module
      *
      * \param messageType Message type
      * \param messageID Message ID
      * \param message Payload
      *
      * \return Error code
      */
	virtual IOM_ERROR sendMessage( MODULE_MSG_TYPE  messageType,
	                               MODULE_MSG_ID    messageID,
	                               PayloadRAW      *messagePtr,
	                               lu_uint32_t retries= 0
	                             ) = 0;

	/**
     * \brief Send a message to the module and wait for a reply
     *
     * \param messagePtr Structure containing the message to send and the
     * expected reply parameters
     *
     * \return Error code
     */
    virtual IOM_ERROR sendMessage(sendReplyStr *messagePtr,lu_uint32_t retries= 0) = 0;

	/**
	 * \brief Decode a module message
	 * 
	 * If necessary the module/channels internal state is updated
	 * 
	 * \param message Message to decode
	 * 
	 * \return Error code
	 */
	virtual IOM_ERROR decodeMessage(ModuleMessage *message) = 0;

	/**
	 * \brief Notify the timer tick is expired
	 * 
	 * This function should be periodically called in order to keep the internal logic
	 * updated (internal timers, etc,...)
	 * 
	 * \param dTime Time elapsed from the previous tick (in ms)
	 * 
	 * \return Error code
	 */
	virtual IOM_ERROR tickEvent(lu_uint32_t dTime) = 0;

	/**
	 * \brief Disable module timeout manager (if supported)
	 *
	 * \param disable if true disable the timeout manager otherwise it
	 * enables it
	 *
	 * \return Error code
	 */
	virtual void disableTimeout(lu_bool_t disable) = 0;

	/**
	 * \brief Return the module global information
	 *
	 * \param version Where the module information is saved
	 *
	 * \return Error code
	 */
	virtual IOM_ERROR getInfo(IOModuleInfoStr& moduleInfo) = 0;

	/**
     * \brief Return module information details
     *
     * \param version Where the module information is saved
     *
     * \return Error code
     */
	virtual IOM_ERROR getDetails(IOModuleDetailStr& moduleInfo) = 0;

    /**
     * \brief Return module System API
     *
     * \param version Where the module information is saved
     *
     * \return Error code
     */
	virtual void getSystemAPI(ModuleVersionStr& sysAPI) = 0;


    /**
     * \brief Return the raw value from a channel
     *
     * Depending on the channel implementation, get the latest value of the
     * channel -- it could ask it directly to the source, force a refresh, etc.
     * Please note that this is read from the module itself to handle the case
     * of trying to get a channel value from a channel that hasn't been created.
     *
     * \param id Channel ID referred to this module.
     * \param data Where to store the data read from the channel.
     *
     * \return Error code.
     */
    virtual IOM_ERROR readRAWChannel(IChannel::ChannelIDStr id, IChannel::ValueStr& data) = 0;
    virtual IOM_ERROR readRAWChannel(CHANNEL_TYPE channelType,
                                    lu_uint32_t channelID,
                                    IChannel::ValueStr& data)
    {
        IChannel::ChannelIDStr id(channelType, channelID);
        return readRAWChannel(id, data);
    }

    /**
     * \brief Checks if this module is specifically the Master (MCM).
     */
	lu_bool_t isMCM() {return (mid.type == MODULE_MCM)? LU_TRUE : LU_FALSE;};

	/**
     * \brief Ping a module
     *
     * If the received payload is different from the sent payload
     * an error is reported
     *
     * \param payloadSize Payload size
     * \param timeout Timeout in ms (maximum timeout 1000 ms)
     *
     * \return Error code
     */
    virtual IOM_ERROR ping(lu_uint32_t timeout, lu_uint32_t payloadSize = 8) = 0;

    /**
     * \brief Get module time
     *
     * \param time Pointer where the time is saved
     * \param timeout Timeout in ms (maximum timeout 1000 ms)
     *
     * \return Error code
     */
    virtual IOM_ERROR getTime(TimeManager::TimeStr& time, lu_uint32_t timeout) = 0;

    /**
     * \brief Get this module's name (like "PSM1")
     *
     * \return none
     */
    const lu_char_t* getName() { return name; }

    /**
     * \brief Stop updating - app closing
     */
    virtual void stop() = 0;

    virtual lu_bool_t getStatus(MSTATUS status) = 0;

    virtual ModuleUID getModuleUID() = 0;

    const IOModuleIDStr mid; // this module's ID

private:
    lu_char_t* name; // this module's name
};

#endif // !defined(EA_D3100DEC_3501_4cbe_B01C_7B9F2DC90573__INCLUDED_)

/*
 *********************** End of file ******************************************
 */




