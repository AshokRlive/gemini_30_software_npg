/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       IO Module common definitions
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   18/11/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_4FDDA2AC_5C95_4196_964B_10661D5781EC__INCLUDED_)
#define EA_4FDDA2AC_5C95_4196_964B_10661D5781EC__INCLUDED_


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <sstream>
#include <iomanip>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "ModuleProtocol.h"
#include "MainAppEnum.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
/* Module status enum*/
enum MSTATUS
{
    MSTATUS_PRESENT,
    MSTATUS_CONFIGURED,
    MSTATUS_DISABLED,
    MSTATUS_DETECTED,
    MSTATUS_REGISTERED,
    MSTATUS_ACTIVE
};

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

struct IOModuleIDStr
{
public:
    IOModuleIDStr()
     {
         type = MODULE_LAST;
         id = MODULE_ID_LAST;
     }

     IOModuleIDStr(MODULE _type, MODULE_ID _id)
     {
         type = _type;
         id = _id;
     }


    MODULE type;
    MODULE_ID id;

    std::string toString()
    {
        std::ostringstream ss;
        ss << MODULE_ToSTRING(type);

        // Append ID for multiple modules
        if(type != MODULE_MCM && type != MODULE_PSM && type != MODULE_HMI)
        {
            ss << (lu_int32_t)(id) + 1;
        }else{
            ss << " "; //append space for aligning purpose
        }

        return ss.str();
    }
};

/**
 * C++ Expansion of the original Module Unique ID structure
 */
struct ModuleUID : public ModuleUIDStr
{
public:
    ModuleUID(const ModuleUIDStr& moduleUID)
    {
        *this = moduleUID;
    }
    ModuleUID()
    {
        serialNumber = DEFAULT_MODULE_UID.serialNumber;
        supplierId = DEFAULT_MODULE_UID.supplierId;
    }
    bool operator==(const ModuleUIDStr& moduleUID) const
    {
        return ((moduleUID.serialNumber == serialNumber) &&
                (moduleUID.supplierId == supplierId) );
    }
    bool operator!=(const ModuleUIDStr& moduleUID) const
    {
        return !(*this == moduleUID);
    }
    ModuleUID& operator=(const ModuleUIDStr& moduleUID)
    {
        serialNumber = moduleUID.serialNumber;
        supplierId = moduleUID.supplierId;
        return *this;
    }
    const std::string toString()
    {
        std::stringstream ss;
        ss << std::setw(2) << std::setfill('0') << (lu_uint32_t)supplierId << "-"
           << std::setw(6) << std::setfill('0') << (lu_uint32_t)serialNumber;
        return ss.str();
    }
};

struct IOModuleRef
{
    IOModuleIDStr mid;
    ModuleUID moduleUID;
};

struct IOModuleStatusStr
{
    lu_bool_t active;       //module is running OK
    lu_bool_t registered;   //The module is present in the registration DB
    lu_bool_t configured;   //The module is present in the configuration file  //TODO to be removed
    lu_bool_t present;      //The module is connected and present on the bus
    lu_bool_t detected;     //The module has ever been detected in the bus
    lu_bool_t disabled;     //The module has been disabled due to tampering
public:
    std::string toString()
    {
      std::stringstream ss;
      /* Legend: OK=run|R=registered|C=configured|P=present|D=detected|S=disabled */
      ss << ((active != LU_TRUE)?     "  ": "OK") << "|"
         << ((registered != LU_TRUE)? " " : "R" ) << "|"
         << ((configured != LU_TRUE)? " " : "C" ) << "|"
         << ((present != LU_TRUE)?    " " : "P" ) << "|"
         << ((detected != LU_TRUE)?   " " : "D" ) << "|"
         << ((disabled != LU_TRUE)?   " " : "S" ) << "|";
      return ss.str();
    }

};

struct IOModuleInfoStr
{
public:
    IOModuleIDStr mid;
    ModuleUID moduleUID;
    IOModuleStatusStr status;
    FSM_STATE fsm; // The state of Finite State Machine
    ModuleHBeatSStr hBeatInfo;//Latest reported info from HeartBeat message
    ModuleInfoDef   mInfo;    //Latest reported info from ModuleInfo message
};

struct IOModuleDetailStr
{
public:
    IOModuleIDStr mid;          //Module ID
    lu_uint32_t architecture;   //System Architecture of the Module
    lu_uint32_t svnRevisionBoot;//BootLoader SVN revision
    lu_uint32_t svnRevisionApp; //Application SVN revision
    lu_uint32_t uptime;         //Module uptime (in seconds)
};

/*Channel reference in configuration*/
struct ChannelRef
{
public:
    MODULE moduleType;
    MODULE_ID moduleID;
    lu_uint32_t channelID;
};

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */


#ifdef __GNUC__
__inline
#endif
static IOM_ERROR REPLY_STATUS_to_IOM_ERROR(REPLY_STATUS reply)
{
    IOM_ERROR ret;
    switch(reply)
    {
        case REPLY_STATUS_OKAY:
            ret = IOM_ERROR_NONE;
            break;
        case REPLY_STATUS_SELECT_ERROR:
            ret = IOM_ERROR_SELECT;
            break;
        case REPLY_STATUS_ALREADY_OPERATING_ERROR:  /*operate received while operating */
        case REPLY_STATUS_OPERATE_ERROR:
        case REPLY_STATUS_OPERATE_REJECT_ERROR:
            ret = IOM_ERROR_OPERATE_ERROR;
            break;
        case REPLY_STATUS_COMPLETE_ERROR:   /* Operation is complete and cannot be cancelled */
        case REPLY_STATUS_CANCEL_ERROR:
            ret = IOM_ERROR_CANCEL_ERROR;
            break;
        case REPLY_STATUS_LOCAL_REMOTE_ERROR:
            ret = IOM_ERROR_LOCAL_REMOTE;
            break;
        case REPLY_STATUS_RELAY_ERROR:
            ret = IOM_ERROR_RELAY;
            break;
        case REPLY_STATUS_BATTERY_SUPPLY_ERROR:
            ret = IOM_ERROR_BATTERY;
            break;
        case REPLY_STATUS_VMOTOR_ERROR:
            ret = IOM_ERROR_VMOTOR;
            break;
        case REPLY_STATUS_OPERATION_IN_PROGRESS:
            ret = IOM_ERROR_ALREADY;
            break;
        case REPLY_STATUS_INHIBIT_ERROR:
            ret = IOM_ERROR_SELECT;
            break;
        case REPLY_STATUS_TIMEOUT_ERROR:
            ret = IOM_ERROR_TIMEOUT;
            break;
        case REPLY_STATUS_PARAM_ERROR:
            ret = IOM_ERROR_PARAM;
            break;
        case REPLY_STATUS_CONFIG_ERROR:
            ret = IOM_ERROR_CONFIG;
            break;
        case REPLY_STATUS_NOT_ONLINE_ERROR:
            ret = IOM_ERROR_OFFLINE;
            break;
        case REPLY_STATUS_WRITE_ERROR:
            ret = IOM_ERROR_WRITE;
            break;
        case REPLY_STATUS_NO_EVENTS:
            ret = IOM_ERROR_NOT_FOUND;
            break;
        default:
            ret = IOM_ERROR_GENERIC;
    }
    return ret;
}


#endif // !defined(EA_4FDDA2AC_5C95_4196_964B_10661D5781EC__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
