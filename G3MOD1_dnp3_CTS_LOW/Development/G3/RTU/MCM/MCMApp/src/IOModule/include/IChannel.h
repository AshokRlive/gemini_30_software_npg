/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Channel interface definition
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_02822390_53A3_4df5_BFF9_716514738251__INCLUDED_)
#define EA_02822390_53A3_4df5_BFF9_716514738251__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Mutex.h"
#include "IOData.h"
#include "ChannelObserver.h"

//deprecated?
#include "IOModuleCommon.h"




/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
class IChannel
{
public:
    /**
     * Channel reference
     */
    struct ChannelIDStr
    {
    public:
        CHANNEL_TYPE type;
        lu_uint32_t id;
    public:
        /**
         * \brief Constructor
         *
         * \param channelType Channel type
         * \param channelID Channel ID
         */
        ChannelIDStr(CHANNEL_TYPE channelType = CHANNEL_TYPE_LAST, lu_uint32_t channelID = 0);

        /**
         * \brief Comparison operator overload
         *
         * \return String containing the Channel ID in human-readable form
         */
        bool operator==(const ChannelIDStr a) const
        {
            return (a.type == this->type) && (a.id == this->id);
        }

        /**
         * \brief Convert channel reference to string ID
         *
         * \return String containing the Channel ID in human-readable form
         */
        std::string toString() const;
    };

    /**
     * Channel status flags type
     */
    struct FlagsStr
    {
    public:
        lu_bool_t online;   //Channel availability
        lu_bool_t alarm;    //Channel-related alarm (if applicable)
        lu_bool_t restart;  //Channel is just initialised: value is not yet valid
        lu_bool_t outRange; //Channel value is out of bounds (overflow/underflow)
        lu_bool_t filtered; //Analogue channel value has been filtered in origin
        lu_bool_t simulated;//Channel value is simulated
    public:
        /* Automatic initialisation */
        FlagsStr();
        /* Operators over the structure */
        bool operator==(const FlagsStr flags);
        bool operator!=(const FlagsStr flags) {return !(*this==flags);};
        /* Operator & compares two flag sets and gives a resulting set */
        FlagsStr operator&(const FlagsStr flags);
        /* Convert to string */
        virtual const std::string toString() const;
    };

    /**
     * Channel value and flags type
     *
     * This structure contains a Channel value and its related flags.
     */
    struct ValueStr
    {
        IOData* dataPtr;
        FlagsStr flags;
    public:
        /**
         * \brief Constructor to allow initialisation like: ValueStr(myIOData);
         */
        ValueStr(IOData& data) : dataPtr(&data)  {};
    };

public:
    /*
     * \brief IChannel class constructor
     *
     * \param type Channel Type
     * \param id Channel ID
     */
    IChannel(CHANNEL_TYPE type, lu_uint32_t id);

    virtual ~IChannel();

	/**
	 * \brief Read the channel value
	 * 
	 * \param dataPtr where the data is saved
	 * 
	 * \return Error code
	 */
	virtual IOM_ERROR read(ValueStr& valuePtr) = 0;

	/**
	 * \brief Write to the channel
	 * 
	 * \param dataPtr Data to write
	 * 
	 * \return Error code
	 */
	virtual IOM_ERROR write(ValueStr& valuePtr) = 0;

    /**
     * \brief Set Channel online/offline status only
     *
     * \param status New Channel status to be set in the flags.
     *
     * Default behaviour: change status and update observers only when different
     * from current status.
     */
    virtual void setActive(const lu_bool_t onlineStatus = LU_TRUE);

    /**
     * \brief Get Channel online/offline status only
     *
     * \return Current Channel status.
     *
     * Default behaviour: change status and update observers only when different
     * from current status.
     */
    virtual lu_bool_t isActive() {return flags.online;}

    /**
     * \brief Send a combined select+operate commands to an output channel
     *
     * To be overridden by output channels only.
     *
     * The operate should be sent within select timeout windows or it will fail
     * This function blocks until a reply is received or the timeout expires
     *
     * \param value Value to set, when it applies.
     * \param local Local/Remote status of command originator. LU_TRUE when "local"
     * \param auxValue Auxiliary value (such as limit or delay) when it applies.
     *
     *  Default behaviour: command not supported
     *
     * \return Error code
     */
    virtual IOM_ERROR selOperate(const lu_uint16_t value);
    virtual IOM_ERROR selOperate(const lu_uint16_t value,
                                 const lu_bool_t local
                                );
    virtual IOM_ERROR selOperate(const lu_uint16_t value,
                                 const lu_bool_t local,
                                 const lu_int32_t auxValue
                                );


/* Note: pueyos_a - (Rant mode) CAN time out values should be per object, not
 *      per call... we never call operate with a different timeout, right?
 */

    /**
     * \brief Send a select command to an output channel
     *
     * To be overridden by output channels only.
     *
     * The operate should be sent within selecTimeout windows or it will fail
     * This function blocks until a reply is received or the timeout expires
     *
     * \param selectTimeout select duration (in seconds)
     * \param local LU_TRUE if the command originator is "local"
     *
     *  Default behaviour: command not supported
     *
     * \return Error code
     */
    virtual IOM_ERROR select(const lu_uint8_t selectTimeout, const lu_bool_t local);

    /**
     * \brief Send an operate command to an output channel
     *
     * To be overridden by output channels only.
     *
     * The command is sent immediately but the operation takes a time that it
     * is already specified in the channel configuration.
     * This function blocks until a reply is received or the timeout expires.
     *
     * Default behaviour: command not supported
     *
     * \param value Set the output value to On or Off: LU_TRUE to set to On
     * \param local LU_TRUE if the command originator is "local"
     * \param delay Operation delay (in seconds)
     *
     * \return Error code
     */
    virtual IOM_ERROR operate(const lu_bool_t value, const lu_bool_t local, const lu_uint8_t delay = 0);

    /**
     * \brief Send an immediate operate command to an output channel
     *
     * To be overridden by output channels only.
     * This Operate command specifies the operation duration.
     *
     * This function blocks until a reply is received or the timeout expires
     *
     * Default behaviour: command not supported
     *
     * \param timeoutPtr Reply timeout
     * \param duration operation duration (in seconds)
     * \param limit if the limit is exceeded the channel is automatically
     *  is turned off
     * \param local LU_TRUE if the command originator is "local"
     *
     * \return Error code
     */
    virtual IOM_ERROR operate( const timeval *timeoutPtr,
                               const lu_uint16_t duration,
                               const lu_int32_t limit,
                               const lu_bool_t local
                             );

    /**
     * \brief Send a cancel command
     *
     * Meant to be overridden by output channels.
     *
     * This will cancel a select or the command (if a command is active)
     * This function blocks until a reply is received or the timeout expires
     *
     * Default behaviour: command not supported
     *
     * \param local LU_TRUE if the command originator is "local"
     *
     * \return Error code
     */
    virtual IOM_ERROR cancel(const lu_bool_t local);

	/**
	 * \brief Attach an observer to the channel
	 *
	 * When the channel status changes all the observer are automatically called
	 *
	 * \param observer New observer
	 *
	 * \return Error code
	 */
	virtual IOM_ERROR attach(ChannelObserver *observer);

	/**
	 * \brief Remove an observer
	 *
	 * \param observer Observer to remove
	 *
	 * \return error code
	 */
	virtual void detach(ChannelObserver *observer);

	/**
     * \brief Prevent update of observers
     *
     * \return None
     */
	virtual void stopUpdate();

    /*
     * \brief Get ID number of this Channel
     *
     * \return Channel's ID number.
     */
	const ChannelIDStr& getID() {return ch_id;}

    /*
     * \brief Get the name of this Channel in human-readable format.
     *
     * \return C string containing the Channel type and name.
     */
    const lu_char_t* getName() {return name;}

public:
    const ChannelIDStr ref; // this channel's reference

protected:
    ChannelIDStr ch_id; // this channel's ID
    Mutex mutex;        //Mutex to protect per-Channel data update
    FlagsStr flags;     //Channel-related flags

private:
    lu_char_t* name;       	//This channel's name
    ChannelObserverQueue observers; //Observer Queue
    MasterMutex queueMutex; //Per-channel Mutex to protect Observer Queue

protected:
    /**
     * \brief Compose the channel name and store it locally
     */
    void updateName();

    /**
     * \brief Notify all observers the channel value has changed
     */
    virtual void updateObservers();



};

#endif // !defined(EA_02822390_53A3_4df5_BFF9_716514738251__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
