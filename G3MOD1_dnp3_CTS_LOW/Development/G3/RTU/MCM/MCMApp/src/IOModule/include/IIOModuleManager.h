/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       IOModuleManager interface definition
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   21/06/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_97CE67EF_12B4_4af3_AC59_7FD1BB615C00__INCLUDED_)
#define EA_97CE67EF_12B4_4af3_AC59_7FD1BB615C00__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <vector>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IIOModule.h"
#include "CANProtocol.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
class IIOModuleManager
{
public:
    typedef std::vector<IIOModule*> ModuleList;
public:
    /**
     * \brief Common OLR button delays configuration
     */
    struct OLRButtonConfig
    {
    public:
        lu_uint32_t toggleTimeOFF_ms;   //Delay for long pressed button to set to OFF
        lu_uint32_t toggleTimeLR_ms;    //Delay for short press to toggle Local/Remote
    public:
        OLRButtonConfig() : toggleTimeOFF_ms(3000), //Default values
                            toggleTimeLR_ms(500)
        {};
    };

public:
    IIOModuleManager() {}
    virtual ~IIOModuleManager() {}

    /**
     * \brief Create a module even if it is not connected
     *
     * \param module Module type
     * \param moduleID Module ID number
     * \param iFace Interface where the Module is supposed to be connected
     *
     * \return pointer to the module. NULL if it wasn't successful
     */
    virtual IIOModule *createModule(MODULE module,
                    MODULE_ID moduleID,
                    COMM_INTERFACE iFace = COMM_INTERFACE_UNKNOWN) = 0;


	/**
	 * \brief Get a module reference
	 * 
	 * \param module Module type
	 * \param moduleId Module Identifier (address)
	 * 
	 * \return Pointer to the module. NULL if the module doesn't exist
	 */
	virtual IIOModule *getModule(MODULE module, MODULE_ID moduleID) = 0;
	virtual IIOModule *getModule(IOModuleIDStr mID)
    {
        return getModule(mID.type, mID.id);
    }

	/**
	 * \brief Get list of all modules
	 * 
	 * \return Module list. Empty list when error.
	 */
    virtual std::vector<IIOModule*> getAllModules() = 0;

    /**
     * \brief Get a channel reference
     *
     * \param module Module type
     * \param moduleId Module Identifier (address)
     * \param chType Channel type
     * \param chNumber Channel Number
     *
     * \return Pointer to the channel. NULL if the channel/module doesn't exist
     */
    virtual IChannel *getChannel( MODULE module,
                                  MODULE_ID moduleID,
                                  CHANNEL_TYPE chType,
                                  lu_uint32_t chNumber
                                ) = 0;

    /**
      * \brief Stop all modules
      *
      * \return Error code
      */
    virtual IOM_ERROR stopAllModules() = 0;
};


#endif // !defined(EA_97CE67EF_12B4_4af3_AC59_7FD1BB615C00__INCLUDED_)


/*
 *********************** End of file ******************************************
 */
