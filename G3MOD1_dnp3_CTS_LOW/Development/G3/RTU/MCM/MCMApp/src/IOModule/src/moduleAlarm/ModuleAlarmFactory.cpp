/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ModuleAlarmFactory.cpp 14 Apr 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/IOModule/src/ModuleAlarmFactory.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 14 Apr 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14 Apr 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "ModuleAlarmFactory.h"

/* Alarm IDs */
//#include "SysAlarm/SysAlarmAuxSwitchControllerEnum.h"
//#include "SysAlarm/SysAlarmBatteryChargerEnum.h"
//#include "SysAlarm/SysAlarmCalibrationEnum.h"
//#include "SysAlarm/SysAlarmDigitalOutControllerEnum.h"
//#include "SysAlarm/SysAlarmFanControllerEnum.h"
//#include "SysAlarm/SysAlarmHmiControllerEnum.h"
//#include "SysAlarm/SysAlarmIoManagerEnum.h"
//#include "SysAlarm/SysAlarmLcdDriverEnum.h"
#include "SysAlarm/SysAlarmMCMEnum.h"
#include "SysAlarm/SysAlarmNvramEnum.h"
//#include "SysAlarm/SysAlarmSerialProtocolEnum.h"
//#include "SysAlarm/SysAlarmStatusManagerEnum.h"
//#include "SysAlarm/SysAlarmSupplyControllerEnum.h"
//#include "SysAlarm/SysAlarmSwitchControllerEnum.h"
#include "SysAlarm/SysAlarmSystemEnum.h"

/* Alarm types */
#include "ModuleAlarmStanding.h"
#include "ModuleAlarmPersistent.h"
#include "ModuleAlarmReoccurring.h"
#include "ModuleAlarmAutomatic.h"
#include "ModuleAlarmAutoClear.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
ModuleAlarm* ModuleAlarmFactory::createAlarm(const SYS_ALARM_SUBSYSTEM subSystem,
                                             const lu_uint32_t alarmCode
                                             )
{
    ModuleAlarm::AlarmIDStr id(subSystem, alarmCode);
    return createAlarm(id);
}

ModuleAlarm* ModuleAlarmFactory::createAlarm(const ModuleAlarm::AlarmIDStr id)
{
    /* Don't forget to add/remove alarm codes here as well as in the XML files! */
#if ( (SYSALC_SYSTEM_EnumSize != 21) || (SYSALC_MCM_EnumSize != 17) || (SYSALC_NVRAM_EnumSize != 3) )
#error "Alarm codes updated: please update ModuleAlarmFactory::createAlarm as well!"
#endif

    ModuleAlarm* newAlarm = NULL;
    /**
     * This is the list of possible alarm combinations reported by the MCM-only
     * (NOT the ones from Slave Modules), and its default severity.
     */
    switch(id.subSystem)
    {
        case SYS_ALARM_SUBSYSTEM_SYSTEM:
            switch(id.alarmCode)
            {
                case SYSALC_SYSTEM_NOCONFIG:
                case SYSALC_SYSTEM_BADCONFIG:
                case SYSALC_SYSTEM_SYSTEMINIT:
                    newAlarm = new ModuleAlarmPersistent(id);
                    newAlarm->severity = SYS_ALARM_SEVERITY_CRITICAL;
                    break;
                case SYSALC_SYSTEM_PRIMARY_FACTORY_NVRAM:
                case SYSALC_SYSTEM_PRIMARY_FACTORY_CAL:
                case SYSALC_SYSTEM_CONFIGPARSE:
                    newAlarm = new ModuleAlarmPersistent(id);
                    newAlarm->severity = SYS_ALARM_SEVERITY_WARNING;
                    break;
                case SYSALC_SYSTEM_FACTORY_NVRAM:
                case SYSALC_SYSTEM_FACTORY_CAL:
                    newAlarm = new ModuleAlarmPersistent(id);
                    newAlarm->severity = SYS_ALARM_SEVERITY_WARNING;
                    break;
                case SYSALC_SYSTEM_MODULE_REGISTRATION:
                    newAlarm = new ModuleAlarmStanding(id);
                    newAlarm->severity = SYS_ALARM_SEVERITY_ERROR;
                    break;
                case SYSALC_SYSTEM_SLAVE_MODULE:
                    newAlarm = new ModuleAlarmAutomatic(id);
                    newAlarm->severity = SYS_ALARM_SEVERITY_CRITICAL;
                    break;
                default:
                    break;
            }
            break;
        case SYS_ALARM_SUBSYSTEM_MCM:
            switch(id.alarmCode)
            {
                case SYSALC_MCM_FEATURE_MISMATCH:
                    newAlarm = new ModuleAlarmPersistent(id);
                    newAlarm->severity = SYS_ALARM_SEVERITY_ERROR;
                    break;
                case SYSALC_MCM_SENSOR_FAILURE:
                case SYSALC_MCM_LOCALREMOTE_LINE:
                    newAlarm = new ModuleAlarmReoccurring(id);
                    newAlarm->severity = SYS_ALARM_SEVERITY_ERROR;
                    break;
                case SYSALC_MCM_CAN0_BUS_ERROR:
                case SYSALC_MCM_CAN1_BUS_ERROR:
                case SYSALC_MCM_RTU_VOLTAGE_UNSAFE:
                case SYSALC_MCM_CPU_VOLTAGE_UNSAFE:
                case SYSALC_MCM_CPU_OVERTEMP:
                case SYSALC_MCM_CPU_UNDERTEMP:
                    newAlarm = new ModuleAlarmReoccurring(id);
                    newAlarm->severity = SYS_ALARM_SEVERITY_WARNING;
                    break;
                case SYSALC_MCM_MODULE_SERIAL:
                    newAlarm = new ModuleAlarmStanding(id);
                    newAlarm->severity = SYS_ALARM_SEVERITY_CRITICAL;
                    break;
                case SYSALC_MCM_MODULE_NOT_CFG:
                    newAlarm = new ModuleAlarmReoccurring(id);
                    newAlarm->severity = SYS_ALARM_SEVERITY_CRITICAL;
                    break;
                case SYSALC_MCM_MODULE_UNEXPECTED:
                    newAlarm = new ModuleAlarmAutoClear(id);
                    newAlarm->severity = SYS_ALARM_SEVERITY_INFO;
                    break;
                case SYSALC_MCM_MODULE_UNREGISTERED:
                    newAlarm = new ModuleAlarmAutoClear(id);
                    newAlarm->severity = SYS_ALARM_SEVERITY_ERROR;
                    break;
                case SYSALC_MCM_MODULE_MISSING:
                    newAlarm = new ModuleAlarmAutoClear(id);
                    newAlarm->severity = SYS_ALARM_SEVERITY_CRITICAL;
                    break;
                case SYSALC_MCM_MODULE_SYSTEMAPI:
                case SYSALC_MCM_MODULE_REMOVED:
                    newAlarm = new ModuleAlarmAutomatic(id);
                    newAlarm->severity = SYS_ALARM_SEVERITY_CRITICAL;
                    break;
                default:
                    break;
            }
            break;
        case SYS_ALARM_SUBSYSTEM_NVRAM:
            switch(id.alarmCode)
            {
                case SYSALC_NVRAM_CRC_ERR:
                case SYSALC_NVRAM_READ_ERR:
                case SYSALC_NVRAM_WRITE_ERR:
                    newAlarm = new ModuleAlarmStanding(id);
                    newAlarm->severity = SYS_ALARM_SEVERITY_WARNING;
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }
    return newAlarm;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
