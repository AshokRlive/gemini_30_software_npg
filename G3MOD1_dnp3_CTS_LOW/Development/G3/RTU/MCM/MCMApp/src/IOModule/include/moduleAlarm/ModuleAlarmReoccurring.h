/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ModuleAlarmReoccurring.h 14 Apr 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/IOModule/include/ModuleAlarmReoccurring.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Module Alarm that goes inactive when acknowledged, and back to active
 *       when re-issued.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 14 Apr 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14 Apr 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MODULEALARMREPEATABLE_H__INCLUDED
#define MODULEALARMREPEATABLE_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "ModuleAlarm.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Repeatable Module Alarm
 *
 * This alarm goes inactive when acknowledged, and requires to be activated again
 * to be set back to active.
 */
struct ModuleAlarmReoccurring : public ModuleAlarm
{
public:
    ModuleAlarmReoccurring() : ModuleAlarm() {};
    ModuleAlarmReoccurring(const ModuleAlarm::AlarmIDStr id) : ModuleAlarm(id) {};
    virtual ~ModuleAlarmReoccurring();

    /* ==Overridden from ModuleAlarm== */
    virtual ModuleAlarmReoccurring* clone() const { return new ModuleAlarmReoccurring(*this); };
    virtual void activate();
    virtual void acknowledge();
};

#endif /* MODULEALARMREPEATABLE_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
