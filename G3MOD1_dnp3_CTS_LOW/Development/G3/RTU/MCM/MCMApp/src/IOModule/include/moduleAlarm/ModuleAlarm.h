/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ModuleAlarm.h 22 Jan 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/IOModule/include/ModuleAlarm.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Module Alarm definition module.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 22 Jan 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   22 Jan 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MODULEALARM_H__INCLUDED
#define MODULEALARM_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <vector>
#include <string>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "SysAlarmCodeEnum.h"
#include "IOModuleCommon.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/* Forward declaration */
class ModuleAlarm;

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

//Note: must be a vector of pointers to avoid "object slicing" of any derived object
typedef std::vector<ModuleAlarm*> ModuleAlarmList;   //List of Module Alarms

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/* NOTE: "static const ModuleAlarmList emptyAlarmList(0)" is declared down after
 * the class implementation since the vector requires the class to be fully
 * implemented when creating the vector -- even when the created vector is empty!
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Module Alarm data
 *
 * This class acts as an structure to maintain Module-related alarms.
 * The class does not handle any lock for ensuring exclusive access to the alarm.
 */
struct ModuleAlarm
{
public:
    typedef lu_uint32_t AlarmState;             //Type for internal state of the alarm
    static const AlarmState STATE_INACTIVE = 0; //Idle state of the alarm
    struct AlarmIDStr
    {
    public:
        SYS_ALARM_SUBSYSTEM subSystem; //Alarm subsystem - related to SYS_ALARM_SUBSYSTEM_*
        lu_uint32_t         alarmCode; //Subsystem-specific alarm code - e.g. SYSALC_SYSTEM_*
    public:
        AlarmIDStr() :
            subSystem(SYS_ALARM_SUBSYSTEM_LAST),
            alarmCode(0)
        {};
        AlarmIDStr(SYS_ALARM_SUBSYSTEM subSys, lu_uint32_t codeAlarm) :
                                                            subSystem(subSys),
                                                            alarmCode(codeAlarm)
        {};
        AlarmIDStr& operator=(const ModuleAlarm& moduleAlarm);

        /*!
         * \brief Tells if the alarm ID is valid
         */
        lu_bool_t isValid() const;

        /*!
         * \brief Gives the Alarm Code string
         */
        const lu_char_t* toCString() const;
    };

public:
    SYS_ALARM_SUBSYSTEM subSystem; //Alarm subsystem - related to SYS_ALARM_SUBSYSTEM_*
    lu_uint32_t         alarmCode; //Subsystem-specific alarm code - e.g. SYSALC_SYSTEM_*
    SYS_ALARM_SEVERITY  severity;  //Severity of the alarm
    lu_uint16_t         parameter; //additional debug info

public:
    ModuleAlarm();
    ModuleAlarm(const SYS_ALARM_SUBSYSTEM subSystem, const lu_uint32_t alarmCode);
    ModuleAlarm(const ModuleAlarm::AlarmIDStr id);
    /*
     * Custom constructor for creating an specific alarm
     */
    ModuleAlarm(SYS_ALARM_SUBSYSTEM subSys,
                lu_uint32_t codeAlarm,
                AlarmState stateAlarm,
                SYS_ALARM_SEVERITY  severityAlarm,
                lu_uint16_t         alarmParameter = 0);
    virtual ~ModuleAlarm();

    /**
     * \brief Clone method
     *
     * IMPORTANT: Implement it in each derived class
     */
    virtual ModuleAlarm* clone() const { return new ModuleAlarm(*this); };

public:
    /**
     * \brief Operator to overload assignments between an alarm and an alarm ID
     */
    ModuleAlarm& operator=(const ModuleAlarm::AlarmIDStr& alarmID);

    /**
     * \brief Gets the ID of an alarm
     *
     * \return ID structure of the alarm
     */
    ModuleAlarm::AlarmIDStr getID();

    /**
     * \brief Checks this alarm ID against the one given
     *
     * Compares both alarm IDs and gives a matching result.
     *
     * \param alarmID ID structure to compare against
     * \param alarmToCompare Alarm structure to compare against its ID.
     *
     * \return LU_TRUE when they match.
     */
    lu_bool_t checkID(const ModuleAlarm::AlarmIDStr& alarmID);
    lu_bool_t checkID(const ModuleAlarm& alarmToCompare);

    /**
     * \brief Check validity of the alarm
     *
     * \return alarm validity
     */
    lu_bool_t isValid();

    /**
     * \brief Invalidates this alarm
     *
     * An invalidated alarm can be still operated. This might be used to remove
     * invalid alarms from lists.
     * An invalidated alarm cannot be valid again.
     */
    virtual void invalidate();

    /**
     * \brief Tells when this alarm is in an state that must be considered active.
     *
     * Usually, an alarm is active when it is activated or re-activated.
     *
     * \return LU_TRUE when an alarm is considered in an active state.
     */
    virtual lu_bool_t isActive();

    /**
     * \brief Tells when the alarm is considered severe
     *
     * \return LU_TRUE when the severity of the alarm is more than INFO.
     */
    virtual lu_bool_t isSevere();

    /**
     * \brief Tells when the alarm is in active and considered severe enough.
     *
     * \return LU_TRUE when alarm is active and severe.
     */
    virtual lu_bool_t isImportant();

    /**
     * \brief Gets the current state of the alarm.
     *
     * \return Current state of the alarm.
     */
    virtual AlarmState getState();

    /**
     * \brief Get the status and description of an alarm in string format.
     *
     * \return String containing a line with an alarm table entry description
     */
    std::string toString();

    /**
     * \brief Get the status and description of an alarm in short string format.
     *
     * \return String containing a line with less alarm information
     */
    std::string toShortString();

    /**
     * \brief Get the status of an alarm in string format.
     *
     * \return String containing the status of the current alarm
     */
    std::string stateToString();


    /* ===== Module Alarm's main available actions ==== */
    /**
     * \brief Activates an alarm
     *
     * This method tells the alarm that a stimulus to activate it has been
     * triggered.
     */
    virtual void activate();

    /**
     * \brief Deactivates an alarm
     *
     * This method tells the alarm that a stimulus to activate it is not present
     * anymore. Take in mind that this might or might not affect the status of
     * the alarm, since it depends on the alarm type.
     */
    virtual void deactivate();

    /**
     * \brief Acknowledges an alarm
     *
     * Tells an alarm that it has been successfully reported.
     */
    virtual void acknowledge();

    /**
     * \brief Clears an alarm
     *
     * Declares an alarm as cleared, that might mean that the alarm is inactive.
     */
    virtual void clear();

protected:
    AlarmState state; //Reported alarm state - related to SYS_ALARM_STATE and STATE_INACTIVE

private:
    /**
     * \brief Initialises the default values of an alarm
     */
    void init();

private:
    lu_bool_t valid;    //validity of the alarm

};


static const ModuleAlarmList emptyAlarmList(0); //Empty list of Module Alarms


#endif /* MODULEALARM_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
