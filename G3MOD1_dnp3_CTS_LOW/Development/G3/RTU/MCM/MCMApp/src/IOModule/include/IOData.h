/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_48B6251C_C31F_48f9_AA8B_EB62BFB89B6F__INCLUDED_)
#define EA_48B6251C_C31F_48f9_AA8B_EB62BFB89B6F__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stddef.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "TimeManager.h"    //timestamp

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * This interface defines a general way to access different type of data
 * transparently. The assignment operator and the unary cast operator are
 * overloaded. It's possible to use the assignment operator to
 * set/get the internal value of the class. The appropriate casting is
 * transparently applied.
 */
class IOData
{
public:
    IOData() {};
    virtual ~IOData() {};

    void setTime(TimeManager::TimeStr& timeStamp)
    {
        timestamp = timeStamp;
    };

    void getTime(TimeManager::TimeStr& timeStamp)
    {
        timeStamp = timestamp;
    }

    TimeManager::TimeStr *getTime()
    {
        return &timestamp;
    }

    virtual IOData& operator=(lu_uint8_t   a) = 0;
    virtual IOData& operator=(lu_int8_t    a) = 0;
    virtual IOData& operator=(lu_uint16_t  a) = 0;
    virtual IOData& operator=(lu_int16_t   a) = 0;
    virtual IOData& operator=(lu_uint32_t  a) = 0;
    virtual IOData& operator=(lu_int32_t   a) = 0;
    virtual IOData& operator=(lu_float32_t a) = 0;
    virtual operator lu_int8_t()    = 0;
    virtual operator lu_uint8_t()   = 0;
    virtual operator lu_int16_t()   = 0;
    virtual operator lu_uint16_t()  = 0;
    virtual operator lu_int32_t()   = 0;
    virtual operator lu_uint32_t()  = 0;
    virtual operator lu_float32_t() = 0;

protected:
    TimeManager::TimeStr timestamp;
};

template<class T>
class IODataT : public IOData
{

public:
    IODataT() {}

    virtual ~IODataT() {}

    virtual IOData& operator=(lu_uint8_t a)
    {
        val = (T)(a);
        return *this;
    }

    virtual IOData& operator=(lu_int8_t a)
    {
        val = (T)(a);
        return *this;
    }

    virtual IOData& operator=(lu_uint16_t a)
    {
        val = (T)(a);
        return *this;
    }

    virtual IOData& operator=(lu_int16_t a)
    {
        val = (T)(a);
        return *this;
    }

    virtual IOData& operator=(lu_uint32_t a)
    {
        val = (T)(a);
        return *this;
    }

    virtual IOData& operator=(lu_int32_t a)
    {
        val = (T)(a);
        return *this;
    }

    virtual IOData& operator=(lu_float32_t a)
    {
        val = (T)(a);
        return *this;
    }

    virtual operator lu_int8_t()
    {
        return (lu_int8_t)(val);
    }

    virtual operator lu_uint8_t()
    {
        return (lu_uint8_t)(val);
    }

    virtual operator lu_int16_t()
    {
        return (lu_int16_t)(val);
    }

    virtual operator lu_uint16_t()
    {
        return (lu_uint16_t)(val);
    }

    virtual operator lu_int32_t()
    {
        return (lu_int32_t)(val);
    }

    virtual operator lu_uint32_t()
    {
        return (lu_uint32_t)(val);
    }

    virtual operator lu_float32_t()
    {
        return (lu_float32_t)(val);
    }

protected:
    T val;

};

class IODataUint8 : public IODataT<lu_uint8_t>
{

public:
    IODataUint8() {};
    virtual ~IODataUint8() {};

    IODataUint8& operator=(lu_uint8_t a)
    {
        val = (lu_uint8_t)(a);
        return *this;
    }

    IODataUint8& operator=(lu_int8_t a)
    {
        val = (lu_uint8_t)(a);
        return *this;
    }

    IODataUint8& operator=(lu_uint16_t a)
    {
        val = (lu_uint8_t)(a);
        return *this;
    }

    IODataUint8& operator=(lu_int16_t a)
    {
        val = (lu_uint8_t)(a);
        return *this;
    }

    IODataUint8& operator=(lu_uint32_t a)
    {
        val = (lu_uint8_t)(a);
        return *this;
    }

    IODataUint8& operator=(lu_int32_t a)
    {
        val = (lu_uint8_t)(a);
        return *this;
    }

    IODataUint8& operator=(lu_float32_t a)
    {
        val = (lu_uint8_t)(a);
        return *this;
    }
};

class IODataInt8 : public IODataT<lu_int8_t>
{

public:
    IODataInt8() {};
    virtual ~IODataInt8() {};

    IODataInt8& operator=(lu_uint8_t a)
    {
        val = (lu_int8_t)(a);
        return *this;
    }

    IODataInt8& operator=(lu_int8_t a)
    {
        val = (lu_int8_t)(a);
        return *this;
    }

    IODataInt8& operator=(lu_uint16_t a)
    {
        val = (lu_int8_t)(a);
        return *this;
    }

    IODataInt8& operator=(lu_int16_t a)
    {
        val = (lu_int8_t)(a);
        return *this;
    }

    IODataInt8& operator=(lu_uint32_t a)
    {
        val = (lu_int8_t)(a);
        return *this;
    }

    IODataInt8& operator=(lu_int32_t a)
    {
        val = (lu_int8_t)(a);
        return *this;
    }

    IODataInt8& operator=(lu_float32_t a)
    {
        val = (lu_int8_t)(a);
        return *this;
    }
};

class IODataUint16 : public IODataT<lu_uint16_t>
{

public:
    IODataUint16() {};
    virtual ~IODataUint16() {};

    IODataUint16& operator=(lu_uint8_t a)
    {
        val = (lu_uint16_t)(a);
        return *this;
    }

    IODataUint16& operator=(lu_int8_t a)
    {
        val = (lu_uint16_t)(a);
        return *this;
    }

    IODataUint16& operator=(lu_uint16_t a)
    {
        val = (lu_uint16_t)(a);
        return *this;
    }

    IODataUint16& operator=(lu_int16_t a)
    {
        val = (lu_uint16_t)(a);
        return *this;
    }

    IODataUint16& operator=(lu_uint32_t a)
    {
        val = (lu_uint16_t)(a);
        return *this;
    }

    IODataUint16& operator=(lu_int32_t a)
    {
        val = (lu_uint16_t)(a);
        return *this;
    }

    IODataUint16& operator=(lu_float32_t a)
    {
        val = (lu_uint16_t)(a);
        return *this;
    }
};

class IODataInt16 : public IODataT<lu_int16_t>
{

public:
    IODataInt16() {};
    virtual ~IODataInt16() {};

    IODataInt16& operator=(lu_uint8_t a)
    {
        val = (lu_int16_t)(a);
        return *this;
    }

    IODataInt16& operator=(lu_int8_t a)
    {
        val = (lu_int16_t)(a);
        return *this;
    }

    IODataInt16& operator=(lu_uint16_t a)
    {
        val = (lu_int16_t)(a);
        return *this;
    }

    IODataInt16& operator=(lu_int16_t a)
    {
        val = (lu_int16_t)(a);
        return *this;
    }

    IODataInt16& operator=(lu_uint32_t a)
    {
        val = (lu_int16_t)(a);
        return *this;
    }

    IODataInt16& operator=(lu_int32_t a)
    {
        val = (lu_int16_t)(a);
        return *this;
    }

    IODataInt16& operator=(lu_float32_t a)
    {
        val = (lu_int16_t)(a);
        return *this;
    }
};

class IODataUint32 : public IODataT<lu_uint32_t>
{

public:
    IODataUint32() {};
    virtual ~IODataUint32() {};

    IODataUint32& operator=(lu_uint8_t a)
    {
        val = (lu_uint32_t)(a);
        return *this;
    }

    IODataUint32& operator=(lu_int8_t a)
    {
        val = (lu_uint32_t)(a);
        return *this;
    }

    IODataUint32& operator=(lu_uint16_t a)
    {
        val = (lu_uint32_t)(a);
        return *this;
    }

    IODataUint32& operator=(lu_int16_t a)
    {
        val = (lu_uint32_t)(a);
        return *this;
    }

    IODataUint32& operator=(lu_uint32_t a)
    {
        val = (lu_uint32_t)(a);
        return *this;
    }

    IODataUint32& operator=(lu_int32_t a)
    {
        val = (lu_uint32_t)(a);
        return *this;
    }

    IODataUint32& operator=(lu_float32_t a)
    {
        val = (lu_uint32_t)(a);
        return *this;
    }
};

class IODataInt32 : public IODataT<lu_int32_t>
{

public:
    IODataInt32() {};
    virtual ~IODataInt32() {};

    IODataInt32& operator=(lu_uint8_t a)
    {
        val = (lu_int32_t)(a);
        return *this;
    }

    IODataInt32& operator=(lu_int8_t a)
    {
        val = (lu_int32_t)(a);
        return *this;
    }

    IODataInt32& operator=(lu_uint16_t a)
    {
        val = (lu_int32_t)(a);
        return *this;
    }

    IODataInt32& operator=(lu_int16_t a)
    {
        val = (lu_int32_t)(a);
        return *this;
    }

    IODataInt32& operator=(lu_uint32_t a)
    {
        val = (lu_int32_t)(a);
        return *this;
    }

    IODataInt32& operator=(lu_int32_t a)
    {
        val = (lu_int32_t)(a);
        return *this;
    }

    IODataInt32& operator=(lu_float32_t a)
    {
        val = (lu_int32_t)(a);
        return *this;
    }
};

class IODataFloat32 : public IODataT<lu_float32_t>
{

public:
    IODataFloat32() {};
    virtual ~IODataFloat32() {};

    IODataFloat32& operator=(lu_uint8_t a)
    {
        val = (lu_float32_t)(a);
        return *this;
    }

    IODataFloat32& operator=(lu_int8_t a)
    {
        val = (lu_float32_t)(a);
        return *this;
    }

    IODataFloat32& operator=(lu_uint16_t a)
    {
        val = (lu_float32_t)(a);
        return *this;
    }

    IODataFloat32& operator=(lu_int16_t a)
    {
        val = (lu_float32_t)(a);
        return *this;
    }

    IODataFloat32& operator=(lu_uint32_t a)
    {
        val = (lu_float32_t)(a);
        return *this;
    }

    IODataFloat32& operator=(lu_int32_t a)
    {
        val = (lu_float32_t)(a);
        return *this;
    }

    IODataFloat32& operator=(lu_float32_t a)
    {
        val = (lu_float32_t)(a);
        return *this;
    }
};

#endif // !defined(EA_48B6251C_C31F_48f9_AA8B_EB62BFB89B6F__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
