/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ModuleAlarmAutomatic.h 23 Apr 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/IOModule/include/moduleAlarm/ModuleAlarmAutomatic.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 23 Apr 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   23 Apr 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MODULEALARMAUTOMATIC_H__INCLUDED
#define MODULEALARMAUTOMATIC_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "ModuleAlarm.h"
#include "ModuleAlarmReoccurring.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Auto-acknowledgeable module alarm
 *
 * This alarm becomes automatically acknowledged as soon as it is deactivated.
 */
class ModuleAlarmAutomatic: public ModuleAlarmReoccurring
{
public:
    ModuleAlarmAutomatic() : ModuleAlarmReoccurring() {};
    ModuleAlarmAutomatic(const ModuleAlarm::AlarmIDStr id) : ModuleAlarmReoccurring(id) {};
    virtual ~ModuleAlarmAutomatic();

    /* ==Overridden from ModuleAlarm== */
    virtual ModuleAlarmAutomatic* clone() const { return new ModuleAlarmAutomatic(*this); };
    virtual void deactivate();
};

#endif /* MODULEALARMAUTOMATIC_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
