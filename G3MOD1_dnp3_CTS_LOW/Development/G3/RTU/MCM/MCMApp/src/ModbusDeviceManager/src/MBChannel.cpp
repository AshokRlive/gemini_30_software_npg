/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MBChannel.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13 May 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "MBChannel.h"
#include "mbm_helpers.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define DSICARD_EVENT_INVALID_TYPE(event)\
           DBG_WARN("Discarded event.Channel type invalid:%i",\
                event.channel.channelType)
/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static TimeManager& timeMgr = TimeManager::getInstance();

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
namespace mbm{


MBChannel::MBChannel(CHANNEL_TYPE chtype, lu_uint32_t _chid)
                            :IChannel(chtype, _chid),
                            log(Logger::getLogger(SUBSYSTEM_ID_MODBUS_MASTER)),
                            m_value(0),
                            mp_conf(NULL),
                            mp_socket(NULL)

{
    DBG_INFO("C: [Ctor]MBDevice Channel: %s", getID().toString().c_str());

    timeMgr.getTime(m_timeStamp);   //Use creation time for 1st channel timestamp
}

MBChannel::~MBChannel()
{
    DBG_INFO("C: [Dtor]MBDevice Channel: %s", getID().toString().c_str());

    releaseConfig();
}


void MBChannel::releaseConfig()
{
    if(mp_conf != NULL)
    {
        DBG_INFO("Releasing config: %s.",mp_conf->channel.toString().c_str());
        delete mp_conf;
        mp_conf = NULL;
    }
}


void MBChannel::setConfig(ChannelConf& conf)
{
    if(mp_conf == NULL)
    {
        mp_conf = new ChannelConf();
        memcpy(mp_conf, &conf, sizeof(ChannelConf));
    }
}

void MBChannel::applyConfig(zmq::socket_t& socket)
{
    if(mp_conf != NULL)
    {

        MsgHeader head(MSG_TYPE_CFG, MSG_ID_CFG_CHANNEL_C);
        socket.send(&head, sizeof(head), ZMQ_SNDMORE);
        dump_data(&head,sizeof head);
        socket.send(mp_conf, sizeof(*mp_conf), 0);
        dump_data(mp_conf,sizeof(*mp_conf));

        DBG_INFO("<===  Send channel config. %s",mp_conf->toString().c_str());


        // Get reply
        zmq::message_t reply;
        do{
           socket.recv(&reply);
        }
        while(reply.more());

        mp_socket = &socket; // Keep reference of socket for writing value to this channel.

        flags.online = LU_TRUE;
    }
    else
    {
        log.error("No configuration");
    }

}



IOM_ERROR MBChannel::read(ValueStr& valuePtr)
{
    IOM_ERROR ret;

    if(valuePtr.dataPtr == NULL)
    {
        return IOM_ERROR_PARAM;
    }

    {
        /* Valid local value. Lock mutex */
        LockingMutex lMutex(mutex);

        /* Use the last updated value */

        if(ch_id.type == CHANNEL_TYPE_AINPUT)
        {
            *(valuePtr.dataPtr) = *((lu_float32_t*)&m_value);
        }
        else if(ch_id.type == CHANNEL_TYPE_DINPUT)
        {
            *(valuePtr.dataPtr) = m_value;
        }

        valuePtr.dataPtr->setTime(m_timeStamp);
        ret = IOM_ERROR_NONE;
    }

    valuePtr.flags = flags;
    return ret;
}

/*
 * Selection of MB channel is always success.
 */
IOM_ERROR MBChannel::select(lu_uint8_t selectTimeout, lu_bool_t local)
{
    LU_UNUSED(selectTimeout);
    LU_UNUSED(local);
    return IOM_ERROR_NONE;
}

IOM_ERROR MBChannel::operate(lu_bool_t v, lu_bool_t local, lu_uint8_t delay)
{
    LU_UNUSED(local);
    LU_UNUSED(delay);
    IODataUint8 data;
    IChannel::ValueStr value(data);
    data = (lu_uint8_t)v;
    return write(value);
}

IOM_ERROR MBChannel::write(ValueStr& valuePtr)
{
    mbm::MsgHeader msgHeader;
    IOM_ERROR ret = IOM_ERROR_WRITE;
    bool success = false;
    bool writesuccess = false;

    if(valuePtr.dataPtr == NULL)
    {
        return IOM_ERROR_PARAM;
    }

    if(mp_socket == NULL)
    {
        DBG_ERR("%s: ZMQ Socket not available",getName());
        flags.online = LU_FALSE;
        return IOM_ERROR_WRITE;
    }

    if(getID().type != CHANNEL_TYPE_DOUTPUT)
    {
        DBG_ERR("%s: Writing not supported",getName());
        return IOM_ERROR_NOT_SUPPORTED;
    }

    // Get reply
    zmq::message_t reply;

    /* Valid local value. Lock mutex */
    {
        LockingMutex lMutex(mutex);

        /* Update internal value and write to MB module */
        m_value = (lu_uint8_t)(*(valuePtr.dataPtr));


        /* Send ZMQ header*/
        MsgHeader head(MSG_TYPE_WRITE, MSG_ID_WRITE_CHANNEL_C);
        mp_socket->send(&head, sizeof(head), ZMQ_SNDMORE);
        dump_data(&head,sizeof head);


        /* Send ZMQ content*/
        ChannelValue content;
        content.chRef = (*mp_conf).channel;
        content.chValue = m_value;
        mp_socket->send(&content, sizeof(content), 0);
        DBG_INFO("<===  Write channel. %s",content.toString().c_str());

        lu_int32_t idxxx = 0;

        do{
            success = mp_socket->recv(&reply);
            if(success == false)
            {
                DBG_ERR("ZMQ send failed!!!!!");
                perror("ZMQ recv failed");
            }
            else
            {
                if(reply.size() != sizeof(msgHeader))
                {
                    DBG_ERR("Invalid header size:%i expected:%i %s",reply.size(),sizeof(msgHeader),__AT__);
                }
                if(idxxx == 0) // Header
                {
                    memcpy(&msgHeader, reply.data(), sizeof(msgHeader));

                }
                else if(idxxx == 1) // Payload
                {
                    DBG_INFO("==> Received reply. size:%i ", reply.size());
                    if(msgHeader.type == MSG_TYPE_WRITE && msgHeader.id == MSG_ID_WRITE_CHANNEL_R
                                    && reply.size() == sizeof(MsgAck))
                    {
                        mbm::MsgAck* p = (mbm::MsgAck*)(reply.data());
                        writesuccess = ((WRITE_ERR)p->errorno) == WRITE_SUCCESS;
                    }
                }
                idxxx ++;
            }

        }
        while(success && reply.more());


        if(writesuccess == false)
        {
            log.error("%s - write error", getName());
            DBG_ERR("%s write failed!",getName());

            discardRcvMsg(*mp_socket, reply);
        }
        else
        {
            DBG_INFO("%s write succeeded! new value:%d",getName(),m_value);
            flags.restart = LU_FALSE;
            ret = IOM_ERROR_NONE;
        }
    }

    return ret;
}


void MBChannel::handleEvent(ChannelEvent& event)
{
    if(event.channel.channelId != ch_id.id || event.channel.channelType != ch_id.type)
    {
        DBG_ERR("Unexpected event. Expected channel id[%i:%i] actual:[%i:%i]",
                        ch_id.type,ch_id.id,
                        event.channel.channelType, event.channel.channelId);
    }

      /* Start critical region */
      {
          LockingMutex lMutex(mutex);
          lu_int32_t oldValue = m_value;

          /* update current value & quality */
          if(ch_id.type == CHANNEL_TYPE_AINPUT)
          {
              m_value = event.analogValue;
          }
          else if(ch_id.type == CHANNEL_TYPE_DINPUT)
          {
              m_value = event.digitalValue;
          }
          else
          {
              log.error("Unsupported event type:%i",ch_id.type);
              return;
          }

          DBG_TRACK("%s: value changed from %i to %i online:%i",getName(),oldValue,m_value,event.isOnline);
          LU_UNUSED(oldValue);

          flags.online = event.isOnline;
          flags.restart = LU_FALSE;

          /* Update time stamp from module time */
          m_timeStamp.time = event.time.timestamp;
          m_timeStamp.synchronised = timeMgr.getSyncStatus();

      }

      /* Update observers */
      updateObservers();
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

}/* End of namespace mbm*/
/*
 *********************** End of file ******************************************
 */
