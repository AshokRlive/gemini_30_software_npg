//
//  Hello World server in C++
//  Binds REP socket to tcp://*:5555
//  Expects "Hello" from client, replies with "World"
//
#include <zmq.hpp>
#include <string>
#include <iostream>
#ifndef _WIN32
#include <unistd.h>
#else
#include <windows.h>
#endif
#include <unistd.h> // sleep

#include <assert.h>
#include <exception>

#include "MBMConfigProto.h"
#include "MBDebug.h"
#include "mbm_helpers.h"


#define SNAME "SIM:"  // simulator name

static bool running = false;

static zmq::context_t s_context (1);
static zmq::socket_t s_socket(s_context, ZMQ_REP);
static pthread_t s_db_thread = 0;
static int eventIdx = 0;

static void initDB();

using namespace mbm;

static void handleCommand(mbm::MSG_ID id, zmq::message_t& msg)
{
    std::string s;
    switch(id)
    {
        case MSG_ID_CMD_PING_C    : s = "PING";     break;
        case MSG_ID_CMD_PING_R    : s = "?";        break;
        case MSG_ID_CMD_START_C   : s = "START";    break;
        case MSG_ID_CMD_START_R   : s = "?";        break;
        case MSG_ID_CMD_STOP_C    : s = "STOP";     running = false; break;
        case MSG_ID_CMD_STOP_R    : s = "";         break;
        default:                    s = "N/A";      break;
    }

    DBG_INFO("%s Accept command: %s ",SNAME, s.c_str());

    LU_UNUSED(msg);
}

static void handleCfg(mbm::MSG_ID id, zmq::message_t& msg)
{
    if(msg.more())
    {
        switch (id) {
            case MSG_ID_CFG_CHANNEL_C:
            {
                if(s_socket.recv(&msg,0))
                {
                    ChannelConf* chcfg = (ChannelConf*)(msg.data());

                    if(msg.size() != sizeof(ChannelConf))
                        DBG_ERR("%s received channel cfg expected size:%i actual size:%i",SNAME,sizeof(ChannelConf),msg.size());

                    DBG_INFO("%s Accepted channel cfg.channel id:%i, device id: %i, datatype: %i, polling rate: %i",
                                    SNAME,
                                    chcfg->channel.channelId,
                                    chcfg->channel.device.deviceId,
                                    chcfg->datatype,
                                    chcfg->pollingRate
                          );
                    //dump_data(msg.data(),msg.size());
                }
            }
            break;

            case MSG_ID_CFG_DEVICE_C:
            {
                if(s_socket.recv(&msg,0)){

                    DeviceConf* devcfg = (DeviceConf*)msg.data();
                    assert(msg.size() == sizeof(DeviceConf));

                    DBG_INFO("%s Accepted device cfg(size:%i). Device ID: %i, Type: %i",
                                    SNAME,
                                    msg.size(),
                                    devcfg->ref.deviceId,
                                    devcfg->ref.deviceType
                          );
                    dump_data(msg.data(),msg.size());
                }
            }
            break;

            default:
                break;
        }

    }
    else
    {
        DBG_WARN("Rejected cfg:%d cause conf content is not found",id);
    }


}

static void handlePoll(mbm::MSG_ID id,zmq::message_t& msg)
{
    DBG_INFO("%s Accept polling command: %d ", SNAME,id);

    LU_UNUSED(msg);
}


void* runDatabase(void*)
{
    blockAllExceptTerm();

    try{
    DBG_INFO("%s Enter MBDatabase thread",SNAME);

    zmq::socket_t socket (s_context, ZMQ_PUB);
    socket.bind (MB_HOST_ADDRESS_SUB);
    DBG_INFO("%s Bind to %s",SNAME,MB_HOST_ADDRESS_SUB);

    // Prepare event source channel.
    ChannelRef ch;
    ch.channelId = 0;
    ch.channelType = mbm::MBDEVICE_CHANNEL_DI;
    ch.device.deviceId = 0;
    ch.device.deviceType = DEVICE_TYPE_SERIAL;


    MsgHeader header;

    while(1)
    {
        /* Send event header*/
        header.type = MSG_TYPE_EVENT;
        header.id = (header.id == MSG_ID_EVENT_ANALOGUE)? MSG_ID_EVENT_DIGITAL: MSG_ID_EVENT_ANALOGUE;
        socket.send(&header, sizeof(header), ZMQ_SNDMORE);

        /* Send event body */
        std::string ename;
        ename = commandName((MSG_TYPE)(header.type), (MSG_ID)(header.id));

        ChannelEvent event;
        event.channel = ch;
        event.isOnline = LU_TRUE;
        if(header.id == MSG_ID_EVENT_ANALOGUE)
        {
            ch.channelType = MBDEVICE_CHANNEL_AI;
            event.analogValue = rand() % 20 + 20;// 20 -40
        }
        else
        {
            ch.channelType = MBDEVICE_CHANNEL_DI;
            event.digitalValue = rand() % 20 + 1;// 1 -20
        }


        socket.send(&event, sizeof(event),0);
        DBG_INFO("%s Published:%s[size:%i]",SNAME, ename.c_str(),sizeof(event));

        eventIdx ++;
        sleep(rand() % 3 + 1);// sleep 1-4 seconds
    }

    DBG_INFO("%s Exit MBDatabase thread",SNAME);

    }catch(...)
    {
        DBG_ERR("%s database exception!!!",SNAME);
    }
    return 0;
}

static void initDB(){
    static lu_bool_t dbinit = LU_FALSE;

    if(dbinit == LU_FALSE)
    {
        DBG_INFO("%s Start MBDatabase thread",SNAME);
        int ret;
        ret = pthread_create(&s_db_thread,NULL, &runDatabase,NULL);
        if(ret != 0)
        {
            DBG_ERR("%s Fail to create MB event thread.",SNAME);
        }

        dbinit = LU_TRUE;
    }
    else
    {
        DBG_ERR("%s MB Database has already been initialised",SNAME);
    }
}

static void closeDB(){
    if(s_db_thread != 0)
    {
        pthread_cancel(s_db_thread);
        void* ret;

        int s = pthread_join(s_db_thread,&ret);
        if(s != 0 )
        {
          DBG_ERR("%s pthread_join failed",SNAME);
        }
        DBG_INFO("%s Joined with thread %i; returned value was %s\n",SNAME,
                              s_db_thread, (char *) ret);
        free(ret);      /* Free memory allocated by thread */
        s_db_thread = -1;
    }
}

void* MBMSimulatorRoutine (void*) {
    blockAllExceptTerm();

    DBG_INFO("%s Enter MBServer thread body. Server is launching...",SNAME);

    //Simulate initialisation
    sleep(1);

    s_socket.bind (MB_HOST_ADDRESS_REP);
    DBG_INFO("%s Bind to %s",SNAME,MB_HOST_ADDRESS_REP);

    initDB();

    running = true;
    while (running) {
        DBG_INFO("%s Server is running. Ready for handling request...",SNAME);
        zmq::message_t request;
        s_socket.recv (&request);//Wait for request

        // Parse 1st frame as header
        assert (request.size() == sizeof(mbm::MsgHeader));
        mbm::MsgHeader header;
        memcpy(&header, request.data(), request.size());

        MSG_ID id = static_cast<MSG_ID>(header.id);
        switch (header.type) {
            case  mbm::MSG_TYPE_CMD  :
                handleCommand(id,request);
                break;

            case  mbm::MSG_TYPE_CFG  :
                handleCfg(id,request);
                break;

//            case  mbm::MSG_TYPE_POLL :
//                handlePoll(id,request);
//                break;

            default:
                DBG_ERR("%s Unsupported request [%d:%i]",SNAME,header.type,header.id);
                break;
        }

          // Discard unprocessed message parts.
        discardRcvMsg(s_socket,request);


        //  Send reply to client
        zmq::message_t reply (5);
        memcpy ((void *) reply.data (), "ACK", 5);
        s_socket.send (reply);
    }

    closeDB();

    s_socket.close();
    s_context.close();

    DBG_INFO("%s Exit server thread",SNAME);
    return 0;
}
