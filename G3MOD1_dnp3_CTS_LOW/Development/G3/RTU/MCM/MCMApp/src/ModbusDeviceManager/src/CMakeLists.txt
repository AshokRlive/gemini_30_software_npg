cmake_minimum_required(VERSION 2.8)

add_library( ModbusDeviceManager STATIC
             MBChannel.cpp
             MBDevice.cpp
             MBDeviceManager.cpp
             MBMController.cpp
             MBDeviceFactory.cpp
             #MBMSim.cpp
           ) 
# MCMApp common includes
include_GeneralHeaders()

# Add dependency on LinIoTag Library
ADD_DEPENDENCIES(ModbusDeviceManager LinIoTarg)

# Common Library includes
#include_directories("${CMAKE_G3_PATH_COMMON_LIBRARY}/util/include")
#include_directories("${CMAKE_G3_PATH_COMMON_LIBRARY}/crc32/include/")
#include_directories("${CMAKE_G3_PATH_COMMON_LIBRARY}/LinearInterpolation/include/")
#include_directories("${CMAKE_G3_PATH_COMMON_LIBRARY}/NVRAMBlock/include/")

# Access to the TMW utils code
SET(CMAKE_MY_G3_PATH_MCM_THIRDPARTY "../../../../thirdParty")
include_directories(${CMAKE_MY_G3_PATH_MCM_THIRDPARTY}/TriangleMicroworks)

# Control Logic Interface include
include_directories(../../IODevice/include/)
include_directories(../../IOModule/include/)
include_directories(../../../includeGenerated/)


include_directories(../proto/)

# Generate Doxygen documentation
#gen_doxygen("MemoryManager" "")
