/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MBDevice.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13 May 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MBDEVICE_H_
#define MBDEVICE_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <zmq.hpp>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "MBChannel.h"
#include "MBMConfigProto.h"
#include "Logger.h"
#include "IDevice.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */

namespace mbm{

class MBDevice : public IDevice
{
public:
    MBDevice(MODULE_ID id);
    virtual ~MBDevice();

    lu_uint8_t getIndex(){return (lu_uint8_t)mid.id;};

    IOM_ERROR deviceRef(DeviceRef* ref)
    {
        if(mp_conf != NULL)
        {
            *ref = mp_conf->ref;
            return IOM_ERROR_NONE;
        }
        else
        {
            return IOM_ERROR_NOT_FOUND;
        }
    };

    MBChannel* getMBChannel(CHANNEL_TYPE chType, lu_uint16_t chIdx);

    lu_uint16_t getChannelNum(CHANNEL_TYPE chType);

    /*Override*/
    virtual IOM_ERROR readRAWChannel(IChannel::ChannelIDStr id,
                    IChannel::ValueStr& data);

    void printID();

    void printConfig();

    void applyConfig(zmq::socket_t& socket);

    void setConfig(DeviceConf& conf);

    void getDeviceStats(zmq::socket_t& socket,lu_uint8_t devID);

    DeviceConf* getConfig()
    {
        return mp_conf;
    }

    void setChannels(CHANNEL_TYPE type, MBChannel* chs[], lu_uint16_t chsNum);

    virtual IChannel* getChannel(CHANNEL_TYPE type, lu_uint32_t index);

    /**
     * Handling analogue event.
     */
    void handleEvent(ChannelEvent& event);



    static void sendDeviceNumConfig(zmq::socket_t& socket,lu_uint8_t tcpDevNum,lu_uint8_t serialDevNum);

private:

    void releaseConfig();

    lu_bool_t verifyChannel(ChannelRef& ch);

    Logger& log;

    MBChannel** mp_dinChs;
    lu_uint16_t m_dinChsNum;

    MBChannel** mp_doutChs;
    lu_uint16_t m_doutChsNum;

    MBChannel** mp_ainChs;
    lu_uint16_t m_ainChsNum;

    DeviceConf* mp_conf;
};

}/* End of namespace mbm*/

#endif /* MBDEVICE_H_ */

/*
 *********************** End of file ******************************************
 */
