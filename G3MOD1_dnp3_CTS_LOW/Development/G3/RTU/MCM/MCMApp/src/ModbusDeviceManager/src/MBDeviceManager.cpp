/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MBDeviceManager.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   2 Jun 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include  <assert.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MBDeviceManager.h"
#include "mbm_helpers.h"
#include "MBMController.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define DSICARD_EVENT_NO_DEVICE(event)\
           DBG_WARN("Discarded event.Device not found at index:%i.",event.channel.device.deviceId)
/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

namespace mbm{

MBDeviceManager::MBDeviceManager():IIOModuleManager(),
                            log(Logger::getLogger(SUBSYSTEM_ID_MODBUS_MASTER)),
                            m_controller(NULL),
                            mp_allDevices(NULL),
                            m_deviceNum(0)
{
    DBG_CTOR();

    m_controller = new MBMController(this);
}

MBDeviceManager::~MBDeviceManager()
{
    DBG_DTOR();

    stopAllModules();
}

void MBDeviceManager::start()
{
    if(m_deviceNum > 0)
    {
        if(m_controller != NULL && m_controller->isRunning() == LU_FALSE)
            m_controller->start();
        else
            log.warn("Cannot start MBDevice Manager which is already started!");
    }
    else
    {
        DBG_WARN("No Modbus device configured. Module NOT started.");
    }
}


void MBDeviceManager::init(IMBDeviceFactory& factory)
{
    factory.createAllMBDevices(&mp_allDevices, &m_deviceNum);


    /* Print device configuration*/
    DBG_INFO("MBDeviceManager::init deviceNum:%i",m_deviceNum);
    mbm::MBDevice* device = NULL;
    mbm::DeviceConf* conf = NULL;
    for (int i = 0; i < m_deviceNum; ++i)
    {
        device = getMBDevice((MODULE_ID)i);
        assert( device!= NULL);
        conf = device->getConfig();
        DBG_INFO("\n[DeviceConfig] %s\n", conf->toString().c_str()
                        );
    }
}


void MBDeviceManager::handleEvent(ChannelEvent& event)
{
    MBDevice* device = NULL;

    device = getMBDevice(static_cast<MODULE_ID>(event.channel.device.deviceId));
    if(device != NULL)
    {
        device->handleEvent(event);
    }
    else
    {
        DSICARD_EVENT_NO_DEVICE(event);
        DBG_INFO("Cannot hand digital event, device not found:%i",event.channel.device.deviceId);
    }
}


IIOModule *MBDeviceManager::createModule(MODULE module,
                MODULE_ID moduleID,
                COMM_INTERFACE iFace)
{
    LU_UNUSED(module);
    LU_UNUSED(moduleID);
    LU_UNUSED(iFace);
    DBG_ERR("Unsupported operation:%s",__PRETTY_FUNCTION__);
    return NULL;
}

MBDevice* MBDeviceManager::getMBDevice(MODULE_ID moduleID)
{
    return (MBDevice*)getModule(MODULE_MBDEVICE,moduleID);
}


IIOModule* MBDeviceManager::getModule(MODULE module, MODULE_ID moduleID)
{
    LU_UNUSED(module);
    if(moduleID >=0 && moduleID < m_deviceNum)
          return dynamic_cast<IIOModule*>(mp_allDevices[moduleID]);
      else
          return NULL;

}


std::vector<IIOModule*> MBDeviceManager::getAllModules()
{
    std::vector<IIOModule*> ret;
    IIOModule* modulePtr;

    /* Scan the local database */
    for(lu_uint32_t moduleID = 0; moduleID < m_deviceNum; ++moduleID)
    {
        /* If the module is valid add to the output list */
        modulePtr = mp_allDevices[moduleID];
        if(modulePtr != NULL)
        {
            ret.push_back(modulePtr);
        }
    }
    return ret;
}


IChannel *MBDeviceManager::getChannel( MODULE module,
                          MODULE_ID moduleID,
                          CHANNEL_TYPE chType,
                          lu_uint32_t chNumber
                        )
{
    IIOModule* p_module = getModule(module,moduleID);

    MBDevice* device = dynamic_cast<MBDevice*>(p_module);
    assert (device != NULL);
    device->printID();
    device->printConfig();

    if (p_module != NULL)
    {
        return p_module->getChannel(chType, chNumber);

    }

    return NULL;
}

void MBDeviceManager::getDeviceNum(lu_uint8_t* tcpDevNum, lu_uint8_t* serialDevNum)
{
   lu_uint8_t devicesNum;
   devicesNum  = getTotalDeviceNum();

   *tcpDevNum = 0;
   *serialDevNum = 0;
   for (int i = 0; i < devicesNum; ++i)
    {
       DeviceRef ref;
       if(mp_allDevices[i]->deviceRef(&ref) == IOM_ERROR_NONE)
       {
           if (ref.deviceType == mbm::DEVICE_TYPE_SERIAL)
           {
               *serialDevNum += 1;
           }
           else if (ref.deviceType == mbm::DEVICE_TYPE_TCP)
           {
               *tcpDevNum += 1;
           }
       }
    }

}

IOM_ERROR MBDeviceManager::stopAllModules()
{
    if(m_controller != NULL)
    {
        DBG_INFO("Stopping MBMController...");
        delete m_controller;
        m_controller = NULL;
        DBG_INFO("[SUCCESS] Stopped MBMController");
    }

    return IOM_ERROR_NONE;
}
/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
}
/*
 *********************** End of file ******************************************
 */
