/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MBChannel.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13 May 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MBCHANNEL_H_
#define MBCHANNEL_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <zmq.hpp>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IChannel.h"
#include "MBMConfigProto.h"
#include "Logger.h"
#include "TimeManager.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */


namespace mbm{

/**
 * \brief Generic Modbus Channel implementation
 *
 * This class implements the basics of the channels related to Modbus devices.
 */
class MBChannel : public IChannel
{
public:
    MBChannel(CHANNEL_TYPE chtype, lu_uint32_t chid);
    virtual ~MBChannel();

    virtual void setConfig(ChannelConf& conf);

    virtual void applyConfig(zmq::socket_t& socket);


    /**
    * Handling channel event.
    */
   void handleEvent(ChannelEvent& event);


    /************************************/
    /*	IChannel Interface              */
    /************************************/

    virtual IOM_ERROR read(ValueStr& valuePtr);

    virtual IOM_ERROR write(ValueStr& valuePtr);

    virtual IOM_ERROR select(lu_uint8_t selectTimeout, lu_bool_t local);

    /*
     * Note: This method is called by Digital Output CLogic.
     */
    IOM_ERROR operate(lu_bool_t value, lu_bool_t local, lu_uint8_t delay);



private:
    void releaseConfig();

    Logger& log;

    lu_int32_t m_value;

    TimeManager::TimeStr m_timeStamp;

    ChannelConf* mp_conf;

    zmq::socket_t* mp_socket;
};

}
#endif /* MBCHANNEL_H_ */

/*
 *********************** End of file ******************************************
 */
