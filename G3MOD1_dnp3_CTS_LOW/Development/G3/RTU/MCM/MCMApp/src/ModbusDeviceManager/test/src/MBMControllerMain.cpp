/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MBMControllerMain.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17 Jun 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <pthread.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "MBDeviceManager.h"
#include "MBMController.h"
#include "DummyMBConfigRepo.h"
#include "MBDeviceFactory.h"
#include "mbm_helpers.h"
#include "Thread.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */





/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
//extern void* runDatabase(void*);
//extern void* MBMSimulatorRoutine(void*);



/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

using namespace mbm;

int main(int argc, char **argv)
{
    LU_UNUSED(argc);
    LU_UNUSED(argv);

    SIG_BlockAllExceptTerm();

    MBDeviceManager deviceMgr;
    DummyMBConfigRepo repo;

    MBDeviceFactory factory(repo);
    deviceMgr.init(factory);

    MBMController ctl(&deviceMgr);
    ctl.start();
    ctl.join();
}



/*
 *********************** End of file ******************************************
 */
