/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:IModbusDeviceFactory.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11 Jun 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef MBM_MBDEVICEFACTORY_H_
#define MBM_MBDEVICEFACTORY_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IModbusConfigRepository.h"
#include "IMBDeviceFactory.h"
#include "MBDevice.h"
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
namespace mbm{

class MBDeviceFactory:public IMBDeviceFactory{
public:
    MBDeviceFactory(IModbusConfigRepository& configRepo);

    virtual ~MBDeviceFactory();

    virtual void createAllMBDevices(MBDevice*** devices, lu_uint8_t* num);

private:
    void createChannels(MBDevice& device,
                    const CHANNEL_TYPE chtype);

    IModbusConfigRepository& m_configRepo;
};
}
#endif /* MBM_MBDEVICEFACTORY_H_ */

/*
 *********************** End of file ******************************************
 */
