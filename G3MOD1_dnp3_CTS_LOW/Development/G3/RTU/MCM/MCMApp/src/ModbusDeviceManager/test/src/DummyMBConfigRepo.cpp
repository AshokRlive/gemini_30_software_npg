/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:XMLModbusDeviceFactory.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19 May 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "DummyMBConfigRepo.h"
#include "mbm_helpers.h"
#include "LinIoTarg/liniodefs.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
const static lu_uint16_t DEVICE_NUM  = 4;
const static lu_uint16_t CHS_NUM  = 10;


/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:DefaultMBConfig.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   20 Jun 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

using namespace mbm;


 /************************************/
 /*  Predefined Device Configuration */
 /************************************/
 const lu_uint8_t DEF_DEV_NUM = 4;

 const lu_uint16_t DEF_CH_NUM = 10;

 const  lu_uint8_t slaveType[DEF_DEV_NUM] =
 {
     DEVICE_TYPE_SERIAL,    // device 0
     DEVICE_TYPE_SERIAL,       // device 1
     DEVICE_TYPE_SERIAL,    // device 2
     DEVICE_TYPE_SERIAL,    // device 3
 };

 const  lu_uint16_t slaveAddresses[DEF_DEV_NUM] =
 {
     1,    // device 0
     3,    // device 1
     2,    // device 2
     4,    // device 3
 };

 const  char* chlName[DEF_DEV_NUM] =
 {
     "channel-TH",    // device 0
     "channel1",    // device 1
     "channel-CR",    // device 2
     "channel3",    // device 3
 };

 /*Only for serial*/
 const  char* portName[DEF_DEV_NUM] =
 {
    "/dev/ttymxc3",  // device 0
    "/dev/ttymxc3",  // device 1
    "/dev/ttymxc3",  // device 2
    "/dev/ttymxc3",  // device 3
 };

 const  lu_uint8_t bModbusRTU[DEF_DEV_NUM] =
 {
      1, // device 0
      1, // device 1
      1, // device 2
      1, // device 3
 };
 const  LIN232_BAUD_RATE baudRate[DEF_DEV_NUM] =
 {
     LIN232_BAUD_19200, // device 0
     LIN232_BAUD_19200, // device 1
     LIN232_BAUD_19200, // device 2
     LIN232_BAUD_19200, // device 3
 };
 const  LIN232_DATA_BITS numDataBits[DEF_DEV_NUM] =
 {
     LIN232_DATA_BITS_8, // device 0
     LIN232_DATA_BITS_8, // device 1
     LIN232_DATA_BITS_8, // device 2
     LIN232_DATA_BITS_8, // device 3
 };
 const  LIN232_STOP_BITS numStopBits[DEF_DEV_NUM] =
 {
     LIN232_STOP_BITS_1, // device 0
     LIN232_STOP_BITS_1, // device 1
     LIN232_STOP_BITS_1, // device 2
     LIN232_STOP_BITS_1, // device 3
 };

 const  LIN232_PARITY parity[DEF_DEV_NUM] =
 {
   LIN232_PARITY_NONE,  // device 0
   LIN232_PARITY_NONE,  // device 1
   LIN232_PARITY_NONE,  // device 2
   LIN232_PARITY_NONE,  // device 3
 };


 /*Only for tcp*/
 const char* ipAddress[DEF_DEV_NUM] =
 {
     "10.11.1.182     ",    // device 0
     "10.11.35.250       ",    // device 1
     "10.11.35.250       ",    // device 2
     "10.11.11.126      ",    // device 3
 };

 /*Only for tcp*/
 TMWTYPES_USHORT tcpPort[DEF_DEV_NUM] =
 {
     505,
     504,
     502,
     1003,
 };



 /*******************************************/
 /* Predefined Device Channel Configuration */
 /*******************************************/

 const ChannelConf AI_CHANNELS[DEF_DEV_NUM][DEF_CH_NUM] =
 {
     /*device 0*/
     {
        /*deviceId,   deviceType, channelId, channelType,            reg,    datatype,              regType,                pollingRate*/
         {{{0,         0},          0,       MBDEVICE_CHANNEL_AI},   1005,  REG_DATA_TYPE_FLOAT32, REG_TYPE_HOLDING_REGISTERS,  0, 0,    0,0,{' '} }, // Channel 0
         {{{0,         0},          1,       MBDEVICE_CHANNEL_AI},   1007,  REG_DATA_TYPE_FLOAT32, REG_TYPE_HOLDING_REGISTERS,  0, 0,    0,0,{' '} }, // Channel 1
         {{{0,         0},          0,       MBDEVICE_CHANNEL_AI},   1009,  REG_DATA_TYPE_FLOAT32, REG_TYPE_HOLDING_REGISTERS,  0, 0,    0,0,{' '} }, // Channel 2
         {{{0,         0},          3,       MBDEVICE_CHANNEL_AI},   1011,  REG_DATA_TYPE_FLOAT32, REG_TYPE_HOLDING_REGISTERS,  0, 0,    0,0,{' '} }, // Channel 3
         {{{0,         0},          4,       MBDEVICE_CHANNEL_AI},   1059,  REG_DATA_TYPE_FLOAT32, REG_TYPE_HOLDING_REGISTERS,  0, 0,    0,0,{' '} }, // Channel 4
         {{{0,         0},          5,       MBDEVICE_CHANNEL_AI},   1033,  REG_DATA_TYPE_FLOAT32, REG_TYPE_HOLDING_REGISTERS,  0, 0,    0,0,{' '} }, // Channel 5
         {{{0,         0},          6,       MBDEVICE_CHANNEL_AI},   1091,  REG_DATA_TYPE_FLOAT32, REG_TYPE_HOLDING_REGISTERS,  0, 0,    0,0,{' '} }, // Channel 6
         {{{0,         0},          7,       MBDEVICE_CHANNEL_AI},   1083,  REG_DATA_TYPE_FLOAT32, REG_TYPE_HOLDING_REGISTERS,  0, 0,    0,0,{' '} }, // Channel 7
         {{{0,         0},          8,       MBDEVICE_CHANNEL_AI},   1065,  REG_DATA_TYPE_FLOAT32, REG_TYPE_HOLDING_REGISTERS,  0, 0,    0,0,{' '} }, // Channel 8
         {{{0,         0},          9,       MBDEVICE_CHANNEL_AI},   1061,  REG_DATA_TYPE_FLOAT32, REG_TYPE_HOLDING_REGISTERS,  0, 0,    0,0,{' '} }, // Channel 9
     },

     /*device 1*/
     {
        /*deviceId,   deviceType,  channelId, channelType,            reg,    datatype,              regType,                pollingRate*/
         {{{1,         0},          0,        MBDEVICE_CHANNEL_AI},   30,   REG_DATA_TYPE_FLOAT32, REG_TYPE_HOLDING_REGISTERS,  0, 0,    0,0,{' '} }, // Channel 0
         {{{1,         0},          1,        MBDEVICE_CHANNEL_AI},   66,   REG_DATA_TYPE_FLOAT32, REG_TYPE_HOLDING_REGISTERS,  0, 0,    0,0,{' '} }, // Channel 1
         {{{1,         0},          2,        MBDEVICE_CHANNEL_DI},   32,   REG_DATA_TYPE_FLOAT16, REG_TYPE_HOLDING_REGISTERS,  0, 0,    0,0,{' '} }, // Channel 2
         {{{1,         0},          3,        MBDEVICE_CHANNEL_AI},   38,   REG_DATA_TYPE_FLOAT32, REG_TYPE_HOLDING_REGISTERS,  0, 0,    0,0,{' '} }, // Channel 3
         {{{1,         0},          4,        MBDEVICE_CHANNEL_AI},   0,    REG_DATA_TYPE_FLOAT32, REG_TYPE_HOLDING_REGISTERS,  0, 0,    0,0,{' '} }, // Channel 4
         {{{1,         0},          5,        MBDEVICE_CHANNEL_AI},   2,    REG_DATA_TYPE_FLOAT32, REG_TYPE_HOLDING_REGISTERS,  0, 0,    0,0,{' '} }, // Channel 5
         {{{1,         0},          6,        MBDEVICE_CHANNEL_AI},   48,   REG_DATA_TYPE_FLOAT32, REG_TYPE_HOLDING_REGISTERS,  0, 0,    0,0,{' '} }, // Channel 6
         {{{1,         0},          7,        MBDEVICE_CHANNEL_AI},   54,   REG_DATA_TYPE_FLOAT32, REG_TYPE_HOLDING_REGISTERS,  0, 0,    0,0,{' '} }, // Channel 7
         {{{1,         0},          8,        MBDEVICE_CHANNEL_AI},   4,    REG_DATA_TYPE_FLOAT32, REG_TYPE_HOLDING_REGISTERS,  0, 0,    0,0,{' '} }, // Channel 8
         {{{1,         0},          9,        MBDEVICE_CHANNEL_AI},   10,   REG_DATA_TYPE_FLOAT32, REG_TYPE_HOLDING_REGISTERS,  0, 0,    0,0,{' '} }, // Channel 9
     },

     /*device 2*/
     {
        /*deviceId,   deviceType,  channelId, channelType,            reg,    datatype,              regType,                pollingRate*/
         {{{2,         1},          0,        MBDEVICE_CHANNEL_AI},   18,     REG_DATA_TYPE_FLOAT32, REG_TYPE_HOLDING_REGISTERS,  5000, 0,    0,0,{' '} }, // Channel 0
         {{{2,         1},          1,        MBDEVICE_CHANNEL_AI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 1
         {{{2,         1},          2,        MBDEVICE_CHANNEL_AI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 2
         {{{2,         1},          3,        MBDEVICE_CHANNEL_AI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 3
         {{{2,         1},          4,        MBDEVICE_CHANNEL_AI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 4
         {{{2,         1},          5,        MBDEVICE_CHANNEL_AI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 5
         {{{2,         1},          6,        MBDEVICE_CHANNEL_AI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 6
         {{{2,         1},          7,        MBDEVICE_CHANNEL_AI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 7
         {{{2,         1},          8,        MBDEVICE_CHANNEL_AI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 8
         {{{2,         1},          9,        MBDEVICE_CHANNEL_AI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 9
     },

     /*device 3*/
     {
        /*deviceId,   deviceType,  channelId, channelType,            reg,    datatype,              regType,                pollingRate*/
         {{{3,         0},          0,        MBDEVICE_CHANNEL_AI},   9000,   REG_DATA_TYPE_FLOAT32, REG_TYPE_HOLDING_REGISTERS,  5000, 0,    0,0,{' '} }, // Channel 0
         {{{3,         0},          1,        MBDEVICE_CHANNEL_AI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 1
         {{{3,         0},          2,        MBDEVICE_CHANNEL_AI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 2
         {{{3,         0},          3,        MBDEVICE_CHANNEL_AI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 3
         {{{3,         0},          4,        MBDEVICE_CHANNEL_AI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 4
         {{{3,         0},          5,        MBDEVICE_CHANNEL_AI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 5
         {{{3,         0},          6,        MBDEVICE_CHANNEL_AI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 6
         {{{3,         0},          7,        MBDEVICE_CHANNEL_AI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 7
         {{{3,         0},          8,        MBDEVICE_CHANNEL_AI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 8
         {{{3,         0},          9,        MBDEVICE_CHANNEL_AI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 9
     },
 };

 const ChannelConf DI_CHANNELS[DEF_DEV_NUM][DEF_CH_NUM] =
 {
     /*device 0*/
     {
        /*deviceId,   deviceType,  channelId, channelType,            reg,    datatype,              regType,                pollingRate*/
         {{{0,         1},          0,        MBDEVICE_CHANNEL_DI},   46,     REG_DATA_TYPE_FLOAT32, REG_TYPE_HOLDING_REGISTERS,  5000, 0,    0,0,{' '} }, // Channel 0
         {{{0,         1},          1,        MBDEVICE_CHANNEL_DI},   48,     REG_DATA_TYPE_FLOAT32, REG_TYPE_HOLDING_REGISTERS,  5000, 0,    0,0,{' '} }, // Channel 1
         {{{0,         0},          2,        MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 2
         {{{0,         0},          3,        MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 3
         {{{0,         0},          4,        MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 4
         {{{0,         0},          5,        MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 5
         {{{0,         0},          6,        MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 6
         {{{0,         0},          7,        MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 7
         {{{0,         0},          8,        MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 8
         {{{0,         0},          9,        MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 9
     },

     /*device 1*/
     {
        /*deviceId,   deviceType,  channelId, channelType,            reg,    datatype,              regType,                pollingRate*/
         {{{1,         0},          0,        MBDEVICE_CHANNEL_DI},   48,     REG_DATA_TYPE_FLOAT32, REG_TYPE_HOLDING_REGISTERS,  5000, 0,    0,0,{' '} }, // Channel 0
         {{{1,         0},          1,        MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 1
         {{{1,         0},          2,        MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 2
         {{{1,         0},          3,        MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 3
         {{{1,         0},          4,        MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 4
         {{{1,         0},          5,        MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 5
         {{{1,         0},          6,        MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 6
         {{{1,         0},          7,        MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 7
         {{{1,         0},          8,        MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 8
         {{{1,         0},          9,        MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 9
     },

     /*device 2*/
     {
        /*deviceId,   deviceType,  channelId, channelType,           reg,    datatype,              regType,                pollingRate*/
         {{{2,         1},          0,       MBDEVICE_CHANNEL_DI},   20,     REG_DATA_TYPE_FLOAT32, REG_TYPE_HOLDING_REGISTERS,  10000, 0,    0,0,{' '} }, // Channel 0
         {{{2,         1},          1,       MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,         500, 0,    0,0,{' '} }, // Channel 1
         {{{2,         1},          2,       MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,         500, 0,    0,0,{' '} }, // Channel 2
         {{{2,         1},          3,       MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,         500, 0,    0,0,{' '} }, // Channel 3
         {{{2,         1},          4,       MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,         500, 0,    0,0,{' '} }, // Channel 4
         {{{2,         1},          5,       MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,         500, 0,    0,0,{' '} }, // Channel 5
         {{{2,         1},          6,       MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,         500, 0,    0,0,{' '} }, // Channel 6
         {{{2,         1},          7,       MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,         500, 0,    0,0,{' '} }, // Channel 7
         {{{2,         1},          8,       MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,         500, 0,    0,0,{' '} }, // Channel 8
         {{{2,         1},          9,       MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,         500, 0,    0,0,{' '} }, // Channel 9
     },

     /*device 3*/
     {
        /*deviceId,   deviceType,  channelId, channelType,            reg,    datatype,              regType,                pollingRate*/
         {{{3,         0},          0,        MBDEVICE_CHANNEL_DI},   9002,   REG_DATA_TYPE_FLOAT32, REG_TYPE_HOLDING_REGISTERS,  5000, 0,    0,0,{' '} }, // Channel 0
         {{{3,         0},          1,        MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 1
         {{{3,         0},          2,        MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 2
         {{{3,         0},          3,        MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 3
         {{{3,         0},          4,        MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 4
         {{{3,         0},          5,        MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 5
         {{{3,         0},          6,        MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 6
         {{{3,         0},          7,        MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 7
         {{{3,         0},          8,        MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 8
         {{{3,         0},          9,        MBDEVICE_CHANNEL_DI},   10003,  REG_DATA_TYPE_FLOAT32, REG_TYPE_INPUT_STATUS,        500, 0,    0,0,{' '} }, // Channel 9
     },
 };



/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
namespace mbm{

DummyMBConfigRepo::DummyMBConfigRepo():IModbusConfigRepository()
{
    DBG_CTOR();
}

DummyMBConfigRepo::~DummyMBConfigRepo()
{
    DBG_DTOR();
}


lu_uint8_t DummyMBConfigRepo::getDeviceNum()
{
    return DEVICE_NUM;
}



 void DummyMBConfigRepo::getDeviceConf(const lu_uint8_t devIdx, DeviceConf* devConf)
 {
     mbm::MMBSessionConf* sesnconf;
     mbm::MBLinkConf* mblinkconf;
     mbm::Lin232Conf* serialconf;
     mbm::LinTCPConf* tcpconf;

     /* Get device index*/
     assert (devIdx < DEVICE_NUM);

     /* Configure MMB device reference*/
     devConf->ref.deviceId= devIdx;
     devConf->ref.deviceType = slaveType[devIdx];

     /* Configure Device channel numbers  */
     devConf->aichlNum = this->getChannelNum(devIdx, CHANNEL_TYPE_AINPUT);
     devConf->dichlNum = this->getChannelNum(devIdx, CHANNEL_TYPE_DINPUT);


     if(devConf->ref.deviceType == mbm::DEVICE_TYPE_SERIAL)
     {
         sesnconf = &(devConf->conf.serialDevConf.sesnconf);
         mblinkconf= &(devConf->conf.serialDevConf.mblinkconf);
         serialconf = &(devConf->conf.serialDevConf.serialconf);
         tcpconf = NULL;
     }
     else
     {
         sesnconf =&(devConf->conf.tcpDevConf.sesnconf);
         mblinkconf=&(devConf->conf.tcpDevConf.mblinkconf);
         tcpconf =&(devConf->conf.tcpDevConf.tcpconf);
         serialconf = NULL;
     }


     /*Configure MMB session*/
     sesnconf->slaveAddress              = slaveAddresses[devIdx];
     sesnconf->defaultResponseTimeout    = 1000;

     /*Configure MMB channel*/
     mblinkconf->maxQueueSize             = 1000;
     mblinkconf->rxFrameTimeout           = 1000;
     mblinkconf->channelResponseTimeout   = 1000;
     mblinkconf->type= 2;

     /*Configure MMB tcp Link*/
     if(tcpconf != NULL)
     {
         tcpconf->disconnectOnNewSyn = 0;
         tcpconf->ipConnectTimeout   = 1000;
         tcpconf->ipPort             = tcpPort[devIdx];
         tcpconf->mode               = LINTCP_MODE_CLIENT;
         tcpconf->role               = LINTCP_ROLE_OUTSTATION;

         memcpy(tcpconf->ipAddress, ipAddress[devIdx] ,20);
         memcpy(tcpconf->chnlName, chlName[devIdx] ,20);

     }

     /*Configure MMB serial Link*/
     if(serialconf != NULL)
     {
         serialconf->bModbusRTU  = bModbusRTU[devIdx];
         serialconf->baudRate    = baudRate[devIdx];
         serialconf->numDataBits = numDataBits[devIdx];
         serialconf->numStopBits = numStopBits[devIdx];
         serialconf->parity      = parity[devIdx];
         serialconf->portMode     = LIN232_MODE_NONE;

         memcpy(serialconf->chnlName, chlName[devIdx],20);
         memcpy(serialconf->portName, portName[devIdx],20);
     }
 }

 lu_int32_t DummyMBConfigRepo::getChannelNum(const lu_uint8_t deviceIdx, CHANNEL_TYPE type)
 {
     LU_UNUSED(deviceIdx);
     LU_UNUSED(type);

     assert(deviceIdx < DEVICE_NUM);


     if(type == CHANNEL_TYPE_AINPUT)
         return CHS_NUM;
     else if(type == CHANNEL_TYPE_DINPUT)
         return 1;
     else
         return 0;
 }

 void DummyMBConfigRepo::getChannelConf(const ChannelRef& ch, ChannelConf* conf )
 {
     assert(ch.channelId < CHS_NUM);
     assert(ch.device.deviceId < DEVICE_NUM);

     if(ch.channelType  == MBDEVICE_CHANNEL_AI)
     {
         memcpy(conf, &(AI_CHANNELS[ch.device.deviceId][ch.channelId]),sizeof(ChannelConf));
     }

//     if(ch.channelType  == MBDEVICE_CHANNEL_DI)
//     {
//         memcpy(conf, &(DI_CHANNELS[ch.device.deviceId][ch.channelId]),sizeof(ChannelConf));
//     }

     conf->channel = ch;

 }
/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

}
/*
 *********************** End of file ******************************************
 */
