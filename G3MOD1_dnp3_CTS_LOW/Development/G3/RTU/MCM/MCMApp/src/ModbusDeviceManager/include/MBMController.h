/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MBMasterContorller.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   8 May 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MBMASTERCONTORLLER_H_
#define MBMASTERCONTORLLER_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <zmq.hpp>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "Thread.h"
#include "Logger.h"
#include "IMBMController.h"
#include "MBMConfigProto.h"
#include "MBDeviceManager.h"
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
namespace mbm{

class MBMController:public IMBMController
{
public:
    MBMController(MBDeviceManager* deviceMgr);

    virtual ~MBMController();

    void pollChannel();

    /************************************/
    /*		Thread Implementation       */
    /************************************/

    virtual THREAD_ERR start();

    virtual THREAD_ERR stop();

protected:
    virtual void threadBody();

    void sendConfig();

    /**
     * Init sockets for communicating with MBM server.
     */
    void initSockets();

    /**
     * Launch Modbs virtual module.
     */
    void launchMBModule();

    /**
     * Closes the sockets for communicating with MBM server.
     */
    void closeSockets();


    /**
     * Send a request message to MBM Server and wait reply.
     */
    void sendMessage(MSG_TYPE type, MSG_ID msgId);

    /**
     * Receive events from MBM server.
     */
    void receiveEvents(MsgHeader header, zmq::message_t& msg);

    /**
     * Handling channel event.
     */
    void handleEvent(zmq::message_t& event);


private:
    Logger& log;
    zmq::context_t* m_context;
    zmq::socket_t*  m_socket_req;
    zmq::socket_t*  m_socket_sub;

    pthread_t       m_server_thread;
    MBDeviceManager* mp_deviceMgr;


};

}/*namespace mbm*/


#endif /* MBMASTERCONTORLLER_H_ */

/*
 *********************** End of file ******************************************
 */
