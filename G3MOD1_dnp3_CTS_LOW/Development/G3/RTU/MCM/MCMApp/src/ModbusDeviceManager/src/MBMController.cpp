/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MBMContorller.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   8 May 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
//#include <iostream>
#include <sstream>
#include <unistd.h> // sleep
#include <exception>
#include <pthread.h>
#include <ZmqContext.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "mbm_helpers.h"
#include "MBMController.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

#define SUB_ALL_EVENT 1
#define SUB_ANALOG_EVENT 0
#define SUB_DIGITAL_EVENT 0

/*
 * Run ModBus virtual module simulator.
 */
#define SIMULATE_MBM 0


/** Delete an object and set pointer to NULL */
#define DELETE_POINTER(objName, objPtr)\
            if(objPtr != NULL)\
                            {\
                DBG_INFO("C: [Dtor]Delete %s",objName);\
                delete objPtr;\
                objPtr = NULL;\
                            }\

#define CHECK_INTERRUPT() \
        if(isInterrupting() == LU_TRUE) \
        {\
            closeSockets();\
            DBG_INFO("Thread interrupted");\
            return;\
        }\


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/**
 * ModBus virtual module thread entry point.
 *
 * \param ctx: ZMQ context.
 */
extern void* MBModuleEntryPoint(void* ctx);

 /* Synchronisation condition shared between Modbus Module and Main App.*/
extern Semaphore semMBMInit;


#if SIMULATE_MBM
extern void* MBMSimulatorRoutine(void*);
#endif

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


namespace mbm{

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
MBMController::MBMController(MBDeviceManager* deviceMgr):
                log(Logger::getLogger(SUBSYSTEM_ID_MODBUS_MASTER)),
                m_context(NULL),
                m_socket_req(NULL),
                m_socket_sub(NULL),
                m_server_thread(-1),
                mp_deviceMgr(deviceMgr)
{
    DBG_CTOR();
    m_context = ZmqContext::getInstance();
   //m_context = new zmq::context_t(1);// Create context with 1 IO thread
    DBG_INFO("ZMQ Context created: %p for Modbus", (void*)m_context);
}

MBMController::~MBMController()
{
    DBG_DTOR();
    stop();
}


THREAD_ERR MBMController::start()
{
    DBG_INFO("C: Start MBMController");

/* removed by sid
#if MB_MODULE_RUNNING_IN_PROC == 0  // Running in thread
    launchMBModule();
#endif
*/

    launchMBModule();

    return Thread::start();
}

THREAD_ERR MBMController::stop()
{

    if(Thread::isRunning())
    {
        
        DBG_INFO("C: Stopping MBMController thread");
        Thread::stop();

        /*Signal MBMController thread to exit blocking*/
        semMBMInit.post();

        Thread::join();
        DBG_INFO("C: Joined MBMController thread");
    }

    /*commented out since ZmqContext(singleton) is responsible for object desctruction*/
//    if(m_context != NULL)
//    {
//        DELETE_POINTER("context",m_context);
//        DBG_INFO("[SUCCESS] context deleted");
//    }
    return THREAD_ERR_NONE;
}

void MBMController::receiveEvents(MsgHeader header, zmq::message_t& msg)
{
    if (m_socket_sub->recv(&msg, ZMQ_NOBLOCK))
    {
        if (msg.size() == sizeof(MsgHeader))
        {
            memcpy(&header, msg.data(), msg.size());
            DBG_INFO("==> Received: %s ",
                            commandName((MSG_TYPE )header.type, (MSG_ID )header.id));

            if (header.type == MSG_TYPE_EVENT)
            {
                /* Handle MB event message only.*/
                switch (header.id)
                {
                    case MSG_ID_EVENT_ANALOGUE:
                    case MSG_ID_EVENT_DIGITAL:
                        while (msg.more())
                        {
                            m_socket_sub->recv(&msg);
                            handleEvent(msg);
                        }
                    break;

                    default:
                        DBG_WARN("Unrecognised event id: %i", header.id);
                }
            }
            else
            {
                DBG_WARN("Unrecognised event type: %i", header.type);
            }
        }
        else
        {
            DBG_ERR("C: Invalid event header. Expected:%u Actual %u",
                            sizeof(MsgHeader), msg.size());
        }

        // Discard unprocessed ZMQ frames;
        discardRcvMsg(*m_socket_sub, msg);
    }
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void MBMController::threadBody()
{
    DBG_INFO("C: Enter MBMController thread body");

#if MB_MODULE_RUNNING_MODE == MB_MODULE_IN_THREAD // Running in thread
    /*If MB module is running in thread, we have to wait its socket to be init
     * before connecting to it. Otherwise, an exception of "Connection Refused"
     * would be thrown in the next InitSocket().
     */
    DBG_INFO("Waiting Modbus Module to be launched...");
    semMBMInit.wait();
    DBG_INFO("Modbus Module is now launched!");
#endif

    try{
    initSockets();
    }catch (zmq::error_t& e) {
        DBG_ERR("Cannot open sockets: %s",e.what());
        closeSockets();
        return;
    }

    // Ping server
    sendMessage(MSG_TYPE_CMD, MSG_ID_CMD_PING_C);
    CHECK_INTERRUPT();

    sendConfig();
    CHECK_INTERRUPT();

    // Send start command
    sendMessage(MSG_TYPE_CMD,MSG_ID_CMD_START_C);
    CHECK_INTERRUPT();

    DBG_INFO("\n ------->Start receiving MB module events<--------");
    MsgHeader header;
    zmq::message_t msg;
    while(1)
    {
        if(isInterrupting() == LU_TRUE)
        {
             DBG_INFO("C: Interrupt received, killing MBMController…");
             break;
        }

        try
        {
            receiveEvents(header, msg);
            usleep(5000);
        } catch(zmq::error_t& e)
        {
            DBG_ERR("receiveEvents failed: %s",e.what());
            break;
        }
    }

    sendMessage(MSG_TYPE_CMD,MSG_ID_CMD_STOP_C);
    closeSockets();
    DBG_INFO("C: Exit MBMController thread body!");
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


void MBMController::initSockets()
{
    // Create REQ socket to connect to MBMaster
    m_socket_req = new zmq::socket_t(*m_context, ZMQ_REQ);

    //  Configure socket to not wait at close time
    lu_int32_t linger = 0;
    lu_uint32_t timout = 30000; //ms
    m_socket_req->setsockopt (ZMQ_LINGER, &linger, sizeof (linger));
    m_socket_req->setsockopt (ZMQ_RCVTIMEO, &timout, sizeof (timout));

    // Create SUB socket to subscribe MB channel events
    m_socket_sub = new zmq::socket_t(*m_context, ZMQ_SUB);
    m_socket_sub->setsockopt (ZMQ_LINGER, &linger, sizeof (linger));

    m_socket_req->connect (MB_HOST_ADDRESS_REP);
    DBG_INFO("Connect to REP socket:%s",MB_HOST_ADDRESS_REP);
    m_socket_sub->connect (MB_HOST_ADDRESS_SUB);
    DBG_INFO("Connect to SUB socket:%s",MB_HOST_ADDRESS_SUB);


#if SUB_ALL_EVENT
    m_socket_sub->setsockopt(ZMQ_SUBSCRIBE, "", 0);//Subscribe to all events.
#else

    MsgHeader header;
    header.type = MSG_TYPE_EVENT;

#if SUB_ANALOG_EVENT
    header.id = MSG_ID_EVENT_ANALOGUE;
    m_socket_sub->setsockopt(ZMQ_SUBSCRIBE, &header, sizeof(header));//Subscribe to all events.
#endif

#if SUB_DIGITAL_EVENT
    header.id = MSG_ID_EVENT_DIGITAL;
    m_socket_sub->setsockopt(ZMQ_SUBSCRIBE, &header, sizeof(header));//Subscribe to all events.
#endif

#endif


    DBG_INFO("C: Connected to MB Host: %s", MB_HOST_ADDRESS_REP);
}


void MBMController::closeSockets()
{
    DELETE_POINTER("socket_req",m_socket_req);
    DELETE_POINTER("subscriber",m_socket_sub);
}

void MBMController::sendConfig()
{
    lu_uint8_t devicesNum;
    mbm::MBDevice* device;

    DBG_INFO("C: Sending configuration...");

    /* Send device number*/
    lu_uint8_t tcpDevNum = 0;
    lu_uint8_t serialDevNum = 0;

    mp_deviceMgr->getDeviceNum(&tcpDevNum, &serialDevNum);
    try
    {
    MBDevice::sendDeviceNumConfig(*m_socket_req,tcpDevNum,serialDevNum);

    devicesNum = mp_deviceMgr->getTotalDeviceNum();
    for (lu_uint8_t i = 0; i < devicesNum; ++i)
    {
        device = mp_deviceMgr->getMBDevice(i);
        try{
            device->applyConfig(*m_socket_req);
        }catch (...) {
            DBG_ERR("Failed to send device cfg. Exception raised.");
            break;
        }
    }
        DBG_INFO("C:[SUCCESS] Sending configuration");
    }catch (zmq::error_t& e) {
        DBG_ERR("C: [FAIL] Sending configuration: %s",e.what());
    }
}

void MBMController::sendMessage(MSG_TYPE msgType, MSG_ID msgId)
{
    MsgHeader msgheader;

    // Send request
    msgheader.type = MSG_TYPE_CMD;
    msgheader.id = msgId;

    try
    {
        m_socket_req->send(&msgheader,sizeof(msgheader),0);
        DBG_INFO("<=== Send request: %s",commandName(msgType,msgId));
        LU_UNUSED(msgType);
        // Wait reply
        zmq::message_t reply;
        do{
            if(m_socket_req->recv(&reply))
                DBG_INFO("===> Reply %s",(char*)reply.data());
            else
            {
                DBG_ERR("===> No Reply");
                break;
            }

        }while(reply.more());

    } catch(zmq::error_t& e)
    {
        DBG_ERR("C: Sending request failed: %s",e.what());
    }

}



void MBMController::handleEvent(zmq::message_t& event)
{
    if(event.size() != sizeof(ChannelEvent))
    {
        DBG_ERR("Invalid event size: %i, expected:%i",
                        event.size(),
                        sizeof(ChannelEvent));
        return;
    }

    ChannelEvent* p_event = (ChannelEvent*)(event.data());
    mp_deviceMgr->handleEvent(*p_event);
}


void MBMController::pollChannel()
{
    //TODO to be implemented.
}

void MBMController::launchMBModule()
{
#if SIMULATE_MBM
    int ret = pthread_create(&m_server_thread, NULL, &MBMSimulatorRoutine, NULL);
#else
    #if (MB_MODULE_RUNNING_MODE == MB_MODULE_IN_THREAD)
        int ret = pthread_create(&m_server_thread, NULL, &MBModuleEntryPoint, (void*)(*m_context));
        if(ret != 0){
            log.error("Fail to create MBServer thread!Err:%i",ret);
            DBG_ERR  ("Fail to create MBServer thread!Err:%i",ret);
        }

    #elif (MB_MODULE_RUNNING_MODE == MB_MODULE_IN_PROCESS)
        pid_t pid;
        pid = fork();
        if(pid == -1)
        {

            log.error("Modbus fork failed, error %d\n", errno);

        }
        else if(pid == 0)
        {
            log.info("Modbus fork successful with pid %d\n", pid);
            MBModuleEntryPoint((void*)(*m_context));

        }
    #endif
#endif

}


}/* End of namespace mbm*/


/*
 *********************** End of file ******************************************
 */
