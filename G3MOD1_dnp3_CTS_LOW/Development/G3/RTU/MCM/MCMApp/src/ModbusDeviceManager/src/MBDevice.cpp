/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MBDevice.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13 May 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MBDevice.h"
#include "mbm_helpers.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define DSICARD_EVENT_NO_CHANNEL(event)\
           DBG_WARN("Discarded event.Channel not found at index:%i, type:%i.",\
                event.channel.channelId,event.channel.channelType)
/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
namespace mbm{

MBDevice::MBDevice(MODULE_ID id):
                                IDevice(MODULE_MBDEVICE, id),
                                log(Logger::getLogger(SUBSYSTEM_ID_MODBUS_MASTER)),
                                mp_dinChs(NULL),
                                m_dinChsNum(0),
                                mp_doutChs(NULL),
                                m_doutChsNum(0),
                                mp_ainChs(NULL),
                                m_ainChsNum(0),
                                mp_conf(NULL)
{
    DBG_INFO("C: [Ctor]MBDevice %i",id);
}

MBDevice::~MBDevice()
{
    DBG_INFO("C: [Dtor]MBDevice %i",getIndex());

    // Delete channels
    for (int i = 0; i < m_dinChsNum; ++i) {
        delete mp_dinChs[i];
        mp_dinChs[i] = NULL;
    }
    delete[]mp_dinChs;
    mp_dinChs = NULL;

    for (int i = 0; i < m_doutChsNum; ++i) {
        delete mp_doutChs[i];
        mp_doutChs[i] = NULL;
    }
    delete[]mp_doutChs;
    mp_doutChs = NULL;

    for (int i = 0; i < m_ainChsNum; ++i) {
        delete mp_ainChs[i];
        mp_ainChs[i] = NULL;
    }
    delete[]mp_ainChs;
    mp_ainChs = NULL;

    releaseConfig();
}

void MBDevice:: printConfig()
{
    DBG_INFO("---> Print Device[%i:%i] Configuration",
                    mp_conf->ref.deviceType, mp_conf->ref.deviceId);
    DBG_INFO("---> End of Device[%i:%i] Configuration",
                    mp_conf->ref.deviceType, mp_conf->ref.deviceId);
}

void MBDevice:: printID()
{
    DBG_INFO("!!! I am MB Device: %i",getIndex());
}

MBChannel* MBDevice::getMBChannel(CHANNEL_TYPE chType, lu_uint16_t  chIdx)
{
    switch(chType){
        case CHANNEL_TYPE_AINPUT:
            if(chIdx < m_ainChsNum)
                return mp_ainChs[chIdx];
            break;

        case CHANNEL_TYPE_DINPUT:
            if(chIdx < m_dinChsNum)
                return mp_dinChs[chIdx];
            break;

        case CHANNEL_TYPE_DOUTPUT:
            if(chIdx < m_doutChsNum)
                return mp_doutChs[chIdx];
            break;

        default:
            DBG_ERR("Unsupported channel type: %i",chType);
            break;
    }

    return NULL;
}

IChannel* MBDevice:: getChannel(CHANNEL_TYPE type, lu_uint32_t number)
{
   mbm::MBChannel* ch;
   ch = getMBChannel(type,number);

   if(ch != NULL)
       return dynamic_cast<IChannel*>(ch);
   else
       return NULL;
}

lu_uint16_t MBDevice::getChannelNum(CHANNEL_TYPE chType)
{
    switch(chType){
    case CHANNEL_TYPE_AINPUT:
        return m_ainChsNum;

    case CHANNEL_TYPE_DINPUT:
        return m_dinChsNum;

    default:
        return 0;
    }
}


void MBDevice::setChannels(CHANNEL_TYPE type, MBChannel* chs[], lu_uint16_t chsNum)
{
    switch (type) {
        case CHANNEL_TYPE_DINPUT:
            mp_dinChs = chs;
            m_dinChsNum = chsNum;
            break;

        case CHANNEL_TYPE_AINPUT:
            mp_ainChs = chs;
            m_ainChsNum = chsNum;
            break;

        case CHANNEL_TYPE_DOUTPUT:
            mp_doutChs = chs;
            m_doutChsNum = chsNum;
            break;

        default:
            log.error("Unsupported channel type:%i",type);
            break;
    }
}


void MBDevice::setConfig(DeviceConf& conf)
{
    if(mp_conf == NULL)
    {
        mp_conf = new DeviceConf();
        memcpy(&(*mp_conf), &conf, sizeof(DeviceConf));

        //DBG_INFO("Set configuration to:  %s",mp_conf->toString().c_str());
    }
}

void MBDevice:: applyConfig(zmq::socket_t& socket)
{
    // Send device configuration
    if(mp_conf != NULL)
    {
        MsgHeader head(MSG_TYPE_CFG, MSG_ID_CFG_DEVICE_C);
        socket.send(&head,sizeof head, ZMQ_SNDMORE);
        //dump_data(&head,sizeof head);

        socket.send(mp_conf,sizeof(DeviceConf),0);
        dump_data(mp_conf,sizeof(DeviceConf));

        DBG_INFO("<=== Send device config.%s", mp_conf->toString().c_str());

        // Get reply
        zmq::message_t reply;
        do{
           socket.recv(&reply);
           DBG_INFO("===> Receive size:%i",reply.size());
        }
        while(reply.more());
    }
    else
    {
        DBG_ERR("No configuration");
    }

    // Send channel configuration
    for (int i = 0; i < m_dinChsNum; ++i) {
        mp_dinChs[i]->applyConfig(socket);
    }

    for (int i = 0; i < m_ainChsNum; ++i) {
        mp_ainChs[i]->applyConfig(socket);
    }

    for (int i = 0; i < m_doutChsNum; ++i) {
        mp_doutChs[i]->applyConfig(socket);
    }
}

void MBDevice::releaseConfig()
{
    if(mp_conf != NULL)
    {
        DBG_INFO("Releasing config: %s.",mp_conf->ref.toString().c_str());
        delete mp_conf;
        mp_conf = NULL;
    }
}

void MBDevice::handleEvent(ChannelEvent& event)
{
    if(verifyChannel(event.channel) == LU_FALSE)
        return;

    mbm::MBChannel* ch;
    ch = getMBChannel((CHANNEL_TYPE)(event.channel.channelType),
                    event.channel.channelId);


    if(ch != NULL)
    {
        ch->handleEvent(event);
    }
    else
    {
        DSICARD_EVENT_NO_CHANNEL(event);
        DBG_ERR("Discarded event since channel[%i:%i] is not found.",
                        event.channel.channelType,
                        event.channel.channelId);
    }

}

/************************************/
/*	SerialMBDevice Implementation   */
/************************************/
/*

SerialMBDevice::SerialMBDevice(lu_uint16_t deviceId,const SerialChlConf* chlConf)
                                            :MBDevice(deviceId),
                                             chlConf(chlConf)

{
    log.debug("C: [Ctor]SerialMBDevice %i",id);
}

SerialMBDevice::~SerialMBDevice()
{
    log.debug("C: [Dtor]SerialMBDevice %i",id);
}

void SerialMBDevice::sendConfig(zmq::socket_t& socket)
{
    MsgHeader head(MSG_TYPE_CFG, MSG_ID_CFG_SERIAL_LINK_C);
    socket.send(&head,sizeof head, ZMQ_SNDMORE);

    socket.send(&conf,sizeof(conf),0);
}*/


/************************************/
/*  TCPMBDevice Implementation      */
/************************************/
/*

TCPMBDevice::TCPMBDevice(lu_uint16_t deviceId)
                                            :MBDevice(deviceId)

{
    log.debug("C: [Ctor]TCPMBDevice %i",id);
}

TCPMBDevice::~TCPMBDevice()
{
    log.debug("C: [Dtor]TCPMBDevice %i",id);
}


void TCPMBDevice::sendConfig(zmq::socket_t& socket)
{
    MsgHeader head(MSG_TYPE_CFG, MSG_ID_CFG_TCP_SESSION_C);
    socket.send(&head,sizeof head, ZMQ_SNDMORE);
    socket.send(&conf,sizeof(conf),0);
}
*/

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

lu_bool_t MBDevice::verifyChannel(ChannelRef& ch)
{
    if(mp_conf == NULL)
    {
        DBG_ERR("No configuration available for the device:%i", getIndex());
        return LU_FALSE;
    }

    if(ch.device.deviceType != mp_conf->ref.deviceType)
    {
        DBG_ERR("Mismatched device type. Expected:%i Actual:%i ",
                        mp_conf->ref.deviceType,
                        ch.device.deviceType);

       return LU_FALSE;
    }

    if(ch.device.deviceId != mp_conf->ref.deviceId)
    {
        DBG_ERR("Mismatched device id. Expected:%i Actual:%i ",
                        mp_conf->ref.deviceId,
                        ch.device.deviceId);

       return LU_FALSE;
    }

    return LU_TRUE;
}




void MBDevice::sendDeviceNumConfig(zmq::socket_t& socket,lu_uint8_t tcpDevNum,lu_uint8_t serialDevNum)
{
    MsgHeader head(MSG_TYPE_CFG, MSG_ID_CFG_DEVICE_NUM_C);
    socket.send(&head,sizeof head, ZMQ_SNDMORE);

    DeviceNum conf;
    conf.tcpDevNum = tcpDevNum;
    conf.serialDevNum = serialDevNum;

    socket.send(&conf,sizeof(conf),0);
    //dump_data(m_conf,sizeof(DeviceConf));

    DBG_INFO("<=== Send device number config. tcpDevNum num:%i serialDevNum:%i",tcpDevNum,serialDevNum);

    // Get reply
    zmq::message_t reply;
    do{
       socket.recv(&reply);
       DBG_INFO("===> Receive size:%i",reply.size());
    }
    while(reply.more());

}

IOM_ERROR MBDevice::readRAWChannel(IChannel::ChannelIDStr id,
                IChannel::ValueStr& data)
{
    IChannel* ch = NULL;
    ch = getChannel(id.type,id.id);

    if(ch != NULL)
    {
       ch->read(data);
       return IOM_ERROR_NONE;
    }
    else
    {
        return IOM_ERROR_NOT_FOUND;
    }

}

void MBDevice::getDeviceStats(zmq::socket_t& socket,lu_uint8_t devID)
{
    MsgHeader msgheader;
    struct mbm::devicestats stats;
    // Send request
    msgheader.type = MSG_TYPE_READ;
    msgheader.id   = MSG_ID_READ_STATS_C;
    try
    {
        /*send header*/
        socket.send(&msgheader,sizeof(msgheader),ZMQ_SNDMORE);
        DBG_INFO("<=== Send request: %s",commandName(MSG_TYPE_READ,MSG_ID_READ_STATS_C));

        /*send device ID*/
        socket.send(&devID,sizeof(devID),0);
        DBG_INFO("<=== Send device ID: %d",devID);

        // Wait reply
        zmq::message_t reply;
        do{
            if(socket.recv(&reply))
            {
                stats = *(struct mbm::devicestats *)(reply.data());
                if(reply.size() == sizeof(struct mbm::devicestats))
                {
                    DBG_INFO("===> Reply : %s",stats.toString().c_str());
                }
            }
            else
            {
                DBG_ERR("===> No Reply");
                break;
            }
        }while(reply.more());
    } catch(zmq::error_t& e)
    {
        DBG_ERR("C: Sending request failed: %s",e.what());
    }
}

}//namespace mbm

/*
 *********************** End of file ******************************************
 */
