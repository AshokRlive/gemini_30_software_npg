/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MBMConfigProto.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   12 May 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef MBMCONFIGPROTO_H_
#define MBMCONFIGPROTO_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <time.h> //include timespec
#include <sstream>      // std::ostringstream
#include <iostream>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "StringUtil.h"
#include "vector"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define MB_MODULE_IN_PROCESS    0
#define MB_MODULE_IN_THREAD     1

#define MB_MODULE_RUNNING_MODE          MB_MODULE_IN_THREAD


#if (MB_MODULE_RUNNING_MODE == MB_MODULE_IN_PROCESS)
    #define MB_HOST_ADDRESS_REP "ipc:///tmp/modbus_master_req.ipc"
    #define MB_HOST_ADDRESS_SUB "ipc:///tmp/modbus_master_sub.ipc"
#elif(MB_MODULE_RUNNING_MODE == MB_MODULE_IN_THREAD)
    #define MB_HOST_ADDRESS_REP "inproc:///tmp/modbus_master_req.inproc"
    #define MB_HOST_ADDRESS_SUB "inproc:///tmp/modbus_master_sub.inproc"
#endif


/*Removed Because it causes problems during parsing with regards to memory and
 * handling packed objects
 */
//
//#pragma pack(push)
//#pragma pack(1)34

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * A protocol for configuring and communicating with ModbusMaster Virtual Module.
 */

/* ModBus Master name space*/
namespace mbm {

typedef std::vector<lu_uint16_t> registervalues;

/************************************/
/*	TMW library configuration struct*/
/************************************/

struct MMBSessionConf
{
    lu_uint16_t slaveAddress;
    lu_uint32_t defaultResponseTimeout;
};

struct MBLinkConf
{
    lu_uint8_t type;     /* MBLINK_TYPE*/
    lu_uint16_t maxQueueSize;
    lu_uint32_t rxFrameTimeout;
    lu_uint32_t  channelResponseTimeout;
};

struct LinTCPConf
{
public:
    static const int CHNLNAME_SIZE = 256;
    static const int IPADDRESS_SIZE = 20;

public:
    lu_char_t   chnlName[CHNLNAME_SIZE];     /* User specified channel name */
    lu_char_t   ipAddress[IPADDRESS_SIZE];
    lu_uint16_t ipPort;
    lu_uint32_t  ipConnectTimeout;
    lu_uint8_t     mode;            /* LINTCP_MODE */
    lu_bool_t   disconnectOnNewSyn;
    lu_uint8_t     role;            /* LINTCP_ROLE */

public:
    /* String conversion functions */
    void toIPAddress(std::string s)
    {
        stringToCString(s, this->ipAddress, IPADDRESS_SIZE);
    }
    void toChnlName(std::string s)
    {
        stringToCString(s, this->chnlName, CHNLNAME_SIZE);
    }
};


struct Lin232Conf
{
public:
    static const int CHNLNAME_SIZE = 256;
    static const int PORTNAME_SIZE = 256;

public:
    lu_char_t    chnlName[CHNLNAME_SIZE];    /* User specified channel name */
    lu_char_t    portName[PORTNAME_SIZE];    /* "/dev/ttyS0", "/dev/ttyS1", etc. */
    lu_uint8_t portMode;     /* flow control: none, hardware LIN232_PORT_MODE*/
    lu_int64_t baudRate;     /* baud rate LIN232_BAUD_RATE*/
    lu_uint8_t    parity;    /* !LIN232_PARITY */
    lu_uint8_t numDataBits;  /* 7 or 8 */
    lu_uint8_t numStopBits;  /* 1 or 2 */
    lu_bool_t    bModbusRTU; /* MODBUS RTU or not */

public:
    /* String conversion functions */
    void toChnlName(std::string s)
    {
        stringToCString(s, this->chnlName, CHNLNAME_SIZE);
    }
    void toPortName(std::string s)
    {
        stringToCString(s, this->portName, PORTNAME_SIZE);
    }
};



/******************************************/
/* ModBus Master configuration message ID */
/******************************************/

typedef enum
{
    MSG_TYPE_CMD        = 0x00,
    MSG_TYPE_CFG        = 0x01,
    MSG_TYPE_EVENT      = 0x02,
    MSG_TYPE_READ       = 0x03,
    MSG_TYPE_WRITE      = 0x04,

    MSG_TYPE_INVALID       = 0xFF
}MSG_TYPE;


typedef enum
{

    /* MSG_TYPE_COMMAND */
    MSG_ID_CMD_PING_C         = 0x00,
    MSG_ID_CMD_PING_R         = 0x01,/* MsgAck */
    MSG_ID_CMD_START_C        = 0X02,
    MSG_ID_CMD_START_R        = 0X03,/* MsgAck */
    MSG_ID_CMD_STOP_C         = 0X04,
    MSG_ID_CMD_STOP_R         = 0X05,/* MsgAck */

    /* MSG_TYPE_CFG */
    MSG_ID_CFG_DEVICE_NUM_C   = 0X00,/* DeviceNum */
    MSG_ID_CFG_DEVICE_NUM_R   = 0X01,/* MsgAck */
    MSG_ID_CFG_DEVICE_C       = 0X02,/* DeviceConf */
    MSG_ID_CFG_DEVICE_R       = 0X03,/* MsgAck */
    MSG_ID_CFG_CHANNEL_C      = 0X04,/* ChannelConf */
    MSG_ID_CFG_CHANNEL_R      = 0X05,/* MsgAck */

    /* MSG_TYPE_EVENT */
    MSG_ID_EVENT_ANALOGUE     = 0X00, /* ChannelEventAnalogue*/
    MSG_ID_EVENT_DIGITAL      = 0X01,  /* ChannelEventDigital */

    /* MSG_TYPE_READ */
    MSG_ID_READ_CHANNEL_C     = 0X00, /* ChannelRef*/
    MSG_ID_READ_CHANNEL_R     = 0X01, /* ChannelValue*/
    MSG_ID_READ_STATS_C       = 0X02,
    MSG_ID_READ_STATS_R       = 0X03,  /* deviceStats */

    /* MSG_TYPE_CONSOLE_COMMANDS */
    MSG_ID_READ_CMDLINE_C    = 0X04, /* devRegInfo*/
    MSG_ID_READ_CMDLINE_R    = 0X05, /* readDevRegValue*/

    /* MSG_TYPE_WRITE */
    MSG_ID_WRITE_CHANNEL_C    = 0X00, /* ChannelValue*/
    MSG_ID_WRITE_CHANNEL_R    = 0X01, /* WRITE_ERR*/

    MSG_ID_WRITE_CMDLINE_C    = 0X02, /* writeRegValueStr*/
    MSG_ID_WRITE_CMDLINE_R    = 0X03, /* WRITE_ERR*/

    MSG_ID_INVALID         = 0xFF
}MSG_ID;


typedef enum
{
    MSG_ERR_NONE = 0,
    MSG_ERR_UNSUPPORTED,
    MSG_ERR_SERVER_ERR,
    MSG_ERR_MALFORMED
}MSG_ERR;

typedef enum
{
    WRITE_SUCCESS = 0,
    WRITE_FAILURE,
    WRITE_TIMEOUT,
    WRITE_CANCELLED
}WRITE_ERR;

typedef enum
{
    READ_SUCCESS = 0,
    READ_FAILURE,
    READ_TIMEOUT,
    READ_CANCELLED
}READ_ERR;

typedef enum
{
    REG_DATA_TYPE_FLOAT16  = 0,
    REG_DATA_TYPE_FLOAT32   ,
    REG_DATA_TYPE_INT16     ,
    REG_DATA_TYPE_INT32     ,
    REG_DATA_TYPE_UINT16,
    REG_DATA_TYPE_UINT32

}REG_DATA_TYPE; /* Only used by analogue channel*/

typedef enum
{
    MBDEVICE_CHANNEL_DI    = 0   ,
    MBDEVICE_CHANNEL_AI          ,
    MBDEVICE_CHANNEL_DO
}MBDEVICE_CHANNEL;

typedef enum
{
    REG_TYPE_COIL_STATUS        = 0,
    REG_TYPE_INPUT_STATUS,

    REG_TYPE_HOLDING_REGISTERS,
    REG_TYPE_INPUT_REGISTERS
}REG_TYPE;



/**********************************************/
/* ModBus Master configuration message struct */
/**********************************************/

struct MsgHeader
{
    lu_uint8_t type; /*! MSG_TYPE*/
    lu_uint8_t id;   /*! MSG_ID*/

public:
    MsgHeader(MSG_TYPE type, MSG_ID id)
    {
        this->type = type;
        this->id = id;
    }
    MsgHeader()
    {
        this->type = MSG_TYPE_INVALID;
        this->id = MSG_ID_INVALID;
    }


};

struct MsgAck
{
   lu_uint16_t errorno; /*! WRITE_ERR*/
};

enum DEVICE_TYPE
{
    DEVICE_TYPE_SERIAL = 0,
    DEVICE_TYPE_TCP
};


struct DeviceRef
{
    lu_uint8_t  deviceId;
    lu_uint8_t  deviceType; /* DEVICE_TYPE*/

public:
    std::string toString()
    {
        std::ostringstream ss;
        ss << "Device["<< to_string(deviceType)<<":"<< to_string(deviceId)<<"]";
        return ss.str();
    }
};


struct SerialDeviceConf
{
    MMBSessionConf sesnconf;
    MBLinkConf mblinkconf;
    Lin232Conf serialconf;
};


struct TCPDeviceConf
{
    MMBSessionConf sesnconf;
    MBLinkConf  mblinkconf;
    LinTCPConf tcpconf;
};

struct DeviceNum
{
    lu_uint16_t tcpDevNum;
    lu_uint16_t serialDevNum;
};


struct DeviceConf
{
    DeviceRef ref;
    lu_uint16_t aichlNum;/* Analogue input channel number*/
    lu_uint16_t dichlNum;/* Digital input channel number */
    lu_uint16_t dochlNum;/* Digital output channel number */

    lu_uint8_t    byteswap : 1;/*! enable byte swap - LU_TRUE or LU_FALSE */
    lu_uint8_t    wordswap : 1;/*! enable word swap - LU_TRUE or LU_FALSE */
    lu_uint8_t    unused   : 6;

    lu_uint32_t offLinePollingRate;/*Poll rate for when the device is offline*/
    lu_uint8_t  numOfRetries;/*num of retries when register is offline*/

    lu_bool_t active;  /* Determine whether the session/device is active or inactive. An
                        * inactive session/device will not transmit or receive frames.
                        */
    union
    {
        SerialDeviceConf serialDevConf;
        TCPDeviceConf tcpDevConf;
    }conf;

public:
    std::string toString()
    {
        std::string refstr = ref.toString();
        std::ostringstream ss;

        /*Append channel number*/
        ss << refstr;
        ss << " AI Num:"<<aichlNum;
        ss << " DI Num:"<<dichlNum;
        ss << " DO Num:"<<dochlNum;
        ss << " ByteSwap:"<< (byteswap == 1?"true":"false");
        ss << " WordSwap:"<< (wordswap == 1?"true":"false");

        /*Append link config*/
        ss << "\n" << refstr;
        MBLinkConf* link = ref.deviceType == DEVICE_TYPE_SERIAL ?
                        &(conf.serialDevConf.mblinkconf):&(conf.tcpDevConf.mblinkconf);
        ss <<" maxQueueSize:"            << link->maxQueueSize;
        ss <<" rxFrameTimeout:"          << link->rxFrameTimeout;
        ss <<" channelResponseTimeout:"  << link->channelResponseTimeout;

        /*Append session config*/
        ss << "\n" << refstr;
        MMBSessionConf* sesn = ref.deviceType == DEVICE_TYPE_SERIAL ?
                        &(conf.serialDevConf.sesnconf):&(conf.tcpDevConf.sesnconf);
        ss << " slaveAddress:"          << sesn->slaveAddress;
        ss << " defaultResponseTimeout:"<< sesn->defaultResponseTimeout;

        /*Append IO config*/
        ss << "\n" << refstr;
        if(ref.deviceType == DEVICE_TYPE_SERIAL)
        {
            Lin232Conf& lin232 = conf.serialDevConf.serialconf;
            ss << " chnlName:"<<lin232.chnlName;
            ss << " bModbusRTU:"<< to_string(lin232.bModbusRTU);
            ss << " baudRate:"<<lin232.baudRate;
            ss << " numDataBits:"<<to_string(lin232.numDataBits);
            ss << " numStopBits:"<<to_string(lin232.numStopBits);
            ss << " parity:"<<to_string(lin232.parity);
            ss << " portMode:"<<to_string(lin232.portMode);
            ss << " portName:"<<lin232.portName;
        }
        else if(ref.deviceType == DEVICE_TYPE_TCP)
        {
            LinTCPConf& tcp = conf.tcpDevConf.tcpconf;
            ss << " chnlName:"<< tcp.chnlName;
            ss << " disconnectOnNewSyn:"<< to_string(tcp.disconnectOnNewSyn);
            ss << " ipAddress:"<< tcp.ipAddress;
            ss << " ipConnectTimeout:"<< tcp.ipConnectTimeout;
            ss << " ipPort:"<< tcp.ipPort;
            ss << " mode:"<< to_string(tcp.mode);
            ss << " role:"<< to_string(tcp.role);
        }

        return ss.str();

    }
};

/**
 * G3 channel reference.
 */
struct ChannelRef
{
    DeviceRef device;
    lu_uint16_t channelId;
    lu_uint8_t channelType; /* MBDEVICE_CHANNEL */

public:
    std::string toString()
    {
        std::ostringstream ss;
        ss << "Channel["<< to_string(channelType)<<":"<< to_string(channelId)<<"]"<<device.toString();
        return ss.str();
    }
};

/**
 * G3 Device Channel Configuration.
 */
struct ChannelConf
{
public:
    static const lu_uint16_t unitSize = 256;

public:
    ChannelRef channel;
    lu_uint32_t reg;
    lu_uint8_t datatype;     /*! REG_DATA_TYPE*/
    lu_uint8_t regType;      /*! REG_TYPE*/
    lu_uint32_t pollingRate; /* milli-seconds */
    lu_uint16_t bitmask;     /* Bit mask applied to a channel value, digital channels only */

    /*Used by Analog Channels only*/
    lu_float32_t scalingFactor;
    lu_float32_t offset;
    lu_char_t unit[unitSize];

public:
    std::string toString()
    {
        std::ostringstream ss;
        ss << channel.toString();   //Append channel number
        ss << " reg:" << reg;
        ss << " datatype:" << to_string(datatype);
        ss << " regType:" << to_string(regType);
        ss << " pollingRate:" << pollingRate;
        ss << " bitmask:" << bitmask;
        ss << " scalingFactor:" << scalingFactor;
        ss << " offset:" << offset;
        ss << " unit: " << unit;
        return ss.str();
    }

};


struct ChannelValue
{
    ChannelRef chRef;
    lu_int32_t chValue;

public:
    std::string toString()
    {
        std::ostringstream ss;
        ss << chRef.toString();        /*Append channel number*/
        ss << " value:" << chValue;
        return ss.str();
    }
};


struct TimeDef
{
    timespec timestamp;
};


struct ChannelEvent
{
    /*! Channel ID */
    ChannelRef    channel;
    bool    isOnline ;    /* channel is online */

    /*! Event timestamp */
    TimeDef time;

    /*! Channel value */
    union
    {
        lu_int32_t analogValue  ;
        lu_int8_t  digitalValue ;
    };
};


/*structure to hold the device statistics info*/
struct devicestats
{
    lu_uint64_t respStatusSuccess;
    lu_uint64_t respStatusFailure;
    lu_uint64_t respStatusTimeout;
    lu_uint64_t respStatusCanceled;

public:
    devicestats():  respStatusSuccess(0),
                    respStatusFailure(0),
                    respStatusTimeout(0),
                    respStatusCanceled(0)
    {}

    std::string toString()
    {
        std::ostringstream ss;
        ss << " Device Stats : ";
        ss << " Successful requests : " << to_string(respStatusSuccess);
        ss << " Failed requests : " << to_string(respStatusFailure);
        ss << " Timeout requests : " << to_string(respStatusTimeout);
        ss << " Cancelled requests : " << to_string(respStatusCanceled);
        return ss.str();
    }
};


/*structure to hold device info*/
struct devRegInfo
{
    lu_uint8_t  deviceId;
    mbm::REG_TYPE regType;
    lu_uint16_t startAddress;
    lu_uint16_t quantity;
    lu_uint8_t tries;       //Amount of tries for message sending

public:
    devRegInfo() :  deviceId(0),  //Invalid
                    startAddress(0),
                    quantity(0),
                    tries(1)
    {}

    bool validate()
    {
        return( (deviceId != 0) &&
                (quantity != 0) &&
                (((lu_uint32_t)startAddress + (lu_uint32_t)quantity) <= 0xFFFF) &&
                (tries > 0)
                );
    }

    std::string toString()
    {
        std::ostringstream ss;
        ss << " Device ID : "<< to_string(deviceId) ;
        ss << " for register type: " << to_string(regType) ;
        ss << " from register address " << to_string(startAddress);
        ss << " with no of registers : " << to_string(quantity);
        ss << " no of tries : " << to_string(tries);
        return ss.str();
  }
};


struct readRegValueStr
{
    mbm::READ_ERR readErr;  //Resulting error code
    registervalues values;  //values to read
};


struct writeRegValueStr
{
    devRegInfo devreginfo;  //Device to write to
    registervalues values;  //values to write
};

//#pragma pack(pop)


/************************************/
/*	Help Methods                    */
/************************************/

inline const lu_char_t* commandName(MSG_TYPE msgType, MSG_ID msgId)
{
    switch (msgType)
    {
        case MSG_TYPE_CMD  :
            switch(msgId)
            {
              case MSG_ID_CMD_PING_C  :
              case MSG_ID_CMD_PING_R  :return "Command PING";
              case MSG_ID_CMD_START_C :
              case MSG_ID_CMD_START_R :return "Command START";
              case MSG_ID_CMD_STOP_C  :
              case MSG_ID_CMD_STOP_R  :return "Command STOP";
              default: return "Unknown";
            }
            break;
        case MSG_TYPE_CFG  :
            switch(msgId)
            {
              case MSG_ID_CFG_DEVICE_C  :
              case MSG_ID_CFG_DEVICE_R  :return "Config MBdevice";
              case MSG_ID_CFG_CHANNEL_C :
              case MSG_ID_CFG_CHANNEL_R :return "Config MBChannel";
              default: return "Unknown";
            }
            break;
        case MSG_TYPE_READ :
            switch(msgId)
            {
              case MSG_ID_READ_CHANNEL_C  :
              case MSG_ID_READ_CHANNEL_R  :return "Read MBChannel";
              case MSG_ID_READ_STATS_C     :return "READ Stats";
              case MSG_ID_READ_STATS_R     :
              default: return "Unknown";
            }
            break;
        case MSG_TYPE_WRITE :
            switch(msgId)
            {
              case MSG_ID_WRITE_CHANNEL_C  :
              case MSG_ID_WRITE_CHANNEL_R  :return "Write MBChannel";
              default: return "Unknown";
            }
            break;
        case MSG_TYPE_EVENT:
            switch(msgId)
            {
              case MSG_ID_EVENT_ANALOGUE :return "Event ANALOG";
              case MSG_ID_EVENT_DIGITAL  :return "Event DIGITAL";
              default: return "Unknown";
            }
            break;
          default: return "Unknown";
    }
}


} /** End of name space "mbm" */

#endif /* MBMCONFIGPROTO_H_ */

/*
 *********************** End of file ******************************************
 */
