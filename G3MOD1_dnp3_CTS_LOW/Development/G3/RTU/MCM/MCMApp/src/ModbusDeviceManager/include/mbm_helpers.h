/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:helpers.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15 May 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef G3_LU_HELPERS_H_
#define G3_LU_HELPERS_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <sstream>
#include <typeinfo> // for DBG_CTOR();
#include <stdio.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "zmq.hpp"
#include "MBDebug.h"
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
# define DATA_DUMP_ENABLE 0

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * Print byte data as hex string.
 */
inline void dump_data(std::string name, void* _data, size_t len)
{
#if DATA_DUMP_ENABLE
    unsigned char* data  = (unsigned char*)_data;
    printf("\n========%s[size:%u]=========\n",name.c_str(), len);

    int rows = len/8 + 1;
    for (int i = 0; i < rows; i++)
    {
        int pos;
        for (int j = 0; j < 8; j++){
            pos = i*8 + j;
            if(pos < len)
                printf("%02X ",data[pos]);
        }

        printf("\n");
    }
#else
    LU_UNUSED(name);
    LU_UNUSED(_data);
    LU_UNUSED(len);
#endif
}

/**
 * Print byte data as hex string.
 */
inline void dump_data(void* _data, size_t len)
{
    dump_data("DUMP",_data, len);
}


/**
 * Receive and discard message frames in a socket.
 */
inline void discardRcvMsg(zmq::socket_t& socket,zmq::message_t& msg)
{
    while(msg.more())
    {
        try
        {
            socket.recv(&msg);
            printf("Discarded unprocessed message. size:%i\n",msg.size());
        }
        catch (zmq::error_t& e)
        {}
    }
}




#endif /* G3_LU_HELPERS_H_ */

/*
 *********************** End of file ******************************************
 */
