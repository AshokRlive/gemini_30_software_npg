/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:IModbusConfigRepository.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11 Jun 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef IMODBUSCONFIGREPOSITORY_H_
#define IMODBUSCONFIGREPOSITORY_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "MBMConfigProto.h"
#include "MainAppEnum.h"
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
namespace mbm{


class IModbusConfigRepository
{
public:
    IModbusConfigRepository(){};

    virtual ~IModbusConfigRepository(){};

    virtual lu_uint8_t getDeviceNum() = 0;

    virtual void getDeviceConf(const lu_uint8_t deviceIdx, DeviceConf* conf) = 0;

    virtual lu_int32_t getChannelNum(const lu_uint8_t deviceIdx, CHANNEL_TYPE type) = 0;

    virtual void getChannelConf(const ChannelRef& ch, ChannelConf* conf ) = 0;
};


}
#endif /* IMODBUSCONFIGREPOSITORY_H_ */

/*
 *********************** End of file ******************************************
 */
