/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:ModbusDeviceFactory.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11 Jun 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MBDeviceFactory.h"

#include "mbm_helpers.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

namespace mbm{

MBDeviceFactory::MBDeviceFactory(IModbusConfigRepository& configRepo):
                                IMBDeviceFactory(),
                                m_configRepo(configRepo)
{
}

MBDeviceFactory::~MBDeviceFactory()
{
}

void MBDeviceFactory::createAllMBDevices(MBDevice*** devices, lu_uint8_t* num)
{

    const lu_uint8_t deviceNum = m_configRepo.getDeviceNum();
    mbm::MBDevice* device;

    *devices = new mbm::MBDevice*[deviceNum];
    for (lu_uint16_t deviceIdx = 0; deviceIdx < deviceNum; ++deviceIdx)
    {
        device = new mbm::MBDevice(static_cast<MODULE_ID>(deviceIdx));

        (*devices)[deviceIdx] = device;

        // Configure device
        DeviceConf conf;
        memset(&conf,0,sizeof(conf));
        m_configRepo.getDeviceConf(deviceIdx,&conf);
        device->setConfig(conf);

        createChannels(*device, CHANNEL_TYPE_DINPUT);
        createChannels(*device, CHANNEL_TYPE_AINPUT);
        createChannels(*device, CHANNEL_TYPE_DOUTPUT);
    }

    *num = deviceNum;
}

void MBDeviceFactory::createChannels(MBDevice& device,
                                    const CHANNEL_TYPE chtype
                                    )
{


    const lu_uint8_t deviceIdx = device.getIndex();
    lu_uint16_t ch_num = m_configRepo.getChannelNum(deviceIdx, chtype);

    MBChannel** chs = new MBChannel* [ch_num];

    ChannelConf ch_conf;
    ChannelRef ch_ref;

    for (lu_uint32_t chid = 0; chid < ch_num; ++chid)
    {
        ch_ref.channelId = chid;
        ch_ref.channelType = chtype;
        device.deviceRef(&(ch_ref.device));

        chs[chid] = new MBChannel(chtype, chid);

        m_configRepo.getChannelConf(ch_ref, &ch_conf);
        chs[chid]->setConfig(ch_conf);
    }


    device.setChannels(chtype, chs, ch_num);
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */



}
/*
 *********************** End of file ******************************************
 */
