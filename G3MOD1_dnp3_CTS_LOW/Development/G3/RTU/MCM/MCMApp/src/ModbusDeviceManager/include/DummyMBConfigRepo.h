/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:XMLModbusDeviceFactory.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19 May 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef XMLMODBUSDEVICEFACTORY_H_
#define XMLMODBUSDEVICEFACTORY_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IModbusConfigRepository.h"
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - VariablesIModbusDeviceFactory
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
namespace mbm{

class DummyMBConfigRepo :public IModbusConfigRepository
{
public:
    DummyMBConfigRepo();

    virtual ~DummyMBConfigRepo();

    virtual lu_uint8_t getDeviceNum();

    virtual void getDeviceConf(const lu_uint8_t deviceIdx, DeviceConf* conf) ;

    virtual lu_int32_t getChannelNum(const lu_uint8_t deviceIdx, CHANNEL_TYPE type) ;

    virtual void getChannelConf(const ChannelRef& ch, ChannelConf* conf ) ;

};

}/*End of namespace mbm*/

#endif /* XMLMODBUSDEVICEFACTORY_H_ */

/*
 *********************** End of file ******************************************
 */
