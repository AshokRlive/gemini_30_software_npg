/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MBDeviceManager.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   2 Jun 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MBDEVICEMANAGER_H_
#define MBDEVICEMANAGER_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "Logger.h"
#include "IIOModule.h"
#include "Mutex.h"
#include "IIOModuleManager.h"

#include "MBDevice.h"
#include "IMBDeviceFactory.h"
#include "IMBMController.h"
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

namespace mbm{


class MBDeviceManager : public IIOModuleManager
{
public:
    MBDeviceManager();
    virtual ~MBDeviceManager();

    void init(IMBDeviceFactory& factory);

    void start();


    MBDevice* getMBDevice(MODULE_ID moduleID);

    MBDevice* getMBDevice(lu_uint8_t moduleID)
    {
        return getMBDevice(static_cast<MODULE_ID>(moduleID));
    }


    void handleEvent(ChannelEvent& event);


    void getDeviceNum(lu_uint8_t* tcpDevNum, lu_uint8_t* serialDevNum);

    lu_uint8_t getTotalDeviceNum(){return m_deviceNum;};


    /************************************/
    /*	IIOModuleManager Interface      */
    /************************************/

    virtual IIOModule *createModule(MODULE module,
                    MODULE_ID moduleID,
                    COMM_INTERFACE iFace);

    virtual IIOModule *getModule(MODULE module, MODULE_ID moduleID);

    virtual std::vector<IIOModule*> getAllModules();

    virtual IChannel *getChannel( MODULE module,
                                  MODULE_ID moduleID,
                                  CHANNEL_TYPE chType,
                                  lu_uint32_t chNumber
                                );

    virtual IOM_ERROR stopAllModules();

private:
    Logger& log;
    IMBMController* m_controller;

    MBDevice** mp_allDevices;
    lu_uint8_t m_deviceNum;
};


}
#endif /* MBDEVICEMANAGER_H_ */

/*
 *********************** End of file ******************************************
 */
