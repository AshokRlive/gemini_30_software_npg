/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: FPIResetLogic.h 3 Sep 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/CLogic/include/ControlLogic/FPIResetLogic.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       FPI Reset Control Logic
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 3 Sep 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   3 Sep 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef FPIRESETLOGIC_H__INCLUDED
#define FPIRESETLOGIC_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "OperatableControlLogic.h"
#include "FPICANChannel.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief FPI Reset control logic.
 *
 * This Control Logic operates an FPI channel in the FPM in order to reset its
 * state.
 */
class FPIResetLogic: public OperatableControlLogic
{
public:
    struct Config : public OperatableControlLogic::Config
    {
    public:
        IChannel* channel;      //Channel to operate
        bool dependsLVRestore;  //true when using LV_FAIL input restore to trip
        bool dependsLVLoss;     //true when using LV_FAIL input loss to trip
        PointIdStr dInput[FPIRESET_INPUT_LAST]; //Input Digital virtual point list
    };

public:
    FPIResetLogic(Mutex* mutex                 ,
                  GeminiDatabase& database     ,
                  FPIResetLogic::Config& conf
                  );
   virtual ~FPIResetLogic();

   /* == Inherited from ControlLogic == */
   virtual GDB_ERROR init();
   virtual GDB_ERROR getInputValue(lu_uint16_t inputPoint, PointData* data);
   virtual GDB_ERROR getOutputValue(PointData* data);

protected:
   /* == Implements mandatory OperatableControlLogic == */
   virtual GDB_ERROR startOp(const SwitchLogicOperation &operation, const GDB_ERROR errorCode);

   /* == Inherited from ControlLogic == */
   virtual void newData(PointIdStr pointID, PointData& pointData);

private:
   FPIResetLogic::Config conf;
   FPICANChannel* channelFPI;
   DIObserver* dObservers[FPIRESET_INPUT_LAST]; //Observers over the CL inputs
   bool dependsLV;     //true when LV_FAIL input is in use
};


#endif /* FPIRESETLOGIC_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
