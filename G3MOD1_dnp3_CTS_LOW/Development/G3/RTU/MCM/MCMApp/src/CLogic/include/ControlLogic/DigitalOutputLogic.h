/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id: DigitalOutputLogic.h 21 Aug 2012 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/RTU/MCM/src/CLogic/include/DigitalOutputLogic.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Control Logic for Digital Output.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 21 Aug 2012 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   21/08/12      pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef DIGITALOUTPUTLOGIC_H__INCLUDED
#define DIGITALOUTPUTLOGIC_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IChannel.h"
#include "OperatableControlLogic.h"
#include "ControlLogicDigitalPoint.h"

class DOLOperationCmdJob;
class DOLOpCmdTimeoutTmr;

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Digital Output Control Logic Block
 *
 * The control logic is split in three different stages: offline, wait command,
 * and wait completion.
 *
 * The Control Logic is offline at object creation and should be ready (wait
 * command) in no time. When the command is requested, the request is accepted and
 * the function return with an error code.
 * The next stage of the logic is executed in a background task (as a database
 * job) because we need to wait until the Digital Output pulse is generated. After
 * the expected time (plus a 20% of time to ensure task completion check), the
 * control logic send a cancel to the module to see if last operation was completed
 * successfully or not. In any case, the control logic goes back to the wait
 * command stage to be ready for another command.
 */
class DigitalOutputLogic: public OperatableControlLogic
{
public:
    /**
     * \brief Digital Output logic control block
     */
    struct OpParams
    {
    public:
        lu_uint32_t pulseWidth;     /* Output pulse width in ms */
        lu_uint32_t polarity;       /* LED Polarity - 1 = Green for on */
    };

    /**
     * \brief configuration block for Digital Output Control Logic
     */
    struct Config: public OperatableControlLogic::Config
    {
    public:
        /* Amounts of different types of Control Logic's Pseudo Points */
        static const lu_uint16_t BINARYPOINT_NUM = DOL_POINT_TYPE_BINARY_COUNT;
        static const lu_uint32_t ALLPOINT_NUM = DOL_POINT_EnumSize;
    public:
        OpParams opParams;                  //Operation parameters
        PointIdStr dInput[DOL_INPUT_LAST];  //Digital Input point list
        ControlLogicDigitalPoint::Config bPoint[BINARYPOINT_NUM]; //Control Logic digital points
        IChannel *channel;       //Reference to the output channel
    };

public:
    /**
     * \brief Custom constructor
     *
     * \param mutex Reference to the G3DB common mutex
     * \param database Reference to the Gemini database
     * \param config reference to the Control Logic's configuration block
     */
    DigitalOutputLogic( Mutex *mutex,
                        GeminiDatabase& database,
                        DigitalOutputLogic::Config &config
                        );
    virtual ~DigitalOutputLogic();

    /* == inherited from ControlLogic == */
    virtual GDB_ERROR init();
    virtual GDB_ERROR getInputValue(lu_uint16_t inputPoint, PointData *data);
    virtual GDB_ERROR getOutputValue(PointData* data);

protected:
    /* == Implements mandatory OperatableControlLogic == */
    virtual GDB_ERROR startOp(const SwitchLogicOperation &operation, const GDB_ERROR errorCode);

protected:
    friend class DOLOperationCmdJob;
    friend class DOLOpCmdTimeoutTmr;

    typedef ControlLogic::OperationJob<DigitalOutputLogic> SendOperateCmds;
    //Note that "friend class SendOperateCmds" is not working in GCC < 4.5.7
    friend class ControlLogic::OperationJob<DigitalOutputLogic>;

    /**
     * \brief Send select+operate commands to the output
     *
     * This code is intended to run in an independent job to free the caller
     * thread from executing it.
     *
     * \return Error Code
     */
//    GDB_ERROR sendOperateCmds();
    void operationTimeout();
    void cancelOperation(lu_bool_t cancel);

    void runOperation(SendOperateCmds& operationJob, const bool delayed);

private:
    /**
     * \brief Check common preliminary conditions that can inhibit the operation
     *
     * \return Error code. GDB_ERROR_NONE when all preliminary checks passed.
     */
    GDB_ERROR preliminaryChecks();

private:
    enum DOL_STAGE
    {
        DOL_STAGE_OFF_LINE         = 0,
        DOL_STAGE_WAIT_COMMAND        ,
        DOL_STAGE_SEND_OPERATION_CMDS ,
        DOL_STAGE_WAIT_COMPLETION     ,

        DOL_STAGE_LAST
    };

private:
    DOL_STAGE stage;
    IChannel* channelDO;    //Reference to the output channel
    OpParams opParams;      //Digital Output operation parameters

    DIObserver* dObservers[DOL_INPUT_LAST];

    SwitchLogicOperation currentOperation;      //Current operation parameters
    DOLOpCmdTimeoutTmr* operationTimeoutTimer;  //Timer Job for operation check
    lu_uint32_t pulseWidth;     /* Output pulse width in ms */
    lu_bool_t operationActive;  /* Digital Output operation is active */
};


/**
 * \brief Operation Command timeout timer
 *
 * This class waits a timer to finish to then send a Cancel command.
 */
class DOLOpCmdTimeoutTmr : public TimedJob
{

public:
    /**
     * \brief Custom constructor
     *
     * \param cLogic Reference to the control logic to use
     * \param timeout Wait time value in ms
     */
    DOLOpCmdTimeoutTmr(DigitalOutputLogic &cLogic, lu_uint32_t timeout):
                                                           TimedJob(timeout, 0),
                                                           cLogic(cLogic)
    {};

    virtual ~DOLOpCmdTimeoutTmr() {};

protected:
    /**
     * \brief User define operation  This method is called by the run public method
     * only if the configured timer is expired
     *
     * \param timePtr Current absolute time
     *
     * \return none
     */
    virtual void job(TimeManager::TimeStr *timePtr);

private:
    DigitalOutputLogic &cLogic;
};


#endif /* DIGITALOUTPUTLOGIC_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
