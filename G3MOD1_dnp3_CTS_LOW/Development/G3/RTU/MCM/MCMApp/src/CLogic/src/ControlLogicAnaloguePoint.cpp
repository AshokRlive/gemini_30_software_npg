/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Control Logic custom analogue point implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "ControlLogicAnaloguePoint.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

ControlLogicAnaloguePoint::ControlLogicAnaloguePoint( Mutex *mutex                           ,
                                                      GeminiDatabase& g3database             ,
                                                      Config &config
                                                    ) :
                                                        AnaloguePoint(mutex, g3database, config),
                                                        incomingValue(std::numeric_limits<lu_float32_t>::quiet_NaN())
{
}


GDB_ERROR ControlLogicAnaloguePoint::init()
{
    if(initialised)
    {
        return GDB_ERROR_INITIALIZED;
    }
    if(!persistent)
    {
    	m_data.setTime(TimeMgr.now());
    }
    initialised = true;
    return GDB_ERROR_NONE;
}


GDB_ERROR ControlLogicAnaloguePoint::setValue(const lu_float32_t value,
                                              const TimeManager::TimeStr &newTimeStamp,
                                              const lu_bool_t forced
                                             )
{
    return setValue(value, newTimeStamp, incomingFlags, forced);
}


GDB_ERROR ControlLogicAnaloguePoint::setValue(const lu_float32_t value,
                                              const TimeManager::TimeStr &newTimeStamp,
                                              const PointFlags& flags,
                                              const lu_bool_t forced
                                             )
{
    GDB_ERROR ret;
    PointDataFloat32 pointData = m_data;
    lu_uint32_t dataChange = DATACHANGE_VALUE;   //Ini/Inv flags will be applied as well!

    if(forced == LU_TRUE)
    {
        dataChange = dataChange | DATACHANGE_FORCE;
    }
    if(value != incomingValue)
    {
        incomingValue = value;
        dataChange = dataChange | DATACHANGE_VALUE;
    }
    if(flags != incomingFlags)
    {
        incomingFlags = flags;
        dataChange = dataChange | DATACHANGE_FLAGS;
    }
    if(dataChange == DATACHANGE_NONE)
    {
        return GDB_ERROR_NONE;  //Nothing to do
    }
    pointData.setFlags(flags);
    pointData.setInitialFlag(false); //Not the initial value anymore
    pointData.setTime(newTimeStamp);
    pointData = value;
    ret = scheduleUpdate(*this, pointData, static_cast<DATACHANGE>(dataChange));
    if(ret != GDB_ERROR_NONE)
    {
        log.error("%s: Point (%s) - error: %s",
                    __AT__, pointID.toString().c_str(), GDB_ERROR_ToSTRING(ret)
                  );
    }
    return ret;
}


GDB_ERROR ControlLogicAnaloguePoint::setQuality(const lu_bool_t newStatus,
                                                const TimeManager::TimeStr& newTimeStamp
                                                )
{
    incomingFlags.setQuality((newStatus == LU_TRUE)? POINT_QUALITY_ON_LINE : POINT_QUALITY_OFF_LINE);
    return AbstractPoint::setQuality(m_data, newStatus, newTimeStamp);
}


GDB_ERROR ControlLogicAnaloguePoint::setFlags(const PointFlags& newFlags, const TimeManager::TimeStr& timestamp)
{
    incomingFlags = newFlags;
    return AbstractPoint::setFlags(m_data, newFlags, timestamp);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
void ControlLogicAnaloguePoint::update(PointData* newData, DATACHANGE dataChange)
{
    updateAnaloguePoint(newData, dataChange);
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */


