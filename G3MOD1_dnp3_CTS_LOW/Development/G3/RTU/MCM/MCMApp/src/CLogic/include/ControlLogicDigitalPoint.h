/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Control Logic custom digital point interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_97E9C188_E5B6_47a5_BAEF_ADAF1ED5CC6E__INCLUDED_)
#define EA_97E9C188_E5B6_47a5_BAEF_ADAF1ED5CC6E__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "DigitalPoint.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Implementation for Control Logic's Digital pseudo point.
 *
 * The data change source is only from the setter methods.
 * NOTE: This implementation is valid for Single and Double Binary Digital
 *      points since the source change comes only from the setters and therefore
 *      no debounce is needed.
 */
class ControlLogicDigitalPoint : public DigitalPoint
{
public:
    ControlLogicDigitalPoint( Mutex *mutex              ,
                              GeminiDatabase& g3database  ,
                              DigitalPoint::Config &config,
                              POINT_TYPE pointType = POINT_TYPE_BINARY
                            );

    virtual ~ControlLogicDigitalPoint() {};

    /* == Inherited from IPoint == */
    virtual GDB_ERROR init();

    /* == Inherited from DigitalPoint == */
    virtual GDB_ERROR setFlags(const PointFlags& newFlags, const TimeManager::TimeStr& timestamp); //Now public

    /**
     * \brief Write a new value to the point
     *
     * \param value Value to write
     * \param timestamp Time of the change
     * \param flags Point flags to set (optional)
     * \param forced Set to LU_TRUE to force point update even if it is the same (optional)
     *
     * \return Error code
     */
    GDB_ERROR setValue(const lu_uint32_t value,
                       const TimeManager::TimeStr& timestamp,
                       const lu_bool_t forced = LU_FALSE
                       );
    GDB_ERROR setValue(const lu_uint32_t value,
                       const TimeManager::TimeStr& timestamp,
                       const PointFlags& flags,
                       const lu_bool_t forced = LU_FALSE
                       );

    /**
     * \brief Write a new status to the point
     *
     * \param status Online/offline status
     * \param timestamp Time of the change
     *
     * \return Error code
     */
    GDB_ERROR setQuality(const lu_bool_t status, const TimeManager::TimeStr& timestamp);

protected:
    /* == Inherited from DigitalPoint == */
    virtual void update(PointData* newData, DATACHANGE dataChange);

private:
    /* Values for help setting DATACHANGE */
    lu_uint8_t incomingValue;   //Latest value set with setValue
    PointFlags incomingFlags;   //Latest flags set with setValue/setFlags/setQuality
};


#endif // !defined(EA_97E9C188_E5B6_47a5_BAEF_ADAF1ED5CC6E__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
