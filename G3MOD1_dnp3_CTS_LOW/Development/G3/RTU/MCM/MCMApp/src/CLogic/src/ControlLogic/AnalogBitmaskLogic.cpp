/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:AnalogBitmaskLogic.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief Logic to generate an analogue value from up to 16 different
 *              single binary VPs.
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   28 Sep 2016     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "AnalogBitmaskLogic.h"
#include "bitOperations.h"
#include "StringUtil.h"
#include "Debug.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
AnalogBitmaskLogic::AnalogBitmaskLogic(Mutex* mutex,
                                        GeminiDatabase& database,
                                        AnalogBitmaskLogic::Config& config) :
                                            ControlLogic(mutex, database, config, CONTROL_LOGIC_TYPE_BTA),
                                            configured(false),
                                            m_publishJob(NULL)
{
    //Adjust vector sizes to do not waste memory
    clPoint.reserve(Config::ALLPOINT_NUM);
    m_dInput.reserve(LU_MIN(config.numDigInputs, Config::ALLINPOINT_NUM));

    /* Allocate Control Logic virtual points */
    clPoint.push_back(new ControlLogicAnaloguePoint(mutex, database, config.aPoint));

    m_debounce_ms = config.debounce_ms;
    configured = true;
}

AnalogBitmaskLogic::~AnalogBitmaskLogic()
{
    database.cancelJob(m_publishJob);

    stopUpdate();

    /* Release digital input observers */
    MasterLockingMutex mLock(m_inListMutex, LU_TRUE);   //Protect the observer list

    for (lu_uint32_t i = 0; i < m_dInput.size(); ++i)
    {
        detachObserver(m_dInput[i]);
    }
}


GDB_ERROR AnalogBitmaskLogic::init()
{
    GDB_ERROR ret = initCheck();
    if(ret == GDB_ERROR_NONE)
    {
        ret = activateCLPoints();   //Initialise Control Logic points as well
    }
    if(ret != GDB_ERROR_NONE)
    {
        return ret;
    }

    startCLPoints(true);
    initialised = true;

    {//Input list mutex lock
        MasterLockingMutex mLock(m_inListMutex, LU_TRUE);
        GDB_ERROR res = GDB_ERROR_NONE;
        for(lu_uint32_t i = 0; i < m_dInput.size(); ++i)
        {
            /* Check input configuration */
            if(m_dInput[i] == NULL)
            {
                continue;   //skip unassigned
            }
            res = attachObserver(m_dInput[i]);
            if(res != GDB_ERROR_NONE)
            {
                ret = res;  //Mandatory input not configured
            }
        }
        if(ret != GDB_ERROR_NONE)
        {
            //Detach all already attached and disable Control Logic indications
            initialised = false;
            for(lu_uint32_t i = 0; i < m_dInput.size(); ++i)
            {
                detachObserver(m_dInput[i]);
            }
            startCLPoints(false);
        }
    }

    return ret;
}


GDB_ERROR AnalogBitmaskLogic::getInputValue(lu_uint16_t inputPoint, PointData* data)
{
    GDB_ERROR ret = GDB_ERROR_PARAM;

    if(data == NULL)
    {
        return GDB_ERROR_PARAM;
    }

    // Protect the list
    MasterLockingMutex mLock(m_inListMutex);

    try
    {
        if(m_dInput.at(inputPoint) == NULL)
        {
            ret = GDB_ERROR_NOPOINT;    //unassigned
        }
        else
        {
            PointIdStr id = m_dInput.at(inputPoint)->getPointID();
            ret = database.getValue(id, data);
        }
    }
    catch (const std::out_of_range& e)
    {
        ret = GDB_ERROR_NOPOINT;
    }
    return ret;
}


GDB_ERROR AnalogBitmaskLogic::addPoint(PointIdStr point)
{
    if(initialised)
        return GDB_ERROR_INITIALIZED;

    if(!configured)
        return GDB_ERROR_NOT_INITIALIZED;

    MasterLockingMutex mLock(m_inListMutex, LU_TRUE);
    if(point.isValid())
    {
        m_dInput.push_back(new DIObserver(point, *this));
    }
    else
    {
        m_dInput.push_back(NULL);   //not assigned: value treated as: 0 [online]
    }
    return GDB_ERROR_NONE;
}


void AnalogBitmaskLogic::newData(PointIdStr pointID, PointData& pointData)
{
    bool publishIt = false;

    if(!enabled)
    {
        return;
    }
    //An observed point has changed: update pre-output
    MasterLockingMutex mLock(m_inListMutex);

    m_preOutput.flags = ValidFlags;  //Validate point flags

    /* Review ALL inputs for validity & value */
    for (lu_uint32_t i = 0; i < m_dInput.size(); ++i)
    {
        if(m_dInput[i] == NULL)
        {
            continue;   //skip unassigned
        }
        if( !(m_dInput[i]->getFlags().isActive()) )
        {
            //When any input is invalid/offline, the calculated value gets invalidated
            m_preOutput.flags.setInvalid(true);
        }
        if(m_dInput[i]->getPointID() == pointID)
        {
            //Found the one that changed the value (not the flags)
            lu_uint8_t value = (lu_uint8_t)pointData;
            LU_SETBITVALUE(m_preOutput.value, i, value);
            //(Re)start counting debounce for settling data
            publishIt = true;
        }
    }
    /* When value changed or final flag changed */
    if( publishIt || (clPoint[BTA_POINT_ANALOGUE]->getFlags() != m_preOutput.flags) )
    {
        m_publishJob = new PublishJob(++m_publishID, *this, m_debounce_ms);
        GDB_ERROR ret = database.addJob(m_publishJob);
        if(ret != GDB_ERROR_NONE)
        {
            log.info("%s: unable to start publishing periodic job - immediate publish! Error: %s",
                        this->toString().c_str(), GDB_ERROR_ToSTRING(ret)
                      );
            TimeManager::TimeStr curTime = timeManager.now();   /* Initial time stamp */
            publish(++m_publishID, curTime);
        }
    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void AnalogBitmaskLogic::publish(const lu_uint32_t publishID, TimeManager::TimeStr& currentTime)
{
    MasterLockingMutex mLock(m_inListMutex);    //Prevent publishing while updating flags

    //Check publish job is the latest
    if(publishID != m_publishID)
    {
        DBG_INFO("%s: Publish job %i discarded", this->toString().c_str(), publishID);
        return; //Discard this job
    }

    //update output
    DBG_INFO("%s: Publishing new value: %i %s",
                this->toString().c_str(), m_preOutput.value,
                m_preOutput.flags.toString().c_str());
    lu_float32_t aValue = m_preOutput.value;
    setValue(BTA_POINT_ANALOGUE, aValue, currentTime, m_preOutput.flags);
}


/*
 *********************** End of file ******************************************
 */
