/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id: DummySwitchLogic.h $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/RTU/MCM/src/CLogic/src/DummySwitchLogic.cpp $
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Dummy Switch control logic implementation
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 2012-07-31 16:14:00 +0100 (Tue, 31 Jul 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   31/07/12      pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "DummySwitchLogic.h"
#include "MCMLED.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

DummySwitchLogic::DummySwitchLogic( Mutex* mutex,
                                    GeminiDatabase& g3database,
                                    IMemoryManager& memMgr,
                                    DummySwitchLogic::Config& conf
                                ) : OperatableControlLogic(mutex, g3database, conf, CONTROL_LOGIC_TYPE_DUMMYSW),
                                    memoryManager(memMgr),
                                    actualPosition(DBINARY_INDETERMINATE),
                                    postOpTimeOn(false),
                                    postOpTimer(NULL)
{
    operationParameters = conf.opParams;

    /* Allocate Control Logic virtual points */
    clPoint.reserve(Config::ALLPOINT_NUM);  //Adjust vector size to do not waste memory
    for(lu_uint32_t id = 0; id < Config::BINARYPOINT_NUM; ++id)
    {
        clPoint.push_back(new ControlLogicDigitalPoint(mutex, database, conf.bPoint[id], POINT_TYPE_BINARY));
    }
    for(lu_uint32_t id = 0; id < Config::DBINARYPOINT_NUM; ++id)
    {
        clPoint.push_back(new ControlLogicDigitalPoint(mutex, database, conf.dbPoint[id], POINT_TYPE_DBINARY));
    }

    /* Retrieve Dummy Switch position from memory */
    lu_bool_t memfail = LU_TRUE;
    lu_uint8_t storedPosition;  //value is stored in reduced format
    if(memoryManager.read(IMemoryManager::STORAGE_VAR_DUMMYSW, &storedPosition) == LU_TRUE)
    {
        actualPosition = static_cast<DBINARY_STATUS>(storedPosition);
        if( (actualPosition == DBINARY_OFF) || (actualPosition == DBINARY_ON) )
        {
            memfail = LU_FALSE;
        }
    }
    if(memfail == LU_TRUE)
    {
        /* Failed: Set the default state */
        actualPosition = DBINARY_OFF;
        log.error("%s reading from NVRAM memory failed. "
                    "Setting Dummy Switch position to default: %s (%i)",
                    this->toString().c_str(),
                    (actualPosition == DBINARY_OFF)? "Open" :
                                    (actualPosition == DBINARY_ON)? "Closed" : "Unknown",
                    actualPosition
                    );
    }
}


DummySwitchLogic::~DummySwitchLogic()
{
    database.cancelJob(postOpTimer);

    /* Switch OFF LED */
    MCMLED& dummyLED = MCMLED::getMCMLED(MCMLED::LED_ID_DUMMYSW);
    dummyLED.setStatus(MCMLED::LED_STATUS_OFF);
}


GDB_ERROR DummySwitchLogic::init()
{
    GDB_ERROR ret = initCheck();
    if(ret == GDB_ERROR_NONE)
    {
        ret = activateCLPoints();   //Initialise Control Logic points as well
    }
    if(ret != GDB_ERROR_NONE)
    {
        return ret;
    }

    /* Set starting Dummy Switch position on LED */
    switchDummyLED((actualPosition == DBINARY_OFF)? LU_TRUE : LU_FALSE); // open when position DBINARY_OFF

    /* Start Control Logic Points */
    TimeManager::TimeStr timestamp = timeManager.now();

    setValue(DSL_POINT_FAULT, 0, timestamp, ValidFlags); //Reset error flag

    //Set the initial Switch value
    setValue(DSL_POINT_OPENSTATUS, getDBINARYOpenValue(actualPosition), timestamp, ValidFlags);
    setValue(DSL_POINT_CLOSESTATUS, getDBINARYCloseValue(actualPosition), timestamp, ValidFlags);
    setValue(DSL_POINT_STATUS, actualPosition, timestamp, ValidFlags);

    initialised = true;

    /* Initialise input points */
    ret = initInhibit();
    if(ret != GDB_ERROR_NONE)
    {
        //Detach all already attached and disable Control Logic indications
        initialised = false;
        startCLPoints(false, timestamp);
    }

    return ret;
}


GDB_ERROR DummySwitchLogic::getInputValue(lu_uint16_t inputPoint, PointData *data)
{
    return checkInputValue(DSL_INPUT_OLR, DSL_INPUT_INHIBIT, inputPoint, data);
}


GDB_ERROR DummySwitchLogic::getOutputValue(PointData* data)
{
    if(data == NULL)
    {
        return GDB_ERROR_PARAM;
    }
    if(!initialised)
    {
        return GDB_ERROR_NOT_INITIALIZED;
    }
    /* Report state */
    PointDataUint8* data8 = (PointDataUint8*)data;
    *data8 = actualPosition;
    /* Dummy Switch is always online */
    data->setOnlineFlag(true);
    return GDB_ERROR_NONE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
GDB_ERROR DummySwitchLogic::startOp(const SwitchLogicOperation &operation,
                                    const GDB_ERROR errorCode)
{
    DBINARY_STATUS startPos;
    DBINARY_STATUS endPos;
    CTEventSwitchStr event;     //Event to be reported
    lu_uint32_t light;          //LED light colour to be used
    GDB_ERROR ret = GDB_ERROR_NONE;

    log.info("%s - Operation: %i - Local: %i ",
                this->toString().c_str(), operation.operation, operation.local
              );

    /* Decode command */
    confPosition(operation.operation, startPos, endPos);
    if(operation.operation == SWITCH_OPERATION_OPEN)
    {
        light = (operationParameters.openColour == LED_COLOUR_GREEN) ?
                                            LED_COLOUR_GREEN : LED_COLOUR_RED;
    }
    else
    {
        light = (operationParameters.openColour == LED_COLOUR_GREEN) ?
                                            LED_COLOUR_RED : LED_COLOUR_GREEN;
    }

    LockingMutex lMutex(*globalMutex);

    /* Get current time for output operations */
    TimeManager::TimeStr acceptedOperationTime = timeManager.now();

    event.eventType = (endPos == DBINARY_OFF)? EVENT_TYPE_SWITCH_REQ_OPEN :
                                               EVENT_TYPE_SWITCH_REQ_CLOSE;
    event.moduleID = DummySwitchID;// Indicate this is a dummy switch event.
    event.moduleType = MODULE_MCM;

    /* == Preliminary checks == */
    if(errorCode != GDB_ERROR_NONE)
    {
        refuseOperationMsg(errorCode);
        if(errorCode != GDB_ERROR_NOT_INITIALIZED)
        {
            //Report Event
            event.value = (errorCode == GDB_ERROR_LOCAL_REMOTE)?
                                                    EVENTSWITCH_VALUE_OLR_FAIL :
                                                    EVENTSWITCH_VALUE_INHIBIT;
            eventLog->logEvent(event);
            /* Set error flag */
            setValue(DSL_POINT_FAULT, 1, acceptedOperationTime);
        }
        return errorCode;
    }

    /* Check switch position */
    if( (actualPosition != startPos) && (!(operationParameters.allowForced)) )

    {
        log.warn("%s - Wrong switch position %i. Expected: %i",
                    this->toString().c_str(),
                    actualPosition,
                    startPos
                  );
        //Report Event
        event.value = EVENTSWITCH_VALUE_POS_FAIL;
        eventLog->logEvent(event);

        /* Set error flag */
        setValue(DSL_POINT_FAULT, 1, acceptedOperationTime);
        return GDB_ERROR_POSITION;
    }

    /* Check post-operation time to refuse to switch again before timer expires */
    if(postOpTimeOn)
    {
        log.warn("%s - operation refused: unable to switch again yet",
                    this->toString().c_str()
                  );
        //Report Event
        event.value = EVENTSWITCH_VALUE_IN_OPERATION;
        eventLog->logEvent(event);

        /* Set error flag */
        setValue(DSL_POINT_FAULT, 1, acceptedOperationTime);
        return GDB_ERROR_ALREADY_ACTIVE;
    }

    /* == Start Operation == */

    /* Reset error flag */
    setValue(DSL_POINT_FAULT, 0, acceptedOperationTime);

    /* NOTE: Dummy Switch status should be set independently of the LED HW failure */

    /* Leave dummy switched to its new state: */
    actualPosition = endPos;

    /* Set the new Switch value */
    setValue(DSL_POINT_OPENSTATUS, getDBINARYOpenValue(endPos), acceptedOperationTime);
    setValue(DSL_POINT_CLOSESTATUS, getDBINARYCloseValue(endPos), acceptedOperationTime);
    setValue(DSL_POINT_STATUS, endPos, acceptedOperationTime);

    /* Set LED value */
    switchDummyLED((operation.operation == SWITCH_OPERATION_OPEN)? LU_TRUE : LU_FALSE);

    if(operationParameters.interval > 0)
    {
        /* Start counting the post-operation: inhibit any other switch operation during a given interval. */
        postOpTimeOn = true;
        //Note that the DSLPostOpTimer job will be deleted by the scheduler
        postOpTimer = new DSLPostOpTimer(*this, operationParameters.interval);
        database.addJob(postOpTimer);
    }

    log.info("%s - Operation finished. End position %i", this->toString().c_str(), endPos);
    //Report Event
    event.value = EVENTSWITCH_VALUE_OK;
    eventLog->logEvent(event);

    return ret;
}


void DummySwitchLogic::endPostOpTime()
{
    postOpTimer = NULL;     //Unlink Timed Job (will be deleted by the Scheduler)
    postOpTimeOn = false;   //Grant next operation
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void DummySwitchLogic::switchDummyLED(lu_bool_t openIt)
{
    /* Update Dummy switch LED */
    MCMLED& dummyLED = MCMLED::getMCMLED(MCMLED::LED_ID_DUMMYSW);
    MCMLED::LED_STATUS result;
    //Please note that here, "!=" acts as a logical XOR
    result = ( (operationParameters.openColour == LED_COLOUR_GREEN) != (openIt == LU_TRUE) )?
                MCMLED::LED_STATUS_SOLID_1 : MCMLED::LED_STATUS_SOLID_2;
    if(dummyLED.setStatus(result) < 0)
    {
        log.error("%s: Error driving Dummy Switch LED", this->toString().c_str());
    }
    /* Write change to memory anyway */
    lu_uint8_t storedPosition;  //value is stored in reduced format
    storedPosition = static_cast<lu_uint8_t>(actualPosition);
    if(memoryManager.write(IMemoryManager::STORAGE_VAR_DUMMYSW, &storedPosition) == LU_FALSE)
    {
        log.error("%s: writing Dummy Switch position to NVRAM memory failed",
                    this->toString().c_str()
                    );
    }
}


/*
 *********************** End of file ******************************************
 */
