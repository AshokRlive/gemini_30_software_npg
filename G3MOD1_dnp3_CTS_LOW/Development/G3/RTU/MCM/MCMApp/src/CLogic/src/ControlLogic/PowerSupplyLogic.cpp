/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:PowerSupplyLogic.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief Power Supply Controller Logic
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   21 Mar 2018     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "PowerSupplyLogic.h"
#include "PowerSupplyCANChannel.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
PowerSupplyLogic::PowerSupplyLogic(Mutex* mutex,
                                    GeminiDatabase& g3database,
                                    PowerSupplyLogic::Config& conf
                                    ) :
                                        OperatableControlLogic(mutex, g3database, conf, CONTROL_LOGIC_TYPE_POWER_SUPPLY),
                                        m_config(conf),
                                        m_powerChannel(conf.channel),
                                        m_durationJob(NULL)
{}

PowerSupplyLogic::~PowerSupplyLogic()
{
    database.cancelJob(m_durationJob);
}


GDB_ERROR PowerSupplyLogic::init()
{
    GDB_ERROR ret = initCheck();
    if(m_powerChannel == NULL)
    {
        ret = GDB_ERROR_NOT_INITIALIZED;
    }
    if(ret == GDB_ERROR_NONE)
    {
        /* Initialise input points */
        ret = initInhibit();
    }
    if(ret == GDB_ERROR_NONE)
    {
        initialised = true;
    }
    return ret;
}


GDB_ERROR PowerSupplyLogic::getInputValue(lu_uint16_t inputPoint, PointData* data)
{
    return checkInputValue(POWER_SUPPLY_INPUT_OLR, POWER_SUPPLY_INPUT_INHIBIT, inputPoint, data);
}


GDB_ERROR PowerSupplyLogic::getOutputValue(PointData* data)
{
    if(data == NULL)
    {
        return GDB_ERROR_PARAM;
    }
    if(!initialised)
    {
        return GDB_ERROR_NOT_INITIALIZED;
    }
    /* Check online status */
    data->setOnlineFlag(m_powerChannel->isActive() == LU_TRUE);
    return GDB_ERROR_NONE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
GDB_ERROR PowerSupplyLogic::startOp(const SwitchLogicOperation& operation,
                                    const GDB_ERROR errorCode)
{
    if(errorCode != GDB_ERROR_NONE)
    {
        refuseOperationMsg(errorCode);
        return errorCode;
    }

    GDB_ERROR ret;

    if(m_powerChannel->isActive() == LU_FALSE)
    {
        log.info("%s - Error: output channel not available.", this->toString().c_str());
        return GDB_ERROR_INHIBIT;
    }

    //Store requested operation with final duration
    SwitchLogicOperation opParams;
    opParams = operation;
    opParams.duration = (operation.duration == 0)? m_config.duration_s : operation.duration;

    log.info("%s Requested %s (channel %d) - Duration: %i%s seconds",
                this->toString().c_str(),
                SWITCH_OPERATION_ToSTRING(operation.operation),
                m_config.channelRef.channelID,
                opParams.duration,
                (opParams.duration == 0)? "(latch)" : ""
              );

    /* Check preconditions */
    ret = preliminaryChecks(opParams);

    /* TODO: pueyos_a - add Power Supply event??? */
    //Report Event
//    CTEventCLogicStr event;
//    event.eventType = EVENT_TYPE_CLOGIC_;
//    event.cLogicType = (operation.operation == SWITCH_OPERATION_CLOSE)? EVENT_TYPE_CLOGICTYPE_START : EVENT_TYPE_CLOGICTYPE_STOP;
//    event.value = (ret == GDB_ERROR_NONE)? EVENTCLOGIC_VALUE_OK : //Request accepted
//                                            EVENTCLOGIC_VALUE_FAIL; //Request denied
//    eventLog->logEvent(event);

    if(ret != GDB_ERROR_NONE)
    {
        return ret;
    }

    if(operation.operation == SWITCH_OPERATION_OPEN)
    {
        /* Start "Disconnect" operation requested */
        database.cancelJob(m_durationJob);  //Cancel pending job if any to re-trigger
        ret = sendOperateCmds(opParams);    //Operate command

        if(opParams.duration != 0)
        {
            //Prepare cancel job for later:
            m_durationJob = new DurationJob(*this, opParams.duration);
            database.addJob(m_durationJob);
        }
    }
    else
    {
        /* Cancel "Disconnect" operation requested */
        database.cancelJob(m_durationJob);  //Cancel timer if any and and do it now
        ret = sendOperateCmds(opParams);  //Cancel command
    }
    return ret;
}


void PowerSupplyLogic::resumePower(const TimeManager::TimeStr& currentTime)
{
    LU_UNUSED(currentTime);

    /* Unlink Timed Job (will be deleted by the Scheduler) */
    m_durationJob = NULL;

    SwitchLogicOperation opParams;
    opParams.operation = SWITCH_OPERATION_CLOSE;    //Cancel command
    //Use current L/R status:
    OLR_STATE status;
    database.getLocalRemote(status);
    opParams.local = (status == OLR_STATE_LOCAL)? LU_TRUE : LU_FALSE;

    sendOperateCmds(opParams);  //Sends Cancel only
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
GDB_ERROR PowerSupplyLogic::preliminaryChecks(const SwitchLogicOperation& operation)
{
    GDB_ERROR ret = checkAll(operation.local);
    if(ret != GDB_ERROR_NONE)
    {
        log.error("%s - Error: %s - %s operation rejected (%s)",
                    this->toString().c_str(),
                    m_powerChannel->getName(),
                    SWITCH_OPERATION_ToSTRING(operation.operation),
                    GDB_ERROR_ToSTRING(ret)
                  );
        return ret;
    }
    if(m_powerChannel->isActive() == LU_FALSE)
    {
        log.error("%s - Error: channel not available.", this->toString().c_str());
        return GDB_ERROR_INHIBIT;
    }
    return ret;
}


GDB_ERROR PowerSupplyLogic::sendOperateCmds(const SwitchLogicOperation& operation)
{
    IOM_ERROR iomRet;
    GDB_ERROR ret = GDB_ERROR_NONE;

    log.info("%s start '%sconnect' operation.",
                    this->toString().c_str(),
                    (operation.operation == SWITCH_OPERATION_OPEN)? "dis" : "");

    PowerSupplyCANChannel* powerChannel = dynamic_cast<PowerSupplyCANChannel*>(m_powerChannel);
    if(operation.operation == SWITCH_OPERATION_OPEN)
    {
        /* Start "Disconnect" operation requested */

        /* Send Select & Operate to the configured channel */
        /* Warning: The Duration parameter is used by the MCM only since the
         *          duration on the PSM might be too short -- PSM uses 8-bit
         *          parameter for duration that yields to 255 sec (4.25 minutes)
         *          only. We do not use the PSM duration here for that reason.
         *          Duration parameter for PSM is then always 0.
         */
        iomRet = powerChannel->selOperate(0, operation.local);
        {
            if(iomRet != IOM_ERROR_NONE)
            {
                log.error("%s - Sending Operate command error: %s",
                            this->toString().c_str(), IOM_ERROR_ToSTRING(iomRet)
                          );
                ret = GDB_ERROR_CMD;
            }
            else
            {
                log.info("%s - Operation started.", this->toString().c_str());
            }
        }
    }
    else
    {
        /* Cancel "Disconnect" operation requested */

        /* Send Cancel to the configured channel */
        IOM_ERROR iomRet = powerChannel->cancel(operation.local);
        if(iomRet != IOM_ERROR_NONE)
        {
            ret = GDB_ERROR_CMD;
            if(iomRet == IOM_ERROR_CANCEL_ERROR)
            {
                log.error("%s - Sending Cancel command error: "
                            "operation %s might not be active - Error: %s",
                            this->toString().c_str(),
                            powerChannel->getName(),
                            IOM_ERROR_ToSTRING(iomRet)
                          );
            }
            else
            {
                log.error("%s - Sending Cancel command error: %s",
                            this->toString().c_str(), IOM_ERROR_ToSTRING(iomRet)
                          );
            }
        }
    }
    return ret;
}


/*
 *********************** End of file ******************************************
 */
