/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Equipment Fault Handler (alarm) implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   26/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "LogicGateControlLogic.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
LogicGateControlLogic::LogicGateControlLogic( Mutex* mutex                   ,
                                              GeminiDatabase& g3database     ,
                                              LogicGateControlLogic::Config& conf
                                            ) :
                              ControlLogic(mutex, g3database, conf, CONTROL_LOGIC_TYPE_LGT),
                              logic(conf.logic)
{
    //DBG_CTOR();

    /* Allocate Control Logic virtual points */
    clPoint.reserve(Config::ALLPOINT_NUM);  //Adjust vector size to do not waste memory
    for(lu_uint32_t id = 0; id < Config::BINARYPOINT_NUM; ++id)
    {
        clPoint.push_back(new ControlLogicDigitalPoint(mutex, database, conf.bPoint[id], POINT_TYPE_BINARY));
    }
}


LogicGateControlLogic::~LogicGateControlLogic()
{
    stopUpdate();

    /* Release digital input observers */
    MasterLockingMutex mLock(listMutex, LU_TRUE);   //Protect the observer list

    for (lu_uint32_t i = 0; i < dObservers.size(); ++i)
    {
        detachObserver(dObservers[i]);
    }

    //DBG_DTOR();
}


GDB_ERROR LogicGateControlLogic::init()
{
    GDB_ERROR ret = initCheck();
    if(ret == GDB_ERROR_NONE)
    {
        ret = activateCLPoints();   //Initialise Control Logic points as well
    }
    /* Check configuration: logic operation type (AND/OR/...) */
    if(logic >= LOGIC_GATE_OPERATE_LAST)
    {
        log.error("%s - Error: invalid logic op (%i) configuration", this->toString().c_str(), logic);
        ret = GDB_ERROR_CONFIG;
    }
    if(ret != GDB_ERROR_NONE)
    {
        return ret;
    }

    startCLPoints(true);
    initialised = true;

    /* Initialise input points */
    {//Input list mutex lock
        MasterLockingMutex mLock(listMutex, LU_TRUE);
        GDB_ERROR res = GDB_ERROR_NONE;
        for(lu_uint32_t i = 0; i < dObservers.size(); ++i)
        {
            /* Check input configuration */
            res = attachObserver(dObservers[i]);
            if(res != GDB_ERROR_NONE)
            {
                ret = res;  //Mandatory input not configured
            }
        }
        if(ret != GDB_ERROR_NONE)
        {
            //Detach all already attached and disable Control Logic indications
            initialised = false;
            for(lu_uint32_t i = 0; i < dObservers.size(); ++i)
            {
                detachObserver(dObservers[i]);
            }
            startCLPoints(false);
        }
    }
    return ret;
}


GDB_ERROR LogicGateControlLogic::getInputValue(lu_uint16_t inputPoint, PointData *data)
{
    GDB_ERROR ret = GDB_ERROR_PARAM;

    if(data == NULL)
    {
        return GDB_ERROR_PARAM;
    }

    // Protect the list
    MasterLockingMutex mLock(listMutex);

    try
    {
        PointIdStr id = dObservers.at(inputPoint)->getPointID();
        ret = database.getValue(id, data);
    }
    catch (const std::out_of_range& e)
    {
        ret = GDB_ERROR_NOPOINT;
    }
    return ret;
}


GDB_ERROR LogicGateControlLogic::addPoint(PointIdStr point)
{
    if(initialised)
        return GDB_ERROR_INITIALIZED;

    if(point.isValid())
    {
        MasterLockingMutex mLock(listMutex, LU_TRUE);
        dObservers.push_back(new DIObserver(point, *this));
        return GDB_ERROR_NONE;
    }
    log.error("%s Error adding point %i,%i to group %i.",
                this->toString().c_str(), point.group, point.ID, groupID);
    return GDB_ERROR_NOPOINT;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
void LogicGateControlLogic::newData(PointIdStr pointID, PointData& pointData)
{
    LU_UNUSED(pointData);

    if(!enabled)
        return;

    //An observed point has changed: check all and update output
    MasterLockingMutex mLock(listMutex);
    lu_uint8_t result = 0;

    /* TODO: pueyos_a - [LOGIC_GATE] check online/offline status of inputs */
    PointFlags outFlags(PointFlags::VALID);
    outFlags.setOnline(true);

    for (lu_uint32_t i = 0; i < dObservers.size(); ++i)
    {
#if LOGIC_GATE_FLAGS
        PointFlags ptFlags = dObservers[i]->getFlags();
        if(ptFlags.isInvalid() || ptFlags.isInitial()
            //|| (!ptFlags.isOnline())  /* TODO: pueyos_a -  What to do if a Logic Gate input is offline? */
            )
        {
            outFlags.setInvalid(true);
        }
#endif
        if(i==0)
        {
            result = dObservers[i]->getValue();   //get 1st value to compare against
        }
        else
        {
            switch(logic)
            {
                case LOGIC_GATE_OPERATE_OR:
                    result |= dObservers[i]->getValue();
                    break;
                case LOGIC_GATE_OPERATE_AND:
                    result &= dObservers[i]->getValue();
                    break;
                default:
                    log.error("%s - Unsupported logic %i", this->toString().c_str(), logic);
                    return;
                    break;
            }
        }
    }
    log.debug("%s: incoming change to %d %s from %s point. New value: %d %s",
                    this->toString().c_str(),
                    (lu_uint8_t)pointData, pointData.getFlags().toString().c_str(),
                    pointID.toString().c_str(),
                    result, outFlags.toString().c_str());
    //update output
    TimeManager::TimeStr timestamp = timeManager.now();
    setValue(LOGIC_GATE_POINT_OUTPUT, result, timestamp, outFlags);
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
