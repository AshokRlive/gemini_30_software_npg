/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_5EB93F3A_75D4_476c_92CA_46FFA5B65563__INCLUDED_)
#define EA_5EB93F3A_75D4_476c_92CA_46FFA5B65563__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <map>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "Mutex.h"
#include "ObserveableObj.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Handle the switch authority of the system
 * 
 * This is a Monostate Class that keeps the same state for all instances.
 * The switch authority status is saved in a local "static" variable. Access to
 * this variable is guarded by a local static mutex.
 */
class SwitchAuthority : public SubjectObj<void>
{
public:
	/**
	 * Switch types
	 */
	typedef enum
	{
	    SWTYPE_SWITCH,
	    SWTYPE_CIRCUITBREAKER_TRIP,
	    SWTYPE_UPPER_AUTHORITY,
	    SWTYPE_LAST
	} SWTYPE;

    /**
     * Acquisition functions (for Upper Authorities)
     */
    typedef enum
    {
        ACQ_FUNC_GET,        //Get authority if free refuse if not free
        ACQ_FUNC_RESERVE,    //Get authority if free, reserve authority if not free
        ACQ_FUNC_LAST
    } ACQ_FUNC;

public:
    SwitchAuthority() {};
    virtual ~SwitchAuthority() {};

	/**
	 * \brief Try to acquire the switch authority
	 * 
	 * Only one switch at a time (of each type) can be active. This function is
	 * used in order to acquire the switch authority. If the switch authority is
	 * available it's locked and assigned to the current switch. If a switch is
	 * already operating the request is rejected and the function returns an error.
	 * 
     * \param type Type of switch authority
	 * \param switchID ID of the switch that tries to acquire the authority
	 * 
	 * \return Error code: LU_FALSE when the authority is (or already was) acquired.
	 */
	lu_bool_t acquire(const SwitchAuthority::SWTYPE type, const lu_uint16_t switchID);

    /**
     * \brief Grant other type of switch to operate
     *
     * After successfully acquired the the switch authority, the owner can grant
     * another type of owner to operate meanwhile it keeps its own type's switch
     * authority.
     *
     * If the caller doesn't own the switch authority an error is returned.
     *
     * \param type Type of switch authority
     * \param switchID ID of the owner of the authority that grants others.
     *
     * \return Error code: LU_TRUE when authority has been successfully granted.
     */
    lu_bool_t grant(const SwitchAuthority::SWTYPE type, const lu_uint16_t switchID);

	/**
	 * \brief This function releases the switch authority previously acquired.
	 * 
	 * If the caller doesn't own the switch authority an error is returned.
	 * 
	 * \example printf("%s", SwitchAuthority::toStringC(myAuthority));
	 *
     * \param type Type of switch authority
	 * \param switchID ID of the switch that relinquishes the authority.
	 * 
	 * \return Error code: LU_TRUE when the authority is successfully released.
	 */
	lu_bool_t relinquish(const SwitchAuthority::SWTYPE type, const lu_uint16_t switchID);

	/**
     * \brief Return a C string representing values for Authority types (SWTYPE)
     *
     * \param authType Type of authority
     *
     * \return C string corresponding to the given type of authority
     */
	static const lu_char_t* toStringC(const SwitchAuthority::SWTYPE authType)
	{
	    switch(authType)
	    {
	        case SWTYPE_SWITCH:
	            return "SWITCH";
	            break;
            case SWTYPE_CIRCUITBREAKER_TRIP:
                return "CB TRIP";
                break;
            case SWTYPE_UPPER_AUTHORITY:
                return "UPPER AUTH";
                break;
            default:
                return "INVALID";
                break;
	    }
	}

private:
	/**
     * \brief Check if the authority is granted in all the types.
     *
     * \return True if authority is granted.
     */
    bool isGranted();

    /**
     * \brief Check if no authority is reserved in all the types.
     *
     * \return True if no owner has any authority.
     */
    bool isFree();

    /**
     * \brief Check if Upper authority locked while no switches and CB reserved.
     *
     * \return True if no switch/CB has any authority while upper authority locked it.
     */
    bool isLockedFree();

private:
    typedef enum
    {
        STATE_ACQUIRED, //Authority unavailable
        STATE_GRANTED   //Authority for others is granted, but token is not released yet
    } STATE;

    typedef std::map<lu_uint16_t, SwitchAuthority::STATE> State;

private:
    /* Mutex access lock that guards access to the switch authority status.
     * The object uses 1 lock only since one Type might need to check the
     * authority status of other types.
     */
    static MasterMutex accessLock;

    /* The object keeps 1 authority status per Type */
    /* Storage of ongoing authority acquisitions
     * Being SWTYPE type        the switch operation type
     * and   lu_uint16_t ID     the owner ID
     * Access using:
     *      STATE x = authority[type][ID];
     *      State s = authority[type];
     *      STATE x = s.second;
     *      owner = s.first;
     */
    static State authority[SwitchAuthority::SWTYPE_LAST];   //Vector of types containing maps of IDs
};


#endif // !defined(EA_5EB93F3A_75D4_476c_92CA_46FFA5B65563__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
