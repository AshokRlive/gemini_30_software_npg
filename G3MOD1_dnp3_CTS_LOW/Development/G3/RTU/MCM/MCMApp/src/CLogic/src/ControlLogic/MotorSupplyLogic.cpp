/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MotorSupplyLogic.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   12 Apr 2018     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "MotorSupplyLogic.h"
#include "PowerSupplyCANChannel.h"
#include "SwitchCANChannel.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
MotorSupplyLogic::MotorSupplyLogic(Mutex* mutex,
                                    GeminiDatabase& g3database,
                                    MotorSupplyLogic::Config& conf
                                    ) :
                                        OperatableControlLogic(mutex, g3database, conf, CONTROL_LOGIC_TYPE_MOTOR_SUPPLY),
                                        m_config(conf),
                                        m_durationJob(NULL)
{
    m_control = new DIObserver(m_config.controlInput, *this);
}

MotorSupplyLogic::~MotorSupplyLogic()
{
    detachObserver(m_control);
    database.cancelJob(m_durationJob);
}


GDB_ERROR MotorSupplyLogic::init()
{
    GDB_ERROR ret = initCheck();
    if( (m_config.motorChannel == NULL) || (m_config.switchChannel == NULL) )
    {
        ret = GDB_ERROR_NOT_INITIALIZED;
    }
    if(ret == GDB_ERROR_NONE)
    {
        initialised = true;

        /* Initialise input points */
        ret = initInhibit();
        if(ret == GDB_ERROR_NONE)
        {
            attachObserver(m_control);  //Optional input: ignores outcome
        }
        if(ret != GDB_ERROR_NONE)
        {
            //Detach all already attached and disable Control Logic indications
            initialised = false;
            startCLPoints(false);
        }
    }
    return ret;
}


GDB_ERROR MotorSupplyLogic::getInputValue(lu_uint16_t inputPoint, PointData* data)
{
    GDB_ERROR ret = checkInputValue(MOTOR_SUPPLY_INPUT_OLR, MOTOR_SUPPLY_INPUT_INHIBIT, inputPoint, data);
    if(ret == GDB_ERROR_NOPOINT)
    {
        //Check Control Logic's specific points
        if(inputPoint >= MOTOR_SUPPLY_INPUT_LAST)
        {
            return GDB_ERROR_PARAM;
        }
        if( (inputPoint == MOTOR_SUPPLY_INPUT_CONTROL) && (m_control != NULL) )
        {
            return database.getValue(m_control->getPointID(), data);
        }
    }
    return GDB_ERROR_NOPOINT;
}


GDB_ERROR MotorSupplyLogic::getOutputValue(PointData* data)
{
    if(data == NULL)
    {
        return GDB_ERROR_PARAM;
    }
    if(!initialised)
    {
        return GDB_ERROR_NOT_INITIALIZED;
    }
    /* Check online status */
    data->setOnlineFlag(isAllChannelsActive());
    return GDB_ERROR_NONE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
GDB_ERROR MotorSupplyLogic::startOp(const SwitchLogicOperation& operation,
                                    const GDB_ERROR errorCode)
{
    if(errorCode != GDB_ERROR_NONE)
    {
        refuseOperationMsg(errorCode);
        return errorCode;
    }

    GDB_ERROR ret;

    if(!isAllChannelsActive())
    {
        log.info("%s - Error: output channel not available.", this->toString().c_str());
        return GDB_ERROR_INHIBIT;
    }

    //Store requested operation with final duration
    SwitchLogicOperation opParams;
    opParams = operation;
    opParams.duration = (operation.duration == 0)? m_config.duration_s : operation.duration;

    log.info("%s Requested %s - Duration: %i%s seconds",
                this->toString().c_str(),
                SWITCH_OPERATION_ToSTRING(operation.operation),
                opParams.duration,
                (opParams.duration == 0)? "(latch)" : ""
              );

    /* Check preconditions */
    ret = preliminaryChecks(opParams);

    if(ret != GDB_ERROR_NONE)
    {
        return ret;
    }

    if(operation.operation == SWITCH_OPERATION_CLOSE)
    {
        /* Start Motor Supply requested */
        database.cancelJob(m_durationJob);  //Cancel timer if any and and do it now
        ret = sendOperateCmds(opParams);    //Operate command
        if(opParams.duration != 0)
        {
            //Prepare cancel job for later:
            m_durationJob = new DurationJob(*this, opParams.duration);
            database.addJob(m_durationJob);
        }
    }
    else
    {
        /* Stop Motor Supply requested */
        database.cancelJob(m_durationJob);  //Cancel pending job if any to re-trigger
        ret = sendOperateCmds(opParams);    //Cancel command
    }

    /* TODO: pueyos_a - add Motor Supply event??? */
    //Report Event
//    CTEventCLogicStr event;
//    event.eventType = EVENT_TYPE_CLOGIC_;
//    event.cLogicType = (operation.operation == SWITCH_OPERATION_CLOSE)? EVENT_TYPE_CLOGICTYPE_START : EVENT_TYPE_CLOGICTYPE_STOP;
//    event.value = (ret == GDB_ERROR_NONE)? EVENTCLOGIC_VALUE_OK : //Request accepted
//                                            EVENTCLOGIC_VALUE_FAIL; //Request denied
//    eventLog->logEvent(event);

    return ret;
}


void MotorSupplyLogic::newData(PointIdStr pointID, PointData& pointData)
{
    log.debug("%s - newData arrived: (%s) v=%d [%s]",
                this->toString().c_str(), pointID.toString().c_str(),
                (lu_uint32_t)(*(PointDataUint8*)&pointData),
                pointData.getFlags().toString().c_str());
    if(!enabled)
    {
        return;
    }
    if(pointID == m_control->getPointID())
    {
        SwitchLogicOperation op;
        //Check change type
        switch (pointData.getEdge())
        {
            case EDGE_TYPE_NEGATIVE:
                op.operation = SWITCH_OPERATION_OPEN;
                break;
            case EDGE_TYPE_POSITIVE:
                op.operation = SWITCH_OPERATION_CLOSE;
                break;
            default:
                return; //No change, do nothing
                break;
        }
        /* A signal rise/fall triggers operation */
        log.info("%s - operation %s requested from Control input %s",
                    this->toString().c_str(),
                    SWITCH_OPERATION_ToSTRING(op.operation),
                    pointID.toString().c_str());

        //Internal Operation does not depend on Local/Remote: use current
        OLR_STATE olr;
        database.getLocalRemote(olr);
        op.local = (olr == OLR_STATE_REMOTE)? LU_FALSE : LU_TRUE;
        startOperation(op);
    }
    else
    {
        OperatableControlLogic::newData(pointID, pointData);
    }
}


void MotorSupplyLogic::endSupply(const TimeManager::TimeStr& currentTime)
{
    LU_UNUSED(currentTime);

    /* Unlink Timed Job (will be deleted by the Scheduler) */
    m_durationJob = NULL;

    SwitchLogicOperation opParams;
    opParams.operation = SWITCH_OPERATION_OPEN;    //Cancel command
    //Use current L/R status:
    OLR_STATE status;
    database.getLocalRemote(status);
    opParams.local = (status == OLR_STATE_LOCAL)? LU_TRUE : LU_FALSE;

    sendOperateCmds(opParams);  //Sends Cancel only
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
GDB_ERROR MotorSupplyLogic::preliminaryChecks(const SwitchLogicOperation& operation)
{
    GDB_ERROR ret = checkAll(operation.local);
    if(ret != GDB_ERROR_NONE)
    {
        log.error("%s - Error: %s operation rejected (%s)",
                    this->toString().c_str(),
                    SWITCH_OPERATION_ToSTRING(operation.operation),
                    GDB_ERROR_ToSTRING(ret)
                  );
        return ret;
    }
    if(!isAllChannelsActive())
    {
        log.error("%s - Error: channel not available.", this->toString().c_str());
        return GDB_ERROR_INHIBIT;
    }
    return ret;
}


GDB_ERROR MotorSupplyLogic::sendOperateCmds(const SwitchLogicOperation& operation)
{
    IOM_ERROR iomRet;
    GDB_ERROR ret = GDB_ERROR_NONE;

    log.info("%s %s Motor Supply.",
                    this->toString().c_str(),
                    (operation.operation == SWITCH_OPERATION_OPEN)? CANChannel::Cancel_CString : CANChannel::Operate_CString);

    PowerSupplyCANChannel* mSupplyChannel = dynamic_cast<PowerSupplyCANChannel*>(m_config.motorChannel);
    SwitchCANChannel* mSwitchChannel = dynamic_cast<SwitchCANChannel*>(m_config.switchChannel);
    if(operation.operation == SWITCH_OPERATION_CLOSE)
    {
        /* Start operation requested */

        /* First send command to PSM */
        iomRet = mSupplyChannel->selOperate(groupID, operation.local, operation.duration);
        if(iomRet == IOM_ERROR_NONE)
        {
            /* Now send command to Switch module */
            //NOTE: duration sent to Switch Module is 0 since it is dealt on MCM
            //      due to DSM not supporting 'duration' yet
            iomRet = mSwitchChannel->selOperateMSupply(groupID, operation.local);
            if(iomRet != IOM_ERROR_NONE)
            {
                //Switch module command failed, cancel previous successful operation
                iomRet = mSupplyChannel->cancel(operation.local);
            }
        }
        if(iomRet != IOM_ERROR_NONE)
        {
            log.error("%s - Sending %s command error: %s",
                        this->toString().c_str(), CANChannel::Operate_CString,
                        IOM_ERROR_ToSTRING(iomRet)
                      );
            ret = GDB_ERROR_CMD;
        }
        else
        {
            std::ostringstream ss;
            ss << this->toString() << " - Operation started";
            if(m_config.duration_s != 0)
            {
                ss << " for " << (lu_uint32_t)m_config.duration_s << " seconds";
            }
            log.info(ss.str().c_str());
        }
    }
    else
    {
        /* Cancel operation requested */

        //First send command to Switch module
        iomRet = mSwitchChannel->cancelMSupply(groupID, operation.local);
        if(iomRet != IOM_ERROR_NONE)
        {
            log.error("%s - Sending Switch Supply %s command error: %s",
                        this->toString().c_str(), CANChannel::Cancel_CString,
                        IOM_ERROR_ToSTRING(iomRet)
                      );
        }
        //Now send command to PSM
        iomRet = mSupplyChannel->cancel(groupID, operation.local);
        if(iomRet != IOM_ERROR_NONE)
        {
            log.error("%s - Sending Main Supply %s command error: %s",
                        this->toString().c_str(), CANChannel::Cancel_CString,
                        IOM_ERROR_ToSTRING(iomRet)
                      );
        }
    }
    return ret;
}


bool MotorSupplyLogic::isAllChannelsActive()
{
    bool onlineFlag = false;
    if( (m_config.motorChannel->isActive() == LU_TRUE) &&
        (m_config.switchChannel->isActive() == LU_TRUE)
        )
    {
        onlineFlag = true;
    }
    return onlineFlag;
}


/*
 *********************** End of file ******************************************
 */
