/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:PowerSupplyLogic.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief Power Supply Controller Logic
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   21 Mar 2018     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef POWERSUPPLYLOGIC_H_
#define POWERSUPPLYLOGIC_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IChannel.h"
#include "OperatableControlLogic.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * Class PowerSupplyLogic
 *
 * This class controls the behaviour of the Power Supply Controller channels
 */
class PowerSupplyLogic : public OperatableControlLogic
{
public:
    /**
     * \brief Power Supply Controller logic configuration
     */
    struct Config : public OperatableControlLogic::Config
    {
    public:
        IChannel* channel;      /* Reference to the output channel */
        ChannelRef channelRef;  /* Channel ID (for event report only) */
        lu_uint32_t duration_s; /* Duration of the operation -- 0 for latched */
    };

public:
    /**
     * \brief Custom constructor
     *
     * \param mutex Reference to the G3DB common mutex
     * \param database Reference to the Gemini database
     * \param config reference to the Control Logic's configuration block
     */
    PowerSupplyLogic(Mutex *mutex                 ,
                    GeminiDatabase& database     ,
                    PowerSupplyLogic::Config &conf
                    );
    virtual ~PowerSupplyLogic();

    /* == inherited from ControlLogic == */
    virtual GDB_ERROR init();
    virtual GDB_ERROR getInputValue(lu_uint16_t inputPoint, PointData *data);
    virtual GDB_ERROR getOutputValue(PointData* data);

protected:
    /**
     * \brief Timed Job for cancelling disconnection
     */
    class DurationJob : public TimedJob
    {
    public:
        /**
         * \brief Custom constructor
         *
         * \param CLogic Reference to the Control Logic object.
         * \param timerInterval_s Period time, expressed in seconds
         */
        DurationJob(PowerSupplyLogic& CLogic, const lu_uint32_t timerInterval_s) :
                                            TimedJob(timerInterval_s * 1000, 0),
                                            cLogic(CLogic)
        {};
        virtual ~DurationJob() {};

    protected:
        /* == Inherited from TimedJob == */
        virtual void job(TimeManager::TimeStr* timerPtr)
        {
            cLogic.resumePower(*timerPtr);
        }

    private:
        PowerSupplyLogic& cLogic;
    };

protected:
    /* == Implements mandatory OperatableControlLogic == */
    virtual GDB_ERROR startOp(const SwitchLogicOperation &operation, const GDB_ERROR errorCode);

    /**
     * \brief Resume power supply
     *
     * \param currentTime Time of occurrence of the resuming
     */
    void resumePower(const TimeManager::TimeStr& currentTime);

private:
    /**
     * \brief Sends CAN commands to the Module
     *
     * \param operation Parameters of the operation
     *
     * \return Error code.
     */
    GDB_ERROR sendOperateCmds(const SwitchLogicOperation& operation);

    /**
     * \brief Check common preliminary conditions that can inhibit the operation
     *
     * \param operation Parameters of the operation
     *
     * \return Error code. GDB_ERROR_NONE when all preliminary checks passed.
     */
    GDB_ERROR preliminaryChecks(const SwitchLogicOperation& operation);

private:
    PowerSupplyLogic::Config m_config;  //Control Logic confg
    IChannel* m_powerChannel;           //Channel to operate
    DurationJob* m_durationJob;         //Job for cancelling disconnection
};


#endif /* POWERSUPPLYLOGIC_H_ */

/*
 *********************** End of file ******************************************
 */
