/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       SwitchGear Control Logic.
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_CE2C4EFA_E9DB_4922_8D57_478DE84C6292__INCLUDED_)
#define EA_CE2C4EFA_E9DB_4922_8D57_478DE84C6292__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IChannel.h"
#include "OperatableControlLogic.h"
#include "ControlLogicDigitalPoint.h"
#include "ControlLogicAnaloguePoint.h"
#include "SwitchAuthority.h"
#include "PowerSupplyCANChannel.h"
#include "IIOModuleManager.h"

//forward declarations

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/**
 * Macro for common definitions of SwitchGear Logic Timed Jobs
 *
 * \param name Name of the class to be created
 * \param callbackName Name of the SwitchGear Logic method to be called back
 */
#define SGL_CREATE_TIMEDJOB(name, callbackName)                                         \
    class name : public TimedJob                                                        \
    {                                                                                   \
    public:                                                                             \
        /**                                                                             \
         * \brief Custom constructor                                                    \
         *                                                                              \
         * \param cLogic Reference to the control logic to report to.                   \
         * \param Timeout timer value in ms.                                            \
         */                                                                             \
        name(SwitchGearLogic& cLogic, const lu_uint32_t timeout):                       \
                                                                TimedJob(timeout, 0),   \
                                                                cLogic(cLogic)          \
        {};                                                                             \
        virtual ~name() {};                                                             \
                                                                                        \
    protected:                                                                          \
        /* == Inherited from TimedJob == */                                             \
        virtual void job(TimeManager::TimeStr *timePtr)                                 \
        {                                                                               \
            LU_UNUSED(timePtr);                                                         \
            cLogic.callbackName();                                                      \
        };                                                                              \
    private:                                                                            \
        SwitchGearLogic& cLogic;                                                        \
    }


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Switch Gear control Logic Block
 * 
 * The control logic is split in three different stages: wait command, operate and
 * operation completed.
 * 
 * The first stage is executed as part of the command request. If all the checks
 * are passed the request is accepted and the function return with an error code.
 * The next stage of the logic is executed in a background task (as a database
 * job) because for each command sent to a slave board we have to wait for a reply
 * (the functions "blocks"). If the activation sequence is correctly executed the
 * logic proceeds to the next step. In the third and last stage we wait for the
 * operation completion, an error form a slave board or a timeout. If any of these
 * events occur the control logic status is updated and the logic goes back to the
 * initial stage.
 *
 * TIMINGS:
 * |    op. delay  | pre-op delay  | Operation time |  Overrun time     |   Safety threshold |
 *                                                  |  post-op time  |
 *
 * |               |               |                |                   |                    |
 * [start    ]     [Motor    ]     [Motor   ]       [Motor    ]         [Motor          ]    [Motor supply     ]
 * [operation]     [supply on]     [relay on]       [relay off]         [supply off     ]    [forced to off    ]
 *                 [(PSM)    ]     [(SCM)   ]       [(SCM)    ]         [(Cancel command]    [(by PSM if Cancel]
 *                                                                      [sent to PSM)   ]    [not received)    ]
 *
 * |               |                                |                   |
 * [Send CAN  ]    [Send CAN  ]                     [Send Synch]        [Send Cancel]
 * [msg to SCM]    [msg to PSM]                     [msg to SCM]        [msg to PSM ]
 *
 * |                                                                 |
 * [Inhibit other       ]                                            [Grant other]
 * [operations except CB]                                            [operations ]
 */
class SwitchGearLogic : public OperatableControlLogic
{
public:
    /**
     * \brief Operation-specific configuration parameters
     */
    struct SGLOperationCfg
    {
    public:
    public:
        lu_uint32_t timeoutOpenS;           //Open Operation timeout in seconds
        lu_uint32_t timeoutCloseS;          //Close Operation timeout in seconds
        lu_uint32_t overrunOpenMs;          //Overrun timer for open operation
        lu_uint32_t overrunCloseMs;         //Overrun timer for close operation
        lu_uint32_t currentThreshold;       //Motor Current Threshold in Amps
        lu_uint32_t motorOvercurrentTime;   //Motor OverCurrent Measurement Delay in milliseconds
        lu_uint32_t preOperationTime_ms;    //Pre-operation Time, between Motor on and switch start
        lu_uint32_t postOperationTime_ms;   //Post-operation Time before allowing another operation
        lu_uint32_t delayedOperationLocal;  //Delayed (local) operation in seconds
        lu_uint32_t delayedOperationRemote; //Delayed (remote) operation in seconds
        IOModuleIDStr moduleSwitch;         //Associated module (for event report only)
        lu_uint8_t switchID;                //Associated module's switch ID
        bool checkPosition;                 //Do check switch position before operation
        lu_bool_t concurrent;               //Allow Circuit Breaker & SwitchGear concurrent operations
        lu_bool_t isCBreaker;               //Tells it is a Circuit Breaker instead of a SwitchGear
        lu_bool_t openWhenEarthed;          //Shows status as open when Earthed

        SGLOperationCfg() :
                                timeoutOpenS(0),
                                timeoutCloseS(0),
                                overrunOpenMs(0),
                                overrunCloseMs(0),
                                currentThreshold(0),
                                motorOvercurrentTime(0),
                                preOperationTime_ms(0),
                                postOperationTime_ms(0),
                                delayedOperationLocal(0),
                                delayedOperationRemote(0),
                                switchID(0),
                                checkPosition(true),
                                concurrent(LU_TRUE),
                                isCBreaker(LU_FALSE),
                                openWhenEarthed(LU_FALSE)
        {};
    };

    /**
     * \brief Configuration block for SwitchGear Control Logic
     */
    struct Config : public OperatableControlLogic::Config
    {
    public:
        /* Amounts of different types of Control Logic's Pseudo Points */
        static const lu_uint16_t BINARYPOINT_NUM = SGL_POINT_TYPE_BINARY_COUNT;
        static const lu_uint16_t DBINARYPOINT_NUM = SGL_POINT_TYPE_DOUBLEBINARY_COUNT;
        static const lu_uint16_t ANALOGUEPOINT_NUM = SGL_POINT_TYPE_ANALOGUE_COUNT;
        static const lu_uint32_t ALLPOINT_NUM = SGL_POINT_EnumSize;
    public:
        /* input channels indexing */
        enum INCHANNELIDX
        {
            INCHANNELIDX_OVERCURRENT,   //Overcurrent indication channel
            INCHANNELIDX_MOTORCURRENT,  //Motor current measurement channel
            INCHANNELIDX_LAST
        };

        /* output channels indexing */
        enum OUTCHANNELIDX
        {
            OUTCHANNELIDX_MSUPPLY = 0,   //Motor Supply channel
            OUTCHANNELIDX_OPEN,          //Open switch channel
            OUTCHANNELIDX_CLOSE,         //Close switch channel
            OUTCHANNELIDX_LAST
        };

    public:
        PointIdStr dInput[SGL_INPUT_LAST];              //Input Digital virtual point list
        DigitalPoint::Config bPoint[BINARYPOINT_NUM];   //single binary CLogic virtual points
        DigitalPoint::Config dbPoint[DBINARYPOINT_NUM]; //double binary CLogic virtual points
        AnaloguePoint::Config aPoint[ANALOGUEPOINT_NUM];//analogue CLogic virtual points
        IChannel *inChannel[INCHANNELIDX_LAST];         //Reference to the input channels
        IChannel *outChannel[OUTCHANNELIDX_LAST];       //Reference to the output channels
        SGLOperationCfg operationCfg;                   //Operation parameters
    };

public:
    /**
     * \brief Custom constructor
     *
     * \param mutex Reference to the G3DB common mutex
     * \param database Reference to the Gemini database
     * \param config reference to the Control Logic's configuration block
     */
    SwitchGearLogic(Mutex* mutex, GeminiDatabase& database, SwitchGearLogic::Config& config, IIOModuleManager&   moduleManager);
    virtual ~SwitchGearLogic();

    /* == inherited from ControlLogic == */
    virtual GDB_ERROR init();
    virtual GDB_ERROR getInputValue(lu_uint16_t inputPoint, PointData *data);
    virtual GDB_ERROR getOutputValue(PointData* data);

protected:
    /* == Implements mandatory OperatableControlLogic == */
    virtual GDB_ERROR startOp(const SwitchLogicOperation &operation, const GDB_ERROR errorCode);

    /* Incoming events-related methods */
    GDB_ERROR newData();
    void newChannelData(IChannel *subject);
    /* == Inherited from ControlLogic == */
    void newData(PointIdStr pointID, PointData& pointData);

    /* G3DB Job-related methods */
    GDB_ERROR sendOperateCmds();
    GDB_ERROR operationTimedOut();
    void overrunFinished();
    void stopSWOperationLoop();
    void stopSWOperationTimeout();
    void grantOperation();

private:
    /**
     * \brief cancel, finish or check if operation is finished
     *
     * \param currentTime
     * \param error
     */
    void finishOperation(TimeManager::TimeStr &currentTime, lu_bool_t error);

    /**
     * \brief Send cancel of the Motor Supply operation in order to finish it
     *
     * \param currentTime
     * \param error
     */
    void stopMSOperation(lu_bool_t error);
    void stopMSOperation(TimeManager::TimeStr &currentTime, lu_bool_t error);

    /**
     * \brief Check DI points observer for inhibition
     *
     * If a DI point observer is present, checks its value and status to see if
     * it should trigger an inhibition. If it is not configured, does not
     * triggers any inhibition.
     *
     * \param digitalInput Digital input index of the input to be checked
     * \param checkValue Value to check against.
     *
     * \return Inhibition of the point. Gives LU_TRUE if the point matches the
     * given value or is not online.
     */
    inline lu_bool_t checkDIObserver(SGL_INPUT digitalInput, lu_bool_t checkValue);

    /**
     * \brief Get the DI value reported from observer
     *
     * If a DI point observer is present, get its value. Otherwise returns 0.
     *
     * \param digitalInput Digital input index of the input to be checked
     *
     * \return value of the DI point. Value 0 when observer not present.
     */
    inline lu_uint8_t getDIObserverValue(const SGL_INPUT digitalInput);

    /**
     * \brief Get the DI point flags string reported from observer
     *
     * If a DI point observer is present, get its flags string.
     *
     * \param digitalInput Digital input index of the input to be checked
     *
     * \return Point flags string. Predefined one if no point found.
     */
    inline const lu_char_t* getDIObserverFlags(const SGL_INPUT digitalInput);

    /**
     * \brief Reset the SwitchGear Logic's error-indication pseudo points (flags)
     *
     * \param currentTime Time stamp to be used for the pseudo points (flags)
     */
    void resetCLogicIndications(const TimeManager::TimeStr& timeStamp);

    /**
     * \brief Update Status, Open, and Close Control Logic pseudo points
     *
     * \param status Double Binary value with the status. Must follow DBINARY_STATUS values.
     */
    void updateOpenCloseStatus(const lu_uint8_t status, const TimeManager::TimeStr& timestamp);

    /**
     * \brief Check common preliminary conditions that can inhibit the operation
     *
     * \return Error code. GDB_ERROR_NONE when all preliminary checks passed.
     */
    GDB_ERROR preliminaryChecks();

    /**
     * \brief Finish SwitchGear operation
     *
     * This updates final flags and sets the SwitchGear available for a new operation
     */
    void operationDone();

    void OperationEnd();

    /**
     * \brief Check SwitchGear status to set some pseudo points offline/online
     *
     * This is intended to set online or offline some specific Control Logic
     * pseudo points in order to reflect the non-operatable status of the
     * Switch Gear.
     */
    void checkSwitchQuality();

    /**
     * \brief Send Motor supply start command
     *
     * This is intended to be sent to the PSM motor supply channel in order to
     * start the motor supply.
     *
     * \return Error code.
     */
    IOM_ERROR sendMotorSupplyCommand();
	
	IOM_ERROR sendG3ModSwitchCommand();

    /**
     * \brief Send Switch command to the module
     *
     * This is intended to be sent to the SCM/DSM Switch channel in order to
     * start the switch operation.
     *
     * \param delay Operation delay, in milliseconds (not including pre-operation time)
     *
     * \return Error code.
     */
    IOM_ERROR sendModuleSwitchCommand(const lu_uint32_t delay);


    /**
     * \brief Handle error codes in order to set the fault indications
     *
     * \param gdbError Overall error code
     * \param iomError Module-related error code
     * \param timeStamp Time stamp to be used for the fault indication update.
     */
    void handleFault(const GDB_ERROR gdbError,
                     const IOM_ERROR iomError,
                     const TimeManager::TimeStr timeStamp
                     );

    /**
     * \brief Check Battery's Health
     *
     * This is intended to check Battery signals for PSM : Battery Disconnected, Battery low, Battery Fault.
     * If any of these signals are found then battery is unhealthy and we would not be able to
     * carry out Switch operation successfully
     *
     * \return Error code.
     */
    IOM_ERROR checkBatteryHealth();

private:
    /* Inner classes */

    /**
     * \brief Job in charge of sending Select+Operate commands to the involved channels
     */
    class SGLSendCmdsJob : public IJob
    {
    public:
        SGLSendCmdsJob(SwitchGearLogic &cLogic): cLogic(cLogic) {};
        virtual ~SGLSendCmdsJob() {};

        /**
         * \brief Method executed by the job scheduler
         */
        virtual void run();

    private:
        SwitchGearLogic& cLogic;
    };

    /**
     * \brief Job in charge of sending the motor supply start command
     */
    SGL_CREATE_TIMEDJOB(SGLG3ModSwitchJob, sendG3ModSwitchCommand);

    /**
     * \brief Job in charge of signal when the operation timed out
     */
    SGL_CREATE_TIMEDJOB(SGLOpTimeoutTJob, operationTimedOut);

    /**
     * \brief Job in charge of monitoring the overrun time
     */
    SGL_CREATE_TIMEDJOB(SGLOverrunTJob, overrunFinished);

    /**
     * \brief Job in charge of monitoring the post-operation time to disallow other operation
     */
    SGL_CREATE_TIMEDJOB(SGLPostOpTJob, grantOperation);

    /**
     * \brief Job in charge of sending one cancel message try
     */
    SGL_CREATE_TIMEDJOB(SGLCancelTJob, stopSWOperationLoop);

    /**
     * \brief Job in charge of checking if all the cancel retries are timed out
     */
    SGL_CREATE_TIMEDJOB(SGLCancelMaxTJob, stopSWOperationTimeout);

    /**
     * \brief Digital virtual points observer
     */
    class SGLChObserver : public ChannelObserver
    {
    public:
        SGLChObserver(IChannel& subject, SwitchGearLogic& cLogic);
        virtual ~SGLChObserver();

        /* == Inherited from ChannelObserver */
        virtual void update(IChannel *subject);

    private:
        IChannel& channel;
        SwitchGearLogic& cLogic;
    };

private:
    /**
     * \brief Set a Control Logic's binary indication (pseudo-point)
     *
     * \param point Index of the point to be set.
     * \param value Value to be set for that point
     * \param timestamp Time stamp to be used
     * \param forced Value is forced to be updated (eventing) with the new timestamp
     */
    void setPseudoBinValue(const SGL_POINT point, const bool value, const TimeManager::TimeStr timestamp, lu_bool_t forced = LU_FALSE)
    {
        clLocalPoint[point] = value;
        setValue(point, value, timestamp, ValidFlags, forced);
    }

    /**
     * \brief Start the tasks after the switch operation has finished
     *
     * The switch operation finishes when the switch module reports it. This
     * method starts the post-operation and overrun timers.
     *
     * \param currentTime Time stamp of the main operation finalisation
     */
    void endOfOperation(TimeManager::TimeStr currentTime);


    /**
     * \brief Functions to translate between GDB or IOM error codes to Events
     *
     * \param gdbError GDB error code
     * \param iomError IOM error code
     *
     * \return Event Log value code inferred from the given code
     */
    EVENTSWITCH_VALUE GDBtoEvent(const GDB_ERROR gdbError);
    EVENTSWITCH_VALUE IOMtoEvent(const IOM_ERROR iomError);

    /**
     * \brief Logs an event to the Event Log based on outcome value
     *
     * This is meant to be used for logging events of an specified type, using
     * the event value (status) that properly fits with the given outcome.
     *
     * \param eventType Type of the event to log
     * \param outcome Outcome of the operation to log (success/error)
     * \param eventValue Event type: Event Log value to log
     * \param logOK Do log the event in the case of OK result
     */
    void logEventOp(const EVENT_TYPE_SWITCH eventType,
                    const GDB_ERROR outcome,
                    const bool logOK = true);
    void logEventOp(const EVENT_TYPE_SWITCH eventType,
                    const EVENTSWITCH_VALUE eventValue,
                    const bool logOK = true);

    /**
     * \brief Initialise the given list of channel observers
     *
     * Creates each channel observer (and attaches it to the channel) from the
     * given list, or logs an error if anything failed.
     *
     * \param channels List of channels to observe
     * \param observers Returns here the list of observers created
     * \param amount Amount of channels to observe
     * \param channelType Type of channels in the list (for log message)
     *
     * \return Error code if any channel failed to be initialised
     */
    GDB_ERROR initChannels(IChannel* channels[],
                            SGLChObserver* observers[],
                            const lu_uint32_t amount,
                            const char* channelType);

    /**
     * \brief Release all the channel observers
     *
     * Empties the channel observer In/Out lists
     */
    void releaseChannels();

    /**
     * \brief Detach and release all the input observers
     *
     * Empties the Input points observer lists
     */
    void releaseInputs();

private:
    /* Internal state machine states */
    typedef enum
    {
        SGL_STAGE_OFF_LINE         = 0,
        SGL_STAGE_WAIT_COMMAND        ,
        SGL_STAGE_SEND_OPERATION_CMDS ,
        SGL_STAGE_WAIT_COMPLETION     ,
        SGL_STAGE_WAIT_OVERRUN        ,

        SGL_STAGE_LAST
    } SGL_STAGE;

    /**
     * \brief Structure for request's data storage
     */
    struct Request
    {
        DBINARY_STATUS finalPosition;   //Expected switch final position
        OLR_STATE olr;                  //Local/Remote
    };
private:
    SGLOperationCfg cfgOperation;   //Operation-specific configuration parameters (local copy)
    SGL_STAGE stage;                //Current stage of the state machine

    SwitchAuthority switchAuthority;//Switch Authority Controller object
    SwitchLogicOperation operation; //Operation parameters

    SwitchAuthority::SWTYPE authType;   //Type of authority to request

    IChannel* inchannel[Config::INCHANNELIDX_LAST];     //input channels
    IChannel* outchannel[Config::OUTCHANNELIDX_LAST];   //output channels
    PowerSupplyCANChannel* mSupplyChannel;              //PSM's Motor Supply-specific channel reference

    /* Observers */
    SGLChObserver* chInObserver[Config::INCHANNELIDX_LAST];    //Input channels observers
    SGLChObserver* chOutObserver[Config::OUTCHANNELIDX_LAST];  //Output channels observers
    DIObserver* dObserver[SGL_INPUT_LAST];   //Input points observers

	SGLG3ModSwitchJob* g3modSwitchJob; //Job for operation(Motor sypply abnd switch operation) with delay
    SGLOpTimeoutTJob* operationTJob;    //Switch "real action" time out
    SGLOverrunTJob* overrunTJob;        //Time for keeping the motor supply on after operation
    SGLPostOpTJob* postOpTJob;          //Time before allowing another operation
    SGLCancelTJob* cancelResendTJob;    //Job to send CAN cancel command
    SGLCancelMaxTJob* cancelMaximumTJob; //cancel message retry loop timeout (times out if no CAN Cancel was successful)

    PointFlags switchQuality;           //Current quality of the Control Logic, reflected at some Pseudo points
    lu_bool_t motorCurrentOvDebouncing; //Currently counting time for debouncing motor OverCurrent
    TimeManager::TimeStr motorCurrentOvDebounce;    //Time for debouncing motor OverCurrent

    std::vector<bool> clLocalPoint; //Local copy of the status indications

    /* Deprecated indications */
    lu_bool_t msActive;
    lu_bool_t swActive;
    lu_bool_t motorFault;


    lu_uint8_t swChannel;           //Relay channel ID??
    lu_uint32_t totalDuration;      //Operation total duration (in seconds)
    lu_uint32_t operationTimeout;   //Operation timeout time (in seconds)
    lu_uint32_t overrunTime;        //Overrun time in ms
    lu_uint32_t maxMSupplyTime_ms;  //Max value supported for "motor supply on" time
    lu_uint32_t timeMotorOn_ms;     //Full time for the motor to be on


    /* parameters of the requested operation */
    Request lastRequest;

    /* parameters of the accepted operation request */
    bool forcedOperation;           //Trying to open when opened or close when closed
    Request acceptedRequest;
    TimeManager::TimeStr acceptedOperationTime; //Time stamp of the accepted operation

    bool preFinished;   //one of the 2 jobs at the end of the operation has finished
	
	IIOModuleManager& moduleManager;
};


#endif // !defined(EA_CE2C4EFA_E9DB_4922_8D57_478DE84C6292__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
