/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Control Logic custom analogue point interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_132A12C8_3226_4a3b_980F_D8CA135DBB7E__INCLUDED_)
#define EA_132A12C8_3226_4a3b_980F_D8CA135DBB7E__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "AnaloguePoint.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Implementation for Control Logic's Analogue pseudo point.
 * The data change source is only from the setter methods.
 */
class ControlLogicAnaloguePoint : public AnaloguePoint
{

public:
    ControlLogicAnaloguePoint( Mutex *mutex                           ,
                               GeminiDatabase& database               ,
                               Config &config
                             );
    virtual ~ControlLogicAnaloguePoint() {};

    /* == Inherited from IPoint == */
    virtual GDB_ERROR init();

    /* == Inherited from AnaloguePoint == */
    virtual GDB_ERROR setFlags(const PointFlags& newFlags, const TimeManager::TimeStr& timestamp); //Now public

    /**
     * \brief Write a new value to the point
     *
     * \param value Value to write
     * \param timestamp Time of the change
     * \param flags Point flags to set (optional)
     * \param forced Set to LU_TRUE to force point update even if it is the same (optional)
     *
     *
     * \return Error code
     */
    GDB_ERROR setValue( const lu_float32_t value,
                        const TimeManager::TimeStr &timestamp,
                        const lu_bool_t forced = LU_FALSE
                        );
    GDB_ERROR setValue( const lu_float32_t value,
                        const TimeManager::TimeStr& timestamp,
                        const PointFlags& flags,
                        const lu_bool_t forced
                        );

    /**
     * \brief Write a new status to the point
     *
     * \param status Online/offline status
     * \param timestamp Time of the change
     *
     * \return Error code
     */
    GDB_ERROR setQuality(const lu_bool_t status, const TimeManager::TimeStr& timestamp);

protected:
    /* == Inherited from AnaloguePoint == */
    virtual void update(PointData* newData, DATACHANGE dataChange);

private:
    lu_float32_t incomingValue; //Latest value set with setValue
    PointFlags incomingFlags;   //Latest flags set with setValue/setFlags/setQuality
};

#endif // !defined(EA_132A12C8_3226_4a3b_980F_D8CA135DBB7E__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
