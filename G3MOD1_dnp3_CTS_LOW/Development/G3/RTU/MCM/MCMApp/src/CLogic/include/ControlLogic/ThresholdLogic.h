/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ThresholdLogic.h 5 Sep 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/CLogic/include/ControlLogic/ThresholdLogic.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Threshold detection Control Logic (aka HiHiLoLo Control Logic).
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 5 Sep 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   5 Sep 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef THRESHOLDLOGIC_H__INCLUDED
#define THRESHOLDLOGIC_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "ControlLogic.h"
#include "ControlLogicDigitalPoint.h"
#include "HIHILOLOFilter.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Threshold detection Control Logic
 *
 * This Control Logic evaluates an analogue input and enables different digital
 * pseudo points depending on threshold definitions.
 *
 * This Control Logic is driven by events in the inputs, and not from an
 * "operate" command.
 */
class ThresholdLogic: public ControlLogic
{
public:
    /**
     * \brief configuration block for this Control Logic
     */
    struct Config : public ControlLogicConf
    {
    public:
        /* Amounts of different types of Control Logic's Pseudo Points */
        static const lu_uint16_t BINARYPOINT_NUM = THR_POINT_TYPE_BINARY_COUNT;
    public:
        PointIdStr aInput; //Analogue Input point
        HIHILOLOFilter::Config filterConf;  //Note: Filters are sorted from LoLo to HiHi
        ControlLogicDigitalPoint::Config bPoint[BINARYPOINT_NUM]; //CLogic analogue Points
        lu_bool_t binOutEnabled[BINARYPOINT_NUM];
    };

public:
    /**
     * \brief Custom constructor
     *
     * \param mutex Reference to the G3DB common mutex
     * \param database Reference to the Gemini database
     * \param config reference to the Control Logic's configuration block
     */
    ThresholdLogic(Mutex* mutex, GeminiDatabase& database, ThresholdLogic::Config& config);
    virtual ~ThresholdLogic();

    /* == Inherited from ControlLogic == */
    virtual GDB_ERROR init();
    virtual GDB_ERROR getInputValue(lu_uint16_t inputPoint, PointData *data);

protected:
    /* == Inherited from ControlLogic == */
    virtual void newData(PointIdStr pointID, PointData& pointData);

private:
    ThresholdLogic::Config conf;
    HIHILOLOFilter m_filter;    // Filter used for checking thresholds
    AIObserver* aObserver;      // Analogue Input point observer
    PointFlags outFlags;        //Current common status of all the pseudo points
    std::vector<bool> clLocalPoint; //Local copy of all the pseudo point values
};


#endif /* THRESHOLDLOGIC_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
