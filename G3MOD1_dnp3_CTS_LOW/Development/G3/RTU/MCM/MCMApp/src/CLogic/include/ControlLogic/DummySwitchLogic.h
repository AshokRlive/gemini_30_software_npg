/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id: DummySwitchLogic.h $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/RTU/MCM/src/CLogic/include/DummySwitchLogic.h $
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Dummy Switch control logic
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 2012-07-31 16:14:00 +0100 (Tue, 31 Jul 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   31/07/12      pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef DUMMYSWITCHLOGIC_H_
#define DUMMYSWITCHLOGIC_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IChannel.h"
#include "IStatusManager.h"
#include "IMemoryManager.h"
#include "OperatableControlLogic.h"
#include "ControlLogicDigitalPoint.h"

/* forward declarations */

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Dummy Switch control Logic Block
 *
 * Every switch is executed immediately at the command request.
 */
class DummySwitchLogic : public OperatableControlLogic
{
public:
    /**
     * \brief Dummy Switch logic control block
     */
    struct OpParams
    {
    public:
        LED_COLOUR openColour;   //Open Operation LED colour
        lu_uint32_t interval;    //post-operation time in ms (time between 2 consecutive switch operations)
        bool allowForced;        //Allow forced operation over this logic

    public:
        OpParams() : openColour(LED_COLOUR_RED),
                                    interval(0),
                                    allowForced(LU_FALSE)
        {};
    };

    /**
     * \brief Dummy Switch Control Logic's configuration block
     */
    struct Config : public OperatableControlLogic::Config
    {
    public:
        /* Amounts of different types of Control Logic's Pseudo Points */
        static const lu_uint16_t BINARYPOINT_NUM = DSL_POINT_TYPE_BINARY_COUNT;
        static const lu_uint16_t DBINARYPOINT_NUM = DSL_POINT_TYPE_DOUBLEBINARY_COUNT;
        static const lu_uint32_t ALLPOINT_NUM = DSL_POINT_EnumSize;
    public:
        OpParams opParams;    /* Control Logic operation parameters */
        PointIdStr dInput[DSL_INPUT_LAST];  //Digital Input point
        ControlLogicDigitalPoint::Config bPoint[BINARYPOINT_NUM]; //Control Logic digital points
        ControlLogicDigitalPoint::Config dbPoint[DBINARYPOINT_NUM]; //Control Logic digital points
    };

public:
    /**
     * \brief Custom constructor
     *
     * \param statMan Reference to the Status Manager
     * \param database Reference to the Gemini Database
     * \param config Reference to the Dummy Switch Control Logic's configuration block
     */
    DummySwitchLogic(Mutex* mutex,
                     GeminiDatabase& database,
                     IMemoryManager& memMgr,
                     DummySwitchLogic::Config &conf
                     );
    virtual ~DummySwitchLogic();

    /* == inherited from ControlLogic == */
    virtual GDB_ERROR init();
    virtual GDB_ERROR getInputValue(lu_uint16_t inputPoint, PointData *data);
    virtual GDB_ERROR getOutputValue(PointData* data);

protected:
    /**
     * This timer adds a time to wait after an operation has been finished before
     * allowing another operation over this switch. It's the time between 2
     * consecutive commands, so this timer is here to refuse any switch
     * command that is received before this timer expires.
     */
    class DSLPostOpTimer : public TimedJob
    {

    public:
        /**
         * \brief Custom constructor
         *
         * \param cLogic Reference to the control logic (dummy switch) to use
         * \param Timeout timer value in ms
         *
         * \return none
         */
        DSLPostOpTimer(DummySwitchLogic &cLogic, lu_uint32_t timeout):
                                                               TimedJob(timeout, 0),
                                                               cLogic(cLogic)
        {};

        virtual ~DSLPostOpTimer() {};

    protected:
        /**
         * \brief User define operation  This method is called by the run public method
         * only if the configured timer is expired
         *
         * \param timePtr Current absolute time
         *
         * \return none
         */
        virtual void job(TimeManager::TimeStr *timePtr)
        {
            LU_UNUSED(timePtr);

            /* Signal timeout to the related Control Logic */
            cLogic.endPostOpTime();
        }

    private:
        DummySwitchLogic &cLogic;
    };

protected:
    /* == Implements mandatory OperatableControlLogic == */
    virtual GDB_ERROR startOp(const SwitchLogicOperation &operation, const GDB_ERROR errorCode);

    /**
     * \brief End of the Post-operation time reached
     *
     * This states that the post-operation time has been finished.
     */
    void endPostOpTime();


private:
    /**
     * \brief Switches the Dummy LED.
     *
     * Set the LED colour to open or close position depending on the configuration.
     *
     * \param openIt Operation to be done. Set to LU_TRUE to open or LU_FALSE to close.
     */
    void switchDummyLED(lu_bool_t openIt);

private:
    IMemoryManager &memoryManager;
    OpParams operationParameters;    //switch operation parameters

    DBINARY_STATUS actualPosition;  /* Current position of the dummy switch */
    bool postOpTimeOn;              //True when counting post-operation time
    DSLPostOpTimer* postOpTimer;    //Timer for post-op counting. Keep pointer for cancelling

};


#endif /* DUMMYSWITCHLOGIC_H_ */

/*
 *********************** End of file ******************************************
 */
