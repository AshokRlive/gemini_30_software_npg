/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Local Remote control logic implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   09/08/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "LocalRemoteControlLogic.h"
#include "timeOperations.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static const OLR_STATE defaultOLRState = OLR_STATE_OFF;

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

LocalRemoteControlLogic::LocalRemoteControlLogic( Mutex* mutex,
                                                  GeminiDatabase& g3database,
                                                  IMemoryManager& memMgr,
                                                  LocalRemoteControlLogic::Config& conf
                                                 ) :
                                     ControlLogic(mutex, g3database, conf, CONTROL_LOGIC_TYPE_OLR),
                                     memoryManager(memMgr),
                                     inputChannel(conf.getOLRChannel),
                                     outChannel(conf.setOLRChannel),
                                     currentStatus(OLR_STATE_LAST),
                                     lastSource(OLR_SOURCE_BKPLANESW)
{
    enabled = true; //OLR is forced to always been enabled!

    sourceTimestamp.tv_sec = 0;
    sourceTimestamp.tv_nsec = 0;

    /* Allocate Control Logic virtual points */
    clPoint.reserve(Config::ALLPOINT_NUM);  //Adjust vector size to do not waste memory
    for(lu_uint32_t id = 0; id < Config::BINARYPOINT_NUM; ++id)
    {
        clPoint.push_back(new ControlLogicDigitalPoint(mutex, database, conf.bPoint[id], POINT_TYPE_BINARY));
    }
    for(lu_uint32_t id = 0; id < Config::DBINARYPOINT_NUM; ++id)
    {
        clPoint.push_back(new ControlLogicDigitalPoint(mutex, database, conf.dbPoint[id], POINT_TYPE_DBINARY));
    }
}


LocalRemoteControlLogic::~LocalRemoteControlLogic()
{
    /* Release observers */
    if(inputChannel != NULL)
    {
        inputChannel->detach(this);
    }
    inputChannel = NULL;
}


GDB_ERROR LocalRemoteControlLogic::init()
{
    GDB_ERROR ret = initCheck();
    if(ret == GDB_ERROR_NONE)
    {
        ret = activateCLPoints();   //Initialise Control Logic points as well
    }
    if(ret != GDB_ERROR_NONE)
    {
        return ret;
    }

    startCLPoints(true);


    /* Recover OLR status from memory */
    OLR_STATE newStatus;
    OLR_SOURCE source = OLR_SOURCE_DEFAULT; //source of the OLR set up
    bool memfail = true;
    lu_uint8_t storedOLR;  //value is stored in reduced format
    if(memoryManager.read(IMemoryManager::STORAGE_VAR_OLR, &storedOLR) == LU_TRUE)
    {
        newStatus = static_cast<OLR_STATE>(storedOLR);
        if( (newStatus >= OLR_STATE_OFF) && (newStatus < OLR_STATE_LAST) )
        {
            memfail = false;
            source = OLR_SOURCE_MEMORY;
        }
    }
    if(memfail)
    {
        //Failed: Set the default state
        newStatus = defaultOLRState;
        source = OLR_SOURCE_DEFAULT;
        log.error("%s reading from NVRAM memory failed. "
                    "Setting now OLR state to default: %s (%i)",
                    this->toString().c_str(),
                    OLR_STATE_ToSTRING(newStatus), newStatus
                    );
    }
    setLocalRemote(newStatus, source);

    /* start channel observer */
    IOM_ERROR res = IOM_ERROR_CONFIG;
    if( (inputChannel != NULL) && (outChannel != NULL) )
    {
        res = inputChannel->attach(this);   //Observe input channel only
    }
    if(res != IOM_ERROR_NONE)
    {
        ret = GDB_ERROR_NOT_INITIALIZED;
        log.error("%s - configuring input channel error: %s",
                    this->toString().c_str(), IOM_ERROR_ToSTRING(res));
    }

    if(ret != GDB_ERROR_NONE)
    {
        //Detach all already attached and disable Control Logic indications
        if(inputChannel != NULL)
        {
            inputChannel->detach(this);
        }
        inputChannel = NULL;
        startCLPoints(false);
    }
    else
    {
        initialised = true;
        //update(inputChannel); //Force 1st update
    }
    return ret;
}


GDB_ERROR LocalRemoteControlLogic::getInputValue(lu_uint16_t inputPoint, PointData *data)
{
    LU_UNUSED(inputPoint);
    LU_UNUSED(data);
    return GDB_ERROR_NOT_SUPPORTED;
}


GDB_ERROR LocalRemoteControlLogic::setLocalRemote(OLR_STATE status, OLR_SOURCE source)
{
    GDB_ERROR ret = GDB_ERROR_LOCAL_REMOTE;
    log.debug("%s - trying to set from %s (%d) to %s (%d), source: %s (%i)",
                this->toString().c_str(),
                OLR_STATE_ToSTRING(currentStatus), currentStatus,
                OLR_STATE_ToSTRING(status), status,
                OLR_SOURCE_ToSTRING(source), source
              );

    IODataUint8 data;
    IChannel::ValueStr value(data);
    data = (lu_uint8_t)status;
    if(outChannel->write(value) == IOM_ERROR_NONE)   //Try to set it!
    {
        lastSource = source;
        clock_gettimespec(&sourceTimestamp);
        ret = GDB_ERROR_NONE;
    }

    return ret;
}


void LocalRemoteControlLogic::update(IChannel *subject)
{
    //incoming update from input channel
    IODataUint8 data;
    IChannel::ValueStr value(data);
    subject->read(value);
    lu_uint8_t newValue = data;
    OLR_STATE newOLRPosition = static_cast<OLR_STATE>(newValue);
    log.debug("%s state changed from %d (%s) to %d (%s)",
                this->toString().c_str(),
                currentStatus,
                OLR_STATE_ToSTRING(currentStatus),
                newOLRPosition,
                OLR_STATE_ToSTRING(newOLRPosition)
                );
    currentStatus = newOLRPosition;
    TimeManager::TimeStr timestamp;
    data.getTime(timestamp);

    /* Update CL's virtual points */
    PointFlags pointFlags;
    pointFlags.setInitial(value.flags.restart == LU_TRUE);
    pointFlags.setInvalid(false);
    pointFlags.setOnline(value.flags.online == LU_TRUE);
    setValue(OLR_POINT_OFF   , (currentStatus == OLR_STATE_OFF)? 1 : 0, timestamp, pointFlags);
    setValue(OLR_POINT_LOCAL , (currentStatus == OLR_STATE_LOCAL)? 1 : 0, timestamp, pointFlags);
    setValue(OLR_POINT_REMOTE, (currentStatus == OLR_STATE_REMOTE)? 1 : 0, timestamp, pointFlags);
    setValue(OLR_POINT_OFFLOCALREMOTE, currentStatus, timestamp, pointFlags);

    /* Start critical section */
    LockingMutex lMutex(*globalMutex);

    /* Get change source validity */
    struct timespec appCurrentTime;
    clock_gettimespec(&appCurrentTime);
    if(timespec_elapsed_ms(&sourceTimestamp, &appCurrentTime) > 5000)
    {
        //If change was requested too much time ago, then assume that change was made from Backplane.
        lastSource = OLR_SOURCE_BKPLANESW;
    }

    /* Store current OLR status in memory only if set by user */
    if(lastSource != OLR_SOURCE_BKPLANESW)
    {
        lu_uint8_t storedOLR;  //value is stored in reduced format
        storedOLR = static_cast<OLR_STATE>(currentStatus);
        if(memoryManager.write(IMemoryManager::STORAGE_VAR_OLR, &storedOLR) == LU_FALSE)
        {
            log.error("%s writing to NVRAM memory failed", this->toString().c_str());
        }
    }

    log.info("%s OLR set to %s (%i) status from %s (%i)",
                this->toString().c_str(),
                OLR_STATE_ToSTRING(currentStatus), currentStatus,
                OLR_SOURCE_ToSTRING(lastSource), lastSource
              );

    //Report Event
    CTEventOLRStr event;
    event.value = (currentStatus == OLR_STATE_OFF)? EVENTOLR_VALUE_OFF :
                    (currentStatus == OLR_STATE_LOCAL)? EVENTOLR_VALUE_LOCAL :
                        (currentStatus == OLR_STATE_REMOTE)? EVENTOLR_VALUE_REMOTE : EVENTOLR_VALUE_ERROR;
    //event.source = lastSource;
    eventLog->logEvent(event);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
