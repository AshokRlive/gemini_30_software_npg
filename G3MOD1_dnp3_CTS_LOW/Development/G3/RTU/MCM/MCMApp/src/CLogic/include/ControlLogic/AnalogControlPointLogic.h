/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: AnalogControlPointLogic.h 2 Sep 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/CLogic/include/ControlLogic/AnalogControlPointLogic.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Analog Control Point control logic.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 2 Sep 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   2 Sep 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef ANALOGCONTROLPOINTLOGIC_H__INCLUDED
#define ANALOGCONTROLPOINTLOGIC_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "MCMConfigEnum.h"  //For EDGE_DRIVEN_MODE
#include "OperatableControlLogic.h"
#include "ControlLogicAnaloguePoint.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Converts a Control Logic operation command to a digital Virtual Point
 *
 * This class sets the value of a single binary pseudo point when operated.
 */
class AnalogControlPointLogic: public OperatableControlLogic
{
public:
    struct Config : public OperatableControlLogic::Config
    {
    public:
        /* Amounts of different types of Control Logic's Pseudo Points */
        static const lu_uint16_t ANALOGPOINT_NUM = ANALOGCTRL_POINT_TYPE_ANALOGUE_COUNT;
        static const lu_uint16_t ALLPOINT_NUM =  ANALOGCTRL_POINT_EnumSize;
    public:
        AnaloguePoint::Config aPoint[ANALOGPOINT_NUM];   //single binary CLogic virtual points
    };

public:
    AnalogControlPointLogic(Mutex* mutex                 ,
                             GeminiDatabase& database     ,
                             AnalogControlPointLogic::Config& conf
                           );
    virtual ~AnalogControlPointLogic();

     /* == Inherited from OperatableControlLogic == */
    virtual GDB_ERROR writeOutputValue(AnalogueOutputOperation &val);

     /* == Inherited from ControlLogic == */
    virtual GDB_ERROR init();
    virtual GDB_ERROR getInputValue(lu_uint16_t inputPoint, PointData* data);

protected:
    /* == Implements mandatory OperatableControlLogic == */
    virtual GDB_ERROR startOp(const SwitchLogicOperation &operation, const GDB_ERROR errorCode);

private:

private:
    AnalogControlPointLogic::Config conf;
};


#endif /* ANALOGCONTROLPOINTLOGIC_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
