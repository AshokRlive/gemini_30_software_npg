/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Control logic implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "ControlLogic.h"
#include "EventLogManager.h"
#include "ControlLogicDigitalPoint.h"
#include "ControlLogicAnaloguePoint.h"
#include "ControlLogicCounterPoint.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
Mutex*          ControlLogic::globalMutex = NULL;
IEventLogManager* ControlLogic::eventLog  = NULL;
TimeManager&    ControlLogic::timeManager = TimeManager::getInstance();

const PointFlags ControlLogic::ValidFlags(PointFlags::VALID);
const PointFlags ControlLogic::InvalidFlags(PointFlags::INVALID);
const PointFlags ControlLogic::InitialFlags(PointFlags::INVALID, true);

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
ControlLogic::ControlLogic( Mutex *mutex            ,
                            GeminiDatabase& g3database,
                            const ControlLogicConf config,
                            const CONTROL_LOGIC_TYPE CLtype
                          ) :
                                database(g3database),
                                log(Logger::getLogger(SUBSYSTEM_ID_CLOGIC)),
                                cLogicType(CLtype),
                                groupID(config.groupID),
                                enabled(config.enabled),
                                initialised(false)
{
    /* Initialise common parameters */
    if(this->globalMutex == NULL)
    {
        this->globalMutex = mutex;
    }
    if(eventLog == NULL)
    {
        eventLog = EventLogManager::getInstance();
    }
}


ControlLogic::~ControlLogic()
{
    stopUpdate();

    /* Release all Control Logic Pseudo points */
    for(lu_uint32_t id = 0; id < clPoint.size(); ++id)
    {
        delete clPoint[id];
        clPoint[id] = NULL;
    }
}


std::string ControlLogic::toString()
{
    std::stringstream ss;
    ss << CONTROL_LOGIC_TYPE_ToSTRING(getType()) << " " << getID();
    return ss.str();
}


IPoint* ControlLogic::getPoint(const PointIdStr pointID)
{
    if(!pointID.isValid() || (pointID.group != groupID) || clPoint.empty())
    {
        return NULL;
    }
    IPoint* pointCL = NULL;
    try
    {
        pointCL = clPoint.at(pointID.ID);
    }
    catch (const std::out_of_range& e)
    {
        pointCL = NULL;
    }
    return pointCL;
}


POINT_TYPE ControlLogic::getPointType(PointIdStr pointID)
{
    if(pointID.group != groupID)
        return POINT_TYPE_INVALID;
    try
    {
        IPoint* pointCL = clPoint.at(pointID.ID);
        if(pointCL != NULL)
            return pointCL->getType();
    }
    catch (const std::out_of_range& e)
    {}
    return POINT_TYPE_INVALID;
}


GDB_ERROR ControlLogic::getOutputValue(PointData* data)
{
    LU_UNUSED(data);

    //Default behaviour
    log.warn("ControlLogic::getOutputValue for %s is not supported", this->toString().c_str());
    return GDB_ERROR_NOT_SUPPORTED;
}


GDB_ERROR ControlLogic::setLocalRemote(OLR_STATE status, OLR_SOURCE source)
{
    LU_UNUSED(status);
    LU_UNUSED(source);

    //Default behaviour
    log.warn("ControlLogic::setLocalRemote for group %i is not supported", groupID);
    return GDB_ERROR_NOT_SUPPORTED;
}


GDB_ERROR ControlLogic::startOperation(SwitchLogicOperation &operation)
{
    LU_UNUSED(operation);

    //Default behaviour
    log.warn("ControlLogic::startOperation for group %i is not supported", groupID);
    return GDB_ERROR_NOT_SUPPORTED;
}


void ControlLogic::stopUpdate()
{
    for(lu_uint32_t id = 0; id < clPoint.size(); ++id)
    {
        if(clPoint[id] != NULL)
        {
            clPoint[id]->stopUpdate();
        }
    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
void ControlLogic::newData(PointIdStr pointID, PointData& pointData)
{
    //Default behaviour
    LU_UNUSED(pointID);
    LU_UNUSED(pointData);
}


GDB_ERROR ControlLogic::setValue(IPoint& pointCL,
                                const lu_uint32_t value,
                                const TimeManager::TimeStr& timestamp,
                                const PointFlags& flags,
                                const lu_bool_t forced)
{
    switch (pointCL.getType())
    {
        case POINT_TYPE_BINARY:
        case POINT_TYPE_DBINARY:
        {
            ControlLogicDigitalPoint* dPoint;
            dPoint = dynamic_cast<ControlLogicDigitalPoint*>(&pointCL);
            return dPoint->setValue(value, timestamp, flags, forced);
            break;
        }
        case POINT_TYPE_COUNTER:
        {
            ControlLogicCounterPoint* cPoint;
            cPoint = dynamic_cast<ControlLogicCounterPoint*>(&pointCL);
            return cPoint->setValue(value, timestamp, flags, forced);
            break;
        }
        default:
            break;
    }
    return GDB_ERROR_NOPOINT;
}


GDB_ERROR ControlLogic::setValue(IPoint& pointCL,
                                const lu_float32_t value,
                                const TimeManager::TimeStr& timestamp,
                                const PointFlags& flags,
                                const lu_bool_t forced)
{
    switch (pointCL.getType())
    {
        case POINT_TYPE_ANALOGUE:
        {
            ControlLogicAnaloguePoint* aPoint;
            aPoint = dynamic_cast<ControlLogicAnaloguePoint*>(&pointCL);
            return aPoint->setValue(value, timestamp, flags, forced);
            break;
        }
        default:
            break;
    }
    return GDB_ERROR_NOPOINT;
}

GDB_ERROR ControlLogic::setQuality(const lu_uint32_t pointID, const POINT_QUALITY status, const TimeManager::TimeStr& timestamp)
{
    return setQuality(pointID, (status == POINT_QUALITY_ON_LINE)? LU_TRUE : LU_FALSE, timestamp);
}

GDB_ERROR ControlLogic::setQuality(const lu_uint32_t pointID, const lu_bool_t status, const TimeManager::TimeStr& timestamp)
{
    try
    {
        IPoint* pointCL = clPoint.at(pointID);
        if(pointCL != NULL)
        {
            switch (pointCL->getType())
            {
                case POINT_TYPE_BINARY:
                case POINT_TYPE_DBINARY:
                {
                    ControlLogicDigitalPoint* dPoint;
                    dPoint = dynamic_cast<ControlLogicDigitalPoint*>(pointCL);
                    return dPoint->setQuality(status, timestamp);
                    break;
                }
                case POINT_TYPE_ANALOGUE:
                {
                    ControlLogicAnaloguePoint* aPoint;
                    aPoint = dynamic_cast<ControlLogicAnaloguePoint*>(pointCL);
                    return aPoint->setQuality(status, timestamp);
                    break;
                }
                case POINT_TYPE_COUNTER:
                {
                    ControlLogicCounterPoint* cPoint;
                    cPoint = dynamic_cast<ControlLogicCounterPoint*>(pointCL);
                    return cPoint->setQuality(status, timestamp);
                    break;
                }
                default:
                    break;
            }
        }
    }
    catch (const std::out_of_range& e)
    {}
    return GDB_ERROR_NOPOINT;
}

GDB_ERROR ControlLogic::setFlags(const lu_uint32_t pointID, const PointFlags status, const TimeManager::TimeStr& timestamp)
{
    try
    {
        IPoint* pointCL = clPoint.at(pointID);
        if(pointCL != NULL)
        {
            switch (pointCL->getType())
            {
                case POINT_TYPE_BINARY:
                case POINT_TYPE_DBINARY:
                {
                    ControlLogicDigitalPoint* dPoint;
                    dPoint = dynamic_cast<ControlLogicDigitalPoint*>(pointCL);
                    return dPoint->setFlags(status, timestamp);
                    break;
                }
                case POINT_TYPE_ANALOGUE:
                {
                    ControlLogicAnaloguePoint* aPoint;
                    aPoint = dynamic_cast<ControlLogicAnaloguePoint*>(pointCL);
                    return aPoint->setFlags(status, timestamp);
                    break;
                }
                case POINT_TYPE_COUNTER:
                {
                    ControlLogicCounterPoint* cPoint;
                    cPoint = dynamic_cast<ControlLogicCounterPoint*>(pointCL);
                    return cPoint->setFlags(status, timestamp);
                    break;
                }
                default:
                    break;
            }
        }
    }
    catch (const std::out_of_range& e)
    {}
    return GDB_ERROR_NOPOINT;
}


GDB_ERROR ControlLogic::initCheck()
{
    GDB_ERROR ret = GDB_ERROR_NONE;

    if(initialised)
    {
        ret = GDB_ERROR_INITIALIZED;    //Already initialised
    }
    else if(!enabled)
    {
        ret = GDB_ERROR_NOT_ENABLED;    //Not enabled
    }
    return ret;
}


GDB_ERROR ControlLogic::activateCLPoints()
{
    GDB_ERROR ret = GDB_ERROR_NONE; //Cumulative result
    GDB_ERROR res = GDB_ERROR_NONE; //Result of each point initialisation
    for (CLPointList::const_iterator it = clPoint.begin(); it != clPoint.end(); it++)
    {
        res = (*it)->init();
        if(res != GDB_ERROR_NONE)
        {
            ret = res;
            log.error("%s - Error initialising indication point %s: %s",
                            this->toString().c_str(),
                            (*it)->getID().toString().c_str(),
                            GDB_ERROR_ToSTRING(ret)
                            );
        }
    }
    return ret;
}


GDB_ERROR ControlLogic::startCLPoints(const bool online)
{
    TimeManager::TimeStr timeStamp = timeManager.now(); //Current time stamp
    PointFlags status(PointFlags::INVALID, online);     //Flags to apply

    return startCLPoints(status, timeStamp);
}

GDB_ERROR ControlLogic::startCLPoints(const bool online,
                                      const TimeManager::TimeStr& timeStamp)
{
    PointFlags status(PointFlags::INVALID, online);    //Flags to apply
    return startCLPoints(status, timeStamp);
}

GDB_ERROR ControlLogic::startCLPoints(const PointFlags& flags)
{
    TimeManager::TimeStr timeStamp = timeManager.now(); //Current time stamp
    return startCLPoints(flags, timeStamp);
}

GDB_ERROR ControlLogic::startCLPoints(const PointFlags& flags,
                                      const TimeManager::TimeStr& timeStamp)
{
    GDB_ERROR ret = GDB_ERROR_NONE; //Cumulative result
    GDB_ERROR res = GDB_ERROR_NONE; //Result of each point initialisation
    for (lu_uint32_t id = 0; id < getPointNum(); ++id)
    {
        res = setFlags(id, flags, timeStamp);
        if(res != GDB_ERROR_NONE)
        {
            ret = res;
            log.error("%s - Error starting flags of indication point %d,%d: %s",
                            this->toString().c_str(),
                            groupID, id,
                            GDB_ERROR_ToSTRING(ret)
                            );
        }
    }
    return ret;
}


GDB_ERROR ControlLogic::attachObserver(IPointObserver *observer)
{
    GDB_ERROR ret = GDB_ERROR_NOPOINT;
    if(observer != NULL)
    {
        if(observer->getPointID().isValid())
        {
            /* Attach observer */
            ret = database.attach(observer->getPointID(), observer);
            if(ret != GDB_ERROR_NONE)
            {
                logInputPointAttachError(observer->getPointID(), ret);
            }
        }
    }
    return ret;
}


void ControlLogic::detachObserver(IPointObserver *observer)
{
    if(observer == NULL)
    {
        return;
    }
    database.detach(observer->getPointID(), observer);
    delete observer;
    observer = NULL;
}


void ControlLogic::logInputPointAttachError(const PointIdStr inPointID, const GDB_ERROR errorGDB)
{
    log.error("%s - virtual input point %s attach error: %s",
                                this->toString().c_str(),
                                inPointID.toString().c_str(),
                                GDB_ERROR_ToSTRING(errorGDB)
                                );
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */


