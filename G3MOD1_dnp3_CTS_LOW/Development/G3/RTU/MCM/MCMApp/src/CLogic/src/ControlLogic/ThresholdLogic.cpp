/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ThresholdLogic.cpp 5 Sep 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/CLogic/src/ControlLogic/ThresholdLogic.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Threshold detection Control Logic (aka HiHiLoLo Control Logic).
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 5 Sep 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   5 Sep 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <limits>
#include <assert.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "ThresholdLogic.h"
#include "Debug.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/* Debug Macro*/
#define DEBUG_THR_LOGIC 0
#if DEBUG_THR_LOGIC
#define LOG_PRINTCONFIGDEBUG(a,b)    printConfigDebug(a,b)
    #define LOG_THRESHOLD_INCOMING \
        DBG_INFO("%s INCOMING val:%.3f F:%s", conf.aInput.toString().c_str(), (lu_float32_t)pointData, pointData.getFlags().toString().c_str())
    #define LOG_THRESHOLD_TRIGGERD \
        DBG_INFO("%s TRIGGERED %4s val:%.3f F:%s", conf.aInput.toString().c_str(), thresholdName(i), value, outFlags.toString().c_str())
    #define LOG_THRESHOLD_RECOVERED \
        DBG_INFO("%s RECOVERED %4s val:%.3f F:%s", conf.aInput.toString().c_str(), thresholdName(i), value, outFlags.toString().c_str())

#else
    #define LOG_PRINTCONFIGDEBUG(a,b) do {} while(0)
    #define LOG_THRESHOLD_INCOMING
    #define LOG_THRESHOLD_TRIGGERD
    #define LOG_THRESHOLD_RECOVERED
#endif

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
#if DEBUG_THR_LOGIC
/**
 * Gets threshold name.
 */
static const lu_char_t* thresholdName(lu_uint32_t thresholdId)
{
    switch (thresholdId)
    {
        case THR_POINT_HIHI: return "HiHi";
        case THR_POINT_HI: return "Hi  ";
        case THR_POINT_LO: return "Lo  ";
        case THR_POINT_LOLO: return "LoLo";
        default:return "Unknown";
    }
}

static void printConfigDebug(const std::string& logicName, ThresholdLogic::Config& conf)
{
    /* Debug: print config */
    for (lu_uint32_t i = 0; i < conf.BINARYPOINT_NUM; ++i)
    {
        lu_uint32_t filterPos = 0;
        //Convert from binPoint index to filter index
        switch(i)
        {
            case THR_POINT_HIHI:
                filterPos = HIHILOLOFilter::HIHI;
                break;
            case THR_POINT_HI:
                filterPos = HIHILOLOFilter::HI;
                break;
            case THR_POINT_LO:
                filterPos = HIHILOLOFilter::LO;
                break;
            case THR_POINT_LOLO:
                filterPos = HIHILOLOFilter::LOLO;
                break;
            default:
                break;
        }
        DBG_INFO("%s Point:%s ThresholdSet[%s]: %s, threshold:%.3f,hysteresis:%.3f,recovery:%.3f",
                        logicName.c_str(),
                        conf.aInput.toString().c_str(),
                        thresholdName(i),
                        (conf.binOutEnabled[i] == LU_FALSE)? "disabled" : "enabled",
                        conf.filterConf.limit[filterPos].threshold,
                        conf.filterConf.limit[filterPos].hysteresis,
                        conf.filterConf.limit[filterPos].recovery);
    }
}
#endif

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

ThresholdLogic::ThresholdLogic( Mutex* mutex,
                                GeminiDatabase& g3database,
                                ThresholdLogic::Config& config
                                ) :
                                    ControlLogic(mutex, g3database, config, CONTROL_LOGIC_TYPE_THR),
                                    conf(config),
                                    m_filter(config.filterConf),
                                    aObserver(NULL)
{
    assert(HIHILOLOFilter::Config::Limits == ThresholdLogic::Config::BINARYPOINT_NUM);

    /* Allocate Control Logic virtual points */
    clPoint.reserve(Config::BINARYPOINT_NUM);       //Adjust vector size to do not waste memory
    clLocalPoint.reserve(Config::BINARYPOINT_NUM);  //Adjust vector size to do not waste memory

    for(lu_uint32_t id = 0; id < Config::BINARYPOINT_NUM; ++id)
    {
        clPoint.push_back(new ControlLogicDigitalPoint(mutex, database, config.bPoint[id]));
        clLocalPoint.push_back(false);  //All statuses are initially false
    }

    /* Create input point */
    aObserver = new AIObserver(config.aInput, *this);
}


ThresholdLogic::~ThresholdLogic()
{
    /* Release observers */
    detachObserver(aObserver);
}


GDB_ERROR ThresholdLogic::init()
{
    GDB_ERROR ret = initCheck();
    if(ret == GDB_ERROR_NONE)
    {
        ret = activateCLPoints();   //Initialise Control Logic points as well
    }
    if(ret != GDB_ERROR_NONE)
    {
        return ret;
    }

    /* Start Control Logic Points */
    LOG_PRINTCONFIGDEBUG(this->toString(), conf);

    /* All indication points are set to offline, invalid, and initial at the
     * beginning. Quality and validity will change from the analogue input
     * flags at the newData() method.
     */
    startCLPoints(false);
    TimeManager::TimeStr timeStamp = timeManager.now(); /* Initial time stamp */
    PointFlags initialFlags = outFlags;
    for (lu_uint32_t id = 0; id < getPointNum(); ++id)
    {
        //setValue(id, 0, timeStamp, outFlags);
        if(id < Config::BINARYPOINT_NUM)
        {
            clLocalPoint[id] = false;   //local status flags back to no indication
        }
    }
    initialised = true;

    /* Initialise input points */
    ret = attachObserver(aObserver);
    if(ret != GDB_ERROR_NONE)
    {
        //Detach all already attached and disable Control Logic indications
        initialised = false;
        startCLPoints(false);
    }

    return ret;
}


GDB_ERROR ThresholdLogic::getInputValue(lu_uint16_t inputPoint, PointData* data)
{
    if( (data == NULL) ||
        (inputPoint > THR_INPUT_LAST)
      )
    {
        return GDB_ERROR_PARAM;
    }

    if(aObserver != NULL)
    {
        return database.getValue(aObserver->getPointID(), data);
    }
    return GDB_ERROR_NOPOINT;
}


void ThresholdLogic::newData(PointIdStr pointID, PointData& pointData)
{
    LU_UNUSED(pointID);

    TimeManager::TimeStr timeStamp;
    pointData.getTime(timeStamp);

    LOG_THRESHOLD_INCOMING;

    bool updateFlags = false;

    /* Check point input flags and set pseudo-point flags accordingly */
    if( ( pointData.getQuality() != outFlags.getQuality() ) ||
        ( pointData.getInvalidFlag() != outFlags.isInvalid() ) ||
        ( pointData.getInitialFlag() != outFlags.isInitial() )
        )
    {
        /* If the input changes flags, ALL the pseudo-points change flags as well */
        outFlags.setOnline(pointData.getOnlineFlag());
        outFlags.setInvalid(pointData.getInvalidFlag());
        outFlags.setInitial(pointData.getInitialFlag());
        updateFlags = true;
    }

    lu_float32_t value;
    AFILTER_RESULT filterRes;
    HIHILOLOFilter::REGION region;

    /* Apply value changes only when value is valid */
    if(pointData.isActive())
    {
        value = (lu_float32_t)pointData;
        filterRes = m_filter.run(value);
        region = m_filter.getCurrentRegion();
    }
    for (lu_uint32_t i = 0; i < conf.BINARYPOINT_NUM; ++i)
    {
        bool applyUpdateFlags = updateFlags;    //Custom condition for current threshold indication
        //Custom Flags for current threshold indication:
        PointFlags thrFlags = outFlags;
        thrFlags.setOnline(conf.binOutEnabled[i] == LU_TRUE);

        if(pointData.isActive())
        {
            if(filterRes == AFILTER_RESULT_OUT_LIMIT_THR)
            {
                /* Filter reports a change */
                if( ((i==THR_POINT_LOLO) && (region == HIHILOLOFilter::REGION_LOLO)) ||
                    ((i==THR_POINT_LO  ) && (region == HIHILOLOFilter::REGION_LO)) ||
                    ((i==THR_POINT_HI  ) && (region == HIHILOLOFilter::REGION_HI)) ||
                    ((i==THR_POINT_HIHI) && (region == HIHILOLOFilter::REGION_HIHI))
                    )
                {
                    /* Limit trespassed to its associated region: report it */
                    if(conf.binOutEnabled[i] == LU_TRUE)
                    {
                        setValue(i, 1, timeStamp, thrFlags);
                        clLocalPoint[i] = true;
                        applyUpdateFlags = false;
                    }
                    LOG_THRESHOLD_TRIGGERD;
                }
                else
                {
                    /* This point is not set: check if it should go back to 0 */
                    if(clLocalPoint[i])
                    {
                        //Was 1, must set to 0
                        if(conf.binOutEnabled[i] == LU_TRUE)
                        {
                            setValue(i, 0, timeStamp, thrFlags);
                            clLocalPoint[i] = false;
                            applyUpdateFlags = false;
                        }
                        LOG_THRESHOLD_RECOVERED;
                    }
                }
            }
        }
        if( applyUpdateFlags && (conf.binOutEnabled[i] == LU_TRUE) )
        {
            //Update only the flags if needed and not already done
            setFlags(i, thrFlags, timeStamp);
        }
    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
