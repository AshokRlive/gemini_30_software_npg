/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: DigitalControlPointLogic.h 2 Sep 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/CLogic/include/ControlLogic/DigitalControlPointLogic.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Digital Control Point control logic.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 2 Sep 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   2 Sep 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef DIGITALCONTROLPOINTLOGIC_H__INCLUDED
#define DIGITALCONTROLPOINTLOGIC_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "MCMConfigEnum.h"  //For EDGE_DRIVEN_MODE
#include "OperatableControlLogic.h"
#include "ControlLogicDigitalPoint.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Converts a Control Logic operation command to a digital Virtual Point
 *
 * This class sets the value of a single binary pseudo point when operated.
 */
class DigitalControlPointLogic: public OperatableControlLogic
{
public:
    struct Config : public OperatableControlLogic::Config
    {
    public:
        /* Amounts of different types of Control Logic's Pseudo Points */
        static const lu_uint16_t BINARYPOINT_NUM = DIGCTRL_POINT_TYPE_BINARY_COUNT;
        static const lu_uint16_t ALLPOINT_NUM =  DIGCTRL_POINT_EnumSize;
    public:
        bool pulseEnabled; //Generate a pulse instead of a level change
        lu_uint32_t pulseDuration_ms; //Generate a pulse instead of a level change
        EDGE_DRIVEN_MODE trigger;  //Event that generates the pulse, if enabled
        DigitalPoint::Config bPoint[BINARYPOINT_NUM];   //single binary CLogic virtual points
    };

public:
    DigitalControlPointLogic(Mutex* mutex                 ,
                             GeminiDatabase& database     ,
                             DigitalControlPointLogic::Config& conf
                           );
    virtual ~DigitalControlPointLogic();

    /* == Inherited from ControlLogic == */
    virtual GDB_ERROR init();
    virtual GDB_ERROR getInputValue(lu_uint16_t inputPoint, PointData* data);

protected:
    /* == Implements mandatory OperatableControlLogic == */
    virtual GDB_ERROR startOp(const SwitchLogicOperation &operation, const GDB_ERROR errorCode);

    /**
     * \brief Set the output back to 0 to finish the pulse
     *
     * \param timeStamp Time stamp of the event
     */
    void endPulse(TimeManager::TimeStr& timeStamp);

private:
    /**
     * \brief One-shot job for pulse generation only
     */
    class PulseEnd : public TimedJob
    {
    public:
        PulseEnd(DigitalControlPointLogic& cLogic, const lu_uint32_t timeout):
                                                    TimedJob(timeout, 0),
                                                    m_cLogic(cLogic)
        {};
        virtual ~PulseEnd() {};

    protected:
        /* == Inherited from TimedJob == */
        virtual void job(TimeManager::TimeStr *timePtr)
        {
            m_cLogic.endPulse(*timePtr);
        }

    private:
        DigitalControlPointLogic& m_cLogic;   //Reference to the Control Logic
    };

private:
    DigitalControlPointLogic::Config conf;
    PulseEnd* pulseEndJob;  //Database Job to finish the pulse generation
};


#endif /* DIGITALCONTROLPOINTLOGIC_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
