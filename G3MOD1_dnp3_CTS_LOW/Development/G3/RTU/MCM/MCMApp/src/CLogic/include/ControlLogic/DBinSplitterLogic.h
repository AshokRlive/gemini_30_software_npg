/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: DBinSplitterLogic.h 13 Sep 2016 pueyos_a $
 *               $HeadURL: http://10.11.0.6:81/svn/gemini_30_software/trunk/Development/G3/RTU/MCM/MCMApp/src/CLogic/include/ControlLogic/DBinSplitterLogic.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Double Binary VP to Single Binaries Logic
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 13 Sep 2016 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13 Sep 2016   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef DBINSPLITTERLOGIC_H__INCLUDED
#define DBINSPLITTERLOGIC_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "ControlLogic.h"
#include "ControlLogicDigitalPoint.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Takes a Double Binary value and split it in Single Binary points
 *
 * This takes a Double Binary point and offers its 4 possible values as Single
 * Binary indications.
 */
class DBinSplitterLogic: public ControlLogic
{
public:
    /**
     * \brief configuration block
     */
    struct Config : public ControlLogicConf
    {
    public:
        /* Amounts of different types of Control Logic's Pseudo Points */
        static const lu_uint16_t SBPOINT_NUM = DTS_POINT_TYPE_BINARY_COUNT;
        static const lu_uint16_t ALLPOINT_NUM = SBPOINT_NUM;
    public:
        PointIdStr dbInput;             //Double Binary Input point
        ControlLogicDigitalPoint::Config bPoint[SBPOINT_NUM]; //CLogic single bin Points
    };
public:
    DBinSplitterLogic(Mutex* mutex, GeminiDatabase& database, DBinSplitterLogic::Config& config);
    virtual ~DBinSplitterLogic();

    /* == Inherited from ControlLogic == */
    virtual GDB_ERROR init();
    virtual GDB_ERROR getInputValue(lu_uint16_t inputPoint, PointData *data);

protected:
    /* == Inherited from ControlLogic == */
    virtual void newData(PointIdStr pointID, PointData& pointData);

private:
    DIObserver dbInput;    //Input point
};


#endif /* DBINSPLITTERLOGIC_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
