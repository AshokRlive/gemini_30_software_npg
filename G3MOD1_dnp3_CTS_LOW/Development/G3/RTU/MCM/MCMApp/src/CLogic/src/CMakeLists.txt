cmake_minimum_required(VERSION 2.8)

add_library( CLogic STATIC
             ControlLogicDigitalPoint.cpp
             ControlLogicAnaloguePoint.cpp
             ControlLogicCounterPoint.cpp
#            DummyControlLogicFactory.cpp
             SwitchAuthority.cpp
           ) 

ADD_DEPENDENCIES(CLogic globalHeaders)

# MCMApp common includes
include_GeneralHeaders()

include_directories(${CMAKE_G3_PATH_COMMON_LIBRARY}/util/include/)
include_directories(${CMAKE_G3_PATH_MCM_COMMONLIB}/LEDMirror/include/)

include_directories(../include/)
include_directories(../include/ControlLogic/)

# DBEngine Interface include
include_directories(../../DBEngine/include/)

# Virtual Point Interface include
include_directories(../../Points/include/)

# IOModule Interface include
include_directories(../../IOModule/include/)

# CANIOModule Interface include (Used for dummyFactoryChannel.h)
include_directories(../../CANIOModule/include/)

# Memory Manager
include_directories(../../MemoryManager/include/)

# Status Manager
include_directories(../../StatusManager/include/)

#G3 protocol
include_directories(../../ModuleProtocol/include/)


# Generate Doxygen documentation
#gen_doxygen("CLogic" "")
