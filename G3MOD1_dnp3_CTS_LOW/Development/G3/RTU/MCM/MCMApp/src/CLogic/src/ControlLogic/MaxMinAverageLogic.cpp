/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: MaxMinAverageLogic.cpp 28 Jun 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/CLogic/src/MaxMinAverageLogic.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Maximum, minimum, and average values calculation Control Logic.
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 28 Jun 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   28 Jun 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <limits>
#include <assert.h>
#include <math.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "MaxMinAverageLogic.h"
#include "dbg.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/* Debug */
#define DEBUG_MAXMINAVG     0
#define SIMULATE_MAXMINAVG  0

#if DEBUG_MAXMINAVG
#define DBG(M,...)      DBG_INFO("MaxMinAvg::" M,##__VA_ARGS__)
#define DBG_FUNC(M,...) DBG_INFO("MaxMinAvg::========>>"M,##__VA_ARGS__)
#else
#define DBG(M,...)
#define DBG_FUNC(M,...)
#endif

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
MaxMinAverageLogic::MaxMinAverageLogic( Mutex* mutex,
                                        GeminiDatabase& g3database,
                                        MaxMinAverageLogic::Config& config
                                        ) :
                                        ControlLogic(mutex, g3database, config, CONTROL_LOGIC_TYPE_MMAVG),
                                        operationConfig(config.operationParams),
                                        rawValue(std::numeric_limits<lu_float32_t>::quiet_NaN()),
                                        rawValueValid(false),
                                        resultValid(false),
                                        maxValue(rawValue),
                                        minValue(rawValue),
                                        avgValue(0),
                                        numSamples(0),
                                        publishJob(NULL),
                                        tickJob(NULL),
                                        startJob(NULL)
{
    /* Allocate Control Logic virtual points */
    clPoint.reserve(config.allPointNum);  //Adjust vector size to do not waste memory

    TimeManager::TimeStr curTime = timeManager.now();
    counterFlags.setOnline(enabled);
    analogueFlags.setOnline(false);     //Will be updated later

    for(lu_uint32_t id = 0; id < Config::ANALOGUEPOINT_NUM; ++id)
    {
        clPoint.push_back(new ControlLogicAnaloguePoint(mutex, database, config.aPoint[id]));
    }

    if(config.operationParams.supportCounter == true)
    {
        for(lu_uint32_t id = 0; id < Config::COUNTERPOINT_NUM; ++id)
        {
            clPoint.push_back(new ControlLogicCounterPoint(mutex, database, config.cPoint[id]));
        }
    }

    /* Create input points */
    for(lu_uint32_t i = 0; i < MMAVG_INPUT_LAST; ++i)
    {
        aObservers[i] = new AIObserver(config.aInput[i], *this);
    }

    /* Set initial online/offline status */
    for(lu_uint32_t i = 0; i < getPointNum(); ++i)
    {
        if(clPoint[i]->getType() == POINT_TYPE_COUNTER)
        {
            setFlags(i, counterFlags, curTime);
        }
        else
        {
            setFlags(i, analogueFlags, curTime);
        }
    }
}


MaxMinAverageLogic::~MaxMinAverageLogic()
{
    stopUpdate();

    timeManager.detachMinute(this);

    /* Cancel related jobs */
    database.cancelJob(tickJob);
    database.cancelJob(publishJob);
    database.cancelJob(startJob);

    /* Release observers */
    for(lu_uint32_t i = 0; i < MMAVG_INPUT_LAST; ++i)
    {
        detachObserver(aObservers[i]);
    }
}


GDB_ERROR MaxMinAverageLogic::init()
{
    GDB_ERROR ret = initCheck();
    if(ret == GDB_ERROR_NONE)
    {
        ret = activateCLPoints();   //Initialise Control Logic points as well
    }
    if(ret != GDB_ERROR_NONE)
    {
        return ret;
    }

    if(operationConfig.periodTime_s == 0)
    {
        log.warn("%s - Invalid MaxMinAvg period time:0, forced now to 1 minute (default)",
                this->toString().c_str());
        operationConfig.periodTime_s = 60;
    }

    startCLPoints(true);
    initialised = true;

    /* Initialise input points */
    for (lu_uint32_t i = 0; i < MMAVG_INPUT_LAST; ++i) //List of action inputs
    {
        GDB_ERROR res = attachObserver(aObservers[i]);
        if(res != GDB_ERROR_NONE)
        {
            ret = GDB_ERROR_NOT_INITIALIZED;
        }
    }

    if(ret != GDB_ERROR_NONE)
    {
        //Detach all already attached and disable Control Logic indications
        initialised = false;
        for (lu_uint32_t i = 0; i < MMAVG_INPUT_LAST; ++i)
        {
            detachObserver(aObservers[i]);
        }
        startCLPoints(false);
    }
    else
    {
        /* Start sampling after a delayed job */
        startJob = new MaxMinAverageLogic::StartDelayJob(*this, operationConfig.startDelay_s);
        ret = database.addJob(startJob);
        if(ret != GDB_ERROR_NONE)
        {
            log.error("%s: unable to perform delayed start of periodic job - starting now!. Error: %s",
                        this->toString().c_str(), GDB_ERROR_ToSTRING(ret)
                      );
            ret = GDB_ERROR_NONE;
            startSampling();
        }
    }
    return ret;
}


GDB_ERROR MaxMinAverageLogic::getInputValue(lu_uint16_t inputPoint, PointData* data)
{
    if( (data == NULL) ||
        (inputPoint > MMAVG_INPUT_LAST)
      )
    {
        return GDB_ERROR_PARAM;
    }

    if(aObservers[inputPoint] != NULL)
    {
        return database.getValue(aObservers[inputPoint]->getPointID(), data);
    }
    else
    {
        return GDB_ERROR_NOPOINT;
    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
void MaxMinAverageLogic::newData(PointIdStr pointID, PointData& pointData)
{
#if SIMULATE_MAXMINAVG
    return;
#endif

    if(!enabled)
        return;

    DBG_FUNC("newData");

    PointIdStr point = aObservers[0]->getPointID();
    if( (pointID.group != point.group) || (pointID.ID != point.ID) )
        return;

    TimeManager::TimeStr currentTime = timeManager.now();

    {//Mutex critical section
        MasterLockingMutex lock(pMutex);

        /* Update max, min, average values */
        rawValue = (lu_float32_t)pointData;
        const PointFlags newFlags = pointData.getFlags();
        if( newFlags.isActive() && (newFlags.isOverflow() == false) )
        {
            rawValueValid = true;
        }
        else
        {
            //Point becomes offline/inactive/overflow. Next results are invalid
            if(resultValid)
            {
                DBG("Results invalidated");
            }
            rawValueValid = false;
            resultValid = false;
        }

        DBG("newData value: %f %s%s",rawValue,
                                    (!rawValueValid)? "(invalid)" : "",
                                    (!initialised)? " - Not used" : "");

        if(!initialised)
            return;


        /* Check online/offline change */
        if(newFlags.getQuality() != analogueFlags.getQuality())
        {
            /* Set pseudo point's flags as the same of the input point */
            PointFlags qFlags = newFlags;
            for(lu_uint32_t i = 0; i < getPointNum(); ++i)
            {
                /* Set invalid when result is invalid (was offline/invalid during the sampling) */
                qFlags.setInvalid(!resultValid);
                if(clPoint[i]->getType() == POINT_TYPE_COUNTER)
                {
                    qFlags.setOnline(true);
                    //Note: Counters are never updating flags outside publish()
                }
                else
                {
                    qFlags.setOnline(newFlags.isOnline());
                    setFlags(i, qFlags, currentTime);
                }
                DBG("Pseudo Point %i online flag change %s => %s",i, analogueFlags.toString().c_str(), qFlags.toString().c_str());
            }
            analogueFlags.setOnline(newFlags.isOnline());
        }
    }
}


void MaxMinAverageLogic::updateTime(TimeManager::TimeStr& currentTime)
{
    //On-the-minute update
    if( (operationConfig.intervalType == OperationParams::INTERVAL_OCLOCK)
        && (!currentTime.badTime)
        )
    {
        //Separate if in order to avoid calculation when not necessary
        if( (currentTime.time.tv_sec % operationConfig.periodTime_s) == 0)
        {
            /* Minute matches with the configured time */
            publish(currentTime);
        }
    }
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void MaxMinAverageLogic::startSampling()
{
    DBG_FUNC("startSampling after %i s", operationConfig.startDelay_s);

    GDB_ERROR ret;

    /* Start delay finished */
    startJob = NULL;    //Unlink job

    if(rawValueValid)
    {
        /* When the input is online after publish, the new calculation might be valid */
        resultValid = true;
        DBG("Result is going to be valid from startup");
    }
    /* Reset values to current value and consider it as the first sample if it is valid*/
    minValue = rawValue;
    maxValue = rawValue;
    avgValue = rawValue;
    numSamples = (rawValueValid)? 1 : 0;

    /* schedule a job to publish min,max,avg values */
    switch(operationConfig.intervalType)
    {
        case OperationParams::INTERVAL_OCLOCK:
        {
            if(timeManager.attachMinute(this) != 0)
            {
                log.error("%s, Unable to attach to the minute tick",
                          toString().c_str());
                return;
            }
        }
        break;
        case OperationParams::INTERVAL_PERIOD:
        default:
        {
            publishJob = new MMAvgPublishJob(*this, operationConfig.periodTime_s);
            ret = database.addJob(publishJob);
            if(ret != GDB_ERROR_NONE)
            {
                log.error("%s: unable to start publishing periodic job - feature disabled. Error: %s",
                            this->toString().c_str(), GDB_ERROR_ToSTRING(ret)
                          );
                return; //does not set initialised to true
            }
        }
        break;
    }

    tickJob = new MMAvgTickJob(*this, operationConfig.samplingRate_ms);
    ret = database.addJob(tickJob);

    if(ret != GDB_ERROR_NONE)
    {
        log.error("%s: unable to start periodic job - feature disabled. Error: %s",
                    this->toString().c_str(), GDB_ERROR_ToSTRING(ret)
                  );
        return; //does not set initialised to true
    }
    initialised = true; //start sampling now
}


void MaxMinAverageLogic::publish(TimeManager::TimeStr& currentTime)
{
    DBG_FUNC("%s publish ",currentTime.timeString().c_str());

    MasterLockingMutex lock(pMutex, LU_TRUE);

    /* Set invalid when result is invalid (was offline/invalid during the sampling) */
    analogueFlags.setInvalid(!resultValid);
    counterFlags.setInvalid(!resultValid);
    counterFlags.setOnline(true);
    DBG("Points are %svalid", (resultValid)? "" : "in");

    /* Publish results as analogue values */
    setValue(MMAVG_POINT_A_MIN, minValue, currentTime, analogueFlags, LU_TRUE);
    setValue(MMAVG_POINT_A_MAX, maxValue, currentTime, analogueFlags, LU_TRUE);
    setValue(MMAVG_POINT_A_AVG, avgValue, currentTime, analogueFlags, LU_TRUE);

    if (operationConfig.supportCounter == true)
    {
        /* Publish results as counters */
        CountValue cValue;
        cValue = floatToUInt(minValue);
        counterFlags.setOverflow(cValue.overflow);
        setValue(MMAVG_POINT_C_MIN, cValue.value, currentTime, counterFlags, LU_TRUE);
        cValue = floatToUInt(maxValue);
        counterFlags.setOverflow(cValue.overflow);
        setValue(MMAVG_POINT_C_MAX, cValue.value, currentTime, counterFlags, LU_TRUE);
        cValue = floatToUInt(avgValue);
        counterFlags.setOverflow(cValue.overflow);
        setValue(MMAVG_POINT_C_AVG, cValue.value, currentTime, counterFlags, LU_TRUE);
    }

#if SIMULATE_MAXMINAVG
    return;
#endif

    /* Reset values to current value and consider it as the first sample if it is valid*/
    minValue = rawValue;
    maxValue = rawValue;
    avgValue = rawValue;
    numSamples = (rawValueValid)? 1 : 0;
    if(rawValueValid)
    {
        /* When the input is online after publish, the new calculation might be valid */
        resultValid = true;
        DBG("Result is going to be valid");
    }

}


void MaxMinAverageLogic::tickEvent(TimeManager::TimeStr& currentTime)
{

#if SIMULATE_MAXMINAVG
    simulate();
    publish(currentTime);
    return;
#endif

    LU_UNUSED(currentTime);
    MasterLockingMutex lock(pMutex, LU_TRUE);

    /* Get a sample and update max, min, average values */

    maxValue = LU_MAX(maxValue, rawValue);
    minValue = LU_MIN(minValue, rawValue);

    /* Incremental average calculation: An1 = An + ( (Vn1 - An) / (n + 1) ) */
    avgValue += (rawValue - avgValue) / (lu_float32_t)(++numSamples);
    DBG("averageValue changed to %f", avgValue);
}


MaxMinAverageLogic::CountValue MaxMinAverageLogic::floatToUInt(const lu_float32_t value)
{
    lu_float32_t calc;      //Store scaled value
    lu_int32_t res;         //Store signed result
    CountValue ret;         //Store final converted value

    calc = value * operationConfig.scale;   //Apply conversion scaling

    if(isnanf(calc))
    {
        ret.overflow = LU_TRUE;
        ret.value = 0;
        DBG("%s floatToUInt scale:%f - %f->NaN* (%uU)",this->toString().c_str(),operationConfig.scale,value,ret.value);
        return ret;
    }

    res = (lu_int32_t) (calc + ((calc < 0)? -0.5 : 0.5));  //Round to nearest integer
    ret.value = (lu_uint32_t)(res); //Store in an unsigned int
    ret.overflow = ((calc > std::numeric_limits<lu_int32_t>::max()) ||
                    (calc < std::numeric_limits<lu_int32_t>::min()) )? LU_TRUE : LU_FALSE;
    DBG("%s floatToUInt scale:%f - %f->%i%s (%uU)",this->toString().c_str(),operationConfig.scale,value,(lu_int16_t)res,(ret.overflow)?"*":"",ret.value);
    return ret;
}


void MaxMinAverageLogic::simulate()
{
#if SIMULATE_MAXMINAVG
       numSamples = 1;
       static const int SCALES = 6;
       static int scaling = SCALES+1;
       static lu_float32_t sc[SCALES] = {
                       50, 10000, 1000, 100, 0.1, 1
       };
       static const int SAMPLES = 15;
       static int count = 0;
       static lu_float32_t v[SAMPLES][2] = {
                       {100000.9, -199999.88},
                       {32767, 32767.1},
                       {32768, -32768.1},
                       {33000, 33000.2},
                       {10999.9, -10999.33},
                       {10199.9, -10199.1},
                       {10000.0, -10000.78},
                       {1000.0, -1000.78},
                       {100.0, -100.78},
                       {3.4027, 3.4022},
                       {3.64, 3.66},
                       {0.7278, 0.7274},
                       {0.40278, -0.40274},
                       {0.9, -0.19},
                       {1.0, 0.0}
                   };

       if(scaling > 0)
       {
           if(count <= 0)
           {
               scaling--;
               count = SAMPLES;
               printf("New Scale = %f\n", sc[scaling-1]);
               operationConfig.scale = sc[scaling-1];
           }

           if(count > 0)
           {
               count--;
               minValue = -(v[count][0]);
               maxValue = v[count][0];
               avgValue = v[count][1];
           }
       }
       else
       {
           DBG("=== FINIS CORONAT OPUS ===\n");
       }

#endif
}


/*
 *********************** End of file ******************************************
 */
