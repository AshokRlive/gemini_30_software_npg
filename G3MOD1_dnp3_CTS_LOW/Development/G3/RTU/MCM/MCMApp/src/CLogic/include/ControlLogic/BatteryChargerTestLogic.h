/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: BatteryChargerTestLogic.cpp 21-Nov-2012 10:18:39 pueyos_a $
 *               $HeadURL: Y:\workspace\G3Trunk\G3\RTU\MCM\src\CLogic\src\BatteryChargerTestLogic.cpp $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       BatteryChargerTestLogic header module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 21-Nov-2012 10:18:39	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   21-Nov-2012 10:18:39  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_FB4856AF_6BB5_4242_866B_434221528289__INCLUDED_)
#define EA_FB4856AF_6BB5_4242_866B_434221528289__INCLUDED_



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IChannel.h"
#include "OperatableControlLogic.h"
#include "IStatusManager.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * Class BatteryChargerTestLogic
 *
 * This class controls the behaviour of the Battery Test Control Logic in all
 * of its flavours (PSM_CH_BCHARGER_*).
 *
 */
class BatteryChargerTestLogic : public OperatableControlLogic
{
public:
    /**
     * \brief Battery Charger Test logic control Block
     */
    struct OpParams
    {
    public:
        lu_uint16_t duration;      /* Test duration - may be the "Maximum" duration */
    };

    struct Config : public OperatableControlLogic::Config
    {
    public:
        OpParams opParams;      /* Control Logic operation parameters */
        IChannel* channel;      /* Reference to the output channel */
        ChannelRef channelRef;  /* Channel ID (for event report only) */
    };

public:
    /**
     * \brief Custom constructor
     *
     * \param mutex Reference to the G3DB common mutex
     * \param database Reference to the Gemini database
     * \param config reference to the Control Logic's configuration block
     */
	BatteryChargerTestLogic(Mutex *mutex                 ,
                            GeminiDatabase& database     ,
                            BatteryChargerTestLogic::Config &conf
                            );
	virtual ~BatteryChargerTestLogic();

    /* == inherited from ControlLogic == */
	virtual GDB_ERROR init();
    virtual GDB_ERROR getInputValue(lu_uint16_t inputPoint, PointData *data);
    virtual GDB_ERROR getOutputValue(PointData* data);

protected:
    /* == Implements mandatory OperatableControlLogic == */
    virtual GDB_ERROR startOp(const SwitchLogicOperation &operation, const GDB_ERROR errorCode);

private:
    /**
     * \brief Check common preliminary conditions that can inhibit the operation
     *
     * \return Error code. GDB_ERROR_NONE when all preliminary checks passed.
     */
    GDB_ERROR preliminaryChecks();

protected:
    typedef ControlLogic::OperationJob<BatteryChargerTestLogic> SendOperateCmds;
    //Note that "friend class SendOperateCmds" is not working in GCC < 4.5.7
    friend class ControlLogic::OperationJob<BatteryChargerTestLogic>;

    /* Implement method needed for ControlLogic::OperationJob */
    void runOperation(SendOperateCmds& operationJob, const bool delayed);

private:
    OpParams opParams;     /* Control Logic operation parameters */
    IChannel* battChannel;
    ChannelRef battChannelRef;  /* Channel ID */

    struct timespec acceptedOperationTime;  //tracks operation time for operation timeout
    SwitchLogicOperation m_operation;   //Requested operation to perform
};

#endif // !defined(EA_FB4856AF_6BB5_4242_866B_434221528289__INCLUDED_)

/*
 *********************** End of file ******************************************
 */

