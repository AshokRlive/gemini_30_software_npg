/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Control logic public interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_77428455_DD72_419b_BC83_389729B0D9DE__INCLUDED_)
#define EA_77428455_DD72_419b_BC83_389729B0D9DE__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <vector>
#include <stdexcept>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "timeOperations.h"
#include "ControlLogicDef.h"
#include "ControlLogicCommon.h"
#include "GeminiDatabase.h"
#include "IPoint.h"
#include "IPointObserver.h"
#include "Mutex.h"
#include "Logger.h"
#include "IEventLogManager.h"
#include "TimeManager.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Virtual container for the control logic
 *
 * This is a base class for every Control Logic. Provides basic common
 * functionality and a common interface.
 */
class ControlLogic
{
public:
    /**
     * \brief Control Logic's input point reference
     */
    struct PointRef
    {
    public:
        PointIdStr pointID; //ID of the Virtual Point acting as a source of the input
        bool mandatory;     //States whether this input is mandatory or optional
        lu_uint32_t index;  //Index of the Control Logic Input (xxx_INPUT_xxx)
    public:
        PointRef() : mandatory(false)   //by default, all inputs are optional
        {};
    };

    /**
     * \brief binary status for double binary points
     */
    typedef enum
    {
        DBINARY_INTERMEDIATE  = 0,
        DBINARY_OFF           = 1,
        DBINARY_ON            = 2,
        DBINARY_INDETERMINATE = 3
    }DBINARY_STATUS;

    /**
     * \brief Common Valid and Invalid flag sets
     */
    static const PointFlags ValidFlags;     //online, valid, not initial
    static const PointFlags InvalidFlags;   //offline, invalid, and initial
    static const PointFlags InitialFlags;   //ONLINE, invalid, and initial


    /**
     * \brief Get the Open value from a DBINARY_STATUS.
     *
     * \param status DBINARY_STATUS to be interpreted.
     *
     * \return LU_TRUE when Open status
     */
    inline lu_bool_t getDBINARYOpenValue(DBINARY_STATUS status)
    {
        switch(status)
        {
            case DBINARY_OFF:
            case DBINARY_INDETERMINATE:
                return LU_TRUE;
            case DBINARY_ON:
            case DBINARY_INTERMEDIATE:
                return LU_FALSE;
            default:
                return LU_FALSE;
        }
    }

    /**
     * \brief Get the Close value from a DBINARY_STATUS.
     *
     * \param status DBINARY_STATUS to be interpreted.
     *
     * \return LU_TRUE when Close status
     */
    inline lu_bool_t getDBINARYCloseValue(DBINARY_STATUS status)
    {
        switch(status)
        {
            case DBINARY_ON:
            case DBINARY_INDETERMINATE:
                return LU_TRUE;
            case DBINARY_OFF:
            case DBINARY_INTERMEDIATE:
                return LU_FALSE;
            default:
                return LU_FALSE;
        }
    }

public:
    /**
	 * \brief Default constructor
	 * 
	 * \param mutex Global mutex
	 * \param database database reference
	 * \param config Basic Control Logic configuration
	 * \param CLType Control Logic type
	 * 
	 * \return None
	 */
    ControlLogic(Mutex *mutex,
                GeminiDatabase& database,
                const ControlLogicConf config,
                const CONTROL_LOGIC_TYPE CLtype
                );

    virtual ~ControlLogic();

    /**
     * \brief Start up the Control Logic
     *
     * Note that the usual expected initialisation order is detailed here, but
     * some Control Logics may need to modify it to its needs:
     *  1) Check if the logic is not initialised and enabled with initCheck()
     *  2) Do any Logic-specific initialisation, such as timers or channels
     *  3) Activate Control Logic indications (pseudo-points) with startCLPoints(true)
     *  4) Set the Logic as initialised (yes, even when is not really finished)
     *  5) Attach observers to the input points, with initInhibit() and attachObserver()
     *  6) If the attach failed:
     *      6.1) set the Logic as NOT initialised
     *      6.2) detach all the observers, cancel jobs
     *      6.3) Deactivate the Control Logic indications with startCLPoints(false)
     *  7) when success, Start timers and jobs
     *
     * All this procedure is done in order to prevent a race condition when
     * starting up the Logic points:
     *  - If the observers are attached and later the points are activated, then
     *    the newData() update from the attached inputs will be lost;
     *  - If the points are activated first, then the observers are attached,
     *    and then the flags are set afterwards, the points' flags may be
     *    overwritten after the newData() update, causing for example that some
     *    points show up as "invalid" when they are not.
     *
     * \return Error code
     */
    virtual GDB_ERROR init() = 0;

    /**
     * \brief Get the control logic ID (groupID)
     *
     * \return Control logic ID
     */
    inline lu_uint16_t getID() {return groupID;};

    /**
     * \brief Get control logic type
     *
     * \return Control logic type
     */
    virtual CONTROL_LOGIC_TYPE getType() { return cLogicType; };

    /**
     * \brief Get if the Control Logic can be operated
     *
     * \return "operatability" of the Control Logic
     */
    virtual bool isOperatable() { return false; };

    /**
     * \brief Get control logic ID in a string
     *
     * \return Control logic ID in string format
     */
    virtual std::string toString();

    /**
     * \brief Get pointer to the Logic Point (pseudo-point).
     *
     * \param pointID ID of the point.
     *
     * \return Pointer to the Virtual Point, or NULL when error/if not found.
     */
    virtual IPoint* getPoint(PointIdStr pointID);

    /**
     * \brief Get amount of Pseudo Points in the Control Logic
     *
     * \return Total amount of Pseudo Points in this Control Logic
     */
    virtual lu_uint32_t getPointNum() { return clPoint.size(); };

    /**
     * \brief Get the type of the Virtual Point
     *
     * \param PointID Point ID
     *
     * \return Point type, POINT_TYPE_INVALID if not available
     */
    virtual POINT_TYPE getPointType(PointIdStr pointID);

    /**
     * \brief Get the value & status of a input embedded virtual point
     *
     * \param inputPoint Input point ID
     * \param data Buffer where the value is saved
     *
     * \return Error Code
     */
    virtual GDB_ERROR getInputValue(lu_uint16_t inputPoint, PointData* data) = 0;

    /**
     * \brief Get the value & status of an output
     *
     * Note that this involves the output, that is mostly output channels, and
     * a Control Logic may have more than one.
     *
     * \param data Buffer where the output value & status is saved
     *
     * \return Error Code
     */
    virtual GDB_ERROR getOutputValue(PointData* data);

    /**
	 * \brief Change the Off/Local/Remote state of the system
	 * 
	 * The backplane local/remote lines are set and then the internal virtual points
	 * are updated
	 * 
	 * \param status New Off/Local/Remote status
     * \param source Element from where the action was originated
     * \param force Optional parameter: set to LU_TRUE to force OLR change
	 * 
	 * \return Error Code
	 */
	virtual GDB_ERROR setLocalRemote(OLR_STATE status, OLR_SOURCE source);

    /**
     * \brief Start a Control Logic's operation
     *
     * Use to operate a Switch or a Digital Output.
     * If the preliminary checks are passed the operation is started in the background
     * and the function returns immediately with an error code.
     *
     * \param operation Operation to perform: open or close
     * \param local true if the operation is local false if it is remote
     *
     * \return Error Code
     */
    virtual GDB_ERROR startOperation(SwitchLogicOperation &operation);

    /**
     * \brief Stop Updates
     *
     * Stop updates and detach observers
     *
     * \param none
     *
     * \return none
     */
    virtual void stopUpdate();

protected:
    /**
     * \brief Inner class for common point observer code
     */
    /**
     * \brief Digital virtual points observer
     *
     * This inner class implements common point observer code.
     * Observes over a designed virtual point, reporting any change to the
     * associated Control Logic. It stores some data locally.
     */
    template <typename T>
    class PointObserver : public IPointObserver
    {
    public:
        PointObserver(PointIdStr pointID, ControlLogic& cLogic) :
                                                               pointID(pointID),
                                                               cLogic(cLogic),
                                                               value(0)
        {};
        virtual ~PointObserver() {};

        /**
         * \brief Get the point quality
         *
         * For performance the local copy is returned
         *
         * \return Observed virtual point quality
         */
        inline POINT_QUALITY getQuality() { return flags.getQuality(); };
        inline PointFlags getFlags() { return flags; };
        inline bool isActive() { return flags.isActive(); };

        /* == Inherited from IPointObserver == */
        inline PointIdStr getPointID() { return pointID; }
        virtual void update(PointIdStr pointID, PointData* pointDataPtr)
        {
            //Default behaviour
            value = *pointDataPtr;              //Store the new value locally
            flags = pointDataPtr->getFlags();   //Store the new flags locally

            cLogic.newData(pointID, *pointDataPtr);    /* Notify control logic that an input has changed */
        }

        /**
         * \brief Get the point value
         *
         * For performance the local copy is returned
         *
         * \return Observed virtual point value
         */
        inline T getValue() {return value;}

    protected:
        PointIdStr pointID;     //Point ID
        ControlLogic& cLogic;   //Control Logic to be reported when point changes
        PointFlags flags;       //Local copy stored for performance reasons
        T value;                //Local copy stored for performance reasons
    };

    /* Alias for different PointData types */
    typedef PointObserver<lu_uint8_t> DIObserver;
    typedef PointObserver<lu_float32_t> AIObserver;
    typedef PointObserver<lu_uint32_t> CObserver;

    /**
     * \brief Operation job
     *
     * Generic class for starting and using a G3DB Job.
     */
    template <typename CLogicT>
    class OperationJob : public IJob
    {
    public:
        static const lu_uint32_t MAXDELAY_MS = 1500;
        static const lu_uint32_t MAXDELAY_NONE = 0;
    public:
        /**
         * \brief Custom constructor
         *
         * The job is created and could be inhibited if it takes too much time
         * to be executed by the scheduler. This avoids starting an operation
         * way too much later than intended.
         *
         * \param cLogic Reference to the control logic to notify
         * \param maxDelay Maximum operation delay allowed.
         */
        OperationJob(CLogicT& cLogic,
                     const lu_uint32_t maxDelay = OperationJob::MAXDELAY_MS) :
                            m_cLogic(cLogic),
                            m_OperationMaxDelay_ms(maxDelay)
        {
//            log();
            clock_gettimespec(&m_acceptedOpTime);   //time stamp the creation of the job
        };
        virtual ~OperationJob() {};

    protected:
        /**
         * \brief Method executed by the job scheduler
         *
         * Note that the Control Logic that requires to use this must implement
         * a method like:
         *      void runOperation(ControlLogic::OperationJob<DigitalOutputLogic>& operationJob,
         *                          const bool delayed);
         * and use the parameter to know which job called in case of having
         * several of these jobs.
         *
         * And do not forget to friend it if implemented as protected:
         *      friend class ControlLogic::OperationJob<DigitalOutputLogic>;
         */
        virtual void run()
        {
            bool delayed = false;
            if(m_OperationMaxDelay_ms > 0)
            {
                /* check that the operation has not been delayed too much by the
                 * database job scheduler.
                 */
                struct timespec timeNow;
                clock_gettimespec(&timeNow);
                lu_uint64_t operationDelay = timespec_elapsed_ms(&m_acceptedOpTime, &timeNow);
                if(operationDelay > m_OperationMaxDelay_ms)
                {
                    Logger::getLogger(SUBSYSTEM_ID_CLOGIC).error(
                                "%s - operation delayed excessively. Delay: %dms - Maximum: %ulms",
                                m_cLogic.toString().c_str(), operationDelay, m_OperationMaxDelay_ms
                              );
                    delayed = true;
                }
            }
            m_cLogic.runOperation(*this, delayed);
        }
    private:
        CLogicT& m_cLogic;
//        Logger& log;
        lu_uint32_t m_OperationMaxDelay_ms;
        struct timespec m_acceptedOpTime;
    };

protected:
    /**
     * \brief Notification of a change in an input
     *
     * This method is intended to be called when an observer notifies a change
     * in a Control Logic input.
     *
     * \param pointID Point ID
     * \param pointData New data of the point (reference for faster link)
     */
    virtual void newData(PointIdStr pointID, PointData& pointData);

    /**
     * \brief Set the value of a Control Logic Pseudo Point
     *
     * This overloaded method allows to set a value in a Control Logic Point
     * by specifying its index and a proper value.
     *
     * \param CLpointID Control Logic Point ID (Pseudo point index)
     * \param value Value to set (type should match the point type)
     * \param timestamp Time stamp of the point change
     * \param flags Point flags to set (optional)
     * \param forced Set to LU_TRUE to force point update even if it is the same (optional)
     *
     * \return Error code
     */
    GDB_ERROR setValue( IPoint& CLpointID,
                        const lu_uint32_t value,
                        const TimeManager::TimeStr& timestamp,
                        const PointFlags& flags,
                        const lu_bool_t forced = LU_FALSE
                        );
    GDB_ERROR setValue( IPoint& CLpointID,
                        const lu_float32_t value,
                        const TimeManager::TimeStr& timestamp,
                        const PointFlags& flags,
                        const lu_bool_t forced = LU_FALSE
                        );
    template <typename T>
    GDB_ERROR setValue( const lu_uint32_t CLpointID,
                        const T value,
                        const TimeManager::TimeStr& timestamp,
                        const lu_bool_t forced = LU_FALSE
                        )
    {
        try
        {
            IPoint* pointCL = clPoint.at(CLpointID);
            return setValue(CLpointID, value, timestamp, pointCL->getFlags(), forced);
        }
        catch (const std::out_of_range& e)
        {}
        return GDB_ERROR_NOPOINT;

    }
    template <typename T>
    GDB_ERROR setValue( const lu_uint32_t CLpointID,
                        const T value,
                        const TimeManager::TimeStr& timestamp,
                        const PointFlags& flags,
                        const lu_bool_t forced = LU_FALSE
                        )
    {
        try
        {
            IPoint* pointCL = clPoint.at(CLpointID);
            if(pointCL != NULL)
            {
                switch (pointCL->getType())
                {
                    case POINT_TYPE_BINARY:
                        return setValue(*pointCL, (lu_uint32_t)value & 0x01, timestamp, flags, forced);
                        break;
                    case POINT_TYPE_DBINARY:
                        return setValue(*pointCL, (lu_uint32_t)value & 0x03, timestamp, flags, forced);
                        break;
                    case POINT_TYPE_ANALOGUE:
                        return setValue(*pointCL, (lu_float32_t)value, timestamp, flags, forced);
                        break;
                    case POINT_TYPE_COUNTER:
                        return setValue(*pointCL, (lu_uint32_t)value, timestamp, flags, forced);
                        break;
                    default:
                        break;
                }
            }
        }
        catch (const std::out_of_range& e)
        {}
        return GDB_ERROR_NOPOINT;
    }

    /**
     * \brief Set the online/offline status of a Control Logic Pseudo Point
     *
     * This overloaded method allows to set the online/offline status of a
     * Control Logic Point using its index.
     *
     * \param CLpointID Control Logic Point ID (Pseudo point index)
     * \param status Online/offline status to set
     * \param timestamp Time stamp of the point change
     *
     * \return Error code
     */
    GDB_ERROR setQuality(const lu_uint32_t CLpointID, const lu_bool_t status, const TimeManager::TimeStr& timestamp);
    GDB_ERROR setQuality(const lu_uint32_t CLpointID, const POINT_QUALITY status, const TimeManager::TimeStr& timestamp);

    /**
     * \brief Set ALL the common flags of a Control Logic Pseudo Point
     *
     * This overloaded method allows to set all the common flags of a Control
     * Logic Point using its index.
     *
     * \param CLpointID Control Logic Point ID (Pseudo point index)
     * \param status Set of flags to be set
     * \param timestamp Time stamp of the point change
     *
     * \return Error code
     */
    GDB_ERROR setFlags(const lu_uint32_t CLpointID, const PointFlags status, const TimeManager::TimeStr& timestamp);

    /**
     * \brief Helper method for initialising a Control Logic
     */
    GDB_ERROR initCheck();

    /**
     * \brief Initialise (activate) ALL the Control Logic Points (pseudo points)
     *
     * This activates Control Logic Points to be used.
     *
     * \return Error code
     */
    virtual GDB_ERROR activateCLPoints();

    /**
     * \brief Start (set) ALL the Control Logic Points (pseudo points)
     *
     * This sets the flags of all the Control Logic Points to the parameters
     * given.
     * Note that this does not set the point value.
     * Overloaded methods for:
     *      - Setting a point as online/offline & invalid with the current time
     *      - Setting a point as online/offline & invalid with a given time
     *      - Setting all point's flags with the current time
     *      - Setting all point's flags with a given time
     *
     * \return Error code
     */
    virtual GDB_ERROR startCLPoints(const bool online);
    virtual GDB_ERROR startCLPoints(const bool online,
                                    const TimeManager::TimeStr& timeStamp);
    virtual GDB_ERROR startCLPoints(const PointFlags& flags);
    virtual GDB_ERROR startCLPoints(const PointFlags& flags,
                                    const TimeManager::TimeStr& timeStamp);

    /**
     * \brief Attach a Control Logic's point observer over an Input point
     *
     * Attaches any Control Logic's observer over an input point.
     *
     * \return Error code
     */
    virtual GDB_ERROR attachObserver(IPointObserver *observer);

    /**
     * \brief Attach a Control Logic's point observer over an Input point
     *
     * Attaches any Control Logic's observer over an input point.
     *
     * \return Error code
     */
    virtual void detachObserver(IPointObserver *observer);

    /**
     * \brief Logs an error when an input point failed to attach
     *
     * \param inPointID Point ID of the input
     * \param errorGDB Error code to print
     */
    void logInputPointAttachError(const PointIdStr inPointID, const GDB_ERROR errorGDB);

protected:
    typedef std::vector<IPoint*> CLPointList;

protected:
    static Mutex *globalMutex;
    GeminiDatabase& database;
    static IEventLogManager* eventLog;
    static TimeManager& timeManager;
    Logger& log;

    CONTROL_LOGIC_TYPE cLogicType;
    lu_uint16_t groupID;    //Group ID number of this Control Logic
    bool enabled;           //Control Logic is enabled
    bool initialised;       //Control Logic has been initialised properly
    CLPointList clPoint;    //List of all of the Control Logic's Pseudo Points
};

#endif // !defined(EA_77428455_DD72_419b_BC83_389729B0D9DE__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
