/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id: DigitalOutputLogic.cpp 21 Aug 2012 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//G3Trunk/G3/RTU/MCM/src/CLogic/src/DigitalOutputLogic.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Implementation of the Control Logic for Digital Output.
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 21 Aug 2012 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   21/08/12      pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "DigitalOutputLogic.h"
#include "OutputDigitalCANChannel.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/* Select timeout in seconds */
static const lu_uint32_t selectTimeoutSec   =  1;

/* Threshold to ensure IOM has completed the operation (in ms) */
static lu_uint32_t safetyThreshold = 200;

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

DigitalOutputLogic::DigitalOutputLogic( Mutex *mutex,
                                  GeminiDatabase& g3database,
                                  DigitalOutputLogic::Config &conf
                                ) :
                                    OperatableControlLogic(mutex, g3database, conf, CONTROL_LOGIC_TYPE_DOL),
                                    stage(DOL_STAGE_OFF_LINE),
                                    channelDO(conf.channel),
                                    operationTimeoutTimer(NULL),
                                    pulseWidth(0),
                                    operationActive(LU_FALSE)
{
    opParams = conf.opParams;

    /* Allocate Control Logic virtual points */
    clPoint.reserve(Config::ALLPOINT_NUM);  //Adjust vector size to do not waste memory
    for(lu_uint32_t id = 0; id < Config::BINARYPOINT_NUM; ++id)
    {
        clPoint.push_back(new ControlLogicDigitalPoint(mutex, database, conf.bPoint[id], POINT_TYPE_BINARY));
    }

    /* Get Digital Input points */
    for(lu_uint32_t i = 0; i < DOL_INPUT_LAST; ++i)
    {
        /* Create an observer */
        dObservers[i] = new DIObserver(conf.dInput[i], *this);
    }
}


DigitalOutputLogic::~DigitalOutputLogic()
{
    /* cancel Timed Jobs */
    database.cancelJob(operationTimeoutTimer);

    /* Detach and release digital input observers */
    for(lu_uint32_t i = 0; i < DOL_INPUT_LAST; ++i)
    {
        detachObserver(dObservers[i]);
    }
}


GDB_ERROR DigitalOutputLogic::init()
{
    GDB_ERROR ret = initCheck();
    if(ret == GDB_ERROR_NONE)
    {
        ret = activateCLPoints();   //Initialise Control Logic points as well
    }
    if(ret != GDB_ERROR_NONE)
    {
        return ret;
    }

    startCLPoints(true);
    initialised = true;
    stage = DOL_STAGE_WAIT_COMMAND; //Update stage

    /* Initialise input points */
    ret = initInhibit();
    if(ret != GDB_ERROR_NONE)
    {
        //Detach all already attached and disable Control Logic indications
        initialised = false;
        stage = DOL_STAGE_OFF_LINE; //Revert stage
        startCLPoints(false);
    }
    return ret;
}


GDB_ERROR DigitalOutputLogic::getInputValue( lu_uint16_t inputPoint,
                                              PointData *data
                                            )
{
    GDB_ERROR ret = checkInputValue(DOL_INPUT_OLR, DOL_INPUT_INHIBIT, inputPoint, data);
    if(ret == GDB_ERROR_NOPOINT)
    {
        if(inputPoint > DOL_INPUT_LAST)
        {
            return GDB_ERROR_PARAM;
        }
        if(dObservers[inputPoint]->getPointID().isValid() == LU_TRUE)
        {
            ret = database.getValue(dObservers[inputPoint]->getPointID(), data);
        }
    }
    return ret;
}


GDB_ERROR DigitalOutputLogic::getOutputValue(PointData* data)
{
    if(data == NULL)
    {
        return GDB_ERROR_PARAM;
    }
    if(channelDO == NULL)
    {
        return GDB_ERROR_NOT_INITIALIZED;
    }
    /* Check online status */
    data->setOnlineFlag(channelDO->isActive() == LU_TRUE);
    return GDB_ERROR_NONE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
GDB_ERROR DigitalOutputLogic::startOp(const SwitchLogicOperation& operation,
                                      const GDB_ERROR errorCode)
{
    if(errorCode != GDB_ERROR_NONE)
    {
        refuseOperationMsg(errorCode);
        return errorCode;
    }

    GDB_ERROR ret = GDB_ERROR_NONE;

    log.info("%s. Operation: %i - Local: %i",
             this->toString().c_str(), operation.operation, operation.local
             );

    /* TODO: pueyos_a - ensure we need here globalmutex and doesn't collide with switchgear logic */
    LockingMutex lMutex(*globalMutex);

    /* Check stage */
    if(stage != DOL_STAGE_WAIT_COMMAND)
    {
        log.warn("%s. Operation already in progress (stage: %i)",
                  this->toString().c_str(), stage
                  );
        return GDB_ERROR_ALREADY_ACTIVE;
    }

    /* check preconditions */
    ret = preliminaryChecks();
    if(ret != GDB_ERROR_NONE)
    {
        return ret;
    }

    /* Reset temporary variables */
    operationTimeoutTimer = NULL;
    operationActive = LU_FALSE;

    currentOperation = operation;   //store operation parameters for later use

    /* Get current time an save it */
    TimeManager::TimeStr acceptedOperationTime = timeManager.now();

    /* Reset error flag */
    setValue(DOL_POINT_FAULT, 0, acceptedOperationTime, ValidFlags);
    /* Set the operating flag */
    setValue(DOL_POINT_OPERATING, 1, acceptedOperationTime, ValidFlags);

    //Report Event
    CTEventCLogicStr event;
    event.eventType = EVENT_TYPE_CLOGIC_DIGITAL_OUTPUT;
    event.cLogicType = EVENT_TYPE_CLOGICTYPE_DIGITAL_OUTPUT;
    event.value = EVENTCLOGIC_VALUE_REQ;
    eventLog->logEvent(event);

    /* Set next stage */
    stage = DOL_STAGE_SEND_OPERATION_CMDS;

    /* Schedule a command to be send to the IOM slave card */
    database.addJob(new SendOperateCmds(*this));

    return ret;
}


void DigitalOutputLogic::runOperation(SendOperateCmds& operationJob, const bool delayed)
{
    LU_UNUSED(operationJob);

    bool fault = false;

    if(stage != DOL_STAGE_SEND_OPERATION_CMDS)
    {
        /* Unexpected stage */
        log.error("%s - Stage: %i. Unexpected Operation", this->toString().c_str(), stage);
        return;
    }

    /* Check preconditions again */
    if(delayed)
    {
        fault = true; //Do not execute if has been delayed excessively
    }
    else if(channelDO->isActive() == LU_FALSE)
    {
        /* Check again online status */
        log.error("%s. - Output is offline", this->toString().c_str());
        fault = true;
    }
    else
    {
        GDB_ERROR ret = checkAll(currentOperation.local);
        if(ret != GDB_ERROR_NONE)
        {
            log.error("%s - Error: operation rejected (%s)",
                        this->toString().c_str(),
                        GDB_ERROR_ToSTRING(ret)
                      );
            fault = true;
        }
    }
    if(fault)
    {
        /* Signal the fault in the Control Logic indication */
        TimeManager::TimeStr timeStamp = timeManager.now();
        setValue(DOL_POINT_FAULT, 1, timeStamp, ValidFlags);
        stage = DOL_STAGE_WAIT_COMMAND;
    }
    else
    {
        log.info("%s - Operate", this->toString().c_str());

        /* Send commands to module */
        operationActive = LU_TRUE;
        lu_bool_t value = (currentOperation.operation == SWITCH_OPERATION_CLOSE)?
                            LU_TRUE : LU_FALSE;

        /* Select command */
        IOM_ERROR iomRet = channelDO->select(selectTimeoutSec, currentOperation.local);
        if(iomRet == IOM_ERROR_NONE)
        {
            /* Select successful: send Operate command */
            iomRet = channelDO->operate(value, currentOperation.local);
        }
        if(iomRet != IOM_ERROR_NONE)
        {
            /* TODO: pueyos_a - pass around iomRet in order to print proper error */
            /* Handle error */
            cancelOperation(LU_TRUE);
            return;
        }
    }

    //Report Event
    CTEventCLogicStr event;
    event.eventType = EVENT_TYPE_CLOGIC_DIGITAL_OUTPUT;
    event.cLogicType = EVENT_TYPE_CLOGICTYPE_DIGITAL_OUTPUT;
    event.value = (fault == LU_TRUE)? EVENTCLOGIC_VALUE_FAIL : EVENTCLOGIC_VALUE_OK;
    eventLog->logEvent(event);

    if(fault)
    {
        return;
    }

    log.info("%s - operation started", this->toString().c_str());

    /* Set next stage */
    LockingMutex lMutex(*globalMutex);
    stage = DOL_STAGE_WAIT_COMPLETION;

    /* FIXME: pueyos_a - WRONGWRONGWRONG!!! if the channel is non-CAN (e.g. Modbus), we are pointing to garbage data!!!!! TAKE INFO FROM CONFIG, do not ask the channel!! */
    /* Get the pulse width from the configured channel */
    OutputDigitalCANChannelConf cfgDO;
    OutputDigitalCANChannel *pOutChan = (OutputDigitalCANChannel *)channelDO;
    pOutChan->getConfiguration(cfgDO);

    ((OutputDigitalCANChannel *)channelDO)->getConfiguration(cfgDO);

    pulseWidth = cfgDO.pulseLengthMs;


    /* Save a reference to the operation timeout timer.
     * We need it to cancel the job
     */
    operationTimeoutTimer = new DOLOpCmdTimeoutTmr(*this,
                                                  lu_uint32_t(pulseWidth) + safetyThreshold
                                                  );

    /* Start operation timeout job */
    database.addJob(operationTimeoutTimer);
}


void DOLOpCmdTimeoutTmr::job(TimeManager::TimeStr *timePtr)
{
    LU_UNUSED(timePtr);

    /* Send Operation Timeout signal */
    cLogic.operationTimeout();
}


void DigitalOutputLogic::operationTimeout()
{
    /* Unlink Timed Job (will be deleted by the Scheduler) */
    operationTimeoutTimer = NULL;

    /* Check if the signal is active */
    if(stage != DOL_STAGE_WAIT_COMPLETION)
    {
        log.error("%s - Stage: %i. Unexpected Signal",
                    this->toString().c_str(), stage
                  );

        return;
    }

    log.info("%s - pulse finished", this->toString().c_str());

    /* Send Cancel message to check if it was successfully finished or not */
    cancelOperation(LU_FALSE);
}


void DigitalOutputLogic::cancelOperation(lu_bool_t cancel)
{
    IOM_ERROR iomRet = IOM_ERROR_NONE;

    /* Stop all operations / Check result */
    if(operationActive == LU_TRUE)
    {
        /* Get current time an save it */
        TimeManager::TimeStr acceptedOperationTime = timeManager.now();

        /* Cancel Digital Output operation to see result */
        iomRet = channelDO->cancel(currentOperation.local);
        /* store result in error flag: it can only be cleared if it was
         * successful and was not forcefully cancelled.
         */
        setValue(DOL_POINT_FAULT,
                 ((iomRet == IOM_ERROR_NONE) && (cancel == LU_FALSE) )? 0 : 1,
                 acceptedOperationTime,
                 ValidFlags
                 );
        /* Reset the operating flag in any case */
        setValue(DOL_POINT_OPERATING, 0, acceptedOperationTime, ValidFlags);

        stage = DOL_STAGE_WAIT_COMMAND;
        operationActive = LU_FALSE;
    }
    else
    {
        iomRet = IOM_ERROR_ALREADY;
    }
    if( (iomRet == IOM_ERROR_NONE) && (cancel == LU_FALSE) )
    {
        log.info("%s. Digital Output Operation was successful.", this->toString().c_str());
    }
    else
    {
        log.info("%s. Digital Output Operation failed. Error %i: %s",
                this->toString().c_str(),
                (cancel == LU_TRUE)? 0 : iomRet,
                (cancel == LU_TRUE)? "Cancelled" : IOM_ERROR_ToSTRING(iomRet)
              );
    }

    //Report Event
    CTEventCLogicStr event;
    event.eventType = EVENT_TYPE_CLOGIC_DIGITAL_OUTPUT;
    event.cLogicType = EVENT_TYPE_CLOGICTYPE_DIGITAL_OUTPUT;
    event.value = ((iomRet != IOM_ERROR_NONE) || (cancel == LU_TRUE))? EVENTCLOGIC_VALUE_FAIL :
                                                                       EVENTCLOGIC_VALUE_FINISHED;
    eventLog->logEvent(event);
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
GDB_ERROR DigitalOutputLogic::preliminaryChecks()
{
    GDB_ERROR ret = GDB_ERROR_NONE;
    EVENTCLOGIC_VALUE eventReport = EVENTCLOGIC_VALUE_FAIL;
    std::string refusedCLTitle (this->toString());
    refusedCLTitle.append(" operation refused");

    /* Check Online status */
    if(channelDO->isActive() == LU_FALSE)
    {
        log.warn("%s Unable to operate: Digital output is offline.", refusedCLTitle.c_str());

        /* TODO: pueyos_a - [EVENT] report Event when operating an offline CLogic?? */
        eventReport = EVENTCLOGIC_VALUE_FAIL;   //EVENTCLOGIC_VALUE_INHIBIT;
        ret = GDB_ERROR_INHIBIT;
    }
    /* check inhibit input */
    else if(dObservers[DOL_INPUT_INHIBIT]->getPointID().isValid() == LU_TRUE)
    {
        /* inhibit when the value is triggering it or when the point is offline */
        if( (dObservers[DOL_INPUT_INHIBIT]->getValue() == LU_TRUE) ||
            (dObservers[DOL_INPUT_INHIBIT]->getQuality() != POINT_QUALITY_ON_LINE)
            )
        {
            log.warn("%s: Operation inhibited %s.",
                    refusedCLTitle.c_str(),
                    (dObservers[DOL_INPUT_INHIBIT]->getQuality() != POINT_QUALITY_ON_LINE)?
                                    " (Inhibit input point is offline)" : ""
                  );
        /* TODO: pueyos_a - Set the event to the correct value */
            eventReport = EVENTCLOGIC_VALUE_FAIL; //EVENTCLOGIC_VALUE_INHIBIT;
            ret = GDB_ERROR_INHIBIT;
        }
    }

    if(ret != GDB_ERROR_NONE)
    {
        //Report Event
        CTEventCLogicStr event;
        event.eventType = EVENT_TYPE_CLOGIC_DIGITAL_OUTPUT;
        event.cLogicType = EVENT_TYPE_CLOGICTYPE_DIGITAL_OUTPUT;
        event.value = eventReport;
        eventLog->logEvent(event);
    }
    return ret;
}


/*
 *********************** End of file ******************************************
 */
