/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: PseudoClockLogic.cpp 26 Jun 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/CLogic/src/PseudoClockLogic.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Pseudo-Clock Control Logic.
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 26 Jun 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   26 Jun 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "PseudoClockLogic.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
PseudoClockLogic::PseudoClockLogic( Mutex* mutex,
                                    GeminiDatabase& g3database,
                                    PseudoClockLogic::Config& config
                                    ) :
                                        OperatableControlLogic(mutex, g3database, config, CONTROL_LOGIC_TYPE_PCLK),
                                        m_config(config),
                                        m_outEnabled(config.startupEnabled),
                                        m_value(false),
                                        m_tickTimer(NULL),
                                        m_pulseTimer(NULL)
{
    /* Allocate Control Logic virtual points */
    clPoint.reserve(Config::ALLPOINT_NUM);  //Adjust vector size to do not waste memory
    for(lu_uint32_t id = 0; id < Config::BINARYPOINT_NUM; ++id)
    {
        clPoint.push_back(new ControlLogicDigitalPoint(mutex, database, config.bPoint[id], POINT_TYPE_BINARY));
    }
}


PseudoClockLogic::~PseudoClockLogic()
{
    /* Detach time observer */
    timeManager.detachMinute(this);
    stopTicking();
    database.cancelJob(m_pulseTimer);
}


GDB_ERROR PseudoClockLogic::init()
{
    GDB_ERROR ret = initCheck();
    if(ret == GDB_ERROR_NONE)
    {
        ret = activateCLPoints();   //Initialise Control Logic points as well
    }
    if(ret != GDB_ERROR_NONE)
    {
        return ret;
    }

    /* Start timers for ticking */
    TimeManager::TimeStr timeStamp = timeManager.now();
    switch(m_config.timingType)
    {
        case Config::TIMING_OCLOCK:
        {
            /* Create and Attach time observer */
            lu_int32_t res = timeManager.attachMinute(this);
            if(res != 0)
            {
                /* Observer not attached */
                log.error("%s Error: unable to connect to the clock - feature disabled",
                                this->toString().c_str());
                disableAll(timeStamp);
                ret = GDB_ERROR_NOT_INITIALIZED;
            }
            break;
        }
        case Config::TIMING_INTERVAL:
        {
            if (m_config.tickTime_ms == 0)
            {
                log.error("%s Error: Bad ticking time 0 in configuration - feature disabled",
                          this->toString().c_str());
                disableAll(timeStamp);
                ret = GDB_ERROR_NOT_INITIALIZED;
            }
            else
            {
                /* Start ticking */
                if(!startTicking())
                {
                    disableAll(timeStamp);
                    ret = GDB_ERROR_NOT_INITIALIZED;
                }
            }
            break;
        }
        default:
            disableAll(timeStamp);
            ret = GDB_ERROR_NOT_INITIALIZED;
    }

    if(ret == GDB_ERROR_NONE)
    {
        startCLPoints(true);
        initialised = true;
    }
    else
    {
        m_outEnabled = false;   //No initialisation disables the Logic Output
    }

    /* Initialise input points */
    if(ret == GDB_ERROR_NONE)
    {
        /* Attach Observers */
        ret = initInhibit();
        if(ret != GDB_ERROR_NONE)
        {
            //Detach all already attached and disable Control Logic indications
            initialised = false;
            disableAll(timeStamp);
        }
    }

    return ret;
}


GDB_ERROR PseudoClockLogic::getInputValue(lu_uint16_t inputPoint, PointData* data)
{
    return checkInputValue(PCLK_INPUT_OLR, PCLK_INPUT_INHIBIT, inputPoint, data);
}


GDB_ERROR PseudoClockLogic::getOutputValue(PointData* data)
{
    if(data == NULL)
    {
        return GDB_ERROR_PARAM;
    }
    if(!initialised)
    {
        return GDB_ERROR_NOT_INITIALIZED;
    }
    /* Report state */
    PointDataUint8* data8 = (PointDataUint8*)data;
    *data8 = m_value;
    return GDB_ERROR_NONE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
GDB_ERROR PseudoClockLogic::startOp(const SwitchLogicOperation& operation,
                                    const GDB_ERROR errorCode)
{

    if(errorCode != GDB_ERROR_NONE)
    {
        refuseOperationMsg(errorCode);
        return errorCode;
    }

    bool oldEnabled = m_outEnabled;

    {//Critical Section
        MasterLockingMutex wMutex(m_enLock, LU_TRUE);  //Lock access to m_outEnabled
        m_outEnabled = (operation.operation == SWITCH_OPERATION_CLOSE);

        if (m_outEnabled != oldEnabled)
        {
            if (m_config.timingType == Config::TIMING_INTERVAL)
            {
                /* Interval ticking enable/disable only */
                if(m_outEnabled)
                {
                    if(!startTicking())
                    {
                        m_outEnabled = false; //Failed to start ticking: disabled
                    }
                }
                else
                {
                    stopTicking();
                }
            }
            log.info("%s %s", this->toString().c_str(), (m_outEnabled)? "Enabled" : "Disabled");
        }
    }

    return GDB_ERROR_NONE;
}


void PseudoClockLogic::tickUpdate(TimeManager::TimeStr* timerPtr)
{
    //Interval update signalled
    changeValue(*timerPtr);
}


void PseudoClockLogic::PulseJob::job(TimeManager::TimeStr* timerPtr)
{
    //End of pulse
    cLogic.updateValue(false, *timerPtr);
    cLogic.m_pulseTimer = NULL;    //Job will be deleted by the Manager
}


void PseudoClockLogic::PeriodicJob::job(TimeManager::TimeStr* timerPtr)
{
    cLogic.tickUpdate(timerPtr);
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void PseudoClockLogic::updateTime(TimeManager::TimeStr& currentTime)
{
    //Minute update signalled
    if( (NSEC_TO_MSEC(currentTime.time.tv_nsec) % m_config.tickTime_ms) == 0)
    {
        //Real time matches with the configured tick time
        changeValue(currentTime);
    }
}


void PseudoClockLogic::disableAll(const TimeManager::TimeStr& timeStamp)
{
    m_outEnabled = false;   //Not needed to lock since done only at startup
    /* Set all flags as disabled */
    startCLPoints(false, timeStamp);
}


void PseudoClockLogic::setOutQuality(const bool online, const TimeManager::TimeStr& timeStamp)
{
    for (lu_uint32_t id = 0; id < Config::ALLPOINT_NUM; ++id)
    {
        setQuality(id, (online)? LU_TRUE : LU_FALSE, timeStamp);
    }
}


void PseudoClockLogic::changeValue(const TimeManager::TimeStr& timeStamp)
{
    //Critical Section
    MasterLockingMutex rMutex(m_enLock);  //Lock access to m_outEnabled
    if(m_outEnabled)
    {
        //Tick happened so start changing value
        if(m_config.pulseWidth_ms == 0)
        {
            /* Toggle output value */
            updateValue(!m_value, timeStamp);
        }
        else
        {
            /* Start pulse */
            updateValue(true, timeStamp);
            //Schedule end of pulse
            m_pulseTimer = new PulseJob(*this, m_config.pulseWidth_ms);
            GDB_ERROR res = database.addJob(m_pulseTimer);
            if (res != GDB_ERROR_NONE)
            {
                log.error("%s Error: Unable to set pulse timer - %s",
                          this->toString().c_str(), GDB_ERROR_ToSTRING(res));
                delete m_pulseTimer;
                m_pulseTimer = NULL;
                updateValue(false, timeStamp); //Finish pulse earlier
            }
        }
    }
}


void PseudoClockLogic::updateValue(const bool newValue, const TimeManager::TimeStr& timeStamp)
{
    m_value = newValue;
    setValue(PCLK_POINT_CLOCK, (m_value==true)? 1 : 0, timeStamp, ValidFlags);
}


bool PseudoClockLogic::startTicking()
{
    if(m_tickTimer == NULL)
    {
        m_tickTimer = new PeriodicJob(*this, m_config.tickTime_ms);
        /* Add the job to the DB scheduler */
        GDB_ERROR gdbRet = database.addJob(m_tickTimer);
        if(gdbRet != GDB_ERROR_NONE)
        {
            delete m_tickTimer;
            m_tickTimer = NULL;
            log.error("%s Error: Unable to set periodic job timer - feature disabled",
                      this->toString().c_str());
            return false;
        }
    }
    return true;
}


void PseudoClockLogic::stopTicking()
{
    database.cancelJob(m_tickTimer);
}


/*
 *********************** End of file ******************************************
 */
