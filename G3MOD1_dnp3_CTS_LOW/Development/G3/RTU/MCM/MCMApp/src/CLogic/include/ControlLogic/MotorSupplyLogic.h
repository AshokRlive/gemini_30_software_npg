/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:MotorSupplyLogic.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   12 Apr 2018     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MOTORSUPPLYLOGIC_H_
#define MOTORSUPPLYLOGIC_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IChannel.h"
#include "OperatableControlLogic.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Class MotorSupplyLogic for controlling Motor Supply channels
 */
class MotorSupplyLogic : public OperatableControlLogic
{
public:
    /**
     * \brief Motor Supply Controller logic configuration
     */
    struct Config : public OperatableControlLogic::Config
    {
    public:
        PointIdStr controlInput;            //Control Input (Digital)
        IChannel* motorChannel;             //PSM's output motor channel
        IChannel* switchChannel;            //Switch module's output motor channel
        IOModuleIDStr moduleSwitch;         //Associated module
        lu_uint8_t switchID;                //Associated module's switch ID
        lu_uint32_t duration_s;             //Duration of the operation -- 0 for latched
        //Currently unused parameters:
        lu_uint32_t currentThreshold;       //Motor Current Threshold in Amps
        lu_uint32_t motorOvercurrentTime;   //Motor OverCurrent Measurement Delay in milliseconds
    public:
        Config() :  motorChannel(NULL),
                    switchChannel(NULL),
                    switchID(0),
                    duration_s(60),
                    currentThreshold(0),
                    motorOvercurrentTime(0)
        {};
    };

public:
    /**
     * \brief Custom constructor
     *
     * \param mutex Reference to the G3DB common mutex
     * \param database Reference to the Gemini database
     * \param config reference to the Control Logic's configuration block
     */
    MotorSupplyLogic(Mutex *mutex                 ,
                    GeminiDatabase& database     ,
                    MotorSupplyLogic::Config &conf
                    );
    virtual ~MotorSupplyLogic();

    /* == inherited from ControlLogic == */
    virtual GDB_ERROR init();
    virtual GDB_ERROR getInputValue(lu_uint16_t inputPoint, PointData *data);
    virtual GDB_ERROR getOutputValue(PointData* data);

protected:
    /**
     * \brief Timed Job for cancelling Motor Supply
     */
    class DurationJob : public TimedJob
    {
    public:
        /**
         * \brief Custom constructor
         *
         * \param CLogic Reference to the Control Logic object.
         * \param timerInterval_s Period time, expressed in seconds
         */
        DurationJob(MotorSupplyLogic& CLogic, const lu_uint32_t timerInterval_s) :
                                            TimedJob(timerInterval_s * 1000, 0),
                                            cLogic(CLogic)
        {};
        virtual ~DurationJob() {};

    protected:
        /* == Inherited from TimedJob == */
        virtual void job(TimeManager::TimeStr* timerPtr)
        {
            cLogic.endSupply(*timerPtr);
        }

    private:
        MotorSupplyLogic& cLogic;
    };

protected:
    /* == Implements mandatory OperatableControlLogic == */
    virtual GDB_ERROR startOp(const SwitchLogicOperation &operation, const GDB_ERROR errorCode);

    /* == Overrides from OperatableControlLogic == */
    virtual void newData(PointIdStr pointID, PointData& pointData);

    /**
     * \brief End Motor Supply
     *
     * \param currentTime Time of occurrence of the resuming
     */
    void endSupply(const TimeManager::TimeStr& currentTime);

private:
    /**
     * \brief Sends channel commands to the Modules
     *
     * \param operation Parameters of the operation
     *
     * \return Error code.
     */
    GDB_ERROR sendOperateCmds(const SwitchLogicOperation& operation);

    /**
     * \brief Check common preliminary conditions that can inhibit the operation
     *
     * \param operation Parameters of the operation
     *
     * \return Error code. GDB_ERROR_NONE when all preliminary checks passed.
     */
    GDB_ERROR preliminaryChecks(const SwitchLogicOperation& operation);

    /**
     * \brief Check whether all the output channels are active
     *
     * \return True if all channels are active.
     */
    bool isAllChannelsActive();

private:
    MotorSupplyLogic::Config m_config;  //Control Logic config
    DurationJob* m_durationJob;         //Job for cancelling Motor Supply
    DIObserver* m_control;              //Control input change observer
};


#endif /* MOTORSUPPLYLOGIC_H_ */

/*
 *********************** End of file ******************************************
 */
