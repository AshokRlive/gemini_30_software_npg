/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Local/Remote control logic interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   09/08/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_61DD2CCB_4867_499b_9525_63F8C576DE20__INCLUDED_)
#define EA_61DD2CCB_4867_499b_9525_63F8C576DE20__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */



/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IChannel.h"
#include "ControlLogic.h"
#include "ControlLogicDigitalPoint.h"
#include "IMemoryManager.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Off/Local/Remote state Control Logic
 *
 * This objects checks the state of the Local/Remote lines and sets the internal
 * OLR status accordingly. When the application commands to set this state, the
 * CL tries to set it, but depending on what controls these lines, it may or may
 * not change.
 *
 * To know the OLR status, check the values of the related CL virtual points,
 * using getValue() of OLR_DBPOINT_OFFLOCALREMOTE point, or similar (but DO NOT
 * use these values inside this object!).
 */
class LocalRemoteControlLogic : public ControlLogic, public ChannelObserver
{
public:
    struct Config : public ControlLogicConf
    {
    public:
        /* Amounts of different types of Control Logic's Pseudo Points */
        static const lu_uint16_t BINARYPOINT_NUM = OLR_POINT_TYPE_BINARY_COUNT;
        static const lu_uint16_t DBINARYPOINT_NUM = OLR_POINT_TYPE_DOUBLEBINARY_COUNT;
        static const lu_uint32_t ALLPOINT_NUM = OLR_POINT_EnumSize;
    public:
        ControlLogicDigitalPoint::Config bPoint[BINARYPOINT_NUM]; //Control Logic digital points
        ControlLogicDigitalPoint::Config dbPoint[DBINARYPOINT_NUM]; //Control Logic digital points
        IChannel* getOLRChannel;        //Input to check remote line status
        IChannel* setOLRChannel;        //Output to set Local/Remote lines
    };

public:
    /**
     * \brief Default constructor.
     *
     * \param mutex CL Global mutex.
     * \param database database reference.
     * \param memMgr Memory Manager to store OLR status.
     * \param conf CL configuration.
     */
    LocalRemoteControlLogic(Mutex* mutex,
                            GeminiDatabase& database,
                            IMemoryManager& memMgr,
                            LocalRemoteControlLogic::Config& conf
                           );
    virtual ~LocalRemoteControlLogic();

    /* == Inherited from ControlLogic == */
    virtual GDB_ERROR init();
    virtual GDB_ERROR getInputValue(lu_uint16_t inputPoint, PointData *data);

    /**
     * \brief Change the Off/Local/Remote state of the system
     *
     * \param status New Off/Local/Remote status
     * \param source Element from where the action was originated
     * \param force Optional parameter: set to LU_TRUE to force OLR change
     *
     * \return Error Code
     */
    virtual GDB_ERROR setLocalRemote(OLR_STATE status, OLR_SOURCE source);

private:
    void update(IChannel *subject);

private:
    /* Index of internal channels */
    typedef enum
    {
        LR_CH_LOCAL = 0,
        LR_CH_REMOTE,
        LR_CH_LAST
    }LR_CH;

private:
    IMemoryManager &memoryManager;
    IChannel* inputChannel;
    IChannel* outChannel;
    OLR_STATE currentStatus;    //Current status of the OLR
    OLR_SOURCE lastSource;      //Source of the last change in the OLR
    struct timespec sourceTimestamp;    //Timestamp of the last request of OLR change
};

#endif // !defined(EA_61DD2CCB_4867_499b_9525_63F8C576DE20__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
