/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "DummyControlLogicFactory.h"
#include "LogicGateControlLogic.h"
#include "FanTestControlLogic.h"
#include "LocalRemoteControlLogic.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

typedef struct
{
    lu_uint16_t groupID;
    ControlLogicDigitalPointConfig vpoint[OLR_DPOINT_LAST];

}FactoryLocalRemoteControlLogicConf;

typedef struct
{
    lu_uint16_t groupID;
    EFH_Operate logic;
    ControlLogicDigitalPointConfig alarmPointConfig;
    PointIdStr                *list;
    lu_uint32_t                listSize;
}FactoryEquipmentFaultHandlerConf;

typedef struct
{
    lu_uint16_t groupID;
    MODULE module;
    MODULE_ID moduleID;
    lu_uint32_t channel;
}FactoryFanTestConf;

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


PointIdStr EFHPointList0[] =
{
    {0, 2},
    {0, 3}
};
static const lu_uint32_t EFHPointList0Size = sizeof(EFHPointList0) / sizeof(PointIdStr);

static FactoryLocalRemoteControlLogicConf OLRConfArray[] =
{
    /* groupID   invert      chatterNo    chatterTime */
    {  1,
            {
                {LU_FALSE, 100     , 1          }, /* Virtual point: Off              */
                {LU_FALSE, 100     , 1          }, /* Virtual point: Local            */
                {LU_FALSE, 100     , 1          }, /* Virtual point: Remote           */
                {LU_FALSE, 100     , 1          }  /* Virtual point: Off/Local/Remote */
            }
    }
};
static const lu_uint32_t OLRConfArraySize = sizeof(OLRConfArray) / sizeof(FactoryLocalRemoteControlLogicConf);

static FactoryEquipmentFaultHandlerConf EFHConfArray[] =
{
    /* groupID logic                     invert    chatterNo chatterTime    list              list size       */
    {  2     , EFH_OPERATE_AND, {LU_FALSE, 100     , 1          }, &EFHPointList0[0], EFHPointList0Size},
    {  3     , EFH_OPERATE_OR , {LU_FALSE, 100     , 1          }, &EFHPointList0[0], EFHPointList0Size}
};
static const lu_uint32_t EFHConfArraySize = sizeof(EFHConfArray) / sizeof(FactoryEquipmentFaultHandlerConf);


static FactoryFanTestConf FTConfArray[] =
{
    /* groupID   Module      Module ID    Fan channel*/
    {  4      ,  MODULE_PSM, MODULE_ID_0, PSM_CH_FAN_FAN_TEST }
};
static const lu_uint32_t FTConfArraySize = sizeof(FTConfArray) / sizeof(FactoryFanTestConf);



/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
DummyControlLogicFactory::DummyControlLogicFactory(IStatusManager &statMan,
                                                    IIOModuleManager& MManager)
                                                        : statusManager(statMan),
                                                          MManager(MManager)
{}


lu_uint16_t DummyControlLogicFactory::getCLogicNumber()
{

    return (EFHConfArraySize + FTConfArraySize + OLRConfArraySize);
}

ControlLogic* DummyControlLogicFactory::newCLogic( Mutex *mutex,
                                                   GeminiDatabase *database,
                                                   lu_uint16_t group
                                                 )
{
    lu_uint32_t i;

    /* Search for Local/Remote block */
    for(i = 0; i < OLRConfArraySize; ++i)
    {
        FactoryLocalRemoteControlLogicConf *FLRConfPtr;

        FLRConfPtr = &OLRConfArray[i];

        if(FLRConfPtr->groupID == group)
        {
            /* Control logic block found */
            LocalRemoteControlLogicConf LRConf;

            LRConf.groupID = FLRConfPtr->groupID;
            for( lu_uint32_t j = 0;
                 j < OLR_DPOINT_LAST;
                 ++j
               )
            {
                LRConf.vpoint[j].chatterNo   = FLRConfPtr->vpoint[j].chatterNo;
                LRConf.vpoint[j].chatterTime = FLRConfPtr->vpoint[j].chatterTime;
                LRConf.vpoint[j].invert      = FLRConfPtr->vpoint[j].invert;
            }

            LRConf.localChannel  = MManager.getModule(MODULE_MCM, MODULE_ID_0)->getChannel(CHANNEL_TYPE_DOUTPUT, MCM_CH_DOUTPUT_LOCAL );
            LRConf.remoteChannel = MManager.getModule(MODULE_MCM, MODULE_ID_0)->getChannel(CHANNEL_TYPE_DOUTPUT, MCM_CH_DOUTPUT_REMOTE);

            /* Create object */
            LocalRemoteControlLogic *localRemoteControlLogicPtr =
                          new LocalRemoteControlLogic(statusManager, database, mutex, LRConf);

            return localRemoteControlLogicPtr;
        }
    }

    /* Search for an Equipment Fault Handler block */
    for(i = 0; i < EFHConfArraySize; ++i)
    {
        FactoryEquipmentFaultHandlerConf *FFEConfPtr;

        FFEConfPtr = &EFHConfArray[i];
        if(FFEConfPtr->groupID == group)
        {
            /* Control logic block found */
            EquipmentFaultHandlerConf EFHConf;

            EFHConf.groupID = FFEConfPtr->groupID;
            EFHConf.logic = FFEConfPtr->logic;
            EFHConf.alarmPointConfig.invert = FFEConfPtr->alarmPointConfig.invert;
            EFHConf.alarmPointConfig.chatterNo = FFEConfPtr->alarmPointConfig.chatterNo;
            EFHConf.alarmPointConfig.chatterTime = FFEConfPtr->alarmPointConfig.chatterTime;

            /* Create object */
            LogicGateControlLogic *equipmentFaultHandlerPtr =
                           new LogicGateControlLogic(mutex, database, EFHConf);

            /* Add points */
            if(equipmentFaultHandlerPtr != NULL)
            {
                for(lu_uint32_t j = 0; j < FFEConfPtr->listSize; ++j)
                {
                    equipmentFaultHandlerPtr->addPoint(FFEConfPtr->list[j]);
                }

                return equipmentFaultHandlerPtr;
            }
        }
    }

    /* Search for an Equipment Fault Handler block */
    for(i = 0; i < FTConfArraySize; ++i)
    {
        FactoryFanTestConf *FTConfPtr;

        FTConfPtr = &FTConfArray[i];
        if(FTConfPtr->groupID == group)
        {
            FanTestcontrolLogicConf FTConf;

            /* Control logic block found
             * Create object
             */
            FTConf.groupID = FTConfPtr->groupID;
            FTConf.channel = MManager.getModule(FTConfPtr->module, FTConfPtr->moduleID)->getChannel(CHANNEL_TYPE_FAN, FTConfPtr->channel);
            FanTestControlLogic *fanTestControlLogicPtr =
                           new FanTestControlLogic(mutex, database, FTConf);

            return fanTestControlLogicPtr;
        }
    }

    return NULL;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
