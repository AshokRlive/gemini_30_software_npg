/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: BatteryChargerTestLogic.cpp 21-Nov-2012 10:18:39 pueyos_a $
 *               $HeadURL: Y:\workspace\G3Trunk\G3\RTU\MCM\src\CLogic\src\BatteryChargerTestLogic.cpp $
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       BatteryChargerTestLogic module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 21-Nov-2012 10:18:39	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   21-Nov-2012 10:18:39  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "BatteryChargerTestLogic.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

BatteryChargerTestLogic::BatteryChargerTestLogic(Mutex *mutex                ,
                                                GeminiDatabase& g3database   ,
                                                BatteryChargerTestLogic::Config& config
                                                ) :
                                                OperatableControlLogic(mutex, g3database, config, CONTROL_LOGIC_TYPE_BATTERY),
                                                opParams(config.opParams),
                                                battChannel(config.channel),
                                                battChannelRef(config.channelRef),
                                                acceptedOperationTime(TIMESPEC_ZERO)
{
    if(battChannel == NULL)
    {
        log.error("%s Unable to configure channel to operate",
                    this->toString().c_str()
                    );
    }
}


BatteryChargerTestLogic::~BatteryChargerTestLogic()
{}


GDB_ERROR BatteryChargerTestLogic::init()
{
    GDB_ERROR ret = initCheck();
    if(battChannel == NULL)
    {
        ret = GDB_ERROR_NOT_INITIALIZED;
    }
    if(ret == GDB_ERROR_NONE)
    {
        /* Initialise input points */
        ret = initInhibit();
    }
    if(ret == GDB_ERROR_NONE)
    {
        initialised = true;
    }
    return ret;
}


GDB_ERROR BatteryChargerTestLogic::getInputValue(lu_uint16_t inputPoint,
                                                 PointData *data
                                                )
{
    return checkInputValue(BATTERY_CHARGER_INPUT_OLR, BATTERY_CHARGER_INPUT_INHIBIT, inputPoint, data);
}


GDB_ERROR BatteryChargerTestLogic::getOutputValue(PointData* data)
{
    if(data == NULL)
    {
        return GDB_ERROR_PARAM;
    }
    if(battChannel == NULL)
    {
        return GDB_ERROR_NOT_INITIALIZED;
    }
    /* Check online status */
    data->setOnlineFlag(battChannel->isActive() == LU_TRUE);
    return GDB_ERROR_NONE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
GDB_ERROR BatteryChargerTestLogic::startOp(const SwitchLogicOperation& operation,
                                           const GDB_ERROR errorCode)
{
    if(errorCode != GDB_ERROR_NONE)
    {
        refuseOperationMsg(errorCode);
        return errorCode;
    }

    if(battChannel->isActive() == LU_FALSE)
    {
        log.info("%s - Error: Battery charger channel not available.", this->toString().c_str());
        return GDB_ERROR_INHIBIT;
    }

    //Prepare Event to report
    CTEventCLogicStr event;
    event.eventType = EVENT_TYPE_CLOGIC_BATTERY;
    switch(battChannelRef.channelID)
    {
        case PSM_CH_BCHARGER_BATTERY_DISCONNECT:
            event.cLogicType = EVENT_TYPE_CLOGICTYPE_BATTERY_DISCONNECT;
            break;
        case PSM_CH_BCHARGER_BATTERY_TEST:
            event.cLogicType = EVENT_TYPE_CLOGICTYPE_BATTERY_TEST;
            break;
        case PSM_CH_BCHARGER_INHIBIT:
            event.cLogicType = EVENT_TYPE_CLOGICTYPE_BATTERY_CHARGER_INHIBIT;
            break;
        case PSM_CH_BCHARGER_BATT_TEST_INHIBIT:
            event.cLogicType = EVENT_TYPE_CLOGICTYPE_BATTERY_CHARGER_TEST;
            break;
        default:
            event.cLogicType = EVENT_TYPE_CLOGICTYPE_LAST;  //Unknown
    }

    //Remember requested operation with final duration
    m_operation = operation;
    m_operation.duration = (operation.duration == 0)? opParams.duration : operation.duration;

    log.info("%s Requested %s %s(channel %d) Test - Duration: %i %s",
                this->toString().c_str(),
                SWITCH_OPERATION_ToSTRING(operation.operation),
                PSM_CH_BCHARGER_ToSTRING(battChannelRef.channelID),
                battChannelRef.channelID,
                m_operation.duration,
                (battChannelRef.channelID == PSM_CH_BCHARGER_BATTERY_TEST)? "minutes" : "seconds"
              );

    /* Check preconditions */
    GDB_ERROR ret = preliminaryChecks();

    //Report Event
    event.value = (ret == GDB_ERROR_NONE)? EVENTCLOGIC_VALUE_OK : //Request accepted
                                            EVENTCLOGIC_VALUE_FAIL; //Request denied
    eventLog->logEvent(event);

    if(ret != GDB_ERROR_NONE)
    {
        return ret;
    }

    clock_gettimespec(&acceptedOperationTime);  //Get timestamp to check timeout later

    /* Schedule a Job to start the operation */
    database.addJob(new SendOperateCmds(*this));

    return GDB_ERROR_NONE;
}


void BatteryChargerTestLogic::runOperation(SendOperateCmds& operationJob, const bool delayed)
{
    LU_UNUSED(operationJob);

    if(delayed)
        return; //Do not execute if has been delayed excessively

    IOM_ERROR iomRet;

    log.info("%s prepares operation.", this->toString().c_str());

    /* Check preconditions again */
    GDB_ERROR ret = preliminaryChecks();
    if(ret != GDB_ERROR_NONE)
    {
        return;
    }

    if(m_operation.operation == SWITCH_OPERATION_OPEN)
    {
        /* Cancel operation requested */

        /* Send Cancel to Battery test using the configured channel */
        IOM_ERROR iomRet = battChannel->cancel(m_operation.local);
        if(iomRet != IOM_ERROR_NONE)
        {
            if(iomRet == IOM_ERROR_CANCEL_ERROR)
            {
                log.error("%s - Sending Cancel command error: "
                            "operation %s might not be active - Error: %s",
                            this->toString().c_str(),
                            battChannel->getName(),
                            IOM_ERROR_ToSTRING(iomRet)
                          );
            }
            else
            {
                log.error("%s - Sending Cancel command error: %s",
                            this->toString().c_str(), IOM_ERROR_ToSTRING(iomRet)
                          );
            }
        }
    }
    else
    {
        /* Start operation requested */

        /* Send Select & Operate to Battery test using the configured channel */
        iomRet = battChannel->selOperate(m_operation.duration, m_operation.local);
        {
            if(iomRet != IOM_ERROR_NONE)
            {
                log.error("%s - Sending Operate command error: %s",
                            this->toString().c_str(), IOM_ERROR_ToSTRING(iomRet)
                          );
            }
            else
            {
                log.info("%s - Operation started.", this->toString().c_str());
            }
        }
    }
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
GDB_ERROR BatteryChargerTestLogic::preliminaryChecks()
{
    GDB_ERROR ret = checkAll(m_operation.local);
    if(ret != GDB_ERROR_NONE)
    {
        log.error("%s - Error: %s - %s operation rejected (%s)",
                    this->toString().c_str(),
                    battChannel->getName(),
                    SWITCH_OPERATION_ToSTRING(m_operation.operation),
                    GDB_ERROR_ToSTRING(ret)
                  );
        return ret;
    }
    if(battChannel->isActive() == LU_FALSE)
    {
        log.error("%s - Error: channel not available.", this->toString().c_str());
        return GDB_ERROR_INHIBIT;
    }
    return ret;
}


/*
 *********************** End of file ******************************************
 */

