/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: PLTULogic.cpp 10 Jun 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/CLogic/src/PLTULogic.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       PLTU: Parasitic Load Trip Unit implementation
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 10 Jun 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   10 Jun 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "PLTULogic.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define POINT_VAL_QUALITY(PLTUInput)    \
        (dObservers[PLTUInput]->getPointID().isValid() == LU_FALSE)? 0 : dObservers[PLTUInput]->getValue(), \
        (dObservers[PLTUInput]->getPointID().isValid() == LU_FALSE)? "-" :   \
            ( (dObservers[PLTUInput]->getQuality() == POINT_QUALITY_LAST) ||    \
              (dObservers[PLTUInput]->getQuality() == POINT_QUALITY_INVALID) )? "-absent" :    \
                  (dObservers[PLTUInput]->getQuality() == POINT_QUALITY_OFF_LINE)? "-offline" : ""

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
PLTULogic::PLTULogic(Mutex* mutex,
                     GeminiDatabase& g3database,
                     PLTULogic::Config& conf
                    ) :
                        ControlLogic(mutex, g3database, conf, CONTROL_LOGIC_TYPE_PLTU),
                        operationConfig(conf),
                        stage(PLTU_STAGE_OFF_LINE),
                        tripCB(NULL)
{
    /* Check config limits */
    if(operationConfig.operationParameters.voltageDelay < 1)
    {
        operationConfig.operationParameters.voltageDelay = 1;   //force minimum value
    }

    /* Allocate Control Logic virtual points */
    clPoint.reserve(Config::ALLPOINT_NUM);  //Adjust vector size to do not waste memory
    for(lu_uint32_t id = 0; id < Config::BINARYPOINT_NUM; ++id)
    {
        clPoint.push_back(new ControlLogicDigitalPoint(mutex, database, conf.bPoint[id], POINT_TYPE_BINARY));
    }

    /* Get Digital Input points */
    for(lu_uint32_t i = 0; i < PLTU_INPUT_LAST; ++i)
    {
        /* Create observer */
        dObservers[i] = new DIObserver(conf.dInput[i], *this);
    }
}


PLTULogic::~PLTULogic()
{
    stopUpdate();

    database.cancelJob(tripCB);

    /* Release digital input observers */
    for(lu_uint32_t i = 0; i < PLTU_INPUT_LAST; ++i)
    {
        detachObserver(dObservers[i]);
    }
}


GDB_ERROR PLTULogic::init()
{
    GDB_ERROR ret = initCheck();
    if(ret == GDB_ERROR_NONE)
    {
        ret = activateCLPoints();   //Initialise Control Logic points as well
    }
    if(ret != GDB_ERROR_NONE)
    {
        return ret;
    }

    startCLPoints(true);
    initialised = true;

    /* Initialise input points */
    for (lu_uint32_t i = 0; i < PLTU_INPUT_LAST; ++i) //List of action inputs
    {
        GDB_ERROR res = attachObserver(dObservers[i]);
        if( (res != GDB_ERROR_NONE) &&
            (i != PLTU_INPUT_INHIBIT) && (i != PLTU_INPUT_RESET_INDICATIONS) //Optional input not configured
            )
        {
            ret = res;
        }
    }
    if(ret != GDB_ERROR_NONE)
    {
        //Detach all already attached and disable Control Logic indications
        initialised = false;
        for (lu_uint32_t i = 0; i < PLTU_INPUT_LAST; ++i)
        {
            detachObserver(dObservers[i]);
        }
        startCLPoints(false);
    }
    return ret;
}


GDB_ERROR PLTULogic::getInputValue(lu_uint16_t inputPoint, PointData* data)
{
    if( (data == NULL) ||
        (inputPoint >= PLTU_INPUT_LAST)
      )
    {
        return GDB_ERROR_PARAM;
    }
    if(dObservers[inputPoint] != NULL)
    {
        return database.getValue(dObservers[inputPoint]->getPointID(), data);
    }
    return GDB_ERROR_NOPOINT;
}


GDB_ERROR PLTULogic::getOutputValue(PointData* data)
{
    return database.getOutputValue(PointIdStr(operationConfig.switchCBID, 0), data);
}



/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
void PLTULogic::newData(PointIdStr pointID, PointData& pointData)
{
    GDB_ERROR ret = GDB_ERROR_NONE;

    if(!enabled)
    {
        return;
    }
    LockingMutex lMutex(*globalMutex);

    TimeManager::TimeStr timestamp;
    pointData.getTime(timestamp);

    if(!pointID.isValid())
    {
        return; //invalid point: nothing to do
    }

    if(pointID == dObservers[PLTU_INPUT_RESET_INDICATIONS]->getPointID() )
    {
        //reset indications only
        setValue(PLTU_POINT_INHIBIT, 0, timestamp, ValidFlags);
        setValue(PLTU_POINT_FAULT, 0, timestamp, ValidFlags);
        setValue(PLTU_POINT_OPERATED, 0, timestamp, ValidFlags);
        //Note that PLTU_BPOINT_VMISMATCH is never reset here
    }

    /* Lists of VP to be reflected (immediate update) */
    struct PointReflect
    {
        PLTU_INPUT inputPoint;
        PLTU_POINT pointCL;
    };
    PointReflect reflectVP[] = {
                                    {PLTU_INPUT_SW1_L1, PLTU_POINT_SW1_L1},
                                    {PLTU_INPUT_SW1_L2, PLTU_POINT_SW1_L2},
                                    {PLTU_INPUT_SW1_L3, PLTU_POINT_SW1_L3},
                                    {PLTU_INPUT_SW2_L1, PLTU_POINT_SW2_L1},
                                    {PLTU_INPUT_SW2_L2, PLTU_POINT_SW2_L2},
                                    {PLTU_INPUT_SW2_L3, PLTU_POINT_SW2_L3}
                                    };
    lu_uint32_t size = sizeof(reflectVP) / sizeof(PointReflect);
    TimeManager::TimeStr timestampCLD;
    for (lu_uint32_t i = 0; i < size; ++i)
    {
        if(clPoint[reflectVP[i].pointCL] != NULL)
        {
            if(pointID == operationConfig.dInput[reflectVP[i].inputPoint])
            {
                lu_uint8_t value;
                value = pointData;
                pointData.getTime(timestampCLD);
                setValue(reflectVP[i].pointCL, value, timestampCLD, pointData.getFlags());
            }
        }
    }

    if (!initialised)
        return;

    /* Incoming event: recalculate conditions */
    lu_bool_t trip = LU_FALSE;  //Condition to trip the CB

    ret = preliminaryChecks();

    /* Get associated CB switch OPERATING status */
    do
    {
        if(ret != GDB_ERROR_NONE)
        {
            break;
        }
        PointDataUint8 statusCB;
        PointIdStr statusPointCB;
        statusPointCB.group = operationConfig.switchCBID;
        statusPointCB.ID = SGL_POINT_OPERATING;
        if(database.getValue(statusPointCB, &statusCB) != GDB_ERROR_NONE)
        {
            log.error("%s: Unable to get PLTU %i CB %i Operating status.",
                        __AT__, groupID, statusPointCB.group);
            ret = GDB_ERROR_NOPOINT;
            break;
        }
        lu_uint8_t statusCBval = statusCB;
        if(statusCBval != 0)
        {
            ret = GDB_ERROR_INHIBIT;    //CB already operating!
            break;
        }
        else
        {
            lu_bool_t sw1closed = getDBINARYCloseValue(static_cast<DBINARY_STATUS>(dObservers[PLTU_INPUT_SW1_POS]->getValue()));
            lu_bool_t sw2closed = getDBINARYCloseValue(static_cast<DBINARY_STATUS>(dObservers[PLTU_INPUT_SW2_POS]->getValue()));
            lu_bool_t sw1L1 = dObservers[PLTU_INPUT_SW1_L1]->getValue();
            lu_bool_t sw1L2 = dObservers[PLTU_INPUT_SW1_L2]->getValue();
            lu_bool_t sw1L3 = dObservers[PLTU_INPUT_SW1_L3]->getValue();
            lu_bool_t sw2L1 = dObservers[PLTU_INPUT_SW2_L1]->getValue();
            lu_bool_t sw2L2 = dObservers[PLTU_INPUT_SW2_L2]->getValue();
            lu_bool_t sw2L3 = dObservers[PLTU_INPUT_SW2_L3]->getValue();

            /* The PLTU must trip the CB when any of the closed ring switch have
             * its voltage presence Lx values mismatched.
             */
            if ( (sw1closed && ((sw1L1 != sw1L2) || (sw1L2 != sw1L3)) ) ||
                 (sw2closed && ((sw2L1 != sw2L2) || (sw2L2 != sw2L3)) )
                )
            {
                trip = LU_TRUE;
            }
        }
    } while(0);

    if(ret != GDB_ERROR_NONE)
    {
        if(stage == PLTU_STAGE_OFF_LINE)
        {
            /* TODO: pueyos_a - [PLTU] sure it should set inhibit now? */
            setValue(PLTU_POINT_INHIBIT, 1, timestamp, ValidFlags);
        }
        if(stage == PLTU_STAGE_WAIT_TRIP)
        {
            stage = PLTU_STAGE_OFF_LINE;
            database.cancelJob(tripCB); //cancel waiting job
            setValue(PLTU_POINT_INHIBIT, 1, timestamp, ValidFlags);
            log.debug("%s: PLTU %i trip conditions RESET.", __AT__, groupID);
        }
        return; //no trip
    }


    if(stage == PLTU_STAGE_OFF_LINE)
    {
        if(trip == LU_TRUE)
        {
            //start CB trip operation count
            tripCB = new PLTUTimeoutTrip(*this, operationConfig.operationParameters.voltageDelay);
            database.addJob(tripCB);
            stage = PLTU_STAGE_WAIT_TRIP;   //now waiting before tripping CB
            /* Clear the rest of indicators */
            setValue(PLTU_POINT_INHIBIT, 0, timestamp, ValidFlags);
            setValue(PLTU_POINT_FAULT, 0, timestamp, ValidFlags);
            setValue(PLTU_POINT_OPERATED, 0, timestamp, ValidFlags); //too early to clear??
            //Note that PLTU_BPOINT_VMISMATCH is never reset here

            log.debug("%s: PLTU %i trip conditions fulfilled.", __AT__, groupID);
        }
    }
    if(stage == PLTU_STAGE_WAIT_TRIP)
    {
        //Already detected a fail: check for conditions reset (fault clearing)
        if(trip == LU_FALSE)
        {
            stage = PLTU_STAGE_OFF_LINE;    //abort trip
            database.cancelJob(tripCB);     //cancel waiting job
            setValue(PLTU_POINT_INHIBIT, 1, timestamp, ValidFlags);
            log.debug("%s: PLTU %i trip conditions reset.", __AT__, groupID);
        }
    }
}


void PLTULogic::tripCBTimeout(TimeManager::TimeStr& timestamp)
{
    /* Unlink Timed Job (will be deleted by the Scheduler) */
    tripCB = NULL;

    if(stage != PLTU_STAGE_WAIT_TRIP)
    {
        log.debug("PLTU %i trip CB switch %i was aborted",
                        groupID, operationConfig.switchCBID);
        setValue(PLTU_POINT_INHIBIT, 1, timestamp, ValidFlags);    //internal error
        return;
    }
    SwitchLogicOperation switchOperation;
    switchOperation.local = LU_FALSE;                   //remote operation only
    switchOperation.operation = SWITCH_OPERATION_OPEN;  //equivalent to trip
    GDB_ERROR res = database.startOperation(operationConfig.switchCBID, switchOperation);
    if(res != GDB_ERROR_NONE)
    {
        setValue(PLTU_POINT_FAULT, 1, timestamp, ValidFlags);
        log.error("PLTU %i is unable to trip CB switch %i, result: %s",
                    groupID, operationConfig.switchCBID, GDB_ERROR_ToSTRING(res)
                    );
    }
    else
    {
        //Operation started
        setValue(PLTU_POINT_OPERATED, 1, timestamp, ValidFlags);
        stage = PLTU_STAGE_OFF_LINE;
        log.info("PLTU %i trips CB switch %i", groupID, operationConfig.switchCBID);
    }
    //Note: no need to wait for the CL switch to finish operation
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
GDB_ERROR PLTULogic::preliminaryChecks()
{
    GDB_ERROR ret = GDB_ERROR_NONE;

    for(lu_uint32_t i = 0; i < PLTU_INPUT_LAST; ++i)
    {
        if(dObservers[i] == NULL)
        {
            return GDB_ERROR_NOT_INITIALIZED;
        }
    }

    /* Check validity of all the inputs.
     */

    /* Check Inhibit input points */
    if( (checkDIObserver(PLTU_INPUT_INHIBIT, LU_TRUE) == LU_TRUE) ||
        (checkDIObserver(PLTU_INPUT_OLR    , OLR_STATE_OFF) == LU_TRUE) ||  //OLR not in remote
        (checkDIObserver(PLTU_INPUT_OLR    , OLR_STATE_LOCAL) == LU_TRUE) ||
        (checkDIObserver(PLTU_INPUT_POSITION, DBINARY_OFF) == LU_TRUE) ||    //CB open
        (checkDIObserver(PLTU_INPUT_SW1_POS, DBINARY_INDETERMINATE) == LU_TRUE) ||  //Any switch in DBI
        (checkDIObserver(PLTU_INPUT_SW2_POS, DBINARY_INDETERMINATE) == LU_TRUE) ||
        //Lx values do not need to check the value, only if they are offline:
        (checkDIObserver(PLTU_INPUT_SW1_L1) == LU_TRUE) ||
        (checkDIObserver(PLTU_INPUT_SW1_L2) == LU_TRUE) ||
        (checkDIObserver(PLTU_INPUT_SW1_L3) == LU_TRUE) ||
        (checkDIObserver(PLTU_INPUT_SW2_L1) == LU_TRUE) ||
        (checkDIObserver(PLTU_INPUT_SW2_L2) == LU_TRUE) ||
        (checkDIObserver(PLTU_INPUT_SW2_L3) == LU_TRUE)
      )
    {
        log.debug("PLTU %s Inhibited: "
                    "OLR[%i]:%i%s|Inh:%i%s|"
                    "CBpos[%i]:%i%s|Sw1pos:%i%s|Sw2pos:%i%s|"
                    "Sw1.L1:%i%s|Sw1.L2:%i%s|Sw1.L3:%i%s|"
                    "Sw2.L1:%i%s|Sw2.L2:%i%s|Sw2.L3:%i%s.",
                    this->toString().c_str(),
                    OLR_STATE_REMOTE,
                    POINT_VAL_QUALITY(PLTU_INPUT_OLR),
                    POINT_VAL_QUALITY(PLTU_INPUT_INHIBIT),
                    DBINARY_ON,
                    POINT_VAL_QUALITY(PLTU_INPUT_POSITION),
                    POINT_VAL_QUALITY(PLTU_INPUT_SW1_POS),
                    POINT_VAL_QUALITY(PLTU_INPUT_SW2_POS),
                    POINT_VAL_QUALITY(PLTU_INPUT_SW1_L1),
                    POINT_VAL_QUALITY(PLTU_INPUT_SW1_L2),
                    POINT_VAL_QUALITY(PLTU_INPUT_SW1_L3),
                    POINT_VAL_QUALITY(PLTU_INPUT_SW2_L1),
                    POINT_VAL_QUALITY(PLTU_INPUT_SW2_L2),
                    POINT_VAL_QUALITY(PLTU_INPUT_SW2_L3)
                  );
        ret = GDB_ERROR_INHIBIT;
    }

    GDB_ERROR dbi = checkSwitchesMismatch();
    TimeManager::TimeStr timestamp = timeManager.now();
    setValue(PLTU_POINT_VMISMATCH, (dbi==GDB_ERROR_NONE)? 0 : 1, timestamp, ValidFlags);
    if(dbi != GDB_ERROR_NONE)
    {
        ret = GDB_ERROR_INHIBIT;
    }
    return ret;
}


inline lu_bool_t PLTULogic::checkDIObserver(PLTU_INPUT digitalInput)
{
    if(digitalInput < PLTU_INPUT_LAST)
    {

        DIObserver *observerPtr = dObservers[digitalInput];
        if(observerPtr->getPointID().isValid() == LU_TRUE)
        {
            if(observerPtr->getQuality() != POINT_QUALITY_ON_LINE)
            {
                return LU_TRUE;
            }
        }
    }

    return LU_FALSE;
}

template <typename ValueType>
inline lu_bool_t PLTULogic::checkDIObserver(PLTU_INPUT digitalInput,
                                            ValueType checkValue
                                            )
{
    if(digitalInput < PLTU_INPUT_LAST)
    {

        DIObserver *observerPtr = dObservers[digitalInput];
        if(observerPtr->getPointID().isValid() == LU_TRUE)
        {
            if( (observerPtr->getValue() == checkValue) ||
                (observerPtr->getQuality() != POINT_QUALITY_ON_LINE)
                )
            {
                return LU_TRUE;
            }
        }
    }

    return LU_FALSE;
}

GDB_ERROR PLTULogic::checkSwitchesMismatch()
{
    GDB_ERROR ret = GDB_ERROR_INHIBIT;

    /* When both ring switches are closed, their voltage presences must match -
     * otherwise is a DBI (there is a difference among all SWnLx values: sensor failure).
     */
    lu_bool_t sw1closed = getDBINARYCloseValue(static_cast<DBINARY_STATUS>(dObservers[PLTU_INPUT_SW1_POS]->getValue()));
    lu_bool_t sw2closed = getDBINARYCloseValue(static_cast<DBINARY_STATUS>(dObservers[PLTU_INPUT_SW2_POS]->getValue()));
    lu_bool_t sw1L1 = dObservers[PLTU_INPUT_SW1_L1]->getValue();
    lu_bool_t sw1L2 = dObservers[PLTU_INPUT_SW1_L2]->getValue();
    lu_bool_t sw1L3 = dObservers[PLTU_INPUT_SW1_L3]->getValue();
    lu_bool_t sw2L1 = dObservers[PLTU_INPUT_SW2_L1]->getValue();
    lu_bool_t sw2L2 = dObservers[PLTU_INPUT_SW2_L2]->getValue();
    lu_bool_t sw2L3 = dObservers[PLTU_INPUT_SW2_L3]->getValue();
    if( (sw1closed && sw2closed) &&
        ( (sw1L1 != sw2L1) || (sw1L2 != sw2L2) || (sw1L3 != sw2L3) )
      )
    {
        log.debug("PLTU %d Inhibited: "
                  "SWnLx mismatch! (Sensor DBI):"
                  "Sw1pos:%i%s|Sw2pos:%i%s|"
                  "Sw1.L1:%i%s|Sw1.L2:%i%s|Sw1.L3:%i%s|"
                  "Sw2.L1:%i%s|Sw2.L2:%i%s|Sw2.L3:%i%s.",
                  groupID,
                  POINT_VAL_QUALITY(PLTU_INPUT_SW1_POS),
                  POINT_VAL_QUALITY(PLTU_INPUT_SW2_POS),
                  POINT_VAL_QUALITY(PLTU_INPUT_SW1_L1),
                  POINT_VAL_QUALITY(PLTU_INPUT_SW1_L2),
                  POINT_VAL_QUALITY(PLTU_INPUT_SW1_L3),
                  POINT_VAL_QUALITY(PLTU_INPUT_SW2_L1),
                  POINT_VAL_QUALITY(PLTU_INPUT_SW2_L2),
                  POINT_VAL_QUALITY(PLTU_INPUT_SW2_L3)
                );
    }
    else
    {
        ret = GDB_ERROR_NONE;
    }
    return ret;
}

/*
 *********************** End of file ******************************************
 */
