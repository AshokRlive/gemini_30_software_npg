/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Control Logic Block factory interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   28/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_EA2B8859_FA91_4d01_8797_CE5D6B02AE4B__INCLUDED_)
#define EA_EA2B8859_FA91_4d01_8797_CE5D6B02AE4B__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "ControlLogic.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class IControlLogicFactory
{

public:

    IControlLogicFactory() {};
    virtual ~IControlLogicFactory() {};

    /**
     * \brief Configures the common mutex lock
     *
     * \param mutex Reference to the common mutex
     */
    virtual void setMutexLock(Mutex& mutex) = 0;

    /**
	 * \brief Get the number of control logic block that the factory can create
	 * 
	 * \return Control logic block number
	 */
    virtual lu_uint16_t getCLogicNumber() = 0;

	/**
	 * \brief Get a new control logic block
	 * 
	 * \param mutex Global mutex
	 * \param database local database
	 * \param group the group ID of the control logic to create
	 * 
	 * \return Pointer to the control logic. NULL in case of error
	 */
	virtual ControlLogic* newCLogic(//Mutex* mutex            ,
	                                //GeminiDatabase& database,
	                                lu_uint16_t group
	                               ) =0;
};
#endif // !defined(EA_EA2B8859_FA91_4d01_8797_CE5D6B02AE4B__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
