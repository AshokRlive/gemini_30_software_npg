/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: PseudoClockLogic.h 26 Jun 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/CLogic/include/PseudoClockLogic.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Pseudo-Clock Control Logic.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 26 Jun 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   26 Jun 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef PSEUDOCLOCKLOGIC_H__INCLUDED
#define PSEUDOCLOCKLOGIC_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "LockingMutex.h"
#include "OperatableControlLogic.h"
#include "ControlLogicDigitalPoint.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief PCLK Pseudo-Clock Control Logic
 *
 * This class toggles an output every minute on the minute.
 * Actually, is every time the seconds reaches 0 in the real-time clock, thus
 * making any clock adjustments to re-adjust this as well. Effectively any
 * clock adjustment can make this pseudo-clock to take less or more than an
 * elapsed (monotonic) minute.
 */
class PseudoClockLogic: public OperatableControlLogic, private TimeObserver
{
public:
    /**
     * \brief configuration block for PCLK Control Logic
     */
    struct Config : public OperatableControlLogic::Config
    {
    public:
        /* Amounts of different types of Control Logic's Pseudo Points */
        static const lu_uint16_t BINARYPOINT_NUM = PCLK_POINT_TYPE_BINARY_COUNT;
        static const lu_uint32_t ALLPOINT_NUM = PCLK_POINT_EnumSize;
        enum TIMING
        {
            TIMING_OCLOCK,     //Time specification is seconds from o'clock
            TIMING_INTERVAL    //Time specification is an interval, every n milliseconds
        };
    public:
        ControlLogicDigitalPoint::Config bPoint[BINARYPOINT_NUM];    //CL digital Points
        lu_bool_t startupEnabled;   //Control Logic enabled at startup
        lu_uint32_t pulseWidth_ms;  //For Pulse events, width of the pulse (in milliseconds) - 0 for toggle
        TIMING timingType;          //Type of timing for tick generation
        lu_uint32_t tickTime_ms;    //milliseconds for ticking
    public:
        Config() :  startupEnabled(LU_TRUE),
                    pulseWidth_ms(0),
                    timingType(TIMING_OCLOCK),
                    tickTime_ms(60000)             //Default 1 minute
        {};
        bool validate()
        {
            return ((pulseWidth_ms < tickTime_ms) && //Valid pulse width
                    ((timingType == TIMING_OCLOCK) || (timingType == TIMING_INTERVAL))
                   );
        }
    };

public:
    /**
     * \brief Custom constructor
     *
     * \param mutex Reference to the G3DB common mutex
     * \param database Reference to the Gemini database
     * \param config reference to the Control Logic's configuration block
     */
    PseudoClockLogic(Mutex *mutex, GeminiDatabase& database, PseudoClockLogic::Config& config);
    virtual ~PseudoClockLogic();

    /* == Inherited from ControlLogic == */
    virtual GDB_ERROR init();
    virtual GDB_ERROR getInputValue(lu_uint16_t inputPoint, PointData *data);
    virtual GDB_ERROR getOutputValue(PointData* data);

protected:
    /* == Implements mandatory OperatableControlLogic == */
    virtual GDB_ERROR startOp(const SwitchLogicOperation &operation, const GDB_ERROR errorCode);

    void tickUpdate(TimeManager::TimeStr* timerPtr);

protected:
    /**
     * \brief Class for tick generation
     */
    class PeriodicJob : public TimedJob
    {
    public:
        PeriodicJob(PseudoClockLogic& cLogicPCL, const lu_uint32_t timerInterval) :
                            TimedJob(timerInterval, timerInterval),
                            cLogic(cLogicPCL)
        {};
        virtual ~PeriodicJob() {};

    protected:
        /* == Inherited from TimedJob == */
        virtual void job(TimeManager::TimeStr* timerPtr);

    private:
        PseudoClockLogic& cLogic;
    };

    /**
     * \brief Class for generating a pulse output
     */
    class PulseJob : public TimedJob
    {
    public:
        PulseJob(PseudoClockLogic& cLogicPCL, const lu_uint32_t timerInterval) :
                            TimedJob(timerInterval, 0),
                            cLogic(cLogicPCL)
        {};
        virtual ~PulseJob() {};

    protected:
        /* == Inherited from TimedJob == */
        virtual void job(TimeManager::TimeStr* timerPtr);

    private:
        PseudoClockLogic& cLogic;
    };

private:
    /**
     * \brief Effectively change the value of the output
     *
     * Changes the internal value and updates the output pount if enabled.
     *
     * \param newValue New value to set to the output
     * \param timeStamp Time stamp of the event that made the change
     */
    void updateValue(const bool newValue, const TimeManager::TimeStr& timeStamp);


private:
    /* == Inherited from TimeObserver == */
    virtual void updateTime(TimeManager::TimeStr& currentTime);

    /**
     * \brief Disables the Control Logic when error or disabled
     *
     * \param timeStamp Time stamp of the event that made to disable the Control Logic
     */
    virtual void disableAll(const TimeManager::TimeStr& timeStamp);

    /**
     * \brief Set the all Control Logic Points online/offline
     *
     * \param online Quality to be set for all Control Logic Points
     * \param timeStamp Time stamp of the event that made to disable the Control Logic
     */
    void setOutQuality(const bool online, const TimeManager::TimeStr& timeStamp);

    /**
     * \brief Change the value of the output, according to configuration
     *
     * \param timeStamp Time stamp of the event that made the change
     */
    void changeValue(const TimeManager::TimeStr& timeStamp);

    /**
     * \brief Start counting the clock tick
     *
     * \return Operation successful when true
     */
    bool startTicking();

    /**
     * \brief Stop counting the clock tick
     */
    void stopTicking();

private:
    PseudoClockLogic::Config m_config;  //Configuration
    bool m_outEnabled;          //Enables/disables output change
    bool m_value;               //Value for output

    MasterMutex m_enLock;       //Lock for enable/disable operations

    PeriodicJob* m_tickTimer;   //Periodic job for time tick when interval is specified
    PulseJob* m_pulseTimer;     //Periodic job for pulse generation
};

#endif /* PSEUDOCLOCKLOGIC_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
