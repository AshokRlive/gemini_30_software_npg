/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: CounterCommandLogic.cpp 8 Dec 2015 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/CLogic/src/ControlLogic/CounterCommandLogic.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 8 Dec 2015 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   8 Dec 2015   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "CounterCommandLogic.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

CounterCommandLogic::CounterCommandLogic(Mutex* mutex,
                                        GeminiDatabase& g3database,
                                        CounterCommandLogic::Config& config
                                        ) :
                                        OperatableControlLogic(mutex, g3database, config, CONTROL_LOGIC_TYPE_COUNTER_COMMAND),
                                        conf(config)
{}

CounterCommandLogic::~CounterCommandLogic()
{
}


GDB_ERROR CounterCommandLogic::init()
{
    GDB_ERROR ret = initCheck();
    if(database.getPointType(conf.cPointID) != POINT_TYPE_COUNTER)
    {
        log.error("%s: Point %s is not a counter point.",
                        this->toString().c_str(),
                        conf.cPointID.toString().c_str());
        ret = GDB_ERROR_CONFIG;
    }
    if(ret == GDB_ERROR_NONE)
    {
        /* Initialise input points */
        ret = initInhibit();
    }
    if(ret == GDB_ERROR_NONE)
    {
        initialised = true;
    }
    return ret;
}


GDB_ERROR CounterCommandLogic::getInputValue(lu_uint16_t inputPoint, PointData* data)
{
    LU_UNUSED(inputPoint);
    LU_UNUSED(data);

    return GDB_ERROR_NOT_SUPPORTED;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
GDB_ERROR CounterCommandLogic::startOp(const SwitchLogicOperation& operation,
                                        const GDB_ERROR errorCode)
{
    GDB_ERROR ret;

    log.info("%s: Request operation: %i - Local: %i",
             this->toString().c_str(), operation.operation, operation.local
            );

    if(errorCode != GDB_ERROR_NONE)
    {
        refuseOperationMsg(errorCode);
        return errorCode;
    }

    LockingMutex lMutex(*globalMutex);

    /* Send command to output point */
    TimeManager::TimeStr timeStamp = timeManager.now(); //Time stamp of the request
    switch(conf.action)
    {
        case COUNTER_COMMAND_ACTION_CLEAR:
            ret = database.clear(conf.cPointID);
            break;
        case COUNTER_COMMAND_ACTION_FREEZE:
            ret = database.freeze(conf.cPointID, LU_FALSE);
            break;
        case COUNTER_COMMAND_ACTION_FREEZE_AND_CLEAR:
            ret = database.freeze(conf.cPointID, LU_TRUE);
            break;
        default:
            ret = GDB_ERROR_NOT_SUPPORTED;
            break;
    }
    if(ret != GDB_ERROR_NONE)
    {
        refuseOperationMsg(ret);
    }
    else
    {
        log.info("%s: %s action done over %s.",
                 this->toString().c_str(),
                 COUNTER_COMMAND_ACTION_ToSTRING(conf.action),
                 conf.cPointID.toString().c_str());
    }
    return ret;

}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
