/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: DigitalControlPointLogic.cpp 2 Sep 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/CLogic/src/ControlLogic/DigitalControlPointLogic.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Digital Control Point control logic.
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 2 Sep 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   2 Sep 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "DigitalControlPointLogic.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

DigitalControlPointLogic::DigitalControlPointLogic( Mutex* mutex,
                                                    GeminiDatabase& g3database,
                                                    DigitalControlPointLogic::Config& config
                                                    ) :
                                                    OperatableControlLogic(mutex, g3database, config, CONTROL_LOGIC_TYPE_DIGCTRLPOINT),
                                                    conf(config),
                                                    pulseEndJob(NULL)
{
    /* Allocate Control Logic virtual points */
    clPoint.reserve(Config::ALLPOINT_NUM);  //Adjust vector size to do not waste memory

    for(lu_uint32_t id = 0; id < Config::BINARYPOINT_NUM; ++id)
    {
        clPoint.push_back(new ControlLogicDigitalPoint(mutex, database, config.bPoint[id], POINT_TYPE_BINARY));
    }
}


DigitalControlPointLogic::~DigitalControlPointLogic()
{
    database.cancelJob(pulseEndJob);
}


GDB_ERROR DigitalControlPointLogic::init()
{
    GDB_ERROR ret = initCheck();
    if(ret == GDB_ERROR_NONE)
    {
        ret = activateCLPoints();   //Initialise Control Logic points as well
    }
    if(ret != GDB_ERROR_NONE)
    {
        return ret;
    }

    startCLPoints(true);
    initialised = true;

    /* Initialise input points */
    ret = initInhibit();
    if(ret != GDB_ERROR_NONE)
    {
        //Detach all already attached and disable Control Logic indications
        initialised = false;
        startCLPoints(false);
    }
    return ret;
}


GDB_ERROR DigitalControlPointLogic::getInputValue(lu_uint16_t inputPoint, PointData *data)
{
    return checkInputValue(DIGCTRL_INPUT_OLR, DIGCTRL_INPUT_INHIBIT, inputPoint, data);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
GDB_ERROR DigitalControlPointLogic::startOp(const SwitchLogicOperation& operation,
                                            const GDB_ERROR errorCode)
{
    log.info("%s: Request operation: %i - Local: %i",
             this->toString().c_str(), operation.operation, operation.local
            );

    if(errorCode != GDB_ERROR_NONE)
    {
        refuseOperationMsg(errorCode);
        return errorCode;
    }

    LockingMutex lMutex(*globalMutex);

    if(pulseEndJob != NULL)
    {
        refuseOperationMsg(GDB_ERROR_ALREADY_ACTIVE);
        return GDB_ERROR_ALREADY_ACTIVE;
    }

    /* Set output value */
    TimeManager::TimeStr timeStamp = timeManager.now(); //Time stamp of the request
    bool newOutput = (operation.operation == SWITCH_OPERATION_CLOSE);
    if(conf.pulseEnabled)
    {
        if( (conf.trigger == EDGE_DRIVEN_MODE_BOTH) ||
            ( (newOutput == true) && (conf.trigger == EDGE_DRIVEN_MODE_RISE) ) ||
            ( (newOutput == false) && (conf.trigger == EDGE_DRIVEN_MODE_FALL) )
            )
        {
            //Start pulse:
            setValue(DIGCTRL_POINT_OUTPUT, 1, timeStamp, ValidFlags);
            database.cancelJob(pulseEndJob);
            pulseEndJob = new PulseEnd(*this, conf.pulseDuration_ms);
            database.addJob(pulseEndJob);

            log.info("%s - started pulse in output for %i ms",
                    this->toString().c_str(), conf.pulseDuration_ms);

        }
    }
    else
    {
        //Set output value to the one given:
        setValue(DIGCTRL_POINT_OUTPUT, newOutput, timeStamp, ValidFlags);

        log.info("%s: set to %i successfully", this->toString().c_str(), (newOutput)? 1 : 0);
    }
    return GDB_ERROR_NONE;
}


void DigitalControlPointLogic::endPulse(TimeManager::TimeStr& timeStamp)
{
    /* Unlink Timed Job (will be deleted by the Scheduler) */
    pulseEndJob = NULL;

    setValue(DIGCTRL_POINT_OUTPUT, 0, timeStamp, ValidFlags);
    log.info("%s - pulse finished", this->toString().c_str());
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
