/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Control Logic custom digital point implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "ControlLogicDigitalPoint.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define CLPOINT_INVALID_VALUE 99

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
ControlLogicDigitalPoint::ControlLogicDigitalPoint(Mutex* gMutex,
                                                   GeminiDatabase& g3database,
                                                   DigitalPoint::Config& config,
                                                   POINT_TYPE pointType) :
                                    DigitalPoint(gMutex, g3database, pointType, config),
                                    incomingValue(CLPOINT_INVALID_VALUE)
{
    if(pointType == POINT_TYPE_DBINARY)
    {
        m_conf.invert = LU_FALSE;   //Double Binary points cannot be inverted
    }
    m_delayList = m_conf.sortDelay(pointType);  //Sort delay for delayed report
}


GDB_ERROR ControlLogicDigitalPoint::init()
{
    if(initialised)
        return GDB_ERROR_INITIALIZED;

    return DigitalPoint::init();
}


GDB_ERROR ControlLogicDigitalPoint::setValue(const lu_uint32_t value,
                                             const TimeManager::TimeStr& newTimeStamp,
                                             const lu_bool_t forced
                                            )
{
    return setValue(value, newTimeStamp, m_preData.getFlags(), forced);
}


GDB_ERROR ControlLogicDigitalPoint::setValue(const lu_uint32_t value,
                                             const TimeManager::TimeStr& newTimeStamp,
                                             const PointFlags& flags,
                                             const lu_bool_t forced
                                            )
{


    /* XXX: pueyos_a -  (to be removed) */
//    if(pointID == PointIdStr(5,10))
//            {
//                log.debug("%s - setValue to %d %s scheduled", pointID.toString().c_str(), value, flags.toString().c_str());
//            }



    //Please note that Initial/Invalid flags will be reset in any case
    GDB_ERROR ret;
    PointDataUint8 pointData = m_data;
    lu_uint32_t dataChange = DATACHANGE_NONE;

    if(forced == LU_TRUE)
    {
        dataChange = dataChange | DATACHANGE_FORCE;
    }

    /* XXX: pueyos_a - datachange (to be removed) */
//    if(value != incomingValue)
//    {
//        incomingValue = value;
//        dataChange = dataChange | DATACHANGE_VALUE;
//    }
//    if(flags != incomingFlags)
//    {
//        incomingFlags = flags;
//        dataChange = dataChange | DATACHANGE_FLAGS;
//    }
    incomingValue = value;
    incomingFlags = flags;

    pointData.setFlags(flags);
    pointData.setInitialFlag(false); //Not the initial value anymore
    pointData.setTime(newTimeStamp);
    pointData = value;

//    ret = scheduleUpdate(*this, pointData, static_cast<DATACHANGE>(dataChange));
    ret = scheduleUpdate(*this, pointData);
    if(ret != GDB_ERROR_NONE)
    {
        log.error("%s: CL Digital Point (%s) - error: %s",
                    __AT__, pointID.toString().c_str(), GDB_ERROR_ToSTRING(ret)
                  );
    }
    return ret;
}


GDB_ERROR ControlLogicDigitalPoint::setQuality(const lu_bool_t newStatus,
                                               const TimeManager::TimeStr& newTimeStamp
                                               )
{
    /* FIXME: pueyos_a - [DACTION] use m_data or incoming??? */
    incomingFlags.setQuality((newStatus == LU_TRUE)? POINT_QUALITY_ON_LINE : POINT_QUALITY_OFF_LINE);
    return AbstractPoint::setQuality(m_data, newStatus, newTimeStamp);
}


GDB_ERROR ControlLogicDigitalPoint::setFlags(const PointFlags& newFlags, const TimeManager::TimeStr& timestamp)
{
    incomingFlags = newFlags;
    return AbstractPoint::setFlags(m_data, newFlags, timestamp);
}


void ControlLogicDigitalPoint::update(PointData* newData, DATACHANGE dataChange)
{
    updatePoint(*(dynamic_cast<PointDataUint8*>(newData)), (dataChange & DATACHANGE_FORCE), true);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */


