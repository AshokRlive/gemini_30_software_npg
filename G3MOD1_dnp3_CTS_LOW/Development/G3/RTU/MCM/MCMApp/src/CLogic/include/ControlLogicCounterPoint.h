/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ControlLogicCounterPoint.h 14 Jul 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/CLogic/include/ControlLogicCounterPoint.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Control Logic custom Counter Point interface
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 14 Jul 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14 Jul 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef CONTROLLOGICCOUNTERPOINT_H__INCLUDED
#define CONTROLLOGICCOUNTERPOINT_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "CounterPoint.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Implementation for Control Logic's Counter pseudo point.
 *
 * The data change source is only from the setter methods.
 */
class ControlLogicCounterPoint: public CounterPoint
{
public:
    ControlLogicCounterPoint(Mutex *mutex,
                             GeminiDatabase& g3database  ,
                             CounterPoint::Config &config);
    virtual ~ControlLogicCounterPoint();

    /* == Inherited from IPoint == */
    virtual GDB_ERROR init();

    /* == Inherited from CounterPoint == */
    virtual GDB_ERROR setFlags(const PointFlags& newFlags, const TimeManager::TimeStr& timestamp); //Now public

    /**
     * \brief Write a new value to the point
     *
     * This sets the counter to an specific value
     *
     * \param value Value to write
     * \param timestamp Time of the change
     * \param flags Point flags to set (optional)
     * \param forced Set to LU_TRUE to force point update even if it is the same (optional)
     *
     * \return Error code
     */
    GDB_ERROR setValue( const lu_uint32_t value,
                        const TimeManager::TimeStr& timestamp,
                        const lu_bool_t forced = LU_FALSE
                        );
    GDB_ERROR setValue( const lu_uint32_t value,
                        const TimeManager::TimeStr& timestamp,
                        const PointFlags& flags,
                        const lu_bool_t forced = LU_FALSE
                        );
    /**
     * \brief Write a new status to the point
     *
     * \param status Online/offline status
     * \param timestamp Time of the change
     *
     * \return Error code
     */
    GDB_ERROR setQuality(const lu_bool_t status, const TimeManager::TimeStr& timestamp);

    /**
     * \brief Set the current value as the frozen value
     *
     * \param timestamp Time of the change
     *
     * \return Error code
     */
    void freeze(TimeManager::TimeStr& timestamp);

protected:
    /* == Inherited from DigitalPoint == */
    virtual void update(PointData* newData, DATACHANGE dataChange);

private:
    PointFlags incomingFlags;   //Latest flags set with setValue/setFlags/setQuality
};


#endif /* CONTROLLOGICCOUNTERPOINT_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
