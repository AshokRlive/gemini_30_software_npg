/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: AnalogControlPointLogic.cpp 2 Sep 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/CLogic/src/ControlLogic/AnalogControlPointLogic.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Analog Control Point control logic.
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 2 Sep 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   2 Sep 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "AnalogControlPointLogic.h"
#include "CLogicDebug.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

AnalogControlPointLogic::AnalogControlPointLogic( Mutex* mutex,
                                                    GeminiDatabase& g3database,
                                                    AnalogControlPointLogic::Config& config
                                                    ) :
                                                    OperatableControlLogic(mutex, g3database, config, CONTROL_LOGIC_TYPE_ANALOGCTRLPOINT),
                                                    conf(config)

{
    /* Allocate Control Logic virtual points */
    clPoint.reserve(Config::ALLPOINT_NUM);  //Adjust vector size to do not waste memory

    for(lu_uint32_t id = 0; id < Config::ANALOGPOINT_NUM; ++id)
    {
        clPoint.push_back(new ControlLogicAnaloguePoint(mutex, database, config.aPoint[id]));
    }

    DBG_TRACK("AnalogControlPointLogic constructed");
}


AnalogControlPointLogic::~AnalogControlPointLogic()
{
}


GDB_ERROR AnalogControlPointLogic::init()
{
    GDB_ERROR ret = initCheck();
    if(ret == GDB_ERROR_NONE)
    {
        ret = activateCLPoints();   //Initialise Control Logic points
    }
    if(ret == GDB_ERROR_NONE)
    {
        /* Start Control Logic Points */
        ret = startCLPoints(true);  //All set to Initial, Invalid, and online
    }
    if(ret == GDB_ERROR_NONE)
    {
        /* Initialise input points */
        ret = initInhibit();
    }
    if(ret == GDB_ERROR_NONE)
    {
        initialised = true;
    }
    return ret;
}


GDB_ERROR AnalogControlPointLogic::getInputValue(lu_uint16_t inputPoint, PointData *data)
{
    return checkInputValue(DIGCTRL_INPUT_OLR, DIGCTRL_INPUT_INHIBIT, inputPoint, data);
}


GDB_ERROR AnalogControlPointLogic::writeOutputValue(AnalogueOutputOperation &val)
{
    DBG_INFO("writeOutputValue called(), val: %f",val.value);

    TimeManager::TimeStr timeStamp = timeManager.now(); //Time stamp of the request

    /* Is mandatory that incoming values must have valid flags */
    return setValue(ANALOGCTRL_POINT_OUTPUT, val.value, timeStamp, ValidFlags);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
GDB_ERROR AnalogControlPointLogic::startOp(const SwitchLogicOperation& operation,
                                            const GDB_ERROR errorCode)
{
    LU_UNUSED(operation);
    LU_UNUSED(errorCode);

    log.warn("%s: unsupported operation:startOp", this->toString().c_str());
    return GDB_ERROR_NOT_SUPPORTED;
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
