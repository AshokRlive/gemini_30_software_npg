/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: OperatableControlLogic.cpp 4 Mar 2015 pueyos_a $
 *               $HeadURL: http://10.11.0.6:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/CLogic/src/ControlLogic/OperatableControlLogic.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 4 Mar 2015 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   4 Mar 2015   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "OperatableControlLogic.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
OperatableControlLogic::OperatableControlLogic(Mutex *mutex,
                                            GeminiDatabase& database,
                                            const OperatableControlLogic::Config config,
                                            const CONTROL_LOGIC_TYPE CLtype
                                            ) :
                                                ControlLogic(mutex, database, config, CLtype),
                                                m_inhibitPoint(config.inhibitPoint),
                                                m_inhibited(false),
                                                m_dependsOLR(config.useLocalRemote)
{
    m_inhObserver = new DIObserver(m_inhibitPoint.pointID, *this);
}

OperatableControlLogic::~OperatableControlLogic()
{
    database.detach(m_inhibitPoint.pointID, m_inhObserver);
    delete m_inhObserver;
}


GDB_ERROR OperatableControlLogic::startOperation(SwitchLogicOperation& operation)
{
    GDB_ERROR ret = GDB_ERROR_NONE;

    if(!initialised)
    {
        ret = GDB_ERROR_NOT_INITIALIZED;
    }
    else if(!enabled)
    {
        ret = GDB_ERROR_NOT_ENABLED;
    }
    else if(m_inhibited)
    {
        ret = GDB_ERROR_INHIBIT;
    }
    else
    {
        ret = checkOLR(operation.local);
    }
    return startOp(operation, ret);
}


GDB_ERROR OperatableControlLogic::getOutputValue(PointData* data)
{
    /* Default behaviour - Operatable is ONLINE by default */
    if(data == NULL)
    {
        return GDB_ERROR_PARAM;
    }
    if(!initialised)
    {
        return GDB_ERROR_NOT_INITIALIZED;
    }
    data->setOnlineFlag(true);  //always online
    return GDB_ERROR_NONE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
void OperatableControlLogic::newData(PointIdStr pointID, PointData& pointData)
{
    //Newdata of inhibition input only

    /* Check inhibition point */
    if( (m_inhibitPoint.pointID.isValid()) && (m_inhibitPoint.pointID == pointID) )
    {
        //Digital input
        if(pointData.isActive())
        {
            m_inhibited = (DigitalPointValue)pointData;
        }
        else
        {
            m_inhibited = true; //DBI means it inhibits
        }
    }
}


GDB_ERROR OperatableControlLogic::initInhibit()
{
    GDB_ERROR ret = GDB_ERROR_NONE;

    //Check point validity
    if(m_inhibitPoint.pointID.isValid())
    {
        /* Digital observer */
        ret = database.attach(m_inhibitPoint.pointID, m_inhObserver);
        if(ret != GDB_ERROR_NONE)
        {
            logInputPointAttachError(m_inhibitPoint.pointID, ret);
        }
    }
    else if(m_inhibitPoint.mandatory)
    {
        //Failed to configure a mandatory point: error
        log.error("%s Mandatory virtual input point %d not configured!",
                    this->toString().c_str(),
                    m_inhibitPoint.index
                    );
        ret = GDB_ERROR_NOT_INITIALIZED;
    }
    return ret;
}


GDB_ERROR OperatableControlLogic::checkOLR(const lu_bool_t local)
{
    if(m_dependsOLR)
    {
        OLR_STATE olr;
        if(database.getLocalRemote(olr) == GDB_ERROR_NONE)
        {
            if( ( (local == LU_TRUE) && (olr != OLR_STATE_LOCAL) ) ||
                ( (local == LU_FALSE) && (olr != OLR_STATE_REMOTE) )
                )
            {
                return GDB_ERROR_LOCAL_REMOTE;
            }
        }
        else
        {
            log.warn("%s: Local/Remote input point is offline.", this->toString().c_str());
            return GDB_ERROR_INHIBIT;
        }
    }
    return GDB_ERROR_NONE;
}


GDB_ERROR OperatableControlLogic::checkInhibit()
{
    return (m_inhibited)? GDB_ERROR_INHIBIT : GDB_ERROR_NONE;
}


GDB_ERROR OperatableControlLogic::checkAll(const lu_bool_t local)
{
    if (m_inhibited)
    {
        return GDB_ERROR_INHIBIT;
    }
    else
    {
        return checkOLR(local);
    }
}


GDB_ERROR OperatableControlLogic::checkInputValue(const lu_uint16_t OLR_INPUT,
                                                  const lu_uint16_t INHIBIT_INPUT,
                                                  const lu_uint16_t inputPoint,
                                                  PointData *data
                                                  )
{
    if(data == NULL)
    {
        return GDB_ERROR_PARAM;
    }
    PointIdStr point;
    if(inputPoint == OLR_INPUT)
    {
        OLR_STATE olr;
        GDB_ERROR ret;
        ret = database.getLocalRemote(olr);
        *data = (DigitalPointValue)olr;
        return ret;
    }
    if(inputPoint == INHIBIT_INPUT)
    {
        return database.getValue(m_inhibitPoint.pointID, data);
    }
    return GDB_ERROR_NOPOINT;
}


void OperatableControlLogic::confPosition(const SWITCH_OPERATION operation,
                                            DBINARY_STATUS& startPos,
                                            DBINARY_STATUS& endPos
                                            )
{
    if(operation == SWITCH_OPERATION_OPEN)
    {
        startPos = DBINARY_ON ;
        endPos   = DBINARY_OFF;
    }
    else
    {
        startPos = DBINARY_OFF;
        endPos   = DBINARY_ON ;
    }
}


void OperatableControlLogic::refuseOperationMsg(const GDB_ERROR errorCode)
{
    log.warn("%s - cannot operate: %s.", this->toString().c_str(), GDB_ERROR_ToSTRING(errorCode));
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
