/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_3A4A88C8_EF89_4770_836C_2901448E4FFC__INCLUDED_)
#define EA_3A4A88C8_EF89_4770_836C_2901448E4FFC__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "IControlLogicFactory.h"
#include "IStatusManager.h"
#include "IIOModuleManager.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Dummy control logic block factory
 * 
 * Used for testing. It creates control logic blocks with hard-coded configuration
 */
class DummyControlLogicFactory : public IControlLogicFactory
{

public:
    DummyControlLogicFactory(IStatusManager &statMan, IIOModuleManager &MManager);
    virtual ~DummyControlLogicFactory() {};

    /**
     * \brief Get a new control logic block
     *
     * \param mutex Global mutex
     * \param database local database
     * \param group the group ID of the control logic to create
     *
     * \return Pointer to the control logic. NULL in case of error
     */
    virtual ControlLogic* newCLogic(Mutex* mutex, GeminiDatabase* database, lu_uint16_t group);
    /**
     * \brief Get the number of control logic block that the factory can create
     *
     * \return Control logic block number
     */
    virtual lu_uint16_t getCLogicNumber();

private:
    IStatusManager &statusManager;
    IIOModuleManager &MManager;
};
#endif // !defined(EA_3A4A88C8_EF89_4770_836C_2901448E4FFC__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
