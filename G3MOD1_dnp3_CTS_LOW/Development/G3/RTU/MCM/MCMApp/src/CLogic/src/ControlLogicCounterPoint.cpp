/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ControlLogicCounterPoint.cpp 14 Jul 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/CLogic/src/ControlLogicCounterPoint.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Control Logic custom Counter Point interface.
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 14 Jul 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14 Jul 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "ControlLogicCounterPoint.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
ControlLogicCounterPoint::ControlLogicCounterPoint(Mutex* gMutex,
                                                   GeminiDatabase& g3database,
                                                   CounterPoint::Config& conf) :
                                                       CounterPoint(gMutex,
                                                                   g3database,
                                                                   conf)
{
    m_config.clearOffline = LU_FALSE; //Control Logic Counter Points have no clear-when-lost
}


ControlLogicCounterPoint::~ControlLogicCounterPoint()
{}


GDB_ERROR ControlLogicCounterPoint::init()
{
    if(initialised)
        return GDB_ERROR_INITIALIZED;

    return CounterPoint::init();   //Call the parent init
}


GDB_ERROR ControlLogicCounterPoint::setValue(const lu_uint32_t value,
                                             const TimeManager::TimeStr& newTimeStamp,
                                             const lu_bool_t forced)
{
    return setValue(value, newTimeStamp, incomingFlags, forced);
}


GDB_ERROR ControlLogicCounterPoint::setValue(const lu_uint32_t value,
                                             const TimeManager::TimeStr& newTimeStamp,
                                             const PointFlags& flags,
                                             const lu_bool_t forced)
{
    GDB_ERROR ret;
    PointDataCounter32 pointData = m_data;
    lu_uint32_t dataChange = DATACHANGE_VALUE;

    if(forced == LU_TRUE)
    {
        dataChange = dataChange | DATACHANGE_FORCE;
    }
    if(flags != incomingFlags)
    {
        incomingFlags = flags;
        dataChange = dataChange | DATACHANGE_FLAGS;
    }
    if(dataChange == DATACHANGE_NONE)
    {
        return GDB_ERROR_NONE;  //Nothing to do
    }
    pointData.setFlags(flags);
    pointData.setInitialFlag(false); //Not the initial value anymore
    pointData.setTime(newTimeStamp);
    pointData.setEdge(EDGE_TYPE_NONE);  //No edge: set absolute value
    pointData = value;
    ret = scheduleUpdate(*this, pointData, static_cast<DATACHANGE>(dataChange));
    if(ret != GDB_ERROR_NONE)
    {
        log.error("%s: CL Counter Point (%s) - error: %s",
                    __AT__, pointID.toString().c_str(), GDB_ERROR_ToSTRING(ret)
                  );
    }
    return ret;
}


GDB_ERROR ControlLogicCounterPoint::setQuality(const lu_bool_t newStatus,
                                               const TimeManager::TimeStr& newTimeStamp)
{
    incomingFlags.setQuality((newStatus == LU_TRUE)? POINT_QUALITY_ON_LINE : POINT_QUALITY_OFF_LINE);
    return CounterPoint::setQuality(m_data, newStatus, newTimeStamp);
}


GDB_ERROR ControlLogicCounterPoint::setFlags(const PointFlags& newFlags, const TimeManager::TimeStr& timestamp)
{
    incomingFlags = newFlags;
    return AbstractPoint::setFlags(m_data, newFlags, timestamp);
}


void ControlLogicCounterPoint::freeze(TimeManager::TimeStr& timestamp)
{
    /* Schedule an update to command the point to freeze the value: only timestamp is used */
    GDB_ERROR ret;
    PointDataCounter32 pointData;
    pointData.setTime(timestamp);
    ret = scheduleUpdate(*this, pointData, DATACHANGE_FREEZE);
    if(ret != GDB_ERROR_NONE)
    {
        log.error("%s: Point (%s) - error: %s",
                    __AT__, pointID.toString().c_str(), GDB_ERROR_ToSTRING(ret)
                  );
    }
}


void ControlLogicCounterPoint::update(PointData* newData, DATACHANGE dataChange)
{
    bool changed = false;       //A change in the point data
    PointDataCounter32 eventData;  //data copy used to update observers
    TimeManager::TimeStr timestamp;
    newData->getTime(timestamp);

    /* Lock global mutex */
    {
        LockingMutex lMutex(*mutex);
        /* Check flags change */
        if( (dataChange & DATACHANGE_FORCE) || (newData->compareFlags(m_lastFlags) == LU_FALSE) )
        {
            /* Input flags changed */
            m_data.setTime(timestamp);
            m_data.setFlags(newData->getFlags());
            changed = true;
        }
        m_lastFlags = newData->getFlags();    //store latests flags
        /* Counter data change: get directly the reported value and event it */
        if( (dataChange & DATACHANGE_FORCE) || (dataChange & DATACHANGE_VALUE) )
        {
            /* Value change */
            lu_uint64_t value;
            m_data.setTime(timestamp);
            //Direct set of the value
            value = (lu_uint32_t)*((PointDataCounter32*)newData);
            m_data.setEdge(EDGE_TYPE_POSITIVE);   //Counters are always increasing
            if( !(dataChange & DATACHANGE_FORCE) )
            {
                value = calcRollover(value);    //Apply rollover only when not forced
            }
            else
            {
                m_data = (lu_uint32_t)value;
            }
            /* Re-adjust flag when forced and changed by calcRollover() */
            if( (dataChange & DATACHANGE_FORCE) && (newData->getOverflowFlag() == true) )
            {
                m_data.setOverflowFlag(true);
            }
        }
        if(changed)
        {
            /* Update point */
            eventData = m_data;   //value to be used for update
        }
    }//End of Mutex section

    if(changed)
    {
        /* Update all observers */
        updateObservers(&eventData);
        if(persistent)
        {
            /* Store persistent value */
            storePersistent(lucyMem::MemType_COUNTER, eventData);
        }

    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
