    /*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: PLTULogic.h 10 Jun 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/CLogic/include/PLTULogic.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       PLTU: Parasitic Load Trip Unit
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 10 Jun 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   10 Jun 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef PLTULOGIC_H__INCLUDED
#define PLTULOGIC_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IChannel.h"
#include "ControlLogic.h"
#include "ControlLogicDigitalPoint.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief PLTU: Parasitic Load Trip Unit Control Logic.
 *
 * The Parasitic Load Trip Unit logic is used to trip isolated high voltage
 * supplies where the low-voltage is operated fully interconnected. That means
 * that in the event of a high voltage phase-to-earth or two-phase-to-earth
 * fault, the PLTU will detect the loss of some of the phases and will trip the
 * Ring Main Unit (RMU) circuit breaker (CB).
 * This Control Logic monitors line voltages and status from 2 switches, and
 * operates (trip/reset) an associated switch (CB) depending on the overall
 * status of all these components.
 *
 * This Control Logic is driven by events in the inputs, and not from an
 * "operate" command.
 */
class PLTULogic: public ControlLogic
{
public:
    struct PLTUOperationParameters
    {
    public:
        lu_uint32_t voltageDelay;   //Voltage measurement mismatch delay, in seconds
    };

    /**
     * \brief configuration block for PLTU Control Logic
     */
    struct Config : public ControlLogicConf
    {
    public:
        /* Amounts of different types of Control Logic's Pseudo Points */
        static const lu_uint16_t BINARYPOINT_NUM = PLTU_POINT_TYPE_BINARY_COUNT;
        static const lu_uint32_t ALLPOINT_NUM = PLTU_POINT_EnumSize;
    public:
        PLTUOperationParameters operationParameters;
        PointIdStr dInput[PLTU_INPUT_LAST]; //Digital Input point list
        ControlLogicDigitalPoint::Config bPoint[BINARYPOINT_NUM];    //CL digital Points
        lu_uint16_t switchCBID;              //Associated module's CB switch CLogic ID
    };

public:
    /**
     * \brief Custom constructor
     *
     * \param mutex Reference to the G3DB common mutex
     * \param database Reference to the Gemini database
     * \param config reference to the Control Logic's configuration block
     */
    PLTULogic(Mutex* mutex, GeminiDatabase& database, PLTULogic::Config& config);
    virtual ~PLTULogic();

    /* == Inherited from ControlLogic == */
    virtual GDB_ERROR init();
    virtual GDB_ERROR getInputValue(lu_uint16_t inputPoint, PointData *data);
    virtual GDB_ERROR getOutputValue(PointData* data);

protected:
    /**
     * \brief Operation Command timeout timer
     *
     * This class waits a timer to finish to then send a trip/reset command to the CB.
     */
    class PLTUTimeoutTrip : public TimedJob
    {

    public:
        /**
         * \brief Custom constructor
         *
         * \param cLogic Reference to the control logic to use
         * \param timeout Wait time value in seconds
         */
        PLTUTimeoutTrip(PLTULogic &cLogic, lu_uint32_t timeout):
                                                               TimedJob((timeout*1000), 0),
                                                               cLogic(cLogic)
        {};

        virtual ~PLTUTimeoutTrip() {};

    protected:
        /* ==Inherited from TimedJob== */
        void job(TimeManager::TimeStr *timePtr)
        {
            cLogic.tripCBTimeout(*timePtr);
        }

    private:
        PLTULogic &cLogic;
    };

protected:
    /* == Inherited from ControlLogic == */
    virtual void newData(PointIdStr pointID, PointData& pointData);

    /**
     * \brief Executes a trip on the Circuit Breaker
     *
     * This method is intended to be called when the time out expires so the
     * Circuit breaker is activated.
     *
     * \param timestamp Time stamp of the action (current time)
     */
    void tripCBTimeout(TimeManager::TimeStr& timestamp);

private:
    /**
     * \brief Check common preliminary conditions that can inhibit the operation
     *
     * \param timestamp Time stamp to use (current time)
     *
     * \return Error code. GDB_ERROR_NONE when all preliminary checks passed.
     */
    GDB_ERROR preliminaryChecks();

    /**
     * \brief Check common preliminary conditions that can inhibit the operation
     *
     * \return Error code. GDB_ERROR_NONE when switch statuses does not mismatch (no inhibit).
     */
    GDB_ERROR checkSwitchesMismatch();

    /**
     * \brief Check DI points observer for inhibition
     *
     * If a DI point observer is present, checks its status to see if
     * it should trigger an inhibition. If it is not configured, does not
     * triggers any inhibition.
     *
     * \param digitalInput Digital input index of the input to be checked
     *
     * \return Inhibition of the point. Gives LU_TRUE if the point is not online.
     */
    inline lu_bool_t checkDIObserver(PLTU_INPUT digitalInput);

    /**
     * \brief Check DI points observer for inhibition
     *
     * If a DI point observer is present, checks its value and status to see if
     * it should trigger an inhibition. If it is not configured, does not
     * triggers any inhibition.
     *
     * \param digitalInput Digital input index of the input to be checked
     * \param checkValue Value to check against.
     *
     * \return Inhibition of the point. Gives LU_TRUE if the point matches the
     * given value or is not online.
     */
    template <typename ValueType>
    inline lu_bool_t checkDIObserver(PLTU_INPUT digitalInput, ValueType checkValue);

private:
    enum PLTU_STAGE
    {
        PLTU_STAGE_OFF_LINE         = 0,    //Ready for operation
        PLTU_STAGE_WAIT_TRIP,               //Waiting for CB trip
        PLTU_STAGE_LAST
    };

private:
    PLTULogic::Config operationConfig;  //Configuration of the PLTU
    PLTU_STAGE stage;                   //Operation stage
    PLTUTimeoutTrip* tripCB;            //Timer job for trip/reset the CB

    DIObserver* dObservers[PLTU_INPUT_LAST]; //Observers over the CL inputs
};


#endif /* PLTULOGIC_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
