/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:AnalogBitmaskLogic.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief Logic to generate an analogue value from up to 16 different
 *              single binary VPs.
 *       
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   28 Sep 2016     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef ANALOGBITMASKLOGIC_H_
#define ANALOGBITMASKLOGIC_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "ControlLogic.h"
#include "ControlLogicAnaloguePoint.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Converts several Single Binary values and composes an analogue value
 *
 * This takes several (up to 16) Single Binary values and composes an analogue
 * value directly from these bits.
 * Please note that it does not compose an IEEE 754 floating point analogue from
 * these bits; it really composes the value as an int and exports it (not cast)
 * as an analogue value:
 *      bitfield --(compose)--> 16-bit int value --(assign)--> analogue value
 */
class AnalogBitmaskLogic: public ControlLogic
{
public:
    /**
     * \brief configuration block
     */
    struct Config: public ControlLogicConf
    {
    public:
        static const lu_uint16_t ALLINPOINT_NUM = 16; //Max number of inputs
        /* Amounts of different types of Control Logic's Pseudo Points */
        static const lu_uint16_t AIPOINT_NUM = BTA_POINT_TYPE_ANALOGUE_COUNT;
        static const lu_uint16_t ALLPOINT_NUM = AIPOINT_NUM;
    public:
        lu_uint32_t debounce_ms;    //Time to settle results, in ms
        lu_uint32_t numDigInputs;   //Expected amount of digital inputs (optional)
        //Note that ALL input points should be added with addPoint()
        ControlLogicAnaloguePoint::Config aPoint; //CLogic analogue result point
    };
public:
    AnalogBitmaskLogic(Mutex* mutex, GeminiDatabase& database, AnalogBitmaskLogic::Config& config);
    virtual ~AnalogBitmaskLogic();

    /* == Inherited from ControlLogic == */
    virtual GDB_ERROR init();
    virtual GDB_ERROR getInputValue(lu_uint16_t inputPoint, PointData *data);

    /**
     * \brief Add a new Single Binary point to the engine
     *
     * \param point Point to add -- if invalid, a value 0 [online] is assumed
     *
     * \return Error code
     */
    GDB_ERROR addPoint(PointIdStr point);

protected:
    /* == Inherited from ControlLogic == */
    virtual void newData(PointIdStr pointID, PointData& pointData);

protected:
    /**
     * \brief Job for publishing point data after debounce.
     */
    class PublishJob: public TimedJob
    {
    public:
        /**
         * \brief Custom constructor
         *
         * \param publishID ID for publishing. Should match the latest value
         * \param CLogic Reference to the Control Logic object.
         * \param delay_ms Delay (debounce) time, expressed in milliseconds
         */
        PublishJob(const lu_uint32_t publishID,
                    AnalogBitmaskLogic& CLogic,
                    const lu_uint32_t delay_ms) :
                                                            TimedJob(delay_ms, 0),
                                                            m_pubID(publishID),
                                                            m_cLogic(CLogic)
        {};
        virtual ~PublishJob() {};
    protected:
        /* == Inherited from TimedJob == */
        virtual void job(TimeManager::TimeStr* timerPtr)
        {
            m_cLogic.publish(m_pubID, *timerPtr);
        }
    private:
        lu_uint32_t m_pubID;  //ID for publishing
        AnalogBitmaskLogic& m_cLogic;
    };

private:
    /**
     * \brief Publish current Max, Min, and Average values.
     */
    void publish(const lu_uint32_t publishID, TimeManager::TimeStr& currentTime);

private:
    /* \brief Structure containing pre-processed value & flags before publishing */
    struct PreOutput
    {
    public:
        lu_uint16_t value;
        PointFlags flags;
    public:
        PreOutput(): value(0)
        {};
    };

private:
    bool configured;
    lu_uint32_t m_debounce_ms;    //Time to settle results, in ms

    /* Temporary value publishing */
    PreOutput m_preOutput;  //Output value & flags before debouncing
    PublishJob* m_publishJob;
    lu_uint32_t m_publishID;    //Latest ID for publishing

    /* List of Observers over the Virtual Point Inputs */
    MasterMutex m_inListMutex;  //Mutex to protect the point observers List
    std::vector<DIObserver*> m_dInput;    //Input point observers List
};


#endif /* ANALOGBITMASKLOGIC_H_ */

/*
 *********************** End of file ******************************************
 */
