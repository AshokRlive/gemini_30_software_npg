/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: MaxMinAverageLogic.h 28 Jun 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/CLogic/include/MaxMinAverageLogic.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Maximum, minimum, and average values calculation Control Logic.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 28 Jun 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   28 Jun 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MAXMINAVERAGELOGIC_H__INCLUDED
#define MAXMINAVERAGELOGIC_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "ControlLogic.h"
#include "ControlLogicAnaloguePoint.h"
#include "ControlLogicCounterPoint.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Max/Min/Average Control Logic
 *
 * This Control Logic calculates the maximum, minimum, and average values of all
 * samples of a given input in the specified amount of time. After that time,
 * calculations are updated in the output points.
 * To comply with UKPN specification, the output is also offered converted from
 * analogue to a counter.
 *
 * This Control Logic is driven by events in the inputs, and not from an
 * "operate" command.
 */
class MaxMinAverageLogic: public ControlLogic, private TimeObserver
{
public:
    struct OperationParams
    {
    public:
        enum INTERVAL
        {
            INTERVAL_OCLOCK,    //Time specification is seconds from o'clock
            INTERVAL_PERIOD     //Time specification is a period, every n seconds
        };
    public:
        lu_uint32_t startDelay_s;   //Delay time (in secs) before starting sampling data (avoids invalid 1st calculation)
        INTERVAL intervalType;      //Type of time to count
        lu_uint32_t periodTime_s;   //Measurement period time, in seconds

        bool supportCounter;        //Indicates we are using counter pseudo outputs
        lu_float32_t scale;         //Conversion scaling factor for counter pseudo outputs

        /* TODO: pueyos_a - add MMAVG sampling rate? */
        lu_uint32_t samplingRate_ms;//Sampling rate, in ms
    public:
        OperationParams() : startDelay_s(0),
                                     intervalType(INTERVAL_PERIOD),
                                     periodTime_s(30 * 60),
                                     supportCounter(false),
                                     scale(1.0),
                                     samplingRate_ms(1000)
        {};
    };

    /**
     * \brief configuration block for Max/Min/Average Control Logic
     */
    struct Config : public ControlLogicConf
    {
    public:
        /* Amounts of different types of Control Logic's Pseudo Points */
        static const lu_uint16_t ANALOGUEPOINT_NUM = MMAVG_POINT_TYPE_ANALOGUE_COUNT;
        static const lu_uint16_t COUNTERPOINT_NUM =  MMAVG_POINT_TYPE_COUNTER_COUNT;
    public:
        lu_uint32_t allPointNum;
        OperationParams operationParams;
        PointIdStr aInput[MMAVG_INPUT_LAST]; //Analogue Input point list
        ControlLogicAnaloguePoint::Config aPoint[ANALOGUEPOINT_NUM]; //CLogic analogue Points
        ControlLogicCounterPoint::Config cPoint[COUNTERPOINT_NUM];   //CLogic counter Points
    };

public:
    /**
     * \brief Custom constructor
     *
     * \param mutex Reference to the G3DB common mutex
     * \param database Reference to the Gemini database
     * \param config reference to the Control Logic's configuration block
     */
    MaxMinAverageLogic(Mutex* mutex, GeminiDatabase& database, MaxMinAverageLogic::Config& config);
    virtual ~MaxMinAverageLogic();

    /* == Inherited from ControlLogic == */
    virtual GDB_ERROR init();
    virtual GDB_ERROR getInputValue(lu_uint16_t inputPoint, PointData *data);

protected:
    /**
     * \brief Max/Min/Average Job for publishing point data at a configured interval.
     */
    class MMAvgPublishJob : public TimedJob
    {
    public:
        /**
         * \brief Custom constructor
         *
         * \param CLogic Reference to the Control Logic object.
         * \param timerInterval_s Period time, expressed in seconds
         */
        MMAvgPublishJob(MaxMinAverageLogic& CLogic, lu_uint32_t timerInterval_s) :
                            TimedJob(timerInterval_s * 1000, timerInterval_s * 1000),
                            cLogic(CLogic)
        {};
        virtual ~MMAvgPublishJob() {};

    protected:
        /* == Inherited from TimedJob == */
        virtual void job(TimeManager::TimeStr* timerPtr)
        {
            cLogic.publish(*timerPtr);
        }

    private:
        MaxMinAverageLogic& cLogic;
    };

    /**
     * \brief Max/Min/Average job for generating tick event.
     */
    class MMAvgTickJob : public TimedJob
    {
    public:
        /**
         * \brief Custom constructor
         *
         * \param CLogic Reference to the Control Logic object.
         * \param timerInterval_ms Sampling rate, expressed in milliseconds
         */
        MMAvgTickJob(MaxMinAverageLogic& CLogic, lu_uint32_t timerInterval_ms) :
                            TimedJob(timerInterval_ms, timerInterval_ms),
                            cLogic(CLogic)
        {};
        virtual ~MMAvgTickJob() {};

    protected:
        /* == Inherited from TimedJob == */
        virtual void job(TimeManager::TimeStr* timerPtr)
        {
            cLogic.tickEvent(*timerPtr);
        }

    private:
        MaxMinAverageLogic& cLogic;
    };


    /**
     * \brief Max/Min/Average one-shot job for starting sampling after a delay.
     */
    class StartDelayJob : public TimedJob
    {
    public:
        /**
         * \brief Custom constructor
         *
         * \param CLogic Reference to the Control Logic object.
         * \param timerInterval_s Period time, expressed in seconds
         */
        StartDelayJob(MaxMinAverageLogic& CLogic, lu_uint32_t timerInterval_s) :
                            TimedJob(timerInterval_s * 1000, 0),
                            cLogic(CLogic)
        {};
        virtual ~StartDelayJob() {};

    protected:
        /* == Inherited from TimedJob == */
        virtual void job(TimeManager::TimeStr* timerPtr)
        {
            LU_UNUSED(timerPtr);
            cLogic.startSampling();
        }

    private:
        MaxMinAverageLogic& cLogic;
    };


protected:
    /* == Inherited from ControlLogic == */
    virtual void newData(PointIdStr pointID, PointData& pointData);

    /* == Inherited from TimeObserver == */
    virtual void updateTime(TimeManager::TimeStr& currentTime);

private:
    /**
     * \brief Structure containing an analogue value converted to counter
     */
    struct CountValue
    {
    public:
        lu_uint32_t value;  //Represented value
        lu_bool_t overflow; //Indication that value doesn't fit in the representation
    public:
        CountValue() : value(0), overflow(false)
        {};
    };

private:
    /**
     * \brief Convert an analogue value to a 16-bit unsigned int
     *
     * \warning Note that no overflow check is done!
     *
     * \param value Analogue value to convert
     *
     * \return Result of the conversion, including a flag for rollover (overflow)
     */
    MaxMinAverageLogic::CountValue floatToUInt(const lu_float32_t value);

    /**
     * Simulates input changes for testing purpose only.
     */
    void simulate();

    /**
     * \brief Timed job event report
     */
    void tickEvent(TimeManager::TimeStr& currentTime);

    /**
     * \brief Timed job event report
     */
    void startSampling();

    /**
     * \brief Publish current Max, Min, and Average values.
     */
    void publish(TimeManager::TimeStr& currentTime);

//private:
//    /**
//     * \brief Structure containing an analogue value converted to counter
//     */
//    struct CountValue
//    {
//    public:
//        lu_uint32_t value;  //Represented value
//        bool overflow;      //Value doesn't fit inside the represented value
//    private:
//        CountValue() : overflow(false)
//        {};
//    };

private:
    OperationParams operationConfig;
    AIObserver* aObservers[MMAVG_INPUT_LAST];   //Analogue Input point observer
    MasterMutex pMutex;    //Controls access to calculation values
    lu_float32_t rawValue;
    bool         rawValueValid;
    bool         resultValid;   //States if the calculation will be valid
    lu_float32_t maxValue;      //Value calculation
    lu_float32_t minValue;      //Value calculation
    lu_float32_t avgValue;      //Value calculation
    lu_uint32_t numSamples;     //Amount of samples acquired  //TODO to be removed.
    TimedJob* publishJob;
    TimedJob* tickJob;
    TimedJob* startJob;
    PointFlags analogueFlags;   //Current analogue pseudo points flags
    PointFlags counterFlags;    //Current counter pseudo points flags
};


#endif /* MAXMINAVERAGELOGIC_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
