/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "SwitchAuthority.h"
#include "LockingMutex.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
MasterMutex SwitchAuthority::accessLock(LU_TRUE); //Priority inheritance enabled just in case
SwitchAuthority::State SwitchAuthority::authority[SwitchAuthority::SWTYPE_LAST];   //init by ctor

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
lu_bool_t SwitchAuthority::acquire(const SwitchAuthority::SWTYPE type,
                                   const lu_uint16_t switchID
                                  )
{
    MasterLockingMutex lmutex(accessLock);
    switch(type)
    {
        case  SwitchAuthority::SWTYPE_UPPER_AUTHORITY:
        {
            //Upper authority can get (when free) or reserve (get when free, or lock until freed)
            bool freed = isFree();
            if( freed ||                                 //SwitchAuthority::ACQ_FUNC_GET
                (switchID == SwitchAuthority::ACQ_FUNC_RESERVE) //Reserve authority
                )
            {
                authority[type][switchID] = SwitchAuthority::STATE_ACQUIRED;
                return (freed)? LU_TRUE : LU_FALSE; //true when acquired, false when reserved
            }
        }
        break;
        case SwitchAuthority::SWTYPE_SWITCH:
        {
            if(isGranted())
            {
                /* Not reserved neither by upper authority nor other switch */
                State::iterator thisSwitch = authority[type].find(switchID);
                if( thisSwitch == authority[type].end() )
                {
                    //Not found: Assign the authority to the caller
                    authority[type][switchID] = SwitchAuthority::STATE_ACQUIRED;
                    return LU_TRUE;
                }
            }
        }
        break;
        case SwitchAuthority::SWTYPE_CIRCUITBREAKER_TRIP:
        {
            if(authority[SwitchAuthority::SWTYPE_UPPER_AUTHORITY].empty())
            {
                /* Not reserved by upper authority */
                State::iterator thisSwitch = authority[type].find(switchID);
                if( thisSwitch == authority[type].end() )
                {
                    //Not found: Assign one authority to the caller
                    authority[type][switchID] = SwitchAuthority::STATE_ACQUIRED;
                    return LU_TRUE;
                }
            }
        }
        break;
        default:
        break;
    }
    return LU_FALSE;
}


lu_bool_t SwitchAuthority::grant(const SwitchAuthority::SWTYPE type,
                                 const lu_uint16_t switchID
                                )
{
    MasterLockingMutex lmutex(accessLock, LU_TRUE);
    State::iterator thisSwitch = authority[type].find(switchID);
    if( thisSwitch != authority[type].end() )
    {
        /* The owner can grant another owner's operation (another owner type),
         * but keeps the authority.
         */
         thisSwitch->second = SwitchAuthority::STATE_GRANTED;
         return LU_TRUE;
    }
    return LU_FALSE;
}


lu_bool_t SwitchAuthority::relinquish(const SwitchAuthority::SWTYPE type,
                                      const lu_uint16_t switchID
                                      )
{
    MasterLockingMutex lmutex(accessLock);
    State::iterator thisSwitch = authority[type].find(switchID);
    if( thisSwitch != authority[type].end() )
    {
        /* The owner releases the authority */
        authority[type].erase(switchID);

        if(isLockedFree())
        {
            updateObservers();
        }
        return LU_TRUE;
    }
    return LU_FALSE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
bool SwitchAuthority::isGranted()
{
    for (lu_uint32_t i = 0; i < SwitchAuthority::SWTYPE_LAST; ++i)
    {
        for(State::iterator itMap = authority[i].begin(); itMap != authority[i].end(); ++itMap)
        {
            if(itMap->second == SwitchAuthority::STATE_ACQUIRED)
            {
                //At least one does not grant!
                return false;
                break;
            }
        }
    }
    return true;
}


bool SwitchAuthority::isFree()
{
    for (lu_uint32_t i = 0; i < SwitchAuthority::SWTYPE_LAST; ++i)
    {
        if( !(authority[i].empty()) )
        {
            //Not empty means there is an ID that acquired or is granting authority
            return false;
        }
    }
    return true;
}


bool SwitchAuthority::isLockedFree()
{
    for (lu_uint32_t i = 0; i < SwitchAuthority::SWTYPE_LAST; ++i)
    {
        if(i == SwitchAuthority::SWTYPE_UPPER_AUTHORITY)
        {
            if( (authority[i].empty()) )
            {
                return false;   //Not locked
            }
            if( !(authority[i].empty()) )
            {
                return false;   //no-authority still has acquired authority
            }
        }
    }
    return true;
}


/*
 *********************** End of file ******************************************
 */
