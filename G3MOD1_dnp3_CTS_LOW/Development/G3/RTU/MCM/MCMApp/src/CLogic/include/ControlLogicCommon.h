/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ControlLogicCommon.h 5 Nov 2013 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/CLogic/include/ControlLogicCommon.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 5 Nov 2013 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   5 Nov 2013    pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef CONTROLLOGICCOMMON_H__INCLUDED
#define CONTROLLOGICCOMMON_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "ControlLogicDef.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/**
 * \brief Control Logic's basic Configuration
 */
struct ControlLogicConf
{
public:
    lu_uint16_t groupID;
    bool enabled;           //Control Logic is enabled
public:
    ControlLogicConf() : groupID(0),
                         enabled(true)
    {};
};


/**
 * \brief Control Logic Switch-related operation
 *
 * This structure is used to communicate CL actions.
 */
struct SwitchLogicOperation
{
public:
    SWITCH_OPERATION operation; //Operation to perform
    lu_bool_t local;            //true if the command is local
    lu_uint32_t duration;       //duration of the operation, if needed
public:
    SwitchLogicOperation() : operation(SWITCH_OPERATION_LAST),
                             local(LU_TRUE),
                             duration(0)
    {};
};

struct AnalogueOutputOperation
{
    lu_float32_t value;
    lu_bool_t    local;            //true if the command is local
};


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */


#endif /* CONTROLLOGICCOMMON_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
