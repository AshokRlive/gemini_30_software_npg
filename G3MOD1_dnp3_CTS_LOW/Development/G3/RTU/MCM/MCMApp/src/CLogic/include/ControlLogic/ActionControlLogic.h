/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ActionControlLogic.h 1 Jul 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/CLogic/include/ActionControlLogic.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Control Logic specialised in operate other Control Logics.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 1 Jul 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   1 Jul 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef ACTIONCONTROLLOGIC_H__INCLUDED
#define ACTIONCONTROLLOGIC_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "ControlLogic.h"
#include "DigitalChangeEvent.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#ifndef ACTCL_BPOINT_LAST
#define ACTCL_BPOINT_LAST 0
#endif

#define ACTCL_MAX_VPOINTS ACTCL_BPOINT_LAST  //max number of CL pseudo virtual points of any type

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Converts a digital input to a Control Logic operation command.
 *
 * This class observes single binary digital virtual point inputs and operates
 * an associated Control Logic when they change. Each input must have an associated
 * action to perform.
 * The number of inputs is variable and are added dynamically.
 *
 * This Control Logic is driven by events in the inputs, and not from an
 * "operate" command.
 */
class ActionControlLogic: public ControlLogic
{
public:
    /**
     * \brief configuration block for Action Control Logic
     */
    struct Config : public ControlLogicConf
    {
    public:
        /* Amounts of different types of Control Logic's Pseudo Points */
        static const lu_uint32_t ALLPOINT_NUM = 0;
        static const lu_uint32_t MAXINPUTS = 2;
    public:
        lu_uint32_t delay;          //Operation delay, in seconds
        lu_uint16_t controlLogicID; //Associated Control Logic to operate
    };

public:
    /**
     * \brief Custom constructor
     *
     * \param mutex Reference to the G3DB common mutex
     * \param database Reference to the Gemini database
     * \param config reference to the Control Logic's configuration block
     */
    ActionControlLogic(Mutex* mutex,
                       GeminiDatabase& database,
                       ActionControlLogic::Config& config
                      );
    virtual ~ActionControlLogic();

    /* == Inherited from ControlLogic == */
    virtual GDB_ERROR init();
    virtual GDB_ERROR getInputValue(lu_uint16_t inputPoint, PointData *data);
    virtual GDB_ERROR getOutputValue(PointData* data);

    /**
     * \brief Add a new point to the monitor engine
     *
     * \param point Point to add
     *
     * \return Error code
     */
    GDB_ERROR addPoint( PointIdStr point,
                        DigitalChangeEvent::Config eventCfg,
                        const ACTION_CLOGIC_CMD action,
                        const ACTION_CLOGIC_OLR olr = ACTION_CLOGIC_OLR_UNUSED
                      );

protected:
    /**
     * \brief Object to keep actions and its related parameters together
     */
    class ActionObserver : private IPointObserver
    {
    public:
        ActionObserver( ActionControlLogic& actionCLogic,
                        GeminiDatabase& database,
                        PointIdStr point,
                        DigitalChangeEvent::Config eventCfg,
                        const ACTION_CLOGIC_CMD actionLogic,
                        const ACTION_CLOGIC_OLR localRemote = ACTION_CLOGIC_OLR_UNUSED
                        );
        virtual ~ActionObserver();

        virtual GDB_ERROR init() { return m_observer->init(); };

        /* == Inherited from IPointObserver == */
        virtual PointIdStr getPointID() { return m_pointID; };

    protected:
        /* == Inherited from IPointObserver == */
        virtual void update(PointIdStr pointID, PointData* pointDataPtr);

    private:
        ActionControlLogic& m_actionLogic;
        PointIdStr m_pointID;
        DigitalChangeEvent* m_observer;
        const ACTION_CLOGIC_CMD m_action;
        const ACTION_CLOGIC_OLR m_OLR;
    };

    /**
     * \brief One-shot job for action execution delay
     */
    class TimedJobAction : public TimedJob
    {
    public:
        TimedJobAction( ActionControlLogic& cLogic,
                        const ACTION_CLOGIC_CMD action,
                        const ACTION_CLOGIC_OLR olr,
                        const lu_uint32_t timeout):
                                                    TimedJob(timeout, 0),
                                                    m_cLogic(cLogic),
                                                    m_action(action),
                                                    m_olr(olr)
        {};
        virtual ~TimedJobAction() {};

    protected:
        /* == Inherited from TimedJob == */
        virtual void job(TimeManager::TimeStr *timePtr);

    private:
        ActionControlLogic& m_cLogic;   //Reference to the Control Logic
        const ACTION_CLOGIC_CMD m_action;
        const ACTION_CLOGIC_OLR m_olr;
    };

protected:
    /**
     * \brief Schedule a Control Logic operation (action)
     *
     * action Action to perform
     * olr Dependency on the Off/Local/Remote status
     */
    virtual void startAction(const ACTION_CLOGIC_CMD action, const ACTION_CLOGIC_OLR olr);

    /**
     * \brief Operates the associated Control Logic
     *
     * action Action to perform
     * olr Dependency on the Off/Local/Remote status
     */
    virtual void action(const ACTION_CLOGIC_CMD action, const ACTION_CLOGIC_OLR olr);

private:
    ActionControlLogic::Config config;  //Configuration of the Control Logic
    TimedJobAction* actionJob;          //Job for delayed operation

    /* List of Observers over the Virtual Point Inputs */
    MasterMutex listMutex;   // Mutex to protect the point observers List
    std::vector<ActionObserver*> dObservers;    //Input point observers List
};


#endif /* ACTIONCONTROLLOGIC_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
