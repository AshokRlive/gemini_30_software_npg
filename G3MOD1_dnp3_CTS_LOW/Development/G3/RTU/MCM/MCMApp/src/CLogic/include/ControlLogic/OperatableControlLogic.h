/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: OperatableControlLogic.h 4 Mar 2015 pueyos_a $
 *               $HeadURL: http://10.11.0.6:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/CLogic/include/ControlLogic/OperatableControlLogic.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 4 Mar 2015 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   4 Mar 2015   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef OPERATABLECONTROLLOGIC_H__INCLUDED
#define OPERATABLECONTROLLOGIC_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "ControlLogic.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Operatable Control Logic virtual class.
 *
 * This is a class that provides specific functionality for operatable control
 * logics. "Operatable" means that this Control Logic can be operated from SCADA,
 * so in fact are the ones that can be assigned as Protocol Point Outputs.
 */
class OperatableControlLogic: public ControlLogic
{
public:
    struct Config : public ControlLogicConf
    {
    public:
        bool useLocalRemote;
        ControlLogic::PointRef inhibitPoint;
    public:
        Config() : ControlLogicConf(),
                   useLocalRemote(false)
        {};
    };

public:
    OperatableControlLogic(Mutex *mutex,
                           GeminiDatabase& database,
                           const OperatableControlLogic::Config config,
                           const CONTROL_LOGIC_TYPE CLtype
                           );
    virtual ~OperatableControlLogic();

    /* == Inherited from ControlLogic == */
    virtual bool isOperatable() { return true; };
    GDB_ERROR startOperation(SwitchLogicOperation &operation);
    virtual GDB_ERROR getOutputValue(PointData* data);  //WARNING: always online: override when needed

    virtual GDB_ERROR writeOutputValue(AnalogueOutputOperation &val)
    {
        LU_UNUSED(val);
        log.warn("Unsupported operation: writeOutputValue");
        return GDB_ERROR_NOT_SUPPORTED;
    }

protected:
    /**
     * \brief Start an operation
     *
     * Starts a Control Logic operation with given parameters. An operation may
     * be a switch-related operation (open/close) or a fixed operation with
     * duration (start a test).
     * This is mandatory to be implemented by the Control Logic.
     *
     * \param operation Operation to perform
     * \param errorCode Result of the early detection. GDB_ERROR_NONE if nothing detected.
     *
     * \return Error Code
     */
    virtual GDB_ERROR startOp(const SwitchLogicOperation &operation,
                              const GDB_ERROR errorCode) = 0;

    /* == Inherited from ControlLogic == */
    virtual void newData(PointIdStr pointID, PointData& pointData);

    /* TODO: pueyos_a - add tools, common methods, like: */
    /*
        setValue(DSL_BPOINT_OPENSTATUS, getDBINARYOpenValue(endPos), acceptedOperationTime);
        setValue(DSL_BPOINT_CLOSESTATUS, getDBINARYCloseValue(endPos), acceptedOperationTime);
        setValue(DSL_DBPOINT_STATUS, endPos, acceptedOperationTime);
     */


    virtual GDB_ERROR initInhibit();
    virtual GDB_ERROR checkOLR(const lu_bool_t local);
    virtual GDB_ERROR checkInhibit();
    virtual GDB_ERROR checkAll(const lu_bool_t local);
    virtual GDB_ERROR checkInputValue(const lu_uint16_t OLR_INPUT,
                                      const lu_uint16_t INHIBIT_INPUT,
                                      const lu_uint16_t inputPoint,
                                      PointData *data
                                      );

    virtual void refuseOperationMsg(const GDB_ERROR errorCode);

    virtual void confPosition(const SWITCH_OPERATION operation, DBINARY_STATUS& startPos, DBINARY_STATUS& endPos);

protected:
    PointRef m_inhibitPoint;    //Digital input point used for inhibition (optional use)
    bool m_inhibited;           //This Control Logic has its operation inhibited (optional)
    bool m_dependsOLR;          //The Control Logic operation depends on matching the OLR status

private:
    DIObserver* m_inhObserver;  //Observer over the inhibit point
};

#endif /* OPERATABLECONTROLLOGIC_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
