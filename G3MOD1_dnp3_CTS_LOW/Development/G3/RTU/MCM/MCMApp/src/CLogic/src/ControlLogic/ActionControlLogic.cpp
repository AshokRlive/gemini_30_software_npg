/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ActionControlLogic.cpp 1 Jul 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/CLogic/src/ActionControlLogic.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Control Logic specialised in operating other Control Logics.
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 1 Jul 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   1 Jul 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "ActionControlLogic.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
ActionControlLogic::ActionControlLogic( Mutex* mutex,
                                        GeminiDatabase& g3database,
                                        ActionControlLogic::Config& conf
                                        ) :
                                            ControlLogic(mutex, g3database, conf, CONTROL_LOGIC_TYPE_ACTION),
                                            config(conf),
                                            actionJob(NULL)
{
    dObservers.reserve(Config::MAXINPUTS);  //Adjust vector size to do not waste memory
}


ActionControlLogic::~ActionControlLogic()
{
    database.cancelJob(actionJob);

    /* Release observers */
    MasterLockingMutex mLock(listMutex, LU_TRUE);   //Protect the observers list

    for (lu_uint32_t i = 0; i < dObservers.size(); ++i)
    {
        delete dObservers[i];   //This also detaches the ActionObserver
        dObservers[i] = NULL;
    }
}


GDB_ERROR ActionControlLogic::init()
{
    GDB_ERROR ret = initCheck();
    if(ret != GDB_ERROR_NONE)
    {
        return ret;
    }

    /* Check associated Control Logic */
    if( !(database.isOperatable(config.controlLogicID)) )
    {
        log.error("%s - associated Control Logic %s %i is invalid: cannot be operated.",
                    this->toString().c_str(),
                    database.getTypeName(config.controlLogicID),
                    config.controlLogicID
                    );
        //Disable CLogic
        ret = GDB_ERROR_PARAM;
    }
    /* Check validity of input list */
    if(dObservers.empty())
    {
        /* Note that the observers should be already created by addPoint() before calling init() */
        log.error("%s - No inputs configured", this->toString().c_str());
        ret = GDB_ERROR_PARAM;
    }
    if(ret == GDB_ERROR_NONE)
    {
        initialised = true;
    }

    /* Initialise input points */
    for (lu_uint32_t i = 0; i < dObservers.size(); ++i) //List of action inputs
    {
        /* Initialise event handlers */
        if(dObservers[i]->init() != GDB_ERROR_NONE)
        {
            ret = GDB_ERROR_NOT_INITIALIZED;
        }
    }

    if(ret != GDB_ERROR_NONE)
    {
        //Detach all already attached and disable Control Logic indications
        initialised = false;
        dObservers.clear();
    }

    return ret;
}


GDB_ERROR ActionControlLogic::getInputValue(lu_uint16_t inputPoint, PointData* data)
{
    GDB_ERROR ret = GDB_ERROR_PARAM;

    if(data == NULL)
    {
        return GDB_ERROR_PARAM;
    }

    // Protect the list
    MasterLockingMutex mLock(listMutex);

    try
    {
        PointIdStr id = dObservers.at(inputPoint)->getPointID();
        ret = database.getValue(id, data);
    }
    catch (const std::out_of_range& e)
    {
        ret = GDB_ERROR_NOPOINT;
    }
    return ret;
}


GDB_ERROR ActionControlLogic::getOutputValue(PointData* data)
{
    /* Check online status of the target Control Logic */
    return database.getOutputValue(PointIdStr(config.controlLogicID, 0), data);
}


GDB_ERROR ActionControlLogic::addPoint(PointIdStr point,
                                        DigitalChangeEvent::Config eventCfg,
                                        ACTION_CLOGIC_CMD action,
                                        ACTION_CLOGIC_OLR olr
                                       )
{
    if(initialised)
    {
        return GDB_ERROR_INITIALIZED;
    }
    if(point.isValid())
    {
        MasterLockingMutex mLock(listMutex, LU_TRUE);
        dObservers.push_back(new ActionObserver(*this, database, point, eventCfg, action, olr));
        return GDB_ERROR_NONE;
    }
    log.error("%s - Error adding point %s to group %i.",
                this->toString().c_str(), point.toString().c_str(), groupID);
    return GDB_ERROR_NOPOINT;
}


ActionControlLogic::ActionObserver::ActionObserver(
                                        ActionControlLogic& actionCLogic,
                                        GeminiDatabase& database,
                                        PointIdStr point,
                                        DigitalChangeEvent::Config eventCfg,
                                        const ACTION_CLOGIC_CMD actionLogic,
                                        const ACTION_CLOGIC_OLR localRemote) :
                                            m_actionLogic(actionCLogic),
                                            m_pointID(point),
                                            m_observer(new DigitalChangeEvent(database, point, eventCfg)),
                                            m_action(actionLogic),
                                            m_OLR(localRemote)
{
    m_observer->attach(this);
}

ActionControlLogic::ActionObserver::~ActionObserver()
{
    if(m_observer != NULL)
    {
        m_observer->detach(this);
    }
    delete m_observer;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
void ActionControlLogic::startAction(const ACTION_CLOGIC_CMD action, const ACTION_CLOGIC_OLR olr)
{
    if(!initialised || !enabled)  //Check before delay
    {
        return;
    }
    log.info("%s will start action in %i sec", this->toString().c_str(), config.delay);

    database.cancelJob(actionJob);
    actionJob = new TimedJobAction(*this, action, olr, config.delay * 1000);
    database.addJob(actionJob);
}


void ActionControlLogic::action(const ACTION_CLOGIC_CMD action, const ACTION_CLOGIC_OLR olr)
{
    /* Unlink Timed Job (will be deleted by the Scheduler) */
    actionJob = NULL;

    if(!initialised || !enabled)  //Check after delay
    {
        return;
    }
    log.info("%s starts action over Control Logic %i",
                this->toString().c_str(), config.controlLogicID);

    OLR_STATE currentOLR;
    OLR_STATE useOLR;
    lu_bool_t local = LU_FALSE;
    if( (olr != ACTION_CLOGIC_OLR_LOCAL) && (olr != ACTION_CLOGIC_OLR_REMOTE) )
    {
        if(database.getLocalRemote(currentOLR) == GDB_ERROR_NONE)
        {
            if(olr == ACTION_CLOGIC_OLR_NONOFF)
            {
                if(currentOLR != OLR_STATE_OFF)
                {
                    //do it!
                    useOLR = currentOLR;    //use current local indication
                }
                else
                {
                    //don't!
                    return;
                }
            }
            else
            {
                //don't know current status
                useOLR = OLR_STATE_LOCAL;   //assume local?
            }
            if(olr == ACTION_CLOGIC_OLR_UNUSED)
            {
                useOLR = currentOLR;   //use current local indication
            }
        }
    }
    else
    {
        //use incoming local indication
        useOLR = (olr == ACTION_CLOGIC_OLR_REMOTE)? OLR_STATE_REMOTE : OLR_STATE_LOCAL;
    }
    local = (useOLR == OLR_STATE_LOCAL)? LU_TRUE : LU_FALSE;
    SwitchLogicOperation switchOperation;
    switchOperation.local = local;
    switchOperation.operation = (action==ACTION_CLOGIC_CMD_OPEN)? SWITCH_OPERATION_OPEN : SWITCH_OPERATION_CLOSE;
    GDB_ERROR res = database.startOperation(config.controlLogicID, switchOperation);
    if(res != GDB_ERROR_NONE)
    {
        log.error("%s is unable to command Control Logic %i, error: %s",
                    this->toString().c_str(),
                    config.controlLogicID,
                    GDB_ERROR_ToSTRING(res)
                  );
    }

}


void ActionControlLogic::TimedJobAction::job(TimeManager::TimeStr* timePtr)
{
    LU_UNUSED(timePtr);

    m_cLogic.action(m_action, m_olr);
}


void ActionControlLogic::ActionObserver::update(PointIdStr pointID,
                                                PointData* pointDataPtr)
{
    LU_UNUSED(pointID);

    if(pointDataPtr->isActive() && (pointDataPtr->getEdge() != EDGE_TYPE_NONE))
    {
        m_actionLogic.startAction(m_action, m_OLR);
    }
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
/*
 *********************** End of file ******************************************
 */
