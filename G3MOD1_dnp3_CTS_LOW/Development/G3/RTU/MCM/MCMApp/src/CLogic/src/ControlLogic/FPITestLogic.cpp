/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: FPITestLogic.cpp 3 Sep 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/CLogic/src/ControlLogic/FPITestLogic.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       FPI Test Control Logic
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 3 Sep 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   3 Sep 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "FPITestLogic.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

FPITestLogic::FPITestLogic( Mutex* mutex                 ,
                            GeminiDatabase& database     ,
                            FPITestLogic::Config& config
                            ) :
                            OperatableControlLogic(mutex, database, config, CONTROL_LOGIC_TYPE_FPI_TEST)
{
    channelFPI = dynamic_cast<FPICANChannel*>(config.channel);
    if(channelFPI == NULL)
    {
        log.error("%s Unable to configure FPI Test Logic %i channel",
                    this->toString().c_str(), config.groupID
                    );
    }
}


FPITestLogic::~FPITestLogic()
{}


GDB_ERROR FPITestLogic::init()
{
    GDB_ERROR ret = initCheck();
    if(channelFPI == NULL)
    {
        ret = GDB_ERROR_NOT_INITIALIZED;
    }
    if(ret == GDB_ERROR_NONE)
    {
        /* Initialise input points */
        ret = initInhibit();
    }
    if(ret == GDB_ERROR_NONE)
    {
        initialised = true;
    }

    return ret;
}


GDB_ERROR FPITestLogic::getInputValue(lu_uint16_t inputPoint, PointData *data)
{
    return checkInputValue(FPITEST_INPUT_OLR, FPITEST_INPUT_INHIBIT, inputPoint, data);
}


GDB_ERROR FPITestLogic::getOutputValue(PointData* data)
{
    if(data == NULL)
    {
        return GDB_ERROR_PARAM;
    }
    if(channelFPI == NULL)
    {
        return GDB_ERROR_NOT_INITIALIZED;
    }
    /* Check online status */
    data->setOnlineFlag(channelFPI->isActive() == LU_TRUE);
    return GDB_ERROR_NONE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
GDB_ERROR FPITestLogic::startOp(const SwitchLogicOperation& operation,
                                const GDB_ERROR errorCode)
{
    LU_UNUSED(operation);

    if(errorCode != GDB_ERROR_NONE)
    {
        refuseOperationMsg(errorCode);
        return errorCode;
    }

    if(channelFPI->isActive() == LU_FALSE)
    {
        log.info("%s - Error: channel not available.", this->toString().c_str());
        return GDB_ERROR_INHIBIT;
    }

    log.info("%s - Operate.", this->toString().c_str());

    /* Send command */
    IOM_ERROR chRet = channelFPI->operate();
    if(chRet != IOM_ERROR_NONE)
    {
        log.error("%s - Operation failed: %i (%s)",
                  this->toString().c_str(), chRet, IOM_ERROR_ToSTRING(chRet)
                  );
    }

    return ((chRet == IOM_ERROR_NONE) ? GDB_ERROR_NONE : GDB_ERROR_CMD);
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
