/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: DBinSplitterLogic.cpp 13 Sep 2016 pueyos_a $
 *               $HeadURL: http://10.11.0.6:81/svn/gemini_30_software/trunk/Development/G3/RTU/MCM/MCMApp/src/CLogic/src/ControlLogic/DBinSplitterLogic.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Double Binary VP to Single Binaries Logic
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 13 Sep 2016 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13 Sep 2016   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "DBinSplitterLogic.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
DBinSplitterLogic::DBinSplitterLogic(Mutex* mutex,
                                    GeminiDatabase& database,
                                    DBinSplitterLogic::Config& config
                                    ) :
                                    ControlLogic(mutex, database, config, CONTROL_LOGIC_TYPE_DTS),
                                    dbInput(config.dbInput, *this)
{
    /* Allocate Control Logic virtual points */
    clPoint.reserve(config.ALLPOINT_NUM);  //Adjust vector size to do not waste memory

    TimeManager::TimeStr curTime = timeManager.now();   /* Initial time stamp */
    PointFlags sBinFlags;
    sBinFlags.setOnline(enabled);

    for(lu_uint32_t id = 0; id < Config::SBPOINT_NUM; ++id)
    {
        clPoint.push_back(new ControlLogicDigitalPoint(mutex, database, config.bPoint[id], POINT_TYPE_BINARY));
        setFlags(id, sBinFlags, curTime); //Set initial flags
    }
}

DBinSplitterLogic::~DBinSplitterLogic()
{
    stopUpdate();
    detachObserver(&dbInput);
}

GDB_ERROR DBinSplitterLogic::init()
{
    GDB_ERROR ret = initCheck();
    if(ret == GDB_ERROR_NONE)
    {
        ret = activateCLPoints();   //Initialise Control Logic points as well
    }
    if(ret != GDB_ERROR_NONE)
    {
        return ret;
    }

    startCLPoints(true);
    initialised = true;

    /* Initialise input points */
    ret = attachObserver(&dbInput);
    if(ret != GDB_ERROR_NONE)
    {
        //Detach all already attached and disable Control Logic indications
        initialised = false;
        detachObserver(&dbInput);
        startCLPoints(false);
    }
    return ret;
}


GDB_ERROR DBinSplitterLogic::getInputValue(lu_uint16_t inputPoint,
                                            PointData* data)
{
    GDB_ERROR ret = GDB_ERROR_NOPOINT;

    if(data == NULL)
    {
        return GDB_ERROR_PARAM;
    }

    if(inputPoint == 0)
    {
        ret = database.getValue(dbInput.getPointID(), data);
    }
    return ret;
}


void DBinSplitterLogic::newData(PointIdStr pointID, PointData& pointData)
{
    LU_UNUSED(pointID);

    if(!enabled)
    {
        return;
    }

    /* An observed point has changed: check all and update output */
    const PointFlags outFlags = pointData.getFlags();

    TimeManager::TimeStr timestamp;
    pointData.getTime(timestamp);

    lu_uint8_t rawValue = (lu_uint8_t)pointData;

    for (lu_uint32_t id = 0; id < Config::SBPOINT_NUM; ++id)
    {
        setValue(id, (rawValue == id), timestamp, outFlags);
    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
