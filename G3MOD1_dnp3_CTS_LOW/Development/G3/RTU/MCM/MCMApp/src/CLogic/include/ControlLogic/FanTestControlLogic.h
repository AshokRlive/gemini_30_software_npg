/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Fan Test control Logic interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   27/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_36990FF3_C592_48d9_A195_55D2B8595FD6__INCLUDED_)
#define EA_36990FF3_C592_48d9_A195_55D2B8595FD6__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "OperatableControlLogic.h"
#include "FanCANChannel.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class FanTestControlLogic : public OperatableControlLogic
{
public:
    /**
     * \brief Fan Test logic control block
     */
    struct FANOperationParameters
    {
    public:
        lu_uint8_t duration;      //Test duration in seconds
    };

    struct Config : public OperatableControlLogic::Config
    {
    public:
        FANOperationParameters operationParameters;     /* Control Logic operation parameters */
        IChannel* channel;  //Channel to operate
    };


public:
    FanTestControlLogic( Mutex* mutex                 ,
                         GeminiDatabase& database     ,
                         FanTestControlLogic::Config& conf
                       );
    virtual ~FanTestControlLogic() {};

    /* == inherited from ControlLogic == */
    virtual GDB_ERROR init();
    virtual GDB_ERROR getInputValue(lu_uint16_t inputPoint, PointData *data);
    virtual GDB_ERROR getOutputValue(PointData* data);

protected:
    /* == Implements mandatory OperatableControlLogic == */
    virtual GDB_ERROR startOp(const SwitchLogicOperation &operation, const GDB_ERROR errorCode);

private:
    FANOperationParameters operationParameters;     /* Control Logic operation parameters */
    FanCANChannel* fanChannel;
};

#endif // !defined(EA_36990FF3_C592_48d9_A195_55D2B8595FD6__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
