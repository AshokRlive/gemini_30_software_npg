/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: FPIResetLogic.cpp 3 Sep 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/CLogic/src/ControlLogic/FPIResetLogic.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 3 Sep 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   3 Sep 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "FPIResetLogic.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
FPIResetLogic::FPIResetLogic(Mutex* mutex,
                             GeminiDatabase& database,
                             FPIResetLogic::Config& config
                             ) :
                             OperatableControlLogic(mutex, database, config, CONTROL_LOGIC_TYPE_FPI_RESET),
                             conf(config),
                             dObservers()
{
    channelFPI = dynamic_cast<FPICANChannel*>(config.channel);
    if(channelFPI == NULL)
    {
        log.error("%s Unable to configure FPI Reset Logic %i channel", this->toString().c_str());
    }

    /* Get Digital Input points */
    dependsLV = conf.dependsLVRestore || conf.dependsLVLoss;
    if(dependsLV)
    {
        /* Create and Attach an observer */
        dObservers[FPIRESET_INPUT_LVFAIL] = new DIObserver(conf.dInput[FPIRESET_INPUT_LVFAIL], *this);
    }
}


FPIResetLogic::~FPIResetLogic()
{
    /* Detach and release digital input observers */
    for(lu_uint32_t i = 0; i < FPIRESET_INPUT_LAST; ++i)
    {
        detachObserver(dObservers[i]);
    }
}


GDB_ERROR FPIResetLogic::init()
{
    GDB_ERROR ret = initCheck();
    if(channelFPI == NULL)
    {
        ret = GDB_ERROR_NOT_INITIALIZED;
    }
    if(ret == GDB_ERROR_NONE)
    {
        initialised = true;

        /* Initialise input points */
        ret = initInhibit();
        if(ret == GDB_ERROR_NONE)
        {
            if(dependsLV)
            {
                ret = attachObserver(dObservers[FPIRESET_INPUT_LVFAIL]);
            }
        }
        if(ret != GDB_ERROR_NONE)
        {
            //Detach all already attached and disable Control Logic indications
            initialised = false;
            detachObserver(dObservers[FPIRESET_INPUT_LVFAIL]);
            startCLPoints(false);
        }
    }
    return ret;
}


GDB_ERROR FPIResetLogic::getInputValue(lu_uint16_t inputPoint, PointData* data)
{
    GDB_ERROR ret = checkInputValue(FPIRESET_INPUT_OLR, FPIRESET_INPUT_INHIBIT, inputPoint, data);
    if(ret == GDB_ERROR_NOPOINT)
    {
        //Check FPIReset-specific points
        if(inputPoint >= FPIRESET_INPUT_LAST)
        {
            return GDB_ERROR_PARAM;
        }
        if(dObservers[inputPoint] != NULL)
        {
            return database.getValue(dObservers[inputPoint]->getPointID(), data);
        }
    }
    return ret;
}


GDB_ERROR FPIResetLogic::getOutputValue(PointData* data)
{
    if(data == NULL)
    {
        return GDB_ERROR_PARAM;
    }
    if(channelFPI == NULL)
    {
        return GDB_ERROR_NOT_INITIALIZED;
    }
    /* Check online status */
    data->setOnlineFlag(channelFPI->isActive() == LU_TRUE);
    return GDB_ERROR_NONE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
GDB_ERROR FPIResetLogic::startOp(const SwitchLogicOperation& operation, const GDB_ERROR errorCode)
{
    LU_UNUSED(operation);

    if(errorCode != GDB_ERROR_NONE)
    {
        refuseOperationMsg(errorCode);
        return errorCode;
    }

    if(channelFPI->isActive() == LU_FALSE)
    {
        log.info("%s - Error: channel not available.", this->toString().c_str());
        return GDB_ERROR_INHIBIT;
    }

    log.info("%s - FPI %i Resets.", this->toString().c_str(), groupID);

    /* Send command */
    IOM_ERROR chRet = channelFPI->operate();
    if(chRet != IOM_ERROR_NONE)
    {
        log.error("%s - Operation failed: %i (%s)",
                    this->toString().c_str(), chRet, IOM_ERROR_ToSTRING(chRet)
                  );
    }

    return ((chRet == IOM_ERROR_NONE) ? GDB_ERROR_NONE : GDB_ERROR_CMD);
}


void FPIResetLogic::newData(PointIdStr pointID, PointData& pointData)
{
    if(!enabled)
        return;

    if(pointID == dObservers[FPIRESET_INPUT_LVFAIL]->getPointID())
    {
        //Check change type
        if( ( (conf.dependsLVRestore) && (pointData.getEdge() == EDGE_TYPE_NEGATIVE) ) ||
            ( (conf.dependsLVLoss) && (pointData.getEdge() == EDGE_TYPE_POSITIVE) )
            )
        {
            /* A signal rise/fall triggers an FPI reset (when configured to do so) */
            SwitchLogicOperation op;
            //Internal Operation does not depend on Local/Remote: use current
            OLR_STATE olr;
            database.getLocalRemote(olr);
            op.local = (olr == OLR_STATE_REMOTE)? LU_FALSE : LU_TRUE;
            startOperation(op);
        }
    }
    else
    {
        OperatableControlLogic::newData(pointID, pointData);
    }
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
