/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <limits>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "FanTestControlLogic.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

FanTestControlLogic::FanTestControlLogic( Mutex* mutex                   ,
                                          GeminiDatabase& g3database     ,
                                          FanTestControlLogic::Config& config
                                        ) :
                                     OperatableControlLogic(mutex, g3database, config, CONTROL_LOGIC_TYPE_FAN_TEST),
                                     operationParameters(config.operationParameters)
{
    fanChannel = dynamic_cast<FanCANChannel*>(config.channel);
    if(fanChannel == NULL)
    {
        log.error("%s Unable to configure Fan Test channel", this->toString().c_str());
    }
}


GDB_ERROR FanTestControlLogic::init()
{
    GDB_ERROR ret = initCheck();
    if(fanChannel == NULL)
    {
        ret = GDB_ERROR_NOT_INITIALIZED;
    }
    if(ret == GDB_ERROR_NONE)
    {
        initialised = true;
    }
    return ret;
}


GDB_ERROR FanTestControlLogic::getInputValue(lu_uint16_t inputPoint, PointData *data)
{
    return checkInputValue(FAN_INPUT_OLR, FAN_INPUT_INHIBIT, inputPoint, data);
}


GDB_ERROR FanTestControlLogic::getOutputValue(PointData* data)
{
    if(data == NULL)
    {
        return GDB_ERROR_PARAM;
    }
    if(fanChannel == NULL)
    {
        return GDB_ERROR_NOT_INITIALIZED;
    }
    /* Check online status */
    data->setOnlineFlag(fanChannel->isActive() == LU_TRUE);
    return GDB_ERROR_NONE;
}


GDB_ERROR FanTestControlLogic::startOp(const SwitchLogicOperation &operation,
                                       const GDB_ERROR errorCode)
{
    IOM_ERROR chRet;
    lu_uint8_t realDuration;
    lu_uint16_t durationSet;

    if(errorCode != GDB_ERROR_NONE)
    {
        refuseOperationMsg(errorCode);
        return errorCode;
    }
    if(fanChannel->isActive() == LU_FALSE)
    {
        log.error("%s - Error: channel not available.", this->toString().c_str());
        return GDB_ERROR_INHIBIT;
    }

    /* when duration is set to 0, take the default duration */
    durationSet = (operation.duration == 0)? operationParameters.duration : operation.duration;
    const lu_uint64_t maxVal = std::numeric_limits<typeof(realDuration)>::max();
    realDuration = LU_MIN(durationSet, maxVal);
    log.info("%s - Start Test, duration: %i seconds", this->toString().c_str(), realDuration);

    /* Send command */
    chRet = fanChannel->fanTest(realDuration);
    log.info("%s Command result: %i (%s)",
                this->toString().c_str(), chRet, IOM_ERROR_ToSTRING(chRet)
                );

    //Report Event
    CTEventCLogicStr event;
    event.eventType = EVENT_TYPE_CLOGIC_FANTEST;  //to be defined
    event.cLogicType = EVENT_TYPE_CLOGICTYPE_FANTEST;
    event.value = (chRet == IOM_ERROR_NONE)? EVENTCLOGIC_VALUE_OK : EVENTCLOGIC_VALUE_FAIL;
    eventLog->logEvent(event);

    return ((chRet == IOM_ERROR_NONE) ? GDB_ERROR_NONE : GDB_ERROR_CMD);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
