/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: LoggerStatus.cpp 27 Mar 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/StatusManager/src/LoggerStatus.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 27 Mar 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   27 Mar 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "LoggerStatus.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

LoggerStatus::LoggerStatus(IMemoryManager& memoryMgr) :
                        memoryManager(memoryMgr),
                        log(Logger::getLogger(SUBSYSTEM_ID_MAIN))
{
    /* read Log Levels */
    SUBSYSTEM_ID subsystem;
    lu_uint8_t logLevels[SUBSYSTEM_ID_LAST]; //Used u8 instead of LOG_LEVEL to save memory

    /* Read all log levels from memory */
    if( memoryManager.read(IMemoryManager::STORAGE_VAR_LOGLEVEL,
                            (lu_uint8_t *)logLevels
                            ) == LU_FALSE
        )
    {
        log.error("%s: reading of log levels from memory failed.", __AT__);
        log.setAllLogLevel(LOG_LEVEL_WARNING);  //default log level
    }
    else
    {
        /* Set all log levels from the ones obtained */
        for(lu_uint32_t i = 0; i < SUBSYSTEM_ID_LAST; i++)
        {
            subsystem = static_cast<SUBSYSTEM_ID>(i);
            Logger::getLogger(subsystem).setLogLevel(static_cast<LOG_LEVEL>(logLevels[subsystem]));
        }
    }

    //After reading them, attach observer for any new log level change
    if(log.attach(*this) == LU_FALSE)
    {
        log.error("%s: unable to record any log level change.", __AT__);
    }
}

LoggerStatus::~LoggerStatus()
{
    log.detach(*this);
}


void LoggerStatus::update()
{
    /* save Log Levels */
    SUBSYSTEM_ID subsystem;
    lu_uint8_t logLevels[SUBSYSTEM_ID_LAST]; //Used u8 instead of LOG_LEVEL to save memory

    /* Get all log levels currently set */
    for(lu_uint32_t i = 0; i < SUBSYSTEM_ID_LAST; i++)
    {
        subsystem = static_cast<SUBSYSTEM_ID>(i);
        logLevels[subsystem] = Logger::getLogger(subsystem).getLogLevel();
    }

    /* Write all log levels to memory*/
    if( memoryManager.write(IMemoryManager::STORAGE_VAR_LOGLEVEL,
                            (lu_uint8_t *)logLevels
                            ) == LU_FALSE
        )
    {
        log.error("%s: writing log levels to memory failed.", __AT__);
    }
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
