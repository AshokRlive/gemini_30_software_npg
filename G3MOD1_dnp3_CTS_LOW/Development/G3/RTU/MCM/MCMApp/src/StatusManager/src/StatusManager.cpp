/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Status Manager module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   09/05/12      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "StatusManager.h"
#include "MCMDoorSwitch.h"  //For Power saving timer
#include "MCMLED.h"         //To set power saving mode for the LEDs

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define POWERSAVEDELAY  2000    //default power saving On delay time in milliseconds

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

StatusManager::StatusManager( IMCMApplication& iMCMApp,
                                GeminiDatabase& database,
                                IMemoryManager& memoryMgr
                                ) :
                                    mcmApplication(iMCMApp),
                                    log(Logger::getLogger(SUBSYSTEM_ID_STATMAN)),
                                    g3Database(database),
                                    memoryManager(memoryMgr),
                                    m_protocolStackDelay_sec(0),
                                    configFileTimestamp(""),
									configFileDesc(""),
                                    RTUServiceMode(SERVICEMODE_INVALID),                //will be read from NVRAM
                                    RTUServiceModeReason(OUTSERVICEREASON_UNDEFINED),   //will be read from NVRAM
                                    powerSavingDelay(POWERSAVEDELAY),
                                    powerSaveTimer(NULL),
                                    siteName("RTU Gemini3"), //Default RTU site name
                                    pwerSavings(NULL),
                                    pwerSavingSize(0)
{
    configFileVersion.major = 0;
    configFileVersion.minor = 0;

    /* Read last Service Mode/Upgrade State from non-volatile memory */
    SERVICEMODE newServiceMode = SERVICEMODE_INSERVICE;  //By default, is in service;
    OUTSERVICEREASON newServiceReason = OUTSERVICEREASON_UNDEFINED;  //don't know why was out of service -- if it was.
    lu_uint8_t storedServiceMode;  //value is stored in reduced format
    if(memoryManager.read(IMemoryManager::STORAGE_VAR_SERVICE, &storedServiceMode) == LU_TRUE)
    {
        newServiceMode = static_cast<SERVICEMODE>(storedServiceMode);
        lu_uint8_t storedServiceReason;  //value is stored in reduced format
        if(memoryManager.read(IMemoryManager::STORAGE_VAR_SERVICEREASON, &storedServiceReason) == LU_TRUE)
        {
            newServiceReason = static_cast<OUTSERVICEREASON>(storedServiceReason);
        }
    }
    setServiceMode(newServiceMode, newServiceReason);  //Set service mode to execute actions
}


StatusManager::~StatusManager()
{
    /* Cancel timed job if already active */
    g3Database.cancelJob(powerSaveTimer);
}


void StatusManager::setPowerSavingComps(ISupportPowerSaving** pwerSavings,
                            lu_uint32_t pwerSavingSize)
{
    this->pwerSavings = pwerSavings;
    this->pwerSavingSize = pwerSavingSize;
}


MCM_ERROR StatusManager::factoryReset()
{
    fprintf(stdout, "Nope.\n");
    MCM_ERROR ret = MCM_ERROR_COMMISSION;
//    /* De-commission */
//    if( ModuleRegistrationManager::getInstance()->decommissionModules()
//                                                              == MRDB_ERROR_NONE
//      )
//    {
//        ret = MCM_ERROR_NONE;
//    }
    /* Delete config file */
    // TODO add & call API in ConfigurationToolHandler to do this
//    FileTransferManager ftm;
//    ftm.eraseConfig();
    return ret;
}


void StatusManager::setServiceMode(SERVICEMODE serviceMode,
                                    OUTSERVICEREASON reason = OUTSERVICEREASON_UNDEFINED)
{
    if(RTUServiceMode != serviceMode)
    {
        //Service mode changed
        RTUServiceMode = serviceMode;
        if(serviceMode == SERVICEMODE_OUTSERVICE)
        {
            //Entering service mode implies: 1)store reason, 2)suspend SCADA communications
            RTUServiceModeReason = reason;
            mcmApplication.setSCADAService(LU_FALSE);
        }
        else
        {
            //Exiting service mode implies resume SCADA communications
            mcmApplication.setSCADAService(LU_TRUE);
        }

        //store new service mode in memory
        lu_uint8_t storedServMode;   //value is stored in reduced format
        lu_uint8_t storedServReason; //value is stored in reduced format
        storedServMode = static_cast<SERVICEMODE>(RTUServiceMode);
        storedServReason = static_cast<OUTSERVICEREASON>(RTUServiceModeReason);
        memoryManager.write(IMemoryManager::STORAGE_VAR_SERVICE, &storedServMode);
        memoryManager.write(IMemoryManager::STORAGE_VAR_SERVICEREASON, &storedServReason);
    }
}


void StatusManager::getServiceMode(SERVICEMODE &serviceMode, OUTSERVICEREASON &reason)
{
    serviceMode = RTUServiceMode;
    reason = RTUServiceModeReason;
}


void StatusManager::setPowerSavingDelay(lu_uint64_t delay)
{
    powerSavingDelay = delay;
}


void StatusManager::setPowerMode(lu_bool_t powerMode)
{
    lu_bool_t runItNow = LU_TRUE;   //start Power Saving mode now or delayed
    if( (powerMode == LU_TRUE) && (powerSavingDelay != 0) )
    {
        /* Delay power saving On action */
        powerSaveTimer = new PowerSaveTimer(*this, powerSavingDelay);
        if( g3Database.addJob(powerSaveTimer) == GDB_ERROR_NONE)
        {
            runItNow = LU_FALSE;
        }
    }
    if(powerMode == LU_FALSE)
    {
        g3Database.cancelJob(powerSaveTimer);   //Out of power saving mode cancels running timer
    }
    if(runItNow == LU_TRUE)
    {
        //set power saving mode now
        setPowerSavingMode(powerMode);
    }
}


void StatusManager::setSiteNameRTU(std::string siteNameRTU)
{
    siteName = siteNameRTU;
}


std::string StatusManager::getSiteNameRTU()
{
    return siteName;
}


std::string StatusManager::getConfigFileTimestamp()
{
    return configFileTimestamp;
}


void StatusManager::setConfigFileTimestamp(std::string cfgFileTimestamp)
{
    configFileTimestamp = cfgFileTimestamp;
}


void StatusManager::setConfigFileDesc(std::string cfgFileDescription)
{
	this->configFileDesc = cfgFileDescription;

}

std::string StatusManager::getConfigFileDesc()
{
	return this->configFileDesc;
}

void StatusManager::setConfigFileVersion(BasicVersionStr version)
{
    configFileVersion = version;
}


lu_bool_t StatusManager::getConfigFileVersion(BasicVersionStr *version)
{
    version->major = configFileVersion.major;
    version->minor = configFileVersion.minor;

    return configFilePresent;
}


void StatusManager::setConfigFilePresence(lu_bool_t filePresent)
{
    configFilePresent = filePresent;
}


lu_bool_t StatusManager::getConfigFilePresence()
{
    return configFilePresent;
}


void StatusManager::setConfigFileCRC(lu_uint32_t calculatedCRC, lu_bool_t validCRC)
{
    configFileCRCValid = validCRC;
    configFileCRC = calculatedCRC;
}


lu_bool_t StatusManager::getConfigFileCRC(lu_uint32_t& calculatedCRC)
{
    calculatedCRC = configFileCRC;
    return configFileCRCValid;
}


lu_uint32_t StatusManager::getMaxEventsStorage()
{
    lu_uint32_t memEventSize = 0;
    memoryManager.read(IMemoryManager::STORAGE_VAR_EVENTLOG, (lu_uint8_t*)&memEventSize);
    return memEventSize;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void StatusManager::setPowerSavingMode(lu_bool_t powerMode)
{
    /* Unlink Timed Job (will be deleted by the Scheduler) */
    powerSaveTimer = NULL;

    /* Set the MCM LEDs in power saving mode on/off */
    MCMLED::setPowerSaveMode(powerMode);

    if(pwerSavings != NULL)
    {
        lu_uint16_t i;
        for (i = 0; i < pwerSavingSize; ++i)
        {
            if(pwerSavings[i] != NULL)
            {
                pwerSavings[i]->setPowerSaveMode(powerMode);
            }
        }
    }
}


/*
 *********************** End of file ******************************************
 */
