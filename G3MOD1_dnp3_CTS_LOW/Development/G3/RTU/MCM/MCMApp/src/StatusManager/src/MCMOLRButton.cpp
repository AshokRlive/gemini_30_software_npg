/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id: MCMOLRButton.cpp 4 Sep 2013 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/HMIManager/src/MCMOLRButton.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MCM front OLR button handler implementation module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 4 Sep 2013 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   4 Sep 2013    pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MCMOLRButton.h"
#include "timeOperations.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */


MCMOLRButton::MCMOLRButton(IIOModuleManager &moduleManager,
                           GeminiDatabase &Gdatabase,
                           const IIOModuleManager::OLRButtonConfig conf):
                                            database(Gdatabase),
                                            config(conf),
                                            channel(NULL),
                                            buttonPosition(2),   //invalid by default
                                            countOff(LU_FALSE), //not counting
                                            counterJob(NULL)
{
    /* Start Off/Local/Remote physical BUTTON observer */
    IIOModule *moduleMCMPtr = moduleManager.getModule(MODULE_MCM, MODULE_ID_0);

    if(moduleMCMPtr != NULL)
    {
        channel = moduleMCMPtr->getChannel(CHANNEL_TYPE_DINPUT, MCM_CH_DINPUT_OLR_BUTTON);
        if(channel != NULL)
        {
            channel->attach(this);  //The attach action already reads (updates) the initial channel status
        }
        else
        {
            Logger::getLogger(SUBSYSTEM_ID_HMIM).error("%s: "
                            "MCM Input Channel %d not found for MCM's front button",
                            __AT__, MCM_CH_DINPUT_OLR_BUTTON
                          );
        }
    }
    clock_gettimespec(&btnPressTime);  //get a default value for the initial pressing
}


MCMOLRButton::~MCMOLRButton()
{
    /* Cancel timed job if already active */
    database.cancelJob(counterJob);

    if(channel != NULL)
    {
        channel->detach(this);
        channel = NULL;
    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
void MCMOLRButton::CountOLROffTimer::job(TimeManager::TimeStr *timePtr)
{
    LU_UNUSED(timePtr);

    /* Signal timeout to the related channel observer */
    channelObserver.endTimer();
}


void MCMOLRButton::endTimer()
{
    /* Unlink Timed Job (will be deleted by the Scheduler) */
    counterJob = NULL;

    if(countOff == LU_TRUE)
    {
        //OFF counter timed out before button was released
        countOff = LU_FALSE;
        database.setLocalRemote(OLR_STATE_OFF, OLR_SOURCE_FBUTTON);
    }
}


void MCMOLRButton::update(IChannel *subject)
{
    IOM_ERROR chRet;
    struct timespec btnReleaseTime;
    IODataUint8 data;
    IChannel::ValueStr valueRead(data);

     /* Read the channel value */
    chRet = subject->read(valueRead);

    if( (chRet != IOM_ERROR_NONE) || (valueRead.flags.online != LU_TRUE) )
    {
        /* No Local/Remote button channel available */
        countOff = LU_FALSE;
        btnPressTime = TIMESPEC_ZERO;
        return; //Nothing more we can do.
    }

    lu_uint8_t data8 = data;    //convert to uint8_t first
    if(buttonPosition != data8)
    {
        //edge detect
        if(data8 == 1)
        {
            //button pressed event (already debounced): start counting!
            clock_gettimespec(&btnPressTime);
            /* Start counting the time for automatically switch to off. */
            database.cancelJob(counterJob);
            countOff = LU_TRUE;
            counterJob = new CountOLROffTimer(*this, config.toggleTimeOFF_ms);
            database.addJob(counterJob);
        }
        else
        {
            //button released: check time and change OLR
            if(countOff == LU_FALSE)
            {
                return; //not counting
            }
            countOff = LU_FALSE;    //disable counting
            database.cancelJob(counterJob);

            clock_gettimespec(&btnReleaseTime);
            lu_uint32_t lapseMs = timespec_elapsed_ms(&btnPressTime, &btnReleaseTime);
            Logger::getLogger(SUBSYSTEM_ID_HMIM).debug("%s - OLR button released at %lu.%03lu: lapse=%lu",
                        __AT__,
                        btnReleaseTime.tv_sec,
                        btnReleaseTime.tv_nsec * 1000000,
                        lapseMs
                        );
            if(lapseMs >= config.toggleTimeOFF_ms)
            {
                //button long pressed: set to OFF
                database.setLocalRemote(OLR_STATE_OFF, OLR_SOURCE_FBUTTON);
            }
            else if(lapseMs >= config.toggleTimeLR_ms)
            {
                //get current state to decide where to switch to
                OLR_STATE currentOLR;
                GDB_ERROR result = database.getLocalRemote(currentOLR);
                if(result == GDB_ERROR_NONE)
                {
                    /* Change current status
                     * If it was OFF, always set to LOCAL;
                     * If it was LOCAL, set to REMOTE;
                     * If it was REMOTE, set to LOCAL;
                     */
                    OLR_STATE newOLR = (currentOLR == OLR_STATE_OFF)? OLR_STATE_LOCAL :
                                       (currentOLR == OLR_STATE_LOCAL)? OLR_STATE_REMOTE :
                                                                        OLR_STATE_LOCAL;
                    database.setLocalRemote(newOLR, OLR_SOURCE_FBUTTON);
                }
                else
                {
                    Logger::getLogger(SUBSYSTEM_ID_HMIM).error("%s - Error getting current OLR status: %s",
                                        __AT__,
                                        GDB_ERROR_ToSTRING(result)
                                        );
                }
            }
        }
    }
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
