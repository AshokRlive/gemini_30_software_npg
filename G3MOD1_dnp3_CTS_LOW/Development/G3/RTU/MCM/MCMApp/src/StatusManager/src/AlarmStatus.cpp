/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: AlarmStatus.cpp 24 Apr 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/StatusManager/src/AlarmStatus.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 24 Apr 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24 Apr 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "AlarmStatus.h"
#include "bitOperations.h"
#include "LockingMutex.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
AlarmStatus::AlarmStatus(IChannel* channelSeverity[AlarmStatus::SEVERITY_LAST],
                         MonitorProtocolManager& protocolMonitor
                         ) :
                                 monitorProtocol(protocolMonitor),
                                 log(Logger::getLogger(SUBSYSTEM_ID_MCMMODULE)),
                                 lastSeverity(0)
{
    //Copy the provided array locally
    for (lu_uint32_t severity = 0; severity < AlarmStatus::SEVERITY_LAST; ++severity)
    {
        severityChannel[severity] = channelSeverity[severity];
        if(channelSeverity[severity] != NULL)
        {
            severityChannel[severity]->attach(this);
        }
    }
}


AlarmStatus::~AlarmStatus()
{
    for (lu_uint32_t severity = 0; severity < AlarmStatus::SEVERITY_LAST; ++severity)
    {
        if(severityChannel[severity] != NULL)
        {
            severityChannel[severity]->detach(this);
        }
    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void AlarmStatus::update(IChannel* subject)
{
    LockingMutex lMutex(accessLock);

    IOM_ERROR chRet;
    IODataUint8 data;
    IChannel::ValueStr valueRead(data);

    /* The alarm channel has been updated: read the channel value */
    for (lu_uint32_t severity = 0; severity < AlarmStatus::SEVERITY_LAST; ++severity)
    {
        if(severityChannel[severity] == subject)
        {
            //Found related severity
            chRet = subject->read(valueRead);
            if(chRet != IOM_ERROR_NONE)
            {
                log.error("%s - %s error %i reading MCM-related alarm channel: %s",
                          __AT__,
                          SYS_ALARM_SEVERITY_ToSTRING(severity),
                          chRet,
                          IOM_ERROR_ToSTRING(chRet)
                          );
                return;
            }
            lu_uint8_t newData = data;
            lu_bool_t newValue = (newData != 0)? LU_TRUE : LU_FALSE;
            LU_SETBITVALUE(lastSeverity, severity, newValue);

            /* Review current severities and update OK LED accordingly */
            for (lu_uint32_t sev = 0; sev < AlarmStatus::SEVERITY_LAST; ++sev)
            {
                if(LU_GETBIT(lastSeverity, sev) != 0)
                {
                    switch(sev)
                    {
                        case AlarmStatus::SEVERITY_CRITICAL:
                            monitorProtocol.send_OK_LED_state(OK_LEDSTATE_CRITICAL);
                            return; //max severity found: no need to check more
                            break;
                        case AlarmStatus::SEVERITY_ERROR:
                            monitorProtocol.send_OK_LED_state(OK_LEDSTATE_ALARM);
                            return; //max severity found: no need to check more
                            break;
                        case AlarmStatus::SEVERITY_WARNING:
                            monitorProtocol.send_OK_LED_state(OK_LEDSTATE_WARNING);
                            return; //max severity found: no need to check more
                        default:
                            break;
                    }
                }
            }
            monitorProtocol.send_OK_LED_state(OK_LEDSTATE_HEALTHY);
            return; //No need to check for more channels
        }
    }
}


/*
 *********************** End of file ******************************************
 */
