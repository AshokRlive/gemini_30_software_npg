/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Status Manager header module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   09/05/12      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_645294B7_EE43_4123_A6C3_C122D81BB3DD__INCLUDED_)
#define EA_645294B7_EE43_4123_A6C3_C122D81BB3DD__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Logger.h"
#include "GeminiDatabase.h"
#include "IIOModuleManager.h"
#include "IStatusManager.h"
#include "IMemoryManager.h"
#include "IMCMApplication.h"
#include "ISupportPowerSaving.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

class PowerSaveTimer;       //Power saving timer

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * The StatusManager keeps the state of some of the RTU components.
 */
class StatusManager : public IStatusManager
{
public:
    StatusManager( IMCMApplication& iMCMApp,
                     GeminiDatabase& database,
                     IMemoryManager& memoryMgr
                     );

    virtual ~StatusManager();

    virtual MCM_ERROR factoryReset();

    /**
     * \brief Set the RTU Service Mode.
     *
     * Set the RTU service mode to "In Service"/"Out of Service".
     *
     * \param serviceMode Service mode to be set
     * \param reason Reason for setting the RTU out of service [Optional, default=none]
     *
     * \return Error Code
     */
    virtual void setServiceMode(SERVICEMODE serviceMode, OUTSERVICEREASON reason);

    /**
     * \brief Get the RTU Service Mode.
     *
     * Get the value of the RTU Service Mode, and the reason of being out of
     * service if applicable.
     *
     * \param serviceMode Service mode to be returned
     * \param reason Reason for the RTU to be out of service to be returned
     *
     * \return Error Code
     */
    virtual void getServiceMode(SERVICEMODE &serviceMode, OUTSERVICEREASON &reason);

    /**
     * \brief Set the delay for Power Saving Mode
     *
     * \param delay Delay to set the Power Saving Mode, in ms.
     */
    virtual void setPowerSavingDelay(lu_uint64_t delay);

    /**
     * \brief Set Power Mode
     *
     * Shuts down/awake all the RTU's LEDs by handling the MCM's LEDs and also
     * by sending LED shut/awake to the rest of the RTU modules. Shut down LEDs
     * is made after a delay.
     *
     * \param powerMode Power Saving mode. Set to LU_TRUE for shut down LEDs
     */
    virtual void setPowerMode(lu_bool_t powerMode);

    /**
     * \brief Sets the RTU site name
     *
     * Set a name for the RTU.
     */
    virtual void setSiteNameRTU(std::string siteNameRTU);

    /**
     * \brief Gets the RTU site name
     *
     * Gets a string containing the RTU site name.
     */
    virtual std::string getSiteNameRTU();

    virtual void setConfigFileTimestamp(std::string cfgFileTimestamp);
    virtual std::string getConfigFileTimestamp();
    virtual std::string getConfigFileDesc();
    virtual void setConfigFileDesc(std::string cfgFileDescription);
    virtual void setConfigFileVersion(BasicVersionStr version);
    virtual lu_bool_t getConfigFileVersion(BasicVersionStr *version);
    virtual void setConfigFilePresence(lu_bool_t filePresent);
    virtual lu_bool_t getConfigFilePresence();
    virtual void setConfigFileCRC(lu_uint32_t calculatedCRC, lu_bool_t validCRC = LU_TRUE);
    virtual lu_bool_t getConfigFileCRC(lu_uint32_t& calculatedCRC);

    virtual lu_uint32_t getProtocolStackDelay() const
    {
        return m_protocolStackDelay_sec;
    }

    virtual void setProtocolStackDelay(const lu_uint32_t protocolStackDelaySec)
    {
        m_protocolStackDelay_sec = protocolStackDelaySec;
    }

    /**
     * \brief Obtain the max amount of events that can be stored
     *
     * \return Overall number of events that can be stored (does not count already stored events)
     */
    virtual lu_uint32_t getMaxEventsStorage();

    /**
     * Set the components that support power saving mode.
     */
    void setPowerSavingComps(ISupportPowerSaving** pwerSavings,
                                lu_uint32_t pwerSavingSize);

private:
    /**
     * \brief Object that counts time to activate Power Saving feature.
     *
     * Being a TimedJob, the object is auto-destroyed after it finishes counting.
     */
    class PowerSaveTimer : public TimedJob
    {
    public:
        /**
         * \brief Custom constructor
         *
         * \param statusMgr Reference to StatusManager, NOT IStatusManager since we use a protected method.
         * \param timeout Timer value in ms
         *
         * \return none
         */
        PowerSaveTimer( StatusManager &statusMgr,
                        lu_uint32_t timeout):
                                       TimedJob(timeout, 0),
                                       statusManager(statusMgr)
        {};

        virtual ~PowerSaveTimer() {};

    protected:
        /**
         * \brief User define operation  This method is called by the run public method
         * only if the configured timer is expired
         *
         * \param timePtr Current absolute time
         *
         * \return none
         */
        void job(TimeManager::TimeStr *timePtr)
        {
            LU_UNUSED(timePtr);
            statusManager.setPowerSavingMode(LU_TRUE);
        }

    private:
        StatusManager &statusManager;   //Reference to StatusManager, NOT IStatusManager since we use a protected method.
    };

private:
    /**
     * \brief Set Power Saving Mode
     *
     * Sets the power saving mode from a Shuts down/awake all the RTU's LEDs by handling the MCM's LEDs and also
     * by sending LED shut/awake to the rest of the RTU modules. Shut down LEDs
     * is made after a delay.
     *
     * \param powerMode Power Saving mode. Set to LU_TRUE for shut down LEDs
     */
    void setPowerSavingMode(lu_bool_t powerMode);

private:
    IMCMApplication& mcmApplication;
    Logger& log;

    GeminiDatabase& g3Database;             //Keep a copy of the Gemini 3 Database for storage
    IMemoryManager& memoryManager;          //Keep a copy of the Memory Manager for storage

    lu_uint32_t m_protocolStackDelay_sec;   //Protocol stack initialisation delay

    /* Config File version */
    BasicVersionStr configFileVersion;      //config file version numbers
    std::string configFileTimestamp;        //config file time stamp
    std::string configFileDesc;        		//config file description
    lu_uint32_t configFileCRC;              //Configuration File CRC
    lu_bool_t configFileCRCValid;           //Validity of the CRC of the config file
    lu_bool_t configFilePresent;            //Config file is present in the file system

    /* Service mode */
    SERVICEMODE RTUServiceMode;     //Service mode for RTU (in service/out of service)
    OUTSERVICEREASON RTUServiceModeReason;  //reason for having the RTU out of service

    lu_uint64_t powerSavingDelay;       //Power saving delay in milliseconds
    PowerSaveTimer* powerSaveTimer;     //Power saving delay timer
    std::string siteName;               //RTU site name

    ISupportPowerSaving** pwerSavings; // The components that support power saving.
    lu_uint32_t pwerSavingSize;       // The number of power saving components.

};



#endif // !defined(EA_645294B7_EE43_4123_A6C3_C122D81BB3DD__INCLUDED_)

/*
 *********************** End of file ******************************************
 */

