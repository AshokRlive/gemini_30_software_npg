/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id: MCMDoorSwitch.cpp 5 Sep 2013 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/HMIManager/src/MCMDoorSwitch.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 5 Sep 2013 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   5 Sep 2013    pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MCMDoorSwitch.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */


MCMDoorSwitch::MCMDoorSwitch(IChannel& doorChannel, IStatusManager& statusManager):
                                    doorChannel(doorChannel),
                                    statusManager(statusManager),
                                    log(Logger::getLogger(SUBSYSTEM_ID_MCMMANAGER))
{
    doorChannel.attach(this);
}


MCMDoorSwitch::~MCMDoorSwitch()
{
    doorChannel.detach(this);
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

void MCMDoorSwitch::update(IChannel *subject)
{
    const lu_char_t* FTITLE = "MCMDoorSwitch::update:";
    IOM_ERROR chRet;
    IODataUint8 data;
    IChannel::ValueStr valueRead(data);

    /* Read the channel value */
    chRet = subject->read(valueRead);

    lu_uint8_t doorValue = data;
    lu_bool_t doorClosed = (doorValue == 0)? LU_TRUE : LU_FALSE;
    if( (chRet == IOM_ERROR_NONE) && (valueRead.flags.online == LU_TRUE) )
    {
        if (doorClosed == LU_TRUE)
        {
            log.info("%s Door closed. Starting countdown for Power Saving mode.", FTITLE);
        }
        else
        {
            log.info("%s Door open. Awakening devices.", FTITLE);
        }
        statusManager.setPowerMode(doorClosed);
    }
    else
    {
        log.error("%s error %i reading door switch: %s",
                    FTITLE, chRet, IOM_ERROR_ToSTRING(chRet));
    }
}

/*
 *********************** End of file ******************************************
 */
