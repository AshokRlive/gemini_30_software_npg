/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id: MCMDoorSwitch.h 5 Sep 2013 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/HMIManager/include/MCMDoorSwitch.h $
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MCM door switch handler header module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date: 5 Sep 2013 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   5 Sep 2013    pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MCMDOORSWITCH_H_
#define MCMDOORSWITCH_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "IStatusManager.h"
#include "IChannel.h"
#include "Logger.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
class MCMDoorSwitch : private ChannelObserver
{
public:
    /**
     * \brief custom constructor
     *
     * \param GmoduleManager Reference to the Module Manager
     * \param GstatusManager Reference to the Status Manager
     */
    MCMDoorSwitch(IChannel& doorChannel, IStatusManager& statusManager);
    virtual ~MCMDoorSwitch();

private:
    virtual void update(IChannel *subject);

    IChannel& doorChannel;  //MCM channel used by the door switch
    IStatusManager& statusManager;
    Logger& log;
};

#endif /* MCMDOORSWITCH_H_ */

/*
 *********************** End of file ******************************************
 */
