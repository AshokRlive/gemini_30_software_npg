/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id: IStatusManager.h 31 Aug 2012 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//G3Trunk/G3/RTU/MCM/src/StatusManager/include/IStatusManager.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Interface for the Status Manager
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 31 Aug 2012 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   31 Aug 2012 pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef ISTATUSMANAGER_H__INCLUDED
#define ISTATUSMANAGER_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <string>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "versions.h"
#include "G3ConfigProtocol.h"   //Service Mode/Reason

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/**
 * \brief General error codes
 */
enum MCM_ERROR
{
    MCM_ERROR_NONE = 0,     //no error
    MCM_ERROR_COMMISSION,   //error trying to commission all modules
    MCM_ERROR_LAST
};

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * This is the StatusManager. It is the entry point to the whole startup procedure
 * called from the main() thread, This component starts all the components of the
 * system and keeps the state of several of them.
 */
class IStatusManager
{
public:
    IStatusManager() {};
    virtual ~IStatusManager() {};

    /* TODO: pueyos_a - decide if the MCMApp will be able to do a (soft) factory reset */
    /**
     * \brief Reset the RTU configuration to a factory default.
     *
     * All the online modules are removed from the commission database, and the
     * configuration file is deleted. History of previous module operation is kept.
     *
     * \return Error Code
     */
    virtual MCM_ERROR factoryReset() = 0;

    /**
     * \brief Set the RTU Service Mode.
     *
     * Set the RTU service mode to "In Service"/"Out of Service".
     *
     * \param serviceMode Service mode to be set
     * \param reason Reason for setting the RTU out of service [Optional, default=none]
     */
    virtual void setServiceMode(SERVICEMODE serviceMode, OUTSERVICEREASON reason) = 0;

    /**
     * \brief Get the RTU Service Mode.
     *
     * Get the value of the RTU Service Mode, and the reason of being out of
     * service if applicable.
     *
     * \param serviceMode Service mode to be returned
     * \param reason Reason for the RTU to be out of service to be returned
     */
    virtual void getServiceMode(SERVICEMODE &serviceMode, OUTSERVICEREASON &reason) = 0;

    /**
     * \brief Set Power Saving delay time
     *
     * Sets the delay before entering the Power Saving mode.
     *
     * \param delay Power Saving mode delay.
     */
    virtual void setPowerSavingDelay(const lu_uint64_t delay) = 0;

    /**
     * \brief Set Power Mode
     *
     * Shuts down/awake all the RTU's LEDs by handling the MCM's LEDs and also
     * by sending LED shut/awake to the rest of the RTU modules. Shut down LEDs
     * is made after a delay.
     *
     * \param powerMode Power Saving mode. Set to LU_TRUE for shut down LEDs
     */
    virtual void setPowerMode(lu_bool_t powerMode) = 0;

    /**
     * \brief Store the name of the RTU
     *
     * Store the name of the RTU as it was read from the configuration file.
     *
     * \param siteName Name of the RTU
     */
    virtual void setSiteNameRTU(std::string siteName) = 0;

    /**
     * \brief Returns the name of the RTU
     *
     * Returns a string containing the current name of the RTU.
     */
    virtual std::string getSiteNameRTU() = 0;

    /**
     * \brief Set of functions for get/set the configuration file attributes
     *
     * \param cfgFileTimestamp Time stamp of the configuration, as string
     * \param version Structure containing the config file version
     * \param filePresent LU_TRUE when the configuration file is present and valid
     * \param calculatedCRC CRC of the config file
     * \param validCRC Validity of the CRC associated to the config file
     *
     * \return time stamp/error code
     */
    virtual void setConfigFileTimestamp(std::string cfgFileTimestamp) = 0;
    virtual std::string getConfigFileTimestamp() = 0;
    virtual void setConfigFileDesc(std::string cfgFileDescription) = 0;
    virtual std::string getConfigFileDesc() = 0;
    virtual void setConfigFileVersion(BasicVersionStr version) = 0;
    virtual lu_bool_t getConfigFileVersion(BasicVersionStr *version) = 0;
    virtual void setConfigFilePresence(lu_bool_t filePresent) = 0;
    virtual lu_bool_t getConfigFilePresence() = 0;
    virtual void setConfigFileCRC(lu_uint32_t calculatedCRC, lu_bool_t validCRC = LU_TRUE) = 0;
    virtual lu_bool_t getConfigFileCRC(lu_uint32_t& calculatedCRC) = 0;

    /**
     * \brief Get/set the Protocol Stack initial delay
     *
     * \param protocolStackDelaySec Delay time, in seconds
     *
     * \return Delay time, in seconds
     */
    virtual lu_uint32_t getProtocolStackDelay() const = 0;
    virtual void setProtocolStackDelay(const lu_uint32_t protocolStackDelaySec = 0) = 0;

    /**
     * \brief Obtains the max amount of events that can be stored
     *
     * \return Overall number of events that can be stored (does not count already stored events)
     */
    virtual lu_uint32_t getMaxEventsStorage() = 0;
};

#endif /* ISTATUSMANAGER_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
