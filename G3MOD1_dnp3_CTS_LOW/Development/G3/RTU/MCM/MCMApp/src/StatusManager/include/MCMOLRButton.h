/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id: MCMOLRButton.h 4 Sep 2013 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/HMIManager/include/MCMOLRButton.h $
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MCM front OLR button handler header module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date: 4 Sep 2013 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   4 Sep 2013    pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MCMOLRBUTTON_H_
#define MCMOLRBUTTON_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "GeminiDatabase.h"
#include "IIOModuleManager.h"
#include "IChannel.h"
#include "Logger.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Class to handle the use of the MCM front OLR button
 *
 * This class observes the OLR front button on the MCM and updates the OLR
 * status depending on the action taken by the user.
 * When the button is pressed briefly, it switches from any position to REMOTE
 * or LOCAL. Please note that when the RTU is in OFF position, it always
 * switches to LOCAL.
 * When long pressing the button, it switches automatically to OFF position
 * regardless of whether the user releases the button or not.
 * To achieve that, the object detects the button pressing and start a timer:
 * when the user releases the button, the timer is discarded; but if the user
 * keeps the button pressed, the timer reaches its end and the RTU is switched
 * to OFF position.
 */
class MCMOLRButton : public ChannelObserver
{
public:
    /**
     * \brief custom constructor
     *
     * \param GmoduleManager Reference to the Module Manager
     * \param Gdatabase Reference to the Gemini database
     * \param config OLR button delay configuration
     */
    MCMOLRButton(IIOModuleManager &GmoduleManager,
                 GeminiDatabase &Gdatabase,
                 const IIOModuleManager::OLRButtonConfig config
                 );
    virtual ~MCMOLRButton();

private:
    /**
     * \brief Object that counts time to activate OLR's OFF position in MCM's front button
     *
     * This timer helps counting time to switch to OFF position (long press).
     * Being a TimedJob, the object is auto-destroyed after it finishes counting.
     */
    class CountOLROffTimer : public TimedJob
    {
    public:
        /**
         * \brief Custom constructor
         *
         * \param channelObserver Related OLR channel observer
         * \param counterID Counter ID; It will notify this number when counter is finished
         * \param timeout Timer value in ms
         */
        CountOLROffTimer(MCMOLRButton& channelObserver,
                        lu_uint32_t timeout):
                                       TimedJob(timeout, 0),
                                       channelObserver(channelObserver)
        {};

        virtual ~CountOLROffTimer() {};

    protected:
        /**
         * \brief User define operation  This method is called by the run public method
         * only if the configured timer is expired
         *
         * \param timePtr Current time
         */
        virtual void job(TimeManager::TimeStr *timePtr);

    private:
        MCMOLRButton& channelObserver;
    };

protected:
    friend class CountOLROffTimer;

    /**
     * \brief notify that a channel has been updated
     *
     * The subject is passed as a parameter: the observer can monitor multiple
     * channels at the same time
     *
     * \param subject The changed channel.
     *
     * \return None
     */
    void update(IChannel *subject);

    /**
     * \brief notify this object that the OFF OLR position timer has expired.
     *
     * This is used to notify the object that the timer created at the button
     * pressing action has expired. Since the button can be pressed several
     * times, the object creates 1 timer per button press, and uses only the
     * latest created (identified by the current ID) by ignoring the previous
     * ones.
     */
    void endTimer();

private:
    GeminiDatabase& database;
    IIOModuleManager::OLRButtonConfig config;
    IChannel* channel;  //MCM channel used by the button

    lu_uint8_t buttonPosition;      //last button value reported
    struct timespec btnPressTime;   //time stamp to count when button is pressed
    lu_bool_t countOff;             //States if we are counting time
    CountOLROffTimer* counterJob;   //Counting button press time out job
};





#endif /* MCMOLRBUTTON_H_ */

/*
 *********************** End of file ******************************************
 */
