/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: AlarmStatus.h 24 Apr 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/StatusManager/include/AlarmStatus.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Observer of the Critical/Error/Warning alarms to set the MCM's OK LED
 *       by notifying it to the Monitor.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 24 Apr 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24 Apr 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef ALARMSTATUS_H__INCLUDED
#define ALARMSTATUS_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "IChannel.h"
#include "MonitorProtocol.h"
#include "MonitorProtocolManager.h"
#include "Logger.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Observer of the MCM alarms to set the MCM's OK LED
 *
 * Class to observe the Critical/Error/Warning alarms in order to set the MCM's
 * OK LED by notifying it to the Monitor.
 */
class AlarmStatus: public ChannelObserver
{
public:
    typedef enum
    {
        SEVERITY_CRITICAL,
        SEVERITY_ERROR,
        SEVERITY_WARNING,
        SEVERITY_LAST
    } SEVERITY;
public:
    /**
     * \brief custom constructor
     *
     * \param channelSeverity Array of channels to observe, sorted by AlarmStatus::SEVERITY
     * \param protocolMonitor Reference to the Monitor Protocol Manager
     */
    AlarmStatus(IChannel* channelSeverity[AlarmStatus::SEVERITY_LAST],
                MonitorProtocolManager& protocolMonitor);
    virtual ~AlarmStatus();

private:
    /* == Inherited from ChannelObserver == */
    virtual void update(IChannel *subject);

private:
    IChannel* severityChannel[AlarmStatus::SEVERITY_LAST];    //channels to check for severity grades
    MonitorProtocolManager& monitorProtocol;
    Logger& log;
    Mutex accessLock;           //Mutex to control access over the update
                                // (observer is updated from several channels)
    lu_uint8_t lastSeverity;    //Bit field of severity values
};


#endif /* ALARMSTATUS_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
