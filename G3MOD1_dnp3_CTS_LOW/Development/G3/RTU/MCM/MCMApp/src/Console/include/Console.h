/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM App
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Console - Command Line header module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   16/08/13      wang_p      Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(_G3_MCM_APP_CONSOLE_H_)
#define _G3_MCM_APP_CONSOLE_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "LockingMutex.h"
#include "GeminiDatabase.h"
#include "StatusManager.h"
#include "ModuleManager.h"
#include "IMemoryManager.h"
#include "MonitorProtocol.h"    //APP_EXIT codes
#include "EventLogManager.h"
#include "CommandLineInterpreter.h"
#include <zmq.hpp>

#include "MBMConfigProto.h"
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
typedef std::vector<CommandCLI*> CLICmdList;    //List of commands

/**
 * \brief class to implement help display (list of available commands)
 *
 * This uses a vector for command list that can be set at creation time or
 * afterwards.
 */
class CmdHelp : public CommandCLI
{
public:
    CmdHelp(): CommandCLI("help", "h", "This help list screen", LU_FALSE)
    {}
    CmdHelp(const CLICmdList commandsList):
            CommandCLI("help", "h", "This help list screen", LU_FALSE),
            m_commandList(commandsList)
    {}
    /* Inherited */
    void call(CommandLineInterpreter& cli);

    /**
     * \brief Set the command's list to be displayed by this help
     *
     * \param commandsList List of commands to display.
     */
    void setCmdList(const CLICmdList commandsList)
    {
        //Note that assignment removes previous content of the local vector, but
        //  does not delete the commands from memory.
        m_commandList = commandsList;
    }

private:
    CLICmdList m_commandList;   //List of commands to display.
};


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * Console - Intended for debug / testing use only.
 */
class Console
{
public:
    /**
     * \brief Custom constructor
     *
     * \param gdatabase Reference to the Gemini database
     * \param gstatusManager reference to the Status Manager
     * \param gmoduleManager reference to the Module Manager
     * \param geventLog reference to the Event Log
     *
     * \return None
     */
	Console(//MCMApplication          &mcmApplication,
            GeminiDatabase          &gdatabase,
            IStatusManager          &gstatusManager,
            ModuleManager           &gmoduleManager,
            EventLogManager         &geventLog,
            MonitorProtocolManager  &gMonitorManager,
            IMemoryManager          &gMemoryManager
            );
	~Console();

	/**
     * \brief Start the Command Line Interpreter
     */
	APP_EXIT_CODE start(lu_bool_t internal = LU_FALSE);

    /**
     * \brief Stop the Command Line Interpreter
     *
     * \param exitCode Exit code to tell to the application
     */
    void stop(APP_EXIT_CODE exitCode = APP_EXIT_CODE_RESTART);

private:
    /* TODO: pueyos_a - [MODBUS] use MBDeviceManager-bound types? */
    /* \brief Structure for Modbus interaction */
    struct MBParams
    {
    public:
        lu_uint8_t deviceID;    //Modbus device ID
        lu_uint8_t tries;       //Amount of tries for message sending
        lu_uint16_t startAddr;  //Modbus start address
        lu_uint16_t quantity;   //Quantity of Modbus registers to process
    };

private:
    /**
     * \brief Print an error when trying to print a point
     *
     * \param cli Reference of the Command Line Interpreter used
     * \param errorCode Gemini Database error code
     * \param pointID ID of the point
     */
    void printPointError(CommandLineInterpreter& cli, const GDB_ERROR errorCode, const PointIdStr pointID);

    /**
     * \brief Print to console Virtual Point-related data
     *
     * \param cli Reference of the Command Line Interpreter used
     * \param pointID ID of the point
     */

    void printPointData(CommandLineInterpreter& cli,
                        const PointIdStr pointID);

    /**
     * \brief Print to console all the point-related data of a given Control Logic
     *
     * \param cli Reference of the Command Line Interpreter used
     * \param ctLogicID Control Logic's ID.
     * \param numCLPoints Number of points of this Control Logic.
     */
    void printCLPointData(CommandLineInterpreter& cli,
                            const lu_uint16_t ctLogicID,
                            const lu_uint32_t numCLPoints);

    /**
     * \brief Read a channel value and its related flags
     *
     * The channel value is asked directly to the module.
     *
     * \param cli Reference of the Command Line Interpreter used
     * \param module Reference to the module
     * \param channel Reference of the channel to read
     * \param valueRead Returns here the last stored value and flags of the channel
     * \param valueRead Returns here the raw value and (supposed) flags of the channel
     *
     * \return LU_TRUE when reading was successful
     */
    lu_bool_t readChannelValue( CommandLineInterpreter& cli,
                                IIOModule& module,
                                const IChannel::ChannelIDStr channel,
                                IChannel::ValueStr& valueRead,
                                IChannel::ValueStr& rawValueRead
                                );

    /**
     * \brief Print to console a channel value and its related flags
     *
     * \param cli Reference of the Command Line Interpreter used
     * \param channel Reference of the channel to read
     * \param value Value and flags to print
     * \param rawValue Raw value and flags to print
     */
    void printChannelValue(CommandLineInterpreter& cli,
                            const IChannel::ChannelIDStr channel,
                            IChannel::ValueStr& value,
                            IChannel::ValueStr& rawValue
                            );

    /**
     * \brief Ask the user to choose a module
     *
     * Shows a list of current present modules, and ask the user to select one.
     *
     * \param cli Reference of the Command Line Interpreter used
     *
     * \return Pointer to the module selected, or NULL when cancel/invalid.
     */
    IIOModule* askModule(CommandLineInterpreter& cli);

    /**
     * \brief Ask the user to choose a module and list its alarms
     *
     * Shows a list of current present modules with its alarm status, and ask
     * the user to select one. Then shows the alarm list.
     *
     * \param cli Reference of the Command Line Interpreter used
     *
     * \return Pointer to the module selected, or NULL when cancel/invalid.
     */
    IIOModule* askModuleAlarms(CommandLineInterpreter& cli);

    /**
     * \brief Ask the user to choose a channel type
     *
     * Shows a list of valid (limited) channel types, and ask the user to select one.
     *
     * \param cli Reference of the Command Line Interpreter used
     *
     * \return Channel type selected, or CHANNEL_TYPE_LAST when cancel/invalid
     */
    CHANNEL_TYPE askChannelType(CommandLineInterpreter& cli);

    /**
     * \brief Starts a Control Logic operation
     *
     * \param cli Reference to the Command Line Interpreter
     * \param cLogic ID number of the Control Logic (group number).
     * \param operation Operation to perform (usually open/close).
     * \param local Source of the operation (from Local or Remote position).
     * \param duration Duration of the operation.
     *
     * \return result of the operation: false if user cancels.
     */
    bool controlLogicOperate(CommandLineInterpreter& cli,
                               const lu_uint16_t cLogic,
                               const lu_uint16_t operation,
                               const lu_uint16_t local,
                               const lu_uint32_t duration = 0
                               );

    /**
     * \brief Proceed to write an analogue value
     *
     * \param cli Reference to the Command Line Interpreter
     * \param cLogic Control Logic to operate
     * \param value Analogue value to use in the operation
     * \param local Source of the operation (from Local or Remote position).
     *
     * \return result of the operation: false if user cancels.
     */
    bool writeAnalogOperation(CommandLineInterpreter& cli,
                                      const lu_uint16_t cLogic,
                                      const lu_float32_t value,
                                      const lu_uint16_t local
                                      );

    /**
     * \brief Gets the string associated to Virtual Point's common flags.
     *
     * The string will contain common flags in string format, typically online
     * and restart (uninitialised) flags.
     *
     * \param data Point data info of the point, containing the flags to be used.
     *
     * \return String containing the common flags, for use in a printf-like message.
     */
    const std::string getCommonFlagsString(PointData& data);

    /**
     * \brief Gets input from the user expecting a sub-command, and executes it.
     *
     * This procedure gets the input from the user, expecting a sub-command from
     * a given list. If the command is found, it executes it; otherwise cancels
     * the sub-command wait. Note that main commands are ignored and only
     * sub-commands are accepted.
     *
     * \param title Text to display with the sub-command request to the user.
     * \param cli Reference to the Command Line Interpreter
     * \param subCmdList Sub-command list to check against.
     * \param helper Help object to use for giving help information to the user.
     */
    void checkSubcommand(const lu_char_t* title,
                            CommandLineInterpreter& cli,
                            CLICmdList& subcmdList,
                            CmdHelp& helper);

    /**
     * \brief Asks the user for Modbus-specific basic parameters
     *
     * \param cli Reference to the Command Line Interpreter
     * \param params Where to store the obtained params (not updated if user cancels)
     *
     * \return result of the operation: false if user cancels.
     */
    bool askModbusParams(CommandLineInterpreter& cli,  mbm::devRegInfo& params);

    /**
     * \brief Asks the user for Modbus-specific address-related parameters.
     *
     * Note that it includes the basic paramteres as well.
     *
     * \param cli Reference to the Command Line Interpreter
     * \param params Where to store the obtained params (not updated if user cancels)
     *
     * \return result of the operation: false if user cancels.
     */
    bool askModbusAddrParams(CommandLineInterpreter& cli, mbm::devRegInfo& params);

    /**
     * \brief Asks the user for Modbus-specific read parameters (including the basic ones)
     *
     * \param cli Reference to the Command Line Interpreter
     * \param params Where to store the obtained params (not updated if user cancels)
     *
     * \return result of the operation: false if user cancels.
     */
    bool askModbusReadParams(CommandLineInterpreter& cli, mbm::devRegInfo& params);

    /**
     * \brief Reads several Modbus registers and print out the values
     *
     * This asks the user for Modbus parameters, gets the values directly from
     * Modbus device, and proceeds to print these values out.
     *
     * \param cli Reference to the Command Line Interpreter
     * \param type Type of Modbus register to read.
     */
    mbm::READ_ERR readModbusRegister(CommandLineInterpreter& cli, mbm::REG_TYPE regtype);

    /**
     * \brief Proceeds to write a list of values to a Modbus register
     *
     * This writes the given values directly to a Modbus register.
     *
     * \param cli Reference to the Command Line Interpreter
     * \param type Type of Modbus register.
     * \param params Modbus params to use.
     * \param values List of values to write to the Modbus device.
     *
     * \return result of the operation: false if writing fails.
     */
    mbm::WRITE_ERR writeModbusRegister(CommandLineInterpreter& cli,
                                        const mbm::REG_TYPE regtype,
                                        const mbm::devRegInfo& params,
                                        const std::vector<lu_uint16_t>& values);

    /**
     * \brief Reads the stats of a Modbus device
     *
     * \param params Parameters of the Modbus register to get stats from
     * \param stats Where to store the obtained stats
     *
     * \return Result of the operation: true when successful
     */
    bool readModbusStats(mbm::devRegInfo params, mbm::devicestats& stats);

    /**
     * \brief Prints out a list of values related to Modbus registers.
     *
     * \param cli Reference to the Command Line Interpreter
     * \param values List of values to display.
     */
    void printModbusRegValues(CommandLineInterpreter& cli, std::vector<lu_uint16_t>& values);

    //----------------------------------------------
    /* Command definitions (definitions of command classes) */
    //
    //           Object name            Command string  Alt cmd     Hidden      Help text
    //           -----------            --------------  -------     ------      ---------
    COMMANDCLASS(CLICInfo,              "about",        "info",     LU_FALSE,   "RTU general information");
    COMMANDCLASS(CLICIdentity,          "identity",     "id",       LU_FALSE,   "Current MCM running application");
    COMMANDCLASS(CLICCommission,        "register",     "c",        LU_FALSE,   "Module commissioning (Registration)");
    COMMANDCLASS(CLICModuleList,        "mlist",        "",         LU_FALSE,   "Status list of modules connected.");
    COMMANDCLASS(CLICModuleVersion,     "mversion",     "minfo",    LU_FALSE,   "List of module's Version and revision numbers.");
    COMMANDCLASS(CLICControlLogicList,  "clist",        "",         LU_FALSE,   "List of active Control Logic Blocks.");
    COMMANDCLASS(CLICVPointValue,       "vpoint",       "vp",       LU_FALSE,   "Print Virtual Point value.");
    COMMANDCLASS(CLICAllVPointValue,    "vpointall",    "vpa",      LU_FALSE,   "Print all Virtual Points value.");
    COMMANDCLASS(CLICGroupVPointValue,  "vpointg" ,     "vpg",      LU_FALSE,   "Print all Virtual Points value from a group.");
    COMMANDCLASS(CLICChannelValue,      "channel",      "ch",       LU_FALSE,   "Print Channel value.");
    COMMANDCLASS(CLICAllChannelValue,   "channelall",   "cha",      LU_FALSE,   "Print all Channels values from a module.");
    COMMANDCLASS(CLICModuleAlarms,      "alarms",       "alarm",    LU_FALSE,   "Print all the alarms related to a module.");
    COMMANDCLASS(CLICAckModuleAlarms,   "ackalarms",    "ackalarm", LU_FALSE,   "Acknowledge all the alarms related to a module.");
    COMMANDCLASS(CLICGetLogLevel,       "getloglevel",  "gll",      LU_FALSE,   "List the log level of all subsystems.");
    COMMANDCLASS(CLICSetLogLevel,       "setloglevel",  "sll",      LU_FALSE,   "Set a log level for a specified subsystem.");
    COMMANDCLASS(CLICCANStats,          "canstats",     "can",      LU_FALSE,   "CAN bus statistics.");
    COMMANDCLASS(CLICSetOLR,            "olr",          "",         LU_FALSE,   "[Test] Set Off/Local/Remote status");
    COMMANDCLASS(CLICServiceMode,       "service",      "",         LU_FALSE,   "[Test] Set Service mode.");
    COMMANDCLASS(CLICOperateCL,         "opcl",         "os",       LU_FALSE,   "[Test] Operate a Control Logic (e. g. SwitchGear).");
    COMMANDCLASS(CLICWriteAnalog,       "writeAnalogue","wa",       LU_FALSE,   "[Test] Write analogue value.");
    COMMANDCLASS(CLICFreeze,            "freeze",       "",         LU_FALSE,   "[Test] Freeze and/or clear a counter point.");
    COMMANDCLASS(CLICModuleTout,        "mtimeout",     "",         LU_FALSE,   "[Test] Enable/disable Module timeout manager.");
    COMMANDCLASS(CLICModuleReset,       "modreset",     "",         LU_FALSE,   "[Test] Hard-reset a Module.");
    COMMANDCLASS(CLICActivateConfig,    "activate",     "actcfg",   LU_FALSE,   "Activate an already uploaded RTU configuration.");
    COMMANDCLASS(CLICRestoreConfig,     "restore",      "",         LU_FALSE,   "Restore the RTU configuration backup.");
    COMMANDCLASS(CLICDeleteConfig,      "deleteconfig", "",         LU_FALSE,   "Delete RTU configuration.");
    COMMANDCLASS(CLICUpdater,           "update",       "",         LU_FALSE,   "Switch to Update mode (terminates the Application).");
    COMMANDCLASS(CLICRestart,           "restart",      "",         LU_FALSE,   "Restart RTU.");
    COMMANDCLASS(CLICReboot,            "reboot",       "",         LU_FALSE,   "RTU system reboot.");
    COMMANDCLASS(CLICShutdown,          "shutdown",     "halt",     LU_FALSE,   "RTU system shutdown for disconnection.");
//    COMMANDCLASS(CLICFactoryReset,      "factoryreset", "",         LU_FALSE,   "[Upd] RTU's Factory reset.");
    COMMANDCLASS(CLICBootloader,        "bootloader",   "bl",       LU_FALSE,   "[Test] Set all the Slave Modules in BootLoader mode.");
    COMMANDCLASS(CLICHeartBeat,         "heartbeat",    "hb",       LU_FALSE,   "[Test] Enable/disable Slave Module HeartBeat.");
    COMMANDCLASS(CLICTestEvents,        "testevent",    "",         LU_TRUE,    "[Test] Generate false event test pattern.");
    COMMANDCLASS(CLICWDogStop,          "wdogstop",     "wd",       LU_FALSE,   "[Test] Stop kicking Watchdog.");
    COMMANDCLASS(CLICLabels,            "labels",       "",         LU_TRUE,    "[Test] Show all labels stored.");
    COMMANDCLASS(CLICModbusTest,        "modbus",       "mb",       LU_FALSE,   "[Test] Modbus command access. 'modbus help' for more.");
    COMMANDCLASS(CLICFactoryHAT,        "FactoryHAT",   "",         LU_FALSE,   "Restart program entering factory HAT test mode");

    /* Modbus-specific sub-command objects */
    COMMANDCLASS(MBCLICRead,            "read",         "r",        LU_FALSE,   "Read a value from a modbus register.");
    COMMANDCLASS(MBCLICWrite,           "write",        "w",        LU_FALSE,   "Write a value to a modbus register.");
    COMMANDCLASS(MBCLICStats,           "stat",         "info",     LU_FALSE,   "Modbus stats. Params: deviceID retries");
    //Modbus read sub-commands
    COMMANDCLASS(MBCLICReadCoil,        "coil",         "c",        LU_FALSE,   "Read coil register. Params: deviceID retries start_addr count");
    COMMANDCLASS(MBCLICReadInputReg,    "input",        "i",        LU_FALSE,   "Read Input register. Params: deviceID retries start_addr count");
    COMMANDCLASS(MBCLICReadHoldReg,     "hold",         "hr",       LU_FALSE,   "Read Holding register. Params: deviceID retries start_addr count");
    COMMANDCLASS(MBCLICReadDiscrReg,    "discrete",     "d",        LU_FALSE,   "Read Discrete register. Params: deviceID retries start_addr count");
    //Modbus write sub-commands
    COMMANDCLASS(MBCLICWriteCoil,       "coil",         "c",        LU_FALSE,   "Write coil register. Params: deviceID retries start_addr value(s)");
    COMMANDCLASS(MBCLICWriteHoldReg,    "hold",         "hr",       LU_FALSE,   "Write Holding register. Params: deviceID retries start_addr value(s)");

private:
    static const lu_uint32_t MAX_CMD_AMOUNT = 50;   //max amount of commands used in this Console. Change when needed.
    /* Connected consoles socket values */
    static const lu_uint32_t MAXCLIENTS = 5;        //Max number of simultaneous console clients supported.
    static const lu_uint32_t SOCKETLISTENRETRYTIME_US = 500000; //Retry time for socket retry
    static const lu_uint32_t EXITWAIT_US = 500000;  //Retry time to check for exiting the server
    static const lu_uint32_t SOCKETLISTENTOUT_MS = 100;  //Accept checking timeout

private:
    /* References to other objects */
//    MCMApplication          &mcmApp;
    GeminiDatabase          &database;
    IStatusManager          &statusManager;
    ModuleManager           &moduleManager;
    EventLogManager         &eventLog;
    MonitorProtocolManager  &monitorManager;
    IMemoryManager          &memoryManager;

    Logger& log;

    CommandCLI** cmdList;       //List of supported commands
    lu_uint32_t cmdListSize;    //Size of the list of supported commands

    /* Modbus sub-commands */
    CLICmdList m_mbCmdList; //List of supported modbus sub-commands
    CmdHelp m_mbHelp;       //Help object for modbus sub-commands
    CLICmdList m_mbReadCmdList;     //List of supported modbus reading sub-commands
    CmdHelp m_mbReadHelp;           //Help object for modbus reading sub-commands
    CLICmdList m_mbWriteCmdList;    //List of supported modbus writing sub-commands
    CmdHelp m_mbWriteHelp;          //Help object for modbus writing sub-commands

    APP_EXIT_CODE exitCode;     //Console's intended exit code

    /* Connected consoles */
    MasterMutex clientAccess;   //Mutex to control access to the clients array
    CommandLineInterpreter* clients[MAXCLIENTS];    //Array of connected clients
};

#endif // !defined(_G3_MCM_APP_CONSOLE_H_)

/*
 *********************** End of file ******************************************
 */
