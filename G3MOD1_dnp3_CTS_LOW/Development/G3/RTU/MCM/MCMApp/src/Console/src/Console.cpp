/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Status Manager - Command Line Interpreter module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   09/05/12      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/utsname.h>
#include <sys/sysinfo.h>
#include <limits>
#include <algorithm>
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/un.h>      // AF_UNIX

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "lu_util.hpp"
#include "GlobalDefinitions.h"
#include "bitOperations.h"
#include "RTUInfo.h"

#include "FileTransferManager.h"
#include "LabelPool.h"

//#include "ConfigurationToolHandler.h"
#include "ModuleRegistrationManager.h"
#include "TimeManager.h"
#include "Console.h"

#include "ZmqContext.h"
#include "mbm_helpers.h"
#include "dbg.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/**
 * \brief Adds a command to the main command list
 *
 * \param CLICName Name of the Command object class (an existing CommandCLI class)
 */
#define ADD_CONSOLE_CMD(CLICName)                           \
    cmdList[cmdListSize++] = new Console::CLICName(*this)

/**
 * \brief Adds a command to the specified sub-command list
 *
 * \param list Vector of sub-commands to add this command to.
 * \param CLICName Name of the Command object class (an existing CommandCLI class)
 */
#define ADD_CONSOLE_SUBCMD(list, CLICName)                  \
    list.push_back(new Console::CLICName(*this))

/**
 * \brief Remove and delete commands from the specified sub-command list
 *
 * \param list Vector of sub-commands to clear.
 */
#define REMOVE_SUBCMD_LIST(list)                                              \
    for(CLICmdList::const_iterator it = list.begin(); it != list.end(); ++it) \
    {                                                                         \
        delete (*it);                                                         \
    }


/** Delete an object and set pointer to NULL */
#define DELETE_POINTER(objName, objPtr)\
            if(objPtr != NULL)\
                            {\
                DBG_INFO("C: [Dtor]Delete %s",objName);\
                delete objPtr;\
                objPtr = NULL;\
                            }\
/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/**
 * \brief Asks the user for a binary operation
 *
 * \param cli Reference to the Command Line Interpreter
 * \param operation Where to return the obtained value (not updated if user cancels)
 * \param openMsg Text associated with option 0 [optional]
 * \param closeMsg Text associated with option 1 [optional]
 *
 * \return result of the operation: false if user cancels.
 */
static bool askOperation(CommandLineInterpreter& cli,
                          lu_uint16_t& operation,
                          const lu_char_t* openMsg = "open",
                          const lu_char_t* closeMsg = "close"
                          )
{
    cli.message("\tOperation(0 to %s - 1 to %s): ", openMsg, closeMsg);
    return (cli.askAndCheck(&operation, 0, 1) == LU_TRUE);
}

/**
 * \brief Asks the user for a local or remote operation
 *
 * \param cli Reference to the Command Line Interpreter
 * \param local Where to return the obtained value (not updated if user cancels)
 *
 * \return result of the operation: false if user cancels.
 */
static bool askLocalRemote(CommandLineInterpreter& cli, lu_uint16_t& local)
{
    cli.message("\tLocal(0 remote - 1 local): ");
    return (cli.askAndCheck(&local, 0, 1) == LU_TRUE);
}

/**
 * \brief Asks the user for an analogue value
 *
 * \param cli Reference to the Command Line Interpreter
 * \param result Where to store the float value obtained (not updated if user cancels)
 *
 * \return result of the operation: false if user cancels.
 */
static bool askAnalogValue(CommandLineInterpreter& cli, lu_float32_t* result)
{
    cli.message("\tAnalogue Value: ");

    std::string buf;
    if (cli.input(buf) <= 0)
    {
        return LU_FALSE;
    }
    return sscanf(buf.c_str(), "%f", result) > 0;
}

/**
 * \brief Asks the user for the duration of an operation
 *
 * \param cli Reference to the Command Line Interpreter
 * \param duration Where to return the obtained value (not updated if user cancels)
 *
 * \return result of the operation: false if user cancels.
 */
template <typename T>
static bool askDuration(CommandLineInterpreter& cli, T& duration)
{
    cli.message("\tDuration of the operation - 0 for default: ");
    return (cli.askAndCheck(&duration, 0, std::numeric_limits<T>::max()) == LU_TRUE);
}

/**
 * \brief Prints a message stating that something has been cancelled
 *
 * Note that putting it here reduces the amount of literal strings in the code.
 *
 * \param cli Reference to the Command Line Interpreter
 */
static void printCancelled(CommandLineInterpreter& cli)
{
    cli.message("Cancelled.\n");
}

/**
 * \brief Prints a message stating that something has been done
 *
 * Note that putting it here reduces the amount of literal strings in the code.
 *
 * \param cli Reference to the Command Line Interpreter
 */
static void printDone(CommandLineInterpreter& cli)
{
    cli.message("Done.\n");
}

/**
 * \brief Gets first token form a user input, discarding the rest.
 *
 * Tokens are space-separated.
 *
 * \param cli Reference to the Command Line Interpreter
 * \param token Resulting token.
 */
static void getToken(CommandLineInterpreter& cli, std::string& token)
{
    cli.input(token);
    token = token.substr(0, token.find_first_of(" \n")); //ignore further input
    toLower(token);
}


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

Console::Console(//MCMApplication         &gMCMApplication,
                GeminiDatabase          &gdatabase,
                IStatusManager          &gstatusManager,
                ModuleManager           &gmoduleManager,
                EventLogManager         &geventLog,
                MonitorProtocolManager  &gMonitorManager,
                IMemoryManager          &gMemoryManager
                ) :
//                        mcmApplication(gMCMApplication),
                        database(gdatabase),
                        statusManager(gstatusManager),
                        moduleManager(gmoduleManager),
                        eventLog(geventLog),
                        monitorManager(gMonitorManager),
                        memoryManager(gMemoryManager),
                        log(Logger::getLogger(SUBSYSTEM_ID_CONSOLE)),
                        cmdList(NULL),
                        cmdListSize(0),
                        exitCode(APP_EXIT_CODE_RESTART)    //normal by default
{
    cmdList = new CommandCLI*[MAX_CMD_AMOUNT];
    ADD_CONSOLE_CMD(CLICInfo            );
    ADD_CONSOLE_CMD(CLICIdentity        );
    ADD_CONSOLE_CMD(CLICCommission      );
    ADD_CONSOLE_CMD(CLICModuleList      );
    ADD_CONSOLE_CMD(CLICModuleVersion   );
    ADD_CONSOLE_CMD(CLICControlLogicList);
    ADD_CONSOLE_CMD(CLICVPointValue     );
    ADD_CONSOLE_CMD(CLICAllVPointValue  );
    ADD_CONSOLE_CMD(CLICGroupVPointValue);
    ADD_CONSOLE_CMD(CLICChannelValue    );
    ADD_CONSOLE_CMD(CLICAllChannelValue );
    ADD_CONSOLE_CMD(CLICModuleAlarms    );
    ADD_CONSOLE_CMD(CLICAckModuleAlarms );
    ADD_CONSOLE_CMD(CLICGetLogLevel     );
    ADD_CONSOLE_CMD(CLICSetLogLevel     );
    ADD_CONSOLE_CMD(CLICCANStats        );
    ADD_CONSOLE_CMD(CLICUpdater         );
    ADD_CONSOLE_CMD(CLICRestart         );
    ADD_CONSOLE_CMD(CLICReboot          );
    ADD_CONSOLE_CMD(CLICShutdown        );
    ADD_CONSOLE_CMD(CLICActivateConfig  );
    ADD_CONSOLE_CMD(CLICRestoreConfig   );
    ADD_CONSOLE_CMD(CLICDeleteConfig    );
    ADD_CONSOLE_CMD(CLICSetOLR          );
    ADD_CONSOLE_CMD(CLICServiceMode     );
    ADD_CONSOLE_CMD(CLICOperateCL       );
    ADD_CONSOLE_CMD(CLICWriteAnalog     );
    ADD_CONSOLE_CMD(CLICFreeze          );
    ADD_CONSOLE_CMD(CLICBootloader      );
    ADD_CONSOLE_CMD(CLICModuleReset     );
    ADD_CONSOLE_CMD(CLICModuleTout      );
    ADD_CONSOLE_CMD(CLICHeartBeat       );
    ADD_CONSOLE_CMD(CLICTestEvents      );
    ADD_CONSOLE_CMD(CLICWDogStop        );
    ADD_CONSOLE_CMD(CLICLabels          );
    ADD_CONSOLE_CMD(CLICModbusTest      );
    ADD_CONSOLE_CMD(CLICFactoryHAT      );
//    ADD_CONSOLE_CMD(CLICFactoryReset    );

    /* Initialise Modbus sub-command list */
    ADD_CONSOLE_SUBCMD(m_mbCmdList, MBCLICRead);
    ADD_CONSOLE_SUBCMD(m_mbCmdList, MBCLICWrite);
    ADD_CONSOLE_SUBCMD(m_mbCmdList, MBCLICStats);

    ADD_CONSOLE_SUBCMD(m_mbReadCmdList, MBCLICReadCoil);
    ADD_CONSOLE_SUBCMD(m_mbReadCmdList, MBCLICReadInputReg);
    ADD_CONSOLE_SUBCMD(m_mbReadCmdList, MBCLICReadHoldReg);
    ADD_CONSOLE_SUBCMD(m_mbReadCmdList, MBCLICReadDiscrReg);

    ADD_CONSOLE_SUBCMD(m_mbWriteCmdList, MBCLICWriteCoil);
    ADD_CONSOLE_SUBCMD(m_mbWriteCmdList, MBCLICWriteHoldReg);

    m_mbHelp.setCmdList(m_mbCmdList);
    m_mbReadHelp.setCmdList(m_mbReadCmdList);
    m_mbWriteHelp.setCmdList(m_mbWriteCmdList);


    /* Initialise clients list */
    MasterLockingMutex mMutex(clientAccess, LU_TRUE);
    for (lu_uint32_t i = 0; i < MAXCLIENTS; ++i)
    {
        clients[i] = NULL;
    }
}

Console::~Console()
{
    stop();
    if(*cmdList != NULL)
    {
        //delete all instances of command handlers
        for(lu_uint32_t index = 0; index < cmdListSize; ++index)
        {
            delete (cmdList[index]);
        }
        delete cmdList;
    }
    /* delete all instances of sub-command handlers */
    REMOVE_SUBCMD_LIST(m_mbWriteCmdList);
    REMOVE_SUBCMD_LIST(m_mbReadCmdList);
    REMOVE_SUBCMD_LIST(m_mbCmdList);
}

void Console::stop(APP_EXIT_CODE newExitCode)
{
    //remove connected clients
    MasterLockingMutex mMutex(clientAccess, LU_TRUE);
    for (lu_uint32_t i = 0; i < MAXCLIENTS; ++i)
    {
        if(clients[i] != NULL)
        {
            clients[i]->stop();
            clients[i] = NULL;
        }
    }
    exitCode = newExitCode;
}

APP_EXIT_CODE Console::start(lu_bool_t internalConsole)
{
    lu_int32_t sockFd;      //Listening socket
    lu_int32_t accSockFd;   //Accepted socket
    lu_int32_t optVal = 1;
    struct sockaddr_un servAddr;

    exitCode = APP_EXIT_CODE_LAST;

    if(internalConsole == LU_TRUE)
    {
        /* Internal console: a thread that never stops by itself */
        clients[0] = new CommandLineInterpreter(cmdList, cmdListSize, fileno(stdin), fileno(stdout));
        THREAD_ERR resThr = clients[0]->start();
        if(resThr != THREAD_ERR_NONE)
        {
            log.error("Error %i starting internal console thread.", resThr);
        }
    }

    /* TODO: pueyos_a - Deal properly with socket initialisation errors: retry in suitable errors/situations */

    // Create a local socket
    unlink(CONSOLESOCKETNAME);
    sockFd = socket(AF_UNIX, SOCK_STREAM, 0);
    if(sockFd < 0)
    {
        log.error("Unable to create socket for Console: error %i (%s).", errno, strerror(errno));

        /* If no socket available, we don't want to stop the app */
        while(exitCode == APP_EXIT_CODE_LAST)
        {
            lucy_usleep(EXITWAIT_US);
        }
        return exitCode;
    }

    memset(&servAddr, 0, sizeof(struct sockaddr_un));
    servAddr.sun_family = AF_UNIX;
    strncpy(servAddr.sun_path, CONSOLESOCKETNAME, sizeof(servAddr.sun_path)-1);

    // Reuse address
    setsockopt(sockFd, SOL_SOCKET, SO_REUSEADDR, &optVal, sizeof(optVal));

    // Set to non-blocking mode
    lu_int32_t sockFlags = fcntl(sockFd, F_GETFL);
    fcntl(sockFd, F_SETFL, sockFlags | O_NONBLOCK);

    if (bind(sockFd, (struct sockaddr*)&servAddr, sizeof(servAddr)) < 0)
    {
        close(sockFd);
        log.error("Error %i binding to the Console socket (%s).", errno, strerror(errno));

        /* If unable to bind to socket, we don't want to stop the app */
        while(exitCode == APP_EXIT_CODE_LAST)
        {
            lucy_usleep(EXITWAIT_US);
        }
        return exitCode;
    }

    if( listen(sockFd, 0) < 0 )
    {
        close(sockFd);
        log.error("Error %i setting the Console socket in listening mode (%s).",
                    errno, strerror(errno)
                    );

        /* If unable to listen socket, we don't want to stop the app */
        while(exitCode == APP_EXIT_CODE_LAST)
        {
            lucy_usleep(EXITWAIT_US);
        }
        return exitCode;
    }

    fd_set             readfds;
    const struct timeval ACCEPTTOUT = ms_to_timeval(SOCKETLISTENTOUT_MS);
    struct timeval     tout;
    lu_int32_t         ret;

    //Variables to control re-printing of accept errors:
    const lu_uint32_t ACCEPTMSGREPRINT = (3600000/SOCKETLISTENTOUT_MS); //3600 secs = 1 hour
    lu_uint32_t acceptfail = 0;

    /* accept connections until exit request */
    while(exitCode == APP_EXIT_CODE_LAST)
    {
        FD_ZERO(&readfds);
        FD_SET(sockFd, &readfds);
        tout = ACCEPTTOUT;
        ret = select(sockFd + 1, &readfds, NULL, NULL, &tout);
        if (ret > 0)
        {
            if (FD_ISSET(sockFd, &readfds) > 0)
            {
                /* Check if there is any client slot available */
                MasterLockingMutex mMutex(clientAccess);
                lu_uint32_t pos;
                for (pos = 0; pos < MAXCLIENTS; ++pos)
                {
                    if(clients[pos] == NULL)
                    {
                        break;  //found empty slot
                    }
                }
                if(pos == MAXCLIENTS)
                {
                    //no empty slots found: refuse connection
                    accSockFd = accept(sockFd, NULL, NULL);
                    if(accSockFd < 0)
                    {
                        //Please note this is not an important error
                        log.info("Error refusing Console Connection %i (%s).",
                                    errno, strerror(errno));
                    }
                    else
                    {
                        log.info("Console Connection refused: no client slots available.");
                        close(accSockFd);
                    }
                }
                else
                {
                    if ((accSockFd = accept(sockFd, NULL, NULL)) < 0)
                    {
                        /* Accept failure: ignore retries and socket blocking notification */
                        if ((errno != EAGAIN) && (errno != EWOULDBLOCK))
                        {
                            if(acceptfail++ == 0)
                            {
                                log.error("Error %i when accepting Console connection (%s).",
                                            errno, strerror(errno));
                            }
                            if(acceptfail > ACCEPTMSGREPRINT)
                            {
                                acceptfail = 0; //next time, re-issue the error message
                            }
                        }
                    }
                    else
                    {
                        /* Accepted connection: associate a CLI */
                        clients[pos] = new CommandLineInterpreter(cmdList, cmdListSize, accSockFd, accSockFd);
                        THREAD_ERR resThr = clients[pos]->start();
                        if(resThr != THREAD_ERR_NONE)
                        {
                            log.error("Error %i starting console client thread.", resThr);
                        }
                    }
                }
            }
        }//endif of select() output

        /* Review sockets to see if any of them died */
        MasterLockingMutex mMutex(clientAccess);
        for (lu_uint32_t i = 0; i < MAXCLIENTS; ++i)
        {
            if(clients[i] != NULL)
            {
                if(clients[i]->isRunning() == LU_FALSE)
                {
                    //Client died: get rid of it
                    accSockFd = clients[i]->getInConsole();
                    delete clients[i];
                    clients[i] = NULL;
                    close(accSockFd);   //close associated input/output socket
                }
            }
        }
    }
    close(sockFd);  //close listening socket
    return exitCode;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/* =============== CLI Command call implementations ============== */
void Console::CLICInfo::call(CommandLineInterpreter& cli)
{
    cli.message("-----\n");

    /* Get MCM firmware version string values */
    cli.message("%s\n"
                "  SDP version: %s\n"
                "  OS version: %s\n"
                "  Filesystem version: %s\n",
                    getMCMAppVersionString().c_str(),
                    trim_all(getSDPVersion()).c_str(),
                    getOSVersion().c_str(),
                    getRootFSVersion().c_str()
                    );

    /* Get 3 main APIs revision numbers */
    cli.message("Main APIs revision numbers: \n"
                    "\tModule System API version: %d.%d\n"
                    "\tSchema API version: %d.%d\n"
                    "\tG3 Configuration Tool Protocol API version: %d.%d\n",
                    MODULE_SYSTEM_API_VER_MAJOR,
                    MODULE_SYSTEM_API_VER_MINOR,
                    SCHEMA_MAJOR,
                    SCHEMA_MINOR,
                    G3_CONFIG_SYSTEM_API_MAJOR,
                    G3_CONFIG_SYSTEM_API_MINOR
                    );
#ifdef IEC61131
    cli.message("IEC-61131: ON\n");
#else
    cli.message("IEC-61131: OFF\n");
#endif
#ifdef IEC61499
    cli.message("IEC-61499: ON\n");
#else
    cli.message("IEC-61499: OFF\n");
#endif

    /* CPU serial num */
    cli.message("CPU serial Number: [%016llX]\n", getCPUSerialNum());

    cli.message("-----\n");

    /* Get System and Application Uptime */
    TimeManager& timeManager = TimeManager::getInstance();
    TimeManager::TimeStr currentTime = timeManager.now();
    cli.message("Time (Format: DD/MM/YYYY HH:MM:SS.mS): %s %s\n",
                    currentTime.toString(false, //without synch
                                        TimeManager::TimeStr::DATEFORMAT_LITTLEENDIAN
                                        ).c_str(),
                    timeManager.getPOSIXTimezoneName()
                    );

    lu_uint64_t uptime_sec;
    uptimeDaysStr uptime;
    uptime_sec = getSystemUptime();
    uptime.convertSeconds(uptime_sec);
    cli.message("Op. System  uptime: %llu seconds (Approx. %s)\n",
                    uptime_sec,
                    uptime.toString().c_str()
                    );

    uptime_sec = timeManager.getAppRunningTime_ms() / 1000;
    SYNC_SOURCE source;
    lu_uint64_t synctime_ms;
    lu_bool_t isSync = timeManager.getSyncStatus(&source, &synctime_ms);
    uptime.convertSeconds(uptime_sec);
    uptimeDaysStr synctime;
    synctime.convertSeconds(synctime_ms / 1000);
    cli.message("Application uptime: %llu seconds (Approx. %s)\n"
                "Time is%s synchronised; latest from %s about %llu seconds ago (%s).\n",
                uptime_sec,
                uptime.toString().c_str(),
                (isSync == LU_FALSE)? " not" : "",
                SYNC_SOURCE_ToSTRING(source),
                (synctime_ms / 1000),
                synctime.toString().c_str()
                );

    cli.message("-----\n");

    /* Get Event storage info */
    cli.message("Maximum number of events: %d\n", console.statusManager.getMaxEventsStorage());
    cli.message("-----\n");

    /* Get Configuration File info */
    if(console.statusManager.getConfigFilePresence() == LU_FALSE)
    {
        cli.message("Configuration file not present.\n");
    }
    else
    {
        BasicVersionStr cfgFileRev;
        console.statusManager.getConfigFileVersion(&cfgFileRev);
        cli.message("Configuration file information:\n"
                    "\tTime stamp: %s\n"
                    "\tRevision: %d.%d\n",
                    console.statusManager.getConfigFileTimestamp().c_str(),
                    cfgFileRev.major, cfgFileRev.minor
                    );
        /* Get file CRC */
        lu_uint32_t cfgFileCRC;
        if(console.statusManager.getConfigFileCRC(cfgFileCRC) == LU_FALSE)
        {
            cli.message("\tInvalid CRC of Configuration file.\n");
        }
        else
        {
            cli.message("\tCRC: 0x%08x\n", cfgFileCRC);
        }
    }

    /* Get net info */
    netConfigVector netCfg = getNetConfig();
    if(netCfg.size() > 0)
    {
        cli.message("Network configurations:\n");
        for (lu_uint32_t i = 0; i < netCfg.size(); ++i)
        {
            cli.message("\tDevice: %s\n"
                        "\t\tIP: %s\n"
                        "\t\tMask: %s\n"
                        "\t\tGateway: %s\n"
                        "\t\tBroadcast: %s\n"
                        "\t\tMAC: %s\n",
                        netCfg[i].devName.c_str(),
                        netCfg[i].toString(netCfg[i].ip).c_str(),
                        netCfg[i].toString(netCfg[i].mask).c_str(),
                        netCfg[i].toString(netCfg[i].gateway).c_str(),
                        netCfg[i].toString(netCfg[i].broadcast).c_str(),
                        netCfg[i].toString(netCfg[i].mac).c_str()
                       );
        }
    }
    cli.message("-----\n");
}


void Console::CLICIdentity::call(CommandLineInterpreter& cli)
{
    cli.message(" Running %s", getMCMAppVersionString().c_str());
}


void Console::CLICCommission::call(CommandLineInterpreter& cli)
{
    MRDB_ERROR res;
    cli.message("Start registering modules (commissioning)...\n");
    res = ModuleRegistrationManager::getInstance()->commissionModules(console.moduleManager);
    switch(res)
    {
    case MRDB_ERROR_NONE:
        cli.message("Modules registered (Commissioning finished).\n");
        return;
    case MRDB_ERROR_INITIALIZED:
        cli.message("Error: Module Manager not initialised.\n");
        return;
    default:
        cli.message("Error registering modules\n");
        break;
    }
}


void Console::CLICSetOLR::call(CommandLineInterpreter& cli)
{
    lu_uint32_t olr;
    OLR_STATE statusOLR;
    if(console.database.getLocalRemote(statusOLR) != GDB_ERROR_NONE)
    {
        statusOLR = OLR_STATE_LAST; //invalid
    }
    cli.message("\tCurrent Off/Local/Remote status: %s (%d)\n",
                (statusOLR == OLR_STATE_LAST)? "Unknown" : OLR_STATE_ToSTRING(statusOLR),
                statusOLR
                );
    cli.message("\tSet Off/Local/Remote (Off(%d), Local(%d), Remote(%d); (9) to cancel): ",
                OLR_STATE_OFF, OLR_STATE_LOCAL, OLR_STATE_REMOTE
                );
    OLR_STATE minVal = std::min(OLR_STATE_OFF, OLR_STATE_LOCAL);
    minVal = std::min(minVal, OLR_STATE_REMOTE);
    OLR_STATE maxVal = std::max(OLR_STATE_OFF, OLR_STATE_LOCAL);
    maxVal = std::max(maxVal, OLR_STATE_REMOTE);
    if(cli.askAndCheck(&olr, minVal, maxVal) == LU_TRUE)
    {
        GDB_ERROR ret = console.database.setLocalRemote(static_cast<OLR_STATE>(olr), OLR_SOURCE_CLI);
        if(ret == GDB_ERROR_NONE)
        {
            /* Read back status to check change */
            lucy_usleep(1000000);    //Leave time for inputs to settle down
            OLR_STATE newStatusOLR = OLR_STATE_LAST;
            console.database.getLocalRemote(newStatusOLR);
            cli.message("\nstatus %s %s (%d)\n",
                        (newStatusOLR != statusOLR)? "set correctly to" : "is",
                        (newStatusOLR == OLR_STATE_LAST)? "Unknown" : OLR_STATE_ToSTRING(newStatusOLR),
                        newStatusOLR
                        );
        }
        else
        {
            cli.message("Operation error: %s\n", GDB_ERROR_ToSTRING(ret));
        }
    }
    else
    {
        printCancelled(cli);
    }
}


void Console::CLICServiceMode::call(CommandLineInterpreter& cli)
{
    lu_uint32_t newMode;
    lu_uint32_t newReason;
    SERVICEMODE mode;
    OUTSERVICEREASON reason;
    console.statusManager.getServiceMode(mode, reason);
    cli.message("Current service mode is %s (%i)%s%s.\n"
                    "Set mode (%i to put RTU out of service, %i to put RTU back in service, 0 to cancel): ",
                    SERVICEMODE_ToSTRING(mode),
                    mode,
                    (mode != SERVICEMODE_OUTSERVICE)? "" : " Reason: ",
                    (mode != SERVICEMODE_OUTSERVICE)? "" : OUTSERVICEREASON_ToSTRING(reason),
                    SERVICEMODE_OUTSERVICE,
                    SERVICEMODE_INSERVICE
                    );
    if(cli.askAndCheck(&newMode, 1, 2) == LU_TRUE)
    {
        switch(newMode)
        {
        case SERVICEMODE_INSERVICE:
            break;
        case SERVICEMODE_OUTSERVICE:
            cli.message("Please state the reason to put this RTU out of service using the code number:\n");
            for (lu_uint32_t i = 0; i < OUTSERVICEREASON_LAST; ++i)
            {
                cli.message("\t%i: %s.\n", i, OUTSERVICEREASON_ToSTRING(i));
            }
            cli.message("Reason code (Last one was %i):", reason);
            if(cli.askAndCheck(&newReason, 0, (OUTSERVICEREASON_LAST - 1)) != LU_TRUE)
            {
                cli.message("Invalid reason. Operation cancelled.\n");
                newMode = 0;
            }
            break;
        default:
            newMode = 0;
            break;
        }
        if(newMode != 0)
        {
            mode = static_cast<SERVICEMODE>(newMode);
            reason = static_cast<OUTSERVICEREASON>(newReason);
            console.statusManager.setServiceMode(mode, reason);
        }
    }
    else
    {
        printCancelled(cli);
    }
}


void Console::CLICModuleList::call(CommandLineInterpreter& cli)
{
    std::vector<IIOModule*> moduleList;
    moduleList = console.moduleManager.getAllModules();
    cli.message("Modules found: %i\n", moduleList.size());
    IOModuleInfoStr moduleInfo;
    for(std::vector<IIOModule*>::iterator it=moduleList.begin(); it!=moduleList.end(); ++it)
    {
        if((*it)->getInfo(moduleInfo) == IOM_ERROR_NONE)
        {
            cli.message("  %-4s [%i:%i]: %s - S/N: %s - FSM:%d, Status: %d, Alarms: %i\n",
                    (*it)->getName(),
                    moduleInfo.mid.type, moduleInfo.mid.id,    //CAN ID 0:0 denomination format
                    moduleInfo.status.toString().c_str(),
                    moduleInfo.moduleUID.toString().c_str(),
                    moduleInfo.fsm,
                    moduleInfo.hBeatInfo.status,
                    moduleInfo.hBeatInfo.error
                    );
        }
    }
    cli.message("\n"
                "Legend: OK=run|R=registered|C=configured|P=present|D=detected|S=disabled.\n"
                "\n");
}


void Console::CLICModuleVersion::call(CommandLineInterpreter& cli)
{
    std::vector<IIOModule*> moduleList;
    moduleList = console.moduleManager.getAllModules();
    cli.message("Modules found: %i\n", moduleList.size());
    IOModuleInfoStr moduleInfo;
    for(std::vector<IIOModule*>::iterator it=moduleList.begin(); it!=moduleList.end(); ++it)
    {
        if((*it)->getInfo(moduleInfo) == IOM_ERROR_NONE)
        {
            cli.message(
                    "  %-4s - [Firmware] version: %c-%u.%u.%u SVN%lu\t - Feature rev: %u.%u -- "
                    "System API: %u.%u \n         [BootLoader] API: %u.%u - FW: %c-%u.%u.%u SVN%u\n",
                    (*it)->getName(),
                    getReleaseTypeSymbol(moduleInfo.mInfo.application.software.relType),
                    moduleInfo.mInfo.application.software.version.major,
                    moduleInfo.mInfo.application.software.version.minor,
                    moduleInfo.mInfo.application.software.patch,
                    moduleInfo.mInfo.svnRevisionApp,
                    moduleInfo.mInfo.featureRevision.major,
                    moduleInfo.mInfo.featureRevision.minor,
                    moduleInfo.mInfo.application.systemAPI.major,
                    moduleInfo.mInfo.application.systemAPI.minor,
                    moduleInfo.mInfo.bootloader.systemAPI.major,
                    moduleInfo.mInfo.bootloader.systemAPI.minor,
                    getReleaseTypeSymbol(moduleInfo.mInfo.bootloader.software.relType),
                    moduleInfo.mInfo.bootloader.software.version.major,
                    moduleInfo.mInfo.bootloader.software.version.minor,
                    moduleInfo.mInfo.bootloader.software.patch,
                    moduleInfo.mInfo.svnRevisionBoot
                    );
        }
    }
}


void Console::CLICControlLogicList::call(CommandLineInterpreter& cli)
{
    lu_uint32_t cLogicNum = console.database.getCLogicNum();

    cli.message("Control logic blocks %i:\n", cLogicNum);
    for(lu_uint32_t i = 0; i <= cLogicNum; ++i)
    {
        cli.message("\tIndex %i %s\n", i, console.database.getTypeName(i));
    }
}


void Console::CLICVPointValue::call(CommandLineInterpreter& cli)
{
    PointIdStr pointID;
    lu_bool_t result = LU_FALSE;

    cli.message("\tVirtual Point Group: ");
    if(cli.askAndCheck(&pointID.group, 0, std::numeric_limits<lu_uint16_t>::max()) == LU_TRUE)
    {
        cli.message("\tVirtual Point ID: ");
        if(cli.askAndCheck(&pointID.ID, 0, std::numeric_limits<lu_uint16_t>::max()) == LU_TRUE)
        {
            cli.message("\n");
            if(console.database.getPointType(pointID) == POINT_TYPE_INVALID)
            {
                console.printPointError(cli, GDB_ERROR_NOPOINT, pointID);
            }
            else
            {
                console.printPointData(cli, pointID);
            }
            result = LU_TRUE;
        }
    }
    if(result == LU_FALSE)
    {
        printCancelled(cli);
    }
}


void Console::CLICAllVPointValue::call(CommandLineInterpreter& cli)
{
    PointIdStr pointID;
    lu_uint32_t vPointNum = console.database.getVPointNum(0);
    lu_uint32_t cLogicNum = console.database.getCLogicNum();
    pointID.group = 0;
    /* get all Virtual Points (always numbered as group 0) */
    for(lu_uint32_t i = 0; i < vPointNum; ++i)
    {
        pointID.ID = i;
        console.printPointData(cli, pointID);
    }
    /* get all Control Logic Points (always numbered from group 1) */
    for(lu_uint32_t i = 1; i <= cLogicNum; ++i)
    {
        console.printCLPointData(cli, i, console.database.getVPointNum(i));
    }
}


void Console::CLICGroupVPointValue::call(CommandLineInterpreter& cli)
{
    PointIdStr pointID;
    lu_uint32_t cLogicNum = console.database.getCLogicNum();
    cli.message("\tVirtual Point Groups available:\n");
        std::stringstream options;  //Option number list
        for(lu_uint32_t i = 0; i <= cLogicNum; ++i)
        {
            cli.message("\t%u: %s\n", i, console.database.getTypeName(i));
            options << (lu_uint32_t)i << ", ";
        }
        cli.message("\t%u: -Cancel-\n", cLogicNum + 1);
        cli.message("Virtual Point Group: (");
        cli.message(options.str());
        cli.message("%u): ", cLogicNum + 1);

    if(cli.askAndCheck(&pointID.group, 0, cLogicNum) == LU_TRUE)
    {
        console.printCLPointData(cli, pointID.group, console.database.getVPointNum(pointID.group));
    }
    else
    {
        printCancelled(cli);
    }
}


void Console::CLICChannelValue::call(CommandLineInterpreter& cli)
{
    lu_bool_t result = LU_FALSE;

    IIOModule* module = console.askModule(cli);
    if(module != NULL)
    {
        IChannel::ChannelIDStr channelID;
        channelID.type = console.askChannelType(cli);
        if(channelID.type != CHANNEL_TYPE_LAST)
        {
            cli.message("Channel ID: ");
            if(cli.askAndCheck(&channelID.id, 0, std::numeric_limits<lu_uint16_t>::max()) == LU_TRUE)
            {
                cli.message("\n");
                IODataFloat32 value;    //Please note that the value is automatically converted later
                IChannel::ValueStr valueRead(value);
                IODataFloat32 rawValue;    //Please note that the value is automatically converted later
                IChannel::ValueStr rawValueRead(rawValue);
                if(console.readChannelValue(cli, *module, channelID, valueRead, rawValueRead) == LU_TRUE)
                {
                    console.printChannelValue(cli, channelID,valueRead, rawValueRead);
                    result = LU_TRUE;
                }
            }
        }
    }
    if(result == LU_FALSE)
    {
        printCancelled(cli);
    }
}


void Console::CLICAllChannelValue::call(CommandLineInterpreter& cli)
{
    lu_bool_t result = LU_FALSE;
    IChannel::ChannelIDStr channelID;
    IODataFloat32 data;    //Please note that the value is automatically converted
    IChannel::ValueStr value(data);
    IODataFloat32 rawData;    //Please note that the value is automatically converted
    IChannel::ValueStr rawValue(rawData);

    IIOModule* module = console.askModule(cli);

    if(module != NULL)
    {
        result = LU_TRUE;
        lu_bool_t printed = LU_FALSE;   //Printed at least one channel

        /* Print channels of each type */
        for (lu_int32_t i = 0; i < CHANNEL_TYPE_LAST; ++i)
        {
            channelID.type = static_cast<CHANNEL_TYPE>(i);
            /* Read all available channels */
            channelID.id = 0;
            while(module->getChannel(channelID) != NULL)
            {
                if(console.readChannelValue(cli, *module, channelID, value, rawValue) == LU_TRUE)
                {
                    console.printChannelValue(cli, channelID, value, rawValue);
                    printed = LU_TRUE;
                }
                ++(channelID.id);
            }
        }
        if(printed == LU_FALSE)
        {
            cli.message("No active channels found.\n");
        }
    }
    if(result == LU_FALSE)
    {
        printCancelled(cli);
    }
}


void Console::CLICModuleAlarms::call(CommandLineInterpreter& cli)
{
    console.askModuleAlarms(cli);
}


void Console::CLICAckModuleAlarms::call(CommandLineInterpreter& cli)
{
    IIOModule* module = console.askModuleAlarms(cli);
    if(module != NULL)
    {
        if(cli.waitYesNo("    Do you want to acknowledge all of these alarms") == cli.YESNORESULT_YES)
        {
            module->ackAlarms();
            cli.message("Alarms acknowledged.\n");
            return;
        }
    }
}


void Console::CLICGetLogLevel::call(CommandLineInterpreter& cli)
{
    cli.performGettingLogLevel();
}


void Console::CLICSetLogLevel::call(CommandLineInterpreter& cli)
{
    lu_bool_t success = cli.performSettingLogLevel();
    if(success == LU_TRUE)
    {
        cli.message("Setting Log Level: Done\n");
    }
    //Note: storage in memory is done by an status observer attached to Logger itself
}


void Console::CLICCANStats::call(CommandLineInterpreter& cli)
{
    IIOModule* module = console.askModule(cli);
    if(module != NULL)
    {
        IOM_ERROR res = IOM_ERROR_PARAM;
        typedef std::vector<MCMIOModule::StatsCANStr> CANStatsList;
        CANStatsList infoList;
        if(module->isMCM() == LU_TRUE)
        {
            MCMIOModule *mcmModule = dynamic_cast<MCMIOModule*>(module);
            if(mcmModule != NULL)
            {
                /* MCM Module does report CAN stats to Config Tool having 2 CAN ports */
                res = mcmModule->getCANStats(infoList);
            }
        }
        else
        {
            CANIOModule *canModule = dynamic_cast<CANIOModule*>(module);
            if(canModule != NULL)
            {
                CanStatsStr info;
                res = canModule->getCANStats(info);
                MCMIOModule::StatsCANStr entry;
                entry = info;
                if(res == IOM_ERROR_NONE)
                {
                    entry.validateAll();   //Sets all entries coming from CAN module as valid ones
                    infoList.push_back(entry);   //Compose 1-element vector for reporting
                }
            }
        }

        if(res != IOM_ERROR_NONE)
        {
            cli.message("Error getting CAN stats of module %s. Error %i: %s",
                        module->getName(), res, IOM_ERROR_ToSTRING(res));
        }
        else
        {
            lu_uint32_t canID = 0;
            for(CANStatsList::iterator it = infoList.begin(), end = infoList.end(); it != end; ++it)
            {
                cli.message("CAN%u:\n", canID++);
                cli.message((*it).toString());
            }
        }
    }
    else
    {
        cli.message("Invalid module. Cancelled.\n");
    }

}


void Console::CLICWriteAnalog::call(CommandLineInterpreter& cli)
{

    bool succeeded = false;
    CONTROL_LOGIC_TYPE cLogicType;
    lu_uint32_t cLogicGroup;
    lu_uint32_t cLogicNum;
    lu_uint16_t local = 0;


    cLogicNum = console.database.getCLogicNum();

    /* Print list of CL available */
    cli.message("Control Logic blocks available:\n");
      std::stringstream options;  //Option number list
      for(lu_uint32_t i = 1; i <= cLogicNum; ++i)
      {
          cLogicType = console.database.getType(i);
          if(cLogicType == CONTROL_LOGIC_TYPE_ANALOGCTRLPOINT)
          {
              cli.message("\t%u: %s\n", i, console.database.getTypeName(i));
              options << (lu_uint32_t)i << ", ";
          }
      }
      cli.message("\t0: -Cancel-\n");
      cli.message("Select Control Logic block: (");
      cli.message(options.str());
      cli.message("0): ");

    /* Select control logic group */
    cli.message("Select Control Logic block: ");
    if(cli.askAndCheck(&cLogicGroup, 1, cLogicNum) == LU_TRUE)
    {
        cli.message("You selected logic group: %d ", cLogicGroup);
        if(console.database.getType(cLogicGroup) == CONTROL_LOGIC_TYPE_ANALOGCTRLPOINT)
        {
            if(askLocalRemote(cli, local) )
            {
                lu_float32_t value;
                askAnalogValue(cli, &value);
                succeeded = console.writeAnalogOperation(cli, cLogicGroup, value, local);
            }
        }
        else
        {
            cli.message("Unsupported operation.\n");
        }
    }
    if(succeeded)
    {
        cli.message("Operation proceeded.\n");
    }
    else
    {
        cli.message("Operation cancelled/rejected.\n");
    }

}


void Console::CLICOperateCL::call(CommandLineInterpreter& cli)
{
    bool cancelled = true;
    lu_uint32_t clogic;
    lu_uint16_t operation;
    lu_uint16_t local = 0;   //parameters for Switch/action
    lu_uint16_t duration = 0;       //parameters for test
    lu_uint32_t cLogicNum = console.database.getCLogicNum();

    if(cli.waitYesNo("CAUTION! Operating a Switch or any Control Logic could be very dangerous!\n "
                    "Please take maximum care to ensure the operation could be done without "
                    "any harm to people or equipment. Are you sure you want to proceed"
                    ) != cli.YESNORESULT_YES)
    {
        printCancelled(cli);
        return;
    }


    /* Print list of CL available */
    cli.message("Control Logic blocks available:\n");
    std::stringstream options;  //Option number list
    for(lu_uint32_t i = 0; i <= cLogicNum; ++i)
    {
        //List it only if it can be operated
        if(console.database.isOperatable(i) == LU_TRUE)
        {
            cli.message("\t%u: %s\n", i, console.database.getTypeName(i));
            options << (lu_uint32_t)i << ", ";
        }
    }
    cli.message("\t0: -Cancel-\n");
    cli.message("Select Control Logic block: (");
    cli.message(options.str());
    cli.message("0): ");

    if(cli.askAndCheck(&clogic, 1, cLogicNum) == LU_TRUE)	//0 is VPoint group, do not accept it
    {
        //Refuse non-operable Logics
        if(!console.database.isOperatable(clogic))
        {
            cli.message("Cannot operate %s %u: Cancelled.\n", console.database.getTypeName(clogic), clogic);
            return;
        }
        CONTROL_LOGIC_TYPE cLogicType = console.database.getType(clogic);

        /* When a Control Logic is added, ensure that is updated in the switch
         * statement (if is operatable) and then put the number of Control Logics
         * (straight a number, not the constant!) and the last one in the ASSERT.
         * E.g.:    CHECK_CONTROL_LOGIC(28, CONTROL_LOGIC_TYPE_BYPASS)
         */
        LU_STATIC_ASSERT(CHECK_CONTROL_LOGIC(29, CONTROL_LOGIC_TYPE_MOTOR_SUPPLY), Control_Logic_amount_mismatch);
        switch(cLogicType)
        {
            case CONTROL_LOGIC_TYPE_SGL:
            case CONTROL_LOGIC_TYPE_DUMMYSW:
                if( askOperation(cli, operation, "open(trip)", "close") )
                {
                    if( askLocalRemote(cli, local) )
                    {
                        cancelled = !(console.controlLogicOperate(cli, clogic, operation, local));
                    }
                }
                break;
            case CONTROL_LOGIC_TYPE_DOL:
            case CONTROL_LOGIC_TYPE_DIGCTRLPOINT:
                if( askOperation(cli, operation, "off", "on") )
                {
                    if( askLocalRemote(cli, local) )
                    {
                        cancelled = !(console.controlLogicOperate(cli, clogic, operation, local));
                    }
                }
                break;
            case CONTROL_LOGIC_TYPE_POWER_SUPPLY:
                if( askOperation(cli, operation, "off", "on") )
                {
                    if( askLocalRemote(cli, local) )
                    {
                        cli.message("NOTE: Duration 0 applies the configured duration\n");
                        lu_uint32_t longDuration;
                        if( askDuration(cli, longDuration) )
                        {
                            cancelled = !(console.controlLogicOperate(cli, clogic, operation, local, longDuration));
                        }
                    }
                }
                break;
            case CONTROL_LOGIC_TYPE_BATTERY:
            case CONTROL_LOGIC_TYPE_MOTOR_SUPPLY:
                if( askOperation(cli, operation, "stop", "start") )
                {
                    if( askLocalRemote(cli, local) )
                    {
                        /* This command depends on the operation: START uses duration, STOP cancels */
                        if(operation == 0)
                        {
                            /* Stop operation */
                            cancelled = !(console.controlLogicOperate(cli, clogic, operation, local, duration));
                        }
                        else
                        {
                            /* Start command */
                            cli.message("NOTE: Duration 0 applies the configured duration\n");
                            if( askDuration(cli, duration) )
                            {
                                cancelled = !(console.controlLogicOperate(cli, clogic, operation, local, duration));
                            }
                        }
                    }
                }
                break;

            case CONTROL_LOGIC_TYPE_FAN_TEST:
                if( askLocalRemote(cli, local) )
                {
                    if( askDuration(cli, duration) )
                    {
                        cancelled = !(console.controlLogicOperate(cli, clogic, 1, local, duration));
                    }
                }
                break;
            case CONTROL_LOGIC_TYPE_FPI_TEST:
            case CONTROL_LOGIC_TYPE_FPI_RESET:
            case CONTROL_LOGIC_TYPE_COUNTER_COMMAND:
                if( askLocalRemote(cli, local) )
                {
                    cancelled = !(console.controlLogicOperate(cli, clogic, 1, local));
                }
                break;
            case CONTROL_LOGIC_TYPE_ANALOGCTRLPOINT:
                if( askLocalRemote(cli, local) )
                {
                    AnalogueOutputOperation anaOp;
                    anaOp.local = (local == 1)? LU_TRUE : LU_FALSE;
                    std::string anaTxt;
                    cli.ask("\tAnalogue value to set: ", anaTxt);
                    std::stringstream ss(anaTxt);
                    ss >> anaOp.value;
                    cancelled = console.database.writeAnalogOperation(clogic, anaOp);
                    if(!cancelled)
                    {
                        cli.message("Written value %f to %s %u",
                                    anaOp.value, console.database.getTypeName(clogic), clogic);
                    }
                }
                break;
            case CONTROL_LOGIC_TYPE_PCLK:
            default:
                if( askOperation(cli, operation, "stop", "start") )
                {
                    if( askLocalRemote(cli, local) )
                    {
                        cancelled = !(console.controlLogicOperate(cli, clogic, operation, local));
                    }
                }
                break;
        }
    }
    if(cancelled)
    {
        printCancelled(cli);
    }
}


void Console::CLICFreeze::call(CommandLineInterpreter& cli)
{
    PointIdStr pointID;
    lu_bool_t result = LU_FALSE;

    cli.message("\tVirtual Point Group: ");
    if(cli.askAndCheck(&pointID.group, 0, std::numeric_limits<lu_uint16_t>::max()) == LU_TRUE)
    {
        cli.message("\tVirtual Point ID: ");
        if(cli.askAndCheck(&pointID.ID, 0, std::numeric_limits<lu_uint16_t>::max()) == LU_TRUE)
        {
            cli.message("\n");
            if(console.database.getPointType(pointID) != POINT_TYPE_COUNTER)
            {
                console.printPointError(cli, GDB_ERROR_NOPOINT, pointID);
            }
            else
            {
                lu_uint32_t andClear;
                cli.message("\tType of freeze to apply?:\n"
                            "\t\t1 to Freeze only\n"
                            "\t\t2 to Freeze-and-clear (reset counter after freeze)\n"
                            "\t\t0 to cancel\n"
                            "\tType?: ");
                if (cli.askAndCheck(&andClear, 1, 2) == LU_TRUE)
                {
                    console.database.freeze(pointID, (andClear==2));
                    result = LU_TRUE;
                }
            }
        }
    }
    if(result == LU_FALSE)
    {
        printCancelled(cli);
    }
}


void Console::CLICModuleTout::call(CommandLineInterpreter& cli)
{
    lu_uint32_t mode;
    cli.message("This will enable/disable the timeout manager in all the modules. "
                    "That means the HeartBeat message to the Slave Modules is not checked, "
                    "so any Slave Module absence is not detected.\n"
                    "CAUTION: Disabling it could be dangerous.\n"
                    "Set to 1 to enable, 0 to disable: ");
    if( cli.askAndCheck(&mode, 0, 1) == LU_TRUE )
    {
        //valid input
        cli.message("%s module timeout manager...\n",
                        (mode == 0)? "Disabling" : "Enabling");
        std::vector<IIOModule*> moduleList;
        moduleList = console.moduleManager.getAllModules();
        cli.message("Modules found: %i\n", moduleList.size());
        IOModuleInfoStr moduleInfo;
        for(std::vector<IIOModule*>::iterator it=moduleList.begin(); it!=moduleList.end(); ++it)
        {
            (*it)->disableTimeout((mode == 0)? LU_TRUE : LU_FALSE);
        }
        printDone(cli);
    }
    else
    {
        printCancelled(cli);
    }
}


void Console::CLICActivateConfig::call(CommandLineInterpreter& cli)
{
    if( cli.waitYesNo("This will activate the latest uploaded configuration file.\n"
                    "CAUTION: The current configuration will be disabled and backed up.\n"
                    "Are you sure to proceed") == cli.YESNORESULT_YES )
    {
        //Note: we use here an instance of the FileTransferManager with unattached CRC observers
        CTH::FileTransferManager ftm;
        if(ftm.activateConfigFile() != CTH_ACK)
        {
            cli.message("Error trying to activate the configuration file.");
        }
        else
        {
            bool activated = false;
            lu_uint32_t crc;
            if(ftm.updateConfigCRC("", &crc))
            {
                if(console.memoryManager.write(IMemoryManager::STORAGE_VAR_CFGFILECRC, (lu_uint8_t*)&crc) == LU_TRUE)
                {
                    activated = true;
                    cli.message("Done - Configuration file activated successfully.");
                }
            }
            if(!activated)
            {
                cli.message("Failed to activate the configuration file - CRC not stored.");
            }
        }
    }
    else
    {
        printCancelled(cli);
    }
}


void Console::CLICRestoreConfig::call(CommandLineInterpreter& cli)
{

    if( cli.waitYesNo("This will restore the previous Configuration file.\n"
                    "CAUTION: The current configuration will be disabled and backed up, "
                    "and the backup will become the current and active configuration.\n"
                    "Are you sure to proceed") == cli.YESNORESULT_YES )
    {
        //Note: we use here an instance of the FileTransferManager with unattached CRC observers
        CTH_ACKNACKCODE res;
        CTH::FileTransferManager ftm;
        res = ftm.restoreConfigFile();
        if(res != CTH_ACK)
        {
            cli.message("Error trying to restore the Configuration file: %s",
                        (res == CTH_NACK_FILENOTEXIST)? "No backup available." :
                                                        "Error writing file(s)"
                        );
        }
        else
        {
            bool activated = false;
            lu_uint32_t crc;
            if(ftm.updateConfigCRC("", &crc))
            {
                if(console.memoryManager.write(IMemoryManager::STORAGE_VAR_CFGFILECRC, (lu_uint8_t*)&crc) == LU_TRUE)
                {
                    activated = true;
                    cli.message("Done - Configuration file restored successfully.");
                }
            }
            if(!activated)
            {
                cli.message("Failed to activate the Configuration file - CRC not stored.");
            }
        }
    }
    else
    {
        printCancelled(cli);
    }
}


void Console::CLICDeleteConfig::call(CommandLineInterpreter& cli)
{
    /* TODO: pueyos_a - CONSOLE Implement delete config if it really goes here */
    cli.message("Command not supported yet");
    if( cli.waitYesNo("This will delete the Gemini3 configuration file! "
                    "CAUTION: That means the RTU may not be configured next time the RTU is restarted. "
                    " Are you sure") == cli.YESNORESULT_YES )
    {
        /* erase config */
        CTH::eraseConfigFile();
        printDone(cli);
    }
    else
    {
        printCancelled(cli);
    }
}


void Console::CLICUpdater::call(CommandLineInterpreter& cli)
{
    if( cli.waitYesNo("Update Mode requires to restart the RTU and suspend its normal operation. "
                      "Are you sure") == cli.YESNORESULT_YES )
    {
        //valid input
        cli.message("Entering update mode.\n"
                    "MCM Application is going to close...\n");
        console.stop(APP_EXIT_CODE_START_MCM_UPDATER);
    }
    else
    {
        printCancelled(cli);
    }
}


void Console::CLICRestart::call(CommandLineInterpreter& cli)
{
    //Report Event
    CTEventSystemStr event;
    event.eventType = EVENT_TYPE_SYSTEM_RESTARTREQ;
    event.value = EVENT_REQUEST_VALUE_CLI;
    console.eventLog.logEvent(event);

    //Asked to restart
    console.log.warn("MCM application is going to restart from console.");
    cli.message("MCM application is going to restart...\n");
    console.stop(APP_EXIT_CODE_RESTART);
}


void Console::CLICReboot::call(CommandLineInterpreter& cli)
{
    //Report Event
    CTEventSystemStr event;
    event.eventType = EVENT_TYPE_SYSTEM_REBOOTREQ;
    event.value = EVENT_REQUEST_VALUE_CLI;
    console.eventLog.logEvent(event);

    console.log.warn("RTU is going to reboot from console.");
    cli.message("RTU is going to reboot...\n");
    console.stop(APP_EXIT_CODE_REBOOT_RTU);
}


void Console::CLICShutdown::call(CommandLineInterpreter& cli)
{
    //Report Event
    CTEventSystemStr event;
    event.eventType = EVENT_TYPE_SYSTEM_SHUTDOWNREQ;
    event.value = EVENT_REQUEST_VALUE_CLI;
    console.eventLog.logEvent(event);

    console.log.warn("RTU is going to be shut down from console.");
    cli.message("RTU is going to be shut down...\n");
    console.stop(APP_EXIT_CODE_SHUTDOWN_RTU);
}


void Console::CLICFactoryHAT::call(CommandLineInterpreter& cli)
{
    LU_UNUSED(cli);
    //Report Event
    CTEventSystemStr event;
    event.eventType = EVENT_TYPE_SYSTEM_RESTARTREQ;
    event.value = EVENT_REQUEST_VALUE_CLI;
    console.eventLog.logEvent(event);

    console.log.warn("MCM application is going to restart from console in HAT mode.");
    console.stop(APP_EXIT_CODE_START_MCM_UPDATER_HAT);
}

void Console::CLICModuleReset::call(CommandLineInterpreter& cli)
{
    if( cli.waitYesNo("This will try to restart the selected module.\n"
                    "CAUTION: The module may restart without further warning and abort current operations.\n"
                    "Are you sure to proceed") == cli.YESNORESULT_YES )
    {
        IIOModule* module = console.askModule(cli);
        if(module != NULL)
        {
            if(module->isMCM() == LU_TRUE)
            {
                //Report Event
                CTEventSystemStr event;
                event.eventType = EVENT_TYPE_SYSTEM_RESTARTREQ;
                event.value = EVENT_REQUEST_VALUE_CLI;
                console.eventLog.logEvent(event);

                //Asked to restart
                console.log.warn("MCM application is going to restart from console.");
                cli.message("MCM application is going to restart...\n");
                console.stop(APP_EXIT_CODE_RESTART);
            }
            else
            {
                CANIOModule* moduleCAN = dynamic_cast<CANIOModule*>(module);
                IOM_ERROR res = moduleCAN->sendRestartCmd(MD_RESTART_WARM);
                if(res != IOM_ERROR_NONE)
                {
                    cli.message("Error trying to restart the %s module. Error: %s",
                                module->getName(), IOM_ERROR_ToSTRING(res));
                }
                else
                {
                    return;
                }
            }
        }
    }
    printCancelled(cli);
}


void Console::CLICBootloader::call(CommandLineInterpreter& cli)
{
    lu_uint32_t mode;
    cli.message("BootLoader keep mode. Signals the Slave Modules to do not exit from the BootLoader when they start.\n"
                    "CAUTION: This command also RESTARTS all Slave Modules.\n"
                    "Set to 1 to enter BL mode, 0 to exit BL mode: ");
    if(cli.askAndCheck(&mode, 0, 1) == LU_TRUE)
    {
        // Set/Reset the HeartBeat to BootLoader mode
        cli.message("%s BootLoader mode.\n", (mode == 0)? "Exiting" : "Entering");
        console.moduleManager.setBootloaderMode( (mode==1)? LU_TRUE : LU_FALSE );
        console.moduleManager.restartAllModules();
    }
    else
    {
        printCancelled(cli);
    }
}


void Console::CLICHeartBeat::call(CommandLineInterpreter& cli)
{
    lu_uint32_t mode;
    cli.message("Enables or disables the sending of the HeartBeat message to the Slave Modules.\n"
                    "CAUTION: This command affects the communication with the Slave Modules.\n"
                    "Set to 1 to enable, 0 to disable: ");
    if(cli.askAndCheck(&mode, 0, 1) == LU_TRUE)
    {
        console.moduleManager.setHBeatEnable(mode);
    }
    else
    {
        printCancelled(cli);
    }
}


void Console::CLICTestEvents::call(CommandLineInterpreter& cli)
{
    lu_uint32_t eventAmount = 0;
    //Generate time stamp common to some generated event entries
    struct timespec timesp;
    TimeManager::getInstance().getTime(&timesp); //Get current time
    TimeMilisStr timestamp;
    timestamp.sec = timesp.tv_sec + 60;
    timestamp.msec = timesp.tv_nsec / 1000000L;

    for (lu_uint32_t eventClass = 0; eventClass < EVENT_CLASS_LAST; ++eventClass)
    {
        switch(eventClass)
        {
            case EVENT_CLASS_SYSTEM:
            {
                CTEventSystemStr event;
                for (lu_uint32_t eventType = 0; eventType < EVENT_TYPE_SYSTEM_LAST; ++eventType)
                {
                    switch(eventType)
                    {
                        case EVENT_TYPE_SYSTEM_STARTUP:
                        case EVENT_TYPE_SYSTEM_COLDBOOT:
                        case EVENT_TYPE_SYSTEM_WARMBOOT:
                        case EVENT_TYPE_SYSTEM_SHUTDOWN:
                        case EVENT_TYPE_SYSTEM_RESTARTREQ:
                        case EVENT_TYPE_SYSTEM_REBOOTREQ:
                        case EVENT_TYPE_SYSTEM_SHUTDOWNREQ:
                        case EVENT_TYPE_SYSTEM_ERROR:
                        case EVENT_TYPE_SYSTEM_READ_CONFIG:
                        case EVENT_TYPE_SYSTEM_ERROR_STARTING:
                        case EVENT_TYPE_SYSTEM_DOOR_OPEN:
                            event.eventType = eventType;
                            break;
                        default:
                            continue;
                    }
                    event.value = 1;
                    event.unused = 0;
                    console.eventLog.logEvent(event);
                    eventAmount++;
                }
            }
            break;
            case EVENT_CLASS_COMM:
            {
                CTEventCommStr event;
                event.action = EVENTCOMM_ACTION_ERROR_STARTING;
                event.unused = 0;

                event.device = EVENTCOMM_DEVICE_SCADA;
                console.eventLog.logEvent(event);
                eventAmount++;
                event.device = EVENTCOMM_DEVICE_CONFIGTOOL;
                console.eventLog.logEvent(event);
                eventAmount++;
            }
            break;
            case EVENT_CLASS_OLR:
            {
                CTEventOLRStr event;
                for (lu_uint32_t eventType = 0; eventType < EVENTOLR_VALUE_LAST; ++eventType)
                {
                    event.value = eventType;
                    event.unused = 0;
                    console.eventLog.logEvent(event);
                    eventAmount++;
                }
            }
            break;
            case EVENT_CLASS_SWITCH:
            {
                CTEventSwitchStr event;

                for (lu_uint32_t eventType = 0; eventType < EVENT_TYPE_SWITCH_LAST; ++eventType)
                {
                    switch(eventType)
                    {
                        case EVENT_TYPE_SWITCH_REQ_OPEN:
                        case EVENT_TYPE_SWITCH_OPENING:
                        case EVENT_TYPE_SWITCH_OPEN:
                        case EVENT_TYPE_SWITCH_REQ_CLOSE:
                        case EVENT_TYPE_SWITCH_CLOSING:
                        case EVENT_TYPE_SWITCH_CLOSE:
                            event.eventType = eventType;
                            event.moduleID = 4;
                            event.moduleType = MODULE_DSM;
                            event.value = EVENTSWITCH_VALUE_IN_OPERATION;
                            event.switchID = 1;
                            console.eventLog.logEvent(event);
                            eventAmount++;
                            break;
                        default:
                            break;
                    }
                }
            }
            break;
            case EVENT_CLASS_CLOGIC:
            {
                CTEventCLogicStr event;
                for (lu_uint32_t eventType = 0; eventType < EVENT_TYPE_CLOGIC_LAST; ++eventType)
                {
                    switch(eventType)
                    {
                        case EVENT_TYPE_CLOGIC_FANTEST:
                            event.eventType = eventType;
                            event.cLogicType = EVENT_TYPE_CLOGICTYPE_FANTEST;
                            event.value = EVENTCLOGIC_VALUE_OK;
                            event.unused = 0;
                            console.eventLog.logEvent(event);
                            eventAmount++;
                        break;
                        case EVENT_TYPE_CLOGIC_BATTERY:
                            event.eventType = eventType;
                            event.unused = 0;
                            event.value = EVENTCLOGIC_VALUE_OK;

                            event.cLogicType = EVENT_TYPE_CLOGICTYPE_BATTERY_TEST;
                            console.eventLog.logEvent(event);
                            eventAmount++;
                            event.cLogicType = EVENT_TYPE_CLOGICTYPE_BATTERY_CHARGER_TEST;
                            console.eventLog.logEvent(event);
                            eventAmount++;
                            event.cLogicType = EVENT_TYPE_CLOGICTYPE_BATTERY_DISCONNECT;
                            console.eventLog.logEvent(event);
                            eventAmount++;
                        break;
                        case EVENT_TYPE_CLOGIC_DIGITAL_OUTPUT:
                            event.eventType = eventType;
                            event.cLogicType = EVENT_TYPE_CLOGICTYPE_DIGITAL_OUTPUT;
                            event.value = 1;
                            event.unused = 0;
                            console.eventLog.logEvent(event);
                            eventAmount++;
                        break;
                    }
                }
            }
            break;
            case EVENT_CLASS_DATETIME:
            {
                CTEventDatetimeStr event;
                event.timestamp = timestamp;
                event.isdst = 1;
                console.eventLog.logEvent(event);
                eventAmount++;
            }
            break;
            case EVENT_CLASS_DNP3SCADA_POINT:
            {
                SLEventEntryStr event;
                CTEventDNP3PointStr eventDNP;
                for (lu_uint32_t pointType = 0; pointType < EVENT_TYPE_DNP3SCADA_LAST; ++pointType)
                {
                    eventDNP.pointType = pointType;
                    eventDNP.dnpPointID = pointType + 1;
                    switch(pointType)
                    {
                        case EVENT_TYPE_DNP3SCADA_SINGLEBINARY:
                            eventDNP.value.valueDig = 1;
                            eventDNP.status = 0xFF;
                            break;
                        case EVENT_TYPE_DNP3SCADA_DOUBLEBINARY:
                            eventDNP.value.valueDig = 2;
                            eventDNP.status = 0xFF;
                            break;
                        case EVENT_TYPE_DNP3SCADA_ANALOGUE:
                        case EVENT_TYPE_DNP3SCADA_ANLGOUT:
                            eventDNP.value.valueAna = -1.2345;
                            eventDNP.status = 0x7F;
                            break;
                        default:
                            continue;
                            break;
                    }
                    event.eventClass = eventDNP.EventClass;
                    event.timestamp.sec = timestamp.sec;
                    event.timestamp.nsec = timestamp.msec * 1000000L;
                    event.payload.eventDNP3 = eventDNP;
                    console.eventLog.logEvent(event);
                    eventAmount++;
                }
            }
            break;
            default:
                break;
        } //endswitch eventClass
    } //endfor eventClass
    cli.message("Event test pattern generated, %i events. Please check event list.\n", eventAmount);
}

void Console::CLICWDogStop::call(CommandLineInterpreter& cli)
{
    console.log.warn("Watchdog kicking stopped from console.");
    cli.message("RTU will be restarted by Watchdog...\n");

    // Stop kicking the Watchdog
    console.monitorManager.send_req_setWdogKick_msg(LU_FALSE);
}


void Console::CLICLabels::call(CommandLineInterpreter& cli)
{
    cli.message("Labels registered:\n");
    LabelPool::LabelIndex i=0;
    std::string label;
    size_t len = 0;
    do
    {
        label = LabelPool::getInstance().getLabel(i++);
        len = label.length();
        if(len > 0)
        {
            cli.message("\t- '%s'\n", label.c_str());
        }
    } while (len > 0);
    cli.message("%u labels stored.\n", i-1);
}


void Console::CLICModbusTest::call(CommandLineInterpreter& cli)
{
    console.checkSubcommand("Modbus", cli, console.m_mbCmdList, console.m_mbHelp);
}


void Console::MBCLICRead::call(CommandLineInterpreter& cli)
{
    console.checkSubcommand("Reading", cli, console.m_mbReadCmdList, console.m_mbReadHelp);
}


void Console::MBCLICWrite::call(CommandLineInterpreter& cli)
{
    console.checkSubcommand("Writing", cli, console.m_mbWriteCmdList, console.m_mbWriteHelp);
}


void Console::MBCLICStats::call(CommandLineInterpreter& cli)
{
    mbm::devicestats mbStats;
    mbm::devRegInfo params;

    if(!console.askModbusParams(cli, params))
    {
        printCancelled(cli);
        return;
    }
    if(console.readModbusStats(params, mbStats) == false)
    {
        cli.message("Error: Failed to obtain Modbus device stats\n");
        return;
    }
    cli.message("Stats of the device:\n%s", mbStats.toString().c_str());
}


void Console::MBCLICReadCoil::call(CommandLineInterpreter& cli)
{
    console.readModbusRegister(cli, mbm::REG_TYPE_COIL_STATUS);
}


void Console::MBCLICReadInputReg::call(CommandLineInterpreter& cli)
{
    console.readModbusRegister(cli, mbm::REG_TYPE_INPUT_REGISTERS);
}


void Console::MBCLICReadHoldReg::call(CommandLineInterpreter& cli)
{
    console.readModbusRegister(cli, mbm::REG_TYPE_HOLDING_REGISTERS);
}


void Console::MBCLICReadDiscrReg::call(CommandLineInterpreter& cli)
{
    console.readModbusRegister(cli, mbm::REG_TYPE_INPUT_STATUS);
}


void Console::MBCLICWriteCoil::call(CommandLineInterpreter& cli)
{
    mbm::devRegInfo params;
    std::vector<lu_uint16_t> values;
    if(console.askModbusAddrParams(cli, params))
    {
        std::string token;
        bool finish = false;
        do
        {
            cli.message("Value (0 or 1, use 'x' to finish): ");
            getToken(cli, token);

            lu_char_t c = token[0];
            switch(c)
            {
                case 'x':
                    finish = true;  //Finish evaluating input
                    break;
                case '0':
                    values.push_back(0);
                    break;
                case '1':
                    values.push_back(1);
                    break;
                default:
                    cli.message("Error: can't understand '%s'\n", trim_back(token).c_str());
                    break;
            }
        }
        while( (!finish) && (values.size() <= (LU::maxValue(params.startAddress) - params.startAddress)) );
        cli.message("\n");

        if(console.writeModbusRegister(cli, mbm::REG_TYPE_COIL_STATUS, params, values) == mbm::WRITE_SUCCESS)
        {
            printDone(cli);
            return;
        }
    }
    printCancelled(cli);
}


void Console::MBCLICWriteHoldReg::call(CommandLineInterpreter& cli)
{
    mbm::devRegInfo params;
    std::vector<lu_uint16_t> values;
    if(console.askModbusAddrParams(cli, params))
    {
        std::string token;
        lu_uint32_t value;  //sscanf requires this to be an int, not a short int
        do
        {
            cli.message("Value in hex (0x####) ('x' to finish): ");
            getToken(cli, token);
            if(token.substr(0, 1).compare("x") == 0)
            {
                break;  //Finish
            }
            lu_uint32_t pos = (token.substr(0, 2).compare("0x") == 0)? 2 : 0;

            if(sscanf(token.c_str() + pos, "%x", &value) == 1)
            {
                lu_uint16_t val16;
                val16 = LU_GETBITMASKVALUE(value, 0x0000FFFF, 0);
                values.push_back(val16);
            }
            else
            {
                cli.message("Error: can't understand '%s'\n", trim_back(token).c_str());
            }
        }
        while(values.size() <= (LU::maxValue(params.startAddress) - params.startAddress));
        cli.message("\n");

        if(console.writeModbusRegister(cli, mbm::REG_TYPE_HOLDING_REGISTERS, params, values) == mbm::WRITE_SUCCESS)
        {
            printDone(cli);
            return;
        }
    }
    printCancelled(cli);
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void CmdHelp::call(CommandLineInterpreter& cli)
{
    cli.message("\nCommands available:\n");

    /* print help-specific line */
    cli.message("\t%s (%s): %s\n", commandName.c_str(), commandNameAlt.c_str(), description.c_str());
    /* print all the rest of command's lines */
    for(CLICmdList::const_iterator it = m_commandList.begin(); it != m_commandList.end(); ++it)
    {
        if((*it)->hidden != LU_TRUE)  //print only the ones that are not hidden
        {
            cli.message("\t%s%s%s%s: %s\n",
                    ((*it)->commandName.c_str()),
                    ((*it)->commandNameAlt.empty())? "" : " (",
                    ((*it)->commandNameAlt.empty())? "" : (*it)->commandNameAlt.c_str(),
                    ((*it)->commandNameAlt.empty())? "" : ")",
                    ((*it)->description.c_str())
                    );
        }
    }
}


void Console::printPointError(CommandLineInterpreter& cli, const GDB_ERROR errorCode, const PointIdStr pointID)
{
    if(errorCode != GDB_ERROR_NONE)
    {
        cli.message("Error getting point [%s]: %s\n", pointID.toString().c_str(), GDB_ERROR_ToSTRING(errorCode));
    }
}


void Console::printPointData(CommandLineInterpreter& cli, const PointIdStr pointID)
{
    if(pointID.isValid() == false)
    {
        cli.message("Point ID %d,%d is not valid", pointID.group, pointID.ID);
        return;
    }
    TimeManager::TimeStr timestamp;
    POINT_TYPE type = database.getPointType(pointID);

    switch(type)
    {
        case POINT_TYPE_BINARY:
        case POINT_TYPE_DBINARY:
        {
            //digital value
            PointDataUint8 data;
            GDB_ERROR gdbRet = database.getValue(pointID, &data);
            if(gdbRet != GDB_ERROR_NONE)
            {
                printPointError(cli, gdbRet, pointID);
                return;
            }
            PointDataUint8 rawData;
            rawData = 0;
            bool validRaw;  //States if the raw value was validly recovered
            validRaw = (database.getRAWValue(pointID, &rawData) == GDB_ERROR_NONE);
            data.getTime(timestamp);
            cli.message("\t%s Binary Input Point [%s] \t%s%s\n"
                        "\t  DValue: %u  %s\n"
                        "\t  RawValue: %u%s\n"
                        "\t  Time: %s\n",
                        (type == POINT_TYPE_BINARY)? "Single" : "Double",
                        pointID.toString().c_str(),
                        getCommonFlagsString(data).c_str(),
                        (data.getChatterFlag() == true)? "|InChatter" : "",
                        ((lu_uint8_t)data) & ((type==POINT_TYPE_BINARY)? 0x01 : 0x03),  //Get digital value only
                        database.getValueLabel(pointID, &data),
                        (lu_uint8_t)rawData, (validRaw)? "" : "(invalid)",
                        timestamp.toString(true).c_str()
                       );
        }
        break;
        case POINT_TYPE_ANALOGUE:
            {
                //Analogue value
                PointDataFloat32 data;
                GDB_ERROR gdbRet = database.getValue(pointID, &data);
                if(gdbRet != GDB_ERROR_NONE)
                {
                    printPointError(cli, gdbRet, pointID);
                    return;
                }
                bool validRaw;  //States if the raw value was validly recovered
                PointDataFloat32 rawData;
                rawData = 0;
                PointDataFloat32 minData, maxData;
                validRaw = (database.getRAWValue(pointID, &rawData) == GDB_ERROR_NONE);
                database.getPointRange(pointID, &minData, &maxData);
                data.getTime(timestamp);
                AFILTER_RESULT afResult = data.getAFilterStatus();
                cli.message("\tAnalogue Input Point [%s] \t %s%s\n"
                            "\t  AValue: %f  %s  Range: [%f, %f]\n"
                            "\t  RawValue: %f %s\n"
                            "\t  Filter:%i (%s)\n"
                            "\t  Time: %s\n",
                             pointID.toString().c_str(),
                             getCommonFlagsString(data).c_str(),
                             (data.getOverflowFlag() == true)? "|overflow" : "",
                             (lu_float32_t)(data),
                             database.getValueLabel(pointID, &data),
                             (lu_float32_t)(minData), (lu_float32_t)(maxData),
                             (lu_float32_t)(rawData), (validRaw)? "" : "(invalid)",
                             afResult,
                             IAnalogueFilter::AFILTER_RESULT_ToString(afResult),
                             timestamp.toString(true).c_str()
                           );
            }
            break;
        case POINT_TYPE_COUNTER:
            {
                //Counter value: show Counter frozen AND unfrozen value
                GDB_ERROR gdbRet;
                PointDataCounter32 data;    //current value
                PointDataCounter32 frzData; //frozen value
                data = 0;
                frzData = 0;
                PointDataCounter32 minData, maxData;
                gdbRet = database.getValue(pointID, &data);
                if(gdbRet != GDB_ERROR_NONE)
                {
                    printPointError(cli, gdbRet, pointID);
                	return;
                }
                bool validFrz;  //States if the frozen value was validly recovered
                validFrz = (database.getFrozenValue(pointID, &frzData) == GDB_ERROR_NONE);
                database.getPointRange(pointID, &minData, &maxData);
                data.getTime(timestamp);
                TimeManager::TimeStr frzTimestamp;
                frzData.getTime(frzTimestamp);
                cli.message("\tCounter Input Point [%s] \tRange: [%u, %u]\n"
                            "\t  Curr Value: %u  %s  %s%s\n"
                            "\t  Time: %s\n"
                            "\t  Frozen Value: %u %s  %s%s\n"
                            "\t  Time: %s\n",
                            pointID.toString().c_str(),
                            (lu_uint32_t)(minData), (lu_uint32_t)(maxData),
                            (lu_uint32_t)data,
                            database.getValueLabel(pointID, &data),
                            getCommonFlagsString(data).c_str(),
                            (data.getOverflowFlag() == true)? "|rollover" : "",
                            timestamp.toString(true).c_str(),
                            (lu_uint32_t)frzData, (validFrz)? "" : "(invalid)",
                            getCommonFlagsString(frzData).c_str(),
                            (frzData.getOverflowFlag() == true)? "|rollover" : "",
                            frzTimestamp.toString(true).c_str()
                           );
            }
            break;
        default:
            break;
    }
}


void Console::printCLPointData(CommandLineInterpreter& cli,
                                const lu_uint16_t ctLogicID,
                                const lu_uint32_t numCLPoints)
{
    PointIdStr pointID;

    pointID.group = ctLogicID;
    for(lu_uint32_t j = 0; j < numCLPoints; ++j)
    {
        pointID.ID = j;
        printPointData(cli, pointID);
    }
}


lu_bool_t Console::readChannelValue(CommandLineInterpreter& cli,
                                    IIOModule& module,
                                    const IChannel::ChannelIDStr channel,
                                    IChannel::ValueStr& valueRead,
                                    IChannel::ValueStr& rawValueRead
                                    )
{
    lu_bool_t ret = LU_FALSE;

    if(valueRead.dataPtr == NULL)
        return LU_FALSE;

    valueRead.flags.online = LU_FALSE;    //default value

    if(module.getStatus(MSTATUS_PRESENT) == LU_FALSE)
    {
        cli.message("%s Module is not present.\n", module.getName());
        ret = LU_FALSE;
    }
    else
    {
        if( module.readRAWChannel(channel, rawValueRead) != IOM_ERROR_NONE )
        {
            cli.message("Unable to read %s/%s channel raw value.\n",
                        module.getName(),
                        channel.toString().c_str()
                        );
        }
        IChannel* pChan = module.getChannel(channel);
        if( pChan != NULL )
        {
            if( pChan->read(valueRead) == IOM_ERROR_NONE )
            {
                ret = LU_TRUE;
            }
        }
        if(ret != LU_TRUE)
        {
            cli.message("Unable to read %s/%s channel value.\n",
                        module.getName(),
                        channel.toString().c_str()
                        );
        }
    }
    return ret;
}


void Console::printChannelValue(CommandLineInterpreter& cli,
                                const IChannel::ChannelIDStr channel,
                                IChannel::ValueStr& value,
                                IChannel::ValueStr& rawValue
                                )
{
    /* print out value depending on channel type */
    switch(channel.type)
    {
        case CHANNEL_TYPE_AINPUT:
        case CHANNEL_TYPE_INTERNAL_AINPUT:
        {
            //Analog type
            cli.message("%s value: %f (raw: %f) \t%s\n\tTime: %s\n",
                    channel.toString().c_str(),
                    (lu_float32_t)*(value.dataPtr),
                    (lu_float32_t)*(rawValue.dataPtr),
                    rawValue.flags.toString().c_str(),
                    value.dataPtr->getTime()->toString().c_str()
                    );
        }
        break;
        case CHANNEL_TYPE_DINPUT:
        case CHANNEL_TYPE_INTERNAL_DINPUT:
        case CHANNEL_TYPE_INTERNAL_DDINPUT:
        {
            //Digital type
            cli.message("%s value: %u (raw: %u) \t%s\n\tTime: %s\n",
                    channel.toString().c_str(),
                    (lu_uint8_t)*(value.dataPtr),
                    (lu_uint32_t)*(rawValue.dataPtr),
                    rawValue.flags.toString().c_str(),
                    value.dataPtr->getTime()->toString().c_str()
                    );
        }
        break;
        default:
        break;
    }
}


IIOModule* Console::askModule(CommandLineInterpreter& cli)
{
    IIOModule* ret = NULL;
    lu_uint32_t moduleIndex = 0;
    IOModuleIDStr moduleID;

    /* Show module list */
    cli.message("Select Module: \n");
    std::vector<IIOModule*> moduleList;
    moduleList = moduleManager.getAllModules();
    for(lu_uint32_t i=0; i < moduleList.size(); ++i)
    {
        moduleID = moduleList[i]->getID();
        cli.message("\t%i: %s [%i:%i]\n",
                    i+1,    //Note: Internal module 0 is module 1 for the user
                    moduleList[i]->getName(),
                    moduleID.type, moduleID.id    //CAN ID 0:0 denomination format
                    );
    }
    cli.message("\t0: Cancel\n");
    if(cli.askAndCheck(&moduleIndex, 1, moduleList.size()) == LU_TRUE)
    {
        --moduleIndex;  //Adjust from user numbering to internal
        ret = moduleList[moduleIndex];
    }
    cli.message("\n");
    return ret;
}


IIOModule* Console::askModuleAlarms(CommandLineInterpreter& cli)
{
    IIOModule* module = NULL;

    /* Show module list with alarm codes */
    cli.message("Select Module: \n");
    std::vector<IIOModule*> moduleList;
    moduleList = moduleManager.getAllModules();
    cli.message("Modules found: %i\n", moduleList.size());
    IOModuleInfoStr info;
    for(lu_uint32_t i=0; i < moduleList.size(); ++i)
    {
        moduleList[i]->getInfo(info);
        IOModuleIDStr moduleID = moduleList[i]->getID();
        /* Compose alarm status string */
        std::stringstream ss;
        if (info.hBeatInfo.error & (MODULE_BOARD_ERROR_ALARM_CRITICAL |
                                   MODULE_BOARD_ERROR_ALARM_ERROR   |
                                   MODULE_BOARD_ERROR_ALARM_WARNING |
                                   MODULE_BOARD_ERROR_ALARM_INFO)
            )
        {
            if(info.hBeatInfo.error & MODULE_BOARD_ERROR_ALARM_CRITICAL)
            {
                ss << "Critical, ";
            }
            if(info.hBeatInfo.error & MODULE_BOARD_ERROR_ALARM_ERROR)
            {
                ss << "Error, ";
            }
            if(info.hBeatInfo.error & MODULE_BOARD_ERROR_ALARM_WARNING)
            {
                ss << "Warn, ";
            }
            if(info.hBeatInfo.error & MODULE_BOARD_ERROR_ALARM_INFO)
            {
                ss << "Info, ";
            }
        }
        else
        {
            ss << "None. ";
        }
        std::string alms = ss.str();
        cli.message("\t%i: %s [%i:%i] Active alarms: %s.\n",
                    i+1,        //Note: Internal module 0 is module 1 for the user
                    moduleList[i]->getName(),
                    moduleID.type, moduleID.id,    //CAN ID 0:0 denomination format
                    alms.substr(0, alms.size()-2).c_str()   //Remove ending comma
                    );
    }
    cli.message("\t0: Cancel\n");

    lu_uint32_t moduleIndex = 0;
    if(cli.askAndCheck(&moduleIndex, 1, moduleList.size()) == LU_TRUE)
    {
        --moduleIndex;  //Adjust from user numbering to internal
        module = moduleList[moduleIndex];
    }
    cli.message("\n");

    if(module != NULL)
    {
        cli.message("  Retrieving module alarms...\n");
        ModuleAlarmLister alarmList = module->getAlarms();
        size_t listSize = alarmList.size();
        if(listSize == 0)
        {
            cli.message("No alarms available for this module.\n");
            return NULL;
        }
        //Print header:
        cli.message("   SubSystem, AlarmCode, Severity, State, (parameter)\n"
                    "--------------------------------------------------------\n"
                    );
        //Print out the alarm list:
        for (size_t i = 0; i < listSize; ++i)
        {
            if(alarmList[i].isValid() == LU_TRUE)
            {
                cli.message(alarmList[i].toString());
                cli.message("\n");
            }

        }
        cli.message("--\n");
    }
    else
    {
        printCancelled(cli);
    }
    return module;
}


CHANNEL_TYPE Console::askChannelType(CommandLineInterpreter& cli)
{
    cli.message("Channel Type: \n");
    lu_uint32_t idxType;
    /* Present available options */
    const lu_uint32_t numOptions = 4;
    CHANNEL_TYPE options[numOptions] = {
                            CHANNEL_TYPE_DINPUT,
                            CHANNEL_TYPE_AINPUT,
                            CHANNEL_TYPE_INTERNAL_DINPUT,
                            CHANNEL_TYPE_INTERNAL_DDINPUT
                            };
    for(idxType = 0; idxType < numOptions; ++idxType)
    {
        cli.message("\t%i: %s\n",
                        idxType+1,
                        CHANNEL_TYPE_ToSTRING(options[idxType])
                        );
    }
    cli.message("\t%i: Cancel\n", 9);
    if(cli.askAndCheck(&idxType, 1, idxType) == LU_TRUE)
    {
        return options[idxType-1];  //Adjust from user numbering to internal
    }
    return CHANNEL_TYPE_LAST;
}


bool Console::writeAnalogOperation(CommandLineInterpreter& cli,
                                  const lu_uint16_t cLogic,
                                  const lu_float32_t value,
                                  const lu_uint16_t local
                                  )
{
    cli.message("Starting writing analogue value. CLogic: %d, value:%f, local:%s\n",
                        cLogic,
                        value,
                        (local != 0)? "Local" : "Remote");

    AnalogueOutputOperation op;
    op.value = value;
    op.local = local;
    GDB_ERROR ret = database.writeAnalogOperation(cLogic, op);

    if(ret == GDB_ERROR_NONE)
    {
        cli.message("Operation started...\n");
    }
    else
    {
        cli.message("Operation error: %s\n", GDB_ERROR_ToSTRING(ret));
    }
    return (ret == GDB_ERROR_NONE);
}


bool Console::controlLogicOperate(CommandLineInterpreter& cli,
                                  const lu_uint16_t cLogic,
                                  const lu_uint16_t operation,
                                  const lu_uint16_t local,
                                  const lu_uint32_t duration
                                  )
{
    SwitchLogicOperation sglOperation;
    sglOperation.operation = (operation == 0) ? SWITCH_OPERATION_OPEN : SWITCH_OPERATION_CLOSE;
    sglOperation.local = local;
    sglOperation.duration = duration;

    cli.message("Starting operation. CLogic: %d, operation:%s, local:%s, duration=%d\n",
                    cLogic,
                    SWITCH_OPERATION_ToSTRING(sglOperation.operation),
                    (local != 0)? "Local" : "Remote",
                    duration);
    GDB_ERROR ret = database.startOperation(cLogic, sglOperation);
    if(ret == GDB_ERROR_NONE)
    {
        cli.message("Operation started...\n");
    }
    else
    {
        cli.message("Operation error: %s\n", GDB_ERROR_ToSTRING(ret));
    }
    return (ret == GDB_ERROR_NONE);
}


const std::string Console::getCommonFlagsString(PointData& data)
{
    std::string ret;
    POINT_QUALITY q = data.getQuality();
    ret = (q == POINT_QUALITY_ON_LINE)? "Online" : (q == POINT_QUALITY_OFF_LINE)? "Offline" :"NotInit";
    if(data.getInitialFlag() == true)
    {
        ret.append("|New");
    }
    if(data.getInvalidFlag() == true)
    {
        ret.append("|invalid");
    }
    return ret;
}


void Console::checkSubcommand(const lu_char_t* title, CommandLineInterpreter& cli, CLICmdList& subcmdList, CmdHelp& helper)
{
    std::string subcmd;
    std::string token;

    /* Note that using cli.input() we only get the first pending token and
       leave the rest for later */
    cli.message("%s subcommand ('h' for help): ", title);
    cli.input(subcmd);
    lu_uint32_t inputPos = subcmd.find_first_of(" \n");
    token = subcmd.substr(0, inputPos);

    if( (token.compare(helper.commandName) == 0) ||
        (token.compare(helper.commandNameAlt) == 0)
        )
    {
        helper.call(cli);
        return;
    }
    for(CLICmdList::const_iterator it = subcmdList.begin(); it != subcmdList.end(); ++it)
    {
        if( (token.compare((*it)->commandName) == 0) ||
            (token.compare((*it)->commandNameAlt) == 0)
            )
        {
            //subcommand found
            (*it)->call(cli);
            return;
        }
    }
    cli.message("Subcommand not found: ");
    printCancelled(cli);
}


bool Console::askModbusParams(CommandLineInterpreter& cli,  mbm::devRegInfo& params)
{
    cli.message("\tModbus device ID [1..%u]?: ", LU::maxValue(params.deviceId));
    if(cli.askAndCheck(&(params.deviceId), 1, LU::maxValue(params.deviceId)) == LU_TRUE)
    {
        cli.message("\tNumber of message tries (0 for default)?: ");
        if(cli.askAndCheck(&(params.tries), 0, LU::maxValue(params.tries)) == LU_TRUE)
        {
            return true;
        }
    }
    return false;   //Cancelled
}


bool Console::askModbusAddrParams(CommandLineInterpreter& cli,  mbm::devRegInfo& params)
{
    if(askModbusParams(cli, params))
    {
        cli.message("\tModbus start address? (%i to %i): ", 0, LU::maxValue(params.startAddress));
        if(cli.askAndCheck(&(params.startAddress), 0, LU::maxValue(params.startAddress)) == LU_TRUE)
        {
            return true;
        }
    }
    return false;   //Cancelled
}


bool Console::askModbusReadParams(CommandLineInterpreter& cli,  mbm::devRegInfo& params)
{
    if(askModbusAddrParams(cli, params))
    {
        lu_uint16_t maxAmount = (LU::maxValue(params.startAddress) - params.startAddress);
        cli.message("\tAmount of registers (%s%d)?: ", (maxAmount>1)? "1-" : "", maxAmount);
        if(cli.askAndCheck(&(params.quantity), 1, maxAmount) == LU_TRUE)
        {
            return true;
        }
    }
    return false;   //Cancelled
}


mbm::READ_ERR Console::readModbusRegister(CommandLineInterpreter& cli, mbm::REG_TYPE regtype)
{
    mbm::READ_ERR status = mbm::READ_FAILURE;
    mbm::readRegValueStr* valueStatus; //pointer to returned read status & value
    mbm::devRegInfo reginfo;        //register details from which to read.
    lu_uint16_t retryCounter = 0;   //counter to keep track of no of retries
    bool success = false;           //outcome of the socket operation

    zmq::message_t reply;

    /*creating a ZMQ socket*/
    zmq::socket_t sock(*(ZmqContext::getInstance()), ZMQ_REQ);

    try
    {
        //  Configure socket to not wait at close time
        lu_int32_t linger = 0;
        lu_uint32_t timout = 30000; //ms
        sock.setsockopt (ZMQ_LINGER, &linger, sizeof (linger));
        sock.setsockopt (ZMQ_RCVTIMEO, &timout, sizeof (timout));

        DBG_INFO("Console: Connect to REP socket:%s",MB_HOST_ADDRESS_REP);
        sock.connect(MB_HOST_ADDRESS_REP);
    }
    catch (zmq::error_t& e)
    {
        DBG_ERR("Cannot open sockets: %s",e.what());
        return status;
    }

    if(!askModbusReadParams(cli, reginfo))
    {
        printCancelled(cli);
        return status;
    }

    //this do loop is to try the number of retries
    do
    {
        /* Send ZMQ content*/
        mbm::MsgHeader replyMsgHeader;

        reginfo.regType = regtype;

        /* Send ZMQ header*/
        try
        {
            mbm::MsgHeader head(mbm::MSG_TYPE_READ, mbm::MSG_ID_READ_CMDLINE_C);
            sock.send(&head, sizeof(head), ZMQ_SNDMORE);
            sock.send(&reginfo, sizeof(reginfo), 0);
            success = true;
        }
        catch (zmq::error_t& e)
        {
            DBG_ERR("Cannot send request: %s", e.what());
            success = false;
        }

        if(success)
        {
            //Request sent successfully
            DBG_INFO("Console: <=== send read request for %s",reginfo.toString().c_str());
            lu_int32_t idxxx = 0;

            do
            {
                try
                {
                    success = sock.recv(&reply);
                }
                catch (zmq::error_t& e)
                {
                    DBG_ERR("Cannot receive message: %s",e.what());
                    success = false;
                }

                if(success)
                {
                    if(idxxx == 0) // Header
                    {
                        if(reply.size() != sizeof(replyMsgHeader))
                        {
                            DBG_ERR("Console: Invalid header size:%i expected:%i %s",reply.size(),sizeof(replyMsgHeader),__AT__);
                            break;
                        }
                        else
                        {
                            DBG_INFO("Console: ==> Received reply. size:%i ", reply.size());
                            memcpy(&replyMsgHeader, reply.data(), sizeof(replyMsgHeader));
                        }

                    }
                    else if(idxxx == 1) // Payload
                    {
                        DBG_INFO("Console: ==> Received reply. size:%i ", reply.size());
                        if( (replyMsgHeader.type == mbm::MSG_TYPE_READ ) && ( replyMsgHeader.id == mbm::MSG_ID_READ_CMDLINE_R )
                                        && (reply.size() == sizeof(mbm::readRegValueStr)) )
                        {
                            valueStatus = (mbm::readRegValueStr*)(reply.data());
                            status = valueStatus->readErr;
                        }
                    }
                    idxxx++;
                }
            } while(success && reply.more());
        }
        retryCounter++;

    } while( (status != mbm::READ_SUCCESS) && (retryCounter < reginfo.tries) );//retries if failed

    if(status == mbm::READ_SUCCESS)
    {
        printModbusRegValues(cli, valueStatus->values);
    }
    else
    {
       DBG_ERR("Console: read register error!");

       discardRcvMsg(sock, reply);
       cli.message("Error: Unable to read modbus register [%d, %d]\n", reginfo.deviceId, reginfo.startAddress);
    }

    return status;
}


mbm::WRITE_ERR Console::writeModbusRegister(CommandLineInterpreter& cli,
                                    const mbm::REG_TYPE regtype,
                                    const mbm::devRegInfo& params,
                                    const std::vector<lu_uint16_t>& values
                                    )
{
    mbm::WRITE_ERR status = mbm::WRITE_FAILURE;
    mbm::writeRegValueStr wRegValStr;
    bool success = false;           //outcome of the socket operation
    lu_uint16_t retryCounter = 0;   //counter to keep track of number of retries
    zmq::message_t reply;
    zmq::socket_t sock(*(ZmqContext::getInstance()), ZMQ_REQ);

    try
    {
        //  Configure socket to not wait at close time
        lu_int32_t linger = 0;
        lu_uint32_t timout = 30000; //ms
        sock.setsockopt (ZMQ_LINGER, &linger, sizeof (linger));
        sock.setsockopt (ZMQ_RCVTIMEO, &timout, sizeof (timout));

        DBG_INFO("Console: Connect to REP socket:%s",MB_HOST_ADDRESS_REP);
        sock.connect(MB_HOST_ADDRESS_REP);
    }
    catch (zmq::error_t& e)
    {
        DBG_ERR("Cannot open sockets: %s",e.what());
        return status;
    }

    if(values.size() <= 0)
    {
        printCancelled(cli);
        return mbm::WRITE_FAILURE;
    }

    /* Retry write several times */
    do
    {
        /* Send ZMQ content */
        mbm::MsgHeader replyMsgHeader;
        wRegValStr.devreginfo = params;
        wRegValStr.devreginfo.regType = regtype;
        wRegValStr.devreginfo.quantity = values.size();
        wRegValStr.values = values;

        /* Send ZMQ header*/
        try
        {
            mbm::MsgHeader head(mbm::MSG_TYPE_WRITE, mbm::MSG_ID_WRITE_CMDLINE_C);
            sock.send(&head, sizeof(head), ZMQ_SNDMORE);
            sock.send(&wRegValStr, sizeof(wRegValStr), 0);
            success = true;
        }
        catch (zmq::error_t& e)
        {
            DBG_ERR("Cannot send request: %s",e.what());
            success = false;
        }
        if(success)
        {
            //Request sent successfully
            DBG_INFO("Console: <=== send write request for %s",wRegValStr.devreginfo.toString().c_str());

            lu_int32_t idxxx = 0;

            do
            {
                try
                {
                    success = sock.recv(&reply);
                }
                catch (zmq::error_t& e)
                {
                    DBG_ERR("Cannot receive message: %s",e.what());
                    success = false;
                }

                if (success)
                {
                    if(idxxx == 0) // Header
                    {
                        if(reply.size() != sizeof(replyMsgHeader))
                        {
                            DBG_ERR("Console: Invalid header size:%i expected:%i %s",reply.size(),sizeof(replyMsgHeader),__AT__);
                            break;
                        }
                        else
                        {
                            DBG_INFO("Console: ==> Received reply. size:%i ", reply.size());
                            memcpy(&replyMsgHeader, reply.data(), sizeof(replyMsgHeader));
                        }

                    }
                    else if(idxxx == 1) // Payload
                    {
                        DBG_INFO("Console: ==> Received reply. size:%i ", reply.size());
                        if(replyMsgHeader.type == mbm::MSG_TYPE_WRITE && replyMsgHeader.id == mbm::MSG_ID_WRITE_CMDLINE_R
                                        && reply.size() == sizeof(mbm::WRITE_ERR))
                        {
                            status = *(mbm::WRITE_ERR *)(reply.data());
                        }
                    }
                    idxxx ++;
                 }

            } while(success && reply.more());
        }
        retryCounter++;

    } while( (status != mbm::WRITE_SUCCESS) && (retryCounter < wRegValStr.devreginfo.tries) );//retries if failed


    if(status == mbm::WRITE_SUCCESS)
    {
        cli.message("Success: Write success for modbus device with ID : %d & register address starting at : %d\n", wRegValStr.devreginfo.deviceId, wRegValStr.devreginfo.startAddress);
    }
    else
    {
        DBG_ERR("Console: write register error!");

        discardRcvMsg(sock, reply);
        cli.message("Error: Unable to write modbus device with ID : %d & register address starting at : %d\n", wRegValStr.devreginfo.deviceId, wRegValStr.devreginfo.startAddress);
    }
    return status;
}


bool Console::readModbusStats(mbm::devRegInfo params, mbm::devicestats& stats)
{
    bool status = false;            //function result
    lu_uint16_t retryCounter = 0;   //counter to keep track of number of retries
    bool success = false;           //socket operation result

    if(params.deviceId == 0)
    {
        return false;   //invalid params
    }

    /*creating a ZMQ socket*/
    zmq::message_t reply;
    zmq::socket_t sock(*(ZmqContext::getInstance()), ZMQ_REQ);

    try
    {
        //  Configure socket to not wait at close time
        lu_int32_t linger = 0;
        lu_uint32_t timout = 30000; //ms
        sock.setsockopt(ZMQ_LINGER, &linger, sizeof (linger));
        sock.setsockopt(ZMQ_RCVTIMEO, &timout, sizeof (timout));

        DBG_INFO("Console: Connect to REP socket:%s", MB_HOST_ADDRESS_REP);
        sock.connect(MB_HOST_ADDRESS_REP);
    }
    catch (zmq::error_t& e)
    {
        DBG_ERR("Cannot open sockets: %s", e.what());
        return status;
    }

    //this do loop is to try the number of retries
    do
    {
        /* Send ZMQ content*/
        mbm::MsgHeader replyMsgHeader;

        /* Send ZMQ header*/
        mbm::MsgHeader head(mbm::MSG_TYPE_READ, mbm::MSG_ID_READ_STATS_C);
        try
        {
            sock.send(&head, sizeof(head), ZMQ_SNDMORE);
            sock.send(&params.deviceId, sizeof(params.deviceId), 0);
            success = true;
        }
        catch (zmq::error_t& e)
        {
            DBG_ERR("Cannot send request: %s", e.what());
            success = false;
        }

        if(success)
        {
            DBG_INFO("Console: <=== send stats request for  device %d", params.deviceId);

            lu_int32_t idxxx = 0;

            do
            {
                try
                {
                    success = sock.recv(&reply);
                }
                catch (zmq::error_t& e)
                {
                    DBG_ERR("Cannot receive message: %s",e.what());
                    success = false;
                }
                if(success)
                {
                    if(idxxx == 0) // Header
                    {
                        if(reply.size() != sizeof(replyMsgHeader))
                        {
                            DBG_ERR("Console: Invalid header size:%i expected:%i %s",reply.size(),sizeof(replyMsgHeader),__AT__);
                            break;
                        }
                        else
                        {
                            DBG_INFO("Console: ==> Received reply. size:%i ", reply.size());
                            memcpy(&replyMsgHeader, reply.data(), sizeof(replyMsgHeader));
                        }

                    }
                    else if(idxxx == 1) // Payload
                    {
                        DBG_INFO("Console: ==> Received reply. size:%i ", reply.size());
                        if( (replyMsgHeader.type == mbm::MSG_TYPE_READ) &&
                            (replyMsgHeader.id == mbm::MSG_ID_READ_STATS_R) &&
                            (reply.size() == sizeof(mbm::devicestats))
                            )
                        {
                            stats = *(static_cast<mbm::devicestats*>(reply.data()));
                            status = true;
                        }
                        else if  ( (replyMsgHeader.type == mbm::MSG_TYPE_READ) &&
                                        (replyMsgHeader.id == mbm::MSG_ID_READ_STATS_R) &&
                                        (reply.size() == sizeof(mbm::MsgAck))
                                        )
                        {
                            retryCounter = params.tries;
                        }
                    }
                    idxxx++;
                }

            } while( success && reply.more() );
        }
        retryCounter++;

    } while( !status && (retryCounter < params.tries) );    //retries if failed

    if(!status)
    {
        DBG_ERR("Console: Read Device Statistics error!");

        discardRcvMsg(sock, reply);
    }
    return status;
}


void Console::printModbusRegValues(CommandLineInterpreter& cli, std::vector<lu_uint16_t>& values)
{
    int index = 0 ;
    cli.message("Read value: ");
    for(std::vector<lu_uint16_t>::const_iterator it = values.begin(); it != values.end(); ++it)
    {
        cli.message("Number : %d Value : 0x%02X 0x%02X \n",index, LU_GETBITMASKVALUE((*it), 0xFF00, 8), LU_GETBITMASKVALUE((*it), 0x00FF, 0));
        index++;
    }
    cli.message("\n");
}


/*
 *********************** End of file ******************************************
 */
