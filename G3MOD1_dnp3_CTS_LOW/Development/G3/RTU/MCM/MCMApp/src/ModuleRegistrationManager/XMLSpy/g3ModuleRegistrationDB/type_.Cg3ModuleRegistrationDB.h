#ifndef _ALTOVA_INCLUDED_g3ModuleRegistrationDB_ALTOVA__ALTOVA_Cg3ModuleRegistrationDB
#define _ALTOVA_INCLUDED_g3ModuleRegistrationDB_ALTOVA__ALTOVA_Cg3ModuleRegistrationDB



namespace g3ModuleRegistrationDB
{

class Cg3ModuleRegistrationDB : public TypeBase
{
public:
	g3ModuleRegistrationDB_EXPORT Cg3ModuleRegistrationDB(xercesc::DOMNode* const& init);
	g3ModuleRegistrationDB_EXPORT Cg3ModuleRegistrationDB(Cg3ModuleRegistrationDB const& init);
	void operator=(Cg3ModuleRegistrationDB const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_altova_Cg3ModuleRegistrationDB); }
	MemberElement<Cg3ModuleRegistrationDBType, _altova_mi_altova_Cg3ModuleRegistrationDB_altova_g3ModuleRegistrationDB2> g3ModuleRegistrationDB2;
	struct g3ModuleRegistrationDB2 { typedef Iterator<Cg3ModuleRegistrationDBType> iterator; };
	g3ModuleRegistrationDB_EXPORT void SetXsiType();

	// document functions
	g3ModuleRegistrationDB_EXPORT static Cg3ModuleRegistrationDB LoadFromFile(const string_type& fileName);
	g3ModuleRegistrationDB_EXPORT static Cg3ModuleRegistrationDB LoadFromString(const string_type& xml);
	g3ModuleRegistrationDB_EXPORT static Cg3ModuleRegistrationDB LoadFromBinary(const std::vector<unsigned char>& data);
	g3ModuleRegistrationDB_EXPORT void SaveToFile( const string_type& fileName, bool prettyPrint );
	g3ModuleRegistrationDB_EXPORT void SaveToFile( const string_type& fileName, bool prettyPrint, const string_type& encoding );
	g3ModuleRegistrationDB_EXPORT void SaveToFile( const string_type& fileName, bool prettyPrint, const string_type& encoding, bool bBigEndian, bool bBOM );
	g3ModuleRegistrationDB_EXPORT string_type SaveToString(bool prettyPrint);
	g3ModuleRegistrationDB_EXPORT std::vector<unsigned char> SaveToBinary(bool prettyPrint);
	g3ModuleRegistrationDB_EXPORT std::vector<unsigned char> SaveToBinary(bool prettyPrint, const string_type& encoding);
	g3ModuleRegistrationDB_EXPORT std::vector<unsigned char> SaveToBinary(bool prettyPrint, const string_type& encoding, bool bBigEndian, bool bBOM);
 	g3ModuleRegistrationDB_EXPORT static Cg3ModuleRegistrationDB CreateDocument();
	g3ModuleRegistrationDB_EXPORT void DestroyDocument();
	g3ModuleRegistrationDB_EXPORT void SetDTDLocation(const string_type& dtdLocation);
	g3ModuleRegistrationDB_EXPORT void SetSchemaLocation(const string_type& schemaLocation);
protected:
	XercesTreeOperations::DocumentType GetDocumentNode() { return (XercesTreeOperations::DocumentType)m_node; }
};


}	// namespace g3ModuleRegistrationDB

#endif // _ALTOVA_INCLUDED_g3ModuleRegistrationDB_ALTOVA__ALTOVA_Cg3ModuleRegistrationDB
