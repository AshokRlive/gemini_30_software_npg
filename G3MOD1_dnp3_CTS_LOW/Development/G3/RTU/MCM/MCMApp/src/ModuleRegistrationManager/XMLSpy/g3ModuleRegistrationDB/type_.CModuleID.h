#ifndef _ALTOVA_INCLUDED_g3ModuleRegistrationDB_ALTOVA__ALTOVA_CModuleID
#define _ALTOVA_INCLUDED_g3ModuleRegistrationDB_ALTOVA__ALTOVA_CModuleID



namespace g3ModuleRegistrationDB
{

class CModuleID : public TypeBase
{
public:
	g3ModuleRegistrationDB_EXPORT CModuleID(xercesc::DOMNode* const& init);
	g3ModuleRegistrationDB_EXPORT CModuleID(CModuleID const& init);
	void operator=(CModuleID const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_altova_CModuleID); }

	enum EnumValues {
		Invalid = -1,
		k_0 = 0, // 0
		k_1 = 1, // 1
		k_2 = 2, // 2
		k_3 = 3, // 3
		k_4 = 4, // 4
		k_5 = 5, // 5
		k_6 = 6, // 6
		k_7 = 7, // 7
		EnumValueCount
	};
	void operator= (const unsigned& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::IntegerFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator unsigned()
	{
		return CastAs<unsigned >::Do(GetNode(), 0);
	}
};


}	// namespace g3ModuleRegistrationDB

#endif // _ALTOVA_INCLUDED_g3ModuleRegistrationDB_ALTOVA__ALTOVA_CModuleID
