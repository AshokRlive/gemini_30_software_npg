/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Module Registration Manager Interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   10/01/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_53C7775D_6E2E_41b4_A9ED_E39D96557898__INCLUDED_)
#define EA_53C7775D_6E2E_41b4_A9ED_E39D96557898__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "IIOModuleManager.h"
#include "ModuleRegistrationManagerCommon.h"
#include "ModuleRegistrationDB.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class ModuleRegistrationManager
{

public:
    /**
     * \brief Get an instance
     *
     * \return A ModuleRegistrationManager instance pointer
     */
    static ModuleRegistrationManager *getInstance();

    /**
	 * \brief Commission all modules
	 * 
	 * Save all the modules to the local database
	 * 
	 * \param moduleManager Reference to the module manager
	 * 
	 * \return Error code
	 */
    MRDB_ERROR commissionModules(IIOModuleManager &moduleManager);

    /**
     * \brief De-commission all modules
     *
     * Eliminate all the modules from the local database
     *
     * \param moduleManager Reference to the module manager
     *
     * \return Error code
     */
    MRDB_ERROR decommissionModules();

	/**
	 * \brief Validate a module against the registered list of modules
	 * 
	 * \param module Reference to the module to validate
	 * 
	 * \return Error code
	 */
	MRDB_ERROR validateModule(const IOModuleRef& mRef);

    /**
     * \brief Validate a module against the registered list of modules
     *
     * \param module Module type
     * \param moduleID Module ID
     * \param moduleUID Module unique ID (serial) number
     *
     * \return Error code
     */
    MRDB_ERROR validateModule(MODULE module, MODULE_ID moduleID, ModuleUID moduleUID);

private:
	ModuleRegistrationManager() {};

    ModuleRegistrationDB db;

    static ModuleRegistrationManager *instance;
};
#endif // !defined(EA_53C7775D_6E2E_41b4_A9ED_E39D96557898__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
