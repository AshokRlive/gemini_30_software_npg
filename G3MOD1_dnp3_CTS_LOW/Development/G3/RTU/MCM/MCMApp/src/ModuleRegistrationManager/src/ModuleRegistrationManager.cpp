/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Module Registration Manager Implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   10/01/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "ModuleRegistrationManager.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

ModuleRegistrationManager *ModuleRegistrationManager::instance = NULL;

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

ModuleRegistrationManager *ModuleRegistrationManager::getInstance()
{
    if(instance == NULL)
    {
        instance = new ModuleRegistrationManager;
    }

    return instance;
}

MRDB_ERROR ModuleRegistrationManager::commissionModules(IIOModuleManager &moduleManager)
{
    std::vector<IIOModule*> moduleList;
    moduleList = moduleManager.getAllModules();
    return db.storeModules(moduleList);
}

MRDB_ERROR ModuleRegistrationManager::decommissionModules()
{
    std::vector<IIOModule*> moduleList;
    /* Store empty table of modules. */
    return db.storeModules(moduleList);
}


MRDB_ERROR ModuleRegistrationManager::validateModule(const IOModuleRef& mRef)
{
    return validateModule(mRef.mid.type, mRef.mid.id, mRef.moduleUID);
}

MRDB_ERROR ModuleRegistrationManager::validateModule( MODULE module,
                                                      MODULE_ID moduleID,
                                                      ModuleUID moduleUID
                                                    )
{
    MRDB_ERROR ret;
    ModuleUID dbUID;

    /*Force HMI to be always registered*/
    if(module == MODULE_HMI)
    {
        return MRDB_ERROR_NONE;
    }

    ret = db.getModule(module, moduleID, dbUID);
    if(ret == MRDB_ERROR_NONE)
    {
        if(moduleUID == dbUID)
        {
            ret = MRDB_ERROR_NONE;
        }
        else
        {
            ret = MRDB_ERROR_NOT_FOUND;
        }
    }

    return ret;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
