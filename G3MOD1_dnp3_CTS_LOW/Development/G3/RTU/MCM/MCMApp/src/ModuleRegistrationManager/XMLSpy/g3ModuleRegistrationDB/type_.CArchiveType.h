#ifndef _ALTOVA_INCLUDED_g3ModuleRegistrationDB_ALTOVA__ALTOVA_CArchiveType
#define _ALTOVA_INCLUDED_g3ModuleRegistrationDB_ALTOVA__ALTOVA_CArchiveType



namespace g3ModuleRegistrationDB
{

class CArchiveType : public TypeBase
{
public:
	g3ModuleRegistrationDB_EXPORT CArchiveType(xercesc::DOMNode* const& init);
	g3ModuleRegistrationDB_EXPORT CArchiveType(CArchiveType const& init);
	void operator=(CArchiveType const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_altova_CArchiveType); }
	MemberElement<CCommissionedModulesType, _altova_mi_altova_CArchiveType_altova_record> record;
	struct record { typedef Iterator<CCommissionedModulesType> iterator; };
};


}	// namespace g3ModuleRegistrationDB

#endif // _ALTOVA_INCLUDED_g3ModuleRegistrationDB_ALTOVA__ALTOVA_CArchiveType
