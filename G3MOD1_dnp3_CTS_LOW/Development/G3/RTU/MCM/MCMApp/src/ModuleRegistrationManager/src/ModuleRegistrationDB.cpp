/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Module Registration DB implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   10/01/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <sys/stat.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "ModuleRegistrationDB.h"
#include "TimeManager.h"
#include "LockingMutex.h"

#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>

#include "Altova/Altova.h"
#include "Altova/SchemaTypes.h"
#include "Altova/AltovaException.h"

#include "AltovaXML/XmlException.h"
#include "AltovaXML/Node.h"

#include "g3ModuleRegistrationDB/g3ModuleRegistrationDB.h"

using namespace altova;
using namespace g3ModuleRegistrationDB;

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

const lu_char_t ModuleRegistrationDB::DBFileName[] = "registrationDB.xml";

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

ModuleRegistrationDB::ModuleRegistrationDB() : RWMutex(LU_TRUE),
                                               db(NULL),
                                               log(Logger::getLogger(SUBSYSTEM_ID_MRDB))
{
    /* Initialise parser */
    if( initParser() != MRDB_ERROR_NONE)
    {
        //error in file. Create a new one and try again
        createRegistration();
        initParser();
    }
}



ModuleRegistrationDB::~ModuleRegistrationDB()
{
    releaseParser();
}


MRDB_ERROR ModuleRegistrationDB::storeModules(std::vector<IIOModule*>& moduleList)
{
    MRDB_ERROR ret = MRDB_ERROR_NONE;
    struct timespec timestamp;
    struct tm tempTm;

    if(db == NULL)
    {
        /* parser not initialised/unavailable */
        return MRDB_ERROR_INITPARSER;
    }

    LockingMutex lmutex(RWMutex);

    try
    {
        /* Check the existence of Active and Archive */
        if( (!db->g3ModuleRegistrationDB2.first().Active.exists()) ||
            (!db->g3ModuleRegistrationDB2.first().Archive.exists())
          )
        {
            return MRDB_ERROR_EXCEPTION;
        }

        /* Get a reference for Active and Archive entries */
        CCommissionedModulesType active = db->g3ModuleRegistrationDB2.first().Active.first();
        CArchiveType archive = db->g3ModuleRegistrationDB2.first().Archive.first();

        /* Append a new entry in the Archive section */
        CCommissionedModulesType archiveRecord = archive.record.append();

        /* Get current time */
        TimeManager::getInstance().getTime(&timestamp);

        /* Convert seconds to a broken-down UTC time  */
        gmtime_r(&(timestamp.tv_sec), &tempTm);

        /* Create XML date and time */
        DateTime date( tempTm.tm_year + 1900,
                       tempTm.tm_mon + 1,
                       tempTm.tm_wday + 1,
                       tempTm.tm_hour,
                       tempTm.tm_min,
                       tempTm.tm_sec
                     );

        /* TODO: pueyos_a - [USER] store in RegistrationDB the user that executed the action */
        /* Set the current date and user to the archive record */
        archiveRecord.Date = date;
        archiveRecord.UserAccount = _T("NA");

        /* Set the current date and user to the active record */
        active.Date = date;
        active.UserAccount = _T("NA");

        /* Remove all the modules from the Active section */
        while(active.module.count() > 0)
        {
            active.module.remove();
        }

        /* Add an entry for each module to both the Active section
         * and the last record of the Archive section
         */
        for(std::vector<IIOModule*>::iterator it = moduleList.begin(); it != moduleList.end(); ++it)
        {
            IOModuleInfoStr moduleInfo;
            (*it)->getInfo(moduleInfo);

            /* Add a module only if it's present on Bus */
            if(moduleInfo.status.present == LU_TRUE)
            {
                CmoduleType moduleRecordArchive = archiveRecord.module.append();
                CmoduleType moduleRecordActive = active.module.append();

                moduleRecordArchive.Type = moduleInfo.mid.type;
                moduleRecordActive.Type = moduleInfo.mid.type;

                moduleRecordArchive.Address = moduleInfo.mid.id;
                moduleRecordActive.Address = moduleInfo.mid.id;

                moduleRecordArchive.SupplierID = moduleInfo.moduleUID.supplierId;
                moduleRecordActive.SupplierID = moduleInfo.moduleUID.supplierId;

                moduleRecordArchive.ModuleSerialNumber = moduleInfo.moduleUID.serialNumber;
                moduleRecordActive.ModuleSerialNumber = moduleInfo.moduleUID.serialNumber;

                moduleRecordArchive.ReleaseType = moduleInfo.mInfo.application.software.relType;
                moduleRecordActive.ReleaseType = moduleInfo.mInfo.application.software.relType;

                moduleRecordArchive.SWMajor = moduleInfo.mInfo.application.software.version.major;
                moduleRecordActive.SWMajor = moduleInfo.mInfo.application.software.version.major;

                moduleRecordArchive.SWMinor = moduleInfo.mInfo.application.software.version.minor;
                moduleRecordActive.SWMinor = moduleInfo.mInfo.application.software.version.minor;

                moduleRecordArchive.SWPatch = moduleInfo.mInfo.application.software.patch;
                moduleRecordActive.SWPatch = moduleInfo.mInfo.application.software.patch;
            }
        }

        /* Save to file */
        db->SaveToFile(_T(DBFileName), true);
    }
    catch(...)
    {
        log.error("ModuleRegistrationDB::storeModules Exception raised");

        ret = MRDB_ERROR_EXCEPTION;
    }

    return ret;
}


MRDB_ERROR ModuleRegistrationDB::getModule( MODULE type,
                                            MODULE_ID id,
                                            ModuleUID &moduleUID
                                          )
{
    MRDB_ERROR ret = MRDB_ERROR_NOT_FOUND;

    if(db == NULL)
    {
        /* parser not initialised/unavailable */
        return MRDB_ERROR_INITPARSER;
    }

    /*Force HMI to be always registered*/
    if(type == MODULE_HMI)
    {
        return MRDB_ERROR_NONE;
    }

    LockingMutex lmutex(RWMutex);

    try
    {
        /* check the Active section exists */
        if(!db->g3ModuleRegistrationDB2.first().Active.exists())
        {
            return ret;
        }

        /* Get a reference to the Active section */
        CCommissionedModulesType activeRecord = db->g3ModuleRegistrationDB2.first().Active.first();

        /* Scan through all the modules of the Active section */
        for( Iterator<CmoduleType> itModule = activeRecord.module.all();
             itModule;
             ++itModule
           )
        {
            try
            {
                /* Get current module type and id/address */
                lu_uint32_t mType = itModule.Type;
                MODULE moduleType = static_cast<MODULE>(mType);
                MODULE_ID moduleAddress = static_cast<MODULE_ID>(itModule.Address.GetEnumerationValue());

                if( (type == moduleType   ) &&
                    (id   == moduleAddress)
                  )
                {
                    /* Module found. save its UID */
                    moduleUID.serialNumber = itModule.ModuleSerialNumber;
                    moduleUID.supplierId = itModule.SupplierID;

                    /* Exit the loop. Report item found */
                    ret = MRDB_ERROR_NONE;
                    break;
                }
            }
            catch(...)
            {
                /* Invalid properties. Go the the next one */
            }
        }
    }
    catch(...)
    {
        log.error("ModuleRegistrationDB::getModule Exception raised");

        ret = MRDB_ERROR_EXCEPTION;
    }

    return  ret;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

MRDB_ERROR ModuleRegistrationDB::initParser()
{
    const lu_char_t* FTITLE = "ModuleRegistrationDB::initParser:";

    if(db != NULL)
    {
        return MRDB_ERROR_INITIALIZED;
    }

    try
    {
        log.info("%s Initialising module registration database parser", FTITLE);
        /* Initialise xercesc parser*/
        xercesc::XMLPlatformUtils::Initialize();

        /* Check if the db file exists. If it doesn't exist create and save a
         * template DB file
         */
        struct stat buffer;
        int status = stat(DBFileName, &buffer);
        if(status < 0)
        {
            /* File doesn't exists */
            createRegistration();
        }

        /* Load the configuration file */
        db = new Cg3ModuleRegistrationDB(Cg3ModuleRegistrationDB::LoadFromFile(_T(DBFileName)));

    }
    catch (CXmlException& e)
    {
        log.error("%s %s", FTITLE, e.GetInfo().c_str());
        return MRDB_ERROR_INITPARSER;
    }
    catch (xercesc::XMLException& e)
    {
        log.error("%s Xerces XMLException: %s: %s", FTITLE, e.getSrcFile(), e.getMessage());
        return MRDB_ERROR_INITPARSER;
    }
    catch (xercesc::DOMException& e)
    {
        log.error("%s Xerces DOMException %i: %s", FTITLE, e.code, e.msg);
        return MRDB_ERROR_INITPARSER;
    }
    catch (altova::Exception& exception)
    {
        log.error("%s Alt_Exception: %s", FTITLE, exception.message().c_str());
        return MRDB_ERROR_INITPARSER;
    }
    catch (altova::Error& exception)
    {
        log.error("%s Alt_Error: %s", FTITLE, exception.message().c_str());
        return MRDB_ERROR_INITPARSER;
    }
    catch (std::exception& e)
    {
        log.error("%s Exception:  %s", FTITLE, e.what());
        return MRDB_ERROR_INITPARSER;
    }
    catch (...)
    {
        log.error("%s Unknown error", FTITLE);
        return MRDB_ERROR_INITPARSER;
    }

    return  MRDB_ERROR_NONE;
}


MRDB_ERROR ModuleRegistrationDB::releaseParser()
{
    if(db != NULL)
    {
        try
        {
            /* Destroy the configuration file handler */
            db->DestroyDocument();

            /* Close the xercesc parser */
            xercesc::XMLPlatformUtils::Terminate();

            /* Free configuration handler */
            delete db;
            db = NULL;
        }
        catch(...)
        {
            log.error("ModuleRegistrationDB::releaseParser Exception raised");
        }
    }

    return MRDB_ERROR_NONE;
}


MRDB_ERROR ModuleRegistrationDB::createRegistration()
{
    try
    {
        /* Create a temporary template DB */
        Cg3ModuleRegistrationDB dbTemplate = Cg3ModuleRegistrationDB::CreateDocument();

        /* Create root element */
        Cg3ModuleRegistrationDBType root = dbTemplate.g3ModuleRegistrationDB2.append();

        /* Add empty <active> entry  */
        root.Active.append();

        /* Add empty <archive> entry  */
        root.Archive.append();

        /* Save XML document */
        dbTemplate.SaveToFile(_T(DBFileName), true);

        /* Destroy template */
        dbTemplate.DestroyDocument();
    }
    catch (CXmlException& e)
    {
        log.error("%s %s", __AT__, e.GetInfo().c_str());
        return MRDB_ERROR_INITPARSER;
    }
    catch (xercesc::XMLException& e)
    {
        log.error("%s Xerces XMLException: %s: %s", __AT__, e.getSrcFile(), e.getMessage());
        return MRDB_ERROR_INITPARSER;
    }
    catch (xercesc::DOMException& e)
    {
        log.error("%s Xerces DOMException %i: %s", __AT__, e.code, e.msg);
        return MRDB_ERROR_INITPARSER;
    }
    catch (altova::Exception& exception)
    {
        log.error("%s Alt_Exception: %s", __AT__, exception.message().c_str());
        return MRDB_ERROR_INITPARSER;
    }
    catch (altova::Error& exception)
    {
        log.error("%s Alt_Error: %s", __AT__, exception.message().c_str());
        return MRDB_ERROR_INITPARSER;
    }
    catch (std::exception& e)
    {
        log.error("%s Exception:  %s", __AT__, e.what());
        return MRDB_ERROR_INITPARSER;
    }
    catch(...)
    {
        log.error("ModuleRegistrationDB::createRegistration Exception raised");
    }
    return MRDB_ERROR_NONE;
}


/*
 *********************** End of file ******************************************
 */
