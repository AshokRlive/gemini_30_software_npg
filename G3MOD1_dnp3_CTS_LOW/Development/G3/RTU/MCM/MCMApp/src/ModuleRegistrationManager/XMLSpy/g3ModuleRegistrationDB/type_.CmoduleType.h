#ifndef _ALTOVA_INCLUDED_g3ModuleRegistrationDB_ALTOVA__ALTOVA_CmoduleType
#define _ALTOVA_INCLUDED_g3ModuleRegistrationDB_ALTOVA__ALTOVA_CmoduleType



namespace g3ModuleRegistrationDB
{

class CmoduleType : public TypeBase
{
public:
	g3ModuleRegistrationDB_EXPORT CmoduleType(xercesc::DOMNode* const& init);
	g3ModuleRegistrationDB_EXPORT CmoduleType(CmoduleType const& init);
	void operator=(CmoduleType const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_altova_CmoduleType); }

	MemberAttribute<int,_altova_mi_altova_CmoduleType_altova_Type, 0, 0> Type;	// Type CModuleTypes
	MemberAttribute<unsigned,_altova_mi_altova_CmoduleType_altova_Address, 0, 8> Address;	// Address CModuleID

	MemberAttribute<unsigned,_altova_mi_altova_CmoduleType_altova_SupplierID, 0, 0> SupplierID;	// SupplierID CunsignedInt

	MemberAttribute<unsigned,_altova_mi_altova_CmoduleType_altova_ModuleSerialNumber, 0, 0> ModuleSerialNumber;	// ModuleSerialNumber CunsignedInt

	MemberAttribute<unsigned,_altova_mi_altova_CmoduleType_altova_SWMajor, 0, 0> SWMajor;	// SWMajor CunsignedInt

	MemberAttribute<unsigned,_altova_mi_altova_CmoduleType_altova_SWMinor, 0, 0> SWMinor;	// SWMinor CunsignedInt

	MemberAttribute<unsigned,_altova_mi_altova_CmoduleType_altova_SWPatch, 0, 0> SWPatch;	// SWPatch CunsignedInt

	MemberAttribute<unsigned,_altova_mi_altova_CmoduleType_altova_ReleaseType, 0, 0> ReleaseType;	// ReleaseType CunsignedByte
	g3ModuleRegistrationDB_EXPORT void SetXsiType();
};


}	// namespace g3ModuleRegistrationDB

#endif // _ALTOVA_INCLUDED_g3ModuleRegistrationDB_ALTOVA__ALTOVA_CmoduleType
