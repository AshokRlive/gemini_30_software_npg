#ifndef _ALTOVA_INCLUDED_g3ModuleRegistrationDB_ALTOVA_xs_ALTOVA_CgMonth
#define _ALTOVA_INCLUDED_g3ModuleRegistrationDB_ALTOVA_xs_ALTOVA_CgMonth



namespace g3ModuleRegistrationDB
{

namespace xs
{	

class CgMonth : public TypeBase
{
public:
	g3ModuleRegistrationDB_EXPORT CgMonth(xercesc::DOMNode* const& init);
	g3ModuleRegistrationDB_EXPORT CgMonth(CgMonth const& init);
	void operator=(CgMonth const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_xs_altova_CgMonth); }
	void operator= (const altova::DateTime& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::GMonthFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator altova::DateTime()
	{
		return CastAs<altova::DateTime >::Do(GetNode(), 0);
	}
};



} // namespace xs

}	// namespace g3ModuleRegistrationDB

#endif // _ALTOVA_INCLUDED_g3ModuleRegistrationDB_ALTOVA_xs_ALTOVA_CgMonth
