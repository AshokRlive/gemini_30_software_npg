/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Module Registration DB interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   10/01/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_D4B92838_C18D_4323_A2C0_7EE8A1F375AC__INCLUDED_)
#define EA_D4B92838_C18D_4323_A2C0_7EE8A1F375AC__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <iostream>
#include <string>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Mutex.h"
#include "Logger.h"
#include "IIOModule.h"
#include "ModuleRegistrationManagerCommon.h"

namespace g3ModuleRegistrationDB
{
    class Cg3ModuleRegistrationDB;
}


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * This is the registration database. It stores/retrieves the available modules to
 * a file stored in the local filesystem. The file is a XML, CRC protected, file.
 */
class ModuleRegistrationDB
{

public:
    ModuleRegistrationDB();
    virtual ~ModuleRegistrationDB();

    /**
     * \brief Store the available modules to the filesystem
     *
     * Information saved:
     *  - Module type
     *  - Module Address
     *  - Module UID, stored separately in:
     *      - Supplier ID
     *      - Serial Number
     *  - SW Version
     *  - Registration data
     *
     * \param moduleList List if the modules to save
     *
     * \return Error code
     */
    MRDB_ERROR storeModules(std::vector<IIOModule*>& moduleList);

    /**
     * \brief Retrieve the list of registered modules from the filesystem
     *
     * \param type Type of the module we want to retrieve information
     * \param id ID of the module we want to retrieve information
     * \param moduleUID Where the module unique ID (serial) info is stored
     *
     * \return Error code
     */
    MRDB_ERROR getModule(MODULE type, MODULE_ID id, ModuleUID &moduleUID);

private:
    /**
     * Mutex used to synchronise R/W access to the DB
     */
    Mutex RWMutex;

    /* Parser descriptor */
    g3ModuleRegistrationDB::Cg3ModuleRegistrationDB *db;
    Logger& log;

    static const lu_char_t DBFileName[];

    /**
     * \brief Initialise the parser
     *
     * This function must be called before configuring the system
     *
     * \param configName Pointer to the configuration file name
     *
     * \return Error code
     */
    MRDB_ERROR initParser();

    /**
     * \brief Release the parser resources
     *
     * This method is automatically called when the class is destroyed
     *
     * \return Error code
     */
    MRDB_ERROR releaseParser();

    MRDB_ERROR createRegistration();

};
#endif // !defined(EA_D4B92838_C18D_4323_A2C0_7EE8A1F375AC__INCLUDED_)


/*
 *********************** End of file ******************************************
 */
