#ifndef _ALTOVA_INCLUDED_g3ModuleRegistrationDB_ALTOVA__ALTOVA_CModuleTypes
#define _ALTOVA_INCLUDED_g3ModuleRegistrationDB_ALTOVA__ALTOVA_CModuleTypes



namespace g3ModuleRegistrationDB
{

class CModuleTypes : public TypeBase
{
public:
	g3ModuleRegistrationDB_EXPORT CModuleTypes(xercesc::DOMNode* const& init);
	g3ModuleRegistrationDB_EXPORT CModuleTypes(CModuleTypes const& init);
	void operator=(CModuleTypes const& other) { m_node = other.m_node; }
	static altova::meta::SimpleType StaticInfo() { return altova::meta::SimpleType(types + _altova_ti_altova_CModuleTypes); }
	void operator= (const int& value) 
	{
		altova::XmlFormatter* Formatter = static_cast<altova::XmlFormatter*>(altova::IntegerFormatter);
		XercesTreeOperations::SetValue(GetNode(), Formatter->Format(value));
	}	
		
	operator int()
	{
		return CastAs<int >::Do(GetNode(), 0);
	}
};


}	// namespace g3ModuleRegistrationDB

#endif // _ALTOVA_INCLUDED_g3ModuleRegistrationDB_ALTOVA__ALTOVA_CModuleTypes
