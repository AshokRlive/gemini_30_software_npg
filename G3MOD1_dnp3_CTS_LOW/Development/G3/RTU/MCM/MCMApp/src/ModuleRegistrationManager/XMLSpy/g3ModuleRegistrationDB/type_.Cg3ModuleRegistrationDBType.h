#ifndef _ALTOVA_INCLUDED_g3ModuleRegistrationDB_ALTOVA__ALTOVA_Cg3ModuleRegistrationDBType
#define _ALTOVA_INCLUDED_g3ModuleRegistrationDB_ALTOVA__ALTOVA_Cg3ModuleRegistrationDBType



namespace g3ModuleRegistrationDB
{

class Cg3ModuleRegistrationDBType : public TypeBase
{
public:
	g3ModuleRegistrationDB_EXPORT Cg3ModuleRegistrationDBType(xercesc::DOMNode* const& init);
	g3ModuleRegistrationDB_EXPORT Cg3ModuleRegistrationDBType(Cg3ModuleRegistrationDBType const& init);
	void operator=(Cg3ModuleRegistrationDBType const& other) { m_node = other.m_node; }
	static altova::meta::ComplexType StaticInfo() { return altova::meta::ComplexType(types + _altova_ti_altova_Cg3ModuleRegistrationDBType); }
	MemberElement<CCommissionedModulesType, _altova_mi_altova_Cg3ModuleRegistrationDBType_altova_Active> Active;
	struct Active { typedef Iterator<CCommissionedModulesType> iterator; };
	MemberElement<CArchiveType, _altova_mi_altova_Cg3ModuleRegistrationDBType_altova_Archive> Archive;
	struct Archive { typedef Iterator<CArchiveType> iterator; };
};


}	// namespace g3ModuleRegistrationDB

#endif // _ALTOVA_INCLUDED_g3ModuleRegistrationDB_ALTOVA__ALTOVA_Cg3ModuleRegistrationDBType
