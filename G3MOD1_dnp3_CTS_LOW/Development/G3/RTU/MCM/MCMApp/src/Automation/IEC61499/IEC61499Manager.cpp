/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:IEC61499Manager.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14 Jul 2017     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <fstream>
#include <sys/stat.h>   // chmod
#include <stdlib.h>     // strtol
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "IEC61499Manager.h"
#include "Debug.h"
#include "Logger.h"
#include "Base64.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
const lu_char_t* FORTE_PROC_NAME    = "forte";
const lu_char_t* FORTE_BIN_FILE     = "bin/forte";
const lu_char_t* FORTE_BOOT_FILE    = "forte.fboot";

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
IEC61499Manager::IEC61499Manager(IAutomationDatabase* db):AutomationManager(db)
{
    killForteProcess();
}


lu_bool_t IEC61499Manager::start()
{
    /* Start forte if file exists */
    if( access( FORTE_BIN_FILE, F_OK ) != -1 ) {
        DBG_INFO("=== Start forte ===");

        /* Change file permission*/
        char mode[]= "0755";
        int i;
        i = strtol(mode, 0, 8);
        int result = chmod("./bin/forte", i);
        if(result != 0)
        {
            Logger::getLogger(SUBSYSTEM_ID_AUTO_SCHEME)
            .error("Cannot change forte file permission");
        }

        /*if FORTE is not strated*/
        //if(system("pidof forte"))
        {
        	killForteProcess();
        }
        //if forte is not running
        if(system("pidof forte"))
        {
			/* Run forte as a separate process */
			system("nice -10 ./bin/forte &");

			/* To check that forte is started*/
			while(system("pidof forte"))
			{
				system("sleep 1");
			}
			DBG_INFO(" forte pid =%d",system("pidof forte"));
        }

        /* Auto fail if boot file not found*/
        if(access( FORTE_BOOT_FILE, F_OK ) != 0)
            setAutoFailed(true); //TODO add hook to engine to check auto fail
        else
            setAutoRunning(true); //TODO add monitor to monitor auto running state

    }
    else
    {
        /*Forte file not exist*/
        Logger::getLogger(SUBSYSTEM_ID_AUTO_SCHEME)
        .error("Forte not running. \"%s\" not found!",
                        FORTE_BIN_FILE);
        setAutoRunning(false);
        setAutoFailed(true);
    }

    return true;
}

lu_bool_t IEC61499Manager::stop()
{
    setAutoRunning(false);
    setAutoFailed(false);
    return killForteProcess();
}

lu_bool_t IEC61499Manager::killForteProcess()
{
    int val =0;

    DBG_INFO("=== Stop forte ===");

    /* Wait till the fore process is terminated.*/
    do{
         val = system("killall -9 forte");
         DBG_INFO("=== Stop System return = %d", val);
         //sleep(1);
         system("sleep 1");
    //}while(!val);
    }while(!system("pidof forte"));
     return 1;
}


void IEC61499Manager::writeForteBootFile(std::string& base64Str)
{
    /* Remove before write */
    remove(FORTE_BOOT_FILE);

    if(base64Str.size() > 1)
    {
        std::ofstream out(FORTE_BOOT_FILE);
        out << base64_decode(base64Str);
        out.close();
    }
}
/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
#if 0
pid_t proc_find(const char* name)
{
    DIR* dir;
    struct dirent* ent;
    char* endptr;
    char buf[512];

    if (!(dir = opendir("/proc"))) {
        perror("can't open /proc");
        return -1;
    }

    while((ent = readdir(dir)) != NULL) {
        /* if endptr is not a null character, the directory is not
         * entirely numeric, so ignore it */
        long lpid = strtol(ent->d_name, &endptr, 10);
        if (*endptr != '\0') {
            continue;
        }

        /* try to open the cmdline file */
        snprintf(buf, sizeof(buf), "/proc/%ld/cmdline", lpid);
        FILE* fp = fopen(buf, "r");

        if (fp) {
            if (fgets(buf, sizeof(buf), fp) != NULL) {
                /* check the first token in the file, the program name */
                char* first = strtok(buf, " ");
                if (!strcmp(first, name)) {
                    fclose(fp);
                    closedir(dir);
                    return (pid_t)lpid;
                }
            }
            fclose(fp);
        }

    }

    closedir(dir);
    return -1;
}
#endif
/*
 *********************** End of file ******************************************
 */
