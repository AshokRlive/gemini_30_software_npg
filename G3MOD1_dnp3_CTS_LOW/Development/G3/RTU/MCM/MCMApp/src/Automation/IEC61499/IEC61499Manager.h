/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:IEC61499Manager.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14 Jul 2017     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MCMAPP_SRC_AUTOMATION_IEC61499_IEC61499MANAGER_H_
#define MCMAPP_SRC_AUTOMATION_IEC61499_IEC61499MANAGER_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "../include/AutomationManager.h"
#include "lu_types.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class IEC61499Manager:public AutomationManager
{
    public:
       static void writeForteBootFile(std::string& base64Str);

    public:
        IEC61499Manager(IAutomationDatabase* db);
        virtual ~IEC61499Manager() {}

        /** Inherit from AutomationManager*/
        virtual lu_bool_t start() ;
        virtual lu_bool_t stop() ;

    private:
        lu_bool_t killForteProcess();
};

#endif /* MCMAPP_SRC_AUTOMATION_IEC61499_IEC61499MANAGER_H_ */

/*
 *********************** End of file ******************************************
 */
