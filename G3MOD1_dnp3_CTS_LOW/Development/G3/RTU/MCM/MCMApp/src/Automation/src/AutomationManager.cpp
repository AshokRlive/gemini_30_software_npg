/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:AbstractAutomationManager.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14 Jul 2017     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "GlobalDefs.h"
#include "../include/AutomationManager.h"

#include "MCMLED.h"
#include "AutomationLogic.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
/* LED status*/
#define AUTOMATION_LED_SUPPORT      1

#if AUTOMATION_LED_SUPPORT
#define LED_OFF                     MCMLED::LED_STATUS_OFF
#define LED_SOLID_RED               MCMLED::LED_STATUS_SOLID_1
#define LED_FLASH_GREEN             MCMLED::LED_STATUS_SLOWFLASH_2
#define LED_ALTERNATE_RED_GREEN     MCMLED::LED_STATUS_ALTERNATE
#endif

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
 LEDController AutomationManager::LED_CONTROLLER(MCMLED::getMCMLED(MCMLED::LED_ID_AUTO));

 static lu_uint8_t autoDebugEnabled = 0; // Disabled by default

 static AutomationManager* pAutoManager = NULL;

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
AutomationManager::AutomationManager(IAutomationDatabase* db):
        m_autoRunning(LU_FALSE),
         m_autoFailed(LU_FALSE),
         m_autoInhibited(LU_FALSE),
         mp_db(db)
{
    pAutoManager = this;
}

AutomationManager::~AutomationManager()
{
}

void AutomationManager::init()
{
    setAutoRunning(LU_FALSE);
    setAutoFailed(LU_FALSE);
}

void setAutomationDebugMode(bool enabled)
{
    if(enabled)
    {
#if FIREWALL_SUPPORT
        system("/etc/init.d/firewall stop &");
#endif
        autoDebugEnabled = 1;
    }
    else
    {
#if FIREWALL_SUPPORT
        system("/etc/init.d/firewall start &");
#endif
        autoDebugEnabled = 0;
    }

    if(pAutoManager != NULL)
    {
        pAutoManager->updateLEDStatus();
    }

    // Log event
    AutomationLogic::logEvent(enabled ? EVENT_TYPE_AUTO_DEBUG_ENABLED : EVENT_TYPE_AUTO_DEBUG_DISABLED);
}

lu_bool_t isAutomationDebugMode()
{
    return autoDebugEnabled > 0 ? LU_TRUE : LU_FALSE;
}

lu_bool_t isAutomationRunning()
{
    return pAutoManager == NULL ? 0 : pAutoManager->isAutomationRunning();
}

lu_bool_t isAutomationFailed()
{
    return pAutoManager == NULL ? 0 : pAutoManager->isAutomationFailed();
}

lu_bool_t isAutomationInhibited()
{
    return pAutoManager == NULL ? 0 : pAutoManager->isAutomationInhibited();
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void AutomationManager::setAutoRunning(lu_bool_t value)
{
    m_autoRunning = value;
    updateLEDStatus();
    getDatabase().setPointValue(ENGINE_POINT_INDEX_RUNNING, value, true);

    if(value)
        AutomationLogic::logEvent(EVENT_TYPE_AUTO_RUNNING);
}

void AutomationManager::setAutoFailed(lu_bool_t value)
{
    m_autoFailed = value;
    updateLEDStatus();
    getDatabase().setPointValue(ENGINE_POINT_INDEX_FAIL, value,true);

    if(value)
        AutomationLogic::logEvent(EVENT_TYPE_AUTO_FAIL);
}

void AutomationManager::setAutoDebugging(lu_bool_t value)
{
    setAutomationDebugMode(value);
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

void AutomationManager::updateLEDStatus()
{
#if AUTOMATION_LED_SUPPORT
     MCMLED::LED_STATUS status;

     if(m_autoRunning == 0)
     {
         status = m_autoFailed == 0 ? LED_OFF : LED_SOLID_RED;
     }
     else
     {
         status = m_autoFailed == 0 ? LED_FLASH_GREEN : LED_SOLID_RED;
     }

     if(isAutomationDebugMode() && m_autoRunning)
     {
         status = LED_ALTERNATE_RED_GREEN;
     }

     LED_CONTROLLER.setStatus(status);
#endif
}




/*
 *********************** End of file ******************************************
 */
