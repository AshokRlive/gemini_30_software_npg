/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:AutomationEngineThread.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   29 Apr 2016     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "IEC61131EngineThread.h"
extern "C" {
#include "AutomationScheme.h"
#include "AutomationEngine.h"
#include "LoggerInterface.h"
}

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
#define AUTO_LIB_DIR "lib/automation/"
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
IEC61131EngineThread::~IEC61131EngineThread()
{
    stop();

    if (mp_context != NULL)
    {
        automationengine_de_init((AutomationContext*)mp_context);
        mp_context = NULL;
    }

    setRunningStatus(LU_FALSE);
}



THREAD_ERR IEC61131EngineThread::stop()
{
    return Thread::stop();
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void IEC61131EngineThread::threadBody()
{
    lu_int32_t error = 0;
    std::string libPath(m_manager.getDatabase().getLibName());
    libPath = AUTO_LIB_DIR + libPath;

    /* Initialise engine context*/
    lu_bool_t initialised = automationengine_init(mp_context, libPath.c_str());

    if(initialised == LU_FALSE)
    {
        logger_err("Failed to initialise automation context.");
        setFailingStatus(LU_TRUE);
        return;
    }

    /* Register required callbacks */
    error = (mp_context->registerCallback)(AS_CALLBACK_ID_GET_CONSTANT,
                 (void*) IEC61131Manager::callbackGetConstant)|error;
    error = (mp_context->registerCallback)(AS_CALLBACK_ID_GET_INPUT,
                 (void*) IEC61131Manager::callbackGetInput)|error;
    error = (mp_context->registerCallback)(AS_CALLBACK_ID_SET_POINT,
                 (void*) IEC61131Manager::callbackSetPoint)|error;
    error = (mp_context->registerCallback)(AS_CALLBACK_ID_OPERATE,
                    (void*) IEC61131Manager::callbackOperate)|error;

    /* Update status*/
    setRunningStatus(LU_TRUE);
    setFailingStatus(error == 0 ? LU_FALSE : LU_TRUE);

    /* Run automation scheme*/
    error = automationengine_run(mp_context)|error;

    /* Update status*/
    setRunningStatus(LU_FALSE);
    setFailingStatus(error == 0 ? LU_FALSE : LU_TRUE);

    /*Clean before exit*/
    automationengine_de_init(mp_context);
    mp_context = NULL;

}
/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

void IEC61131EngineThread::setRunningStatus(lu_bool_t running)
{
    logger_info("Auto Running:%d", running);
    m_manager.setAutoRunning(running);
}

void IEC61131EngineThread::setFailingStatus(lu_bool_t failing)
{
    logger_info("Auto Fail:%d", failing);
    m_manager.setAutoFailed(failing);
}
/*
 *********************** End of file ******************************************
 */
