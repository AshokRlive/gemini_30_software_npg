/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:AutomationDebugThread.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   29 Apr 2016     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef AUTOMATIONDEBUGTHREAD_H_
#define AUTOMATIONDEBUGTHREAD_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "IEC61131Manager.h"
#include "lu_types.h"
#include "Thread.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class IEC61131DebugThread: public Thread
{
public:
    IEC61131DebugThread(IEC61131Manager& manager,void* autoctxtPtr, void** dbgctxtPtr)
            :Thread(SCHED_TYPE_FIFO, Thread::PRIO_MIN + 26, LU_TRUE, "AutomationDebugThread"),
             m_manager(manager),
             mp_dbgctxt(dbgctxtPtr),
             mp_autoctxt(autoctxtPtr)
             {}

    virtual ~IEC61131DebugThread();

    IEC61131Manager& getManager()
    {
        return m_manager;
    }
    void* getAutoContext()
    {
        return mp_autoctxt;
    }

protected:
    virtual void threadBody();
private:
    /*Implementation of debugsocket_connchg_handler*/
    static void connchg_handler(void* dbgContext, lu_int32_t existingConnNum);

    IEC61131Manager& m_manager;
    void** mp_dbgctxt;
    void* mp_autoctxt;

};

#endif /* AUTOMATIONDEBUGTHREAD_H_ */

/*
 *********************** End of file ******************************************
 */
