/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:AutomationEngineThread.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   29 Apr 2016     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef AUTOMATIONENGINETHREAD_H_
#define AUTOMATIONENGINETHREAD_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "IEC61131Manager.h"
#include "lu_types.h"
#include "Thread.h"
extern "C" {
    #include "AutomationEngine.h"
}
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */


class IEC61131EngineThread: public Thread
{
public:
    IEC61131EngineThread(AutomationContext* context, IEC61131Manager& manager)
            :Thread(SCHED_TYPE_FIFO, Thread::PRIO_MIN + 26, LU_TRUE, "AutomationEngineThread"),
             m_manager(manager),
             mp_context(context){}

    virtual ~IEC61131EngineThread();

    virtual THREAD_ERR stop();
protected:
    virtual void threadBody();

private:
    void setRunningStatus(lu_bool_t running);
    void setFailingStatus(lu_bool_t hasError);

private:
    IEC61131Manager& m_manager;
    AutomationContext* mp_context;

};
#endif /* AUTOMATIONENGINETHREAD_H_ */

/*
 *********************** End of file ******************************************
 */

