/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:IEC61499Logic.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14 Jul 2017     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "IEC61499Logic.h"
#include "IEC61499Manager.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
extern g3db::G3DBServer g_dbserver;

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
IEC61499Logic::IEC61499Logic(
                Mutex* mutex,
                GeminiDatabase& database,
                AutomationLogic::Config& config
                )
:AutomationLogic(mutex,database,config,CONTROL_LOGIC_TYPE_IEC61499,
                new IEC61499Manager(this))
{
    g_dbserver.registerHandler(this);
}

IEC61499Logic::~IEC61499Logic()
{
    g_dbserver.deregisterHandler(this);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void IEC61499Logic::handleInputChange(lu_uint32_t index, POINT_TYPE type, PointData *newData)
{
    AutomationLogic::handleInputChange(index, type, newData);

    /* Publish input change to G3DBClient.*/
    if(index >= IEC61499Manager::ENGINE_INPUT_NUM)
    {
        index = index - IEC61499Manager::ENGINE_INPUT_NUM;
        g3db::DBEvent publish;
        g3db::InputEvent& evt = publish.inputEvent;
        TimeManager::TimeStr ts;

        publish.eventId = g3db::DB_EVENT_ID_INPUT_UPDATE;
        strncpy(publish.databaseId, getDBId(), g3db::DB_ID_LENGHT);

        // Event point ID
        evt.id = index;

        // Event  flags
        evt.quality.online = newData->getFlags().isOnline();

        // Timestamp
        newData->getTime(ts);
        evt.timestamp.value.tv_sec = ts.time.tv_sec;
        evt.timestamp.value.tv_nsec = ts.time.tv_nsec;

        // Event value
        switch(type)
        {
            case POINT_TYPE_ANALOGUE:
            {
                evt.value.type = g3db::VALUE_TYPE_ANALOGUE;
                PointDataFloat32* avalue = (PointDataFloat32*)(newData);
                evt.value.analogue = *avalue;
            }
                break;

            case POINT_TYPE_BINARY:
            case POINT_TYPE_DBINARY:
            {
                PointDataUint8* bvalue = (PointDataUint8*)(newData);
                evt.value.digital = *bvalue;
                evt.value.type = g3db::VALUE_TYPE_DIGITAL;
            }
                break;

            case POINT_TYPE_COUNTER:
            {
                PointDataCounter32* cvalue = (PointDataCounter32*)(newData);
                evt.value.digital = *cvalue;
                evt.value.type = g3db::VALUE_TYPE_DIGITAL;
            }
                break;

            default:
                evt.value.type = g3db::VALUE_TYPE_INVALID;
        }

        g_dbserver.publish(publish);

    }
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
lu_bool_t IEC61499Logic::handle(g3db::DBQuery& query)
{
    if(strcmp(getDBId(), query.databaseId) != 0)
    {
        /* Do not handle the query that is not for this database*/
        return LU_FALSE;
    }


    /*Get input value*/
    if(query.reqId == g3db::DB_REQUEST_ID_GET_INPUT)
    {
        static TimeManager::TimeStr ts;
        static PointDataFloat32   fval;
        static PointDataCounter32   cval;
        static PointDataUint8       dval;

        PointData* data;
        POINT_TYPE type;
        lu_uint16_t index = query.ref.id + IEC61499Manager::ENGINE_INPUT_NUM;

        type = getInputType(index);
        switch(type)
        {
            case POINT_TYPE_ANALOGUE :data = &fval;break;
            case POINT_TYPE_BINARY   :
            case POINT_TYPE_DBINARY  :data = &dval;break;
            case POINT_TYPE_COUNTER  :data = &cval;break;
            default                  :data = NULL; break;
        }

        if(getInputValue(index, data) == GDB_ERROR_NONE)
        {
            if(query.value.type == g3db::VALUE_TYPE_ANALOGUE)
                query.value.analogue = *data;
            else
                query.value.digital = *data;

            query.reqStatus = g3db::DB_REQUEST_STATUS_OK;
            query.quality.online = data->getFlags().isOnline();
            data->getTime(ts);
            query.timestamp.value = ts.time;
        }
        else
        {
            query.reqStatus = g3db::DB_REQUEST_STATUS_DB_ERR;
            query.value.type = g3db::VALUE_TYPE_INVALID;
        }
    }

    /*Set output value*/
    else if(query.reqId == g3db::DB_REQUEST_ID_SET_OUTPUT)
    {
        lu_uint16_t index = query.ref.id + IEC61499Manager::ENGINE_POINT_NUM;
        if(setPointValue(index,
                        query.value.digital, query.quality.online) == GDB_ERROR_NONE)
        {
          query.reqStatus = g3db::DB_REQUEST_STATUS_OK;
        }
        else
        {
          query.reqStatus = g3db::DB_REQUEST_STATUS_DB_ERR;
          query.value.type = g3db::VALUE_TYPE_INVALID;
        }

    }

    /*Get constant value*/
    else if(query.reqId == g3db::DB_REQUEST_ID_GET_CONSTANT)
    {
        lu_int32_t val;
        if(getConstantValue(query.ref.id, val) == GDB_ERROR_NONE)
        {
            query.reqStatus = g3db::DB_REQUEST_STATUS_OK;
            query.value.type = g3db::VALUE_TYPE_DIGITAL;
            query.value.digital = val;
        }
        else
        {
            query.value.type = g3db::VALUE_TYPE_INVALID;
            query.reqStatus = g3db::DB_REQUEST_STATUS_DB_ERR;
        }

    }

    /*Operate control */
    else if(query.reqId == g3db::DB_REQUEST_ID_CONTROL)
    {
        if(operateOutput(query.ref.id, query.value.digital) == GDB_ERROR_NONE)
            query.reqStatus = g3db::DB_REQUEST_STATUS_OK;
        else
            query.reqStatus = g3db::DB_REQUEST_STATUS_DB_ERR;
    }

    return LU_TRUE;
}
/*
 *********************** End of file ******************************************
 */
