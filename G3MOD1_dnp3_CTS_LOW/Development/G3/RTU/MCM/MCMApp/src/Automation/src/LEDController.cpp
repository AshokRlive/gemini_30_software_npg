/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:AutomationLEDController.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17 May 2016     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <unistd.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "../../Automation/include/LEDController.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define LED_RED    0
#define LED_GREEN  1

#define ON  0
#define OFF 1

#define FLASH_RATE_SLOW  1000000  //microsecond
#define FLASH_RATE_FLAST  250000  //microsecond

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
typedef struct
{
  lu_bool_t      state[2];  // the ON and OFF state definitions. Add more states if required
} ledTable;


static ledTable LED1[] =
{
  {OFF, OFF},// LED_STATUS_OFF
  {ON,  ON },// LED_STATUS_SOLID_1
  {OFF, ON },// LED_STATUS_SLOWFLASH_1
  {OFF, ON },// LED_STATUS_FASTFLASH_1
  {OFF, OFF},// LED_STATUS_SOLID_2
  {OFF, OFF},// LED_STATUS_SLOWFLASH_2
  {OFF, OFF},// LED_STATUS_FASTFLASH_2
  {ON,  ON },// LED_STATUS_SOLID_1_2
  {OFF, ON },// LED_STATUS_SLOWFLASH_1_2
  {OFF, ON },// LED_STATUS_FASTFLASH_1_2
  {OFF, ON },// LED_STATUS_ALTERNATE
};


static ledTable LED2[]=
{
  {OFF, OFF},// LED_STATUS_OFF
  {OFF, OFF},// LED_STATUS_SOLID_1
  {OFF, OFF},// LED_STATUS_SLOWFLASH_1
  {OFF, OFF},// LED_STATUS_FASTFLASH_1
  {ON,  ON },// LED_STATUS_SOLID_2
  {OFF, ON },// LED_STATUS_SLOWFLASH_2
  {OFF, ON },// LED_STATUS_FASTFLASH_2
  {ON,  ON },// LED_STATUS_SOLID_1_2
  {OFF, ON },// LED_STATUS_SLOWFLASH_1_2
  {OFF, ON },// LED_STATUS_FASTFLASH_1_2
  {ON,  OFF},// LED_STATUS_ALTERNATE
};


// Self adjusting constants for loop indexes
#define  MAX_PATTERN  (sizeof(LED1)/sizeof(ledTable))
#define  MAX_STATE    (sizeof(LED1[0])/sizeof(lu_bool_t))

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
LEDController::LEDController(MCMLED& led)
                        :
                Thread(SCHED_TYPE_FIFO, Thread::PRIO_MIN + 26, LU_TRUE, "LEDCtrlThread"),
                m_led(led),
                m_status(MCMLED::LED_STATUS_OFF),
                m_period(FLASH_RATE_SLOW)
{
    start();
}

LEDController::~LEDController()
{
    // Turn off LED
    m_led.setLED(0, OFF);
    m_led.setLED(1, OFF);
}

lu_int32_t LEDController::setStatus(MCMLED::LED_STATUS newStatus)
{
    if (newStatus < 0 || newStatus > MAX_PATTERN)
    {
        return -1;
    }

    if(newStatus == MCMLED::LED_STATUS_FASTFLASH_1
                    ||newStatus == MCMLED::LED_STATUS_FASTFLASH_1_2
                    ||newStatus == MCMLED::LED_STATUS_FASTFLASH_2
                    ||newStatus == MCMLED::LED_STATUS_ALTERNATE)
        this->m_period = FLASH_RATE_FLAST;
    else
        this->m_period = FLASH_RATE_SLOW;

    this->m_status = newStatus;
    return 0;
}



/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
void LEDController::threadBody()
{
    // Initialise status
    m_led.setLED(0, OFF);
    m_led.setLED(1, OFF);

    lu_uint32_t stateIdx = 0;

    MCMLED::LED_STATUS status;

    while(1)
    {
        status = getStatus();

        // Update LED state
        m_led.setLED(LED_RED,   LED1[status].state[stateIdx]);
        m_led.setLED(LED_GREEN, LED2[status].state[stateIdx]);

        // Transit state
        stateIdx ++;
        if(stateIdx >= MAX_STATE)
            stateIdx = 0;

        usleep(m_period);
    }
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
