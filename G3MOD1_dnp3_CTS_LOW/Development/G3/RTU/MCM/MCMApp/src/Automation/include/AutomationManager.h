/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:IAutomationManager.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13 Jul 2017     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef MCMAPP_SRC_AUTOMATION_INCLUDE_ABSTRACTAUTOMATIONMANAGER_H_
#define MCMAPP_SRC_AUTOMATION_INCLUDE_ABSTRACTAUTOMATIONMANAGER_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "IAutomationDatabase.h"
#include "LEDController.h"
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define AUTO_INHIBIT_INDEX AutomationManager::ENGINE_INPUT_INDEX_INHIBIT
#define AUTO_INHIBIT_TRUE  AutomationManager::INHIBIT_TRUE
#define AUTO_INHIBIT_FALSE AutomationManager::INHIBIT_FALSE

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
void setAutomationDebugMode(bool enabled);

lu_bool_t isAutomationDebugMode();

lu_bool_t isAutomationRunning();

lu_bool_t isAutomationFailed();

lu_bool_t isAutomationInhibited();

/**
 * Abstract automation manager for managing start/stop of automation engine and also auto LED status.
 */
class AutomationManager
{
public:
    static const lu_uint16_t ENGINE_INPUT_INDEX_INHIBIT = 0;

    /* The number of inputs managed by engine. */
    static const lu_uint16_t ENGINE_INPUT_NUM = 1;


    static const lu_bool_t INHIBIT_FALSE  = 0;
    static const lu_bool_t INHIBIT_TRUE   = 1;


    static const lu_uint16_t ENGINE_POINT_INDEX_RUNNING = 0;
    static const lu_uint16_t ENGINE_POINT_INDEX_FAIL    = 1;

    /* The number of points managed by engine. */
    static const lu_uint16_t ENGINE_POINT_NUM = 2;

public:
    AutomationManager(IAutomationDatabase* db);
    virtual ~AutomationManager();

    /**
     * Initialise the status of automation(e.g. LED, points).
     */
    void init();

    /* Starts automation engine and scheme*/
    virtual lu_bool_t start() = 0;

    /* Stops automation engine and scheme*/
    virtual lu_bool_t stop() = 0;

    /* Gets the automation database.*/
    IAutomationDatabase& getDatabase(){return *mp_db;}

    /* Sets LED flashing pattern upon the current automation status.*/
    void updateLEDStatus();

    lu_bool_t isAutomationRunning(){return m_autoRunning;}

    lu_bool_t isAutomationFailed() {return m_autoFailed;}

    lu_bool_t isAutomationInhibited() {return m_autoInhibited;}


protected:
    /* Sets automation running status and update the status of LED.*/
    void setAutoRunning(lu_bool_t value);

    /* Sets automation fail status and update the status of LED.*/
    void setAutoFailed(lu_bool_t value);

    /* Sets automation debug status and update the status of LED.*/
    void setAutoDebugging(lu_bool_t value);

public:
    void setAutoInhibited(lu_bool_t value)
    {
        m_autoInhibited = value;
    }

private:
    lu_bool_t m_autoRunning;
    lu_bool_t m_autoFailed;
    lu_bool_t m_autoInhibited;
    IAutomationDatabase* mp_db;

    static LEDController LED_CONTROLLER;
};



#endif /* MCMAPP_SRC_AUTOMATION_INCLUDE_ABSTRACTAUTOMATIONMANAGER_H_ */

/*
 *********************** End of file ******************************************
 */
