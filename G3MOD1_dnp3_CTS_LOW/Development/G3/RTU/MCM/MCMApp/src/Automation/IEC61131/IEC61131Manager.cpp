/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:AutomationManager.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   29 Apr 2016     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


#include <stdarg.h>
#include <stdio.h>
#include <memory.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "IEC61131Manager.h"
#include "IEC61131DebugThread.h"
#include "IEC61131EngineThread.h"
#include "Logger.h"
#include "Debug.h"
extern "C" {
#include "LoggerInterface.h"
}
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static IEC61131Manager* getManager(void* context);
static IAutomationDatabase& getDB(void* context);

/*
****************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
IEC61131Manager::IEC61131Manager(IAutomationDatabase* db):
                AutomationManager(db),
                mp_engineThread(NULL),
                mp_debugThread(NULL)

{
    AutomationContext* context;
    context = (AutomationContext*)calloc(1, sizeof(AutomationContext));
    mp_context = context;

    context->invoker = (void*)(this);

    mp_debugThread = new IEC61131DebugThread(*this, context, &(context->dbgctxt));
    mp_debugThread->start();
}

IEC61131Manager::~IEC61131Manager()
{
    stop();

    delete mp_debugThread;
    mp_debugThread = NULL;

    /*Clean before exit*/
    free(mp_context);
    mp_context = NULL;

}

lu_bool_t IEC61131Manager::start()
{
    /* Checks if the thread is running*/
    if(mp_engineThread != NULL )
    {
        if(mp_engineThread->isRunning())
        {
            logger_err("AutoEngine thread is already running");
            return LU_FALSE;
        }
        else
        {
            delete mp_engineThread;
            mp_engineThread = NULL;
        }
    }

    /* Launch scheme thread*/
    THREAD_ERR err;
    mp_engineThread = new IEC61131EngineThread((AutomationContext*)mp_context, *this);
    err = mp_engineThread->start();

    return err == THREAD_ERR_NONE ? LU_TRUE : LU_FALSE;
}

lu_bool_t IEC61131Manager::stop()
{
    if (mp_engineThread != NULL)
    {
        if (mp_engineThread->isRunning())
        {
            mp_engineThread->stop();
        }

        delete mp_engineThread;
        mp_engineThread = NULL;
    }

    return LU_TRUE;
}

lu_bool_t IEC61131Manager::isRunning()
{
    return (mp_engineThread != NULL && mp_engineThread->isRunning())?LU_TRUE:LU_FALSE;
}
/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

AS_ERROR IEC61131Manager::callbackGetConstant(void* context, lu_uint32_t index, lu_int32_t* value)
{
    AS_ERROR err = AS_ERROR_NONE ;
    lu_int32_t data;
    IAutomationDatabase& db = getDB(context);

    GDB_ERROR ret = db.getConstantValue(index, data);

    if(ret == GDB_ERROR_NONE)
    {
        *value = data;
    }
    else
    {
        err = AS_ERROR_GENERAL;
    }

    return err;
}


AS_ERROR IEC61131Manager::callbackGetInput(void* context, lu_uint32_t index, lu_uint32_t* value)
{
    AS_ERROR err = AS_ERROR_NONE ;
    PointDataCounter32 data; //TODO checktype.
    IAutomationDatabase& db = getDB(context);

    /* Convert index passed from scheme to the index in database.*/
    index = index + ENGINE_INPUT_NUM;

    GDB_ERROR ret = db.getInputValue(index, &data);

    if(ret == GDB_ERROR_NONE)
    {
        *value = data;
    }
    else
    {
        err = AS_ERROR_GENERAL;
    }

    return err;
}


AS_ERROR IEC61131Manager::callbackSetPoint(void* context, lu_uint32_t index, lu_uint32_t value)
{
    IAutomationDatabase& db = getDB(context);

    /* Convert index passed from scheme to the index in database.*/
    index = index + ENGINE_POINT_NUM;

    db.setPointValue(index, value, true);

    return AS_ERROR_NONE;
}

AS_ERROR IEC61131Manager::callbackGetPoint(void* context, lu_uint32_t index, lu_uint32_t* value)
{
    IAutomationDatabase& db = getDB(context);
    db.getPointValue(index, (lu_int32_t*)value, NULL);
    return AS_ERROR_NONE;
}


AS_ERROR IEC61131Manager::callbackOperate (void* context, lu_uint32_t index, lu_uint16_t operationCode)
{
    IAutomationDatabase& db = getDB(context);
    db.operateOutput(index, operationCode);
    return AS_ERROR_NONE;
}

static IEC61131Manager* getManager(void* context)
{
    AutomationContext* ctxt = (AutomationContext*) (context);
    return (IEC61131Manager*) (ctxt->invoker);
}

static IAutomationDatabase& getDB(void* context)
{
    return getManager(context)->getDatabase();
}

/*
 ******************************************************************************
 * Implementation of LoggerInterface.h
 ******************************************************************************
 */
void logger_err(const lu_char_t* msg,  ...)
{
    char buffer[256];
    memset(buffer,0, 256);

    static Logger& log = Logger::getLogger(SUBSYSTEM_ID_AUTO_SCHEME);
    va_list args;
    va_start (args, msg);

    vsprintf(buffer, msg, args);

    log.error("AUTOMATION >> %s", buffer);
    DBG_ERR  ("AUTOMATION >> %s", buffer);

    va_end (args);
}

void logger_info(const lu_char_t* msg, ...)
{
#ifndef NDEBUG
    char buffer[256];
    memset(buffer,0, 256);

    va_list args;
    va_start (args, msg);

    vsprintf(buffer, msg, args);
    DBG_INFO("AUTOMATION >> %s", buffer);

    va_end (args);
#endif
}



/*
 *********************** End of file ******************************************
 */
