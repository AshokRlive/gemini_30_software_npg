/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: AutomationLogic.h
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: wang_p $: (Author of last commit)
 *       \date   $Date: 03 Jun 2015 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   03 Jun 2015   wang_p    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef AUTOMATIONLOGIC_H__INCLUDED
#define AUTOMATIONLOGIC_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "AutomationManager.h"
#include "lu_types.h"
#include "Thread.h"
#include "OperatableControlLogic.h"
#include "ControlLogicAnaloguePoint.h"
#include "ControlLogicDigitalPoint.h"
#include "ControlLogicCounterPoint.h"
#include "IAutomationDatabase.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/* XXX: pueyos_a -  (to be removed) */
//#define AUTOHACK_DEBUG_61499

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
#define LIBRARY_FILE_NAME_LEN 100

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */


/*!
 * \brief Control Logic for automation scheme.
 */
class AutomationLogic: public IAutomationDatabase,public OperatableControlLogic
{
    public:
	/* Forward declaration*/
	struct Config;
	class LogicInput;
	class LogicOutput;

    /**
     * \brief Custom constructor
     *
     * \param mutex Reference to the G3DB common mutex
     * \param database Reference to the Gemini database
     * \param config reference to the Control Logic's configuration block
     */
    AutomationLogic(Mutex* mutex,
                    GeminiDatabase& database,
                    AutomationLogic::Config& config,
                    CONTROL_LOGIC_TYPE type,
                    AutomationManager* manager);
    virtual ~AutomationLogic();


    /* == Inherited from IAutomationLogic == */
    virtual GDB_ERROR getInputValue   (lu_uint16_t index, PointData* data);
    virtual GDB_ERROR setPointValue(lu_uint16_t index, lu_int32_t value, bool online);
    virtual GDB_ERROR getPointValue(lu_uint16_t index, lu_int32_t* valuePtr, bool* online);
    virtual GDB_ERROR getConstantValue(lu_uint16_t index, lu_int32_t& value) ;
    virtual POINT_TYPE getInputType   (lu_uint16_t index);
    virtual POINT_TYPE getPointType   (lu_uint16_t index);
    virtual GDB_ERROR operateOutput(lu_uint16_t index,lu_uint16_t opCode);
    virtual const lu_char_t* getLibName();
    virtual const lu_char_t* getResName();
    virtual lu_int32_t getSchemeID();

    lu_uint16_t getID()
    {
        return OperatableControlLogic::getID();
    }

    GeminiDatabase& getDB(){return database;}


    /* == Inherited from ControlLogic == */
    virtual GDB_ERROR init();

    GDB_ERROR start();
    GDB_ERROR stop();

    static void logEvent(const EVENT_TYPE_AUTOMATION eventValue);

protected:
    virtual void handleInputChange(lu_uint32_t index, POINT_TYPE type, PointData *newPointData);

    /* == Inherited from OperatableControlLogic == */
    virtual GDB_ERROR startOp(const SwitchLogicOperation &operation, const GDB_ERROR errorCode);

    /* == Inherited from ControlLogic == */
	virtual void newData(PointIdStr pointID, PointData& pointData)
	{
		LU_UNUSED(pointID);
		LU_UNUSED(pointData);
	};


private:

	bool isAuotstartEnabled(){return m_autoStart;};

    /**
     * Indicates if the automation should be inhibited.
     * Override to inhibit the start of the automation.
     */
	virtual bool isInhibited();

private:
    lu_char_t  m_libFileName[LIBRARY_FILE_NAME_LEN];
    lu_char_t  m_resName    [LIBRARY_FILE_NAME_LEN];
    const lu_int32_t m_schemeID;

    bool       m_autoStart;

    lu_int32_t* mp_constants;
    const lu_uint32_t m_constantsNum;

    LogicInput** mp_inputs;
    const lu_uint32_t m_inputNum;

    LogicOutput** mp_outputs;
    const lu_uint32_t m_outputNum;

    AutomationManager* mp_manager;

public:
    /**
	 * \brief configuration block for Automation Control Logic
	 */
	struct Config : public OperatableControlLogic::Config
	{
	public:
		Config( lu_uint32_t _constantsNum ,
				lu_uint32_t _inputsNum ,
				lu_uint32_t _bPointNum ,
				lu_uint32_t _dbPointNum,
				lu_uint32_t _aPointNum ,
				lu_uint32_t _cPointNum ,
				lu_uint32_t _outputsNum)
		:constantsNum (_constantsNum ),
		 inputsNum (_inputsNum ),
		 bPointNum (_bPointNum ),
		 dbPointNum(_dbPointNum),
		 aPointNum (_aPointNum ),
		 cPointNum (_cPointNum ),
		 outputsNum(_outputsNum),
		 allPointNum(bPointNum + dbPointNum + aPointNum + cPointNum),
		 constants (new lu_int32_t[constantsNum]				 ),
		 inputs (new PointIdStr[inputsNum]                       ),
		 bPoint (new ControlLogicDigitalPoint::Config[bPointNum] ),
		 dbPoint(new ControlLogicDigitalPoint::Config[dbPointNum]),
		 aPoint (new ControlLogicAnaloguePoint::Config[aPointNum]),
		 cPoint (new ControlLogicCounterPoint::Config[cPointNum] ),
		 outputs(new lu_uint16_t[outputsNum]                     ),
		 schemeID(0),
		 autoStart(false)
		{

		}

		~Config()
		{
			delete[] constants  ;
			delete[] inputs  ;
			delete[] bPoint  ;
			delete[] dbPoint ;
			delete[] aPoint  ;
			delete[] cPoint  ;
			delete[] outputs ;
		}
		void addConstant(const lu_uint32_t index, const lu_int32_t value)
		{
			if(index > constantsNum)
			{
				return;
			}
			constants[index] = value;
		}
	public:
		const lu_uint32_t constantsNum;
		const lu_uint32_t inputsNum;
		const lu_uint32_t bPointNum;
		const lu_uint32_t dbPointNum;
		const lu_uint32_t aPointNum;
		const lu_uint32_t cPointNum;
		const lu_uint32_t outputsNum;
		const lu_uint32_t allPointNum;

	public:
		//Arrays of elements for configuration
		lu_int32_t* 						const constants;
		PointIdStr* 						const inputs;
		ControlLogicDigitalPoint::Config* 	const bPoint;
		ControlLogicDigitalPoint::Config* 	const dbPoint;
		ControlLogicAnaloguePoint::Config*  const aPoint;
		ControlLogicCounterPoint::Config* 	const cPoint;
		lu_uint16_t* 						const outputs; // The output logic IDs
		lu_char_t  libSchemeFile[LIBRARY_FILE_NAME_LEN];
		lu_char_t  resName[LIBRARY_FILE_NAME_LEN];
		lu_int32_t schemeID;
		bool       autoStart;
	};


	class LogicOutput
	{
	public:
		LogicOutput(lu_int16_t _outputLogicId)
					:outputLogicId(_outputLogicId)
		{}

		const lu_int16_t outputLogicId;
	};

	class LogicInput : public IPointObserver
	{
	public:
		LogicInput(AutomationLogic* logic, PointIdStr& pid, lu_uint32_t index):
		    mp_logic(logic),
		    m_pid(pid),
		    m_index(index),
		    m_type(POINT_TYPE_INVALID)
		{

		}

		virtual void update(PointIdStr pointID, PointData *pointDataPtr);
		virtual PointIdStr getPointID();
		POINT_TYPE getInputType();

//		GDB_ERROR getData(PointData* data);

	private:
		AutomationLogic* mp_logic;
		PointIdStr m_pid;
//		PointDataT<lu_int32_t> m_data; // a copy of input point data
		const lu_uint32_t m_index;
		POINT_TYPE m_type;
	};

	class AutoStartJob: public TimedJob
    {
    public:
	    AutoStartJob( AutomationLogic& cLogic):
	                                TimedJob(3000, 0),
                                    m_cLogic(cLogic)
        {};
        virtual ~AutoStartJob() {};

    protected:
        void job(TimeManager::TimeStr *timePtr)
        {
            LU_UNUSED(timePtr);
            m_cLogic.start();
        }

    private:
        AutomationLogic& m_cLogic;
    };

};



#endif /* AUTOMATIONLOGIC_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
