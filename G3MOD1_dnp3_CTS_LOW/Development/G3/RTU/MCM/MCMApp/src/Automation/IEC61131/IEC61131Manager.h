/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:AutomationManager.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   29 Apr 2016     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef AUTOMATIONMANAGER_H_
#define AUTOMATIONMANAGER_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "../include/AutomationManager.h"
#include "lu_types.h"
#include "Thread.h"

extern "C" {
#include "AutomationScheme.h"
}

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class IEC61131Manager : public AutomationManager
{
public:
    IEC61131Manager(IAutomationDatabase* db);
    virtual ~IEC61131Manager();

    /** === Override === */
    virtual lu_bool_t start();
    virtual lu_bool_t stop();
    virtual lu_bool_t isRunning();

    static AS_ERROR callbackGetConstant(void* context, lu_uint32_t index, lu_int32_t* value);
    static AS_ERROR callbackDelay (void* context, lu_uint32_t milliseconds);
    static AS_ERROR callbackOperate (void* context, lu_uint32_t index, lu_uint16_t operationCode);
    static AS_ERROR callbackSetLineNumber(void* context, lu_uint32_t line_number);
    static AS_ERROR callbackSetPoint(void* context, lu_uint32_t index, lu_uint32_t value);
    static AS_ERROR callbackGetPoint(void* context, lu_uint32_t index, lu_uint32_t* value);
    static AS_ERROR callbackGetInput(void* context, lu_uint32_t index, lu_uint32_t* value);

private:
    Thread* mp_engineThread;
    Thread* mp_debugThread;

    void* mp_context;

    friend class IEC61131EngineThread;
    friend class IEC61131DebugThread;
};

#endif /* AUTOMATIONMANAGER_H_ */

/*
 *********************** End of file ******************************************
 */
