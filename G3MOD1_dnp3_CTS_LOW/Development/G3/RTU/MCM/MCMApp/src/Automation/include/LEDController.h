/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:AutomationLEDController.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17 May 2016     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef AUTOMATIONLEDCONTROLLER_H_
#define AUTOMATIONLEDCONTROLLER_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "Thread.h"
#include "MCMLED.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
/**
 * A component for controlling MCM LED.
 *
 * fixes #3529
 * This controller is created to replace the Timer-driven LEDBlinker which
 * has a bug that BlinkThread may crash if the status is changed too
 * frequently.
 */
class LEDController: private Thread
{
public:
    LEDController(MCMLED& led);
    virtual ~LEDController();
    lu_int32_t setStatus(MCMLED::LED_STATUS newStatus);

protected:
    virtual void threadBody();
    MCMLED::LED_STATUS getStatus()
    {
        return m_status;
    }
private:
    MCMLED& m_led;
    volatile MCMLED::LED_STATUS m_status;
    volatile lu_uint32_t m_period; // microseconds
};

#endif /* AUTOMATIONLEDCONTROLLER_H_ */

/*
 *********************** End of file ******************************************
 */
