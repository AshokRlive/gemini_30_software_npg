/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:AutomationDebugThread.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   29 Apr 2016     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */


#include "IEC61131DebugThread.h"

#include "lu_types.h"
#include "Debug.h"
extern "C" {
    #include "LoggerInterface.h"
    #include "dbgserver/Debugger.h"
    #include "dbgserver/IDebuggerProtocol.h"
}

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
IEC61131DebugThread::~IEC61131DebugThread()
{
    stop();

    if(*mp_dbgctxt != NULL)
    {
        debugger_destroy(*mp_dbgctxt);
        *mp_dbgctxt = NULL;
    }
}

lu_int32_t debugger_get_variable(void* invokerPtr, const DebugVarRef ref)
{
    IEC61131DebugThread* invoker = (IEC61131DebugThread*)(invokerPtr);
    void* autoCtxt = invoker->getAutoContext();

    lu_int32_t value = -1;
    switch(ref.type)
    {
        case DBG_VAR_INPUT    :
            IEC61131Manager::callbackGetInput(autoCtxt, ref.index, (lu_uint32_t*)&value);
            break;

        case DBG_VAR_CONSTANT :
            IEC61131Manager::callbackGetConstant(autoCtxt, ref.index, &value);
            break;

        case DBG_VAR_OUTPUT   :
            IEC61131Manager::callbackGetPoint(autoCtxt, ref.index, (lu_uint32_t*)&value);
            break;

        default:
            value = 0;
            break;
    }

    return value;

}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
void IEC61131DebugThread::threadBody()
{
    lu_int32_t schid = m_manager.getDatabase().getSchemeID();
    const lu_char_t* libName = m_manager.getDatabase().getLibName();

    if((*mp_dbgctxt = debugger_init(DEFAULT_DEBUG_PORT, schid, libName, (void*)this)) != NULL)
    {
        debugsocket_register_connchg_handler(
                        debugger_get_socket_contxt(*mp_dbgctxt),
                        IEC61131DebugThread::connchg_handler
                        );

        debugger_run(*mp_dbgctxt);
    }
    else
    {
        logger_err("Failed to initialise debugger.");
    }
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

void IEC61131DebugThread::connchg_handler(void* debugContext, lu_int32_t existingConnNum)
{
    IEC61131DebugThread* invoker = (IEC61131DebugThread*)debugger_get_invoker(debugContext);
    logger_info("AutomationDebugThread >> established connections:%d",existingConnNum);

    //invoker->m_manager.setAutoDebugging(existingConnNum > 0 ? LU_TRUE : LU_FALSE);


    if(existingConnNum == 0)
    {
        /*Reset debugger if no client connected.*/
        logger_info("No client connected, reset debug context and resume running.");

        debugger_reset_all(debugContext);
        debugger_resume(debugContext);
    }
}

/*
 *********************** End of file ******************************************
 */
