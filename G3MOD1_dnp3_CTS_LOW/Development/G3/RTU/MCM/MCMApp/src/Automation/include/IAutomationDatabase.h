/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE IAutomationDatabase.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   29 Apr 2016     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef IAUTOMATIONDATABASE_H_
#define IAUTOMATIONDATABASE_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "MainAppEnum.h"
#include "PointData.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * The interface of automation database for reading/writing/ data
 * and controlling operation.
 */
class IAutomationDatabase
{
public:
   IAutomationDatabase() {};
   virtual ~IAutomationDatabase() {};

   virtual GDB_ERROR  getInputValue   (lu_uint16_t index, PointData* data) = 0;
   virtual GDB_ERROR  setPointValue   (lu_uint16_t index, lu_int32_t value, bool online) = 0;
   virtual GDB_ERROR  getPointValue   (lu_uint16_t index, lu_int32_t* valuePtr, bool* online) = 0;
   virtual GDB_ERROR  getConstantValue(lu_uint16_t index, lu_int32_t& value) = 0;
   virtual GDB_ERROR  operateOutput   (lu_uint16_t index, lu_uint16_t opCode) = 0;
   virtual POINT_TYPE getInputType    (lu_uint16_t index) = 0;
   virtual POINT_TYPE getPointType    (lu_uint16_t index) = 0;

   virtual const lu_char_t* getLibName() = 0;
   virtual lu_int32_t getSchemeID() = 0;
};



#endif /* IAUTOMATIONDATABASE_H_ */

/*
 *********************** End of file ******************************************
 */
