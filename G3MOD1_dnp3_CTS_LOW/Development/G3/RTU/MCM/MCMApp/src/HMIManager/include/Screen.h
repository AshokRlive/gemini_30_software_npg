/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       HMI Screen public interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   23/02/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_766E8459_DFDA_4b20_BE43_B2EF924C75A9__INCLUDED_)
#define EA_766E8459_DFDA_4b20_BE43_B2EF924C75A9__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "ListMgr.h"
#include "QueueObj.h"
#include "RAWBuffer.h"  //StringRAW definition
#include "HMIManagerCommon.h"
/* FIXME: pueyos_a - Screen Manager should include Screen.h, not Screen including ScreenManager.h! */
#include "ScreenManager.h"
#include "Logger.h"

/* Forward Declaration */
class Screen;
class ButtonsStatus;
class LCDData;

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef ListMgr<Screen> ScreenList;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Screen interface class
 *
 * This is a composite. It represents both the primitives (screens)
 * and the containers (menus).
 *
 * The mediator pattern has also been used to decouple the different "widgets".
 * The mediator is the ScreenManager class
 *
 * The implementation relies on the fact that the screens are accessed only
 * by one mediator (the ScreenManager) from only one context. We don't need any
 * synchronisation mechanism to access critical section because  all the
 * resources are already accessed "atomically" by the mediator.
 */
class Screen : public QueueObj<Screen>
{
public:
    static const lu_uint32_t maxTitleSize = 32;
    static const lu_uint32_t maxHeaderSize = 32;
    static const lu_uint32_t maxFooterSize = 16;
    /* The maximum page number is 254 so ->254/254 */
    static const lu_uint32_t currentLastPageStringSize = 7;
    static const lu_char_t* NotAvailableString;
    static const lu_uint8_t normalPadChar = ' ';
    static const lu_uint8_t ProgressBarChar = '-';  //Char used for progress bar
    static const lu_uint8_t lContent = 0xAB; //'«' Indication for more content at the left of the screen
    static const lu_uint8_t rContent = 0xBB; //'»' Indication for more content at the right of the screen
    /* Menu selection chars :
     * Note that lSelection is the char that appears at the left of the selection,
     * while rselection is the char that appears at the right - Not the char
     * direction itself.
     * */
    static const lu_uint8_t lSelection = rContent; //former '>';
    static const lu_uint8_t rSelection = lContent; //former '<';
    static const lu_uint8_t selectedPadChar = '-';

public:
    /**
     * \brief Custom Constructor
     *
     * \param mediator Reference to the mediator
     * \param accessLevel Screen access level
     * \param title Pointer to the screen title
     * \param rows Screen rows
     * \param columns Screen columns
     *
     * \return None
     */
    Screen( ScreenManager &mediator,
            lu_uint32_t accessLevel,
            const lu_char_t *title,
            lu_uint8_t rows,
            lu_uint8_t columns
          );

    ~Screen();

    /**
     * Get the Screen parent
     *
     * \return Pointer to the screen parent. NULL if the parent is not set
     */
    Screen *getParent();

    /**
     * \brief Notify a button event
     *
     * The screen interface handles the ESC and OK buttons. If the event
     * is hot handled is is forwarded to the custom handler of the class
     * (customButtonEventHandler)
     *
     * \param status The buttons status
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR buttonEvent(ButtonsStatus &status);

    /**
     * Get the screen title
     *
     * \param stringBuffer Buffer where the title is saved
     *
     * \return Error Code
     */
    HMI_M_ERROR getTitle(StringRAW &stringBuffer);

    /**
     * \brief Draw the screen
     *
     * \param fullSynch Force a dull update of the LCD
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR draw(lu_bool_t fullSynch) = 0;

    /**
     * \brief Entering Screen event
     *
     * Notifies the screen that has been entered
     */
    virtual void eventEnter()
    {};

    /**
     * \brief Exiting Screen event
     *
     * Notifies the screen that has been abandoned
     */
    virtual void eventExit()
    {};

    /**
     * \brief Reset the screen internal history
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR resetHistory() = 0;

    /**
     * \brief Periodic tick event
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR tickEvent() = 0;

    /***********************/
    /* Container interface */
    /***********************/

    /**
     * \brief Add a child to the screen.
     *
     * The child is added using the virtual method addChild
     * If the child is correctly added this screen is set as the child parent
     *
     * \param child Pointer to the child to add
     *
     * \return Error Code
     */
    HMI_M_ERROR add(Screen *child);

    /**
     * \brief Remove a child from the screen
     *
     * Default implementation: operation not supported
     *
     * \param child Pointer to the child to remove
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR remove(Screen *child);

    /**
     * \brief Get the child in idx position
     *
     * Default implementation: operation not supported
     *
     * \param idx Position of the child to retrieve
     *
     * \return Pointer to the child. NULL if doesn't exist
     */
    virtual Screen *getChild(lu_uint32_t idx);

protected:
    /**
     * \brief Add a child to the screen
     *
     * The parent is sent by the caller
     *
     * Default Implementation: Not Supported
     *
     * \param child Pointer to the child to add
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR addChild(Screen *child);

    /**
     * \brief Custom Button event handler
     *
     * This method is called by the buttonEvent method id the button event
     * has not been handled by the Screen handler
     *
     * \param status The buttons status
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR customButtonEventHandler(ButtonsStatus &status) = 0;

    /**
     * \brief Write a string if the middle of the buffer
     *
     * The selected pattern is added if necessary
     *
     * \param stringBuffer Reference to the buffer where the
     *        string will be written
     * \param string Pointer to the string to write
     * \param selected TRUE to add the selected pattern
     *
     * \return None
     */
    void centredWrite( StringRAW &stringBuffer,
                       lu_char_t *string,
                       lu_bool_t selected
                     );

    /**
     * \brief centeredWrite Write a string if the middle of the buffer
     *
     * A counter in the form of "currentPage/lastPage" is also added.
     * The counter is right aligned in the buffer
     *
     * \param stringBuffer Reference to the buffer where the
     *        string will be written
     * \param string Pointer to the string to write
     * \param currentPage Current page/selection
     * \param lastPage Last page/selection
     *
     * \return None
     */
    void centredWrite( StringRAW &stringBuffer,
                       lu_char_t *string,
                       lu_uint8_t currentPage,
                       lu_uint8_t lastPage
                     );

    /**
     * \brief Remove the NULL termination character from a buffer
     *
     * It is assumed that the buffer has been filled  using a function with
     * a behaviour similar to the snprintf function. The termination is
     * added only if the string length is less than the buffer size. If the
     * string length is negative nothing has been written to the buffer.
     *
     * \param stringBuffer Reference to the buffer
     * \param strLen Length of the string written into the buffer.
     *
     * \return none
     */
    static void removeTermination(StringRAW &stringBuffer, lu_int32_t strLen)
    {
        /* Remove NULL termination if necessary */
        if( (strLen > -1) && ((lu_uint32_t)(strLen) <= stringBuffer.payloadLen) )
        {
            stringBuffer.payloadPtr[strLen] = Screen::normalPadChar;
        }
    }

protected:
    Logger& log;
    static LCDData *lcdBuffer;

    ScreenManager &mediator;
    lu_uint32_t accessLevel;

    lu_uint8_t rows;
    lu_uint8_t columns;

    lu_char_t title[maxTitleSize];

private:
    Screen *parent;
};


class IHMIFactory
{
public:
    IHMIFactory(){};

	virtual ~IHMIFactory(){};

	/**
	 * \brief Get the root screen
	 *
	 * All the sub screens are also created
	 *
	 * \param mediator Reference to the mediator
	 * \param rows Number of rows on the LCD
	 * \param columns Number of columns on the LCD
	 *
	 * \return Pointer to the root screen.
	 */
	virtual Screen* getScreen( ScreenManager &mediator,
	                           lu_uint8_t rows,
                               lu_uint8_t columns
                             ) = 0;

	virtual Screen* getLogonScreen( ScreenManager &mediator,
	                               lu_uint8_t rows,
	                               lu_uint8_t columns
	                             ) = 0;
};


#endif // !defined(EA_766E8459_DFDA_4b20_BE43_B2EF924C75A9__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
