/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "Event.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */


Event::EVENT_TYPE ButtonEvent::getEventType()
{
    return EVENT_TYPE_BUTTON;
}

HMI_M_ERROR ButtonEvent::getButtonState(lu_uint32_t &state)
{
    state = buttonState;

    return HMI_M_ERROR_NONE;
}

HMI_M_ERROR ButtonEvent::getOLR(HMI_OLR_STATE &olr)
{
    LU_UNUSED(olr);

    return HMI_M_ERROR_NOT_SUPPORTED;
}

HMI_M_ERROR ButtonEvent::getStatus(lu_bool_t &online)
{
    LU_UNUSED(online);

    return HMI_M_ERROR_NOT_SUPPORTED;
}

Event::EVENT_TYPE OLREvent::getEventType()
{
    return EVENT_TYPE_OLR;
}

HMI_M_ERROR OLREvent::getButtonState(lu_uint32_t &state)
{
    LU_UNUSED(state);

    return HMI_M_ERROR_NOT_SUPPORTED;
}

HMI_M_ERROR OLREvent::getOLR(HMI_OLR_STATE &olr)
{
    olr = this->olr;

    return HMI_M_ERROR_NONE;
}

HMI_M_ERROR OLREvent::getStatus(lu_bool_t &online)
{
    LU_UNUSED(online);

    return HMI_M_ERROR_NOT_SUPPORTED;
}

Event::EVENT_TYPE StatusEvent::getEventType()
{
    return EVENT_TYPE_STATUS;
}

HMI_M_ERROR StatusEvent::getButtonState(lu_uint32_t &state)
{
    LU_UNUSED(state);

    return HMI_M_ERROR_NOT_SUPPORTED;
}

HMI_M_ERROR StatusEvent::getOLR(HMI_OLR_STATE &olr)
{
    LU_UNUSED(olr);

    return HMI_M_ERROR_NOT_SUPPORTED;
}

HMI_M_ERROR StatusEvent::getStatus(lu_bool_t &online)
{
    online = this->online;

    return HMI_M_ERROR_NONE;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
