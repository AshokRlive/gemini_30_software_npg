/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: AlarmScreen.cpp 19 May 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/HMIManager/src/AlarmScreen.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 19 May 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19 May 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <cstring>
#include <stdio.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "AlarmScreen.h"
#include "IIOModule.h"
#include "ModuleAlarm.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static TimeManager& timeManager = TimeManager::getInstance();

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

AlarmScreen::AlarmScreen(ScreenManager &mediator,
                        lu_uint32_t accessLevel,
                        const lu_char_t *title,
                        lu_uint8_t rows,
                        lu_uint8_t columns,
                        IIOModuleManager& moduleMgr,
                        IStatusManager& statusMgr,
                        lu_uint32_t screenID
                        ) : InfoScreen(mediator, accessLevel, title, rows, columns, moduleMgr, statusMgr, screenID),
                        innerRows(0),
                        innerColumns(0),
                        idxSelection(1),
                        showingAlarm(LU_FALSE)
{
    currentAlarm.invalidate();
}


AlarmScreen::~AlarmScreen()
{
    eventExit();    //destroy all data
}


HMI_M_ERROR AlarmScreen::draw(lu_bool_t fullSynch)
{
    /* Exit if we don't have a screen buffer */
    if(lcdBuffer == NULL)
    {
        return HMI_M_ERROR_NO_SCREEN;
    }

    if(screenType != HMI_INFO_SCREEN_TYPE_ALARMS)
    {
        return HMI_M_ERROR_PARAM;
    }

    PayloadRAW bufferRAW;
    StringRAW  stringBuffer;

    lcdBuffer->getLCDBuffer(bufferRAW);    /* Get LCD Buffer */

    /* Get inner window size */
    if(showingAlarm == LU_TRUE)
    {
        innerRows = rows - 1;
        innerColumns = columns;
    }
    else
    {
        innerRows = rows - 1;
        innerColumns = columns - 1;
    }

    createContent();

    /* Get first line */
    stringBuffer.payloadPtr = reinterpret_cast<lu_char_t*>(bufferRAW.payloadPtr);
    stringBuffer.payloadLen = columns;  //Add null terminator
    lu_int32_t strLen;

    /* Write title */
    if(showingAlarm == LU_FALSE)
    {
        /* Write title and page number in the first row */
        //Please note conversion from amount of lines to last page index
        centredWrite(stringBuffer, title, idxSelection, content.height() );
    }
    else
    {
        centredWrite(stringBuffer, (lu_char_t*)"--ACT=Ack ESC=exit--", LU_FALSE);
    }

    /* Write lines of content */
    for(lu_uint8_t i = 1; i < rows; ++i)
    {
        /* Get new line */
        stringBuffer.payloadPtr = reinterpret_cast<lu_char_t*>(&bufferRAW.payloadPtr[i * columns]);

        /* Clear the line */
        memset(stringBuffer.payloadPtr, Screen::normalPadChar, stringBuffer.payloadLen);

        /* Add content: get the part of the line to be displayed */
        std::string line;
        if(showingAlarm == LU_FALSE)
        {
            line = content.getLine(ypos + i - 1);
        }
        else
        {
            line = content.getLine(i - 1);
        }
        std::string snippet = (xpos > line.length())? "" : line.substr(xpos, innerColumns);
        if( (xpos > 0) && (line.length() > 0) )
        {
            snippet[0] = Screen::lContent;
        }
        if(showingAlarm == LU_FALSE)
        {
            //Note: constant used since ternary operator (?:) doesn't work well with objects in BOTH true/false parts
            const char selection = Screen::lSelection;
            const char normal= Screen::normalPadChar;
            strLen = snprintf(stringBuffer.payloadPtr,
                              stringBuffer.payloadLen,
                              "%c%s",
                              ((ypos+i) == idxSelection)? selection : normal, //({Screen::lContent}) : ({Screen::rContent}), //(Screen::normalPadChar),
                              snippet.c_str()
                            );
        }
        else
        {
            strLen = snprintf(stringBuffer.payloadPtr,
                              stringBuffer.payloadLen,
                              "%s",
                              snippet.c_str()
                            );
        }

        removeTermination(stringBuffer, strLen);  //Remove NULL termination if necessary

        if( (snippet.length() == innerColumns) && (line.length() >= (xpos+innerColumns)) )
        {
            stringBuffer.payloadPtr[strLen-1] = Screen::rContent;
        }
    }

    /* Send the buffer to the mediator */
    lcdBuffer->setPosition(0, 0, rows*columns);
    mediator.writeLCD(*lcdBuffer, fullSynch);

    return HMI_M_ERROR_NONE;
}


void AlarmScreen::eventEnter()
{
    resetListing();
}


void AlarmScreen::eventExit()
{
    content.clear();
    listAlarmID.clear();    //remove alarmID vector as well
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
HMI_M_ERROR AlarmScreen::tickEvent()
{
    static TimeManager::TimeStr lastUpdate;

    TimeManager::TimeStr timeNow;
    timeManager.getTime(timeNow);
    if(lastUpdate.elapsed_ms(timeNow) > AlarmScreen::HMI_ALARMSCREEN_REFRESH_MS)
    {
        lastUpdate = timeNow;
        return draw(LU_FALSE);
    }
    return HMI_M_ERROR_NONE;
}

HMI_M_ERROR AlarmScreen::customButtonEventHandler(ButtonsStatus& status)
{
    lu_bool_t forceDraw = LU_FALSE;
    buttonStatus bStatus;

    /* UP and DOWN buttons are for the list only */
    if(showingAlarm == LU_FALSE)
    {
        /* Get UP button */
        status.getStatus(HMI_M_BUTTON_IDX_UP, bStatus);
        if( (bStatus.event == LU_TRUE) && (bStatus.pressed == LU_TRUE) )
        {
            if(idxSelection > 1)
            {
                --idxSelection;
                forceDraw = LU_TRUE;    //Redraw screen
                if(idxSelection < (ypos+1))
                {
                    /* Get previous line if available */
                    if(ypos > 0)
                    {
                        --ypos;
                    }
                }
            }
        }

        /* Get DOWN button */
        status.getStatus(HMI_M_BUTTON_IDX_DOWN, bStatus);
        if( (bStatus.event == LU_TRUE) && (bStatus.pressed == LU_TRUE) )
        {
            if(idxSelection < content.height())
            {
                ++idxSelection;
                forceDraw = LU_TRUE;    //Redraw screen
                if(idxSelection > (ypos + (innerRows)) )
                {
                    /* Get next line if available */
                    if( (ypos + (innerRows)) < content.height() )
                    {
                        ++ypos;
                    }
                }
            }
        }
    }

    /* Get LEFT button */
    status.getStatus(HMI_M_BUTTON_IDX_LEFT, bStatus);
    if( (bStatus.event == LU_TRUE) && (bStatus.pressed == LU_TRUE) )
    {
        /* Get previous column if available */
        if(xpos > 0)
        {
            --xpos;
            forceDraw = LU_TRUE;    //Redraw screen
        }
    }

    /* Get RIGHT button */
    status.getStatus(HMI_M_BUTTON_IDX_RIGHT, bStatus);
    if( (bStatus.event == LU_TRUE) && (bStatus.pressed == LU_TRUE) )
    {
        /* Get next line if available */
        if( ((xpos-1) + innerColumns) < content.width() )
        {
            ++xpos;
            forceDraw = LU_TRUE;    //Redraw screen
        }
    }

    /* Get OK button */
    status.getStatus(HMI_M_BUTTON_IDX_OK, bStatus);
    if( (bStatus.event == LU_TRUE) && (bStatus.pressed == LU_TRUE) )
    {
        //Show alarm detail if it was not already showing it
        showingAlarm = LU_TRUE;
        xpos = 0;
        ypos = 0;
        forceDraw = LU_TRUE;    //Redraw screen
    }

    if(showingAlarm == LU_TRUE)
    {
        /* Get ACT button */
        status.getStatus(HMI_M_BUTTON_IDX_ACT_ONLY, bStatus);
        if( (bStatus.event == LU_TRUE) && (bStatus.pressed == LU_TRUE) )
        {
            ackAlarm();     //Try to acknowledge the selected alarm
            resetListing(); //Force to go back to alarm list (repainting it)
            forceDraw = LU_TRUE;    //Redraw screen
        }
    }

    /* Force a redraw of the screen if necessary */
    if(forceDraw == LU_TRUE)
    {
        draw(LU_FALSE);
    }

    return HMI_M_ERROR_NONE;
}


void AlarmScreen::createContent()
{
    if(showingAlarm == LU_TRUE)
    {
        /* Alarm detail & action screen */
        lu_uint32_t index = idxSelection-1;
        showingAlarm = LU_FALSE;
        if(index >= listAlarmID.size())
        {
            return;
        }
        IIOModule* module = moduleManager.getModule(listAlarmID[index].moduleID);
        if(module == NULL)
        {
            return;
        }
        currentAlarm = module->getAlarm(listAlarmID[index].alarmID);
        if(currentAlarm.isValid() == LU_FALSE)
        {
            return; //since showingAlarm is false, it will go back to the Alarm List
        }
        /* TODO: pueyos_a - protect showingAlarm? */
        showingAlarm = LU_TRUE;
        content.clear();
        content.message(currentAlarm.getID().toCString());
        content.message("%s %s (%d)",
                        module->getName(),
                        SYS_ALARM_SEVERITY_ToSTRING(currentAlarm.severity),
                        currentAlarm.parameter
                        );
        content.message(currentAlarm.stateToString());
        return;
    }

    //Alarms info screen
    content.clear();
    listAlarmID.clear();


    bool alarmsPresent = false;
    std::vector<IIOModule*> moduleList;
    moduleList = moduleManager.getAllModules();
    for(std::vector<IIOModule*>::iterator it=moduleList.begin(); it!=moduleList.end(); ++it)
    {
        ModuleAlarmLister lister = (*it)->getAlarms();
        if(lister.size() > 0)
        {
            for (lu_uint32_t j = 0; j < lister.size(); ++j)
            {
                //Print out the alarm list
                if(lister[j].isValid() == LU_TRUE)
                {
                    alarmsPresent = true;
                    AlarmListID alarm;
                    alarm.alarmID = lister[j].getID();
                    alarm.moduleID = (*it)->getID();
                    listAlarmID.push_back(alarm);
                    content.message("%s|%s", (*it)->getName(), lister[j].toShortString().c_str());
                }
            }
        }
    }
    if(!alarmsPresent)
    {
        content.message("--No alarms--");
    }
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void AlarmScreen::ackAlarm()
{
    lu_uint32_t index = idxSelection-1;
    IIOModule* module = moduleManager.getModule(listAlarmID[index].moduleID);
    if(module == NULL)
    {
        return;
    }
    module->ackAlarm(listAlarmID[index].alarmID);
}


void AlarmScreen::resetListing()
{
    showingAlarm = LU_FALSE;
    currentAlarm.invalidate();
    idxSelection = 1;
    xpos = 0;
    ypos = 0;
}

/*
 *********************** End of file ******************************************
 */
