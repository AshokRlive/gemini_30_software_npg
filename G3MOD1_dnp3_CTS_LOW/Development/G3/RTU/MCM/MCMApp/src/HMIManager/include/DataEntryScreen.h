/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Data entry type screen public interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24/02/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_81DAAF04_5057_4f84_82A0_C3EDCC2D0F8A__INCLUDED_)
#define EA_81DAAF04_5057_4f84_82A0_C3EDCC2D0F8A__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Screen.h"
#include "DataEntry.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class DataEntryScreen : public Screen
{

public:
    DataEntryScreen( ScreenManager &mediator,
                     lu_uint32_t accessLevel,
                     const lu_char_t *title,
                     lu_uint8_t rows,
                     lu_uint8_t columns
                   );
    virtual ~DataEntryScreen();

    /**
     * \brief Draw the screen
     *
     *  \param fullSynch Force a dull update of the LCD
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR draw(lu_bool_t fullSynch);

    /**
     * \brief Reset the screen internal history
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR resetHistory();

    /**
     * \brief Periodic tick event
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR tickEvent();

    /********************/
    /* Custom interface */
    /********************/

    /**
     * \brief Add an entry to display to the list
     *
     * \data pointer to the entry to add
     *
     * \return Error Code
     */
    HMI_M_ERROR addEntry(DataEntry *data);

protected:
    /**
     * \brief Custom Button event handler
     *
     * This method is called by the buttonEvent method id the button event
     * has not been handled by the Screen handler
     *
     * \param status The buttons status
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR customButtonEventHandler(ButtonsStatus &status);

private:
    MasterMutex queueMutex;  // Mutex to protect the dataEntries queue
    DataEntryQueue dataEntries;

    lu_uint8_t currentPageIdx;
    lu_uint8_t lastPage;

};
#endif // !defined(EA_81DAAF04_5057_4f84_82A0_C3EDCC2D0F8A__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
