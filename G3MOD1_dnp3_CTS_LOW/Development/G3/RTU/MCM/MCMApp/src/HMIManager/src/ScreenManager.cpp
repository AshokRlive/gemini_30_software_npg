/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <cstring> //MG

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "ScreenManager.h"
#include "GeminiDatabase.h"
#include "Screen.h"
#include "LocalRemoteControlLogic.h"
#include "timeOperations.h"
#include "LogonScreen.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define RETRY(_ret_, _action_)\
{\
    _ret_ = HMI_M_ERROR_CHANNEL;\
    if(chOnline == LU_TRUE)\
    {\
        for(lu_uint32_t i = 0; i < maxRetries; ++i)\
        {\
            if(chOnline == LU_FALSE || _action_ == IOM_ERROR_NONE)\
            {\
                _ret_ = HMI_M_ERROR_NONE;\
                break;\
            }\
        }\
        if(_ret_ == HMI_M_ERROR_CHANNEL)\
        {\
            log.error("ScreenManager Retry error (%s).",\
                        #_action_\
                      );\
            chOnline = LU_FALSE;\
            logout();\
        }\
    }\
}

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
OLRObserver::OLRObserver(PointIdStr pointId,
                        ScreenManager &screenManager) :
                                 pointID(pointId),
                                 screenManager(screenManager)
{}


void OLRObserver::update(PointIdStr pointID, PointData *pointDataPtr)
{
    LU_UNUSED(pointID);

    valueOLR = (lu_uint8_t)(*pointDataPtr); //Update local value
    screenManager.setCurrentOLRstatus();    //notify to the HMI LEDs

    Logger::getLogger(SUBSYSTEM_ID_HMIM).info("OLRObserver::update: "
                    "HMI is reading OLR change to %i",
                    valueOLR
                    );
}


HMI_M_ERROR ButtonsStatus::getStatus( HMI_M_BUTTON_IDX entry,
                                      buttonStatus &status
                                    )
{
    /* Check input parameters */
    if( (buttons                         == NULL)   ||
        (static_cast<lu_uint32_t>(entry) >  entries)
      )
    {
        return HMI_M_ERROR_PARAM;
    }

    /* Return the button status */
    status = buttons[entry];

    return HMI_M_ERROR_NONE;
}

ScreenManager::ScreenManager( SCHED_TYPE schedType,
                              lu_uint32_t priority,
                              GeminiDatabase &database,
                              HMICANChannel &hmiChannel
                            ) :
                                          Thread(schedType, priority, LU_TRUE, "ScreenManager"),
                                          database(database),
                                          log(Logger::getLogger(SUBSYSTEM_ID_HMIM)),
                                          rootScreen(NULL),
                                          logonScreen(NULL),
                                          activeScreen(NULL),
                                          hmiChannel(hmiChannel),
                                          chOnline(LU_FALSE),
                                          buttonHandler(*this),
                                          started(LU_FALSE),
                                          suspended(LU_FALSE)
{
    lu_uint16_t olrIdx = 0;

    hmiChannel.attach(this);
    chOnline = hmiChannel.isActive();

    lu_uint32_t cLogicNum = database.getCLogicNum();

    /* Search for the OLR control logic block */
    for(lu_uint32_t i = 0; i < cLogicNum; ++i)
    {
        if(database.getType(i) == CONTROL_LOGIC_TYPE_OLR)
        {
            /* OLR found */
            olrIdx = i;
            break;
        }
    }

    if(olrIdx == 0)
    {
        log.warn("%s OLR Control Logic not found. Function disabled", __AT__);
    }
    else
    {
        /* Start observer */
        GDB_ERROR ret = GDB_ERROR_NOPOINT;
        PointIdStr point(olrIdx, OLR_POINT_OFFLOCALREMOTE);   //point to observe
        olrObserver = new OLRObserver(point, *this);
        ret = database.attach(point, olrObserver);
        if(ret != GDB_ERROR_NONE)
        {
            log.error("%s HMI was unable to attach to the OLR point. Error: %s",
                        __AT__, GDB_ERROR_ToSTRING(ret));
        }
    }
}

ScreenManager::~ScreenManager()
{
    hmiChannel.detach(this);

    /* Stop the internal thread */
    Thread::stop();

    /* Wait the internal thread to exit */
    //Thread::join();

    if(olrObserver != NULL)
    {
        PointIdStr pointID = olrObserver->getPointID();
        database.detach(pointID, olrObserver);
        delete olrObserver;
        olrObserver = NULL;
    }

    /* Delete the root screen if available */
    if(rootScreen != NULL)
    {
        delete rootScreen;
    }

    delete logonScreen;
}


HMI_M_ERROR ScreenManager::startServer()
{
    HMI_M_ERROR ret;
    suspended = LU_FALSE;

    if(started == LU_TRUE)
        return HMI_M_ERROR_NONE;

    /* Draw the active screen */
    if(activeScreen != NULL)
    {
        activeScreen->draw(LU_TRUE);
    }

    /* Set the off/Local/Remote State */
    setCurrentOLRstatus();

    /* Start the thread */
    if(Thread::start() == THREAD_ERR_NONE)
    {
        started = LU_TRUE;
        ret = HMI_M_ERROR_NONE;
    }
    else
    {
        log.error("ScreenManager::startServer Thread not running");
        ret = HMI_M_ERROR_THREAD;
    }

    return ret;
}


HMI_M_ERROR ScreenManager::initialise(Screen *root, Screen *logon)
{
    if(root == NULL)
    {
        return HMI_M_ERROR_PARAM;
    }

    /* Set root and active screen */
    rootScreen = root;
    logonScreen = logon;
    activeScreen = logon == NULL? root:logon;

    /* If running draw the screen */
    if (isRunning() == LU_TRUE)
    {
        if (activeScreen != NULL)
        {
            activeScreen->resetHistory();
            activeScreen->draw(LU_FALSE);
        }
    }

    return HMI_M_ERROR_NONE;
}


HMI_M_ERROR ScreenManager::parentScreen()
{
    /* Get the parent screen */
    Screen *parent = activeScreen->getParent();

    /* Activate parent screen */
    return nextScreen(parent);
}


HMI_M_ERROR ScreenManager::selectedScreen(lu_uint32_t idx)
{
    /* Get selected screen */
    Screen *selected = activeScreen->getChild(idx);

    if(selected == NULL)
    {
        return HMI_M_ERROR_PARAM;
    }

    /* Reset history */
    selected->resetHistory();

    /* Activate screen */
    return nextScreen(selected);
}


HMI_M_ERROR ScreenManager::homeScreen()
{
    if(rootScreen != NULL)
    {
        /* Reset root history */
        rootScreen->resetHistory();

        /* Set the current screen to the root screen */
        return nextScreen(rootScreen);
    }
    else
    {
        return HMI_M_ERROR_ROOT;
    }
}

HMI_M_ERROR ScreenManager::logout()
{
    return nextScreen(logonScreen);
}

HMI_M_ERROR ScreenManager::setLeds()
{
    return HMI_M_ERROR_NOT_SUPPORTED;
}


HMI_M_ERROR ScreenManager::writeLCD(LCDData &data, lu_bool_t fullSynch)
{
    HMI_M_ERROR ret;
    struct timeval timeout;

    timeout.tv_sec = 0;
    timeout.tv_usec = 100000;

    if(log.isDebugEnabled() == LU_TRUE)
    {
        lu_uint8_t rowScreen;
        lu_uint8_t colScreen;
        lu_uint8_t lenScreen;
        data.getPosition(rowScreen, colScreen, lenScreen);
        if(log.isDebugEnabled() == LU_TRUE)
        {
            PayloadRAW buffer;
            data.getLCDBuffer(buffer);
            lu_uint8_t* bufferPrint = new lu_uint8_t[buffer.payloadLen + 1];
            memcpy(bufferPrint, buffer.payloadPtr, buffer.payloadLen);
            bufferPrint[buffer.payloadLen] = '\0';
            /* Remove non-printable chars */
            for (lu_uint32_t idx = 0; idx < buffer.payloadLen; ++idx)
            {
                if( !(isprint(bufferPrint[idx])) && (bufferPrint[idx] < 0x80) )
                {
                    // Non-printable and not extended char
                    bufferPrint[idx] = '_'; //Put a space instead
                }
            }
            log.debug("HMI @%i,%i write: [%s]", rowScreen, colScreen, bufferPrint);
        }
    }
    RETRY(ret, hmiChannel.writeLCD(timeout, data));
    if(ret == HMI_M_ERROR_NONE)
    {
        RETRY(ret, hmiChannel.synchLCD(timeout, fullSynch));
    }

    return ret;
}


HMI_M_ERROR ScreenManager::clearLCD()
{
    HMI_M_ERROR ret;
    struct timeval timeout;

    timeout.tv_sec = 0;
    timeout.tv_usec = 100000;

    RETRY(ret, hmiChannel.clearLCD(timeout));

    return ret;
}


HMI_M_ERROR ScreenManager::playSound()
{
    return HMI_M_ERROR_NOT_SUPPORTED;
}


HMI_M_ERROR ScreenManager::screenSize(lu_int8_t &rows, lu_int8_t &columns)
{

#if GET_SIZE_FROM_HMI

    HMI_M_ERROR ret;
    struct timeval timeout;

    timeout.tv_sec = 0;
    timeout.tv_usec = 100000;

    RETRY(ret, hmiChannel.getDisplayInfo(timeout, rows, columns));
    return ret;
#else
    rows = HMI_MAX_DISPLAY_ROW;
    columns = HMI_MAX_DISPLAY_COL;
    return HMI_M_ERROR_NONE;
#endif
}


void ScreenManager::buttonEvent(ButtonsStatus &status)
{
    /* Forward event to the active screen */
    if(activeScreen != NULL)
    {
        activeScreen->buttonEvent(status);
    }
}


void ScreenManager::update(IChannel *subject)
{
    LU_UNUSED(subject);

    if(chOnline != hmiChannel.isActive())
    {
        /* Schedule a online changing event */
        chOnline = hmiChannel.isActive();
        eventQueue.push(std::auto_ptr<Event>(new StatusEvent(chOnline)));
    }

    /* Get the last event */
    HMICANChannel::HMI_EVENT_TYPE event = hmiChannel.getLastEvent();

    if(event == HMICANChannel::HMI_EVENT_TYPE_BUTTON)
    {
        /* Button pressed event */
        lu_uint32_t state;
        hmiChannel.getButtonsEvent(state);

        /* Add the event to the internal queue */
        eventQueue.push(std::auto_ptr<Event>(new ButtonEvent(state)));
    }
    else if(event == HMICANChannel::HMI_EVENT_TYPE_OLR)
    {
        /* Get olr event */
        HMI_OLR_STATE olr;
        hmiChannel.getOlrRequest(olr);

        /* Add the event to the internal queue */
        eventQueue.push(std::auto_ptr<Event>(new OLREvent(olr)));
    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void ScreenManager::threadBody()
{
    const lu_char_t *FTITLE = "ScreenManager::threadBody:";
    lu_int32_t ret = 0;
    struct timeval timeRef; //time reference for timeout/tickEvent related calculations
    struct timeval timeNew; //time stamp for timeout/tickEvent related calculations

    struct timeval timeout = ms_to_timeval(periodicTickMs);

    /* Set initial OLR status */
    setCurrentOLRstatus();

    while(isRunning() == LU_TRUE)
    {
        { //Scope of eventPtr defined here to force jobPtr to be deleted if sudden exit

            /* Block here until there is an event available (or time out) */
            clock_gettimeval(&timeRef); //get present time stamp
            std::auto_ptr<Event> eventPtr = eventQueue.popLock(timeout, ret);


            if(isInterrupting() == LU_TRUE ||suspended == LU_TRUE)
                continue;   //exit thread



            if(eventPtr.get()== NULL)
            {
                if( (ret == OBJECTQUEUERESULT_OK) )
                {
                    ret = -1;   //force to handle this as an error
                }
            }
            switch(ret)
            {
                case OBJECTQUEUERESULT_OK:
                {
                    Event::EVENT_TYPE eventType = eventPtr->getEventType();
                    switch(eventType)
                    {
                        case Event::EVENT_TYPE_BUTTON:
                            if(chOnline == LU_TRUE)
                            {
                                /* Send the event to the button handler.
                                 * The button handler will update the button status and
                                 * notify the mediator if something happened.
                                 * The mediator will forward the notification to the
                                 * active screen
                                 */
                                lu_uint32_t buttons;
                                eventPtr->getButtonState(buttons);
                                buttonHandler.buttonEventRAW(buttons);
                            }
                            else
                            {
                                /* System offline. Ignore event */
                                log.info("%s, HMI Manager offline. Button event discarded.",FTITLE);
                            }
                        break;

                        case Event::EVENT_TYPE_OLR:
                            if(chOnline)
                            {
                                HMI_OLR_STATE olr;
                                OLR_STATE lrIndication;
                                eventPtr->getOLR(olr);

                                /* Convert event */
                                switch(olr)
                                {
                                    case HMI_OLR_STATE_OFF:
                                        lrIndication = OLR_STATE_OFF;
                                    break;

                                    case HMI_OLR_STATE_LOCAL:
                                        lrIndication = OLR_STATE_LOCAL;
                                    break;

                                    case HMI_OLR_STATE_REMOTE:
                                        lrIndication = OLR_STATE_REMOTE;
                                    break;

                                    default:
                                        lrIndication = OLR_STATE_LAST;
                                    break;
                                }

                                if(database.setLocalRemote(lrIndication, OLR_SOURCE_HMI) != GDB_ERROR_NONE)
                                {
                                    setCurrentOLRstatus();  //refresh OLR status from DB if possible
                                }
                            }
                            else
                            {
                                /* System offline. Ignore event */
                                log.info("%s HMI Manager offline. OLR event discarded", FTITLE);
                            }
                        break;

                        case Event::EVENT_TYPE_STATUS:
                        {
                            /* Get status */
                            lu_bool_t online;
                            eventPtr->getStatus(online);
                            if(online == LU_TRUE)
                            {
                                /*HMI come to online, Set OLR status */
                                setCurrentOLRstatus();

                                /* Draw screen, Force a full resynch */
                                if(activeScreen != NULL)
                                    activeScreen->draw(LU_TRUE);
                            }
                            else
                            {
                                /* HMI come to offline */
                                logout();
                            }
                        }
                        break;

                        default:
                            log.error("%s Unsupported event %i", FTITLE, eventType);
                        break;
                    }//endSwitch(eventType)
                }
                break;
                case OBJECTQUEUERESULT_TIMEOUT:
                {
                    /* Forward event only if HMI is online */
                    if(chOnline == LU_TRUE)
                    {
                        if(activeScreen != NULL)
                            activeScreen->tickEvent();
                    }
                }
                break;
                default:
                {
                    //error
                    log.error("%s Error %i retrieving events", FTITLE, ret);
                }
            }//endSwitch(ret)

            if(ret != OBJECTQUEUERESULT_TIMEOUT)
            {
                /* Adjust next timeout for tickEvent */
                clock_gettimeval(&timeNew); //get new present time stamp
                lu_uint64_t tdiff_ms = timeval_elapsed_ms(&timeRef, &timeNew);
                if(tdiff_ms > 1)
                {
                    //adjust timeout for tickEvent to meet periodicTickMs
                    timeout = ms_to_timeval(periodicTickMs - tdiff_ms);
                }
                else
                {
                    //force timeout
                    timeout = ms_to_timeval(0);
                }
            }
            else
            {
                //renew timeout
                timeout = ms_to_timeval(periodicTickMs);
            }
        } //End of eventPtr scope: Event is already deleted in the eventPtr destructor
    } //endwhile

    //Clear screen before exiting screen manager
    if(chOnline == LU_TRUE)
    {
        clearLCD();
    }
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

HMI_M_ERROR ScreenManager::nextScreen(Screen *screen)
{
    if(screen == NULL)
    {
        return HMI_M_ERROR_PARAM;
    }

    activeScreen->eventExit();

    /* Save new screen */
    activeScreen = screen;
    /* Draw screen */
    activeScreen->eventEnter();
    activeScreen->draw(LU_FALSE);

    return HMI_M_ERROR_NONE;
}

HMI_M_ERROR ScreenManager::setCurrentOLRstatus()
{
    HMI_M_ERROR ret = HMI_M_ERROR_INVALID_POINT;
    OLR_STATE olr;
    HMI_OLR_STATE hmiOlr;

    /* Get OLR status */
    if(database.getLocalRemote(olr) == GDB_ERROR_NONE)
    {

        /* Convert OLR_STATE to HMI_OLR_STAT */
        switch(olr)
        {
            case OLR_STATE_OFF:
                hmiOlr = HMI_OLR_STATE_OFF;
                break;
            case OLR_STATE_LOCAL:
                hmiOlr = HMI_OLR_STATE_LOCAL;
                break;
            case OLR_STATE_REMOTE:
                hmiOlr = HMI_OLR_STATE_REMOTE;
                break;
            default:
                hmiOlr = HMI_OLR_STATE_OFF;
                break;
        }

        //TODO check if hmiChannel is online
        /* Send command */
        RETRY(ret, hmiChannel.setOLRState(hmiOlr));
    }
    return ret;
}


/*
 *********************** End of file ******************************************
 */
