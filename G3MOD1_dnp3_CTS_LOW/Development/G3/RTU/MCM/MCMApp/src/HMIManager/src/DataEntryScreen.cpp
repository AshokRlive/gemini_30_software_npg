/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Data entry type screen implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24/02/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

//#include <cstdlib>
#include <stdlib.h>
#include <cstring>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "DataEntryScreen.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */


DataEntryScreen::DataEntryScreen( ScreenManager &mediator,
                                  lu_uint32_t accessLevel,
                                  const lu_char_t *title,
                                  lu_uint8_t rows,
                                  lu_uint8_t columns
                                ) : Screen(mediator, accessLevel, title, rows, columns),
                                    currentPageIdx(0),
                                    lastPage(0)
{}

DataEntryScreen::~DataEntryScreen()
{
    // Protect the entry list
    MasterLockingMutex mLock(queueMutex, LU_TRUE);

    /* Delete all entries */
    DataEntry *entryTmp = dataEntries.getFirst();
    while(entryTmp != NULL)
    {
        /* Remove entry from the queue */
        dataEntries.remove(entryTmp);

        /* Delete entry */
        delete entryTmp;

        /* Get the new first entry */
        entryTmp = dataEntries.getFirst();
    }
}


HMI_M_ERROR DataEntryScreen::draw(lu_bool_t fullSynch)
{
    PayloadRAW bufferRAW;
    StringRAW  stringBuffer;

    if(lcdBuffer == NULL)
    {
        return HMI_M_ERROR_NO_SCREEN;
    }

    /* Get LCD Buffer */
    lcdBuffer->getLCDBuffer(bufferRAW);

    /***************/
    /* Write title */
    /***************/

    /* Get first line */
    stringBuffer.payloadPtr = reinterpret_cast<lu_char_t*>(bufferRAW.payloadPtr);
    stringBuffer.payloadLen = columns + 1;
    if(lastPage > 0)
    {
        centredWrite(stringBuffer, title, currentPageIdx+1, lastPage);
    }
    else
    {
        centredWrite(stringBuffer, title, LU_FALSE);
    }

    /*****************/
    /* Write Entries */
    /*****************/

    // Protect the entry list
    MasterLockingMutex mLock(queueMutex);

    DataEntry *entryTmp;
    /* Get first entry of the page. The number of available rows is
     * (rows - 1): the first row is reserved for the title
     */
    entryTmp = dataEntries.getIdx(currentPageIdx * (rows - 1));

    /* Update all the remaining rows */
    for(lu_uint8_t i = 1; i < rows; ++i)
    {
        /* Get new line */
        stringBuffer.payloadPtr =
               reinterpret_cast<lu_char_t*>(&bufferRAW.payloadPtr[i * columns]);

        if(entryTmp != NULL)
        {
            /* Valid Entry. Write it */
            entryTmp->write(stringBuffer);

            /* Get next entry */
            entryTmp = dataEntries.getNext(entryTmp);
        }
        else
        {
            /* No more screens. Clear the line */
            memset(stringBuffer.payloadPtr, ' ', stringBuffer.payloadLen);
        }
    }

    /* Send the buffer to the mediator */
    lcdBuffer->setPosition(0, 0, rows*columns);
    mediator.writeLCD(*lcdBuffer, fullSynch);

    return HMI_M_ERROR_NONE;
}

HMI_M_ERROR DataEntryScreen::resetHistory()
{
    currentPageIdx = 0;

    return HMI_M_ERROR_NONE;
}

HMI_M_ERROR DataEntryScreen::tickEvent()
{
    return draw(LU_FALSE);
}


HMI_M_ERROR DataEntryScreen::addEntry(DataEntry *data)
{
    if(data == NULL)
    {
        return HMI_M_ERROR_PARAM;
    }

    // Protect the entry list
    MasterLockingMutex mLock(queueMutex, LU_TRUE);

    if(dataEntries.add(data) == LU_TRUE)
    {
        /* Get the number of available raws for data:
         * the first raw is reserver for the title
         */
        lu_uint32_t availableRows = rows - 1;

        /* Update the maximum number of pages. Approximate by excess */
        div_t result = div(dataEntries.getEntries(), availableRows);
        lastPage = result.quot;
        if(result.rem > 0)
        {
            lastPage++;
        }
        return HMI_M_ERROR_NONE;
    }
    else
    {
        return HMI_M_ERROR_ADD;
    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

HMI_M_ERROR DataEntryScreen::customButtonEventHandler(ButtonsStatus &status)
{
    lu_bool_t forceDraw = LU_FALSE;

    buttonStatus bStatus;

    status.getStatus(HMI_M_BUTTON_IDX_DOWN, bStatus);
    if( (bStatus.event == LU_TRUE) && (bStatus.pressed == LU_TRUE) )
    {
        /* Scroll down to next page */
        if(currentPageIdx < (lastPage - 1))
        {
            /* Activate next page */
            currentPageIdx++;

            forceDraw = LU_TRUE;
        }
    }

    status.getStatus(HMI_M_BUTTON_IDX_UP, bStatus);
    if( (bStatus.event == LU_TRUE) && (bStatus.pressed == LU_TRUE) )
    {
        /* Scroll up to previous page */
        if(currentPageIdx > 0)
        {
            /* Activate next entry */
            currentPageIdx--;

            forceDraw = LU_TRUE;
        }
    }

    if(forceDraw == LU_TRUE)
    {
        return draw(LU_FALSE);
    }

    return HMI_M_ERROR_NONE;
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
