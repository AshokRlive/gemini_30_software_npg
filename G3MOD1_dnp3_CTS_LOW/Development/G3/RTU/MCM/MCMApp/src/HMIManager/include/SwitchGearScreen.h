/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Switch Gear Control Screen Interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   27/02/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_5A3D0892_A455_4f68_A76F_AD08F78595DE__INCLUDED_)
#define EA_5A3D0892_A455_4f68_A76F_AD08F78595DE__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Screen.h"
#include "GeminiDatabase.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Control a switch gear using a Switch Gear Control Block
 *
 * Screen layout
 *
 * Title
 * Switch status: open/close
 * Operation state: operating/non operating
 * Error code
 */
class SwitchGearScreen : public Screen
{

public:
    /**
     * \brief Custom Constructor
     *
     * \param mediator Reference to the mediator
     * \param accessLevel Screen access level
     * \param title Pointer to the screen title
     * \param rows Screen rows
     * \param columns Screen columns
     * \param database Reference to the gemini database
     * \param cLogicGID Group ID of the control logic to use
     *
     * \return None
     */
    SwitchGearScreen( ScreenManager &mediator,
                      lu_uint32_t accessLevel,
                      const lu_char_t *title,
                      lu_uint8_t rows,
                       lu_uint8_t columns,
                      GeminiDatabase &database,
                      lu_uint16_t cLogicGID,
                      lu_uint64_t openClosePressDelay_ms
                     );
    virtual ~SwitchGearScreen();

    /**
     * \brief Draw the screen
     *
     *  \param fullSynch Force a dull update of the LCD
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR draw(lu_bool_t fullSynch);

    /**
     * \brief Reset the screen internal history
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR resetHistory();

    /**
     * \brief Periodic tick event
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR tickEvent();
protected:

    /**
     * \brief Custom Button event handler
     *
     * This method is called by the buttonEvent method id the button event
     * has not been handled by the Screen handler
     *
     * \param status The buttons status
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR customButtonEventHandler(ButtonsStatus &status);

    void startSwitch(SWITCH_OPERATION operation);

protected:
    /**
     * \brief Database job in charge of start a switch command.
     *
     * This is the job used to start the operation only after the user presses the
     * OPEN or CLOSE buttons for enough time.
     * Being a TimedJob, the object is auto-destroyed after it finishes counting.
     */
    class StartSwitchJob : public TimedJob
    {
    public:
        StartSwitchJob(SwitchGearScreen &switchScreen,
                        lu_uint32_t timeout,
                        SWITCH_OPERATION operation
                        ):
                            TimedJob(timeout, 0),
                            swScreen(switchScreen),
                            operation(operation)
                            {};
        virtual ~StartSwitchJob() {};

        /* == Inherited from TimedJob == */
        void job(TimeManager::TimeStr *timePtr)
        {
            LU_UNUSED(timePtr);
            swScreen.startSwitch(operation);
        }

    private:
        SwitchGearScreen &swScreen;
        SWITCH_OPERATION operation;
    };

private:
    /**
     * \brief Composes a progress bar
     *
     * \param sBar Where to store the progress bar. Current content will be erased.
     */
    void updateProgressBar(std::string& sBar);

private:
    static const lu_uint8_t positionLine = 1;
    static const lu_uint8_t operatingLine = 2;
    static const lu_uint8_t errorLine = 3;

private:
    GeminiDatabase &database;
    lu_uint16_t cLogicGID;
    lu_bool_t operating;
    CONTROL_LOGIC_TYPE cLogicType;
    GDB_ERROR commandError;
    StartSwitchJob* switchJob;  //Switch Command job
    lu_uint64_t actPressDelay_ms; //Time (ms) for keeping pressed ACT+OPEN/CLOSE buttons
    bool requesting;            //Operation has been requested
    lu_uint32_t m_reqStart_ms;  //starting time of the request, im milliseconds
};


#endif // !defined(EA_5A3D0892_A455_4f68_A76F_AD08F78595DE__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
