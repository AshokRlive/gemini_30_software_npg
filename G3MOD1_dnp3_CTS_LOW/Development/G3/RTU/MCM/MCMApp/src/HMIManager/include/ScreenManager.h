/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       HMI screen manager interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   23/02/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_3421A928_DAB9_4ed5_898C_E2BA0039630F__INCLUDED_)
#define EA_3421A928_DAB9_4ed5_898C_E2BA0039630F__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Thread.h"
#include "IPoint.h"
#include "IPointObserver.h"
#include "Event.h"
#include "ObjectQueue.h"
#include "ButtonHandler.h"
#include "HMICANChannel.h"
#include "Logger.h"

/* Forward declaration */
class GeminiDatabase;
class Screen;
class OLRObserver;  //forward declaration of OLRObserver class for ScreenManager

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 * Set to 1 to force HMI manager to get screen size from HMI. This is not supported
 *  cause the design of HMIManager and Screen Manager cannot handle this very well.
 *
 * Explanation:
 * Getting size from HMI is not preferred since HMI may not be present at this moment.
 *
 * If we really need to get size from HMI, the design of HMIManager and Screen
 * Manager must be improved a lot, because before reading size we have to consider the state of HMI,
 * and check if it is present and active, and also, next time we detect HMI popping up,
 *  we must request size again and re-create all screens based on the new size,
 *  which is very costly.
 *
 * If we don't remove the dependency between Screens and  HMI size,
 * it is always too difficult to handle dynamically changed HMI size.
 *   So the simple solution is to use a fixed predefined Screen size.
 *
 */
#define GET_SIZE_FROM_HMI 0

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Buttons status manager
 */
class ButtonsStatus
{

public:
    /**
     * \brief Custom constructor
     *
     * \param status Pointer to the buttons status array
     * \param entries Status array entries
     *
     * \return None
     */
    ButtonsStatus(buttonStatus *status, lu_uint32_t entries) : buttons(status) ,
                                                               entries(entries)
    {};

    virtual ~ButtonsStatus() {};

    /**
     * \brief Get the status of a button
     *
     * \param entry Button id
     * \param status Buffer where the button status is saved
     *
     * \return Error Code
     */
    HMI_M_ERROR getStatus(HMI_M_BUTTON_IDX entry, buttonStatus &status);
private:
    buttonStatus *buttons;
    lu_uint32_t entries;
};

/**
 * \brief Screen manager
 *
 * This is a mediator. It handles the screens and the buttons state.
 * It is directly connected to the HMI channel.
 *
 * The events are handled in the screen manager thread.
 */
class ScreenManager : public ChannelObserver, private Thread
{

public:
    /**
     * \brief Custom Constructor
     *
     * \param schedType Thread type
     * \param priority thread priority
     * \param database Reference to the internal database
     * \param hmiChannel Pointer to the HMI channel
     * \param startScreen Pointer to the root screen
     *
     * \return None
     */
    ScreenManager( SCHED_TYPE schedType,
                   lu_uint32_t priority,
                   GeminiDatabase &database,
                   HMICANChannel &hmiChannel
                 );
    virtual ~ScreenManager();

    /**
     * \brief Start the manager thread
     *
     * The root screen screen is also draw to the LCD
     *
     * \return Error Code
     */
    HMI_M_ERROR startServer();

    /**
     * \brief Initialise screen manager with screens.
     * 
     * If the screen  manager thread is running the screen is also drawn
     * 
     * \param 
     *
     * \return Error code
     */
    HMI_M_ERROR initialise(Screen *rootScreen, Screen *logonScreen);

    /**
     * \brief Display the parent screen of the active screen
     *
     * \return Error code
     */
    HMI_M_ERROR parentScreen();

    /**
     * \brief Display the idx child of the active screen
     *
     * \return Error code
     */
    HMI_M_ERROR selectedScreen(lu_uint32_t idx);

    /**
     * \brief Display the root screen
     *
     * \return Error code
     */
    HMI_M_ERROR homeScreen();

    HMI_M_ERROR logout();

    /**
     * \brief Set LEDs
     *
     * Not supported
     *
     * \return Error Code
     */
    HMI_M_ERROR setLeds();

    /**
     * \brief Write to the LCD
     *
     * It retries until the screen is successfully written
     *
     * \param data Reference to the data to write
     * \param fullSynch Force LCD full resynch
     *
     * \return Error Code
     */
    HMI_M_ERROR writeLCD(LCDData &data, lu_bool_t fullSynch);

    /**
     * \brief Clear entire LCD
     *
     * \return Error code
     */
    HMI_M_ERROR clearLCD();

    /**
     * \brief Play a sound
     *
     * Not Supported
     *
     * \return Error code
     */
    HMI_M_ERROR playSound();

    /**
     * \brief Get screen size
     *
     * \param rows Buffer where the number of rows is saved
     * \param columns Buffer where the number of columns is saved
     *
     * \return Error code
     */
    HMI_M_ERROR screenSize(lu_int8_t &rows, lu_int8_t &columns);

    /**
     * \brief Notify a button event
     *
     * The event is forwarded to the active screen
     *
     * \param status Reference to the buttons status
     *
     * \return None
     */
    void buttonEvent(ButtonsStatus &status);

    /**
     * \brief notify that a channel has been updated
     *
     * The subject is passed as a parameter: the observer can monitor multiple
     * channels at the same time
     *
     * \param subject The changed channel.
     *
     * \return None
     */
    virtual void update(IChannel *subject);

    void suspend()
    {
        this->suspended = LU_TRUE;
    };

protected:
    /**
     * \brief Main thread of the screen manager
     *
     * Wait for an event from the eventQueue.
     * A buttonEvent the event is forwarded to the active screen.
     * A OLR event is handled directly in the thread.
     *
     * This thread also generates the periodic tick for the screen.
     *
     * \return None
     */
    virtual void threadBody();

private:
    /**
     * \brief Activate this screen
     *
     * The draw method of the new screen is automatically called
     *
     * \param screen pointer to the screen to activate
     *
     * \return Error code
     */
    HMI_M_ERROR nextScreen(Screen *screen);

    HMI_M_ERROR setCurrentOLRstatus();

friend class OLRObserver;

private:
    static const lu_uint32_t periodicTickMs = 500;
    static const lu_uint32_t maxRetries = 10;

private:
    GeminiDatabase &database;
    Logger& log;
    Screen *rootScreen;
    Screen *logonScreen;
    Screen *activeScreen;
    HMICANChannel &hmiChannel;
    lu_bool_t chOnline;
    ButtonHandler buttonHandler;
    //EventQueue eventQueue;
    ObjectQueue<Event> eventQueue;     //Queue that stores Screen events

    OLRObserver *olrObserver;   //Updates OLR LEDs from any OLR status change

    lu_bool_t started;
    lu_bool_t suspended;
};


/**
 * Observer used to monitor the status of the Off/Local/Remote point.
 * For performance reasons the observed point value is saved in a local buffer.
 */
class OLRObserver : public IPointObserver
{
public:
    /**
     * \brief Default constructor
     *
     * \param screenManager HMI screen manager to be updated when OLR
     * point changes.
     */
    OLRObserver(PointIdStr pointId, ScreenManager &screenManager);
    virtual ~OLRObserver() {};

    /**
     * \brief Return the observed point ID
     *
     * \return Observed point ID
     */
    inline PointIdStr getPointID() {return pointID;};

    /**
     * \brief Custom observer function
     *
     * \param pointID Point ID
     * \param pointDataPtr Point data
     */
    virtual void update(PointIdStr pointID, PointData *pointDataPtr);

private:
    PointIdStr pointID;
    ScreenManager &screenManager;
    lu_uint8_t valueOLR;    //local copy
};


#endif // !defined(EA_3421A928_DAB9_4ed5_898C_E2BA0039630F__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
