/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: InfoScreen.h 15 May 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/HMIManager/include/InfoScreen.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       RTU Information Screens
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 15 May 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15 May 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef INFOSCREEN_H__INCLUDED
#define INFOSCREEN_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Screen.h"
#include "ScreenContent.h"
#include "IIOModuleManager.h"
#include "IStatusManager.h"
#include "MCMConfigEnum.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Display different types of RTU Information
 *
 * RTU Information types depends on the screen ID, and range from hardware
 * information to module alarms.
 * Horizontal and vertical screen scrolling is provided.
 */
class InfoScreen : public Screen
{
public:
    /**
     * \brief Custom Constructor
     *
     * \param mediator Reference to the mediator
     * \param accessLevel Screen access level
     * \param title Pointer to the screen title
     * \param rows Screen rows
     * \param columns Screen columns
     * \param database Reference to the gemini database
     * \param screenID Type of information to display
     *
     * \return None
     */
    InfoScreen( ScreenManager &mediator,
                lu_uint32_t accessLevel,
                const lu_char_t *title,
                lu_uint8_t rows,
                lu_uint8_t columns,
                IIOModuleManager& moduleMgr,
                IStatusManager& statusMgr,
                lu_uint32_t screenID
                );
    virtual ~InfoScreen();

    /* ==Inherited from Screen == */
    virtual HMI_M_ERROR draw(lu_bool_t fullSynch);
    virtual HMI_M_ERROR resetHistory();
    virtual HMI_M_ERROR tickEvent();
    virtual void eventEnter();
    virtual void eventExit();

protected:
    /* ==Inherited from Screen == */
    virtual HMI_M_ERROR customButtonEventHandler(ButtonsStatus &status);

    /**
     * \brief Create info screen content
     */
    void createContent();

protected:
    IIOModuleManager& moduleManager;
    IStatusManager& statusManager;
    lu_uint32_t screenType;     //Type of info screen
    lu_bool_t created;          //Info screen content already created (for static info only)
    ScreenContent content;      //Complete information content
    lu_uint32_t xpos, ypos;     //Current screen position coordinates
};

#endif /* INFOSCREEN_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
