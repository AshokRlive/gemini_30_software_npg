/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ScreenContent.cpp 19 May 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/HMIManager/src/ScreenContent.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 19 May 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19 May 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdio.h>
#include <stdarg.h>
#include <algorithm>    //use of std::remove()

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "ScreenContent.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

ScreenContent::~ScreenContent()
{
    clear();
}

void ScreenContent::clear()
{
    /* Clear strings one by one before clearing vector */
    for(ContentScreen::iterator it = content.begin(), end = content.end(); it != end; ++it)
    {
        it->clear();    //remove string
    }
    content.clear();    //remove vector
    height_m = 0;
    width_m = 0;
}


std::string ScreenContent::getLine(lu_uint32_t line)
{
    std::string ret;
    //Check line boundaries
    if(line < height_m)
    {
        ret = content.at(line);
    }
    return ret;
}


size_t ScreenContent::height()
{
    return height_m;
}


size_t ScreenContent::width()
{
    return width_m;
}


void ScreenContent::message(const std::string message)
{
    std::string line(message);
    line.erase( std::remove(line.begin(), line.end(), '\n'), line.end() );    //remove EOLs, if any
    content.push_back(message);
    recalculate();
}


void ScreenContent::message(const lu_char_t* message, ...)
{
    FILE* logStream;    //stream in memory for string conversion
    lu_char_t* lineBuffer;
    size_t bufSize;
    va_list args;

    logStream = open_memstream(&lineBuffer, &bufSize);
    va_start(args, message);
    vfprintf(logStream, message, args);
    va_end(args);
    fflush(logStream);
    rewind(logStream);
    lu_char_t* lineBuf = new lu_char_t[bufSize];
    fread(lineBuf, 1, bufSize, logStream);
    std::string line(lineBuf, bufSize);
    fclose(logStream);
    delete [] lineBuf;
    line.erase( std::remove(line.begin(), line.end(), '\n'), line.end() );    //remove EOLs, if any
    content.push_back(line);
    recalculate();
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void ScreenContent::recalculate()
{
    height_m = content.size();
    width_m = 0;
    for(ContentScreen::iterator it = content.begin(), end = content.end(); it != end; ++it)
    {
        width_m = std::max((*it).length(), width_m);
    }
}


/*
 *********************** End of file ******************************************
 */
