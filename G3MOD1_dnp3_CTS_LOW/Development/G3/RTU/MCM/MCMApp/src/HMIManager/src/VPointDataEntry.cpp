/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Analogue Data entry implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24/02/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <cstdio>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "VPointDataEntry.h"
#include "Screen.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
template <typename DataType>
static void sprintValue(StringRAW& buffer,
                        const DataType value,
                        const lu_char_t* label, //= NULL,
                        const lu_char_t* format, // = NULL,
                        const lu_bool_t valid //= LU_FALSE
                        )
{
    lu_int32_t res = -1;
    buffer.payloadPtr[0] = '\0';    //Sanitize before using
    if( ( (label == NULL) && (format == NULL) ) || (valid == LU_FALSE) )
    {
        //Not available
        res = snprintf(buffer.payloadPtr, buffer.payloadLen, Screen::NotAvailableString);
    }
    else if(label != NULL)
    {
        if(strlen(label) > 0)
        {
            //Label instead of value
            res = snprintf(buffer.payloadPtr, buffer.payloadLen, "%s", label);
        }
    }
    if(res < 0)
    {
        //Print value if no other thing printed
        res = snprintf(buffer.payloadPtr, buffer.payloadLen, format, value);
        //Check correctness of the outcome
        if(isprint(buffer.payloadPtr[0]) == 0)
        {
            buffer.payloadPtr[0] = '\0';
        }
    }
    if(res <= 0)
    {
        buffer.payloadPtr[0] = '\0';
    }
}

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

VPointDataEntry::VPointDataEntry( const lu_char_t *header,
                                  const lu_char_t *footer,
                                  GeminiDatabase &database,
                                  PointIdStr point
                                ): DataEntry(header, footer),
                                   database(database),
                                   point(point)

{}



VPointDataEntry::~VPointDataEntry(){}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
HMI_M_ERROR VPointDataEntry::getDataString(StringRAW &buffer)
{
    /* This is a protected method.
     * We don't need to check the input parameters
     */
    if(database.getPointType(point) != POINT_TYPE_INVALID)
    {
        switch(database.getPointType(point))
        {
            case POINT_TYPE_ANALOGUE:
            {
                PointDataFloat32 value;
                database.getValue(point, &value);
                lu_float32_t fValue = (lu_float32_t)(value);
                sprintValue(buffer, fValue, database.getValueLabel(point, &value), "%.03f", value.isActive());
                break;
            }
            case POINT_TYPE_BINARY:
            case POINT_TYPE_DBINARY:
            {
                PointDataUint8 value;
                database.getValue(point, &value);
                lu_uint8_t dValue = (lu_uint8_t)(value);
                sprintValue(buffer, dValue, database.getValueLabel(point, &value), "%1u", value.isActive());
                break;
            }
            case POINT_TYPE_COUNTER:
            {
                PointDataCounter32 value;
                // TODO - Verify!!
                database.getValue(point, &value);
                lu_uint32_t cValue = (lu_uint32_t)(value);
                sprintValue(buffer, cValue, database.getValueLabel(point, &value), "%u", value.isActive());
                break;
            }
            default:
                snprintf(buffer.payloadPtr, buffer.payloadLen, Screen::NotAvailableString);
                break;
        }
    }
    else
    {
        /* Error reading from the database */
        snprintf( buffer.payloadPtr, buffer.payloadLen, Screen::NotAvailableString);
    }
    buffer.payloadPtr[buffer.payloadLen-1] = '\0';

    return HMI_M_ERROR_NONE;
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */





/*
 *********************** End of file ******************************************
 */
