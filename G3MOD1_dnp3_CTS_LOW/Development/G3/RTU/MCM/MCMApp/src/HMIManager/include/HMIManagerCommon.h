/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       HMI Manager Global header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   23/02/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_DE0A0133_2102_4fec_94AC_751C386C9503__INCLUDED_)
#define EA_DE0A0133_2102_4fec_94AC_751C386C9503__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

enum HMI_M_ERROR
{
    HMI_M_ERROR_NONE      = 0,
    HMI_M_ERROR_PARAM        ,
    HMI_M_ERROR_NO_SCREEN    ,
    HMI_M_ERROR_CLIST        ,
    HMI_M_ERROR_NOT_SUPPORTED,
    HMI_M_ERROR_ADD          ,
    HMI_M_ERROR_THREAD       ,
    HMI_M_ERROR_ROOT         ,
    HMI_M_ERROR_CHANNEL      ,
    HMI_M_ERROR_INVALID_POINT,

    HMI_M_ERROR_LAST
};

enum HMI_M_BUTTON_IDX
{
    HMI_M_BUTTON_IDX_UP = 0,
    HMI_M_BUTTON_IDX_DOWN  ,
    HMI_M_BUTTON_IDX_LEFT  ,
    HMI_M_BUTTON_IDX_RIGHT ,
    HMI_M_BUTTON_IDX_OK    ,
    HMI_M_BUTTON_IDX_MENU  ,
    HMI_M_BUTTON_IDX_ESC   ,
    HMI_M_BUTTON_IDX_SW1   ,
    HMI_M_BUTTON_IDX_SW2   ,
    HMI_M_BUTTON_IDX_SW3   ,
    HMI_M_BUTTON_IDX_SW4   ,
    /* ACT & Open */
    HMI_M_BUTTON_IDX_OPEN  ,
    /* ACT & Close */
    HMI_M_BUTTON_IDX_CLOSE ,
    HMI_M_BUTTON_IDX_ACT_ONLY,
    HMI_M_BUTTON_IDX_LAST
};

struct buttonStatus
{
    lu_bool_t pressed;
    lu_bool_t event;
};

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

#endif // !defined(EA_DE0A0133_2102_4fec_94AC_751C386C9503__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
