/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Analogue Values Screen Public Interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24/02/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_776834C2_7D13_499f_92BF_6E5D4F8441E5__INCLUDED_)
#define EA_776834C2_7D13_499f_92BF_6E5D4F8441E5__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Screen.h"
#include "GeminiDatabase.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Display 3 phase currents and voltages + neutral current.
 *
 * Screen layout
 *
 * aa: current unit
 * bb: voltage unit
 *
 * Page 1:
 * I0 1234aa V0 12345bb
 * I1 1234aa V1 12345bb
 * I2 1234aa V2 12345bb
 *
 * Page 2:
 * In 1234aa
 */
class PhaseScreen : public Screen
{
public:
    enum PHASE
    {
        PHASE_A = 0,
        PHASE_B = 1,
        PHASE_C = 2,
        PHASE_I_A = PHASE_A * 2,
        PHASE_V_A = (PHASE_A * 2) + 1 ,
        PHASE_I_B = PHASE_B * 2,
        PHASE_V_B = (PHASE_B * 2) + 1 ,
        PHASE_I_C = PHASE_C * 2,
        PHASE_V_C = (PHASE_C * 2) + 1 ,
        PHASE_I_N = PHASE_V_C + 1,

        PHASE_LAST = PHASE_I_N + 1
    };

public:
    /**
     * \brief Custom Constructor
     *
     * \param mediator Reference to the mediator
     * \param accessLevel Screen access level
     * \param title Pointer to the screen title
     * \param rows Screen rows
     * \param columns Screen columns
     * \param database Reference to the gemini database
     * \param phases 3phases and neutral (see PHASE enum for the order)
     * \param currentFooter pointer to the currents footer
     * \param voltageFooter pointer to the voltages footer
     *
     * \return None
     */
    PhaseScreen( ScreenManager &mediator,
                 lu_uint32_t accessLevel,
                 const lu_char_t *title,
                 lu_uint8_t rows,
                 lu_uint8_t columns,
                 GeminiDatabase &database,
                 PointIdStr phases[PHASE_LAST],
                 const lu_char_t *currentFooter,
                 const lu_char_t *voltageFooter
                );

    virtual ~PhaseScreen() {};

    /**
     * \brief Draw the screen
     *
     *  \param fullSynch Force a dull update of the LCD
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR draw(lu_bool_t fullSynch);

    /**
     * \brief Reset the screen internal history
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR resetHistory();

    /**
     * \brief Periodic tick event
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR tickEvent();

protected:

    /**
     * \brief Custom Button event handler
     *
     * This method is called by the buttonEvent method id the button event
     * has not been handled by the Screen handler
     *
     * \param status The buttons status
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR customButtonEventHandler(ButtonsStatus &status);

private:
    static void sprintValue(lu_char_t* dataString,
                            const lu_uint32_t datasize,
                            const lu_char_t* label = NULL,
                            const lu_char_t* format = NULL,
                            const lu_float32_t value = 0,
                            const lu_char_t* footer = NULL,
                            const lu_bool_t valid = LU_FALSE
                            );

private:
    enum PAGE_IDX
    {
        PAGE_IDX_1 = 1,
        PAGE_IDX_2 = 2,

        PAGE_IDX_MAX = PAGE_IDX_2
    };

    GeminiDatabase &database;
    lu_char_t currentFooter[maxFooterSize];
    lu_char_t voltageFooter[maxFooterSize];
    PointIdStr phases[PHASE_LAST];

    PAGE_IDX page;

};
#endif // !defined(EA_776834C2_7D13_499f_92BF_6E5D4F8441E5__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
