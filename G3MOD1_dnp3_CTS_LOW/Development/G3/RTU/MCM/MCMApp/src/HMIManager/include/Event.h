/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_DBE5FF8D_58EB_41e8_9D32_35E848CC7C35__INCLUDED_)
#define EA_DBE5FF8D_58EB_41e8_9D32_35E848CC7C35__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "QueueMgr.h"
#include "QueueObj.h"
#include "HMIManagerCommon.h"
#include "ModuleProtocol/ModuleProtocolHMIController.h"
#include "GlobalDefs.h"

class Event;
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


typedef QueueMgr<Event> EventQueue;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class Event : public QueueObj<Event>
{
public:
    enum EVENT_TYPE
    {
        EVENT_TYPE_BUTTON = 0,
        EVENT_TYPE_OLR       ,
        EVENT_TYPE_STATUS    ,

        EVENT_TYPE_INVALID
    };

public:
    Event() {};
    ~Event() {};

    /**
     * \brief Get event type
     *
     * \return Event type
     */
    virtual EVENT_TYPE getEventType() = 0;

    /**
     * \brief Get Buttons state
     *
     * \param state Where the state is saved
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR getButtonState(lu_uint32_t &state) = 0;

    /**
     * \brief Get OLR event value
     *
     * \param olr Buffer where the event is saved
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR getOLR(HMI_OLR_STATE &olr) = 0;

    /**
     * \brief Get online/offline event value
     *
     * \param online Where the status is saved
     *
     * \return error code
     */
    virtual HMI_M_ERROR getStatus(lu_bool_t &online) = 0;
};

class ButtonEvent : public Event
{
public:
	ButtonEvent(lu_uint32_t buttonState) : buttonState(buttonState) {};
	virtual ~ButtonEvent() {};

    /**
     * \brief Get event type
     *
     * \return Event type
     */
    virtual EVENT_TYPE getEventType();

    /**
     * \brief Get Buttons state
     *
     * \param state Where the state is saved
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR getButtonState(lu_uint32_t &state);

    /**
     * \brief Get OLR event value
     *
     * \param olr Buffer where the event is saved
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR getOLR(HMI_OLR_STATE &olr);

    /**
     * \brief Get online/offline event value
     *
     * \param online Where the status is saved
     *
     * \return error code
     */
    virtual HMI_M_ERROR getStatus(lu_bool_t &online);
private:
	lu_uint32_t buttonState;
};

class OLREvent : public Event
{

public:
	OLREvent(HMI_OLR_STATE olr) : olr(olr) {};
	virtual ~OLREvent() {};

    /**
     * \brief Get event type
     *
     * \return Event type
     */
    virtual EVENT_TYPE getEventType();

    /**
     * \brief Get Buttons state
     *
     * \param state Where the state is saved
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR getButtonState(lu_uint32_t &state);

    /**
     * \brief Get OLR event value
     *
     * \param olr Buffer where the event is saved
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR getOLR(HMI_OLR_STATE &olr);

    /**
     * \brief Get online/offline event value
     *
     * \param online Where the status is saved
     *
     * \return error code
     */
    virtual HMI_M_ERROR getStatus(lu_bool_t &online);
private:
    HMI_OLR_STATE olr;
};

class StatusEvent : public Event
{

public:
	StatusEvent(lu_bool_t online) : online(online) {};
	virtual ~StatusEvent() {};

    /**
     * \brief Get event type
     *
     * \return Event type
     */
    virtual EVENT_TYPE getEventType();

    /**
     * \brief Get Buttons state
     *
     * \param state Where the state is saved
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR getButtonState(lu_uint32_t &state);

    /**
     * \brief Get OLR event value
     *
     * \param olr Buffer where the event is saved
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR getOLR(HMI_OLR_STATE &olr);

    /**
     * \brief Get online/offline event value
     *
     * \param online Where the status is saved
     *
     * \return error code
     */
    virtual HMI_M_ERROR getStatus(lu_bool_t &online);

private:
    lu_bool_t online;

};

#endif // !defined(EA_DBE5FF8D_58EB_41e8_9D32_35E848CC7C35__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
