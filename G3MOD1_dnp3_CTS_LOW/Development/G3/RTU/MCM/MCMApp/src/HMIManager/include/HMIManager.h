/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   01/03/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_60FD109F_33AB_434d_B9EC_8A1D4C43BEF2__INCLUDED_)
#define EA_60FD109F_33AB_434d_B9EC_8A1D4C43BEF2__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "HMIManagerCommon.h"
#include "Screen.h"
#include "ScreenManager.h"
#include "Mutex.h"
#include "CANInterface.h"
#include "ISupportPowerSaving.h"
#include "Timer.h"
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * At the moment this is just a wrapper around the ScreenManager class
 *
 * It hides the ScreenManager interface: the ScreenManager is invisible and not
 * accessible from outside the HMIManager
 */
class HMIManager: private ChannelObserver,public ISupportPowerSaving,private ITimerHandler
{
public:
    /**
     * \brief custom constructor
     *
     * \param schedType Scheduler to be used by the HMI Manager
     * \param priority HMI Manager thread priority
     * \param database Reference to the database
     * \param hmiChannel Reference to the HMI channel
     *
     * \return None
     */
    HMIManager(SCHED_TYPE schedType,
                lu_uint32_t priority,
                GeminiDatabase &database,
                IChannel& hmiDetect,
                IChannel& hmiPowerEnable,
                IChannel& hmiPowerGood,
                HMICANChannel* hmiChannel
               );

    virtual ~HMIManager();

    void initialize(IHMIFactory& hmiFactory);

    /* Override*/
    virtual void setPowerSaveMode(lu_bool_t mode) ;

    /**
     * Terminate HMI manager by cutting off HMI power and stopping screen manager.
     *
     */
    void terminate();

private:

    /* Inherited method for timer handling -- Bring up HMI interface */
    virtual void handleAlarmEvent(Timer &source);

    /* Inherited from IChannelObserver */
    virtual void update(IChannel *subject);

    void setHMIPower();

    void setCANIface();

    void setScreenServer();

    Logger&     log;
    Mutex       mutex;

    ScreenManager* screenManager;
    CANInterface&  can1;

    // HMI related channels
    IChannel& hmiDetectCH;
    IChannel& hmiPowerEnableCH;
    IChannel& hmiPowerGoodCH;

    // HMI status
    lu_bool_t hmiDetected;
    lu_bool_t hmiPowerEnabled;
    lu_bool_t hmiPowerGood;
    lu_bool_t powerSavingEnabled;

    lu_bool_t terminated; //Indicate if this HMI manager has been terminated.


    Timer canIfaceTimer;
};
#endif // !defined(EA_60FD109F_33AB_434d_B9EC_8A1D4C43BEF2__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
