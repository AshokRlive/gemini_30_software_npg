/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       HMI buttons handler implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24/02/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "ButtonHandler.h"
#include "ScreenManager.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/**
 * \brief Valid event bitmask
 *
 * For each button has been defined the equivalent activation bitmask
 */
lu_uint32_t buttonsMask[HMI_M_BUTTON_IDX_LAST] =
{
    HMI_BUTTON_BIT_MASK_PB_NAV_UP,
    HMI_BUTTON_BIT_MASK_PB_NAV_DOWN,
    HMI_BUTTON_BIT_MASK_PB_NAV_LEFT,
    HMI_BUTTON_BIT_MASK_PB_NAV_RIGHT,
    HMI_BUTTON_BIT_MASK_ID_PB_OK,
    HMI_BUTTON_BIT_MASK_PB_MENU,
    HMI_BUTTON_BIT_MASK_PB_ESC,
    HMI_BUTTON_BIT_MASK_SOFT_LEFT_1,
    HMI_BUTTON_BIT_MASK_SOFT_LEFT_2,
    HMI_BUTTON_BIT_MASK_SOFT_RIGHT_1,
    HMI_BUTTON_BIT_MASK_SOFT_RIGHT_2,
    HMI_BUTTON_BIT_MASK_SW_OPEN | HMI_BUTTON_BIT_MASK_PB_SW_ACTIVATE,
    HMI_BUTTON_BIT_MASK_PB_SW_CLOSE  | HMI_BUTTON_BIT_MASK_PB_SW_ACTIVATE,
    HMI_BUTTON_BIT_MASK_PB_SW_ACTIVATE
};



/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

ButtonHandler::ButtonHandler(ScreenManager &mediator) : mediator(mediator),
                                                        RAWState(0)
{
    for(lu_uint32_t i = 0; i < HMI_M_BUTTON_IDX_LAST; ++i )
    {
        buttonsStatus[i].pressed = LU_FALSE;
        buttonsStatus[i].event  = LU_FALSE;
    }
}

ButtonHandler::~ButtonHandler(){}

void ButtonHandler::buttonEventRAW(lu_uint32_t state)
{
    lu_bool_t notifyMediator = LU_FALSE;

    /* Look for an event. Scan through all the buttons */
    for(lu_uint32_t i = 0; i < HMI_M_BUTTON_IDX_LAST; ++i)
    {
        /* Check if the current button is pressed */
        lu_bool_t pressed =
              ( (state & buttonsMask[i]) == buttonsMask[i])? LU_TRUE : LU_FALSE;

        /* Get a reference to the previous status of the current button */
        buttonStatus *prevState = &buttonsStatus[i];

        /* Button status changed */
        if(prevState->pressed != pressed)
        {
            /* Update button status */
            prevState->pressed = pressed;
            prevState->event = LU_TRUE;

            /* An event happen Notify the mediator */
            notifyMediator = LU_TRUE;
        }
    }

    if(notifyMediator == LU_TRUE)
    {
        /* Notify mediator */
        ButtonsStatus buttons(buttonsStatus, HMI_M_BUTTON_IDX_LAST);
        mediator.buttonEvent(buttons);

        /* Reset event flag */
        for(lu_uint32_t i = 0; i < HMI_M_BUTTON_IDX_LAST; ++i)
        {
            buttonsStatus[i].event = LU_FALSE;
        }
    }
}
/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
