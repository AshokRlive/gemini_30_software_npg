/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   01/03/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "HMIManager.h"
#include "LockingMutex.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/* Time delay for bringing up CAN interface*/
static const struct timeval CAN_IFACE_UP_DELAY = {2, 0}; //2 seconds

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

HMIManager::HMIManager(SCHED_TYPE schedType,
        lu_uint32_t priority,
        GeminiDatabase &database,
        IChannel& g_hmiDetect,
        IChannel& g_hmiPowerEnable,
        IChannel& g_hmiPowerGood,
        HMICANChannel* hmiChannel) :
                log(Logger::getLogger(SUBSYSTEM_ID_HMIM)),
                mutex(LU_TRUE),
                screenManager(NULL),
                can1(CANInterface::getInstance(COMM_INTERFACE_CAN1)),
                hmiDetectCH(g_hmiDetect),
                hmiPowerEnableCH(g_hmiPowerEnable),
                hmiPowerGoodCH(g_hmiPowerGood) ,
                hmiDetected         (LU_FALSE) ,
                hmiPowerEnabled     (LU_FALSE) ,
                hmiPowerGood        (LU_FALSE) ,
                powerSavingEnabled  (LU_FALSE) ,
                terminated          (LU_FALSE) ,
                canIfaceTimer(CAN_IFACE_UP_DELAY.tv_sec,
                              CAN_IFACE_UP_DELAY.tv_usec,
                                this,
                                Timer::TIMER_TYPE_ONESHOT,
                                "CANInterfaceTmr")

{
    if(hmiChannel != NULL)
    {
        screenManager = new ScreenManager(schedType, priority, database,
                        *hmiChannel);
    }
}


HMIManager::~HMIManager()
{
    hmiDetectCH.detach(this);
    hmiPowerGoodCH.detach(this);

    if(screenManager != NULL)
    {
        delete screenManager;
        screenManager = NULL;
    }
}


void HMIManager:: initialize(IHMIFactory& hmiFactory)
{
   lu_int8_t rows, columns;

   if(screenManager == NULL)
       return;

#if GET_SIZE_FROM_HMI
   lu_uint32_t counter = 0;
   while(LU_TRUE)
   {
       if(screenManager.screenSize(rows, columns) == HMI_M_ERROR_NONE)
       {
           Screen *root = hmiFactory.getScreen(screenManager, rows, columns);

           screenManager.initialise(root, logon);
       }
       else if(++counter > 5)
       {
           return HMI_M_ERROR_CHANNEL;
       }
   }
#else
   screenManager->screenSize(rows, columns);
   Screen *root = hmiFactory.getScreen(*screenManager, rows, columns);
   Screen *logon = hmiFactory.getLogonScreen(*screenManager, rows, columns);
   screenManager->initialise(root, logon);
   log.info("HMI Screens initialised");
#endif

   hmiDetectCH.attach(this); // attach and update local
   hmiPowerGoodCH.attach(this);// attach and update local
}

void HMIManager::terminate()
{
    terminated = LU_TRUE;

    powerSavingEnabled = LU_FALSE;
    hmiDetected = LU_FALSE;
    hmiPowerGood = LU_FALSE;
    hmiPowerEnabled = LU_FALSE;

    setScreenServer(); // Stop Screen server
    setHMIPower();// Switch off HMI power

    log.info("HMI Manager has been terminated.");
}

void HMIManager::setPowerSaveMode(lu_bool_t enabled)
{
    if(terminated == LU_TRUE)
        return; //Do nothing if this manager is terminated

    LockingMutex lMutex(mutex);

    powerSavingEnabled = enabled;
    setHMIPower();
    setScreenServer();// Start/Stop screen server
}
/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void HMIManager::update(IChannel *subject)
{
    if(terminated == LU_TRUE)
        return; //Do nothing if this manager is terminated

    static lu_uint8_t errs = 0;
    IOM_ERROR chRet;
    IODataUint8 data;
    IChannel::ValueStr valueRead(data);

    log.debug("HMI channel value changed (%s)",subject->getName());

    /* Read the channel value */
    chRet = subject->read(valueRead);

    if( (chRet == IOM_ERROR_NONE) && (valueRead.flags.online == LU_TRUE) )
    {
        /*Succeeded reading value*/
        LockingMutex lMutex(mutex);

        if(subject == &hmiDetectCH)
        {
             hmiDetected = ((lu_uint8_t)data) == 0 ? LU_FALSE : LU_TRUE;

             /* HMI detected */
             if(hmiDetected == LU_TRUE)
             {
                 hmiPowerEnabled = LU_TRUE;
             }
             /* HMI NOT detected */
             else
             {
                 hmiPowerEnabled = LU_FALSE;
             }
             setHMIPower();
        }

        else if(subject == &hmiPowerGoodCH)
        {
            hmiPowerGood = ((lu_uint8_t)data) == 0 ? LU_FALSE : LU_TRUE;
            if(hmiPowerGood ==LU_TRUE)
            {
                log.info("HMI power is GOOD.");
                lu_int32_t res;
                res = canIfaceTimer.start(); //Set interface up later
                if(res < 0)
                {
                    log.error("%s System timer is unavailable", __AT__);
                    setCANIface();  //If we don't have a timer, try right now
                }
            }
            else
            {
                log.info("HMI power is NOT GOOD.");
                setCANIface();      //Set interface down immediately
            }
            setScreenServer();
        }
        errs = 0;//Clear error counters
    }
    else
    {
        /*Failed to read channel value*/
        if(errs < 10)
        {
            log.error("Error reading HMI channel:%s errs:%i %s",
                            subject->getName(),errs,__AT__);
            errs ++ ;
        }
    }
}


void HMIManager::setHMIPower()
{
    IODataUint8 data;
    IChannel::ValueStr value(data);

    lu_bool_t powerOn = ((hmiPowerEnabled == LU_TRUE) && (powerSavingEnabled == LU_FALSE));

    if(powerOn)
    {
        data = (lu_uint8_t)1; //Power on
    }
    else
    {
        data = (lu_uint8_t)0; // Power off
    }


    if(powerOn == LU_TRUE)
       log.info("Powering ON HMI");
    else
       log.info("Powering OFF HMI");

    IOM_ERROR res = hmiPowerEnableCH.write(value);
    if(res != IOM_ERROR_NONE)
    {
        log.error("Failed to set HMI power to:%i. Error %i: %s",
                    (lu_uint8_t)data, res, IOM_ERROR_ToSTRING(res));
    }
}


void HMIManager::setCANIface()
{

    if(hmiPowerGood == LU_TRUE && hmiDetected == LU_TRUE)
    {
#if POLLING_CAN_INTERFACE_STATE
        /* Bring up CAN Interface*/
        CANInterface::CANIFACE_ERROR ret;
        ret = can1.up();

        if(ret == CANInterface::CANIFACE_ERROR_NONE)
            log.info("Bring up can1");
        else
            log.error("Failed to bring up can1. Error %i",ret);
#else
#endif
        log.info("Enable can1 interface");
        can1.setEnabled(LU_TRUE);
    }

    else
    {
#if POLLING_CAN_INTERFACE_STATE
        /* Bring down CAN Interface*/
        CANInterface::CANIFACE_ERROR ret;
        ret = can1.down();

        if(ret == CANInterface::CANIFACE_ERROR_NONE)
           log.info("Bring down can1");
        else
           log.error("Failed to bring down can1 err: %i",ret);
#endif
        log.info("Disable can1 interface");
        can1.setEnabled(LU_FALSE);
    }
}

void HMIManager::setScreenServer()
{
    if(screenManager == NULL)
        return;

    /* Start screen server*/
    if( (hmiPowerGood == LU_TRUE) && (hmiDetected == LU_TRUE) )
    {
        HMI_M_ERROR ret = screenManager->startServer();
        if (ret == HMI_M_ERROR_NONE)
            log.warn("HMI Screen Server started");
        else
            log.error("Failed to start HMI Screen Server. err:%i",ret);
    }

    /* Stop screen server*/
    else
    {
        screenManager->suspend();
        screenManager->logout();
        log.warn("HMI Screen server suspended");
    }
}

void HMIManager::handleAlarmEvent(Timer &source)
{
    LU_UNUSED(source);
    log.debug("Timer goes off, setting CAN interface!");
    setCANIface();
}
/*
 *********************** End of file ******************************************
 */
