/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:LogonScreen.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   29 Jun 2017     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <string.h>
#include <openssl/evp.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "LogonScreen.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
lu_int32_t calculate_md512(lu_uint8_t *message, size_t message_len,
                lu_uint8_t **digest, lu_uint32_t *digest_len);
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static const lu_char_t* TITLE = "Enter the PIN:";
static lu_char_t* ERR_MSG = (lu_char_t*)"Invalid PIN!";
static lu_char_t* TIP_MSG = (lu_char_t*)"Press OK to finish";

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

LogonScreen::LogonScreen(ScreenManager& mediator,
                            const lu_uint8_t rows,
                            const lu_uint8_t columns
                           ) : Screen(mediator, 0, TITLE, rows, columns),
                           idxSelection(0),
                           displayMsg(TIP_MSG),
                           pinlist(NULL)
{
    resetPin();
}

HMI_M_ERROR LogonScreen::draw(lu_bool_t fullSynch)
{

    PayloadRAW bufferRAW;
    StringRAW  stringBuffer;

    if(lcdBuffer == NULL)
    {
        /* No LCD buffer. Exit immediately */
        return HMI_M_ERROR_NO_SCREEN;
    }

    /* Get LCD Buffer */
    lcdBuffer->getLCDBuffer(bufferRAW);

    /* Clear screen*/
     memset( bufferRAW.payloadPtr,
             Screen::normalPadChar,
             bufferRAW.payloadLen
           );

    /***************/
    /* Write title */
    /***************/
    /* Get first line */
    stringBuffer.payloadPtr = reinterpret_cast<lu_char_t*>(bufferRAW.payloadPtr);
    stringBuffer.payloadLen = columns + 1;  //Add null terminator

    /* Write the screen title in the centre */
    centredWrite(stringBuffer, title, LU_FALSE);

    /*****************/
    /* Write Entries */
    /*****************/
    const lu_uint16_t PAD_SIZE = (columns - PIN_LENGTH * 2)/2;

    /* Draw PIN CODE row (one space between two pin chars)*/
    if(rows > 1)
    {
        // Get line
        stringBuffer.payloadPtr =
              reinterpret_cast<lu_char_t*>(&bufferRAW.payloadPtr[1 * columns]);

        // Fill PIN code with space
        lu_uint16_t i;
        for (i = 0; i < PIN_LENGTH; i++)
        {
            stringBuffer.payloadPtr[PAD_SIZE + i*2] = inputPin[i];
            stringBuffer.payloadPtr[PAD_SIZE + i*2+1] = ' '; // space
        }
    }

    /* Draw cursor row */
    if(rows > 2)
    {
        // Get line
        stringBuffer.payloadPtr =
                reinterpret_cast<lu_char_t*>(&bufferRAW.payloadPtr[2 * columns]);

        // Set cursor
        stringBuffer.payloadPtr[PAD_SIZE + idxSelection*2] = '-';
    }

    /* Draw message row*/
    if(rows > 3)
    {
        stringBuffer.payloadPtr =
                reinterpret_cast<lu_char_t*>(&bufferRAW.payloadPtr[3 * columns]);

        if(displayMsg != NULL)
            centredWrite(stringBuffer, displayMsg, LU_FALSE);
    }


    /* Send the buffer to the mediator */
    lcdBuffer->setPosition(0, 0, rows*columns);
    mediator.writeLCD(*lcdBuffer, fullSynch);

    return HMI_M_ERROR_NONE;

}

HMI_M_ERROR LogonScreen::resetHistory()
{
    //TODO
    return HMI_M_ERROR_NONE;
}

HMI_M_ERROR LogonScreen::tickEvent()
{
    /* Nothing to do. The screen is updated only
     * when we press the up/down arrows
     * */
    return HMI_M_ERROR_NONE;

}

HMI_M_ERROR LogonScreen::customButtonEventHandler(ButtonsStatus &status)
{

    lu_bool_t forceDraw = LU_FALSE;
    buttonStatus bStatus;

    // Protect the list
    //MasterLockingMutex mLock (listMutex);

    /* Check DOWN button */
    status.getStatus(HMI_M_BUTTON_IDX_DOWN, bStatus);
    if( (bStatus.event == LU_TRUE) && (bStatus.pressed == LU_TRUE) )
    {
        /* Calculate maximum entry index */
        if(inputPin[idxSelection] > PIN_CHAR_MIN && inputPin[idxSelection] <= PIN_CHAR_MAX)
            inputPin[idxSelection] --;
        else if(inputPin[idxSelection] == PIN_CHAR_MIN)
            inputPin[idxSelection] = PIN_CHAR_MAX;
        else
            inputPin[idxSelection] = PIN_CHAR_MIN; // ?->0 when press "DOWN"

        /* Redraw screen */
        forceDraw = LU_TRUE;
    }

    /* Check UP button */
    status.getStatus(HMI_M_BUTTON_IDX_UP, bStatus);
    if( (bStatus.event == LU_TRUE) && (bStatus.pressed == LU_TRUE) )
    {
        /* Calculate maximum entry index */
        if(inputPin[idxSelection] < PIN_CHAR_MAX && inputPin[idxSelection] >= PIN_CHAR_MIN)
            inputPin[idxSelection] ++;
        else if(inputPin[idxSelection] == PIN_CHAR_MAX)
            inputPin[idxSelection] = PIN_CHAR_MIN;
        else
            inputPin[idxSelection] = PIN_CHAR_MIN + 1; // ?->1 when press "UP"

        /* Redraw screen */
        forceDraw = LU_TRUE;
    }

    /* Check RIGHT button */
    status.getStatus(HMI_M_BUTTON_IDX_RIGHT, bStatus);
    if( (bStatus.event == LU_TRUE) && (bStatus.pressed == LU_TRUE) )
    {
        /* Calculate maximum entry index */
        if(idxSelection >= PIN_LENGTH - 1)
            idxSelection = 0;
        else
            idxSelection ++;

        /* Redraw screen */
        forceDraw = LU_TRUE;
    }

    /* Check RIGHT button */
    status.getStatus(HMI_M_BUTTON_IDX_LEFT, bStatus);
    if( (bStatus.event == LU_TRUE) && (bStatus.pressed == LU_TRUE) )
    {
        /* Calculate maximum entry index */
        if(idxSelection == 0 )
            idxSelection = PIN_LENGTH - 1;
        else
            idxSelection --;

        /* Redraw screen */
        forceDraw = LU_TRUE;
    }

    /* Check OK button */
    status.getStatus(HMI_M_BUTTON_IDX_OK, bStatus);
    if( (bStatus.event == LU_TRUE) && (bStatus.pressed == LU_TRUE) )
    {
        /* Enter active menu */
        if(checkPin())
        {
            /* Pin valid*/
            displayMsg = NULL;
            mediator.homeScreen();
        }
        else
        {
            // Pin invalid
           displayMsg = ERR_MSG;
           forceDraw = LU_TRUE;
        }
    }
    else
    {
        displayMsg = TIP_MSG; // Clear error message.
    }

    /* If necessary redraw the screen */
    if(forceDraw == LU_TRUE)
    {
        return draw(LU_FALSE);
    }

    return HMI_M_ERROR_NONE;

}

void LogonScreen::setPinList(std::vector<std::string>& _pinlist)
{
    if(pinlist != NULL)
    {
        delete pinlist;
    }
    pinlist = new std::vector<std::string>(_pinlist);
}
/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
lu_bool_t LogonScreen::checkPin()
{
    if(pinlist == NULL)
        return LU_TRUE; // No pin configured

    lu_bool_t  success = LU_FALSE;
    lu_uint8_t* hashBuf;
    lu_uint32_t hashSize;
    lu_uint32_t i;


    calculate_md512(inputPin, (size_t)PIN_LENGTH, &hashBuf, &hashSize);

    if(hashSize != PIN_HASH_LENGTH_MAX/2)
        return LU_FALSE;

    // Convert byte to hex string
    char strbuf[PIN_HASH_LENGTH_MAX];
    for(i = 0; i < hashSize; i++)
    {
        sprintf(strbuf+i*2, "%02x", hashBuf[i]);
    }
    OPENSSL_free(hashBuf);
    std::string hashStr(strbuf);


    for(std::vector<std::string>::iterator it = pinlist->begin(); it != pinlist->end(); ++it)
    {
        if(hashStr.length() == it->length())
        {
            if(hashStr.compare(*it) == 0)
            {
                success = LU_TRUE;
                break;
            }
        }
    }


    return success;
}

void LogonScreen::resetPin()
{
    memset(inputPin, '?', PIN_LENGTH);
    idxSelection = 0;
    displayMsg = TIP_MSG;
}



lu_int32_t calculate_md512(lu_uint8_t *message, size_t message_len,
                lu_uint8_t **digest, lu_uint32_t *digest_len)
{
    EVP_MD_CTX *mdctx;

    if((mdctx = EVP_MD_CTX_create()) == NULL)
        return -1;

    if(1 != EVP_DigestInit_ex(mdctx, EVP_sha512(), NULL))
        return -1;

    if(1 != EVP_DigestUpdate(mdctx, message, message_len))
        return -1;

    if((*digest = (unsigned char *)OPENSSL_malloc(EVP_MD_size(EVP_sha512()))) == NULL)
        return -1;

    if(1 != EVP_DigestFinal_ex(mdctx, *digest, digest_len))
        return -1;

    EVP_MD_CTX_destroy(mdctx);
    return 0;
}
/*
 *********************** End of file ******************************************
 */
