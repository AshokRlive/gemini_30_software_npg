/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: ControlScreen.cpp 06-Dec-2012 14:38:13 pueyos_a $
 *               $HeadURL: U:\G3\RTU\MCM\src\HMIManager\src\ControlScreen.cpp $
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       ControlScreen module creates a HMI Control Screen
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 06-Dec-2012 14:38:13	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   06-Dec-2012 14:38:13  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <cstring>
#include <stdio.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "ControlScreen.h"
#include "FanTestControlLogic.h"
#include "BatteryChargerTestLogic.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/**
 * \brief Get the error string for a GDB_ERROR type error
 *
 * \param error Error to convert
 *
 * \return Error string
 */
inline const lu_char_t *getErrorString(GDB_ERROR error);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

ControlScreen::ControlScreen(ScreenManager& mediator,
                            const lu_uint32_t accessLevel,
                            const lu_char_t* title,
                            const lu_uint8_t rows,
                            const lu_uint8_t columns,
                            GeminiDatabase& database,
                            const lu_uint16_t cLogicGID
                           ) : Screen(mediator, accessLevel, title, rows, columns),
                               database(database),
                               cLogicGID(cLogicGID),
                               operating(LU_FALSE),
                               flashOn(LU_TRUE),
                               cLogicType(CONTROL_LOGIC_TYPE_INVALID),
                               commandError(GDB_ERROR_NONE)
{
    cLogicType = database.getType(cLogicGID);
}


HMI_M_ERROR ControlScreen::draw(lu_bool_t fullSynch)
{
    typedef enum    //Pseudo Clock status
    {
        HMI_PCLOCK_STATUS_UNKNOWN,
        HMI_PCLOCK_STATUS_ENABLED,
        HMI_PCLOCK_STATUS_DISABLED
    } HMI_PCLOCK_STATUS;

    HMI_PCLOCK_STATUS pClockStatus = HMI_PCLOCK_STATUS_UNKNOWN;
    PayloadRAW bufferRAW;
    StringRAW  stringBuffer;
    lu_int32_t strLen;


    if(lcdBuffer == NULL)
    {
        /* No LCD buffer. Exit immediately */
        return HMI_M_ERROR_NO_SCREEN;
    }

    /* Get specific Pseudo Clock status */
    if(cLogicType == CONTROL_LOGIC_TYPE_PCLK)
    {
        PointIdStr pClockID(cLogicGID, PCLK_POINT_CLOCK);
        PointDataUint8 value;
        if(database.getValue(pClockID, &value) == GDB_ERROR_NONE)
        {
            switch(value.getQuality())
            {
                case POINT_QUALITY_OFF_LINE:
                    pClockStatus = HMI_PCLOCK_STATUS_DISABLED;   //Action: enable
                    break;
                case POINT_QUALITY_ON_LINE:
                    pClockStatus = HMI_PCLOCK_STATUS_ENABLED;   //Action: disable
                    break;
                default:
                    break;
            }
        }
    }

    /* Get LCD Buffer */
    lcdBuffer->getLCDBuffer(bufferRAW);

    /***************/
    /* Write title */
    /***************/

    /* Get first line */
    stringBuffer.payloadPtr = reinterpret_cast<lu_char_t*>(bufferRAW.payloadPtr);
    stringBuffer.payloadLen = columns + 1;  //Add null terminator

    /* Write the screen title in the centre */
    centredWrite(stringBuffer, title, LU_FALSE);

    /*****************/
    /* Write Entries */
    /*****************/

    /* Update all the remaining rows */
    for(lu_uint8_t i = 1; i < rows; ++i)
    {
        /* Reset string length */
        strLen = -1;

        /* Get new line */
        stringBuffer.payloadPtr =
               reinterpret_cast<lu_char_t*>(&bufferRAW.payloadPtr[i * columns]);

        /* Clear the line */
        memset( stringBuffer.payloadPtr,
                Screen::normalPadChar,
                stringBuffer.payloadLen
              );

        /* When a Control Logic is added, ensure that is updated in the switch
         * statement (if is operatable) and then put the number of Control Logics
         * (straight a number, not the constant!) and the last one in the ASSERT.
         * E.g.:    CHECK_CONTROL_LOGIC(28, CONTROL_LOGIC_TYPE_BYPASS)
         */
        LU_STATIC_ASSERT(CHECK_CONTROL_LOGIC(29, CONTROL_LOGIC_TYPE_MOTOR_SUPPLY), Control_Logic_amount_mismatch);
        if(i == positionLine)
        {
            /* Display action message */
            switch(cLogicType)
            {
                case CONTROL_LOGIC_TYPE_FAN_TEST:
                case CONTROL_LOGIC_TYPE_BATTERY:
                case CONTROL_LOGIC_TYPE_FPI_TEST:
                case CONTROL_LOGIC_TYPE_FPI_RESET:
                case CONTROL_LOGIC_TYPE_POWER_SUPPLY:
                case CONTROL_LOGIC_TYPE_MOTOR_SUPPLY:
                    strLen = snprintf( stringBuffer.payloadPtr,
                                       stringBuffer.payloadLen,
                                       "ACT+CLOSE to start"
                                     );
                    break;
                case CONTROL_LOGIC_TYPE_PCLK:
                    strLen = snprintf( stringBuffer.payloadPtr,
                                       stringBuffer.payloadLen,
                                       (pClockStatus == HMI_PCLOCK_STATUS_DISABLED)? "Status: disabled" :
                                              (pClockStatus == HMI_PCLOCK_STATUS_ENABLED)? "Status: enabled" :
                                                                                           "ESC: back"
                                     );
                    break;
                default:
                    break;
            }
        }
        else if(i == operatingLine)
        {
            //add here operating state when applicable
            switch(cLogicType)
            {
                case CONTROL_LOGIC_TYPE_FAN_TEST:
                case CONTROL_LOGIC_TYPE_BATTERY:
                case CONTROL_LOGIC_TYPE_FPI_TEST:
                case CONTROL_LOGIC_TYPE_FPI_RESET:
                case CONTROL_LOGIC_TYPE_POWER_SUPPLY:
                case CONTROL_LOGIC_TYPE_MOTOR_SUPPLY:
                    strLen = snprintf( stringBuffer.payloadPtr,
                                       stringBuffer.payloadLen,
                                       "ACT+OPEN to stop"
                                     );
                    break;
                case CONTROL_LOGIC_TYPE_PCLK:

                    strLen = snprintf( stringBuffer.payloadPtr,
                                       stringBuffer.payloadLen,
                                       (pClockStatus == HMI_PCLOCK_STATUS_DISABLED)? "ACT+OPEN to enable" :
                                           (pClockStatus == HMI_PCLOCK_STATUS_ENABLED)? "ACT+OPEN to disable" :
                                                                                        "ACT+OPEN to toggle"
                                     );
                    break;
                default:
                    break;
            }
        }
        else if(i == errorLine)
        {
            if(commandError != GDB_ERROR_NONE)
            {
                /* If we had an error back when we sent a command
                 * to the control logic display it
                 */
                if(commandError != GDB_ERROR_POSITION)
                {
                    strLen = snprintf( stringBuffer.payloadPtr,
                                       stringBuffer.payloadLen,
                                       "Err: %s",
                                       getErrorString(commandError)
                                     );
                }
            }
            else
            {
                if(fullSynch == LU_TRUE)
                {
                    strLen = snprintf( stringBuffer.payloadPtr,
                                       stringBuffer.payloadLen,
                                       "Command accepted"
                                     );
                }
            }
        }
        /* Remove NULL termination if necessary */
        removeTermination(stringBuffer, strLen);
    }

    /* Send the buffer to the mediator */
    lcdBuffer->setPosition(0, 0, rows*columns);
    mediator.writeLCD(*lcdBuffer, fullSynch);

    return HMI_M_ERROR_NONE;
}


HMI_M_ERROR ControlScreen::resetHistory()
{
    /* Reset operation flag */
    operating = LU_FALSE;

    /* Reset command error */
    commandError = GDB_ERROR_NONE;

    return HMI_M_ERROR_NONE;
}


HMI_M_ERROR ControlScreen::tickEvent()
{
    draw(LU_FALSE);

    return HMI_M_ERROR_NONE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
HMI_M_ERROR ControlScreen::customButtonEventHandler(ButtonsStatus &status)
{
    buttonStatus bStatusOpen;
    buttonStatus bStatusClose;

    //get current button status
    status.getStatus(HMI_M_BUTTON_IDX_OPEN, bStatusOpen);
    status.getStatus(HMI_M_BUTTON_IDX_CLOSE, bStatusClose);

    /* CLOSE button starts operation */
    if( (bStatusClose.event == LU_TRUE) && (bStatusClose.pressed == LU_TRUE) )
    {
        /* Request start operation with default duration */
        SwitchLogicOperation swOperation;
        swOperation.operation = SWITCH_OPERATION_CLOSE;
        swOperation.local = LU_TRUE;    //Always local from HMI
        commandError = database.startOperation(cLogicGID, swOperation);
        draw((commandError == GDB_ERROR_NONE)? LU_TRUE : LU_FALSE);
    }
    /* OPEN button cancels operation */
    if( (bStatusOpen.event == LU_TRUE) && (bStatusOpen.pressed == LU_TRUE) )
    {
        /* Request cancel operation */
        SwitchLogicOperation swOperation;
        swOperation.operation = SWITCH_OPERATION_OPEN;
        swOperation.local = LU_TRUE;    //Always local from HMI
        commandError = database.startOperation(cLogicGID, swOperation);
        draw((commandError == GDB_ERROR_NONE)? LU_TRUE : LU_FALSE);
    }
    return HMI_M_ERROR_NONE;
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/* TODO: pueyos_a - unify all these HMI getErrorString into a common one */

inline const lu_char_t *getErrorString(GDB_ERROR errorCode)
{
    /* Note: This gets the SHORT string of the GDB error code, not the longer 
     * 		 message coming from GDB_ERROR_ToSTRING() */
    switch(errorCode)
    {
        case GDB_ERROR_ALREADY_ACTIVE:
            return ("Operating");
        break;

        case GDB_ERROR_AUTHORITY:
            return ("Authority");
        break;

        case GDB_ERROR_POSITION:
            return ("Position");
        break;

        case GDB_ERROR_LOCAL_REMOTE:
            return ("Local/Remote");
        break;

        case GDB_ERROR_INHIBIT:
            return ("Inhibit");
        break;

        case GDB_ERROR_NOT_SUPPORTED:
            return ("Not Supported");
        break;

        case GDB_ERROR_NOPOINT:
            return ("Module Error");
        break;

        default:
        break;
    }
    return GDB_ERROR_ToSTRING(errorCode);
}

/*
 *********************** End of file ******************************************
 */

