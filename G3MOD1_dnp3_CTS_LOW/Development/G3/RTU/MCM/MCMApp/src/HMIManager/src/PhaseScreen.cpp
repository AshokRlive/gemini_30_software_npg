/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Analogue Values Screen Implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24/02/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <cstring>
#include <stdio.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "PhaseScreen.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

PhaseScreen::PhaseScreen( ScreenManager &mediator,
                          lu_uint32_t accessLevel,
                          const lu_char_t *title,
                          lu_uint8_t rows,
                          lu_uint8_t columns,
                          GeminiDatabase &database,
                          PointIdStr phases[PHASE_LAST],
                          const lu_char_t *currentFooter,
                          const lu_char_t *voltageFooter
                         ) : Screen(mediator, accessLevel, title, rows, columns),
                             database(database)
{
    /* Store the 3 phase and neutral array locally */
    for(lu_uint32_t i = 0; i < PHASE_LAST; ++i)
    {
        this->phases[i] = phases[i];
    }

    /* Save current footer */
    if(currentFooter)
    {
        strncpy(this->currentFooter, currentFooter, sizeof(this->currentFooter));
        this->currentFooter[sizeof(this->currentFooter)-1] = '\0';
    }
    else
    {
        this->currentFooter[0] = '\0';
    }
    /* Save voltage footer */
    if(voltageFooter)
    {
        strncpy(this->voltageFooter, voltageFooter, sizeof(this->voltageFooter));
        this->voltageFooter[sizeof(this->voltageFooter)-1] = '\0';
    }
    else
    {
        this->voltageFooter[0] = '\0';
    }
    /* set the current page to page 1 */
    page = PAGE_IDX_1;
}

HMI_M_ERROR PhaseScreen::draw(lu_bool_t fullSynch)
{
    PayloadRAW bufferRAW;
    StringRAW  stringBuffer;
    PointDataFloat32 data;
    lu_char_t dataAString[32];
    lu_char_t dataVString[32];

    /* If we don't have a screen buffer exit */
    if(lcdBuffer == NULL)
    {
        return HMI_M_ERROR_NO_SCREEN;
    }

    /* Get LCD Buffer */
    lcdBuffer->getLCDBuffer(bufferRAW);

    /***************/
    /* Write title */
    /***************/

    /* Get first line */
    stringBuffer.payloadPtr = reinterpret_cast<lu_char_t*>(bufferRAW.payloadPtr);
    stringBuffer.payloadLen = columns + 1;  //Add null terminator

    /* Write title and page number */
    centredWrite(stringBuffer, title, page, PAGE_IDX_MAX);

    /**************/
    /* Write Data */
    /**************/
    for(lu_uint8_t i = 1; i < rows; ++i)
    {
        lu_int32_t strLen = -1;

        /* Get new line */
        stringBuffer.payloadPtr =
               reinterpret_cast<lu_char_t*>(&bufferRAW.payloadPtr[i * columns]);

        /* Clear the line */
        memset( stringBuffer.payloadPtr,
                Screen::normalPadChar,
                stringBuffer.payloadLen
              );

        /* Check the screen */
        if(page == PAGE_IDX_1)
        {
            lu_uint32_t phaseIdx = i-1;
            if(phaseIdx <= PHASE_C)
            {
                /* Get current */
                if(database.getValue(phases[i*2], &data) == GDB_ERROR_NONE)
                {
                    sprintValue(dataAString, sizeof(dataAString),
                                database.getValueLabel(phases[i*2], &data),
                                "%4.0f%s",
                                (lu_float32_t)(data),
                                currentFooter,
                                data.isActive());
                }
                else
                {
                    sprintValue(dataAString, sizeof(dataAString));
                }

                /* Get voltage */
                if(database.getValue(phases[(i*2)+1], &data) == GDB_ERROR_NONE)
                {
                    sprintValue(dataVString, sizeof(dataVString),
                                database.getValueLabel(phases[(i*2)+1], &data),
                                "%5.0f%s",
                                (lu_float32_t)(data),
                                voltageFooter,
                                data.isActive());
                }
                else
                {
                    sprintValue(dataVString, sizeof(dataVString));
                }

                strLen = snprintf( stringBuffer.payloadPtr, stringBuffer.payloadLen,
                                   "I%i %s  V%i %s",
                                    phaseIdx, dataAString,
                                    phaseIdx, dataVString
                                 );
            }
        }
        else if(page == PAGE_IDX_2)
        {
            if(i == 1)
            {
                if(database.getValue(phases[PHASE_I_N], &data) == GDB_ERROR_NONE)
                {
                    sprintValue(dataAString, sizeof(dataAString),
                                database.getValueLabel(phases[PHASE_I_N], &data),
                                "%4.0f%s",
                                (lu_float32_t)(data),
                                currentFooter,
                                data.isActive());
                }
                else
                {
                    sprintValue(dataAString, sizeof(dataAString));
                }

                strLen = snprintf( stringBuffer.payloadPtr, stringBuffer.payloadLen,
                                    "In %s", dataAString
                                 );
            }
        }

        /* Check if we need to remove the NULL terminator
         * added by the snprintf function
         */
        removeTermination(stringBuffer, strLen);
    }

    /* Send the buffer to the mediator */
    lcdBuffer->setPosition(0, 0, rows*columns);
    mediator.writeLCD(*lcdBuffer, fullSynch);

    return HMI_M_ERROR_NONE;
}

HMI_M_ERROR PhaseScreen::resetHistory()
{
    /* Reset page index */
    page = PAGE_IDX_1;

    return HMI_M_ERROR_NONE;
}

HMI_M_ERROR PhaseScreen::tickEvent()
{
    /* redraw the screen */
    return draw(LU_FALSE);
}




/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

HMI_M_ERROR PhaseScreen::customButtonEventHandler(ButtonsStatus &status)
{
    lu_bool_t forceDraw = LU_FALSE;
    buttonStatus bStatus;

    /* Get button UP */
    status.getStatus(HMI_M_BUTTON_IDX_UP, bStatus);
    if( (bStatus.event == LU_TRUE) && (bStatus.pressed == LU_TRUE) )
    {
        /* Get previous screen if available */
        if(page == PAGE_IDX_2)
        {
            page = PAGE_IDX_1;

            /* Redraw screen */
            forceDraw = LU_TRUE;
        }
    }

    /* Get button down */
    status.getStatus(HMI_M_BUTTON_IDX_DOWN, bStatus);
    if( (bStatus.event == LU_TRUE) && (bStatus.pressed == LU_TRUE) )
    {
        /* Get next screen if available */
        if(page == PAGE_IDX_1)
        {
            page = PAGE_IDX_2;

            /* Update screen */
            forceDraw = LU_TRUE;
        }
    }

    /* Force a redraw of the screen if necessary */
    if(forceDraw == LU_TRUE)
    {
        draw(LU_FALSE);
    }

    return HMI_M_ERROR_NONE;
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void PhaseScreen::sprintValue(  lu_char_t* dataString,
                                const lu_uint32_t datasize,
                                const lu_char_t* label,
                                const lu_char_t* format,
                                const lu_float32_t value,
                                const lu_char_t* footer,
                                const lu_bool_t valid)
{
    if( ( (label == NULL) && (format == NULL) ) || (valid == LU_FALSE) )
    {
        //Not available
        snprintf(dataString, datasize, Screen::NotAvailableString);
        return;
    }
    if(strlen(label) > 0)
    {
        //Label instead of value
        snprintf(dataString, datasize, "%s%s", label, footer);
        return;
    }
    //float value
    snprintf(dataString, datasize, format, value, footer);
}





/*
 *********************** End of file ******************************************
 */
