/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:LogonScreen.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   29 Jun 2017     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef RTU_MCM_MCMAPP_SRC_HMIMANAGER_SRC_LOGONSCREEN_H_
#define RTU_MCM_MCMAPP_SRC_HMIMANAGER_SRC_LOGONSCREEN_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Screen.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define PIN_LENGTH 4
#define PIN_HASH_LENGTH_MAX 128 // Use the length of hash of MD512

#define PIN_CHAR_MIN '0'
#define PIN_CHAR_MAX '9'

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Logon screen.
 *
 *
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */

class LogonScreen: public Screen
{
public:
    LogonScreen(ScreenManager& mediator,
                    const lu_uint8_t rows,
                    const lu_uint8_t columns
                    );
    /*Override*/
    virtual ~LogonScreen(){};

    /*Override*/
    virtual HMI_M_ERROR draw(lu_bool_t fullSynch);

    /*Override*/
    virtual HMI_M_ERROR resetHistory();

    /*Override*/
    virtual HMI_M_ERROR tickEvent();

    void eventEnter()
    {
        resetPin(); // Clear user input pin when exit.
    }

    void setPinList(std::vector<std::string>& _pinlist);

    /*Override to disable "Esc" & "Menu" */
   HMI_M_ERROR buttonEvent(ButtonsStatus &status)
   {
       return customButtonEventHandler(status);
   }

   protected:
    /*Override*/
    virtual HMI_M_ERROR customButtonEventHandler(ButtonsStatus &status);

   private:
    lu_bool_t checkPin();

    void resetPin();

   private:
    lu_uint32_t idxSelection;
    lu_char_t* displayMsg;
    std::vector<std::string>* pinlist;
    lu_uint8_t inputPin[PIN_LENGTH];
};

#endif /* RTU_MCM_MCMAPP_SRC_HMIMANAGER_SRC_LOGONSCREEN_H_ */

/*
 *********************** End of file ******************************************
 */
