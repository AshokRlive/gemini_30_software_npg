/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Switch Gear Control Screen implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   27/02/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <cstring>
#include <stdio.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "SwitchGearScreen.h"
#include "SwitchGearLogic.h"
#include "timeOperations.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/* Uses configuration labels in controls. Set to 0 for hardcoded labels */
#define USELABELINCONTROL  0

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/**
 * \brief Get the SHORT error string for a GDB_ERROR type error
 *
 * \param error Error to convert
 *
 * \return Error string
 */
inline const lu_char_t *getErrorString(GDB_ERROR error);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
const lu_char_t* PRESS_ACT = "Press ACT+OPEN/CLOSE";

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
SwitchGearScreen::SwitchGearScreen(ScreenManager &mediator,
                                    lu_uint32_t accessLevel,
                                    const lu_char_t *title,
                                    lu_uint8_t rows,
                                    lu_uint8_t columns,
                                    GeminiDatabase &database,
                                    lu_uint16_t cLogicGID,
                                    lu_uint64_t openClosePressDelay_ms
                                   ) : Screen(mediator, accessLevel, title, rows, columns),
                                       database(database),
                                       cLogicGID(cLogicGID),
                                       operating(LU_FALSE),
                                       cLogicType(CONTROL_LOGIC_TYPE_INVALID),
                                       commandError(GDB_ERROR_NONE),
                                       switchJob(NULL),
                                       actPressDelay_ms(openClosePressDelay_ms),
                                       requesting(false),
                                       m_reqStart_ms(0)
{
    cLogicType = database.getType(cLogicGID);
}


SwitchGearScreen::~SwitchGearScreen()
{
    database.cancelJob(switchJob);
}


HMI_M_ERROR SwitchGearScreen::draw(lu_bool_t fullSynch)
{
    PayloadRAW bufferRAW;
    StringRAW  stringBuffer;
    lu_int32_t strLen;

    /* No LCD buffer. Exit immediately */
    if(lcdBuffer == NULL)
    {
        return HMI_M_ERROR_NO_SCREEN;
    }

    /* Get LCD Buffer */
    lcdBuffer->getLCDBuffer(bufferRAW);

    /***************/
    /* Write title */
    /***************/

    /* Get first line */
    stringBuffer.payloadPtr = reinterpret_cast<lu_char_t*>(bufferRAW.payloadPtr);
    stringBuffer.payloadLen = columns + 1;  //Add null terminator

    /* Write the screen title in the centre */
    centredWrite(stringBuffer, title, LU_FALSE);

    /*****************/
    /* Write Entries */
    /*****************/

    /* Update all the remaining rows */
    for(lu_uint8_t i = 1; i < rows; ++i)
    {
        /* Reset string length */
        strLen = -1;

        /* Get new line */
        stringBuffer.payloadPtr =
               reinterpret_cast<lu_char_t*>(&bufferRAW.payloadPtr[i * columns]);

        /* Clear the line */
        memset( stringBuffer.payloadPtr,
                Screen::normalPadChar,
                stringBuffer.payloadLen
              );

        if(i == positionLine)
        {
            /* Display switch position */
            const lu_char_t *position;
            PointDataUint8 data;
            GDB_ERROR res;
            PointIdStr point;

            /* When a Control Logic is added, ensure that is updated in the switch
             * statement (if is operatable) and then put the number of Control Logics
             * (straight a number, not the constant!) and the last one in the ASSERT.
             * E.g.:    CHECK_CONTROL_LOGIC(28, CONTROL_LOGIC_TYPE_BYPASS)
             */
            LU_STATIC_ASSERT(CHECK_CONTROL_LOGIC(29, CONTROL_LOGIC_TYPE_MOTOR_SUPPLY), Control_Logic_amount_mismatch);

            /* Get input value */
            switch(cLogicType)
            {
                case CONTROL_LOGIC_TYPE_SGL:
                {
                    //switch has the position as an Input Point.
                    point = PointIdStr(cLogicGID, SGL_POINT_STATUS);
                    res = database.getValue(point, &data);
                }
                break;
                case CONTROL_LOGIC_TYPE_DUMMYSW:
                {
                    //Dummy switch has the position as a status CL Point.
                    point = PointIdStr(cLogicGID, DSL_POINT_STATUS);
                    res = database.getValue(point, &data);
                }
                break;
                case CONTROL_LOGIC_TYPE_DOL:
                {
                    //Digital output doesn't show position, it's the action
                    res = GDB_ERROR_NONE;
                }
                break;
                case CONTROL_LOGIC_TYPE_DIGCTRLPOINT:
                {
                    //Show output point value
                    point = PointIdStr(cLogicGID, DIGCTRL_POINT_OUTPUT);
                    res = database.getValue(point, &data);
                    switch((lu_uint32_t)(data))
                    {
                        case 0: data = ControlLogic::DBINARY_OFF; break;
                        case 1: data = ControlLogic::DBINARY_ON; break;
                        default:break;
                    }
                }
                break;
                default: //any other logic
                    res = GDB_ERROR_NOPOINT;
                break;
            }//endswitch
            std::string posName;
            if(res == GDB_ERROR_NONE)
            {
                /* Decode data into a string */

                /* FIXME: pueyos_a - DO NOT USE LABELS YET
                 *      As decided bu Julian Fryers, allowing to configure the
                 *      position labels may defeat the purpose of the Open/Close
                 *      commands, and as such the hardcoded text stays for now. */
#if USELABELINCONTROL
                posName = database.getValueLabel(point, &data);
                if(posName.length() == 0)
                {
                    //No label: print value instead
                    posName = to_string((lu_uint32_t)data);
                }
                position = posName.c_str();
#else
                /* FIXME: pueyos_a - use labels instead */
                switch((lu_uint32_t)(data))
                {
                    case ControlLogic::DBINARY_INTERMEDIATE:
                        position = "Intermediate";
                    break;

                    case ControlLogic::DBINARY_OFF:
                        /* TODO: pueyos_a - Print "Tripped" in the HMI -- labels? */
//                        if(database.getSwitchType(cLogicGID) == CB) -- need some way to know if a CLogic is a CB
                        {
                            position = "Open";
                        }
//                        else
//                        {
//                            position = "Tripped";
//                        }
                    break;

                    case ControlLogic::DBINARY_ON:
                        position = "Close";
                    break;

                    case ControlLogic::DBINARY_INDETERMINATE:
                        position = "Indeterminate";
                    break;

                    default:
                        position = "Unknown";
                    break;
                }
#endif
            }
            else
            {
                /* Error reading position */
                position = const_cast<lu_char_t*>(Screen::NotAvailableString);
                posName = Screen::NotAvailableString;
            }

            if(cLogicType != CONTROL_LOGIC_TYPE_DOL)
            {
                /* Display switch position */
                strLen = snprintf( stringBuffer.payloadPtr,
                                   stringBuffer.payloadLen,
                                   "Pos: %s",
                                   position
                                 );
            }
        }
        else if(i == operatingLine)
        {
            if(requesting)
            {
                /* Show a progress bar */
                std::string sBar;
                sBar.reserve(stringBuffer.payloadLen);
                updateProgressBar(sBar);
                strLen = snprintf( stringBuffer.payloadPtr,
                                   stringBuffer.payloadLen,
                                   sBar.c_str()
                                 );
            }
            else
            {
                /* Some Control Logics are never in operating state since it's
                 * instantaneous, so in that case we do not show anything.
                 */

                /* When a Control Logic is added, ensure that is updated in the switch
                 * statement (if is operatable) and then put the number of Control Logics
                 * (straight a number, not the constant!) and the last one in the ASSERT.
                 * E.g.:    CHECK_CONTROL_LOGIC(28, CONTROL_LOGIC_TYPE_BYPASS)
                 */
                LU_STATIC_ASSERT(CHECK_CONTROL_LOGIC(29, CONTROL_LOGIC_TYPE_MOTOR_SUPPLY), Control_Logic_amount_mismatch);

                if( (cLogicType != CONTROL_LOGIC_TYPE_DUMMYSW) &&
                    (cLogicType != CONTROL_LOGIC_TYPE_DIGCTRLPOINT)
                    )
                {
                    PointIdStr point;
                    PointDataUint8 data;

                    /* Operating status virtual point */
                    point.group = cLogicGID;

                    switch(cLogicType)
                    {
                        case CONTROL_LOGIC_TYPE_DOL:
                            point.ID = DOL_POINT_OPERATING;
                            break;
                        default:
                            point.ID = SGL_POINT_OPERATING;
                            break;
                    }

                    /* Get value */
                    if(database.getValue(point, &data) == GDB_ERROR_NONE)
                    {
                        if((lu_uint32_t)(data) == LU_TRUE)
                        {
                            /* Set the operation in progress flag */
                            operating = LU_TRUE;
                            /* Show increasing '...' to indicate that HMI is operating */
                            static lu_uint32_t flashCount = 0;
                            std::string opIndication("Operating");
                            opIndication.append(std::string(flashCount+1, '.'));
                            flashCount = (flashCount + 1) % 3;
                            strLen = snprintf( stringBuffer.payloadPtr,
                                               stringBuffer.payloadLen,
                                               opIndication.c_str()
                                             );
                        }
                        else
                        {
                            /* Not in operation */
                            if(operating == LU_TRUE)
                            {
                                /* Was operating: operation finished. Check result */
                                operating = LU_FALSE;
                                std::string opResult("Operation ");
                                std::string opResult2("finished");
                                PointIdStr pointFault;
                                PointDataUint8 fault;
                                pointFault.group = cLogicGID;
                                pointFault.ID = SGL_POINT_FAULT;
                                if(database.getValue(pointFault, &fault) == GDB_ERROR_NONE)
                                {
                                    lu_uint8_t uFault = (lu_uint8_t)(fault);
                                    if(uFault == LU_TRUE)
                                    {
                                        opResult2 = (uFault == LU_FALSE)? "successful" : "failed";
                                    }
                                }
                                opResult.append(opResult2);
                                strLen = snprintf( stringBuffer.payloadPtr,
                                                   stringBuffer.payloadLen,
                                                   opResult.c_str()
                                                 );
                            }
                            else
                            {
                                strLen = snprintf( stringBuffer.payloadPtr,
                                                   stringBuffer.payloadLen,
                                                   PRESS_ACT
                                                 );
                            }
                        }
                    }
                    else
                    {
                        /* Error reading status */
                        strLen = snprintf( stringBuffer.payloadPtr,
                                           stringBuffer.payloadLen,
                                           Screen::NotAvailableString
                                         );
                    }
                }
                else
                {
                    if(operating == LU_FALSE)
                    {
                        strLen = snprintf( stringBuffer.payloadPtr,
                                           stringBuffer.payloadLen,
                                           PRESS_ACT
                                         );
                    }
                }

            }
        }
        else if(i == errorLine)
        {
            /* When a Control Logic is added, ensure that is updated in the switch
             * statement (if is operatable) and then put the number of Control Logics
             * (straight a number, not the constant!) and the last one in the ASSERT.
             * E.g.:    CHECK_CONTROL_LOGIC(28, CONTROL_LOGIC_TYPE_BYPASS)
             */
            LU_STATIC_ASSERT(CHECK_CONTROL_LOGIC(29, CONTROL_LOGIC_TYPE_MOTOR_SUPPLY), Control_Logic_amount_mismatch);

            if(requesting)
            {
                //clear error line
                std::string cleared(columns, ' ');
                strLen = snprintf( stringBuffer.payloadPtr,
                                   stringBuffer.payloadLen,
                                   "%s", cleared.c_str()
                                 );
            }
            else if(commandError != GDB_ERROR_NONE)
            {
            	/* If we had an error back when we sent a command
            	 * to the control logic display it
            	 */
                strLen = snprintf( stringBuffer.payloadPtr,
                                   stringBuffer.payloadLen,
                                   "Err: %s",
                                   getErrorString(commandError)
                                 );
            }
            else if(cLogicType == CONTROL_LOGIC_TYPE_DUMMYSW)
            {
                //Dummy switch has 1 type of error only.
                PointIdStr pointFault(cLogicGID, DSL_POINT_FAULT);
                PointDataUint8 fault;

                if(database.getValue(pointFault, &fault) == GDB_ERROR_NONE)
                {
                    if((lu_uint32_t)(fault) == LU_TRUE)
                    {
                        /* A fault occurred during operation. */
                        strLen = snprintf( stringBuffer.payloadPtr,
                                           stringBuffer.payloadLen,
                                           "Err: Position"
                                         );
                    }
                }
            }
            else if(cLogicType == CONTROL_LOGIC_TYPE_DOL)
            {
                //Digital Output has 1 type of error only.
                PointIdStr pointFault(cLogicGID, DOL_POINT_FAULT);
                PointDataUint8 fault;

                if(database.getValue(pointFault, &fault) == GDB_ERROR_NONE)
                {
                    if((lu_uint32_t)(fault) == LU_TRUE)
                    {
                        /* A fault occurred during operation. */
                        strLen = snprintf( stringBuffer.payloadPtr,
                                           stringBuffer.payloadLen,
                                           "Error"
                                         );
                    }
                }
            }
            else if(cLogicType == CONTROL_LOGIC_TYPE_SGL)
            {
                /* TODO: pueyos_a - disabled operation detection: polling causes problems. Pending to change it to observers */
            	/* Display an error only after an operation is started */
                //if(operating == LU_TRUE)
                {
                    /* Read control logic error points */
                    PointIdStr point;
                    PointDataUint8 fault;

                    point.group = cLogicGID;
                    static const lu_uint16_t pseudoPoint[] =
                        {
                            SGL_POINT_MOTORFAULT,
                            SGL_POINT_BATTERY_FAULT,
                            SGL_POINT_RELAY_FAULT,
                            SGL_POINT_OVERCURRENT_FAULT,
                            SGL_POINT_POSITION_FAULT,
                            SGL_POINT_INHIBIT,
                            SGL_POINT_FAULT
                        };
                    static const lu_uint32_t pseudoSize = sizeof(pseudoPoint) / sizeof(lu_uint32_t);

                    for (lu_uint32_t i = 0; i < pseudoSize; ++i)
                    {
                        point.ID = pseudoPoint[i];

                        if(database.getValue(point, &fault) == GDB_ERROR_NONE)
                        {
                            if((lu_uint8_t)(fault) == LU_TRUE)
                            {
                                strLen = snprintf( stringBuffer.payloadPtr,
                                                   stringBuffer.payloadLen,
                                                   "Err: %s",
                                                   SGL_POINT_ToSTRING(point.ID)
                                                 );
                                break;
                            }
                        }
                        else
                        {
                            /* Error reading point */
                            strLen = snprintf( stringBuffer.payloadPtr,
                                               stringBuffer.payloadLen,
                                               "Err: %s",
                                               Screen::NotAvailableString
                                             );
                            break;
                        }
                    }
                }
            }
            else
            {
                //no error display
            }
        }

        /* Remove NULL termination if necessary */
        removeTermination(stringBuffer, strLen);
    }

    /* Send the buffer to the mediator */
    lcdBuffer->setPosition(0, 0, rows*columns);
    mediator.writeLCD(*lcdBuffer, fullSynch);

    return HMI_M_ERROR_NONE;
}

HMI_M_ERROR SwitchGearScreen::resetHistory()
{
    /* Reset operation flag */
    operating = LU_FALSE;

    /* Reset command error */
    commandError = GDB_ERROR_NONE;

    return HMI_M_ERROR_NONE;
}

HMI_M_ERROR SwitchGearScreen::tickEvent()
{
    draw(LU_FALSE);

    return HMI_M_ERROR_NONE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
void SwitchGearScreen::startSwitch(SWITCH_OPERATION operation)
{
    /* Unlink Timed Job (will be deleted by the Scheduler) */
    switchJob = NULL;

    /* operation parameters */
    SwitchLogicOperation swOperation;
    swOperation.operation = operation;
    swOperation.local = LU_TRUE;    //Always local from HMI

    /* start operation! */
    commandError = database.startOperation(cLogicGID, swOperation);

    /* reset the operation in progress flag */
    operating = LU_FALSE;

    /* update display */
    requesting = false;
    draw(LU_FALSE);
}


HMI_M_ERROR SwitchGearScreen::customButtonEventHandler(ButtonsStatus &status)
{
    HMI_M_ERROR ret = HMI_M_ERROR_NONE;
    buttonStatus bStatusClose;
    buttonStatus bStatusOpen;

    //get current button status
    status.getStatus(HMI_M_BUTTON_IDX_CLOSE, bStatusClose);
    status.getStatus(HMI_M_BUTTON_IDX_OPEN, bStatusOpen);

    if( ( (bStatusClose.event == LU_TRUE) && (bStatusClose.pressed == LU_TRUE) ) ||
        ( (bStatusOpen.event == LU_TRUE) && (bStatusOpen.pressed == LU_TRUE) )
        )
    {
        //an expected button has been pressed
        /* create a GDB Timed Job to start switching after configured time,
         * in order to start switching only after user has kept pressed buttons
         * for actPressDelay_ms (+ Job delay) time.
         */
        database.cancelJob(switchJob);
        struct timespec relatime;
        clock_gettimespec(&relatime);               //current relative time
        m_reqStart_ms = timespec_to_ms(&relatime);  //store request starting time
        switchJob = new StartSwitchJob(*this,
                                       actPressDelay_ms,
                                       (bStatusClose.event == LU_TRUE)? SWITCH_OPERATION_CLOSE : SWITCH_OPERATION_OPEN
                                       );
        ret = ( database.addJob(switchJob) == GDB_ERROR_NONE )? HMI_M_ERROR_NONE : HMI_M_ERROR_THREAD;

        requesting = true;
        draw(LU_FALSE);
    }
    else if( ( (bStatusClose.event == LU_TRUE) && (bStatusClose.pressed == LU_FALSE) ) ||
             ( (bStatusOpen.event == LU_TRUE) && (bStatusOpen.pressed == LU_FALSE) )
            )
    {
        //button released
        database.cancelJob(switchJob);
        operating = LU_FALSE;   //Clear last operation indication
        requesting = false;     //Cancel requesting
        draw(LU_FALSE);
    }
    return ret;
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
inline const lu_char_t *getErrorString(GDB_ERROR errorCode)
{
    switch(errorCode)
    {
        case GDB_ERROR_ALREADY_ACTIVE:
            return ("Operating");
        break;

        case GDB_ERROR_AUTHORITY:
            return ("Authority");
        break;

        case GDB_ERROR_POSITION:
            return ("Position");
        break;

        case GDB_ERROR_LOCAL_REMOTE:
            return ("Local/Remote");
        break;

        case GDB_ERROR_INHIBIT:
            return ("Inhibit");
        break;

        case GDB_ERROR_NOT_SUPPORTED:
            return ("Not Supported");
        break;

        case GDB_ERROR_NOPOINT:
            return ("Module Error");
        break;

        default:
        break;
    }
    return GDB_ERROR_ToSTRING(errorCode);
}


void SwitchGearScreen::updateProgressBar(std::string& sBar)
{
    static const std::string BarTitle("Hold "); //Bar title at the left
    static const std::string BarBorder("|");    //Bar borders
    static const lu_uint32_t BarOverhead = BarTitle.length() + (2 * BarBorder.length());
    static const lu_int32_t BarLength = columns - BarOverhead;  //Chars per progress bar
    static const lu_uint32_t TimePerBar_ms = actPressDelay_ms/BarLength;

    sBar = BarTitle;

    //Calculate amount for progress bar
    struct timespec relatime;
    clock_gettimespec(&relatime);   //relative time
    lu_int32_t bar = timespec_to_ms(&relatime) - m_reqStart_ms;
    bar = (bar / TimePerBar_ms) + 1;
    //set bar amount inside [0,BarLength]:
    lu_int32_t numBar = LU_MAX(LU_MIN(bar, BarLength), 0);

    //Compose progress bar
    sBar.append(BarBorder);
    sBar.append(LU_MAX(bar, 0), Screen::ProgressBarChar);  //Amount of progress
    if(numBar < BarLength)
    {
        /* Show animated "fan" in the front of the bar to indicate that HMI is
         * aware that buttons are pressed, even when the bar takes time to update.
         */
        static const std::string fan("|/-");
        static lu_uint32_t count = 0;
        count = (count + 1) % 3;
        sBar.append( fan.substr(count, 1) );
        numBar++;
    }
    sBar.append(LU_MAX(BarLength - numBar, 0), Screen::normalPadChar); //Empty space
    sBar.append(BarBorder);

}


/*
 *********************** End of file ******************************************
 */
