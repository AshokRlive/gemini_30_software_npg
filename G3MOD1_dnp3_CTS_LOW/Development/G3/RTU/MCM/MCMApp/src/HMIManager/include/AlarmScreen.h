/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: AlarmScreen.h 19 May 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/HMIManager/include/AlarmScreen.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 19 May 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19 May 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef ALARMSCREEN_H__INCLUDED
#define ALARMSCREEN_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <vector>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "InfoScreen.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Display RTU Alarms list
 *
 * Allows to select an entry in the list.
 * Horizontal and vertical screen scrolling is provided.
 */
class AlarmScreen: public InfoScreen
{
public:
    AlarmScreen(ScreenManager &mediator,
                lu_uint32_t accessLevel,
                const lu_char_t *title,
                lu_uint8_t rows,
                lu_uint8_t columns,
                IIOModuleManager& moduleMgr,
                IStatusManager& statusMgr,
                lu_uint32_t screenID
                );
    virtual ~AlarmScreen();

    /* ==Inherited from InfoScreen== */
    virtual HMI_M_ERROR draw(lu_bool_t fullSynch);
    virtual void eventEnter();
    virtual void eventExit();

protected:
    /* ==Inherited from InfoScreen == */
    virtual HMI_M_ERROR customButtonEventHandler(ButtonsStatus &status);
    virtual HMI_M_ERROR tickEvent();
    virtual void createContent();

private:
    /**
     * \brief Tries to acknowledge the current alarm
     */
    void ackAlarm();

    /**
     * \brief Resets the listing parameters and current indexes
     */
    void resetListing();

private:
    /**
     * Structure to identify an alarm coming from any module
     */
    struct AlarmListID
    {
        IOModuleIDStr moduleID;
        ModuleAlarm::AlarmIDStr alarmID;
    };

    typedef std::vector<AlarmListID> ListAlarmID;

private:
    static const lu_uint32_t HMI_ALARMSCREEN_REFRESH_MS = 1000; //Alarm list refresh rate
private:
    lu_uint8_t innerRows;       //Rows of the inner window
    lu_uint8_t innerColumns;    //Columns of the inner window
    lu_uint32_t idxSelection;   //Selected line
    ListAlarmID listAlarmID;    //Copy of the list of alarms by ID
    lu_bool_t showingAlarm;     //Screen is showing an alarm instead of the list
    ModuleAlarm currentAlarm;   //Copy of the currently selected alarm
};

#endif /* ALARMSCREEN_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
