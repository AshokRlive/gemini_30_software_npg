/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <cstring>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "DataEntry.h"
#include "Screen.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

DataEntry::DataEntry(const lu_char_t *header, const lu_char_t *footer) :
                                                            headerLen(0),
                                                            footerLen(0)
{
    /* Set the header length */
    if(header != NULL)
    {
        /* Save header  */
        strncpy(this->header, header, sizeof(this->header));
        this->header[sizeof(this->header)-1] = '\0';

        /* Calculate length */
        headerLen = strlen(this->header);
    }

    /* Set the footer length */
    if(footer!= NULL)
    {
        /* Save footer  */
        strncpy(this->footer, footer, sizeof(this->footer));
        this->footer[sizeof(this->footer)-1] = '\0';

        footerLen = strlen(this->footer);
    }
}

HMI_M_ERROR DataEntry::write(StringRAW &buffer)
{
    /* Check input parameters */
    if( (buffer.payloadPtr == NULL) ||
        (buffer.payloadLen == 0   )
      )
    {
        return HMI_M_ERROR_PARAM;
    }

    /* clear the input buffer */
    memset(buffer.payloadPtr, Screen::normalPadChar, buffer.payloadLen);

    /* Set local variables */
    lu_uint32_t spaceLeft = buffer.payloadLen;
    lu_uint32_t currentPosition = 0;
    lu_uint32_t copyData = 0;

    /* Write the header if available */
    if(headerLen > 0)
    {
        copyData = LU_MIN(headerLen, spaceLeft);
        if(copyData > 0)
        {
            memcpy(&buffer.payloadPtr[currentPosition], header, copyData);

            /* Update space left and current position */
            currentPosition += copyData;
            spaceLeft -= copyData;

            /* Add a space */
            if(spaceLeft > 0)
            {
                currentPosition++;
                spaceLeft--;
            }
        }
    }

    /* Write custom data */
    StringRAW customData;
    lu_char_t customDataBuffer[32];
    customData.payloadPtr = customDataBuffer;
    customData.payloadLen = sizeof(customDataBuffer);

    /* Store the data in a local buffer*/
    getDataString(customData);
    lu_uint32_t customDataLen = strlen(customData.payloadPtr);
    if(customDataLen > 0)
    {
        copyData = LU_MIN(customDataLen, spaceLeft);
        if(copyData > 0)
        {
            memcpy( &buffer.payloadPtr[currentPosition],
                    customData.payloadPtr,
                    copyData
                  );

            /* Update space left and current position */
            currentPosition += copyData;
            spaceLeft -= copyData;

            /* Add a space */
            if(spaceLeft > 0)
            {
                currentPosition++;
                spaceLeft--;
            }
        }
    }

    /* Write the footer if available  */
    if(footerLen > 0)
    {
        copyData = LU_MIN(footerLen, spaceLeft);
        if(copyData > 0)
        {
            memcpy(&buffer.payloadPtr[currentPosition], footer, copyData);

            /* Update space left and current position */
            currentPosition += copyData;
            spaceLeft -= copyData;
        }
    }

    return HMI_M_ERROR_NONE;
}



/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
