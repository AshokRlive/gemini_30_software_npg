/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ScreenContent.h 19 May 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/HMIManager/include/ScreenContent.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 19 May 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19 May 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef SCREENCONTENT_H__INCLUDED
#define SCREENCONTENT_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <string>
#include <vector>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef std::vector<std::string> ContentScreen;   //Lines of content (to use inside ScreenContent)

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Manages the content of an HMI screen independently of their size
 *
 * This class allows to fill in contents for the screen and keep these contents
 * afterwards, giving dimensions of what it is stored, and allowing different
 * printing formats of this information.
 *
 * NOTE: This object is not intended for content creation from multiple threads.
 */
class ScreenContent
{
public:
    ScreenContent() : height_m(0), width_m(0)
    {};
    virtual ~ScreenContent();

    /**
     * \brief Clears all the content in the screen
     */
    virtual void clear();

    /**
     * \brief Get a line text from the content
     *
     * \param line Line number
     *
     * \return Line content, or empty string if line number not valid
     */
    virtual std::string getLine(lu_uint32_t line);

    /**
     * \brief Get the height of the content
     *
     * \return Number of lines of text in the content
     */
    virtual size_t height();

    /**
     * \brief Get the width of the content
     *
     * \return Text length of longest line of stored content
     */
    virtual size_t width();

    /**
     * \brief Add a line to the content
     *
     * Special chars and EOL might be not allowed.
     *
     * \param message String of text to add
     */
    virtual void message(const std::string message);

    /**
     * \brief Add a line to the content
     *
     * Special chars and EOL might be not allowed.
     *
     * \param message Line of text to add, in printf format (format, params)
     */
    virtual void message(const lu_char_t* message, ...);

private:
    /**
     * \brief Recalculates dimensions of the content text
     */
    void recalculate();

private:
    ContentScreen content;  //All the lines of text are stored here
    size_t height_m;        //Max number of lines of stored content
    size_t width_m;         //Length of longest line of stored content
};

#endif /* SCREENCONTENT_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
