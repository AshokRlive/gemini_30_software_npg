/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: InfoScreen.cpp 15 May 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/HMIManager/src/InfoScreen.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       RTU Information Screens Implementation
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 15 May 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15 May 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <cstring>
#include <stdio.h>
#include <vector>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "InfoScreen.h"
#include "svnRevision.h"
#include "RTUInfo.h"
#include "TimeManager.h"
#include "IIOModule.h"
#include "StringUtil.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/**
 * Convert an array to a string in hex format with trailing "0x".
 *
 * \param s String containing the list of values to print.
 *
 * \return Resulting string.
 */
template< typename T >
std::string from_hex_string(std::string s)
{
    T value = 0;
    std::vector<T> values;
    lu_uint32_t ini = 0;
    lu_uint32_t fin = 0;
    fin = s.find(",");
    while(fin < s.length())
    {
        std::stringstream ss ;
        ss << std::hex << s.substr(ini, fin);
        ss >> value;
        values.push_back(value);
        ini = fin + 1;
        fin = s.find(",", ini);
    }
    std::string ret;
    for(typename std::vector<T>::iterator it = values.begin(); it != values.end(); it++)
    {
        lu_char_t c = static_cast<lu_char_t>(*it);
        ret += c;
    }
    return ret;
}

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

InfoScreen::InfoScreen( ScreenManager &mediator,
                        lu_uint32_t accessLevel,
                        const lu_char_t *title,
                        lu_uint8_t rows,
                        lu_uint8_t columns,
                        IIOModuleManager& moduleMgr,
                        IStatusManager& statusMgr,
                        lu_uint32_t screenID
                        ) : Screen(mediator, accessLevel, title, rows, columns),
                            moduleManager(moduleMgr),
                            statusManager(statusMgr),
                            screenType(screenID),
                            created(LU_FALSE),
                            xpos(0),
                            ypos(0)
{}


InfoScreen::~InfoScreen()
{
    content.clear();
}


HMI_M_ERROR InfoScreen::draw(lu_bool_t fullSynch)
{
    PayloadRAW bufferRAW;
    StringRAW  stringBuffer;

    /* Exit if we don't have a screen buffer */
    if(lcdBuffer == NULL)
    {
        return HMI_M_ERROR_NO_SCREEN;
    }

    /* Get LCD Buffer */
    lcdBuffer->getLCDBuffer(bufferRAW);

    createContent();

    /* Get first line */
    stringBuffer.payloadPtr = reinterpret_cast<lu_char_t*>(bufferRAW.payloadPtr);
    stringBuffer.payloadLen = columns + 1;  //Add null terminator
    lu_int32_t strLen;

    /* Write title and page number in the first row */
    //Please note conversion from amount of lines to last page index
    centredWrite(stringBuffer, title, ypos+1, LU_MAX( (content.height()-(rows-1)) + 1, 1) );

    /* Write content */
    for(lu_uint8_t i = 1; i < rows; ++i)
    {
        /* Get new line */
        stringBuffer.payloadPtr = reinterpret_cast<lu_char_t*>(&bufferRAW.payloadPtr[i * columns]);

        /* Clear the line */
        memset( stringBuffer.payloadPtr,
                Screen::normalPadChar,
                stringBuffer.payloadLen
              );

        /* Add content: get the part of the line to be displayed */
        std::string line = content.getLine(ypos + i -1);
        std::string snippet = (xpos > line.length())? "" : line.substr(xpos, columns);
        if( (xpos > 0) && (line.length() > 0) )
        {
            snippet[0] = Screen::lContent;
        }
        strLen = snprintf(stringBuffer.payloadPtr,
                          stringBuffer.payloadLen,
                          "%s",
                          snippet.c_str()
                        );

        removeTermination(stringBuffer, strLen);  //Remove NULL termination if necessary

        if( (snippet.length() == columns) && (line.length() >= (xpos+columns)) )
        {
            stringBuffer.payloadPtr[strLen-1] = Screen::rContent;
        }
    }
    /* Send the buffer to the mediator */
    lcdBuffer->setPosition(0, 0, rows*columns);
    mediator.writeLCD(*lcdBuffer, fullSynch);

    return HMI_M_ERROR_NONE;
}


HMI_M_ERROR InfoScreen::resetHistory()
{
    /* Reset page index */
    xpos = 0;
    ypos = 0;
    return HMI_M_ERROR_NONE;
}


HMI_M_ERROR InfoScreen::tickEvent()
{
    /* redraw the screen */
    return draw(LU_FALSE);
}


void InfoScreen::eventEnter()
{
    created = LU_FALSE;
}


void InfoScreen::eventExit()
{
    content.clear();
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
HMI_M_ERROR InfoScreen::customButtonEventHandler(ButtonsStatus& status)
{
    lu_bool_t forceDraw = LU_FALSE;
    buttonStatus bStatus;

    /* Get UP button */
    status.getStatus(HMI_M_BUTTON_IDX_UP, bStatus);
    if( (bStatus.event == LU_TRUE) && (bStatus.pressed == LU_TRUE) )
    {
        /* Get previous line if available */
        if(ypos > 0)
        {
            --ypos;
            forceDraw = LU_TRUE;    //Redraw screen
        }
    }

    /* Get DOWN button */
    status.getStatus(HMI_M_BUTTON_IDX_DOWN, bStatus);
    if( (bStatus.event == LU_TRUE) && (bStatus.pressed == LU_TRUE) )
    {
        /* Get next line if available */
        if( (ypos + (rows-1)) < content.height() )
        {
            ++ypos;
            forceDraw = LU_TRUE;    //Redraw screen
        }
    }

    /* Get LEFT button */
    status.getStatus(HMI_M_BUTTON_IDX_LEFT, bStatus);
    if( (bStatus.event == LU_TRUE) && (bStatus.pressed == LU_TRUE) )
    {
        /* Get previous column if available */
        if(xpos > 0)
        {
            --xpos;
            forceDraw = LU_TRUE;    //Redraw screen
        }
    }

    /* Get RIGHT button */
    status.getStatus(HMI_M_BUTTON_IDX_RIGHT, bStatus);
    if( (bStatus.event == LU_TRUE) && (bStatus.pressed == LU_TRUE) )
    {
        /* Get next line if available */
        if( ((xpos-1) + columns) < content.width() )
        {
            ++xpos;
            forceDraw = LU_TRUE;    //Redraw screen
        }
    }

    /* Get OK button */
    status.getStatus(HMI_M_BUTTON_IDX_OK, bStatus);
    if( (bStatus.event == LU_TRUE) && (bStatus.pressed == LU_TRUE) )
    {
        if( (xpos > 0) || (ypos > 0) )
        {
            xpos = 0;
            ypos = 0;
            forceDraw = LU_TRUE;    //Redraw screen
        }
    }

    /* Force a redraw of the screen if necessary */
    if(forceDraw == LU_TRUE)
    {
        draw(LU_FALSE);
    }

    return HMI_M_ERROR_NONE;
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void InfoScreen::createContent()
{
    if(created == LU_FALSE)
    {
        content.clear();
    }
    switch (screenType)
    {
        case HMI_INFO_SCREEN_TYPE_HARDWARE:
        {
            //Hardware info screen
            /* Show modules' S/N and Feat Rev */
            std::vector<IIOModule*> moduleList;
            moduleList = moduleManager.getAllModules();
            IOModuleInfoStr modInfo;
            for(std::vector<IIOModule*>::iterator it=moduleList.begin(); it!=moduleList.end(); ++it)
            {
                if((*it)->getID().type != MODULE_MCM)
                {
                    if((*it)->getInfo(modInfo) == IOM_ERROR_NONE)
                    {
                        if(modInfo.status.present == LU_TRUE)
                        {
                            content.message("%s S/N:%s Feat. %d.%d",
                                            (*it)->getName(),
                                            modInfo.moduleUID.toString().c_str(),
                                            modInfo.mInfo.featureRevision.major,
                                            modInfo.mInfo.featureRevision.minor
                                            );
                        }
                    }
                }
            }
            /* Show last the MCM itself - contains CPU S/N */
            IIOModule* mcmModule = moduleManager.getModule(MODULE_MCM, MODULE_ID_0);
            mcmModule->getInfo(modInfo);
            content.message("MCM S/N:%s Feat. %d.%d",
                            modInfo.moduleUID.toString().c_str(),
                            modInfo.mInfo.featureRevision.major,
                            modInfo.mInfo.featureRevision.minor
                            );
            content.message("MCM CPU:%016llX", getCPUSerialNum());
            break;
        }

        case HMI_INFO_SCREEN_TYPE_SOFTWARE:
        {
            //Software info screen
            content.message("SDP V. %s", trim_all(getSDPVersion()).c_str() );
            GenericVersionStr appFw;
            appFw.relType = MCM_SOFTWARE_VERSION_RELTYPE;
            appFw.version.major = MCM_SOFTWARE_VERSION_MAJOR;
            appFw.version.minor = MCM_SOFTWARE_VERSION_MINOR;
            appFw.patch = MCM_SOFTWARE_VERSION_PATCH;
            content.message("MCM :%s SVN%u", composeVersionString(appFw).c_str(), _SVN_REVISION);
            /* Show modules' FirmWare Rev numbers */
            std::vector<IIOModule*> moduleList;
            moduleList = moduleManager.getAllModules();
            IOModuleInfoStr modInfo;
            for(std::vector<IIOModule*>::iterator it=moduleList.begin(); it!=moduleList.end(); ++it)
            {
                if((*it)->getID().type != MODULE_MCM)
                {
                    if((*it)->getInfo(modInfo) == IOM_ERROR_NONE)
                    {
                        if(modInfo.status.present == LU_TRUE)
                        {
                            content.message("%s:%c-%u.%u.%u SVN%lu API:%u.%u",
                                            (*it)->getName(),
                                            getReleaseTypeSymbol(modInfo.mInfo.application.software.relType),
                                            modInfo.mInfo.application.software.version.major,
                                            modInfo.mInfo.application.software.version.minor,
                                            modInfo.mInfo.application.software.patch,
                                            modInfo.mInfo.svnRevisionApp,
                                            modInfo.mInfo.application.systemAPI.major,
                                            modInfo.mInfo.application.systemAPI.minor
                                            );
                        }
                    }
                }
            }
            content.message("---");
            content.message("OS: %s", getOSVersion().c_str());
            content.message("FS: %s", getRootFSVersion().c_str());
            content.message("Module API V. %d.%d",
                            MODULE_SYSTEM_API_VER_MAJOR,
                            MODULE_SYSTEM_API_VER_MINOR
                            );
            content.message("Schema API V. %d.%d",
                            SCHEMA_MAJOR,
                            SCHEMA_MINOR
                            );
            content.message("G3 Cfg API V. %d.%d",
                            G3_CONFIG_SYSTEM_API_MAJOR,
                            G3_CONFIG_SYSTEM_API_MINOR
                            );
            break;
        }

        case HMI_INFO_SCREEN_TYPE_CONFIG:
            //Configuration File info screen
            if(created == LU_FALSE)
            {
                /* Site name "as is": currently is a bunch of hexcodes */
                content.message("Site: %s", from_hex_string<lu_uint32_t>(statusManager.getSiteNameRTU()).c_str());

                if(statusManager.getConfigFilePresence() == LU_FALSE)
                {
                    content.message("Configuration file not present.\n");
                }
                else
                {
                    BasicVersionStr cfgFileRev;
                    statusManager.getConfigFileVersion(&cfgFileRev);
                    content.message("Revision: %d.%d",
                                    cfgFileRev.major, cfgFileRev.minor
                                    );
                    content.message("Time stamp: %s",
                                    statusManager.getConfigFileTimestamp().c_str()
                                    );
                }
                created = LU_TRUE;
            }
            break;

        case HMI_INFO_SCREEN_TYPE_NETWORK:
        {
            /* TODO: pueyos_a - [USER] IP addresses should be accessible only to authorised users */
            //Network info screen
            if(created == LU_FALSE)
            {
                netConfigVector netCfg = getNetConfig();
                if(netCfg.size() > 0)
                {
                    for (lu_uint32_t i = 0; i < netCfg.size(); ++i)
                    {
                        content.message("Device: %s", netCfg[i].devName.c_str());
                        content.message(" IP: %s", netCfg[i].toString(netCfg[i].ip).c_str());
                        content.message(" Mask: %s", netCfg[i].toString(netCfg[i].mask).c_str());
                        content.message(" GW: %s", netCfg[i].toString(netCfg[i].gateway).c_str());
                        content.message(" Bcast: %s", netCfg[i].toString(netCfg[i].broadcast).c_str());
                        content.message(" MAC: %s", netCfg[i].toString(netCfg[i].mac).c_str());
                        content.message("-----");
                    }
                }
                else
                {
                    content.message("No info available");
                }
            }
            break;
        }

        case HMI_INFO_SCREEN_TYPE_TIME:
        {
            //Time info screen
            TimeManager& timeManager = TimeManager::getInstance();
            TimeManager::TimeStr currentTime = timeManager.now();
            content.message("Date: %s", currentTime.dateString().c_str() );
            content.message("Time: %s %s", currentTime.timeString().c_str(), timeManager.getTimezoneName());
            content.message(" %synchronised",   //"ynchronised" with no "s" to deal with uppercases
                            (currentTime.synchronised == LU_FALSE)? " Not s" : " S"
                            );
            lu_uint64_t synctime_ms;
            uptimeDaysStr synctime;
            SYNC_SOURCE source;
            timeManager.getSyncStatus(&source, &synctime_ms);
            synctime.convertSeconds(synctime_ms / 1000);
            content.message("Last sync from %s",
                            SYNC_SOURCE_ToSTRING(source)
                            );
            content.message("%llu seconds ago (%s).",
                            (synctime_ms / 1000),
                            synctime.toShortString().c_str()
                            );
            /* Get System and Application Uptime */
            lu_uint64_t uptime_sec;
            uptimeDaysStr uptime;
            uptime_sec = getSystemUptime();
            uptime.convertSeconds(uptime_sec);
            content.message("OS  uptime: %llu sec (Approx. %s)",
                            uptime_sec,
                            uptime.toShortString().c_str()
                            );

            uptime_sec = timeManager.getAppRunningTime_ms() / 1000;
            uptime.convertSeconds(uptime_sec);
            content.message("App uptime: %llu sec (Approx. %s)",
                            uptime_sec,
                            uptime.toShortString().c_str()
                            );
            break;
        }

        default:
            break;
    }
}

/*
 *********************** End of file ******************************************
 */
