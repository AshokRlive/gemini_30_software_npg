/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Screen Data Entry interface public interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24/02/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_9B3E3965_225B_487d_8D2B_959BA35C4360__INCLUDED_)
#define EA_9B3E3965_225B_487d_8D2B_959BA35C4360__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "QueueObj.h"
#include "ListMgr.h"
#include "HMIManagerCommon.h"
#include "RAWBuffer.h"
#include "Screen.h"

/* Forward declaration */
class DataEntry;

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/**  DataEntry List Manager */
typedef ListMgr<DataEntry> DataEntryQueue;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief This class models a general screen data entry
 *
 * The pattern used for the entry is "header" "custom data string" "footer"
 *
 * The "custom data string" is retrieved using the virtual method "getDataString"
 */
class DataEntry : public QueueObj<DataEntry>
{

public:
    /**
     * \brief Custom Constructor
     *
     * \param header Pointer to the header string
     * \param footer Pointer to the footer string
     *
     * \return None
     */
    DataEntry(const lu_char_t *header, const lu_char_t *footer);
    virtual ~DataEntry() {};

    /**
     * \brief Write the entry into a buffer
     *
     * The whole buffer is cleared using Screen::normalPadChar
     *
     * \param  buffer Reference to the buffer where the string is written
     *
     * \return Error Code
     */
    HMI_M_ERROR write(StringRAW &buffer);

protected:
    /**
     * \brief Generate the data string
     *
     * \param buffer Reference to the buffer where the data is written
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR getDataString(StringRAW &buffer) = 0;

private:
    lu_char_t header[Screen::maxHeaderSize];
    lu_uint32_t headerLen;
    lu_char_t footer[Screen::maxFooterSize];
    lu_uint32_t footerLen;
};
#endif // !defined(EA_9B3E3965_225B_487d_8D2B_959BA35C4360__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
