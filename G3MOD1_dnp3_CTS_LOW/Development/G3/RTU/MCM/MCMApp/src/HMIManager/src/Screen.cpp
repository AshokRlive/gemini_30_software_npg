/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       HMI Screen implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   23/02/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <cstring>
#include <stdio.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "Screen.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

const lu_char_t* Screen::NotAvailableString = "?";
LCDData* Screen::lcdBuffer = NULL;

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

Screen::Screen( ScreenManager &mediator,
                lu_uint32_t accessLevel,
                const lu_char_t *title,
                lu_uint8_t rows,
                lu_uint8_t columns) :
                                    log(Logger::getLogger(SUBSYSTEM_ID_HMIM)),
                                    mediator(mediator),
                                    accessLevel(accessLevel),
                                    rows(rows),
                                    columns(columns),
                                    parent(NULL)

{
    /* If title not provided use a default one */
    if(title == NULL)
    {
        title = const_cast<lu_char_t*>(NotAvailableString);
    }

    /* Save the title */
    strncpy(this->title, title, sizeof(this->title));
    this->title[sizeof(this->title)-1] = '\0';

    /* If already allocated remove the lcdBuffer */
    if(lcdBuffer != NULL)
    {
        delete lcdBuffer;
    }

    /* Allocate a new lcdBuffer using the new screen size */
    lcdBuffer = new LCDData(rows * columns);
}

Screen::~Screen() {}

Screen *Screen::getParent()
{
    return parent;
}

HMI_M_ERROR Screen::getTitle(StringRAW &stringBuffer)
{
    if(stringBuffer.payloadPtr == NULL)
    {
        return HMI_M_ERROR_PARAM;
    }

    /* Copy the screen title in the buffer */
    strncpy(stringBuffer.payloadPtr, title, stringBuffer.payloadLen);
    stringBuffer.payloadPtr[stringBuffer.payloadLen - 1] = '\0';

    return HMI_M_ERROR_NONE;
}

HMI_M_ERROR Screen::buttonEvent(ButtonsStatus &status)
{
    HMI_M_ERROR ret = HMI_M_ERROR_NOT_SUPPORTED;
    buttonStatus bStatus;

    /* Get ESC button */
    status.getStatus(HMI_M_BUTTON_IDX_ESC, bStatus);
    if( (bStatus.event == LU_TRUE) && (bStatus.pressed == LU_TRUE) )
    {
        mediator.parentScreen(); //Go back to the parent

        ret = HMI_M_ERROR_NONE;
    }

    /* Get MENU button */
    status.getStatus(HMI_M_BUTTON_IDX_MENU, bStatus);
    if( (bStatus.event == LU_TRUE) && (bStatus.pressed == LU_TRUE) )
    {
        mediator.homeScreen();  //Go back to the home screen

        ret = HMI_M_ERROR_NONE;
    }

    /* Event handled ? */
    if(ret == HMI_M_ERROR_NOT_SUPPORTED)
    {
        /* Forward event to the custom event handler */
        ret = customButtonEventHandler(status);
    }

    return ret;
}

HMI_M_ERROR Screen::add(Screen *child)
{
    HMI_M_ERROR ret;

    /* Call the virtual add child */
    ret = addChild(child);

    /* If the child has been added correctly set the parent */
    if(ret == HMI_M_ERROR_NONE)
    {
        child->parent = this;
    }

    return ret;
}

HMI_M_ERROR Screen::remove(Screen *child)
{
    LU_UNUSED(child);

    return HMI_M_ERROR_NOT_SUPPORTED;
}

Screen *Screen::getChild(lu_uint32_t idx)
{
    LU_UNUSED(idx);

    return NULL;
}
/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

HMI_M_ERROR Screen::addChild(Screen *child)
{
    LU_UNUSED(child);

    return HMI_M_ERROR_NOT_SUPPORTED;
}

void Screen::centredWrite( StringRAW &stringBuffer,
                           lu_char_t *string,
                           lu_bool_t selected
                         )
{
    /* This is an internal function. We can assume
     * the parameters are correctly formatted
     */

    /* Get title size */
    lu_uint32_t stringSize = strlen(string);

    /* Place the title in the middle */
    lu_int32_t offset = ((lu_int32_t)(stringBuffer.payloadLen - stringSize)) / 2;

    if(offset < 0)
    {
        /* Title bigger than the line.
         * Copy only the initial part of the string
         */
        stringSize = stringBuffer.payloadLen;
        offset = 0;
    }

    /* Select the padding character */
    lu_char_t padding;
    if(selected == LU_FALSE)
    {
        padding = normalPadChar;
    }
    else
    {
        padding = selectedPadChar;
    }

    /* clear the buffer */
    memset(stringBuffer.payloadPtr, padding, stringBuffer.payloadLen);

    /* Save the string (in the middle of the buffer) */
    memcpy(&stringBuffer.payloadPtr[offset], string, stringSize);

    /* Add selection indicator */
    if(selected == LU_TRUE)
    {
        lu_int32_t offsetSelection = LU_MAX(0, offset-1);
        stringBuffer.payloadPtr[offsetSelection] = Screen::lSelection;

        offsetSelection = LU_MIN(stringBuffer.payloadLen, offset+stringSize);
        stringBuffer.payloadPtr[offsetSelection] = Screen::rSelection;
    }
}

void Screen::centredWrite( StringRAW &stringBuffer,
                           lu_char_t *string,
                           lu_uint8_t currentPage,
                           lu_uint8_t lastPage
                         )
{
    /* This is an internal function. We can assume
     * the parameters are correctly formatted
     */

    /* Write the string in the centre  */
    centredWrite(stringBuffer, string, LU_FALSE);

    /* Get a buffer to store the current/last page */
    lu_char_t currentLast[currentLastPageStringSize];

    /* Create the current/last page string */
    lu_uint32_t currentLastSize = snprintf( currentLast,
                                            sizeof(currentLast),
                                            "%i/%i",
                                            currentPage,
                                            lastPage
                                          );

    /* Copy the created string in the LCD buffer.
     * The current/last page is right aligned
     */
    memcpy( reinterpret_cast<lu_char_t*>(&stringBuffer.payloadPtr[stringBuffer.payloadLen-currentLastSize - 1]),
            currentLast,
            currentLastSize
          );
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */


