/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Analogue Data entry public interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24/02/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_1B9781C3_C903_4315_8F61_407A0D285CFB__INCLUDED_)
#define EA_1B9781C3_C903_4315_8F61_407A0D285CFB__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "DataEntry.h"
#include "GeminiDatabase.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Generate an entry string for a virtual point
 */
class VPointDataEntry : public DataEntry
{

public:
    /**
     * \brief Custom Constructor
     *
     * \param header Pointer to the header string
     * \param footer Pointer to the footer string
     * \param database Reference to the database
     * \param point ID of the point
     *
     * \return None
     */
    VPointDataEntry( const lu_char_t *header,
                     const lu_char_t *footer,
                     GeminiDatabase &database,
                     PointIdStr point
                   );
    virtual ~VPointDataEntry();

protected:
    /**
     * \brief Generate the data string
     *
     * \param buffer Reference to the buffer where the data is written
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR getDataString(StringRAW &buffer);

private:
    GeminiDatabase &database;
    PointIdStr point;

};
#endif // !defined(EA_1B9781C3_C903_4315_8F61_407A0D285CFB__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
