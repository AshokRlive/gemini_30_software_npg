/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: ControlScreen.cpp 06-Dec-2012 14:38:13 pueyos_a $
 *               $HeadURL: U:\G3\RTU\MCM\src\HMIManager\src\ControlScreen.cpp $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       ControlScreen header module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 06-Dec-2012 14:38:13	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   06-Dec-2012 14:38:13  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_30758CF5_266F_498f_A9C7_20823210A09A__INCLUDED_)
#define EA_30758CF5_266F_498f_A9C7_20823210A09A__INCLUDED_



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Screen.h"
#include "GeminiDatabase.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Generates a screen with a basic control
 *
 * The basic control of the screen requires the user to press OK to start an
 * operation.
 *
 * Screen layout:
 * LINE |   USE
 *  0       Title
 *  1       Operation text: Press OK to start operation
 *  2       Operation state: operating/non operating (if supported)
 *  3       Error code
 */
class ControlScreen : public Screen
{
public:
    /**
     * \brief Custom Constructor
     *
     * \param mediator Reference to the mediator
     * \param accessLevel Screen access level
     * \param title Pointer to the screen title
     * \param rows Screen rows
     * \param columns Screen columns
     * \param database Reference to the gemini database
     * \param cLogicGID Group ID of the control logic to use
     *
     * \return None
     */
	ControlScreen( ScreenManager& mediator,
                    const lu_uint32_t accessLevel,
                    const lu_char_t* title,
                    const lu_uint8_t rows,
                    const lu_uint8_t columns,
                    GeminiDatabase& database,
                    const lu_uint16_t cLogicGID
                   );
	virtual ~ControlScreen() {};

    /**
     * \brief Draw the screen
     *
     *  \param fullSynch Force a dull update of the LCD
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR draw(lu_bool_t fullSynch);

    /**
     * \brief Reset the screen internal history
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR resetHistory();

    /**
     * \brief Periodic tick event
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR tickEvent();

protected:
    /* TODO: pueyos_a - Add a channel observer over the battery test status? */
    /**
     * \brief Custom Button event handler
     *
     * This method is called by the buttonEvent method id the button event
     * has not been handled by the Screen handler
     *
     * \param status The buttons status
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR customButtonEventHandler(ButtonsStatus &status);

private:
    static const lu_uint8_t positionLine = 1;
    static const lu_uint8_t operatingLine = 2;
    static const lu_uint8_t errorLine = 3;

private:
    GeminiDatabase &database;
    lu_uint16_t cLogicGID;
    lu_bool_t operating;
    lu_bool_t flashOn;
    CONTROL_LOGIC_TYPE cLogicType;
    GDB_ERROR commandError;
};


#endif // !defined(EA_30758CF5_266F_498f_A9C7_20823210A09A__INCLUDED_)

/*
 *********************** End of file ******************************************
 */

