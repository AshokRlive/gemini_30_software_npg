/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       HMI Menu Screen implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   23/02/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <cstring>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MenuScreen.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

MenuScreen::MenuScreen( ScreenManager &mediator,
                        lu_uint32_t accessLevel,
                        const lu_char_t *title,
                        lu_uint8_t rows,
                        lu_uint8_t columns
                      ) : Screen (mediator, accessLevel, title, rows, columns),
                          idxFirst(-1),
                          idxLast(-1),
                          idxSelection(-1)
{}



MenuScreen::~MenuScreen()
{
    // Protect the list
    MasterLockingMutex mLock (listMutex, LU_TRUE);

    /* Delete all entries */
    Screen *screenTmp = entries.getFirst();
    while(screenTmp != NULL)
    {
        /* Remove entry from the queue */
        entries.remove(screenTmp);

        /* Delete entry */
        delete screenTmp;

        /* Get the new first entry */
        screenTmp = entries.getFirst();
    }
}

HMI_M_ERROR MenuScreen::draw(lu_bool_t fullSynch)
{
    PayloadRAW bufferRAW;
    StringRAW  stringBuffer;
    lu_char_t titleBuffer[Screen::maxTitleSize];
    StringRAW titleString;

    /* If we don't have a screen buffer exit */
    if(lcdBuffer == NULL)
    {
        return HMI_M_ERROR_NO_SCREEN;
    }

    // Protect the list
    MasterLockingMutex mLock (listMutex);

    /* check if first time */
    if(idxFirst == -1)
    {
        /* First time - initialise indexes */
        idxFirst = 0;
        idxLast = LU_MIN(((lu_uint8_t)(rows-1)), entries.getEntries());
        /* Transform the number of elements displayable in an index */
        idxLast = (idxLast > 0)? (idxLast - 1) : 0;
        idxSelection = 0;
    }

    /* Initialise title string */
    titleString.payloadPtr = titleBuffer;
    titleString.payloadLen = sizeof(titleBuffer);

    /* Get LCD Buffer */
    lcdBuffer->getLCDBuffer(bufferRAW);

    /***************/
    /* Write title */
    /***************/

    /* Get first line */
    stringBuffer.payloadPtr = reinterpret_cast<lu_char_t*>(bufferRAW.payloadPtr);
    stringBuffer.payloadLen = columns + 1;  //Add null terminator
    /* If some entries available display the
     * selected entry and the number of entries
     */
    if(entries.getEntries() == 0)
    {
        centredWrite(stringBuffer, title, LU_FALSE);
    }
    else
    {
        /* Transform the idxSelection from an index to a selection number */
        centredWrite(stringBuffer, title, idxSelection+1, entries.getEntries());
    }

    /***************/
    /* Write Menus */
    /***************/
    Screen *screenTmp;
    /* Get first screen to display */
    screenTmp = entries.getIdx(idxFirst);

    /* Scan through all the remaining rows. If screen is available write
     * the screen display in the  centre of the screen otherwise clear
     * the line
     */
    for(lu_uint8_t i = 1; i < rows; ++i)
    {
        /* Get new line */
        stringBuffer.payloadPtr =
               reinterpret_cast<lu_char_t*>(&bufferRAW.payloadPtr[i * columns]);

        /* Check the screen */
        if(screenTmp != NULL)
        {
            /* Valid child. Get the title */
            screenTmp->getTitle(titleString);

            /* Write the Title.
             * Add selection modifiers if necessary
             */
            centredWrite( stringBuffer, titleBuffer,
                          ((idxFirst + (i - 1)) == idxSelection) ? LU_TRUE : LU_FALSE
                        );

            /* Get next screen */
            screenTmp = entries.getNext(screenTmp);
        }
        else
        {
            /* No more screens. Clear the line */
            memset( stringBuffer.payloadPtr,
                    Screen::normalPadChar,
                    stringBuffer.payloadLen
                  );
        }
    }

    /* Send the buffer to the mediator */
    lcdBuffer->setPosition(0, 0, rows*columns);
    mediator.writeLCD(*lcdBuffer, fullSynch);

    return HMI_M_ERROR_NONE;
}

HMI_M_ERROR MenuScreen::resetHistory()
{
    /* Clear internal indexes */
    idxFirst = -1;
    idxLast = -1;
    idxSelection = -1;

    return HMI_M_ERROR_NONE;
}

HMI_M_ERROR MenuScreen::tickEvent()
{
    /* Nothing to do. The screen is updated only
     * when we press the up/down arrows
     * */
    return HMI_M_ERROR_NONE;
}


HMI_M_ERROR MenuScreen::remove(Screen *child)
{
    /* check input parameters */
    if(child == NULL)
    {
        return HMI_M_ERROR_PARAM;
    }

    // Protect the list
    MasterLockingMutex mLock (listMutex, LU_TRUE);

    /* Remove the child from the child list */
    if(entries.remove(child) == LU_TRUE)
    {
        return HMI_M_ERROR_NONE;
    }
    else
    {
     return HMI_M_ERROR_CLIST;
    }
}

Screen *MenuScreen::getChild(lu_uint32_t idx)
{
    // Protect the list
    MasterLockingMutex mLock (listMutex);

    return entries.getIdx(idx);
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

HMI_M_ERROR MenuScreen::addChild(Screen *child)
{
    /* check input parameters */
    if(child == NULL)
    {
        return HMI_M_ERROR_PARAM;
    }

    // Protect the list
    MasterLockingMutex mLock (listMutex, LU_TRUE);

    /* Add the child to the child list */
    if(entries.add(child) == LU_TRUE)
    {
        return HMI_M_ERROR_NONE;
    }
    else
    {
        return HMI_M_ERROR_CLIST;
    }
}

HMI_M_ERROR MenuScreen::customButtonEventHandler(ButtonsStatus &status)
{
    lu_int32_t idxMax;
    lu_bool_t forceDraw = LU_FALSE;
    buttonStatus bStatus;

    // Protect the list
    MasterLockingMutex mLock (listMutex);

    /* Check DOWN button */
    status.getStatus(HMI_M_BUTTON_IDX_DOWN, bStatus);
    if( (bStatus.event == LU_TRUE) && (bStatus.pressed == LU_TRUE) )
    {
        /* Scroll down only if we have some entries */
        if(entries.getEntries() > 0)
        {
            /* Calculate maximum entry index */
            idxMax = entries.getEntries() - 1;

            /* Select next item if there are some others entries */
            if(idxSelection < idxMax)
            {
                /* Activate next item */
                idxSelection++;

                /* Scroll down if the selected item is
                 * beyond the last displayed entry
                 */
                if(idxSelection > idxLast)
                {
                    /* Scroll down */
                    idxFirst = LU_MIN((idxFirst+1), idxMax);
                    idxLast = LU_MIN((idxLast+1), idxMax);
                }

                /* Redraw screen */
                forceDraw = LU_TRUE;
            }
        }
    }

    /* Check UP button */
    status.getStatus(HMI_M_BUTTON_IDX_UP, bStatus);
    if( (bStatus.event == LU_TRUE) && (bStatus.pressed == LU_TRUE) )
    {
        /* Scroll down only if we have some entries */
        if(entries.getEntries() > 0)
        {
            /* Activate previous entry, if available */
            if(idxSelection > 0)
            {
                /* Activate previous entry */
                idxSelection--;

                /* Scroll up if the selected item is
                 * before the first displayed entry
                 */
                if(idxSelection < idxFirst)
                {
                    /* Scroll down */
                    idxFirst--;
                    idxLast--;
                }

                /* Redraw screen */
                forceDraw = LU_TRUE;
            }
        }
    }

    /* Check OK button */
    status.getStatus(HMI_M_BUTTON_IDX_OK, bStatus);
    if( (bStatus.event == LU_TRUE) && (bStatus.pressed == LU_TRUE) )
    {
        /* Enter active menu */
        mediator.selectedScreen(idxSelection);
    }

    /* If necessary redraw the screen */
    if(forceDraw == LU_TRUE)
    {
        return draw(LU_FALSE);
    }

    return HMI_M_ERROR_NONE;
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
