/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       HMI Menu Screen public interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   23/02/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_389807AC_BF48_4978_9116_BB3ECBF802CE__INCLUDED_)
#define EA_389807AC_BF48_4978_9116_BB3ECBF802CE__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Screen.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class MenuScreen : public Screen
{

public:
    /**
     * \brief Custom Constructor
     *
     * \param mediator Reference to the mediator
     * \param accessLevel Screen access level
     * \param title Pointer to the screen title
     * \param rows Screen rows
     * \param columns Screen columns
     *
     * \return None
     */
    MenuScreen( ScreenManager &mediator,
                lu_uint32_t accessLevel,
                const lu_char_t *title,
                lu_uint8_t rows,
                lu_uint8_t columns
              );
    virtual ~MenuScreen();

    /**
     * \brief Draw the screen
     *
     *  \param fullSynch Force a dull update of the LCD
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR draw(lu_bool_t fullSynch);

    /**
     * \brief Reset the screen internal history
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR resetHistory();

    /**
     * \brief Periodic tick event
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR tickEvent();

    /**
     * \brief Remove a child from the screen
     *
     * Default implementation: operation not supported
     *
     * \param child Pointer to the child to remove
     *
     * \return Error Code
     */
    virtual HMI_M_ERROR remove(Screen *child);

    /**
     * \brief Get the child in idx position
     *
     * Default implementation: operation not supported
     *
     * \param idx Position of the child to retrieve
     *
     * \return Pointer to the child. NULL if doesn't exist
     */
    virtual Screen *getChild(lu_uint32_t idx);

protected:
    /**
         * \brief Add a child to the screen
         *
         * The parent is sent by the caller
         *
         * Default Implementation: Not Supported
         *
         * \param child Pointer to the child to add
         *
         * \return Error Code
         */
        virtual HMI_M_ERROR addChild(Screen *child);

        /**
         * \brief Custom Button event handler
         *
         * This method is called by the buttonEvent method id the button event
         * has not been handled by the Screen handler
         *
         * \param status The buttons status
         *
         * \return Error Code
         */
        virtual HMI_M_ERROR customButtonEventHandler(ButtonsStatus &status);

private:
    lu_int32_t idxFirst;
    lu_int32_t idxLast;
    lu_int32_t idxSelection;

    MasterMutex    listMutex;  // Mutex to protect the list
    ScreenList entries;

};
#endif // !defined(EA_389807AC_BF48_4978_9116_BB3ECBF802CE__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
