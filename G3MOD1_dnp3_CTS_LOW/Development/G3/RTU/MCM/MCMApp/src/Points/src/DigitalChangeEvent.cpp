/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: DigitalChangeEvent.cpp 2 Jul 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/Points/src/DigitalChangeEvent.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Digital change event reaction.
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 2 Jul 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   2 Jul 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "DigitalChangeEvent.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static TimeManager& timeManager = TimeManager::getInstance();

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
DigitalChangeEvent::DigitalChangeEvent(GeminiDatabase& g3Database,
                                       PointIdStr sourceIDCfg,
                                       DigitalChangeEvent::Config conf
                                       ) :
                                            database(g3Database),
                                            log(Logger::getLogger(SUBSYSTEM_ID_VPOINT)),
                                            initialised(false),
                                            sourceID(sourceIDCfg),
                                            config(conf),
                                            counting(false)
{
}

DigitalChangeEvent::~DigitalChangeEvent()
{
    database.detach(sourceID, this);
}


GDB_ERROR DigitalChangeEvent::init()
{
    if(initialised)
        return GDB_ERROR_INITIALIZED;

    if(sourceID.isValid() == LU_FALSE)
    {
        log.error("%s Invalid source point %i:%i to detect digital events on.",
                    __AT__, sourceID.group, sourceID.ID);
        return GDB_ERROR_NOPOINT;
    }
    GDB_ERROR ret = database.attach(sourceID, this);
    if(ret != GDB_ERROR_NONE)
    {
        log.error("%s unable to detect digital events on point %s, attaching error: %s",
                    __AT__, sourceID.toString().c_str(), GDB_ERROR_ToSTRING(ret));
    }
    else
    {
        initialised = true;
    }

    return ret;
}


GDB_ERROR DigitalChangeEvent::getValue(PointData* valuePtr)
{
    return database.getValue(sourceID, valuePtr);       //source's current value
}


GDB_ERROR DigitalChangeEvent::getRAWValue(PointData* valuePtr)
{
    return database.getRAWValue(sourceID, valuePtr);    //source's current value
}


GDB_ERROR DigitalChangeEvent::attach(IPointObserver* observer)
{
    GDB_ERROR ret = GDB_ERROR_NONE;

    if(observer == NULL)
    {
        return GDB_ERROR_PARAM;
    }

    /* Protect update using the queue mutex */
    MasterLockingMutex mMutex(queueMutex, LU_TRUE);

    /* Add observer to the queue */
    if (observerQueue.add(observer) == LU_FALSE)
    {
        ret = GDB_ERROR_ADDOBSERVER;
    }
    /* NOTE: It does not have sense to update the new observer right now,
     *      it should update when a DIGITAL CHANGE EVENT happens.
     */

    return ret;
}


GDB_ERROR DigitalChangeEvent::detach(IPointObserver* observer)
{
    GDB_ERROR ret = GDB_ERROR_NONE;

    if(observer == NULL)
    {
        return GDB_ERROR_PARAM;
    }

    /* Protect update using the queue mutex */
    MasterLockingMutex mMutex(queueMutex, LU_TRUE);

    if (observerQueue.remove(observer) == LU_FALSE)
    {
        ret = GDB_ERROR_ADDOBSERVER;
    }

    return ret;
}


void DigitalChangeEvent::stopUpdate()
{
    // Protect the observer queue - priority
    MasterLockingMutex mMutex(queueMutex, LU_TRUE);

    /* Delete all observers */
    observerQueue.removeAll();
}


POINT_QUALITY DigitalChangeEvent::getQuality()
{
    PointDataUint8 pointValue;
    if(database.getValue(sourceID, &pointValue) == GDB_ERROR_NONE)
    {
        return pointValue.getQuality();
    }
    return POINT_QUALITY_INVALID;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void DigitalChangeEvent::update(PointIdStr pointID, PointData* pointDataPtr)
{
    LU_UNUSED(pointID);

    /* Update data received from the source Virtual Point */

    bool updateIt = false;
    PointDataUint8 pointData((PointDataUint8)*pointDataPtr);
    pointData.setEdge(EDGE_TYPE_NONE);

    //Note: when an event is reported, it may be EDGE_TYPE_NONE (flag change only)
    //      or EDGE_TYPE_POSITIVE (flag and/or value change).

    {//Mutex section start
        MasterLockingMutex mMutex(dataMutex);

        if( !(pointDataPtr->isActive()) )
        {
            //Don't use value changes if not a valid change
            counting = false;   //abort any pulse-width counting
        }
        else
        {
            //Point changed: check change type and report event accordingly
            EDGE_TYPE inputEvent = pointDataPtr->getEdge();

            if(config.pulseWidth_ms > 0)
            {
                /* Pulse-driven event */
                if(inputEvent == EDGE_TYPE_POSITIVE)
                {
                    //start counting pulse width
                    timeManager.getTime(riseTime);
                    counting = true;
                }
                else if( (counting == true) && (inputEvent == EDGE_TYPE_NEGATIVE) )
                {
                    //Pulse finished: check width
                    counting = false;
                    TimeManager::TimeStr fallTime;
                    timeManager.getTime(fallTime);
                    if(riseTime.elapsed_ms(fallTime) >= config.pulseWidth_ms)
                    {
                        //valid pulse: generate event
                        pointData.setEdge(EDGE_TYPE_POSITIVE);
                        updateIt = true;

                    }
                }
            }
            else
            {
                /* Edge-driven event */
                if( ( (config.edge == EDGE_DRIVEN_MODE_RISE) ||
                      (config.edge == EDGE_DRIVEN_MODE_BOTH) ) &&
                    (inputEvent == EDGE_TYPE_POSITIVE) &&
                    (lastFlags.isInitial() == false)    //Previous value was not initial
                    )
                {
                    //Expected signal rise
                    pointData.setEdge(EDGE_TYPE_POSITIVE);
                    updateIt = true;
                }
                else if( ((config.edge == EDGE_DRIVEN_MODE_FALL) ||
                          (config.edge == EDGE_DRIVEN_MODE_BOTH) ) &&
                        (inputEvent == EDGE_TYPE_NEGATIVE) &&
                        (lastFlags.isInitial() == false)    //Previous value was not initial
                        )
                {
                    //Expected signal fall
                    pointData.setEdge(EDGE_TYPE_POSITIVE);  //The digital change is always indicated as positive
                    updateIt = true;
                }
            }
            if(updateIt)
            {
                //Value update
                pointData.setInitialFlag(false); //Not the initial value anymore
            }
        }

        if(pointData.compareFlags(lastFlags) != LU_TRUE)
        {
            //Different flags means Flags change: force update if it wasn't intended
            updateIt = true;
        }
        TimeManager::TimeStr timestamp;
        timeManager.getTime(timestamp);
        pointData.setTime(timestamp);

        lastFlags = pointData.getFlags();   //remember latest flags
    }//End of Mutex section

    if(updateIt)
    {
        updateObservers(pointData);
    }
}


void DigitalChangeEvent::updateObservers(PointData &data)
{
    IPointObserver *observerPtr;

    // Protect the observer queue
    MasterLockingMutex mMutex(queueMutex);

    /* TODO: pueyos_a - observer mutex?? Or simply copy the data? */
    // Protect the data to ensure all observers update with same data
    //LockingMutex lMutex(*mutex);

    observerPtr = observerQueue.getFirst();
    while(observerPtr != NULL)
    {
        /* Call observer */
        observerPtr->update(sourceID, &data);

        /* get next observer */
        observerPtr = observerQueue.getNext(observerPtr);
    }
}


/*
 *********************** End of file ******************************************
 */
