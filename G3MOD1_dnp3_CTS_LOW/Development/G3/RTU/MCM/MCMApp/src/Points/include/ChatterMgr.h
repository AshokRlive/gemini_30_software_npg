/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Chatter manager interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_2BC16E89_9C58_43d3_AB69_85BEF9BDF680__INCLUDED_)
#define EA_2BC16E89_9C58_43d3_AB69_85BEF9BDF680__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "QueueObj.h"
#include "ListMgr.h"
#include "TimeManager.h"

/* Forward declaration */
class ChatterEvent;

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef ListMgr<ChatterEvent> ChatterEventQueue;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */


class ChatterEvent : public QueueObj<ChatterEvent>
{
public:

    ChatterEvent(const lu_uint32_t timeOfEvent) : eventTime(timeOfEvent) {};
    virtual ~ChatterEvent() {};

    /**
     * \brief Return the event time
     *
     * \return Event time in seconds
     */
    lu_uint32_t getTime() {return eventTime;};

private:
    /**
     * Event time in seconds
     */
    lu_uint32_t eventTime;
};


/**
 * \brief check the changes of a point
 * 
 * If the number of changes in state of a point exceeds the "chatterNo" threshold
 * within the "ChatterTime" the object enters in a "chatter state". The point must
 * then not change state for the length of of "ChatterTime" in order to go back to
 * a normal state.
 *
 * NOTE: The Chatter Manager only sets/resets the chatter flag.
 *
 * The atomicity of the operations need to be guarantee by the caller.
 */
class ChatterMgr
{
public:
    ChatterMgr(lu_uint32_t chatterNo, lu_uint32_t chatterTime);
    virtual ~ChatterMgr();

    /**
     * \brief Add a new event to the chatter event manager
     *
     * If the number of events within the configured time has exceeded the configured
     * threshold the function returns true.
     *
     * \param eventTime The event relative time in seconds
     *
     * \return TRUE if the chatter threshold has been exceeded
     */
    bool newEvent(const lu_uint32_t eventTime);

    /**
     * \brief Get the chatter state
     *
     * \return TRUE if the object is in chatter state
     */
    bool inChatter();

    /**
     * \brief Update the chatter status
     *
     * No events generated
     *
     * \param time Event time stamp, in seconds
     *
     * \return TRUE if the object is in chatter state
     */
    bool update(lu_uint32_t time);

    /**
     * \brief Lock the chatter manager into chatter
     *
     * Note that this sets the status immediately into chatter, and keeps this
     * state until unlocked.
     *
     * When chatter is reported from origin, the chatter manager locks its status
     * to be according to the source. Note that, however, the chatter manager
     * still updates its internal state so when the source gets out of chatter,
     * the chatter manager still keeps record of the changes received.
     *
     * \param lockIt Set to true to lock the manager into chatter mode
     */
    void setLock(const bool lockIt);

private:
    /**
     * Number of permitted changes in state of a binary point within the "ChatterTime"
     * before the points enters in "chatter mode". The point must then not change
     * state for the length of of "ChatterTime" in order to go back to a normal state
     */
    lu_uint32_t chatterNo;
    /**
     * The time slice while the number of chatter events must not be exceeded.
     * The chatter time is in seconds.
     */
    lu_uint32_t chatterTime;

    /* Status */
    bool chattering;    //TRUE when the object is in chatter mode
    bool lockChatter;   //TRUE when chatter reported from origin

    MasterMutex queueMutex;         //Mutex to protect the queue
    ChatterEventQueue eventQueue;   //Queue containing the time stamps of
                                    // incoming events for calculating chatter
};

#endif // !defined(EA_2BC16E89_9C58_43d3_AB69_85BEF9BDF680__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
