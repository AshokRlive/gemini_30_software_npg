/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Analogue filter factory implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   20/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stddef.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "AnalogueFilterFactory.h"
#include "Logger.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

AnalogueFilterFactory AnalogueFilterFactory::instance;

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

IAnalogueFilter* AnalogueFilterFactory::newFilter(AnalogueFilterFactoryConf &config)
{
    IAnalogueFilter *filter;

    switch(config.filterType)
    {
        case AFILTER_TYPE_DEADBAND:
            filter = new DeadbandFilter(config.DBFilter);
        break;

        case AFILTER_TYPE_LOWER_UPPER_LIMIT:
            filter = new LowerUpperLimitFilter(config.LULFilter);
        break;

        case AFILTER_TYPE_HIHILOLO:
            filter = new HIHILOLOFilter(config.HHLLFilter);
        break;

        case AFILTER_TYPE_NONE:
            filter = NULL;
        break;

        default:
            filter = NULL;
            Logger::getLogger(SUBSYSTEM_ID_VPOINT).error("AnalogueFilterFactory::newFilter:"
                                                        " Unsupported filter: %i",
                                                        config.filterType
                                                      );
        break;
    }

    return filter;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */

