/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Single binary digital point implementation.
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "BinaryDigitalPoint.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define CHANNELVALUE_INVALID 0xFF

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
BinaryDigitalPoint::BinaryDigitalPoint( Mutex* gMutex,
                                        GeminiDatabase& g3database,
                                        BDConfig &conf
                                      ) : DigitalPoint(gMutex,
                                                       g3database,
                                                       POINT_TYPE_BINARY,
                                                       conf),
                                          channel(conf.channelRef),
                                          config(conf),
                                          lastChannelValue(CHANNELVALUE_INVALID),
                                          lastChannelQuality(POINT_QUALITY_INVALID)
{
    if(channel == NULL)
    {
        log.error("%s Binary Digital Point (%s) - Invalid channel configured.",
                    __AT__, pointID.toString().c_str()
                  );
    }
    m_delayList = m_conf.sortDelay(POINT_TYPE_BINARY);   //Final values [0,1]
}


BinaryDigitalPoint::~BinaryDigitalPoint()
{
    /* Detach observers */
    channel->detach(this);
    channel = NULL;
}


GDB_ERROR BinaryDigitalPoint::init()
{
    if(initialised)
    {
        return GDB_ERROR_INITIALIZED;
    }
    if(channel == NULL)
    {
        return GDB_ERROR_NOT_INITIALIZED;
    }
    GDB_ERROR ret = GDB_ERROR_NOT_INITIALIZED;

    m_data.setTime(TimeMgr.now());

    /* Register observer */
    IOM_ERROR chRet = channel->attach(this);
    if(chRet != IOM_ERROR_NONE)
    {
        log.error("%s Binary Digital Point (%s) - attach observer error %i: %s",
                    __AT__, pointID.toString().c_str(), chRet, IOM_ERROR_ToSTRING(chRet)
                  );
    }
    else
    {
        ret = DigitalPoint::init();     //Complete the init process
        if(m_preData.getFlags().isInitial())
        {
            //Force first update if attach updated before init had the chance
            update(channel);
        }
    }

    return ret;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void BinaryDigitalPoint::update(IChannel* subject)
{
    IOM_ERROR chRet;
    PointDataUint8 pointData;               //New point status to report
    IODataUint8 newValue;                   //Channel value
    IChannel::ValueStr dataRead(newValue);  //Channel data

     /* Read the channel value */
    chRet = subject->read(dataRead);
    if(chRet == IOM_ERROR_NONE)
    {
        /* Put new value into a pointdata structure */
        POINT_QUALITY newQuality;
        TimeManager::TimeStr timeStamp;
        pointData = (lu_uint8_t)(newValue);
        newQuality = (dataRead.flags.online == LU_TRUE)? POINT_QUALITY_ON_LINE :
                                                         POINT_QUALITY_OFF_LINE;
        pointData.setQuality(newQuality);
        pointData.setInitialFlag(dataRead.flags.restart);
        pointData.setInvalidFlag(dataRead.flags.restart);
        pointData.setSimulatedFlag(dataRead.flags.simulated);
        newValue.getTime(timeStamp);
        pointData.setTime(timeStamp);




            /* XXX: pueyos_a -  (to be removed) */
//            static bool forceChatter = false;
//            if(pointID == PointIdStr(0,46))
//            {
//                lu_uint8_t chatval = pointData;
//                bool newforce = chatval;
//                if(newforce != forceChatter)
//                {
//                    forceChatter = newforce;
//                    log.debug("%sSET ORIG CHATTER", (forceChatter)? "" : "UN");
//                }
//            }
//            else if(pointID == PointIdStr(0,48))
//            {
//                pointData.setChatterFlag(forceChatter);
//            }
//            else if(pointID == PointIdStr(0,53))
//            {
//                pointData.setChatterFlag(forceChatter);
//            }



    }
    else
    {
        //Error happened: report then the last value reported as offline & invalid
        pointData.setQuality(POINT_QUALITY_OFF_LINE);
        pointData.setInvalidFlag(true);
        log.error("%s: Point (%s) - channel %s read error %i: %s",
                    __AT__, pointID.toString().c_str(), subject->getName(),
                    chRet, IOM_ERROR_ToSTRING(chRet)
                  );
    }

    /* TODO: pueyos_a - Add current time to update for substracting job time from the delay */
    scheduleUpdate(*this, pointData);
}


/*
 *********************** End of file ******************************************
 */
