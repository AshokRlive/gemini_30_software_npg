/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: CounterPoint.cpp 1 Jul 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/Points/src/CounterPoint.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Counter type virtual point.
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 1 Jul 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   1 Jul 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "CounterPoint.h"
#include "Debug.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define DEBUG_COUNTER_VP 0
#if DEBUG_COUNTER_VP
    #define DBGLOG_VPC_HEADER   "VPC"
    #define DBGLOG_VPC_ERROR(text)                  DBG_ERR(DBGLOG_VPC_HEADER" %s %s", pointID.toString().c_str(), text)
    #define DBGLOG_MEM_READ(pointID, eventData)     DBG_INFO(DBGLOG_VPC_HEADER" %s Set Initial Value: %u", pointID.toString().c_str(), (CounterPointValue)eventData)
    #define DBGLOG_MEM_WRITE(pointID, eventData)    DBG_INFO(DBGLOG_VPC_HEADER" %s Set Initial Value: %u", pointID.toString().c_str(), (CounterPointValue)eventData)
#else
    #define DBGLOG_VPC_ERROR(text)
    #define DBGLOG_MEM_READ(pointID, eventData)
    #define DBGLOG_MEM_WRITE(pointID, eventData)
#endif

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static TimeManager& timeManager = TimeManager::getInstance();

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
CounterPoint::CounterPoint( Mutex* gMutex,
                            GeminiDatabase& g3database,
                            CounterPoint::Config& conf
                            ) :
                                AbstractPoint(gMutex, g3database,
                                                POINT_TYPE_COUNTER,
                                                conf.ID,
                                                conf.uniqueID,
                                                conf.persistent),
                                m_config(conf),
                                freezeTimer(NULL)
{
    m_data.setQuality(POINT_QUALITY_ON_LINE); //Counters are always online

    /* TODO: pueyos_a - for a Persistent Counter, try to read value from NVRAM instead of setting it to Preset value */
    m_rollover = (m_config.rolloverValue == 0)? Config::DATA_SIZE : m_config.rolloverValue;
    calcRollover(m_config.resetValue);
    m_frozenData = m_data;

    if(persistent)
    {
        //Read FROZEN value first
        PointDataCounter32 eventData;
        if(readPersistent(lucyMem::MemType_COUNTER_FROZEN, eventData))
        {
            m_frozenData = eventData;
        }
        //Read current counting value
        if(readPersistent(lucyMem::MemType_COUNTER, eventData))
        {
            m_data = eventData;
        }
    }
}

CounterPoint::~CounterPoint()
{
    stopUpdate();
    timeManager.detachMinute(this);
    database.cancelJob(freezeTimer);
}


GDB_ERROR CounterPoint::init()
{
    GDB_ERROR ret = GDB_ERROR_NONE;

    if(initialised)
    {
        return GDB_ERROR_INITIALIZED;
    }
    if(!persistent)
    {
        TimeManager::TimeStr startupTime = timeManager.now();   //Initial time stamp
        m_frozenData.setTime(startupTime);
        m_data.setTime(startupTime);
    }
    if(m_config.freezable == LU_TRUE)
    {
        switch(m_config.freezeTimeType)
        {
            case Config::FREEZE_TIME_OCLOCK:
                if(timeManager.attachMinute(this) != 0)
                {
                    log.error("Counter %s, Unable to attach to the minute tick",
                              pointID.toString().c_str());
                    ret = GDB_ERROR_NOT_INITIALIZED;
                }
                break;
            case Config::FREEZE_TIME_INTERVAL:
                if(m_config.freezeTime == 0)
                {
                    log.info("Counter %s: Counter will update from external sources only",
                             pointID.toString().c_str());
                }
                else
                {
                    freezeTimer = setPeriodic(m_config.freezeTime * 1000);   //seconds to ms
                    if(freezeTimer == NULL)
                    {
                        log.error("Counter %s, Unable to set periodic job timer",
                                  pointID.toString().c_str());
                        ret = GDB_ERROR_NOT_INITIALIZED;
                    }
                }
                break;
            default:
                //Unsupported configuration
                log.error("Counter %s, unsupported freezing configuration",
                          pointID.toString().c_str());
                ret = GDB_ERROR_NOT_SUPPORTED;
        }
    }
    if(ret == GDB_ERROR_NONE)
    {
        initialised = true;
    }
    return ret;
}


GDB_ERROR CounterPoint::getValue(PointData* valuePtr)
{
    return getCurrentValue(valuePtr);
}


GDB_ERROR CounterPoint::getRAWValue(PointData* valuePtr)
{
    return getCurrentValue(valuePtr);
}


GDB_ERROR CounterPoint::getFrozenValue(PointData* valuePtr)
{
    if(valuePtr == NULL)
    {
        return GDB_ERROR_PARAM;
    }
    LockingMutex lMutex(*mutex);
    PointDataCounter32* value = (PointDataCounter32*)(valuePtr);
    *value = (PointDataCounter32)m_frozenData;
    return GDB_ERROR_NONE;
}


GDB_ERROR CounterPoint::getRange(PointData* minValuePtr, PointData* maxValuePtr)
{
    if( (minValuePtr == NULL) || (maxValuePtr == NULL) )
    {
        return GDB_ERROR_PARAM;
    }

    PointDataCounter32* minValue = (PointDataCounter32*)(minValuePtr);
    PointDataCounter32* maxValue = (PointDataCounter32*)(maxValuePtr);

    *minValue = m_config.resetValue;
    *maxValue = (lu_uint32_t)(m_rollover-1);
    return GDB_ERROR_NONE;
}


std::string CounterPoint::getLabel(const PointData* valuePtr)
{
    PointDataCounter32* valPtr = (PointDataCounter32*)(valuePtr);
    lu_uint32_t value = *valPtr;
    return m_config.labels.find(value);
}


GDB_ERROR CounterPoint::freeze(const bool andClear)
{
    TimeManager::TimeStr timestamp;
    timeManager.getTime(timestamp);
    freeze(timestamp, andClear, CounterPoint::FREEZE_TYPE_COMMAND);
    return GDB_ERROR_NONE;
}


GDB_ERROR CounterPoint::clear()
{
    PointDataCounter32 eventData;  //data copy used to update observers
    TimeManager::TimeStr timeStamp;
    timeManager.getTime(timeStamp);

    LockingMutex lMutex(*mutex);
    { //Critical Section
        m_data = m_config.resetValue; //Clear (reset) current data
        m_data.setEdge(EDGE_TYPE_POSITIVE); //Changes in current value are positive-edge
        eventData = m_data;
    }
    updateObservers(&eventData);    //Update all observers
    if(persistent)
    {
        /* Store persistent value */
        storePersistent(lucyMem::MemType_COUNTER, eventData);
    }

    log.info("Counter %s commanded to clear at %s, value=%u",
             pointID.toString().c_str(),
             timeStamp.toString().c_str(),
             (lu_uint32_t)eventData);
    return GDB_ERROR_NONE;
}


GDB_ERROR CounterPoint::getCurrentValue(PointData* valuePtr)
{
    if(valuePtr == NULL)
    {
        return GDB_ERROR_PARAM;
    }
    LockingMutex lMutex(*mutex);
    PointDataCounter32* value = (PointDataCounter32*)(valuePtr);
    *value = m_data;
    return GDB_ERROR_NONE;
}


lu_bool_t CounterPoint::isFreezable()
{
    return m_config.freezable;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
GDB_ERROR CounterPoint::setFlags(const PointFlags& newFlags, const TimeManager::TimeStr& timestamp)
{
    return AbstractPoint::setFlags(m_data, newFlags, timestamp);
}


PointData* CounterPoint::getData()
{
    PointDataCounter32* ret = new PointDataCounter32; //create container, to be destroyed by the caller
    LockingMutex lMutex(*mutex);
    if(m_config.freezable == LU_TRUE)
    {
        *ret = m_frozenData;
    }
    else
    {
        *ret = m_data;
    }
    return ret;
}


void CounterPoint::tickEvent(TimeManager::TimeStr& timeStamp)
{
    /* An interval is complete */
    if(m_config.freezeTime > 0)
    {
        freeze(timeStamp, isConfigToClear());
    }
}


void CounterPoint::updateTime(TimeManager::TimeStr& currentTime)
{
    //On-the-minute update
    if( (m_config.freezeTime > 0) && (!currentTime.badTime) )
    {
        //Separate if in order to avoid calculation when not necessary
        if( (currentTime.time.tv_sec % m_config.freezeTime) == 0)
        {
            /* Minute matches with the configured freeze time */
            freeze(currentTime, isConfigToClear());
        }
    }
}


void CounterPoint::updateCounterPoint(PointData* newData, DATACHANGE dataChange)
{
    bool changed = false;       //A change in the point data
    bool storeIt = false;       //Store background value change if persistent
    PointDataCounter32 eventData;  //data copy used to update observers

    TimeManager::TimeStr timestamp;
    newData->getTime(timestamp);

    /* Lock global mutex */
    {
        LockingMutex lMutex(*mutex);

        PointDataCounter32 oldData = m_data;

        /* Check if the change affects this point */
        if( (dataChange & DATACHANGE_FREEZE) && (m_config.freezable == LU_TRUE) )
        {
            /* Freeze command */
            //Note: do not set changed=true since freeze() already reports the change
            freeze(timestamp, isConfigToClear());
            return;
        }

        /* Check flags change */
        if( (dataChange & DATACHANGE_FORCE) || (newData->compareFlags(m_lastFlags) == LU_FALSE) )
        {
            /* Input flags changed */
            if( (m_config.clearOffline == LU_TRUE) &&                 //Configured to do so
                (m_lastFlags.getQuality() != newData->getQuality()) &&     //Quality change on source
                (newData->getQuality() == POINT_QUALITY_ON_LINE)    //Back online
                )
            {
                //Back online resets counter!
                m_data = m_config.resetValue;
                storeIt = true;         //Force storage of internal count
                if(m_config.freezable == LU_FALSE)
                {
                    changed = true; //data change
                }
            }

            /* Online/InChatter/Invalidity changes between freezings:
             * ONLINE ->    OFFLINE:    DBI (Don't Believe It)
             * ONLINE ->    OFFLINE -> ONLINE:    DBI (equivalent to ONLINE -> OFFLINE)
             * OFFLINE ->   ONLINE:     normal
             * OFFLINE ->   OFFLINE:    do not count (already done at DigitalChangeEvent/ControlLogic)
             * ONLINE ->    ONLINE:     normal
             *
             * For DBI, use setInvalidFlag(), that in turn will set, for example, DNP3's discontinuity flag.
             */
            if( (newData->getQuality() == POINT_QUALITY_OFF_LINE) ||
                (newData->getChatterFlag() == true) ||
                (newData->getInvalidFlag() == true) ||
                (newData->getInitialFlag() == true)
                )
            {
                m_data.setInvalidFlag(true);
                m_frozenData.setInvalidFlag(true);
            }

            /* Check if Counter flags changed */
            if( (dataChange & DATACHANGE_FORCE) ||
                ( (m_config.freezable == LU_FALSE) && (m_data.compareFlags(&oldData) == LU_FALSE) )
                )
            {
                m_data.setTime(timestamp);
                m_data.setFlags(newData->getFlags());
                changed = true;
            }
        }
        m_lastFlags = newData->getFlags();    //store latests flags


        /* Counter data change: Change type is told by source PointData's EDGE flag
         * -When value changes directly (from Control Logic): set the counter to that value
         * -When value is only updated (from digitalChangeEvent): increase counter value
         */
        if( (dataChange & DATACHANGE_VALUE) &&                    //Value change reported
            (newData->getQuality() == POINT_QUALITY_ON_LINE) &&   //online
            (newData->getChatterFlag() == LU_FALSE) &&            //not in chatter
            (newData->getInvalidFlag() == LU_FALSE) &&            //not invalid
            (newData->getInitialFlag() == LU_FALSE)               //not initial value
            )
        {
            /* Value change */
            lu_uint64_t value;
            m_data.setTime(timestamp);
            m_data.setFlags(m_lastFlags);
            if(newData->getEdge() == EDGE_TYPE_NONE)
            {
                //Direct set of the value
                value = (lu_uint32_t)*((PointDataCounter32*)newData);
                m_data.setEdge(EDGE_TYPE_POSITIVE);   //Counters are always increasing
                value = calcRollover(value);
            }
            else
            {
                //Update: increase count (independently of the new value)
                value = (lu_uint32_t)(m_data) + 1;
                m_data.setEdge(EDGE_TYPE_POSITIVE);   //Counters are always increasing
                value = calcRollover(value);
                storeIt = true;         //Force storage of internal count
            }
            if(m_config.freezable == LU_FALSE)
            {
                /* Generate normal counter event only when non-freezable counter */
                if(m_config.eventStepCount == 0)
                {
                    changed = true;         //report value change
                    storeIt = true;         //Force storage of internal count
                }
                else
                {
                    //report when step change
                    changed = ((value % m_config.eventStepCount) == 0) ||
                               (value == m_config.resetValue);
                }
                if(m_data.getOverflowFlag() == true)
                {
                    changed = true;  //report when overflow reached
                }
            }
        }

        if(changed || storeIt)
        {
            /* Update point */
            eventData = m_data;   //value to be used for update
            eventData.setEdge(EDGE_TYPE_POSITIVE);  //Change due to non-timed event
        }
    }//End of Mutex section

    if(changed)
    {
        /* Update all observers */
        updateObservers(&eventData);
    }

    if(persistent && (changed || storeIt))
    {
        /* Store persistent value */
        storePersistent(lucyMem::MemType_COUNTER, eventData);
    }

}


lu_uint32_t CounterPoint::calcRollover(const lu_uint64_t value)
{
    lu_uint64_t calcValue;  //new value w/ rollover
    calcValue = value % m_rollover;
    m_data.setOverflowFlag(value > calcValue); //Rollover report
    if(value > calcValue)
    {
        //Rollover
        m_frozenData.setOverflowFlag(true); //Rollover report
        calcValue = m_config.resetValue;
    }
    m_data = (lu_uint32_t)calcValue;
    return (lu_uint32_t)calcValue;
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void CounterPoint::freeze(TimeManager::TimeStr& timeStamp,
                          const bool andReset,
                          const FREEZE_TYPE type
                          )
{
    /* Note: Frozen counters DO NOT use the step-counting config parameter */

    PointDataCounter32 eventData;  //data copy used to update observers

    LockingMutex lMutex(*mutex);
    { //Critical Section
        lu_uint32_t value = m_data;   //get current count
        m_frozenData = value;
        m_frozenData.setTime(timeStamp);      //Time of freeze
        m_frozenData.setEdge(EDGE_TYPE_NONE); //Frozen value
        m_frozenData.setInitialFlag(m_data.getInitialFlag());
        m_frozenData.setInvalidFlag(m_data.getInvalidFlag());
        /* Set edge type depending on the event type that generated the freezing:
         *      EDGE_TYPE_NONE     for freeze by command
         *      EDGE_TYPE_NEGATIVE for timed freeze
         *      EDGE_TYPE_POSITIVE for normal counting event
         */
        m_frozenData.setEdge((type == CounterPoint::FREEZE_TYPE_TIME)? EDGE_TYPE_NEGATIVE : EDGE_TYPE_NONE);
        eventData = m_frozenData;   //value to be used for update

        /* Update internal values after freeze */
        if(andReset)
        {
            m_data = m_config.resetValue;   //Clear (reset) current data
            m_data.setEdge(EDGE_TYPE_POSITIVE); //Changes in current value are positive-edge
            /* NOTE: freeze-and-clear does NOT require to generate 2 separate
             *      events for freeze and for clear, only one for freeze.
             */
        }
        //Reset frozen counter flags before next iteration
        m_frozenData.setOverflowFlag(false);
        m_frozenData.setInvalidFlag(false);
        m_frozenData.setChatterFlag(false);
    }
    updateObservers(&eventData);    //Update all observers for freeze action

    if(persistent)
    {
        /* Store persistent value */
        storePersistent(lucyMem::MemType_COUNTER_FROZEN, eventData);
    }

    log.info("Counter %s %s%s at %s, value=%u",
             pointID.toString().c_str(),
             (type == CounterPoint::FREEZE_TYPE_TIME)? "frozen" : "commanded to freeze",
             (!andReset)? "" : (type == CounterPoint::FREEZE_TYPE_TIME)? "and cleared" :
                                                                         "and clear",
             timeStamp.toString().c_str(),
             (lu_uint32_t)eventData
             );
}


bool CounterPoint::isConfigToClear()
{
    return (m_config.freezeType == CounterPoint::Config::FREEZE_TYPE_CLEAR);
}


bool CounterPoint::readPersistent(const lucyMem::MemType mType, PointDataCounter32& pointData)
{
    bool ret = false;
    lu_bool_t res;
    std::string sType = (mType == lucyMem::MemType_COUNTER)? "current" : "frozen";
    res = lucyMem::PersistentVPManager::getInstance().read(mType, uniqueID, pointData);
    if(res == LU_FALSE)
    {
        log.debug("%s - Previous %s value not available, using default",
                        pointID.toString().c_str(), sType.c_str()
                        );
        DBGLOG_VPC_ERROR("Previous value not available, using default");
    }
    else
    {
        ret = true;
        log.debug("%s Set Initial %s Value: %u", pointID.toString().c_str(),
                        sType.c_str(), (CounterPointValue)pointData);
        DBGLOG_MEM_READ(pointID, pointData);
    }
    return ret;
}

    bool CounterPoint::storePersistent(const lucyMem::MemType mType, PointDataCounter32& pointData)
{
    bool ret = false;
    lu_bool_t res;
    std::string sType = (mType == lucyMem::MemType_COUNTER)? "current" : "frozen";
    res = lucyMem::PersistentVPManager::getInstance().write(mType, uniqueID, pointData);
    if(res == LU_FALSE)
    {
        log.debug("%s - Failed to store persistent %s value!",
                        pointID.toString().c_str(), sType.c_str()
                        );
        DBGLOG_VPC_ERROR("Failed to store persistent value!");
    }
    else
    {
        ret = true;
        log.debug("%s Stored %s Value: %u", pointID.toString().c_str(),
                        sType.c_str(), (CounterPointValue)pointData);
        DBGLOG_MEM_READ(pointID, pointData);
    }
    return ret;
}


/*
 *********************** End of file ******************************************
 */
