/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Virtual Point interface module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   12/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_A37DCB7B_D800_40e7_9006_5347C46A9266__INCLUDED_)
#define EA_A37DCB7B_D800_40e7_9006_5347C46A9266__INCLUDED_


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "MainAppEnum.h"
#include "PointData.h"
#include "PointID.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
//Forward declarations
class IPointObserver;

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
typedef lu_uint64_t VPointUID;  //Unique Virtual Point ID
static const VPointUID VPointUID_INVALID = 0;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
class IPoint
{
public:
    IPoint() {};

    virtual ~IPoint() {};

    /**
     * \brief Initialise and start up the point activity
     *
     * \return Error code
     */
    virtual GDB_ERROR init() = 0;

    /**
     * \brief Get point ID
     *
     * \return Point ID
     */
    virtual PointIdStr getID() = 0;

    /**
     * \brief Get point type
     *
     * \return Point type
     */
    virtual POINT_TYPE getType() = 0;

    /**
     * \brief Get point online/offline quality flag
     *
     * \return Point quality flag
     */
    virtual POINT_QUALITY getQuality() = 0;

    /**
     * \brief Get point common flags
     *
     * \return Point flags
     */
    virtual PointFlags getFlags() = 0;

    /**
     * \brief Tells if the point is active
     *
     * The point is considered active if its activity flags indicate so. When the
     * flags initial, invalid, offline, and chatter(when applicable) are false,
     * it is considered as active.
     *
     * \return True when flags indicate that the point is active
     */
    virtual bool isActive() = 0;

	/**
	 * \brief Read the point value
	 * 
	 * \param valuePtr where the data is saved
	 * 
	 * \return Error code
	 */
    virtual GDB_ERROR getValue(PointData* valuePtr) = 0;

    /**
     * \brief Read the point RAW value
     *
     * The raw value depends on the point implementation, being anything from the
     * unfiltered to the uninverted value.
     *
     * \param valuePtr where the data is saved
     *
     * \return Error code
     */
    virtual GDB_ERROR getRAWValue(PointData* valuePtr) = 0;

    /**
     * \brief Read frozen value of the point
     *
     * The frozen value depends on the point implementation.
     *
     * \param valuePtr where the data is saved
     *
     * \return Error code
     */
    virtual GDB_ERROR getFrozenValue(PointData* valuePtr) = 0;

    /**
     * \brief Get the range (max and min) values of this virtual point
     *
     * Note that these values are in Engineer units and can be exceeded by the
     * Virtual Point without any warning.
     *
     * \param minValuePtr Object to be used to store the minimum range value
     * \param maxValuePtr Object to be used to store the maximum range value
     *
     * \return Error code
     */
    virtual GDB_ERROR getRange(PointData* minValuePtr, PointData* maxValuePtr) = 0;

    /**
     * \brief Read the label associated with this point value
     *
     * The point value can be mapped to a label. This obtains the content of the
     * label.
     *
     * \param valuePtr where the value is, in order to use the value contained.
     *
     * \return Label string associated with the value. Empty string if unsuccessful.
     */
    virtual std::string getLabel(const PointData* valuePtr) = 0;

    /**
     * \brief Freezes a point value
     *
     * This applies to freezable/resetable points
     *
     * \param andClear The value should reset after freezing (when applicable)
     *
     * \return Error code
     */
    virtual GDB_ERROR freeze(const bool andClear = false) = 0;

    /**
     * \brief Clear (resets) a point value
     *
     * This applies to freezable/resetable points.
     * This function resets a point value immediately to its default value, if
     * any configured -- and only if the point supports it.
     *
     * \return Error code
     */
    virtual GDB_ERROR clear() = 0;

    /**
	 * \brief Attach an observer
	 * 
	 * \param observer New observer
	 * 
	 * \return Error code
	 */
	virtual GDB_ERROR attach(IPointObserver* observer) = 0;

	/**
	 * \brief Remove an observer
	 * 
	 * \param observer Observer to remove
	 * 
	 * \return Error code
	 */
	virtual GDB_ERROR detach(IPointObserver* observer) = 0;

    /**
     * \brief Prevent update of observers
     *
     * Prevent point update and detach observers
     */
	virtual void stopUpdate() = 0;

	//TODO: Future expansions:
	//virtual void simulate(const PointData* valuePtr) = 0;
	//virtual void deSimulate() = 0;
};

#endif // !defined(EA_A37DCB7B_D800_40e7_9006_5347C46A9266__INCLUDED_)

/*
 *********************** End of file ******************************************
 */


