/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Point observe abstract interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13/07/11      galli_m     Initial version.
 *   25/05/17      pueyos_a    Added InputPointObserver mechanism to facilitate
 *                              addition of Point observers with extended
 *                              functionality.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_C7B9E57F_8BCB_4d4a_98E3_FF99F23F7127__INCLUDED_)
#define EA_C7B9E57F_8BCB_4d4a_98E3_FF99F23F7127__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "QueueObj.h"
#include "Table.h"
#include "ListMgr.h"
#include "IPoint.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
typedef ListMgr<IPointObserver> PointObserverQueue;
typedef Table<IPointObserver*> PointObserverList;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

//Forward declaration
template <typename ValType, class ReceiverType> class InputPointObserver;

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Interface for point observers
 */
class IPointObserver : public QueueObj<IPointObserver>
{
public:
    IPointObserver() {};
    virtual ~IPointObserver() {};

	/**
	 * \brief Custom observer function
	 * 
	 * \param pointID Point ID
	 * \param pointDataPtr Point data
	 * 
	 * \return none
	 */
    virtual void update(PointIdStr pointID, PointData *pointDataPtr) = 0;

    /**
     * \brief Get the ID of the subscribed point
     *
     * \return Point ID
     */
    virtual PointIdStr getPointID() = 0;
};


/**
 * \brief Base class for any InputPointObserver receiver
 *
 * Any class that requires one or many InputPointObserver(s) should implement
 * the methods of this class in order to be notified from them.
 *
 * \example: Here is a typical receiver that is creating and attaching a digital
 *          point observer DInputPointObserver inside its own constructor (but
 *          could be elsewhere) and gets the update notifications at newData():
 *              class MyCtrlLogic : protected InputPointObserverReceiver
 *              {
 *              public:
 *                  MyCtrlLogic(GeminiDatabase& db, Config cfg)
 *                  {
 *                      inObserver = new DInputPointObserver(cfg.pointID, *this);
 *                      db.attach(cfg.pointID, inObserver);
 *                  };
 *              protected:
 *                  virtual void newData(PointIdStr pointID, PointData& pointData)
 *                  {
 *                      if(pointID == cfg.pointID)
 *                      {
 *                          DigitalPointValue v = pointData;
 *                          ... do something with v ...
 *                      }
 *                  };
 *              private:
 *                  DInputPointObserver* inObserver;
 *              };
 */
class InputPointObserverReceiver
{
protected:
    template <typename ValType, class ReceiverType> friend class InputPointObserver;
protected:
    /**
      * \brief Notification of a change in an input
      *
      * This method is intended to be called when the observer notifies a change
      * in its input.
      *
      * \param pointID Point ID
      * \param pointData New data of the point (reference for faster link)
      */
    virtual void newData(PointIdStr pointID, PointData& pointData) = 0;
};


/**
 * \brief Base class for the Basic Virtual Point Input observer
 *
 * This class provides a base the Basic Virtual Point Input observer. This class
 * allows to create arrays and vectors of Basic Input Point Observers without
 * needing to specify its type beforehand.
 * Note that you can't use it for non-pointer arrays since this class is abstract,
 * i.e. pure virtual.
 *
 * \example: In order to create an array or a vector of observers mixing types:
 *              InputPointObserverBase* arrayObs[3];
 *              std::vector<InputPointObserverBase*> vectorObs;
 *          Then initialise them:
 *              arrayObs[1] = new AInputPointObserver(config.pointID, *this);
 *              vectorObs.push_back(new DInputPointObserver(config.pointID, *this));
 *          And use them:
 *              if(g3DB.getPointType(arrayObs[1]->getPointID() == POINT_TYPE_COUNTER)
 *              {
 *                  CInputPointObserver* cp = static_cast<CInputPointObserver*>(arrayObs[1]);
                    CounterPointValue v = cp->getValue();
 *              }
 */
class InputPointObserverBase : public IPointObserver
{
public:
    InputPointObserverBase(PointIdStr pointID) : m_pointID(pointID) {};
    virtual ~InputPointObserverBase() {};
    /* == Inherited from IPointObserver == */
    inline PointIdStr getPointID() { return m_pointID; }
protected:
    PointIdStr m_pointID;   //Point ID
};


/**
 * \brief Basic Virtual Point Input observer implementation
 *
 * This class provides a basic common functionality for a Virtual Point Input
 * observer. The template provides a way to define it for different types of
 * input data, and to appoint any class as the observer receiver to report to.
 *
 * \NOTE: Use DInputPointObserver, AInputPointObserver, or CInputPointObserver
 *      instead this one whenever possible.
 *
 */
template <typename ValType, class ReceiverType>
class InputPointObserver : public InputPointObserverBase
{
public:
    /**
     * \brief Custom constructor
     *
     * \param pointID Virtual Point ID
     * \param receiver Object to call, that had inherited from InputPointObserverReceiver
     */
    InputPointObserver(PointIdStr pointID, ReceiverType& receiver) :
                                                    InputPointObserverBase(pointID),
                                                    m_receiver(receiver),
                                                    m_value((ValType)0)
    {};
    virtual ~InputPointObserver() {};
    /**
     * \brief Get point attributes
     *
     * Gets the point value/quality/flags/status
     * For performance reasons the local copy is returned
     *
     * \return Attribute from Observed Virtual Point
     */
    inline ValType getValue() {return m_value;}
    inline PointFlags getFlags() { return m_flags; };
    inline POINT_QUALITY getQuality() { return m_flags.getQuality(); };
    inline bool isActive() { return m_flags.isActive(); };

    /* == Inherited from IPointObserver == */
    virtual void update(PointIdStr pointID, PointData* pointDataPtr)
    {
        //Default behaviour
        m_value = *pointDataPtr;              //Store the new value locally
        m_flags = pointDataPtr->getFlags();   //Store the new flags locally
        m_receiver.newData(pointID, *pointDataPtr);    /* Notify receiver that an input has changed */
    }

protected:
    ReceiverType& m_receiver;   //Control Logic to be reported when point changes
    ValType m_value;            //Local copy stored for performance reasons
    PointFlags m_flags;         //Local copy stored for performance reasons
};


/* === Specific instances for common data types === */
typedef InputPointObserver<DigitalPointValue,  InputPointObserverReceiver> DInputPointObserver;
typedef InputPointObserver<AnaloguePointValue, InputPointObserverReceiver> AInputPointObserver;
typedef InputPointObserver<CounterPointValue,  InputPointObserverReceiver> CInputPointObserver;


#endif // !defined(EA_C7B9E57F_8BCB_4d4a_98E3_FF99F23F7127__INCLUDED_)

/*
 *********************** End of file ******************************************
 */


