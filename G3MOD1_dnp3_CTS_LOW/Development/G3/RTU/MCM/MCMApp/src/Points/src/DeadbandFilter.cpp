/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Analogue deadband filter implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   20/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "DeadbandFilter.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
DeadbandFilter::DeadbandFilter(DeadbandFilter::Config& conf) :
                                                    m_initial(false),
                                                    m_deadband(conf.deadband)
{
    if(conf.useNominal == LU_TRUE)
    {
        //Nominal value provided by the user
        m_initial = true;
        m_nominal = conf.nominal;
    }
}

AFILTER_RESULT DeadbandFilter::run(lu_float32_t value)
{
    if(!m_initial)
    {
        //Initial value not provided, use this one
        m_nominal = value;
        status = AFILTER_RESULT_OUT_LIMIT_THR;  //Report this
        m_initial = true;   //value already provided
    }
    else
    {
        if ( (value < (m_nominal - m_deadband)) ||
             (value > (m_nominal + m_deadband))
           )
        {
            /* Deadband threshold exceeded. Update nominal value */
            m_nominal = value;
            status = AFILTER_RESULT_OUT_LIMIT_THR;
        }
        else
        {
            /* Deadband threshold not exceeded */
            status = AFILTER_RESULT_IN_LIMIT;
        }
    }

    return status;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */


