/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: CounterPoint.h 1 Jul 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/Points/include/CounterPoint.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Counter type virtual point.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 1 Jul 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   1 Jul 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef COUNTERPOINT_H__INCLUDED
#define COUNTERPOINT_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "AbstractPoint.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Virtual Counter point common container.
 *
 * This is a base class for all common data and logic for all counter points.
 * All the pure virtual methods must be implemented in the children as this is
 * only an abstract container.
 *
 * NOTE: Counter Pointer always reports events, encoding in the EDGE flag the
 *      reasons for the change:
 *          Edge            Meaning
 *          ------          ---------
 *          EDGE_POSITIVE   a change in the current count value (step, clear)
 *          EDGE_NONE       a change in frozen value from a command (freeze)
 *          EDGE_NEGATIVE   a change in frozen value due to timed event (auto freeze)
 */
class CounterPoint: public AbstractPoint, private TimeObserver
{
public:
    struct Config : public AbstractPoint::BaseConfig<CounterPointValue>
    {
    public:
        static const lu_uint32_t DATA_SIZE = 0xFFFFFFFFL; //32-bit unsigned counter (limit)
        static const lu_uint32_t MAX_PERSISTENT = 16;     //Max amount of persistent Counters

        enum FREEZE_TYPE
        {
            FREEZE_TYPE_ONLY,   //freeze-only
            FREEZE_TYPE_CLEAR   //freeze-and-clear
        };
        enum FREEZE_TIME
        {
            FREEZE_TIME_OCLOCK,     //Time specification is seconds from o'clock
            FREEZE_TIME_INTERVAL    //Time specification is an interval, every n seconds
        };
    public:
        PointIdStr ID;          //Counter Point ID

        lu_bool_t clearOffline;     //Clear when source lost: clears value when back online

        lu_uint32_t resetValue;     //Offset value (initial and after-rollover value)
        lu_uint32_t rolloverValue;  //Max value of the counter before rolling over
        lu_uint32_t eventStepCount; //Step increment that generates an event (Unused by Frozen counters)

        lu_bool_t freezable;        //Freezable object
        FREEZE_TYPE freezeType;     //freeze-only OR freeze-and-clear
        FREEZE_TIME freezeTimeType; //Type of time to count
        lu_uint32_t freezeTime;     //seconds

    public:
        Config() :  clearOffline(LU_FALSE),
                    resetValue(0),
                    rolloverValue(DATA_SIZE),
                    eventStepCount(0),
                    freezable(LU_FALSE),
                    freezeType(FREEZE_TYPE_ONLY),
                    freezeTimeType(FREEZE_TIME_INTERVAL),
                    freezeTime(0)
        {};
    };

public:
    /**
     * \brief Custom constructor
     *
     * This is a pure virtual base class so it should not be possible to
     * instantiate it.
     *
     * \param mutex Global access mutex
     * \param database Gemini database reference
     * \param config Point configuration
     */
    CounterPoint(Mutex *gMutex,
                 GeminiDatabase& g3database,
                 CounterPoint::Config &config
                 );
    virtual ~CounterPoint();

    /* == Inherited from IPoint == */
    virtual GDB_ERROR init();
    virtual PointFlags getFlags() { return m_data.getFlags(); };
    virtual GDB_ERROR getValue(PointData *valuePtr);
    virtual GDB_ERROR getRAWValue(PointData* valuePtr);
    virtual GDB_ERROR getFrozenValue(PointData* valuePtr);
    virtual GDB_ERROR getRange(PointData* minValuePtr, PointData* maxValuePtr);
    virtual POINT_QUALITY getQuality() {return m_data.getQuality(); };
    virtual std::string getLabel(const PointData* valuePtr);
    GDB_ERROR freeze(const bool andClear = false);
    GDB_ERROR clear();

    /**
     * \brief Read the point current counting value.
     *
     * \param valuePtr where the data is saved.
     *
     * \return Error code.
     */
    virtual GDB_ERROR getCurrentValue(PointData *valuePtr);

    /**
     * \brief Check whether this counter is configured to be freezable or not
     *
     * \return LU_TRUE when it is freezable
     */
    lu_bool_t isFreezable();

protected:
    /* == Inherited from AbstractPoint == */
    virtual GDB_ERROR setFlags(const PointFlags& newFlags, const TimeManager::TimeStr& timestamp);
    virtual void update(PointData* newData, DATACHANGE dataChange = DATACHANGE_BOTH) = 0;
    virtual PointData* getData();
    virtual void tickEvent(TimeManager::TimeStr& timeStamp);

    /* == Inherited from TimeObserver == */
    virtual void updateTime(TimeManager::TimeStr& currentTime);

    /**
     * \brief Common update functionality for Counter Points
     *
     * It checks for a point value/status change or freeze command and updates
     * the observers accordingly.
     *
     * \param newData New value/status data
     * \param dataChange Fields of data affected by the change
     */
    virtual void updateCounterPoint(PointData* newData, DATACHANGE dataChange);

    /**
     * \brief Calculate new value after applying rollover
     *
     * \param New counter value before applying rollover
     *
     * \return New value after applying rollover
     */
    lu_uint32_t calcRollover(const lu_uint64_t value);

    /**
     * \brief Recovers the specified value type from persistent storage
     *
     * Fetches the value type (normal counter/frozen) from persistent storage
     * and stores it in the given point data
     *
     * \param mType Type of the value (normal/frozen) to fetch
     * \param pointData Where to store the retrieved value
     *
     * \return true when success
     */
    bool readPersistent(const lucyMem::MemType mType, PointDataCounter32& pointData);

    /**
     * \brief Stores the specified value type into persistent storage
     *
     * Stores the given value as value type (normal counter/frozen) into
     * persistent storage
     *
     * \param mType Type of the value (normal/frozen) to store
     * \param pointData Value to be stored
     *
     * \return true when success
     */
    bool storePersistent(const lucyMem::MemType mType, PointDataCounter32& pointData);

private:
    /**
     * \brief Types of freeze command
     */
    typedef enum
    {
        FREEZE_TYPE_COMMAND, //Freeze due to command
        FREEZE_TYPE_TIME     //Freeze due to timed event
    } FREEZE_TYPE;
private:
    /**
     * \brief Perform a freezing of the current value
     *
     * \param timestamp Time when the freeze takes place
     * \param andReset Implies a freeze-and-clear command
     * \param type Type of freeze command
     *
     * \return
     */
    virtual void freeze(TimeManager::TimeStr& timeStamp,
                        const bool andReset,
                        const FREEZE_TYPE type = FREEZE_TYPE_TIME);

    /**
     * \brief Tells if the Counter is configured to Clear (Reset) after freeze
     *
     * \return Current freeze-then-reset configuration
     */
    bool isConfigToClear();

protected:
    PointDataCounter32 m_data;       //Stored value (current)
    PointDataCounter32 m_frozenData; //Stored value (frozen)
    PointFlags m_lastFlags;          //Copy of the latest Flag report from the input

    /* Point configuration */
    CounterPoint::Config m_config;   //point configuration
    lu_uint64_t m_rollover;            //Rollover value - 64 bit for easier calculation

    PointPeriodicJob* freezeTimer;   //Periodic job for freezing the value when interval is specified
};


#endif /* COUNTERPOINT_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
