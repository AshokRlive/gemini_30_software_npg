/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Double Binary digital point implementation.
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "DoubleBinaryDigitalPoint.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/**
 * \brief Generate a Double Binary Value using two channels
 *
 * \param ch0 Channel 0 value (least-significant bit)
 * \param ch1 Channel 1 value (most-significant bit)
 *
 * \return Double binary value
 */
static inline lu_uint8_t genDBPValue(lu_uint8_t ch0, lu_uint8_t ch1)
{
    return ((ch0 | (ch1 << 1)) & 0x3);
}


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/** Periodic Job interval in ms (minimum resolution for debounce) */
static const lu_uint32_t DoubleBinaryPeriodicJobInterval = 100;

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
void DoubleBinaryDigitalPoint::DBDConfig::init()
{
    debounce = 0;
    for (lu_uint32_t i = 0; i < DBP_CHANNEL_IDX_LAST; ++i)
    {
        channelRef[i] = NULL;
    }
    DigitalPoint::Config::init();
}


DoubleBinaryDigitalPoint::DoubleBinaryDigitalPoint( Mutex *gMutex,
                                                    GeminiDatabase& g3database,
                                                    DBDConfig &config
                                                  ) :
                                          DigitalPoint(gMutex, g3database, POINT_TYPE_DBINARY, config),
                                          debounce(config.debounce),
                                          debounceRunning(false)
{
    /* Save channel reference */
    channel[DBP_CHANNEL_IDX_0] = config.channelRef[DBP_CHANNEL_IDX_0];
    channel[DBP_CHANNEL_IDX_1] = config.channelRef[DBP_CHANNEL_IDX_1];

    /* Initialise the internal values */
    channelData[DBP_CHANNEL_IDX_0] = 0;
    channelData[DBP_CHANNEL_IDX_1] = 0;

    m_conf.invert = LU_FALSE;   //Double Binary points cannot be inverted
    m_delayList = m_conf.sortDelay(POINT_TYPE_DBINARY);

    //Copy the map since the constructor doesn't assign arrays
    // (and I don't want to create a copy constructor in all the config parents)
    memcpy(&(m_conf.map), &(config.map), sizeof(m_conf.map));
}


DoubleBinaryDigitalPoint::~DoubleBinaryDigitalPoint()
{
    /* Detach observers */
    for (lu_uint32_t idx = DBP_CHANNEL_IDX_0; idx < DBP_CHANNEL_IDX_LAST; ++idx)
    {
        channel[idx]->detach(this);
        channel[idx] = NULL;
    }
}


GDB_ERROR DoubleBinaryDigitalPoint::init()
{
    if(initialised)
        return GDB_ERROR_INITIALIZED;

    if(channel == NULL)
        return GDB_ERROR_NOT_INITIALIZED;

    GDB_ERROR ret = GDB_ERROR_NONE;

    /* Register observer for both channels */
    IOM_ERROR chRet;
    for (lu_uint32_t idx = 0; idx < DBP_CHANNEL_IDX_LAST; ++idx)
    {
        chRet = channel[idx]->attach(this);
        if(chRet != IOM_ERROR_NONE)
        {
            ret = GDB_ERROR_NOT_INITIALIZED;
            log.error("%s Double Binary Digital Point (%s) - attach observer on channel %i error %i: %s",
                        __AT__, pointID.toString().c_str(), (idx+1), chRet, IOM_ERROR_ToSTRING(chRet)
                      );
            break;
        }
    }
    if(ret == GDB_ERROR_NONE)
    {
        //Note: we don't call DigitalPoint::init() here for we need the periodic
        //      job always running (at a different pace) for double value
        //      debouncing even when no chatter was configured.

        /* Always allocate a periodic Job (will be disposed of by the Job Manager at the end) */
        tickJob = setPeriodic(DoubleBinaryPeriodicJobInterval);
        if(tickJob != NULL)
        {
            initialised = true;
        }
        else
        {
            ret = GDB_ERROR_NOT_INITIALIZED;
        }
    }
    if(ret != GDB_ERROR_NONE)
    {
        /* Detach previously attached channels */
        for (lu_uint32_t idx = 0; idx < DBP_CHANNEL_IDX_LAST; ++idx)
        {
            channel[idx]->detach(this);
        }
    }

    return ret;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
void DoubleBinaryDigitalPoint::tickEvent(TimeManager::TimeStr& timeStamp)
{
    //Overridden for debounce check
    bool changed = false;
    PointDataUint8 eventData;
    lu_uint8_t oldRawValue;

    { /* Lock global mutex */
        LockingMutex lMutex(*mutex);
        oldRawValue = m_rawValue;
        if(debounceRunning)
        {
            /* Running the debounce: check if time is up */
            if(timeTmp.elapsed_ms(timeStamp) >= debounce)
            {
                /* Debounce timeStamp elapsed.
                 * Update value, call observers.
                 */
                debounceRunning = LU_FALSE;
                eventData = calcNewValue();
                eventData.setTime(timeStamp);
                changed = true;
            }
        }
    } //Release mutex
    if(changed)
    {
        updatePoint(eventData, false, true);   //Update from settled value
    }

    /* Apply chatter calculation */
    DigitalPoint::tickEvent(timeStamp);
}


void DoubleBinaryDigitalPoint::update(DBP_CHANNEL_IDX channelIdx, PointDataUint8& newData, DATACHANGE dataChange)
{
    /* We use this update to report a value or quality change in the input points.
     * Status of this double point is calculated later in this method since it
     * comes from 2 sources.
     */
    bool changed = false;
    PointDataUint8 eventData = newData;   //data copy used to update observers
    lu_uint8_t newInValue;      //Incoming raw value

    { /* Lock global mutex */
        LockingMutex lMutex(*mutex);

        /* Store incoming data */
        channelData[channelIdx] = newData;
        channelData[channelIdx].setFlags(newData.getFlags());

        newInValue = genDBPValue(channelData[DBP_CHANNEL_IDX_0],
                                 channelData[DBP_CHANNEL_IDX_1]);
        if(debounce)
        {
            /* Debounce enabled.
             * Store temporary time stamp and start debounce
             */
            newData.getTime(timeTmp);
            debounceRunning = true; //This starts the debounce on tickEvent
        }
        else
        {
            eventData = calcNewValue();
            changed = true;
        }
    } //Release mutex
    if(changed)
    {
        //Update point now
        updatePoint(eventData, dataChange, true);
        { /* Lock global mutex again */
            LockingMutex lMutex(*mutex);

            //When not debouncing, store the raw value, un-debounced and un-customised
            m_rawValue = newInValue;
        }
    }
}


GDB_ERROR DoubleBinaryDigitalPoint::scheduleUpdate(DBP_CHANNEL_IDX channelIdx,
                DoubleBinaryDigitalPoint& point, PointDataUint8& pointData,
                DATACHANGE dataChange)
{
    DPointEventJob* pEvent = new DPointEventJob(channelIdx, point, pointData, dataChange);
    GDB_ERROR ret = database.addJob(pEvent);
    if (ret != GDB_ERROR_NONE)
    {
        //Failed to schedule: run now!
        pEvent->run();
        delete pEvent;
    }
    return ret;

}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void DoubleBinaryDigitalPoint::update(IChannel* subject)
{
    IOM_ERROR chRet;
    DBP_CHANNEL_IDX index = DBP_CHANNEL_IDX_LAST;   //invalid value by default

    /* Find source ID */
    for (lu_uint32_t idx = 0; idx < DBP_CHANNEL_IDX_LAST; ++idx)
    {
        if( subject->getID() == channel[idx]->getID() )
        {
            index = static_cast<DBP_CHANNEL_IDX>(idx);    //channel found
            break;
        }
    }
    if(index >= DBP_CHANNEL_IDX_LAST)
    {
        return; //invalid channel: ignore
    }

    PointDataUint8 pointData;               //New point status to report
    IODataUint8 newValue;                   //Channel value
    IChannel::ValueStr dataRead(newValue);  //Channel data

     /* Read the channel value */
    chRet = subject->read(dataRead);
    if(chRet == IOM_ERROR_NONE)
    {
        POINT_QUALITY newQuality;
        TimeManager::TimeStr timeStamp;
        pointData = (lu_uint8_t)(newValue);
        newQuality = (dataRead.flags.online == LU_TRUE)? POINT_QUALITY_ON_LINE :
                                                          POINT_QUALITY_OFF_LINE;
        pointData.setQuality(newQuality);
        pointData.setInitialFlag(dataRead.flags.restart);
        pointData.setInvalidFlag(dataRead.flags.restart);
        pointData.setSimulatedFlag(dataRead.flags.simulated);
        newValue.getTime(timeStamp);
        pointData.setTime(timeStamp);

        /* Store incoming (latest) values */
        incomingData[index] = pointData;
    }
    else
    {
        //Error: report then the last value reported as offline & invalid
        pointData = incomingData[index];
        pointData.setQuality(POINT_QUALITY_OFF_LINE);
        pointData.setInvalidFlag(true);
        log.error("%s: Point (%s) - channel %s read error %i: %s",
                    __AT__, pointID.toString().c_str(), subject->getName(),
                    chRet, IOM_ERROR_ToSTRING(chRet)
                  );
    }
    scheduleUpdate(index, *this, pointData);
}


PointDataUint8 DoubleBinaryDigitalPoint::calcNewValue()
{
    /* Calculate new value and flags */
    PointDataUint8 eventData;   //data copy used to compose new point data

    lu_uint8_t newValue = genDBPValue(channelData[DBP_CHANNEL_IDX_0],
                                      channelData[DBP_CHANNEL_IDX_1]
                                     );
    eventData = newValue;
    //Double binary point flags depend on the status of both inputs
    eventData.setQuality( ( (channelData[DBP_CHANNEL_IDX_0].getQuality() == POINT_QUALITY_ON_LINE) &&
                            (channelData[DBP_CHANNEL_IDX_1].getQuality() == POINT_QUALITY_ON_LINE) )?
                                        POINT_QUALITY_ON_LINE : POINT_QUALITY_OFF_LINE
                        );
    eventData.setInitialFlag(channelData[DBP_CHANNEL_IDX_0].getInitialFlag() ||
                             channelData[DBP_CHANNEL_IDX_1].getInitialFlag()
                             );
    eventData.setInvalidFlag(channelData[DBP_CHANNEL_IDX_0].getInvalidFlag() ||
                             channelData[DBP_CHANNEL_IDX_1].getInvalidFlag()
                             );
    eventData.setChatterFlag(channelData[DBP_CHANNEL_IDX_0].getChatterFlag() ||
                             channelData[DBP_CHANNEL_IDX_1].getChatterFlag()
                             );
    eventData.setSimulatedFlag(channelData[DBP_CHANNEL_IDX_0].getSimulatedFlag() ||
                               channelData[DBP_CHANNEL_IDX_1].getSimulatedFlag() ||
                               m_simulating
                             );
    return eventData;
}


/*
 *********************** End of file ******************************************
 */

