/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:PersistentVPData.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       Data storage format for persistent data
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   30 Nov 2017     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef PERSISTENTVPDATA_H_
#define PERSISTENTVPDATA_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <iostream>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "PersistentVPcommon.h"
#include "IPoint.h"
#include "bitOperations.h"

namespace lucyMem
{

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define MEMFILENAME "persistentVP.mem"
#define MEMFILENAME_FROZEN "persistentVPCfrozen.mem"

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/**
 * \brief Point flags structure for reduced space
 */
struct PointFlagsReduced
{
public:
    lu_uint8_t online    : 1; //Point is online
    lu_uint8_t initial   : 1; //Value still has its initialisation value (was never updated).
    lu_uint8_t invalid   : 1; //Value is valid.
    lu_uint8_t overflow  : 1; //Value has overflowed/rolled over
    lu_uint8_t chatter   : 1; //Digital point is in chatter
    lu_uint8_t simulated : 1; //Value is being simulated locally.
    lu_uint8_t unused    : 1;
public:
    /**
     * \brief Default constructor: offline, initial & invalid
     */
    PointFlagsReduced() :
                        online(false),
                        initial(true),
                        invalid(true),
                        overflow(false),
                        chatter(false),
                        simulated(false),
                        unused(0)
    {};

    /**
     * \brief Creates a reduced-space structure from virtual point flag structure
     *
     * \param flags Flags in point structure
     *
     * \return Resulting flags are stored in this structure
     */
    PointFlagsReduced(const PointFlags& flags) :
                        online(flags.isOnline()),
                        initial(flags.isInitial()),
                        invalid(flags.isInvalid()),
                        overflow(flags.isOverflow()),
                        chatter(flags.isChatter()),
                        simulated(flags.isSimulated()),
                        unused(0)
    {};

    /**
     * \brief Converts a virtual point flag structure into reduced one
     *
     * \param flags Flags in point structure
     *
     * \return Resulting flags are stored in this structure
     */
    void setFlags(const PointFlags& flags)
    {
        online = flags.isOnline();
        initial = flags.isInitial();
        invalid = flags.isInvalid();
        overflow = flags.isOverflow();
        chatter = flags.isChatter();
        simulated = flags.isSimulated();
        unused = 0;
    }

    /**
     * \brief Converts this reduced-space structure to a virtual point flag structure
     *
     * \return Resulting flags in Virtual Point structure
     */
    PointFlags getFlags() const
    {
        PointFlags flags;
        flags.setOnline(online);
        flags.setInitial(initial);
        flags.setInvalid(invalid);
        flags.setOverflow(overflow);
        flags.setChatter(chatter);
        flags.setSimulated(simulated);
        return flags;
    }
};


/**
 * \brief Type of memory entry
 */
typedef lu_uint8_t MemEntryType;

/**
 * \brief Point data to be stored in memory
 *
 * Point data to be stored in persistent memory. This includes not only the
 * value but the time stamp, synchronisation, and flags.
 *
 * WARNING: do NOT pack this structure: this creates nasty side effects when the
 *          compiler tries to deal with unaligned 64-bit integers (when VPointUID
 *          is a lu_uint64_t), converting the number into a shifted one.
 *          Currently the size of the structure is 32 bytes; in order to save
 *          storage, use serialization byte-by-byte, but that can save just
 *          9 bytes [sz=23 bytes] and is discarded here.
 */
//#pragma pack(push)
//#pragma pack(1) //DO NOT pack!!!
struct MemEntry
{
    MemEntryType type;      //Uses MemType enum
    VPointUID uid;
    PointFlagsReduced flags;
    bool synchronised;          //True when time is synchronised
    struct timespec timestamp;  //Absolute time (as in system time: CLOCK_REALTIME)
    union
    {
        DigitalPointValue digital;
        AnaloguePointValue analogue;
        CounterPointValue counter;
    } value;
public:
    /**
     * \brief Convert the point data content into a string
     *
     * Note that this is the default implementation for integers (single/double
     * binary points and counters). Other implementations must need to override
     * this method.
     */
    virtual const std::string toString() const
    {
        std::stringstream ss;
        ss << "type=" << (lu_uint32_t)type
           << " uid=" << uid
           << " t=" << timestamp.tv_sec;
        if(synchronised)
        {
            ss << "(SYNC)";
        }
        ss << " " << flags.getFlags().toString();
        switch(type)
        {
            case MemType_DIGITAL:
                ss << " Vd=" << (lu_uint32_t)value.digital;  //special case: 8-bit int is printed as char
                break;
            case MemType_ANALOGUE:
                ss << " Va=" << (lu_float32_t)value.analogue;
                break;
            case MemType_COUNTER:
            case MemType_COUNTER_FROZEN:
                ss << " Vc=" << (lu_uint32_t)value.counter;
                break;
        }
        return ss.str();
    }
};
//#pragma pack(pop)

const std::streamsize EntrySize = sizeof(MemEntry);


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */



}//end namespace lucyMem

#endif /* PERSISTENTVPDATA_H_ */

/*
 *********************** End of file ******************************************
 */
