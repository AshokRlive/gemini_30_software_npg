/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Analogue deadband filter interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   20/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_92BECFE5_48AA_400c_B283_39B557A5E697__INCLUDED_)
#define EA_92BECFE5_48AA_400c_B283_39B557A5E697__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IAnalogueFilter.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/**
 * \brief Deadband analogue filter
 *
 * The filter threshold is: value > nominal +- (deadband/2)
 *
 * When the threshold is exceeded the nominal value is updated using the
 * current value
 *
 */
class DeadbandFilter : public IAnalogueFilter
{
public:
    struct Config
    {
    public:
        lu_float32_t deadband;  //Deadband, half range (either side of the nominal value)
        lu_bool_t useNominal;   //The initial Nominal Value must be used
        lu_float32_t nominal;   //Initial nominal value
    };


public:
    /**
     * \brief Default constructor
     *
     * \param nominal Initial nominal value
     * \param deadband Deadband
     *
     */
    DeadbandFilter(DeadbandFilter::Config& conf);
    virtual ~DeadbandFilter() {};

    /**
	 * \brief Run the filter
	 * 
	 * \param value New value to add to the filter
	 * 
	 * \return filter result
	 */
    virtual AFILTER_RESULT run(lu_float32_t value);

private:
    bool m_initial;           //Initial (nominal) value has been provided
    lu_float32_t m_nominal;   //Deadband center value that changes over time
    lu_float32_t m_deadband;  //Half of the Deadband size, used for center +/- deadband
};


#endif // !defined(EA_92BECFE5_48AA_400c_B283_39B557A5E697__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
