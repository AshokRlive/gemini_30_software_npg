/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: LabelPool.h 15 Apr 2015 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/Points/include/LabelPool.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 15 Apr 2015 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15 Apr 2015   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef LABELPOOL_H__INCLUDED
#define LABELPOOL_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <string>
#include <vector>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "LockingMutex.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Label storage pool
 *
 * This is a Pool pattern object that stores strings (labels) in an unique
 * storage in a bid of saving memory from duplicated labels.
 *
 * Labels are stored in an indexed table, with both index and labels being unique.
 *
 * This class is a singleton.
 */
class LabelPool
{
public:
    /**
     * \brief Type of the index used for indexing labels in the pool
     */
    typedef lu_uint32_t LabelIndex;

public:
    /**
     * \brief Get a LabelPool instance.
     *
     * Usage example:
     *  LabelPool& labelPool = LabelPool::getInstance();
     *
     * \return Reference to this singleton
     */
    static LabelPool& getInstance()
    {
        static LabelPool instance;
        return instance;
    };

    /**
     * \brief Add a label to the pool if not already present
     *
     * If not already present, the label is added - otherwise the index returned
     * is the label.
     *
     * \param label Text to add
     *
     * \return index of the label
     */
    LabelPool::LabelIndex addLabel(const std::string label);

    /**
     * \brief Get a label from the pool
     *
     * \param index of the label
     *
     * \return label Text
     */
    const std::string getLabel(const LabelPool::LabelIndex index);

private:
    /**
     * Private constructor (Singleton)
     */
    LabelPool() : mLock(LU_TRUE) {};
    /**
     * Copy Constructor
     */
    LabelPool(const LabelPool &labelPool) { LU_UNUSED(labelPool); };
    virtual ~LabelPool() {};

private:
    /* Note that we store it in a vector because (slower) adding is done once,
     *      during startup, but getting should be done quicker (during operation).
     */
    typedef std::vector<std::string> LabelList;

private:
    LabelList labelList;
    MasterMutex mLock;   //Lock to access the list
};


#endif /* LABELPOOL_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
