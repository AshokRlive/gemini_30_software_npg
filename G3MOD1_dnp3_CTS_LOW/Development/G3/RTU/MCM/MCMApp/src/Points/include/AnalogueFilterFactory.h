/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Analogue filter factory interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   20/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_0F3B0810_B0CF_47d5_8078_F201027BA976__INCLUDED_)
#define EA_0F3B0810_B0CF_47d5_8078_F201027BA976__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "IAnalogueFilter.h"
#include "DeadbandFilter.h"
#include "LowerUpperLimitFilter.h"
#include "HIHILOLOFilter.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef enum
{
    AFILTER_TYPE_NONE,
    AFILTER_TYPE_LOWER_UPPER_LIMIT,
    AFILTER_TYPE_DEADBAND,
    AFILTER_TYPE_HIHILOLO,

    AFILTER_TYPE_LAST
}AFILTER_TYPE;

struct AnalogueFilterFactoryConf
{

public:
    AFILTER_TYPE filterType;
    DeadbandFilter::Config          DBFilter ;
    LowerUpperLimitFilter::Config   LULFilter;
    HIHILOLOFilter::Config          HHLLFilter;
};

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class AnalogueFilterFactory
{

public:
    virtual ~AnalogueFilterFactory() {};

    /**
	 * \brief Get the analogue filter instance
	 * 
	 * \return Pointer to the analogue filter factory
	 */
    static AnalogueFilterFactory *getInstance() {return &instance;};

	/**
	 * \brief Create a new analogue filter
	 * 
	 * \param config Filter configuration
	 * 
	 * \return Pointer to the filter
	 */
	IAnalogueFilter* newFilter(AnalogueFilterFactoryConf& config);

private:
    AnalogueFilterFactory() {};

    static AnalogueFilterFactory instance;
};


#endif // !defined(EA_0F3B0810_B0CF_47d5_8078_F201027BA976__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
