/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Dummy Factory class for virtual points
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   18/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_974BD523_CEE4_46fb_A3C4_431394C6AE52__INCLUDED_)
#define EA_974BD523_CEE4_46fb_A3C4_431394C6AE52__INCLUDED_


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "IPointFactory.h"
#include "IIOModuleManager.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Dummy virtual point factory
 * 
 * Used for testing creates points with hard-coded configuration
 */
class DummyPointFactory : public IPointFactory
{

public:
    DummyPointFactory(IIOModuleManager &MManager);
    virtual ~DummyPointFactory();

    /**
     * \brief Return the total number of points the factory can create
     *
     * \return Points number
     */
    virtual lu_uint16_t getPointNumber();

    /**
     * \brief Create a new virtual point
     *
     * \param mutex Reference to the mutex used in the virtual point constructor
     * \param database Reference to the database used in the
     * virtual point constructor
     * \param pointID Virtual point point ID
     *
     * \return Pointer to the new virtual point. NULL in case of error.
     */
    virtual IPoint *newPoint( Mutex *mutex            ,
                              GeminiDatabase *database,
                              PointIdStr pointID
                            );

private:
    IIOModuleManager &MManager;

};
#endif // !defined(EA_974BD523_CEE4_46fb_A3C4_431394C6AE52__INCLUDED_)


/*
 *********************** End of file ******************************************
 */


