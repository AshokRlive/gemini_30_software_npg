/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: PersistentVPcommon.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief Common definitions for Persistent Virtual Points
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   18 Jan 2018     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef PERSISTENTVPCOMMON_H_
#define PERSISTENTVPCOMMON_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IPoint.h"     //For VPointUID definition

namespace lucyMem
{

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/**
 * \brief Type of memory entry
 */
typedef enum
{
    MemType_NONE,
    MemType_DIGITAL,
    MemType_ANALOGUE,
    MemType_COUNTER,
    MemType_COUNTER_FROZEN,
    MemType_LAST
}MemType;

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

static const lu_uint8_t NUMFILES = 2; //Files handled -- VP & Frozen
struct FileInfo
{
    const lu_char_t* fileName;
};

struct FileperType
{
    MemType type;
    lu_uint8_t fileNum;
    const lu_char_t* fileName;
};

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
static const FileInfo FileData[NUMFILES] = {
                {"persistentVP.mem" },
                {"persistentVPCfrozen.mem"}
            };
//Ensure lucyMem::NUMFILES was not modified without adding entries on FileData array
LU_STATIC_ASSERT( (NUMFILES == 2), FileData_Array_size_changed);

static const FileperType FileType[MemType_LAST] = {
                {MemType_NONE,              0, FileData[0].fileName },
                {MemType_DIGITAL,           0, FileData[0].fileName },
                {MemType_ANALOGUE,          0, FileData[0].fileName },
                {MemType_COUNTER,           0, FileData[0].fileName },
                {MemType_COUNTER_FROZEN,    1, FileData[1].fileName}
            };
//Ensure MemType enum was not modified without adding entries on FileType array
LU_STATIC_ASSERT( (MemType_LAST == (MemType_COUNTER_FROZEN+1)), FileType_Array_size_changed);

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

}//end namespace lucyMem

#endif /* PERSISTENTVPCOMMON_H_ */

/*
 *********************** End of file ******************************************
 */
