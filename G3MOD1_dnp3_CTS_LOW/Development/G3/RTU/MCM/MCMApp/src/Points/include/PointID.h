/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:PointID.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *      Point reference definition
 *
 *    CURRENT REVISION:
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   30 Sep 2015     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef POINTID_H_
#define POINTID_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <sstream>
#include <string>   //PointId toString()

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Virtual Point ID structure
 */
struct PointIdStr
{
public:
    lu_uint16_t group;
    lu_uint16_t ID;

    static const lu_uint16_t invalidID    = 0xffff;
    static const lu_uint16_t invalidGroup = 0xffff;

public:
    PointIdStr() : //Set default values
                    group(invalidGroup),
                    ID(invalidID)
    {};

    PointIdStr(lu_uint16_t pointGroup, lu_uint16_t pointID) : //Create point ID
                                                            group(pointGroup),
                                                            ID(pointID)
    {};

    bool operator==(const PointIdStr a)
    {
        return ( (this->group == a.group) && (this->ID == a.ID) );
    }
    bool operator!=(const PointIdStr a)
    {
        return !(*this == a);
    }
    bool isValid() const
    {
        return (this->group != invalidGroup) && (this->ID != invalidID);
    }
    const std::string toString() const
    {
        if(isValid())
        {
            std::ostringstream ss;
            ss << this->group << "," << this->ID;
            return ss.str();
        }
        return "-invalid-";
    }
};


#endif /* POINTID_H_ */

/*
 *********************** End of file ******************************************
 */
