/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: StandardCounterPoint.h 14 Jul 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/Points/include/StandardCounterPoint.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Standard (Virtual Point source) Counter type virtual point.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 14 Jul 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14 Jul 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef STANDARDCOUNTERPOINT_H__INCLUDED
#define STANDARDCOUNTERPOINT_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "CounterPoint.h"
#include "DigitalChangeEvent.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Custom implementation for Standard Counter Point.
 *
 * This Counter Point will report events for data change and freeze.
 * The data source is a Single Binary digital Virtual Point.
 */
class StandardCounterPoint: public CounterPoint, private IPointObserver
{
public:
    struct StdConfig : public CounterPoint::Config
    {
        PointIdStr IDRef;       //Reference of the virtual point to count (digital VP)
        DigitalChangeEvent::Config eventCfg;    //Digital event configuration
    };

public:
    /**
     * \brief Custom constructor
     *
     * This is a pure virtual base class so it should not be possible to
     * instantiate it.
     *
     * \param mutex Global access mutex
     * \param database Gemini database reference
     * \param config Point configuration
     */
    StandardCounterPoint(Mutex *mutex              ,
                         GeminiDatabase& database   ,
                         StandardCounterPoint::StdConfig &config
                         );
    virtual ~StandardCounterPoint();

    /* == Inherited from IPoint == */
    virtual GDB_ERROR init();

protected:
    friend class DigitalChangeEvent;

    /* == Inherited from CounterPoint == */
    virtual void update(PointData* newData, DATACHANGE dataChange = DATACHANGE_BOTH);
    void updateTime(TimeManager::TimeStr& currentTime);

    /* == Inherited from IPointObserver == */
    virtual void update(PointIdStr pointID, PointData* pointDataPtr);
    virtual PointIdStr getPointID() { return IDRef; };

private:
    StdConfig config;           //Standard Counter-specific configuration
    PointIdStr IDRef;           //Reference of the virtual point to count (digital VP)
    DigitalChangeEvent event;   //Digital Input event handler
};


#endif /* STANDARDCOUNTERPOINT_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
