/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Double Binary digital point interface.
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_D314D012_766A_4e51_B60C_84D434E4FF66__INCLUDED_)
#define EA_D314D012_766A_4e51_B60C_84D434E4FF66__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "DigitalPoint.h"
#include "IChannel.h"
#include "ChannelObserver.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/**
 * Custom implementation for Double Binary digital point.
 * The data source is an IOChannel.
 * Implements debouncing from 2 sources, settling the value after the 2 values
 * are stable for a given period of time.
 * Online/offline status changes are applied immediately.
 */
class DoubleBinaryDigitalPoint : public DigitalPoint, private ChannelObserver
{
public:
    /**
     * \brief Source of the change
     *
     * Index of the point that is the source of the Double Point change
     */
    enum DBP_CHANNEL_IDX
    {
        DBP_CHANNEL_IDX_0 = 0,
        DBP_CHANNEL_IDX_1    ,
        DBP_CHANNEL_IDX_LAST
    };

    /**
     * \brief Double Binary Digital Point Configuration
     */
    struct DBDConfig : public DigitalPoint::Config
    {
    public:
        lu_uint32_t debounce;   //Debounce time (in milliseconds)
        IChannel* channelRef[DBP_CHANNEL_IDX_LAST]; //Channel reference
    public:
        DBDConfig()
        {
            init();
        }
        /**
         * \brief Reset structure to default values
         */
        void init();
    };

public:
    DoubleBinaryDigitalPoint( Mutex *mutex                          ,
                              GeminiDatabase& database              ,
                              DBDConfig &config
                            );
    virtual ~DoubleBinaryDigitalPoint();

    /* == Inherited from IPoint == */
    virtual GDB_ERROR init();

protected:
    /**
     * This class schedules a point update from a channel observer, keeping the
     * reference of the originator channel index.
     */
    class DPointEventJob : public IJob
    {
    public:
        /**
         * \brief Custom constructor
         *
         * \param pointObj Point to be reported to
         * \param pointData New value/status data change to be scheduled
         * \param dataChange Fields of data affected by the change
         */
        DPointEventJob( DBP_CHANNEL_IDX channelIdx,
                        DoubleBinaryDigitalPoint& pointObj,
                        PointDataUint8& pointData,
                        DATACHANGE dataChange) :
                                                channelIndex(channelIdx),
                                                point(pointObj),
                                                data(pointData),
                                                change(dataChange)
        {};
        virtual ~DPointEventJob() {};

        /**
         * \brief Method executed by the job scheduler
         */
        virtual void run()
        {
            point.update(channelIndex, data, change);
        }

    private:
        DBP_CHANNEL_IDX channelIndex;
        DoubleBinaryDigitalPoint& point;
        PointDataUint8 data;
        DATACHANGE change;
    };

protected:
    /* == Inherited from DigitalPoint == */
    virtual void tickEvent(TimeManager::TimeStr& timePtr);
private:
    /**
     * \brief Custom point update
     *
     * Updates Double Point from a change in one of the inputs
     *
     * \param channelIdx Double Point's Index of the channel to update
     * \param pointData New value/status data change to be scheduled
     * \param dataChange Fields of data affected by the change
     */
    virtual void update(DBP_CHANNEL_IDX channelIdx,
                        PointDataUint8& newData,
                        DATACHANGE dataChange);

    /**
     * \brief Schedules a Database Job to update a point
     *
     * It is used instead of AbstractPoint::scheduleUpdate() in order to provide
     * which channel has changed (channel index).
     *
     * NOTE: the job created here will be automatically destroyed by the G3
     *      database Scheduler.
     *
     * \param channelIdx Index of the source of change
     * \param point Point to be reported to
     * \param pointData New value/status data change to be scheduled
     * \param dataChange Fields of data affected by the change
     *
     * \return Error code
     */
    GDB_ERROR scheduleUpdate(DoubleBinaryDigitalPoint::DBP_CHANNEL_IDX channelIdx,
                             DoubleBinaryDigitalPoint& point,
                             PointDataUint8& pointData,
                             DATACHANGE dataChange = DATACHANGE_ALL);

private:
    /* == Inherited from ChannelObserver == */
    virtual void update(IChannel* subject);

    /**
     * \brief Calculate new value and flags
     *
     * Obtains the Double Binary point value and flags from source channel.
     *
     * \return Resulting point data
     */
    PointDataUint8 calcNewValue();

private:
    /* Point configuration */
    IChannel *channel[DBP_CHANNEL_IDX_LAST];    //Input channels' references
    lu_uint8_t m_map[4];            //Value map for combination of inputs
    lu_uint32_t debounce;           //Configured debounce time

    /* Point Status */
    PointDataUint8 channelData[DBP_CHANNEL_IDX_LAST]; //Raw data from source
    PointDataUint8 incomingData[DBP_CHANNEL_IDX_LAST]; //Latest value/flags set (setValue/setFlags/setQuality)
    bool debounceRunning;           //debounce calculation is running
    TimeManager::TimeStr timeTmp;   //Temporary time stamp for debouncing purposes
};


#endif // !defined(EA_D314D012_766A_4e51_B60C_84D434E4FF66__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
