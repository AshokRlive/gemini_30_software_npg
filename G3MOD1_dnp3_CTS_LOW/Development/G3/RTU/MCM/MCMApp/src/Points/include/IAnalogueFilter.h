/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Point Analogue filter virtual interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   20/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_BA0E7397_CAF6_4bce_8CC2_BAF48B747B36__INCLUDED_)
#define EA_BA0E7397_CAF6_4bce_8CC2_BAF48B747B36__INCLUDED_


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef enum __attribute__((packed, aligned(1)))    //__attribute__ makes the enum 1-byte wide
{
    AFILTER_RESULT_NONE      = 0,
    AFILTER_RESULT_NA,                  //Analogue filter Not available
    AFILTER_RESULT_IN_LIMIT,            //Value within limit (filtered)
    AFILTER_RESULT_IN_LIMIT_THR,        //Value just re-entered the limit area
    AFILTER_RESULT_OUT_LIMIT,           //Value out of limit
    AFILTER_RESULT_OUT_LIMIT_THR,       //Value just exceed the threshold

    AFILTER_RESULT_LAST
}AFILTER_RESULT;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class IAnalogueFilter
{
public:
    struct ThresholdParams
    {
    public:
        bool active;                //This threshold is in use
        lu_float32_t threshold;     //Threshold value
        lu_float32_t hysteresis;    //Related hysteresis (as "absolute value" delta, not %)
        lu_float32_t recovery;      //Related (calculated) recovery threshold
    public:
        ThresholdParams() : active(false), threshold(0.0), hysteresis(0.0), recovery(0.0)
        {};
    };

public:
    IAnalogueFilter() : status(AFILTER_RESULT_NA)
    {}

    virtual ~IAnalogueFilter() {};

    /**
	 * \brief Run the filter
	 * 
	 * \param value New value to add to the filter
	 * 
	 * \return filter result
	 */
    virtual AFILTER_RESULT run(lu_float32_t value) = 0;

    /**
	 * \brief Get filter status
	 * 
	 * \return Filter status
	 */
	inline AFILTER_RESULT getStatus() {return status;};

	/**
	 * \brief Function to get the string associated to the AFILTER_RESULT enum
	 *
	 * \param filterRes enum value
	 *
	 * \return Char string associated to the given value
	 */
	static const lu_char_t* AFILTER_RESULT_ToString(const AFILTER_RESULT filterRes)
	{
	    return (lu_char_t*)(
	           (filterRes == AFILTER_RESULT_NONE)? "None" :
	               (filterRes == AFILTER_RESULT_NA)? "NotAvail" :
	                   (filterRes == AFILTER_RESULT_IN_LIMIT)? "within" :
	                       (filterRes == AFILTER_RESULT_IN_LIMIT_THR)? "inLimitTHR" :
	                           (filterRes == AFILTER_RESULT_OUT_LIMIT)? "outLimit" :
	                               (filterRes == AFILTER_RESULT_OUT_LIMIT_THR)? "outLimitTHR" :
	                                               "invalid"
	           );
	};

public:
    static const lu_uint16_t AF_MAX_HYSTERESIS_PERCENT = 100;

protected:
    AFILTER_RESULT status;
};

#endif // !defined(EA_BA0E7397_CAF6_4bce_8CC2_BAF48B747B36__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
