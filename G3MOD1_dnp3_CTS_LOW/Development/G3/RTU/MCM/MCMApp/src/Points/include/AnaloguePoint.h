/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Virtual analogue point interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   20/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_F73049FA_07DD_44f6_85B4_36EC834F5CB1__INCLUDED_)
#define EA_F73049FA_07DD_44f6_85B4_36EC834F5CB1__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "AbstractPoint.h"
#include "IAnalogueFilter.h"
#include "AnalogueFilterFactory.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Virtual Analogue point common container.
 *
 * This is a base class for all specific implementations of an Analogue Point.
 * Common m_data and logic for all analogue points is implemented here.
 */
class AnaloguePoint : public AbstractPoint
{
public:
    /**
     * \brief Full value range configuration
     */
    struct FullRange
    {
    public:
        lu_float32_t maxValue;  //Maximum Value of the full range
        lu_float32_t minValue;  //Minimum Value of the full range
    };

    /**
     * \brief Common overflow/underflow configuration
     */
    struct OverRange : IAnalogueFilter::ThresholdParams
    {
    public:
        bool upper; //States that this is an Upper threshold. Set to .False. for underflow

    public:
        bool isValid();
        void update();
    };

    /**
     * \brief Common Analogue Point Configuration
     */
    struct Config : public AbstractPoint::BaseConfig<AnaloguePointValue>
    {
    public:
        PointIdStr ID;                      //Analogue Point ID
        AnalogueFilterFactoryConf filter;   //Filter config to be used
        lu_float32_t scale;                 //Value scale
        lu_float32_t offset;                //Value offset
        FullRange fullRange;                //Full range of the Virtual Point

        /* TODO: pueyos_a - what to do when overflow == underflow. Is it still valid? */
        OverRange overflow;                 //Overflow limits
        OverRange underflow;                //Underflow limits

    public:
        bool isValid();
        void update();
    };

public:
    /**
     * \brief Custom constructor
     *
     * This is a pure virtual base class so it should not be possible to
     * instantiate it.
     *
     * \param mutex Global access mutex
     * \param database Gemini database reference
     * \param config Point configuration
     */
    AnaloguePoint( Mutex *mutex               ,
                   GeminiDatabase& database   ,
                   AnaloguePoint::Config &config
                 );
    virtual ~AnaloguePoint();

     /* == Inherited from IPoint == */
    virtual GDB_ERROR init() = 0;
    virtual PointFlags getFlags() { return m_data.getFlags(); };
    virtual GDB_ERROR getValue(PointData *valuePtr);
    virtual GDB_ERROR getRAWValue(PointData* valuePtr);
    virtual POINT_QUALITY getQuality() {return m_data.getQuality(); };
    virtual GDB_ERROR getRange(PointData* minValuePtr, PointData* maxValuePtr);
    virtual std::string getLabel(const PointData* valuePtr);

protected:
    /* == Inherited from AbstractPoint == */
    virtual GDB_ERROR setFlags(const PointFlags& newFlags, const TimeManager::TimeStr& timestamp);
    virtual void update(PointData* newData, DATACHANGE dataChange = DATACHANGE_ALL) = 0;
    virtual PointData* getData();
    virtual void tickEvent(TimeManager::TimeStr& timeStamp)
    {
        LU_UNUSED(timeStamp);   //default implementation for analogue's tickEvent
    };

    /**
     * \brief Common update functionality for Analogue Points
     *
     * It checks for a point value/status change and updates the observers accordingly.
     *
     * \param newData New value/status m_data
     * \param dataChange Fields of m_data affected by the change
     */
    virtual void updateAnaloguePoint(PointData* newData, DATACHANGE dataChange);

private:
    /**
     * \brief Set the overflow flag if current value exceeds overflow limits
     */
    bool calcOverflow(const lu_float32_t reportedValue);

protected:
    typedef enum
    {
        REGION_UNDERFLOW,
        REGION_NORMAL,
        REGION_OVERFLOW
    } REGION;

protected:
    PointDataFloat32 m_data;      //stored value (last value & flags reported)
    lu_float32_t m_rawValue;      //Last reported m_data, unfiltered and unprocessed
    //PointDataFloat32 simulatedData; //Simulated (forced) point value & flags

    /* Point configuration */
    REGION m_region;                    //Over Range region
    OverRange overflow;                 //Overflow limits
    OverRange underflow;                //Underflow limits
    lu_float32_t scale;
    lu_float32_t offset;
    FullRange fullRange;                //Full range of the Virtual Point
    PointLabels<lu_float32_t> m_labels; //Value labels storage (copy from configuration)
    IAnalogueFilter *filter;

};


#endif // !defined(EA_F73049FA_07DD_44f6_85B4_36EC834F5CB1__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
