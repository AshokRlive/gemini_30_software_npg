/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       analogue Upper/Lower limit filter interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   20/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_F77264E7_4442_4f71_9EA4_6710E4DBCDD2__INCLUDED_)
#define EA_F77264E7_4442_4f71_9EA4_6710E4DBCDD2__INCLUDED_


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "IAnalogueFilter.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Upper/Lower Limit analogue filter
 * 
 * The filter behaviour is:
 * if "in limit" value < lowerLimit -> "Out of limit"
 * if "in limit" value > upperLimit -> "Out of limit"
 * 
 * if "out of limit" value < upperLimit * (1 - hysteresis) -> " In limit "
 * if "out of limit" value > lowerLimit * (1 - hysteresis) -> " In limit "
 */
class LowerUpperLimitFilter : public IAnalogueFilter
{
public:
     struct Config
     {
     public:
         IAnalogueFilter::ThresholdParams lower; //lower Limit
         IAnalogueFilter::ThresholdParams upper; //upper Limit
     public:
         /**
         * \brief check validity of the thresholds and hysteresis
         *
         * Thresholds and hysteresis are expected to be in the same units, and
         * fulfil all of the the following conditions when they are active:
         *      LowerThreshold <= LowerHysteresis
         *      LowerThreshold < UpperThreshold
         *      UpperHysteresis <= UpperThreshold
         *
         * \return True when the config is valid
         */
         bool isValid()
         {
             update();  //recalculate recovery thresholds

             bool lowerHyThr = lower.active && (lower.threshold <= lower.recovery);
             bool upperHyThr = upper.active && (upper.threshold >= upper.recovery);
             bool upperlowerHy = (lower.recovery < upper.threshold) &&
                                 (upper.recovery > lower.threshold); //Note: this forces Lower < Upper thresholds
             return (lowerHyThr || upperHyThr || (lowerHyThr && upperHyThr && upperlowerHy) );
         }
         void update()
         {
             lower.recovery = lower.threshold + lower.hysteresis;
             upper.recovery = upper.threshold - upper.hysteresis;
         }
     };

public:
	/**
	 * \brief Default constructor
	 * 
     * \param config Filter configuration
	 */
    LowerUpperLimitFilter(LowerUpperLimitFilter::Config& config);
    virtual ~LowerUpperLimitFilter() {};

    /**
	 * \brief Run the filter
	 * 
	 * \param value New value to add to the filter
	 * 
	 * \return filter result
	 */
    virtual AFILTER_RESULT run(lu_float32_t value);

private:
    typedef enum
    {
        REGION_LOWER,
        REGION_MIDDLE,
        REGION_UPPER
    } REGION;
private:
    LowerUpperLimitFilter::Config m_config;
    REGION m_region;
};


#endif // !defined(EA_F77264E7_4442_4f71_9EA4_6710E4DBCDD2__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
