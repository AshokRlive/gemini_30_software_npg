/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Virtual digital point implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "DigitalPoint.h"
#include "Debug.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define DEBUG_DIG_VP 0
#if DEBUG_DIG_VP
    #define DBGLOG_VPD_HEADER   "VPD"
    #define DBGLOG_VPD_ERROR(text)                  DBG_ERR(DBGLOG_VPD_HEADER" %s %s", pointID.toString().c_str(), text)
    #define DBGLOG_MEM_READ(pointID, eventData)     DBG_INFO(DBGLOG_VPD_HEADER" %s Set Initial Value: %u", pointID.toString().c_str(), m_rawValue)
    #define DBGLOG_MEM_WRITE(pointID, eventData)    DBG_INFO(DBGLOG_VPD_HEADER" %s Set Initial Value: %u", pointID.toString().c_str(), m_rawValue)
#else
    #undef DBG_DTOR
    #define DBG_DTOR()
    #define DBGLOG_VPD_ERROR(text)
    #define DBGLOG_MEM_READ(pointID, eventData)
    #define DBGLOG_MEM_WRITE(pointID, eventData)
#endif


#define RAWVALUE_INVALID 0xFF

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/* XXX: pueyos_a -  (to be removed) */
#define DEBUG_VPOINTS 0
#if DEBUG_VPOINTS
//Point under test:
#define TEST_POINT PointIdStr(5,10)
std::string getAction(DigitalPoint::DACTION a)
{
    std::string ret;
    switch (a)
    {
        case DigitalPoint::DACTION_NONE:
            ret = "A=None";
            break;
        case DigitalPoint::DACTION_DELAY:
            ret = "A=Delay";
            break;
        case DigitalPoint::DACTION_CANCEL_OR_PUB:
            ret = "A=Can/Pub";
            break;
        case DigitalPoint::DACTION_PUBLISH:
            ret = "A=Pub";
            break;
        default:
            break;
    }
    return ret;
}

/* XXX: pueyos_a -  (to be removed) */
std::string getEdge(PointData& p)
{
    std::string ret;
    switch(p.getEdge())
    {
        case EDGE_TYPE_NONE:
            ret = "E=None";
            break;
        case EDGE_TYPE_POSITIVE:
            ret = "E=Pos";
            break;
        case EDGE_TYPE_NEGATIVE:
            ret = "E=Neg";
            break;
        default:
            ret = "E=??";
            break;
    }
    return ret;
}
#endif


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/** Periodic Job interval in ms: just for the Chatter Manager, so 1 second
 * interval resolution (-20%) should be enough.
 */
static const lu_uint32_t DigitalPointPeriodicTickInterval_ms = 800; //1000;

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
void DigitalPoint::Config::init()
{
    invert =LU_FALSE;
    chatterNo = 0;
    chatterTime = 0;
    for (lu_uint32_t i = 0; i < 4; ++i)
    {
        map[i] = i; //default values
    }
}


DigitalPoint::Config::DelayList DigitalPoint::Config::sortDelay(const POINT_TYPE pointType)
{
    DelayList ret;
    lu_uint32_t numEntries = (pointType == POINT_TYPE_BINARY)? 2 : 4;

    //Note that this is O(n^2), but is OK since it is intended for max of 4 delays
    for(lu_uint32_t i = 0; i < numEntries; ++i)
    {
        Delay entry(i); //default delay is 0
        for(DelayList::const_iterator it = delay.begin(); it != delay.end(); ++it)
        {
            if((*it).eventVal == i)
            {
                entry.delay_ms = (*it).delay_ms;
                break;  //found
            }
        }
        ret.push_back(entry);
    }
    //Note that the resulting list is ordered by the event value
    return ret;
};


DigitalPoint::DigitalPoint( Mutex* mutex              ,
                            GeminiDatabase& g3database,
                            POINT_TYPE pointType,
                            DigitalPoint::Config& config
                          ) :
                                AbstractPoint(mutex, g3database, pointType,
                                                config.ID,
                                                config.uniqueID,
                                                config.persistent),
                                m_conf(config),
                                chatter(config.chatterNo, config.chatterTime),
                                tickJob(NULL),
                                delayedJob(NULL),
                                /* TODO: pueyos_a - [DACTION] never publish raw value when invalid */
                                m_rawValue(RAWVALUE_INVALID)
{
    if( m_conf.invert && (pointType == POINT_TYPE_BINARY) )
    {
        //Apply single binary inversion by forcing the mapping
        m_conf.map[0] = 1;
        m_conf.map[1] = 0;
    }
    //Set max raw value for boundary check
    m_maxRawValue = (pointType == POINT_TYPE_BINARY)?  1 : 3;

    m_data = m_conf.map[0]; //initial (invalid) value
    m_preData = m_data;
    if(persistent)
    {
        /* Get initial value if persistent */
        PointDataUint8 eventData;
        lu_bool_t res;
        res = lucyMem::PersistentVPManager::getInstance().read(lucyMem::MemType_DIGITAL, uniqueID, eventData);
        if(res == LU_FALSE)
        {
            log.debug("%s - Previous value not available, using default", pointID.toString().c_str());
            DBGLOG_VPD_ERROR("Previous value not available, using default");
        }
        else
        {
            m_rawValue = (DigitalPointValue)eventData;
            m_data = eventData;
            m_preData = m_data;
            log.debug("%s Set Initial Value: %d", pointID.toString().c_str(), m_rawValue);
            DBGLOG_MEM_READ(pointID, eventData);
        }
    }
}

DigitalPoint::~DigitalPoint()
{
    DBG_DTOR();
    stopUpdate();

    /* Cancel common Timed Jobs */
    database.cancelJob(delayedJob); //Disposed of by the Job Manager
    database.cancelJob(tickJob);    //Disposed of by the Job Manager
}


GDB_ERROR DigitalPoint::init()
{
    //Default behaviour
    if(initialised)
    {
        return GDB_ERROR_INITIALIZED;
    }
    TimeManager::TimeStr initTime = TimeMgr.now();
    m_data.setTime(initTime);
    m_preData.setTime(initTime);
    /* Allocate a periodic Job mainly for chatter */
    if(m_conf.chatterNo != 0)   //configured for chatter
    {
        tickJob = setPeriodic(DigitalPointPeriodicTickInterval_ms);
        if(tickJob == NULL)
        {
            return GDB_ERROR_NOT_INITIALIZED;
        }
    }
    initialised = true;
    return GDB_ERROR_NONE;

}


GDB_ERROR DigitalPoint::getValue(PointData* valuePtr)
{
    if(valuePtr == NULL)
    {
        return GDB_ERROR_PARAM;
    }

    if(!initialised)
    {
        return GDB_ERROR_NOT_INITIALIZED;
    }

    LockingMutex lMutex(*mutex);
    PointDataUint8* value = (PointDataUint8*)(valuePtr);
    if(m_simulating)
    {
        *value = m_simulatedData;   //Get simulated flags and value
    }
    else
    {
        *value = m_data;            //Get latest flags and value
    }

    return GDB_ERROR_NONE;
}


GDB_ERROR DigitalPoint::getRAWValue(PointData* valuePtr)
{
    if(valuePtr == NULL)
    {
        return GDB_ERROR_PARAM;
    }

    if(!initialised)
    {
        return GDB_ERROR_NOT_INITIALIZED;
    }

    LockingMutex lMutex(*mutex);
    PointDataUint8* value = (PointDataUint8*)(valuePtr);
    *value = m_data;        //Assign latest flags
    //Get RAW value: if not valid yet, just report a default value
    *value = (m_rawValue == RAWVALUE_INVALID)? m_conf.map[0] : m_rawValue;

    return GDB_ERROR_NONE;
}


GDB_ERROR DigitalPoint::getRange(PointData* minValuePtr, PointData* maxValuePtr)
{
    if( (minValuePtr == NULL) || (maxValuePtr == NULL) )
    {
        return GDB_ERROR_PARAM;
    }

    PointDataUint8* minValue = (PointDataUint8*)(minValuePtr);
    PointDataUint8* maxValue = (PointDataUint8*)(maxValuePtr);

    switch (this->getType())
    {
        case POINT_TYPE_BINARY:
            *minValue = 0;
            *maxValue = 1;
            break;
        case POINT_TYPE_DBINARY:
        {
            /* max & min range of mapped values */
            lu_uint8_t minVal = m_conf.map[0];
            lu_uint8_t maxVal = m_conf.map[0];
            for (lu_uint32_t i = 1; i < 4; ++i)
            {
                minVal = LU_MIN(minVal, m_conf.map[i]);
                maxVal = LU_MAX(maxVal, m_conf.map[i]);
            }
            *minValue = minVal;
            *maxValue = maxVal;
            break;
        }
        default:
            return GDB_ERROR_NOPOINT;
            break;
    }
    return GDB_ERROR_NONE;
}


std::string DigitalPoint::getLabel(const PointData* valuePtr)
{
    PointDataUint8* valPtr = (PointDataUint8*)(valuePtr);
    lu_uint8_t value = *valPtr;
    return m_conf.labels.find(value);
}


void DigitalPoint::simulate(const PointData* valuePtr)
{
    PointDataUint8* pointPtr = (PointDataUint8*)(valuePtr);
    lu_uint8_t value = *pointPtr;
    m_simulatedData = value;
    m_simulatedData.setFlags(valuePtr->getFlags());
    m_simulating = true;
    //Force update with the simulated data
    publish(m_simulatedData, true);
}


void DigitalPoint::deSimulate()
{
    m_simulating = false;
    //Force update with the real data
    publish(m_data, true);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
GDB_ERROR DigitalPoint::setFlags(const PointFlags& newFlags, const TimeManager::TimeStr& timestamp)
{
    return AbstractPoint::setFlags(m_data, newFlags, timestamp);
}


PointData* DigitalPoint::getData()
{
    PointDataUint8* ret = new PointDataUint8; //create container, to be destroyed by the caller
    LockingMutex lMutex(*mutex);
    *ret = m_data;
    return ret;
}


bool DigitalPoint::flagChangeExceptChatter(const PointDataUint8& newData) const
{
    PointFlags exceptChatter = m_preData.getFlags();
    exceptChatter.setChatter(newData.getChatterFlag());
    return (exceptChatter != newData.getFlags());
}


void DigitalPoint::update(PointData* data, DATACHANGE dataChange)
{


    /* XXX: pueyos_a -  (to be removed) */
#if DEBUG_VPOINTS
    if(pointID == TEST_POINT)
            {
                log.debug("----");
            }
#endif

    //Be aware that default updatePoint() needs DATACHANGE_FORCE only
    updatePoint(*(dynamic_cast<PointDataUint8*>(data)), (dataChange & DATACHANGE_FORCE), true);
}


void DigitalPoint::updatePoint(PointDataUint8& newData,
                                const bool forced,
                                const bool fromOrigin)
{
    bool publishIt = false;
    PointDataUint8 eventData;   //data copy used to update observers
    DigitalPoint::DACTION res;

    if(!initialised)
    {
        return;
    }

    { //Lock global mutex
        LockingMutex lMutex(*mutex);



        /* XXX: pueyos_a -  (to be removed) */
#if DEBUG_VPOINTS
        if(pointID == TEST_POINT)
                {
                    log.debug("%s - updatePoint to %d %s %s",
                                    pointID.toString().c_str(),
                                    (DigitalPointValue)newData, newData.getFlags().toString().c_str(),
                                    (fromOrigin)? "orig" : "");
                }
#endif




/* XXX: pueyos_a -  (to be removed) */
#if 0 //DEBUG
{
        if(pointID == PointIdStr(0,55))
        {
            log.debug("-DBL-DLY-");
        }
        else if(pointID == PointIdStr(0,56))
        {
            log.debug("-DBL-now-");
        }
        else if(pointID == PointIdStr(2,3))
        {
            log.debug("--timeof-");
        }
}
#endif //DEBUG

        /* XXX: pueyos_a -  (to be removed) */
        PointDataUint8 tmpData(m_preData);  //data copy used to log changes



        /* XXX: pueyos_a -  (to be removed) */
#if DEBUG_VPOINTS
        if(pointID == TEST_POINT)
                {
                    log.debug("%s - updatePoint WAS %d %s %s",
                                    pointID.toString().c_str(),
                                    (DigitalPointValue)m_preData, m_preData.getFlags().toString().c_str(),
                                    getEdge(m_preData).c_str());
                }
#endif


        res = applyUpdate(newData, forced, fromOrigin);
        eventData = m_preData;


        /* XXX: pueyos_a -  (to be removed) */
#if DEBUG_VPOINTS
        if(pointID == TEST_POINT)
                {
                    log.debug("%s - updatePoint event %d %s %s %s (%s)",
                                    pointID.toString().c_str(),
                                    (DigitalPointValue)m_preData,
                                    m_preData.getFlags().toString().c_str(),
                                    getEdge(m_preData).c_str(),
                                    getAction(res).c_str(),
                                    (res!=DigitalPoint::DACTION_CANCEL_OR_PUB) ? "" :
                                        (delayedJob != NULL)? "CANCEL" : "PUB"
                                    );
                }
#endif


        switch (res)
        {
            case DigitalPoint::DACTION_DELAY:
            {


                /* XXX: pueyos_a -  (to be removed) */
#if DEBUG_VPOINTS
                if(delayedJob != NULL)
                {
                    log.debug("Point %s delayed change %d %s discarded",
                                pointID.toString().c_str(), (lu_uint8_t)tmpData,
                                tmpData.getFlags().toString().c_str());
                }
#endif

                //This final value has a delayed report
                lu_uint32_t delay_ms = m_delayList[(lu_uint8_t)m_preData].delay_ms;
                database.cancelJob(delayedJob);   //Cancel any previous delayed report
                delayedJob = new PointTimedJob(*this, eventData, delay_ms);
                database.addJob(delayedJob);


                /* XXX: pueyos_a -  (to be removed) */
#if DEBUG_VPOINTS
                log.debug("Point %s change to %d %s delayed for %d ms",
                            pointID.toString().c_str(), (lu_uint8_t)eventData,
                            eventData.getFlags().toString().c_str(), delay_ms);
#endif

                break;
            }
            case DigitalPoint::DACTION_CANCEL_OR_PUB:
            {
                bool pending = (delayedJob != NULL);

                database.cancelJob(delayedJob);   //Cancel any previous delayed report

                if(pending)
                {
                    //Hide (discard) point change if any pending
                    log.debug("Point %s delayed change %d %s discarded",
                                pointID.toString().c_str(), (lu_uint8_t)tmpData,
                                tmpData.getFlags().toString().c_str());
                }
                else
                {
                    //Not for cancelling, so publish it
                    publishIt = true;
                }

                break;
            }
            case DigitalPoint::DACTION_PUBLISH:
            {
                //Publish now
                database.cancelJob(delayedJob);   //Cancel any previous delayed report
                publishIt = true;
                break;
            }
            default:

                //No change
                log.debug("Point %s no change %d %s",
                            pointID.toString().c_str(), (lu_uint8_t)m_preData,
                            m_preData.getFlags().toString().c_str());

                break;
        }
    }//End of Mutex section
    if(publishIt)
    {
        publish(eventData, forced);
    }
}


DigitalPoint::DACTION DigitalPoint::applyUpdate(PointDataUint8& newData,
                                                const bool forced,
                                                const bool originalSource)
{
    DACTION ret = DACTION_NONE;
    bool valueChanged = false;  //Condition for a change in the point value
    bool oldChatter;            //Previous chatter flag value
    bool newChatter;            //incoming chatter flag value
    lu_uint8_t rawValue;        //incoming value (temporal storage for pre-check)
    bool iniFlagRaw = newData.getInitialFlag(); //Incoming flags non-processed

    TimeManager::TimeStr timestamp;
    newData.getTime(timestamp);

    /* Identify possible value change (pre-check) */
    rawValue = newData;
    if(rawValue > m_maxRawValue)
    {
        //incoming value is invalid
        log.debug("%s Invalid incoming value %d not in [0,%d]",
                        pointID.toString().c_str(), rawValue, m_maxRawValue);
        return DACTION_NONE;
    }
    valueChanged =
            forced ||                                       //forced change
            (m_preData.getInitialFlag() == iniFlagRaw) ||   //was initial. e.g. 0[initial] to 0[valid]
            (m_rawValue == RAWVALUE_INVALID);

    if(!valueChanged)
    {
        //m_rawValue is not RAWVALUE_INVALID so we can check it
        valueChanged |=
            //Value change (m_rawValue != rawValue) must not be checked -- use
            //mapped value instead:
            (m_conf.map[m_rawValue] != m_conf.map[rawValue]); //mapped value change
    }

    /* Process flag change */
    if( flagChangeExceptChatter(newData) )
    {
        m_preData.setFlags(newData.getFlags());
        ret = DACTION_PUBLISH;      //Flag change implies publishing now
    }

    /* Check chatter flag change */
    oldChatter = m_preData.getChatterFlag();
    newChatter = newData.getChatterFlag();
    if(originalSource)
    {
        chatter.setLock(newChatter);
    }


    /* XXX: pueyos_a -  (to be removed) */
#if DEBUG_VPOINTS
    if(pointID == TEST_POINT)
            {
                log.debug("%s - applyupdate %d %s raw=%d[%d] -> %d[%d]",
                                pointID.toString().c_str(),
                                (DigitalPointValue)newData,
                                newData.getFlags().toString().c_str(),
                                m_rawValue, m_conf.map[m_rawValue],
                                rawValue, m_conf.map[rawValue]);
            }
#endif



    /* Check if the value change affects this point */
    m_rawValue = rawValue;
    if(valueChanged)
    {
        //Apply inversion or custom value before comparing (instead of XOR it)
        lu_uint8_t value = m_conf.map[rawValue];

        /* Store point change */
        lu_uint8_t oldValue = m_preData;
        if(value != oldValue)
        {
            m_preData.setEdge((oldValue < value)? EDGE_TYPE_POSITIVE : EDGE_TYPE_NEGATIVE);
        }
        else
        {
            m_preData.setEdge(EDGE_TYPE_NONE); //no value change
        }
        m_preData = value;
        m_preData.setInitialFlag(newData.getInitialFlag());
        m_preData.setInvalidFlag(newData.getInvalidFlag());
        m_preData.setTime(timestamp);

        /* FIXME: pueyos_a - [Bug #3508] uses relative time instead of the point timestamp
         * in order to use the current time coming from tickEvent(), that is
         * wrong in case of receiving a group of point updates in a package
         * (such as coming from a DNP3 slave field device) -- the point is going
         * to be set as "in chatter", even when the updates come with different
         * enough time stamps */
        newChatter = chatter.newEvent(timestamp.relatime.tv_sec);
        //NOTE: newChatter is reported as the original chatter if has been already
        // locked, but still obtains the calculated chatter status internally.
    }

    /* Apply chatter flag change */
    if(newChatter != oldChatter)
    {
        m_preData.setChatterFlag(newChatter);
        ret = DACTION_PUBLISH;          //Flag change implies publishing now
    }

    /* Delayed report */
    if( (ret != DACTION_PUBLISH) &&         //No flag change (otherwise publish now)
        (!m_preData.getChatterFlag()) &&    //Not already in chatter
        valueChanged                        //Change in value
        )
    {
        lu_uint8_t valueIdx = m_preData;
        lu_uint32_t delay_ms = 0;
        if(valueIdx < m_delayList.size())   //valid delay index
        {
            delay_ms = m_delayList[valueIdx].delay_ms;
        }
        if( m_preData.getFlags().isActive() &&          //Valid flags
            (m_preData.getEdge() != EDGE_TYPE_NONE) &&  //Value change
            (delay_ms != 0)                             //Applicable delay
            )
        {
            //This final value has a delayed report
            ret = DACTION_DELAY;         //do not publish yet
        }
        else
        {
            if(!valueChanged)
            {
                ret = DACTION_CANCEL_OR_PUB; //Cancel if already delaying - otherwise publish it
            }
            else
            {
                ret = DACTION_PUBLISH;
            }
        }
    }



    /* XXX: pueyos_a -  (to be removed) */
#if DEBUG_VPOINTS
    if(pointID == TEST_POINT)
            {
                log.debug("%s - applyupdate valueChanged=%d %d %s %s %s",
                                pointID.toString().c_str(),
                                valueChanged,
                                (DigitalPointValue)m_preData, m_preData.getFlags().toString().c_str(),
                                getAction(ret).c_str(),
                                getEdge(m_preData).c_str());
            }
#endif



    return ret;
}


void DigitalPoint::tickEvent(TimeManager::TimeStr& timeStamp)
{
    /* FIXME: pueyos_a - [Bug #3508] point chatter uses relative time instead of the
     * point timestamp in order to use the current time coming from
     * tickEvent(), that is wrong in case of receiving a group of point
     * updates in a package (such as coming from a DNP3 slave field device) --
     * the point is going to be set as "in chatter", even when the updates
     * come with different enough time stamps */

    PointDataUint8 newData;

    if(!initialised)
    {
        return;
    }

    /* Lock global mutex */
    {
        LockingMutex lMutex(*mutex);    //Protect access to m_predata
        newData = m_preData;
    }

    /* Update the chatter manager */
    bool oldChatter = chatter.inChatter();
    if (chatter.update(timeStamp.relatime.tv_sec) != oldChatter)
    {
        /* Report chatter change */
        newData.setChatterFlag(chatter.inChatter());
        newData.setEdge(EDGE_TYPE_NONE);
        newData.setTime(timeStamp);


        /* XXX: pueyos_a -  (to be removed) */
#if DEBUG_VPOINTS
        log.debug("Point %s tick %s chatter", pointID.toString().c_str(),
                        (newData.getChatterFlag())? "IN" : "OUT");
#endif

        updatePoint(newData, false, false);
    }
}


void DigitalPoint::publish(const PointDataUint8& newData, const bool forced)
{
    bool publishIt = false;
    PointDataUint8 eventData;   //data copy used to update observers
    if(!initialised)
    {
        return;
    }

    { //Lock global mutex
        LockingMutex lMutex(*mutex);

        /* Unlink Timed Job (will be deleted by the Scheduler) */
        delayedJob = NULL;  //Executing: not able to be cancelled anymore

        //Publish preparation
        if( forced ||
            ( (lu_uint8_t)m_data != (lu_uint8_t)m_preData ) ||
            ( !m_data.compareFlags(m_preData.getFlags()) )
          )
        {
            m_preData = newData;
            m_data = m_preData;
            publishIt = true;
        }

        if(m_simulating)
        {
            eventData = m_simulatedData;
            publishIt = true;
        }
        else
        {
            eventData = m_data;
        }
    }//End of Mutex section

    if(publishIt)
    {
        if(persistent)
        {
            /* Store persistent value */
            lu_bool_t res;
            res = lucyMem::PersistentVPManager::getInstance().write(lucyMem::MemType_DIGITAL, uniqueID, eventData);
            if(res == LU_FALSE)
            {
                log.error("%s - Failed to store persistent value!", pointID.toString().c_str());
                DBGLOG_VPD_ERROR("- Failed to store persistent value!");
            }
        }
        /* XXX: pueyos_a -  (to be removed) */
#if DEBUG_VPOINTS
        log.debug("Point %s updated now to %d %s",
                    pointID.toString().c_str(), (lu_uint8_t)eventData,
                    eventData.getFlags().toString().c_str());
#endif

        updateObservers(&eventData);
    }
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
