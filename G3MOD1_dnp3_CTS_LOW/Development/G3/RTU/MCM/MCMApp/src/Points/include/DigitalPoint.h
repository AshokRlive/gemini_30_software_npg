/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Virtual digital point interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_77028E04_B142_4bae_9804_F5219B3C491C__INCLUDED_)
#define EA_77028E04_B142_4bae_9804_F5219B3C491C__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "AbstractPoint.h"
#include "ChatterMgr.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Virtual digital point common container.
 *
 * This is a base class for all specific implementations of a Digital Point.
 * Common data and logic for all digital points is implemented here.
 * All the pure virtual methods need to be implemented in the children as this
 * is only a container.
 */
class DigitalPoint : public AbstractPoint
{
public:
    /**
     * \brief Common Digital Point Configuration
     */
    struct Config : public AbstractPoint::BaseConfig<DigitalPointValue>
    {
    public:
        /**
         * \brief Delayed report configuration structure
         */
        struct Delay
        {
        public:
            lu_uint32_t eventVal;   //Final value of the report to be delayed
            lu_uint32_t delay_ms;   //Delayed report time, in ms (0 for not delayed)
        public:
            Delay() : delay_ms(0)
            {};
            Delay(const lu_uint32_t eventValue) : eventVal(eventValue), delay_ms(0)
            {};
        };

        /**
         * \brief Delayed report list (each final value has a delay) type
         */
        typedef std::vector<Delay> DelayList;

    public:
        PointIdStr ID;
        lu_bool_t invert;       //Point value inversion; not used in Double Binary points
        lu_uint32_t chatterNo;  //Number of samples to be in chatter
        lu_uint32_t chatterTime;//Amount of time to consider being in chatter
        lu_uint8_t map[4];      //Value map for custom values, sorted by value
        DelayList delay;        //Delayed report UNSORTED list (one entry per value

    public:
        Config() { init(); };
        /**
         * \brief Reset structure to default values
         */
        void init();
        /**
         * \brief Add an entry to the delay list
         *
         * \param entry Delay config entry for the list
         */
        virtual void addDelay(const Delay entry) { delay.push_back(entry); };
        /**
         * \brief Obtain a sorted delay list
         *
         * Since the delay list can be added unsorted, this allows to get another
         * already sorted out, discarding the entries not needed
         *
         * \param pointType Type of point, being single or double binary.
         *
         * \return Sorted list
         */
        virtual DelayList sortDelay(const POINT_TYPE pointType);
    };

public:
    /**
     * \brief Custom constructor
     *
     * This is a pure virtual base class so it should not be possible to
     * instantiate it.
     *
     * \param mutex Global access mutex
     * \param database Gemini database reference
     * \param pointType Type of the point that it is being created
     * \param config Point configuration
     */
    DigitalPoint( Mutex *mutex              ,
                  GeminiDatabase& database  ,
                  POINT_TYPE pointType,
                  DigitalPoint::Config &config
                );
    virtual ~DigitalPoint();

    /* == Inherited from IPoint == */
    virtual GDB_ERROR init();
    virtual PointFlags getFlags() { return m_data.getFlags(); };
    virtual GDB_ERROR getValue(PointData *valuePtr);
    virtual GDB_ERROR getRAWValue(PointData* valuePtr);
    virtual GDB_ERROR getRange(PointData* minValuePtr, PointData* maxValuePtr);
    virtual POINT_QUALITY getQuality() {return m_data.getQuality(); };
    virtual std::string getLabel(const PointData* valuePtr);
    virtual void simulate(const PointData* valuePtr);
    virtual void deSimulate();

protected:
    /**
     * This class schedules a point update.
     */
    class PointTimedJob : public TimedJob
    {
    public:
        /**
         * \brief Custom constructor
         *
         * \param pointObj Point to be reported to
         * \param pointData New value/status data change to be scheduled
         * \param timeout Timer value in ms
         */
        PointTimedJob(DigitalPoint& pointObj,
                      PointDataUint8& pointData,
                      const lu_uint32_t timeout) :
                                              TimedJob(timeout, 0),
                                              m_point(pointObj),
                                              m_pointData(pointData)
        {};
        virtual ~PointTimedJob() {};

        /**
         * \brief Method executed by the job scheduler
         *
         * \param timePtr Current time
         */
        virtual void job(TimeManager::TimeStr* timePtr)
        {
            LU_UNUSED(timePtr);
            m_point.publish(m_pointData, false);
        }

    private:
        DigitalPoint& m_point;
        PointDataUint8 m_pointData;
    };

protected:
    /* == Inherited from AbstractPoint == */
    virtual GDB_ERROR setFlags(const PointFlags& newFlags, const TimeManager::TimeStr& timestamp);
    virtual PointData* getData();
    virtual void tickEvent(TimeManager::TimeStr& timeStamp);
    virtual void update(PointData* data, DATACHANGE dataChange = DATACHANGE_BOTH);

    /**
     * \brief Apply the point update
     *
     * Applies changes to the point, including value, flag, and delay processing
     *
     * \param newData Incoming data change
     * \param dataChange Change intended in the point data
     * \param originalSource Source of the change: from original or calculated
     */
    virtual void updatePoint(PointDataUint8& newData,
                             const bool forced,
                             const bool originalSource);

    /**
     * \brief Publish a change in the point to the observers
     *
     * \param newData Data to publish. Internal variables will be
     */
    virtual void publish(const PointDataUint8& newData, const bool forced);

private:

/* XXX: pueyos_a -  (to be removed) */
public:

    /**
     * \brief possible actions to take after processing the point
     *
     */
    typedef enum
    {
        DACTION_NONE,            //No action needed
        DACTION_PUBLISH,         //Publish point change
        DACTION_DELAY,           //Delay point change: do not publish it yet
        DACTION_CANCEL_OR_PUB    //Cancel any pending delayed change; otherwise publish it now
    } DACTION;

private:
    /**
     * \brief Checks if all the flags changed, except for chatter flag
     *
     * \param newData Point data, containing the flags
     *
     * \return Result of comparison: true if all flags except chatter are the same
     */
    virtual bool flagChangeExceptChatter(const PointDataUint8& newData) const;

    /**
     * \brief Update point value and flags by applying all configured calculations
     *
     * Applies all the checks and calculations needed for the point.
     * Point object's pre-data and raw value might be modified.
     *
     * \param newData Incoming point's data and flags
     * \param forced Change is forced and has to be published even if no data changes
     * \param originalSource Incoming flags come from the original source
     *
     * \return Action needed to be taken after point processing
     */
    virtual DACTION applyUpdate(PointDataUint8& newData,
                                const bool forced,
                                const bool originalSource = true);

protected:
    /* Point configuration */
    DigitalPoint::Config m_conf;    //Digital Point general configuration
    ChatterMgr chatter;             //Manager for value chatter detection
    Config::DelayList m_delayList;  //SORTED delay list

    /* Jobs */
    PointPeriodicJob* tickJob;      //Periodic job for tick event
    PointTimedJob* delayedJob;      //Latest delayed job generated (for cancelling)

    /* Point status */
    PointDataUint8 m_data;          //Stored data (last value & flags reported outside)
    PointDataUint8 m_preData;       //Temporal pre-processed data before final filtering
    lu_uint8_t m_rawValue;          //Last received value, not inverted and unprocessed
    lu_uint8_t m_maxRawValue;       //Max raw value, for checking boundaries
    PointDataUint8 m_simulatedData; //Simulated (forced) point value & flags
};


#endif // !defined(EA_77028E04_B142_4bae_9804_F5219B3C491C__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
