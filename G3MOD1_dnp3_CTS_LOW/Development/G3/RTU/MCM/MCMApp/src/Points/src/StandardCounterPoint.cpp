/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: StandardCounterPoint.cpp 14 Jul 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/Points/src/StandardCounterPoint.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Standard (Virtual Point source) Counter type virtual point.
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 14 Jul 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14 Jul 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "StandardCounterPoint.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
StandardCounterPoint::StandardCounterPoint(Mutex* gMutex,
                                           GeminiDatabase& g3database,
                                           StandardCounterPoint::StdConfig& conf
                                           ) :
                                               CounterPoint(gMutex, g3database, conf),
                                               config(conf),
                                               IDRef(conf.IDRef),
                                               event(g3database, conf.IDRef, conf.eventCfg)
{}


StandardCounterPoint::~StandardCounterPoint()
{}


GDB_ERROR StandardCounterPoint::init()
{
    if(initialised)
    {
        return GDB_ERROR_INITIALIZED;
    }
    GDB_ERROR ret = event.attach(this);
    if(ret == GDB_ERROR_NONE)
    {
        ret = event.init();
    }
    if(ret != GDB_ERROR_NONE)
    {
        log.error("Unable to initialise Counter Point %s, error: %s",
                  pointID.toString().c_str(), GDB_ERROR_ToSTRING(ret)
                  );
    }
    else
    {
        ret = CounterPoint::init();   //Call the parent init
    }
    return ret;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
void StandardCounterPoint::updateTime(TimeManager::TimeStr& currentTime)
{
    if( (currentTime.time.tv_sec % config.freezeTime) == 0 )
    {
        /* Schedule an update to command the point to freeze the value: only timestamp is used */
        GDB_ERROR ret;
        PointDataCounter32 pointData;
        pointData.setTime(currentTime);
        ret = scheduleUpdate(*this, pointData, DATACHANGE_FREEZE);
        if(ret != GDB_ERROR_NONE)
        {
            log.error("%s: Point (%s) - error: %s",
                        __AT__, pointID.toString().c_str(), GDB_ERROR_ToSTRING(ret)
                      );
        }
    }
}


void StandardCounterPoint::update(PointData* newData, DATACHANGE dataChange)
{
    updateCounterPoint(newData, dataChange);
}


void StandardCounterPoint::update(PointIdStr pointID, PointData* pointData)
{
    LU_UNUSED(pointID);

    /* Incoming data from the observed Virtual Point (from Digital Change Event object) */

    GDB_ERROR ret;
    PointDataCounter32 newData;     //Counter Point's new data to be scheduled

    /* NOTE: pointData value not used because the report itself implies an
     * increase of 1 when the EDGE flag is EDGE_FLAG_POSITIVE
     *
     */
    DATACHANGE change = DATACHANGE_FLAGS;
    if(pointData->getEdge() == EDGE_TYPE_POSITIVE)
    {
        change = DATACHANGE_BOTH;
    }
    newData = *((PointDataCounter32*)pointData);    //Copy flags and time stamp
    ret = scheduleUpdate(*this, newData, change);
    if(ret != GDB_ERROR_NONE)
    {
        log.error("%s: Control Logic Counter Point %s, error: %s",
                    __AT__, pointID.toString().c_str(), GDB_ERROR_ToSTRING(ret)
                  );
    }
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
