/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Dummy Factory class for virtual points
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   18/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "DummyPointFactory.h"
#include "BinaryDigitalPoint.h"
#include "DoubleBinaryDigitalPoint.h"
#include "StandardAnaloguePoint.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


typedef struct
{
    PointIdStr id;
    lu_bool_t invert;
    lu_uint32_t chatterNo;
    lu_uint32_t chatterTime;
    MODULE module;
    MODULE_ID moduleID;
    lu_uint32_t channel;
}dummyConfigBinary;

typedef struct
{
    PointIdStr id;
    lu_bool_t invert;
    lu_uint32_t chatterNo;
    lu_uint32_t chatterTime;
    lu_uint32_t debounce;
    MODULE module0;
    MODULE_ID moduleID0;
    lu_uint32_t channel0;
    MODULE module1;
    MODULE_ID moduleID1;
    lu_uint32_t channel1;
}dummyConfigDBinary;

typedef struct
{
    PointIdStr id;
    struct
    {
        AFILTER_TYPE filterType;
        lu_float32_t nominal;
        lu_float32_t deadband;
        lu_float32_t lowerLimit;
        lu_float32_t upperLimit;
        lu_uint8_t   hysteresis;
    }AFilter;
    lu_float32_t  scale     ;
    lu_float32_t  offset    ;
    lu_float32_t  overflow  ;
    lu_float32_t  hysteresis;
    MODULE module;
    MODULE_ID moduleID;
    lu_uint32_t channel;
}dummyConfigStdAnalogue;

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


#if 1
static dummyConfigBinary dummyConfigBinaryArray[] =
{
    /*  group  id    invert chatterNo chatterTime module       moduleID     channel                    */
#if 0
    {  {0    , 0 } , 0    , 10      , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_LS_OVERVOLTAGE},
    {  {0    , 1 } , 0    , 10      , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_LS_UNDERVOLTAGE},
    {  {0    , 2 } , 0    , 10      , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_LS_SELECT_OPERATE_TIMEOUT},
    {  {0    , 3 } , 0    , 10      , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_MS_SELECT_OPERATE_TIMEOUT},
    {  {0    , 4 } , 0    , 10      , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_MS_MOTOR_ON_TIMEOUT},
    {  {0    , 5 } , 0    , 10      , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_MS_RELAY_ON},
    {  {0    , 6 } , 0    , 10      , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_MS_OVERVOLTAGE},
    {  {0    , 7 } , 0    , 10      , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_MS_OVERCURRENT},
    {  {0    , 8 } , 0    , 10      , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_MS_LOCAL_REMOTE_FAULT},
    {  {0    , 9 } , 0    , 10      , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_AUX_OVERVOLTAGE},
    {  {0    , 10} , 0    , 10      , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_AUX_UNDERVOLTAGE},
    {  {0    , 11} , 0    , 10      , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_AC_SELECT_OPERATE_TIMEOUT},
    {  {0    , 12} , 0    , 10      , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_AC_OFF},//
    {  {0    , 13} , 0    , 10      , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_AC_OVERVOLTAGE},
    {  {0    , 14} , 0    , 10      , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_AC_UNDERVOLTAGE},
    {  {0    , 15} , 0    , 10      , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_CH_FAULT},
    {  {0    , 16} , 0    , 10      , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_CH_ON},
    {  {0    , 17} , 0    , 10      , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_BT_COMMS_FAIL},
    {  {0    , 18} , 0    , 10      , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_BT_DISCONNECTED},
    {  {0    , 19} , 0    , 10      , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_BT_OVER_TEMPERATURE},
    {  {0    , 20} , 0    , 10      , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_BT_CHARGING_OVERRIDE},
    {  {0    , 21} , 0    , 10      , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_BT_TEST_FAIL},
    {  {0    , 22} , 0    , 10      , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_BT_LOW},
    {  {0    , 23} , 0    , 10      , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_BT_FAULT},
    {  {0    , 24} , 0    , 10      , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_FAN_ON},
    {  {0    , 25} , 0    , 10      , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_FAN_FAULT},
    {  {0    , 26} , 0    , 10      , 30        , MODULE_MCM , MODULE_ID_0, MCM_CH_DINPUT_DOOR_OPEN}
#else
    {  {0    , 0 } , 0    , 0       , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_LS_OVERVOLTAGE},
    {  {0    , 1 } , 0    , 0       , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_LS_UNDERVOLTAGE},
    {  {0    , 2 } , 0    , 0       , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_LS_SELECT_OPERATE_TIMEOUT},
    {  {0    , 3 } , 0    , 0       , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_MS_SELECT_OPERATE_TIMEOUT},
    {  {0    , 4 } , 0    , 0       , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_MS_MOTOR_ON_TIMEOUT},
    {  {0    , 5 } , 0    , 0       , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_MS_RELAY_ON},
    {  {0    , 6 } , 0    , 0       , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_MS_OVERVOLTAGE},
    {  {0    , 7 } , 0    , 0       , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_MS_OVERCURRENT},
    {  {0    , 8 } , 0    , 0       , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_MS_LOCAL_REMOTE_FAULT},
    {  {0    , 9 } , 0    , 0       , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_AUX_OVERVOLTAGE},
    {  {0    , 10} , 0    , 0       , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_AUX_UNDERVOLTAGE},
    {  {0    , 11} , 0    , 0       , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_AC_SELECT_OPERATE_TIMEOUT},
    {  {0    , 12} , 0    , 0       , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_AC_OFF},//
    {  {0    , 13} , 0    , 0       , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_AC_OVERVOLTAGE},
    {  {0    , 14} , 0    , 0       , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_AC_UNDERVOLTAGE},
    {  {0    , 15} , 0    , 0       , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_CH_FAULT},
    {  {0    , 16} , 0    , 0       , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_CH_ON},
    {  {0    , 17} , 0    , 0       , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_BT_COMMS_FAIL},
    {  {0    , 18} , 0    , 0       , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_BT_DISCONNECTED},
    {  {0    , 19} , 0    , 0       , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_BT_OVER_TEMPERATURE},
    {  {0    , 20} , 0    , 0       , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_CH_SYSTEM_RUNNING},
    {  {0    , 21} , 0    , 0       , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_BT_TEST_FAIL},
    {  {0    , 22} , 0    , 0       , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_BT_LOW},
    {  {0    , 23} , 0    , 0       , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_BT_FAULT},
    {  {0    , 24} , 0    , 0       , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_FAN_ON},
    {  {0    , 25} , 0    , 0       , 30        , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_FAN_FAULT},
    {  {0    , 26} , 0    , 0       , 30        , MODULE_MCM , MODULE_ID_0, MCM_CH_DINPUT_DOOR_SWITCH}
#endif
};
static const lu_uint32_t dummyConfigBinaryArraySize = sizeof(dummyConfigBinaryArray) / sizeof(dummyConfigBinary);
#else
static dummyConfigBinary dummyConfigBinaryArray[1];
static const lu_uint32_t dummyConfigBinaryArraySize = 0;
#endif

#if 1
static dummyConfigDBinary dummyConfigDBinaryArray[] =
{
    /*  group  id   invert chatterNo chatterTime debounce  module0      moduleID0    channel0                                   module1      moduleID1    channel1               */
    {  {0    , 27} , 0    , 10      , 30        , 100     , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_LS_OVERVOLTAGE             , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_LS_UNDERVOLTAGE },
    {  {0    , 28} , 0    , 10      , 30        , 0       , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_AC_OFF                     , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_FAN_FAULT       },
    {  {0    , 29} , 0    , 10      , 30        , 1000    , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_AC_OFF                     , MODULE_PSM , MODULE_ID_0, PSM_CH_DINPUT_FAN_FAULT       }
};
static const lu_uint32_t dummyConfigDBinaryArraySize = sizeof(dummyConfigDBinaryArray) / sizeof(dummyConfigDBinary);
#else
static dummyConfigDBinary dummyConfigDBinaryArray[1];
static const lu_uint32_t dummyConfigDBinaryArraySize = 0;
#endif

#if 1
static dummyConfigStdAnalogue dummyConfigStdAnalogueArray[] =
{
    /*  group  id    filterType                      nominal deadband  lowerLimit  upperLimit hysteresis    scale offset  overflow  hysteresis  module       moduleID     channel*/
    {  {0    , 30} , {AFILTER_TYPE_DEADBAND         , 1000  , 60      , 0        , 0         , 0          }, 1   , 0    , 3500     , 15        , MODULE_PSM , MODULE_ID_0, PSM_CH_AINPUT_MAIN_DC_CURRENT},
    {  {0    , 31} , {AFILTER_TYPE_LOWER_UPPER_LIMIT, 0     , 0       , 1000     , 2000      , 10         }, 1   , 0    , 3600     , 15        , MODULE_PSM , MODULE_ID_0, PSM_CH_AINPUT_LS_CURRENT},
    {  {0    , 32} , {AFILTER_TYPE_UPPER_LIMIT      , 0     , 0       , 0        , 2500      , 20         }, 1   , 0    , 3700     , 15        , MODULE_PSM , MODULE_ID_0, PSM_CH_AINPUT_MS_VOLTAGE},
    {  {0    , 33} , {AFILTER_TYPE_DEADBAND         , 3000  , 60      , 0        , 0         , 0          }, 1   , 0    , 0        , 0         , MODULE_PSM , MODULE_ID_0, PSM_CH_AINPUT_MS_CURRENT},
    {  {0    , 34} , {AFILTER_TYPE_DEADBAND         , 0     , 60      , 0        , 0         , 0          }, 1   , 0    , 3500     , 15        , MODULE_PSM , MODULE_ID_0, PSM_CH_AINPUT_MS_PEAK_HOLD_CURRENT},
    {  {0    , 35} , {AFILTER_TYPE_LOWER_UPPER_LIMIT, 0     , 0       , 1000     , 2000      , 10         }, 1   , 0    , 3600     , 15        , MODULE_PSM , MODULE_ID_0, PSM_CH_AINPUT_AC_VOLTAGE},
    {  {0    , 36} , {AFILTER_TYPE_UPPER_LIMIT      , 0     , 0       , 0        , 2500      , 20         }, 1   , 0    , 3700     , 15        , MODULE_PSM , MODULE_ID_0, PSM_CH_AINPUT_AC_LOWER_TRIP_LEVEL},
    {  {0    , 37} , {AFILTER_TYPE_DEADBAND         , 0     , 60      , 0        , 0         , 0          }, 1   , 0    , 0        , 0         , MODULE_PSM , MODULE_ID_0, PSM_CH_AINPUT_AC_UPPER_TRIP_LEVEL},
    {  {0    , 38} , {AFILTER_TYPE_DEADBAND         , 0     , 60      , 0        , 0         , 0          }, 1   , 0    , 3500     , 15        , MODULE_PSM , MODULE_ID_0, PSM_CH_AINPUT_COMMS_2_AUX_VOLTAGE},
    {  {0    , 39} , {AFILTER_TYPE_LOWER_UPPER_LIMIT, 0     , 0       , 1000     , 2000      , 10         }, 1   , 0    , 3600     , 15        , MODULE_PSM , MODULE_ID_0, PSM_CH_AINPUT_CH_VOLTAGE},
    {  {0    , 40} , {AFILTER_TYPE_UPPER_LIMIT      , 0     , 0       , 0        , 2500      , 20         }, 1   , 0    , 3700     , 15        , MODULE_PSM , MODULE_ID_0, PSM_CH_AINPUT_CH_CHARGING_CURRENT},
    {  {0    , 41} , {AFILTER_TYPE_DEADBAND         , 0     , 60      , 0        , 0         , 0          }, 0.1 , 0    , 0        , 0         , MODULE_PSM , MODULE_ID_0, PSM_CH_AINPUT_BT_VOLTAGE},
    {  {0    , 42} , {AFILTER_TYPE_DEADBAND         , 0     , 60      , 0        , 0         , 0          }, 1   , 0    , 3500     , 15        , MODULE_PSM , MODULE_ID_0, PSM_CH_AINPUT_DRAIN_CURRENT},
    {  {0    , 43} , {AFILTER_TYPE_LOWER_UPPER_LIMIT, 0     , 0       , 1000     , 2000      , 10         }, 1   , 0    , 3600     , 15        , MODULE_PSM , MODULE_ID_0, PSM_CH_AINPUT_ON_ELAPSED_TIME},
    {  {0    , 44} , {AFILTER_TYPE_UPPER_LIMIT      , 0     , 0       , 0        , 2500      , 20         }, 1   , 0    , 3700     , 15        , MODULE_PSM , MODULE_ID_0, PSM_CH_AINPUT_TS_INTERNAL_TEMPERATURE},
    {  {0    , 45} , {AFILTER_TYPE_DEADBAND         , 0     , 60      , 0        , 0         , 0          }, 1   , 0    , 0        , 0         , MODULE_PSM , MODULE_ID_0, PSM_CH_AINPUT_TS_BATTERY_TEMPERATURE}

};
static const lu_uint32_t dummyConfigStdAnalogueArraySize = sizeof(dummyConfigStdAnalogueArray) / sizeof(dummyConfigStdAnalogue);
#else
static dummyConfigStdAnalogue dummyConfigStdAnalogueArray[1];
static const lu_uint32_t dummyConfigStdAnalogueArraySize = 0;
#endif

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */




DummyPointFactory::DummyPointFactory(IIOModuleManager &MManager) :
                                                              MManager(MManager)
{

}



DummyPointFactory::~DummyPointFactory()
{

}

lu_uint16_t DummyPointFactory::getPointNumber()
{
    return( dummyConfigBinaryArraySize     +
            dummyConfigDBinaryArraySize    +
            dummyConfigStdAnalogueArraySize
          );
}

IPoint *DummyPointFactory::newPoint( Mutex *mutex,
                                     GeminiDatabase *database,
                                     PointIdStr pointID
                                   )
{
    IPoint *pointPtr = NULL;
    lu_uint16_t i;
    IChannel *chPtr0;
    IChannel *chPtr1;

    /* Search for the point */
    for(i = 0; i < dummyConfigBinaryArraySize; i++)
    {
        BinaryDigitalPointConfig BDConfig;
        dummyConfigBinary *dummyConfigBinaryPtr;

        dummyConfigBinaryPtr = &dummyConfigBinaryArray[i];

        if(dummyConfigBinaryPtr->id == pointID)
        {
            chPtr0 = MManager.getModule(dummyConfigBinaryPtr->module, dummyConfigBinaryPtr->moduleID)->getChannel(CHANNEL_TYPE_DINPUT, dummyConfigBinaryPtr->channel);
            if(chPtr0 != NULL)
            {
                BDConfig.ID = dummyConfigBinaryPtr->id;
                BDConfig.chatterNo = dummyConfigBinaryPtr->chatterNo;
                BDConfig.chatterTime = dummyConfigBinaryPtr->chatterTime;
                BDConfig.invert = dummyConfigBinaryPtr->invert;
                BDConfig.channelRef = chPtr0;
                pointPtr = new BinaryDigitalPoint(mutex, database, BDConfig);

                return pointPtr;
            }
        }
    }


    for(i = 0; i < dummyConfigDBinaryArraySize; i++)
    {
        DoubleBinaryDigitalPointConfig DBDConfig;
        dummyConfigDBinary *dummyConfigDBinaryPtr;

        dummyConfigDBinaryPtr = &dummyConfigDBinaryArray[i];
        if(dummyConfigDBinaryPtr->id == pointID)
        {
            chPtr0 = MManager.getModule(dummyConfigDBinaryPtr->module0, dummyConfigDBinaryPtr->moduleID0)->getChannel(CHANNEL_TYPE_DINPUT, dummyConfigDBinaryPtr->channel0);
            chPtr1 = MManager.getModule(dummyConfigDBinaryPtr->module1, dummyConfigDBinaryPtr->moduleID1)->getChannel(CHANNEL_TYPE_DINPUT, dummyConfigDBinaryPtr->channel1);
            if( (chPtr0 != NULL) && (chPtr1 != NULL) )
            {
                DBDConfig.ID = dummyConfigDBinaryPtr->id;
                DBDConfig.chatterNo = dummyConfigDBinaryPtr->chatterNo;
                DBDConfig.chatterTime = dummyConfigDBinaryPtr->chatterTime;
                DBDConfig.invert = dummyConfigDBinaryPtr->invert;
                DBDConfig.debounce = dummyConfigDBinaryPtr->debounce;
                DBDConfig.channelRef[DBP_CHANNEL_IDX_0] = chPtr0;
                DBDConfig.channelRef[DBP_CHANNEL_IDX_1] = chPtr1;
                pointPtr = new DoubleBinaryDigitalPoint(mutex, database, DBDConfig);

                return pointPtr;
            }
        }
    }

    for(i = 0; i < dummyConfigStdAnalogueArraySize; i++)
    {
        StdAnaloguePointConfig StdAPConfig;
        dummyConfigStdAnalogue *dummyConfigStdAnaloguePtr;

        dummyConfigStdAnaloguePtr = &dummyConfigStdAnalogueArray[i];

        if(dummyConfigStdAnaloguePtr->id == pointID)
        {
            chPtr0 = MManager.getModule(dummyConfigStdAnaloguePtr->module, dummyConfigStdAnaloguePtr->moduleID)->getChannel(CHANNEL_TYPE_AINPUT, dummyConfigStdAnaloguePtr->channel);
            if(chPtr0 != NULL)
            {
                StdAPConfig.ID = dummyConfigStdAnaloguePtr->id;
                StdAPConfig.filter.filterType = dummyConfigStdAnaloguePtr->AFilter.filterType;
                switch(StdAPConfig.filter.filterType)
                {
                    case AFILTER_TYPE_DEADBAND:
                        StdAPConfig.filter.filterConf.DBFilter.deadband = dummyConfigStdAnaloguePtr->AFilter.deadband;
                        StdAPConfig.filter.filterConf.DBFilter.nominal  = dummyConfigStdAnaloguePtr->AFilter.nominal;
                    break;

                    case AFILTER_TYPE_UPPER_LIMIT:
                        StdAPConfig.filter.filterConf.ULFilter.upperLimit = dummyConfigStdAnaloguePtr->AFilter.upperLimit;
                        StdAPConfig.filter.filterConf.ULFilter.hysteresis = dummyConfigStdAnaloguePtr->AFilter.hysteresis;
                    break;

                    case AFILTER_TYPE_LOWER_UPPER_LIMIT:
                        StdAPConfig.filter.filterConf.LULFilter.upperLimit = dummyConfigStdAnaloguePtr->AFilter.upperLimit;
                        StdAPConfig.filter.filterConf.LULFilter.lowerLimit = dummyConfigStdAnaloguePtr->AFilter.lowerLimit;
                        StdAPConfig.filter.filterConf.LULFilter.hysteresis = dummyConfigStdAnaloguePtr->AFilter.hysteresis;
                    break;

                    default:
                        return NULL;
                    break;
                }

                StdAPConfig.offset = dummyConfigStdAnaloguePtr->offset;
                StdAPConfig.scale = dummyConfigStdAnaloguePtr->scale;
                StdAPConfig.overflow = dummyConfigStdAnaloguePtr->overflow;
                StdAPConfig.hysteresis = dummyConfigStdAnaloguePtr->hysteresis;

                StdAPConfig.channelRef = chPtr0;
                pointPtr = new StandardAnaloguePoint(mutex, database, StdAPConfig);

                return pointPtr;
            }
        }
    }

    return pointPtr ;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
