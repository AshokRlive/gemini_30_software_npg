/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: DigitalChangeEvent.h 2 Jul 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/Points/include/DigitalChangeEvent.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Digital change event reaction.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 2 Jul 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   2 Jul 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef DIGITALCHANGEEVENT_H__INCLUDED
#define DIGITALCHANGEEVENT_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "MCMConfigEnum.h"      //for EDGE_DRIVEN_MODE definition
#include "BinaryDigitalPoint.h"
#include "IPointObserver.h"
#include "GeminiDatabase.h"
#include "TimeManager.h"
#include "Logger.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*
 * \brief Digital change event reaction
 *
 * This object stores the configuration about how to react to a single binary
 * digital point event. In addition, provides functionality for the detection
 * of these changes and generates an event when needed.
 *
 * When a Pulse Width is provided (not 0), then edge configuration is ignored.
 * Otherwise the edge configuration is used.
 */
struct DigitalChangeEvent : public IPoint, private IPointObserver
{
public:
    struct Config
    {
    public:
        /* Note: a pulse width value of 0 means it is an edge-driven change */
        EDGE_DRIVEN_MODE edge;      //For edge events, type of edge that applies
        lu_uint32_t pulseWidth_ms;  //For Pulse events, width of the pulse (in milliseconds)
    public:
        Config() :  edge(EDGE_DRIVEN_MODE_LAST),
                    pulseWidth_ms(0)
        {};
    };

public:
    /**
     * \brief Custom constructors
     *
     * \param g3Database Reference to the Gemini 3 database
     * \param sourceIDCfg ID of the Binary Digital Point that acts as source of digital changes
     * \param edgeCfg Edge type configuration
     * \param pulseWidthCfg Pulse width configuration
     */
    DigitalChangeEvent( GeminiDatabase& g3Database,
                        PointIdStr sourceIDCfg,
                        DigitalChangeEvent::Config conf);
    virtual ~DigitalChangeEvent();

    /* == Inherited from IPoint == */
    virtual GDB_ERROR init();
    virtual PointIdStr getID() {return sourceID;};
    virtual POINT_TYPE getType() {return POINT_TYPE_BINARY;};
    virtual PointFlags getFlags() { return lastFlags; };
    virtual bool isActive() {return lastFlags.isActive(); };
    virtual GDB_ERROR getValue(PointData *valuePtr);
    virtual GDB_ERROR getRAWValue(PointData *valuePtr);
    virtual GDB_ERROR getFrozenValue(PointData* valuePtr) {return getValue(valuePtr); };
    virtual std::string getLabel(const PointData* valuePtr)
    {
        //default implementation
        LU_UNUSED(valuePtr);
        return "";
    }
    virtual GDB_ERROR freeze(const bool andClear)
    {
        LU_UNUSED(andClear);
        return GDB_ERROR_NOT_SUPPORTED;
    }
    virtual GDB_ERROR clear()
    {
        return GDB_ERROR_NOT_SUPPORTED;
    }
    virtual GDB_ERROR getRange(PointData* minValuePtr, PointData* maxValuePtr)
    {
        LU_UNUSED(minValuePtr);
        LU_UNUSED(maxValuePtr);
        return GDB_ERROR_NOT_SUPPORTED;
    }
    virtual POINT_QUALITY getQuality();
    virtual GDB_ERROR attach(IPointObserver* observer);
    virtual GDB_ERROR detach(IPointObserver* observer);
    void stopUpdate();

    /* == Inherited from IpointObserver == */
    virtual PointIdStr getPointID() { return sourceID; };

private:
    /* == Inherited from IpointObserver == */
    virtual void update(PointIdStr pointID, PointData *pointDataPtr);

    /**
     * \brief Update all the observers
     *
     * This function is NOT thread safe. The caller should lock the gemini
     * global mutex before calling this function
     *
     * \param data Value forwarded to all the observers
     */
    void updateObservers(PointData &data);

private:
    GeminiDatabase& database;   //Reference to the Gemini Database, to attach to the source
    Logger& log;

    bool initialised;           //Object initialisation flag
    PointIdStr sourceID;        //Digital virtual point, source of digital changes
    Config config;              //Digital event configuration

    /* Data storage */
    MasterMutex dataMutex;      //Mutex to protect data access
    PointFlags lastFlags;       //Store latest flags reported to check flag changes
    bool counting;                  //States that we are counting the pulse width
    TimeManager::TimeStr riseTime;  //Timestamp when the signal rose, for pulse detection

    /* Observer queue */
    MasterMutex queueMutex;         //Mutex to protect observer queue
    PointObserverQueue observerQueue;
};


#endif /* DIGITALCHANGEEVENT_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
