/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Standard (channel-related) Analogue point implementation.
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "StandardAnaloguePoint.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
StandardAnaloguePoint::StandardAnaloguePoint( Mutex* gMutex                  ,
                                              GeminiDatabase& g3database    ,
                                              StdConfig& config
                                            ) :
                                                 AnaloguePoint(gMutex, g3database, config),
                                                 channel(config.channelRef)
{
    if(channel == NULL)
    {
        log.error("%s Analogue Point (%s) - Invalid channel configured.",
                    __AT__,pointID.toString().c_str()
                  );
    }
}


StandardAnaloguePoint::~StandardAnaloguePoint()
{
    /* Detach observers */
    channel->detach(this);
    channel = NULL;
}


GDB_ERROR StandardAnaloguePoint::init()
{
    if(initialised)
    {
        return GDB_ERROR_INITIALIZED;
    }
    if(channel == NULL)
    {
        return GDB_ERROR_NOT_INITIALIZED;
    }
    GDB_ERROR ret = GDB_ERROR_NOT_INITIALIZED;
    m_data.setTime(TimeMgr.now());
    /* Register observer */
    IOM_ERROR chRet = channel->attach(this);
    if(chRet != IOM_ERROR_NONE)
    {
        log.error("%s Analogue Point (%s) - attach observer error %i: %s",
                    __AT__, pointID.toString().c_str(), chRet, IOM_ERROR_ToSTRING(chRet)
                  );
    }
    else
    {
        initialised = true;
        ret = GDB_ERROR_NONE;
    }
    return ret;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
void StandardAnaloguePoint::update(PointData* newData, DATACHANGE dataChange)
{
    updateAnaloguePoint(newData, dataChange);
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void StandardAnaloguePoint::update(IChannel* subject)
{
    IOM_ERROR chRet;
    IODataFloat32 newData;
    IChannel::ValueStr valueRead(newData);

    /* Read the channel value */
    chRet = subject->read(valueRead);
    if(chRet == IOM_ERROR_NONE)
    {
        PointDataFloat32 pointData;
        TimeManager::TimeStr timeStamp;
        pointData = (lu_float32_t)(newData);
        pointData.setQuality( (valueRead.flags.online == LU_TRUE)? POINT_QUALITY_ON_LINE :
                                                                   POINT_QUALITY_OFF_LINE );
        pointData.setOverflowFlag(valueRead.flags.outRange);
        pointData.setInitialFlag(valueRead.flags.restart);
        pointData.setInvalidFlag(valueRead.flags.restart);
        newData.getTime(timeStamp);
        pointData.setTime(timeStamp);
        scheduleUpdate(*this, pointData);
    }
    else
    {
        log.error("%s: Point (%s) - channel error %i: %s",
                    __AT__, pointID.toString().c_str(), chRet, IOM_ERROR_ToSTRING(chRet)
                  );
    }
}


/*
 *********************** End of file ******************************************
 */
