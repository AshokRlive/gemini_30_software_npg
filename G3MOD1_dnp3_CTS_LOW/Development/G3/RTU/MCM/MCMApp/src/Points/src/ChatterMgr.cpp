/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Chatter manager implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "ChatterMgr.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
ChatterMgr::ChatterMgr(lu_uint32_t chatterNo, lu_uint32_t chatterTime) :
                                                       chatterNo(chatterNo),
                                                       chatterTime(chatterTime),
                                                       chattering(false),
                                                       lockChatter(false)
{}

ChatterMgr::~ChatterMgr()
{
    ChatterEvent *eventPtr;
    ChatterEvent *eventNextPtr;

    // Protect the event queue
    MasterLockingMutex mLock(queueMutex, LU_TRUE);

    /* Delete all events in the queue */
    eventPtr = eventQueue.getFirst();
    while(eventPtr != NULL)
    {
        eventNextPtr = eventQueue.getNext(eventPtr);
        eventQueue.remove(eventPtr);
        delete eventPtr;

        eventPtr = eventNextPtr;
     }
}


bool ChatterMgr::newEvent(const lu_uint32_t eventTime)
{
    /* Add new event to the queue only if the chatter is enabled */
    if(chatterNo != 0)
    {
        // Protect the event queue
        MasterLockingMutex mLock(queueMutex, LU_TRUE);

        if(eventQueue.add(new ChatterEvent(eventTime)) == LU_TRUE)
        {
            /* Update and return status*/
            return update(eventTime);
        }
    }
    return false;    //failed to add new event
}


bool ChatterMgr::inChatter()
{
    if(lockChatter)
    {
        return true;    //Was set in chatter from origin
    }
    return chattering;
}


bool ChatterMgr::update(lu_uint32_t timestamp)
{
    ChatterEvent *eventPtr;
    ChatterEvent *eventTmpPtr;
    lu_uint32_t entries;

    if(chatterNo == 0)
    {
        return false;   //Not configured to process chatter
    }

    // Protect the event queue
    MasterLockingMutex mLock(queueMutex);

    /* Remove elapsed events */
    eventPtr = eventQueue.getFirst();
    while(eventPtr != NULL)
    {
        if( (eventPtr->getTime() + chatterTime) <= timestamp)
        {
            /* Elapsed event. Get the next event and
             * then remove the current event
             */
            eventTmpPtr = eventQueue.getNext(eventPtr);
            eventQueue.remove(eventPtr);
            delete eventPtr;
            eventPtr = eventTmpPtr;
        }
        else
        {
            break;
        }
    }

    entries = eventQueue.getEntries();
    if(chattering == false)
    {
        /* Check if the number of events in the
         * queue exceeds the chattering threshold
         */

        if (entries > chatterNo)
        {
            chattering = true;
        }
    }
    else
    {
        /* If the queue is empty we can reset the chattering state */
        if(entries == 0)
        {
            chattering = false;
        }
    }

    //Do not issue an out-of-chatter if still in Chatter from origin
    return inChatter();
}


void ChatterMgr::setLock(const bool lockIt)
{
    lockChatter = lockIt;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
