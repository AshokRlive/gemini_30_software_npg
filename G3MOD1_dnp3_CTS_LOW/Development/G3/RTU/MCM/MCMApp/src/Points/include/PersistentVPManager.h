/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:PersistentVPManager.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       Persistent Virtual Points Storage Manager
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   7 Nov 2017     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef PERSISTENTVPMANAGER_H_
#define PERSISTENTVPMANAGER_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <fstream>
#include <string>
#include <vector>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "PersistentVPcommon.h"
#include "Index.hpp"
#include "IPoint.h"

namespace lucyMem
{

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * Class PersistentVPManager
 *
 * This Singleton class keeps track of the Virtual Points statuses stored on a
 * persistent media.
 */
class PersistentVPManager
{
public:
    typedef enum
    {
        ManagerStatus_OK,
        ManagerStatus_INITIALISING,
        ManagerStatus_FAIL,
        ManagerStatus_RESET,
        ManagerStatus_LAST
    }ManagerStatus;
private:
    /**
     * \brief Singleton: Private constructor & destructor
     */
    PersistentVPManager();
    virtual ~PersistentVPManager();

    /*
     * \brief Singleton: Private copy constructor
     */
    PersistentVPManager(const PersistentVPManager &pvm)
    {
        LU_UNUSED(pvm);
    }
public:
    /**
     * \brief Get an instance if this manager.
     *
     * Usage example:
     *  lucyMem::PersistentVPManager& pvm = lucyMem::PersistentVPManager::getInstance();
     *
     * \return Reference to this singleton
     */
    static PersistentVPManager& getInstance()
    {
        static PersistentVPManager instance;
        return instance;
    };

    virtual ManagerStatus getStatus() { return m_status; };

    /**
     * \brief Read a Virtual Point value from non-volatile memory.
     *
     * NOTE: always write the default value to non-volatile memory when first read failed.
     *
     * \param variable Type (enum) of the variable to read
     * \param value Pointer to memory area where to store the value recovered from non-volatile memory
     * \param sizeRead Where to store the size of the value recovered
     *
     * \return Operation result. LU_FALSE when error.
     */
    virtual lu_bool_t read( const MemType entryType,
                            const VPointUID& pointUID,
                            PointData& value);

    /**
     * \brief Write a value to non-volatile memory.
     *
     * NOTE: always write the default value to memory when first read failed.
     * NOTE: size of this variable should have been set before any read/write operation.
     *
     * \param variable Type (enum) of the variable to write
     * \param value Pointer to memory area where is the value to be stored in non-volatile memory
     *
     * \return Operation result. LU_FALSE when error.
     */
    virtual lu_bool_t write(const MemType entryType,
                            const VPointUID& pointUID,
                            /*const*/ PointData& value);

    /**
     * \brief Invalidate the contents of the non-volatile storage.
     *
     * The index is cleared as well.
     */
    virtual void invalidate(const lu_bool_t forced = LU_FALSE);

    /**
     * \brief Update the values to the non-volatile memory.
     *
     * Copies all the variable values cached to the non-volatile memory. The
     * effective writing is done only in there is any data change.
     */
    virtual void flush();

private:
    /**
     * \brief Virtual Point Index
     */
    typedef Index<VPointUID, std::streampos> FileIndex;
    /**
     * \brief File set for file & index relationship
     */
    struct FileSet
    {
    public:
        FileIndex index;
        std::fstream memFile;
        std::string fileName;
    public:
        FileSet(const char* filename) :
            memFile(filename),
            fileName(filename)
        {};

        virtual ~FileSet()
        {
            memFile.close();
        };
    private:
        std::fstream* pFile;
    };

    /**
     * \brief Open file outcome
     */
    typedef enum
    {
        OpenStatus_OK,      //File was opened
        OpenStatus_CREATED, //File was created
        OpenStatus_FAIL,    //Failed to open/create file
        OpenStatus_LAST
    }OpenStatus;

private:
    OpenStatus doOpenFile(FileSet& fileSet, const bool forced = false);
    bool doCreateIndex(FileSet& oneFileSet);
    bool doReadEntry(std::fstream& oneFile,
                    const std::streamsize entrySize,
                    const std::streampos curPos,
                    char* entryPtr);
    bool doWriteEntry(std::fstream& oneFile,
                    const std::streamsize entrySize,
                    const std::streampos curPos,
                    char* entryPtr);

    template<typename PointDataType>
    bool readEntry(FileSet& memFile,
                   const VPointUID& pointUID,
                   const MemType type,
                   PointDataType& value);
    template<typename PointDataType>
    bool writeEntry(FileSet& memFile,
                    const VPointUID& pointUID,
                    const MemType type,
                    PointDataType& value);

private:
    bool m_initialised;     //The manager has been successfully initialised
    ManagerStatus m_status; //Status of the Manager

    //NOTE: The file vector is created at startup and never change size.
    //      Entries on vector are guaranteed to be non-null by initialization
    std::vector<FileSet*> m_fileSet;  //Array of files and related info
};

}//end namespace lucyMem


#endif /* PERSISTENTVPMANAGER_H_ */

/*
 *********************** End of file ******************************************
 */
