/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:PersistentVPManager.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   7 Nov 2017     pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <fstream>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "PersistentVPManager.h"
#include "PersistentVPData.h"
#include "Mutex.h"
#include "Debug.h"

#define DEBUG_MEM_PERSISTVP 0
#if DEBUG_MEM_PERSISTVP
    #include <stdio.h>
    #include <iomanip>
#endif

namespace lucyMem
{

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static MasterMutex locker;         //Protects concurrent access to functionality

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define DBGLOG_ACTION_HEADER    "[MEM]"
#define DBGLOG_CTOR_HEADER      DBGLOG_ACTION_HEADER"Ctor:"
#define DBGLOG_READ_HEADER      DBGLOG_ACTION_HEADER"read:"
#define DBGLOG_WRITE_HEADER     DBGLOG_ACTION_HEADER"write:"

#if DEBUG_MEM_PERSISTVP
    #define DEBUG_MEM_PERSISTVP_ADVANCED 1      //Advanced debug: extra content
    #define DEBUG_MEM_PERSISTVP_HEX 0           //Print hex content
    #define DEBUG_MEM_PERSISTVP_HEX_ADVANCED 1  //Print full hex content

    #define DBGLOG_MSG(header, text)        DBG_INFO(header"%s", text)
    #define DBGLOG_FAIL(header, text)       DBG_ERR(header" %s", text)
    #define DBGLOG_READ(header, trail, entry)   DBG_INFO("%s %s:entry %s", header, trail, entry.toString().c_str());

    #define DBGLOG_CTOR_FAIL(text1, filename)   DBG_ERR(DBGLOG_CTOR_HEADER" %s: %s file", text1, filename)
    #define DBGLOG_CTOR_TYPE_READ(type)     DBG_INFO(DBGLOG_CTOR_HEADER" read type=%u", type)
    #define DBGLOG_CTOR_INDEX_END(indexer)  DBG_INFO(DBGLOG_CTOR_HEADER"reading indexing finished ---!\n"    \
                                                 DBGLOG_CTOR_HEADER"--available:%s", indexer.toString().c_str())
    #define DBGLOG_READ_REF(header, pointType, uid)  DBG_INFO(DBGLOG_READ_HEADER header" %s", entryrefToString(pointType,uid).c_str())
    #define DBGLOG_READ_NOTFOUND(text, uid) DBG_INFO(DBGLOG_READ_HEADER" %s: uid=#%s", text, uidString(uid).c_str())
    #define DBGLOG_WRITE_BAD(memfile)       DBG_ERR(DBGLOG_WRITE_HEADER" write invalid: %s", readResult(memfile).c_str())


    #if DEBUG_MEM_PERSISTVP_ADVANCED
        #define DBGLOG_CTOR_OPENING(filename)   DBG_INFO(DBGLOG_CTOR_HEADER"opening %s file", filename)
        #define DBGLOG_CTOR_OPENRESULT(preText, filename, status)  DBG_INFO(DBGLOG_CTOR_HEADER"%sopen '%s: %s'", preText, filename, (status)? "OK":"Fail")
        #define DBGLOG_READ_INDEX_BACK(header, fSet, uid, pos)     checkIndexBack(fSet.index, header, uid, pos)
        #define DBGLOG_WRITE_START(indexer)     DBG_INFO(DBGLOG_WRITE_HEADER"available:%s", indexer.toString().c_str())
        #define DBGLOG_WRITE_FOUND(entry, pos)  DBG_INFO(DBGLOG_WRITE_HEADER"found #%s @%s", uidString(entry.uid).c_str(), posString(pos).c_str())
        #define DBGLOG_WRITE_ENTRY(text, entry, pos) DBG_INFO(DBGLOG_WRITE_HEADER"%s @%s entry %s", text, posString(pos).c_str(), entry.toString().c_str())
        #define DBGLOG_CHECKREADBACK(text, fileStream, pos) checkReadback(fileStream, text, pos)
    #endif
    #if DEBUG_MEM_PERSISTVP_HEX_ADVANCED
        #define DBGLOG_ENTRY_HEX(header, entry) DBG_INFO(header"HexEntry(sz:%u)=[%s]", sizeof(entry), entrytoHex(entry).c_str())
    #else
        #if DEBUG_MEM_PERSISTVP_HEX
            #define DBGLOG_ENTRY_HEX(header, entry) DBG_INFO(header"HexValue=[%s]", entryvalueHex(entry).c_str())
        #endif
    #endif
#endif
//Define debug entries if not already done
#ifndef DBGLOG_MSG
    //DEBUG_MEM_PERSISTVP debug macros
    #define DBGLOG_MSG(header, text)
    #define DBGLOG_FAIL(header, text)
    #define DBGLOG_READ(header, trail, entry)
    #define DBGLOG_CTOR_FAIL(text1, filename)
    #define DBGLOG_CTOR_TYPE_READ(type)
    #define DBGLOG_CTOR_INDEX_END(indexer)
    #define DBGLOG_READ_REF(header, pointType, uid)
    #define DBGLOG_READ_NOTFOUND(text, uid)
    #define DBGLOG_WRITE_BAD(memfile)
#endif
#ifndef DBGLOG_CTOR_OPENING
    //DEBUG_MEM_PERSISTVP_ADVANCED debug macros
    #define DBGLOG_CTOR_OPENING(filename)
    #define DBGLOG_CTOR_OPENRESULT(preText, filename, status)
    #define DBGLOG_WRITE_START(indexer)
    #define DBGLOG_WRITE_FOUND(entry, pos)
    #define DBGLOG_WRITE_ENTRY(text, entry, pos)
    #define DBGLOG_CHECKREADBACK(text, fileStream, pos)
    #define DBGLOG_READ_INDEX_BACK(header, fSet, uid, pos)
#endif
#ifndef DBGLOG_ENTRY_HEX
    //DEBUG_MEM_PERSISTVP_HEX or DEBUG_MEM_PERSISTVP_HEX_ADVANCED debug macros
    #define DBGLOG_ENTRY_HEX(header, entry)
#endif

#if DEBUG_MEM_PERSISTVP
    /* Debug functions */
    std::string readResult(std::fstream& memFile);
    std::string posString(const std::streampos pos);
    std::string uidString(const VPointUID uid);
    void checkReadback(std::fstream& memFile, const char* trail, const std::streampos& pos);
    void checkIndexBack(Index<VPointUID, std::streampos>& index, const char* trail, VPointUID uid, const std::streampos& pos);
    std::string entryrefToString(const MemType type, const VPointUID& pointUID);

    #if DEBUG_MEM_PERSISTVP_HEX || DEBUG_MEM_PERSISTVP_HEX_ADVANCED
        const std::string entrytoHex(MemEntry& me);
        const std::string entryvalueHex(MemEntry& me);
    #endif
#endif


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
lu_bool_t PersistentVPManager::read(const MemType entryType,
                                    const VPointUID& pointUID,
                                    PointData& pointData)
{
    bool res = false;
    if(!m_initialised)
    {
        return res;
    }
    switch(entryType)
    {
        case MemType_ANALOGUE:
        {
            PointDataFloat32* pData = reinterpret_cast<PointDataFloat32*>(&pointData);
            res = readEntry(*m_fileSet[FileType[entryType].fileNum], pointUID, entryType, *pData);
            break;
        }
        case MemType_DIGITAL:
        {
            PointDataUint8* pData = reinterpret_cast<PointDataUint8*>(&pointData);
            res = readEntry(*m_fileSet[FileType[entryType].fileNum], pointUID, entryType, *pData);
            break;
        }
        case MemType_COUNTER:
        case MemType_COUNTER_FROZEN:
        {
            PointDataCounter32* pData = reinterpret_cast<PointDataCounter32*>(&pointData);
            res = readEntry(*m_fileSet[FileType[entryType].fileNum], pointUID, entryType, *pData);
            break;
        }
        default:
            break;
    }
    return (res)? LU_TRUE : LU_FALSE;
}


lu_bool_t PersistentVPManager::write(const MemType entryType,
                                    const VPointUID& pointUID,
                                    PointData& pointData)
{
    bool res = false;
    if(!m_initialised)
    {
        return res;
    }
    switch(entryType)
    {
        case MemType_ANALOGUE:
        {
            PointDataFloat32* pData = reinterpret_cast<PointDataFloat32*>(&pointData);
            res = writeEntry(*m_fileSet[FileType[entryType].fileNum], pointUID, entryType, *pData);
            break;
        }
        case MemType_DIGITAL:
        {
            PointDataUint8* pData = reinterpret_cast<PointDataUint8*>(&pointData);
            res = writeEntry(*m_fileSet[FileType[entryType].fileNum], pointUID, entryType, *pData);
            break;
        }
        case MemType_COUNTER:
        case MemType_COUNTER_FROZEN:
        {
            PointDataCounter32* pData = reinterpret_cast<PointDataCounter32*>(&pointData);
            res = writeEntry(*m_fileSet[FileType[entryType].fileNum], pointUID, entryType, *pData);
            break;
        }
        default:
            break;
    }
    return (res)? LU_TRUE : LU_FALSE;
}


void PersistentVPManager::invalidate(const lu_bool_t forced)
{
    if( m_initialised || (forced==LU_TRUE) )
    {//Critical Section
        MasterLockingMutex(locker, LU_TRUE);
        m_status = ManagerStatus_RESET;
        for (lu_uint32_t i = 0; i < NUMFILES; ++i)
        {
            m_fileSet[i]->memFile.close();
            m_fileSet[i]->index.clear();
            //Re-Open in trunc mode to remove previous content
            OpenStatus status;
            status = doOpenFile(*m_fileSet[i], forced);
            if(status != OpenStatus_CREATED)
            {
                m_status = ManagerStatus_FAIL;
                DBGLOG_MSG(DBGLOG_ACTION_HEADER, "Error clearing the Persistent storage");
            }
            else
            {
                DBGLOG_MSG(DBGLOG_ACTION_HEADER, "Persistent storage cleared");
            }
        }
        //log.warning("Persistent storage cleared");
    }
}


void PersistentVPManager::flush()
{
    if(m_initialised)
    {//Critical Section
        MasterLockingMutex(locker, LU_TRUE);
        for (lu_uint32_t i = 0; i < NUMFILES; ++i)
        {
            m_fileSet[i]->memFile.flush();
        }
    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
PersistentVPManager::PersistentVPManager() : m_initialised(false)
{
    MasterLockingMutex(locker, LU_TRUE);
    m_status = ManagerStatus_INITIALISING;
    bool status = false;

    try
    {
        /* Initialise file structures */
        m_fileSet.reserve(NUMFILES);  //Adjust vector size to save memory
        /* TODO: pueyos_a - check file/entry validity? */
        for (lu_uint32_t i = 0; i < NUMFILES; ++i)
        {
            FileSet* fset = new FileSet(FileData[i].fileName);
            m_fileSet.push_back(fset);
            /* Open Files */
            OpenStatus res;
            res = doOpenFile(*m_fileSet[i]);
            switch (res)
            {
                case OpenStatus_OK:
                    /* Create Index over the memory block device */
                    status = doCreateIndex(*m_fileSet[i]);
                    if(!status)
                    {
                        DBGLOG_CTOR_FAIL("file indexing failed on", m_fileSet[i]->fileName.c_str());
                        throw std::ios_base::failure("Indexing failed");
                    }
                    break;
                case OpenStatus_CREATED:
                    DBGLOG_CTOR_OPENRESULT("created file", m_fileSet[i]->fileName.c_str(), m_fileSet[i]->memFile.is_open());
                    //Note: newly-created file does not need the index to be populated
                    status = true;
                    break;
                case OpenStatus_FAIL:
                    DBGLOG_CTOR_FAIL("failed to open/create file", m_fileSet[i]->fileName.c_str());
                    throw std::ios_base::failure("File open failed");
                    break;
                default:
                    break;
            }
            if(status)
            {
                m_status = ManagerStatus_OK;
            }
        }
    }
    catch (const std::ios_base::failure& ex)
    {
        std::ostringstream ss;
        ss << "Indexing FAILED: " << ex.what();
        m_status = ManagerStatus_FAIL;
        DBGLOG_FAIL(DBGLOG_CTOR_HEADER, ss.str().c_str());
        //log.error("%s Indexing FAILED", this.toString().c_str());

        invalidate(LU_TRUE);    //re-create all files & clear index
        status = ( (m_status == ManagerStatus_OK) || (m_status == ManagerStatus_RESET) );
    }
    m_initialised = status;
}


PersistentVPManager::~PersistentVPManager()
{
    if(m_initialised)
    {
        MasterLockingMutex(locker, LU_TRUE);
        m_initialised = false;    //Prevent further operation
        for (lu_uint32_t i = 0; i < NUMFILES; ++i)
        {
            m_fileSet[i]->memFile.close();
            delete m_fileSet[i];
            m_fileSet[i] = NULL;
        }
    }
}


PersistentVPManager::OpenStatus PersistentVPManager::doOpenFile(FileSet& oneFileSet, const bool forced)
{
    OpenStatus ret = OpenStatus_FAIL;
    bool res = false;
    std::fstream& memfile = oneFileSet.memFile; //Alias
    if(!forced)
    {
        //First, try to open file as normal

        DBGLOG_CTOR_OPENING(oneFileSet.fileName.c_str());
        memfile.open(oneFileSet.fileName.c_str(), memfile.binary | memfile.in | memfile.out);
        res = memfile.is_open();
        DBGLOG_CTOR_OPENRESULT("", oneFileSet.fileName.c_str(), res);
        if(res)
        {
            memfile.seekg(0);   //rewind
            ret = OpenStatus_OK;
        }

    }
    if(!res || forced)
    {
        //File not found, doesn't exist: create
        memfile.open(oneFileSet.fileName.c_str(), memfile.binary | memfile.in | memfile.out | memfile.trunc);
        res = memfile.is_open();
        DBGLOG_CTOR_OPENRESULT("re", oneFileSet.fileName.c_str(), res);
        if(res)
        {
            ret = OpenStatus_CREATED;
        }
    }
    return ret;
}


bool PersistentVPManager::doCreateIndex(FileSet& oneFileSet)
{
    bool res = false;
    try
    {
        /* Create Index over the memory block device */
        std::streampos curPos;
        MemEntry entry;
        std::fstream& memfile = oneFileSet.memFile; //Alias
        memfile.clear();    //Clear flags
        while(!memfile.eof())
        {
            curPos = memfile.tellg();
            res = doReadEntry(memfile, sizeof(MemEntry), curPos, (char*)(&entry));
            DBGLOG_CTOR_TYPE_READ(entry.type);
            if(memfile.eof())
            {
                DBGLOG_READ(DBGLOG_CTOR_HEADER, "finished", entry);
                break;  //Indexing finished
            }
            /* TODO: pueyos_a - check res */
            else
            {
                oneFileSet.index.add(entry.uid, curPos);
                DBGLOG_READ_INDEX_BACK(DBGLOG_CTOR_HEADER, oneFileSet, entry.uid, curPos);
                DBGLOG_READ(DBGLOG_CTOR_HEADER, "entry", entry);
                DBGLOG_ENTRY_HEX(DBGLOG_CTOR_HEADER, entry);
            }
        }
        res = true; //Finished OK
        DBGLOG_CTOR_INDEX_END(oneFileSet.index);
    }
    catch (const std::ios_base::failure& ex)
    {
        std::ostringstream ss;
        ss << "Indexing FAILED: " << ex.what();
        DBGLOG_FAIL(DBGLOG_CTOR_HEADER, ss.str().c_str());
        //log.error("%s Indexing FAILED", this.toString().c_str());

        invalidate(LU_TRUE);    //re-create file & clear index
        return false;
    }

    return res;
}


bool PersistentVPManager::doReadEntry(std::fstream& oneFile,
                                    const std::streamsize entrySize,
                                    const std::streampos curPos,
                                    char* entryPtr)
{
    try
    {
        oneFile.clear();    //Clear flags
        oneFile.seekg(curPos);  //Position file cursor
        if(!oneFile.read(entryPtr, entrySize))
        {
            return false;
        }
    }
    catch (const std::ios_base::failure& ex)
    {
        //ex.what();
        return false;
    }
    return true;
}

bool PersistentVPManager::doWriteEntry(std::fstream& oneFile,
                                        const std::streamsize entrySize,
                                        const std::streampos curPos,
                                        char* entryPtr)
{
    /* NOTE: we do not read/check previous entry since VP timestamp being the
     *      same is highly unlikely to happen, so is written anyway */
    try
    {
        oneFile.clear();    //Clear flags
        oneFile.seekp(curPos);  //Position file cursor
        if(!oneFile.write(entryPtr, entrySize))
        {
            return false;
        }
        oneFile.flush();    //Force file update
    }
    catch (const std::ios_base::failure& ex)
    {
        //ex.what();
        return false;
    }
    return true;
}


template<typename PointDataType>
bool PersistentVPManager::readEntry(FileSet& memFile,
                                    const VPointUID& pointUID,
                                    const MemType type,
                                    PointDataType& pointData)
{
    std::streampos pos;
    //Prepare entry to search for
    MemEntry entry;
    DBGLOG_READ_REF("about to read", type, pointUID);
    {//Critical Section
        MasterLockingMutex(locker, LU_FALSE);
        /* Find entry */
        try
        {
            pos = memFile.index.get(pointUID);
        }
        catch (const std::exception& ex)
        {
            DBGLOG_READ_NOTFOUND("Not found in index ", pointUID);
            return false;    //Not found!
        }
        /* Read entry */
        try
        {
            doReadEntry(memFile.memFile, sizeof(entry), pos, reinterpret_cast<char*>(&entry));
            DBGLOG_READ(DBGLOG_READ_HEADER, "reading entry", entry);
            DBGLOG_ENTRY_HEX(DBGLOG_READ_HEADER, entry);
        }
        catch (const std::ios_base::failure& ex)
        {
            /* TODO: pueyos_a - failure to access memFile!!!! */
            DBGLOG_FAIL(DBGLOG_READ_HEADER, "error accessing memory file");
            return LU_FALSE;
        }
    } //End of critical section

    /* Convert mem entry into point data */
    TimeManager::TimeStr timestamp;
    timestamp.badTime = false;
    timestamp.neverSynchronised = false;
    timestamp.time = entry.timestamp;
    timestamp.synchronised = entry.synchronised;
    pointData.setTime(timestamp);
    pointData.setFlags(entry.flags.getFlags());
    switch(type)
    {
        case MemType_DIGITAL:
            pointData = entry.value.digital;
            break;
        case MemType_ANALOGUE:
            pointData = entry.value.analogue;
            break;
        case MemType_COUNTER:
        case MemType_COUNTER_FROZEN:
            pointData = entry.value.counter;
            break;
        default:
            DBGLOG_FAIL(DBGLOG_READ_HEADER, "Point type unknown");
            return false;
            break;
    }
    return true;
}

template<typename PointDataType>
bool PersistentVPManager::writeEntry(FileSet& memFile,
                                    const VPointUID& pointUID,
                                    const MemType type,
                                    PointDataType& pointData)
{
    bool ret = LU_FALSE;
    DBGLOG_WRITE_START(memFile.index);
    /* Convert point data into mem entry */
    MemEntry entry;
    entry.type = type;
    entry.uid = pointUID;
    TimeManager::TimeStr timestamp;
    pointData.getTime(timestamp);
    entry.timestamp = timestamp.time;
    entry.synchronised = timestamp.synchronised;
    entry.flags.setFlags(pointData.getFlags());
    switch(type)
    {
        case MemType_DIGITAL:
            entry.value.digital = pointData;
            break;
        case MemType_ANALOGUE:
            entry.value.analogue = pointData;
            break;
        case MemType_COUNTER:
        case MemType_COUNTER_FROZEN:
            entry.value.counter = pointData;
            break;
        default:
            DBGLOG_FAIL(DBGLOG_READ_HEADER, "Point type unknown");
            return false;
            break;
    }

    /* Add entry to mem */
    std::streampos pos;
    bool found = true;
    {//Critical Section
        MasterLockingMutex(locker, LU_FALSE);
        try
        {
            if(!memFile.memFile.is_open())
            {
                DBGLOG_FAIL(DBGLOG_WRITE_HEADER, "Cannot write: file is not open!");
                return ret;
            }
            pos = memFile.index.get(entry.uid);
            DBGLOG_WRITE_FOUND(entry, pos);

        }
        catch (const std::exception& ex)
        {
            found = false;
            DBGLOG_MSG(DBGLOG_WRITE_HEADER, "entry not already there");
        }
        try
        {
            if(found)
            {
                DBGLOG_CHECKREADBACK("PRE-", memFile.memFile, pos);    //read back for check

                //Found: update entry
                doWriteEntry(memFile.memFile, sizeof(entry), pos, reinterpret_cast<char*>(&entry));

                DBGLOG_MSG(DBGLOG_WRITE_HEADER, "entry updated");
                DBGLOG_ENTRY_HEX(DBGLOG_WRITE_HEADER, entry);
            }
            else
            {
                //Not found: add it
                memFile.memFile.seekp(0, std::ios_base::end);   //Set last position
                std::streampos curPos = memFile.memFile.tellp();
                if(curPos < 0)
                {
                    //Case when tellp() fails in an empty file
                    memFile.memFile.clear();    //clear file state flags
                    curPos = 0L;        //Adjust position
                }
                memFile.memFile.write(reinterpret_cast<char*>(&entry), sizeof(entry));
                memFile.memFile.flush();
                if(memFile.memFile.bad() || memFile.memFile.fail())
                {
                    DBGLOG_WRITE_BAD(memFile.memFile);
                }
                memFile.index.add(entry.uid, curPos);

                DBGLOG_WRITE_ENTRY("added", entry, curPos);
                DBGLOG_ENTRY_HEX(DBGLOG_WRITE_HEADER, entry);
            }
            ret = LU_TRUE;

            DBGLOG_CHECKREADBACK("POST-", memFile.memFile, pos);    //read back for check
        }
        catch (const std::ios_base::failure& ex)
        {
            /* TODO: pueyos_a - failure to access memFile!!!! Reset statuses? */
            DBGLOG_FAIL(DBGLOG_WRITE_HEADER, "error accessing memory file");
            ret = LU_FALSE;
        }
    } //End of Critical Section
    return ret;
}



/*
 ******************************************************************************
 * Local Function implementation
 ******************************************************************************
 */


/* --------------------------------------------------
 * Implementation of debug functions
 * --------------------------------------------------
 */
#if DEBUG_MEM_PERSISTVP
std::string readResult(std::fstream& memFile)
{
    std::ostringstream ss;
    ss << "count=" << (int)memFile.gcount()
       << " good=" << (int)memFile.good()
       << " eof="  << (int)memFile.eof()
       << " fail=" << (int)memFile.fail()
       << " bad="  << (int)memFile.bad()    ; //<< std::endl;
    return ss.str();
}

std::string posString(const std::streampos pos)
{
    std::ostringstream ss;
    ss << pos;
    return ss.str();
}

std::string uidString(const VPointUID uid)
{
    std::ostringstream ss;
    ss << uid;
    return ss.str();
}

void checkReadback(std::fstream& memFile, const char* trail, const std::streampos& pos)
{
    MemEntry en;
    char* entryPtr = (char*)(&en);
    en.type = 99;
    std::streampos oldpos = memFile.tellg();
    memFile.clear();    //Clear flags
    if(!memFile.seekg(pos, std::ios_base::beg))  //Position back to read the whole entry
    {
        std::string s = readResult(memFile);
        DBG_ERR("%s unable to seek pos %s!!! (was %s): %s",
                        trail,
                        posString(pos).c_str(),
                        posString(oldpos).c_str(),
                        s.c_str());
    }
    else
    {
        try
        {
            if( memFile.read(entryPtr, sizeof(MemEntry)) )
            {
                DBGLOG_WRITE_ENTRY(trail, en, pos);
            }
            else
            {
                throw std::ios_base::failure("Read back failed");
            }
        }
        catch (const std::ios_base::failure& ex)
        {
            //ex.what();
            DBG_ERR("%s unable to read it back!!!: %s", trail, readResult(memFile).c_str());
        }
    }
}


void checkIndexBack(Index<VPointUID, std::streampos>& index, const char* trail, VPointUID uid, const std::streampos& pos)
{
    std::string gotIt;
    try
    {
        gotIt = uidString(index.get(uid));
        DBG_INFO("%s: indexed (%s, %s) = (%s, %s)", trail, uidString(uid).c_str(), uidString(pos).c_str(),
                        uidString(uid).c_str(), gotIt.c_str());
    }
    catch (const std::exception& ex)
    {
        DBGLOG_READ_NOTFOUND("Not found back in index", uid);
    }
}

std::string entryrefToString(const MemType type, const VPointUID& pointUID)
{
    std::stringstream ss;
    ss << "(" << (lu_uint32_t)(type);
    ss << ",[" << pointUID << "]): ";
    return ss.str();
}

#if DEBUG_MEM_PERSISTVP_HEX || DEBUG_MEM_PERSISTVP_HEX_ADVANCED
const std::string entrytoHex(MemEntry& me)
{
    lu_uint8_t* hbyte = (lu_uint8_t*)(&me);
    std::stringstream ss;
    ss << std::hex;
    ss << std::setw(2);
    ss << std::setfill('0');
    for (lu_uint32_t i = 0; i < sizeof(me); ++i)
    {
        if(*hbyte == 0)
        {
            ss << "00";
        }
        else
        {
            ss << (int)(*hbyte);
        }
        hbyte++;
        if(i % 2)
        {
            ss << " ";
        }
    }
    return ss.str();
}
const std::string entryvalueHex(MemEntry& me)
{
    lu_uint8_t* hbyte = (lu_uint8_t*)(&(me.value.counter));
    std::stringstream ss;
    ss << std::hex;
    ss << std::setw(2);
    ss << std::setfill('0');
    for (lu_uint32_t i = 0; i < sizeof(me.value.counter); ++i)
    {
        if(*hbyte == 0)
        {
            ss << "00";
        }
        else
        {
            ss << (int)(*hbyte);
        }
        hbyte++;
        //if(i % 2)
        {
            ss << " ";
        }
    }
    return ss.str();
}
#endif //DEBUG_MEM_PERSISTVP_HEX || DEBUG_MEM_PERSISTVP_HEX_ADVANCED
#endif //DEBUG_MEM_PERSISTVP

}//end namespace lucyMem

/*
 *********************** End of file ******************************************
 */
