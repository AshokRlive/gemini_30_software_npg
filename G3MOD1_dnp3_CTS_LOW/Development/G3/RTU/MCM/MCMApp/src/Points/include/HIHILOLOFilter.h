/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: HIHILOLOFilter.h 30 Jun 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/Points/include/HIHILOLOFilter.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Analogue Upper-Upper/Upper/Lower/Lower-Lower limit filter interface.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 30 Jun 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   30 Jun 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef HIHILOLOFILTER_H__INCLUDED
#define HIHILOLOFILTER_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <limits>   //For min_float32 value

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IAnalogueFilter.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief HIHI/HI/LO/LOLO analogue filter
 *
 * The filter behaviour is:
 * If the value surpasses any limit in any direction, the value is reported -> "Out of limit".
 * In any other case, is never reported -> "In limit".
 */
class HIHILOLOFilter: public IAnalogueFilter
{
public:
    /*
     * \brief Region index
     */
    typedef enum
    {
        REGION_LOLO,
        REGION_LO,
        REGION_MID,     //Middle region ("normal" values).
        REGION_HI,
        REGION_HIHI
    } REGION;

    /*
     * \brief enum for Limit indexes
     */
    typedef enum
    {
        LOLO,
        LO,
        HI,
        HIHI
    } LIMITS;

    /**
     * \brief filter configuration structure
     */
    struct Config
    {
    public:
        static const lu_uint32_t Limits = 4;    //Amount of limits
    public:
        IAnalogueFilter::ThresholdParams limit[Limits]; //From LoLo (0) to HiHi (3)
    public:
        /**
        * \brief check validity of the thresholds and hysteresis
        *
         * Thresholds and hysteresis are expected to be in the same units, and
         * fulfil all of the the following conditions when they are active:
         * -If there is 0 limits present, then is an invalid configuration.
         * -For the limits present:
        *       HiHi Threshold >= HiHi Hysteresis > Hi Threshold >= Hi Hysteresis
        *       Hi Threshold > Lo Threshold
        *       Lo Hysteresis >= Lo Threshold > LoLo Hysteresis >= LoLo Threshold
        *
        * Please take note that for Low thresholds the hysteresis is always
        * bigger or equal than the threshold, and for the High thresholds is the
        * other way around.
        *
        * \return True when the config is valid
        */
        bool isValid();
        /**
         * \brief Update recovery values based on thresholds and hysteresis
         */
        void update();

    private:
        static const lu_uint32_t HalfLimit = Limits / 2;
    };

public:
    /**
     * \brief constructor
     *
     * \param config Filter configuration
     */
    HIHILOLOFilter(HIHILOLOFilter::Config& config);
    virtual ~HIHILOLOFilter() {};

    /**
     * \brief Run the filter
     *
     * \param value New value to add to the filter
     *
     * \return filter result
     */
    virtual AFILTER_RESULT run(lu_float32_t value);

public:
    /**
     * \brief Obtains the current region visited
     *
     * \return Current filter region visited. Region is updated by run() method.
     */
    virtual HIHILOLOFilter::REGION getCurrentRegion() { return m_region; }

private:
    void updateRegion(const lu_float32_t value);

private:
    HIHILOLOFilter::Config m_limits;
    REGION m_region;  //Last region entered
};

#endif /* HIHILOLOFILTER_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
