/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "LowerUpperLimitFilter.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
LowerUpperLimitFilter::LowerUpperLimitFilter(LowerUpperLimitFilter::Config &conf)
                                            : m_config(conf)
{}

AFILTER_RESULT LowerUpperLimitFilter::run(lu_float32_t value)
{
    if(status == AFILTER_RESULT_NA)
    {
        //Initial value
        if(value < m_config.lower.threshold)
        {
            m_region = REGION_LOWER;
            status = AFILTER_RESULT_OUT_LIMIT_THR;
        }
        else if(value > m_config.upper.threshold)
        {
            m_region = REGION_UPPER;
            status = AFILTER_RESULT_OUT_LIMIT_THR;
        }
        else
        {
            m_region = REGION_MIDDLE;
            status = AFILTER_RESULT_IN_LIMIT_THR;
        }
    }
    else
    {
        /* Check value between limits (regions) and its relationship with the previous value */
        if( m_config.lower.active &&
            (m_region != REGION_LOWER) && (value < m_config.lower.threshold) )
        {
            //entering Lower region
            m_region = REGION_LOWER;
            status = AFILTER_RESULT_OUT_LIMIT_THR;
        }
        else if( m_config.upper.active &&
                (m_region != REGION_UPPER) && (value > m_config.upper.threshold) )
        {
            //entering Upper region
            m_region = REGION_UPPER;
            status = AFILTER_RESULT_OUT_LIMIT_THR;
        }
        else if( m_config.lower.active &&
                (m_region == REGION_LOWER) && (value < m_config.lower.recovery) )
        {
            //still in Lower region
            status = AFILTER_RESULT_OUT_LIMIT;
        }
        else if( m_config.upper.active &&
                (m_region == REGION_UPPER) && (value > m_config.upper.recovery) )
        {
            //still in Upper region
            status = AFILTER_RESULT_OUT_LIMIT;
        }
        else
        {
            m_region = REGION_MIDDLE;
            if(status == AFILTER_RESULT_IN_LIMIT_THR)
                status = AFILTER_RESULT_IN_LIMIT;   //Already entered into limit
            if(status != AFILTER_RESULT_IN_LIMIT)
                status = AFILTER_RESULT_IN_LIMIT_THR;   //Entering in limit
        }
    }
    return status;
}

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */


