/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Standard (channel-related) Analogue point interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   20/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_6D7C076B_3D58_41b3_AEF7_EC2E5F1704CA__INCLUDED_)
#define EA_6D7C076B_3D58_41b3_AEF7_EC2E5F1704CA__INCLUDED_


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "AnaloguePoint.h"
#include "IChannel.h"
#include "ChannelObserver.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * Custom implementation for Standard (non-normalized) analogue point.
 * The data source is an IOChannel.
 */
class StandardAnaloguePoint : public AnaloguePoint, private ChannelObserver
{
public:
    /**
     * \brief Standard channel-bounded Analogue Point Configuration
     */
    struct StdConfig : public AnaloguePoint::Config
    {
    public:
        IChannel* channelRef;
    };

public:
    StandardAnaloguePoint( Mutex* mutex                  ,
                           GeminiDatabase& g3database    ,
                           StdConfig& config
                         );
    virtual ~StandardAnaloguePoint();

    /* == Inherited from IPoint == */
    virtual GDB_ERROR init();

protected:
    /* == Inherited from AnaloguePoint == */
    virtual void update(PointData* newData, DATACHANGE dataChange);

private:
    /* == Inherited from ChannelObserver == */
    virtual void update(IChannel* subject);

protected:
    IChannel* channel;
};

#endif // !defined(EA_6D7C076B_3D58_41b3_AEF7_EC2E5F1704CA__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
