/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Virtual Point IOData implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stddef.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "PointData.h"
#include "bitOperations.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
POINT_QUALITY PointFlags::getQuality() const
{
    return static_cast<POINT_QUALITY>(quality);
}
void PointFlags::setQuality(const POINT_QUALITY quality)
{
    this->quality = quality;
}

bool PointFlags::isOnline() const
{
    return this->quality == POINT_QUALITY_ON_LINE;
}

void PointFlags::setOnline(const bool online)
{
    this->quality = online ? POINT_QUALITY_ON_LINE : POINT_QUALITY_OFF_LINE;
}

bool PointFlags::isInitial() const
{
    return initial;
}
void PointFlags::setInitial(const bool initial)
{
    this->initial = initial;
}

bool PointFlags::isInvalid() const
{
    return invalid;
}
void PointFlags::setInvalid(const bool invalid)
{
    this->invalid = invalid;
}

bool PointFlags::isOverflow() const
{
    return overflow;
}
void PointFlags::setOverflow(const bool overflow)
{
    this->overflow = overflow;
}

bool PointFlags::isChatter() const
{
    return chatter;
}
void PointFlags::setChatter(const bool chatter)
{
    this->chatter = chatter;
}

bool PointFlags::isSimulated() const
{
    return simulated;
}
void PointFlags::setSimulated(const bool simulated)
{
    this->simulated = simulated;
}


bool PointFlags::operator==(const PointFlags flags) const
{
    return ((quality   == flags.quality) &&
            (initial   == flags.initial  ) &&
            (invalid   == flags.invalid  ) &&
            (overflow  == flags.overflow ) &&
            (chatter   == flags.chatter) &&
            (simulated == flags.simulated)
            );
}


bool PointFlags::operator!=(const PointFlags flags) const
{
    return !(*this == flags);
}


bool PointFlags::isActive() const
{
    return ((quality == POINT_QUALITY_ON_LINE) &&
            (initial == LU_FALSE) &&
            (invalid == LU_FALSE) &&
            (chatter == LU_FALSE)
            );
}


const std::string PointFlags::toString() const
{
    std::string s("[");
    s.append((quality == POINT_QUALITY_INVALID)? "Iv" :
                (quality == POINT_QUALITY_ON_LINE)? "On" :
                    (quality == POINT_QUALITY_OFF_LINE)? "Of" : "??");
    s.append((initial  == LU_TRUE)? "R" : "-");
    s.append((invalid  == LU_TRUE)? "I" : "V");
    s.append((overflow == LU_TRUE)? "O" : "-");
    s.append((chatter  == LU_TRUE)? "C" : "-");
    s.append((simulated == LU_TRUE)? "S" : "-");
    s.append("]");

    return s;
}


bool PointData::isActive()
{
    return flags.isActive();
}


lu_bool_t PointData::compareFlags(const PointData* pointData)
{
    return (flags == pointData->getFlags());
}

lu_bool_t PointData::compareFlags(const PointFlags& pointFlags)
{
    return (flags == pointFlags);
}


PointFlags PointData::getFlags() const
{
    return flags;
}

void PointData::setFlags(const PointFlags pointFlags)
{
    flags = pointFlags;
}


void PointData::setTime(const TimeManager::TimeStr& timePtr)
{
    timeStamp = timePtr;
}

void PointData::getTime(TimeManager::TimeStr& timePtr) const
{
    timePtr = timeStamp;
}


POINT_QUALITY PointData::getQuality() const
{
    return flags.getQuality();
}

void PointData::setQuality(const POINT_QUALITY newQuality)
{
    flags.setQuality(newQuality);
}


EDGE_TYPE PointData::getEdge() const
{
    return edge;
}

void PointData::setEdge(const EDGE_TYPE newEdge)
{
    this->edge = newEdge;
}


AFILTER_RESULT PointData::getAFilterStatus() const
{
    return filterResult;
}

void PointData::setAFilterStatus(const AFILTER_RESULT result)
{
    filterResult = result;
}


bool PointData::getOnlineFlag() const
{
    return flags.isOnline();
}
void PointData::setOnlineFlag(const bool newOnline)
{
    flags.setOnline(newOnline);
}

bool PointData::getInitialFlag() const
{
    return flags.isInitial();
}
void PointData::setInitialFlag(const bool newInitial)
{
    flags.setInitial(newInitial);
}

bool PointData::getInvalidFlag() const
{
    return flags.isInvalid();
}
void PointData::setInvalidFlag(const bool newInvalid)
{
    flags.setInvalid(newInvalid);
}

bool PointData::getOverflowFlag() const
{
    return flags.isOverflow();
}
void PointData::setOverflowFlag(const bool newOverflow)
{
    flags.setOverflow(newOverflow);
}

bool PointData::getChatterFlag() const
{
    return flags.isChatter();
}
void PointData::setChatterFlag(const bool newChatter)
{
    flags.setChatter(newChatter);
}

bool PointData::getSimulatedFlag() const
{
    return flags.isSimulated();
}
void PointData::setSimulatedFlag(const bool newSimulated)
{
    flags.setSimulated(newSimulated);
}


lu_uint32_t PointData::getEventID() const
{
    return eventID;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */

