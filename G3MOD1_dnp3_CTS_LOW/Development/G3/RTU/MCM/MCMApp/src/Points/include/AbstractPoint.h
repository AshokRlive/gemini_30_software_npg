/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: AbstractPoint.h 9 Jul 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/Points/include/AbstractPoint.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Base class for all the Virtual Points
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 9 Jul 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   9 Jul 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef ABSTRACTPOINT_H__INCLUDED
#define ABSTRACTPOINT_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <map>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IPoint.h"
#include "IPointObserver.h"
#include "GeminiDatabase.h"
#include "LabelPool.h"
#include "PersistentVPManager.h"

/* Forward declaration */
class AbstractPoint;

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/* \brief Type of change to report
 *
 * This enum behaves like a bit field in order to specify what part really
 * changed in the provided data. This avoids a case like this example:
 * 0) Point status: (value,online) initial= (0,false)
 * 1) Change to online, enqueue change (0 <current>, true)
 * 2) Change value to 1, enqueue change (1, false <current>): note that the point is not yet online
 * 3) Dequeue change (0, true) => current= (0, true)
 * 4) Dequeue change (1, false) => current= (1, false) => Now the point is falsely reported as offline!
 *
 * Usage example:
 *  update(DATACHANGE dataChange)
 *  {
 *      if(dataChange & DATACHANGE_FLAGS)
 *          changeFlags(newFlags);
 *  }
 */
typedef enum
{
    DATACHANGE_NONE  = 0b00000000,  //No change
    DATACHANGE_VALUE = 0b00000001,  //Change in Value
    DATACHANGE_FLAGS = 0b00000010,  //Change in Flags
    DATACHANGE_BOTH  = 0b00000011,  //Change in both Value and Flags
    DATACHANGE_FREEZE= 0b00000100,  //Command a freeze in a counter
    DATACHANGE_FORCE = 0b10000000,  //Force the change (do not check if there is any change)
    DATACHANGE_ALL   = 0b00000111   //Change in all of the flags
} DATACHANGE;

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * Class to schedule a point update.
 *
 * This class schedules a point update using a database IJob.
 */
template <typename Type>
class PointEventJob : public IJob
{
public:
    /**
     * \brief Custom constructor
     *
     * \param pointObj Point to be reported to
     * \param pointData New value/status data change to be scheduled
     * \param dataChange Fields of data affected by the change
     */
    PointEventJob(AbstractPoint& pointObj,
                  Type& pointData,
                  DATACHANGE dataChange) :
                                                      point(pointObj),
                                                      data(pointData),
                                                      change(dataChange)
    {};
    virtual ~PointEventJob() {};

    /**
     * \brief Method executed by the job scheduler
     */
    virtual void run();

private:
    AbstractPoint& point;
    Type data;
    DATACHANGE change;
};


/**
 * \brief Periodically update the point (tickEvent)
 *
 * This periodic job is used to periodically update the point; usually used for
 * chatter, debounce...
 */
class PointPeriodicJob : public TimedJob
{
public:
    PointPeriodicJob(lu_uint32_t timer        ,
                     lu_uint32_t timerInterval,
                     AbstractPoint& point
                    ) :
                        TimedJob(timer, timerInterval),
                        point(point)
    {};
    virtual ~PointPeriodicJob() {};

protected:
    /* == Inherited from TimedJob == */
    virtual void job(TimeManager::TimeStr* timerPtr);

private:
    AbstractPoint& point;
};


/*!
 * \brief Base class for all the Virtual Points
 *
 * This class provides common functionality for all Virtual Points.
 * It is not intended to be used directly to provide a Virtual Point.
 */
class AbstractPoint: public IPoint
{
public:
    /**
     * \brief Point Labels list type
     */
    template <typename DataType>
    class PointLabels
    {
    public:
        /**
         * \brief Adds a label to the list
         */
        void addLabel(const DataType value, const LabelPool::LabelIndex index) { labelList[value] = index; }
        /**
         * \brief Finds the label associated with the given value
         */
        std::string find(const DataType value);
        //LabelPool::LabelIndex operator[](const DataType data) { return labelList[data]; }
    private:
        std::map<DataType, LabelPool::LabelIndex> labelList;    //List that bounds value with label index
    };

    /**
     * \brief Base Config for all virtual points
     */
    template <typename DataType>
    struct BaseConfig
    {
    public:
        /* WARNING: a VP without uniqueID cannot be persistent! */
        VPointUID uniqueID;           //Unique ID of this Virtual Point
        bool persistent;              //This point value is stored
        PointLabels<DataType> labels; //List of label indexes related with values

    public:
        BaseConfig<DataType>() : uniqueID(VPointUID_INVALID), persistent(false)
        {};
        /**
         * \brief add a label to the PointLabels list
         *
         * \param value Value bounded to the label
         * \param label Text of the label
         */
        void addLabel(const DataType value, const std::string& label);
    };
public:
    /**
     * \brief Custom constructor
     *
     * This is a pure virtual base class so it should not be possible to
     * instantiate it.
     *
     * \param globalMutex Global access mutex
     * \param g3database Gemini database reference
     * \param pointType Type of the point that is being created
     * \param ID Point ID
     */
    AbstractPoint(Mutex* globalMutex,
                  GeminiDatabase& g3database,
                  POINT_TYPE pointType,
                  PointIdStr ID,
                  const VPointUID uid = VPointUID_INVALID,
                  const bool persistent = false
                  );
    virtual ~AbstractPoint();

    /* == Inherited from IPoint == */
    virtual GDB_ERROR init() = 0;
    virtual PointIdStr getID() { return pointID; };
    virtual POINT_TYPE getType() { return type; };
    virtual PointFlags getFlags() = 0;
    virtual bool isActive();
    virtual bool isPersistent() { return persistent; };
    virtual GDB_ERROR getValue(PointData* valuePtr) = 0;
    virtual GDB_ERROR getRAWValue(PointData* valuePtr) = 0;
    virtual GDB_ERROR getFrozenValue(PointData* valuePtr) {return getValue(valuePtr); };
    virtual POINT_QUALITY getQuality() = 0;
    virtual GDB_ERROR freeze(const bool andClear = false);
    virtual GDB_ERROR clear();
    virtual GDB_ERROR attach(IPointObserver* observer);
    virtual GDB_ERROR detach(IPointObserver* observer);
    virtual void stopUpdate();

protected:
    /* Friend classes */
    template <typename> friend class PointEventJob;
    friend class PointPeriodicJob;

protected:
    /**
     * \brief Set point common flags
     *
     * Make this public in the intended Points.
     */
    virtual GDB_ERROR setFlags(const PointFlags& newFlags, const TimeManager::TimeStr& timestamp) = 0;

    /**
     * \brief Set point common flags
     *
     * Common implementation for common flags setting.
     */
    template <typename PointDataType>
    GDB_ERROR setFlags(const PointDataType pointData, const PointFlags& newFlags, const TimeManager::TimeStr& timestamp)
    {
        GDB_ERROR ret;
        if(!initialised)
        {
            return GDB_ERROR_NOT_INITIALIZED;
        }
        PointDataType newPointData = pointData;
        newPointData.setTime(timestamp);
        newPointData.setFlags(newFlags);
        ret = scheduleUpdate(*this, newPointData, DATACHANGE_FLAGS);
        if(ret != GDB_ERROR_NONE)
        {
            log.error("%s: Point (%s) - error: %s",
                        __AT__, pointID.toString().c_str(), GDB_ERROR_ToSTRING(ret)
                      );
        }
        return ret;
    }

    /**
     * \brief Set point online flag
     *
     * Common implementation for online flag setting.
     */
    template <typename PointDataType>
    GDB_ERROR setQuality(const PointDataType pointData, const lu_bool_t newStatus, const TimeManager::TimeStr& timestamp)
    {
        GDB_ERROR ret;
        if(!initialised)
        {
            return GDB_ERROR_NOT_INITIALIZED;
        }
        PointDataType newPointData = pointData;
        newPointData.setTime(timestamp);
        newPointData.setQuality((newStatus == LU_TRUE)? POINT_QUALITY_ON_LINE : POINT_QUALITY_OFF_LINE);
        ret = scheduleUpdate(*this, newPointData, DATACHANGE_FLAGS);
        if(ret != GDB_ERROR_NONE)
        {
            log.error("%s: Point (%s) - error: %s",
                        __AT__, pointID.toString().c_str(), GDB_ERROR_ToSTRING(ret)
                      );
        }
        return ret;
    }

    /**
     * \brief Update the point value/status
     *
     * \param data New value/status data
     * \param dataChange Fields of data affected by the change
     */
    virtual void update(PointData* data, DATACHANGE dataChange = DATACHANGE_BOTH) = 0;

    /**
     * \brief Obtain a copy of the current data
     *
     * This method obtains a copy of the current data as it is stored by the
     * data holder, without needing to store it temporally. It is intended to
     * be used when first updating the observer.
     *
     * THE USER OF THIS METHOD IS RESPONSIBLE TO DELETE THE CONTENT OF THE
     * RETURNED POINTER.
     *
     * \return Pointer to a copy of the current data
     */
    virtual PointData* getData() = 0;

    /**
     * \brief Periodic tick
     *
     * When used, it could update the point status (as in chatter, debounce...)
     *
     * \param timeStamp Current absolute timeStamp
     */
    virtual void tickEvent(TimeManager::TimeStr& timeStamp) = 0;

    /**
     * \brief Set a periodic Job to call the Tick Event
     *
     * This method creates a job to periodically call the tickEvent() method.
     *
     * \param periodicTime Periodic time interval, in milliseconds.
     *
     * \return Returns a pointer to the Job in case it is needed, or NULL when failed
     */
    virtual PointPeriodicJob* setPeriodic(const lu_uint32_t periodicTime);

    /**
     * \brief Update the observers in the observer queue
     *
     * Updates all the observers of the Virtual Point with the given data.
     *
     * \param newData Data used to report to the observers.
     */
    virtual void updateObservers(PointData* newData);

    /**
     * \brief Schedules a Database Job to update a point
     *
     * NOTE: the job created here will be automatically destroyed by the G3
     *      database Scheduler.
     *
     * \param database Gemini Database reference
     * \param point Point to be reported to
     * \param pointData New value/status data change to be scheduled
     * \param dataChange Fields of data affected by the change
     *
     * \return Error code
     */
    template <typename Type>
    inline GDB_ERROR scheduleUpdate(AbstractPoint& point,
                                    Type& pointData,
                                    DATACHANGE dataChange = DATACHANGE_BOTH)
    {
        PointEventJob<Type>* pEvent = new PointEventJob<Type>(point, pointData, dataChange);
        GDB_ERROR ret = database.addJob(pEvent);
        if (ret != GDB_ERROR_NONE)
        {
            //Failed to schedule: run now!
            pEvent->run();
            delete pEvent;
        }
        return ret;
    }

protected:
    static Mutex* mutex;    //Global Mutex
    GeminiDatabase& database;
    Logger& log;
    POINT_TYPE type;        //Point type
    PointIdStr pointID;     //Point ID
    VPointUID uniqueID;     //Unique ID
    bool persistent;        //Point value is stored
    bool initialised;       //Initialisation flag
    bool m_simulating;      //Simulation mode enabled

private:
    /* Observer queue */
    MasterMutex queueMutex;     //Per point Mutex to protect Observer Queue
    PointObserverQueue observerQueue;
};


/**
 * \brief Method executed by the job scheduler
 *
 * Note: defined here to allow update() call after having the AbstractPoint
 * object already defined.
 */
template <typename T>
void PointEventJob<T>::run()
{
    point.update(&data, change);
}


template<typename DataType>
inline std::string AbstractPoint::PointLabels<DataType>::find(const DataType value)
{
    typename std::map<DataType, LabelPool::LabelIndex>::iterator it = labelList.find(value);
    if(it != labelList.end())
    {
        return LabelPool::getInstance().getLabel(it->second);
    }
    return "";
}


template<typename DataType>
inline void AbstractPoint::BaseConfig<DataType>::addLabel(const DataType value,
                const std::string& label)
{
    LabelPool::LabelIndex idxLabel;
    idxLabel = LabelPool::getInstance().addLabel(label);
    labels.addLabel(value, idxLabel);   //Add to the map, using the value as a key
}

#endif /* ABSTRACTPOINT_H__INCLUDED */


/*
 *********************** End of file ******************************************
 */
