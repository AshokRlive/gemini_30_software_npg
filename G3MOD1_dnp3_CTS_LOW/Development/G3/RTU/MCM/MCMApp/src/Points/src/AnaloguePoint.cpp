/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Virtual analogue point implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "AnaloguePoint.h"
#include "Debug.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define DEBUG_ANALOGUE_VP 0
#if DEBUG_ANALOGUE_VP
    #define DBGLOG_VPA_HEADER   "VPA"
    #define DBGLOG_VPA_ERROR(text)                  DBG_ERR(DBGLOG_VPA_HEADER" %s %s", pointID.toString().c_str(), text)
    #define DBGLOG_MEM_READ(pointID, eventData)     DBG_INFO(DBGLOG_VPA_HEADER" %s Set Initial Value: %f", pointID.toString().c_str(), m_rawValue)
    #define DBGLOG_MEM_WRITE(pointID, eventData)    DBG_INFO(DBGLOG_VPA_HEADER" %s Set Initial Value: %f", pointID.toString().c_str(), m_rawValue)
#else
    #define DBGLOG_VPA_ERROR(text)
    #define DBGLOG_MEM_READ(pointID, eventData)
    #define DBGLOG_MEM_WRITE(pointID, eventData)
#endif

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
bool AnaloguePoint::OverRange::isValid()
{
    update();

    return( (upper && (threshold >= recovery)) ||
            (!upper && (threshold <= recovery))
            );
}


void AnaloguePoint::OverRange::update()
{
    if(upper)
    {
        recovery = threshold - hysteresis;
    }
    else
    {
        recovery = threshold + hysteresis;
    }
}


bool AnaloguePoint::Config::isValid()
{
    update();

    if(overflow.active)
    {
        if( !overflow.isValid() )
            return false;
    }
    if(underflow.active)
    {
        if( !underflow.isValid() )
            return false;
    }
    return true;
}


void AnaloguePoint::Config::update()
{
    //Set recovery threshold directions
    overflow.upper = true;
    underflow.upper = false;
}


AnaloguePoint::AnaloguePoint( Mutex* gMutex               ,
                              GeminiDatabase& g3database ,
                              AnaloguePoint::Config& config
                            ) :
                                AbstractPoint(gMutex, g3database,
                                                POINT_TYPE_ANALOGUE,
                                                config.ID,
                                                config.uniqueID,
                                                config.persistent),
                                m_rawValue(0),
                                m_region(REGION_NORMAL),
                                overflow(config.overflow),
                                underflow(config.underflow),
                                scale(config.scale),
                                offset(config.offset),
                                fullRange(config.fullRange),
                                m_labels(config.labels)
{
    if(persistent)
    {
        /* Get initial value if persistent */
        PointDataFloat32 eventData;
        lu_bool_t res;
        res = lucyMem::PersistentVPManager::getInstance().read(lucyMem::MemType_ANALOGUE, uniqueID, eventData);
        if(res == LU_FALSE)
        {
            log.debug("%s - Previous value not available, using default", pointID.toString().c_str());
            DBGLOG_VPA_ERROR("Previous value not available, using default");
        }
        else
        {
            m_rawValue = (AnaloguePointValue)eventData;
            m_data = eventData;
            log.debug("%s Set Initial Value: %f", pointID.toString().c_str(), m_rawValue);
            DBGLOG_MEM_READ(pointID, eventData);
        }
    }
    /* Initialise Analogue filter */
    filter = AnalogueFilterFactory::getInstance()->newFilter(config.filter);//MG check return
    TimeManager::TimeStr timeStamp;
    TimeManager::getInstance().getTime(timeStamp);
    m_data.setTime(timeStamp);    //Set default timestamp to current time
}

AnaloguePoint::~AnaloguePoint()
{
    stopUpdate();

    //Remove dynamic objects
    delete filter;
}


GDB_ERROR AnaloguePoint::getValue(PointData* valuePtr)
{
    if(valuePtr == NULL)
    {
        return GDB_ERROR_PARAM;
    }

    LockingMutex lMutex(*mutex);
    PointDataFloat32* value = (PointDataFloat32*)(valuePtr);
    *value = m_data;

    return GDB_ERROR_NONE;
}


GDB_ERROR AnaloguePoint::getRAWValue(PointData* valuePtr)
{
    //Default implementation
    if(valuePtr == NULL)
    {
        return GDB_ERROR_PARAM;
    }

    LockingMutex lMutex(*mutex);
    PointDataFloat32* value = (PointDataFloat32*)(valuePtr);
    *value = m_rawValue;

    return GDB_ERROR_NONE;
}


GDB_ERROR AnaloguePoint::getRange(PointData* minValuePtr, PointData* maxValuePtr)
{
    if( (minValuePtr == NULL) || (maxValuePtr == NULL) )
    {
        return GDB_ERROR_PARAM;
    }

    PointDataFloat32* minValue = (PointDataFloat32*)(minValuePtr);
    PointDataFloat32* maxValue = (PointDataFloat32*)(maxValuePtr);
    *minValue = fullRange.minValue;
    *maxValue = fullRange.maxValue;
    return GDB_ERROR_NONE;
}


std::string AnaloguePoint::getLabel(const PointData* valuePtr)
{
    PointDataFloat32* valPtr = (PointDataFloat32*)(valuePtr);
    lu_float32_t value = *valPtr;
    return m_labels.find(value);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
GDB_ERROR AnaloguePoint::setFlags(const PointFlags& newFlags, const TimeManager::TimeStr& timestamp)
{
    return AbstractPoint::setFlags(m_data, newFlags, timestamp);
}


PointData* AnaloguePoint::getData()
{
    PointDataFloat32* ret = new PointDataFloat32; //create container, to be destroyed by the caller
    LockingMutex lMutex(*mutex);
    *ret = m_data;
    return ret;
}


void AnaloguePoint::updateAnaloguePoint(PointData* newData, DATACHANGE dataChange)
{
    bool changed = false;       //A change in the point data
    bool overflowForced = false;//Overflow flag was forced from the flags settings
    PointDataFloat32 eventData; //data copy used to update observers
    const bool oldOverflow = m_data.getOverflowFlag();   //store previous flag

    /* Initial calculations */
    lu_float32_t valueRaw = *((PointDataFloat32*)newData);  //Raw value
    lu_float32_t reportedValue; //Measured value after the scaling factor has been applied
    reportedValue = offset + (scale * valueRaw);    //Scaled value

    /* Lock global mutex */
    {
        LockingMutex lMutex(*mutex);

        /* Check if the change affects this point */
        if( (dataChange & DATACHANGE_FLAGS) &&
            ( (m_data.compareFlags(newData) == LU_FALSE) || (dataChange & DATACHANGE_FORCE) )
          )
        {
            changed = true; //Flag change
            m_data.setFlags(newData->getFlags());
            if(newData->getOverflowFlag() == LU_TRUE)
            {
                overflowForced = true;  //force overflow from origin
            }
            else
            {
                m_data.setOverflowFlag(calcOverflow(reportedValue));
            }
        }
        if(dataChange & DATACHANGE_VALUE)
        {
            const lu_float32_t oldValue = m_data;
            m_rawValue = valueRaw;   //store latest reported raw value

            /* Always update the filter independently of value changes */
            AFILTER_RESULT newFilter;
            if(filter != NULL)
            {
                /* Apply analogue filter */
                newFilter = filter->run(reportedValue);
            }
            else
            {
                //No filter means that the value is always reported
                newFilter = AFILTER_RESULT_OUT_LIMIT_THR;
            }
            m_data.setAFilterStatus(newFilter);
            if(newFilter == AFILTER_RESULT_OUT_LIMIT_THR)
            {
                changed = true;
            }

            if( (oldValue != reportedValue) ||
                (m_data.getInitialFlag() == true) ||
                (dataChange & DATACHANGE_FORCE)
                )
            {
                changed = true; //value change

                m_data = reportedValue;   //Store the processed value, not the raw one
                /* Update value-related flags */
                m_data.setInitialFlag(newData->getInitialFlag());
                m_data.setInvalidFlag(newData->getInvalidFlag());

                /* Set overflow if necessary */
                if(!overflowForced)
                {
                    m_data.setOverflowFlag(calcOverflow(reportedValue));
                    /* Check flag change */
                    if(m_data.getOverflowFlag() != oldOverflow)
                    {
                        changed = true; //force event generation since flag has changed
                    }
                }
                //else: Channel is in overflow, so force overflow from origin (already done)

            }
        }
        if(changed)
        {
            TimeManager::TimeStr timeStamp;
            newData->getTime(timeStamp);
            m_data.setTime(timeStamp);
            eventData = m_data;           //copy from data
        }
    }//End of Mutex section

    /* Update all observers */
    if(changed)
    {
        updateObservers(&eventData);
        if(persistent)
        {
            /* Store persistent value */
            lu_bool_t res;
            res = lucyMem::PersistentVPManager::getInstance().write(lucyMem::MemType_ANALOGUE, uniqueID, eventData);
            if(res == LU_FALSE)
            {
                log.error("%s - Failed to store persistent value!", pointID.toString().c_str());
                DBGLOG_VPA_ERROR("- Failed to store persistent value!");
            }
        }
    }
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
bool AnaloguePoint::calcOverflow(const lu_float32_t reportedValue)
{
    bool result = false;

    /* Set overflow if necessary */
    if( overflow.active && (m_region != REGION_OVERFLOW) && (reportedValue > overflow.threshold) )
    {
        //Entering in overflow
        m_region = REGION_OVERFLOW;
        result = true;
    }
    else if( underflow.active && (m_region != REGION_UNDERFLOW) && (reportedValue < underflow.threshold) )
    {
        //Entering in underflow
        m_region = REGION_UNDERFLOW;
        result = true;
    }
    else if( overflow.active && (m_region == REGION_OVERFLOW) && (reportedValue > overflow.recovery) )
    {
        //Still in overflow
        result = true;
    }
    else if( underflow.active && (m_region == REGION_UNDERFLOW) && (reportedValue < underflow.recovery) )
    {
        //Still in underflow
        result = true;
    }
    else
    {
        //Not in over range
        m_region = REGION_NORMAL;
        result = false;
    }
    return result;
}


/*
 *********************** End of file ******************************************
 */
