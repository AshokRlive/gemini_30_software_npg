/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Single binary digital point interface.
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_AA814C00_A3E7_4300_B63A_F5410A6F115D__INCLUDED_)
#define EA_AA814C00_A3E7_4300_B63A_F5410A6F115D__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "DigitalPoint.h"
#include "IChannel.h"
#include "ChannelObserver.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/**
 * Custom implementation for Single Binary digital point.
 * The data source is an IOChannel.
 * Checks chattering but only for flag indication.
 */
class BinaryDigitalPoint : public DigitalPoint, private ChannelObserver
{
public:
    /**
     * \brief Single Binary Digital Point Configuration
     */
    struct BDConfig : public DigitalPoint::Config
    {
    public:
        IChannel* channelRef;   //Source channel
    public:
        BDConfig() : channelRef(NULL)
        {};
    };

public:
    BinaryDigitalPoint( Mutex *mutex                    ,
                        GeminiDatabase& database        ,
                        BDConfig &config
                      );

    virtual ~BinaryDigitalPoint();

    /* == Inherited from IPoint == */
    virtual GDB_ERROR init();

private:
    /* == Inherited from ChannelObserver == */
    virtual void update(IChannel *subject);

private:
    IChannel* channel;  //Source channel
    BDConfig config;    //Point configuration

    /* Channel status */
    lu_uint8_t lastChannelValue;        //Latest value reported from the channel
    POINT_QUALITY lastChannelQuality;   //Latest quality reported from the channel

};


#endif // !defined(EA_AA814C00_A3E7_4300_B63A_F5410A6F115D__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
