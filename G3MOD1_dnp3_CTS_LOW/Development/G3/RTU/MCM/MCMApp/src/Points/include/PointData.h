/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Virtual Point IOData interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   12/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_ED56FE68_2FAD_4b36_8CF5_D9D105A0169F__INCLUDED_)
#define EA_ED56FE68_2FAD_4b36_8CF5_D9D105A0169F__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "GlobalDefs.h"
#include "TimeManager.h"
#include "IAnalogueFilter.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef enum __attribute__((packed, aligned(1)))    //__attribute__ makes the enum 1-byte wide
{
    POINT_TYPE_INVALID,
    POINT_TYPE_ANALOGUE,
    POINT_TYPE_BINARY,  //Single Binary digital point
    POINT_TYPE_DBINARY, //Double Binary digital point
    POINT_TYPE_COUNTER,
    POINT_TYPE_LAST
}POINT_TYPE;

typedef enum __attribute__((packed, aligned(1)))
{
    POINT_QUALITY_INVALID,
    POINT_QUALITY_OFF_LINE,
    POINT_QUALITY_ON_LINE,
    POINT_QUALITY_LAST
}POINT_QUALITY;

typedef enum __attribute__((packed, aligned(1)))
{
    EDGE_TYPE_NONE  = 0,
    EDGE_TYPE_POSITIVE ,
    EDGE_TYPE_NEGATIVE ,

    EDGE_TYPE_LAST
}EDGE_TYPE;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
static TimeManager& TimeMgr = TimeManager::getInstance();

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/* TODO: pueyos_a - PointData storage improvements *
 * In the future, in the case of needing to reduce the point storage memory footprint,
 * there is a number of solutions to apply. However, take in mind that these
 * solutions may take the toll over the CPU usage, although this usage may be
 * small enough - being mainly bit and alignment-related operations.
 * Proposed solutions:
 * - #pragma pack(push, 1) packs bytes, reducing size but adding a small internal
 *      alignment calculation overhead.
 * - Gather all the boolean flags together into a byte (bit Field). Reduces size but adds
 *      LU_GETBIT() and LU_SETBITVALUE() to getters and setters that in turn have
 *      a bit-operation calculation overhead.
 * - Conversion of POINT_QUALITY, EDGE_TYPE, and AFILTER_RESULT enums into
 *      C++11 strongly typed enums:
 *          enum EDGE_TYPE {...};   becomes     enum class EdgeType : lu_uint8_t {None, ...};
 *          x=EDGE_TYPE_NONE;       becomes     x=EdgeType::None;
 *   Note that C++11 Strongly typed enums can be emulated by getters and setters,
 *   internally storing a uint8 instead but adding some overhead. Or by using
 *   GNU extensions' enum __attribute__((packed)) {}
 *
 *   Some of these solutions can be combined, but trading size for speed; and
 *   keep in mind that the size reduction might not be that much savings at the
 *   end.
 * For example, some experimental results yield to the following sizes:
 * Implementation           sizeof(PointData)   For 3000 points it adds:
 * ----------------------------------------------------------------------
 * initial implementation   36                  105K (0% save)
 * Packed                   33                  96K (8.3% save)
 * Using a bit field        32                  93K (11.1% save)
 * Using typed enums        28                  82K (22.2% save)
 * Using typed enums & pack 27                  79K (25% save)
 * All solutions            24                  70K (33.3% save)
 */



/**
 * Point flags structure.
 *
 * This structure contains the common flags associated with any point.
 *
 * The structure is packed by __attribute__ in order to reduce memory footprint.
 */
#pragma pack(push, 1)
struct PointFlags
{
public:
    static const bool VALID = true;
    static const bool INVALID = false;
public:
    PointFlags() :  quality(POINT_QUALITY_INVALID),
                    initial(true),
                    invalid(true),
                    overflow(false),
                    chatter(false),
                    simulated(false),
                    unused(0)
    {};
    PointFlags(const bool valid) :
                    quality((valid)? POINT_QUALITY_ON_LINE : POINT_QUALITY_OFF_LINE),
                    initial(!valid),
                    invalid(!valid),
                    overflow(false),
                    chatter(false),
                    simulated(false),
                    unused(0)
    {};
    PointFlags(const bool valid, const bool online) :
                    quality((online)? POINT_QUALITY_ON_LINE : POINT_QUALITY_OFF_LINE),
                    initial(!valid),
                    invalid(!valid),
                    overflow(false),
                    chatter(false),
                    simulated(false),
                    unused(0)
    {};

    virtual ~PointFlags() {};
    bool operator==(const PointFlags flags) const;
    bool operator!=(const PointFlags flags) const;

    /* Getters and setters */
    bool isOnline() const;
    void setOnline(const bool online);
    bool isInitial() const;
    void setInitial(const bool initial);
    bool isInvalid() const;
    void setInvalid(const bool invalid);
    bool isOverflow() const;
    void setOverflow(const bool overflow);
    bool isChatter() const;
    void setChatter(const bool chatter);
    bool isSimulated() const;
    void setSimulated(const bool simulated);

    /* FIXME: pueyos_a - Deprecated: */
    POINT_QUALITY getQuality() const;
    void setQuality(const POINT_QUALITY quality);

    /**
     * \brief Tells if the point is active
     *
     * The point is considered active if its activity flags indicate so. When the
     * flags initial, invalid, offline, and chatter(when applicable) are false,
     * it is considered as active.
     *
     * \return True when flags indicate that the point is active
     */
    virtual bool isActive() const;

    /**
     * \brief Gives away flag status in a C++ string
     *
     * The flag status is offered as a short string, representing each flag with
     * a letter in an specific position. For display purposes, some flags could
     * show letter/- or letter/letter.
     *
     * \return C++ String containing flag status, surrounded by square brackets.
     */
    virtual const std::string toString() const;

private:
    lu_uint8_t quality   : 2; //Point is online
    lu_uint8_t initial   : 1; //Value still has its initialisation value (was never updated).
    lu_uint8_t invalid   : 1; //Value is valid.
    lu_uint8_t overflow  : 1; //Value has overflowed/rolled over
    lu_uint8_t chatter   : 1; //Digital point is in chatter
    lu_uint8_t simulated : 1; //Value is being simulated locally.
    lu_uint8_t unused    : 1;
};


/**
 * This interface defines a general way to access different type of data
 * transparently. The assignment operator and the unary cast operator are
 * overloaded. It's possible to use the assignment operator to set/get the
 * internal value of the class. The appropriate casting is transparently applied.
 * General properties of a point (timestamp, edge, filter result) are directly
 * provided by this class.
 */
class PointData
{
public:
    PointData() :
                timeStamp(TimeMgr.getInitialTime()),
                edge(EDGE_TYPE_NONE),   //This sets filterResult to AFILTER_RESULT_NONE as well
                eventID(0)
    {};

    virtual ~PointData(){};

    /**
     * \brief Tells if the point is active
     *
     * The point is considered active if its activity flags indicate so. When the
     * flags initial, invalid, offline, and chatter(when applicable) are false,
     * it is considered as active.
     *
     * \return True when flags indicate that the point is active
     */
    virtual bool isActive();

    /**
     * \brief Compare all the flags against another pointData object
     *
     * \param pointData The other pointData to compare against
     *
     * \return LU_TRUE if both objects have the same flag values.
     */
    lu_bool_t compareFlags(const PointData* pointData);

    /**
     * \brief Compare all the flags against another pointData object
     *
     * \param pointFlags The point flags structure to compare against
     *
     * \return LU_TRUE if both objects have the same flag values.
     */
    lu_bool_t compareFlags(const PointFlags& pointFlags);

    /**
     * \brief Get the complete set of common flags
     *
     * \return Set of point flags.
     */
    PointFlags getFlags() const;

    /**
     * \brief Set all the point's flags
     *
     * \param pointFlags Set of flags to set
     */
    void setFlags(const PointFlags pointFlags);

	/**
	 * \brief Set the timestamp
	 * 
	 * \param timePtr Timestamp to set
	 */
    void setTime(const TimeManager::TimeStr& timePtr);

	/**
	 * \brief Get the timestamp
	 * 
	 * \param timePtr Where the timestamp is saved
	 */
    void getTime(TimeManager::TimeStr& timePtr) const;

    /* Flags Getters and setters */
    bool getOnlineFlag() const;
    void setOnlineFlag(const bool online);
    bool getInitialFlag() const;
    void setInitialFlag(const bool newInitial);
    bool getInvalidFlag() const;
    void setInvalidFlag(const bool newInvalid);
    bool getOverflowFlag() const;
    void setOverflowFlag(const bool overflow);
    bool getChatterFlag() const;
    void setChatterFlag(const bool chatter);
    bool getSimulatedFlag() const;
    void setSimulatedFlag(const bool simulated);

    EDGE_TYPE getEdge() const;
    void setEdge(const EDGE_TYPE edge);
    AFILTER_RESULT getAFilterStatus() const;
    void setAFilterStatus(const AFILTER_RESULT status);

    /* FIXME: pueyos_a - Deprecated: */
    POINT_QUALITY getQuality() const;
    void setQuality(const POINT_QUALITY newQuality);

    /**
     * \brief Set overflow flag
     *
     * \return Virtual Point Event ID
     */
    lu_uint32_t getEventID() const;

    /**
     * Operator overloads (to be defined in the children classes)
     */
    virtual PointData& operator=(lu_uint8_t a)   = 0;
    virtual PointData& operator=(lu_int8_t a)    = 0;
    virtual PointData& operator=(lu_uint16_t a)  = 0;
    virtual PointData& operator=(lu_int16_t a)   = 0;
    virtual PointData& operator=(lu_uint32_t a)  = 0;
    virtual PointData& operator=(lu_int32_t a)   = 0;
    virtual PointData& operator=(lu_float32_t a) = 0;
    virtual operator lu_int8_t()  = 0;
    virtual operator lu_uint8_t() = 0;
    virtual operator lu_int16_t() = 0;
    virtual operator lu_uint16_t() = 0;
    virtual operator lu_int32_t()  = 0;
    virtual operator lu_uint32_t()  = 0;
    virtual operator lu_float32_t()  = 0;

protected:
    TimeManager::TimeStr timeStamp;     //Time stamp of the last point change
    PointFlags  flags;
    union
    {
        EDGE_TYPE edge;                 //Digital change type
        AFILTER_RESULT filterResult;    //Analogue point filter status
    };

// TODO - SKA - eventID needs to be unique per Virtual Point Event - it will be the id used to store
//        the event in NVRAM (FRAM). This may not be the correct place for this and should be reviewed.
    lu_uint32_t eventID;
};
#pragma pack(pop)


template<class type>
class PointDataT : public PointData
{

public:
	PointDataT() : PointData() { val = static_cast<type>(0); };
	/* Copy constructor */
    PointDataT<type>(PointData& pointData) : PointData(pointData) {};
	virtual ~PointDataT() {};
	virtual PointDataT<type>& operator=(const PointDataT<type>& a)
    {
	    PointData::operator=(a);    //Assign all the PointData data (flags) using base class operator
	    val = PointDataT<type>(a);
	    return *this;
    }

	/* TODO: pueyos_a - assign/return current or simulated value depending on the flag??? */

    virtual PointData& operator=(lu_uint8_t a)
    {
        val = (type)(a);
        return *this;
    }

    virtual PointData& operator=(lu_int8_t a)
    {
        val = (type)(a);
        return *this;
    }

    virtual PointData& operator=(lu_uint16_t a)
    {
        val = (type)(a);
        return *this;
    }

    virtual PointData& operator=(lu_int16_t a)
    {
        val = (type)(a);
        return *this;
    }

    virtual PointData& operator=(lu_uint32_t a)
    {
        val = (type)(a);
        return *this;
    }

    virtual PointData& operator=(lu_int32_t a)
    {
        val = (type)(a);
        return *this;
    }

    virtual PointData& operator=(lu_float32_t a)
    {
        val = (type)(a);
        return *this;
    }

    virtual operator lu_int8_t()
    {
        return (lu_int8_t)(val);
    }

    virtual operator lu_uint8_t()
    {
        return (lu_uint8_t)(val);
    }

    virtual operator lu_int16_t()
    {
        return (lu_int16_t)(val);
    }

    virtual operator lu_uint16_t()
    {
        return (lu_uint16_t)(val);
    }

    virtual operator lu_int32_t()
    {
        return (lu_int32_t)(val);
    }

    virtual operator lu_uint32_t()
    {
        return (lu_uint32_t)(val);
    }

    virtual operator lu_float32_t()
    {
        return (lu_float32_t)(val);
    }

private:
    type val;
};

/* Usual types */
typedef lu_uint8_t DigitalPointValue;
typedef lu_float32_t AnaloguePointValue;
typedef lu_uint32_t CounterPointValue;
typedef PointDataT<DigitalPointValue> PointDataUint8;
typedef PointDataT<AnaloguePointValue> PointDataFloat32;
typedef PointDataT<CounterPointValue> PointDataCounter32;

#endif // !defined(EA_ED56FE68_2FAD_4b36_8CF5_D9D105A0169F__INCLUDED_)


/*
 *********************** End of file ******************************************
 */
