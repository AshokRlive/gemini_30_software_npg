/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: AbstractPoint.cpp 9 Jul 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/Points/src/AbstractPoint.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Implementation of the Base class for all the Virtual Points
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 9 Jul 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   9 Jul 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "AbstractPoint.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
#if defined(G3DBSERVER) && defined(PUBLISH_VIRTUAL_POINT)
#include "G3DBServer.h"
static void s_publishPointEvent(PointIdStr& id, POINT_TYPE type, PointData* newData);
#endif
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
Mutex* AbstractPoint::mutex = NULL;  //Global access mutex

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

AbstractPoint::AbstractPoint(Mutex* globalMutex,
                             GeminiDatabase& g3database,
                             POINT_TYPE pointType,
                             PointIdStr ID,
                             const VPointUID uid,
                             const bool isPersistent) :
                                database(g3database),
                                log(Logger::getLogger(SUBSYSTEM_ID_VPOINT)),
                                type(pointType),
                                pointID(ID),
                                uniqueID(uid),
                                persistent(isPersistent),
                                initialised(false),
                                m_simulating(false)
{
    /* Initialise static variables (only once) */
    if(this->mutex == NULL)
    {
        this->mutex = globalMutex;
    }

    /* Check conditions */
    if( persistent && (uniqueID == VPointUID_INVALID))
    {
        persistent = false; //Force persistency to false when no valid VP unique ID
        log.error("%s (%s) - persistent point does not have unique ID: disabling persistence!",
                    __AT__, pointID.toString().c_str());
    }
}

AbstractPoint::~AbstractPoint()
{
//    DBG_DTOR();
    stopUpdate();
}


bool AbstractPoint::isActive()
{
    if(!initialised)
    {
        return false;
    }
    PointFlags flags = this->getFlags();
    return flags.isActive();
}


GDB_ERROR AbstractPoint::freeze(const bool andClear)
{
    /* Default implementation */
    LU_UNUSED(andClear);

    return GDB_ERROR_NOT_SUPPORTED;
}


GDB_ERROR AbstractPoint::clear()
{
    /* Default implementation */
    return GDB_ERROR_NOT_SUPPORTED;
}


GDB_ERROR AbstractPoint::attach(IPointObserver* observer)
{
    GDB_ERROR ret = GDB_ERROR_NONE;

    if(observer == NULL)
    {
        return GDB_ERROR_PARAM;
    }

    /* Protect update using the queue mutex */
    MasterLockingMutex mMutex(queueMutex, LU_TRUE);

    /* Add observer to the queue */
    if (observerQueue.add(observer) == LU_FALSE)
    {
        ret = GDB_ERROR_ADDOBSERVER;
    }
    else
    {
        /* Observer correctly registered. Immediately synchronise the caller */
        {
            LockingMutex lMutex(*mutex);
            PointData* newData = getData();
            observer->update(pointID, newData);
            delete newData;
        }
    }
    return ret;
}


GDB_ERROR AbstractPoint::detach(IPointObserver* observer)
{
    GDB_ERROR ret = GDB_ERROR_NONE;

    if(observer == NULL)
    {
        return GDB_ERROR_PARAM;
    }

    /* Protect update using the queue mutex */
    MasterLockingMutex mMutex(queueMutex, LU_TRUE);

    if (observerQueue.remove(observer) == LU_FALSE)
    {
        ret = GDB_ERROR_ADDOBSERVER;
    }
    return ret;
}


void AbstractPoint::stopUpdate()
{
    // Protect the observer queue - priority
    MasterLockingMutex mMutex(queueMutex, LU_TRUE);

    /* Delete all observers */
    observerQueue.removeAll();
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
void PointPeriodicJob::job(TimeManager::TimeStr* timerPtr)
{
    /* Call the digital point periodic function */
    point.tickEvent(*timerPtr);
}


PointPeriodicJob* AbstractPoint::setPeriodic(const lu_uint32_t interval)
{
    if(interval == 0)
        return NULL;

    /* Allocate a periodic Job (will be disposed of by the Job Manager at the end) */
    PointPeriodicJob* job = new PointPeriodicJob(interval, interval, *this);
    /* Add the job to the DB scheduler */
    GDB_ERROR gdbRet = database.addJob(job);
    if(gdbRet != GDB_ERROR_NONE)
    {
        log.error("%s (%s) - add periodic job error: %s",
                  __AT__, pointID.toString().c_str(), GDB_ERROR_ToSTRING(gdbRet)
                  );
        delete job;
        return NULL;
    }
    return job;
}


void AbstractPoint::updateObservers(PointData* newData)
{
    IPointObserver *observerPtr;

    if(!initialised)
    {
        return;
    }

    /* Protect update using the global mutex */
    MasterLockingMutex mMutex(queueMutex);

    // Protect the data to ensure all observers update with same data
    LockingMutex lMutex(*mutex);

    observerPtr = observerQueue.getFirst();
    while(observerPtr != NULL)
    {
        /* Call observer */
        observerPtr->update(pointID, newData);

        /* get next observer */
        observerPtr = observerQueue.getNext(observerPtr);
    }

    /* Publish to the world*/
    #if defined(G3DBSERVER) && defined(PUBLISH_VIRTUAL_POINT)
       s_publishPointEvent(pointID, getType(), newData);
    #endif
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
#if defined(G3DBSERVER) && defined(PUBLISH_VIRTUAL_POINT)
extern g3db::G3DBServer g_dbserver;
static void s_publishPointEvent(PointIdStr& id,
                POINT_TYPE type, PointData* newData)
{
   g3db::DBEvent publish;
   g3db::PointEvent& evt = publish.pointEvent;
   TimeManager::TimeStr ts;

   publish.eventId = g3db::DB_EVENT_ID_VPOINT_UPDATE;

   // Event point ID
   evt.ref.id = id.ID;
   evt.ref.group = id.group;
   switch(type)
   {
       case POINT_TYPE_ANALOGUE:
       {
           evt.value.type = g3db::VALUE_TYPE_ANALOGUE;
           PointDataFloat32* avalue = (PointDataFloat32*)(newData);
           evt.value.analogue = *avalue;
       }
           break;

       case POINT_TYPE_BINARY:
       case POINT_TYPE_DBINARY:
       {
           PointDataUint8* bvalue = (PointDataUint8*)(newData);
           evt.value.digital = *bvalue;
           evt.value.type = g3db::VALUE_TYPE_DIGITAL;
       }
           break;

       case POINT_TYPE_COUNTER:
       {
           PointDataCounter32* cvalue = (PointDataCounter32*)(newData);
           evt.value.digital = *cvalue;
           evt.value.type = g3db::VALUE_TYPE_DIGITAL;
       }
           break;

       default:
           evt.value.type = g3db::VALUE_TYPE_INVALID;
   }

   // Event  flags
   evt.quality.online = (newData->getFlags().getQuality() == POINT_QUALITY_ON_LINE);


   // Timestamp
   newData->getTime(ts);
   evt.timestamp.value.tv_sec= ts.time.tv_sec;
   evt.timestamp.value.tv_nsec= ts.time.tv_nsec;

   g_dbserver.publish(publish);
}
#endif
/*
 *********************** End of file ******************************************
 */
