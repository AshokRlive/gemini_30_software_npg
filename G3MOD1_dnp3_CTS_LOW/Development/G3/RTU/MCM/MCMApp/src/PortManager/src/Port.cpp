/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:Port.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   30 Jun 2014     andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdio.h>   /* Standard input/output definitions */
#include <string.h> /* String function definitions */
#include <unistd.h> /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <time.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Port.h"
#include "LockingMutex.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

serialDTRGPIOStr serialPortDTR[] = { { "/dev/ttymxc4", MCMOutputGPIO_CONTROL_DTR},  // RS232-A
                                     { "/dev/ttymxc1", MCMOutputGPIO_CONFIG_DTR},   // RS232-B
                                     { NULL, 0 }
                                   };

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
Port::Port():
            log(Logger::getLogger(SUBSYSTEM_ID_PORT)),
            mutex(),
            inUse(LU_FALSE),
            handle(-1),
            state(PORT_STATE_LAST),
            serDTRGPIO(-1)
{
    // Default Settings
    portType = PORT_TYPE_SERIAL;
    portName = "";
    portPath = "";

    // TODO - SKA - These should be disassociated from the TMW library
    serialConf.serBaudrate 	= LIN232_BAUD_9600;
    serialConf.serDataBits 	= LIN232_DATA_BITS_8;
    serialConf.serStopBits 	= LIN232_STOP_BITS_1;
    serialConf.serParity   	= LIN232_PARITY_NONE;
    serialConf.serPortMode 	= LIN232_MODE_NONE;
    serialConf.serCTSlowCheck = LU_FALSE;

    commDevice = NULL;

    ethernetConf.ipDHCPClient       = LU_FALSE;
    ethernetConf.dhcpServer.enabled = LU_FALSE;

    inUse = LU_FALSE;
}

lu_int32_t Port::openPort()
{
    LockingMutex locker(mutex);


    // Only relevant to serial ports
    if (portType != PORT_TYPE_SERIAL)
    {
        return -1;
    }

    switch (state)
    {
        case PORT_STATE_OPEN  : //Port already opened
            break;

        case PORT_STATE_ERROR :// Port open failed
            break;

        default:// Try to open port

            handle = open(portPath.c_str(), O_RDWR | O_NOCTTY );  //O_NDELAY;
            if (handle < 0) {
                 /*
                * Could not open the port.
                */

                perror("open_port: Unable to open port");
                state = PORT_STATE_ERROR;
            }
            else
            {
//                fcntl(handle, F_SETFL, 0); // blocking //TODO move this to DialupModemSM ??
                state = PORT_STATE_OPEN;
            }
            break;
    }

    return handle;
}

void Port::closePort()
{
    LockingMutex locker(mutex);
    int ret;

    switch (state)
    {
        case PORT_STATE_OPEN  : //Port already open
            configForClosing();
            ret = close(handle);
            if(ret != 0)
                log.error("Port close error:%i",ret);

            break;

        default:
            break;
    }

    handle = -1;
    state = PORT_STATE_CLOSED;
}

void Port::setDTR(bool activate)
{
    FILE* gpioFd;

    if (serDTRGPIO != -1)
    {
        if (initGPIO(serDTRGPIO, GPIOLIB_DIRECTION_OUTPUT, &gpioFd) == 0)
        {
            writeGPIO(gpioFd, activate);
            closeGPIO(gpioFd);
        }
    }
}

void Port::setPortPath(const std::string& path)
{
    lu_uint32_t i;

    portPath = path;

    // TODO - SKA - We need to improve this class to have an applyConfig()
    // method where this sort of thing should occur.  There's no way to
    // determine if the GP portType has been set prior to this method
    // being called.

    // Only apply a DTR GPIO number to serial ports
    if (portType != PORT_TYPE_SERIAL)
    {
        return;
    }

    // Apply DTR GPIO number based on path of serial port
    for (i = 0; serialPortDTR[i].serialPathName != NULL; i++)
    {
        if (path.compare(serialPortDTR[i].serialPathName) == 0)
        {
            serDTRGPIO = serialPortDTR[i].DTRGPIONum;
        }
    }
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void Port::configForClosing()
{
    if(portType == PORT_TYPE_SERIAL)
    {
        struct termios options;

        setDTR(false);  //Drop DTR line if already up
        tcgetattr(handle, &options);

        options.c_cflag &= ~CRTSCTS;         // Disable hardware flow

        //Set speed to max (to speed up the process?)
        cfsetispeed(&options, B115200);
        cfsetospeed(&options, B115200);

        tcsetattr(handle, TCSANOW, &options);
    }
}


/*
 *********************** End of file ******************************************
 */
