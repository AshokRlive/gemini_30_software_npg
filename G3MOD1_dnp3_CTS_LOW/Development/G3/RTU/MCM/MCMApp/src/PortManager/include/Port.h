/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:Port.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   30 Jun 2014     andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef PORT_H_
#define PORT_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <string>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "CommsDevice.h"
#include "Mutex.h"
#include "Logger.h"
#include "MCMIOMap.h"

extern "C"
{
#include "tmwscl/utils/tmwdb.h"
#include "tmwscl/utils/tmwpltmr.h"
#include "tmwscl/utils/tmwtarg.h"

#include "tmwscl/dnp/dnpchnl.h"
#include "tmwscl/dnp/sdnpsesn.h"

#include "LinIoTarg/liniotarg.h"

#include "tmwscl/dnp/database/TMWSDNPDatabase.h" //MG Custom database structure
}


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define IP_ADDRESS_LEN      20
#define MAX_PORT_PATH_LEN   32
/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

struct serialDTRGPIOStr
{
    const char *serialPathName;
    lu_int32_t  DTRGPIONum;

};

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
/**
 * TODO split this class into two: Serial and Ethernet, derived from an abstract class
 */
class Port
{
public:
    enum PORT_STATE
    {
        PORT_STATE_CLOSED = 0,
        PORT_STATE_OPEN,
        PORT_STATE_ERROR,
        PORT_STATE_LAST
    };

    enum PORT_TYPE
    {
        PORT_TYPE_SERIAL,
        PORT_TYPE_ETHERNET,
        PORT_TYPE_USB,
        PORT_TYPE_LAST
    };

    struct DHCPServerStr
    {
        lu_bool_t     enabled;
        std::string   startAddress;
        std::string   endAddress;
        std::string   subnet;
        std::string   router;
    };

    struct EthernetConfStr
    {
        std::string   ipAddress;
        std::string   ipMask;
        std::string   ipBroadcast;
        std::string   ipGateway;
        std::string   ipDNS1;
        std::string   ipDNS2;

        // Ethernet Config
        lu_bool_t   ipDHCPClient;

        // DHCPServer Settings
        DHCPServerStr dhcpServer;

    };

    struct SerialConfStr
    {
        LIN232_BAUD_RATE serBaudrate;
        LIN232_DATA_BITS serDataBits;
        LIN232_STOP_BITS serStopBits;
        LIN232_PARITY    serParity;
        LIN232_PORT_MODE serPortMode;
        lu_bool_t		 serCTSlowCheck;
    };

public:
    Port();
    virtual ~Port() {};

    lu_bool_t getInUse()   { return inUse; }
    void      setInUse()   { inUse = LU_TRUE; }

    void        setPortPath(const std::string& path);
    std::string getPortPath() const { return portPath; };

    void        setPortName(const std::string& name)
    {
        portName = name;;
    }
    std::string getPortName() const { return portName; }

    lu_int32_t openPort();
    void closePort();

    /* /brief Change state of DTR line
     *
     * /param activate - True to activate DTR else false
     *
     * /returns N/A
     **/
    void setDTR(bool activate); // Only applicable to Serial Ports

public:
    // Configuration
    PORT_TYPE   portType;    // Type of Port   e.g. PORT_TYPE_ETHERNET

    SerialConfStr   serialConf;
    EthernetConfStr ethernetConf;

    CommsDevice *commDevice; // MODEM - Only for serial?

private:
    /**
     * \brief Prepares the serial port for fast closing
     *
     * When closing the port, the hardware control was forcing the OS to wait
     * until any RTS/CTS signal appears back from the other party. If there is
     * no modem/other party present, it hangs the execution for more than 20 secs.
     */
    void configForClosing();

private:
    Logger&     log;
    Mutex       mutex;
    bool        inUse;
    lu_int32_t  handle;     // Handle/file descriptor

    std::string portName;   // Name of device e.g. SERIAL_PORT_CONFIG
    std::string portPath;   // Path to device e.g. /dev/ttymxc3

    PORT_STATE  state;

    lu_int32_t serDTRGPIO;  // GPIO # for DTR line on serial ports
};



#endif /* PORT_H_ */

/*
 *********************** End of file ******************************************
 */
