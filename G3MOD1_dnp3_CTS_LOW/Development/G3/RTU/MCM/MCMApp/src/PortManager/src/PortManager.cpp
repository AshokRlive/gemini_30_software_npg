/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:PortManager.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   30 Jun 2014     andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h> // system()

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "PortManager.h"
#include "PortManagerDebug.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
std::string PortManager::setipCmd    = "/sbin/setip";
std::string PortManager::setdhcpdCmd = "/sbin/setdhcpd";

std::string PortManager::restartNetCmd  ="/sbin/lucy_restart_net";

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */


PortManager::PortManager() : log(Logger::getLogger(SUBSYSTEM_ID_SCADA_HANDLER))
{
}

PortManager::~PortManager()
{
    // Loop over the ports, deleting them
    for (PortVector::iterator itPort = ports.begin(); itPort != ports.end(); ++itPort)
    {
        delete *(itPort);
    }

    // Clear the vector
    ports.clear();
}

lu_int32_t PortManager::addPort(Port *port)
{
    std::string portCmd;

    // Search for duplicates
    if (getPort(port->getPortName()))
    {
        // Duplicate Port found!
        log.error("Port [%s] already exists", port->getPortName().c_str());
        return -1;
    }

    // If this is an ethernet port, set it up in the O/S
    if (port->portType == Port::PORT_TYPE_ETHERNET)
    {
        // DHCP Client
        portCmd = setipCmd;

        // Write config files without restarting networking
        // We can break the RTU if we call this script too quickly!
        portCmd += " -w ";
        portCmd += " -t " + port->getPortPath();

        // If the Interface is not DHCP we need to send the ipAddress etc
        if (port->ethernetConf.ipDHCPClient == LU_FALSE)
        {
            if (port->ethernetConf.ipAddress.length() > 0)
            {
                portCmd += " -i " + port->ethernetConf.ipAddress;
            }

            if (port->ethernetConf.ipMask.length() > 0)
            {
                portCmd += " -n " + port->ethernetConf.ipMask;
            }

            if (port->ethernetConf.ipGateway.length() > 0)
            {
                portCmd += " -g " + port->ethernetConf.ipGateway;
            }
        }

        DBG_INFO("Sending [%s] to the OS...\n", portCmd.c_str());

        // Call the O/S script to change the ip address etc
        system((const char *)portCmd.c_str());

        // DHCP Server (eth1 only!!)

        if (port->getPortPath() == "eth1")
        {
            portCmd = setdhcpdCmd;
            // Write config files without restarting networking
            // We can break the RTU if we call this script too quickly!
            portCmd += " -w ";

            if (port->ethernetConf.dhcpServer.enabled == true)
            {

                if (port->ethernetConf.dhcpServer.startAddress.length() > 0)
                {
                    portCmd += " -s " + port->ethernetConf.dhcpServer.startAddress;
                }

                if (port->ethernetConf.dhcpServer.endAddress.length() > 0)
                {
                    portCmd += " -e " + port->ethernetConf.dhcpServer.endAddress;
                }

                if (port->ethernetConf.dhcpServer.router.length() > 0)
                {
                    portCmd += " -g " + port->ethernetConf.dhcpServer.router;
                }

                if (port->ethernetConf.dhcpServer.subnet.length() > 0)
                {
                    portCmd += " -n " + port->ethernetConf.dhcpServer.subnet;
                }
            }
            else
            {
                // Disable the dhcp server
                portCmd += " -d";
            }

            DBG_INFO("Sending [%s] to the OS...\n", portCmd.c_str());

            // Call the O/S script to change the ip address etc
            system((const char *)portCmd.c_str());
        }

        // TODO - SKA - Add Broadcast, DNS1 & DNS2
    }

    ports.push_back(port);

    return 0;
}

Port *PortManager::getPort(std::string portName)
{
    for (PortVector::iterator itPort = ports.begin(); itPort != ports.end(); ++itPort)
    {
        if ((*itPort)->getPortName() == portName)
        {
            // TODO - SKA - Need to review this mechanism
            // If this is a Serial Port it can only be used by a single device
            // TODO - SKA - Consider an RS485 adapter on the RS232???
            if ((*itPort)->portType == Port::PORT_TYPE_SERIAL)
            {
                if ((*itPort)->getInUse() == LU_TRUE)
                {
                    log.error("Serial Port [%s] already in use by another device", (*itPort)->getPortName().c_str());
                    return NULL;
                }
            }

            // Mark as inUse!!!
            (*itPort)->setInUse();

            return *(itPort);
        }
    }

    return NULL;
}

void PortManager::applyConfig()
{
    std::string portCmd = restartNetCmd;

    // Restart networking as necessary
    DBG_INFO("Sending [%s] to the OS...\n", portCmd.c_str());

    // Call the O/S script to restart networking as appropriate
    system((const char *)portCmd.c_str());
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
