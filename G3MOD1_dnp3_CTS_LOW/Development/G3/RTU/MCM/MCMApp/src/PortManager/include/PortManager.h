/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:PortManager.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   30 Jun 2014     andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef PORTMANAGER_H_
#define PORTMANAGER_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <vector>
#include <string>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "Logger.h"
#include "Port.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class PortManager
{
    static std::string setipCmd;
    static std::string setdhcpdCmd;
    static std::string restartNetCmd;

public:
    PortManager();
    virtual ~PortManager();

    /**
      * \brief Add a port reference to the vector of ports
      *
      * This method calls the O/S Script setip for Ethernet ports,
      * creating a temporary Ethernet config file.  When applyConfig()
      * is called, the setip script will compare the temporary config files
      * with the existing ones and only apply the settings if they are different
      *
      * \param portPtr - Pointer to a Port
      *
      * \return Error code
      */
    lu_int32_t addPort(Port *portPtr);


    /**
      * \brief Return a Port pointer
      *
      * \param portName - Name of a port (e.g. "//dev//ttymxc3")
      *
      * \return Error code
      */
    Port *getPort(std::string portName);

    /*
     * \brief Apply the port configuration in the OS
     */
    void applyConfig();

private:
    typedef std::vector<Port*> PortVector;

private:

    // Vector of configured Ports
    PortVector ports;
    Logger& log;
};


#endif /* PORTMANAGER_H_ */

/*
 *********************** End of file ******************************************
 */
