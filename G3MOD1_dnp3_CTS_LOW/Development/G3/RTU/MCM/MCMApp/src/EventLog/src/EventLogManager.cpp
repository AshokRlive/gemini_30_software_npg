/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: EventLogManager.cpp 14-Nov-2012 09:38:35 pueyos_a $
 *               $HeadURL: G3\RTU\MCM\MCMApp\src\EventLog\src\EventLogManager.cpp $
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       EventLogManager module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 14-Nov-2012 09:38:35	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   14-Nov-2012 09:38:35  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

//#define AP_EVENTLOGFILE

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <string>
#include <string.h>
#include <stdio.h>  //debug

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "LockingMutex.h"
#include "EventLogManager.h"
#include "IMemoryManager.h"
#include "TimeManager.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/**
 * Fills the fields for an Event
 *
 * This macro takes an Event type and generates an event based on this type.
 * Usage example: EVENT_FILL(eventComm, event_payload);
 *
 * \param eventFieldName Name of the Event entry structure name (union payload field of SLEventEntry)
 * \param eventPayLoad Event entry structure
 */
#define EVENT_FILL(eventFieldName, eventPayLoad)\
{\
    SLEventEntryStr event;\
    event.timestamp.sec = 0;\
    event.eventClass = eventPayload.EventClass;\
    event.payload.eventFieldName = eventPayload;\
    logEvent(event);\
}

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/**
 * \brief List of events to be checked its previous presence in the event log
 */
/*
struct EventCheckStr
{
    //CTEventSystemStr x;
    SLEventEntryStr event;  //Event entry
    lu_bool_t check;
} eventCheck[] =
                {
                    { { {0,0}, EVENT_CLASS_OLR, {0, 0} }, LU_TRUE },
                    { { {0,0}, EVENT_CLASS_SYSTEM, {EVENT_TYPE_SYSTEM_ERROR, 0, 0} }, LU_TRUE },
                };
*/

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/* XXX: AP - debug events logged (to be removed) */
void debugPrintEvent(int evtype, SLEventEntryStr &event, int pos = -1);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/**
 * \brief Initialise static object instance
 */
EventLogManager EventLogManager::instance;
static IMemoryManager *memoryManager;


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

EventLogManager::EventLogManager() :
                                eventLogMaxEntries(0),
                                eventPos(EVENTLOGMAXENTRIES),
                                granted(LU_FALSE),
                                semOperation(LU_TRUE, 0),   //shared semaphore, acquired by default
                                eventLogLock(LU_TRUE)
{}

EventLogManager::~EventLogManager()
{
    delete [] eventLogList;
}

lu_bool_t EventLogManager::init(void *memoryMgr)
{
    if(memoryMgr == NULL)
    {
        return LU_FALSE;
    }
    granted = LU_FALSE;
//    semOperation.rstValue();
    LockingMutex lMutex(eventLogLock);
    memoryManager = (IMemoryManager *)memoryMgr;
    if( memoryManager->read(IMemoryManager::STORAGE_VAR_EVENTLOG,
                            (lu_uint8_t *)&eventLogMaxEntries,
                            NULL
                            ) != LU_TRUE)
    {
        //set to default value: entries will be held in memory only!
        eventLogMaxEntries = EVENTLOGMAXENTRIES;
    }
    eventLogList = new SLEventEntryStr[eventLogMaxEntries];     //create event list
    if(eventLogList == NULL)
    {
        //unable to create event log in memory!
//        log.error("%s Unable to create event log in memory: no space available.",
//                    __AT__);
        return LU_FALSE;
    }
    eventPos.maxEntries = eventLogMaxEntries;

    eventPos.clearPos();
    size_t readsize = 0;
    if( memoryManager->read(IMemoryManager::STORAGE_VAR_EVENT,
                            (lu_uint8_t *)eventLogList,
							&readsize
                            ) == LU_TRUE)
    {
    	eventPos.logSize = readsize;
        /* Adjust event log positions */
        //note: read() always returns the log entries from older to newer
        eventPos.allLogSize = eventPos.logSize;
        //set insert position after last entry
        eventPos.insertPos = eventPos.logSize % eventPos.maxEntries;
        //enable event writing
        granted = LU_TRUE;
//        semOperation.post();
        return LU_TRUE;
    }
    return LU_FALSE;
}


void EventLogManager::clearEventLog()
{
    granted = LU_FALSE;
//    semOperation.rstValue();            //disable event writing
    LockingMutex lMutex(eventLogLock);  //wait for writing to completion
    memoryManager->invalidate(IMemoryManager::MEM_TYPE_EVENT);
    eventPos.clearPos();
    granted = LU_TRUE;
//    semOperation.post();    //enable event writing
}


void EventLogManager::beginEventLog()
{
    granted = LU_FALSE;
//    semOperation.rstValue();            //disable event writing
    LockingMutex lMutex(eventLogLock);  //wait for writing to completion
    eventPos.beginPos();
    granted = LU_TRUE;
//    semOperation.post();    //enable event writing
}


void EventLogManager::logEvent(SLEventEntryStr &event)
{

    if(eventLogMaxEntries == 0)
    {
        return; //no event log, so nothing to do!
    }
    if(event.timestamp.sec == 0)
    {
        //No timestamp given: add it
        struct timespec newTime;    //to store current time
        TimeManager::getInstance().getTime(&newTime); //Get current time
        /* Convert it to specific field types */
        event.timestamp.sec = newTime.tv_sec;
        event.timestamp.nsec = newTime.tv_nsec;
    }

    /* TODO: AP - Semaphore to be removed - not a good idea to have a wait for a semaphore
     * in an operation that should not block the calling thread. Good for now... */
    if(granted == LU_FALSE)
    {
//        semOperation.wait();    //wait for any high-level operation to finish
    }
    LockingMutex lMutex(eventLogLock);  //Enter critical region

    /* TODO: AP - check (if needed to check) if it's previously in the event log */
    //if(eventCheck[])

    memcpy(&(eventLogList[eventPos.insertPos]), &event, sizeof(SLEventEntryStr));
    if(memoryManager != NULL)
    {
        memoryManager->write(IMemoryManager::STORAGE_VAR_EVENT, (lu_uint8_t *)&event);
    }
    eventPos.incInsertPos();
}


void EventLogManager::logEvent(CTEventSystemStr &eventPayload)
{
    eventPayload.unused = 0;
    EVENT_FILL(eventSystem, eventPayLoad);
}

void EventLogManager::logEvent(CTEventCommStr &eventPayload)
{
    eventPayload.unused = 0;
    EVENT_FILL(eventComm, eventPayLoad);
}

void EventLogManager::logEvent(CTEventOLRStr &eventPayload)
{
    eventPayload.unused = 0;
    EVENT_FILL(eventOLR, eventPayload);
}

void EventLogManager::logEvent(CTEventSwitchStr &eventPayload)
{
    EVENT_FILL(eventSwitch, eventPayload);
}

void EventLogManager::logEvent(CTEventCLogicStr &eventPayload)
{
    eventPayload.unused = 0;
    EVENT_FILL(eventCLogic, eventPayload);
}

void EventLogManager::logEvent(CTEventDatetimeStr &eventPayload)
{
    EVENT_FILL(eventDatetime, eventPayload);
}


EVENTLOG_ERROR EventLogManager::getEventMessages(lu_uint8_t* retBuffer,
                                                const lu_uint32_t maxEntries,
                                                lu_uint32_t& finalEntriesNum
                                                )
{
    finalEntriesNum = 0;

    if( (eventPos.logSize == 0) || (eventLogMaxEntries == 0) )
    {
        return EVENTLOG_ERROR_EMPTY;  //message list is empty
    }
    /* extract from event log buffer */
    static const lu_uint32_t entrySize = sizeof(SLEventEntryStr);   //size of any entry

    lu_uint32_t rest = maxEntries;  //control the space available on the buffer
    SLEventEntryStr *entryPtr;      //points to the extracted event log entry
    entryPtr = reinterpret_cast<SLEventEntryStr *>(retBuffer);

    LockingMutex lMutex(eventLogLock);  //Enter critical region for extracting
    while( (eventPos.logSize > 0) && (rest > entrySize) )
    {
        //extract 1 entry from the list and store it in the given buffer
        memcpy(entryPtr, &(eventLogList[eventPos.extractPos]), entrySize);
        ++entryPtr;
        --rest;
        eventPos.incExtractPos();
    }
    finalEntriesNum = maxEntries - rest;
    return EVENTLOG_ERROR_NONE;
}


#ifdef EVT_OBSER
EVENTLOG_ERROR EventLogManager::getEventMessage(lu_uint32_t sessionID,
                                                lu_char_t *retMessage,
                                                lu_uint32_t maxSize,
                                                lu_uint32_t *finalSize
                                                )
{
    *finalSize = 0;

    lu_uint32_t const entrySize = sizeof(SLEventEntryStr);   //size of any entry
    lu_uint32_t rest = maxSize;     //control the space available on the buffer
    SLEventEntryStr *entryPtr;      //points to the extracted event log entry
    SLEventEntryStr event;          //event entry local copy

    entryPtr = reinterpret_cast<SLEventEntryStr *>(retMessage);
    //LockingMutex lMutex(eventLogLock);
    if(memoryManager != NULL)
    {
        //extract 1 entry and store it in the given buffer
        while( (rest > entrySize) &&
                (memoryManager->read(IMemoryManager::STORAGE_VAR_EVENT, (lu_uint8_t *)&event) == LU_TRUE)
            )
        {
            memcpy(entryPtr, &event, entrySize);
            ++entryPtr;
            rest -= entrySize;
        }
        *finalSize = maxSize - rest;
    }
    return EVENTLOG_ERROR_NONE;
}
#endif






/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/* XXX: AP - debug events logged (to be removed) */
void debugPrintEvent(int evtype, SLEventEntryStr &event, int pos)
{
    fprintf(stdout, "%s logEvent[%d]: (%d, %d = %d) [%s]\r\n",
                    (evtype==0)?
                            "+++store" :
                        (evtype==1)?
                            "---report" :
                        (evtype==2)?
                            "***prev" :
                        (evtype==3)?
                            "###next" :
                            "",

                    pos,
                    event.eventClass,

                    (event.eventClass==EVENT_CLASS_SYSTEM)?
                                        event.payload.eventSystem.eventType :
                    (event.eventClass==EVENT_CLASS_OLR)?
                                    111 :
                    (event.eventClass==EVENT_CLASS_SWITCH)?
                                    event.payload.eventSwitch.eventType :
                                    200,

                    (event.eventClass==EVENT_CLASS_SYSTEM)?
                                    event.payload.eventSystem.value :
                    (event.eventClass==EVENT_CLASS_OLR)?
                                    event.payload.eventOLR.value :
                    (event.eventClass==EVENT_CLASS_SWITCH)?
                                    event.payload.eventSwitch.value :
                                    200,

                    (event.eventClass==EVENT_CLASS_SYSTEM)?
                        (event.payload.eventSystem.value == 0)? "startup" :
                        (event.payload.eventSystem.value == 4)? "restartReq" :
                        (event.payload.eventSystem.value == 17)? "readCfg" :
                        (event.payload.eventSystem.value == 19)? "door" : ""
                        :
                    (event.eventClass==EVENT_CLASS_OLR)?
                        (event.payload.eventOLR.value == 0)? "OLRoff" :
                        (event.payload.eventOLR.value == 1)? "OLRlocal" :
                        (event.payload.eventOLR.value == 2)? "OLRremote" : ""
                        :
                    (event.eventClass==EVENT_CLASS_SWITCH)?
                        (event.payload.eventSwitch.eventType == EVENT_TYPE_SWITCH_REQ_OPEN)? "ReqOpen" :
                        (event.payload.eventSwitch.eventType == EVENT_TYPE_SWITCH_REQ_CLOSE)? "ReqClose" :
                        (event.payload.eventSwitch.eventType == EVENT_TYPE_SWITCH_OPEN)? "Open" :
                        (event.payload.eventSwitch.eventType == EVENT_TYPE_SWITCH_CLOSE)? "Close" : ""
                        :
                        ""
            );
}



/*
 *********************** End of file ******************************************
 */
