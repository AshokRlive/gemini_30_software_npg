/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: EventLogFormat.h 21-Nov-2012 11:19:24 pueyos_a $
 *               $HeadURL: G3\RTU\MCM\MCMApp\src\EventLog\include\EventLogFormat.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Event Log format definitions.
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 21-Nov-2012 11:19:24	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   21-Nov-2012 11:19:24  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_A37F9381_71DF_4197_B121_F97BD84D3834__INCLUDED_)
#define EA_A37F9381_71DF_4197_B121_F97BD84D3834__INCLUDED_



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "LogCommon.h"
#include "EventTypeDef.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
#define EVENTLOGENTRYSIZE (sizeof(SLEventEntryDef))  //(timestamp+type+payload) 8+1+9 = 18 Bytes
#define EVENTLOGMAXSIZE 2250  //in Bytes
#define EVENTLOGMAXENTRIES (EVENTLOGMAXSIZE / sizeof(SLEventEntryDef))
*/
#define EVENTLOGMAXENTRIES 250   //5000 * 19Bytes = 95KB

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

#pragma pack(1) /* Force 1-byte alignment */


/* --- Point Reference --- */

/**
 * \brief G3 DB Point IDs
 */
struct CTG3DBPointIDStr
{
public:
    lu_uint16_t group;
    lu_uint16_t ID;
};

/**
 * \brief DNP Point IDs
 */
struct CTDNPPointIDStr
{
public:
    lu_uint16_t dnpID;
    lu_uint16_t unused;
};

/* ========= EVENT LOG ========= */

#define AP_NEWEVENT
//#ifdef AP_NEWEVENT




/* --- Point Status --- */

//struct CTAnaStatusG3DBStr //Gemini3 DB Analog point status
//{
//public:
//    lu_uint8_t unused : 4;
//    lu_uint8_t overflow : 1;
//    lu_uint8_t filterLimit : 1;
//    lu_uint8_t online : 1;
//    lu_uint8_t valid : 1;
//};
//struct CTDigStatusG3DBStr //Gemini3 DB Digital point status
//{
//public:
//    lu_uint8_t unused : 5;
//    lu_uint8_t chatter : 1;
//    lu_uint8_t online :1;
//    lu_uint8_t valid : 1;
//};
/*
struct CTAnaStatusDNP3Str //DNP3.0 Analog point status
{
public:
    lu_uint8_t online :1;
    lu_uint8_t restart :1;
    lu_uint8_t comm_lost :1;
    lu_uint8_t remote_forced :1;
    lu_uint8_t local_forced :1;
    lu_uint8_t overrange : 1;
    lu_uint8_t reference_err : 1;
    lu_uint8_t unused : 1;
};
struct CTDigStatusDNP3Str //DNP3.0 Digital point status
{
public:
    lu_uint8_t online :1;
    lu_uint8_t restart :1;
    lu_uint8_t comm_lost :1;
    lu_uint8_t remote_forced :1;
    lu_uint8_t local_forced :1;
    lu_uint8_t chatter : 1;
    lu_uint8_t unused : 2;
};
struct CTDigStatusDNP3Str //DNP3.0 Digital point status
{
public:
    lu_uint8_t online :1;
    lu_uint8_t restart :1;
    lu_uint8_t comm_lost :1;
    lu_uint8_t remote_forced :1;
    lu_uint8_t local_forced :1;
    lu_uint8_t rollover : 1;
    lu_uint8_t discontinuity : 1;
    lu_uint8_t unused : 1;
};
*/


/* --- DNP 3.0 Point --- */

/**
 * \brief Event DNP 3.0 point
 */
struct CTEventDNP3PointStr
{
    static const lu_uint32_t EventClass = EVENT_CLASS_DNP3SCADA_POINT;
public:
    lu_uint16_t pointType;  //Point Type. Valid values are EVENT_TYPE_DNP3SCADA enum
    lu_uint16_t dnpPointID;
    union valueDef
    {
        lu_uint32_t valueDig;   //Digital/Counter value
        lu_float32_t valueAna;  //Analogue value
    } value;
    lu_uint8_t status;      //DNP 3.0 status field
};


/* --- S104 Point --- */

/**
 * \brief Event for slave IEC 60870-5-101/104 point
 */
struct CTEventSi870PointStr
{
    static const lu_uint32_t EventClassS101 = EVENT_CLASS_S101_POINT;
    static const lu_uint32_t EventClassS104 = EVENT_CLASS_S104_POINT;
public:
    lu_uint32_t pointType : 8;   //Point Type. Valid values are EVENT_TYPE_S101 /S104 enum
    lu_uint32_t protocolID : 24; //IEC101/104 IOA
    union valueDef
    {
        lu_uint32_t valueDig;       //Digital value
        lu_float32_t valueAna;      //Analogue value
        lu_int16_t valueScaled;     //Analogue Scaled value
        lu_int32_t valueCounter;    //Counter value
    } value;
    lu_uint8_t status;      //flags field
};


/* --- General Events --- */

/**
 * \brief System Event
 */
struct CTEventSystemStr
{
    static const lu_uint32_t EventClass = EVENT_CLASS_SYSTEM;
public:
    lu_uint32_t eventType;
    lu_int32_t value;
    lu_uint8_t unused;
};

/**
 * \brief Communications Event
 */
struct CTEventCommStr
{
    static const lu_uint32_t EventClass = EVENT_CLASS_COMM;
public:
    lu_uint32_t device;
    lu_int32_t action;
    lu_uint8_t unused;
};

/**
 * \brief Off/Local/Remote
 */
struct CTEventOLRStr
{
    static const lu_uint32_t EventClass = EVENT_CLASS_OLR;
public:
    lu_uint8_t value;   /* EVENTOLR_VALUE values */
    /* TODO: AP - add source field to the event */
    //lu_uint8_t source;  /* EVENTOLR_SOURCE values */
    lu_uint64_t unused;
};

/**
 * \brief Switching Event
 */
struct CTEventSwitchStr
{
    static const lu_uint32_t EventClass = EVENT_CLASS_SWITCH;
public:
    lu_uint16_t eventType;
    lu_uint8_t moduleID;
    lu_uint8_t moduleType;
    lu_int32_t value;
    lu_uint8_t switchID;
};

/**
 * \brief Automation Event
 */
struct CTEventAutoStr
{
static const lu_uint32_t EventClass = EVENT_CLASS_AUTOMATION;
public:
    lu_uint16_t eventType; /* EVENT_TYPE_AUTOMATION*/
    lu_int32_t  unused;
};

/**
 * \brief Control Logic Event
 */
struct CTEventCLogicStr
{
static const lu_uint32_t EventClass = EVENT_CLASS_CLOGIC;
public:
    lu_uint16_t eventType;
    lu_uint16_t cLogicType;
    lu_int32_t value;
    lu_uint8_t unused;
};

/**
 * \brief Date and time change Event
 */
struct CTEventDatetimeStr
{
    static const lu_uint32_t EventClass = EVENT_CLASS_DATETIME;
public:
    TimeMilisStr timestamp;
    lu_uint8_t unused : 5;
    lu_uint8_t isdst : 2;   //daylight saving time flag
    lu_uint8_t sync : 1;    //synchronisation
};




/**
 * \brief Event log entry format
 */
struct SLEventEntryStr
{
    TimeSpecStr timestamp;  //time stamp of this entry
    lu_int8_t eventClass;    //type of event of this entry
    union payloadDef        //event info
    {
        CTEventSystemStr eventSystem;
        CTEventCommStr eventComm;
        CTEventOLRStr eventOLR;
        CTEventSwitchStr eventSwitch;
        CTEventCLogicStr eventCLogic;
        CTEventDatetimeStr eventDatetime;
        CTEventDNP3PointStr eventDNP3;      //slave DNP3
        CTEventSi870PointStr eventSi870;    //slave IEC 60870-5-101/104
        CTEventAutoStr      eventAuto;
    } payload;
};

#pragma pack()


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */



#endif // !defined(EA_A37F9381_71DF_4197_B121_F97BD84D3834__INCLUDED_)

/*
 *********************** End of file ******************************************
 */

