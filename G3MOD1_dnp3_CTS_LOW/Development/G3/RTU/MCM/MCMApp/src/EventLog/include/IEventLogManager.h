/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: IEventLogManager 21-Nov-2012 11:59:08 pueyos_a $
 *               $HeadURL: G3\RTU\MCM\MCMApp\src\EventLog\include\IEventLogManager.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       IEventLogManager Interface header module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 21-Nov-2012 11:59:08	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   21-Nov-2012 11:59:08  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_72BFF657_850D_43f6_B220_92702A628026__INCLUDED_)
#define EA_72BFF657_850D_43f6_B220_92702A628026__INCLUDED_


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/* XXX: AP - event list creation at init (to be removed) */
#define AP_EVENT_INIT


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "LogCommon.h"
#include "EventLogFormat.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#ifndef EVENTLOG_ERROR_NONE
typedef enum
{
    EVENTLOG_ERROR_NONE = 0,
    EVENTLOG_ERROR_EMPTY   ,    //list is empty

    EVENTLOG_ERROR_LAST
}EVENTLOG_ERROR;
#endif

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * Interface IEventLogManager
 */
class IEventLogManager
{

public:
	IEventLogManager() {}
	virtual ~IEventLogManager() {}

    /**
     * \brief Initialise the event log
     *
     * Initialise the Event Log by reading the stored one from non-volatile memory.
     *
     * \param memoryMgr pointer to the memory manager.
     * Declared as void pointer to avoid including the memory manager for each
     * event logger.
     */
#ifdef AP_EVENT_INIT
    virtual lu_bool_t init(void *memoryMgr) = 0;
#else
    virtual void init(void *memoryMgr) = 0;
#endif

    /**
     * \brief Clear the Event Log
     *
     * Deletes all entries in the Event Log.
     *
     * \return Error code
     */
    virtual void clearEventLog() = 0;

    /**
     * \brief Begin the Event Log
     *
     * Starts reading from the older entries of the Event Log.
     *
     * \return Error code
     */
    virtual void beginEventLog() = 0;

    /**
     * \brief Log an Event into the Event Log
     *
     * \param event Event to log.
     *
     * \return Error code
     */
    virtual void logEvent(SLEventEntryStr &event) = 0;

    /**
     * \brief Log a System Event
     *
     * Log an event giving only its content. Event type and time stamp will be
     * added automatically.
     *
     * \param event information to log.
     */
    virtual void logEvent(CTEventSystemStr &eventPayload) = 0;

    /**
     * \brief Log a Communications Event
     *
     * Log an event giving only its content. Event type and time stamp will be
     * added automatically.
     *
     * \param event information to log.
     */
    virtual void logEvent(CTEventCommStr &eventPayload) = 0;

    /**
     * \brief Log an Off/Local/Remote Event
     *
     * Log an event giving only its content. Event type and time stamp will be
     * added automatically.
     *
     * \param event information to log.
     */
    virtual void logEvent(CTEventOLRStr &eventPayload) = 0;

    /**
     * \brief Log a Switch Event
     *
     * Log an event giving only its content. Event type and time stamp will be
     * added automatically.
     *
     * \param event information to log.
     */
    virtual void logEvent(CTEventSwitchStr &eventPayload) = 0;

    /**
     * \brief Log a ControlLogic-related Event
     *
     * Log an event giving only its content. Event type and time stamp will be
     * added automatically.
     *
     * \param event information to log.
     */
    virtual void logEvent(CTEventCLogicStr &eventPayload) = 0;

    /**
     * \brief Log a date and time change Event
     *
     * Log an event giving only its content. Event type and time stamp will be
     * added automatically.
     *
     * \param event information to log.
     */
    virtual void logEvent(CTEventDatetimeStr &eventPayload) = 0;

};


#endif // !defined(EA_72BFF657_850D_43f6_B220_92702A628026__INCLUDED_)

/*
 *********************** End of file ******************************************
 */

