/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: EventLogManager.h 14-Nov-2012 09:38:34 pueyos_a $
 *               $HeadURL: G3\RTU\MCM\MCMApp\src\EventLog\include\EventLogManager.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       EventLogManager header module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 14-Nov-2012 09:38:34	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   14-Nov-2012 09:38:34  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_D1BE73FC_2CEC_40fc_B8E0_C6F879204D73__INCLUDED_)
#define EA_D1BE73FC_2CEC_40fc_B8E0_C6F879204D73__INCLUDED_


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Semaphore.h"
#include "Mutex.h"
#include "Thread.h"
#include "IEventLogManager.h"
#include "EventLogFormat.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * Class EventLogManager
 */
class EventLogManager : public IEventLogManager
{
private:
	EventLogManager();
	virtual ~EventLogManager();

    static EventLogManager instance;

public:
    /**
     * \brief Return an instance of the class
     *
     * \return Pointer to a class object
     */
    static EventLogManager* getInstance(void) {return &instance;};

    /**
     * \brief Initialise the event log
     *
     * Initialise the Event Log by reading the stored one from non-volatile memory.
     *
     * \param memoryMgr pointer to the memory manager.
     * Declared as void pointer to avoid including the memory manager for each
     * event logger.
     */
#ifdef AP_EVENT_INIT
    virtual lu_bool_t init(void *memoryMgr);
#else
    virtual void init(void *memoryMgr);
#endif

    /**
     * \brief Clears the Event log list.
     *
     * Eliminates all entries in the event log list.
     *
     * \return Error code
     */
    virtual void clearEventLog();

    /**
     * \brief Start to read event log entries from the beginning.
     *
     * Puts the extraction point back at the beginning of the Event log list,
     * if there is one.
     *
     * \return Error code
     */
    virtual void beginEventLog();


	/**
	 * \brief Log any Event into the Event Log
	 * 
	 * \param event Event to log.
	 */
	virtual void logEvent(SLEventEntryStr& event);

    /**
     * \brief Log a System Event
     *
     * Log an event giving only its content. Event type and time stamp will be
     * added automatically.
     *
     * \param event information to log.
     */
	virtual void logEvent(CTEventSystemStr &eventPayload);

    /**
     * \brief Log a Communications Event
     *
     * Log an event giving only its content. Event type and time stamp will be
     * added automatically.
     *
     * \param event information to log.
     */
    virtual void logEvent(CTEventCommStr &eventPayload);

    /**
     * \brief Log an Off/Local/Remote Event
     *
     * Log an event giving only its content. Event type and time stamp will be
     * added automatically.
     *
     * \param event information to log.
     */
    virtual void logEvent(CTEventOLRStr &eventPayload);

    /**
     * \brief Log a Switch Event
     *
     * Log an event giving only its content. Event type and time stamp will be
     * added automatically.
     *
     * \param event information to log.
     */
    virtual void logEvent(CTEventSwitchStr &eventPayload);

    /**
     * \brief Log a ControlLogic-related Event
     *
     * Log an event giving only its content. Event type and time stamp will be
     * added automatically.
     *
     * \param event information to log.
     */
    virtual void logEvent(CTEventCLogicStr &eventPayload);

    /**
     * \brief Log a date and time change Event
     *
     * Log an event giving only its content. Event type and time stamp will be
     * added automatically.
     *
     * \param event information to log.
     */
    virtual void logEvent(CTEventDatetimeStr &eventPayload);



    /**
	 * \brief Get Event Log entries from the internal buffer
	 * 
	 * Write a bunch of Event Log entries to the given buffer, up to the
	 * maximum given size.
	 *
	 * \param retBuffer Where to copy Event Log entries.
	 * \param maxEntries maximum amount of entries that can be put into the buffer.
	 * \param finalEntriesNum Amount of Event Log entries in the resultant buffer.
	 * 
	 * \return Error code
	 */
    virtual EVENTLOG_ERROR getEventMessages(lu_uint8_t* retBuffer,
                                            const lu_uint32_t maxEntries,
                                            lu_uint32_t& finalEntriesNum
                                            );

private:

#ifdef AP_EVENT_INIT
    SLEventEntryStr *eventLogList;      //event log list
    lu_uint32_t eventLogMaxEntries;     //max amount of entries
#else
    SLEventEntryStr eventLogList[EVENTLOGMAXENTRIES];   //event log list
#endif
    LogPosCtrlStr eventPos;     //event position tracking

    lu_bool_t granted;          //Variable to grant/revoke access to low-level operations (such as writing)
    Semaphore semOperation;     //Semaphore to make exclusive access on high-level operations (such as invalidation)
    Mutex eventLogLock;         //mutex for log operations

};


#endif // !defined(EA_D1BE73FC_2CEC_40fc_B8E0_C6F879204D73__INCLUDED_)

/*
 *********************** End of file ******************************************
 */

