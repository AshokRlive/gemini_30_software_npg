/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: MemServiceEvent.cpp 13-Feb-2013 11:39:52 pueyos_a $
 *               $HeadURL: U:\G3\RTU\MCM\src\MemoryManager\src\MemServiceEvent.cpp $
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MemServiceEvent module for handling Events storage in memory.
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 13-Feb-2013 11:39:52	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   13-Feb-2013 11:39:52  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <cstring>
#include <errno.h>
#include <unistd.h>
#include <sys/stat.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "GlobalDefs.h"     //Thread ID
#include "bitOperations.h"
#include "timeOperations.h"
#include "MemServiceEvent.h"
#include "EventTypeDef.h"
#include "crc4.h"
#include "IStatusManager.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
MemServiceEvent::MemServiceEvent(const lu_char_t *memoryDeviceName) :
                Thread(SCHED_TYPE_FIFO, EVENT_MEMORY_THREAD, LU_FALSE, "MemEventHandler"),
                pipeFIFO(Pipe::PIPE_TYPE_BLOCKREAD),    //Thread will block until there is something to read
                MemoryDevice(memoryDeviceName),
                memDevice(NULL),
                mainAccessLock(LU_TRUE),
//                memAccessLock(LU_TRUE),
                eventSize(0),
                eventEntrySize(0),
                eventPos(0),
                prepared(LU_FALSE),
                dirty(LU_FALSE)
{
}


MemServiceEvent::~MemServiceEvent()
{
    this->close();
}

void MemServiceEvent::setSize(IMemoryManager::STORAGE_VAR variable, size_t size)
{
    const lu_char_t *FTITLE = "MemEventHandler::setSize:";
    MasterLockingMutex lMutex(mainAccessLock, LU_TRUE);
    switch(variable)
    {
        case IMemoryManager::STORAGE_VAR_EVENTMEMSIZE:  //set memory size
            if(size == 0)
            {
                //Get device size
                struct stat deviceStatus;
                lu_int32_t result = stat(MemoryDevice, &deviceStatus);
                if (result == 0)
                {
                    memConfig.memorySize = deviceStatus.st_size;
                }
                else
                {
                    log.error("%s Error %i obtaining event memory size. Using default size: %i.",
                                FTITLE,
                                result,
                                memConfig.memorySize
                                );
                }
            }
            else
            {
                memConfig.memorySize = size;
            }
            log.info("%s Event Log memory size set to %i", FTITLE, memConfig.memorySize);
            /* Set max num of entries if event size already set */
            if(eventEntrySize != 0)
            {
                eventPos.maxEntries = memConfig.memorySize / eventEntrySize;
            }
            break;
        case IMemoryManager::STORAGE_VAR_EVENT: //set event content size (in bytes)
            eventSize = size;
            eventEntrySize = eventSize + sizeof(EventFileDef);  //=size(event+header)
            //get the maximum size available for complete entries (may leave a small space after)
            eventPos.maxEntries = memConfig.memorySize / eventEntrySize;
            log.info("%s Event size set to %i bytes.", FTITLE, eventEntrySize);
            break;
        case IMemoryManager::STORAGE_VAR_EVENTLOG:  //set (limit) event list total entries
            if( (eventSize > 0) && (eventPos.maxEntries > size) )
            {
                //Set the maximum amount of entries for the event list only if max entries fits in available memory
                eventPos.maxEntries = (memConfig.memorySize / eventEntrySize);  //max available
                eventPos.maxEntries = (eventPos.maxEntries >= size)? size : eventPos.maxEntries;
            }
            log.info("%s Event Log size set to %i event entries", FTITLE, eventPos.maxEntries);
            break;
        default:
            return;
    }
}


IMemService::MEMORYERROR MemServiceEvent::init()
{
    const lu_char_t *FTITLE = "MemEventHandler::init:";
    lu_uint8_t *eventBuf = NULL;    //stores 1 header + 1 event
    EventFileStr *fileEntry;        //pointer to event header
    LogPosCtrlStr evArrayPos(eventPos.maxEntries);  //array position control for already read entries

    lu_bool_t markFound = LU_FALSE;
    lu_bool_t fileError = LU_TRUE;
    fpos_t fCurrpos;                //current file position (before reading)

    if(prepared == LU_TRUE)
    {
        return IMemService::MEMORYERROR_NONE;     //already initialised
    }

    evArrayPos.clearPos();

    //disable access for writing
    MasterLockingMutex lMutex(mainAccessLock, LU_TRUE);

    /* Try to access and read log file */
    do
    {
        /* Open device for memory access */
        memDevice = fopen(MemoryDevice, "r+");
        if(memDevice == NULL)
        {
            log.error("%s error accessing Event log in device %s", FTITLE, MemoryDevice);
            if(discardMemLog(FTITLE) == LU_TRUE)
            {
                //unable to re-create file: abort
                return IMemService::MEMORYERROR_FRAM_ACCESS;
            }
            else
            {
                clearFile();
                fileError = LU_FALSE;       //avoid retrying initialisation
                break;                      //not needed to try to read the file
            }
        }
        clearFile();
        eventBuf = new lu_uint8_t[eventEntrySize];
        if(eventBuf == NULL)
        {
            fileError = LU_FALSE;
            log.error("%s Error reserving memory", FTITLE);
            break;
        }
        do
        {
            /* Find initial Event from file */
            fgetpos(memDevice, &fCurrpos);  //save event insert file position
            if(fread(eventBuf, eventEntrySize, 1, memDevice) != 1)
            {
                //EOF reached or error
                if(feof(memDevice) == 0)
                {
                    break;  //not EOF, error EPROTO
                }
                if(markFound == LU_FALSE)
                {
                    //latest entry mark not found: error in file when there are events
                    fileError = (evArrayPos.logSize != 0)? LU_TRUE : LU_FALSE;
                    break;
                }
                eventPos.logSize = evArrayPos.logSize;
                eventPos.allLogSize = evArrayPos.logSize;
                if( evArrayPos.logSize == evArrayPos.maxEntries )
                {
                    //log is full: older is always at insert position
                    eventPos.olderPos = eventPos.insertPos;
                    /* Note: at this point, fWritepos is already positioned at
                     * the entry that is next to fInsertpos. If there is no
                     * entry after "latest" mark, fWritepos was already at the
                     * beginning of the file.
                     */
                }
                else
                {
                    //log is not full: older should be 0
                    eventPos.olderPos = 0;
                    //and file writing position is the current one, at EOF
                    fWritepos = fCurrpos;
                }

                eventPos.beginPos();   //set the extract position to start at the older entry

                /* Check if the file pointer is at the end of the file & array */
                if( (eventPos.insertPos == 0) && (eventPos.allLogSize == eventPos.maxEntries) )
                {
                    //write position is at EOF: correct it to beginning of the file
                    fWritepos = fInitpos;
                }

                fileError = LU_FALSE;
                break;  //EOF: exit loop
            }
            else
            {
                fileEntry = reinterpret_cast<EventFileStr *>(eventBuf);
                /* check entry validity */
                if( LU_GETBIT(fileEntry->status, EVENTFILEDEF_INVALID_BS) == 0 )
                {
                    if( LU_GETBITMASKVALUE(fileEntry->status, EVENTFILEDEF_CRC4_BM, EVENTFILEDEF_CRC4_BS) ==
                                    crc4(eventBuf + sizeof(EventFileStr), eventSize)
                      )
                    {
                        //valid entry: process it
                        if( (markFound == LU_TRUE) && (eventPos.insertPos == evArrayPos.insertPos) )
                        {
                            //this is the entry following the one with the mark.
                            fWritepos = fCurrpos;   //store entry writing file position in this specific case
                        }
                        //check insert mark
                        if( LU_GETBIT(fileEntry->status, EVENTFILEDEF_LATEST_BS) != 0 )
                        {
                            //insertion mark found
                            if(markFound == LU_TRUE)
                            {
                                //insertion mark already found earlier: this entry is invalid!
                                updateHeader(fCurrpos,
                                                EVENTFILEDEF_LATEST_VAL_LATEST,
                                                (LU_GETBIT(fileEntry->status, EVENTFILEDEF_EOB_BS))?
                                                                EVENTFILEDEF_EOB_VAL_EOB :
                                                                EVENTFILEDEF_EOB_VAL_NORMAL,
                                                EVENTFILEDEF_INVALID_VAL_INVALID);
                            }
                            else
                            {
                                //first mark found: set as beginning
                                eventPos.insertPos = evArrayPos.insertPos;  //save this position
                                eventPos.incInsertPos();                    //save next position as insert position
                                fInsertpos = fCurrpos;          //store latest good entry file position
                                markFound = LU_TRUE;            //insertion mark found
                            }
                        }
                    }
                }
                //count this entry in any case (is present at file, invalid or not)
                evArrayPos.incInsertPos();
                if(evArrayPos.logSize > eventPos.maxEntries)
                {
                    //there are more file entries than configured: invalid memory
                    log.warn("%s found more entries than the amount configured: "
                                    "all entries in memory discarded.",
                                    FTITLE
                                    );
                    break;  //abort
                }
                //check last entry in file
                if(LU_GETBIT(fileEntry->status, EVENTFILEDEF_EOB_BS) != 0)
                {
                    /* EOB found */
                    if(markFound == LU_FALSE)
                    {
                        //latest entry mark not found: error in file when there are events
                        fileError = (evArrayPos.logSize != 0)? LU_TRUE : LU_FALSE;
                        break;
                    }
                    eventPos.logSize = evArrayPos.logSize;
                    eventPos.allLogSize = evArrayPos.logSize;
                    if( evArrayPos.logSize == evArrayPos.maxEntries )
                    {
                        //log is full: older is always at insert position
                        eventPos.olderPos = eventPos.insertPos;
                        //And fWritepos is already at the entry that is next to fInsertpos.
                        //If there is no entry after mark, fWritepos was already at the
                        //  beginning of the file.
                    }
                    else
                    {
                        //log is not full: older should be 0
                        eventPos.olderPos = 0;
                        //and file writing position is the current one, at EOF
                        fgetpos(memDevice, &fWritepos);
                    }

                    eventPos.beginPos();   //set the extract position to start at the older entry

                    /* Check if the file pointer is at the end of the file & array */
                    if( (eventPos.insertPos == 0) && (eventPos.allLogSize == eventPos.maxEntries) )
                    {
                        //write position is at EOF: correct it to the beginning of the file
                        fWritepos = fInitpos;
                    }

                    fileError = LU_FALSE;
                    break;  //EOF: exit loop
                }

            }
        } while(1);
    } while(0);
    if(fileError == LU_TRUE)
    {
        //error reading file: create from scratch
        log.warn("%s Invalid log in memory.", FTITLE);
        discardMemLog(FTITLE);
    }
    delete [] eventBuf;    //get rid of temporal buffer
    prepared = LU_TRUE;

    /* Start thread to receive writing operations */
    if(Thread::start() != THREAD_ERR_NONE)
    {
        return IMemService::MEMORYERROR_THREAD;
    }

    return ( (memDevice != NULL)? IMemService::MEMORYERROR_NONE : IMemService::MEMORYERROR_FRAM_ACCESS);
}


lu_bool_t MemServiceEvent::read(IMemoryManager::STORAGE_VAR variable, lu_uint8_t* value, size_t* sizeRead)
{
    const lu_char_t *FTITLE = "MemEventHandler::read:";
    lu_uint8_t *eventBuf = NULL;   //stores header + event
    EventFileStr *fileEntry;
    lu_bool_t fileError = LU_TRUE;
    lu_uint32_t entryNum = 0;   //count all the entries read
    lu_uint32_t validEntryNum = 0;   //count all the valid entries read

    if(variable == IMemoryManager::STORAGE_VAR_EVENTLOG)
    {
        //get configured size of the event log in memory (in maximum entries available)
        memcpy(value, &(eventPos.maxEntries), sizeof(eventPos.maxEntries));
        return LU_TRUE;
    }

    if( (memDevice == NULL) || (prepared == LU_FALSE) ||
        (variable != IMemoryManager::STORAGE_VAR_EVENT)
        )
    {
        return LU_FALSE;    //no file, or not initialised, or variable not supported
    }

    /* Read the COMPLETE Event Log from file */
    //disable any write operation
    MasterLockingMutex lMutex(mainAccessLock);
    eventBuf = new lu_uint8_t[eventEntrySize];
    if(eventBuf == NULL)
    {
        log.error("%s Error reserving memory", FTITLE);
        return LU_FALSE;
    }
    /* Position at the older entry */
    if(eventPos.olderPos == 0)
    {
        rewind(memDevice);  //next file position gets back to the beginning
    }
    else
    {
        fsetpos(memDevice, &fWritepos); //write position points to older entry
    }

    /* Read event entries from older to newer */
    do
    {
        if(fread(eventBuf, eventEntrySize, 1, memDevice) != 1)
        {
            //error reading: abort
            break;
        }
        else
        {
            ++entryNum; //always count a read
            if(entryNum > eventPos.maxEntries)
            {
                //there are more file entries than configured: create file from scratch
                log.warn("%s found more entries than the amount configured: "
                            "all entries in memory discarded.",
                            FTITLE
                            );
                break;  //abort
            }
            fileEntry = reinterpret_cast<EventFileStr *>(eventBuf);
            /* check valid entry (is marked valid and has correct CRC); ignore invalid entries */
            if( (LU_GETBIT(fileEntry->status, EVENTFILEDEF_INVALID_BS) == 0) &&
                ( LU_GETBITMASKVALUE(fileEntry->status, EVENTFILEDEF_CRC4_BM, EVENTFILEDEF_CRC4_BS) ==
                                crc4(eventBuf + sizeof(EventFileStr), eventSize))
                )
            {
                //valid entry: store entry in memory
                memcpy(value + (validEntryNum * eventSize), eventBuf + sizeof(EventFileStr), eventSize);
                validEntryNum++;
                //check insert mark
                if(LU_GETBIT(fileEntry->status, EVENTFILEDEF_LATEST_BS) != 0)
                {
                    prevFileEntry.status = fileEntry->status;   //save previous entry header
                    //insertion mark found (Note that, after init(), it should appear only 1 mark!)
                    fileError = LU_FALSE;   //end of reading procedure
                    break;
                }
                else
                {
                }
            }
            /* check last entry in file */
            if(LU_GETBIT(fileEntry->status, EVENTFILEDEF_EOB_BS) != 0)
            {
                /* EOB found implies either end of event log, or roll back to the beginning */
                if( entryNum < eventPos.allLogSize )
                {
                    rewind(memDevice);      //continue from the beginning of the file
                }
                else
                {
                    fileError = LU_FALSE;   //end of array: end of reading procedure
                    break;
                }
            }
        }
    } while(1);

    if(sizeRead != NULL)
    {
        //also return the size of the array (in number of elements)
        *sizeRead = (fileError == LU_FALSE)? entryNum : 0;
    }
    delete [] eventBuf;    //get rid of temporal buffer

    if(fileError == LU_TRUE)
    {
        log.warn("%s Invalid log in memory.", FTITLE);
        fileError = discardMemLog(FTITLE);
    }
    else
    {
        log.info("%s recovered %d valid events out of %d (max storage is %d events on %d bytes)",
                    FTITLE,
                    validEntryNum, eventPos.logSize,
                    eventPos.maxEntries, memConfig.memorySize
                    );
    }
    initialised = LU_TRUE;
    return (fileError == LU_FALSE)? LU_TRUE : LU_FALSE;   //error or success
}


lu_bool_t MemServiceEvent::write(IMemoryManager::STORAGE_VAR variable, lu_uint8_t* value)
{
    if( (memDevice == NULL) || (initialised == LU_FALSE) ||
        (variable != IMemoryManager::STORAGE_VAR_EVENT)
        )
    {
        return LU_FALSE;
    }
    pipeFIFO.writeP(value, eventSize);
    return LU_TRUE;
}


IMemService::MEMORYERROR MemServiceEvent::invalidate()
{
    MasterLockingMutex lMutex(mainAccessLock, LU_TRUE); //wait for last writing to complete
    discardMemLog("MemEventHandler::invalidate:");  //open a new one
    return ( (memDevice != NULL)? IMemService::MEMORYERROR_NONE : IMemService::MEMORYERROR_FRAM_ACCESS  );
}


void MemServiceEvent::flush()
{
    const lu_char_t *FTITLE = "MemEventHandler::flush:";

    MasterLockingMutex lMutex(mainAccessLock, LU_TRUE); //wait for any writing to complete
    if((initialised == LU_TRUE) && (memDevice != NULL) && (dirty == LU_TRUE) )
    {
        //device is initialised and there is data pending to be written
        log.debug("%s flushing memory of device %s", FTITLE, MemoryDevice);


        fflush(memDevice);
        fdatasync(fileno(memDevice));   //ask kernel to flush
        dirty = LU_FALSE;   //data is written, no data pending
    }
}


void MemServiceEvent::close()
{
    if(initialised == LU_TRUE)
    {
        /* stop thread */
        Thread::stop(); // stop event recorder thread
        MasterLockingMutex lMutex(mainAccessLock, LU_TRUE); //wait for any writing to complete
        Thread::join(); //wait for thread to complete writing

        /* close file and prevent future access */
        if(memDevice != NULL)
        {
            fclose(memDevice);
        }
        memDevice = NULL;
        initialised = LU_FALSE;
        //do not need to grant again
    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void MemServiceEvent::threadBody()
{
    const struct timeval timeout = {0, 500000};
    lu_uint8_t *evEntry = NULL;

    //reserve memory for 1 entry (retry until we have memory)
    do
    {
        evEntry = new lu_uint8_t[eventSize];
        if(evEntry == NULL)
        {
            lucy_usleep(1000);   //delay to avoid excessive hammering
        }
    } while( (evEntry == NULL) && (isRunning() == LU_TRUE) );

    /* Thread main loop */
    while(isRunning() == LU_TRUE)
    {
        lucy_usleep( 10000 );   //Delay to avoid system lockup - temporal fix
        /* block the thread waiting for new data */
        lu_int32_t result = pipeFIFO.readP(evEntry, eventSize, timeout);
        if(result == 0)
        {
            //We do not exit thread until at least this entry is written
            if(isInterrupting() == LU_TRUE)
                continue;   //exit thread
            MasterLockingMutex lMutex(mainAccessLock); //prevent any interruption during writing
            writeToMemory(evEntry);
        }
        else if(result == 1)
        {
            //timed out
            continue;
        }
        else
        {
            //error reading
            log.debug("MemEventHandler::threadBody: error reading Event Entry");
        }
    }
    delete [] evEntry;
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

lu_bool_t MemServiceEvent::writeToMemory(lu_uint8_t* value)
{
    const lu_char_t *FTITLE = "MemEventHandler::write:";
    lu_uint8_t *eventBuf = NULL;   //stores 1 header + 1 event
    EventFileStr *fileEntry;
    LogPosCtrlStr nextEventPos(eventPos.maxEntries);    //pre-calculation of next entry log position
    fpos_t fTemppos;     //current event entry file position


    if(memDevice == NULL)
    {
        return LU_FALSE;
    }

    //Reserve space for temporal storage of header + event
    eventBuf = new lu_uint8_t[eventEntrySize];
    if(eventBuf == NULL)
    {
        log.error("%s Error reserving memory", FTITLE);
        return LU_FALSE;
    }

    /* position file pointer */
    fsetpos(memDevice, &fWritepos);
    fTemppos = fWritepos;

    /* make a copy of the entry to add header */
    fileEntry = reinterpret_cast<EventFileStr *>(eventBuf);
    memcpy(eventBuf + sizeof(EventFileStr), value, eventSize);  //copy event over write buffer

    /* set header values */
    memset(eventBuf, 0, sizeof(EventFileStr));                  //clear header
    LU_SETBIT(fileEntry->status, EVENTFILEDEF_LATEST_BM);       //activate insertion mark
    LU_SETBITMASKVALUE(fileEntry->status, EVENTFILEDEF_CRC4_BM, //write CRC
                 crc4(eventBuf + sizeof(EventFileStr), eventSize), EVENTFILEDEF_CRC4_BS);

    /* calculate next event position tracking */
    nextEventPos = eventPos;
    nextEventPos.incInsertPos();
    if( ( (nextEventPos.insertPos == 0) && (nextEventPos.allLogSize == eventPos.maxEntries) ) ||
     (nextEventPos.allLogSize < eventPos.maxEntries)
    )
    {
        //when this write it's about to roll back, or the log is not about to be full:
        LU_SETBIT(fileEntry->status, EVENTFILEDEF_EOB_BM);     //activate end of buffer mark on this entry
    }


     /* print event content (in hex) to debug log */
    if(log.isDebugEnabled() == LU_TRUE)
    {
        //compose message only when we already are in debug mode
        lu_uint32_t j;
        lu_char_t *evEntry = new lu_char_t[(eventEntrySize*3) + 4];
        sprintf(evEntry, "0x ");
        for(j=0; j<eventEntrySize; j++)
        {
            snprintf((evEntry+3)+(j*3), 4, "%02x ", eventBuf[j]);
        }
        evEntry[3+(j*3)] = '\0';
        log.debug("%s written Event Entry @ [%d]: %s", FTITLE, eventPos.insertPos, evEntry);
        delete [] evEntry;
    }

    /* write new entry */
    if(fwrite(eventBuf, eventEntrySize, 1, memDevice) != 1)
    {
        //error writing
        delete [] eventBuf;    //get rid of temporal buffer
        log.error("%s Error %i writing to Event Memory device %s", FTITLE, errno, MemoryDevice);
        return LU_FALSE;
    }

    /* Update previous entry flags */
    if(nextEventPos.insertPos == 0)
    {
        //next is first entry: buffer rolled back
        rewind(memDevice);  //next file position gets back to the beginning
    }

    if(eventPos.allLogSize > 0)
    {
        /* delete "latest" mark on previous position. */
        updateHeader(fInsertpos, EVENTFILEDEF_LATEST_VAL_NORMAL,
                     ( ((nextEventPos.allLogSize == eventPos.maxEntries) &&
                             (eventPos.insertPos == 0) ) ||          //buffer rolled back: keep EOB in previous entry
                         LU_FALSE //(eventPos.allLogSize < eventPos.maxEntries) //buffer not full
                     )? EVENTFILEDEF_EOB_VAL_EOB : EVENTFILEDEF_EOB_VAL_NORMAL
                 );
    }
    eventPos.incInsertPos();        //increase event log size & position tracking
    dirty = LU_TRUE;                //there is data pending to be written
    prevFileEntry = *fileEntry;     //keep a copy of this entry as previous
    fInsertpos = fTemppos;          //store last entry file position
    fgetpos(memDevice, &fWritepos); //save writing point file position
    delete [] eventBuf;    //get rid of temporal buffer

    return LU_TRUE;
}

void MemServiceEvent::updateHeader(fpos_t &filePos,
                                    EVENTFILEDEF_LATEST_VAL latest,
                                    EVENTFILEDEF_EOB_VAL eob,
                                    EVENTFILEDEF_INVALID_VAL invalidate
                                    )
{
    fpos_t fCurrpos;        //current file position (before reading)
    EventFileStr fileEntry; //file entry header

    /* Try to delete mark on given position. When error, just leave */
    if(memDevice == NULL)
    {
        return; //file isn't open
    }
    do
    {
        fgetpos(memDevice, &fCurrpos); //store current position
        //go to given position
        if(fsetpos(memDevice, &filePos) != 0)
        {
            break; //error
        }
        /* copy the previous file entry header and update it */
        fileEntry = prevFileEntry;
        /* activate end of buffer mark when next position is the beginning of the file (circular buffer) */
        LU_SETBITVALUE(fileEntry.status, EVENTFILEDEF_EOB_BS, (eob == EVENTFILEDEF_EOB_VAL_EOB));
        if(invalidate == EVENTFILEDEF_INVALID_VAL_INVALID)
                {
                    LU_CLEARBIT(fileEntry.status, EVENTFILEDEF_LATEST_BM);  //delete mark
                    LU_SETBIT(fileEntry.status, EVENTFILEDEF_INVALID_BM); //set as invalid
                }
                else
                {
                    //set latest entry mark value as the given one
                    LU_SETBITVALUE(fileEntry.status, EVENTFILEDEF_LATEST_BS,
                                    (latest == EVENTFILEDEF_LATEST_VAL_LATEST)
                                    );
                    LU_CLEARBIT(fileEntry.status, EVENTFILEDEF_INVALID_BM);  //set as valid
                }
        //overwrite entry header
        fwrite(&fileEntry, sizeof(EventFileStr), 1, memDevice);
    } while(0);

    fsetpos(memDevice, &fCurrpos);  //restore current file writing position
}


lu_bool_t MemServiceEvent::discardMemLog(const lu_char_t *FTITLE)
{
    lu_bool_t fileError = LU_TRUE;

    //error reading file: create from scratch
    log.warn("%s Creating a new event log in non-volatile memory.", FTITLE);
    if(memDevice != NULL)
    {
        fclose(memDevice);  //close if previously open
    }
    memDevice = fopen(MemoryDevice, "w");
    if(memDevice != NULL)
    {
        clearFile();
        fileError = LU_FALSE;           //successful re-creation
        /* write an invalid entry with EOB mark */
        prevFileEntry.status = 0;
        LU_SETBIT(prevFileEntry.status, EVENTFILEDEF_INVALID_BM); //set as invalid
        LU_SETBIT(prevFileEntry.status, EVENTFILEDEF_EOB_BM);     //activate end of buffer mark
        fwrite(&prevFileEntry, sizeof(EventFileStr), 1, memDevice); //write header only
        rewind(memDevice);
    }
    else
    {
        log.error("%s New event log creation failed.", FTITLE);
    }
    return fileError;
}

void MemServiceEvent::clearFile()
{
    if(memDevice != NULL)
    {
        rewind(memDevice);          //force file position at the beginning
        fgetpos(memDevice, &fInitpos);
        fWritepos = fInitpos;       //default writing point file position
        fInsertpos = fInitpos;      //default event insert file position
        eventPos.clearPos();        //setting position tracker to default
    }
}

/*
 *********************** End of file ******************************************
 */

