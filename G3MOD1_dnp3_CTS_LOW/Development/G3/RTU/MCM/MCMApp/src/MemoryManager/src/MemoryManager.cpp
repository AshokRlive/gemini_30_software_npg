/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: MemoryManager.cpp 26-Oct-2012 10:04:56 pueyos_a $
 *               $HeadURL: Y:\workspace\G3Trunk\G3\RTU\MCM\src\MemoryManager\src\MemoryManager.cpp $
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MemoryManager module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 26-Oct-2012 10:04:56	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   26-Oct-2012 10:04:56  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <limits>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "MemoryManager.h"
#include "timeOperations.h"

//Memory Service Providers
#include "MemServiceNVRAMID.h"
#include "MemServiceNVRAM.h"
#include "MemServiceFile.h"
#include "MemServiceFRAM.h"
#include "MemServiceEvent.h"

//Includes for Storage type size
#include "IOModuleCommon.h"
#include "NVRAMDefInfo.h"   //for Storage type size
#include "EventLogFormat.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

//time interval for Memory Manager's tick event (in ms)
#define MEMMGR_TICKEVENT_MS 100

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 * Interval of time (ms) for automatic memory flush on each Service Provider.
 * Set to 0 for never.
 * Minimum resolution set by MEMMGR_TICKEVENT_MS.
 */
typedef enum
{
    MEM_FLUSHTIME_NONE       = 0,        //No automatic storage
    MEM_FLUSHTIME_EVENT      = 10000,    //10000 = 10s
    MEM_FLUSHTIME_NVRAMID    = 0,        //Never automatically updated
    MEM_FLUSHTIME_NVRAM      = 100,      //100ms (fastest possible)
    MEM_FLUSHTIME_FRAM       = 1200000,  //1200000ms = 20min
    MEM_FLUSHTIME_FILE       = 60000,    //60000ms = 1min
    MEM_FLUSHTIME_LAST
} MEM_FLUSHTIME;

/**
 * Memory flush interval config
 */
struct memFlushTimeStr
{
    MEM_FLUSHTIME interval;  //default interval
    lu_uint32_t remainingTime;      //remaining time for flushing
};


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/* Automatic Flush: flush time tracking for each Service Provider:
 *      Service Provider's flush time interval in ms (minimum resolution set by MEMMGR_TICKEVENT_MS),
 *      remaining time in ms (to be set automatically, so leave as 0)
 */
static memFlushTimeStr flushTimeService[IMemoryManager::MEM_TYPE_LAST] = {
    {MEM_FLUSHTIME_NONE,     0}, //MEM_TYPE_NONE
    {MEM_FLUSHTIME_EVENT,    0}, //Event memService
    {MEM_FLUSHTIME_NVRAMID,  0},
    {MEM_FLUSHTIME_NVRAM,    0},
    {MEM_FLUSHTIME_FRAM,     0},
    {MEM_FLUSHTIME_FILE,     0}
};

static const lu_char_t* MEM_NAMES[IMemoryManager::MEM_TYPE_LAST] =
{
	"None",
	"Event FRAM",
	"ID NVRAM",
	"APP NVRAM"
	"FRAM",
	"File"
};

#define MEM_NAME(id) ( (id < IMemoryManager::MEM_TYPE_LAST)? MEM_NAMES[id] : "unknown")

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

MemoryManager::MemoryManager(): AbstractMemoryManager(),
                                log(Logger::getLogger(SUBSYSTEM_ID_MEMORYMAN)),
                                refreshTimer(NULL),
                                tickInterval(0),
                                tick_remainingTime(MEMMGR_TICKEVENT_MS)   //tickEvent time
{
    /* Configure storage element's sizes */
    const size_t ReducedEnumSize = sizeof(lu_uint8_t); //Reduced size for normal enums (less than 256 items)
    setSize(IMemoryManager::STORAGE_VAR_ID, sizeof(NVRAMInfoStr));
    setSize(IMemoryManager::STORAGE_VAR_MODULEUID, sizeof(ModuleUID));
    setSize(IMemoryManager::STORAGE_VAR_FEATUREREV, sizeof(BasicVersionStr));
    setSize(IMemoryManager::STORAGE_VAR_EVENT, sizeof(SLEventEntryStr));
    setSize(IMemoryManager::STORAGE_VAR_OLR, ReducedEnumSize); //OLR_STATE
    setSize(IMemoryManager::STORAGE_VAR_CFGFILECRC, sizeof(lu_uint32_t));
    setSize(IMemoryManager::STORAGE_VAR_SERVICE, ReducedEnumSize); //SERVICEMODE
    setSize(IMemoryManager::STORAGE_VAR_SERVICEREASON, ReducedEnumSize); //OUTSERVICEREASON
    setSize(IMemoryManager::STORAGE_VAR_DUMMYSW, ReducedEnumSize); //ControlLogic::DBINARY_STATUS
    setSize(IMemoryManager::STORAGE_VAR_LOGLEVEL, ReducedEnumSize * SUBSYSTEM_ID_LAST); //LOG_LEVEL

    /* Initialise flush timer's list */
    for(lu_uint32_t i = 0; i < IMemoryManager::MEM_TYPE_LAST; i++)
    {
        flushTimeService[i].remainingTime = flushTimeService[i].interval;
    }

    /* Create timer for memory refresh */
    struct timeval startPeriod;
    tickInterval = calcRemainingTime();
    startPeriod = ms_to_timeval(tickInterval);
    refreshTimer = new Timer(startPeriod.tv_sec, startPeriod.tv_usec,
                            this, Timer::TIMER_TYPE_PERIODIC, "MemRefreshTmr");
    /* Start timer */
    if(refreshTimer != NULL)
    {
        /* start timer */
        if(refreshTimer->start() < 0)
        {
            log.fatal("%s System Timer unavailable");
        }
    }
}


MemoryManager::~MemoryManager()
{
    this->stop();
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
IMemService* MemoryManager::createService(IMemoryManager::MEM_TYPE type)
{
    switch(type)
    {
#ifndef TARGET_BOARD_G3_SYSTEMPROC
        case IMemoryManager::MEM_TYPE_NVRAMID:
			return MemoryManagerFactory::create<MemServiceNVRAMID>(MEMDEVICE_IDROM, MEMDEVICE_IDAPPRAM);
			break;
		case IMemoryManager::MEM_TYPE_NVRAM:
			return MemoryManagerFactory::create<MemServiceNVRAM>(MEMDEVICE_APPRAM);
			break;
		case IMemoryManager::MEM_TYPE_FRAM:
			return MemoryManagerFactory::create<MemServiceFRAM>(MEMDEVICE_FRAM0);
			break;
		case IMemoryManager::MEM_TYPE_EVENT:
			return MemoryManagerFactory::create<MemServiceEvent>(MEMDEVICE_EVENTFRAM);
			break;
		case IMemoryManager::MEM_TYPE_FILE:
			return MemoryManagerFactory::create<MemServiceFile>(MEMDEVICE_FILE);
			break;			
#else
        case IMemoryManager::MEM_TYPE_NVRAMID:
            return MemoryManagerFactory::create<MemServiceFile>(MEMDEVICE_FILE_NVRAMID);
            break;
        case IMemoryManager::MEM_TYPE_NVRAM:
            return MemoryManagerFactory::create<MemServiceFile>(MEMDEVICE_FILE_NVRAM);
            break;
        case IMemoryManager::MEM_TYPE_FRAM:
            return MemoryManagerFactory::create<MemServiceFile>(MEMDEVICE_FILE_FRAM);
            break;
        case IMemoryManager::MEM_TYPE_EVENT:
            return MemoryManagerFactory::create<MemServiceFile>(MEMDEVICE_FILE_EVENT);
            break;
        case IMemoryManager::MEM_TYPE_FILE:
            return MemoryManagerFactory::create<MemServiceFile>(MEMDEVICE_FILE);
            break;
#endif
        default:
            break;
    }
    return NULL;    //No Service Provider available for this type
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
lu_uint32_t MemoryManager::stop()
{
    /* stop timer */
    if(refreshTimer != NULL)
    {
        delete refreshTimer;
        refreshTimer = NULL;
    }
    return 0;
}


void MemoryManager::handleAlarmEvent(Timer &source)
{
    const lu_char_t *FTITLE = "MemoryManager::handleAlarmEvent:";

    /* calculate remaining time */
    tick_remainingTime -= tickInterval;

    /* Ask the pending Service Providers to call its tickEvent and/or flush */
    for(lu_uint32_t i = 0; i < IMemoryManager::MEM_TYPE_LAST; i++)
    {
        if( (tick_remainingTime <= 0) && (m_service[i] != NULL) )
        {
            /* Notify tickEvent to Service Provider */
            m_service[i]->tickEvent(MEMMGR_TICKEVENT_MS);  //a complete tickEvent time has passed
            log.debug("%s tickEvent of Mem Service Provider %i (%s).",
                        FTITLE,
                        i,
						MEM_NAME(i)
                    );
        }

        /* Update corresponding registered memory Service Provider data */
        if( (flushTimeService[i].interval > 0) && (m_service[i] != NULL) )
        {
            //update & check elapsed time
            flushTimeService[i].remainingTime -= tickInterval;
            if(flushTimeService[i].remainingTime == 0)
            {
                /* execute flush */
                flushTimeService[i].remainingTime = flushTimeService[i].interval; //reset remaining time
                m_service[i]->flush();
                log.debug("%s flushing memory of Service Provider %i (%s).",
                            FTITLE,
                            i,
							MEM_NAME(i)
                            );
            }
        }
    }
    /* restart tick time if expired */
    if( tick_remainingTime <= 0)
    {
        tick_remainingTime = MEMMGR_TICKEVENT_MS;
    }

    /* re-program the timer */
    tickInterval = calcRemainingTime();   //new interval
    source.start(ms_to_timespec(tickInterval));
}


lu_int64_t MemoryManager::calcRemainingTime()
{
    /* Minimum ms time to wait until next tickEvent or flush (in any Memory
     * Service Provider) that needs to happen. Set to max to find the minimum
     * value later.
     */
    lu_int64_t interval = std::numeric_limits<lu_int64_t>::max();
    for(lu_uint32_t i = 0; i < IMemoryManager::MEM_TYPE_LAST; i++)
    {
        if( (flushTimeService[i].interval > 0) && (m_service[i] != NULL) )
        {
            interval = (interval < flushTimeService[i].remainingTime)?
                        interval : flushTimeService[i].remainingTime;
        }
    }
    return (interval < tick_remainingTime)? interval : tick_remainingTime;
}


/*
 *********************** End of file ******************************************
 */

