/* Based on algorithm found at:
 * http://read.pudn.com/downloads169/sourcecode/math/779571/CRC4.C__.htm
 */

static const lu_uint8_t tab_crc4[16] = {
0, 3, 6, 5,   
12, 15, 10, 9,   
11, 8, 13, 14,   
7, 4, 1, 2 };   
   
lu_uint8_t crc4(lu_uint8_t buf[], lu_uint8_t size)   
{   
    lu_uint8_t k=0, crc=0;   
    for(k=0; k < size; ++k)   
    {   
        crc^=buf[k]>>4;     /* upper half */
        crc=tab_crc4[crc];   
        crc^=buf[k]&0xF;    /* lower half */
        crc=tab_crc4[crc];   
    }   
    return crc;   
}   
   
