/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: 
 *               $Id: MemoryManager.cpp 26-Oct-2012 10:04:56 pueyos_a $
 *               $HeadURL: Y:\workspace\G3Trunk\G3\RTU\MCM\src\MemoryManager\src\MemoryManager.cpp $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MemoryManager header module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 	$: (Revision of last commit)
 *               $Author: pueyos_a	$: (Author of last commit)
 *       \date   $Date: 26-Oct-2012 10:04:56	$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date                  Name        Details
 *   --------------------------------------------------------------------------
 *   26-Oct-2012 10:04:56  pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_E826CC09_031C_44c0_A7DE_056BFBC8D2A0__INCLUDED_)
#define EA_E826CC09_031C_44c0_A7DE_056BFBC8D2A0__INCLUDED_



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "AbstractMemoryManager.h"
#include "Timer.h"
#include "Logger.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * Class MemoryManager
 */
class MemoryManager : public AbstractMemoryManager, private ITimerHandler
{
public:
    MemoryManager();
    virtual ~MemoryManager();

protected:
    /* ==extends AbstractMemoryManager== */
    virtual IMemService* createService(IMemoryManager::MEM_TYPE type);

private:
    lu_uint32_t stop();

    /* ==inherited from ITimerHandler== */
    void handleAlarmEvent(Timer &source);

    /**
     * \brief Calculate interval in ms for next timer expiration
     *
     * Calculates the interval in ms for next timer expiration: the minimum
     * remaining time of the service provider's remaining times.
     *
     * \return minimum remaining time in milliseconds
     */
    lu_int64_t calcRemainingTime();

private:
    Logger& log;

    //TODO: AP - add a mutex specific for access to NVRAM APP - ID/user blocks.

    Timer *refreshTimer;            //Timer for refreshing memory Service Providers
    lu_int64_t tickInterval;        //Last programmed tick interval time in ms
    lu_int64_t tick_remainingTime;  //tickEvent time in ms
};


#endif // !defined(EA_E826CC09_031C_44c0_A7DE_056BFBC8D2A0__INCLUDED_)

/*
 *********************** End of file ******************************************
 */

