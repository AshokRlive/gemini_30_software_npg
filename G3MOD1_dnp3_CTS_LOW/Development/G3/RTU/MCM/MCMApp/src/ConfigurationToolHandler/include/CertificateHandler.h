/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:CertificateHandler.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   16 Dec 2016     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef RTU_MCM_MCMAPP_SRC_CONFIGURATIONTOOLHANDLER_SRC_CERTIFICATEHANDLER_H_
#define RTU_MCM_MCMAPP_SRC_CONFIGURATIONTOOLHANDLER_SRC_CERTIFICATEHANDLER_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "GlobalDefs.h"
#include "IIOModuleManager.h"
#include "CTHMessage.h"
#include "Logger.h"
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
namespace CTH {

class CertificateHandler
{
public:
    CertificateHandler();
    virtual ~CertificateHandler();

    CTHMessage* decodeCertificateMessage(const CTMessageHeader &header,
                                       const CTMessagePayload &payload
                                       );

private:
    /**
     * Runs a cert script.
     */
    lu_bool_t runScript(std::string scriptName, std::string protocol);

    Logger& log;
};

}

#endif /* RTU_MCM_MCMAPP_SRC_CONFIGURATIONTOOLHANDLER_SRC_CERTIFICATEHANDLER_H_ */

/*
 *********************** End of file ******************************************
 */
