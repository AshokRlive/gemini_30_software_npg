/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       System Information module implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11/10/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "Diagnostic.h"
#include "AbstractCTHandler.h"
#include "versions.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
/**
 * Error code when reading channel value.
 */
enum SKIP_POLLING
{
    SKIP_POLLING_NO= 0,
    SKIP_POLLING_YES
};

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

namespace CTH
{

Diagnostic::Diagnostic(IIOModuleManager &moduleMgr) :
                        moduleManager(moduleMgr),
                        log(Logger::getLogger(SUBSYSTEM_ID_CT_HANDLER))
{}

Diagnostic::~Diagnostic() {}


CTHMessage* Diagnostic::decodePollChannel(const CTMessageHeader &header,
                                          const CTMessagePayload &payload
                                          )
{
    if( (payload.payloadLen < sizeof(CTMsgPL_C_RTChannelData)) ||
        ((payload.payloadLen % sizeof(CTMsgPL_C_RTChannelData)) != 0)
        )
    {
        return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
    }

    /* Decode valid payload */
    CTMsgPL_C_RTChannelData* requestPL = reinterpret_cast<CTMsgPL_C_RTChannelData*>(payload.payloadPtr);

    /* Compose reply message */
    CTMsg_R_RTChannelData* reply = new CTMsg_R_RTChannelData(header.requestID, replyID.newID());
    lu_uint32_t chNum = payload.payloadLen / sizeof(CTMsgPL_C_RTChannelData);
    reply->setEntries(chNum);     // Set the number of entries
    chNum = reply->getEntries();  // Get back the number of entries we can effectively store
    CTMsgPL_R_RTChannelData* replyMsgPtr;

    // Set skipping flag to SKIP_POLLING_NO
    memset(errors, 0, sizeof(errors[0][0]) * MODULE_LAST * MODULE_ID_LAST);


    for(lu_uint32_t i = 0; i < chNum; i++)
    {
        replyMsgPtr = reinterpret_cast<CTMsgPL_R_RTChannelData*>(&(*reply)[i]);
        replyMsgPtr-> channelID = requestPL->channelID;
        replyMsgPtr-> moduleID = requestPL->moduleID;
        replyMsgPtr-> moduleType = requestPL->moduleType;
        replyMsgPtr-> channelType = requestPL->channelType;
        readChannelValue(replyMsgPtr);  //fill in channel value
        requestPL++;    //next item in the incoming list
    }
    reply->setEntries(chNum);
    return reply;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void Diagnostic::readChannelValue(CTMsgPL_R_RTChannelData * CTmessage)
{
	const lu_char_t *FTITLE = "Diagnostic::readChannelValue:";
	MODULE mType = (MODULE)CTmessage->moduleType;
	MODULE_ID mId= (MODULE_ID)CTmessage->moduleID;
	lu_uint8_t chType = CTmessage->channelType;
	lu_uint8_t chId = CTmessage->channelID;

    CTmessage->valid = 0;   //Not valid until success
    CTmessage->intvalue = 0;
    CTmessage->floatvalue = 0;

	IIOModule *module = moduleManager.getModule(mType,mId);

	if(module == NULL)
	{
        /*Module not found*/
	    log.info("%s Cannot find module. Type:%i, ID:%i.", FTITLE, mType, mId);
	}

	else if(errors [mType][mId] == SKIP_POLLING_YES)
    {
	    /**
         * Skip sending CAN message to the module that was timeout last time
         * in order to prevent accumulating the Timeout to respond ConfigTool.
         */
        CTmessage->intvalue = 0;
        CTmessage->valid = 0;
    }

	else
	{
	    IOM_ERROR err = IOM_ERROR_NONE;
	    switch(chType)
	    {
	        case ANALOGUE_INPUT_CHANNEL:
	        {
                CTmessage->floatvalue = 0;
                CTmessage->valid = 0;
                IODataFloat32 data;
                IChannel::ValueStr value(data);
                err = module->readRAWChannel((CHANNEL_TYPE)chType, chId, value);
                if(err == IOM_ERROR_NONE )
                {
                    CTmessage->floatvalue = (lu_float32_t)data;
                    CTmessage->online = value.flags.online;
                    CTmessage->alarm = value.flags.alarm;
                    CTmessage->restart = value.flags.restart;
                    CTmessage->outRange = value.flags.outRange;
                    CTmessage->filtered = value.flags.filtered;
                    CTmessage->simulated = value.flags.simulated;
                    CTmessage->valid = 1;
                }
                break;
	        }
	        case DIGITAL_INPUT_CHANNEL:
            {
                CTmessage->intvalue = 0;
                CTmessage->valid = 0;
                IODataInt32 data;
                IChannel::ValueStr value(data);
                err = module->readRAWChannel((CHANNEL_TYPE)chType, chId, value);
                if(err == IOM_ERROR_NONE )
                {
                    CTmessage->intvalue = (lu_uint32_t)data;
                    CTmessage->online = value.flags.online;
                    CTmessage->alarm = value.flags.alarm;
                    CTmessage->restart = value.flags.restart;
                    CTmessage->outRange = value.flags.outRange;
                    CTmessage->filtered = value.flags.filtered;
                    CTmessage->simulated = value.flags.simulated;
                    CTmessage->valid = 1;
                }
                break;
            }
	        default:
	            break;
	    }

	    // Record timeout error for this module
	    if(err == IOM_ERROR_TIMEOUT || err == IOM_ERROR_NOT_RUNNING)
	    {
            errors [mType][mId] = SKIP_POLLING_YES;
	    }
	}
}


}//end namespace CTH

/*
 *********************** End of file ******************************************
 */
