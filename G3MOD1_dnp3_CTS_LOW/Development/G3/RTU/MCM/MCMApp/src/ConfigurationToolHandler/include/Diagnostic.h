/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11/10/11      Pei_wang     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(_G3_CTH_DIAGNOSTIC)
#define _G3_CTH_DIAGNOSTIC

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "GlobalDefs.h"
#include "IIOModuleManager.h"
#include "CTHMessage.h"
#include "Logger.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

namespace CTH
{

class Diagnostic
{

public:
    /**
     * \brief Custom constructor
     *
     * \param moduleMgr Reference to the module manager object
     *
     * \return none
     */
	Diagnostic(IIOModuleManager &moduleMgr);

    virtual ~Diagnostic();

    /**
     * \brief Decode poll channel CT request message and fill reply message.
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return Reply message
     */
    CTHMessage* decodePollChannel(const CTMessageHeader &header,
                                  const CTMessagePayload &payload
                                  );

private:
    /**
     * Read channel value and fill the CT message with the read value.
     */
    void readChannelValue(CTMsgPL_R_RTChannelData * CTmessage);

private:
    IIOModuleManager &moduleManager;
    Logger& log;
    lu_uint8_t errors[MODULE_LAST][MODULE_ID_LAST];
};

}//end namespace CTH


#endif // !defined(EA_2CB0E182_AAEF_4d76_8940_5C7CF27A8B19__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
