/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:CertificateHandler.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   16 Dec 2016     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "CertificateHandler.h"
#include "AbstractCTHandler.h"
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
#define SCRIPT_CREATE_CSR           "create_csr.sh"
#define SCRIPT_CREATE_PKEY          "create_private_key.sh"
#define SCRIPT_CREATE_SELFSIGNED    "create_self_signed_cert.sh"
#define SCRIPT_INSTALL_CERT         "install_certs.sh"
#define SCRIPT_CLEAN_CERT_LOG       "clean_cert_log.sh"

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

namespace CTH
{

CertificateHandler::CertificateHandler() :
                log(Logger::getLogger(SUBSYSTEM_ID_CT_HANDLER))
{
}

CertificateHandler::~CertificateHandler()
{
}

CTHMessage* CertificateHandler::decodeCertificateMessage(const CTMessageHeader &header,
                                       const CTMessagePayload &payload
                                       )
{
    lu_bool_t success = LU_FALSE;
    std::string protocol((lu_char_t*)payload.payloadPtr, payload.payloadLen);

    /* Decode message ID */
    switch(header.messageID)
    {
        case CTMsgID_C_GenPKey:
            success = runScript(SCRIPT_CREATE_PKEY, protocol);
            break;

        case CTMsgID_C_GenCSR:
            success = runScript(SCRIPT_CREATE_CSR, protocol);
            break;

        case CTMsgID_C_InstallCert:
            success = runScript(SCRIPT_INSTALL_CERT, protocol);
            break;

        case CTMsgID_C_GenSelfSigned:
            success = runScript(SCRIPT_CREATE_SELFSIGNED, protocol);
            break;

        case CTMsgID_C_CleanCertLog:
            success = runScript(SCRIPT_CLEAN_CERT_LOG, protocol);
            break;

        default:
            log.error("Unsupported certificate msg id:%d", header.messageID);
    }

    return createAckNack(header, success ? CTH_ACK : CTH_NACK_SYSTEM);
}


lu_bool_t CertificateHandler::runScript(std::string scriptName, std::string protocol)
{
    log.info("Run script \"%s\" for protocol:%s",scriptName.c_str(), protocol.c_str());

    char buff[200];
    snprintf(buff, sizeof(buff), "sh scripts/%s %s &", scriptName.c_str(), protocol.c_str());

    system(buff);

    return true;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

}
/*
 *********************** End of file ******************************************
 */
