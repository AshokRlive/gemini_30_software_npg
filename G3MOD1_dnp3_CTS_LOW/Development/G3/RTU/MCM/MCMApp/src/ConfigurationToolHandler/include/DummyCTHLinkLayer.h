/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Dummy link layer
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/02/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_6865A588_461A_4b3f_A6E8_EAFD7953D5EB__INCLUDED_)
#define EA_6865A588_461A_4b3f_A6E8_EAFD7953D5EB__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "CTHandlerCommon.h"
#include "ICTHLinkLayer.h"
#include "Thread.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Configuration Tool Handler dummy link  layer.
 *
 * Used for testing. It's now disabled and is not build anymore
 */
class DummyCTHLinkLayer : public Thread, public ICTHLinkLayer
{

public:
    DummyCTHLinkLayer();
    virtual ~DummyCTHLinkLayer();

    /**
      * \brief Start link layer server
      *
      * \return Error code
      */
     virtual CTH_ERROR startServer();

     /**
      * \brief Stop link layer server
      *
      * This function is automatically called by the destructor
      *
      * \return Error code
      */
     virtual CTH_ERROR stopServer(lu_bool_t killThread = LU_TRUE);

     /**
      * \brief Register an observer in the link layer
      *
      * \param observer Pointer to the observer to register
      *
      * \return Error Code
      */
     virtual CTH_ERROR attach(ICTHLinkLayerObserver *observer);

     /**
      * \brief Deregister an observer in the link layer
      *
      * \param observer Pointer to the observer to deregister
      *
      * \return Error Code
      */
     virtual CTH_ERROR detach(ICTHLinkLayerObserver *observer);

     /**
      * \brief Send some data over the link layer
      *
      * \param message Message to send
      *
      * \return Error Code
      */
     virtual CTH_ERROR send(CTHMessage &message);

protected:
    /**
     * \brief Thread main loop  This function is called automatically when a thread is
     * started
     */
    virtual void threadBody();

private:
    ICTHLinkLayerObserver *observer;
};


#endif // !defined(EA_6865A588_461A_4b3f_A6E8_EAFD7953D5EB__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
