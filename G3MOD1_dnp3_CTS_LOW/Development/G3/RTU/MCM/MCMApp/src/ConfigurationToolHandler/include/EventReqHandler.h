/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       System & Event log manager for the Configuration Tool Handler.
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   20/09/12      pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#if !defined(EA_DCE13B27_F99F_4b1e_9C2E_11682105901F__INCLUDED_)
#define EA_DCE13B27_F99F_4b1e_9C2E_11682105901F__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "IStatusManager.h"
#include "IIOModuleManager.h"
#include "CTHandlerCommon.h"
#include "CTHMessage.h"
#include "Logger.h"
#include "EventLogManager.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

namespace CTH
{

class EventReqHandler
{

public:
    /**
     * \brief Custom constructor
     *
     * \param moduleMgr Reference to the module manager object
     *
     * \return none
     */
	EventReqHandler();
	virtual ~EventReqHandler();


	CTHMessage* decodeMessageLog(const CTMessageHeader &header,
	                                       const CTMessagePayload &payload);
private:
    /**
     * \brief Process Get latest Event Log entries message.
     *
     * Returns latest Event Log entries available.
     *
     * \param payload Reference to the message payload.
     * \param message Reference to the message to be returned.
     *
     * \return Error code
     */
	 CTHMessage* decodeGetEventListMessage(const CTMessageHeader &header,
                                            const CTMessagePayload &payload);

    /**
     * \brief Clears the Event log list.
     *
     * Eliminates all entries in the event log list.
     *
     * \param payload Reference to the message payload.
     *
     * \return Error code
     */
	 CTHMessage* decodeClearEventLogMessage(const CTMessageHeader &header,
                                         const CTMessagePayload &payload);

    /**
     * \brief Start to read event log entries from the beginning.
     *
     * Puts the extraction point back at the beginning of the Event log list.
     *
     * \param payload Reference to the message payload.
     *
     * \return Error code
     */
	 CTHMessage* decodeBeginEventLogMessage(const CTMessageHeader &header,
                                        const CTMessagePayload &payload);

private:
    Logger& log;
    EventLogManager* eventLog;
};

}//end namespace CTH


#endif // !defined(EA_DCE13B27_F99F_4b1e_9C2E_11682105901F__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
