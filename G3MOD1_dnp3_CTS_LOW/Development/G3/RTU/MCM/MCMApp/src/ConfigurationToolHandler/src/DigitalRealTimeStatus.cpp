/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Configuration Tool Virtual Digital point real time data implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "DigitalRealTimeStatus.h"
#include "TimeManager.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
CTH_ERROR DigitalRealTimeStatus::getRTPointData(CTMsg_R_RTPointData_Digital &RTData)
{
    /* Use internal points list */
    lu_uint32_t nPoints;
    nPoints = getRTPointData(&pointList[lastIdx],        //internal source list
                            (lu_uint32_t)(pointListSize-lastIdx),    //size of the list
                            &RTData[0],                     //where to store it
                            RTData.getMaxEntries()      //maximum number of points that can be stored
                            );
    RTData.setEntries(nPoints);
    /* Adjust internal points list's index */
    if(nPoints+lastIdx >= pointListSize)
    {
        /* Point list finished. Reset last index */
        lastIdx = 0;
    }
    else
    {
        lastIdx += nPoints;
    }
    return (nPoints == 0)? CTH_ERROR_LIST : CTH_ERROR_NONE;
}

lu_uint32_t DigitalRealTimeStatus::getRTPointData(Table<CTMsgPL_C_PointID>& pointList,
                                                  Table<CTMsgPL_R_RTPointData_Digital>& destList
                                                   )
{
    return getRTPointData(pointList.getTable(), pointList.getEntries(), destList.getTable(), destList.getEntries());
}

lu_uint32_t DigitalRealTimeStatus::getRTPointData(CTMsgPL_C_PointID pointList[],
                                                   lu_uint32_t pointListSize,
                                                   CTMsgPL_R_RTPointData_Digital destList[],
                                                   lu_uint32_t destListMaxSize
                                                   )
{
    lu_uint32_t points;         //index
    PointIdStr pointIdTmp;      //temporal pointID for get value from G3DB
    PointDataUint8 pointValue; //temporal storage for point value coming from G3DB
    TimeManager::TimeStr timestamp; //temporal timestamp for copying

    for(points = 0; ((points < pointListSize) && (points < destListMaxSize)); ++points)
    {
        CTMsgPL_R_RTPointData_Digital* RTStatusTmpPtr = &destList[points];
        RTStatusTmpPtr->point = pointList[points];

        pointIdTmp.group = RTStatusTmpPtr->point.group;
        pointIdTmp.ID = RTStatusTmpPtr->point.ID;
        if(database.getValue(pointIdTmp, &pointValue) == GDB_ERROR_NONE)
        {
            /* Valid point. Store data in the message */
            RTStatusTmpPtr->valid   = LU_TRUE;
            RTStatusTmpPtr->online  = (pointValue.getQuality() == POINT_QUALITY_ON_LINE)? LU_TRUE: LU_FALSE;
            RTStatusTmpPtr->chatter = pointValue.getChatterFlag();
            RTStatusTmpPtr->value   = pointValue;
            RTStatusTmpPtr->unused  = 0x00;
        }
        else
        {
            /* Error reading a virtual point */
            RTStatusTmpPtr->valid = LU_FALSE;
        }
        pointValue.getTime(timestamp);
        RTStatusTmpPtr->timestamp.sec = timestamp.time.tv_sec;
        RTStatusTmpPtr->timestamp.nsec = timestamp.time.tv_nsec;
    }
    return points;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
