/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Configuration Tool Engineer Testing operations implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19/09/12      pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "EngineerTest.h"
#include "AbstractCTHandler.h"
#include "LocalRemoteControlLogic.h"
#include "AutomationManager.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
namespace CTH
{

EngineerTest::EngineerTest(IStatusManager &statusMgr,
                            GeminiDatabase &g3database) :
                                statusManager(statusMgr),
                                database(g3database),
                                log(Logger::getLogger(SUBSYSTEM_ID_CT_HANDLER))
{}

EngineerTest::~EngineerTest()
{}

CTHMessage* EngineerTest::decodeGetServiceModeMessage(const CTMessageHeader &header,
                                                      const CTMessagePayload &payload
                                                      )
{
    if( payload.payloadLen == 0 )
    {
        /* Get data */
        SERVICEMODE mode;
        OUTSERVICEREASON reason;
        statusManager.getServiceMode(mode, reason);

        /* Compose return message */
        CTMsg_R_ServiceStatus* reply = new CTMsg_R_ServiceStatus(header.requestID, replyID.newID());
        CTMsgPL_R_ServiceStatus* rPayload;
        rPayload = reinterpret_cast<CTMsgPL_R_ServiceStatus*>(reply->firstEntry());
        rPayload->serviceStatus = (lu_uint8_t)(mode);
        rPayload->reason = reason; //(lu_uint8_t)(reason);

        /* Set auto status*/
        rPayload->autoRunning = isAutomationRunning();
        rPayload->autoDebugging = isAutomationDebugMode();
        rPayload->autoFailed = isAutomationFailed();
        rPayload->autoInhibited = isAutomationInhibited();

        reply->setEntries(1);
        log.info("%s replies with Service Status %i, reason %i",
                __AT__, mode, reason);
        return reply;
    }
    return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
}


CTHMessage* EngineerTest::decodeSetServiceModeMessage(const CTMessageHeader &header,
                                                      const CTMessagePayload &payload,
                                                      const SERVICEMODE mode
                                                      )
{
    OUTSERVICEREASON RTUServiceModeReason = OUTSERVICEREASON_UNDEFINED;

    if(mode == SERVICEMODE_OUTSERVICE)
    {
        /* get payload to store the reason */
        if( payload.payloadLen != sizeof(CTMsgPL_C_SetServiceMode) )
        {
            return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
        }
        CTMsgPL_C_SetServiceMode* reasonPtr = reinterpret_cast<CTMsgPL_C_SetServiceMode*>(payload.payloadPtr);
        if( (reasonPtr->reason) >= OUTSERVICEREASON_LAST )
        {
            log.error("%s Service mode was tried to be set with an invalid reason (%i).",
                        __AT__, reasonPtr->reason
                        );
            return createAckNack(header, CTH_NACK_PAYLOAD); //incorrect payload
        }
        RTUServiceModeReason = (OUTSERVICEREASON)(reasonPtr->reason);
    }
    else
    {
        if(payload.payloadLen != 0)
        {
            return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
        }
    }
    statusManager.setServiceMode(mode, RTUServiceModeReason);
    log.info("%s Service mode was set to %i (reason is %i).",
            __AT__, mode, RTUServiceModeReason
            );
    return createAckNack(header, CTH_ACK);
}


CTHMessage* EngineerTest::decodeGetOLRMessage(const CTMessageHeader &header,
                                              const CTMessagePayload &payload
                                              )
{
    if( payload.payloadLen != 0 )
    {
        return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
    }

    /* Get data */
    OLR_STATE status;
    if(database.getLocalRemote(status) == GDB_ERROR_NONE)
    {
        /* Compose return message */
        CTMsg_R_OLRState* reply;
        reply = new CTMsg_R_OLRState(header.requestID, replyID.newID());
        CTMsgPL_R_OLRState* rPayload;
        rPayload = reinterpret_cast<CTMsgPL_R_OLRState*>(reply->firstEntry());
        rPayload->stateOLR = (lu_uint8_t)((lu_uint32_t)(status));
        reply->setEntries(1);
        log.info("%s Get OLR status: %d (%s)",
                 __AT__, rPayload->stateOLR,
                 OLR_STATE_ToSTRING((lu_uint32_t)(status))
                 );
        return reply;
    }
    return createAckNack(header, CTH_NACK_OLRERROR); //error getting value
}


CTHMessage* EngineerTest::decodeSetOLRMessage(const CTMessageHeader &header,
                                              const CTMessagePayload &payload
                                              )
{
    if( payload.payloadLen != sizeof(CTMsgPL_C_OLRState) )
    {
        return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
    }
    /* Decode valid payload */
    CTMsgPL_C_OLRState *newOLRStatus = reinterpret_cast<CTMsgPL_C_OLRState *>(payload.payloadPtr);

    /* Set data */
    GDB_ERROR ret;
    ret = database.setLocalRemote(static_cast<OLR_STATE>(newOLRStatus->stateOLR),
                                  OLR_SOURCE_CTOOL
                                 );
    if(ret == GDB_ERROR_NONE)
    {
        return createAckNack(header, CTH_ACK);  //Done OK
    }
    log.error("%s Set OLR status failed: %s", __AT__, GDB_ERROR_ToSTRING(ret));
    return createAckNack(header, CTH_NACK_OLRERROR);    //operation error
}


CTHMessage* EngineerTest::decodeSwitchMessage(const CTMessageHeader &header,
                                              const CTMessagePayload &payload
                                              )
{
    CTH_ACKNACKCODE ret = CTH_NACK_GENERIC;

    if( payload.payloadLen != sizeof(CTMsgPL_C_OperateSwitch) )
    {
        return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
    }

    /* Get data */
    CTMsgPL_C_OperateSwitch* switchPtr = reinterpret_cast<CTMsgPL_C_OperateSwitch*>(payload.payloadPtr);

    if(database.isOperatable(switchPtr->clogic))
    {
        SwitchLogicOperation sglOperation;
        sglOperation.operation = (switchPtr->operation == 0) ? SWITCH_OPERATION_OPEN :
                                                                SWITCH_OPERATION_CLOSE;
        sglOperation.local = switchPtr->local;
        log.info("%s Starting Control Logic operation. "
                "CLogic: %i - operation: %i - local: %i\n",
                __AT__,
                switchPtr->clogic,
                sglOperation.operation,
                sglOperation.local
                );
        GDB_ERROR retn = database.startOperation(switchPtr->clogic, sglOperation);
        ret = convertGDBtoCTH_NACK(retn);
        if(ret == CTH_ACK)
        {
            log.info("%s Control Logic %i operation successfully started",
                     __AT__, switchPtr->clogic);
        }
        else
        {
            log.error("%s Control Logic %i operation failed with GDB error %s.",
                        __AT__, switchPtr->clogic, GDB_ERROR_ToSTRING(retn));
        }
    }
    return createAckNack(header, ret);
}


CTHMessage* EngineerTest::decodeControlLogicOperationMessage(const CTMessageHeader &header,
                                                             const CTMessagePayload &payload
                                                             )
{
    CTH_ACKNACKCODE ret = CTH_NACK_GENERIC;

    if( payload.payloadLen != sizeof(CTMsgPL_C_OperateCLogic) )
    {
        return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
    }

    /* Decode valid payload */
    CTMsgPL_C_OperateCLogic* clOpPtr = reinterpret_cast<CTMsgPL_C_OperateCLogic*>(payload.payloadPtr);

    if(database.isOperatable(clOpPtr->clogic))
    {
        log.info("%s Starting Control Logic %i operation for %d seconds.", __AT__,
                    clOpPtr->clogic,
                    clOpPtr->duration
                    );
        SwitchLogicOperation op;
        op.duration = clOpPtr->duration;
        /* TODO: pueyos_a - get local/remote operation from ConfigTool message */
        OLR_STATE olr;
        database.getLocalRemote(olr);
        op.local = (olr == OLR_STATE_LOCAL)? LU_TRUE : LU_FALSE;
        op.operation = SWITCH_OPERATION_CLOSE;
        GDB_ERROR retn = database.startOperation(clOpPtr->clogic, op);
        ret = convertGDBtoCTH_NACK(retn);
        if(retn == GDB_ERROR_NONE)
        {
            log.info("%s Control Logic %i operation successfully started",
                        __AT__, clOpPtr->clogic);
        }
        else
        {
            log.error("%s Control Logic %i operation failed with GDB error %s",
                        __AT__, clOpPtr->clogic, GDB_ERROR_ToSTRING(retn));
        }
    }
    return createAckNack(header, ret); //invalid payload
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
CTH_ACKNACKCODE EngineerTest::convertGDBtoCTH_NACK(const GDB_ERROR errorCode)
{
    CTH_ACKNACKCODE ret;
    switch(errorCode)
    {
    case GDB_ERROR_NONE:
        ret = CTH_ACK;
    break;
    case GDB_ERROR_NOT_SUPPORTED:
        ret = CTH_NACK_OPNOTSUPPORTED;
    break;
    case GDB_ERROR_ALREADY_ACTIVE:
        ret = CTH_NACK_ALREADYACTIVE;
    break;
    case GDB_ERROR_NOPOINT:
        ret = CTH_NACK_NOPOINT;
    break;
    case GDB_ERROR_AUTHORITY:
        ret = CTH_NACK_AUTHORITY;
    break;
    case GDB_ERROR_POSITION:
        ret = CTH_NACK_BADPOSITION;
    break;
    case GDB_ERROR_LOCAL_REMOTE:
        ret = CTH_NACK_LOCALREMOTE;
    break;
    case GDB_ERROR_INHIBIT:
        ret = CTH_NACK_INHIBIT;
    break;
    case GDB_ERROR_CMD:
        ret = CTH_NACK_ERRORCMDSLAVE;
    break;
    case GDB_ERROR_DELAY:
        ret = CTH_NACK_DBDELAY;
    break;
    default:
        ret = CTH_NACK_GENERIC;
    }
    return ret;
}


}//end namespace CTH

/*
 *********************** End of file ******************************************
 */

