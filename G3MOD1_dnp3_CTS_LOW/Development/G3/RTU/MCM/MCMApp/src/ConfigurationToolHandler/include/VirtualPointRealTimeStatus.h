/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Configuration Tool Virtual point real time data common interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13/10/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_E01B270D_F72D_409b_87EE_5C75E4B8DF08__INCLUDED_)
#define EA_E01B270D_F72D_409b_87EE_5C75E4B8DF08__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "CTHMessage.h"
#include "GeminiDatabase.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class VirtualPointRealTimeStatus
{

public:
    VirtualPointRealTimeStatus(GeminiDatabase &database);
    virtual ~VirtualPointRealTimeStatus();

    /**
     * \brief Get the buffer where the list of observed digital point is stored
     *
     * The caller can change the content of the list. When this method is used
     * the list is internally disabled. The list is reactivated using the
     * setDigitalPointList method.
     *
     * Usage Example
     *
     * CTPointListStrTable &CTPointList = getDigitalPointList();
     * .....
     * modify the content of CTPointList
     * size = new size;
     * .....
     * setDigitalPointList(size);
     *
     * \return Reference to the table where the list of observed digital
     * point is stored
     */
    virtual CTPointListStrTable &getPointList(void);

    /**
     * \brief Set the new size of the list of observed digital point
     *
     * See getDigitalPointList for more information.
     *
     * \param size New size
     *
     * \return Error code
     */
    virtual CTH_ERROR setPointList(lu_uint32_t size);

protected:
    GeminiDatabase &database;

    CTMsgPL_C_PointID *pointList;
    lu_uint32_t pointListSizeMax;
    lu_uint32_t pointListSize;
    lu_uint32_t lastIdx;

    CTPointListStrTable pointListTable;

};
#endif // !defined(EA_E01B270D_F72D_409b_87EE_5C75E4B8DF08__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
