/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Configuration Tool Virtual Digital point real time data interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(_COUNTERREALTIMESTATUS_INCLUDED_)
#define _COUNTERREALTIMESTATUS_INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "CTHandlerCommon.h"
#include "CTHMessage.h"
#include "VirtualPointRealTimeStatus.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class CounterRealTimeStatus : public VirtualPointRealTimeStatus
{

public:
    /**
     * \brief Custom constructor
     *
     * \param database Reference to the internal database
     *
     * \return None
     */
    CounterRealTimeStatus(GeminiDatabase &database) :
                                        VirtualPointRealTimeStatus(database) {};
    virtual ~CounterRealTimeStatus() {};

    /**
     * \brief Poll the previously registered counter list
     *
     * If the provided buffer is not big enough, only the first points are returned.
     * The remaining points will be saved in the next requests, following the
     * internal list index.
     *
     * \param  RTData Reference to the buffer where the data will be saved
     *
     * \return Error Code
     */
    CTH_ERROR getRTPointData(CTMsg_R_RTPointData_Counter &RTData);

    /**
     * \brief Poll a given counter list
     *
     * If the provided buffer is not big enough, only the first points are returned.
     *
     * \param  pointList Reference to the list of the desired points
     * \param  pointListSize Size of the list of points
     * \param  destList Reference to the buffer where the data will be saved
     * \param  destListMaxSize Size of the buffer where the data will be saved
     *
     * \return Number of points saved into the destination buffer
     */
    lu_uint32_t getRTPointData(Table<CTMsgPL_C_PointID>& pointList,
                               Table<CTMsgPL_R_RTPointData_Counter>& destList
                               );

    lu_uint32_t getRTPointData(CTMsgPL_C_PointID pointList[],
                               lu_uint32_t pointListSize,
                               CTMsgPL_R_RTPointData_Counter destList[],
                               lu_uint32_t destListMaxSize
                               );
};
#endif // !defined(_COUNTERREALTIMESTATUS_INCLUDED_)

/*
 *********************** End of file ******************************************
 */
