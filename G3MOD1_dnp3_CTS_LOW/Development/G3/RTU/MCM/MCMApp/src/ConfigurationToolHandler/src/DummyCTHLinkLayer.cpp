/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Dummy Link Layer
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <unistd.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "DummyCTHLinkLayer.h"
//#include "SysLogFactory.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
#pragma pack(1)
typedef struct pointListDef
{
    CTMessageHeader header;
    CTMsgPL_C_PointID points[50];
}pointListStr;
#pragma pack()

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

class CTHMessageModuleRInfo : public CTHMessage
{
public:
    CTHMessageModuleRInfo(lu_uint16_t requestID) :
        CTHMessage(CTMsgType_Module, CTMsgID_C_ModuleInfo, requestID, CTMessageHeader::INVALID_ID){};
    ~CTHMessageModuleRInfo() {};

    virtual CTH_ERROR getRAWPayaload(PayloadRAW &payload)
    {
        payload.payloadLen = 0;
        payload.payloadPtr = NULL;

        return CTH_ERROR_NONE;
    }
};

class CTHMessageRTDigitalData : public CTHMessage
{
public:
    CTHMessageRTDigitalData(lu_uint16_t requestID) :
        CTHMessage(CTMsgType_PointData, CTMsgID_C_PollPointDigitalNOTS, requestID, CTMessageHeader::INVALID_ID){};
    ~CTHMessageRTDigitalData() {};

    virtual CTH_ERROR getRAWPayaload(PayloadRAW &payload)
    {
        payload.payloadLen = 0;
        payload.payloadPtr = NULL;

        return CTH_ERROR_NONE;
    }
};

class CTHMessageRTAnalogueData : public CTHMessage
{
public:
    CTHMessageRTAnalogueData(lu_uint16_t requestID) :
        CTHMessage(CTMsgType_PointData, CTMsgID_C_PollPointAnalogueNOTS, requestID, CTMessageHeader::INVALID_ID){};
    ~CTHMessageRTAnalogueData() {};

    virtual CTH_ERROR getRAWPayaload(PayloadRAW &payload)
    {
        payload.payloadLen = 0;
        payload.payloadPtr = NULL;

        return CTH_ERROR_NONE;
    }
};


DummyCTHLinkLayer::DummyCTHLinkLayer() :
                                        Thread(SCHED_TYPE_FIFO, PRIO_MIN, LU_TRUE),
                                        observer(NULL)
{}



DummyCTHLinkLayer::~DummyCTHLinkLayer()
{
    stopServer();
}





CTH_ERROR DummyCTHLinkLayer::startServer()
{
    if (start() == LU_TRUE)
    {
        return CTH_ERROR_NONE;
    }
    else
    {
        return CTH_ERROR_PARAM;
    }
}


CTH_ERROR DummyCTHLinkLayer::stopServer(lu_bool_t killThread)
{
    if (stop() == LU_TRUE)
    {
        return CTH_ERROR_NONE;
    }
    else
    {
        return CTH_ERROR_PARAM;
    }
}


CTH_ERROR DummyCTHLinkLayer::attach(ICTHLinkLayerObserver *observer)
{
    if(observer == NULL)
    {
        return CTH_ERROR_PARAM;
    }

    if(this->observer == NULL)
    {
        this->observer = observer;
        return CTH_ERROR_NONE;
    }
    else
    {
        return CTH_ERROR_OREGISTERED;
    }
}


CTH_ERROR DummyCTHLinkLayer::detach(ICTHLinkLayerObserver *observer)
{
    if(observer == NULL)
    {
        return CTH_ERROR_PARAM;
    }

    if(this->observer == observer)
    {
        this->observer = NULL;
        return CTH_ERROR_NONE;
    }
    else
    {
        return CTH_ERROR_OREGISTERED;
    }
}


CTH_ERROR DummyCTHLinkLayer::send(CTHMessage &message)
{
    PayloadRAW payloadH;
    PayloadRAW payloadP;
    lu_char_t stringBuffer[128];

    message.getRAWHeader(payloadH);
    message.getRAWPayaload(payloadP);

    lu_uint32_t i;
    lu_uint32_t counter;
    for(i = 0, counter = 0; i < payloadH.payloadLen; ++i)
    {
        counter += snprintf( &stringBuffer[counter],
                             128 - counter,
                             "%02x",
                             payloadH.payloadPtr[i]
                           );
    }

//    SysLogFactory::getInstance()->getSysLogManager(SUBSYSTEM_ID_CT_HANDLER)->logMsg
//            (
//                LOG_LEVEL_INFO, SUBSYSTEM_ID_CT_HANDLER,
//                "DummyCTHLinkLayer::send. Header %s - Payload size: %i",
//                stringBuffer, payloadP.payloadLen
//            );

    return CTH_ERROR_NONE;
}



/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void DummyCTHLinkLayer::threadBody()
{
    static lu_uint16_t i = 100;
    PayloadRAW payload;

    pointListStr list;
    list.header.messageType = CTMsgType_PointData;
    list.header.messageID = CTMsgID_C_SetPointDList;
    list.header.replyID = CTMessageHeader::INVALID_ID;

    for(lu_uint32_t i = 0; i < 50; ++i)
    {
        list.points[i].group = 0;
        list.points[i].ID = i;
    }

    while(LU_TRUE)
    {
        lucy_usleep(100000);

        if(observer != NULL)
        {
            list.header.requestID = i++;
            list.header.messageID = CTMsgID_C_SetPointDList;
            payload.payloadPtr = reinterpret_cast<lu_uint8_t*>(&list);
            payload.payloadLen = sizeof(list);
            observer->newData(payload);

            list.header.requestID = i++;
            list.header.messageID = CTMsgID_C_SetPointAList;
            payload.payloadPtr = reinterpret_cast<lu_uint8_t*>(&list);
            payload.payloadLen = sizeof(list);
            observer->newData(payload);

            CTHMessageModuleRInfo rModuleInfo(i++);
            rModuleInfo.getRAWHeader(payload);
            observer->newData(payload);

            CTHMessageRTDigitalData pollDDataD(i++);
            pollDDataD.getRAWHeader(payload);
            observer->newData(payload);


            CTHMessageRTAnalogueData pollDDataA(i++);
            pollDDataA.getRAWHeader(payload);
            observer->newData(payload);
        }
    }
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
