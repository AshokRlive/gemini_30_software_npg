/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: CTEventFormat.h 7 May 2015 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/ConfigurationToolHandler/include/CTEventFormat.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 7 May 2015 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   7 May 2015 pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef CTEVENTFORMAT_H__INCLUDED
#define CTEVENTFORMAT_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <assert.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "EventLogFormat.h"
#include "dbg.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
/**
 * \brief MACRO for overloading operator = on CTH Events]
 *
 * \param Base Event Log format class, base of the CTH one.
 * \param Child CTH Event format class.
 */
#define ADDMETHODS(Base, Child)         \
    Child& operator=(const Base& event) \
    {                                   \
        Base* p = this;                 \
        *p = event;                     \
        reserved = 0;                   \
        return *this;                   \
    }

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
#pragma pack(1) /* Force 1-byte alignment */

/**
 * \brief  Event
 */
struct CTHEventDNP3PointStr : public CTEventDNP3PointStr
{
public:
    lu_uint16_t reserved;
public:
    ADDMETHODS(CTEventDNP3PointStr, CTHEventDNP3PointStr);
};

/**
 * \brief Event S104 Event
 */
struct CTHEventS104PointStr //: public CTEventSi870PointStr
{
public:
    lu_uint8_t pointType;   //Point Type. Valid values are EVENT_TYPE_S104 enum
    lu_uint32_t protocolID; //IEC101/104 IOA
    union valueDef
    {
        lu_uint32_t valueDig;       //Digital value
        lu_float32_t valueAna;      //Analogue value
        lu_int16_t valueScaled;     //Analogue Scaled value
        lu_int32_t valueCounter;    //Counter value
    } value;
    lu_uint8_t status;      //flags field
    lu_uint8_t reserved;
public:
    CTHEventS104PointStr& operator=(const CTEventSi870PointStr event)
    {
        pointType = event.pointType;
        protocolID = event.protocolID;
        switch(pointType)
        {
            case EVENT_TYPE_S104_SINGLEBINARY:
            case EVENT_TYPE_S104_DOUBLEBINARY:
                value.valueDig = event.value.valueDig;
                break;
            case EVENT_TYPE_S104_ANALOGUE_FLOAT:
            case EVENT_TYPE_S104_ANALOGUE_NORMAL:
                value.valueAna = event.value.valueAna;
                break;
            case EVENT_TYPE_S104_ANALOGUE_SCALED:
                value.valueScaled = event.value.valueScaled;
                break;
            case EVENT_TYPE_S104_COUNTER:
                value.valueCounter = event.value.valueCounter;
                break;
            default:
                value.valueDig = 0;
                break;
        }
        status = event.status;
        reserved = 0;
        return *this;
    }
};

/**
 * \brief System Event
 */
struct CTHEventSystemStr : public CTEventSystemStr
{
public:
    lu_uint16_t reserved;
public:
    ADDMETHODS(CTEventSystemStr, CTHEventSystemStr);
};

/**
 * \brief Communications Event
 */
struct CTHEventCommStr : public CTEventCommStr
{
public:
    lu_uint16_t reserved;
public:
    ADDMETHODS(CTEventCommStr, CTHEventCommStr);
};

/**
 * \brief Off/Local/Remote Event
 */
struct CTHEventOLRStr : public CTEventOLRStr
{
public:
    lu_uint16_t reserved;
public:
    ADDMETHODS(CTEventOLRStr, CTHEventOLRStr);
};

/**
 * \brief Switching Event
 */
struct CTHEventSwitchStr : public CTEventSwitchStr
{
public:
    lu_uint16_t reserved;
public:
    ADDMETHODS(CTEventSwitchStr, CTHEventSwitchStr);
};

/**
 * \brief Control Logic Event
 */
struct CTHEventCLogicStr : public CTEventCLogicStr
{
public:
    lu_uint16_t reserved;
public:
    ADDMETHODS(CTEventCLogicStr, CTHEventCLogicStr);
};

/**
 * \brief Date and time change Event
 */
struct CTHEventDatetimeStr : public CTEventDatetimeStr
{
public:
    lu_uint16_t reserved;
public:
    ADDMETHODS(CTEventDatetimeStr, CTHEventDatetimeStr);
};

typedef CTEventAutoStr CTHEventAutoStr;

/**
 * \brief Event log entry format
 */
struct CTHEventEntryStr
{
    TimeSpecStr timestamp;  //time stamp of this entry
    lu_int8_t eventClass;    //type of event of this entry
    union payloadDef        //event info
    {
        CTHEventSystemStr eventSystem;
        CTHEventCommStr eventComm;
        CTHEventOLRStr eventOLR;
        CTHEventSwitchStr eventSwitch;
        CTHEventCLogicStr eventCLogic;
        CTHEventDatetimeStr eventDatetime;
        CTHEventDNP3PointStr eventDNP3;
        CTHEventS104PointStr eventS104;
        CTHEventAutoStr      eventAuto;
    } payload;
public:
    CTHEventEntryStr& operator=(const SLEventEntryStr event)
    {
        timestamp = event.timestamp;
        eventClass = event.eventClass;
        switch(eventClass)
        {
            case EVENT_CLASS_SYSTEM:
                payload.eventSystem = event.payload.eventSystem;
                break;
            case EVENT_CLASS_COMM:
                payload.eventComm = event.payload.eventComm;
                break;
            case EVENT_CLASS_OLR:
                payload.eventOLR = event.payload.eventOLR;
                break;
            case EVENT_CLASS_SWITCH:
                payload.eventSwitch = event.payload.eventSwitch;
                break;
            case EVENT_CLASS_CLOGIC:
                payload.eventCLogic = event.payload.eventCLogic;
                break;
            case EVENT_CLASS_DATETIME:
                payload.eventDatetime = event.payload.eventDatetime;
                break;
            case EVENT_CLASS_DNP3SCADA_POINT:
                payload.eventDNP3 = event.payload.eventDNP3;
                break;
            case EVENT_CLASS_S104_POINT:
            case EVENT_CLASS_S101_POINT:
                payload.eventS104 = event.payload.eventSi870;
                break;
            case EVENT_CLASS_AUTOMATION:
                payload.eventAuto = event.payload.eventAuto;
                break;
            default:
                DBG_ERR("Event class not implemented! Event class: %d", eventClass);
                assert(false);
                break;
        }
        return *this;
    }
};

#pragma pack()

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */


#endif /* CTEVENTFORMAT_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
