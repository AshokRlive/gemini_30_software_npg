/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       System & Event log manager implementation for the Configuration Tool
 *       Handler.
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   20/09/12      pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <string>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "EventReqHandler.h"
#include "AbstractCTHandler.h"
#include "CTEventFormat.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

namespace CTH
{

EventReqHandler::EventReqHandler() :
                    log(Logger::getLogger(SUBSYSTEM_ID_CT_HANDLER))
{
    /* Get EventLog instance */
    eventLog = EventLogManager::getInstance();
}

EventReqHandler::~EventReqHandler() {}


CTHMessage* EventReqHandler::decodeMessageLog(const CTMessageHeader &header,
                                       const CTMessagePayload &payload)
{
    log.info("%s \"%s\" message received.", __AT__, CTMsgID_Log_ToSTRING(header.messageID));

    /* Decode message ID */
    switch(header.messageID)
    {
        case CTMsgID_C_GetEventLog:
            return decodeGetEventListMessage(header,payload);

        case CTMsgID_C_ClearEventLog:
            return decodeClearEventLogMessage(header,payload);

        case CTMsgID_C_BeginEventLog:
            return decodeBeginEventLogMessage(header,payload);

        default:
            return createAckNack(header,CTH_NACK_UNSUPPORTED);

    }//endswitch(header.messageID)
}


CTHMessage* EventReqHandler::decodeGetEventListMessage(const CTMessageHeader &header,
                                                    const CTMessagePayload &payload)
{
    CTH_ACKNACKCODE ret = CTH_ACK;

    if(payload.payloadLen != 0)
    {
        /* Invalid payload */
        ret = CTH_NACK_PAYLOAD;
        return createAckNack(header,ret);
    }

    CTMsg_R_EventList* reply = new CTMsg_R_EventList(header.requestID, replyID.newID());

    lu_char_t *logPtr = reinterpret_cast<lu_char_t *>(&(*reply)[0]);
    lu_uint32_t paySize;

    static const lu_uint32_t maxSize = CTMsg_MaxPayloadSize / sizeof(CTHEventEntryStr);
    SLEventEntryStr* buffer = new SLEventEntryStr[maxSize];
    if(eventLog->getEventMessages((lu_uint8_t*)buffer, maxSize, paySize) == EVENTLOG_ERROR_NONE)
    {
        CTHEventEntryStr* eventEntry = (CTHEventEntryStr*)logPtr;
        for (lu_uint32_t i = 0; i < paySize; ++i)
        {
            *eventEntry = (SLEventEntryStr)(buffer[i]);
            ++eventEntry;
        }
        delete [] buffer;
        reply->setEntries(paySize * sizeof(CTHEventEntryStr));  //Num of bytes
        return reply;
    }
    else
    {
        log.info("%s Event Log is empty", __AT__);
        ret = CTH_NACK_LOGLISTEMPTY;

        delete [] buffer;
        // Delete Msg
        delete reply;
    }
    return createAckNack(header,ret);
}


CTHMessage* EventReqHandler::decodeClearEventLogMessage(const CTMessageHeader &header,
                                                            const CTMessagePayload &payload)
{
    CTH_ACKNACKCODE ret = CTH_ACK;

    if(payload.payloadLen != 0)
    {
        /* Invalid payload */
        ret =  CTH_NACK_PAYLOAD;
    }
    else
    {
        eventLog->clearEventLog();
        log.info("%s Event Log Cleared.", __AT__);
    }
    return createAckNack(header,ret);
}


CTHMessage* EventReqHandler::decodeBeginEventLogMessage(const CTMessageHeader &header,
                                                        const CTMessagePayload &payload)
{
    CTH_ACKNACKCODE ret = CTH_ACK;

    if(payload.payloadLen != 0)
    {
        /* invalid payload */
        ret = CTH_NACK_PAYLOAD;
    }
    else
    {
        eventLog->beginEventLog();
        log.info("%s Event Log position set at the beginning.", __AT__);
    }
    return createAckNack(header,ret);
}


}//end namespace CTH


/*
 *********************** End of file ******************************************
 */
