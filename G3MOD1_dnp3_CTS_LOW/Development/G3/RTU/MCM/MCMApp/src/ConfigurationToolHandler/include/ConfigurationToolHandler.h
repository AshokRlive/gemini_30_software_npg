/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Configuration Tool Handler public interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_7BBCE995_755D_476e_84A1_3115B6226548__INCLUDED_)
#define EA_7BBCE995_755D_476e_84A1_3115B6226548__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "IMCMApplication.h"
#include "IEventLogManager.h"
#include "RAWBuffer.h"
#include "IStatusManager.h"
#include "IIOModuleManager.h"
#include "GeminiDatabase.h"
#include "IMemoryManager.h"
#include "CTHMessage.h"
#include "CTHandlerCommon.h"
#include "SystemInformation.h"
#include "Diagnostic.h"
#include "AnalogueRealTimeStatus.h"
#include "CounterRealTimeStatus.h"
#include "DigitalRealTimeStatus.h"
#include "FileTransferManager.h"
#include "EngineerTest.h"
#include "LogReqHandler.h"
#include "EventReqHandler.h"
#include "AbstractCTHandler.h"
#include "ICTHLinkLayer.h"
#include "CertificateHandler.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

namespace CTH
{
/**
 * \brief Configuration Tool Protocol Handler.
 *
 * The registered link layer reference is used to write data. An observer is
 * also registered into the link layer to receive notifications when new data
 * is available. If the data is valid a reply is generated in the
 * "observer context". This class doesn't need a thread. It's event driven and
 * runs in the link layer context. The class can be easily expanded to support
 * more than one link layer at the same time
 */
class ConfigurationToolHandler : public CTH::AbstractCTHandler, private CTH::FileCRCObserver
{

public:
    /**
     * \brief Custom constructor
     *
     * \param moduleMgr reference to the module manager
     * \param database Reference to the Gemini database
     * \param linkLayer Reference to the transport link layer
     *
     * \return None
     */
    ConfigurationToolHandler( IMCMApplication& mcmApp,
                              IStatusManager& statusMgr,
                              IIOModuleManager& moduleMgr,
                              GeminiDatabase& database,
                              ICTHLinkLayer& linkLayer,
                              UserManager& userManager,
                              IMemoryManager& memoryManager
                            );
    virtual ~ConfigurationToolHandler();

    virtual void observerUpdate(lu_uint32_t* newCRC);

private:
   virtual CTHMessage* decodePayload(const CTMessageHeader &header,
                                   const CTMessagePayload &payload);
    /**
     * \brief Decode a RTData type message
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return reply message
     */
   CTHMessage* decodeMessageRTData(const CTMessageHeader &header,
                                  const CTMessagePayload &payload);

    /**
     * \brief Decode a Module type message
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return reply message
     */
    CTHMessage* decodeMessageModule(const CTMessageHeader &header,
                                    const CTMessagePayload &payload);

    /**
     * \brief Decode a "Date & Time" type message
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return reply message
     */
    CTHMessage* decodeMessageDate(const CTMessageHeader &header,
                                const CTMessagePayload &payload);

    /**
     * \brief Decode a "RTU Control" type message
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return reply message
     */
    CTHMessage* decodeMessageRTUControl(const CTMessageHeader &header,
                                       const CTMessagePayload &payload);

    /**
     * \brief Decode an "Event & Control Log" type message
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return reply message
     */
    CTHMessage* decodeMessageLog(const CTMessageHeader &header,
                                const CTMessagePayload &payload);

    /**
     * \brief Decode a RTU info type message
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return reply message
     */
    CTHMessage* decodeMessageRTUInfo(const CTMessageHeader &header,
                                    const CTMessagePayload &payload);

    /**
     * \brief Decode a Testing type message
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return reply message.
     */
    CTHMessage* decodeMessageTesting(const CTMessageHeader &header,
                                   const CTMessagePayload &payload);

    CTHMessage* decodeMessageUpgrade(const CTMessageHeader &header,
                                                             const CTMessagePayload &payload);

    /**
     * \brief Decode a Users handling type message
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return reply message.
     */
    CTHMessage* decodeMessageLogin(const CTMessageHeader &header,
                                 const CTMessagePayload &payload);

    CTHMessage* decodeMessageDiagnostic(const CTMessageHeader &header,
                                      const CTMessagePayload &payload);

    /**
     * \brief General decoder for RTData type point list messages
     *
     * \param target Reference to the list object
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return reply message.
     */
    template<typename T> CTHMessage* decodeRTPointList( T &target,
                                                    const CTMessageHeader &header,
                                                    const CTMessagePayload &payload);

    /**
     * \brief Decoder for RTU restart/reboot/shutdown actions
     *
     * \param payload Payload of the messsage
     * \param Event type to log in the Event Log
     * \param forced States whether user is trying to do the action or is forced.
     *
     * \return ACK/NACK code of the resulting action
     */
    CTH_ACKNACKCODE restartRTU(const CTMessagePayload &payload, EVENT_TYPE_SYSTEM action, bool forced = false);

private:
    IMCMApplication& mcmApplication;
    IStatusManager& statusManager;
    IIOModuleManager& moduleManager;
    IMemoryManager& memoryManager;

    /* Modules created by this handler */
    SystemInformation systemInformation;
    EngineerTest engineerTest;
    EventReqHandler eventReqHdl;
    LogReqHandler logReqHdl;
    Diagnostic diagnostic;

    DigitalRealTimeStatus digitalRTStatus;
    AnalogueRealTimeStatus analogueRTStatus;
    CounterRealTimeStatus counterRTStatus;

    CertificateHandler cert;

    IEventLogManager *eventLog;
};


}//end namespace CTH


#endif // !defined(EA_7BBCE995_755D_476e_84A1_3115B6226548__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
