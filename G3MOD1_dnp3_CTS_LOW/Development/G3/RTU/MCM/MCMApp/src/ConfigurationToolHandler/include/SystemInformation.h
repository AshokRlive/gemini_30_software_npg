/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       System Information module public interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11/10/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_2CB0E182_AAEF_4d76_8940_5C7CF27A8B19__INCLUDED_)
#define EA_2CB0E182_AAEF_4d76_8940_5C7CF27A8B19__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <time.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "GlobalDefs.h"
#include "CTHMessage.h"
#include "CTHandlerCommon.h"
#include "IStatusManager.h"
#include "IIOModuleManager.h"
#include "UserManager.h"
#include "Logger.h"
#include "IEventLogManager.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
namespace CTH
{

class SystemInformation
{

public:
    /**
     * \brief Custom constructor
     *
     * \param statusMgr Reference to the Status Manager object
     * \param moduleMgr Reference to the Module Manager object
     *
     * \return none
     */
    SystemInformation(IStatusManager& statusMgr,
                        IIOModuleManager& moduleMgr,
                        UserManager& userMgr
                        );

    virtual ~SystemInformation();

    /**
     * \brief Retrieve modules information
     *
     * Returns the information for all the modules available. If the provided
     * message buffer is not big enough, only the information for the firsts
     * modules is saved.
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return Reply message
     */
    CTHMessage* getModuleInfo(const CTMessageHeader &header,
                              const CTMessagePayload &payload
                              );

    /**
     * \brief Retrieve further module information
     *
     * Returns additional information for the specified module.
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return Reply message
     */
    CTHMessage* getModuleDetail(const CTMessageHeader &header,
                                const CTMessagePayload &payload
                                );

    /**
     * \brief Retrieve further module information
     *
     * Returns additional information for the specified module.
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return Reply message
     */
    CTHMessage* getModuleCANStats(const CTMessageHeader &header,
                                  const CTMessagePayload &payload
                                  );

    /**
     * \brief Retrieve modules alarms list.
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return Reply message
     */
    CTHMessage* getModuleAlarm(const CTMessageHeader &header,
                               const CTMessagePayload &payload
                               );

    /**
     * \brief Acknowledge modules alarms list.
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return Reply message
     */
    CTHMessage* ackModuleAlarm(const CTMessageHeader &header,
                               const CTMessagePayload &payload
                               );

    /**
     * \brief Returns message with current date and time.
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return Reply message
     */
    CTHMessage* decodeGetDateMessage(const CTMessageHeader &header,
                                     const CTMessagePayload &payload
                                     );

    /**
     * \brief Process Set Date message.
     *
     * It checks message and sets RTU's date and time.
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return Reply message
     */
    CTHMessage* decodeSetDateMessage(const CTMessageHeader &header,
                                     const CTMessagePayload &payload
                                     );


    /**
     * \brief Process System API info request message.
     *
     * Returns list composing different System's API versions.
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return Reply message
     */
    CTHMessage* decodeGetSysAPI(const CTMessageHeader &header,
                                const CTMessagePayload &payload
                                );

    /**
     * \brief Process Get RTU Information message.
     *
     * Returns several data about the RTU.
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return Reply message
     */
    CTHMessage* decodeGetRTUInfo(const CTMessageHeader &header,
                                 const CTMessagePayload &payload
                                 );

    /**
     * \brief Process Get Config Information message.
     *
     * Returns data about the Configuration File.
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return Reply message
     */
    CTHMessage* decodeGetConfigInfo(const CTMessageHeader &header,
                                    const CTMessagePayload &payload
                                    );

    /**
     * \brief Process Get Network Information message.
     *
     * Returns information of every Ethernet network device.
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return Reply message
     */
    CTHMessage* decodeGetNetInfo(const CTMessageHeader &header,
                                 const CTMessagePayload &payload
                                 );

    /**
     * \brief Process Get Battery Information message.
     *
     * Returns information of the configured battery.
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return Reply message
     */
    CTHMessage* decodeGetBatteryInfo(const CTMessageHeader &header,
                                     const CTMessagePayload &payload
                                     );

    /**
     * \brief Process Check Alive message.
     *
     * Checks if the session ID is valid and return ACK or NACK. When valid,
     * session is refreshed.
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return Reply message
     */
    CTHMessage* decodeCheckAlive(const CTMessageHeader &header,
                                 const CTMessagePayload &payload
                                 );

    /**
     * \brief Process a User Login message.
     *
     * Check user info against stored user list and returns the user's access
     * level.
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return Reply message
     */
    CTHMessage* decodeUserLoginMessage(const CTMessageHeader &header,
                                       const CTMessagePayload &payload
                                       );

    CTHMessage* decodeGetMDMessage(const CTMessageHeader &header,
                                          const CTMessagePayload &payload
                                          );


private:
    IStatusManager &statusManager;
    IIOModuleManager &moduleManager;
    UserManager& userManager;
    Logger &log;
    IEventLogManager *eventLog;

    IChannel* doorChannel;  //Door open channel
};

}//end namespace CTH


#endif // !defined(EA_2CB0E182_AAEF_4d76_8940_5C7CF27A8B19__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
