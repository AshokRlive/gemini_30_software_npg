/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Configuration Tool Virtual point real time data common interface
 *       implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <cstring>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "VirtualPointRealTimeStatus.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

VirtualPointRealTimeStatus::VirtualPointRealTimeStatus(GeminiDatabase &database) :
                                                       database(database),
                                                       pointList(NULL),
                                                       pointListSizeMax(0),
                                                       pointListSize(0),
                                                       lastIdx(0)
{
    /* Allocate the point list for virtual points + Control Logic virtual points */
    lu_uint32_t totalSize = database.getVPointNum(0);   //group 0 VPs
    lu_uint32_t cLogicNum = database.getCLogicNum();
    for (lu_uint32_t clIndex = 1; clIndex <= cLogicNum; ++clIndex)
    {
        totalSize += database.getVPointNum(clIndex);    //CL VPs
    }
    pointList = new CTMsgPL_C_PointID[totalSize];
    if(pointList != NULL)
    {
        pointListSizeMax = totalSize;
    }

    pointListTable.setTable(pointList, pointListSizeMax);
}

VirtualPointRealTimeStatus::~VirtualPointRealTimeStatus()
{
    if(pointList != NULL)
    {
        delete[] pointList;
    }
}

CTPointListStrTable &VirtualPointRealTimeStatus::getPointList(void)
{
    /* Reset list size */
    pointListSize = 0;
    lastIdx = 0;

    return pointListTable;
}

CTH_ERROR VirtualPointRealTimeStatus::setPointList(lu_uint32_t size)
{
    CTH_ERROR ret = CTH_ERROR_NONE;

    /* Simply set the list size */
    if(size <= pointListSizeMax)
    {
        pointListSize = size;
        lastIdx = 0;
    }
    else
    {
        /* List too big */
        ret = CTH_ERROR_PARAM;

        /* Reset list size and internal counter */
        pointListSize = 0;
        lastIdx = 0;
    }

    return ret;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
