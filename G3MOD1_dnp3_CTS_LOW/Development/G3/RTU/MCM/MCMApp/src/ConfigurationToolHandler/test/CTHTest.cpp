/*
 * CTHTest.cpp
 *
 *  Created on: 12 Oct 2011
 *      Author: galli_m
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <errno.h>
#include <mqueue.h>
#include <unistd.h>


#include "CTHandlerCommon.h"

#define IPC_SEC_TO_NSEC(a)       (a * 1000000000)
#define IPC_MSEC_TO_NSEC(a)      (a * 1000000)
#define IPC_USEC_TO_NSEC(a)      (a * 1000)

static mqd_t queueHandlerIn;
static mqd_t queueHandlerOut;

static __inline__ void CTHAddTime(struct timespec *time, lu_uint32_t timeMs)
{
    if (time != NULL)
    {
        time->tv_sec += (lu_uint32_t)(timeMs / 1000);
        time->tv_nsec += IPC_MSEC_TO_NSEC((timeMs % 1000));

        if (time->tv_nsec  >= IPC_SEC_TO_NSEC(1))
        {
           time->tv_sec++;
           time->tv_nsec -= IPC_SEC_TO_NSEC(1);
        }
    }
}

static __inline__ lu_uint32_t CTHGetMin(lu_uint32_t a, lu_uint32_t b)
{
    return (a < b) ? a : b;
}

static CTH_ERROR sendIPCMessage( lu_int8_t *command,
                                 lu_int32_t commandSize,
                                 lu_int8_t *payload,
                                 lu_int32_t payloadSize
                                )
{
    lu_int32_t retVal;
    lu_int32_t errTmp;
    struct timespec timeOut;
    CTH_ERROR ret = CTH_ERROR_NONE;

    if( (command == NULL) ||
        (commandSize > CTH_MQUEUE_MESSAGE_SIZE)
      )
    {
        return CTH_ERROR_PARAM;
    }

    clock_gettime(CLOCK_REALTIME, &timeOut);
    CTHAddTime(&timeOut, 100);
    retVal = mq_timedsend( queueHandlerIn,
                          command,
                          commandSize,
                          CTH_MQUEUE_DEFAULT_PRIO,
                          &timeOut
                         );
    if(retVal < 0)
    {
        /* Error */
        errTmp = errno;
        printf("sendIPCMessage Error %i sending command\n", errTmp);
        ret =  CTH_ERROR_IPC;
    }

    if( (ret == CTH_ERROR_NONE) &&
        (payload != NULL)
      )
    {
        lu_uint32_t currentIdx = 0;
        lu_uint32_t size;

        while(payloadSize > 0)
        {
            size = CTHGetMin(CTH_MQUEUE_MESSAGE_SIZE, payloadSize);

            /* Send payload */
            clock_gettime(CLOCK_REALTIME, &timeOut);
            CTHAddTime(&timeOut, 100);
            retVal = mq_timedsend( queueHandlerIn,
                                   &payload[currentIdx],
                                   size,
                                   CTH_MQUEUE_DEFAULT_PRIO,
                                   &timeOut
                                 );
            if(retVal < 0)
            {
                /* Error */
                errTmp = errno;
                printf( "sendIPCMessage Error %i sending payload (block size %i - sent: %i - remaining: %i)\n",
                        errTmp, size, currentIdx, payloadSize
                      );
                ret =  CTH_ERROR_IPC;
                break;
            }
            else
            {
                /* Update payload counters */
                payloadSize -= size;
                currentIdx += size;
            }
        }
    }

    return ret;
}

CTH_ERROR recvIPCPayload( lu_int8_t *payload,
                          lu_int32_t payloadSize
                        )
{
    lu_uint32_t currentIdx = 0;
    lu_int32_t recvLen;
    struct timespec timeOut;
    CTH_ERROR ret = CTH_ERROR_NONE;

    while(payloadSize > 0)
    {
        //MG Use temporary buffer ????
        clock_gettime(CLOCK_REALTIME, &timeOut);
        CTHAddTime(&timeOut, 100);
        recvLen = mq_timedreceive( queueHandlerOut,
                                   &payload[currentIdx],
                                   CTH_MQUEUE_MESSAGE_SIZE,
                                   CTH_MQUEUE_DEFAULT_PRIO,
                                   &timeOut
                                 );

        if(recvLen < 0)
        {
            /* Error */
            lu_int32_t errTmp = errno;
            printf("ConfigurationToolHandlerProxyServer receive payload error %i\n",
                    errTmp
                   );

            ret = CTH_ERROR_IPC;
            break;
        }
        else
        {
            /* Update receive counters */
            payloadSize -= recvLen;
            currentIdx += recvLen;
        }
    }

    return ret;
}

void moudleInfoTest(void)
{
    lu_int32_t errTmp;
    lu_int32_t retVal;
    struct timespec timeOut;
    IPCCommandHeaderStr IPCCommand;
    CTH_ERROR cthRet;

    printf("Sending module info request...\n");
    IPCCommand.command = IPC_COMMAND_MODULE_INFO_C;
    IPCCommand.payloadSize = 0;

    clock_gettime(CLOCK_REALTIME, &timeOut);
    CTHAddTime(&timeOut, 200);
    retVal = mq_timedsend( queueHandlerIn,
                      (lu_int8_t*)&IPCCommand,
                      sizeof(IPCCommand),
                      CTH_MQUEUE_DEFAULT_PRIO,
                      &timeOut
                    );
    if(retVal < 0)
    {
        errTmp = errno;
        printf("Error %i sending message\n", retVal);
    }
    else
    {
        /* Wait reply */
        lu_int8_t msgBuffer[CTH_MQUEUE_MESSAGE_SIZE];
        lu_uint32_t payloadSize;
        IPCResponseHeaderStr *IPCResponseHeaderPtr = (IPCResponseHeaderStr*)msgBuffer;
        lu_int32_t readLen;

        printf("Waiting reply...\n");

        clock_gettime(CLOCK_REALTIME, &timeOut);
        CTHAddTime(&timeOut, 200);
        readLen = mq_timedreceive(queueHandlerOut, msgBuffer, sizeof(msgBuffer), NULL, &timeOut);
        if((readLen < 0) || (readLen < sizeof(IPCResponseHeaderStr)))
        {
            printf("Invalid header\n");
        }
        else
        {
            printf("Command: %i - Error code: %i - Payload: %i\n", IPCResponseHeaderPtr->command, IPCResponseHeaderPtr->payload, IPCResponseHeaderPtr->payloadSize);
            payloadSize = IPCResponseHeaderPtr->payloadSize;

            /* Read payload */
            cthRet = recvIPCPayload(msgBuffer, payloadSize);
            if(cthRet != CTH_ERROR_NONE)
            {
                printf("Invalid payload: %i\n", cthRet);
            }
            else
            {
                lu_uint32_t i;
                CTMsgPL_R_ModuleInfo *moduleInfoPtr= (CTMsgPL_R_ModuleInfo*)msgBuffer;

                printf("Modules:\n");
                for(i = 0; i < (payloadSize / sizeof(CTMsgPL_R_ModuleInfo)); i++)
                {
                    printf( "\t%i - Type: %i - ID: %i - Serial: %09i - SW ver: %i.%i - online: %i\n",
                            i, moduleInfoPtr[i].type,
                            moduleInfoPtr[i].id,
                            moduleInfoPtr[i].serial,
                            moduleInfoPtr[i].sw.major,
                            moduleInfoPtr[i].sw.minor,
                            moduleInfoPtr[i].online
                          );
                }
            }
        }
    }
}


void digitalRTTest(int points)
{
    lu_int32_t errTmp;
    struct timespec timeOut;
    IPCCommandHeaderStr IPCCommand;
    CTH_ERROR cthRet;
    CTPointListStr *pointList = (CTPointListStr*)(malloc(points * sizeof(CTPointListStr)));
    lu_uint32_t i;
    lu_uint64_t pollCommandCounter = 0;

    for(i = 0; i < points; i++)
    {
        pointList[i].point.group = 0;
        pointList[i].point.ID = i;
    }

    printf("Sending digital point list...\n");
    IPCCommand.command = IPC_COMMAND_DIGITAL_RT_SET_LIST_C;
    IPCCommand.payloadSize = points * sizeof(CTPointListStr);

    cthRet = sendIPCMessage((lu_int8_t*)(&IPCCommand), sizeof(IPCCommand), (lu_int8_t*)(&pointList[0]), IPCCommand.payloadSize);

    if(cthRet != CTH_ERROR_NONE)
    {
        errTmp = errno;
        printf("Error %i sending message\n", cthRet);
    }
    else
    {
        /* Wait reply */
        lu_int8_t msgBuffer[CTH_MQUEUE_MESSAGE_SIZE];
        lu_uint32_t payloadSize;
        IPCResponseHeaderStr *IPCResponseHeaderPtr = (IPCResponseHeaderStr*)msgBuffer;
        lu_int32_t readLen;

        printf("Waiting reply...\n");

        clock_gettime(CLOCK_REALTIME, &timeOut);
        CTHAddTime(&timeOut, 200);
        readLen = mq_timedreceive(queueHandlerOut, msgBuffer, sizeof(msgBuffer), NULL, &timeOut);
        if((readLen < 0) || (readLen < sizeof(IPCResponseHeaderStr)))
        {
            printf("Invalid header\n");
        }
        else
        {
            printf("Command: %i - Error code: %i - Payload: %i\n", IPCResponseHeaderPtr->command, IPCResponseHeaderPtr->payload, IPCResponseHeaderPtr->payloadSize);

            while(LU_TRUE)
            {
                printf("Send poll command (%lli)...\n", pollCommandCounter++);

                IPCCommand.command = IPC_COMMAND_DIGITAL_RT_POLL_C;
                IPCCommand.payloadSize = 0;

                cthRet = sendIPCMessage((lu_int8_t*)(&IPCCommand), sizeof(IPCCommand),NULL, 0);
                if(cthRet != CTH_ERROR_NONE)
                {
                    errTmp = errno;
                    printf("Error %i sending message\n", cthRet);
                }
                else
                {
                    /* Wait reply */
                    lu_int8_t msgBuffer[CTH_MQUEUE_MESSAGE_SIZE];
                    lu_uint32_t payloadSize;
                    IPCResponseHeaderStr *IPCResponseHeaderPtr = (IPCResponseHeaderStr*)msgBuffer;
                    lu_int32_t readLen;

                    printf("Waiting reply...\n");

                    clock_gettime(CLOCK_REALTIME, &timeOut);
                    CTHAddTime(&timeOut, 200);
                    readLen = mq_timedreceive(queueHandlerOut, msgBuffer, sizeof(msgBuffer), NULL, &timeOut);

                    if((readLen < 0) || (readLen < sizeof(IPCResponseHeaderStr)))
                    {
                        printf("Invalid header\n");
                    }
                    else
                    {

                        CTMsgPL_R_RTPointData_DigitalNOTS       RTDataArray[200];
                        printf("Header received. Command: %i - Error: %i - Payload: %i\n", IPCResponseHeaderPtr->command, IPCResponseHeaderPtr->payload, IPCResponseHeaderPtr->payloadSize);
                        /* Read payload */
                        payloadSize = IPCResponseHeaderPtr->payloadSize;

                        /* Read payload */
                        cthRet = recvIPCPayload((lu_int8_t*)(RTDataArray), payloadSize);
                        if(cthRet != CTH_ERROR_NONE)
                        {
                            printf("Invalid payload: %i\n", cthRet);
                        }
                        else
                        {
                            printf("Valid payload\n");
                            for(lu_uint32_t i = 0; i < payloadSize / sizeof(CTMsgPL_R_RTPointData_DigitalNOTS); i++)
                            {
                                printf("Point %i:%i Valid: %i - Online: %i - Chatter: %i - Value: %i\n",
                                        RTDataArray[i].ID.group, RTDataArray[i].ID.ID,
                                        RTDataArray[i].valid,
                                        RTDataArray[i].online,
                                        RTDataArray[i].chatter,
                                        RTDataArray[i].value
                                      );
                            }
                        }
                    }
                }

                printf("\n");
                usleep(100000);
            }
        }
    }
}

int main(int argc, char *argv[])
{
    lu_int32_t errTmp;
    lu_int32_t oflag = O_RDWR;
    struct mq_attr attr;

    attr.mq_flags   = 0                             ;
    attr.mq_curmsgs = 0                             ;
    attr.mq_maxmsg  = CTH_MQUEUE_MAX_NUMBER_MESSAGES;
    attr.mq_msgsize = CTH_MQUEUE_MESSAGE_SIZE       ;

    printf("Opening message queue: %s\n", CTH_MQUEUE_NAME_IN);
    queueHandlerIn = mq_open( CTH_MQUEUE_NAME_IN,
                         oflag                  ,
                         CTH_MQUEUE_DEFAULT_MODE,
                         &attr
                       );
    if(queueHandlerIn < 0)
    {
        /* Error */
        errTmp = errno;
        printf("Error opening message queue: %i\n", errTmp);
        return 0;
    }

    printf("Opening message queue: %s\n", CTH_MQUEUE_NAME_OUT);
    queueHandlerOut = mq_open( CTH_MQUEUE_NAME_OUT,
                         oflag                   ,
                         CTH_MQUEUE_DEFAULT_MODE ,
                         &attr
                       );
    if(queueHandlerOut < 0)
    {
        /* Error */
        errTmp = errno;
        printf("Error opening message queue: %i\n", errTmp);
        return 0;
    }


    moudleInfoTest();
    int points = (argc > 1) ? atoi(argv[1]) : 0;
    printf("Poll %i points\n", points);
    if(points > 0)
    {
        digitalRTTest(points);
    }


    printf("Closing message queue: %s\n", CTH_MQUEUE_NAME_IN);
    if ( mq_close(queueHandlerIn) < 0)
    {
        errTmp = errno;
        printf("Error closing message queue: %i\n", errTmp);
    }

    printf("Closing message queue: %s\n", CTH_MQUEUE_NAME_OUT);
    if ( mq_close(queueHandlerOut) < 0)
    {
        errTmp = errno;
        printf("Error closing message queue: %i\n", errTmp);
    }

    return 0;

}
