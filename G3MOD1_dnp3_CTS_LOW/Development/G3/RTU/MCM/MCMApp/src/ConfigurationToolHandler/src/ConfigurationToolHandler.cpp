/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "ConfigurationToolHandler.h"
#include "ModuleRegistrationManager.h"
#include "AutomationManager.h"

namespace CTH
{
/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */


ConfigurationToolHandler::ConfigurationToolHandler(
			IMCMApplication& mcmApp,
			IStatusManager& statusMgr,
			IIOModuleManager& moduleMgr,
			GeminiDatabase& database,
			ICTHLinkLayer& linkLayer,
			UserManager& userManager,
			IMemoryManager& memoryMgr
		  ) :
		  AbstractCTHandler(linkLayer),
		  mcmApplication(mcmApp),
		  statusManager(statusMgr),
		  moduleManager(moduleMgr),
		  memoryManager(memoryMgr),
		  systemInformation(statusMgr, moduleMgr, userManager),
		  engineerTest(statusMgr, database),
		  eventReqHdl(),
		  logReqHdl(),
		  diagnostic(moduleMgr),
		  digitalRTStatus(database),
		  analogueRTStatus(database),
		  counterRTStatus(database),
		  cert()
{
    eventLog = EventLogManager::getInstance();
    fileTransfer.attach(this);
}


ConfigurationToolHandler::~ConfigurationToolHandler()
{
    fileTransfer.detach(this);
}


void ConfigurationToolHandler::observerUpdate(lu_uint32_t* newCRC)
{
    /* store new Configuration file's CRC code in NV-memory */
    memoryManager.write(IMemoryManager::STORAGE_VAR_CFGFILECRC, (lu_uint8_t*)newCRC);
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

CTHMessage* ConfigurationToolHandler::decodePayload(const CTMessageHeader &header,
                                                    const CTMessagePayload &payload)
{
    /* Decode payload */
    switch(header.messageType)
    {
        case CTMsgType_PointData:
            return decodeMessageRTData(header, payload);
        break;

        case CTMsgType_Module:
            return decodeMessageModule(header, payload);
        break;

        case CTMsgType_Date:
            return decodeMessageDate(header, payload);
        break;

        case CTMsgType_RTUControl:
            return decodeMessageRTUControl(header, payload);
        break;

        case CTMsgType_Log:
            return decodeMessageLog(header, payload);
        break;

        case CTMsgType_RTUInfo:
            return decodeMessageRTUInfo(header, payload);
        break;

        case CTMsgType_Testing:
            return decodeMessageTesting(header, payload);
        break;

        case CTMsgType_Login:
            return decodeMessageLogin(header, payload);
        break;

        case CTMsgType_Diagnostic:
            return decodeMessageDiagnostic(header, payload);
        break;

        case CTMsgType_Upgrade:
            return decodeMessageUpgrade(header, payload);

        case CTMsgType_Certificate:
           return cert.decodeCertificateMessage(header, payload);

        default:
            /* ---------Unsupported Message Type-----------*/
           return createAckNack(header, CTH_NACK_UNSUPPORTED);
    }
}

CTHMessage* ConfigurationToolHandler::decodeMessageRTData(const CTMessageHeader &header,
                                                          const CTMessagePayload &payload)
{
    log.info("%s \"%s\" message received.", __AT__, CTMsgID_PointData_ToSTRING(header.messageID));

    /* Decode message ID */
    switch(header.messageID)
    {
        case CTMsgID_C_SetPointDList:
            return decodeRTPointList(digitalRTStatus, header, payload);
        break;

        case CTMsgID_C_SetPointAList:
            return decodeRTPointList(analogueRTStatus, header, payload);
        break;

        case CTMsgID_C_SetPointCList:
            return decodeRTPointList(counterRTStatus, header, payload);
        break;

        case CTMsgID_C_PollPointDigital:
            if(payload.payloadLen != 0)
            {
                return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
            }
            CTMsg_R_RTPointData_Digital* rtDigital;
            rtDigital = new CTMsg_R_RTPointData_Digital(header.requestID, replyID.newID());
            if(digitalRTStatus.getRTPointData(*rtDigital) == CTH_ERROR_NONE)
            {
                log.info("%s send Digital Data", __AT__);
                return rtDigital;
            }
            else
            {
                delete rtDigital;
                return createAckNack(header, CTH_NACK_NOLIST);
            }
            break;

        case CTMsgID_C_PollPointAnalogue:
            if(payload.payloadLen != 0)
            {
                return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
            }
            CTMsg_R_RTPointData_Analog* rtAnalogue;
            rtAnalogue = new CTMsg_R_RTPointData_Analog(header.requestID, replyID.newID());
            if(analogueRTStatus.getRTPointData(*rtAnalogue) == CTH_ERROR_NONE)
            {
                log.info("%s send Analogue Data", __AT__);
                return rtAnalogue;
            }
            else
            {
                delete rtAnalogue;
                return createAckNack(header, CTH_NACK_NOLIST);
            }
            break;

        case CTMsgID_C_PollPointCounter:
            if(payload.payloadLen != 0)
            {
                return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
            }
            CTMsg_R_RTPointData_Counter* rtCounter;
            rtCounter = new CTMsg_R_RTPointData_Counter(header.requestID, replyID.newID());
            if(counterRTStatus.getRTPointData(*rtCounter) == CTH_ERROR_NONE)
            {
                log.info("%s send Counter Data", __AT__);
                return rtCounter;
            }
            else
            {
                delete rtCounter;
                return createAckNack(header, CTH_NACK_NOLIST);
            }
            break;

        default:
            /* Unsupported message type */
            return createAckNack(header, CTH_NACK_UNSUPPORTED);;

    }//endswitch(header.messageID)
    return NULL;
}


CTHMessage* ConfigurationToolHandler::decodeMessageModule(const CTMessageHeader &header,
                                                            const CTMessagePayload &payload)
{
    log.info("%s \"%s\" message received.", __AT__, CTMsgID_Module_ToSTRING(header.messageID));

    switch(header.messageID)
    {
        case CTMsgID_C_ModuleInfo:
            return systemInformation.getModuleInfo(header, payload);
            break;

        case CTMsgID_C_ModuleDetail:
            return systemInformation.getModuleDetail(header, payload);
            break;

        case CTMsgID_C_ModuleAlarm:
            return systemInformation.getModuleAlarm(header, payload);
        	break;

        case CTMsgID_C_ModuleClearAlarm:
            return systemInformation.ackModuleAlarm(header, payload);
            break;

        case CTMsgID_C_ModuleCANStats:
            return systemInformation.getModuleCANStats(header, payload);
            break;
        default:
            return createAckNack(header, CTH_NACK_UNSUPPORTED);;

    }//endswitch(header.messageID)
    return NULL;
}


CTHMessage* ConfigurationToolHandler::decodeMessageDate(const CTMessageHeader &header,
                                                        const CTMessagePayload &payload)
{
    log.info("%s \"%s\" message received.", __AT__, CTMsgID_Date_ToSTRING(header.messageID));

    /* Decode message ID */
    switch(header.messageID)
    {
        case CTMsgID_C_GetDate:
            /* Get date message */
            return systemInformation.decodeGetDateMessage(header, payload);
            break;

        case CTMsgID_C_SetDate:
            /* Set date message */
            return systemInformation.decodeSetDateMessage(header, payload);
            break;

        default:
            /* Unsupported message type */
            return createAckNack(header, CTH_NACK_UNSUPPORTED);

    }//endswitch(header.messageID)
}


CTHMessage* ConfigurationToolHandler::decodeMessageRTUControl(const CTMessageHeader &header,
                                                              const CTMessagePayload &payload)
{
    log.info("%s \"%s\" message received.", __AT__, CTMsgID_RTUControl_ToSTRING(header.messageID));
    CTH_ACKNACKCODE result= CTH_ACK;

    /* Decode message ID */
    switch(header.messageID)
    {
        case CTMsgID_C_OnServiceMode:
            /* Put RTU on service */
            return engineerTest.decodeSetServiceModeMessage(header, payload, SERVICEMODE_INSERVICE);
        break;

        case CTMsgID_C_OutServiceMode:
        {
            /* Put RTU Out of service */
            return engineerTest.decodeSetServiceModeMessage(header, payload, SERVICEMODE_OUTSERVICE);
        }
        break;

        case CTMsgID_C_GetServiceMode:
        {
            /* Get the Service mode */
            return engineerTest.decodeGetServiceModeMessage(header, payload);
        }
        break;

        case CTMsgID_C_Forced_Reset:
        {
            /* Reset the RTU */
            result = restartRTU(payload, EVENT_TYPE_SYSTEM_REBOOTREQ, true);
        }
        break;

        case CTMsgID_C_Reset:
        {
            /* Reset the RTU */
            result = restartRTU(payload, EVENT_TYPE_SYSTEM_REBOOTREQ);
        }
        break;

        case CTMsgID_C_Forced_Restart:
        {
            /* Reset the RTU */
            result = restartRTU(payload, EVENT_TYPE_SYSTEM_RESTARTREQ, true);
        }
        break;

        case CTMsgID_C_Restart:
        {
            /* Reset the RTU */
            result = restartRTU(payload, EVENT_TYPE_SYSTEM_RESTARTREQ);
        }
        break;

        case CTMsgID_C_Forced_Shutdown:
        {
            /* Reset the RTU */
            result = restartRTU(payload, EVENT_TYPE_SYSTEM_SHUTDOWNREQ, true);
        }
        break;

        case CTMsgID_C_Shutdown:
        {
            /* Reset the RTU */
            result = restartRTU(payload, EVENT_TYPE_SYSTEM_SHUTDOWNREQ);
        }
        break;

        case CTMsgID_C_Register:
        {
            /*Do commissioning/module registration */

            /* Validation */
            if(payload.payloadLen != 0)
            {
                result = CTH_NACK_PAYLOAD;
            }
            else
            {
                result = (ModuleRegistrationManager::getInstance()->commissionModules(moduleManager)
                                                              == MRDB_ERROR_NONE)?
                                              CTH_ACK : CTH_NACK_COMMISSIONFAIL;
            }
        }
        break;

        case CTMsgID_C_EraseConfig:
        {
            /*Erase RTU configuration*/
        	return fileTransfer.eraseConfig(header,payload);
        }
        break;

        case CTMsgID_C_EraseCommissioningCache:
        {
            /*Erase RTU commissioning cache*/
        	return fileTransfer.eraseCommissioningCache(header,payload);
        }
        break;


        case CTMsgID_C_FactoryReset:
        {
            /* Reset RTU Configuration to factory default */
            result = (statusManager.factoryReset() == MCM_ERROR_NONE)?
                                            CTH_ACK : CTH_NACK_GENERIC;
        }
        break;

        case CTMsgID_C_ActivateConfig:
        {
            /* Activate configuration */
            result = fileTransfer.activateConfigFile();
        }
        break;

        case CTMsgID_C_RestoreConfig:
        {
            /* Restore configuration */
            result = fileTransfer.restoreConfigFile();
        }
        break;

        case CTMsgID_C_EnableDebugMode:
        {
            setAutomationDebugMode(true);
        }
        break;

        case CTMsgID_C_DisableDebugMode:
        {
            setAutomationDebugMode(false);
        }
        break;


        case CTMsgID_C_GetDebugMode:
        {
            /* Compose return message */
           CTMsg_R_DebugMode* reply = new CTMsg_R_DebugMode(header.requestID, replyID.newID());
           CTMsgPL_R_DebugMode* rPayload;
           rPayload = reinterpret_cast<CTMsgPL_R_DebugMode*>(reply->firstEntry());
           rPayload->rtuDebugEnabled = isAutomationDebugMode();
           reply->setEntries(1);
           return reply;
        }
        break;

        default:
            /* Unsupported message type */
            return createAckNack(header, CTH_NACK_UNSUPPORTED);

    }//endswitch(header.messageID)

    return createAckNack(header, result);
}


CTHMessage* ConfigurationToolHandler::decodeMessageLog(const CTMessageHeader &header,
                                                       const CTMessagePayload &payload)
{
    //Note that the message reception is logged inside decodeMessageLog() calls

    /* Decode message ID */
    switch(header.messageID)
    {
        case CTMsgID_C_GetLastLog:
        case CTMsgID_C_GetLogLevel:
        case CTMsgID_C_SetLogLevel:
        case CTMsgID_C_GetLogFileName:
            return logReqHdl.decodeMessageLog(header,payload);

        case CTMsgID_C_GetEventLog:
        case CTMsgID_C_ClearEventLog:
        case CTMsgID_C_BeginEventLog:
            return eventReqHdl.decodeMessageLog(header,payload);

        default:
            /* Unsupported message type */
            return createAckNack(header, CTH_NACK_UNSUPPORTED);

    }//endswitch(header.messageID)
    return NULL;
}


CTHMessage* ConfigurationToolHandler::decodeMessageRTUInfo(const CTMessageHeader &header,
                                                           const CTMessagePayload &payload)
{
    log.info("%s \"%s\" message received.", __AT__, CTMsgID_RTUInfo_ToSTRING(header.messageID));

    /* Decode message ID */
    switch(header.messageID)
    {
        case CTMsgID_C_GetSysAPI:
            /* Get G3 System API version message */
            return systemInformation.decodeGetSysAPI(header, payload);
            break;

        case CTMsgID_C_GetRTUInfo:
            /* Get RTU info message*/
            return systemInformation.decodeGetRTUInfo(header, payload);
            break;

        case CTMsgID_C_GetNetInfo:
            /* Get network info message*/
            return systemInformation.decodeGetNetInfo(header, payload);
            break;

        case CTMsgID_C_GetConfigInfo:
            /* Get Configuration-related info message */
            return systemInformation.decodeGetConfigInfo(header, payload);
            break;

        case CTMsgID_C_GetBatteryCfg:
            /* Get battery info message */
            return systemInformation.decodeGetBatteryInfo(header, payload);
            break;

        case CTMsgID_C_GetSDPVersion:
           return decodeGetSDPVersion(header, payload);

        default:
            /* Unsupported message type */
            return createAckNack(header, CTH_NACK_UNSUPPORTED);;

    }//endswitch(header.messageID)
    return NULL;
}


CTHMessage* ConfigurationToolHandler::decodeMessageTesting(const CTMessageHeader &header,
                                                           const CTMessagePayload &payload)
{
    log.info("%s \"%s\" message received.", __AT__, CTMsgID_Testing_ToSTRING(header.messageID));

    /* Decode message ID */
    switch(header.messageID)
    {
        case CTMsgID_C_GetOLR:
            /* Get Off/Local/Remote status*/
            return engineerTest.decodeGetOLRMessage(header, payload);
            break;

        case CTMsgID_C_SetOLR:
            /* Set Off/Local/Remote status*/
            return engineerTest.decodeSetOLRMessage(header, payload);
            break;

        case CTMsgID_C_OperateSwitch:
            /* Operate a switch */
            return engineerTest.decodeSwitchMessage(header, payload);
            break;

        case CTMsgID_C_OperateCLogic:
            /* Control Logic operation */
            return engineerTest.decodeControlLogicOperationMessage(header, payload);
            break;

        default:
            /* Unsupported message type */
            return createAckNack(header, CTH_NACK_UNSUPPORTED);;
    }//endswitch(header.messageID)
    return NULL;
}


CTHMessage* ConfigurationToolHandler::decodeMessageUpgrade(const CTMessageHeader &header,
                                                         const CTMessagePayload &payload)
{
    log.info("%s \"%s\" message received.", __AT__, CTMsgID_Upgrade_ToSTRING(header.messageID));

    if(payload.payloadLen != 0)
    {
        return createAckNack(header, CTH_NACK_PAYLOAD);
    }
    else
    {
        /* Decode message ID */
        switch(header.messageID)
        {
            case CTMsgID_C_ForcedUpgradeModeOn:
            {
                /* Enter upgrading mode*/
                bool ret = mcmApplication.restart(IMCMApplication::RESTART_TYPE_FORCED,
                                                  APP_EXIT_CODE_START_MCM_UPDATER);
                return createAckNack(header, (ret)? CTH_ACK : CTH_NACK_RESTARTREFUSAL);
                break;
            }

            case CTMsgID_C_UpgradeModeOn:
            {
                /* Enter upgrading mode*/
                bool ret = mcmApplication.restart(IMCMApplication::RESTART_TYPE_TRY,
                                                  APP_EXIT_CODE_START_MCM_UPDATER);
                return createAckNack(header, (ret)? CTH_ACK : CTH_NACK_RESTARTREFUSAL);
                break;
            }

            case CTMsgID_C_UpgradeModeOff:
                /* Exit upgrading mode*/
                //Do nothing since MCMApplication is never in upgrade mode.
                return createAckNack(header, CTH_ACK);
                break;

            default:
                /* ---------Unsupported Message IDs-----------*/
               return createAckNack(header, CTH_NACK_UNSUPPORTED);

        }//endswitch(header.messageID)
    }
}


CTHMessage* ConfigurationToolHandler::decodeMessageLogin(const CTMessageHeader &header,
                                                         const CTMessagePayload &payload)
{
    log.info("%s \"%s\" message received.", __AT__, CTMsgID_Login_ToSTRING(header.messageID));

    /* Decode message ID */
    switch(header.messageID)
    {
        case CTMsgID_C_CheckAlive:
            /* Alive check: reply App status or NACK */
            return systemInformation.decodeCheckAlive(header, payload);
            break;

        case CTMsgID_C_UserLogin:
            /* User ID check */
            return systemInformation.decodeUserLoginMessage(header, payload);
            break;

        case CTMsgID_C_GetMDAlgorithm:
            return systemInformation.decodeGetMDMessage(header, payload);
            break;

        default:
            /* Unsupported message type */
            return createAckNack(header, CTH_NACK_UNSUPPORTED);;

    }//endswitch(header.messageID)
    return NULL;
}


CTHMessage* ConfigurationToolHandler::decodeMessageDiagnostic(const CTMessageHeader &header,
                                                              const CTMessagePayload &payload)
{
    log.info("%s \"%s\" message received.", __AT__, CTMsgID_Diagnostic_ToSTRING(header.messageID));

    switch(header.messageID)
    {
        case CTMsgID_C_GetChannel:
            return diagnostic.decodePollChannel(header, payload);
            break;

        //TODO refactoring needed
        case CTMsgID_C_GetPointDigital:
        {
            if( (payload.payloadLen % sizeof(CTMsgPL_C_PointID)) != 0 )
            {
                return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
            }
            /* Calculate entries number */
            lu_uint32_t pointNum = payload.payloadLen / sizeof(CTMsgPL_C_PointID);
            CTMsgPL_C_PointID *pointList = new CTMsgPL_C_PointID[pointNum];

            // Copy point list
            memcpy(pointList, payload.payloadPtr, pointNum * sizeof(CTMsgPL_C_PointID));
            
            
            
            /* Compose reply list from incoming point list */
            CTMsg_R_RTGetPointData_Digital* rtDigital;
            rtDigital = new CTMsg_R_RTGetPointData_Digital(header.requestID, replyID.newID());

            //Prepare tables to get value list
            Table<CTMsgPL_C_PointID> srcPointList(pointList, pointNum);
            Table<CTMsgPL_R_RTPointData_Digital> destPointList(rtDigital->firstEntry(), rtDigital->getMaxEntries());
            lu_uint32_t numPoints;
            numPoints = digitalRTStatus.getRTPointData(srcPointList, destPointList);
            rtDigital->setEntries(numPoints);   //Set message size
            delete [] pointList;
            if(numPoints > 0)
            {
                log.info("send Digital Data %u points", numPoints);
                return rtDigital;
            }
            else
            {
                log.info("%s No points sent.", __AT__);
                delete rtDigital;
                return createAckNack(header, CTH_NACK_NOLIST);  //No points to send
            }
            break;
        }

        case CTMsgID_C_GetPointAnalogue:
        {
            if( (payload.payloadLen % sizeof(CTMsgPL_C_PointID)) != 0 )
            {
                return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
            }
            /* Calculate entries number */
            lu_uint32_t pointNum = payload.payloadLen / sizeof(CTMsgPL_C_PointID);
            CTMsgPL_C_PointID *pointList = new CTMsgPL_C_PointID[pointNum];

            // Copy point list
            memcpy(pointList, payload.payloadPtr, pointNum * sizeof(CTMsgPL_C_PointID));

            /* Compose reply list from incoming point list */
            CTMsg_R_RTGetPointData_Analog* rtAnalogue;
            rtAnalogue = new CTMsg_R_RTGetPointData_Analog(header.requestID, replyID.newID());
            //Prepare tables to get value list
            Table<CTMsgPL_C_PointID> srcPointList(pointList, pointNum);
            Table<CTMsgPL_R_RTPointData_Analog> destPointList(rtAnalogue->firstEntry(), rtAnalogue->getMaxEntries());
            lu_uint32_t numPoints;
            numPoints = analogueRTStatus.getRTPointData(srcPointList, destPointList);
            rtAnalogue->setEntries(numPoints);   //Set message size
            delete [] pointList;
            if(numPoints > 0)
            {
                log.info("send Analogue Data %u points",  numPoints);
                return rtAnalogue;
            }
            else
            {
                log.info("%s No points sent.", __AT__);
                delete rtAnalogue;
                return createAckNack(header, CTH_NACK_NOLIST);  //No points to send
            }
            break;
        }

        case CTMsgID_C_GetPointCounter:
        {
            if( (payload.payloadLen % sizeof(CTMsgPL_C_PointID)) != 0 )
            {
                return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
            }
            /* Calculate entries number */
            lu_uint32_t pointNum = payload.payloadLen / sizeof(CTMsgPL_C_PointID);
            CTMsgPL_C_PointID *pointList = new CTMsgPL_C_PointID[pointNum];

            // Copy point list
            memcpy(pointList, payload.payloadPtr, pointNum * sizeof(CTMsgPL_C_PointID));

            /* Compose reply list from incoming point list */
            CTMsg_R_RTGetPointData_Counter* rtCounter;
            rtCounter = new CTMsg_R_RTGetPointData_Counter(header.requestID, replyID.newID());
            //Prepare tables to get value list
            Table<CTMsgPL_C_PointID> srcPointList(pointList, pointNum);
            Table<CTMsgPL_R_RTPointData_Counter> destPointList(rtCounter->firstEntry(), rtCounter->getMaxEntries());
            lu_uint32_t numPoints;
            numPoints = counterRTStatus.getRTPointData(srcPointList, destPointList);
            rtCounter->setEntries(numPoints);   //Set message size
            delete [] pointList;
            if(numPoints > 0)
            {
                log.info("send Counter Data %u points", numPoints);
                return rtCounter;
            }
            else
            {
                log.info("%s No points sent.", __AT__);
                delete rtCounter;
                return createAckNack(header, CTH_NACK_NOLIST);  //No points to send
            }
            break;
        }
        default:
            /* Unsupported message type */
            return createAckNack(header, CTH_NACK_UNSUPPORTED);;

    }//endswitch(header.messageID)
    return NULL;
}


template<typename T> CTHMessage* ConfigurationToolHandler::decodeRTPointList(
                                                             T &target,
                                                             const CTMessageHeader &header,
                                                             const CTMessagePayload &payload
                                                                          )
{
    CTH_ACKNACKCODE result= CTH_ACK;

    /*Clear point list*/
    if(payload.payloadLen == 0)
    {
        log.info("%s Reset List", __AT__);
        /* clear list */
        target.setPointList(0);

        result =  CTH_ACK;
    }

    /*Select point list*/
    else if(payload.payloadLen % sizeof(CTMsgPL_C_PointID) == 0)
    {
        CTMsgPL_C_PointID *points = reinterpret_cast<CTMsgPL_C_PointID*>(payload.payloadPtr);
        /* Calculate entries number */
        lu_uint32_t pointNum = payload.payloadLen / sizeof(CTMsgPL_C_PointID);

        log.info("%s New List received (%i elements)", __AT__, pointNum);

        /* Get point list buffer */
        CTPointListStrTable &CTPointList = target.getPointList();

        /* Get a local reference of the table properties
         * (speedup access in the following loop)
         */
        lu_uint32_t entriesBuffer = CTPointList.getEntries();
        CTMsgPL_C_PointID *pointsBuffer = CTPointList.getTable();

        lu_uint32_t i;
        for(i = 0; (i < pointNum) && (i < entriesBuffer); ++i)
        {
            /* Save the new point list */
            pointsBuffer[i]  = points[i];
        }

        /* Set the new size of the point list */
        target.setPointList(i);

        /* Send ACK */
        log.info("%s Send ACK", __AT__);
        result = CTH_ACK;
    }
    else
    {
        /* Invalid payload size - Send NACK */
        log.error("%s List Invalid payload size (%i) Send NACK", __AT__, payload.payloadLen);
        result = CTH_NACK_PAYLOAD;
    }

    return createAckNack(header, result);
}


CTH_ACKNACKCODE ConfigurationToolHandler::restartRTU(const CTMessagePayload &payload,
                                                     EVENT_TYPE_SYSTEM action,
                                                     bool forced
                                                     )
{
    CTH_ACKNACKCODE result;
    bool ret = false;

    /* Check message received */
    if(payload.payloadLen != 0)
    {
        result = CTH_NACK_PAYLOAD;
    }
    else
    {
        //Log Event
        CTEventSystemStr event;
        event.eventType = action;
        event.value = EVENT_REQUEST_VALUE_CONFIGTOOL;
        eventLog->logEvent(event);

        IMCMApplication::RESTART_TYPE type = (!forced)? IMCMApplication::RESTART_TYPE_TRY :
                                                        IMCMApplication::RESTART_TYPE_FORCED;
        switch(action)
        {
            case EVENT_TYPE_SYSTEM_RESTARTREQ:
                ret = mcmApplication.restart(type);
                break;
            case EVENT_TYPE_SYSTEM_REBOOTREQ:
                ret = mcmApplication.reboot(type);
                break;
            case EVENT_TYPE_SYSTEM_SHUTDOWNREQ:
                ret = mcmApplication.shutdown(type);
                break;
            default:
                break;
        }
        result = (ret)? CTH_ACK : CTH_NACK_RESTARTREFUSAL;
    }
    return result;
}

}//end namespace CTH

/* Default implementation of Automation API in the case of automation is not supported*/
#if defined (IEC61131) || defined (IEC61499)
#define AUTOMATION_SUPPORTED 1
#else
#define AUTOMATION_SUPPORTED 0
#endif

#if !AUTOMATION_SUPPORTED
void setAutomationDebugMode(bool enabled)
{
}

lu_bool_t isAutomationDebugMode()
{
    return LU_FALSE;
}

lu_bool_t isAutomationRunning()
{
    return LU_FALSE;
}

lu_bool_t isAutomationFailed()
{
    return LU_FALSE;
}

lu_bool_t isAutomationInhibited()
{
    return LU_FALSE;
}
#endif

/*
 *********************** End of file ******************************************
 */
