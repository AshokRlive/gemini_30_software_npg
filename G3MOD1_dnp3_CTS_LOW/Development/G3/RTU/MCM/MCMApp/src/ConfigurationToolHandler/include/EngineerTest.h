/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [MCM]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Engineer Test module public interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19/09/12      pueyos_a     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#if !defined(EA_C2593053_3588_438a_905F_F311C43F509B__INCLUDED_)
#define EA_C2593053_3588_438a_905F_F311C43F509B__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "CTHandlerCommon.h"
#include "CTHMessage.h"
#include "GeminiDatabase.h"
#include "IStatusManager.h"
#include "Logger.h"

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
namespace CTH
{

/**
 * \brief This class is used to manage the Engineer maintenance test operations
 * that comes as messages from the Configuration Tool itself.
 */
class EngineerTest
{

public:
    /**
     * \brief Custom constructor
     *
     * \param statusMgr Reference to the status manager object
     * \param moduleMgr Reference to the module manager object
     * \param database Reference to the Gemini DataBase
     *
     * \return none
     */
	EngineerTest(IStatusManager &statusMgr,
                GeminiDatabase &database
                );
	virtual ~EngineerTest();

    /**
     * \brief Process Set Service Mode message.
     *
     * It checks message and sets the service mode and its reason to do so
     * when applicable.
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return Reply message
     */
    CTHMessage* decodeGetServiceModeMessage(const CTMessageHeader &header,
                                            const CTMessagePayload &payload
                                            );

    /**
     * \brief Process Set Service Mode message.
     *
     * It checks message and sets the service mode and its reason to do so
     * when applicable.
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     * \param mode Service mode to be set.
     *
     * \return Reply message
     */
    CTHMessage* decodeSetServiceModeMessage(const CTMessageHeader &header,
                                            const CTMessagePayload &payload,
                                            const SERVICEMODE mode
                                            );

    /**
     * \brief Process Get OLR status message.
     *
     * Returns Off/Local/Remote status.
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return Reply message
     */
    CTHMessage* decodeGetOLRMessage(const CTMessageHeader &header,
                                    const CTMessagePayload &payload
                                    );

    /**
     * \brief Process Set OLR status message.
     *
     * Sets the Returns Off/Local/Remote status to the given one.
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return Reply message
     */
    CTHMessage* decodeSetOLRMessage(const CTMessageHeader &header,
                                    const CTMessagePayload &payload
                                    );

    /**
     * \brief Process Operate Switch message.
     *
     * Operates the given switch to the given position.
     *
     * \param header Reference to the message header
     * \param payload Reference to the message payload
     *
     * \return Reply message
     */
    CTHMessage* decodeSwitchMessage(const CTMessageHeader &header,
                                    const CTMessagePayload &payload
                                    );

    /**
     * \brief Process Control Logic Operation request message.
     *
     * Starts a Control Logic's related Operation.
     *
     * \param payload Reference to the message payload.
     *
     * \return Error code
     */
    CTHMessage* decodeControlLogicOperationMessage(const CTMessageHeader &header,
                                                   const CTMessagePayload &payload
                                                   );

private:
    /**
     * \brief Convert a GDB error code to a CTH NACK code
     *
     * \param errorCode GDB error code to convert.
     *
     * \return CTH ACK/NACK Error code.
     */
    static CTH_ACKNACKCODE convertGDBtoCTH_NACK(const GDB_ERROR errorCode);

private:
    IStatusManager &statusManager;
    GeminiDatabase &database;
    Logger& log;
};

}//end namespace CTH

#endif // !defined(EA_C2593053_3588_438a_905F_F311C43F509B__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
