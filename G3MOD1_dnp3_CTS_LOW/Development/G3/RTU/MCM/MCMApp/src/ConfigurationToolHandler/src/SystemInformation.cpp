/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       System Information module implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11/10/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#ifdef AR_ZIP
#define AR_ZIP

#include <assert.h>
#include <linux/zlib.h>
#endif





/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "SystemInformation.h"
#include "AbstractCTHandler.h"
#include "RTUInfo.h"
#include "TimeManager.h"
#include "EventLogManager.h"
#include "timeOperations.h"
#include "CANIOModule.h"
#include "MCMIOModule.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static TimeManager& timeManager = TimeManager::getInstance();
/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
namespace CTH
{

SystemInformation::SystemInformation(IStatusManager& statusMgr,
                                     IIOModuleManager& moduleMgr,
                                     UserManager& userMgr) :
                                         statusManager(statusMgr),
                                         moduleManager(moduleMgr),
                                         userManager(userMgr),
                                         log(Logger::getLogger(SUBSYSTEM_ID_CT_HANDLER)),
                                         doorChannel(NULL)
{
    eventLog = EventLogManager::getInstance();

    //Preparation for Door channel check
    MCMIOModule* mcm = dynamic_cast<MCMIOModule*>(moduleManager.getModule(MODULE_MCM, MODULE_ID_0));
    if(mcm != NULL)
    {
        doorChannel = &(mcm->getChannelDoorSwitch());
    }
}

SystemInformation::~SystemInformation() {}


CTHMessage* SystemInformation::getModuleInfo(const CTMessageHeader &header,
                                             const CTMessagePayload &payload
                                             )
{
    if( payload.payloadLen != 0 )
    {
        return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
    }
    /* Get data */
    std::vector<IIOModule*> moduleList;
    moduleList = moduleManager.getAllModules();
    if (moduleList.empty())
    {
        log.error("%s - No modules available", __AT__);
        return createAckNack(header, CTH_NACK_NOMODULE);    //Error
    }

    /* Compose return message */
    CTMsg_R_ModuleInfo* reply = new CTMsg_R_ModuleInfo(header.requestID, replyID.newID());

    reply->setEntries(moduleList.size());   //Set the number of entries
    lu_uint32_t moduleNum = reply->getEntries();//Get the number of entries we can effectively store

    /* Modules retrieved correctly: Extract the modules information */
    for(lu_uint32_t i = 0; i < moduleNum; ++i)
    {
        IOModuleInfoStr moduleInfo;

        /* Get module info */
        moduleList[i]->getInfo(moduleInfo);

        /* Convert Module info into the Configuration tool module info */
        CTMsgPL_R_ModuleInfo* rPayload = &(*reply)[i];
        rPayload->type      = moduleInfo.mid.type;
        rPayload->id        = moduleInfo.mid.id;
        rPayload->serial    = (moduleInfo.moduleUID.supplierId << 25) +
                              moduleInfo.moduleUID.serialNumber;
        /* Module-related Version/revision values */
        rPayload->software.relType       = moduleInfo.mInfo.application.software.relType;
        rPayload->software.version.major = moduleInfo.mInfo.application.software.version.major;
        rPayload->software.version.minor = moduleInfo.mInfo.application.software.version.minor;
        rPayload->software.patch         = moduleInfo.mInfo.application.software.patch;
        rPayload->feature.major          = moduleInfo.mInfo.featureRevision.major;
        rPayload->feature.minor          = moduleInfo.mInfo.featureRevision.minor;
        rPayload->sysAPI.major           = moduleInfo.mInfo.application.systemAPI.major;
        rPayload->sysAPI.minor           = moduleInfo.mInfo.application.systemAPI.minor;
        rPayload->bootloaderAPI.major    = moduleInfo.mInfo.bootloader.systemAPI.major;
        rPayload->bootloaderAPI.minor    = moduleInfo.mInfo.bootloader.systemAPI.minor;
        rPayload->bootloaderSw.relType   = moduleInfo.mInfo.bootloader.software.relType;
        rPayload->bootloaderSw.version.major    = moduleInfo.mInfo.bootloader.software.version.major;
        rPayload->bootloaderSw.version.minor    = moduleInfo.mInfo.bootloader.software.version.minor;
        rPayload->bootloaderSw.patch     = moduleInfo.mInfo.bootloader.software.patch;
        /* status of the module */
        rPayload->moduleStatus  = moduleInfo.hBeatInfo.status;
        rPayload->moduleErrors  = moduleInfo.hBeatInfo.error;
        rPayload->ok            = moduleInfo.status.active;
        rPayload->registered    = moduleInfo.status.registered;
        rPayload->configured    = moduleInfo.status.configured;
        rPayload->present       = moduleInfo.status.present;
        rPayload->detected      = moduleInfo.status.detected;
        rPayload->disabled      = moduleInfo.status.disabled;
        rPayload->configXML     = LU_TRUE;//Deprecated
        rPayload->unused        = 0x00;
        rPayload->fsm           = moduleInfo.fsm;
    }
    return reply;
}


CTHMessage* SystemInformation::getModuleDetail(const CTMessageHeader &header,
                                               const CTMessagePayload &payload
                                               )
{
    if(payload.payloadLen != sizeof(CTMsgPL_C_ModuleID))
    {
        return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
    }

    /* Decode valid payload */
    CTMsgPL_C_ModuleID* moduleRef = reinterpret_cast<CTMsgPL_C_ModuleID*>(payload.payloadPtr);
    MODULE moduleType = (MODULE)moduleRef->moduleType;
    MODULE_ID moduleID = (MODULE_ID)moduleRef->moduleID;

    /* Get data */
    IIOModule *module = moduleManager.getModule(moduleType, moduleID);  //Retrieve module
    if(module == NULL)
    {
        // Module not found!
        log.error("%s Error getting details of module %i:%i.", __AT__, moduleType, moduleID+1);
        return createAckNack(header, CTH_NACK_NOMODULE);
    }
    IOModuleDetailStr info;
    IOM_ERROR res = module->getDetails(info);
    if(res != IOM_ERROR_NONE)
    {
        log.error("%s Error getting details of module %i:%i. Error %i: %s",
                    __AT__, moduleType, moduleID+1, res, IOM_ERROR_ToSTRING(res));
        return createAckNack(header, CTH_NACK_GENERIC);
    }

    /* Compose return message */
    CTMsg_R_ModuleDetail* reply = new CTMsg_R_ModuleDetail(header.requestID, replyID.newID());
    CTMsgPL_R_ModuleDetail* rPayload = reinterpret_cast<CTMsgPL_R_ModuleDetail*>(reply->firstEntry());

    //Extract the module information
    rPayload->architecture = info.architecture;
    rPayload->svnRevisionBoot = info.svnRevisionBoot;
    rPayload->svnRevisionApp = info.svnRevisionApp;
    rPayload->uptime = info.uptime;
    reply->setEntries(1);
    log.info("%s replies with %s", __AT__, rPayload->toString().c_str());
    return reply;
}


CTHMessage* SystemInformation::getModuleCANStats(const CTMessageHeader &header,
                                                 const CTMessagePayload &payload
                                                 )
{
    if(payload.payloadLen != sizeof(CTMsgPL_C_ModuleID))
    {
        return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
    }

    /* Decode valid payload */
    CTMsgPL_C_ModuleID* moduleRef = reinterpret_cast<CTMsgPL_C_ModuleID*>(payload.payloadPtr);
    MODULE moduleType = (MODULE)moduleRef->moduleType;
    MODULE_ID moduleID = (MODULE_ID)moduleRef->moduleID;

    /* Get data */
    IIOModule *module = moduleManager.getModule(moduleType, moduleID);  //Retrieve module
    if(module == NULL)
    {
        // Module not found!
        log.error("%s Error getting module %i:%i for CAN stats.",
                        __AT__, moduleType, moduleID+1);
        return createAckNack(header, CTH_NACK_NOMODULE);
    }

    IOM_ERROR res = IOM_ERROR_PARAM;
    typedef std::vector<MCMIOModule::StatsCANStr> CANStatsList;
    CANStatsList infoList;

    if(module->isMCM() == LU_TRUE)
    {
        MCMIOModule *mcmModule = dynamic_cast<MCMIOModule*>(module);
        if(mcmModule != NULL)
        {
            /* MCM Module does report CAN stats to Config Tool having 2 CAN ports */
            res = mcmModule->getCANStats(infoList);
        }
    }
    else
    {
        CANIOModule *canModule = dynamic_cast<CANIOModule*>(module);
        if(canModule != NULL)
        {
            CanStatsStr info;
            res = canModule->getCANStats(info);
            if(res == IOM_ERROR_NONE)
            {
                MCMIOModule::StatsCANStr entry;
                entry = info;
                entry.validateAll();   //Sets all entries coming from CAN module as valid ones
                infoList.push_back(entry);   //Compose 1-element vector for reporting
            }
        }
    }

    if(res != IOM_ERROR_NONE)
    {
        log.error("%s Error getting CAN stats of module %s [%i:%i]. Error %i: %s",
                    __AT__, module->getName(), moduleType, moduleID+1, res, IOM_ERROR_ToSTRING(res));
        return createAckNack(header, CTH_NACK_NOMODULE);
    }

    /* Compose return message */
    CTMsg_R_ModuleCANStats* reply = new CTMsg_R_ModuleCANStats(header.requestID, replyID.newID());
    CTMsgPL_R_ModuleCANStats* rPayload = reinterpret_cast<CTMsgPL_R_ModuleCANStats*>(reply->firstEntry());

    for(CANStatsList::iterator it = infoList.begin(), end = infoList.end(); it != end; ++it)
    {
        //Extract the module information
        rPayload->canArbitrationError = (*it).canArbitrationError;
        rPayload->validCANArbitrationError = (*it).validCANArbitrationError;
        rPayload->canBusError = (*it).canBusError;
        rPayload->validCANBusError = (*it).validCANBusError;
        rPayload->canBusOff = (*it).canBusOff;
        rPayload->validCANBusOff = (*it).validCANBusOff;
        rPayload->canDataOverrun = (*it).canDataOverrun;
        rPayload->validCANDataOverrun = (*it).validCANDataOverrun;
        rPayload->canError = (*it).canError;
        rPayload->validCANError = (*it).validCANError;
        rPayload->canErrorPassive = (*it).canErrorPassive;
        rPayload->validCANErrorPassive = (*it).validCANErrorPassive;
        rPayload->canTxCount = (*it).canTxCount;
        rPayload->validCANTxCount = (*it).validCANTxCount;
        rPayload->canRxCount = (*it).canRxCount;
        rPayload->validCANRxCount = (*it).validCANRxCount;
        rPayload->canRxLost = (*it).canRxLost;
        rPayload->validCANRxLost = (*it).validCANRxLost;
        log.info("%s replies with %s", __AT__, rPayload->toString().c_str());
        rPayload++;
    }
    reply->setEntries(infoList.size());
    return reply;
}


CTHMessage* SystemInformation::getModuleAlarm(const CTMessageHeader &header,
                                              const CTMessagePayload &payload
                                              )
{
    if(payload.payloadLen != sizeof(CTMsgPL_C_ModuleID))
    {
        return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
    }

    /* Decode valid payload */
    CTMsgPL_C_ModuleID* mAlarms = reinterpret_cast<CTMsgPL_C_ModuleID*>(payload.payloadPtr);
    MODULE moduleType = (MODULE)mAlarms->moduleType;
    MODULE_ID moduleID = (MODULE_ID)mAlarms->moduleID;

    /* Get data */
    IIOModule *module = moduleManager.getModule(moduleType, moduleID);  //Retrieve module
    if(module == NULL)
    {
        // Module not found!
        log.error("%s Error getting module %i:%i.", __AT__, moduleType, moduleID+1);
        return createAckNack(header, CTH_NACK_NOMODULE);
    }

    lu_uint32_t alarmNum;
    ModuleAlarmLister alarms = module->getAlarms();
    alarmNum = alarms.size();
    /* Compose return message */
    CTMsg_R_ModuleAlarms* reply = new CTMsg_R_ModuleAlarms( header.requestID, replyID.newID());
    reply->setEntries(alarmNum);     // Set the number of entries
    if(alarmNum == 0)
    {
        log.info("%s No alarm info found in %s module.", __AT__, module->getName() );
        //When no alarms available for this module, it returns an empty list
    }
    else
    {
        alarmNum = reply->getEntries(); // Get back the number of entries we can effectively store
        lu_uint32_t alarmTotal = 0;     //Final number of alarms send
        for(lu_uint32_t i = 0; i < alarmNum; ++i)
        {
            if(alarms[i].isValid() == LU_TRUE)
            {
                CTMsgPL_R_ModuleAlarms *rPayload = &(*reply)[alarmTotal++];
                rPayload->subSystem     = static_cast<lu_uint8_t>(alarms[i].subSystem);
                rPayload->alarmCode     = alarms[i].alarmCode;
                rPayload->state         = alarms[i].getState();
                rPayload->severity      = alarms[i].severity;
                rPayload->parameter     = alarms[i].parameter;
            }
        }
        reply->setEntries(alarmTotal); //Re-adjust entries after discarding invalid entries
    }
    return reply;
}


CTHMessage* SystemInformation::ackModuleAlarm(const CTMessageHeader &header,
                                              const CTMessagePayload &payload
                                              )
{
    if(payload.payloadLen != sizeof(CTMsgPL_C_ModuleID))
    {
        return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
    }

    /* Decode valid payload */
    CTMsgPL_C_ModuleID* moduleRef = reinterpret_cast<CTMsgPL_C_ModuleID*>(payload.payloadPtr);
    MODULE moduleType = (MODULE)moduleRef->moduleType;
    MODULE_ID moduleID = (MODULE_ID)moduleRef->moduleID;

    /* Get data */
    IIOModule *module = moduleManager.getModule(moduleType, moduleID);  //Retrieve module
    if(module == NULL)
    {
        // Module not found!
        log.error("%s Error getting module %i:%i.", __AT__, moduleType, moduleID+1);
        return createAckNack(header, CTH_NACK_NOMODULE);
    }
    module->ackAlarms();
    log.info("%s %s module alarms acknowledged.", __AT__, module->getName());
    return createAckNack(header, CTH_ACK);
}


CTHMessage* SystemInformation::decodeGetDateMessage(const CTMessageHeader &header,
                                                    const CTMessagePayload &payload
                                                    )
{
    if( payload.payloadLen != 0 )
    {
        return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
    }

    /* Get data */
    struct tm datetime; //Time set
    SYNC_SOURCE source; //Source of last synchronisation
    lu_bool_t synch;    //Time synchronisation

    /* TODO: pueyos_a - match source of last synch from TimeManager with enum in G3ConfigProtocol.xml */
    //Get the time in UTC to reply to the Config Tool
    timeManager.getTime(&datetime, &synch, &source, true);

    /* compose reply message */
    CTMsg_R_DateTime* reply = new CTMsg_R_DateTime(header.requestID, replyID.newID());
    CTMsgPL_R_DateTime* rPayload = reinterpret_cast<CTMsgPL_R_DateTime*>(reply->firstEntry());
    rPayload->tm_sec =   datetime.tm_sec;
    rPayload->tm_min =   datetime.tm_min;
    rPayload->tm_hour =  datetime.tm_hour;
    rPayload->tm_mday =  datetime.tm_mday;
    rPayload->tm_mon =   datetime.tm_mon;
    rPayload->tm_year =  datetime.tm_year;
    rPayload->tm_wday =  datetime.tm_wday;
    rPayload->tm_yday =  datetime.tm_yday;
    /* TODO: pueyos_a - substitute magic numbers by DATE_DST enum */
    rPayload->isDST = (datetime.tm_isdst < 0)? 0 : (datetime.tm_isdst == 0)? 1 : 2;
    rPayload->sync = synch;
    rPayload->unused = 0;
    rPayload->syncSource = source;
    reply->setEntries(1);      /* Set the number of valid entries */
    if(log.isDebugEnabled() == LU_TRUE)
    {
        log.debug("%s Get date %s.", __AT__, rPayload->toString().c_str());
    }
    return reply;
}


CTHMessage* SystemInformation::decodeSetDateMessage(const CTMessageHeader &header,
                                                    const CTMessagePayload &payload
                                                    )
{

    if( payload.payloadLen != sizeof(CTMsgPL_C_DateTime) )
    {
        return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
    }

    /* Decode valid payload */
    CTMsgPL_C_DateTime* datetimePtr = reinterpret_cast<CTMsgPL_C_DateTime*>(payload.payloadPtr);

    /* Get data */
    struct tm datetime; //Time set
    datetime.tm_sec = datetimePtr->tm_sec;
    datetime.tm_min = datetimePtr->tm_min;
    datetime.tm_hour = datetimePtr->tm_hour;
    datetime.tm_mday = datetimePtr->tm_mday;
    datetime.tm_mon = datetimePtr->tm_mon;
    datetime.tm_year = datetimePtr->tm_year;
    datetime.tm_wday = datetimePtr->tm_wday;
    datetime.tm_yday = datetimePtr->tm_yday;
    /* TODO: pueyos_a - substitute magic numbers by DATE_DST enum */
    datetime.tm_isdst = (datetimePtr->isDST == 0)? -1 : (datetimePtr->isDST == 1)? 0 : 1;
    datetimePtr->sync = (datetimePtr->sync)? LU_TRUE : LU_FALSE;

    /* Update RTU time */
    TimeManager::TERROR res;
    res = timeManager.setTime(datetime,
                            0,          //No milliseconds used
                            datetimePtr->sync,
                            SYNC_SOURCE_CONFIGTOOL,
                            true        //Time from Config Tool always comes as UTC
                            );
    switch (res)
    {
        case TimeManager::TERROR_NONE:
            if(log.isDebugEnabled() == LU_TRUE)
            {
                log.debug("%s Set date to %s", __AT__, datetimePtr->toString().c_str()); //date set
            }
            break;
        case TimeManager::TERROR_SYNC:
            if(log.isDebugEnabled() == LU_TRUE)
            {
                log.debug("%s Tried (but unable) to set date to %s", __AT__, datetimePtr->toString().c_str()); //date set
            }
            break;
        case TimeManager::TERROR_TIMER:
            log.fatal("%s System timer unavailable", __AT__);
            break;
        default:
            log.error("%s Error setting time", __AT__);
            break;
    }
    if(res != TimeManager::TERROR_NONE)
    {
        return createAckNack(header, CTH_NACK_GENERIC); //Date not accepted
    }

    /* Get result (for event log and app log): */
    struct timespec newtime;
    lu_bool_t syn;
    struct tm newtm;
    timeManager.getTime(&newtime, &syn);
    TimeManager::TimeToStructtm(newtime, &newtm);

    //Report Event
    CTEventDatetimeStr event;
    event.timestamp.sec = newtime.tv_sec;
    event.timestamp.msec = NSEC_TO_MSEC(newtime.tv_nsec);
    event.isdst = newtm.tm_isdst;
    event.sync = syn;
    eventLog->logEvent(event);

    log.info("%s Set date to %s", __AT__, datetimePtr->toString().c_str());
    return createAckNack(header, CTH_ACK);
}


CTHMessage* SystemInformation::decodeGetSysAPI(const CTMessageHeader &header,
                                               const CTMessagePayload &payload
                                               )
{
    if( payload.payloadLen != 0 )
    {
        return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
    }

    /* Compose return message */
    CTMsg_R_SystemAPI* reply = new CTMsg_R_SystemAPI(header.requestID, replyID.newID());
    CTMsgPL_R_SystemAPI *sysAPIPtr = reinterpret_cast<CTMsgPL_R_SystemAPI*>(reply->firstEntry());
    sysAPIPtr->MCMModule.systemAPI.major = 0;
    sysAPIPtr->MCMModule.systemAPI.minor = 0;

    /* get MCM Module subsystem API */
    IIOModule *moduleMCMPtr;
    moduleMCMPtr = moduleManager.getModule(MODULE_MCM, MODULE_ID_0);
    if (moduleMCMPtr == NULL)
    {
        log.error("%s error retrieving MCM module version.", __AT__);
    }
    else
    {
        ModuleVersionStr sysAPI;
        moduleMCMPtr->getSystemAPI(sysAPI);
        sysAPIPtr->MCMModule.systemAPI.major = sysAPI.major;
        sysAPIPtr->MCMModule.systemAPI.minor = sysAPI.minor;
    }
    /* get Configuration Tool protocol subsystem API */
    sysAPIPtr->configG3.systemAPI.major = G3_CONFIG_SYSTEM_API_MAJOR;
    sysAPIPtr->configG3.systemAPI.minor = G3_CONFIG_SYSTEM_API_MINOR;

    /* get Schema version */
    sysAPIPtr->schema.major = SCHEMA_MAJOR;
    sysAPIPtr->schema.minor = SCHEMA_MINOR;

    if(log.isDebugEnabled() == LU_TRUE)
    {
        log.debug("%s Get API versions: %s", __AT__, sysAPIPtr->toString().c_str());
    }
    reply->setEntries(1);
    return reply;
}


CTHMessage* SystemInformation::decodeGetRTUInfo(const CTMessageHeader &header,
                                                const CTMessagePayload &payload
                                                )
{
    if( payload.payloadLen != 0 )
    {
        return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
    }

    /* Compose return message */
    CTMsg_R_RTUInfo* reply = new CTMsg_R_RTUInfo(header.requestID, replyID.newID());
    CTMsgPL_R_RTUInfo *rPayload = reinterpret_cast<CTMsgPL_R_RTUInfo*>(reply->firstEntry());

    /* Get RootFS version values */
    std::string rootFS = getRootFSVersion();
    strncpy(rPayload->revisionRootFS, getRootFSVersion().c_str(), rPayload->rootFSlength);

    /* CPU serial num */
    rPayload->serialCPU = getCPUSerialNum();

    /* TODO: AP - cause of last reset */
//    if(system("last -x") != 0)
//    {
        rPayload->lastReset = CTH_LASTRESET_UNKNOWN;
//    }
//    else
//    {
//        rPayload->lastReset = CTH_LASTRESET_;
//    }

    /* get MCM firmware string */
    memset(rPayload->revisionMCM, 0, (rPayload->revMCMlength));
    strncpy(rPayload->revisionMCM, getMCMAppVersionString().c_str(), rPayload->revMCMlength);

    /* Get Kernel version string */
    memset(rPayload->revisionKernel, 0, (rPayload->revKernellength));
    strncpy(rPayload->revisionKernel, getOSVersion().c_str(), rPayload->revKernellength);

    /* Get RTU site name */
    std::string siteName = statusManager.getSiteNameRTU();

    /* Set message size */
    reply->setEntries(1);

    /* Append sitename string at the end of the message */
    reply->appendBytes((lu_uint8_t*)siteName.c_str(),
                    (siteName.length()+1)*sizeof(char)/sizeof(lu_uint8_t));

    if(log.isDebugEnabled() == LU_TRUE)
    {
        log.debug("%s Get RTU Info: %s", __AT__, rPayload->toString().c_str());
    }
    return reply;
}


CTHMessage* SystemInformation::decodeGetConfigInfo(const CTMessageHeader &header,
                                                   const CTMessagePayload &payload
                                                   )
{
    if( payload.payloadLen != 0 )
    {
        return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
    }

    /* Compose return message */
    CTMsg_R_ConfigInfo* reply = new CTMsg_R_ConfigInfo(header.requestID, replyID.newID());
    CTMsgPL_R_ConfigInfo* rPayload = reinterpret_cast<CTMsgPL_R_ConfigInfo*>(reply->firstEntry());;
    rPayload->cfgFilePresent = statusManager.getConfigFilePresence();
    /* Get Configuration File Timestamp string */
    strncpy(rPayload->cfgFileTimestamp,
            statusManager.getConfigFileTimestamp().c_str(),
            rPayload->cfgTstamplength
            );
    strncpy(rPayload->cfgFileDesc,
                statusManager.getConfigFileDesc().c_str(),
                rPayload->cfgTdesclength
                );
    /* Get Configuration File version field */

    statusManager.getConfigFileVersion(&(rPayload->cfgFileRev));
    /* Get file CRC */
    lu_uint32_t crc;
    rPayload->cfgFileCRCValid = statusManager.getConfigFileCRC(crc);
    rPayload->cfgFileCRC = crc;
    rPayload->unused = 0;

    /* Set message size */
    reply->setEntries(1);

    if(log.isDebugEnabled() == LU_TRUE)
    {
        log.debug("%s Get Config Info: %s", __AT__, rPayload->toString().c_str());
    }
    return reply;
}


CTHMessage* SystemInformation::decodeGetNetInfo(const CTMessageHeader &header,
                                                const CTMessagePayload &payload
                                                )
{
    if( payload.payloadLen != 0 )
    {
        return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
    }

    /* Get IP configuration */
    netConfigVector netCfg = getNetConfig();
    if(netCfg.size() <= 0)
    {
        return createAckNack(header, CTH_NACK_GENERIC);    //no list recovered
    }
    else
    {
        /* Compose return message */
        CTMsg_R_NetInfo* reply = new CTMsg_R_NetInfo(header.requestID, replyID.newID());
        CTMsgPL_R_NetInfo* rPayload;
        for (lu_uint32_t i = 0; i < netCfg.size(); ++i)
        {
            rPayload = reinterpret_cast<CTMsgPL_R_NetInfo*>(&(*reply)[i]);
            rPayload->confIP.ip = netCfg[i].ip;
            rPayload->confIP.mask = netCfg[i].mask;
            rPayload->confIP.gateway = netCfg[i].gateway;
            rPayload->confIP.broadcast = netCfg[i].broadcast;
            /* TODO: pueyos_a - CTH: DNS 1 and 2?? */
            rPayload->confIP.dns1 = netCfg[i].dns1;
            rPayload->confIP.dns2 = netCfg[i].dns2;
            strncpy(rPayload->devName, netCfg[i].devName.c_str(), CTMsgPL_R_NetInfo::devNamelength);
            for(lu_uint32_t j = 0; j < 6; ++j)
            {
                rPayload->mac[j] = netCfg[i].mac[j];
            }
            /* TODO: pueyos_a - CTH: net DHCP?? */
            rPayload->dhcp = 0;
            rPayload->unused = 0;
            rPayload->enable = 1; //TODO  get enable status from config or OS ?

#ifdef AP_ETH_ID
            //compose devID
            size_t pos = netCfg[i].devName.find("eth");
            if(pos==std::string::npos)
            {
                //not found
                message[idx].devID = ETHERNET_DEVICE_UNKNOWN;
            }
            else
            {
                //get number from name
                message[idx].devID = strtol(netCfg[i].devName.substr(pos).c_str());
            }
#endif

            //compose debug message
            if(log.isDebugEnabled() == LU_TRUE)
            {
                std::ostringstream ssDebugMsg;
                ssDebugMsg << "\tDev: " <<
                            netCfg[i].devName.c_str()                       <<
                            std::endl <<
                            "\t\tIP: " <<
                            netCfg[i].toString(netCfg[i].ip).c_str()        <<
                            std::endl <<
                            "\t\tMask: " <<
                            netCfg[i].toString(netCfg[i].mask).c_str()      <<
                            std::endl <<
                            "\t\tGateway: " <<
                            netCfg[i].toString(netCfg[i].gateway).c_str()   <<
                            std::endl <<
                            "\t\tBroadcast: " <<
                            netCfg[i].toString(netCfg[i].broadcast).c_str() <<
                            std::endl <<
                            "\t\tMAC: " <<
                            netCfg[i].toString(netCfg[i].mac).c_str()       <<
                            std::endl <<
                            "\t\tDHCP: " << ((rPayload->dhcp == 0)? "Yes" : "No") <<
                            std::endl;
                log.debug("%s Get info on net entry %i: %s",
                            __AT__, i, ssDebugMsg.str().c_str());
            }
        }
        reply->setEntries(netCfg.size());    //Set message size in bytes
        return reply;
    }
}


CTHMessage* SystemInformation::decodeGetBatteryInfo(const CTMessageHeader &header,
                                                    const CTMessagePayload &payload
                                                    )
{
    if( payload.payloadLen != 0 )
    {
        return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
    }

    /* Compose return message */
    CTMsg_R_BatteryCfg* reply = new CTMsg_R_BatteryCfg(header.requestID, replyID.newID());

    /* Get data */
    CTMsgPL_R_BatteryCfg* battCfg = reinterpret_cast<CTMsgPL_R_BatteryCfg*>(reply->firstEntry());

    /*TODO: pueyos_a - Get battery information */
#ifdef AP_GETBATT
    result = PSM.read.battInfo(battCfg->memChunk);
    if(result!=OK)
    {
        delete reply;
        return createAckNack(header, CTH_NACK_NOCONFIG);
    }
    battCfg->type = battCfg->memChunk.type;
#else
    battCfg->type = 0;
#endif

    reply->setEntries(1);
    log.debug("%s Get battery info: Type=%i", __AT__, battCfg->type);
    return reply;
}


CTHMessage* SystemInformation::decodeCheckAlive(const CTMessageHeader &header,
                                                    const CTMessagePayload &payload
                                                    )
{
    if( payload.payloadLen != 0 )
    {
        return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
    }

#ifdef AP_SESSION
    /* check input message */
    CTMsgPL_C_SessionIDStr *sessionIDPtr = reinterpret_cast<CTMsgPL_C_SessionIDStr *>(payload.dataPtr);
    ret = (statusManager.sessionCheck(sessionIDPtr->sessionID)==LU_TRUE)? CTH_ACK : CTH_NACK_INVALIDSESSION;
    if(ret == OK) && (first connection to session)
    {
        //Report Event
        CTEventCommStr event;
        event.device = EVENTCOMM_DEVICE_CONFIGTOOL;
        event.action = EVENTCOMM_ACTION_CONNECT;
        eventLog->logEvent(event);
    }
#endif

    /* Compose return message */
    CTMsg_R_AliveInfo* reply = new CTMsg_R_AliveInfo(header.requestID, replyID.newID());
    CTMsgPL_R_AliveInfo* rPayload;
    rPayload = reinterpret_cast<CTMsgPL_R_AliveInfo*>(reply->firstEntry());
    rPayload->runningApp = CTH_RUNNINGAPP_MCMAPP;
    /* Get system and application uptime */
    rPayload->sysUptime = getSystemUptime();
    rPayload->appUptime = (timeManager.getAppRunningTime_ms() / 1000);
    lu_bool_t doorOpen = LU_TRUE;
    if(doorChannel != NULL)
    {
        IODataUint8 data;
        IChannel::ValueStr value(data);
        if(doorChannel->read(value) == IOM_ERROR_NONE)
        {
            doorOpen = data;
        }
    }
    rPayload->doorOpen = doorOpen;

    reply->setEntries(1);      /* Set the number of valid entries */
    if(log.isDebugEnabled() == LU_TRUE)
    {
        log.debug("%s replies Alive with %s.", __AT__, rPayload->toString().c_str());
    }
    return reply;

}


CTHMessage* SystemInformation::decodeUserLoginMessage(const CTMessageHeader &header,
                                                      const CTMessagePayload &payload
                                                      )
{
    if( payload.payloadLen <= sizeof(CTMsgPL_C_UserLogin) )
    {
        return createAckNack(header, CTH_NACK_PAYLOAD); //invalid payload
    }

    /* Decode valid payload */
    CTMsgPL_C_UserLogin *userLoginPtr = reinterpret_cast<CTMsgPL_C_UserLogin *>(payload.payloadPtr);
    lu_char_t *userNamePtr = reinterpret_cast<lu_char_t *>(payload.payloadPtr + sizeof(CTMsgPL_C_UserLogin));

    /* Check user */
    std::string name(userNamePtr);
    USER_LEVEL accessLevel;
    std::stringstream password;
    for (lu_uint32_t i = 0; i < User::HASH_BUF_SIZE && i < userLoginPtr->passwLen; ++i)
    {
        password << to_hex_string_value<lu_uint8_t>(userLoginPtr->passwHash[i]);
    }

    switch( userManager.userLogin(name, password.str(), &accessLevel) )
    {
        case UserManager::USERLIST_ERROR_NONE:
            //continue composing reply
            break;
        case UserManager::USERLIST_ERROR_NOTFOUND:
            return createAckNack(header, CTH_NACK_INVALIDUSER);
            break;
        case UserManager::USERLIST_ERROR_NOLOGIN:
            return createAckNack(header, CTH_NACK_INVALIDPASSWORD);
            break;
        case UserManager::USERLIST_ERROR_TOO_MANY_ATTEMPTS:
			return createAckNack(header, CTH_NACK_TOO_MANY_LOGIN_ATTEMPTS);
			break;
        default:
            return createAckNack(header, CTH_NACK_GENERIC);
            break;
    }

    /* Compose return message */
    CTMsg_R_UserInfo* reply = new CTMsg_R_UserInfo(header.requestID, replyID.newID());
    CTMsgPL_R_UserInfo* rPayload = reinterpret_cast<CTMsgPL_R_UserInfo*>(reply->firstEntry());
    rPayload->userLevel = (lu_uint8_t)accessLevel;
    /* TODO: pueyos_a - Set session ID here or on userlogin reply message??? */
    rPayload->sessionID = 0;
    /* TODO: pueyos_a - Session handling when user logs in */
    //sessionAdd(userNamePtr, ip, mac); //TODO: AP - do session-add here or inside userlogin???
    reply->setEntries(1);      /* Set the number of valid entries */
    return reply;
}

CTHMessage* SystemInformation::decodeGetMDMessage(const CTMessageHeader &header,
                                                      const CTMessagePayload &payload
                                                      )
{

    /* Decode valid payload */
    std::string userName( reinterpret_cast<lu_char_t const*>(payload.payloadPtr), payload.payloadLen ) ;

    std::string md = userManager.getMDAlgorithm(userName);

    /* Compose return message */
    CTMsg_R_MDAlgorithm* reply = new CTMsg_R_MDAlgorithm(header.requestID, replyID.newID());
    CTMsgPL_R_MDAlgorithm* rPayload = reinterpret_cast<CTMsgPL_R_MDAlgorithm*>(reply->firstEntry());
    memset(rPayload->mdAlgorithm, 0, (rPayload->ALGORITHM_LENGTH));
    strncpy(rPayload->mdAlgorithm, md.c_str(), rPayload->ALGORITHM_LENGTH);
    reply->setEntries(1);      /* Set the number of valid entries */
    return reply;
}



/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


}//end namespace CTH

/*
 *********************** End of file ******************************************
 */
