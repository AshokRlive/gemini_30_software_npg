/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MCM Board main
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   08/12/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "IMCMApplication.h"
#include "RTUInfo.h"
#include "TimeManager.h"
#include "MemoryManager.h"
#include "Logger.h"
#include "EventLogManager.h"
#include "GeminiDatabase.h"
#include "ModuleManager.h"
#include "CommsDeviceManager.h"
#include "ConfigurationManager.h"
#include "IStatusManager.h"
#include "Console.h"
#include "PortManager.h"
#include "SCADAProtocolManager.h"
#include "ConfigurationToolHandler.h"
#if CT_ZMQ_SUPPORT
#include "ZMQLinkLayer.h"
#else
#include "HttpCTHLinkLayer.h"
#endif
#include "DataBaseLogging.h"
#include "MCMOLRButton.h"
#include "MCMDoorSwitch.h"
#include "MCMLED.h"
#include "MonitorProtocolManager.h"
#include "timeOperations.h"
#include "LoggerStatus.h"
#include "AlarmStatus.h"
#include "Utilities.h"      //for CRC checking
#include "ModuleAlarmFactory.h"
#include "ModuleAlarmChannel.h" //for Alarm Channel configuration
#include "SysAlarm/SysAlarmSystemEnum.h"    //for alarm reporting
#include "SysAlarm/SysAlarmMCMEnum.h"    //for alarm reporting
#include "SysAlarm/SysAlarmNvramEnum.h"    //for alarm reporting
#include "ISupportPowerSaving.h"
#include "Debug.h"
#include "ProtocolStackDiag.h"
#include "SwitchAuthority.h"

/* TMW includes*/
extern "C"
{
#include "tmwscl/utils/tmwtimer.h"
}


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/* Command line arguments */
#define CMD_LINE_HELP           "--help"
#define CMD_LINE_HELP_SHORT     "-h"
#define CMD_LINE_VERSION        "--version"
#define CMD_LINE_VERSION_SHORT  "-v"
#define CMD_LINE_NO_CRC         "--noCRC"
#define CMD_LINE_NO_CRC_SHORT   "--nocrc"
#define CMD_LINE_CONSOLE        "--console"
#define CMD_LINE_CONSOLE_SHORT  "-c"

/* Configuration file name */
#define DEFAULT_CONFIG_GZ "G3Config.xml.gz"

#define CGI_SOCKET_NAME "/var/lib/lighttpd/sockets/g3.comm.socket"

#define CMD_LDCONFIG  "/sbin/lucy_ldconfig"

/* Restart procedure time notification */
/* These times are the security timeout after restart decision for MCM Monitor.
 * When the time is out and the application didn't exit, the MCM Monitor will
 * force the MCM application to exit.
 */
#define MCM_RESTART_TIMEOUT_MS  5000
#define MCM_REBOOT_TIMEOUT_MS   5000
#define MCM_SHUTDOWN_TIMEOUT_MS 5000

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/**
 * \brief Check presence of a command line option.
 *
 * \param buffer Char buffer that contains the option from the command line.
 * \param command Valid command Cstring to be compared to.
 *
 * \return LU_TRUE when the option matches the given command.
 */
static lu_bool_t checkCommand(lu_char_t *buffer, const lu_char_t *command);

/**
 * \brief Show in the console the command-line options available.
 */
static void printCommandLineHelp();

static std::string getCGISocketPath()
{
	std::string cgiSockPath(CGI_SOCKET_NAME);

	#ifdef TARGET_BOARD_G3_SYSTEMPROC
	    {
	    	lu_char_t* lighttpdRoot;
	    	lighttpdRoot = getenv("LIGHTTPD_ROOT");
	    	if(lighttpdRoot == 0)
	    	{
	    		printf("Environment variable: LIGHTTPD_ROOT not found!\n");
	    		exit(-1);
	    	}
	    	cgiSockPath.insert(0, lighttpdRoot) ;
	    }
	#endif

	return cgiSockPath;
}
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static Logger& log = Logger::getLogger(SUBSYSTEM_ID_MAIN, "MCMApp"); //Set Logger identity for all loggers in this executable


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

/*!
 *   \brief Class for starting and controlling all MCMApplication processes
 */
class MCMApplication : public IMCMApplication
{
public:
    MCMApplication();
    ~MCMApplication();

    APP_EXIT_CODE start(lu_bool_t noCRC, lu_bool_t internalConsole);
    virtual bool reboot(RESTART_TYPE type);
    virtual bool restart(RESTART_TYPE type, APP_EXIT_CODE exitcode = APP_EXIT_CODE_RESTART);
    virtual bool shutdown(RESTART_TYPE type);
    virtual lu_int32_t setSCADAService(lu_bool_t enable);
    virtual void endOfInitialisation();

//    virtual MCM_ERROR factoryReset();

protected:
    /**
     * This timer waits to initialise all the SCADA protocol stacks after a
     * configurable time in order to allow the RTU to initialise everything
     * before the SCADA is reported.
     */
    class SCADAStackDelay : public TimedJob
    {

    public:
        /**
         * \brief Custom constructor
         *
         * \param app Reference to the MCMapplication
         * \param time_ms timer value in ms
         */
        SCADAStackDelay(MCMApplication& app, lu_uint32_t time_ms):
                                                               TimedJob(time_ms, 0),
                                                               m_appMCM(app)
        {};
        virtual ~SCADAStackDelay() {};
    protected:
        /**
         * \brief User define operation  This method is called by the run public method
         * only if the configured timer is expired
         *
         * \param timePtr Current absolute time
         */
        virtual void job(TimeManager::TimeStr *timePtr)
        {
            LU_UNUSED(timePtr);

            PRINTCPUTIME(".Starting SCADA");
            lu_int32_t resSCADA = m_appMCM.setSCADAService(LU_TRUE);
            if(resSCADA != SCADAP_ERROR_NONE)
            {
                log.error("Error %i: SCADA protocol stack cannot be started.", resSCADA);
                m_appMCM.issueAlarm(SYS_ALARM_SUBSYSTEM_SYSTEM, SYSALC_SYSTEM_SYSTEMINIT, SYS_ALARM_SEVERITY_ERROR);
            }
            m_appMCM.endOfInitialisation(); //Signal EOI now
        }
    private:
        MCMApplication& m_appMCM;
    };

private:
    lu_int32_t stop();

    /**
     * \brief Starts the HMI manager
     *
     * \param geminiDatabase Reference to the G3 Database
     * \param configurationManager Reference to the Configuration Manager
     * \param moduleManager Reference to the Module Manager
     * \param hmiManager Reference to the HMI Manager
     *
     * \return value 0 when successful, negative value when failed.
     */
    lu_int32_t startHMIManager(GeminiDatabase &database,
                                ConfigurationManager &configurationManager,
                                ModuleManager &moduleManager,
                                HMIManager** hmiManager
                                );

    /**
     * \brief Adds an alarm entry to the internal alarm list.
     *
     * This function is intended to simplify the addition of a module alarm. The new
     * alarm will be added as active.
     *
     * \param subSystem Alarm subsystem ID.
     * \param alarmCode Alarm code ID.
     * \param severity Severity of the alarm to add.
     * \param param Alarm parameter value (if any).
     */
    void issueAlarm(SYS_ALARM_SUBSYSTEM subSystem,
                    lu_uint32_t         alarmCode
                    );
    void issueAlarm(SYS_ALARM_SUBSYSTEM subSystem,
                    lu_uint32_t         alarmCode,
                    SYS_ALARM_SEVERITY  severity,
                    lu_uint16_t         parameter = 0
                    );

    /**
     * \brief Effectively adds an alarm entry to the internal alarm list.
     *
     * Adds an alarm entry, or updates an existent one. This function is
     * intended to be used when having a complete alarm composed.
     *
     * \param alarm Module Alarm to be added.
     */
    void addAlarm(ModuleAlarm& alarm);

    /**
     * \brief Locks resources in order to restart/reboot/shutdown properly.
     *
     * It locks resources, such as the switch authority, and depending on the
     * circumstances proceeds to restart the application, or refuses it.
     *
     * \param type Type of locking (try/forced/delayed)
     * \param exitCode Operation to perform and exit code to give to the Monitor
     * \timeout_ms Security timeout after restart decision for MCM Monitor
     *
     * \return Result of the operation: true if accepted.
     */
    bool lockRestart(RESTART_TYPE type = RESTART_TYPE_TRY,
                     APP_EXIT_CODE exitCode = APP_EXIT_CODE_RESTART,
                     lu_uint32_t timeout_ms = MCM_RESTART_TIMEOUT_MS);

    /**
     * \brief Does final tasks before closing the application.
     *
     * \param exitCode Operation to perform and exit code to give to the Monitor
     * \timeout_ms Security timeout after restart decision for MCM Monitor
     */
    void restartProcedure(APP_EXIT_CODE exitCode, lu_uint32_t timeout_ms = MCM_RESTART_TIMEOUT_MS);

private:
    /**
     * \brief Class for waiting until resources are freed, such as switchgears.
     *
     * This is an observer intended for observing the Switch Authority when
     * delayed restart applies.
     */
    class RestartObserver : public ObserverObj<void>
    {
    public:
        RestartObserver(MCMApplication& app,
                        APP_EXIT_CODE exit_Code,
                        lu_uint32_t timoutMs) :
                                                parent(app),
                                                exitCode(exit_Code),
                                                timeout_ms(timoutMs)
        {};
        virtual ~RestartObserver() {};

        virtual void observerUpdate()
        {
            parent.restartProcedure(exitCode, timeout_ms);
        }
    private:
        MCMApplication& parent;
        APP_EXIT_CODE exitCode;
        lu_uint32_t timeout_ms;
    };

private:
    /* Log references */
    EventLogManager* eventLog;

    /* References to Application main objects */
    IMemoryManager* memoryManager;
    ConfigurationManager* configurationManager;
    ModuleManager* moduleManager;
    CommsDeviceManager* commsDevManager;
    UserManager* userManager;
    GeminiDatabase* geminiDatabase;
    DataBaseLogging* dataBaseLogging;
    SCADAProtocolManager* scadaProtocolManager;
    PortManager *portManager;
    ICTHLinkLayer* cthLinkLayer;
    CTH::ConfigurationToolHandler* cth;
    HMIManager* hmiManager;
    StatusManager* statusManager;
    AlarmStatus* alarmStatus;
    MCMOLRButton* mcmOLRButton;
    MCMDoorSwitch* doorSwitch;
    Console* console;
    MonitorProtocolManager* monProtocol;

    ISupportPowerSaving** pwerSavings;

    ModuleAlarmList initialAlarmList;
    RestartObserver* restartObserver;
};



/* ============= MAIN =================== */
int main(int argc, char ** argv)
{
    PRINTCPUTIME(".Starting MCM Application.");

    PRINTCPUTIME(".Preparing Signal Masks");
    SIG_BlockAllExceptTerm();

    //Initialise Time Manager and register application start time
    TimeManager::getInstance().start();

    /* Initialise logs */
    PRINTCPUTIME(".Init logger");
    Logger::setAllLogLevel(LOG_LEVEL_WARNING);
    Logger::getBufferLogger();  //Initialise buffer logger

    /* Initialise default parameters */
    lu_bool_t noCRC = LU_FALSE;         //avoid CRC checking
    lu_bool_t console = LU_FALSE;       //internal console
    int ret = 0;
    lu_bool_t launch = LU_TRUE; //When a parameter is present, tells if the app should launch

    /* Parse command line */
    for(lu_int32_t i = 1; i < argc; ++i)
    {
        lu_char_t *argument = argv[i];

        if( checkCommand(argument, CMD_LINE_VERSION) ||
            checkCommand(argument, CMD_LINE_VERSION_SHORT)
            )
        {
            /* Get MCM firmware string values */
            fprintf(stdout, "%s\n", getMCMAppVersionString().c_str());
            ret = 0;
            launch = LU_FALSE;
            break;  //exit loop
        }
        else if( checkCommand(argument, CMD_LINE_HELP) ||
            checkCommand(argument, CMD_LINE_HELP_SHORT)
            )
        {
            /* Help */
            printCommandLineHelp();
            ret = 0;
            launch = LU_FALSE;
            break;  //exit loop
        }
        else
        {
            lu_bool_t invalidCommand = LU_TRUE;
            if(checkCommand(argument, CMD_LINE_NO_CRC) ||
               checkCommand(argument, CMD_LINE_NO_CRC_SHORT) )
            {
                /* Do not check CRC of Configuration File */
                fprintf(stdout, "  %s option: Do not check CRC of Configuration File\n", argv[0]);
                noCRC = LU_TRUE;
                invalidCommand = LU_FALSE;
            }
            if(checkCommand(argument, CMD_LINE_CONSOLE) ||
               checkCommand(argument, CMD_LINE_CONSOLE_SHORT) )
            {
                /* Enable internal console */
                fprintf(stdout, "  %s option: Enable internal console\n", argv[0]);
                console = LU_TRUE;
                invalidCommand = LU_FALSE;
            }

            if(invalidCommand == LU_TRUE)
            {
                /* Not supported */
                fprintf(stdout, "  %s option: Command '%s' not supported\n", argv[0], argument);
                printCommandLineHelp();

                ret = -1;
                launch = LU_FALSE;
                break;  //exit loop
            }
        }
    }
    if(launch == LU_FALSE)
    {
        return ret;    //If a parameter is present, launch App only when needed.
    }

    /* Start application components */
    MCMApplication mcmApplication;
    return mcmApplication.start(noCRC, console);
}



MCMApplication::MCMApplication() :
                memoryManager(NULL),
                configurationManager(NULL),
                moduleManager(NULL),
                commsDevManager(NULL),
                userManager(NULL),
                geminiDatabase(NULL),
                dataBaseLogging(NULL),
                scadaProtocolManager(NULL),
                cthLinkLayer(NULL),
                cth(NULL),
                hmiManager(NULL),
                statusManager(NULL),
                alarmStatus(NULL),
                mcmOLRButton(NULL),
                doorSwitch(NULL),
                console(NULL),
                monProtocol(NULL),
                pwerSavings(NULL),
                restartObserver(NULL)

{
    MCMBoard::getInstance().setMCMApp(this);	//Set own reference for the MCMBoard Singleton
    eventLog = EventLogManager::getInstance();  /* Get Event Log instance */

#if FIREWALL_SUPPORT
    /* Start firewall always*/
    if( access( "/usr/local/gemini/application/config/firewall.rules", F_OK ) != -1 )
    {
        system("/etc/init.d/firewall restart &");// Restart firewall to apply custom rules
    }
    else
    {
        system("/etc/init.d/firewall start &"); // Start firewall directly
    }
#endif
}

MCMApplication::~MCMApplication()
{
    if(pwerSavings != NULL)
    {
        delete [] pwerSavings;
        pwerSavings = NULL;
    }
    delete restartObserver;

#if FIREWALL_SUPPORT
    /*Resume firewall always, no effect if it is already running.*/
    system("/etc/init.d/firewall start &");
#endif
}


APP_EXIT_CODE MCMApplication::start(lu_bool_t noCRC, lu_bool_t intConsole)
{
    IMemoryManager::MEMERROR memResult;

    Logger::g_logger.info("-- Starting MCM Application... --");

    system(CMD_LDCONFIG);

    /* Initialise TMW library */
    tmwtimer_initialize();
    tmwdiag_initialize();

    /* Start the MonitorProtocolManager */
    PRINTCPUTIME(".Starting Monitor protocol");
    monProtocol = new MonitorProtocolManager(*this);

    PRINTCPUTIME(".Starting Memory Manager");
    /* Start non-volatile memory manager */
    memoryManager = new MemoryManager();

    /* specifically initialise NVRAM and Event log memory before the rest */
    /* NVRAM ID Memory initialisation */
    memResult = memoryManager->init(IMemoryManager::MEM_TYPE_NVRAMID);
    switch(memResult)
    {
        case IMemoryManager::MEMERROR_NONE:
            break;
        case IMemoryManager::MEMERROR_ACCESS:
            //Primary Non-volatile ID Memory failed
            issueAlarm(SYS_ALARM_SUBSYSTEM_SYSTEM, SYSALC_SYSTEM_PRIMARY_FACTORY_NVRAM);
            break;
        case IMemoryManager::MEMERROR_ACCESSBKP:
            //Secondary Non-volatile ID Memory failed
            issueAlarm(SYS_ALARM_SUBSYSTEM_SYSTEM, SYSALC_SYSTEM_FACTORY_NVRAM);
            break;
        default:
            //Both NVRAM ID memories failed
            issueAlarm(SYS_ALARM_SUBSYSTEM_SYSTEM, SYSALC_SYSTEM_PRIMARY_FACTORY_NVRAM, SYS_ALARM_SEVERITY_CRITICAL);
            issueAlarm(SYS_ALARM_SUBSYSTEM_SYSTEM, SYSALC_SYSTEM_FACTORY_NVRAM, SYS_ALARM_SEVERITY_CRITICAL);
            issueAlarm(SYS_ALARM_SUBSYSTEM_SYSTEM, SYSALC_SYSTEM_SYSTEMINIT, SYS_ALARM_SEVERITY_CRITICAL);
            break;
    }


    /* Read NVRAM MCM Factory info and check feature revision compatibility */
    BasicVersionStr featureRev;
    if(memoryManager->read(IMemoryManager::STORAGE_VAR_FEATUREREV, (lu_uint8_t*)&featureRev) != LU_TRUE)
    {
        //error reading data: must've been already signalled
        Logger::g_logger.error("Error reading factory information");
        /* TODO: pueyos_a - issue an alarm here? */
        //issueAlarm(SYS_ALARM_SUBSYSTEM_MCM, SYSALC_MCM_FEATURE_MISMATCH, SYS_ALARM_SEVERITY_INFO);
    }
    else
    {
        if( (featureRev.major != MCM_FEATURE_VERSION_MAJOR) ||
            (featureRev.minor != MCM_FEATURE_VERSION_MINOR)
            )
        {
            //feature revision mismatch
            Logger::g_logger.error("Feature revision mismatch: MCM=%i.%i, App=%i,%i",
                                    featureRev.major, featureRev.minor,
                                    MCM_FEATURE_VERSION_MAJOR, MCM_FEATURE_VERSION_MINOR
                                    );
        }
        /* Issue alarm accordingly */
        if (featureRev.major != MCM_FEATURE_VERSION_MAJOR)
        {
            //feature revision major mismatch
            issueAlarm(SYS_ALARM_SUBSYSTEM_MCM, SYSALC_MCM_FEATURE_MISMATCH, SYS_ALARM_SEVERITY_CRITICAL);
            issueAlarm(SYS_ALARM_SUBSYSTEM_SYSTEM, SYSALC_SYSTEM_SYSTEMINIT, SYS_ALARM_SEVERITY_CRITICAL);
        }
        else
        {
            //Note: "(signed)(lu_int64_t)" added to avoid compiler warning when comparing < 0 in case of SCHEMA_MINOR==0
            if ((signed)(lu_int64_t)featureRev.minor < (signed)(lu_int64_t)MCM_FEATURE_VERSION_MINOR)
            {
                //App feature revision is newer than HW Module's
                issueAlarm(SYS_ALARM_SUBSYSTEM_MCM, SYSALC_MCM_FEATURE_MISMATCH, SYS_ALARM_SEVERITY_ERROR);
                issueAlarm(SYS_ALARM_SUBSYSTEM_SYSTEM, SYSALC_SYSTEM_SYSTEMINIT, SYS_ALARM_SEVERITY_ERROR);
            }
            else if ((lu_int64_t)featureRev.minor > (lu_int64_t)MCM_FEATURE_VERSION_MINOR)
            {
                //App feature revision is older than HW Module's (may work anyway)
                issueAlarm(SYS_ALARM_SUBSYSTEM_MCM, SYSALC_MCM_FEATURE_MISMATCH, SYS_ALARM_SEVERITY_WARNING);
                issueAlarm(SYS_ALARM_SUBSYSTEM_SYSTEM, SYSALC_SYSTEM_SYSTEMINIT, SYS_ALARM_SEVERITY_WARNING);
            }
        }
    }

    /* Event Log memory initialisation */
    //Use all available memory in device:
    memoryManager->setSize(IMemoryManager::STORAGE_VAR_EVENTMEMSIZE, 0);
    //Report Event log size
    lu_uint32_t memEventSize = 0;
    memoryManager->read(IMemoryManager::STORAGE_VAR_EVENTLOG, (lu_uint8_t*)&memEventSize);
    Logger::g_logger.info("Max Point Event storage: %u\n", memEventSize);
    //initialise event log from memory and read events from log:
    if(memoryManager->init(IMemoryManager::MEM_TYPE_EVENT) != IMemoryManager::MEMERROR_NONE)
    {
        /* TODO: pueyos_a - handle Event Log Memory initialisation error: set alarm */
        issueAlarm(SYS_ALARM_SUBSYSTEM_NVRAM, SYSALC_NVRAM_CRC_ERR);
    }
    eventLog->init(reinterpret_cast<void *>(memoryManager));  //start using memory manager in the event manager

    //Silently initialise ALL of the rest of memory managers
    memoryManager->init();

    //Initialise Log Level handler: set log level from memory and record any log level change
    LoggerStatus logStatus(*memoryManager);

    /* Start configuration manager */
    PRINTCPUTIME(".Starting Config Manager");

    /* Check CRC of Config file: 
     * if file not present (check is done in Configuration Manager) or 
     * CRC stored in memory manager does not match with the one got from file, 
     * then do not use Configuration file.
     */
    lu_bool_t cfgFilePresent = LU_FALSE; //Configuration file presence in the file system
    lu_bool_t calculatedCRCvalidity = LU_FALSE; //Validity of the calculated CRC
    lu_uint32_t calculatedCRC = 0;
    lu_uint32_t cfgFileCRC = 0; //Stored Configuration file CRC
    const lu_char_t* cfgFileName = DEFAULT_CONFIG_GZ; //Pointer to the file name to be used
    if(noCRC == LU_TRUE)
    {
        /* Do not check file CRC */
        FILE* fileCheck = fopen(cfgFileName, "r");  //Try to open to check file presence only
        if (fileCheck == NULL)
        {
            log.error("Configuration file not available.");
            issueAlarm(SYS_ALARM_SUBSYSTEM_SYSTEM, SYSALC_SYSTEM_NOCONFIG);
        }
        else
        {
            fclose(fileCheck);
            cfgFilePresent = LU_TRUE;
            //calculate CRC anyway
            if(calcFileCRC(cfgFileName, &calculatedCRC) == FILECRC_ERROR_NONE)
            {
                calculatedCRCvalidity = LU_TRUE;
            }
        }
    }
    else
    {
        if(memoryManager->read(IMemoryManager::STORAGE_VAR_CFGFILECRC, (lu_uint8_t*)&cfgFileCRC, NULL) == LU_TRUE)
        {
            /* check CRC of file */
            switch(calcFileCRC(cfgFileName, &calculatedCRC))
            {
                case FILECRC_ERROR_ACCESS:
                    //file not accessible
                    log.error("Configuration file not available.");
                    cfgFileName = "";
                    issueAlarm(SYS_ALARM_SUBSYSTEM_SYSTEM, SYSALC_SYSTEM_NOCONFIG);
                    break;
                case FILECRC_ERROR_NONE:
                    cfgFilePresent = LU_TRUE;
                    calculatedCRCvalidity = LU_TRUE;    //the calculation is valid
                    if(calculatedCRC != cfgFileCRC)
                    {
                        //CRC mismatch
                        log.error("Invalid CRC of configuration file -- "
                                "Configuration not loaded! stored CRC: %08x file CRC: %08x.",
                                cfgFileCRC, calculatedCRC
                                );
                        cfgFileName = "";
                        issueAlarm(SYS_ALARM_SUBSYSTEM_SYSTEM, SYSALC_SYSTEM_BADCONFIG);
                    }
                    break;
                default:
                    log.fatal("Severe Error checking Configuration file.");
                    cfgFileName = "";
                    issueAlarm(SYS_ALARM_SUBSYSTEM_SYSTEM, SYSALC_SYSTEM_NOCONFIG);
                    break;
            }
        }
        else
        {
            /* Failed to read CRC from memory */
            log.warn("Configuration File CRC code not found in NV memory.");
            /* Calculate new file CRC and store it in memory */
            if(calcFileCRC(cfgFileName, &calculatedCRC) == FILECRC_ERROR_NONE)
            {
                cfgFilePresent = LU_TRUE;
                calculatedCRCvalidity = LU_TRUE;    //the calculation is valid
                if(memoryManager->write(IMemoryManager::STORAGE_VAR_CFGFILECRC, (lu_uint8_t*)&calculatedCRC) != LU_TRUE)
                {
                    log.warn("New Configuration File CRC code cannot be stored in NV memory.");
                }
            }
            //if CRC cannot be calculated, or there is no Config File, just continue
        }
    }


    /* Proceed to read the configuration */
    configurationManager = new ConfigurationManager();
    CONFMGR_ERROR configResult;   //result of Configuration manager interaction
    configResult = configurationManager->initParser(cfgFileName);
    //Report Event
    CTEventSystemStr event;
    event.eventType = EVENT_TYPE_SYSTEM_READ_CONFIG;
    event.value = (configResult == CONFMGR_ERROR_NONE)? EVENT_READ_CONFIG_VALUE_OK :
                                                        EVENT_READ_CONFIG_VALUE_ERROR;

    if(configResult != CONFMGR_ERROR_NONE)
    {
        /* Schema version mismatch: skip reading XML file! */
        cfgFilePresent = LU_FALSE;
        configurationManager->releaseParser();
    }

    /* Create User Manager */
    PRINTCPUTIME(".Initialising UserManager");
    userManager = new UserManager();

    /* Initialise gemini database */
    PRINTCPUTIME(".Initialising G3DB");
    geminiDatabase = new GeminiDatabase(SCHED_TYPE_FIFO, GEMINI_DATABASE_THREAD); //MG Use higher priority ???

    /* Initialise statusManager */
    PRINTCPUTIME(".Starting StatusManager");
    statusManager = new StatusManager(*this,
                                    *geminiDatabase,
                                    *memoryManager);
    statusManager->setConfigFilePresence(cfgFilePresent);
    statusManager->setConfigFileCRC(calculatedCRC, calculatedCRCvalidity);
    IIOModuleManager::OLRButtonConfig buttonOLRcfg;
    configResult = configurationManager->init(*statusManager, buttonOLRcfg);
    if(configResult != CONFMGR_ERROR_NONE)
    {
        log.error("Unable to parse configuration file properly: %s", GDB_ERROR_ToSTRING(configResult));
        issueAlarm(SYS_ALARM_SUBSYSTEM_SYSTEM, SYSALC_SYSTEM_SYSTEMINIT, SYS_ALARM_SEVERITY_ERROR);
        issueAlarm(SYS_ALARM_SUBSYSTEM_SYSTEM, SYSALC_SYSTEM_CONFIGPARSE, SYS_ALARM_SEVERITY_ERROR);
    }
    eventLog->logEvent(event);

    /* Initialise available slave modules */
    configurationManager->initModules();

    /* Initialise Module manager and start discovery */
    moduleManager = new ModuleManager( SCHED_TYPE_FIFO,
                                     MODULE_MANAGER_THREAD,
                                     CANIOModuleFactory::getInstance(),
                                     *configurationManager,
                                     *memoryManager,
                                     *monProtocol
                                    );
    MCMIOModule* mcmModule = dynamic_cast<MCMIOModule*>(moduleManager->getModule(MODULE_MCM, MODULE_ID_0));

    DBG_CHECK(mcmModule != NULL, "mcm should not be null");

    /* Create modules stated in the Configuration File*/
    PRINTCPUTIME(".Creating All modules");
    configurationManager->createAllModules(*moduleManager);

    /* TODO: pueyos_a - Do init LED handlers in a better place, maybe in the Module Manager */
    MCMLED& dummyLED = MCMLED::getMCMLED(MCMLED::LED_ID_DUMMYSW);
    dummyLED.setStatus(MCMLED::LED_STATUS_OFF); //default state if no Dummy Switch CL present

    // Load the ports and add to PortServerFactory
    PRINTCPUTIME(".Init Ports");
    portManager = new PortManager();
    CONFMGR_ERROR resPortMan;
    resPortMan = configurationManager->init(*portManager);

    PRINTCPUTIME(".Init Comms Devices");
    commsDevManager = new CommsDeviceManager();
    CONFMGR_ERROR resCommsDeviceMan;
    resCommsDeviceMan = configurationManager->init(*commsDevManager,
                                                   *portManager,
                                                   *moduleManager);

#if MODBUS_MASTER_SUPPORT
    /* Create modbus devices stated in the Configuration File*/
    PRINTCPUTIME(".Creating All Field Devices");
    configurationManager->createAllMBDevice(moduleManager->getMBDeviceManager(), *portManager);
#endif

    /* Configure Gemini3 database */
    PRINTCPUTIME(".Configuring G3DB with XML file");
    configResult = configurationManager->init(*statusManager,
                                                *memoryManager,
                                                *moduleManager,
                                                *geminiDatabase,
                                                *userManager);
    if(configResult != CONFMGR_ERROR_NONE)
    {
        log.error("Unable to parse configuration file properly: %s", GDB_ERROR_ToSTRING(configResult));
        issueAlarm(SYS_ALARM_SUBSYSTEM_SYSTEM, SYSALC_SYSTEM_SYSTEMINIT, SYS_ALARM_SEVERITY_ERROR);
        issueAlarm(SYS_ALARM_SUBSYSTEM_SYSTEM, SYSALC_SYSTEM_CONFIGPARSE, SYS_ALARM_SEVERITY_ERROR);
    }


    /* Prepare SCADA */
    PRINTCPUTIME(".Init SCADA");
    scadaProtocolManager = new SCADAProtocolManager();
    //Read SCADA config */
    CONFMGR_ERROR resSCADA;
    resSCADA = configurationManager->init(*scadaProtocolManager,
                                          *commsDevManager,
                                          *portManager,
                                          *moduleManager,
                                          *geminiDatabase,
                                          *this);
    if(resSCADA != CONFMGR_ERROR_NONE)
    {
        issueAlarm(SYS_ALARM_SUBSYSTEM_SYSTEM, SYSALC_SYSTEM_CONFIGPARSE, SYS_ALARM_SEVERITY_ERROR);
        log.error("Unable to start SCADA Protocol Manager: error parsing configuration");
    }

    /* Register DB logging */
    dataBaseLogging = new DataBaseLogging(*geminiDatabase);
    

    PRINTCPUTIME(".Starting CommsDeviceManager");
    commsDevManager->startDeviceManger();


    /* Initialise Configuration Tool Handler */
    std::string cgipath = getCGISocketPath();
    PRINTCPUTIME(".Starting ConfigTool CGI");
#if CT_ZMQ_SUPPORT
    cthLinkLayer = new ZMQLinkLayer(CTMsg_MaxSize,
                                        SCHED_TYPE_FIFO,
                                        HTTP_CTH_LINKLAYER_THREAD,
										cgipath.c_str());
#else
    cthLinkLayer = new HttpCTHLinkLayer(CTMsg_MaxSize,
                                        SCHED_TYPE_FIFO,
                                        HTTP_CTH_LINKLAYER_THREAD,
										cgipath.c_str());
#endif
    cth = new CTH::ConfigurationToolHandler(*this,
                                            *statusManager,
                                            *moduleManager,
                                            *geminiDatabase,
                                            *cthLinkLayer,
                                            *userManager,
                                            *memoryManager
                                            );

    /* Initialise HMI Manager */
    PRINTCPUTIME(".Starting HMI Manager");
    startHMIManager(*geminiDatabase, *configurationManager, *moduleManager, &hmiManager);

    /* Release configuration manager resources */
    configurationManager->releaseParser();


    /* == Start secondary handlers == */
    PRINTCPUTIME(".Starting secondary handlers");

    /* Configure RTU Alarm Channels */
    if(mcmModule != NULL)
    {
        MCM_CH_DINPUT alarmObsChannel[] = {
                        MCM_CH_DINPUT_RTU_HEALTHY,
                        MCM_CH_DINPUT_CRITICAL_ALARM,
                        MCM_CH_DINPUT_ERROR_ALARM,
                        MCM_CH_DINPUT_WARNING_ALARM
        };
        const lu_uint32_t sizeAlarmObsChannel = sizeof(alarmObsChannel) / sizeof(MCM_CH_DINPUT);
        IChannel* alarmStatusCh[sizeAlarmObsChannel];

        //IChannel* channel = NULL;
        for (lu_uint32_t j = 0; j < sizeAlarmObsChannel; ++j)
        {
            alarmStatusCh[j] = mcmModule->getChannel(CHANNEL_TYPE_DINPUT, alarmObsChannel[j]);
            if( alarmStatusCh[j] != NULL)
            {
                ModuleAlarmChannel* alarmChannel = static_cast<ModuleAlarmChannel*>(alarmStatusCh[j]);
                alarmChannel->configure(*moduleManager);
            }
        }
        if(alarmStatusCh[0] == NULL)
        {
            //No "RTU Healthy" Channel available
            monProtocol->send_OK_LED_state(OK_LEDSTATE_CRITICAL);
        }
        else
        {
            /* Start the Alarm Status Observer */
            alarmStatus = new AlarmStatus(&(alarmStatusCh[1]), *monProtocol);
        }
    }

    /* Start MCM Front button handler */
    mcmOLRButton = new MCMOLRButton(*moduleManager, *geminiDatabase, buttonOLRcfg);

    /* Start Door switch handler */
    if(mcmModule != NULL)
    {
        IChannel& doorCh = mcmModule->getChannelDoorSwitch();
        doorSwitch = new MCMDoorSwitch(doorCh, *statusManager);
    }

    /* Configure power saving components*/
    pwerSavings = new ISupportPowerSaving*[2];
    pwerSavings[0] = hmiManager;
    pwerSavings[1] = moduleManager;
    statusManager->setPowerSavingComps(pwerSavings,2);


    /* === START COMPONENTS === */

    /* start discovery process */
    PRINTCPUTIME(".Starting Module discovery");
    if(moduleManager->discover() != IOM_ERROR_NONE)
    {
        //CAN LED red light
        MCMLED& canLED = MCMLED::getMCMLED(MCMLED::LED_ID_CAN);
        canLED.setStatus(MCMLED::LED_STATUS_SOLID_1);
        //Set RTU alarms accordingly
        issueAlarm(SYS_ALARM_SUBSYSTEM_SYSTEM, SYSALC_SYSTEM_SYSTEMINIT, SYS_ALARM_SEVERITY_CRITICAL);
    }

    PRINTCPUTIME(".Starting CTH");
    cthLinkLayer->startServer();

    /* Start protocol stack */
    if(configResult == CONFMGR_ERROR_NONE)
    {
        /* Delayed startup of the SCADA */
        lu_uint32_t protoStackDelay_sec = statusManager->getProtocolStackDelay();
        //Note that the SCADAStackDelay job will be deleted by the scheduler
        geminiDatabase->addJob(new SCADAStackDelay(*this, SEC_TO_MSEC(protoStackDelay_sec)));
    }

    /* Report pending Module Alarms from initialisation */
    if(mcmModule != NULL)
    {
        mcmModule->setActive(LU_TRUE);
        mcmModule->inhibitAlarms(); //Prevent alarm listing until finished

        for(ModuleAlarmList::iterator it = initialAlarmList.begin(),
                                     end = initialAlarmList.end(); it != end; ++it)
        {
            /* Issue alarm */
            if( (*it)->isActive() == LU_TRUE )
            {
                //Report active and valid alarms only
                mcmModule->activateAlarm((*it)->getID(), (*it)->severity, (*it)->parameter);
            }
        }
        //drop initialisation Alarm list since it is not needed anymore
        initialAlarmList.clear();   //deletes all elements in the list
        ModuleAlarmList().swap(initialAlarmList);   //release some memory

        //Remove early Sensor Failure alarm
        ModuleAlarm::AlarmIDStr sensorFail(SYS_ALARM_SUBSYSTEM_MCM, SYSALC_MCM_SENSOR_FAILURE);
        mcmModule->ackAlarm(sensorFail);

        //Grant alarm listing
        moduleManager->grantAlarms();
    }
    else
    {
        /* Force set of OK LED to critical when MCM Module not initialised */
        /* Note: OK LED is set by the Alarm Status Observer otherwise */
        monProtocol->send_OK_LED_state(OK_LEDSTATE_CRITICAL);
    }


    /* Start Command Line Interpreter (using main thread) and stay there until application restarts */
    PRINTCPUTIME(".Starting console");
    console = new Console(*geminiDatabase, *statusManager, *moduleManager, *eventLog, *monProtocol, *memoryManager);
    APP_EXIT_CODE ret = console->start(intConsole); //Stay at the Command Line Interpreter server

    /* At this point we are leaving the application */
    this->stop();
    return ret;
}


lu_int32_t MCMApplication::setSCADAService(lu_bool_t enable)
{
    SCADAP_ERROR res = SCADAP_ERROR_NOT_INITIALIZED;
    if(scadaProtocolManager != NULL)
    {
        //Only enable the SCADA protocol if we are in service
        SERVICEMODE serviceMode;
        OUTSERVICEREASON reason;
        statusManager->getServiceMode(serviceMode, reason);
        if( (enable == LU_TRUE) && (serviceMode == SERVICEMODE_INSERVICE) )
        {
            res = scadaProtocolManager->startProtocolStack();
        }
        else
        {
            res = scadaProtocolManager->stopProtocolStack();
        }
    }
    return res;
}

void MCMApplication::endOfInitialisation()
{
    /* Set startup channel indication: End-of-initialisation */
    geminiDatabase->setEOIFlag();
    IChannel* startup = moduleManager->getMCM()->getChannel(CHANNEL_TYPE_DINPUT, MCM_CH_DINPUT_RTU_STARTED);
    if(startup != NULL)
    {
        IODataUint8 data;
        IChannel::ValueStr value(data);
        data = 1;
        startup->write(value);
    }
    Logger::g_logger.info("End of initialisation---");
}


bool MCMApplication::restart(RESTART_TYPE type, APP_EXIT_CODE exitcode)
{
    return lockRestart(type, exitcode);
}

bool MCMApplication::reboot(RESTART_TYPE type)
{
    return lockRestart(type, APP_EXIT_CODE_REBOOT_RTU, MCM_REBOOT_TIMEOUT_MS);
}

bool MCMApplication::shutdown(RESTART_TYPE type)
{
    return lockRestart(type, APP_EXIT_CODE_SHUTDOWN_RTU, MCM_SHUTDOWN_TIMEOUT_MS);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
bool MCMApplication::lockRestart(RESTART_TYPE type, APP_EXIT_CODE exitCode, lu_uint32_t timeout_ms)
{
    SwitchAuthority swA;
    switch(type)
    {
        case RESTART_TYPE_TRY:
            if(swA.acquire(SwitchAuthority::SWTYPE_UPPER_AUTHORITY, SwitchAuthority::ACQ_FUNC_GET) == LU_FALSE)
            {
                return false;   //Not acquired
            }
            break;

        case RESTART_TYPE_FORCED:
            swA.acquire(SwitchAuthority::SWTYPE_UPPER_AUTHORITY, SwitchAuthority::ACQ_FUNC_RESERVE);
            break;

        case RESTART_TYPE_DELAY:
            if(swA.acquire(SwitchAuthority::SWTYPE_UPPER_AUTHORITY, SwitchAuthority::ACQ_FUNC_RESERVE) == LU_FALSE)
            {
                /* Not acquired: delay it */
                if(restartObserver == NULL)
                {
                    restartObserver = new RestartObserver(*this, exitCode, timeout_ms);
                    swA.attach(restartObserver);
                    return true;
                }
                return false;   //already requested!
            }
            break;
        default:
            return false;
            break;
    }
    restartProcedure(exitCode, timeout_ms);
    return true;
}


void MCMApplication::restartProcedure(APP_EXIT_CODE exitCode, lu_uint32_t timeout_ms)
{
    /* Lock OLR to OFF without storing the change in NVRAM */
    if(geminiDatabase != NULL)
    {
        geminiDatabase->setLocalRemote(OLR_STATE_OFF, OLR_SOURCE_BKPLANESW);
    }

    /* Notify the Monitor of our intended exit code (5s timeout before we're killed) */
    monProtocol->send_notify_exit(exitCode, timeout_ms);

    console->stop(exitCode);
}


lu_int32_t MCMApplication::startHMIManager(GeminiDatabase &database,
                                        ConfigurationManager &configurationManager,
                                        ModuleManager &moduleManager,
                                        HMIManager **hmiManager
                                        )
{
    const lu_char_t* FTITLE = "MCMApplication:startHMIManager:";

    *hmiManager = NULL;
    lu_int32_t ret = -1;
    HMICANChannel *hmiCh = NULL;

    // Get MCM
    MCMIOModule* mcm = moduleManager.getMCM();

    if(mcm != NULL)
    {
        // Get HMI channel
        IIOModule* hmi = moduleManager.getModule(MODULE_HMI, MODULE_ID_0);
        if(hmi != NULL)
        {
            IChannel *ch = hmi->getChannel(CHANNEL_TYPE_HMI, 0);
            hmiCh = dynamic_cast<HMICANChannel*>(ch);
        }

        if(hmi == NULL)
            log.warn("%s HMI not found in configuration.", FTITLE);
        else if(hmiCh == NULL)
            log.warn("%s HMI channel not found in HMI module.", FTITLE);

        *hmiManager = new HMIManager(SCHED_TYPE_FIFO,
                        HMI_MANAGER_THREAD,
                        database,
                        mcm->getChannelHMIDectect(),
                        mcm->getChannelHMIPowerEnable(),
                        mcm->getChannelHMIPowerGood(),
                        hmiCh
                        );

        if(*hmiManager != NULL)
        {
            configurationManager.init(**hmiManager, database, moduleManager, *statusManager);
            ret = 0;
        }
    }
    else
    {
        log.error("%s MCM not available for HMI initialisation.", FTITLE);
    }
    return ret;
}


lu_int32_t MCMApplication::stop()
{
    /* Stop components */
    PRINTCPUTIME(".Stopping Components.");
    if(console != NULL)
    {
        console->stop();    //Might be done already
    }
    setSCADAService(LU_FALSE);  //Stop protocol stack

    if(cthLinkLayer != NULL)
    {
        PRINTCPUTIME(".Stopping CTH.");
        cthLinkLayer->stopServer();
    }
    if(hmiManager != NULL)
    {
        PRINTCPUTIME(".Stopping HMI.");
        hmiManager ->terminate();
    }
    if(geminiDatabase != NULL)
    {
        PRINTCPUTIME(".Stopping G3DB.");
        geminiDatabase->stop(); // Detach all the observers etc
    }
    if(moduleManager != NULL)
    {
        PRINTCPUTIME(".Stopping Modules.");
        /* Stop updates from occurring */
        moduleManager->stopAllModules();
    }

    /* Eliminate components */
    PRINTCPUTIME(".Eliminating components.");
    delete console;
    delete doorSwitch;
    delete mcmOLRButton;
    delete alarmStatus;

    delete hmiManager;
    hmiManager = NULL;
    PRINTCPUTIME(".hmiManager deleted.");

    delete cth;
    delete cthLinkLayer;
    delete statusManager;
    delete scadaProtocolManager;
    delete dataBaseLogging;

    delete geminiDatabase;
    PRINTCPUTIME(".geminiDatabase deleted.");

    delete userManager;
    PRINTCPUTIME(".userManager deleted.");

    delete moduleManager;
    PRINTCPUTIME(".moduleManager deleted.");

    delete commsDevManager;
    PRINTCPUTIME(".commsDevManager deleted.");

    delete portManager;
    PRINTCPUTIME(".portManager deleted.");

    delete configurationManager;
    PRINTCPUTIME(".configurationManager deleted.");

    /* TODO: pueyos_a - Move LED disabling to proper place */
    MCMLED& dummyLED = MCMLED::getMCMLED(MCMLED::LED_ID_DUMMYSW);
    dummyLED.setStatus(MCMLED::LED_STATUS_OFF);

    memoryManager->close(); //Close here the memory manager to allow other objects to use it
    PRINTCPUTIME(".memoryManager closed.");

    delete memoryManager;
    PRINTCPUTIME(".memoryManager deleted.");
    //delete logger;

    return 0;
}


void MCMApplication::issueAlarm(SYS_ALARM_SUBSYSTEM subSystem,
                                lu_uint32_t         alarmCode
                                )
{
    ModuleAlarm* mAlarm = ModuleAlarmFactory::createAlarm(subSystem, alarmCode);
    if(mAlarm != NULL)
    {
        mAlarm->activate();
        addAlarm(*mAlarm); //store it
    }
}

void MCMApplication::issueAlarm(SYS_ALARM_SUBSYSTEM subSystem,
                                lu_uint32_t         alarmCode,
                                SYS_ALARM_SEVERITY  severity,
                                lu_uint16_t         parameter
                                )
{
    ModuleAlarm* mAlarm = ModuleAlarmFactory::createAlarm(subSystem, alarmCode);
    if(mAlarm != NULL)
    {
        mAlarm->severity = severity;
        mAlarm->parameter = parameter;
        mAlarm->activate();
        addAlarm(*mAlarm); //store it
    }
}


void MCMApplication::addAlarm(ModuleAlarm& alarm)
{
    if(alarm.isValid() == LU_FALSE)
    {
        /* Error when creating alarm that the factory doesn't have */
        log.error("Error trying to create alarm ID (%d,%d) - alarm ID does not exist.",
                    alarm.subSystem, alarm.alarmCode);
        return; //nothing to do
    }
    for(ModuleAlarmList::iterator it = initialAlarmList.begin(),
                                 end = initialAlarmList.end(); it != end; ++it)
    {
        if( (*it)->checkID(alarm) == LU_TRUE)
        {
            //Alarm found: update
            *(*it) = alarm;
            return;  //stop search
        }
    }
    //not found: add new one
    initialAlarmList.push_back(&alarm);
}


static lu_bool_t checkCommand(lu_char_t *buffer, const lu_char_t *command)
{
    return ((strlen(buffer) == strlen(command)) && (strcmp (buffer, command) == 0));
}


static void printCommandLineHelp()
{
    fprintf(stdout, "\nSupported commands:\n"
                    "\t%s (%s): Show this help\n"
                    "\t%s (%s): Print application version\n"
                    "\t%s (%s): Do not check Configuration File integrity\n"
                    "\t%s (%s): Start internal console\n",
                    CMD_LINE_HELP, CMD_LINE_HELP_SHORT,
                    CMD_LINE_VERSION, CMD_LINE_VERSION_SHORT,
                    CMD_LINE_NO_CRC, CMD_LINE_NO_CRC_SHORT,
                    CMD_LINE_CONSOLE, CMD_LINE_CONSOLE_SHORT
                    );
}



/*
 *********************** End of file ******************************************
 */

