/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN Input Analogue channel module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   05/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_F8CFB470_0BC8_482d_9629_7303BC04A88B__INCLUDED_)
#define EA_F8CFB470_0BC8_482d_9629_7303BC04A88B__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "AnalogueCANChannel.h"
#include "TimeManager.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/**
 * \brief Analogue Input channel configuration structure
 */
struct InputAnalogueCANChannelConf
{

public:
    lu_bool_t enable;
    lu_bool_t eventEnable;
    lu_uint16_t eventMs;
public:
    InputAnalogueCANChannelConf() : //Constructor: set default values
            enable(LU_FALSE),
            eventEnable(LU_FALSE),
            eventMs(0)
    {}

};

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class InputAnalogueCANChannel : public AnalogueCANChannel
{
public:
    InputAnalogueCANChannel(lu_uint16_t ID = 0, CANIOModule *board = NULL);
    virtual ~InputAnalogueCANChannel();

    /* === Inherited from CANChannel === */
    virtual IOM_ERROR read(IChannel::ValueStr& valuePtr);
    virtual IOM_ERROR decode(ModuleMessage* message);

    /**
     * \brief Configure the channel
     *
     * The configuration is stored locally.
     *
     * \param conf Reference to the channel configuration
     *
     * \return error Code
     */
    void setConfig(InputAnalogueCANChannelConf &conf){this->conf = conf;}

protected:
    /* === Inherited from CANChannel === */
    virtual IOM_ERROR configure();

private:
    IOM_ERROR handleAEvent(ModuleMessage* message);

private:
	InputAnalogueCANChannelConf conf;   //Channel configuration

    lu_int32_t value;                   //Value reported
    TimeManager::TimeStr timeStamp;

};


#endif // !defined(EA_F8CFB470_0BC8_482d_9629_7303BC04A88B__INCLUDED_)


/*
 *********************** End of file ******************************************
 */
