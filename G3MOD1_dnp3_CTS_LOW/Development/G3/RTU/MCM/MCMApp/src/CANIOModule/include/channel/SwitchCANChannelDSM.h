/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: SwitchCANChannelDSM.h 19 Jun 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/CANIOModule/include/channel/SwitchCANChannelDSM.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       DSM-specific Switch Out Channel public interface.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 19 Jun 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19 Jun 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef SWITCHCANCHANNELDSM_H__INCLUDED
#define SWITCHCANCHANNELDSM_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "SwitchCANChannel.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief DSM-specific switch operation channel
 *
 * This is the specific implementation for operating digital output CAN channels
 * specialised in switch operations.
 */
class SwitchCANChannelDSM: public SwitchCANChannel
{
public:
    struct SwitchCANChannelDSMConf
    {
    public:
        lu_bool_t enable;
        lu_uint16_t pulseLengthMs;
        lu_uint16_t overrunMS;
        lu_uint16_t opTimeoutS;
        lu_uint32_t inhibit;
    public:
        SwitchCANChannelDSMConf() :
                                enable(LU_FALSE),
                                pulseLengthMs(0),
                                overrunMS(0),
                                opTimeoutS(0),
                                inhibit(0)
        {};
        void reset()
        {
            *this = SwitchCANChannelDSMConf();   //re-initialise
        };
    };

public:
    /**
     * \brief Custom constructor
     */
    SwitchCANChannelDSM(lu_uint16_t ID = 0, CANIOModule *board = NULL) :
                                                    SwitchCANChannel(ID, board)
    {};

    /**
     * \brief Configure the channel
     *
     * The configuration is stored locally.
     *
     * \param conf Reference to the channel configuration
     *
     * \return error Code
     */
    void setConfig(SwitchCANChannelDSMConf &conf){this->config = conf;}

    /**
     * \brief Get channel configuration
     *
     * \param configuration where the configuration is saved
     *
     * \return error Code
     */
    IOM_ERROR getConfiguration(SwitchCANChannelDSMConf &configuration);

protected:
    /* === Inherited from SwitchCANChannel === */
    virtual IOM_ERROR configure();

private:
    SwitchCANChannelDSMConf config;
};

#endif /* SWITCHCANCHANNELDSM_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
