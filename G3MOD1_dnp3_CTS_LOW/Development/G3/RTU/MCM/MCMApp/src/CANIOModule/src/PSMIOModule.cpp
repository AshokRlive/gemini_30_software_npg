/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       PSM Module class implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "PSMIOModule.h"
#include "ModuleProtocol.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */



PSMIOModule::PSMIOModule( MODULE_ID id                                 ,
                          COMM_INTERFACE iFace                         ,
                          TSynchManager &tsynch                        ,
                          ICANModuleConfigurationFactory &moduleFactory
                        ) :
                            CANIOModule(MODULE_PSM, id, iFace, tsynch, moduleFactory)
{
    lu_uint32_t i;

    /* Set channel id and board id: Digital Input */
    for(i = 0; i < PSM_CH_DINPUT_LAST; i++)
    {
        BInput[i].setBoard(this);
        BInput[i].setID(i);
    }

    /* Set channel id and board id: Analogue Input */
    for(i = 0; i < PSM_CH_AINPUT_LAST; i++)
    {
        AInput[i].setBoard(this);
        AInput[i].setID(i);
    }

    /* Set channel id and board id: Power supply */
    for(i = 0; i < PSM_CH_PSUPPLY_LAST; i++)
    {
        PSupply[i].setBoard(this);
        PSupply[i].setID(i);
    }

    /* Set channel id and board id: Battery charger */
    for(i = 0; i < PSM_CH_BCHARGER_LAST; i++)
    {
        BCharger[i].setBoard(this);
        BCharger[i].setID(i);
    }

    /* Set channel id and board id: Fan */
    for(i = 0; i < PSM_CH_FAN_LAST; i++)
    {
        Fan[i].setBoard(this);
        Fan[i].setID(i);
    }

	/* Full channels initialisation: Digital Output */
	for (i = 0; i < PSM_CH_DOUT_LAST; i++)
	{
		DOutput[i].setBoard(this);
		DOutput[i].setID(i);
	}

    initStateMachine();
}


PSMIOModule::~PSMIOModule()
{
}


IChannel *PSMIOModule::getChannel(CHANNEL_TYPE type, lu_uint32_t number)
{
    IChannel* channelPtr = NULL;

    switch(type)
    {
        case CHANNEL_TYPE_DINPUT:
            if(number < PSM_CH_DINPUT_LAST)
            {
                channelPtr = &BInput[number];
            }
        break;

        case CHANNEL_TYPE_AINPUT:
            if(number < PSM_CH_AINPUT_LAST)
            {
                channelPtr = &AInput[number];
            }
        break;

        case CHANNEL_TYPE_PSUPPLY:
            if(number < PSM_CH_PSUPPLY_LAST)
            {
                channelPtr = &PSupply[number];
            }
        break;

        case CHANNEL_TYPE_BCHARGER:
            if(number < PSM_CH_BCHARGER_LAST)
            {
                channelPtr = &BCharger[number];
            }
        break;

        case CHANNEL_TYPE_FAN:
            if(number < PSM_CH_FAN_LAST)
            {
                channelPtr = &Fan[number];
            }
        break;

        case CHANNEL_TYPE_DOUTPUT:
            if(number < PSM_CH_DOUT_LAST)
            {
                channelPtr = &DOutput[number];
            }
        break;

        default:
            log.error("PSMIOModule::getChannel: "
                            "Unsupported CAN channel: %i. Function: %s",
                            type, __FUNCTION__
                          );
        break;
    }

    return channelPtr;
}


IOM_ERROR PSMIOModule::tickEvent(lu_uint32_t dTime)
{
    /* Call parent tick function */
    return CANIOModule::tickEvent(dTime);
}


IOM_ERROR PSMIOModule::decodeMessage(ModuleMessage* message)
{
    IOM_ERROR ret = IOM_ERROR_NONE;

    if(message == NULL)
    {
        return IOM_ERROR_PARAM;
    }

    /* General message ? */
    ret = CANIOModule::decodeMessage(message);
    if (ret == IOM_ERROR_NOT_SUPPORTED)
    {
        /* No - Try custom decoder */
        switch(message->header.messageType)
        {
            /* Forward the event to the channel */
            case MODULE_MSG_TYPE_EVENT:
                switch(message->header.messageID)
                {
                    case MODULE_MSG_ID_EVENT_DIGITAL:
                        CANIOModule::handleDEvent(message, BInput, PSM_CH_DINPUT_LAST);
                        ret = IOM_ERROR_NONE;
                    break;

                    case MODULE_MSG_ID_EVENT_ANALOGUE:
                        CANIOModule::handleAEvent(message, AInput, PSM_CH_AINPUT_LAST);
                        ret = IOM_ERROR_NONE;
                    break;

                    default:
                        ret = IOM_ERROR_NOT_SUPPORTED;
                    break;
                }
            break;

            default:
                ret = IOM_ERROR_NOT_SUPPORTED;
            break;
        }
    }

    if(ret == IOM_ERROR_NOT_SUPPORTED)
    {
        log.error("PSMIOModule::decodeMessage: "
                        "%s Unsupported message: 0x%02x:0x%02x %s",
                        getName(),
                        message->header.messageType,
                        message->header.messageID,
                        __AT__
                       );
    }
    return ret;
}


IOM_ERROR PSMIOModule::loadConfiguration()
{
    lu_uint32_t ch_id;
    IOM_ERROR result = IOM_ERROR_NONE;
    IOM_ERROR ret;

    if (configFactory.isConfigured(mid) == LU_FALSE)
    {
        log.warn("%s-was not found in configuration", getName());
        return IOM_ERROR_NOT_FOUND;
    }

    ret = configFactory.getModuleConf<PSMConfigStr>(mid.type, mid.id, conf);
    if(ret != IOM_ERROR_NONE)
    {
        result = ret;
        log.error("%s failed to load Fan configuration. Error %i: %s",
                    getName(), ret, IOM_ERROR_ToSTRING(ret));
    }
    else
    {
        log.debug("%s Get PS Module config: FANfitted=%s, FANspeedSensor=%s, "
                               "temp thr=%i, temp hyst=%i, fault hyst=%i(ms),"
                               "fan Digital Output=%s, fan external supply=%s;"
                               "BattTest=%u(h), BattTestDuration=%u(secs).",
                               getName(),
                               (conf.fanConfig.fanFitted == 1)? "yes" : "no",
                               (conf.fanConfig.fanSpeedSensorFitted == 1)? "yes" : "no",
                               conf.fanConfig.fanTempThreshold,
                               conf.fanConfig.fanTempHysteresis,
                               conf.fanConfig.fanFaultHysteresisMs,
							   (conf.fanConfig.fanDigitalOutput == 1)? "yes" : "no",
							   (conf.fanConfig.fanExternalSupply == 1)? "yes" : "no",
                               conf.chargerCfg.batteryTestPeriodHours,
                               conf.chargerCfg.batteryTestDurationSecs
                              );
     }

    /* ==Configure all Digital Input== */
    InputDigitalCANChannelConf dConf;
    for(ch_id = 0; ch_id < PSM_CH_DINPUT_LAST; ch_id++)
    {
        /* Get configuration */
         ret = configFactory.getIDigitalConf(mid.type, mid.id, ch_id, dConf);

         if(ret == IOM_ERROR_NONE)
         {
             BInput[ch_id].setConfig(dConf);
         }
         else
         {
             result = ret;
             log.error("%s failed to load configuration for %s. Error %i: %s",
                         getName(), BInput[ch_id].getName(),
                         ret, IOM_ERROR_ToSTRING(ret)
                         );
         }

    }

    /* ==Configure all Analogue Input== */
    InputAnalogueCANChannelConf aConf;
    for(ch_id = 0; ch_id < PSM_CH_AINPUT_LAST; ch_id++)
    {
        ret = configFactory.getIAnalogueConf(mid.type, mid.id, ch_id,  aConf);
        if(ret == IOM_ERROR_NONE)
        {
            AInput[ch_id].setConfig(aConf);
        }
        else
        {
            result = ret;
            log.error("%s failed to load configuration for %s. Error %i: %s",
                        getName(), AInput[ch_id].getName(),
                        ret, IOM_ERROR_ToSTRING(ret)
                        );
        }
    }

    /* ==Configure all Digital Output channels== */
    OutputDigitalCANChannelConf doConf;

    for (ch_id = 0; ch_id < PSM_CH_DOUT_LAST; ch_id++) {

        /* Load and set digital configuration only if FAN works
         * as digital output is selected.
         * For normal fan functionality and fan as external supply,
         * disable digital output channel as it can create strange
         * output if user do "digital output test" */
        if (conf.fanConfig.fanDigitalOutput) {
            ret = configFactory.getDigitalOutConf(mid.type, mid.id, ch_id, doConf);
            if (ret == IOM_ERROR_NONE)
                DOutput[ch_id].setConfig(doConf);
            else {
                result = ret;
                log.error("%s failed to load configuration for %s. Error %i: %s",
                        getName(), DOutput[ch_id].getName(), result,
                        IOM_ERROR_ToSTRING(result));
            }
        } else {
            doConf.enable = false;
            doConf.pulseLengthMs = 0;
            DOutput[ch_id].setConfig(doConf);
        }

    }

    return result;
}


IOM_ERROR PSMIOModule::configure()
{
    lu_uint32_t i;
    lu_bool_t hasFailure = LU_FALSE;
    IOM_ERROR ret;

    /* ==General configuration== */
    {
        /* -- Send FAN configuration message(s) to module -- */
        /* Prepare message */
        FanConfigStr messagePayload;
        messagePayload = conf.fanConfig;
        PayloadRAW payload;
        payload.payloadPtr = reinterpret_cast<lu_uint8_t *>(&messagePayload);
        payload.payloadLen = sizeof(FanConfigStr);
        ret = sendCANConfig(payload, MODULE_MSG_ID_CFG_FAN_CONTROLLER_C, MODULE_MSG_ID_CFG_FAN_CONTROLLER_R);
    }
    if(ret == IOM_ERROR_NONE)
    {
        /* -- Send Cyclic Battery Test configuration message(s) to module -- */
        /* Prepare message */
        ChargerConfigStr messagePayload;
        PayloadRAW payload;
        messagePayload = conf.chargerCfg;
        payload.payloadPtr = reinterpret_cast<lu_uint8_t *>(&messagePayload);
        payload.payloadLen = sizeof(ChargerConfigStr);
        ret = sendCANConfig(payload, MODULE_MSG_ID_CFG_BATTERY_CHARGER_C, MODULE_MSG_ID_CFG_BATTERY_CHARGER_R);
    }

   if(ret != IOM_ERROR_NONE)
   {
       log.error("Unable to configure module %s. Error %i: %s",
                       getName(),
                       ret, IOM_ERROR_ToSTRING(ret)
                     );
       hasFailure = LU_TRUE;
   }

    /* ==Configure all Digital Input== */
    for(i = 0; i < PSM_CH_DINPUT_LAST; i++)
    {
        ret = BInput[i].sendConfiguration();
        LOG_CH_CONFIG_FAILURE(ret,"DInput");
    }

    /* ==Configure all Analogue Input== */
    for(i = 0; i < PSM_CH_AINPUT_LAST; i++)
    {
        ret = AInput[i].sendConfiguration();
        LOG_CH_CONFIG_FAILURE(ret,"AInput");
    }

    /* Configure all Digital Output channels */
    for(i = 0; i < PSM_CH_DOUT_LAST; i++)
    {
        ret = DOutput[i].sendConfiguration();
        LOG_CH_CONFIG_FAILURE(ret,"DOutput");
    }

    return (hasFailure == LU_TRUE)? IOM_ERROR_CONFIG : IOM_ERROR_NONE;
}

void PSMIOModule::stop()
{
    lu_int32_t i;

    /* Set channel id and board id: Digital Input */
    for(i = 0; i < PSM_CH_DINPUT_LAST; i++)
    {
        BInput[i].stopUpdate();
    }

    /* Set channel id and board id: Analogue Input */
    for(i = 0; i < PSM_CH_AINPUT_LAST; i++)
    {
        AInput[i].stopUpdate();
    }

    /* Set channel id and board id: Power supply */
    for(i = 0; i < PSM_CH_PSUPPLY_LAST; i++)
    {
        PSupply[i].stopUpdate();
    }

    /* Set channel id and board id: Battery charger */
    for(i = 0; i < PSM_CH_BCHARGER_LAST; i++)
    {
        BCharger[i].stopUpdate();
    }

    /* Set channel id and board id: Fan */
    for(i = 0; i < PSM_CH_FAN_LAST; i++)
    {
        Fan[i].stopUpdate();
    }

    /* Stop all Output channels */
    for(i = 0; i < PSM_CH_DOUT_LAST; i++)
    {
        DOutput[i].stopUpdate();
    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void PSMIOModule::setAllChsActive(lu_bool_t status)
{
    lu_uint32_t i;

    /* Digital Input */
    for(i = 0; i < PSM_CH_DINPUT_LAST; i++)
    {
        BInput[i].setActive(status);
    }

    /* Analogue Input */
    for(i = 0; i < PSM_CH_AINPUT_LAST; i++)
    {
        AInput[i].setActive(status);
    }

    /* Power supply */
    for(i = 0; i < PSM_CH_PSUPPLY_LAST; i++)
    {
        PSupply[i].setActive(status);
    }

    /* Battery charger */
    for(i = 0; i < PSM_CH_BCHARGER_LAST; i++)
    {
        BCharger[i].setActive(status);
    }

    /* Fan */
    for(i = 0; i < PSM_CH_FAN_LAST; i++)
    {
        Fan[i].setActive(status);
    }

    /* Digital Output */
    for(i = 0; i < PSM_CH_DOUT_LAST; i++)
    {
        DOutput[i].setActive(status);
    }
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
IOM_ERROR PSMIOModule::sendCANConfig(PayloadRAW& payload, const MODULE_MSG_ID messageC, const MODULE_MSG_ID messageR)
{
    IOM_ERROR ret = IOM_ERROR_NONE;

    IIOModule::sendReplyStr message;
    message.messageType     = MODULE_MSG_TYPE_CFG;
    message.messageID       = messageC;
    message.messagePtr      = &payload;
    message.rMessageType    = MODULE_MSG_TYPE_CFG;
    message.rMessageID      = messageR;
    message.timeout = ms_to_timeval(200);     //ms timeout for CAN message reply

    /* Send message */
    ret = this->sendMessage(&message);
    if(ret == IOM_ERROR_NONE)
    {
        /* Get payload */
        (message.rMessagePtr)->getPayload(&payload);
        if(payload.payloadLen == MODULE_MESSAGE_SIZE(ModReplyStr))
        {
            ModReplyStr *replyPtr = reinterpret_cast<ModReplyStr*>(payload.payloadPtr);
            if(replyPtr->status != REPLY_STATUS_OKAY)
            {
                log.error("Error Sending configuration to slave module %s. Reply status: %i",
                            getName(),
                            replyPtr->status
                          );
                ret = IOM_ERROR_REPLY;
            }
        }
        else
        {
            /* Wrong payload */
            log.error("Error Sending configuration to slave module %s - reply has invalid size: %i",
                            getName(),
                            payload.payloadLen
                          );
            ret = IOM_ERROR_REPLY;
        }
        /* Release reply message */
        delete (message.rMessagePtr);
    }
    return ret;
}


/*
 *********************** End of file ******************************************
 */
