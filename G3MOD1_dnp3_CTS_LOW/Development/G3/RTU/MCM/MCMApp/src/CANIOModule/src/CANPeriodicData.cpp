/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN periodic data manager implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   01/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "CANPeriodicData.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define MS_2_TICK(ms) ((ms * 1000) / CyclicTimerUsec)


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/* Synch message AND Module's tick event period */
static const lu_uint32_t CyclicTimerSec  = 0     ;
static const lu_uint32_t CyclicTimerUsec = 100000;

/* HeartBeat delay before starting the period */
static const lu_uint32_t HBeatDelayMSec  = 3000  ;
//Iterations to reach the delay
static const lu_uint32_t HBeatDelayIterations  = (HBeatDelayMSec) /
                        ( (CyclicTimerSec * 1000) + (CyclicTimerUsec / 1000) );


/* HeartBeat message period */
static const lu_uint32_t HBeatTickTimeout = MS_2_TICK(500);

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

CANPeriodicData::CANPeriodicData( CANFraming    &socketA  ,
                                  CANFraming    &socketB  ,
                                  CANIOModuleDB &modules  ,
                                  TSynchManager &tsynch   ,
                                  SCHED_TYPE   schedType  ,
                                  lu_uint32_t  priority
                                ) : Thread(schedType, priority, LU_TRUE, "CANPeriodicData"),
                                    socketA(socketA),
                                    socketB(socketB),
                                    modules(modules),
                                    tsynch(tsynch),
                                    cyclicTimer(CyclicTimerSec, CyclicTimerUsec, Timer::TIMER_TYPE_PERIODIC),
                                    hBeatMsg(sizeof(ModuleHBeatMStr)),
                                    canLED(MCMLED::getMCMLED(MCMLED::LED_ID_CAN)),
                                    hBeatCounter(0),
                                    timeCounter(0),
                                    mutexHBeat(LU_TRUE),
                                    log(Logger::getLogger(SUBSYSTEM_ID_CANMANAGER))
{
    ModuleHBeatMStr *hBeatPayloadPtr = reinterpret_cast<ModuleHBeatMStr *>(&(hBeatMsg.payload));
    hBeatPayloadPtr->bootloaderMode = 0;
    hBeatPayloadPtr->powerSaveMode  = 0;
    hBeatPayloadPtr->timeCounter  = 0;

    /* Prepare Heart Beat broadcast message */
    hBeatMsg.header.messageType   = MODULE_MSG_TYPE_LPRIO;
    hBeatMsg.header.messageID     = MODULE_MSG_ID_LPRIO_HBEAT_M;
    hBeatMsg.header.destination   = MODULE_BRD;
    hBeatMsg.header.destinationID = MODULE_ID_BRD;
    hBeatMsg.header.source        = MODULE_MCM;
    hBeatMsg.header.sourceID      = MODULE_ID_0;

    /* Initialise Heart Beat counter */
    hBeatCounter = 0;

    //Configure and set CAN LED signal as ready
    canLED.setStatus(MCMLED::LED_STATUS_SOLID_2);    //Ready state
}

CANPeriodicData::~CANPeriodicData()
{
    Thread::stop(); //stop thread
    canLED.setStatus(MCMLED::LED_STATUS_OFF);    //shut down LED
}

void CANPeriodicData::setBootloaderMode(lu_bool_t mode)
{
    ModuleHBeatMStr *hBeatPayloadPtr = reinterpret_cast<ModuleHBeatMStr *>(&(hBeatMsg.payload));
    mutexHBeat.lock();
    hBeatPayloadPtr->bootloaderMode = mode;
    mutexHBeat.unlock();
}

void CANPeriodicData::setPowerSaveMode(lu_bool_t mode)
{
    ModuleHBeatMStr *hBeatPayloadPtr = reinterpret_cast<ModuleHBeatMStr *>(&(hBeatMsg.payload));
    mutexHBeat.lock();
    hBeatPayloadPtr->powerSaveMode  = mode;
    mutexHBeat.unlock();
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void CANPeriodicData::threadBody()
{
    const lu_char_t* FTITLE = "CANPeriodicData::threadBody:";
    lu_uint32_t delayIter = HBeatDelayIterations;

    log.info("%s Restart all slave boards...",FTITLE);
    restartBoards();

    /* start timer */
    lu_int32_t res;
    res = cyclicTimer.start();
    if(res < 0)
    {
        log.fatal("Unable to create CAN cyclic timer");
        return;
    }

    while(isRunning() == LU_TRUE)
    {
        /* Wait for timer timeout */
        cyclicTimer.wait();

        if(isInterrupting() == LU_TRUE)
            continue;

        //skip Several (HBeatDelayIterations) HeartBeats for the first time
        if(delayIter > 0)
        {
            log.debug("%s HeartBeat - Waiting for slave boards to start", FTITLE);
            delayIter--;
            continue;
        }

        log.debug("%s HeartBeat - Main loop timeout", FTITLE);

        /* Send time synch */
        tsynch.synch();

        /* Send Heart Beat */
        if(++hBeatCounter >= HBeatTickTimeout)
        {
            hBeatCounter = 0;
            generateHBeat();
        }
        else
        {
            canLED.setStatus(MCMLED::LED_STATUS_OFF);
        }

        /* Generate tick event */
        modules.tickEvent(CyclicTimerUsec/1000);
    }
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


void CANPeriodicData::generateHBeat()
{
    const lu_char_t* FTITLE = "CANPeriodicData::generateHBeat:";
    static MCMLED::LED_STATUS canLEDStatus = MCMLED::LED_STATUS_OFF;
    CAN_ERROR canRetA;
    CAN_ERROR canRetB;
    RAWSmallModuleMessage hBeatMsgCopy; //store a copy to avoid on-the-fly payload modifications

    mutexHBeat.lock();
    hBeatMsgCopy = hBeatMsg;
    mutexHBeat.unlock();

    ModuleHBeatMStr *hBeatPayload = reinterpret_cast<ModuleHBeatMStr *>(hBeatMsgCopy.payload);
    /* Get internal time Counter and increase it */
    hBeatPayload->timeCounter = timeCounter;

    log.debug("%s Generate Heart Beat with bits: PowerSave=%i, Bootloader=%i, TimeCounter=%i.",
                    FTITLE,
                    hBeatPayload->powerSaveMode,
                    hBeatPayload->bootloaderMode,
                    hBeatPayload->timeCounter
                  );

    /* Send heartbeat to socketA */
    if(socketA.isActive() == LU_TRUE)
    {
        canRetA = socketA.write(&hBeatMsgCopy);
        switch(canRetA)
        {
            case CAN_ERROR_NONE:
                //Successful write
                canLEDStatus = MCMLED::LED_STATUS_SOLID_2;  //activity OK
                break;
            case CAN_ERROR_INIT:
                //Socket/Interface not initialised: ignore
                canLEDStatus = MCMLED::LED_STATUS_OFF;  //No activity
                break;
            case CAN_ERROR_DOWN:
                //Interface is down: ignore
                canLEDStatus = MCMLED::LED_STATUS_SOLID_1;  //activity error
                break;
            default:
                canLEDStatus = MCMLED::LED_STATUS_SOLID_1;  //activity error
                break;
        }
    }

    /* Send heartbeat to socketB */
    if(socketB.isActive() == LU_TRUE)
    {
        canRetB = socketB.write(&hBeatMsgCopy);
        switch(canRetB)
        {
            case CAN_ERROR_NONE:
                //Successful write
                break;
            case CAN_ERROR_INIT:
                //Socket/Interface not initialised: ignore
                break;
            case CAN_ERROR_DOWN:
                //Interface is down: ignore
                break;
            default:
                break;
        }
    }

    timeCounter = (timeCounter + 1) % HBEAT_MAXTIMECOUNTER;

    //Update CAN LED status -- bounded to CAN0 only
    canLED.setStatus(canLEDStatus, LU_TRUE);
    return;
}

void CANPeriodicData::restartBoards()
{
    CAN_ERROR canRet;
    RAWSmallModuleMessage restartMsg(sizeof(MDCmdRestartStr)); //store a copy to avoid on-the-fly payload modifications

    /* Prepare Heart Beat broadcast message */
    restartMsg.header.messageType   = MODULE_MSG_TYPE_MD_CMD;
    restartMsg.header.messageID     = MODULE_MSG_ID_MD_CMD_RESTART;
    restartMsg.header.destination   = MODULE_BRD;
    restartMsg.header.destinationID = MODULE_ID_BRD;
    restartMsg.header.source        = MODULE_MCM;
    restartMsg.header.sourceID      = MODULE_ID_0;

    MDCmdRestartStr *RestPayload = reinterpret_cast<MDCmdRestartStr *>(restartMsg.payload);
    RestPayload->type = MD_RESTART_WARM;
    restartMsg.payloadLen = sizeof(MDCmdRestartStr);

    /* Send message socketA */
    canRet = socketA.write(&restartMsg);
    if(canRet != CAN_ERROR_NONE)
        log.error("%s fail to write restart message(CAN ERR: %i)",socketA.getName(),canRet);

    // Can1 is not always active so prevent unnecessary errors
    if (socketB.isActive() == LU_TRUE)
    {
        /* Send message socketB */
        canRet = socketB.write(&restartMsg);
        if(canRet != CAN_ERROR_NONE)
            log.error("%s fail to write restart message(CAN ERR: %i)",socketB.getName(),canRet);
    }
}
/*
 *********************** End of file ******************************************
 */
