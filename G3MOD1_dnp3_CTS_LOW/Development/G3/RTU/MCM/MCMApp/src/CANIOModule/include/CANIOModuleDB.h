/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *      CAN Module database
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_41012C59_D437_42b2_A8F1_64CF53A9FE65__INCLUDED_)
#define EA_41012C59_D437_42b2_A8F1_64CF53A9FE65__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Mutex.h"
#include "GlobalDefs.h"//MG
#include "IIOModule.h"
#include "CANIOModuleFactory.h"
#include "ConfigurationManager.h"
#include "Logger.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */



class CANIOModuleDB
{

public:
    CANIOModuleDB( CANIOModuleFactory* factory,
                   TSynchManager &tsynch,
                   ConfigurationManager &cManager
                 );
    virtual ~CANIOModuleDB();

	/**
	 * \brief Return a module.
	 * 
	 * \param module Module to find
	 * \param moduleID Module ID to find
	 * \param iFace Interface type (if known). If the module is not found a new
	 * module is allocated only If a valid interface type is provided
	 *
	 * \return Pointer to the CAN module. NULL in case of error
	 */
	CANIOModule* getModule( MODULE module,
	                        MODULE_ID moduleID,
	                        COMM_INTERFACE iFace = COMM_INTERFACE_UNKNOWN
	                      );

   /**
     * \brief Get a list of all modules
     *
     * \return List of modules. Empty when error.
     */
    virtual std::vector<IIOModule*> getAllModules();

	/**
	 * \brief Find the curret board and decode the message. If the board is not
	 * found a new board is created
	 *
	 * \param messagePtr Message to decode
	 *
	 * \return Error code
	 */
	IOM_ERROR decode(ModuleMessage* messagePtr, COMM_INTERFACE iFace);

	/**
	 * \brief Forward the tick event to all the available boards
	 *
	 * \param dTime time elapsed from previous tick (in ms)
	 *
	 * \return Error code
	 */
	IOM_ERROR tickEvent(lu_uint32_t dTime);


private:
    static const lu_uint32_t modulesSize = (MODULE_LAST * MODULE_ID_BRD);

    Mutex mutex;
    CANIOModuleFactory *factory;
    TSynchManager &tsynch;
    ConfigurationManager &cManager;
    CANIOModule *modules[MODULE_LAST][MODULE_ID_BRD];
    Logger& log;

    /**
     * \brief Convert a MODULE enum to a database index
     *
     * \param module Module enum to convert
     *
     * \return Database index. -1 in case of invalid/unsupported module
     */
//    static inline CAN_BOARD_IDX Module2ModuleIdx(MODULE module);

};
#endif // !defined(EA_41012C59_D437_42b2_A8F1_64CF53A9FE65__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
