/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN Event Manager
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   01/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_96BA2F30_2264_4bc5_8FC1_1FD253C970FD__INCLUDED_)
#define EA_96BA2F30_2264_4bc5_8FC1_1FD253C970FD__INCLUDED_



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "Thread.h"
#include "CANIOModuleDB.h"
#include "CANFraming.h"
#include "Logger.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class CANEventManager : public Thread
{
public:
    CANEventManager( CANFraming    &socketA    ,
                     CANFraming    &socketB    ,
                     CANIOModuleDB &modules    ,
                     SCHED_TYPE     schedType  ,
                     lu_uint32_t    priority
                   );
    ~CANEventManager();

protected:
    /**
     * \brief Main loop
     */
    virtual void threadBody();

private:
    CANFraming    &socketA;
    CANFraming    &socketB;
    CANIOModuleDB &modules;
    Logger& log;

    void handleEvent(CANFraming &socket);
};
#endif // !defined(EA_96BA2F30_2264_4bc5_8FC1_1FD253C970FD__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
