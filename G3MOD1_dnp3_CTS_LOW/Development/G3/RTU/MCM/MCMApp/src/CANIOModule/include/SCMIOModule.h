/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: SCMIOModule.h 1 Jul 2011 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/CANIOModule/include/SCMIOModule.h $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       SCM Module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   01/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_1540A53B_9125_43f3_9B8F_7984C24A10D9__INCLUDED_)
#define EA_1540A53B_9125_43f3_9B8F_7984C24A10D9__INCLUDED_



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "InputDigitalCANChannel.h"
#include "InputAnalogueCANChannel.h"
#include "SwitchCANChannel.h"
#include "CANIOModule.h"
#include "ICANModuleConfigurationFactory.h"
//#include "ModuleProtocolEnumSCM.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define SCM_AUXILIARY_CH_ENABLED 0

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 * \brief SwitchGear Module
 *
 * This class implements the channel mapping and some other details for the SCM.
 */
class SCMIOModule : public CANIOModule
{
public:
    SCMIOModule( MODULE_ID id,
                 COMM_INTERFACE iFace,
                 TSynchManager &tsynch,
                 ICANModuleConfigurationFactory &moduleFactory
               );

    virtual ~SCMIOModule();

    /**
     * \brief Return a channel reference
     *
     * \param type Channel type
     * \param number Channel number
     *
     * \return Channel pointer. NULL if channel is not present
     */
    virtual IChannel *getChannel(CHANNEL_TYPE type, lu_uint32_t number);

    /**
     * \brief Notify the timer tick is expired
     *
     * This function should be periodically called in order to keep the internal logic
     * updated (internal timers, etc,...)
     *
     * \param dTime Time elapsed from the previous tick (in ms)
     *
     * \return Error code
     */
    virtual IOM_ERROR tickEvent(lu_uint32_t dTime);

    /**
     * \brief Decode a module message  If necessary the module/channels internal state
     * is updated
     *
     * \param message Message to decode
     *
     * \return Error code
     */
    virtual IOM_ERROR decodeMessage(ModuleMessage* message);

    virtual IOM_ERROR configure();

    virtual IOM_ERROR loadConfiguration();

    /**
     * \brief Stop all of this module's channels from updating
     *
     * \param none
     *
     * \return none
     */
    virtual void stop();

    /**
     * \brief Get module configuration
     *
     * \param config Where to get the configuration when success
     *
     * \return Error code
     */
    virtual IOM_ERROR getConfiguration(SCMConfigStr& config)
    {
        if(!configOK)
        {
            return IOM_ERROR_CONFIG;
        }
        config = moduleConfig;
        return IOM_ERROR_NONE;
    }


protected:
    /**
     * \brief Update the status of all the channels
     *
     * \param status LU_TRUE if the module is online
     *
     * \return none
     */
    void setAllChsActive(lu_bool_t status);

private:
    bool configOK;              //Module configuration is valid
    SCMConfigStr moduleConfig;  //Module configuration
    InputDigitalCANChannel BInput[SCM_CH_DINPUT_LAST];
    InputAnalogueCANChannel AInput[SCM_CH_AINPUT_LAST];
    SwitchCANChannel switchOut[SCM_CH_SWOUT_LAST];
    OutputDigitalCANChannel DOutput[SCM_CH_DOUT_LAST];
};

#endif // !defined(EA_1540A53B_9125_43f3_9B8F_7984C24A10D9__INCLUDED_)

/*
 *********************** End of file ******************************************
 */

