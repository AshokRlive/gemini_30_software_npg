/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN Fan test custom channel interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_9E1AF472_29A6_43f2_AFD3_1E2CDB819112__INCLUDED_)
#define EA_9E1AF472_29A6_43f2_AFD3_1E2CDB819112__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "OutputCANChannel.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class FanCANChannel : public OutputCANChannel
{

public:
    FanCANChannel(lu_uint16_t ID = 0, CANIOModule *board = NULL);
    virtual ~FanCANChannel() {};

     /**
      * \brief start a fan test
      *
      * This function blocks until a reply is received or the timeout expires
      *
      * \param duration Test duration (in seconds)
      *
      * \return Error code
      */
     virtual IOM_ERROR fanTest(const lu_uint8_t duration);

};
#endif // !defined(EA_9E1AF472_29A6_43f2_AFD3_1E2CDB819112__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
