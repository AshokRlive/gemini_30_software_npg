/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: AbstractCANIOModule_impl.hpp 22 Aug 2016 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/CANIOModule/include/AbstractCANIOModule_impl.hpp $
 *
 *    FILE TYPE: C++ source implementation (header extension for template)
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Implementation code of the AbstractCANIOModule header. This code can
 *       NOT be done in a cpp file since it comes from a template (using C++03).
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 22 Aug 2016 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   22 Aug 2016   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdexcept>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
template <typename CANConfigStr, typename SwitchChannel>
AbstractCANIOModule<CANConfigStr, SwitchChannel>::AbstractCANIOModule(
                        MODULE moduleType,
                        MODULE_ID id,
                        COMM_INTERFACE iFace,
                        TSynchManager &tsynch,
                        ICANModuleConfigurationFactory &moduleFactory,
                        const lu_uint32_t MAX_DINPUT,
                        const lu_uint32_t MAX_AINPUT,
                        const lu_uint32_t MAX_DOUTPUT,
                        const lu_uint32_t MAX_SWOUT
                        ) :
                            CANIOModule(moduleType, id, iFace, tsynch, moduleFactory),
                            m_MAX_DINPUT(MAX_DINPUT),
                            m_MAX_AINPUT(MAX_AINPUT),
                            m_MAX_DOUTPUT(MAX_DOUTPUT),
                            m_MAX_SWOUT(MAX_SWOUT),
                            m_configOK(false)
{
    lu_uint32_t i;

    /* Full channels initialisation */
    //Binary Input
    for(i = 0; i < m_MAX_DINPUT; i++)
    {
        InputDigitalCANChannel* channel = new InputDigitalCANChannel(i, this);
        m_digInput.push_back(channel);
    }
    //Analogue Input
    for(i = 0; i < m_MAX_AINPUT; i++)
    {
        InputAnalogueCANChannel* channel = new InputAnalogueCANChannel(i, this);
        m_anaInput.push_back(channel);
    }

    //Digital Output
    for(i = 0; i < m_MAX_DOUTPUT; i++)
    {
        OutputDigitalCANChannel* channel = new OutputDigitalCANChannel(i, this);
        m_digOutput.push_back(channel);
    }

    //Switch Output
    for(i = 0; i < m_MAX_SWOUT; i++)
    {
        SwitchChannel* channel = new SwitchChannel(i, this);
        m_swOutput.push_back(channel);
    }

    initStateMachine();
}

template<typename CANConfigStr, typename SwitchChannel>
AbstractCANIOModule<CANConfigStr, SwitchChannel>::~AbstractCANIOModule()
{
    /* Remove all channels */
    for (ItDIChannels it = m_digInput.begin(); it != m_digInput.end(); ++it)
    {
        delete (*it);
    }
    for (ItAIChannels it = m_anaInput.begin(); it != m_anaInput.end(); ++it)
    {
        delete (*it);
    }
    for (ItDOChannels it = m_digOutput.begin(); it != m_digOutput.end(); ++it)
    {
        delete (*it);
    }
    for (ItSWChannels it = m_swOutput.begin(); it != m_swOutput.end(); ++it)
    {
        delete (*it);
    }
}


template<typename CANConfigStr, typename SwitchChannel>
IChannel* AbstractCANIOModule<CANConfigStr, SwitchChannel>::getChannel(
                                            CHANNEL_TYPE type, lu_uint32_t number)
{
    IChannel *channelPtr = NULL;

    try
    {
        switch(type)
        {
            case CHANNEL_TYPE_DINPUT:
                channelPtr = m_digInput.at(number);
            break;

            case CHANNEL_TYPE_AINPUT:
                channelPtr = m_anaInput.at(number);
            break;

            case CHANNEL_TYPE_DOUTPUT:
                channelPtr = m_digOutput.at(number);
            break;

            case CHANNEL_TYPE_SW_OUT:
                channelPtr = m_swOutput.at(number);
            break;

            default:
                throw std::out_of_range("Unsupported CAN channel");
            break;
        }
    }
    catch (const std::out_of_range& e)
    {
        log.error("%s - %s: %s %d:%i.", __FUNCTION__, getName(), e.what(), type, number);
    }
    return channelPtr;
}


template<typename CANConfigStr, typename SwitchChannel>
IOM_ERROR AbstractCANIOModule<CANConfigStr, SwitchChannel>::loadConfiguration()
{
    return doLoadConfiguration();
}


template<typename CANConfigStr, typename SwitchChannel>
void AbstractCANIOModule<CANConfigStr, SwitchChannel>::stop()
{
    for (ItDIChannels it = m_digInput.begin(); it != m_digInput.end(); ++it)
    {
        (*it)->stopUpdate();
    }
    for (ItAIChannels it = m_anaInput.begin(); it != m_anaInput.end(); ++it)
    {
        (*it)->stopUpdate();
    }
    for (ItDOChannels it = m_digOutput.begin(); it != m_digOutput.end(); ++it)
    {
        (*it)->stopUpdate();
    }
    for (ItSWChannels it = m_swOutput.begin(); it != m_swOutput.end(); ++it)
    {
        (*it)->stopUpdate();
    }
}


template<typename CANConfigStr, typename SwitchChannel>
IOM_ERROR AbstractCANIOModule<CANConfigStr, SwitchChannel>::tickEvent(lu_uint32_t dTime)
{
    /* Call parent tick function */
    return CANIOModule::tickEvent(dTime);
}


template<typename CANConfigStr, typename SwitchChannel>
IOM_ERROR AbstractCANIOModule<CANConfigStr, SwitchChannel>::decodeMessage(ModuleMessage* message)
{
    IOM_ERROR ret = IOM_ERROR_NONE;

    if(message == NULL)
    {
        return IOM_ERROR_PARAM;
    }

    /* General message ? */
    ret = CANIOModule::decodeMessage(message);
    if (ret == IOM_ERROR_NOT_SUPPORTED)
    {
        /* No - Try custom decoder */
        switch(message->header.messageType)
        {
            /* Forward the event to the channel */
            case MODULE_MSG_TYPE_EVENT:
                switch(message->header.messageID)
                {
                    case MODULE_MSG_ID_EVENT_DIGITAL:
                        CANIOModule::handleDigEvent(*message);
                        ret = IOM_ERROR_NONE;
                    break;

                    case MODULE_MSG_ID_EVENT_ANALOGUE:
                        CANIOModule::handleAnaEvent(*message);
                        ret = IOM_ERROR_NONE;
                    break;

                    default:
                    break;
                }
            break;

            default:
            break;
        }
    }

    if(ret == IOM_ERROR_NOT_SUPPORTED)
    {
        log.warn("%s - %s: Unsupported message: 0x%02x:0x%02x",
                        __FUNCTION__,
                        getName(),
                        message->header.messageType,
                        message->header.messageID
                       );
    }
    return ret;
}


template<typename CANConfigStr, typename SwitchChannel>
IOM_ERROR AbstractCANIOModule<CANConfigStr, SwitchChannel>::getConfiguration(CANConfigStr& config)
{
    if(!m_configOK)
    {
        return IOM_ERROR_CONFIG;
    }
    config = m_moduleConfig;
    return IOM_ERROR_NONE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
template<typename CANConfigStr, typename SwitchChannel>
void AbstractCANIOModule<CANConfigStr, SwitchChannel>::setAllChsActive(lu_bool_t status)
{
    for (ItDIChannels it = m_digInput.begin(); it != m_digInput.end(); ++it)
    {
        (*it)->setActive(status);
    }
    for (ItAIChannels it = m_anaInput.begin(); it != m_anaInput.end(); ++it)
    {
        (*it)->setActive(status);
    }
    for (ItDOChannels it = m_digOutput.begin(); it != m_digOutput.end(); ++it)
    {
        (*it)->setActive(status);
    }
    for (ItSWChannels it = m_swOutput.begin(); it != m_swOutput.end(); ++it)
    {
        (*it)->setActive(status);
    }
}

template<typename CANConfigStr, typename SwitchChannel>
IOM_ERROR AbstractCANIOModule<CANConfigStr, SwitchChannel>::doLoadConfiguration()
{
    const lu_char_t * ErrMSG = "%s failed to load configuration for %s. Error %i: %s";

    IOM_ERROR err = IOM_ERROR_NONE;
    IOM_ERROR ret = IOM_ERROR_NONE;
    lu_uint32_t ch_id;

    if (configFactory.isConfigured(mid) == LU_FALSE)
    {
        log.warn("%s-was not found in configuration", getName());
        return IOM_ERROR_NOT_FOUND;
    }

    ret = configFactory.getModuleConf<CANConfigStr>(mid.type, mid.id, m_moduleConfig);

    if (ret == IOM_ERROR_NONE)
    {
        m_configOK = true;
    }
    else
    {
        err = ret;
        log.error("%s fail to load DSM configuration. Error %i: %s",
                        getName(), ret, IOM_ERROR_ToSTRING(ret));
    }

    /* ==Load all Digital Input channels== */
    InputDigitalCANChannelConf diConf;
    for (ch_id = 0; ch_id < m_MAX_DINPUT; ch_id++)
    {
        ret = configFactory.getIDigitalConf(mid.type, mid.id, ch_id, diConf);
        if (ret == IOM_ERROR_NONE)
        {
            m_digInput[ch_id]->setConfig(diConf);
        }
        else
        {
            err = ret;
            log.error(ErrMSG, getName(), m_digInput[ch_id]->getName(), err, IOM_ERROR_ToSTRING(err) );
        }
    }

        /* ==Load all Analogue Input== */
        InputAnalogueCANChannelConf aConf;
        for (ch_id = 0; ch_id < m_MAX_AINPUT; ch_id++)
        {
            ret = configFactory.getIAnalogueConf(mid.type, mid.id, ch_id, aConf);
            if (ret == IOM_ERROR_NONE)
            {
                m_anaInput[ch_id]->setConfig(aConf);
            }
            else
            {
                err = ret;
                log.error(ErrMSG, getName(), m_anaInput[ch_id]->getName(), ret, IOM_ERROR_ToSTRING(ret) );
            }
        }

        /* ==Load all Switch Out channels== */
        for (lu_uint32_t ch_id = 0; ch_id < m_MAX_SWOUT; ch_id++)
        {
            ret = loadSwitchConfiguration(ch_id);
            if (ret != IOM_ERROR_NONE)
            {
                err = ret;
                log.error(ErrMSG, getName(), m_swOutput[ch_id]->getName(), ret, IOM_ERROR_ToSTRING(ret) );
            }
        }

        /* ==Load all Digital Output channels== */
        OutputDigitalCANChannelConf doConf;
        for (ch_id = 0; ch_id < m_MAX_DOUTPUT; ch_id++)
        {
            ret = configFactory.getDigitalOutConf(mid.type, mid.id, ch_id, doConf);
            if (ret == IOM_ERROR_NONE)
            {
                m_digOutput[ch_id]->setConfig(doConf);
            }
            else
            {
                err = ret;
                log.error(ErrMSG, getName(), m_digOutput[ch_id]->getName(), err, IOM_ERROR_ToSTRING(err) );
            }
        }
    return err;
}

template<typename CANConfigStr, typename SwitchChannel>
IOM_ERROR AbstractCANIOModule<CANConfigStr, SwitchChannel>::sendAllConfiguration(
                                        IIOModule::sendReplyStr& message)
{
    const lu_char_t* ERRMESSAGE = "Error Sending configuration to slave module";
    bool hasFailure = false;
    IOM_ERROR ret;
    PayloadRAW payload;

    log.info("%s: %s sends configuration", __AT__, getName());

    /* Send message */
    ret = this->sendMessage(&message);
    if(ret == IOM_ERROR_NONE)
    {
        /* Get payload */
        (message.rMessagePtr)->getPayload(&payload);
        if(payload.payloadLen == MODULE_MESSAGE_SIZE(ModStatusReplyStr))
        {
            ModStatusReplyStr *replyPtr = reinterpret_cast<ModStatusReplyStr*>(payload.payloadPtr);
            if(replyPtr->status != REPLY_STATUS_OKAY)
            {
                log.error("%s %s %s. Reply status: %i",
                            __AT__,
                            ERRMESSAGE,
                            getName(),
                            replyPtr->status
                          );
                ret = IOM_ERROR_REPLY;
            }
        }
        else
        {
            /* Wrong payload */
            log.error("%s %s %s - reply has invalid size: %i",
                            __AT__,
                            ERRMESSAGE,
                            getName(),
                            payload.payloadLen
                          );
            ret = IOM_ERROR_REPLY;
        }
        /* Release reply message */
        delete (message.rMessagePtr);
    }

    if(ret != IOM_ERROR_NONE)
    {
        hasFailure = true;
        log.error("%s Error configuring %s. Error %i: %s"
                    "Unable to set Open and Close operation LED colours!",
                    __AT__,
                    getName(),
                    ret, IOM_ERROR_ToSTRING(ret)
                  );
    }

    hasFailure |= sendAllChannelsConfiguration();

    return (hasFailure)? IOM_ERROR_CONFIG : IOM_ERROR_NONE;
}


template<typename CANConfigStr, typename SwitchChannel>
bool AbstractCANIOModule<CANConfigStr, SwitchChannel>::sendAllChannelsConfiguration()
{
    bool failed = false;
    lu_uint32_t i;
    IOM_ERROR res;

    /* ==Configure all Digital Input== */
    for(i = 0; i < m_MAX_DINPUT; i++)
    {
        res = m_digInput[i]->sendConfiguration();
        failed |= checkChannelCfgFailure(res, CHANNEL_TYPE_DINPUT, i);
    }

    /* ==Configure all Analogue Input== */
    for(i = 0; i < m_MAX_AINPUT; i++)
    {
        res = m_anaInput[i]->sendConfiguration();
        failed |= checkChannelCfgFailure(res, CHANNEL_TYPE_AINPUT, i);
    }

    /* ==Configure all Digital Out channels== */
    for (i = 0; i < m_MAX_DOUTPUT; i++)
    {
        res = m_digOutput[i]->sendConfiguration();
        failed |= checkChannelCfgFailure(res, CHANNEL_TYPE_DOUTPUT, i);
    }

    /* ==Configure all Switch Out channels== */
    for(i = 0; i < m_MAX_SWOUT; i++)
    {
        res = m_swOutput[i]->sendConfiguration();
        failed |= checkChannelCfgFailure(res, CHANNEL_TYPE_SW_OUT, i);
    }

    return !failed;
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
template<typename CANConfigStr, typename SwitchChannel>
bool AbstractCANIOModule<CANConfigStr, SwitchChannel>::checkChannelCfgFailure(
                                                        const IOM_ERROR errorCode,
                                                        const CHANNEL_TYPE type,
                                                        const lu_uint32_t index)
{
    if (errorCode != IOM_ERROR_NONE)
    {
        log.error("%s Failed to configure %s channel: %i err: %i",
                   getName(), CHANNEL_TYPE_ToSTRING(type), index, errorCode);
        return true;
    }
    return false;
}


/*
 *********************** End of file ******************************************
 */
