/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       IOM module - Input/Output Module.
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   20/09/12      pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#if !defined(EA_7F8FF404_C329_4537_A2F5_2CF29274774C__INCLUDED_)
#define EA_7F8FF404_C329_4537_A2F5_2CF29274774C__INCLUDED_


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "InputDigitalCANChannel.h"
#include "InputAnalogueCANChannel.h"
#include "SwitchCANChannel.h"
#include "OutputDigitalCANChannel.h"
#include "CANIOModule.h"
#include "ICANModuleConfigurationFactory.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief This class controls an IOM module - Input/Output Module.
 *
 */
class IOMIOModule : public CANIOModule
{

public:
    /**
     * \brief Custom constructor
     *
     * \param id Module ID number
     * \param iFace Communication interface
     * \param tsynch Synchronisation manager
     * \param moduleFactory Module generator
     */
	IOMIOModule(MODULE_ID id,
                COMM_INTERFACE iFace,
                TSynchManager& tsynch,
                ICANModuleConfigurationFactory& moduleFactory
                );
	virtual ~IOMIOModule();

	/**
	 * \brief Return a channel reference
	 * 
	 * \param type Channel type
	 * \param number Channel number
	 * 
	 * \return Channel pointer. NULL if channel is not present
	 */
	virtual IChannel * getChannel(CHANNEL_TYPE type, lu_uint32_t number);
	/**
	 * \brief Notify the timer tick is expired
	 * 
	 * This function should be periodically called in order to keep the internal logic
	 * updated (internal timers, etc,...)
	 * 
	 * \param dTime Time elapsed from the previous tick (in ms)
	 * 
	 * \return Error code
	 */
	virtual IOM_ERROR tickEvent(lu_uint32_t dTime);
	/**
	 * \brief Decode a module message
	 * 
	 * If necessary the module/channels internal state is updated
	 * 
	 * \param message Message to decode
	 * 
	 * \return Error code
	 */
	virtual IOM_ERROR decodeMessage(ModuleMessage* message);

	virtual IOM_ERROR configure();

	virtual IOM_ERROR loadConfiguration();

    /**
     * \brief Stop all of this module's channels from updating
     *
     * \param none
     *
     * \return none
     */
    virtual void stop();

protected:
     /**
      * \brief Update the status of all the channels
      *
      * \param status LU_TRUE if the module is online
      *
      * \return none
      */
     void setAllChsActive(lu_bool_t status);

private:
	InputDigitalCANChannel BInput[IOM_CH_DINPUT_LAST];
	InputAnalogueCANChannel AInput[IOM_CH_AINPUT_LAST];
	OutputDigitalCANChannel DOutput[IOM_CH_DOUT_LAST];
};

#endif // !defined(EA_7F8FF404_C329_4537_A2F5_2CF29274774C__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
