/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN tiem synch manager implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   01/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <cstring>
#include <cstdlib>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "TSynchManager.h"
#include "LockingMutex.h"
#include "timeOperations.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

MODULE_ERROR TSynchModuleMessage::getPayload(PayloadRAW *payloadPtr)
{
    if(payloadPtr == NULL)
    {
        return MODULE_ERROR_PARAM;
    }

    payloadPtr->payloadPtr = (lu_uint8_t*)(&this->moduleTsynch);
    payloadPtr->payloadLen = sizeof(moduleTsynch);

    return MODULE_ERROR_NONE;
}

TSynchManager::TSynchManager(CANFraming& socketA, CANFraming& socketB) :
                                                  mutex(LU_TRUE),
                                                  socketA(socketA),
                                                  socketB(socketB),
                                                  timeSlotCounter(0),
                                                  log(Logger::getLogger(SUBSYSTEM_ID_CANTSYNCH))
{
    /* Prepare the time synch message header */
    TSynchMessage.header.messageType   = MODULE_MSG_TYPE_HPRIO;
    TSynchMessage.header.messageID     = MODULE_MSG_ID_HPRIO_TSYNCH;
    TSynchMessage.header.destination   = MODULE_BRD;
    TSynchMessage.header.destinationID = MODULE_ID_BRD;
    TSynchMessage.header.source        = MODULE_MCM;
    TSynchMessage.header.sourceID      = MODULE_ID_0;
}

void TSynchManager::synch()
{
    log.debug("TSynchManager::synch: Generate Time Synch. Slot: %i", timeSlotCounter);

    /* enter critical region */
    {
        LockingMutex lMutex(mutex);

        /* save current time */
        TimeManager::getInstance().getTime(timeDB[timeSlotCounter]);

        /* Send synch message */
        TSynchMessage.moduleTsynch.timeSlot = timeSlotCounter;
        socketA.write(&TSynchMessage);
        socketB.write(&TSynchMessage);

        /* Update time slot */
        timeSlotCounter = (timeSlotCounter + 1) % MODULE_TIME_MAX_TIMESLOT;
    }
}

void TSynchManager::convertTime( ModuleTimeStr* moduleTime,
                                 TimeManager::TimeStr& resulTime
                               )
{
    const lu_char_t *FTITLE = "TSynchManager::convertTime:";
    TimeManager& timeManager = TimeManager::getInstance();
    timeManager.getTime(resulTime);   //current time by default

    /* Check source validity (module reference) */
    if(moduleTime == NULL)
    {
        //if no source, time is considered not synchronised
        resulTime.synchronised = LU_FALSE;
        return; //return Current time
    }

    /* check if the module relative time is valid */
    if( (moduleTime->slot >= MODULE_TIME_MAX_TIMESLOT) ||
        (moduleTime->time == MODULE_TIME_INVALID)
      )
    {
        log.debug("%s Invalid module relative time. Slot: %i - time: %i",
                        FTITLE,
                        moduleTime->slot, moduleTime->time
                      );
        //if no valid source, time is considered not synchronised
        resulTime.synchronised = LU_FALSE;
        return; //return Current time
    }

    /* check if the referred slot is not valid yet */
    if(timespec_compare(&(timeDB[moduleTime->slot].time), &TIMESPEC_ZERO) == LU_TRUE)
    {
        log.debug("%s Invalid time stamp in module relative time. Slot: %i - time: %i",
                        FTITLE,
                        moduleTime->slot, moduleTime->time
                      );
        //if no valid source, time is considered not synchronised
        resulTime.synchronised = LU_FALSE;
        return; //return Current time
    }

    /* Valid module relative time. */
    TimeManager::TimeStr sysSyncTime;   //stores last System synchronisation time
    lu_int32_t slotRel_ns;              //amount of ns from slot sync
    struct timespec timeRef;            //stores time to convert, but from slot sync

    timeManager.getSyncStatus(sysSyncTime);
    LockingMutex lMutex(mutex);
    timeRef = timeDB[moduleTime->slot].relatime;
    /* Convert CAN time in fractions of us to relative time in ns */
    slotRel_ns = USEC_TO_NSEC(moduleTime->time * MODULE_TIME_RESOLUTION_US);
    timeRef.tv_nsec += slotRel_ns;
    timespec_normalise(&timeRef);
    /*
     * CAUTION: we calculate time stamp from when that synchronisation was
     * done, but it could happen that the system date have changed in the
     * meantime. To ensure correct time stamping of the module, we need
     * to compare last synchronisation monotonic time stamp with the one
     * of that synchronisation. If we determine that that event time
     * happened after the time change, the time stamp must have be referred
     * from the system time that changed.
     */
    lu_uint64_t msSinceSlot = timespec_elapsed_ms(&(timeDB[moduleTime->slot].relatime), &(sysSyncTime.relatime));
    lu_uint64_t msSinceRef = timespec_elapsed_ms(&timeRef, &(sysSyncTime.relatime));
    if( (msSinceSlot > 0) && (msSinceRef == 0) )
    {
        //System time changed between synch slot and event: based on System time
        resulTime.relatime = timeRef;
        struct timespec elapFromSys;    //time elapsed from System synch to event
        timespec_elapsed(&(sysSyncTime.relatime), &timeRef, &elapFromSys);
        timespec_add(&(sysSyncTime.time), &elapFromSys, &(resulTime.time));
        resulTime.synchronised = sysSyncTime.synchronised;
    }
    else
    {
        //Rest of the cases always takes time based from slot timestamp
        resulTime.time = timeDB[moduleTime->slot].time;
        resulTime.time.tv_nsec += slotRel_ns;
        timespec_normalise(&(resulTime.time));
        resulTime.relatime = timeDB[moduleTime->slot].relatime;
        resulTime.relatime.tv_nsec += slotRel_ns;
        timespec_normalise(&(resulTime.relatime));
        resulTime.synchronised = timeDB[moduleTime->slot].synchronised;
    }
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
