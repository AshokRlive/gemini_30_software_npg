/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: DSMIOModule.cpp 7 May 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/CANIOModule/src/DSMIOModule.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       DSM Module implementation - Dual Switch Module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 7 May 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   7 May 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "DSMIOModule.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
DSMIOModule::DSMIOModule(MODULE_ID id                                 ,
                         COMM_INTERFACE iFace                         ,
                         TSynchManager &tsynch                        ,
                         ICANModuleConfigurationFactory &moduleFactory
                       ) :
                 CANIOModule(MODULE_DSM, id, iFace, tsynch, moduleFactory),
                 configOK(false),
                 reserved(false)
{
    lu_uint32_t i;

    /* Full channels initialisation: Binary Input */
    for(i = 0; i < DSM_CH_DINPUT_LAST; i++)
    {
        BInput[i].setBoard(this);
        BInput[i].setID(i);
    }

    /* Full channels initialisation: Analogue Input */
    for(i = 0; i < DSM_CH_AINPUT_LAST; i++)
    {
        AInput[i].setBoard(this);
        AInput[i].setID(i);
    }

    /* Full channels initialisation: Switch Output */
    for(i = 0; i < DSM_CH_SWOUT_LAST; i++)
    {
        switchOut[i].setBoard(this);
        switchOut[i].setID(i);
    }

    /* Full channels initialisation: Digital Output */
    for(i = 0; i < DSM_CH_DOUT_LAST; i++)
    {
        DOutput[i].setBoard(this);
        DOutput[i].setID(i);
    }

    initStateMachine();
}

DSMIOModule::~DSMIOModule()
{}


IChannel* DSMIOModule::getChannel(CHANNEL_TYPE type, lu_uint32_t number)
{
    IChannel *channelPtr = NULL;

    switch(type)
    {
        case CHANNEL_TYPE_DINPUT:
            if(number < DSM_CH_DINPUT_LAST)
            {
                channelPtr = &BInput[number];
            }
        break;

        case CHANNEL_TYPE_AINPUT:
            if(number < DSM_CH_AINPUT_LAST)
            {
                channelPtr = &AInput[number];
            }
        break;

        case CHANNEL_TYPE_SW_OUT:
            if(number < DSM_CH_SWOUT_LAST)
            {
                channelPtr = &switchOut[number];
            }
        break;

        case CHANNEL_TYPE_DOUTPUT:
            if(number < DSM_CH_DOUT_LAST)
            {
                channelPtr = &DOutput[number];
            }
        break;

        default:
            log.error("DSMIOModule::getChannel: "
                            "Unsupported CAN channel: %i. Function: %s",
                            type, __FUNCTION__
                          );
        break;
    }

    return channelPtr;
}


IOM_ERROR DSMIOModule::tickEvent(lu_uint32_t dTime)
{
    /* Call parent tick function */
    return CANIOModule::tickEvent(dTime);
}


IOM_ERROR DSMIOModule::decodeMessage(ModuleMessage* message)
{
    IOM_ERROR ret = IOM_ERROR_NONE;

    if(message == NULL)
    {
        return IOM_ERROR_PARAM;
    }

    /* General message ? */
    ret = CANIOModule::decodeMessage(message);
    if (ret == IOM_ERROR_NOT_SUPPORTED)
    {
        /* No - Try custom decoder */
        switch(message->header.messageType)
        {
            /* Forward the event to the channel */
            case MODULE_MSG_TYPE_EVENT:
                switch(message->header.messageID)
                {
                    case MODULE_MSG_ID_EVENT_DIGITAL:
                        CANIOModule::handleDEvent(message, BInput, DSM_CH_DINPUT_LAST);
                        ret = IOM_ERROR_NONE;
                    break;

                    case MODULE_MSG_ID_EVENT_ANALOGUE:
                        CANIOModule::handleAEvent(message, AInput, DSM_CH_AINPUT_LAST);
                        ret = IOM_ERROR_NONE;
                    break;

                    default:
                        ret = IOM_ERROR_NOT_SUPPORTED;
                    break;
                }
            break;

            default:
                ret = IOM_ERROR_NOT_SUPPORTED;
            break;
        }
    }

    if(ret == IOM_ERROR_NOT_SUPPORTED)
    {
        log.warn("DSMIOModule::decodeMessage: "
                        "%s Unsupported message: 0x%02x:0x%02x",
                        getName(),
                        message->header.messageType,
                        message->header.messageID
                       );
    }
    return ret;
}


IOM_ERROR DSMIOModule::loadConfiguration()
{
    const lu_char_t * ErrMSG = "%s failed to load configuration for %s. Error %i: %s";

    lu_uint32_t ch_id;
    IOM_ERROR err = IOM_ERROR_NONE;
    IOM_ERROR ret;

    if (configFactory.isConfigured(mid) == LU_FALSE)
    {
        log.warn("%s-was not found in configuration", getName());
        return IOM_ERROR_NOT_FOUND;
    }

    ret = configFactory.getModuleConf<DSMConfigStr>(mid.type, mid.id, moduleConfig);

    if (ret == IOM_ERROR_NONE)
    {
        log.debug("%s Load configured switch colour = %i (%s).", getName(),
                        moduleConfig.greenLEDPolarity,  //Green polarity = 1
                        (moduleConfig.greenLEDPolarity == 1) ? "green" : "red");
        configOK = true;
    }
    else
    {
        err = ret;
        log.error("%s fail to load DSM configuration. Error %i: %s",
                        getName(), ret, IOM_ERROR_ToSTRING(ret));
    }

    /* ==Load all Digital Input== */
    InputDigitalCANChannelConf dConf;
    for (ch_id = 0; ch_id < DSM_CH_DINPUT_LAST; ch_id++)
    {
        ret = configFactory.getIDigitalConf(mid.type, mid.id, ch_id, dConf);
        if (ret == IOM_ERROR_NONE)
        {
            BInput[ch_id].setConfig(dConf);
        }
        else
        {
            err = ret;
            log.error(ErrMSG, getName(), BInput[ch_id].getName(), ret, IOM_ERROR_ToSTRING(ret) );
        }
    }

    /* ==Load all Analogue Input== */
    InputAnalogueCANChannelConf aConf;
    for (ch_id = 0; ch_id < DSM_CH_AINPUT_LAST; ch_id++)
    {
        ret = configFactory.getIAnalogueConf(mid.type, mid.id, ch_id, aConf);
        if (ret == IOM_ERROR_NONE)
        {
            AInput[ch_id].setConfig(aConf);
        }
        else
        {
            err = ret;
            log.error(ErrMSG, getName(), AInput[ch_id].getName(), ret, IOM_ERROR_ToSTRING(ret) );
        }
    }

    /* ==Load all Switch Out channels== */
    SwitchCANChannelDSM::SwitchCANChannelDSMConf swConf;
    for (ch_id = 0; ch_id < DSM_CH_SWOUT_LAST; ch_id++)
    {
        ret = configFactory.getDSMSwitchOutConf(mid.type, mid.id, ch_id, swConf);
        if (ret == IOM_ERROR_NONE)
        {
            switchOut[ch_id].setConfig(swConf);
        }
        else
        {
            err = ret;
            log.error(ErrMSG, getName(), switchOut[ch_id].getName(), ret, IOM_ERROR_ToSTRING(ret) );
        }
    }

    /* Load all Digital Output channels */
    OutputDigitalCANChannelConf doConf;
    for (ch_id = 0; ch_id < DSM_CH_DOUT_LAST; ch_id++)
    {
        ret = configFactory.getDigitalOutConf(mid.type, mid.id, ch_id, doConf);
        if (ret == IOM_ERROR_NONE)
            DOutput[ch_id].setConfig(doConf);
        else
        {
            err = ret;
            log.error(ErrMSG, getName(), DOutput[ch_id].getName(), err, IOM_ERROR_ToSTRING(err) );
        }
    }
    return err;
}


IOM_ERROR DSMIOModule::configure()
{
    const lu_char_t* FTITLE = "DSMIOModule::configure:";

    lu_bool_t hasFailure = LU_FALSE;
    lu_uint32_t i;
    IOM_ERROR ret;
    log.info("%s %s-send configuration", FTITLE, getName());

    /* ==General configuration== */
    /* Prepare message */
    DSMConfigStr messagePayload;
    messagePayload.greenLEDPolarity = moduleConfig.greenLEDPolarity;
    messagePayload.polarityA = moduleConfig.polarityA;
    messagePayload.polarityB = moduleConfig.polarityB;
    messagePayload.supplyEnable24V = moduleConfig.supplyEnable24V;
    messagePayload.doNotCheckSwitchPositionA = moduleConfig.doNotCheckSwitchPositionA;
    messagePayload.doNotCheckSwitchPositionB = moduleConfig.doNotCheckSwitchPositionB;
    PayloadRAW payload;
    payload.payloadPtr = reinterpret_cast<lu_uint8_t *>(&messagePayload);
    payload.payloadLen = sizeof(DSMConfigStr);
    IIOModule::sendReplyStr message;
    message.messageType     = MODULE_MSG_TYPE_CFG;
    message.messageID       = MODULE_MSG_ID_CFG_DSM_C;
    message.messagePtr      = &payload;
    message.rMessageType    = MODULE_MSG_TYPE_CFG;
    message.rMessageID      = MODULE_MSG_ID_CFG_DSM_R;
    message.timeout = ms_to_timeval(200);     //ms timeout for CAN message reply

    /* Send message */
    ret = this->sendMessage(&message);
    if(ret == IOM_ERROR_NONE)
    {
        /* Get payload */
        (message.rMessagePtr)->getPayload(&payload);
        if(payload.payloadLen == MODULE_MESSAGE_SIZE(ModStatusReplyStr))
        {
            ModStatusReplyStr *replyPtr = reinterpret_cast<ModStatusReplyStr*>(payload.payloadPtr);
            if(replyPtr->status != REPLY_STATUS_OKAY)
            {
                log.error("%s Error Sending configuration to slave module %s. Reply status: %i",
                            FTITLE,
                            getName(),
                            replyPtr->status
                          );
                ret = IOM_ERROR_REPLY;
            }
        }
        else
        {
            /* Wrong payload */
            log.error("%s Error Sending configuration to slave module %s - reply has invalid size: %i",
                            FTITLE,
                            getName(),
                            payload.payloadLen
                          );
            ret = IOM_ERROR_REPLY;
        }
        /* Release reply message */
        delete (message.rMessagePtr);
    }

    if(ret != IOM_ERROR_NONE)
    {
        hasFailure = LU_TRUE;
        log.error("%s Error configuring %s. Error %i: %s"
                    "Unable to set Open and Close operation LED colours!",
                    FTITLE,
                    getName(),
                    ret, IOM_ERROR_ToSTRING(ret)
                  );
    }

    /* ==Configure all Digital Input== */
    for(i = 0; i < DSM_CH_DINPUT_LAST; i++)
    {
        ret = BInput[i].sendConfiguration();
        LOG_CH_CONFIG_FAILURE(ret,"DInput");
    }

    /* ==Configure all Analogue Input== */
    for(i = 0; i < DSM_CH_AINPUT_LAST; i++)
    {
        ret = AInput[i].sendConfiguration();
        LOG_CH_CONFIG_FAILURE(ret,"AInput");
    }

    /* ==Configure all Switch Out channels== */
    for(i = 0; i < DSM_CH_SWOUT_LAST; i++)
    {
        ret = switchOut[i].sendConfiguration();
        LOG_CH_CONFIG_FAILURE(ret,"SWOutput");
    }

    /* Configure all Digital Output channels */
    for(i = 0; i < DSM_CH_DOUT_LAST; i++)
    {
        ret = DOutput[i].sendConfiguration();
        LOG_CH_CONFIG_FAILURE(ret,"DOutput");
    }

    return (hasFailure == LU_TRUE)? IOM_ERROR_CONFIG:IOM_ERROR_NONE;
}


void DSMIOModule::stop()
{
    lu_int32_t i;

    /* Stop all Binary Input channels */
    for(i = 0; i < DSM_CH_DINPUT_LAST; i++)
    {
         BInput[i].stopUpdate();
    }

    /* Stop all Analogue Input channels */
    for(i = 0; i < DSM_CH_AINPUT_LAST; i++)
    {
        AInput[i].stopUpdate();
    }

    /* Stop all Switch Output channels */
    for(i = 0; i < DSM_CH_SWOUT_LAST; i++)
    {
        switchOut[i].stopUpdate();
    }

    /* Stop all Output channels */
    for(i = 0; i < DSM_CH_DOUT_LAST; i++)
    {
        DOutput[i].stopUpdate();
    }
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
void DSMIOModule::setAllChsActive(lu_bool_t status)
{
    lu_uint32_t i;

    /* Binary Input */
    for(i = 0; i < DSM_CH_DINPUT_LAST; i++)
    {
         BInput[i].setActive(status);
    }

    /* Analogue Input */
    for(i = 0; i < DSM_CH_AINPUT_LAST; i++)
    {
        AInput[i].setActive(status);
    }

    /* Switch Output */
    for(i = 0; i < DSM_CH_SWOUT_LAST; i++)
    {
        switchOut[i].setActive(status);
    }

    /* Digital Output */
    for(i = 0; i < DSM_CH_DOUT_LAST; i++)
    {
        DOutput[i].setActive(status);
    }
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
