/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "HMIIOModule.h"
#include "SysAlarm/SysAlarmSystemEnum.h"
#include "SysAlarm/SysAlarmMCMEnum.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define HMI_DETECTED_VALUE 0

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

HMIIOModule::HMIIOModule( MODULE_ID id,
                         COMM_INTERFACE iFace,
                         TSynchManager &tsynch,
                         ICANModuleConfigurationFactory &moduleFactory) :
                       CANIOModule(MODULE_HMI, id, iFace, tsynch, moduleFactory)
{
     lu_uint32_t i;

     /* Set channel ID and module Analogue Input */
     for(i = 0; i < HMI_CH_AINPUT_LAST; i++)
     {
         AInput[i].setBoard(this);
         AInput[i].setID(i);
     }

     /* Initialise HMI channels */
     m_hmiChnl.setBoard(this);
     m_hmiChnl.setID(0);
     mp_hmiDetectChnl = new MCMBIChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_DINPUT, MCM_CH_DINPUT_HMI_DETECT),   MCMInputGPIO_HMI_DETECT);

     initStateMachine();
}



HMIIOModule::~HMIIOModule()
{
    delete mp_hmiDetectChnl;
    mp_hmiDetectChnl = NULL;
}


IChannel *HMIIOModule::getChannel(CHANNEL_TYPE type, lu_uint32_t number)
{
    const lu_char_t* FTITLE = "HMIIOModule::getChannel:";
    IChannel *channelPtr = NULL;

    switch(type)
    {
        case CHANNEL_TYPE_AINPUT:
            if(number < HMI_CH_AINPUT_LAST)
            {
                channelPtr = &AInput[number];
            }
        break;

        case CHANNEL_TYPE_HMI:
            if(number == 0)
            {
                channelPtr = &m_hmiChnl;
            }
        break;

        default:
            log.error("%s Unsupported CAN channel %i for %s.",
                        FTITLE, type, getName()
                      );
        break;
    }

    return channelPtr;
}


IOM_ERROR HMIIOModule::tickEvent(lu_uint32_t dTime)
{
    TimeManager::TimeStr timestamp;

    /* Update HMIDetect channel*/
    mp_hmiDetectChnl->update(timestamp,LU_TRUE);

    /* Call parent tick function */
    return CANIOModule::tickEvent(dTime);
}


IOM_ERROR HMIIOModule::decodeMessage(ModuleMessage *message)
{
    IOM_ERROR ret = IOM_ERROR_NONE;

    if(message == NULL)
    {
        return IOM_ERROR_PARAM;
    }

    /* General message ? */
    ret = CANIOModule::decodeMessage(message);
    if (ret == IOM_ERROR_NOT_SUPPORTED)
    {
        /* No - Try custom decoder */
        switch(message->header.messageType)
        {
            /* Forward the event to the channel */
            case MODULE_MSG_TYPE_EVENT:
                switch(message->header.messageID)
                {
                    case MODULE_MSG_ID_EVENT_ANALOGUE:
                        CANIOModule::handleAEvent(message, AInput, HMI_CH_AINPUT_LAST);
                        ret = IOM_ERROR_NONE;
                    break;

                    default:
                        ret = IOM_ERROR_NOT_SUPPORTED;
                    break;
                }
            break;

            case MODULE_MSG_TYPE_HMI_EVENT:
                /* Forward event to the HMI channel */
                m_hmiChnl.decode(message);
                ret = IOM_ERROR_NONE;
            break;

            default:
                ret = IOM_ERROR_NOT_SUPPORTED;
            break;
        }
    }

    if(ret == IOM_ERROR_NOT_SUPPORTED)
    {
        log.warn("HMIIOModule::decodeMessage: "
                        "CAN module %s -- Unsupported message: 0x%02x:0x%02x",
                        getName(),
                        message->header.messageType,
                        message->header.messageID
                       );
    }
    return ret;
}

IOM_ERROR HMIIOModule::loadConfiguration()
{

    lu_uint32_t ch_id;
    IOM_ERROR err = IOM_ERROR_NONE;
    IOM_ERROR ret;

    if (configFactory.isConfigured(mid) == LU_FALSE)
    {
        log.warn("%s-was not found in configuration", getName());
        return IOM_ERROR_NOT_FOUND;
    }

    /* Configure all Analogue Input */
    InputAnalogueCANChannelConf conf;
    for(ch_id = 0; ch_id < HMI_CH_AINPUT_LAST; ch_id++)
    {

        ret =  configFactory.getIAnalogueConf(mid.type, mid.id, ch_id,  conf);
        if(ret == IOM_ERROR_NONE)
        {
           AInput[ch_id].setConfig(conf);
        }
        else
        {
           err = ret;
           log.error("%s fail to load configuration for %s. Error %i: %s",
                       getName(), AInput[ch_id].getName(),
                       ret, IOM_ERROR_ToSTRING(ret)
                       );
        }
    }

    HMIConfigStr conf_hmi;
    ret = configFactory.getHMIConf(mid.type, mid.id, 0,  conf_hmi);
    if(ret == IOM_ERROR_NONE)
    {
        m_hmiChnl.setConfig(conf_hmi);
    }
    else
    {
        err = ret;
        log.error("%s fail to load HMI configuration. Error %i: %s",
                    getName(), ret, IOM_ERROR_ToSTRING(ret)
                    );
    }

    return err;
}

IOM_ERROR HMIIOModule::configure()
{
    IOM_ERROR ret;
    lu_uint32_t i;
    lu_bool_t hasFailure = LU_FALSE;


    /* Configure all Analogue Input */
    for(i = 0; i < HMI_CH_AINPUT_LAST; i++)
    {
        ret = AInput[i].sendConfiguration();
        LOG_CH_CONFIG_FAILURE(ret,"AInput");
    }

    /* Configure HMI channels */
    ret = m_hmiChnl.sendConfiguration();
    LOG_CH_CONFIG_FAILURE(ret,"HMI");

    return (hasFailure == LU_TRUE)? IOM_ERROR_CONFIG:IOM_ERROR_NONE;;
}

void HMIIOModule::stop()
{
    lu_int32_t i;

    /* Set channel ID and module Analogue Input */
    for(i = 0; i < HMI_CH_AINPUT_LAST; i++)
    {
        AInput[i].stopUpdate();
    }
}

IOM_ERROR HMIIOModule::getInfo(IOModuleInfoStr& minfo)
{
    IOM_ERROR ret;

    ret = CANIOModule::getInfo(minfo);

    /**
     * Change HMI status to be present if it detected on GPIO.
     */
    IODataUint8 curData;
    IChannel::ValueStr value(curData);
    ret = mp_hmiDetectChnl->read(value);
    if(ret == IOM_ERROR_NONE)
    {
        if((lu_uint8_t)(curData) == HMI_DETECTED_VALUE)
        {
            minfo.status.present = LU_TRUE;
        }
    }

    return ret;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

//TODO discuss how to udpate online state.
void HMIIOModule::setAllChsActive(lu_bool_t status)
{
    lu_uint32_t i;

    /* Analogue Input */
    for(i = 0; i < HMI_CH_AINPUT_LAST; i++)
    {
        AInput[i].setActive(status);
    }

    /* Update HMI channel online state */
    m_hmiChnl.setActive(status);
}


void HMIIOModule::issueAlarm( const lu_bool_t issue,
                              const SYS_ALARM_SUBSYSTEM subSystem,
                              const lu_uint32_t alarmCode)
{
    /* Ignore specific alarms for HMI */
    switch(subSystem)
    {
        case SYS_ALARM_SUBSYSTEM_SYSTEM:
            switch(alarmCode)
            {
                //Ignored alarms:
                case SYSALC_SYSTEM_MODULE_REGISTRATION:
                case SYSALC_SYSTEM_SLAVE_MODULE:
                    return;
                default:
                    break;
            }
            break;
        case SYS_ALARM_SUBSYSTEM_MCM:
            switch(alarmCode)
            {
                //Ignored alarms:
                case SYSALC_MCM_MODULE_SERIAL:
                case SYSALC_MCM_MODULE_MISSING:
                case SYSALC_MCM_MODULE_REMOVED:
                case SYSALC_MCM_MODULE_UNREGISTERED:
                    return;
                default:
                    break;
            }
            break;
        default:
            break;
    }
    //When valid alarm for HMI, issue the alarm as usual
    CANIOModule::issueAlarm(issue, subSystem, alarmCode);
}
/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void HMIIOModule::stateChanged(FSM_STATE oldState, FSM_STATE newState)
{
    LU_UNUSED(oldState);

    /* Update HMI channel online state*/
    if(newState == FSM_STATE_ACTIVE)
    {
        m_hmiChnl.setActive(LU_TRUE);
    }
    else
    {
        m_hmiChnl.setActive(LU_FALSE);
    }
}



/*
 *********************** End of file ******************************************
 */
