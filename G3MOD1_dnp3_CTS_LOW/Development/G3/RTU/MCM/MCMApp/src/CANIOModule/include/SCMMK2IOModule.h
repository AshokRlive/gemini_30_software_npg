/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: SCMMK2IOModule.h 11 Jul 2016 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/CANIOModule/include/SCMMK2IOModule.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 11 Jul 2016 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11 Jul 2016   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef SCMMK2IOMODULE_H__INCLUDED
#define SCMMK2IOMODULE_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "GlobalDefs.h"
#include "lu_types.h"
#include "AbstractCANIOModule.h"


//#include "InputDigitalCANChannel.h"
//#include "InputAnalogueCANChannel.h"
//#include "SwitchCANChannel.h"
//#include "CANIOModule.h"
//#include "ICANModuleConfigurationFactory.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
#if 0
/*!
 * \brief SCM MK2 Direct Drive CAN module object
 *
 * Object for handling functionality of SCM MK2 CAN Module
 */
class SCMMK2IOModule : public AbstractCANIOModule<SCMMK2ConfigStr, SwitchCANChannelSCM2>
{
public:
    SCMMK2IOModule( MODULE_ID id,
                    COMM_INTERFACE iFace,
                    TSynchManager& tsynch,
                    ICANModuleConfigurationFactory& moduleFactory
                    ) :
                        AbstractCANIOModule<SCMMK2ConfigStr, SwitchCANChannelSCM2>(
                                MODULE_SCM_MK2,
                                id, iFace, tsynch, moduleFactory,
                                SCM_MK2_CH_DINPUT_LAST,
                                SCM_MK2_CH_AINPUT_LAST,
                                SCM_MK2_CH_DOUT_LAST,
                                SCM_MK2_CH_SWOUT_LAST
                                )
    {};
    virtual ~SCMMK2IOModule() {};

public:
    virtual IOM_ERROR configure();
    virtual IOM_ERROR loadConfiguration();

private:
    IOM_ERROR loadSwitchConfiguration(const lu_uint32_t channelID);
};

#else
//{
/*!
 * \brief SCM MK2 Direct Drive CAN module object
 *
 * Object for handling functionality of SCM MK2 CAN Module
 */
class SCMMK2IOModule : public CANIOModule
{
public:
    SCMMK2IOModule(MODULE_ID id,
                    COMM_INTERFACE iFace,
                    TSynchManager &tsynch,
                    ICANModuleConfigurationFactory &moduleFactory
                    );
    virtual ~SCMMK2IOModule();

    /**
     * \brief Return a channel reference
     *
     * \param type Channel type
     * \param number Channel number
     *
     * \return Channel pointer. NULL if channel is not present
     */
    virtual IChannel *getChannel(CHANNEL_TYPE type, lu_uint32_t number);

    /**
     * \brief Notify the timer tick is expired
     *
     * This function should be periodically called in order to keep the internal logic
     * updated (internal timers, etc,...)
     *
     * \param dTime Time elapsed from the previous tick (in ms)
     *
     * \return Error code
     */
    virtual IOM_ERROR tickEvent(lu_uint32_t dTime);

    /**
     * \brief Decode a module message  If necessary the module/channels internal state
     * is updated
     *
     * \param message Message to decode
     *
     * \return Error code
     */
    virtual IOM_ERROR decodeMessage(ModuleMessage* message);

    virtual IOM_ERROR configure();

    virtual IOM_ERROR loadConfiguration();

    /**
     * \brief Stop all of this module's channels from updating
     *
     * \param none
     *
     * \return none
     */
    virtual void stop();

    /**
     * \brief Get module configuration
     *
     * \param config Where to get the configuration when success
     *
     * \return Error code
     */
    virtual IOM_ERROR getConfiguration(SCMMK2ConfigStr& config)
    {
        if(!configOK)
        {
            return IOM_ERROR_CONFIG;
        }
        config = moduleConfig;
        return IOM_ERROR_NONE;
    }


protected:
    /**
     * \brief Update the status of all the channels
     *
     * \param status LU_TRUE if the module is online
     *
     * \return none
     */
    void setAllChsActive(lu_bool_t status);

private:
    bool configOK;              //Module configuration is valid
    SCMMK2ConfigStr moduleConfig;  //Module configuration
    InputDigitalCANChannel BInput[SCM_MK2_CH_DINPUT_LAST];
    InputAnalogueCANChannel AInput[SCM_MK2_CH_AINPUT_LAST];
    SwitchCANChannelSCM2 switchOut[SCM_MK2_CH_SWOUT_LAST];
    OutputDigitalCANChannel DOutput[SCM_MK2_CH_DOUT_LAST];

};

//}
#endif //if 0


#endif /* SCMMK2IOMODULE_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
