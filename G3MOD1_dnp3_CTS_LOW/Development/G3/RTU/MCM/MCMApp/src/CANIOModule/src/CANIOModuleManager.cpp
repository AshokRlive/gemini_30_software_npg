/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *  28/06/11      galli_m     CAN Board Manager.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <cstring>
#include <cstdlib>
#include <errno.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "timeOperations.h"
#include "CANIOModuleManager.h"
#include "CANInterface.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */




CANIOModuleManager::CANIOModuleManager( SCHED_TYPE            schedType  ,
                                        lu_uint32_t           priority   ,
                                        CANIOModuleFactory   *factory    ,
                                        ConfigurationManager &cManager
                                      ) :
                                   canPSocket(PrimaryCanInterface, LU_FALSE),
                                   canSSocket(SecondaryCanInterface, LU_FALSE),
                                   tsynch(canPSocket, canSSocket),
                                   modules(factory, tsynch, cManager),
                                   periodicThread(canPSocket, canSSocket, modules, tsynch, schedType, priority),
                                   eventManager(canPSocket, canSSocket, modules, schedType, priority),
                                   powerSaving(LU_FALSE),
                                   HMIStatus(LU_FALSE),
                                   log(Logger::getLogger(SUBSYSTEM_ID_CANMANAGER))

{
    /* == WARNING ==: Any message handled here should NOT be bigger than a CAN frame,
     * since this socket handles messages from ALL the Slave modules in only ONE socket
     * (one socket per CAN bus).
     * This way, fragmented messages could be scrambled at any time.
     * In need, the message type handling for that specific message type could be moved
     * to CANIOModule, adding its own CANFraming socket and filter (one socket per module).
     */
#if POLLING_CAN_INTERFACE_STATE
    /* Start CAN0 in any case */
    CANInterface::getInstance(PrimaryCanInterface).up();   //Enable interface 0 always
#endif

    /* Initialise socket CAN filter*/
    CanFilterTable filterTable;

    const lu_uint32_t TABLESIZE = 4;
    struct can_filter filter[TABLESIZE];
    CANHeaderStr* CANHeaderPtr;

    /* Accept all events */
    filter[0].can_id   = 0;
    filter[0].can_mask = 0;
    CANHeaderPtr = (CANHeaderStr*)(&filter[0].can_id);
    CANHeaderPtr->messageType = MODULE_MSG_TYPE_EVENT;
    CANHeaderPtr = (CANHeaderStr*)(&filter[0].can_mask);
    CANHeaderPtr->messageType = CANHEADERDEF_MASK_MESSAGETYPE;

    /* Accept HBeat */
    filter[1].can_id   = 0;
    filter[1].can_mask = 0;
    CANHeaderPtr = (CANHeaderStr*)(&filter[1].can_id);
    CANHeaderPtr->messageType = MODULE_MSG_TYPE_LPRIO;
    CANHeaderPtr->messageID = MODULE_MSG_ID_LPRIO_HBEAT_S;
    CANHeaderPtr = (CANHeaderStr*)(&filter[1].can_mask);
    CANHeaderPtr->messageType = CANHEADERDEF_MASK_MESSAGETYPE;
    CANHeaderPtr->messageID = CANHEADERDEF_MASK_MESSAGEID;


    /* TODO: AP - Move HMI events to another iface */
    /* HMI button pressed/released (should be moved on a different interface) */
    filter[2].can_id   = 0;
    filter[2].can_mask = 0;
    CANHeaderPtr = (CANHeaderStr*)(&filter[2].can_id);
    CANHeaderPtr->messageType = MODULE_MSG_TYPE_HMI_EVENT;
    CANHeaderPtr->messageID = MODULE_MSG_ID_HMI_EVENT_BUTTON;
    CANHeaderPtr = (CANHeaderStr*)(&filter[2].can_mask);
    CANHeaderPtr->messageType = CANHEADERDEF_MASK_MESSAGETYPE;
    CANHeaderPtr->messageID = CANHEADERDEF_MASK_MESSAGEID;

    /* HMI OLR change (should be moved on a different interface) */
    filter[3].can_id   = 0;
    filter[3].can_mask = 0;
    CANHeaderPtr = (CANHeaderStr*)(&filter[3].can_id);
    CANHeaderPtr->messageType = MODULE_MSG_TYPE_HMI_EVENT;
    CANHeaderPtr->messageID = MODULE_MSG_ID_HMI_EVENT_REQ_OLR_CHANGE;
    CANHeaderPtr = (CANHeaderStr*)(&filter[3].can_mask);
    CANHeaderPtr->messageType = CANHEADERDEF_MASK_MESSAGETYPE;
    CANHeaderPtr->messageID = CANHEADERDEF_MASK_MESSAGEID;

    filterTable.setTable(filter, TABLESIZE);
    canPSocket.setFilter(filterTable);
    canSSocket.setFilter(filterTable);
}



CANIOModuleManager::~CANIOModuleManager()
{
    /* Stop periodic thread */
    periodicThread.stop();

    /* Stop event thread */
    eventManager.stop();
}



IOM_ERROR CANIOModuleManager::discover()
{
    IOM_ERROR ret = IOM_ERROR_NONE;
    CANInterface::getInstance(PrimaryCanInterface).setEnabled(LU_TRUE);   //Enable can0

    /* Start threads */
    if ( (eventManager.start() != THREAD_ERR_NONE) ||
         (periodicThread.start() != THREAD_ERR_NONE)
       )
    {
        ret = IOM_ERROR_NOT_RUNNING;
    }
    return ret;
}


void CANIOModuleManager::setBootloaderMode(lu_bool_t mode)
{
    periodicThread.setBootloaderMode(mode);
}


void CANIOModuleManager::setPowerSaveMode(lu_bool_t mode)
{
    periodicThread.setPowerSaveMode(mode);
}


IOM_ERROR CANIOModuleManager::stopAllModules()
{
    /* Stop all CAN events! */
    eventManager.stop();

    /* Loop over the modules, stopping them */
    std::vector<IIOModule*> moduleList;
    moduleList = getAllModules();
    for(std::vector<IIOModule*>::iterator it=moduleList.begin(); it!=moduleList.end(); ++it)
    {
        (*it)->stop();
    }
    return IOM_ERROR_NONE;
}


#if DEBUG_HBEAT_ENABLED
void CANIOModuleManager::setHBeatEnable(lu_uint8_t x)
{
    if(x==0)
    {
        periodicThread.stop();
    }
    if(x==1)
    {
        periodicThread.start();
    }
}
#endif


CAN_ERROR CANIOModuleManager::restartAllModules()
{
    const lu_char_t *FTITLE = "CANIOModuleManager::restartAllModules:";
    CAN_ERROR result = CAN_ERROR_NONE;
    CAN_ERROR ret = CAN_ERROR_NONE;
    PayloadRAW payload;
    MDCmdRestartStr restartMessage;
    restartMessage.type = MD_RESTART_WARM;
    /* allocate a message */
    payload.payloadLen = sizeof(MDCmdRestartStr);
    payload.payloadPtr = reinterpret_cast<lu_uint8_t*>(&restartMessage);
    RAWModuleMessage message(&payload);

    /* Prepare message */
    message.header.messageType   = MODULE_MSG_TYPE_MD_CMD;
    message.header.messageID     = MODULE_MSG_ID_MD_CMD_RESTART;
    message.header.source        = MODULE_MCM;
    message.header.sourceID      = MODULE_ID_0;
    message.header.destination   = MODULE_BRD;
    message.header.destinationID = MODULE_ID_BRD;

    CANFraming *socketPtr;
    for(lu_uint8_t i = 0; i < 2; i++ )  //Send to Primary & secondary CAN sockets
    {
        socketPtr = (i == 0)? &canPSocket : &canSSocket;
        if( (socketPtr == &canSSocket) && ((HMIStatus != LU_TRUE) || (powerSaving != LU_FALSE)) )
        {
            continue;   //skip secondary socket if brought down
        }

        /* Send module restart message */
        ret = socketPtr->write(&message);
        switch(ret)
        {
            case CAN_ERROR_NONE:
                //Successful write
                log.info("%s Module Restart command sent to %s CAN",
                                FTITLE,
                                (i == 0)? "primary" : "secondary"
                              );
                break;
            case CAN_ERROR_INIT:
                //Socket/Interface not initialised: ignore
                break;
            case CAN_ERROR_DOWN:
                //Interface is down: ignore
                break;
            default:
                result = ret;   //remember if any error happened
                log.error("%s Restart command send to %s CAN, error %i: %s",
                                FTITLE,
                                (i == 0)? "primary" : "secondary",
                                ret, IOM_ERROR_ToSTRING(ret)
                              );
                break;
        }
    }
    return result;
}


IIOModule *CANIOModuleManager::createModule(MODULE module, MODULE_ID moduleID, COMM_INTERFACE iFace)
{
    return modules.getModule(module, moduleID, iFace);
}


IIOModule *CANIOModuleManager::getModule(MODULE module, MODULE_ID moduleID)
{
    return modules.getModule(module, moduleID);
}


std::vector<IIOModule*> CANIOModuleManager::getAllModules()
{
    return modules.getAllModules();
}


IChannel *CANIOModuleManager::getChannel( MODULE module,
                                          MODULE_ID moduleID,
                                          CHANNEL_TYPE chType,
                                          lu_uint32_t chNumber
                                        )
{
    /* Get module */
    IIOModule *modulePtr = modules.getModule(module, moduleID);
    if(modulePtr != NULL)
    {
        /* Valid module found. Get the channel */
        return modulePtr->getChannel(chType, chNumber);
    }
    else
    {
        /* Board not found */
        return NULL;
    }
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */

