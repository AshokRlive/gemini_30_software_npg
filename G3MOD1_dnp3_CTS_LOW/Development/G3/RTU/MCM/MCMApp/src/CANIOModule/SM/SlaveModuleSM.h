/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:SlaveModuleSM.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   30 Jan 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef SlaveModuleSM_H_
#define SlaveModuleSM_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "Mutex.h"
#include "Logger.h"
#include "ModuleProtocolEnum.h"
#include "ModuleProtocol.h"
#include "IOModuleCommon.h"
#include "ModuleAlarm.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

typedef ModuleHBeatSStr HBeat;
typedef ModuleInfoStr MInfo;

class SlaveModule_fsm;
class SlaveModuleSMState;

/**
 * \brief
 * This abstract class wraps the implementation of state machine and defines
 *  the action APIs required in state transition.
 * By inheriting this class and implementing all action APIs, we can create a
 * concrete state machine for slave module.
  */
class SlaveModuleSM
{
public:

    class IStateChangeObserver : public QueueObj<IStateChangeObserver>
    {
        public:
        IStateChangeObserver(){};
        virtual ~ IStateChangeObserver(){};

        virtual void stateChanged(FSM_STATE oldState, FSM_STATE newState) = 0;
    };

    typedef QueueMgr<IStateChangeObserver> StateObserverQueue;


    SlaveModuleSM(const IOModuleIDStr& g_id);
    virtual ~SlaveModuleSM();

    /**
    * Set the initial state of this state machine. This function must be called after
    * all module resources are created so this state machine will be able to
    * start using those resources(e.g. loading configuration for channels)
    */
    void init();

    void attach(IStateChangeObserver *observer);

    void detach(IStateChangeObserver *observer);

    /**
     * Enable/Disable debugging state machine, which prints out state changing
     *  message to standard output.
     */
    void setDebugEnabled(lu_bool_t debugEnabled);

    void setHBeatTimeoutEnabled(lu_bool_t enabled);

    lu_bool_t isDebugEnabled();

    /**
     * This method needs to be called when a HeartBeat message is received from
     * this slave module. It will fire a event which drives this state
     *  machine to change its state.
     */
    void handleHBeatEvent(const ModuleHBeatSStr& hb);

    /**
     * This method needs to be called when a Module Info message is received.
     * It will fire a event which drives this state machine to change its state.
     */
    void handleMInfoEvent(const ModuleInfoStr& minfo);

    /**
     * This method needs to be called when a tick event is received.
     * It may fire Timeout events which drives this state machine to change its state.
     */
    void handleTickEvent(lu_uint32_t dTime);

    /**
     * Gets the system API of the slave module.
     */
    const ModuleVersionStr& getSystemAPI() ;

    /**
     * Gets the moduleUID number of the slave module.
     */
    ModuleUID getModuleUID();

    /**
     * Gets all information of the slave module.
     */
    void getAllInfo(IOModuleInfoStr *info);

    /**
     * Gets information details of the slave module.
     */
    void getDetails(IOModuleDetailStr& detail);

    /**
     * Get the name of this state machine.
     */
    virtual const lu_char_t * name()= 0;


    lu_bool_t getStatus(MSTATUS status);


    /*************************************/
    /* State Transition Actions Interface*/
    /*************************************/

    void setPresent(lu_bool_t isPresent)
    {
        this->status.present = isPresent;
    }

    void setDetected(lu_bool_t isDetected)
    {
        this->status.detected = isDetected;
    }

    void setActive(lu_bool_t isActive)
    {
        this->status.active = isActive;
    }

    void setDisabled(lu_bool_t isDisabled)
    {
        this->status.disabled = isDisabled;
    }

    void setMInfoRetries(lu_uint8_t retries)
    {
        this->mInfoRetries = retries;
    }

    lu_uint8_t getMInfoRetries()
    {
        return this->mInfoRetries;
    }

    /**
     * Checks if this module is in any status of Bootloader Programming.
     */
    lu_bool_t isProgingBL(lu_uint8_t status)
    {
        return status == MODULE_BOARD_STATUS_APP_BOOTL_FAIL
                    || status == MODULE_BOARD_STATUS_APP_BOOTL_INIT
                    || status == MODULE_BOARD_STATUS_APP_BOOTL_OK
                    || status == MODULE_BOARD_STATUS_APP_BOOTL_PROG;
    }

    /**
     * Action for handling the first heart beat.
     */
    void firstHbeatRcvAction(const ModuleHBeatSStr&);

    /**
     * Checks if this module is valid for initialisation
     * (e.g.sending configuration and "start" command to this module).
     */
    virtual void validateAction(const MInfo& mInfo) = 0;

    /**
     * Gets the result of validationAction().
     */
    virtual lu_bool_t isValid() = 0;

    /**
     * Loads the configuration for sending it to this module later.
     */
    virtual void loadConfigAction() = 0;

    /**
     * Send a message to this module to request module info.
     */
    virtual void sendMInfoRequestAction() = 0;

    /**
     * Initialise module by sending configuration and start command.
     */
    virtual void initModuleAction() = 0;

    /**
     * Reset the slave module.
     */
    virtual void resetBoardAction() = 0;

    virtual void setAllChsActive(lu_bool_t active) = 0;

    /**
     * Stop slave module eventing so it can be configured & initialised later on.
     */
    virtual void stopEventingAction() = 0;

    /**
     * An action of clearing the history heart-beat information.
     * This function should be called when detecting module is offline.
     */
    void clearHBeat();

protected:
    /**
     * Issues a module alarm and/or notifies the alarm the present state.
     */
    virtual void issueAlarm(const lu_bool_t issue,
                            const SYS_ALARM_SUBSYSTEM subSystem,
                            const lu_uint32_t alarmCode) = 0;

    /*************************************/
    /* Status Flags Mutators             */
    /*************************************/

    void setRegistered(lu_bool_t isRegistered)
    {
        this->status.registered = isRegistered;
    }
    void setConfigured(lu_bool_t isConfigured)
    {
        this->status.configured = isConfigured;
    }

    /*************************************/
    /* Standard Debugging                */
    /*************************************/

    void debugAction(const lu_char_t* actionName)
    {
        log.debug("[ACTION ]%s -> %s ", name(), actionName);
    }

    void debugEvent(const lu_char_t* eventName)
    {
        log.debug("[EVENT  ]%s -> %s ", name(), eventName);
    }


private:
    SlaveModuleSMState& getState();

    lu_bool_t isState(FSM_STATE state);

    lu_bool_t isNotState(FSM_STATE state);

    void fireStateChangeEvent(FSM_STATE oldState, FSM_STATE newState);

    /**
     * Issues/disables the Module UID (serial no.) mismatch alarm
     */
    void issueUIDMismatch(lu_bool_t activate);

public:
    const static lu_uint8_t MAX_MINFO_RETRIES = 3;

    const IOModuleIDStr& mid;//The module's id.

protected:
    Logger& log;
    Mutex mutex;

private:
    SlaveModule_fsm* fsm;   //Finite state machine

    ModuleInfoStr    mInfo; //Local copy of module info message.
    ModuleHBeatSStr  hbeat; //Local copy of heat beat message.

    IOModuleStatusStr   status;
    ModuleUID moduleUID;    //Contains module serial and supplier ID

    lu_uint32_t hbeatTimeout;
    lu_uint32_t mInfoTimeout;
    lu_uint32_t bootTimeout;

    lu_uint8_t  mInfoRetries;   //The retry times of sending module info request.

    lu_bool_t   hbeatTimeoutEnabled;

    FSM_STATE curState; //Current state of this state machine.

    StateObserverQueue observers; //Observer Queue
    MasterMutex queueMutex; //Mutex to protect Observer Queue

};

#endif /* SlaveModuleSM_H_ */

/*
 *********************** End of file ******************************************
 */
