/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       SCM Module implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   01/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "SCMIOModule.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */




SCMIOModule::SCMIOModule( MODULE_ID id                                 ,
                          COMM_INTERFACE iFace                         ,
                          TSynchManager &tsynch                        ,
                          ICANModuleConfigurationFactory &moduleFactory
                        ) :
                           CANIOModule(MODULE_SCM, id, iFace, tsynch, moduleFactory),
                           configOK(false)
{
     lu_uint32_t i;

     /* Full channels initialisation: Binary Input */
     for(i = 0; i < SCM_CH_DINPUT_LAST; i++)
     {
          BInput[i].setBoard(this);
          BInput[i].setID(i);
     }

     /* Full channels initialisation: Analogue Input */
     for(i = 0; i < SCM_CH_AINPUT_LAST; i++)
     {
         AInput[i].setBoard(this);
         AInput[i].setID(i);
     }

     /* Full channels initialisation: Switch Output */
     for(i = 0; i < SCM_CH_SWOUT_LAST; i++)
     {
         switchOut[i].setBoard(this);
         switchOut[i].setID(i);
     }

#if SCM_AUXILIARY_CH_ENABLED
     /* Full channels initialisation: Auxiliary Switch Output */
     for(i = 0; i < SCM_CH_AUXSWOUT_LAST; i++)
     {
         auxSwitchOut[i].setBoard(this);
         auxSwitchOut[i].setID(i);
     }
#endif

     /* Full channels initialisation: Digital Output */
     for(i = 0; i < SCM_CH_DOUT_LAST; i++)
     {
         DOutput[i].setBoard(this);
         DOutput[i].setID(i);
     }

     initStateMachine();
}



SCMIOModule::~SCMIOModule()
{

}


IChannel *SCMIOModule::getChannel(CHANNEL_TYPE type, lu_uint32_t number)
{
    IChannel *channelPtr = NULL;

    switch(type)
    {
        case CHANNEL_TYPE_DINPUT:
            if(number < SCM_CH_DINPUT_LAST)
            {
                channelPtr = &BInput[number];
            }
        break;

        case CHANNEL_TYPE_AINPUT:
            if(number < SCM_CH_AINPUT_LAST)
            {
                channelPtr = &AInput[number];
            }
        break;

        case CHANNEL_TYPE_SW_OUT:
            if(number < SCM_CH_SWOUT_LAST)
            {
                channelPtr = &switchOut[number];
            }
        break;

#if SCM_AUXILIARY_CH_ENABLED
        case CHANNEL_TYPE_AUXSW_OUT:
            if(number < SCM_CH_AUXSWOUT_LAST)
            {
                channelPtr = &auxSwitchOut[number];
            }
        break;
#endif

        case CHANNEL_TYPE_DOUTPUT:
            if(number < SCM_CH_DOUT_LAST)
            {
                channelPtr = &DOutput[number];
            }
        break;

        default:
            log.error("SCMIOModule::getChannel: "
                            "Unsupported CAN channel: %i. Function: %s",
                            type, __FUNCTION__
                          );
        break;
    }

    return channelPtr;
}


IOM_ERROR SCMIOModule::tickEvent(lu_uint32_t dTime)
{
    /* Call parent tick function */
    return CANIOModule::tickEvent(dTime);
}


IOM_ERROR SCMIOModule::decodeMessage(ModuleMessage* message)
{
    IOM_ERROR ret = IOM_ERROR_NONE;

    if(message == NULL)
    {
        return IOM_ERROR_PARAM;
    }

    /* General message ? */
    ret = CANIOModule::decodeMessage(message);
    if (ret == IOM_ERROR_NOT_SUPPORTED)
    {
        /* No - Try custom decoder */
        switch(message->header.messageType)
        {
            /* Forward the event to the channel */
            case MODULE_MSG_TYPE_EVENT:
                switch(message->header.messageID)
                {
                    case MODULE_MSG_ID_EVENT_DIGITAL:
                        CANIOModule::handleDEvent(message, BInput, SCM_CH_DINPUT_LAST);
                        ret = IOM_ERROR_NONE;
                    break;

                    case MODULE_MSG_ID_EVENT_ANALOGUE:
                        CANIOModule::handleAEvent(message, AInput, SCM_CH_AINPUT_LAST);
                        ret = IOM_ERROR_NONE;
                    break;

                    default:
                        ret = IOM_ERROR_NOT_SUPPORTED;
                    break;
                }
            break;

            default:
                ret = IOM_ERROR_NOT_SUPPORTED;
            break;
        }
    }

    if(ret == IOM_ERROR_NOT_SUPPORTED)
    {
        log.warn("SCMIOModule::decodeMessage: "
                        "%s Unsupported message: 0x%02x:0x%02x",
                        getName(),
                        message->header.messageType,
                        message->header.messageID
                       );
    }
    return ret;
}


IOM_ERROR SCMIOModule::loadConfiguration()
{
    lu_uint32_t ch_id;
    IOM_ERROR err = IOM_ERROR_NONE;
    IOM_ERROR ret;

    if (configFactory.isConfigured(mid) == LU_FALSE)
    {
        log.warn("%s-was not found in configuration", getName());
        return IOM_ERROR_NOT_FOUND;
    }

    ret = configFactory.getModuleConf<SCMConfigStr>(mid.type, mid.id, moduleConfig);

    if (ret == IOM_ERROR_NONE)
    {
        log.debug("%s Load configuration: switch colour = %i (%s)%s.", getName(),
                    moduleConfig.greenLEDPolarity,  //Green polarity = 1
                    (moduleConfig.greenLEDPolarity == 1) ? "green" : "red",
                    (moduleConfig.doNotCheckSwitchPosition)? "; do not check switch position" : ""
                );
        configOK = true;
    }
    else
    {
        err = ret;
        log.error("%s fail to load SCM configuration. Error %i: %s",
                        getName(), ret, IOM_ERROR_ToSTRING(ret));
    }

    /* ==Load all Digital Input== */
    InputDigitalCANChannelConf conf;
    for (ch_id = 0; ch_id < SCM_CH_DINPUT_LAST; ch_id++)
    {
        ret = configFactory.getIDigitalConf(mid.type, mid.id, ch_id, conf);
        if (ret == IOM_ERROR_NONE)
        {
            BInput[ch_id].setConfig(conf);
        }
        else
        {
            err = ret;
            log.error("%s fail to load configuration for %s. Error %i: %s",
                            getName(), BInput[ch_id].getName(),
                            ret, IOM_ERROR_ToSTRING(ret)
                            );
        }
    }

    /* ==Load all Analogue Input== */
    InputAnalogueCANChannelConf conf1;
    for (ch_id = 0; ch_id < SCM_CH_AINPUT_LAST; ch_id++)
    {
        ret = configFactory.getIAnalogueConf(mid.type, mid.id, ch_id,
                         conf1);
        if (ret == IOM_ERROR_NONE)
        {
            AInput[ch_id].setConfig(conf1);
        }
        else
        {
            err = ret;
            log.error("%s fail to load configuration for %s. Error %i: %s",
                        getName(), AInput[ch_id].getName(),
                        ret, IOM_ERROR_ToSTRING(ret)
                        );
        }
    }

    /* ==Load all Switch Out channels== */
    SwitchCANChannelConf conf2;
    for (ch_id = 0; ch_id < SCM_CH_SWOUT_LAST; ch_id++)
    {
        ret = configFactory.getSwitchOutConf(mid.type, mid.id, ch_id,
                         conf2);
        if (ret == IOM_ERROR_NONE)
        {
            switchOut[ch_id].setConfig(conf2);
        }
        else
        {
            err = ret;
            log.error("%s fail to load configuration for %s. Error %i: %s",
                            getName(), switchOut[ch_id].getName(),
                            ret, IOM_ERROR_ToSTRING(ret)
                            );
        }

    }

    /* Load all Digital Output channels */
      OutputDigitalCANChannelConf conf_do;
      for (ch_id = 0; ch_id < SCM_CH_DOUT_LAST; ch_id++)
      {
          ret = configFactory.getDigitalOutConf(mid.type, mid.id, ch_id, conf_do);
          if (ret == IOM_ERROR_NONE)
              DOutput[ch_id].setConfig(conf_do);
          else
          {
              err = ret;
              log.error("%s fail to load configuration for %s. Error %i: %s",
                             getName(), DOutput[ch_id].getName(),
                             ret, IOM_ERROR_ToSTRING(ret)
                         );
          }
      }

  return err;
}

IOM_ERROR SCMIOModule::configure()
{
    const lu_char_t* FTITLE = "SCMIOModule::configure:";

    lu_bool_t hasFailure = LU_FALSE;
    lu_uint32_t i;
    IOM_ERROR ret;
    log.info("%s %s-send configuration", FTITLE, getName());

    /* ==General configuration== */
    /* Prepare message */
    SCMConfigStr messagePayload;
    messagePayload.greenLEDPolarity = moduleConfig.greenLEDPolarity;
    messagePayload.doNotCheckSwitchPosition = moduleConfig.doNotCheckSwitchPosition;
    messagePayload.unused = 0;
    PayloadRAW payload;
    payload.payloadPtr = reinterpret_cast<lu_uint8_t *>(&messagePayload);
    payload.payloadLen = sizeof(SCMConfigStr);
    IIOModule::sendReplyStr message;
    message.messageType     = MODULE_MSG_TYPE_CFG;
    message.messageID       = MODULE_MSG_ID_CFG_SCM_C;
    message.messagePtr      = &payload;
    message.rMessageType    = MODULE_MSG_TYPE_CFG;
    message.rMessageID      = MODULE_MSG_ID_CFG_SCM_R;
    message.timeout = ms_to_timeval(200);     //ms timeout for CAN message reply

    /* Send message */
    ret = this->sendMessage(&message);
    if(ret == IOM_ERROR_NONE)
    {
        /* Get payload */
        (message.rMessagePtr)->getPayload(&payload);
        if(payload.payloadLen == MODULE_MESSAGE_SIZE(ModStatusReplyStr))
        {
            ModStatusReplyStr *replyPtr = reinterpret_cast<ModStatusReplyStr*>(payload.payloadPtr);
            if(replyPtr->status != REPLY_STATUS_OKAY)
            {
                log.error("%s Error Sending configuration to slave module %s. Reply status: %i",
                            FTITLE,
                            getName(),
                            replyPtr->status
                          );
                ret = IOM_ERROR_REPLY;
            }
        }
        else
        {
            /* Wrong payload */
            log.error("%s Error Sending configuration to slave module %s - reply has invalid size: %i",
                            FTITLE,
                            getName(),
                            payload.payloadLen
                          );
            ret = IOM_ERROR_REPLY;
        }
        /* Release reply message */
        delete (message.rMessagePtr);
    }

    if(ret != IOM_ERROR_NONE)
    {
        hasFailure = LU_TRUE;
        log.error("%s Error configuring %s. Error %i: %s"
                    "Unable to set Open and Close operation LED colours!",
                    FTITLE,
                    getName(),
                    ret, IOM_ERROR_ToSTRING(ret)
                  );
    }

    /* ==Configure all Digital Input== */
    for(i = 0; i < SCM_CH_DINPUT_LAST; i++)
    {
        ret = BInput[i].sendConfiguration();
        LOG_CH_CONFIG_FAILURE(ret,"DInput");
    }

    /* ==Configure all Analogue Input== */
    for(i = 0; i < SCM_CH_AINPUT_LAST; i++)
    {
        ret = AInput[i].sendConfiguration();
        LOG_CH_CONFIG_FAILURE(ret,"AInput");
    }

    /* ==Configure all Switch Out channels== */
    for(i = 0; i < SCM_CH_SWOUT_LAST; i++)
    {
        ret = switchOut[i].sendConfiguration();
        LOG_CH_CONFIG_FAILURE(ret,"SwitchOutput");
    }

#if SCM_AUXILIARY_CH_ENABLED
    /* ==Configure all Auxiliary Switch Out channels== */
    for(i = 0; i < SCM_CH_AUXSWOUT_LAST; i++)
    {
        ret = auxSwitchOut[i].sendConfiguration();
        LOG_CH_CONFIG_FAILURE(ret,"SwitchAuxOutput");
    }
#endif

    /* ==Configure all Digital Out channels== */
    for (i = 0; i < SCM_CH_DOUT_LAST; i++)
    {
        ret = DOutput[i].sendConfiguration();
        LOG_CH_CONFIG_FAILURE(ret,"DOutput");
    }

    return (hasFailure == LU_TRUE)? IOM_ERROR_CONFIG:IOM_ERROR_NONE;
}

void SCMIOModule::stop()
{
    lu_int32_t i;

    /* Full channels initialisation: Binary Input */
    for(i = 0; i < SCM_CH_DINPUT_LAST; i++)
    {
         BInput[i].stopUpdate();
    }

    /* Full channels initialisation: Analogue Input */
    for(i = 0; i < SCM_CH_AINPUT_LAST; i++)
    {
        AInput[i].stopUpdate();
    }

    /* Full channels initialisation: Switch Output */
    for(i = 0; i < SCM_CH_SWOUT_LAST; i++)
    {
        switchOut[i].stopUpdate();
    }

#if SCM_AUXILIARY_CH_ENABLED
    /* Full channels initialisation: Auxiliary Switch Output */
    for(i = 0; i < SCM_CH_AUXSWOUT_LAST; i++)
    {
        auxSwitchOut[i].stopUpdate();
    }
#endif

    /* Full channels initialisation: Digital Output */
    for(i = 0; i < SCM_CH_DOUT_LAST; i++)
    {
        DOutput[i].stopUpdate();
    }
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void SCMIOModule::setAllChsActive(lu_bool_t status)
{
    lu_uint32_t i;

    /* Binary Input */
    for(i = 0; i < SCM_CH_DINPUT_LAST; i++)
    {
         BInput[i].setActive(status);
    }

    /* Analogue Input */
    for(i = 0; i < SCM_CH_AINPUT_LAST; i++)
    {
        AInput[i].setActive(status);
    }

    /* Switch Output */
    for(i = 0; i < SCM_CH_SWOUT_LAST; i++)
    {
        switchOut[i].setActive(status);
    }

#if SCM_AUXILIARY_CH_ENABLED
    /* Auxiliary Switch Output */
    for(i = 0; i < SCM_CH_AUXSWOUT_LAST; i++)
    {
        auxSwitchOut[i].setActive(status);
    }
#endif

    /* Digital Output */
    for (i = 0; i < SCM_CH_DOUT_LAST; i++)
    {
        DOutput[i].setActive(status);
    }
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */


