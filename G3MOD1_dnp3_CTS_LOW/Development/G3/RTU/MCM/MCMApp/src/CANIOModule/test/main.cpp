#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include "embUnit/embUnit.h"


#include "RAWModuleMessage.h"
#include "CANFraming.h"
//#include "SysLogManagerPrintf.h"
//#include "CANIOModuleManager.h"
//#include "CANIOModuleFactory.h"
#include "IOData.h"
#include "LinearInterpolation.h"


typedef struct MessageWithFilterDef
{
	RAWLargeModuleMessage message;
	CANHeaderStr          *filter;
} MessageWithFilterStr;

/* Type of calibrated value */
typedef enum
{
	CAL_TYPE_1DU16S16  = 0,
	CAL_TYPE_1DU16U16     ,
	CAL_TYPE_1DU16U32	  ,
	CAL_TYPE_1DU16F32	  ,
	CAL_TYPE_1DU32U32	  ,

	CAL_TYPE_LAST
}CAL_TYPE;

typedef struct calSingleValueDef
{
	lu_uint8_t  lastValue;
	lu_int32_t  ValueOne;
	lu_int32_t  ValueTwo;
} calSingleValueStr;

/* Calibration Data format in memory */
typedef struct CalDataHeaderdef
{
	lu_uint8_t calId;
	lu_uint8_t type;
	lu_int16_t length;
} CalDataHeaderStr;


// GLOBALS
MessageWithFilterStr  CANTransaction;
CanFilterTable        filterTable;
struct can_filter     filter[1];



// Extract a calibration element from the given file descriptor (borrowed from lucycansend)
int GetValueFromCSV(FILE *fp, calSingleValueStr *calSingleValue)
{
	int retError;
	int ret;

	retError = 0;

	ret = fscanf(fp, "%d,%d", &calSingleValue->ValueOne, &calSingleValue->ValueTwo);
	switch (ret)
	{
	case 2:
		printf("\n%d %d", calSingleValue->ValueOne, calSingleValue->ValueTwo);
		break;

	case EOF:
		break;

	default:
		retError = 1;
		break;
	}

	return retError;
}



// Send calibration data to the board (borrowed from lucycansend)
void sendCalBoard(lu_uint8_t cal_id, lu_uint8_t cal_type, const char *filename)
{
	LnInterpTable1DU16S16Str *calType1du16s16Ptr;
	LnInterpTable1DU16U16Str *calType1du16u16Ptr;
	LnInterpTable1DU16U32Str *calType1du16u32Ptr;
	LnInterpTable1DU32U32Str *calType1du32u32Ptr;
	FILE *fp;
	calSingleValueStr calSingleValue;
	int retValue = 0;
	lu_uint16_t count = 4;


    // This will contain the calibration data
	fp = fopen(filename, "rb");
	if (fp == NULL)
	{
		printf(" Error in opening file [%s]\n", filename);
		return;
	}

	CANTransaction.message.payload[0] = cal_id;
	CANTransaction.message.payload[1] = cal_type;
	CANTransaction.message.payload[2] = 0;
	CANTransaction.message.payload[3] = 0;

	while(!feof(fp) && retValue == 0)
	{
	  retValue = GetValueFromCSV(fp, &calSingleValue);

	  switch(cal_type)
	  {
		case CAL_TYPE_1DU16S16:
			{
				calType1du16s16Ptr                 = (LnInterpTable1DU16S16Str*)malloc(sizeof(LnInterpTable1DU16S16Str));
				calType1du16s16Ptr->distributionX  = calSingleValue.ValueOne;
				calType1du16s16Ptr->interpolateToX = calSingleValue.ValueTwo;

				memcpy(&CANTransaction.message.payload[count], calType1du16s16Ptr, sizeof(LnInterpTable1DU16S16Str));
				count = count + sizeof(LnInterpTable1DU16S16Str);

				free(calType1du16s16Ptr);
			}
			break;

		case CAL_TYPE_1DU16U16:
			{
				calType1du16u16Ptr                 = (LnInterpTable1DU16U16Str*)malloc(sizeof(LnInterpTable1DU16U16Str));
				calType1du16u16Ptr->distributionX  = calSingleValue.ValueOne;
				calType1du16u16Ptr->interpolateToX = calSingleValue.ValueTwo;

				memcpy(&CANTransaction.message.payload[count], calType1du16u16Ptr, sizeof(LnInterpTable1DU16U16Str));
				count = count + sizeof(LnInterpTable1DU16U16Str);

				free(calType1du16u16Ptr);
			}
			break;

		case CAL_TYPE_1DU16U32:
			{
				calType1du16u32Ptr                 = (LnInterpTable1DU16U32Str*)malloc(sizeof(LnInterpTable1DU16U32Str));
				calType1du16u32Ptr->distributionX  = calSingleValue.ValueOne;
				calType1du16u32Ptr->interpolateToX = calSingleValue.ValueTwo;

				memcpy(&CANTransaction.message.payload[count], calType1du16u32Ptr, sizeof(LnInterpTable1DU16U32Str));
				count = count + sizeof(LnInterpTable1DU16U32Str);

				free(calType1du16u32Ptr);
			}
			break;

		case CAL_TYPE_1DU32U32:
			{
				calType1du32u32Ptr                 = (LnInterpTable1DU32U32Str*)malloc(sizeof(LnInterpTable1DU32U32Str));
				calType1du32u32Ptr->distributionX  = calSingleValue.ValueOne;
				calType1du32u32Ptr->interpolateToX = calSingleValue.ValueTwo;

				memcpy(&CANTransaction.message.payload[count], calType1du32u32Ptr, sizeof(LnInterpTable1DU32U32Str));
				count = count + sizeof(LnInterpTable1DU32U32Str);

				free(calType1du32u32Ptr);
			}
			break;

		default:
			printf("Error in CAL type");
			break;
	  }
	}
    fclose(fp);

	count = count - sizeof(CalDataHeaderStr);
	CANTransaction.message.payload[2] = count & 0xFF;
	CANTransaction.message.payload[3] = count >> 8;
	CANTransaction.message.payloadLen = count + 4;

    // Set filter
	CANTransaction.filter = (CANHeaderStr*)(&filter[0].can_mask);
	CANTransaction.filter->messageType = MODULE_MSG_TYPE_CALTST;
	CANTransaction.filter->messageID   = MODULE_MSG_ID_CALTST_ADD_ELEMENT_R;
}




int main(int argc, char *argv[])
{
    ModuleMessage *readMsg;
    COMM_INTERFACE iface;
    CAN_ERROR ret;
    lu_bool_t reply_expected;
    lu_int8_t bus = 0;
    lu_int32_t value;
    lu_uint32_t i;

	lu_int8_t calFileName[800];
	lu_int8_t calibrationID;
	lu_int8_t calibrationType;
	lu_bool_t cmdCalibration = LU_FALSE;


	// Usage
    if (argc <= 1)
    {
    	printf("%s: No args!!\n", argv[0]);
    	printf("%s: [-mt=] [-mid=] [-dt=] [-did=] [data......]\n\n", argv[0]);
    	printf(" -can CAN Interface (0=can0 [default]; 1=can1)\n");

    	printf(" -mt Message type\n");
    	printf(" -mid Message ID\n");
    	printf(" -dt Destination type (0=PSM; 2=SCM; 3=FDM; 4=HMI)\n");
    	printf(" -did Destination ID (ID = 0 ,1 ,2,...)\n\n");
    	printf(" (Data bytes in Hex)\n");
    	printf("\nExample (CFG_SWITCH to SCM ):- %s -dt=2 -mt=3d -mid=8 0 0 0 0 0 0 0 0\n", argv[0]);

    	printf("\n -calid        Calibration ID");
    	printf("\n -caltype      Calibration Type");
    	printf("\n -caladdfile   Calibration Element File");

    	printf("\n\n");

    	return 0;
    }


    // Initialise the CANTransaction
    CANTransaction.message.header.messageType   = MODULE_MSG_TYPE_LPRIO;
    CANTransaction.message.header.messageID     = MODULE_MSG_ID_LPRIO_PING_C;
    CANTransaction.message.header.source        = MODULE_MCM;
    CANTransaction.message.header.sourceID      = MODULE_ID_0;
    CANTransaction.message.header.destination   = MODULE_PSM;
    CANTransaction.message.header.destinationID = MODULE_ID_0;
    CANTransaction.message.payloadLen           = 0;


    // Set a default filter
	filter[0].can_id   = bus;
    filter[0].can_mask = 0;
    CANTransaction.filter = (CANHeaderStr*)(&filter[0].can_id);
    filterTable.setTable(filter, 1);

    reply_expected = LU_TRUE;

    // Loop around the arguments
  	for(i = 1; i < argc; i++)
  	{
  		/* Get message type */
  		if (sscanf(argv[i], "-can=%i", &value) == 1)
  		{
  		    bus = (value > 0) ? 1 : 0;
  		}
  		/* Get message type */
  		else if (sscanf(argv[i], "-mt=%x", &value) == 1)
  		{
 			CANTransaction.message.header.messageType = (MODULE_MSG_TYPE) value;
   		}
  		/* Get message ID */
  		else if (sscanf(argv[i], "-mid=%x", &value) == 1)
  		{
  			CANTransaction.message.header.messageID = (MODULE_MSG_ID) value;
  		}
  		/* Get dest Type */
  		else if (sscanf(argv[i], "-dt=%x", &value) == 1)
  		{
  			CANTransaction.message.header.destination = (MODULE) value;
  		}
  		/* Get dest ID */
  		else if (sscanf(argv[i], "-did=%x", &value) == 1)
  		{
  			CANTransaction.message.header.destinationID = (MODULE_ID) value;
  		}
  		else if (sscanf(argv[i], "-calid=%x", &value) == 1)
  		{
  			calibrationID = value;
  			cmdCalibration = LU_TRUE;
  		}
  		else if (sscanf(argv[i], "-caltype=%x", &value) == 1)
  		{
  			calibrationType = value;
  			cmdCalibration = LU_TRUE;
  		}
  		else if (sscanf(argv[i], "-caladdfile=%s", calFileName) == 1)
  		{
  			cmdCalibration = LU_TRUE;
  		}
  		else if (sscanf(argv[i], "%x", &value) == 1)
  		{
  			CANTransaction.message.payloadLen++;
  			CANTransaction.message.payload[CANTransaction.message.payloadLen-1] = (lu_uint8_t) value;
  		}
    }

    // Add the calibration file contents to the message if necessary
  	if (cmdCalibration == LU_TRUE)
  	{
  		printf("Calibration command using file [%s]\n", calFileName);
  		sendCalBoard(calibrationID, calibrationType, calFileName);
  	}

  	// Set the CAN interface in use
    iface = (bus > 0) ? COMM_INTERFACE_CAN1 : COMM_INTERFACE_CAN0;

    // Create the socket interface
    CANFraming CANSocket(iface, LU_FALSE);
    CANSocket.setFilter(filterTable);

    printf("Sending on can%d\n", iface);


	if ((ret = CANSocket.write(&(CANTransaction.message))) != CAN_ERROR_NONE)
	{
	  printf("Error writing to CANSocket (%d)\n", ret);
	}

	// Only receive if we expect a reply...
	if (reply_expected)
	{
		/* Wait reply */
		struct timeval tv;
		tv.tv_sec = 0;
		tv.tv_usec = 500000;

		ret = CANSocket.read(&readMsg, &tv);

		/* Handle reply */
		if (ret == CAN_ERROR_TIMEOUT)
		{
			printf("Read timeout\n");
		}
		else if (ret == CAN_ERROR_NONE)
		{
			PayloadRAW payload;

			readMsg->getPayload(&payload);
#if 1
			printf("Reply MsgType:%x\n", readMsg->header.messageType);
			printf("Reply MsgID:%x\n", readMsg->header.messageID);
			printf("Reply payload:");
			for(lu_uint32_t i = 0; i < payload.payloadLen; ++i)
			{
				printf("0x%02x ", payload.payloadPtr[i]);
			}
			printf("\n\n");
#endif

			delete readMsg;
		}
		else
		{
			printf("Error: %i\n", ret);
		}
	}

	printf("Done.\n");
	return (ret);
}





