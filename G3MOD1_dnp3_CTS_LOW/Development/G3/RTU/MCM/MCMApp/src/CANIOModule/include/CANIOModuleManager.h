/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN Board manager
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   28/06/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_A3CFE674_CA84_43f8_9602_EC37680B2470__INCLUDED_)
#define EA_A3CFE674_CA84_43f8_9602_EC37680B2470__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "CANIOModuleDB.h"
#include "TSynchManager.h"
#include "CANEventManager.h"
#include "CANPeriodicData.h"
#include "ModuleProtocol.h"
#include "IIOModuleManager.h"
#include "IIOModule.h"
#include "CANIOModuleFactory.h"
#include "ConfigurationManager.h"
#include "CANFraming.h"
#include "Logger.h"
#include "ISupportBootloaderMode.h"
#include "ISupportPowerSaving.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * Discovery Protocol
 * Heart Beat
 * Time Synchronisation
 * Events
 */
class CANIOModuleManager : public IIOModuleManager,
                        public ISupportBootloaderMode,
                        public ISupportPowerSaving
{
public:
    static const COMM_INTERFACE PrimaryCanInterface   = COMM_INTERFACE_CAN0;
    static const COMM_INTERFACE SecondaryCanInterface = COMM_INTERFACE_CAN1;

public:
    CANIOModuleManager( SCHED_TYPE            schedType  ,
                        lu_uint32_t           priority   ,
                        CANIOModuleFactory   *factory    ,
                        ConfigurationManager &cManager
                      );

    virtual ~CANIOModuleManager();

    /**
	 * \brief Start module discovery procedure
	 * 
	 * \return none
	 */
    virtual IOM_ERROR discover();

    /* Override*/
    virtual void setBootloaderMode(lu_bool_t mode);

    /* Override*/
    virtual void setPowerSaveMode(lu_bool_t mode);

    /**
     * \brief Stop all modules
     *
     * \return Error Code
     */
    virtual IOM_ERROR stopAllModules();

#if DEBUG_HBEAT_ENABLED
    void setHBeatEnable(lu_uint8_t x);
#endif

    /**
     * \brief Restart all CAN modules
     *
     * Restart all the present slave modules by using a broadcast message.
     *
     * \return Error code
     */
    CAN_ERROR restartAllModules();

    /**
     * \brief Force the creation of a module
     *
     * Creates a module even if it's not connected
     *
     * \param module Module type
     * \param moduleId Module Identifier (address)
     * \param iFace Interface where the module is supposed to be located.
     *
     * \return Pointer to the module. NULL if the module doesn't exist
     */
    virtual IIOModule *createModule(MODULE module, MODULE_ID moduleID, COMM_INTERFACE iFace);

    /**
     * \brief Get a module reference
     *
     * \param module Module type
     * \param moduleId Module Identifier (address)
     *
     * \return Pointer to the module. NULL if the module doesn't exist
     */
    virtual IIOModule *getModule(MODULE module, MODULE_ID moduleID);

	/**
	 * \brief Get a list of all modules.
	 * 
	 * \return Module list. Empty list when error.
	 */
	virtual std::vector<IIOModule*> getAllModules();

    /**
     * \brief Get a channel reference
     *
     * \param module Module type
     * \param moduleId Module Identifier (address)
     * \param chType Channel type
     * \param chNumber Channel Number
     *
     * \return Pointer to the channel. NULL if the channel/module doesn't exist
     */
    virtual IChannel *getChannel( MODULE module,
                                  MODULE_ID moduleID,
                                  CHANNEL_TYPE chType,
                                  lu_uint32_t chNumber
                                );

private:
    CANFraming    canPSocket;
    CANFraming    canSSocket;
    TSynchManager tsynch;
    CANIOModuleDB modules;  //Modules database

    CANPeriodicData periodicThread;
    CANEventManager eventManager;
    lu_bool_t powerSaving;          //Power Saving mode status
    lu_bool_t HMIStatus;            //HMI up/down status
    Logger& log;
};


#endif // !defined(EA_A3CFE674_CA84_43f8_9602_EC37680B2470__INCLUDED_)

/*
 *********************** End of file ******************************************
 */

