/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: SwitchCANChannelDSM.cpp 19 Jun 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/CANIOModule/src/channel/SwitchCANChannelDSM.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 19 Jun 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19 Jun 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "SwitchCANChannelDSM.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
IOM_ERROR SwitchCANChannelDSM::getConfiguration(SwitchCANChannelDSMConf& configuration)
{
    configuration = config;

    return IOM_ERROR_NONE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
IOM_ERROR SwitchCANChannelDSM::configure()
{
    IIOModule::sendReplyStr message;
    DualSwitchConfigStr confCmd;
    PayloadRAW payload;

    /* Prepare command */
    confCmd.channel       = this->getID().id;
    confCmd.enable 		  = config.enable;
    confCmd.pulseLengthMS = config.pulseLengthMs;
    confCmd.overrunMS     = config.overrunMS;
    confCmd.opTimeoutS    = config.opTimeoutS;
    confCmd.inhibitBI     = config.inhibit;

    /* Create payload */
    payload.payloadPtr = reinterpret_cast<lu_uint8_t*>(&confCmd);
    payload.payloadLen = sizeof(confCmd);

    /* Prepare message */
    message.messageType     = MODULE_MSG_TYPE_CFG;
    message.messageID       = MODULE_MSG_ID_CFG_DUAL_SWITCH_C;
    message.messagePtr      = &payload;
    message.rMessageType    = MODULE_MSG_TYPE_CFG;
    message.rMessagePtr     = NULL;
    message.rMessageID      = MODULE_MSG_ID_CFG_DUAL_SWITCH_R;
    message.timeout         = CANChannel::CAN_MSG_TIMEOUT;

    return sendCANConfiguration(message);
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
