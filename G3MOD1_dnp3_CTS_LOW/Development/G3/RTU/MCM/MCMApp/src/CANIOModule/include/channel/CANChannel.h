/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN channel module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   27/06/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_96945FBE_6148_4072_9E57_028B6AC719D0__INCLUDED_)
#define EA_96945FBE_6148_4072_9E57_028B6AC719D0__INCLUDED_


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "Mutex.h"
#include "IOModuleCommon.h"
#include "IChannel.h"
#include "IIOModule.h"  //SendReply Message
#include "ModuleMessage.h"
#include "Logger.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/**
 * The retrying times when sending configuration
 */
#define CONFIGURE_RETRY_TIMES 5

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/* Forward declaration */
class CANIOModule;

class CANChannel : public IChannel
{
public:
    //deprecated in favour of CAN_MSG_TIMEOUT
    /** CAN message default timeout sec */
    static const lu_uint32_t CAN_MSG_TIMEOUT_SEC  = 0;
    /** CAN message default timeout usec */
    static const lu_uint32_t CAN_MSG_TIMEOUT_USEC = 400 * 1000;//50 * 1000;

    /** CAN message reply default timeout */
    static const timeval CAN_MSG_TIMEOUT;

    /** CAN message default timeout between select and operate, in secs */
    static const lu_uint8_t CAN_MSG_SELECT_TIMEOUT_SEC  = 1;

    //Common strings for operations, commands, etc.
    static const lu_char_t* Select_CString;
    static const lu_char_t* Operate_CString;
    static const lu_char_t* Cancel_CString;

public:
    CANChannel(CHANNEL_TYPE type, lu_uint16_t id, CANIOModule *board);
    ~CANChannel();

    /**
     * \brief Send the stored configuration to the channel
     *
     *  Default implementation: Do nothing
     *
     * \return Error code
     */
    virtual IOM_ERROR sendConfiguration();

    /* FIXME: pueyos_a - CHANNEL setBoard() and setID() to be deprecated in favour of the constructor itself? */

    /**
     * \brief Set the channel's board
     *
     * \param board The board
     *
     * \return None
     */
    void setBoard(CANIOModule *board);

    /**
     * \brief Set channel ID
     *
     * \param ID Channel ID
     *
     * \return None
     */
    void setID(lu_uint16_t ID)
    {
        this->ch_id.id = ID;
        updateName();
    }

    /**
     * \brief Read the channel value
     *
     * Default implementation: operation unsupported
     *
     * \param valuePtr where the data is saved
     *
     * \return Error code
     */
    virtual IOM_ERROR read(IChannel::ValueStr& valuePtr);

    /**
     * \brief Write to the channel
     *
     * Default implementation: operation unsupported
     *
     * \param valuePtr Data to write
     *
     * \return Error code
     */
    virtual IOM_ERROR write(IChannel::ValueStr& valuePtr);

    /**
     * \brief Decode the message
     *
     * The internal status is updated. If necessary all the registered observers are
     * also notified
     *
     * Default implementation: operation unsupported
     *
     * \param event Event used to update the channel
     *
     * \return Error code
     */
    virtual IOM_ERROR decode(ModuleMessage* message);

    /* Overrides IChannel */
    virtual void setActive(const lu_bool_t onlineStatus);

protected:
    /* Overrides IChannel */
    virtual void updateObservers()
    {
        if(m_enabled)
        {
            IChannel::updateObservers();
        }
    }

    /**
     * \brief Send the stored configuration to the channel
     *
     *  Default implementation: Do nothing
     *
     * \return Error code
     */
    virtual IOM_ERROR configure() = 0;

    /**
     * \brief Locally store the board type and id
     *
     * We store the type and id locally to improve the runtime performance.
     * We avoid calling a function every time we need to log something
     * \return None
     */
    void updateModuleID();

    /**
     * \brief Sends a CAN SendReply command
     *
     * IMPORTANT: this expects the reply to be a ModReplyStr ONLY.
     * CAUTION: in the first method the rMessage is DELETED.
     *
     * \param message SendReply Message to send and check.
     * \param command CAN command message
     * \param msgType CAN Message type
     * \param msgID_C CAN Message ID for command
     * \param msgID_C CAN Message ID for expected reply
     *
     * \return Error code
     *
     * \example: For using the templated definition:
     *          SupplyOperateStr operateCmd( ...filling fields... );
     *          IOM_ERROR iom_error = sendCANSendReply(operateCmd,
     *                                      MODULE_MSG_TYPE_CMD,
     *                                      MODULE_MSG_ID_CMD_PSC_CH_OPERATE_C,
     *                                      MODULE_MSG_ID_CMD_PSC_CH_OPERATE_R);
     */
    IOM_ERROR sendCANSendReply(IIOModule::sendReplyStr& message);

    template <typename MessageType>
    IOM_ERROR sendCANSendReply(MessageType& command,
                                const MODULE_MSG_TYPE msgType,
                                const MODULE_MSG_ID msgID_C,
                                const MODULE_MSG_ID msgID_R)
    {
        ModReplyStr reply;  //Unused for ModReplyStr messages
        return sendFullCANSendReply(command, reply, msgType, msgID_C, msgID_R);
    }

    /**
     * \brief Sends a CAN command and get the reply
     *
     * IMPORTANT: This expects the reply to be a ModReplyStr-compatible, that
     *            is, the reply must have 'channel' and 'status' fields.
     *
     * \param message CAN message to send and check.
     * \param reply Obtains here the CAN command reply content
     * \param msgType CAN Message type
     * \param msgID_C CAN Message ID for command
     * \param msgID_C CAN Message ID for expected reply
     *
     * \return Error code
     *
     * \example:
     *          SupplyCancelStr cancelCmd( ...filling fields... );
     *          SupplyCancelReplyStr cancelReplyCmd;
     *          IOM_ERROR iom_error = sendFullCANSendReply(cancelCmd,
     *                                      cancelReplyCmd,
     *                                      MODULE_MSG_TYPE_CMD,
     *                                      MODULE_MSG_ID_CMD_PSC_CH_CANCEL_C,
     *                                      MODULE_MSG_ID_CMD_PSC_CH_CANCEL_R);
     *          peakCurrent = cancelReplyCmd.peakCurrent;
     */
    template <typename MessageType, typename MessageReplyType>
    IOM_ERROR sendFullCANSendReply(MessageType& command,
                                    MessageReplyType& reply,
                                    const MODULE_MSG_TYPE msgType,
                                    const MODULE_MSG_ID msgID_C,
                                    const MODULE_MSG_ID msgID_R)
    {

        /* Prepare message */
        PayloadRAW payload;
        payload.payloadPtr = reinterpret_cast<lu_uint8_t*>(&command);
        payload.payloadLen = sizeof(command);

        IIOModule::sendReplyStr message;
        message.messageType  = msgType;
        message.messageID    = msgID_C;
        message.messagePtr   = &payload;
        message.rMessageType = msgType;
        message.rMessagePtr  = NULL;
        message.rMessageID   = msgID_R;
        message.timeout      = CANChannel::CAN_MSG_TIMEOUT;

        /* Send the message */
        IOM_ERROR ret = sendCANMsg(message);    //Send the SendReply
        if(ret == IOM_ERROR_NONE)
        {
            ret = processReplyStatus(message, reply); //Copies return message on reply
            /* Release reply message */
            if(message.rMessagePtr != NULL)
            {
                delete message.rMessagePtr;
            }
        }
        return ret;
    }

    /**
     * \brief Sends a CAN configuration-specific SendReply command
     *
     * IMPORTANT: this expects the reply to be a ModReplyStr ONLY.
     * CAUTION: in this method the rMessage is DELETED.
     *
     * \param message SendReply Message to send and check.
     * \param retries Amount of retries to send the message
     *
     * \return result of the operation
     */
    IOM_ERROR sendCANConfiguration(IIOModule::sendReplyStr& message, const lu_uint32_t retries = 0);

    /**
     * \brief Check if the module is active and logs an error when not
     *
     * \param commandMessage C-style string with the type of command issued.
     *
     * \return result of the operation: IOM_ERROR_OFFLINE when not active.
     */
    IOM_ERROR checkActive(const lu_char_t* commandMessage);

    /**
     * \brief Gets a String for logging command operations
     *
     * This obtains a String to add to a log, equivalent to the format:
     *      "%s %s/%s send %s %s command: ID %d, local %i",
                __AT__, moduleID.toString().c_str(), this->getName(),
                opType, operation, transactionID, local
     *
     * \param opType String explaining the type of the operation
     * \param operation String describing the operation
     * \param local Local/Remote status: LU_TRUE for Local
     * \param transactionID ID of the transaction, if any
     *
     * \return String filled with the fields
     *
     * \example: logSendCommand("MSupply", CANChannel::Cancel_CString, LU_TRUE, 4);
     *    obtains:
     *      "DSM1/Switch output Channel:3 send MSupply Cancel command: ID 4, local 1"
     */
    virtual const std::string logSendCommand(const lu_char_t* opType,
                                            const lu_char_t* operation,
                                            const lu_bool_t local = 0,
                                            const lu_uint32_t transactionID = 0
                                            );

private:
    IOM_ERROR sendCANMsg(IIOModule::sendReplyStr& message);

    template <typename MessageReplyType>
    IOM_ERROR processReplyStatus(IIOModule::sendReplyStr& message, MessageReplyType& reply)
    {
        IOM_ERROR ret = IOM_ERROR_CMD;
        PayloadRAW* payload = message.messagePtr;

        /* check reply */
        message.rMessagePtr->getPayload(payload);
        if(payload->payloadLen != MODULE_MESSAGE_SIZE(reply))
        {
            log.error("%s %s/%s reply size error: %i (expected %i)",
                        __AT__, moduleID.toString().c_str(), this->getName(),
                        payload->payloadLen, MODULE_MESSAGE_SIZE(ModReplyStr)
                      );

            ret = IOM_ERROR_INVALID_MSG;
        }
        else
        {
            /* Valid size - Decode basic message: channel & status */
            MessageReplyType* replyPtr = reinterpret_cast<MessageReplyType*>(payload->payloadPtr);
            if(replyPtr->channel != this->getID().id )
            {
                log.error("%s %s/%s wrong reply channel: %i",
                            __AT__, moduleID.toString().c_str(), this->getName(), replyPtr->channel
                          );

                ret = IOM_ERROR_REPLY;
            }
            else
            {
                ret = REPLY_STATUS_to_IOM_ERROR(static_cast<REPLY_STATUS>(replyPtr->status));
            }
            if(ret == IOM_ERROR_NONE)
            {
                log.info("%s %s/%s ACK",
                            __AT__, moduleID.toString().c_str(), this->getName(), replyPtr->channel
                          );
            }
            else
            {
                log.warn("%s %s/%s NACK code %i: %s",
                            __AT__, moduleID.toString().c_str(), this->getName(),
                            replyPtr->status, REPLY_STATUS_ToSTRING(replyPtr->status)
                          );
            }
            reply = *replyPtr;
        }
        return ret;
    }

protected:
    CANIOModule* board;
    IOModuleIDStr moduleID;
    Logger& log;

    bool m_enabled;   //A disabled channel does not update its observers
};


#endif // !defined(EA_96945FBE_6148_4072_9E57_028B6AC719D0__INCLUDED_)


/*
 *********************** End of file ******************************************
 */
