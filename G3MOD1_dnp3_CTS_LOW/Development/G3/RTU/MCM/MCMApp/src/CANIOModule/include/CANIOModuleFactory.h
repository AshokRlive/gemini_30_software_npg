/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_65BF4E1E_9807_46e2_9497_A200B7CC8026__INCLUDED_)
#define EA_65BF4E1E_9807_46e2_9497_A200B7CC8026__INCLUDED_


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "GlobalDefs.h"//MG
#include "CANIOModule.h"
#include "ICANChannelConfigurationFactory.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class CANIOModuleFactory
{
public:
    virtual ~CANIOModuleFactory() {};

    static CANIOModuleFactory *getInstance(){return &instance;};

    /**
     * \brief Return a new IOBoard.
     *
     * This is the "Parametrized Factory Method" of the "Abstract Factory" class
     *
     * \param type Board type
     * \param type Board ID
     * \param interface Communication interface
     * \param moduleFactory Reference to the channel configuration factory
     *
     * \return New CAN Module pointer
     */
    virtual CANIOModule *newBoard( MODULE type ,
                                   MODULE_ID id,
                                   COMM_INTERFACE interface,
                                   TSynchManager &tsynch,
                                   ICANModuleConfigurationFactory &moduleFactory
                                 );

private:

    static CANIOModuleFactory instance;

    CANIOModuleFactory() {};
};
#endif // !defined(EA_65BF4E1E_9807_46e2_9497_A200B7CC8026__INCLUDED_)

/*
 *********************** End of file ******************************************
 */



