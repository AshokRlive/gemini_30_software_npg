/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       HMI Channel public interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   20/02/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_48D7FFF0_F399_41f9_AC09_14E9F05DD016__INCLUDED_)
#define EA_48D7FFF0_F399_41f9_AC09_14E9F05DD016__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "CANChannel.h"
#include "CANIOModule.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Container class for the variable length message: HmiDisplayWriteStr
 */
class LCDData
{

public:
    /**
     * \brief Custom constructor
     *
     * The internal buffer is dynamically allocated using the new operator.
     * The buffer is deallocated by the destructor. Enough space is reserved
     * for the HmiDisplayWriteStr CAN message header.
     *
     * \param bufferSize Size of the internal buffer
     *
     * \return None
     */
    LCDData(lu_uint16_t bufferSize);
    virtual ~LCDData();

    /**
     * \brief Get the string buffer content, without header
     *
     * \param buffer Where the RAW buffer reference is saved
     *
     * \return Error Code
     */
    IOM_ERROR getLCDBuffer(PayloadRAW& buffer);

    /**
     * \brief Set the string buffer start point and size
     *
     * \param posRow String start row
     * \param posColumn String start column
     * \param len String length
     *
     * \return Error Code
     */
    IOM_ERROR setPosition( lu_uint8_t posRow,
                           lu_uint8_t posColumn,
                           lu_uint8_t len
                         );

    /**
     * \brief Get the string buffer start point and size
     *
     * \param posRow String start row
     * \param posColumn String start column
     * \param len String length
     *
     * \return Error Code
     */
    IOM_ERROR getPosition( lu_uint8_t &posRow,
                           lu_uint8_t &posColumn,
                           lu_uint8_t &len
                         );

protected:
    /* HMI class is allowed to get the RAW payload */
    friend class HMICANChannel;

    /**
     * \brief Get the RAW buffer
     *
     * The buffer also contains the HmiDisplayWriteStr header. The values
     * used in the header are the one set using the setPosition function
     *
     * \param buffer Where the RAW buffer reference is saved
     *
     * \return Error Code
     */
    IOM_ERROR getRAWBuffer(PayloadRAW& buffer);

private:
    /**
     * \brief Re-encode buffer content for LCD mapping
     *
     * This substitutes ASCII chars by character codes in the LCD pattern.
     * Currently, only HD44780U ROM code A00 is supported.
     */
    virtual void reEncode();

private:
    lu_uint16_t bufferSize;
    lu_uint8_t *buffer;
};


class HMICANChannel : public CANChannel
{
public:
    enum HMI_EVENT_TYPE
    {
        HMI_EVENT_TYPE_NONE = 0,
        HMI_EVENT_TYPE_BUTTON  ,
        HMI_EVENT_TYPE_OLR     ,

        HMI_EVENT_TYPE_LAST
    };
public:

    /**
     * \brief Custom constructor
     */
    HMICANChannel(lu_uint16_t ID = 0, CANIOModule *board = NULL);

    virtual ~HMICANChannel() {};

    /* === Inherited from CANChannel === */
    virtual IOM_ERROR decode(ModuleMessage* message);

    /**
     * \brief Configure the channel
     *
     * The configuration is stored locally.
     *
     * \param conf Reference to the channel configuration
     *
     * \return error Code
     */
    void setConfig(HMIConfigStr &conf){this->conf = conf;}


    /*****************************/
    /* HMI proprietary interface */
    /*****************************/

    /**
     * \brief Get display information
     *
     * \param timeout CAN command timeout
     * \param numRows Buffer where the number of rows is saved
     * \param numColums Buffer where the number of columns is saved
     *
     * \return Error Code
     */
    IOM_ERROR getDisplayInfo( struct timeval &timeout,
                              lu_int8_t      &numRows,
                              lu_int8_t      &numColums
                            );

    /**
     * \brief Poll the buttons status
     *
     * \param timeout CAN command timeout
     * \param buttonBitMask Buffer where buttons status is saved. Use the
     *        HMI_BUTTON_BIT_MASK enum to decode the bitmask
     *
     * \return Error Code
     */
    IOM_ERROR getButtons(struct timeval &timeout, lu_uint32_t &buttonBitMask);

    /**
     * \brief Set the HMI leds
     *
     * \param ledBitMask LEDs bitmask. Use the HMI_LED_BIT_MASK
     *        enum to set the bitmask
     *
     * \return Error Code
     */
    IOM_ERROR setLeds(lu_uint32_t ledBitMask);

    /**
     * \brief Set the Off/Local/Remote state/LED
     *
     * \param state Off/Local/Remote state
     *
     * \return Error Code
     */
    IOM_ERROR setOLRState(HMI_OLR_STATE state);

    /**
     * \brief Write a string to the LCD
     *
     * The string is saved in a local buffer of the HMI and not displayed.
     * Send the synch command to update the LCD
     *
     * \param timeout CAN command timeout
     * \param data Buffer where the string to write is contained
     *
     * \return Error Code
     */
    IOM_ERROR writeLCD(struct timeval &timeout, LCDData &data);

    /**
     * \brief Update the LCD screen
     *
     * \param timeout CAN command timeout
     * \param fullSync If true the whole buffer is written. If false only the
     * differences between the LCD and the internal buffer are written
     *
     * \return Error Code
     */
    IOM_ERROR synchLCD(struct timeval &timeout, lu_bool_t fullSync = LU_FALSE);

    /**
     * \brief Clear a portion of the LCD
     *
     * To activate the changes a synch command must be sent.
     * If numChars is 0 the whole display is cleared (in this case the screen
     * is cleared straight away without waiting for the synch command)
     *
     * \param timeout CAN command timeout
     * \param posRow Start Row
     * \param posColumn Start column
     * \param numChars Number of characters to clear starting from posRow/posColumn
     *
     * \return Error Code
     */
    IOM_ERROR clearLCD( struct timeval &timeout,
                        lu_uint8_t posRow,
                        lu_uint8_t posColumn,
                        lu_uint8_t numChars
                      );

    /**
     * \brief Clear the entire LCD
     *
     * The screen is cleared straight away without waiting for
     * the synch command. It's a wrapper for clearLCD(timeout, x, x, 0)
     *
     * \param timeout CAN command timeout
     *
     * \return Error Code
     */
    IOM_ERROR clearLCD(struct timeval &timeout);

    /**
     * \brief Play a sound
     *
     * \param timeout CAN command timeout
     * \param frequencyHz Sound frequency in Hz
     * \param durationMs Sound duration is seconds
     *
     * \return Error Code
     */
    IOM_ERROR playSound( struct timeval &timeout,
                         lu_int16_t frequencyHz,
                         lu_int16_t durationMs
                       );

    /**
     * \brief Get the last event type recorded
     *
     * \return Last event type recorded
     */
    HMI_EVENT_TYPE getLastEvent();

    /**
     * \brief Get the button status
     *
     * The status is read from the local buffer (updated using events)
     *
     * \param state Buffer where the buttons status is saved
     *
     * \return Error Code
     */
    IOM_ERROR getButtonsEvent(lu_uint32_t &state);

    /**
     * \brief Get the last OLR request
     *
     * The status is read from the local buffer (updated using events)
     *
     * \param olrRequest Buffer where the request is saved
     *
     * \return Error Code
     */
    IOM_ERROR getOlrRequest(HMI_OLR_STATE &olrRequest);

protected:
    /* === Inherited from CANChannel === */
    virtual IOM_ERROR configure();

private:
    IOM_ERROR handleButtonEvent(ModuleMessage *message);
    IOM_ERROR handleOLREvent(ModuleMessage *message);

    HMIConfigStr conf;

    HMI_EVENT_TYPE lastEvent;
    lu_uint32_t buttonState;
    HMI_OLR_STATE olrRequest;

};


#endif // !defined(EA_48D7FFF0_F399_41f9_AC09_14E9F05DD016__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
