/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN Digital Input channel module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   04/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_B34A1EB7_8DEA_42dc_B4F9_330BC8A7EB77__INCLUDED_)
#define EA_B34A1EB7_8DEA_42dc_B4F9_330BC8A7EB77__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "DigitalCANChannel.h"
#include "TimeManager.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/**
 * \brief Input digital channel configuration structure
 */
struct InputDigitalCANChannelConf
{

public:
    lu_bool_t enable;
    lu_bool_t extEquipInvert;
    lu_bool_t eventEnable;
    lu_uint16_t dbHigh2LowMs;
    lu_uint16_t dbLow2HighMs;

public:
    InputDigitalCANChannelConf() : //Constructor: set default values
            enable(LU_FALSE),
            extEquipInvert(LU_FALSE),
            eventEnable(LU_FALSE),
            dbHigh2LowMs(0),
            dbLow2HighMs(0)
    {}
};

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class InputDigitalCANChannel : public DigitalCANChannel
{

public:
    InputDigitalCANChannel(lu_uint16_t ID = 0,
                            CANIOModule *board = NULL
                            );
    virtual ~InputDigitalCANChannel();

    /* === Inherited from CANChannel === */
    virtual IOM_ERROR read(IChannel::ValueStr& valuePtr);
    virtual IOM_ERROR decode(ModuleMessage* message);

    /**
	 * \brief Configure the channel
	 * 
	 * the configuration is stored locally and also sent to the slave module
	 * 
	 * \param conf Reference to the channel configuration
	 * 
	 * \return error Code
	 */
    void setConfig(InputDigitalCANChannelConf &conf){this->conf = conf;}

protected:
    /* === Inherited from CANChannel === */
    virtual IOM_ERROR configure();

private:
    IOM_ERROR handleDEvent(ModuleMessage* message);

private:
    lu_int8_t currentValue;
    lu_int8_t previousValue;
    TimeManager::TimeStr timeStamp;     //Time stamp of the last DI change
	InputDigitalCANChannelConf conf;    //DI Channel configuration
};


#endif // !defined(EA_B34A1EB7_8DEA_42dc_B4F9_330BC8A7EB77__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
