/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       PSM Module class implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "GenericIOModule.h"
#include "ModuleProtocol.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */



GenericIOModule::GenericIOModule( MODULE  module                        ,
                          MODULE_ID id                                 ,
                          COMM_INTERFACE iFace                         ,
                          TSynchManager &tsynch                        ,
                          ICANModuleConfigurationFactory &moduleFactory
                        ) :
                            CANIOModule(module, id, iFace, tsynch, moduleFactory)
{

    initStateMachine();
}


GenericIOModule::~GenericIOModule(){

}


IChannel *GenericIOModule::getChannel(CHANNEL_TYPE type, lu_uint32_t number)
{
    LU_UNUSED(number);
    IChannel *channelPtr = NULL;

    log.error("GenericIOModule::getChannel: Unsupported channel: %i. Function: %s",
               type, __FUNCTION__
             );

    return channelPtr;
}


IOM_ERROR GenericIOModule::tickEvent(lu_uint32_t dTime)
{
    /* Call parent tick function */
    return CANIOModule::tickEvent(dTime);
}


IOM_ERROR GenericIOModule::decodeMessage(ModuleMessage* message)
{
    IOM_ERROR ret = IOM_ERROR_NONE;

    if(message == NULL)
    {
        return IOM_ERROR_PARAM;
    }

    ret = CANIOModule::decodeMessage(message);    /* General message */

    if(ret == IOM_ERROR_NOT_SUPPORTED)
    {
        log.warn("GenericIOModule::decodeMessage: (CAN ID %i) Unsupported message: 0x%02x:0x%02x",
                        mid.id,
                        message->header.messageType,
                        message->header.messageID
                       );
    }
    return ret;
}

IOM_ERROR GenericIOModule::configure()
{
    return IOM_ERROR_NONE;
}

IOM_ERROR  GenericIOModule::loadConfiguration()
{
    return IOM_ERROR_NONE;
}

void GenericIOModule::stop()
{
    // Loop around channels calling stopUpdate()
    //channel.stopUpdate();
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void GenericIOModule::setAllChsActive(lu_bool_t status)
{
    LU_UNUSED(status);
    //Generic Module does not have any predefined channel
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
