/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCM
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       IOM module implementation - Input/Output Module.
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   20/09/12      pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "IOMIOModule.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */


IOMIOModule::IOMIOModule(MODULE_ID id,
                            COMM_INTERFACE iFace,
                            TSynchManager& tsynch,
                            ICANModuleConfigurationFactory& moduleFactory
                            ):
                                CANIOModule(MODULE_IOM, id, iFace, tsynch, moduleFactory)
{
    lu_uint32_t i;

    /* Full channels initialisation: Binary Input */
    for(i = 0; i < IOM_CH_DINPUT_LAST; i++)
    {
         BInput[i].setBoard(this);
         BInput[i].setID(i);
    }

    /* Full channels initialisation: Analogue Input */
    for(i = 0; i < IOM_CH_AINPUT_LAST; i++)
    {
        AInput[i].setBoard(this);
        AInput[i].setID(i);
    }

    /* Full channels initialisation: Digital Output */
    for(i = 0; i < IOM_CH_DOUT_LAST; i++)
    {
        DOutput[i].setBoard(this);
        DOutput[i].setID(i);
    }

    initStateMachine();
}

IOMIOModule::~IOMIOModule()
{}


IChannel* IOMIOModule::getChannel(CHANNEL_TYPE type, lu_uint32_t number)
{
    IChannel *channelPtr = NULL;

    switch(type)
    {
        case CHANNEL_TYPE_DINPUT:
            if(number < IOM_CH_DINPUT_LAST)
            {
                channelPtr = &BInput[number];
            }
        break;

        case CHANNEL_TYPE_AINPUT:
            if(number < IOM_CH_AINPUT_LAST)
            {
                channelPtr = &AInput[number];
            }
        break;

        case CHANNEL_TYPE_DOUTPUT:
            if(number < IOM_CH_DOUT_LAST)
            {
                channelPtr = &DOutput[number];
            }
        break;

        default:
            log.error("IOMIOModule::getChannel: "
                            "Unsupported CAN channel: %i. Function: %s",
                            type, __FUNCTION__
                          );
        break;
    }

    return channelPtr;
}


IOM_ERROR IOMIOModule::tickEvent(lu_uint32_t dTime)
{
    /* Call parent tick function */
    return CANIOModule::tickEvent(dTime);
}


IOM_ERROR IOMIOModule::decodeMessage(ModuleMessage* message)
{
    IOM_ERROR ret = IOM_ERROR_NONE;

    if(message == NULL)
    {
        return IOM_ERROR_PARAM;
    }

    /* General message ? */
    ret = CANIOModule::decodeMessage(message);
    if (ret == IOM_ERROR_NOT_SUPPORTED)
    {
        /* No - Try custom decoder */
        switch(message->header.messageType)
        {
            /* Forward the event to the channel */
            case MODULE_MSG_TYPE_EVENT:
                switch(message->header.messageID)
                {
                    case MODULE_MSG_ID_EVENT_DIGITAL:
                        CANIOModule::handleDEvent(message, BInput, IOM_CH_DINPUT_LAST);
                        ret = IOM_ERROR_NONE;
                    break;

                    case MODULE_MSG_ID_EVENT_ANALOGUE:
                        CANIOModule::handleAEvent(message, AInput, IOM_CH_AINPUT_LAST);
                        ret = IOM_ERROR_NONE;
                    break;

                    default:
                        ret = IOM_ERROR_NOT_SUPPORTED;
                    break;
                }
            break;

            default:
                ret = IOM_ERROR_NOT_SUPPORTED;
            break;
        }
    }

    if(ret == IOM_ERROR_NOT_SUPPORTED)
    {
        log.warn("IOMIOModule::decodeMessage: "
                        "%s Unsupported message: 0x%02x:0x%02x",
                        getName(),
                        message->header.messageType,
                        message->header.messageID
                       );
    }
    return ret;
}


IOM_ERROR IOMIOModule::loadConfiguration()
{
    const lu_char_t * ErrMSG = "%s failed to load configuration for %s. Error %i: %s";

    lu_uint32_t ch_id;
    IOM_ERROR err = IOM_ERROR_NONE;
    IOM_ERROR ret;

    if (configFactory.isConfigured(mid) == LU_FALSE)
    {
        log.warn("%s-was not found in configuration", getName());
        return IOM_ERROR_NOT_FOUND;
    }

    /* Load all Digital Input */
    InputDigitalCANChannelConf diConf;
    for (ch_id = 0; ch_id < IOM_CH_DINPUT_LAST; ch_id++)
    {
        ret = configFactory.getIDigitalConf(mid.type, mid.id, ch_id, diConf);
        if (ret == IOM_ERROR_NONE)
            BInput[ch_id].setConfig(diConf);
        else
        {
            err = ret;
            log.error(ErrMSG,getName(), BInput[ch_id].getName(), err, IOM_ERROR_ToSTRING(err));
        }
    }

    /* Load all Analogue Input */
    InputAnalogueCANChannelConf aiConf;
    for (ch_id = 0; ch_id < IOM_CH_AINPUT_LAST; ch_id++)
    {
        ret = configFactory.getIAnalogueConf(mid.type, mid.id, ch_id, aiConf);
        if (ret == IOM_ERROR_NONE)
            AInput[ch_id].setConfig(aiConf);
        else
        {
            err = ret;
            log.error(ErrMSG,getName(), AInput[ch_id].getName(), err, IOM_ERROR_ToSTRING(err));
        }
    }

    /* Load all Digital Output channels */
    OutputDigitalCANChannelConf doConf;
    for (ch_id = 0; ch_id < IOM_CH_DOUT_LAST; ch_id++)
    {
        ret = configFactory.getDigitalOutConf(mid.type, mid.id, ch_id, doConf);
        if (ret == IOM_ERROR_NONE)
            DOutput[ch_id].setConfig(doConf);
        else
        {
            err = ret;
            log.error(ErrMSG,getName(), DOutput[ch_id].getName(), err, IOM_ERROR_ToSTRING(err));
        }
    }
    return err;
}


IOM_ERROR IOMIOModule::configure()
{
    lu_bool_t hasFailure = LU_FALSE;
    lu_uint32_t i;
    IOM_ERROR ret;

    /* Configure all Digital Input */
    for(i = 0; i < IOM_CH_DINPUT_LAST; i++)
    {
        ret = BInput[i].sendConfiguration();
        LOG_CH_CONFIG_FAILURE(ret,"DInput");
    }

    /* Configure all Analogue Input */
    for(i = 0; i < IOM_CH_AINPUT_LAST; i++)
    {
        ret = AInput[i].sendConfiguration();
        LOG_CH_CONFIG_FAILURE(ret,"AInput");
    }

    /* Configure all Digital Output channels */
    for(i = 0; i < IOM_CH_DOUT_LAST; i++)
    {
        ret = DOutput[i].sendConfiguration();
        LOG_CH_CONFIG_FAILURE(ret,"DOutput");
    }

    return (hasFailure == LU_TRUE)? IOM_ERROR_CONFIG:IOM_ERROR_NONE;
}


void IOMIOModule::stop()
{
    lu_int32_t i;

    /* Stop all Binary Input channels */
    for(i = 0; i < IOM_CH_DINPUT_LAST; i++)
    {
         BInput[i].stopUpdate();
    }

    /* Stop all Analogue Input channels */
    for(i = 0; i < IOM_CH_AINPUT_LAST; i++)
    {
        AInput[i].stopUpdate();
    }

    /* Stop all Output channels */
    for(i = 0; i < IOM_CH_DOUT_LAST; i++)
    {
        DOutput[i].stopUpdate();
    }
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void IOMIOModule::setAllChsActive(lu_bool_t status)
{
    lu_uint32_t i;

    /* Binary Input */
    for(i = 0; i < IOM_CH_DINPUT_LAST; i++)
    {
         BInput[i].setActive(status);
    }

    /* Analogue Input */
    for(i = 0; i < IOM_CH_AINPUT_LAST; i++)
    {
        AInput[i].setActive(status);
    }

    /* Digital Output */
    for(i = 0; i < IOM_CH_DOUT_LAST; i++)
    {
        DOutput[i].setActive(status);
    }
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */

