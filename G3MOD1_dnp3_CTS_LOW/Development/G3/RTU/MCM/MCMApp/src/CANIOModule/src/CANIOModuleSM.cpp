/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:CANIOModuleSM.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   3 Feb 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdio.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "CANIOModuleSM.h"
#include "ModuleRegistrationManager.h"
#include "SysAlarmMCMEnum.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define CHECK_STATUS_FALSE(S) (getStatus(S) == LU_FALSE)
/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
CANIOModuleSM::CANIOModuleSM(ICANIOModule& g_module):
                            SlaveModuleSM(g_module.mid),
                            module(g_module),
                            valid(LU_FALSE)
{}
CANIOModuleSM::~CANIOModuleSM()
{}


void CANIOModuleSM::validateAction(const MInfo& mInfo)
{
    debugAction("validateAction");

    // Check module registered
    MRDB_ERROR ret;
    ret = ModuleRegistrationManager::getInstance()->validateModule(mid.type,mid.id,getModuleUID());
    setRegistered( (ret == MRDB_ERROR_NONE)? LU_TRUE : LU_FALSE );

    // Check UID (serial + supplierID)
    ModuleUID mUID = getModuleUID();
    if(mUID != mInfo.moduleUID)
    {
        log.error("%s validating failed: mismatched UID (serial)! Expected:%s Actual:%02d-%06d",
                        name(),
                        mUID.toString().c_str(),
                        mInfo.moduleUID.supplierId, mInfo.moduleUID.serialNumber
                  );
        valid = LU_FALSE;
    }

    // Check system API
    else if(mInfo.application.systemAPI.major != MODULE_SYSTEM_API_VER_MAJOR)
    {
        log.error("%s validating failed: unsupported system API! Expected major:%i Actual major:%i",
                        name(),
                        MODULE_SYSTEM_API_VER_MAJOR,
                        mInfo.application.systemAPI.major
                 );
        valid = LU_FALSE;
        issueAlarm(LU_TRUE, SYS_ALARM_SUBSYSTEM_MCM, SYSALC_MCM_MODULE_SYSTEMAPI);
    }

    // Check registration
    else if(CHECK_STATUS_FALSE(MSTATUS_REGISTERED))
    {
        log.error("%s validating failed: module not registered!", name());
        valid = LU_FALSE;
    }
    else if(CHECK_STATUS_FALSE(MSTATUS_CONFIGURED))
    {
        log.error("%s validating failed: module not configured!", name());
        valid = LU_FALSE;
//        issueAlarm(LU_TRUE, SYS_ALARM_SUBSYSTEM_MCM, SYSALC_MCM_MODULE_NOT_CFG);
    }
    else
    {
        log.debug("%s validating passed!", name());
        valid = LU_TRUE;
        /* report alarm state */
        issueAlarm(LU_FALSE, SYS_ALARM_SUBSYSTEM_MCM, SYSALC_MCM_MODULE_SYSTEMAPI);
    }
}


lu_bool_t CANIOModuleSM::isValid()
{
    return valid;
}


void CANIOModuleSM::stopEventingAction()
{
    debugAction("stopEventingAction");

    IOM_ERROR ret = module.sendStopEventingCmd();

    if(ret != IOM_ERROR_NONE)
    {
        log.error("%s failed to stop module eventing. Error %i: %s",
                    name(), ret, IOM_ERROR_ToSTRING(ret)
                    );
    }
}


void CANIOModuleSM::loadConfigAction()
{
    debugAction("loadConfigAction");

    IOM_ERROR ret = module.loadConfiguration();

    if(ret != IOM_ERROR_NONE)
    {
        log.error("%s configuration loading failed! Error %i: %s",
                    name(), ret, IOM_ERROR_ToSTRING(ret)
                    );
        setConfigured(LU_FALSE);
        if(ret != IOM_ERROR_NOT_FOUND)
        {
            issueAlarm(LU_TRUE, SYS_ALARM_SUBSYSTEM_MCM, SYSALC_MCM_MODULE_NOT_CFG);
        }
    }
    else
    {
        setConfigured(LU_TRUE);
        issueAlarm(LU_FALSE, SYS_ALARM_SUBSYSTEM_MCM, SYSALC_MCM_MODULE_NOT_CFG);
    }
}


void CANIOModuleSM::sendMInfoRequestAction()
{
    debugAction("sendMInfoRequest");

    module.requestModuleInfo();
}


void CANIOModuleSM::issueAlarm( const lu_bool_t issue,
                                const SYS_ALARM_SUBSYSTEM subSystem,
                                const lu_uint32_t alarmCode)
{
    if(issue == LU_TRUE)
    {
        debugAction("issueAlarm set");
    }
    else
    {
        debugAction("issueAlarm reset");
    }

    if(issue == LU_TRUE)
    {
        module.activateAlarm(subSystem, alarmCode);
    }
    else
    {
        module.deactivateAlarm(subSystem, alarmCode);
    }
}


void CANIOModuleSM::initModuleAction()
{
    debugAction("initModuleAction");

    if(CHECK_STATUS_FALSE(MSTATUS_CONFIGURED))
    {
        log.warn("%s is NOT initialised because it is not configured", name());
        issueAlarm(LU_TRUE, SYS_ALARM_SUBSYSTEM_MCM, SYSALC_MCM_MODULE_NOT_CFG);
        return;
    }
    // Send configuration
    IOM_ERROR ret = module.configure();

    if (ret != IOM_ERROR_NONE)
    {
        log.error("%s NOT initialised because it failed to send configuration. Error %i: %s",
                    name(), ret, IOM_ERROR_ToSTRING(ret)
                    );
        issueAlarm(LU_TRUE, SYS_ALARM_SUBSYSTEM_MCM, SYSALC_MCM_MODULE_NOT_CFG);
    }
    else
    {
        // Send start command
        ret = module.sendStartCmd(ICANIOModule::CAN_MSG_TIMEOUT_MSEC);

        if (ret != IOM_ERROR_NONE)
        {
            log.error("%s initialisation -failed to send \"Start\" command. Error %i: %s",
                    name(), ret, IOM_ERROR_ToSTRING(ret)
                    );
            issueAlarm(LU_TRUE, SYS_ALARM_SUBSYSTEM_MCM, SYSALC_MCM_MODULE_NOT_CFG);
        }
    }
}


void CANIOModuleSM::resetBoardAction()
{
    IOM_ERROR ret = module.sendRestartCmd(MD_RESTART_WARM);

    if(ret == IOM_ERROR_NONE)
    {
        log.warn("Module:%s has been reset!",name());
    }
    else
    {
        log.error("Failed to reset module: %s, error %i: %s",
                    name(), ret, IOM_ERROR_ToSTRING(ret)
                    );
    }
}


void CANIOModuleSM::setAllChsActive(lu_bool_t isActive)
{
    debugAction("setAllChsActive");

    module.setAllChsActive(isActive);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
