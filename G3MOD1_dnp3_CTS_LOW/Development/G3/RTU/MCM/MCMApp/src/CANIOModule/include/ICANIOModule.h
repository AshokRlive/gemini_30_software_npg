/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:ICANIOModule.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11 Feb 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef ICANIOMODULE_H_
#define ICANIOMODULE_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IIOModule.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
class ICANIOModule: public IIOModule
{
public:
    static const lu_uint32_t CAN_MSG_TIMEOUT_MSEC = 50;

    ICANIOModule(MODULE type, MODULE_ID id):IIOModule(type,id){};

    virtual ~ICANIOModule(){};

    /**
    * \brief Send a restart command to all the slave modules
    *
    * \param type Type of restart desired (as warm, cold, etc)
    *
    * \return error code
    */
    virtual IOM_ERROR sendRestartCmd(MD_RESTART type) = 0;

    virtual IOM_ERROR sendStopEventingCmd() = 0;

    virtual IOM_ERROR sendStartCmd(lu_uint32_t timeout) =0;

    virtual void requestModuleInfo() = 0;

    virtual IOM_ERROR loadConfiguration() = 0;

    /**
    * \brief Configure slave board with configuration loaded previously.
    *
    * \return Error code
    */
    virtual IOM_ERROR configure() = 0;

    virtual void setAllChsActive(lu_bool_t isActive) = 0;
};

#endif /* ICANIOMODULE_H_ */

/*
 *********************** End of file ******************************************
 */
