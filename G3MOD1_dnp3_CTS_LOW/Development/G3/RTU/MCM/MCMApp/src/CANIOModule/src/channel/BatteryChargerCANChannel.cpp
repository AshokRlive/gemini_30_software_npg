/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN Battery channel module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   05/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "BatteryChargerCANChannel.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

BatteryChargerCANChannel::BatteryChargerCANChannel( lu_uint16_t ID     ,
                                                     CANIOModule *board
                                                  ) :
                                                  OutputCANChannel(CHANNEL_TYPE_BCHARGER, ID, board)
{}


IOM_ERROR BatteryChargerCANChannel::selOperate(const lu_uint16_t duration, const lu_bool_t local)
{
    /* Start of critical section to prevent select command while doing a select & operate */
    LockingMutex mMutex(sopMutex);
    IOM_ERROR iomRet = sendSelect(local);
    if(iomRet == IOM_ERROR_NONE)
    {
        iomRet = sendOperate(duration, local);
    }
    return iomRet;
}


IOM_ERROR BatteryChargerCANChannel::cancel(const lu_bool_t local)
{
    IIOModule::sendReplyStr message;
    PayloadRAW payload;
    BatteryCancelStr cancelCmd;

    if(checkActive("cancel") != IOM_ERROR_NONE)
    {
        return IOM_ERROR_OFFLINE;
    }

    /* Prepare command */
    cancelCmd.channel = this->getID().id;
    cancelCmd.local   = local;

    payload.payloadPtr = reinterpret_cast<lu_uint8_t*>(&cancelCmd);
    payload.payloadLen = sizeof(cancelCmd);

    message.messageType  = MODULE_MSG_TYPE_CMD;
    message.messageID    = MODULE_MSG_ID_CMD_BAT_CH_CANCEL_C;
    message.messagePtr   = &payload;
    message.rMessageType = MODULE_MSG_TYPE_CMD;
    message.rMessagePtr  = NULL;
    message.rMessageID   = MODULE_MSG_ID_CMD_BAT_CH_CANCEL_R;
    message.timeout      = CANChannel::CAN_MSG_TIMEOUT;

    log.info("%s %s/%s send command(local: %i)",
                    __AT__,  moduleID.toString().c_str(), this->getName(), local
              );

    return sendCANSendReply(message);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
IOM_ERROR BatteryChargerCANChannel::sendSelect(const lu_bool_t local)
{
    IIOModule::sendReplyStr message;
    PayloadRAW payload;
    BatterySelectStr selectCmd;

    if(checkActive("select") != IOM_ERROR_NONE)
    {
        return IOM_ERROR_OFFLINE;
    }

    /* Prepare command */
    selectCmd.channel       = this->getID().id;
    selectCmd.SelectTimeout = CANChannel::CAN_MSG_SELECT_TIMEOUT_SEC;
    selectCmd.local         = local        ;

    payload.payloadPtr = reinterpret_cast<lu_uint8_t*>(&selectCmd);
    payload.payloadLen = sizeof(selectCmd);

    message.messageType  = MODULE_MSG_TYPE_CMD;
    message.messageID    = MODULE_MSG_ID_CMD_BAT_CH_SELECT_C;
    message.messagePtr   = &payload;
    message.rMessageType = MODULE_MSG_TYPE_CMD;
    message.rMessagePtr  = NULL;
    message.rMessageID   = MODULE_MSG_ID_CMD_BAT_CH_SELECT_R;
    message.timeout      = CANChannel::CAN_MSG_TIMEOUT;

    log.info("%s CAN Channel %s/%s send command (local: %i)",
                    __AT__, moduleID.toString().c_str(), this->getName(), local
              );

    return sendCANSendReply(message);
}


IOM_ERROR BatteryChargerCANChannel::sendOperate(const lu_uint16_t duration,
                                                const lu_bool_t local
                                               )
{
    IIOModule::sendReplyStr message;
    PayloadRAW payload;
    BatteryOperateStr operateCmd;

    if(checkActive("operate") != IOM_ERROR_NONE)
    {
        return IOM_ERROR_OFFLINE;
    }

    /* Prepare command */
    operateCmd.channel            = this->getID().id;
    operateCmd.operationDuration  = duration;
    operateCmd.local              = local   ;   //The PSM may ignore this parameter
    operateCmd.unused             = 0;          //Unused by now - set to 0

    payload.payloadPtr = reinterpret_cast<lu_uint8_t*>(&operateCmd);
    payload.payloadLen = sizeof(operateCmd);

    message.messageType  = MODULE_MSG_TYPE_CMD;
    message.messageID    = MODULE_MSG_ID_CMD_BAT_CH_OPERATE_C;
    message.messagePtr   = &payload;
    message.rMessageType = MODULE_MSG_TYPE_CMD;
    message.rMessagePtr  = NULL;
    message.rMessageID   = MODULE_MSG_ID_CMD_BAT_CH_OPERATE_R;
    message.timeout      = CANChannel::CAN_MSG_TIMEOUT;

    log.info("%s CAN Channel %s/%s send command(supplyDuration %i - local: %i)",
                __AT__, moduleID.toString().c_str(), this->getName(), duration, local
              );

    return sendCANSendReply(message);
}


/*
 *********************** End of file ******************************************
 */
