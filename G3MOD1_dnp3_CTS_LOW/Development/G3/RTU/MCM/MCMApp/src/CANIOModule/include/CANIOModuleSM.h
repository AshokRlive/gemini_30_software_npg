/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:CANIOModuleSM.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   3 Feb 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef CANIOMODULESM_H_
#define CANIOMODULESM_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "SlaveModuleSM.h"
#include "ICANIOModule.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief Implemented state machine for CAN slave module.
 */
class CANIOModuleSM : public SlaveModuleSM
{
public:
    CANIOModuleSM(ICANIOModule& g_module);
    virtual ~CANIOModuleSM();

    /* ==Inherited from SlaveModuleSM== */
    virtual const char* name(){return module.getName();}
    virtual void validateAction(const MInfo& mInfo);
    virtual lu_bool_t isValid();
    virtual void loadConfigAction();
    virtual void sendMInfoRequestAction();
    virtual void initModuleAction();
    virtual void resetBoardAction();
    virtual void setAllChsActive(lu_bool_t isActive);
    virtual void stopEventingAction();

protected:
    /* ==Inherited from SlaveModuleSM== */
    virtual void issueAlarm(const lu_bool_t issue,
                            const SYS_ALARM_SUBSYSTEM subSystem,
                            const lu_uint32_t alarmCode);

private:
    ICANIOModule& module;

    lu_bool_t valid;
};

#endif /* CANIOMODULESM_H_ */

/*
 *********************** End of file ******************************************
 */
