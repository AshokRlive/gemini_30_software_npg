/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: FPMIOModule.cpp 15 Jul 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/CANIOModule/src/FPMIOModule.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       FPM Module implementation - Fault Passage (fault indication) Module
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 15 Jul 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15 Jul 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "FPMIOModule.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
FPMIOModule::FPMIOModule(MODULE_ID id,
                         COMM_INTERFACE iFace,
                         TSynchManager& tsynch,
                         ICANModuleConfigurationFactory& moduleFactory
                         ) :
                         CANIOModule(MODULE_FPM, id, iFace, tsynch, moduleFactory)
{
    lu_uint32_t i;

    /* Full channels initialisation: Binary Input */
    for(i = 0; i < FPM_CH_DINPUT_LAST; i++)
    {
        dInput[i].setBoard(this);
        dInput[i].setID(i);
    }

    /* Full channels initialisation: Analogue Input */
    for(i = 0; i < FPM_CH_AINPUT_LAST; i++)
    {
        aInput[i].setBoard(this);
        aInput[i].setID(i);
    }

    /* Full channels initialisation: specialised channels */
    for(i = 0; i < FPM_CH_FPI_LAST; i++)
    {
        outputFPI[i].setBoard(this);
        outputFPI[i].setID(i);
//        switch (i)
//        {
//            case FPM_CH_FPI_1_TEST:
//            case FPM_CH_FPI_2_TEST:
//
//                break;
//            case FPM_CH_FPI_1_RESET:
//            case FPM_CH_FPI_2_RESET:
//            default:
//                break;
//        }
    }

    initStateMachine();
}

FPMIOModule::~FPMIOModule()
{}


IChannel* FPMIOModule::getChannel(CHANNEL_TYPE type, lu_uint32_t number)
{
    IChannel *channelPtr = NULL;

    switch(type)
    {
        case CHANNEL_TYPE_DINPUT:
            if(number < FPM_CH_DINPUT_LAST)
            {
                channelPtr = &dInput[number];
            }
        break;

        case CHANNEL_TYPE_AINPUT:
            if(number < FPM_CH_AINPUT_LAST)
            {
                channelPtr = &aInput[number];
            }
        break;

        case CHANNEL_TYPE_FPI:
            if(number < FPM_CH_FPI_LAST)
            {
                channelPtr = &outputFPI[number];
            }
        break;

        default:
            log.error("%s: Unsupported CAN channel: %i.", __AT__, type);
        break;
    }

    return channelPtr;
}


IOM_ERROR FPMIOModule::tickEvent(lu_uint32_t dTime)
{
    /* Call parent tick function */
    return CANIOModule::tickEvent(dTime);
}


IOM_ERROR FPMIOModule::decodeMessage(ModuleMessage* message)
{
    IOM_ERROR ret = IOM_ERROR_NONE;

    if(message == NULL)
    {
        return IOM_ERROR_PARAM;
    }

    /* General message ? */
    ret = CANIOModule::decodeMessage(message);
    if (ret == IOM_ERROR_NOT_SUPPORTED)
    {
        /* No - Try custom decoder */
        switch(message->header.messageType)
        {
            /* Forward the event to the channel */
            case MODULE_MSG_TYPE_EVENT:
                switch(message->header.messageID)
                {
                    case MODULE_MSG_ID_EVENT_DIGITAL:
                        CANIOModule::handleDEvent(message, dInput, FPM_CH_DINPUT_LAST);
                        ret = IOM_ERROR_NONE;
                    break;

                    case MODULE_MSG_ID_EVENT_ANALOGUE:
                        CANIOModule::handleAEvent(message, aInput, FPM_CH_AINPUT_LAST);
                        ret = IOM_ERROR_NONE;
                    break;

                    default:
                        ret = IOM_ERROR_NOT_SUPPORTED;
                    break;
                }
            break;

            default:
                ret = IOM_ERROR_NOT_SUPPORTED;
            break;
        }
    }

    if(ret == IOM_ERROR_NOT_SUPPORTED)
    {
        log.warn("%s: %s Unsupported message: 0x%02x:0x%02x",
                        __AT__,
                        getName(),
                        message->header.messageType,
                        message->header.messageID
                       );
    }
    return ret;
}


IOM_ERROR FPMIOModule::loadConfiguration()
{
    lu_uint32_t ch_id;
    IOM_ERROR err = IOM_ERROR_NONE;
    IOM_ERROR ret;

    if (configFactory.isConfigured(mid) == LU_FALSE)
    {
        log.warn("%s-was not found in configuration", getName());
        return IOM_ERROR_NOT_FOUND;
    }

    /* ==Load all Digital Input== */
    InputDigitalCANChannelConf dConf;
    for (ch_id = 0; ch_id < FPM_CH_DINPUT_LAST; ch_id++)
    {
        ret = configFactory.getIDigitalConf(mid.type, mid.id, ch_id, dConf);
        if (ret == IOM_ERROR_NONE)
        {
            dInput[ch_id].setConfig(dConf);
        }
        else
        {
            err = ret;
            log.error("%s fail to load configuration for %s. Error %i: %s",
                            getName(), dInput[ch_id].getName(),
                            ret, IOM_ERROR_ToSTRING(ret)
                            );
        }
    }

    /* ==Load all Analogue Input== */
    InputAnalogueCANChannelConf aConf;
    for (ch_id = 0; ch_id < FPM_CH_AINPUT_LAST; ch_id++)
    {
        ret = configFactory.getIAnalogueConf(mid.type, mid.id, ch_id, aConf);
        if (ret == IOM_ERROR_NONE)
        {
            aInput[ch_id].setConfig(aConf);
        }
        else
        {
            /* FIXME: pueyos_a - Temporally disabled error when configuring an absent analogue channel */
            //err = ret;
            log.error("%s fail to load configuration for %s. Error %i: %s",
                        getName(), aInput[ch_id].getName(),
                        ret, IOM_ERROR_ToSTRING(ret)
                        );
        }
    }

    /* ==Load all FPM config== */
    for (ch_id = 0; ch_id < FPI_CONFIG_CH_LAST; ch_id++)
    {
        ret = configFactory.getFPMConf(mid.type, mid.id, ch_id, fpiConf[ch_id]);
        if (ret != IOM_ERROR_NONE)
        {
            err = ret;
            log.error("%s fail to load configuration for %s. Error %i: %s",
                            getName(),outputFPI[ch_id].getName(),
                            ret, IOM_ERROR_ToSTRING(ret)
                            );
        }
        else
        {
            /** Verify channel id */
            if(fpiConf[ch_id].fpiConfigChannel != ch_id)
            {
                log.error("%s fail to load configuration for %s. Mismatched channel id:%i expected:%i %s",
                                           getName(),outputFPI[ch_id].getName(),
                                           fpiConf[ch_id].fpiConfigChannel, ch_id,
                                           __AT__
                                           );
                err = IOM_ERROR_CONFIG;
            }
        }
    }
    return err;
}


IOM_ERROR FPMIOModule::configure()
{
    lu_bool_t hasFailure = LU_FALSE;
    lu_uint32_t i;
    IOM_ERROR ret;

    /* Configure all Digital Input */
    for(i = 0; i < FPM_CH_DINPUT_LAST; i++)
    {
        ret = dInput[i].sendConfiguration();
        LOG_CH_CONFIG_FAILURE(ret,"DInput");
    }

    /* Configure all Analogue Input */
    for(i = 0; i < FPM_CH_AINPUT_LAST; i++)
    {
        ret = aInput[i].sendConfiguration();
        LOG_CH_CONFIG_FAILURE(ret,"AInput");
    }

    /* Configure all special channels */
    for(i = 0; i < FPM_CH_FPI_LAST; i++)
    {
        ret = outputFPI[i].sendConfiguration();
        LOG_CH_CONFIG_FAILURE(ret,"FPI");
    }

    if(configureFPI() != IOM_ERROR_NONE)
        hasFailure = LU_TRUE;

    return (hasFailure == LU_TRUE)? IOM_ERROR_CONFIG:IOM_ERROR_NONE;
}

IOM_ERROR FPMIOModule::configureFPI()
{
    IOM_ERROR ret = IOM_ERROR_NONE;

    lu_uint32_t ch_id;

    for (ch_id = 0; ch_id < FPI_CONFIG_CH_LAST; ch_id++){
        IIOModule::sendReplyStr message;
        IOM_ERROR error;
        FPMConfigStr& conf = fpiConf[ch_id];
        PayloadRAW payload;

        /* Create payload */
        payload.payloadPtr = reinterpret_cast<lu_uint8_t*>(&conf);
        payload.payloadLen = sizeof(conf);

        /* Prepare message */
        message.messageType     = MODULE_MSG_TYPE_CFG;
        message.messageID       = MODULE_MSG_ID_CFG_FPM_C;
        message.messagePtr      = &payload;
        message.rMessageType    = MODULE_MSG_TYPE_CFG;
        message.rMessagePtr     = NULL;
        message.rMessageID      = MODULE_MSG_ID_CFG_FPM_R;
        message.timeout.tv_sec  = CANChannel::CAN_MSG_TIMEOUT_SEC;
        message.timeout.tv_usec = CANChannel::CAN_MSG_TIMEOUT_USEC;

        log.info("Configuring FPI for module:%s channel:%i", getName(), ch_id);

        /* Send configuration */
        error = sendMessage(&message);

        if(error == IOM_ERROR_NONE)
        {
            /* check reply */
            message.rMessagePtr->getPayload(&payload);
            if(payload.payloadLen == MODULE_MESSAGE_SIZE(ModReplyStr))
            {
                ModReplyStr *replyPtr = reinterpret_cast<ModReplyStr*>(payload.payloadPtr);
                if(replyPtr->status == REPLY_STATUS_OKAY)
                {
                    log.info("FPI configured. module:%s channel:%i", getName(), ch_id);
                }
                else
                {
                    log.error("FPI NOT configured. module:%s channel:%i REPLY_STATUS: %i. %s",
                                    getName(), ch_id, replyPtr->status,__AT__
                              );
                    error = IOM_ERROR_CONFIG;
                }
            }
            else
            {
                log.error("FPI Not configured. module:%s channel:%i Invalid reply playload size: %i (expected %i) %s",
                            getName(), ch_id,
                            payload.payloadLen, MODULE_MESSAGE_SIZE(ModReplyStr),
                            __AT__
                          );

                error = IOM_ERROR_INVALID_MSG;
            }
        }
        else
        {
            log.error("FPI Not configured. module:%s channel:%i sendMessage error %i: %s %s",
                        getName(), ch_id,
                        error, IOM_ERROR_ToSTRING(error),
                        __AT__
                      );
        }

        /* Release reply message */
        if(message.rMessagePtr != NULL)
        {
            delete message.rMessagePtr;
        }

        if(error != IOM_ERROR_NONE)
            ret = error;
    }

    return  ret;
}

void FPMIOModule::stop()
{
    lu_int32_t i;

    /* Stop all Binary Input channels */
    for(i = 0; i < FPM_CH_DINPUT_LAST; i++)
    {
        dInput[i].stopUpdate();
    }

    /* Stop all Analogue Input channels */
    for(i = 0; i < FPM_CH_AINPUT_LAST; i++)
    {
        aInput[i].stopUpdate();
    }

    /* Stop all Switch Output channels */
    for(i = 0; i < FPM_CH_FPI_LAST; i++)
    {
        outputFPI[i].stopUpdate();
    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
void FPMIOModule::setAllChsActive(lu_bool_t status)
{
    lu_uint32_t i;

    /* Binary Input */
    for(i = 0; i < FPM_CH_DINPUT_LAST; i++)
    {
         dInput[i].setActive(status);
    }

    /* Analogue Input */
    for(i = 0; i < FPM_CH_AINPUT_LAST; i++)
    {
        aInput[i].setActive(status);
    }

    /* FPI channels */
    for(i = 0; i < FPM_CH_FPI_LAST; i++)
    {
        outputFPI[i].setActive(status);
    }

}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
