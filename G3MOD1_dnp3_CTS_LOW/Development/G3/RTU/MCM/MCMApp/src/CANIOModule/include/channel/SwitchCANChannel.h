/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Switch Out Channel public interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/11/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_8781428D_5D97_4335_9E4B_4B1FF021AA88__INCLUDED_)
#define EA_8781428D_5D97_4335_9E4B_4B1FF021AA88__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "OutputCANChannel.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

struct SwitchCANChannelConf
{

public:
    lu_bool_t enable;
    lu_uint8_t polarity;
    lu_uint16_t pulseLengthMs;
    lu_uint16_t overrunMS;
    lu_uint16_t opTimeoutS;
    lu_uint16_t inhibit;
};


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
class SwitchCANChannel : public OutputCANChannel
{
public:
    /**
     * \brief Custom constructor
     */
    SwitchCANChannel(lu_uint16_t ID = 0, CANIOModule *board = NULL);

    /* FIXME: pueyos_a - Deprecated? */
    /**
     * \brief Force an update of the internal channel logic
     *
     * Send the resynch CAN message
     *
     * \param timeoutPtr Reply timeout
     *
     * \return Error code
     */
    virtual IOM_ERROR synch(struct timeval *timeoutPtr);

    /* *** Switch Commands *** */
    /**
     * \brief Select/Operate/Cancel a CAN switch channel.
     *
     * This is intended to interact with a switch operation over that channel.
     *
     * \param selectTimeout Time out of the Select for when no Operate is received.
     * \param local Local command, set to 0 for non-local (like Remote)
     * \param value Operation value -- not used at the moment
     * \param delay Operation delay: seconds before starting the switch operation
     * \param preOpDelay_ms Time to wait beteen the end of the delay and effectively start the operation
     *
     * \return Error code
     */
    virtual IOM_ERROR select(const lu_uint8_t selectTimeout, const lu_bool_t local);
    virtual IOM_ERROR operate(const lu_bool_t value,
                              const lu_bool_t local,
                              const lu_uint8_t delay,
                              const lu_uint16_t preOpDelay_ms = 0);
    virtual IOM_ERROR cancel(const lu_bool_t local);

    /* *** Motor Supply Commands *** */
    /**
     * \brief Select/Operate/Cancel a Motor Supply related to a channel.
     *
     * This allows control over the motor supply relay on the Switch Module.
     *
     * The selOperateNSupply method just does both select & operate in one go.
     *
     * \param transactionID Unique ID of the transaction (usually the groupID)
     * \param local Local command, set to 0 for non-local (like Remote)
     * \param duration Duration of the Motor Supply - 0 for latched
     * \param limit Value threshold for the max current allowed - 0 to disable
     * \param limitDuration Time to be over the threshold to declare overcurrent
     *
     * \return Error code
     */
    virtual IOM_ERROR selectMSupply(const lu_uint16_t transactionID,
                                    const lu_bool_t local);
    virtual IOM_ERROR operateMSupply(const lu_uint16_t transactionID,
                                     const lu_bool_t local,
                                     const lu_uint8_t duration = 0,
                                     const lu_int32_t limit = 0,
                                     const lu_int32_t limitDuration = 0);
    virtual IOM_ERROR selOperateMSupply(const lu_uint16_t transactionID,
                                        const lu_bool_t local,
                                        const lu_uint8_t duration = 0,
                                        const lu_int32_t limit = 0,
                                        const lu_int32_t limitDuration = 0);
    virtual IOM_ERROR cancelMSupply(const lu_uint16_t transactionID,
                                    const lu_bool_t local);

    /**
     * \brief Configure the channel
     *
     * The configuration is stored locally.
     *
     * \param conf Reference to the channel configuration
     *
     * \return error Code
     */
    void setConfig(SwitchCANChannelConf &conf){this->conf = conf;}

    /**
     * \brief Get channel configuration
     *
     * \param configuration where the configuration is saved
     *
     * \return error Code
     */
    IOM_ERROR getConfiguration(SwitchCANChannelConf &configuration);

    /* FIXME: pueyos_a - [#2687] temporary to prevent concurrent operations in DSM */
    CANIOModule* getBoard() {return board;}

protected:
    /* === Inherited from CANChannel === */
    virtual IOM_ERROR configure();

private:
    static const lu_char_t* MSupply_CString; //Common string for Motor Supply operation

private:
    SwitchCANChannelConf conf;

};


#endif // !defined(EA_8781428D_5D97_4335_9E4B_4B1FF021AA88__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
