/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN Output Digital channel module module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   05/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "OutputDigitalCANChannel.h"
#include "CANIOModule.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

OutputDigitalCANChannel::OutputDigitalCANChannel( lu_uint16_t ID    ,
                                                  CANIOModule *board
                                                ) :
                                                OutputCANChannel(CHANNEL_TYPE_DOUTPUT, ID, board)
{}

OutputDigitalCANChannel::~OutputDigitalCANChannel()
{}


IOM_ERROR OutputDigitalCANChannel::getConfiguration(OutputDigitalCANChannelConf &configuration)
{
    configuration = conf;

    return IOM_ERROR_NONE;
}


IOM_ERROR OutputDigitalCANChannel::select(const lu_uint8_t selectTimeout, const lu_bool_t local)
{
    IIOModule::sendReplyStr message;
    DigOutSelectStr selectCmd;
    PayloadRAW payload;

    if(checkActive("select") != IOM_ERROR_NONE)
    {
        return IOM_ERROR_OFFLINE;
    }

    /* Prepare command */
    selectCmd.channel           = this->getID().id;
    selectCmd.SelectTimeoutMS   = selectTimeout * 1000;
    selectCmd.local             = local        ;
    /* TODO: pueyos_a - automation sequence to be added */
    selectCmd.autoSequence      = 0;
    selectCmd.unused            = 0;

    payload.payloadPtr = reinterpret_cast<lu_uint8_t*>(&selectCmd);
    payload.payloadLen = sizeof(selectCmd);

    message.messageType  = MODULE_MSG_TYPE_CMD;
    message.messageID    = MODULE_MSG_ID_CMD_DOC_CH_SELECT_C;
    message.messagePtr   = &payload;
    message.rMessageType = MODULE_MSG_TYPE_CMD;
    message.rMessagePtr  = NULL;
    message.rMessageID   = MODULE_MSG_ID_CMD_DOC_CH_SELECT_R;
    message.timeout      = CANChannel::CAN_MSG_TIMEOUT;

    log.info("%s %s/%s send command(selectTimeout %i - local: %i - auto: %i)",
                __AT__, moduleID.toString().c_str(), this->getName(),
                selectTimeout, local, selectCmd.autoSequence
              );

    return sendCANSendReply(message);
}


IOM_ERROR OutputDigitalCANChannel::operate(const lu_bool_t onOff, const lu_bool_t local, const lu_uint8_t delay)
{
    LU_UNUSED(delay);

    IIOModule::sendReplyStr message;
    DigOutOperateDef operateCmd;
    PayloadRAW payload;

    if(checkActive("operate") != IOM_ERROR_NONE)
    {
        return IOM_ERROR_OFFLINE;
    }

    /* Prepare command */
    operateCmd.channel      = this->getID().id;
    operateCmd.local        = local;
    operateCmd.onOff        = onOff;
    /* TODO: pueyos_a - automation sequence to be added */
    operateCmd.autoSequence = 0;
    operateCmd.unused       = 0;

    payload.payloadPtr = reinterpret_cast<lu_uint8_t*>(&operateCmd);
    payload.payloadLen = sizeof(operateCmd);

    message.messageType  = MODULE_MSG_TYPE_CMD;
    message.messageID    = MODULE_MSG_ID_CMD_DOC_CH_OPERATE_C;
    message.messagePtr   = &payload;
    message.rMessageType = MODULE_MSG_TYPE_CMD;
    message.rMessagePtr  = NULL;
    message.rMessageID   = MODULE_MSG_ID_CMD_DOC_CH_OPERATE_R;
    message.timeout      = CANChannel::CAN_MSG_TIMEOUT;

    log.info("%s %s/%s send command(OnOff: %i - local: %i - auto: %i)",
                __AT__, moduleID.toString().c_str(), this->getName(),
                onOff, local, operateCmd.autoSequence
              );

    return sendCANSendReply(message);
}


IOM_ERROR OutputDigitalCANChannel::cancel(const lu_bool_t local)
{
    IIOModule::sendReplyStr message;
    PayloadRAW payload;
    DigOutCancelDef cancelCmd;

    if(checkActive("cancel") != IOM_ERROR_NONE)
    {
        return IOM_ERROR_OFFLINE;
    }

    /* Prepare command */
    cancelCmd.channel = this->getID().id;
    cancelCmd.local   = local;
    /* TODO: pueyos_a - automation sequence to be added */
    cancelCmd.autoSequence = 0;
    cancelCmd.unused = 0;

    payload.payloadPtr = reinterpret_cast<lu_uint8_t*>(&cancelCmd);
    payload.payloadLen = sizeof(cancelCmd);

    message.messageType  = MODULE_MSG_TYPE_CMD;
    message.messageID    = MODULE_MSG_ID_CMD_DOC_CH_CANCEL_C;
    message.messagePtr   = &payload;
    message.rMessageType = MODULE_MSG_TYPE_CMD;
    message.rMessagePtr  = NULL;
    message.rMessageID   = MODULE_MSG_ID_CMD_DOC_CH_CANCEL_R;
    message.timeout      = CANChannel::CAN_MSG_TIMEOUT;

    log.info("%s %s/%s send command(local: %i - auto: %i)",
                __AT__, moduleID.toString().c_str(), this->getName(),
                local, cancelCmd.autoSequence
              );

    return sendCANSendReply(message);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
IOM_ERROR OutputDigitalCANChannel::configure()
{
    IIOModule::sendReplyStr message;
    DigOutConfigDef confCmd;
    PayloadRAW payload;

    /* Prepare command */
    confCmd.channel       = this->getID().id;
    confCmd.outputEnabled = conf.enable;
    confCmd.pulseLengthMS = conf.pulseLengthMs;

    /* Create payload */
    payload.payloadPtr = reinterpret_cast<lu_uint8_t*>(&confCmd);
    payload.payloadLen = sizeof(confCmd);

    /* Prepare message */
    message.messageType     = MODULE_MSG_TYPE_CFG;
    message.messageID       = MODULE_MSG_ID_CFG_DIGITAL_OUT_C;
    message.messagePtr      = &payload;
    message.rMessageType    = MODULE_MSG_TYPE_CFG;
    message.rMessagePtr     = NULL;
    message.rMessageID      = MODULE_MSG_ID_CFG_DIGITAL_OUT_R;
    message.timeout         = CANChannel::CAN_MSG_TIMEOUT;

    return sendCANConfiguration(message);
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
