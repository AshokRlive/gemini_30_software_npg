/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "CANIOModuleFactory.h"
#include "PSMIOModule.h"
#include "SCMIOModule.h"
#include "SCMMK2IOModule.h"
#include "DSMIOModule.h"
#include "FDMIOModule.h"
#include "FPMIOModule.h"
#include "HMIIOModule.h"
#include "IOMIOModule.h"
#include "GenericIOModule.h"
#include "Logger.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

CANIOModuleFactory CANIOModuleFactory::instance;

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */


CANIOModule *CANIOModuleFactory::newBoard( MODULE type             ,
                                           MODULE_ID id            ,
                                           COMM_INTERFACE interface,
                                           TSynchManager &tsynch   ,
                                           ICANModuleConfigurationFactory &moduleFactory
                                         )
{
    CANIOModule *modulePtr = NULL;

    switch(type)
    {
        case MODULE_PSM:
            return (new PSMIOModule(id, interface, tsynch, moduleFactory));
        break;

        case MODULE_SCM:
            return (new SCMIOModule(id, interface, tsynch, moduleFactory));
        break;
#if SCM_MK2_SUPPORT
        case MODULE_SCM_MK2:
            return (new SCMMK2IOModule(id, interface, tsynch, moduleFactory));
        break;
#endif
        case MODULE_DSM:
            return (new DSMIOModule(id, interface, tsynch, moduleFactory));
        break;

        case MODULE_HMI:
            return (new HMIIOModule(id, interface, tsynch, moduleFactory));
        break;

        case MODULE_IOM:
            return (new IOMIOModule(id, interface, tsynch, moduleFactory));
        break;

        case MODULE_FDM:
            return (new FDMIOModule(id, interface, tsynch, moduleFactory));
        break;

        case MODULE_FPM:
            return (new FPMIOModule(id, interface, tsynch, moduleFactory));
        break;

        case MODULE_MCM:
        case MODULE_BRD:
        case MODULE_LAST:
            Logger::getLogger(SUBSYSTEM_ID_CANMANAGER).error("CANIOModuleFactory::newBoard: "
                            "CAN Module type %i not supported.", type
                    );

        break;

        default:
            return (new GenericIOModule(type,id, interface, tsynch, moduleFactory));

    }
    return modulePtr;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
