/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN Module database
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <cstring>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "LockingMutex.h"
#include "CANIOModuleDB.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */



CANIOModuleDB::CANIOModuleDB( CANIOModuleFactory* factory,
                              TSynchManager &tsynch,
                              ConfigurationManager &cManager
                            ):
                                mutex(LU_TRUE)  ,
                                factory(factory),
                                tsynch(tsynch),
                                cManager(cManager),
                                log(Logger::getLogger(SUBSYSTEM_ID_CANMODULEDB))
{
    /* clean module database */
    memset(modules, 0, sizeof(modules));
}




CANIOModuleDB::~CANIOModuleDB()
{
}

CANIOModule* CANIOModuleDB::getModule( MODULE module        ,
                                       MODULE_ID moduleID   ,
                                       COMM_INTERFACE iFace
                                     )
{
    const lu_char_t* FTITLE = "CANIOModuleDB::getModule:";

    if( (module == MODULE_MCM ) ||(module == MODULE_BRD) ||
        (module < 0) || (moduleID  < 0) ||
        (module >= MODULE_LAST) || (moduleID  >= MODULE_ID_BRD)
      )
    {
        log.debug("%s Unsupported CAN Module: %i:%i", FTITLE, module, moduleID);
        return NULL;
    }

    CANIOModule *modulePtr;

    { /* critical region scope */
        LockingMutex lockingMutex(mutex);

        /* Valid ID. Get the module */
        modulePtr = modules[module][moduleID];

        /* allocate a new board? */
        if(modulePtr == NULL)
        {
            log.debug("%s CAN Module: %i:%i not found", FTITLE, module, moduleID);
            if(iFace != COMM_INTERFACE_UNKNOWN)
            {
                /* Try to allocate a new board */
                log.info("%s Allocating new CAN Module: %i:%i", FTITLE, module, moduleID);

                modulePtr = factory->newBoard( module,
                                               moduleID,
                                               iFace,
                                               tsynch,
                                               cManager.getCANChannelFactory()
                                             );

                if(modulePtr == NULL)
                {
                    log.fatal("%s CAN Module (%i:%i) allocation error",
                                    FTITLE, module, moduleID
                                  );
                }

                modules[module][moduleID] = modulePtr;
            }
        }
    }

    return modulePtr;
}


std::vector<IIOModule*> CANIOModuleDB::getAllModules()
{
    std::vector<IIOModule*> ret;

    CANIOModule* modulePtr;

    LockingMutex lockingMutex(mutex);

    /* Scan the local database */
    for(lu_uint32_t moduleIdx = 0; moduleIdx < MODULE_LAST; ++moduleIdx)
    {
        for(lu_uint32_t moduleID = 0; moduleID < MODULE_ID_BRD; ++moduleID)
        {
            /* If the module is valid save a reference in the output buffer */
            modulePtr = modules[moduleIdx][moduleID];
            if(modulePtr != NULL)
            {
                ret.push_back(modulePtr);
            }
        }
    }
    return ret;
}


IOM_ERROR CANIOModuleDB::decode(ModuleMessage* messagePtr, COMM_INTERFACE iFace)
{
    IOM_ERROR ret = IOM_ERROR_NONE;
    IIOModule* modulePtr;

    if(messagePtr == NULL)
    {
        return IOM_ERROR_PARAM;
    }

    /* Get the destination board. If it doesn't exist create one */
    modulePtr = getModule( messagePtr->header.source  ,
                           messagePtr->header.sourceID,
                           iFace
                         );

    if(modulePtr != NULL)
    {
        /* Forward message */
        modulePtr->decodeMessage(messagePtr);
    }
    else
    {
        /* The DB can not return/generate an appropriate module */
        log.error("CANIOModuleDB::decode: "
                        "ModuleDB error. CAN Module %i - CAN ID: %i",
                        messagePtr->header.source,
                        messagePtr->header.sourceID
                      );
        ret = IOM_ERROR_NOT_FOUND;
    }

    return ret;
}


IOM_ERROR CANIOModuleDB::tickEvent(lu_uint32_t dTime)
{
    lu_uint32_t i;
    CANIOModule **modulePtrPtr;

    /* Lock database access */
    LockingMutex lockingMutex(mutex);

    /* Forward the event to all the available boards */
    for( i = 0, modulePtrPtr = &modules[0][0];
         i< modulesSize;
         ++i, ++modulePtrPtr
       )
    {
        if((*modulePtrPtr) != NULL)
        {
            (*modulePtrPtr)->tickEvent(dTime);
        }
    }

    return  IOM_ERROR_NONE;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */


