/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN channel configuration factory interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_B908D578_1C3A_46eb_817B_90078D961465__INCLUDED_)
#define EA_B908D578_1C3A_46eb_817B_90078D961465__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "GlobalDefs.h"
#include "ModuleProtocol.h"
#include "InputDigitalCANChannel.h"
#include "InputAnalogueCANChannel.h"
#include "SwitchCANChannel.h"
#include "SwitchCANChannelDSM.h"
#include "SwitchCANChannelSCM2.h"
#include "FPICANChannel.h"
#include "OutputDigitalCANChannel.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class ICANChannelConfigurationFactory
{

public:
    ICANChannelConfigurationFactory() {};

    virtual ~ICANChannelConfigurationFactory() {};

    /**
     * \brief Get input digital channel configuration
     *
     * \param module Module type
     * \param moduleID Module ID
     * \param channeID Channel ID
     * \param conf reference to the buffer where the configuration will be saved
     *
     * \return Error Code
     */
    virtual IOM_ERROR getIDigitalConf( MODULE module,
                                       MODULE_ID moduleID,
                                       lu_uint32_t channeID,
                                       InputDigitalCANChannelConf &conf
                                      ) = 0;

    /**
     * \brief Get input analogue channel configuration
     *
     * \param module Module type
     * \param moduleID Module ID
     * \param channeID Channel ID
     * \param conf reference to the buffer where the configuration will be saved
     *
     * \return Error Code
     */
    virtual IOM_ERROR getIAnalogueConf( MODULE module,
                                        MODULE_ID moduleID,
                                        lu_uint32_t channeID,
                                        InputAnalogueCANChannelConf &conf
                                       ) = 0;

    /**
     * \brief Get switch out channel configuration
     *
     * \param module Module type
     * \param moduleID Module ID
     * \param channeID Channel ID
     * \param conf reference to the buffer where the configuration will be saved
     *
     * \return Error Code
     */
    virtual IOM_ERROR getSwitchOutConf(MODULE module,
                                       MODULE_ID moduleID,
                                       lu_uint32_t channeID,
                                       SwitchCANChannelConf &conf
                                      ) = 0;

    /**
     * \brief Get DSM-specific switch out channel configuration
     *
     * \param module Module type
     * \param moduleID Module ID
     * \param channeID Channel ID
     * \param conf reference to the buffer where the configuration will be saved
     *
     * \return Error Code
     */
    virtual IOM_ERROR getDSMSwitchOutConf(MODULE module,
                                          MODULE_ID moduleID,
                                          lu_uint32_t channelID,
                                          SwitchCANChannelDSM::SwitchCANChannelDSMConf &conf
                                         ) = 0;

    /**
     * \brief Get SCM2-specific switch out channel configuration
     *
     * \param module Module type
     * \param moduleID Module ID
     * \param channeID Channel ID
     * \param conf reference to the buffer where the configuration will be saved
     *
     * \return Error Code
     */
    virtual IOM_ERROR getSCM2SwitchOutConf(MODULE module,
                                          MODULE_ID moduleID,
                                          lu_uint32_t channelID,
                                          SwitchCANChannelSCM2::SwitchConf &conf
                                         ) = 0;

    /**
     * \brief Get Digital Output channel configuration
     *
     * \param module Module type
     * \param moduleID Module ID
     * \param channeID Channel ID
     * \param conf reference to the buffer where the configuration will be saved
     *
     * \return Error Code
     */
    virtual IOM_ERROR getDigitalOutConf(MODULE module,
                                        MODULE_ID moduleID,
                                        lu_uint32_t channeID,
                                        OutputDigitalCANChannelConf &conf
                                       ) = 0;

    /**
     * \brief Get FPI Output channel configuration
     *
     * \param module Module type
     * \param moduleID Module ID
     * \param channeID Channel ID
     * \param conf reference to the buffer where the configuration will be saved
     *
     * \return Error Code
     */
    virtual IOM_ERROR getFPMConf(MODULE module,
                                    MODULE_ID moduleID,
                                    lu_uint32_t channeID,
                                    FPMConfigStr& conf
                                    ) = 0;

    /**
     * \brief Get HMIt channel configuration
     *
     * \param module Module type
     * \param moduleID Module ID
     * \param channeID Channel ID
     * \param conf reference to the buffer where the configuration will be saved
     *
     * \return Error Code
     */
    virtual IOM_ERROR getHMIConf( MODULE module,
                                  MODULE_ID moduleID,
                                  lu_uint32_t channeID,
                                  HMIConfigStr &conf
                                 ) = 0;

};
#endif // !defined(EA_B908D578_1C3A_46eb_817B_90078D961465__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
