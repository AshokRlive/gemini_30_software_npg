/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN event manager implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   01/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <errno.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */



#include "Timer.h"
#include "CANEventManager.h"
#include "CANIOModuleManager.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static const lu_uint32_t CanTimeoutSec  = 0   ;
static const lu_uint32_t CanTimeoutUsec = 1000;

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */


CANEventManager::CANEventManager( CANFraming    &socketA    ,
                                  CANFraming    &socketB    ,
                                  CANIOModuleDB &modules    ,
                                  SCHED_TYPE     schedType  ,
                                  lu_uint32_t    priority
                                ) :
                                Thread(schedType, priority, LU_TRUE, "CANEventManager"),
                                socketA(socketA),
                                socketB(socketB),
                                modules(modules),
                                log(Logger::getLogger(SUBSYSTEM_ID_CANMANAGER))
{}

CANEventManager::~CANEventManager()
{
    Thread::stop(); //stop thread
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void CANEventManager::threadBody()
{
    fd_set rfds;        //file descriptors set
    lu_int32_t maxFD;   //biggest of them to offer it to select
    lu_int32_t retval;


    /* start main loop:
     * -Wait for messages or timeout
     */
    while(isRunning() == LU_TRUE)
    {
        struct timeval timeoutLocal = {0, 50000};

        /* Initialise file descriptors set to be used by select */
        /* NOTE: Always check the File Descriptors before select() since they
         * can change at any time
         */
        FD_ZERO(&rfds); //Reset the file descriptors waiting list

        /* Add socketA file descriptor to waiting list */
        lu_int32_t canFDA = socketA.getFD();
        if(canFDA >= 0)
        {
            FD_SET(canFDA, &rfds);
        }

        /* Add socketB file descriptor to waiting list */
        lu_int32_t canFDB = socketB.getFD();
        if(canFDB >= 0)
        {
            FD_SET(canFDB, &rfds);
        }

        /* Get maximum file descriptor number */
        maxFD = LU_MAX(canFDA, canFDB);

        /* Wait for incoming messages */
        retval = select(maxFD + 1, &rfds, NULL, NULL, &timeoutLocal);
        if(isInterrupting() == LU_TRUE)
        {
            continue;   //exit thread
        }
        if(retval < 0)
        {
           log.error("CANEventManager::threadBody: select error(%i).", errno);
           continue;
        }
        else if(retval > 0)
        {
            /* No errors. Read from the FDs */
            if(canFDA >= 0)
            {
                if(FD_ISSET(canFDA, &rfds))
                {
                    /* CAN message available for socketA */
                    handleEvent(socketA);
                }
            }
            if(canFDB >= 0)
            {
                if(FD_ISSET(canFDB, &rfds))
                {
                    /* CAN message available for socketB */
                    handleEvent(socketB);
                }
            }
        }
        /* else if(retval == 0) => timed out: nothing to do! */
    }
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

void CANEventManager::handleEvent(CANFraming &socket)
{
    const lu_char_t* FTITLE = "CANEventManager::handleEvent:";
    struct timeval timeout;
    ModuleMessage *messagePtr;
    CAN_ERROR canRet;

    log.debug("%s Read CAN Message", FTITLE);

    /* Initialise can socket timeout */
    timeout.tv_sec  = CanTimeoutSec ;
    timeout.tv_usec = CanTimeoutUsec;

    canRet = socket.read(&messagePtr, &timeout);
    switch(canRet)
    {
    case CAN_ERROR_NONE:
        //Successful read
        {
            /* Message received.
             * Forward the message to the destination module.
             */

            /* Log data received */
            log.debug("%s Message received: msg: %i - msgID: %i - src: %i - CANsrcID: %i",
                        FTITLE,
                        messagePtr->header.messageType,
                        messagePtr->header.messageID,
                        messagePtr->header.source,
                        messagePtr->header.sourceID
                      );

            /* Forward the message to the boards */
            modules.decode(messagePtr, socket.getIface());

            /* De-allocate the message */
            delete messagePtr;
        }
        break;
    case CAN_ERROR_INIT:
        //Socket/Interface not initialised: ignore
        break;
    case CAN_ERROR_DOWN:
        //Interface is down: ignore
        break;
    case CAN_ERROR_ERR_FRAME:
        /* TODO: Handle error frame */
        log.error("%s Error in CAN Frame", FTITLE);
        break;
    default:
        /* General error */
        log.error("%s socket read error: %i", FTITLE, canRet);
        break;
    }
}


/*
 *********************** End of file ******************************************
 */
