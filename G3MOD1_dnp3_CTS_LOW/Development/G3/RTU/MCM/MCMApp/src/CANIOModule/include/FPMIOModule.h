/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: FPMIOModule.h 15 Jul 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/CANIOModule/include/FPMIOModule.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       FPM Module - Fault Passage (fault indication) Module
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 15 Jul 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15 Jul 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef FPMIOMODULE_H__INCLUDED
#define FPMIOMODULE_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "InputDigitalCANChannel.h"
#include "InputAnalogueCANChannel.h"
#include "FPICANChannel.h"
#include "CANIOModule.h"
#include "ICANModuleConfigurationFactory.h"
#include "ModuleProtocolEnumFPM.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Handles the FPM CAN Module
 *
 * FPM - Fault Passage (fault indication) Module
 * This module is in charge of detection of faults in the line and repot them.
 */
class FPMIOModule: public CANIOModule
{
public:
    FPMIOModule(MODULE_ID id,
                    COMM_INTERFACE iFace,
                    TSynchManager &tsynch,
                    ICANModuleConfigurationFactory &moduleFactory
                    );
    virtual ~FPMIOModule();

    /* == Inherited from CANIOModule == */
    virtual IChannel *getChannel(CHANNEL_TYPE type, lu_uint32_t number);
    virtual IOM_ERROR tickEvent(lu_uint32_t dTime);
    virtual IOM_ERROR decodeMessage(ModuleMessage* message);
    virtual IOM_ERROR configure();
    virtual IOM_ERROR loadConfiguration();
    virtual void stop();

protected:
    /* == Inherited from CANIOModule == */
    void setAllChsActive(lu_bool_t status);

private:
    IOM_ERROR configureFPI();

    InputDigitalCANChannel dInput[FPM_CH_DINPUT_LAST];  //Digital inputs
    InputAnalogueCANChannel aInput[FPM_CH_AINPUT_LAST]; //Analogue inputs
    FPICANChannel outputFPI[FPM_CH_FPI_LAST];   //specialised FPI channels

    FPMConfigStr  fpiConf[FPI_CONFIG_CH_LAST];
};

#endif /* FPMIOMODULE_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
