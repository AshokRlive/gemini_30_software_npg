/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Switch Out Channel implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/11/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "SwitchCANChannel.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
const lu_char_t* SwitchCANChannel::MSupply_CString = "MSupply";

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

SwitchCANChannel::SwitchCANChannel(lu_uint16_t ID, CANIOModule *board) :
                                       OutputCANChannel(CHANNEL_TYPE_SW_OUT, ID, board)
{
    memset(&conf, 0, sizeof(conf));
}


IOM_ERROR SwitchCANChannel::select(const lu_uint8_t selectTimeout, const lu_bool_t local)
{
    if(checkActive(CANChannel::Select_CString) != IOM_ERROR_NONE)
    {
        return IOM_ERROR_OFFLINE;
    }

    /* Prepare command */
    SwitchSelectStr selectCmd;
    selectCmd.channel       = this->getID().id;
    selectCmd.SelectTimeout = selectTimeout;
    selectCmd.local         = local        ;

    log.info("%s %s/%s send %s command(selectTimeout %i - local: %i)",
                __AT__, moduleID.toString().c_str(), this->getName(),
                CANChannel::Select_CString, selectTimeout, local
              );

    return sendCANSendReply(selectCmd,
                            MODULE_MSG_TYPE_CMD,
                            MODULE_MSG_ID_CMD_SWC_CH_SELECT_C,
                            MODULE_MSG_ID_CMD_SWC_CH_SELECT_R);
}


IOM_ERROR SwitchCANChannel::operate(const lu_bool_t value,
                                    const lu_bool_t local,
                                    const lu_uint8_t delay,
                                    const lu_uint16_t preOpDelay_ms)
{
    LU_UNUSED(value);

    if(checkActive(CANChannel::Operate_CString) != IOM_ERROR_NONE)
    {
        return IOM_ERROR_OFFLINE;
    }

    /* Prepare command */
    SwitchOperateStr operateCmd;
    operateCmd.channel     = this->getID().id;
    operateCmd.SwitchDelay = delay;
    operateCmd.SwitchPreOpDelay = preOpDelay_ms;
    operateCmd.local       = local;

    log.info("%s %s/%s send %s command(delay: %i - local: %i)",
                __AT__, moduleID.toString().c_str(), this->getName(),
                CANChannel::Operate_CString, delay, local
              );

    return sendCANSendReply(operateCmd,
                            MODULE_MSG_TYPE_CMD,
                            MODULE_MSG_ID_CMD_SWC_CH_OPERATE_C,
                            MODULE_MSG_ID_CMD_SWC_CH_OPERATE_R);
}


IOM_ERROR SwitchCANChannel::cancel(lu_bool_t local)
{
    if(checkActive(CANChannel::Cancel_CString) != IOM_ERROR_NONE)
    {
        return IOM_ERROR_OFFLINE;
    }

    /* Prepare command */
    SwitchCancelStr cancelCmd;
    cancelCmd.channel = this->getID().id;
    cancelCmd.local   = local;

    log.info("%s %s/%s send %s command(local: %i)",
                __AT__, moduleID.toString().c_str(), this->getName(),
                CANChannel::Cancel_CString, local
              );

    return sendCANSendReply(cancelCmd,
                            MODULE_MSG_TYPE_CMD,
                            MODULE_MSG_ID_CMD_SWC_CH_CANCEL_C,
                            MODULE_MSG_ID_CMD_SWC_CH_CANCEL_R);
}


IOM_ERROR SwitchCANChannel::synch(struct timeval *timeoutPtr)
{
    IIOModule::sendReplyStr message;
    PayloadRAW payload;
    SwitchSynchStr synchCmd;

    if(timeoutPtr == NULL)
    {
        return IOM_ERROR_PARAM;
    }

    if(checkActive("synch") != IOM_ERROR_NONE)
    {
        return IOM_ERROR_OFFLINE;
    }

    /* Prepare command */
    synchCmd.channel = this->getID().id;

    payload.payloadPtr = reinterpret_cast<lu_uint8_t*>(&synchCmd);
    payload.payloadLen = sizeof(synchCmd);

    message.messageType  = MODULE_MSG_TYPE_CMD;
    message.messageID    = MODULE_MSG_ID_CMD_SWC_CH_SYNCH_C;
    message.messagePtr   = &payload;
    message.rMessageType = MODULE_MSG_TYPE_CMD;
    message.rMessagePtr  = NULL;
    message.rMessageID   = MODULE_MSG_ID_CMD_SWC_CH_SYNCH_R;
    message.timeout      = *timeoutPtr;

    log.info("%s %s/%s send synch command", __AT__, moduleID.toString().c_str(), this->getName());

    return sendCANSendReply(message);

}


IOM_ERROR SwitchCANChannel::selectMSupply(const lu_uint16_t transactionID, const lu_bool_t local)
{
    if(checkActive(CANChannel::Select_CString) != IOM_ERROR_NONE)
    {
        return IOM_ERROR_OFFLINE;
    }

    /* Prepare command */
    SupplySelectStr selectCmd;
    selectCmd.channel       = this->getID().id;
    selectCmd.operationID   = transactionID;
    selectCmd.SelectTimeout = CANChannel::CAN_MSG_SELECT_TIMEOUT_SEC;
    selectCmd.local         = local;

    log.info("%s, selTout %i",
            logSendCommand(MSupply_CString, CANChannel::Select_CString, local, transactionID).c_str(),
            selectCmd.SelectTimeout);
    return sendCANSendReply(selectCmd,
                            MODULE_MSG_TYPE_CMD,
                            MODULE_MSG_ID_CMD_PSC_CH_SELECT_C,
                            MODULE_MSG_ID_CMD_PSC_CH_SELECT_R);
}


IOM_ERROR SwitchCANChannel::operateMSupply(const lu_uint16_t transactionID,
                                            const lu_bool_t local,
                                            const lu_uint8_t duration,
                                            const lu_int32_t limit,
                                            const lu_int32_t limitDuration)
{
    if(checkActive(CANChannel::Operate_CString) != IOM_ERROR_NONE)
    {
        return IOM_ERROR_OFFLINE;
    }

    /* Prepare command */
    SupplyOperateStr operateCmd;
    operateCmd.channel        = this->getID().id;
    operateCmd.operationID    = transactionID;
    operateCmd.supplyDuration = duration;
    operateCmd.currentLimit   = limit   ;
    operateCmd.currentLimitDuration   = limitDuration;
    operateCmd.local          = local   ;

    log.info("%s, duration %i, Ilimit (%i,%i)",
            logSendCommand(MSupply_CString, CANChannel::Operate_CString, local, transactionID).c_str(),
            duration, limit, limitDuration);
    return sendCANSendReply(operateCmd,
                            MODULE_MSG_TYPE_CMD,
                            MODULE_MSG_ID_CMD_PSC_CH_OPERATE_C,
                            MODULE_MSG_ID_CMD_PSC_CH_OPERATE_R);
}


IOM_ERROR SwitchCANChannel::selOperateMSupply(const lu_uint16_t transactionID,
                                                const lu_bool_t local,
                                                const lu_uint8_t duration,
                                                const lu_int32_t limit,
                                                const lu_int32_t limitDuration)
{
    /* Start of critical section to prevent select command while doing a select & operate */
    LockingMutex mMutex(sopMutex);
    IOM_ERROR iomRet = selectMSupply(transactionID, local);
    if(iomRet == IOM_ERROR_NONE)
    {
        iomRet = operateMSupply(transactionID, local, duration, limit, limitDuration);
    }
    return iomRet;
}


IOM_ERROR SwitchCANChannel::cancelMSupply(const lu_uint16_t transactionID,
                                          const lu_bool_t local)
{
    if(checkActive(CANChannel::Cancel_CString) != IOM_ERROR_NONE)
    {
        return IOM_ERROR_OFFLINE;
    }

    /* Prepare command */
    SupplyCancelStr cancelCmd;
    SupplyCancelReplyStr reply;
    cancelCmd.channel = this->getID().id;
    cancelCmd.operationID = transactionID;
    cancelCmd.local   = local;

    log.info(logSendCommand(MSupply_CString, CANChannel::Cancel_CString, local, transactionID).c_str());

    /* Note that the peak motor current is not used here */
    return sendFullCANSendReply(cancelCmd,
                            reply,
                            MODULE_MSG_TYPE_CMD,
                            MODULE_MSG_ID_CMD_PSC_CH_CANCEL_C,
                            MODULE_MSG_ID_CMD_PSC_CH_CANCEL_R);
}


IOM_ERROR SwitchCANChannel::getConfiguration(SwitchCANChannelConf &configuration)
{
    configuration = conf;

    return IOM_ERROR_NONE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
IOM_ERROR SwitchCANChannel::configure()
{
    /* Prepare command */
    SwitchConfigStr confCmd;
    confCmd.channel       = this->getID().id;
    confCmd.SwitchEnabled = conf.enable;
    confCmd.polarity      = conf.polarity;
    confCmd.pulseLengthMS = conf.pulseLengthMs;
    confCmd.overrunMS     = conf.overrunMS;
    confCmd.opTimeoutS    = conf.opTimeoutS;
    confCmd.inhibitBI     = conf.inhibit;

    return sendCANSendReply(confCmd,
                            MODULE_MSG_TYPE_CFG,
                            MODULE_MSG_ID_CFG_SWITCH_C,
                            MODULE_MSG_ID_CFG_SWITCH_R);
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
