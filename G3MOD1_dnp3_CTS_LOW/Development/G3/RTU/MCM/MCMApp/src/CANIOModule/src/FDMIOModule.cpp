/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       FDM Module implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   01/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "FDMIOModule.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */


FDMIOModule::FDMIOModule( MODULE_ID id                                 ,
                          COMM_INTERFACE iFace                         ,
                          TSynchManager &tsynch                        ,
                          ICANModuleConfigurationFactory &moduleFactory
                        ) :
                           CANIOModule(MODULE_FDM, id, iFace, tsynch, moduleFactory)
{
    lu_uint32_t i;

    /* Set channels board and ID: Analogue input */
    for(i = 0; i < FDM_CH_AINPUT_LAST; i++)
    {
        AInput[i].setBoard(this);
        AInput[i].setID(i);
    }

    /* Set channels board and ID: FPI */
    for(i = 0; i < FDM_CH_FPI_LAST; i++)
    {
        FPI[i].setBoard(this);
        FPI[i].setID(i);
    }

    initStateMachine();
}


FDMIOModule::~FDMIOModule()
{

}

IChannel *FDMIOModule::getChannel(CHANNEL_TYPE type, lu_uint32_t number)
{
    IChannel *channelPtr = NULL;

    switch(type)
    {
        case CHANNEL_TYPE_AINPUT:
            if(number < FDM_CH_AINPUT_LAST)
            {
                channelPtr = &AInput[number];
            }
        break;

        case CHANNEL_TYPE_FPI:
            if(number < FDM_CH_FPI_LAST)
            {
                channelPtr = &FPI[number];
            }
        break;

        default:
            log.error("FDMIOModule::getChannel: "
                        "%s Unsupported CAN channel: %i. Function: %s",
                        getName(), type, __FUNCTION__
                      );
        break;
    }

    return channelPtr;
}


IOM_ERROR FDMIOModule::tickEvent(lu_uint32_t dTime)
{
    /* Call parent tick function */
    return CANIOModule::tickEvent(dTime);
}


IOM_ERROR FDMIOModule::decodeMessage(ModuleMessage* message)
{
    IOM_ERROR ret = IOM_ERROR_NONE;

    if(message == NULL)
    {
        return IOM_ERROR_PARAM;
    }

    /* General message ? */
    ret = CANIOModule::decodeMessage(message);
    if (ret == IOM_ERROR_NOT_SUPPORTED)
    {
        /* No - Try custom decoder */
        switch(message->header.messageType)
        {
            /* Forward the event to the channel */
            case MODULE_MSG_TYPE_EVENT:
                switch(message->header.messageID)
                {
                    case MODULE_MSG_ID_EVENT_ANALOGUE:
                        CANIOModule::handleAEvent(message, AInput, FDM_CH_AINPUT_LAST);
                        ret = IOM_ERROR_NONE;
                    break;

                    default:
                        ret = IOM_ERROR_NOT_SUPPORTED;
                    break;
                }
            break;

            default:
                ret = IOM_ERROR_NOT_SUPPORTED;
            break;
        }
    }

    if(ret == IOM_ERROR_NOT_SUPPORTED)
    {
        log.warn("FDMIOModule::decodeMessage: "
                        "%s Unsupported message: 0x%02x:0x%02x",
                        getName(),
                        message->header.messageType,
                        message->header.messageID
                       );
    }
    return ret;
}


IOM_ERROR FDMIOModule::loadConfiguration()
{
    lu_uint32_t ch_id;
    IOM_ERROR err = IOM_ERROR_NONE;
    IOM_ERROR ret;

    if(configFactory.isConfigured(mid) == LU_FALSE)
    {
        log.warn("%s-was not found in configuration",getName());
        return IOM_ERROR_NOT_FOUND;
    }

     /* Configure all Analogue Input */
     InputAnalogueCANChannelConf conf;
     for(ch_id = 0; ch_id < FDM_CH_AINPUT_LAST; ch_id++)
     {
         ret = configFactory.getIAnalogueConf(mid.type, mid.id, ch_id,  conf);
         if(ret == IOM_ERROR_NONE)
         {
             AInput[ch_id].setConfig(conf);
         }
         else
         {
             err = ret;
             log.error("%s fail to load configuration for %s. Error %i: %s",
                         getName(), AInput[ch_id].getName(),
                         ret, IOM_ERROR_ToSTRING(ret)
                         );
         }
     }
     return err;
}


IOM_ERROR FDMIOModule::configure()
{
    lu_bool_t hasFailure = LU_FALSE;
    lu_uint32_t i;
    IOM_ERROR ret;

    /* Configure all Analogue Input */
    for(i = 0; i < FDM_CH_AINPUT_LAST; i++)
    {
        /* Send configuration */
        ret = AInput[i].sendConfiguration();
        LOG_CH_CONFIG_FAILURE(ret,"AInput");
    }

    /* Configure all FPI channels */
    for(i = 0; i < FDM_CH_FPI_LAST; i++)
    {
        /* Send configuration */
        ret = FPI[i].sendConfiguration();
        LOG_CH_CONFIG_FAILURE(ret,"FPI");
    }
    return (hasFailure == LU_TRUE)? IOM_ERROR_CONFIG:IOM_ERROR_NONE;
}


void FDMIOModule::stop()
{
    lu_int32_t i;

    /* Set channels board and ID: Analogue input */
    for(i = 0; i < FDM_CH_AINPUT_LAST; i++)
    {
        AInput[i].stopUpdate();
    }

    /* Set channels board and ID: FPI */
    for(i = 0; i < FDM_CH_FPI_LAST; i++)
    {
        FPI[i].stopUpdate();
    }
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void FDMIOModule::setAllChsActive(lu_bool_t status)
{
    lu_uint32_t i;

    /* Analogue input */
    for(i = 0; i < FDM_CH_AINPUT_LAST; i++)
    {
        AInput[i].setActive(status);
    }

    /* FPI */
    for(i = 0; i < FDM_CH_FPI_LAST; i++)
    {
        FPI[i].setActive(status);
    }
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
