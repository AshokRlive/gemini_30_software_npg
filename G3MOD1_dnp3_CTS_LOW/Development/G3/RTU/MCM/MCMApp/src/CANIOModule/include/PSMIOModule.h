/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       PSM Module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   29/06/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#if !defined(EA_4C547CFA_1ADF_45e1_B05E_2237A1C06C4F__INCLUDED_)
#define EA_4C547CFA_1ADF_45e1_B05E_2237A1C06C4F__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "InputDigitalCANChannel.h"
#include "InputAnalogueCANChannel.h"
#include "PowerSupplyCANChannel.h"
#include "BatteryChargerCANChannel.h"
#include "FanCANChannel.h"
#include "OutputDigitalCANChannel.h"
#include "CANIOModule.h"
#include "ICANModuleConfigurationFactory.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class PSMIOModule : public CANIOModule
{
public:
    struct PSMConfigStr
    {
    public:
        FanConfigStr fanConfig;
        ChargerConfigStr chargerCfg;
    public:
        PSMConfigStr()
        {
            fanConfig.fanFitted = LU_FALSE;
            fanConfig.fanSpeedSensorFitted = LU_FALSE;
            fanConfig.fanDigitalOutput = LU_FALSE;
            fanConfig.fanExternalSupply = LU_FALSE;
            fanConfig.unused = 0;
            chargerCfg.batteryTestPeriodHours = 0;  //Disabled
            chargerCfg.batteryTestDurationSecs = 0;
        };
    };
public:
    PSMIOModule(MODULE_ID id, COMM_INTERFACE iFace, TSynchManager &tsynch,
                    ICANModuleConfigurationFactory &moduleFactory);
    virtual ~PSMIOModule();

    /**
     * \brief Return a channel reference
     *
     * \param type Channel type
     * \param number Channel number
     *
     * \return Channel pointer. NULL if channel is not present
     */
    virtual IChannel *getChannel(CHANNEL_TYPE type, lu_uint32_t number);

    /**
     * \brief Notify the timer tick is expired
     *
     * This function should be periodically called in order to keep the internal logic
     * updated (internal timers, etc,...)
     *
     * \param dTime Time elapsed from the previous tick (in ms)
     *
     * \return Error code
     */
    virtual IOM_ERROR tickEvent(lu_uint32_t dTime);

    /**
     * \brief Decode a module message  If necessary the module/channels internal state
     * is updated
     *
     * \param message Message to decode
     *
     * \return Error code
     */
    virtual IOM_ERROR decodeMessage(ModuleMessage* message);

    virtual IOM_ERROR loadConfiguration();

    virtual IOM_ERROR configure();

    /**
     * \brief Stop all of this module's channels from updating
     *
     * \param none
     *
     * \return none
     */
    virtual void stop();

protected:
    /**
     * \brief Update the status of all the channels
     *
     * \param status LU_TRUE if the module is online
     *
     * \return none
     */
    void setAllChsActive(lu_bool_t status);

private:
    IOM_ERROR sendCANConfig(PayloadRAW& payload, const MODULE_MSG_ID messageC, const MODULE_MSG_ID messageR);

private:
    InputDigitalCANChannel BInput[PSM_CH_DINPUT_LAST];
    InputAnalogueCANChannel AInput[PSM_CH_AINPUT_LAST];
    PowerSupplyCANChannel PSupply[PSM_CH_PSUPPLY_LAST];
    BatteryChargerCANChannel BCharger[PSM_CH_BCHARGER_LAST];
    FanCANChannel Fan[PSM_CH_FAN_LAST];
    OutputDigitalCANChannel DOutput[PSM_CH_DOUT_LAST];
    PSMConfigStr conf;
};

#endif // !defined(EA_4C547CFA_1ADF_45e1_B05E_2237A1C06C4F__INCLUDED_)
/*
 *********************** End of file ******************************************
 */
