/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN Power Supply Channel module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   05/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */



#if !defined(EA_53EFB643_EFB3_4497_A6F0_1DB7FD5ECD3E__INCLUDED_)
#define EA_53EFB643_EFB3_4497_A6F0_1DB7FD5ECD3E__INCLUDED_


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "OutputCANChannel.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class PowerSupplyCANChannel : public OutputCANChannel
{
public:
    PowerSupplyCANChannel(lu_uint16_t ID = 0, CANIOModule *board = NULL);

    /**
     * \brief Select/Operate/Cancel a CAN Power Supply channel.
     *
     * This is intended to interact with a Power Supply operation over that channel.
     *
     * \param transactionID Unique ID of the transaction (usually the groupID)
     * \param local Local command, set to 0 for non-local (like Remote)
     * \param duration Duration of the Motor Supply - 0 for latched
     * \param limit Value threshold for the max current allowed - 0 to disable
     * \param limitDuration Time to be over the threshold to declare overcurrent
     * \param peakCurrent Returns here the resulting peak current, if any
     *
     * \return Error code
     */
    virtual IOM_ERROR select(const lu_uint16_t transactionID,
                             const lu_bool_t local);
    virtual IOM_ERROR operate(const lu_uint16_t transactionID,
                              const lu_bool_t local,
                              const lu_uint8_t duration = 0,
                              const lu_int32_t limit = 0,
                              const lu_int32_t limitDuration = 0);
    virtual IOM_ERROR selOperate(const lu_uint16_t transactionID,
                                 const lu_bool_t local,
                                 const lu_uint8_t duration = 0,
                                 const lu_int32_t limit = 0,
                                 const lu_int32_t limitDuration = 0);
    virtual IOM_ERROR cancel(const lu_uint16_t transactionID = 1,
                             const lu_bool_t local = LU_FALSE,
                             lu_float32_t* peakCurrent = NULL);

    /**
     * \brief Get the maximum motor supply time accepted
     *
     * \return Maximum motor supply time accepted, in seconds.
     */
    virtual lu_uint32_t getMSupplyMaxTime() const;

private:
    static const lu_char_t* MSupply_CString; //Common string for Motor Supply operation

};


#endif // !defined(EA_53EFB643_EFB3_4497_A6F0_1DB7FD5ECD3E__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
