/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN channel module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   27/06/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <sstream>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "CANChannel.h"
#include "CANIOModule.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
const timeval CANChannel::CAN_MSG_TIMEOUT = {0, 400000};    //400ms should be enough
const lu_char_t* CANChannel::Select_CString = "Select";
const lu_char_t* CANChannel::Operate_CString = "Operate";
const lu_char_t* CANChannel::Cancel_CString = "Cancel";

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */


CANChannel::CANChannel(CHANNEL_TYPE type, lu_uint16_t id, CANIOModule *board) :
                                            IChannel(type,id),
                                            board(board),
                                            log(Logger::getLogger(SUBSYSTEM_ID_CANCHANNEL)),
                                            m_enabled(false)
{
    /* Locally store board ID */
    updateModuleID();
}

CANChannel::~CANChannel()
{
    stopUpdate();
}


IOM_ERROR CANChannel::sendConfiguration()
{
    IOM_ERROR ret = configure();
    if (ret == IOM_ERROR_NONE)
    {
        m_enabled = true; //Allow to update observers from now on
    }
    return ret;
}


void CANChannel::setBoard(CANIOModule *board)
{
    this->board = board;

    /* Locally store board ID */
    updateModuleID();
}


IOM_ERROR CANChannel::read(IChannel::ValueStr& valuePtr)
{
    LU_UNUSED(valuePtr);

    /* Default implementation: operation unsupported */
    return IOM_ERROR_NOT_SUPPORTED;
}


IOM_ERROR CANChannel::write(IChannel::ValueStr& valuePtr)
{
    LU_UNUSED(valuePtr);

    /* Default implementation: operation unsupported */
    return IOM_ERROR_NOT_SUPPORTED;
}


IOM_ERROR CANChannel::decode(ModuleMessage* message)
{
    LU_UNUSED(message);

    /* Default implementation: operation unsupported */
    return IOM_ERROR_NOT_SUPPORTED;
}


void CANChannel::setActive(const lu_bool_t onlineStatus)
{
    if(onlineStatus == LU_FALSE)
    {
        IChannel::setActive(onlineStatus);  //Sets offline and updates observers
        m_enabled = false;    //Inhibit observers update from now on
    }
    else
    {
        m_enabled = true;     //Allow to update observers from now on
        IChannel::setActive(onlineStatus);  //Sets online and updates observers
    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
void CANChannel::updateModuleID()
{
    /* Locally store the module ID */
    if(board != NULL)
    {
        moduleID = board->getID();
    }
    else
    {
        /* Set the Id to an invalid value */
        moduleID.type = MODULE_BRD;
        moduleID.id   = MODULE_ID_BRD;
    }
}


IOM_ERROR CANChannel::sendCANMsg(IIOModule::sendReplyStr& message)
{
    if(board == NULL)
    {
        return IOM_ERROR_NOT_FOUND;
    }
    IOM_ERROR ret = board->sendMessage(&message);
    if(ret != IOM_ERROR_NONE)
    {
        log.error("%s %s/%s sendMessage error: %s",
                    __AT__, moduleID.toString().c_str(), this->getName(), IOM_ERROR_ToSTRING(ret)
                  );
    }
    return ret;
}


IOM_ERROR CANChannel::sendCANSendReply(IIOModule::sendReplyStr& message)
{
    /* Send the message */
    IOM_ERROR ret = sendCANMsg(message);    //Send the SendReply
    if(ret == IOM_ERROR_NONE)
    {
        ModReplyStr reply;  //Unused for ModReplyStr messages
        ret = processReplyStatus(message, reply); //Copies return message on reply
        /* Release reply message */
        if(message.rMessagePtr != NULL)
        {
            delete message.rMessagePtr;
            message.rMessagePtr = NULL;
        }
    }
    return ret;
}


IOM_ERROR CANChannel::sendCANConfiguration(IIOModule::sendReplyStr& message, const lu_uint32_t retries)
{
    IOM_ERROR ret;
    PayloadRAW payload;

    if(board == NULL)
    {
        return IOM_ERROR_NOT_FOUND;
    }

    log.info("%s/%s sends configuration", moduleID.toString().c_str(), this->getName());

    /* Send configuration */
    ret = board->sendMessage(&message, retries);

    if(ret == IOM_ERROR_NONE)
    {
        /* check reply */
        message.rMessagePtr->getPayload(&payload);
        if(payload.payloadLen == MODULE_MESSAGE_SIZE(ModReplyStr))
        {
            ModReplyStr *replyPtr = reinterpret_cast<ModReplyStr*>(payload.payloadPtr);
            ret = REPLY_STATUS_to_IOM_ERROR(static_cast<REPLY_STATUS>(replyPtr->status));
            if(ret == IOM_ERROR_NONE)
            {
                log.info("%s/%s configured", moduleID.toString().c_str(), this->getName());
            }
            else
            {
                log.error("%s/%s not configured. Error: %s",
                            moduleID.toString().c_str(), this->getName(),
                            IOM_ERROR_ToSTRING(ret)
                          );
                return IOM_ERROR_CONFIG;
            }
        }
        else
        {
            log.error("%s/%s reply size error: %i (expected %i)",
                        moduleID.toString().c_str(), this->getName(),
                        payload.payloadLen, MODULE_MESSAGE_SIZE(ModReplyStr)
                      );

            ret = IOM_ERROR_INVALID_MSG;
        }
    }
    else
    {
        log.error("%s/%s sendMessage error: %s",
                    moduleID.toString().c_str(), this->getName(),
                    IOM_ERROR_ToSTRING(ret)
                  );
    }

    /* Release reply message */
    if(message.rMessagePtr != NULL)
    {
        delete message.rMessagePtr;
    }

    return ret;
}


IOM_ERROR CANChannel::checkActive(const lu_char_t* commandMessage)
{
    if(this->isActive() == LU_FALSE)
    {
        log.error("%s/%s Unable to %s: channel is offline.",
                   moduleID.toString().c_str(), this->getName(),
                   (commandMessage != NULL)? commandMessage : "do"
                  );
        return IOM_ERROR_OFFLINE;
    }
    return IOM_ERROR_NONE;
}


const std::string CANChannel::logSendCommand(const lu_char_t* opType,
                                            const lu_char_t* operation,
                                            const lu_bool_t local,
                                            const lu_uint32_t transactionID
                                            )
{
    std::ostringstream oss;
    oss << __AT__ << " " << moduleID.toString() << "/" << this->getName()
        << " send " << opType << " " << operation
        << " command: ID " << (lu_uint32_t)transactionID
        << ", local " << (lu_uint32_t)local;
    return oss.str();
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */

