/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN period data manager module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   01/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_579AE078_5B23_4b71_9E13_96315076906A__INCLUDED_)
#define EA_579AE078_5B23_4b71_9E13_96315076906A__INCLUDED_



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "Mutex.h"
#include "Thread.h"
#include "Timer.h"
#include "CANIOModuleDB.h"
#include "TSynchManager.h"
#include "CANFraming.h"
#include "Logger.h"
#include "MCMLED.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class CANPeriodicData : public Thread
{
public:
    CANPeriodicData( CANFraming    &socketA    ,
                     CANFraming    &socketB    ,
                     CANIOModuleDB &modules    ,
                     TSynchManager &tsynch     ,
                     SCHED_TYPE     schedType  ,
                     lu_uint32_t    priority
                   );
    ~CANPeriodicData();

    /**
     * \brief Set the BootLoader mode.
     *
     * Sets the BootLoader Mode by setting the BootLoader bit on the HeartBeat
     * message sent by the MCM. The bit is set/reset until this function is
     * called again or the application restarts. All the slave modules should
     * not exit the BootLoader mode when they are rebooted.
     *
     * \param mode BootLoader mode to be set. LU_TRUE to set it.
     */
    void setBootloaderMode(lu_bool_t mode);

    /**
     * \brief Set the Power Saving mode.
     *
     * Sets the Power Saving Mode by setting the Power Save bit on the HeartBeat
     * message sent by the MCM. The bit is set/reset until this function is
     * called again or the application restarts. All the slave modules will
     * enter the Power Saving mode immediately.
     *
     * \param mode Power Saving mode to be set. LU_TRUE to enter Power Saving mode.
     */
    void setPowerSaveMode(lu_bool_t mode);

protected:
    /**
     * \brief Main loop
     */
    virtual void threadBody();

private:
    CANFraming &socketA;        //Primary CAN
    CANFraming &socketB;        //Secondary CAN
    CANIOModuleDB &modules;     //Modules database
    TSynchManager &tsynch;      //Time synch manager reference
    Timer cyclicTimer;          //Cyclic timer for emitting synch and HB messages
    RAWSmallModuleMessage hBeatMsg; //Heart Beat message
    MCMLED& canLED;             //CAN LED controller
    lu_uint32_t hBeatCounter;   //Tick counter to check before generating a new HBeat
    lu_uint8_t timeCounter;     //Counter to be added to the HBeat payload
    Mutex mutexHBeat;           //mutex to control modifications of the HBeat payload
    Logger& log;

    /**
     * \brief Generate HeartBeat CAN message
     */
    void generateHBeat();

    /**
     * \brief Send a restart command to all boards
     */
    void restartBoards();
};

#endif // !defined(EA_579AE078_5B23_4b71_9E13_96315076906A__INCLUDED_)


/*
 *********************** End of file ******************************************
 */
