/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN FPI Channel module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   05/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "FPICANChannel.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
FPICANChannel::FPICANChannel(lu_uint16_t ID,
                             CANIOModule *board
                             ) :
                                 OutputCANChannel(CHANNEL_TYPE_FPI, ID, board)
{}


FPICANChannel::~FPICANChannel()
{
}


IOM_ERROR FPICANChannel::operate(const lu_bool_t value, const lu_bool_t local, const lu_uint8_t delay)
{
    LU_UNUSED(value);
    LU_UNUSED(local);
    LU_UNUSED(delay);

    IIOModule::sendReplyStr message;
    FPIOperateDef operateCmd;
    PayloadRAW payload;

    if(checkActive("operate") != IOM_ERROR_NONE)
    {
        return IOM_ERROR_OFFLINE;
    }

    /* Prepare command */
    operateCmd.channel   = this->getID().id;

    payload.payloadPtr = reinterpret_cast<lu_uint8_t*>(&operateCmd);
    payload.payloadLen = sizeof(operateCmd);

    message.messageType  = MODULE_MSG_TYPE_CMD;
    message.messageID    = MODULE_MSG_ID_CMD_FPI_CH_OPERATE_C;
    message.messagePtr   = &payload;
    message.rMessageType = MODULE_MSG_TYPE_CMD;
    message.rMessagePtr  = NULL;
    message.rMessageID   = MODULE_MSG_ID_CMD_FPI_CH_OPERATE_R;
    message.timeout      = CANChannel::CAN_MSG_TIMEOUT;

    log.info("%s %s/%s send command.",
                __AT__, moduleID.toString().c_str(), this->getName()
              );

    return sendCANSendReply(message);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
