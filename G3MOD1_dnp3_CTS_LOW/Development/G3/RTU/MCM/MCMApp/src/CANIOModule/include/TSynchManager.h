/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN time synch manager module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   01/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_F348313E_4EC1_498a_A4D6_037347C0FB08__INCLUDED_)
#define EA_F348313E_4EC1_498a_A4D6_037347C0FB08__INCLUDED_


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "Mutex.h"
#include "TimeManager.h"
#include "ModuleProtocol.h"
#include "CANFraming.h"
#include "Logger.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class TSynchModuleMessage : public ModuleMessage
{
public:
    virtual MODULE_ERROR getPayload(PayloadRAW *payloadPtr);

    ModuleTsynchStr moduleTsynch;
};

class TSynchManager
{
public:
    TSynchManager(CANFraming& socketA, CANFraming& socketB);

    /**
     * \brief Send a broadcast time synch message to all the available sockets
     *
     * \return None
     */
    void synch();

    /**
     * \brief Convert module relative time into an absolute & relative timestamp
     *
     * If the module time is invalid the current time is returned
     *
     * \param moduleTimePtr Pointer to the module relative time structure to convert
     * \param resulTime Where to store the converted time
     */
    void convertTime(ModuleTimeStr* moduleTime, TimeManager::TimeStr& resulTime);

private:
    Mutex mutex;

    CANFraming &socketA;
    CANFraming &socketB;

    lu_uint8_t timeSlotCounter; //current synchronisation slot
    /**
     * \brief stores time stamps for each synchronisation slot
     */
    TimeManager::TimeStr timeDB[MODULE_TIME_MAX_TIMESLOT];
    TSynchModuleMessage TSynchMessage;
    Logger& log;
};
#endif // !defined(EA_F348313E_4EC1_498a_A4D6_037347C0FB08__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
