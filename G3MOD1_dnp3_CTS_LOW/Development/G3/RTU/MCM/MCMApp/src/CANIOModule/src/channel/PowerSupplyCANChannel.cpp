/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN Power Supply Channel module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   05/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "PowerSupplyCANChannel.h"
#include "CANIOModule.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
const lu_char_t* PowerSupplyCANChannel::MSupply_CString = "MSupply";

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
PowerSupplyCANChannel::PowerSupplyCANChannel( lu_uint16_t ID,
                                              CANIOModule *board
                                            ) : OutputCANChannel(CHANNEL_TYPE_PSUPPLY, ID, board)
{}


IOM_ERROR PowerSupplyCANChannel::select(const lu_uint16_t transactionID, const lu_bool_t local)
{
    if(checkActive(CANChannel::Select_CString) != IOM_ERROR_NONE)
    {
        return IOM_ERROR_OFFLINE;
    }

    /* Prepare command */
    SupplySelectStr selectCmd;
    selectCmd.channel       = this->getID().id;
    selectCmd.operationID   = transactionID;
    selectCmd.SelectTimeout = CANChannel::CAN_MSG_SELECT_TIMEOUT_SEC;
    selectCmd.local         = local;

    log.info("%s, selTout %i",
            logSendCommand(MSupply_CString, CANChannel::Select_CString, local, transactionID).c_str(),
            selectCmd.SelectTimeout);
    return sendCANSendReply(selectCmd,
                            MODULE_MSG_TYPE_CMD,
                            MODULE_MSG_ID_CMD_PSC_CH_SELECT_C,
                            MODULE_MSG_ID_CMD_PSC_CH_SELECT_R);
}

IOM_ERROR PowerSupplyCANChannel::operate(const lu_uint16_t transactionID,
                                         const lu_bool_t local,
                                         const lu_uint8_t duration,
                                         const lu_int32_t limit,
                                         const lu_int32_t limitDuration)
{
    if(checkActive(CANChannel::Operate_CString) != IOM_ERROR_NONE)
    {
        return IOM_ERROR_OFFLINE;
    }

    /* Prepare command */
    SupplyOperateStr operateCmd;
    operateCmd.channel        = this->getID().id;
    operateCmd.operationID    = transactionID;
    operateCmd.supplyDuration = duration;
    operateCmd.currentLimit   = limit   ;
    operateCmd.currentLimitDuration   = limitDuration;
    operateCmd.local          = local   ;

    log.info("%s, duration %i, Ilimit (%i,%i)",
            logSendCommand(MSupply_CString, CANChannel::Operate_CString, local, transactionID).c_str(),
            duration, limit, limitDuration);
    return sendCANSendReply(operateCmd,
                            MODULE_MSG_TYPE_CMD,
                            MODULE_MSG_ID_CMD_PSC_CH_OPERATE_C,
                            MODULE_MSG_ID_CMD_PSC_CH_OPERATE_R);
}


IOM_ERROR PowerSupplyCANChannel::selOperate(const lu_uint16_t transactionID,
                                            const lu_bool_t local,
                                            const lu_uint8_t duration,
                                            const lu_int32_t limit,
                                            const lu_int32_t limitDuration)
{
    /* Start of critical section to prevent select command while doing a select & operate */
    LockingMutex mMutex(sopMutex);
    IOM_ERROR iomRet = select(transactionID, local);
    if(iomRet == IOM_ERROR_NONE)
    {
        iomRet = operate(transactionID, local, duration, limit, limitDuration);
    }
    return iomRet;
}


IOM_ERROR PowerSupplyCANChannel::cancel(const lu_uint16_t transactionID,
                                        const lu_bool_t local,
                                        lu_float32_t* peakCurrent)
{
    if(checkActive(CANChannel::Cancel_CString) != IOM_ERROR_NONE)
    {
        return IOM_ERROR_OFFLINE;
    }

    /* Prepare command */
    SupplyCancelStr cancelCmd;
    SupplyCancelReplyStr reply;
    cancelCmd.channel = this->getID().id;
    cancelCmd.operationID = transactionID;
    cancelCmd.local   = local;

    log.info(logSendCommand(MSupply_CString, CANChannel::Cancel_CString, local, transactionID).c_str());

    /* Note that the peak motor current is not used here */
    IOM_ERROR ret = sendFullCANSendReply(cancelCmd,
                                        reply,
                                        MODULE_MSG_TYPE_CMD,
                                        MODULE_MSG_ID_CMD_PSC_CH_CANCEL_C,
                                        MODULE_MSG_ID_CMD_PSC_CH_CANCEL_R);
    if(peakCurrent != NULL)
    {
        *peakCurrent = reply.peakCurrent;   //Return peak current if requested
    }
    return ret;
}


lu_uint32_t PowerSupplyCANChannel::getMSupplyMaxTime() const
{
    SupplyOperateStr* messagePtr = NULL;
    static const lu_uint32_t MAXTIME = (1 << (sizeof(messagePtr->supplyDuration) * 8)) - 1;
    return MAXTIME;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
