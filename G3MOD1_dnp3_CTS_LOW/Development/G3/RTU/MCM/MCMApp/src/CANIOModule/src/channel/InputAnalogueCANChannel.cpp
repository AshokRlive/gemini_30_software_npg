/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN input Analogue channel module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   05/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <cstring>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "InputAnalogueCANChannel.h"
#include "CANIOModule.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static TimeManager& timeManager = TimeManager::getInstance();

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

InputAnalogueCANChannel::InputAnalogueCANChannel( lu_uint16_t ID,
                                                  CANIOModule *board
                                                ) :
                                                  AnalogueCANChannel(CHANNEL_TYPE_AINPUT, ID, board),
                                                  value(0)
{
     timeManager.getTime(timeStamp); //Use creation time for 1st channel timestamp
}



InputAnalogueCANChannel::~InputAnalogueCANChannel()
{}


IOM_ERROR InputAnalogueCANChannel::read(IChannel::ValueStr& valuePtr)
{
    if(valuePtr.dataPtr == NULL)
    {
        return IOM_ERROR_PARAM;
    }

    LockingMutex lMutex(mutex);

    /* save current value */
    *(valuePtr.dataPtr) = (lu_float32_t)value;

    /* Set time stamp */
    valuePtr.dataPtr->setTime(timeStamp);

    /* Set status */
    valuePtr.flags = flags;

    return IOM_ERROR_NONE;
}


IOM_ERROR InputAnalogueCANChannel::decode(ModuleMessage* message)
{
    IOM_ERROR ret = IOM_ERROR_NONE;

    if(message == NULL)
    {
        return IOM_ERROR_PARAM;
    }

    /* Decode message */
    switch(message->header.messageType)
    {
        case MODULE_MSG_TYPE_EVENT:
            if(message->header.messageID == MODULE_MSG_ID_EVENT_ANALOGUE)
            {
               ret = handleAEvent(message);
            }
        break;

        default:
            ret = IOM_ERROR_NOT_SUPPORTED;
        break;
    }


    if(ret == IOM_ERROR_NOT_SUPPORTED)
    {
        log.error("InputAnalogueCANChannel::decode: "
                    "Analogue CAN channel(%i:%i). Unsupported message: 0x%02x:0x%02x",
                    message->header.source,
                    message->header.sourceID,
                    message->header.messageType,
                    message->header.messageID
                  );
    }
    return ret;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
IOM_ERROR InputAnalogueCANChannel::configure()
{
    IIOModule::sendReplyStr message;
    ModuleCfgIoChanAIStr confCmd;
    PayloadRAW payload;

    /* Prepare command */
    confCmd.channel = this->getID().id;
    confCmd.enable = conf.enable;
    confCmd.eventEnable = conf.eventEnable;
    confCmd.eventMs = conf.eventMs;

    /* Create payload */
    payload.payloadPtr = reinterpret_cast<lu_uint8_t*>(&confCmd);
    payload.payloadLen = sizeof(confCmd);

    /* Prepare message */
    message.messageType     = MODULE_MSG_TYPE_IOMAN;
    message.messageID       = MODULE_MSG_ID_IOMAN_CFG_IOCHAN_AI_C;
    message.messagePtr      = &payload;
    message.rMessageType    = MODULE_MSG_TYPE_IOMAN;
    message.rMessagePtr     = NULL;
    message.rMessageID      = MODULE_MSG_ID_IOMAN_CFG_IOCHAN_AI_R;
    message.timeout         = CANChannel::CAN_MSG_TIMEOUT;

    return sendCANConfiguration(message, CONFIGURE_RETRY_TIMES);
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

IOM_ERROR InputAnalogueCANChannel::handleAEvent(ModuleMessage* message)
{
    IOM_ERROR ret = IOM_ERROR_NONE;
    PayloadRAW payload;

    /* Validate message */
    message->getPayload(&payload);
    if(payload.payloadLen == MODULE_MESSAGE_SIZE(ModuleAEventStr))
    {
        ModuleAEventStr *eventPtr =
                         (ModuleAEventStr *)payload.payloadPtr;

       /* Start critical region */
       {
           LockingMutex lMutex(mutex);

           /* update current value & quality */
           value = eventPtr->value;
           flags.online = eventPtr->isOnline;
           flags.restart = LU_FALSE;

           /* Update time stamp from module time */
           board->convertTime(&(eventPtr->time), timeStamp);
       }

       /* Update observers */
       updateObservers();
    }
    else
    {
       ret = IOM_ERROR_INVALID_MSG;
       log.error("InputAnalogueCANChannel::handleAEvent: "
                   "Analogue CAN channel(%i:%i). Message 0x%02x:0x%02x invalid size: %i",
                   message->header.source,
                   message->header.sourceID,
                   MODULE_MSG_TYPE_EVENT,
                   MODULE_MSG_ID_EVENT_DIGITAL,
                   payload.payloadLen
                 );
    }

    return ret;
}

/*
 *********************** End of file ******************************************
 */
