/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_58BA648C_E609_4e38_A9DD_1618D0A03953__INCLUDED_)
#define EA_58BA648C_E609_4e38_A9DD_1618D0A03953__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "ICANChannelConfigurationFactory.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class ICANModuleConfigurationFactory : public ICANChannelConfigurationFactory
{

public:
    ICANModuleConfigurationFactory() {};

    virtual ~ICANModuleConfigurationFactory() {};

	/**
	 * \brief Check if the module is configured
	 * 
	 * \param module Module type
	 * \param moduleId Module ID
	 * 
	 * \return LU_TRUE if the module is configured
	 */
    virtual lu_bool_t isConfigured(MODULE module, MODULE_ID moduleID) = 0;

    lu_bool_t isConfigured(IOModuleIDStr mid) {return isConfigured(mid.type, mid.id);}

	/**
	 * \brief Check if the module is registered
	 *
	 * Check if the module is in the module registration database
	 *
	 * \param module Module type
	 * \param moduleId Module ID
	 * \param moduleUID Module unique ID
	 *
	 * \return LU_TRUE if the module is registered
	 */
    virtual lu_bool_t isRegistered(MODULE module, MODULE_ID moduleID, ModuleUID moduleUID) = 0;

    /**
     * \brief Get module-only configuration
     *
     * Gets module-wide specific configuration (per-module configuration fields).
     *
     * \param module Module type
     * \param moduleID Module ID
     * \param conf Where to store module-specific configuration
     *
     * \return Error Code
     */
    template <typename ConfType>
    IOM_ERROR getModuleConf(MODULE module, MODULE_ID moduleID, ConfType& conf)
    {
        //Note: implemented here for being a template
        void *pConf = (void*)&conf;
        return getModuleConfig(module, moduleID, pConf);
    }

private:
    /**
     * \brief Get module-only configuration
     *
     * Gets module-wide specific configuration (per-module configuration fields).
     * NOTE: It is guaranteed that conf is never pointing to NULL.
     *
     * \param module Module type
     * \param moduleID Module ID
     * \param conf Where to store module-specific configuration.
     *
     * \return Error Code
     */
    virtual IOM_ERROR getModuleConfig(MODULE module, MODULE_ID moduleID, void* conf) = 0;

};

#endif // !defined(EA_58BA648C_E609_4e38_A9DD_1618D0A03953__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
