/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:SlaveModuleSM.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   30 Jan 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <sstream>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "LockingMutex.h"
#include "SlaveModuleSM.h"
#include "SlaveModule_fsm.h"
#include "SysAlarmMCMEnum.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
#define HBEAT_TIMEOUT_MS 2000 //Heart beat lost time out (in milliseconds)
#define MINFO_TIMEOUT_MS 4000 //Module info response time out (in milliseconds)
#define BOOT_TIMEOUT_MS 20000 //Slave booting request time out (in milliseconds)

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
SlaveModuleSM::SlaveModuleSM(const IOModuleIDStr& g_mid):
                    mid(g_mid),
                    log(Logger::getLogger(SUBSYSTEM_ID_CAN_SM)),
                    mutex(LU_TRUE),
                    fsm(NULL),
                    mInfo(),
                    hbeat(),
                    status(),
                    moduleUID(DEFAULT_MODULE_UID),
                    hbeatTimeout(0),
                    mInfoTimeout(0),
                    bootTimeout(0),
                    mInfoRetries(0),
                    hbeatTimeoutEnabled(LU_TRUE),
                    curState(FSM_STATE_LAST)
{
    fsm = new SlaveModule_fsm(*this);

    // Initialise all status
    memset(&status, 0x00, sizeof(status));
    status.active = LU_FALSE;
    status.configured = LU_FALSE;
    status.detected = LU_FALSE;
    status.disabled = LU_FALSE;
    status.present = LU_FALSE;
    status.registered = LU_FALSE;

    // Initialise local copy of heart beat message
    memset(&hbeat, 0x00, sizeof(hbeat));
    hbeat.status = MODULE_BOARD_STATUS_INVALID;

    // Initialise local copy of module info message
    memset(&mInfo, 0x00, sizeof(mInfo));
}


SlaveModuleSM::~SlaveModuleSM()
{
    delete fsm;
}

void SlaveModuleSM::init()
{
    LockingMutex lMutex(mutex);
    
    fsm->enterStartState();
    curState = static_cast<FSM_STATE>(fsm->getState().getId());
}


void SlaveModuleSM::attach(IStateChangeObserver *observer)
{
    if(observer != NULL)
    {
        // Protect the observer queue
        MasterLockingMutex mMutex(queueMutex, LU_TRUE);

        /* Add the observer to the observer list */
        if(observers.enqueue(observer) == LU_TRUE)
        {
            LockingMutex lMutex(mutex);

            //Immediately notify the observer to update observer state.
            observer->stateChanged(curState,curState);
        }
    }
}


void SlaveModuleSM::detach(IStateChangeObserver *observer)
{
    if(observer != NULL)
    {
        // Protect the observer queue
        MasterLockingMutex mMutex(queueMutex, LU_TRUE);
        observers.dequeue(observer);
    }
}

void SlaveModuleSM::setDebugEnabled(lu_bool_t debugEnabled)
{
    fsm->setDebugFlag(debugEnabled);
}

lu_bool_t SlaveModuleSM::isDebugEnabled()
{
    return fsm->getDebugFlag();
}

void SlaveModuleSM::setHBeatTimeoutEnabled(lu_bool_t enabled)
{
    this->hbeatTimeoutEnabled = enabled;
}

void SlaveModuleSM::clearHBeat()
{
    memset(&hbeat, 0x00, sizeof(hbeat));
    hbeat.status = MODULE_BOARD_STATUS_INVALID;
}

void SlaveModuleSM::getAllInfo(IOModuleInfoStr *info)
{
    if (info == NULL)
        return;

    LockingMutex lMutex(mutex);

    /* Fill the info structure */
    info->mid = mid;
    info->moduleUID = moduleUID;
    info->status = status;
    info->fsm = curState;
    info->mInfo = mInfo;
    info->hBeatInfo = hbeat;
}

void SlaveModuleSM::getDetails(IOModuleDetailStr& detail)
{
    detail.mid = this->mid;
    detail.architecture = this->mInfo.systemArch;
    detail.svnRevisionBoot = this->mInfo.svnRevisionBoot;
    detail.svnRevisionApp = this->mInfo.svnRevisionApp;
}

lu_bool_t SlaveModuleSM::getStatus(MSTATUS _status)
{
    LockingMutex lMutex(mutex);
    switch (_status) {
        case MSTATUS_PRESENT     :
            return status.present;

        case MSTATUS_CONFIGURED  :
            return status.configured;

        case MSTATUS_DISABLED    :
            return status.disabled;

        case MSTATUS_DETECTED    :
            return status.detected;

        case MSTATUS_REGISTERED  :
            return status.registered;

        case MSTATUS_ACTIVE      :
            return status.active;

        default:
            log.error("CAN Module Status not found:%i",_status);
            return LU_FALSE;
    }
}

const ModuleVersionStr& SlaveModuleSM::getSystemAPI()
{
    LockingMutex lMutex(mutex);
    return mInfo.application.systemAPI;
}

ModuleUID SlaveModuleSM::getModuleUID()
{
    LockingMutex lMutex(mutex);
    return moduleUID;
}


void SlaveModuleSM::firstHbeatRcvAction(const ModuleHBeatSStr& hbeat)
{
    moduleUID = hbeat.moduleUID;// Update board moduleUID

    log.info("%s has been detected (Heart beat received)",name());
}


void SlaveModuleSM::handleHBeatEvent(const ModuleHBeatSStr& hb)
{
    LockingMutex lMutex(mutex);
    FSM_STATE oldState = curState;
    ModuleUID oldModuleUID = this->moduleUID;

    // Update the local copy of heart beat info
    this->hbeat = hb;
    hbeatTimeout = 0;

    fsm->EVT_HBEAT_RCV(hb);

    // Check if moduleUID changed
    if( (oldModuleUID != DEFAULT_MODULE_UID) &&
        (oldModuleUID != hb.moduleUID) && (this->mid.type != MODULE_HMI) )
    {
        debugEvent("EVT_MISMATCH_MODULEUID");
        fsm->EVT_MISMATCH_MODULEUID();
        issueUIDMismatch(LU_TRUE);
    }

    curState = static_cast<FSM_STATE>(fsm->getState().getId());
    fireStateChangeEvent(oldState,curState);
}

void SlaveModuleSM::handleMInfoEvent(const MInfo& minfo)
{
    LockingMutex lMutex(mutex);
    FSM_STATE oldState = curState;

    // Update the local copy of module info
    this->mInfo = minfo;
    mInfoTimeout = 0;

    /* Check moduleUID not matched, excluding HMI */
    if( (moduleUID == minfo.moduleUID) || (minfo.moduleType == MODULE_HMI) )
    {
        debugEvent("EVT_MINFO_RCV");
        issueUIDMismatch(LU_FALSE);
        fsm->EVT_MINFO_RCV(minfo);
    }
    else
    {
        if(fsm->getState().getId() != FSM_STATE_DISABLED)
        {
            log.error("%s Invalid serial Number. Expected:%s Actual:%02d-%06d",
                        name(),
                        moduleUID.toString().c_str(),
                        minfo.moduleUID.supplierId, minfo.moduleUID.serialNumber
                        );
        }

        debugEvent("EVT_MISMATCH_MODULEUID");
        fsm->EVT_MISMATCH_MODULEUID();
        issueUIDMismatch(LU_TRUE);
    }

    curState = static_cast<FSM_STATE>(fsm->getState().getId());
    fireStateChangeEvent(oldState,curState);
}

void SlaveModuleSM::handleTickEvent(lu_uint32_t dTime)
{
    LockingMutex lMutex(mutex);
    FSM_STATE oldState;

    /*Heart beat timeout event triggering*/
    hbeatTimeout += dTime;
    if(hbeatTimeout > HBEAT_TIMEOUT_MS)
    {
        /*
         * Fire timeout event when and only when timeout enabled, the module
         * has been detected and the module is not under offline state.
         */
        if( (hbeatTimeoutEnabled == LU_TRUE) && (status.detected == LU_TRUE) )
        {
            oldState = curState;

            if(isNotState(FSM_STATE_OFFLINE))
            {
                log.warn("%s Heart-beat timeout!",name());
            }
            debugEvent("EVT_HBEAT_TIMEOUT");
            fsm->EVT_HBEAT_TIMEOUT();

            curState = static_cast<FSM_STATE>(fsm->getState().getId());
            fireStateChangeEvent(oldState,curState);
        }
        hbeatTimeout = 0;
    }

    /*Module info timeout event triggering*/
    if(isState(FSM_STATE_WAITING_INFO))
    {
        mInfoTimeout += dTime;
        if( (mInfoTimeout > MINFO_TIMEOUT_MS) && (getMInfoRetries() < MAX_MINFO_RETRIES) )
        {
            oldState = curState;
            log.warn("%s ModuleInfo reply timeout!(Retried:%d)", name(), getMInfoRetries());

            debugEvent("EVT_MINFO_TIMEOUT");
            fsm->EVT_MINFO_TIMEOUT();
            mInfoTimeout = 0;

            curState = static_cast<FSM_STATE>(fsm->getState().getId());
            fireStateChangeEvent(oldState,curState);
        }
    }
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

SlaveModuleSMState& SlaveModuleSM::getState()
{
    return fsm->getState();
}

lu_bool_t SlaveModuleSM::isState(FSM_STATE state)
{
    return (getState().getId() == state)? LU_TRUE : LU_FALSE;
}

lu_bool_t SlaveModuleSM::isNotState(FSM_STATE state)
{
    return (getState().getId() != state)? LU_TRUE : LU_FALSE;
}


void SlaveModuleSM::fireStateChangeEvent(FSM_STATE oldState, FSM_STATE newState)
{
    if(oldState != newState)
    {
        log.debug("[TRANSIT]%s state changed from \"%s\" to \"%s\"",
                        name(),
                        FSM_STATE_ToSTRING(oldState),
                        FSM_STATE_ToSTRING(newState)
                        );


        IStateChangeObserver *observerPtr;

        // Protect the observer queue
        MasterLockingMutex mMutex(queueMutex);

        // Protect the data to ensure all observers update with same data
        LockingMutex lMutex(mutex);

        /* Notify observer. Get first observer */
        observerPtr = observers.getFirst();
        while(observerPtr != NULL)
        {
            observerPtr->stateChanged(oldState,newState);

            /* Get next observer */
            observerPtr = observers.getNext(observerPtr);
        }
    }
}

void SlaveModuleSM::issueUIDMismatch(lu_bool_t activate)
{
    issueAlarm(activate, SYS_ALARM_SUBSYSTEM_MCM, SYSALC_MCM_MODULE_SERIAL);
}


/*
 *********************** End of file ******************************************
 */
