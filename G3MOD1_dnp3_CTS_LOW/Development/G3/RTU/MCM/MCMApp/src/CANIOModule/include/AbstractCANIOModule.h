/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: AbstractCANIOModule.h 22 Aug 2016 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/CANIOModule/include/AbstractCANIOModule.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 22 Aug 2016 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   22 Aug 2016   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef ABSTRACTCANIOMODULE_H__INCLUDED
#define ABSTRACTCANIOMODULE_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "InputDigitalCANChannel.h"
#include "InputAnalogueCANChannel.h"
//#include "SwitchCANChannelDSM.h"
#include "OutputDigitalCANChannel.h"
#include "CANIOModule.h"
#include "ICANModuleConfigurationFactory.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */


/*!
 * \brief Abstract class for implementing CAN Module objects
 *
 * This abstract class embeds most of the common operations on CAN module
 * objects.
 */
template <typename CANConfigStr, typename SwitchChannel>
class AbstractCANIOModule : public CANIOModule
{
public:
    AbstractCANIOModule(MODULE moduleType,
                        MODULE_ID id,
                        COMM_INTERFACE iFace,
                        TSynchManager &tsynch,
                        ICANModuleConfigurationFactory &moduleFactory,
                        const lu_uint32_t MAX_DINPUT = 0,
                        const lu_uint32_t MAX_AINPUT = 0,
                        const lu_uint32_t MAX_DOUTPUT = 0,
                        const lu_uint32_t MAX_SWOUT = 0
                        );
    virtual ~AbstractCANIOModule();

    /* == Inherited from IIOModule == */
    virtual IChannel *getChannel(CHANNEL_TYPE type, lu_uint32_t number);
    virtual void stop();

    /* == Inherited from CANIOModule == */
    virtual IOM_ERROR configure() = 0;
    virtual IOM_ERROR tickEvent(lu_uint32_t dTime);
    virtual IOM_ERROR decodeMessage(ModuleMessage* message);
    virtual IOM_ERROR loadConfiguration();

    /**
     * \brief Get module configuration
     *
     * \param config Where to get the configuration when success
     *
     * \return Error code
     */
    virtual IOM_ERROR getConfiguration(CANConfigStr& config);

protected:
    /* == Inherited from CANIOModule == */
    void setAllChsActive(lu_bool_t status);

    /**
     * \brief Does the load Configuration work
     *
     * This allows to override loadConfiguration without losing its functionality
     *
     * \return Error code
     */
    virtual IOM_ERROR doLoadConfiguration();

    /**
     * \brief Sends all module and channel configuration to the CAN module
     *
     * \param message Module configuration message, already prepared
     *
     * \return Error code
     */
    virtual IOM_ERROR sendAllConfiguration(IIOModule::sendReplyStr& message);

    /**
     * \brief Sends configuration of all channels to the CAN module
     *
     * This tries to configure all the channels even if one fails configuring.
     *
     * \return True when all channels configured OK
     */
    virtual bool sendAllChannelsConfiguration();

private:
    /**
     * \brief Implements module-specific handling of switch channel configuration
     *
     * Please implement this for the type of Switch channel needed
     *
     * \param channelID ID number of the channel to load its configuration
     *
     * \return Error code
     */
    virtual IOM_ERROR loadSwitchConfiguration(const lu_uint32_t channelID)
    {
        /* Default behaviour: no switches present on this module */
        LU_UNUSED(channelID);
        return IOM_ERROR_NONE;
    }

    /**
     * \brief Check for error after configuring a CAN module channel
     *
     * Checks for error and when failed to configure CAN module channel, it logs
     * an error message and set failure flag.
     *
     * \param errorCode Resultion error code from last operation
     * \param Type of the channel
     * \param Channel Index
     *
     * \return Value true when error happened
     *
     * \example
     *      IOM_ERROR ret = m_digInput[i].sendConfiguration();
     *      hasFailure |= checkChannelCfgFailure(ret, CHANNEL_TYPE_DINPUT, i);
     * Please note the hasFailure OR operation when using it inside a loop.
     */
    virtual bool checkChannelCfgFailure(const IOM_ERROR errorCode,
                                        const CHANNEL_TYPE type,
                                        const lu_uint32_t index);

protected:
    /* Channel storage types */

    /* WARNING: since channels are non-copyable objects, we have to use pointers
     *          to objects instead   cannot use std::vector of channels or other
     *          containers that require copy.
     */
    typedef std::vector<InputDigitalCANChannel*> DIChannels;
    typedef std::vector<InputAnalogueCANChannel*> AIChannels;
    typedef std::vector<OutputDigitalCANChannel*> DOChannels;
    typedef typename std::vector<SwitchChannel*> SWChannels;
    typedef DIChannels::iterator ItDIChannels;
    typedef AIChannels::iterator ItAIChannels;
    typedef DOChannels::iterator ItDOChannels;
    typedef typename SWChannels::iterator ItSWChannels;
//    typedef typename std::vector<SwitchChannel*>::iterator ItSWChannels;

protected:
    /* Size of channel storage */
    const lu_uint32_t m_MAX_DINPUT;
    const lu_uint32_t m_MAX_AINPUT;
    const lu_uint32_t m_MAX_DOUTPUT;
    const lu_uint32_t m_MAX_SWOUT;

protected:
    bool m_configOK;              //Module configuration is valid
    CANConfigStr m_moduleConfig;  //Module configuration

    /* Channel storage */
    DIChannels m_digInput;
    AIChannels m_anaInput;
    DOChannels m_digOutput;
    SWChannels m_swOutput;
};

/* -------------------- */


//Implementation goes here:
#include "AbstractCANIOModule_impl.hpp"



#endif /* ABSTRACTCANIOMODULE_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
