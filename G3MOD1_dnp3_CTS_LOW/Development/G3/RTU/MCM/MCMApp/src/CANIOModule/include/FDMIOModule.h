/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       FDM Module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   01/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_3E41010D_A95F_40d8_AEC0_AE84757BE55E__INCLUDED_)
#define EA_3E41010D_A95F_40d8_AEC0_AE84757BE55E__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "InputAnalogueCANChannel.h"
#include "FPICANChannel.h"
#include "CANIOModule.h"
#include "ICANModuleConfigurationFactory.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class FDMIOModule : public CANIOModule
{

public:
    FDMIOModule( MODULE_ID id,
                 COMM_INTERFACE iFace,
                 TSynchManager &tsynch,
                 ICANModuleConfigurationFactory &moduleFactory
               );
    virtual ~FDMIOModule();

    /**
     * \brief Return a channel reference
     *
     * \param type Channel type
     * \param number Channel number
     *
     * \return Channel pointer. NULL if channel is not present
     */
    virtual IChannel *getChannel(CHANNEL_TYPE type, lu_uint32_t number);

    /**
     * \brief Notify the timer tick is expired
     *
     * This function should be periodically called in order to keep the internal logic
     * updated (internal timers, etc,...)
     *
     * \param dTime Time elapsed from the previous tick (in ms)
     *
     * \return Error code
     */
    virtual IOM_ERROR tickEvent(lu_uint32_t dTime);

    /**
     * \brief Decode a module message  If necessary the module/channels internal state
     * is updated
     *
     * \param message Message to decode
     *
     * \return Error code
     */
    virtual IOM_ERROR decodeMessage(ModuleMessage* message);

     virtual IOM_ERROR configure();


     virtual IOM_ERROR loadConfiguration();

     /**
      * \brief Stop all of this module's channels from updating
      *
      * \param none
      *
      * \return none
      */
     virtual void stop();


protected:
     /**
      * \brief Update the status of all the channels
      *
      * \param status LU_TRUE if the module is online
      *
      * \return none
      */
     void setAllChsActive(lu_bool_t status);

private:
    InputAnalogueCANChannel AInput[FDM_CH_AINPUT_LAST];
    FPICANChannel FPI[FDM_CH_FPI_LAST];

};
#endif // !defined(EA_3E41010D_A95F_40d8_AEC0_AE84757BE55E__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
