/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN FPI Channel module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   05/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_5E74CD15_09B6_4ff0_9D13_0F3D5F9A1FE5__INCLUDED_)
#define EA_5E74CD15_09B6_4ff0_9D13_0F3D5F9A1FE5__INCLUDED_


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "OutputCANChannel.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * \brief This class handles the internal FPI (Fault Passage Indication).
 */
class FPICANChannel : public OutputCANChannel
{
public:
    FPICANChannel(lu_uint16_t ID = 0, CANIOModule *board = NULL);
    virtual ~FPICANChannel();

    /* === Inherited from IChannel === */
    virtual IOM_ERROR operate(const lu_bool_t value = 0, const lu_bool_t local = 0, const lu_uint8_t delay = 0);
};


#endif // !defined(EA_5E74CD15_09B6_4ff0_9D13_0F3D5F9A1FE5__INCLUDED_)


/*
 *********************** End of file ******************************************
 */
