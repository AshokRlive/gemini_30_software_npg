/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN Module class
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   29/06/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_AB764129_BD3C_4b6e_8B4B_ACEF0CC9F57C__INCLUDED_)
#define EA_AB764129_BD3C_4b6e_8B4B_ACEF0CC9F57C__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Mutex.h"
#include "TimeManager.h"
#include "ICANIOModule.h"
#include "IChannel.h"
#include "Logger.h"
#include "ICANModuleConfigurationFactory.h"
#include "TSynchManager.h"
#include "CANFraming.h"
#include "CANIOModuleSM.h"
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/**
 * Log error message and set failure flag when failed to configure CAN module channel.
 */
#define LOG_CH_CONFIG_FAILURE(ret, type)\
 if (ret != IOM_ERROR_NONE)\
    {\
        hasFailure = LU_TRUE;\
        log.error("%s Failed to configure %s channel: %i err: %i",\
                           getName(),\
                           type,\
                           i,\
                           ret\
                           );\
    }
/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * This class provides a default implementation of the decodeMessage method that
 * understands the common messages:
 * - Heart Beat
 * - Module Version
 */
class CANIOModule : public ICANIOModule , private SlaveModuleSM::IStateChangeObserver
{
public:

    CANIOModule( MODULE                          module       ,
                 MODULE_ID                       id           ,
                 COMM_INTERFACE                  iFace        ,
                 TSynchManager                  &tsynch       ,
                 ICANModuleConfigurationFactory &moduleFactory
               );
    virtual ~CANIOModule();


    /************************************/
    /*  IStateChangeObserver Interface  */
    /************************************/

    /**
     * \brief Inherited method. Override to handle state changing event.
     */
    virtual void stateChanged(FSM_STATE oldState, FSM_STATE newState)
    {
        LU_UNUSED(oldState);
        LU_UNUSED(newState);
    }


    /************************************/
    /*  IIOModule Interface             */
    /************************************/
    virtual lu_bool_t getStatus(MSTATUS status)
    {
        return sm->getStatus(status);
    }

    ModuleUID getModuleUID()
    {
        return sm->getModuleUID();
    }

    void disableTimeout(lu_bool_t disable);

    virtual IOM_ERROR decodeMessage(ModuleMessage* message);

    virtual IOM_ERROR sendMessage(ModuleMessage* messagePtr,lu_uint32_t retries = 0);

    virtual IOM_ERROR sendMessage(MODULE_MSG_TYPE messageType,
                                MODULE_MSG_ID messageID,
                                PayloadRAW *messagePtr,
                                lu_uint32_t retries = 0
                                );

    virtual IOM_ERROR sendMessage(sendReplyStr *messagePtr, lu_uint32_t retries = 0);

    virtual IOM_ERROR tickEvent(lu_uint32_t dTime);

    virtual IOM_ERROR getInfo(IOModuleInfoStr& minfo);

    virtual IOM_ERROR getDetails(IOModuleDetailStr& moduleInfo);

    virtual void getSystemAPI(ModuleVersionStr& sysAPI);

    virtual IOM_ERROR ping(lu_uint32_t timeout, lu_uint32_t payloadSize = 8);

    virtual IOM_ERROR getTime(TimeManager::TimeStr& timestamp, lu_uint32_t timeout);

    virtual lu_uint8_t alarmStatus();

    virtual ModuleAlarm getAlarm(const ModuleAlarm::AlarmIDStr id);

    virtual ModuleAlarmLister getAlarms();

    virtual ModuleAlarmLister getAlarms(const SYS_ALARM_SEVERITY severity);

    virtual IOM_ERROR ackAlarms();

    virtual IOM_ERROR readRAWChannel(IChannel::ChannelIDStr id,
                    IChannel::ValueStr& data);
    virtual IOM_ERROR sendRestartCmd(MD_RESTART type);

    virtual IOM_ERROR sendStopEventingCmd();


    /************************************/
    /* CANModule Custom interface       */
    /************************************/

    /**
     * \brief Return module CAN statistics
     *
     * \param stats Where the module CAN stats are saved
     *
     * \return Error code
     */
    virtual IOM_ERROR getCANStats(CanStatsStr &stats);

    /**
     * \brief Convert module relative time into an absolute time
     *
     * \param moduleTimePtr Pointer to the module relative time structure to convert
     * \param absTime Where to store the converted time
     *
     * \return None
     */
     void convertTime(ModuleTimeStr* moduleTimePtr, TimeManager::TimeStr& absTimePtr);

protected:
    void handleDigEvent(ModuleMessage& message);
    void handleAnaEvent(ModuleMessage& message);

    void handleDEvent(ModuleMessage *message,
                    InputDigitalCANChannel *channelPtr,
                    lu_uint8_t maxChannel
                    );

    void handleAEvent(ModuleMessage *message,
                    InputAnalogueCANChannel *channelPtr,
                    lu_uint8_t maxChannel
                    );

    /**
     * \brief Update the status of all the channels
     *
     * \param status LU_TRUE if the module is online
     *
     * \return none
     */
    virtual void setAllChsActive(lu_bool_t isActive) = 0;

    /**
     * Initialise state machine. This function must be called after all channels
     *  have been created.
     */
    void initStateMachine();

    virtual void issueAlarm(const lu_bool_t issue,
                            const SYS_ALARM_SUBSYSTEM subSystem,
                            const lu_uint32_t alarmCode);

private:
    /************************************/
    /* ICANIOModule Interface           */
    /************************************/
    void handleHBeat(ModuleMessage *message);
    void handleMInfoR(ModuleMessage *message);
    void requestModuleInfo();
    IOM_ERROR sendStartCmd(lu_uint32_t timeout);

    /**
     * \brief get the alarms from the CAN module itself
     *
     * Asks the module about the current alarms in the module itself. It also
     * logs any error in CAN communication.
     *
     * \param severity Gets list of Alarms of this severity
     * \param lister Lister object where to add the alarms
     *
     * \return Communication errors, if any
     */
    IOM_ERROR getCANAlarms(const SYS_ALARM_SEVERITY severity, ModuleAlarmLister& lister);

protected:
    ICANModuleConfigurationFactory &configFactory;
    Logger& log;

private:
    Mutex SRmutex;  //Protects Send/Reply message handling

    CANIOModuleSM* sm; //State machine

    CANFraming socket;      //CAN Socket for general messaging
    CANFraming mInfoSocket; //CAN Socket for moduleInfo messages only

    TSynchManager &tsynch;
    lu_uint32_t alarmCheck;     //Alarm checking rate counter
};

#endif // !defined(EA_AB764129_BD3C_4b6e_8B4B_ACEF0CC9F57C__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
