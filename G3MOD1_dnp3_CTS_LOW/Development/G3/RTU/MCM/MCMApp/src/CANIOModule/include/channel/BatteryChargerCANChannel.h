/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CAN Battery channel module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   05/07/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_22B8C22F_07FB_43d9_B8BE_1145E0C3AC52__INCLUDED_)
#define EA_22B8C22F_07FB_43d9_B8BE_1145E0C3AC52__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "OutputCANChannel.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */
class BatteryChargerCANChannel : public OutputCANChannel
{

public:
    BatteryChargerCANChannel(lu_uint16_t ID = 0, CANIOModule *board = NULL);
    virtual ~BatteryChargerCANChannel() {};

    /* === Inherited from CANChannel === */
    virtual IOM_ERROR selOperate(const lu_uint16_t duration, const lu_bool_t local);
    virtual IOM_ERROR cancel(const lu_bool_t local);

private:
    IOM_ERROR sendSelect(const lu_bool_t local);
    IOM_ERROR sendOperate(const lu_uint16_t duration, const lu_bool_t local);
};


#endif // !defined(EA_22B8C22F_07FB_43d9_B8BE_1145E0C3AC52__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
