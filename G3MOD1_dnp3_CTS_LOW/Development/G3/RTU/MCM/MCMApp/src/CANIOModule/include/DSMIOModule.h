/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: DSMIOModule.h 7 May 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/CANIOModule/include/DSMIOModule.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       DSM module - Dual Switch Module
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 7 May 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   7 May 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#if !defined(EA_27E02AA3_96DF_4939_AB10_46031EB47A99__INCLUDED_)
#define EA_27E02AA3_96DF_4939_AB10_46031EB47A99__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "InputDigitalCANChannel.h"
#include "InputAnalogueCANChannel.h"
#include "SwitchCANChannelDSM.h"
#include "OutputDigitalCANChannel.h"
#include "CANIOModule.h"
#include "ICANModuleConfigurationFactory.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Dual-SwitchGear Module
 *
 * This class implements the channel mapping and some other details for the DSM.
 */
class DSMIOModule : public CANIOModule
{
public:
    /**
     * \brief Custom constructor
     *
     * \param id Module ID number
     * \param iFace Communication interface
     * \param tsynch Synchronisation manager
     * \param moduleFactory Module generator
     */
    DSMIOModule(MODULE_ID id,
                COMM_INTERFACE iFace,
                TSynchManager &tsynch,
                ICANModuleConfigurationFactory &moduleFactory
              );
    virtual ~DSMIOModule();

    /* == Inherited from IIOModule == */
    virtual IChannel *getChannel(CHANNEL_TYPE type, lu_uint32_t number);
    virtual void stop();

    /* == Inherited from CANIOModule == */
    virtual IOM_ERROR tickEvent(lu_uint32_t dTime);
    virtual IOM_ERROR decodeMessage(ModuleMessage* message);
    virtual IOM_ERROR configure();
    virtual IOM_ERROR loadConfiguration();

    /**
     * \brief Get module configuration
     *
     * \param config Where to get the configuration when success
     *
     * \return Error code
     */
    virtual IOM_ERROR getConfiguration(DSMConfigStr& config)
    {
        if(!configOK)
        {
            return IOM_ERROR_CONFIG;
        }
        config = moduleConfig;
        return IOM_ERROR_NONE;
    }

    /* FIXME: pueyos_a - [#2687] temporary to prevent concurrent operations in DSM */
    bool acqReserve()
    {
        MasterLockingMutex lMutex(acqLock);
        if(!reserved)
        {
            reserved = true;
            return true;
        }
        return false;
    }
    void acqRelease()
    {
        MasterLockingMutex lMutex(acqLock, LU_TRUE);
        if(reserved)
        {
            reserved = false;
        }
    }


protected:
    /* == Inherited from CANIOModule == */
    void setAllChsActive(lu_bool_t status);

private:
    bool configOK;              //Module configuration is valid
    DSMConfigStr moduleConfig;  //Module configuration
    InputDigitalCANChannel BInput[DSM_CH_DINPUT_LAST];
    InputAnalogueCANChannel AInput[DSM_CH_AINPUT_LAST];
    OutputDigitalCANChannel DOutput[DSM_CH_DOUT_LAST];
    SwitchCANChannelDSM switchOut[DSM_CH_SWOUT_LAST];

    /* FIXME: pueyos_a - [#2687] temporary to prevent concurrent operations in DSM */
    MasterMutex acqLock;
    bool reserved;

};

#endif // !defined(EA_27E02AA3_96DF_4939_AB10_46031EB47A99__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
