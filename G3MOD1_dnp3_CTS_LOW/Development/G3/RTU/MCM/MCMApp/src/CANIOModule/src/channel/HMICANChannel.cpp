/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       HMI Channel implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   20/02/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */
#include <cstring>

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "HMICANChannel.h"
#include "CANIOModule.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static const lu_uint32_t HmiDisplayWriteStrSize = sizeof(HmiDisplayWriteStr);

/* Display char mapping for the LCD HD44780UA00 chip
 * The map tries to substitute UTF-8 characters to the LCD display driver with
 * ROM code A00. Substitution is made by most similar character available - when
 * not possible, it is substituted by a space (0x20).
 */
static const lu_uint8_t HD44780UA00_map[256] =
{
    /*-0    -1    -2    -3    -4    -5    -6    -7    -8    -9    -A    -B    -C    -D    -E    -F*/
    0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, //0-
    0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, //1-
    0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2a, 0x2b, 0x2c, 0x2d, 0x2e, 0x2f, //2-
    0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b, 0x3c, 0x3d, 0x3e, 0x3f, //3-
    0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d, 0x4e, 0x4f, //4-
    0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5a, 0x5b, 0xA4, 0x5d, 0x5e, 0x5f, //5-
    0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6a, 0x6b, 0x6c, 0x6d, 0x6e, 0x6f, //6-
    0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x7a, 0x7b, 0x7c, 0x7d, 0xDE, 0x20, //7-
    0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, //8-
    0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, //9-
    0x20, 0x21, 0xEC, 0xED, 0x6F, 0x5C, 0x7C, 0x53, 0xDE, 0x63, 0x61, 0x7F, 0xB0, 0x20, 0x52, 0xB0, //A-
    0xDF, 0xC5, 0x32, 0x33, 0x60, 0xE4, 0xF7, 0xA5, 0x2C, 0x31, 0xDF, 0x7E, 0x20, 0x20, 0x20, 0x20, //B-
    0x41, 0x41, 0x41, 0x41, 0xE1, 0x41, 0x45, 0xEC, 0x45, 0x45, 0x45, 0x45, 0x49, 0x49, 0x49, 0x49, //C-
    0x44, 0xEE, 0x4F, 0x4F, 0x4F, 0x4F, 0xEF, 0x78, 0x4F, 0x55, 0x55, 0x55, 0xF5, 0x59, 0x20, 0xE2, //D-
    0x61, 0x61, 0x61, 0x61, 0xE1, 0x61, 0x20, 0xEC, 0x65, 0x65, 0x65, 0x65, 0x69, 0x69, 0x69, 0x69, //E-
    0x20, 0xEE, 0x6F, 0x6F, 0x6F, 0x6F, 0xEF, 0xFD, 0x6F, 0x75, 0x75, 0x75, 0xF5, 0x79, 0x20, 0x79  //F-
};


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

LCDData::LCDData(lu_uint16_t bufferSize) : bufferSize(bufferSize), buffer(NULL)
{
    /* Allocate RAW buffer.
     * Allocate a big enough buffer to
     * accommodate both the header and the payload
     */
    buffer = new lu_uint8_t[bufferSize + HmiDisplayWriteStrSize];

    /* Reset initial position and length */
    setPosition(0,0,0);
}


LCDData::~LCDData()
{
    /* Release the buffer */
    if(buffer != NULL)
    {
        delete[] buffer;
    }
}


IOM_ERROR LCDData::getLCDBuffer(PayloadRAW &buffer)
{
    buffer.payloadLen = bufferSize;
    /* Return only the payload buffer */
    buffer.payloadPtr = &(this->buffer[HmiDisplayWriteStrSize]);

    return IOM_ERROR_NONE;
}


IOM_ERROR LCDData::setPosition(lu_uint8_t posRow, lu_uint8_t posColumn, lu_uint8_t len)
{
    if(buffer != NULL)
    {
        /* Get message header */
        HmiDisplayWriteStr *headerPtr =
                                  reinterpret_cast<HmiDisplayWriteStr*>(buffer);

        if(len <= bufferSize)
        {
            headerPtr->posRow    = posRow;
            headerPtr->posColumn = posColumn;
            headerPtr->bufferLen = len;

            return IOM_ERROR_NONE;
        }
        else
        {
            return IOM_ERROR_PARAM;
        }
    }
    else
    {
        return IOM_ERROR_PAYLOAD;
    }
}


IOM_ERROR LCDData::getPosition(lu_uint8_t &posRow, lu_uint8_t &posColumn, lu_uint8_t &len)
{
    if(buffer != NULL)
    {
        /* Get message header */
        HmiDisplayWriteStr *headerPtr =
                                  reinterpret_cast<HmiDisplayWriteStr*>(buffer);

        posRow    = headerPtr->posRow   ;
        posColumn = headerPtr->posColumn;
        len       = headerPtr->bufferLen;

        return IOM_ERROR_NONE;
    }
    else
    {
        return IOM_ERROR_PAYLOAD;
    }
}


IOM_ERROR LCDData::getRAWBuffer(PayloadRAW &buffer)
{
    /* Get message header */
    HmiDisplayWriteStr *headerPtr =
                            reinterpret_cast<HmiDisplayWriteStr*>(this->buffer);

    /* Return the entire RAW buffer */
    reEncode();
    buffer.payloadLen = headerPtr->bufferLen + HmiDisplayWriteStrSize;
    buffer.payloadPtr = this->buffer;

    return IOM_ERROR_NONE;
}


void LCDData::reEncode()
{
    /*Note: Here, UTF-8 is translated roughly to HD44780U ROM A00 charset.
     */

    /* Skip message header and replace all possible chars */
    lu_uint32_t bufSize = + bufferSize + HmiDisplayWriteStrSize;
    for (lu_uint32_t idx = HmiDisplayWriteStrSize; idx < bufSize; ++idx)
    {
        buffer[idx] = HD44780UA00_map[buffer[idx]];
    }
}




HMICANChannel::HMICANChannel(lu_uint16_t ID, CANIOModule *board) :
                                  CANChannel(CHANNEL_TYPE_HMI, ID, board),
                                  lastEvent(HMICANChannel::HMI_EVENT_TYPE_NONE),
                                  buttonState(0),
                                  olrRequest(HMI_OLR_STATE_INVALID)
{
    memset(&conf, 0, sizeof(conf));
}


IOM_ERROR HMICANChannel::decode(ModuleMessage* message)
{
    IOM_ERROR ret = IOM_ERROR_NONE;

    if(message == NULL)
    {
        return IOM_ERROR_PARAM;
    }

    /* Decode message */
    switch(message->header.messageType)
    {
        case MODULE_MSG_TYPE_HMI_EVENT:
            if(message->header.messageID == MODULE_MSG_ID_HMI_EVENT_BUTTON)
            {
                ret = handleButtonEvent(message);
            }
            else if (message->header.messageID == MODULE_MSG_ID_HMI_EVENT_REQ_OLR_CHANGE)
            {
                ret = handleOLREvent(message);
            }
            else
            {
                ret = IOM_ERROR_NOT_SUPPORTED;
            }
        break;

        default:
            ret = IOM_ERROR_NOT_SUPPORTED;
        break;
    }


    if(ret == IOM_ERROR_NOT_SUPPORTED)
    {
        log.error("HMICANChannel::CAN decode channel(%i:%i). Unsupported message: 0x%02x:0x%02x",
                        message->header.source,
                        message->header.sourceID,
                        message->header.messageType,
                        message->header.messageID
                      );
    }

    return IOM_ERROR_NONE;
}

IOM_ERROR HMICANChannel::getDisplayInfo( struct timeval &timeout,
                                         lu_int8_t      &numRows,
                                         lu_int8_t      &numColums
                                       )
{
    const lu_char_t* FTITLE = "HMICANChannel::getDisplayInfo:";
    IOM_ERROR ret;
    IIOModule::sendReplyStr message;
    PayloadRAW payload;

    /* Prepare command */
    payload.payloadPtr = NULL;
    payload.payloadLen = 0;

    message.messageType  = MODULE_MSG_TYPE_CMD;
    message.messageID    = MODULE_MSG_ID_CMD_HMC_GET_DISPLAY_INFO_C;
    message.messagePtr   = &payload;
    message.rMessageType = MODULE_MSG_TYPE_CMD;
    message.rMessagePtr  = NULL;
    message.rMessageID   = MODULE_MSG_ID_CMD_HMC_GET_DISPLAY_INFO_R;
    message.timeout      = timeout;

    log.info("%s %s/%s send command", FTITLE, moduleID.toString().c_str(), this->getName());

    ret = board->sendMessage(&message);
    if(ret == IOM_ERROR_NONE)
    {
        /* check reply */
        message.rMessagePtr->getPayload(&payload);
        if(payload.payloadLen != MODULE_MESSAGE_SIZE(HmiDisplayInfoReplyStr))
        {
            log.error("%s %s/%s reply size error: %i (expected %i)",
                        FTITLE, moduleID.toString().c_str(), this->getName(),
                        payload.payloadLen, MODULE_MESSAGE_SIZE(HmiDisplayInfoReplyStr)
                      );

            ret = IOM_ERROR_INVALID_MSG;
        }
        else
        {
            /* Valid size - Decode message */
            HmiDisplayInfoReplyStr *replyPtr =
                   reinterpret_cast<HmiDisplayInfoReplyStr*>(payload.payloadPtr);

            /* Save information */
            numRows   = replyPtr->numRows  ;
            numColums = replyPtr->numColums;

            log.info("%s %s/%s Rows: %i - Columns: %i",
                        FTITLE, moduleID.toString().c_str(), this->getName(),
                        replyPtr->numRows, replyPtr->numColums
                      );
        }
    }
    else
    {
        log.error("%s %s/%s sendMessage error %i: %s",
                    FTITLE, moduleID.toString().c_str(), this->getName(),
                    ret, IOM_ERROR_ToSTRING(ret)
                  );
    }

    /* Release reply message */
    if(message.rMessagePtr != NULL)
    {
        delete message.rMessagePtr;
    }

    return ret;
}

IOM_ERROR HMICANChannel::getButtons( struct timeval &timeout,
                                     lu_uint32_t &buttonBitMask
                                   )
{
    const lu_char_t* FTITLE = "HMICANChannel::getButtons:";
    IOM_ERROR ret;
    IIOModule::sendReplyStr message;
    PayloadRAW payload;

    /* Prepare command */
    payload.payloadPtr = NULL;
    payload.payloadLen = 0;

    message.messageType  = MODULE_MSG_TYPE_CMD;
    message.messageID    = MODULE_MSG_ID_CMD_HMC_GET_BUTTON_STATE_C;
    message.messagePtr   = &payload;
    message.rMessageType = MODULE_MSG_TYPE_CMD;
    message.rMessagePtr  = NULL;
    message.rMessageID   = MODULE_MSG_ID_CMD_HMC_GET_BUTTON_STATE_R;
    message.timeout      = timeout;

    log.debug("%s %s/%s send command", FTITLE, moduleID.toString().c_str(), this->getName());

    ret = board->sendMessage(&message);
    if(ret == IOM_ERROR_NONE)
    {
        /* check reply */
        message.rMessagePtr->getPayload(&payload);
        if(payload.payloadLen != MODULE_MESSAGE_SIZE(HmiButtonStateReplyStr))
        {
            log.error("%s %s/%s reply size error: %i (expected %i)",
                        FTITLE, moduleID.toString().c_str(), this->getName(),
                        payload.payloadLen, MODULE_MESSAGE_SIZE(HmiButtonStateReplyStr)
                      );

            ret = IOM_ERROR_INVALID_MSG;
        }
        else
        {
            /* Valid size - Decode message */
            HmiButtonStateReplyStr *replyPtr =
                   reinterpret_cast<HmiButtonStateReplyStr*>(payload.payloadPtr);

            /* Save information */
            buttonBitMask = replyPtr->state.buttonBitMask;

            log.debug("%s %s/%s state 0x%08x",
                    FTITLE, moduleID.toString().c_str(), this->getName(), replyPtr->state.buttonBitMask
                  );
        }
    }
    else
    {
        log.error("%s %s/%s sendMessage error %i: %s",
                    FTITLE, moduleID.toString().c_str(), this->getName(),
                    ret, IOM_ERROR_ToSTRING(ret)
                  );
    }

    /* Release reply message */
    if(message.rMessagePtr != NULL)
    {
        delete message.rMessagePtr;
    }

    return ret;
}

IOM_ERROR HMICANChannel::setLeds(lu_uint32_t ledBitMask)
{
    IIOModule::sendReplyStr message;
    PayloadRAW payload;
    HmiLEDWriteStr writeLedCmd;

    if(checkActive("setLeds") != IOM_ERROR_NONE)
    {
        return IOM_ERROR_OFFLINE;
    }

    /* Prepare command */
    writeLedCmd.ledBitMask = ledBitMask;

    payload.payloadPtr = reinterpret_cast<lu_uint8_t*>(&writeLedCmd);
    payload.payloadLen = sizeof(writeLedCmd);

    message.messageType  = MODULE_MSG_TYPE_CMD;
    message.messageID    = MODULE_MSG_ID_CMD_HMC_LED_WRITE_C;
    message.messagePtr   = &payload;
    message.rMessageType = MODULE_MSG_TYPE_CMD;
    message.rMessagePtr  = NULL;
    message.rMessageID   = MODULE_MSG_ID_CMD_HMC_LED_WRITE_R;
    message.timeout      = CANChannel::CAN_MSG_TIMEOUT;

    log.debug("%s %s/%s send command(ledBitMask: 0x%04x)",
                __AT__, moduleID.toString().c_str(), this->getName(), ledBitMask
              );

    return sendCANSendReply(message);
}


IOM_ERROR HMICANChannel::setOLRState(HMI_OLR_STATE state)
{
    IIOModule::sendReplyStr message;
    PayloadRAW payload;
    HmiDisplayOLRStr olrCmd;

    if(checkActive("setOLRState") != IOM_ERROR_NONE)
    {
        return IOM_ERROR_OFFLINE;
    }

    /* Prepare command */
    olrCmd.displayHMIOLRState = state;

    payload.payloadPtr = reinterpret_cast<lu_uint8_t*>(&olrCmd);
    payload.payloadLen = sizeof(olrCmd);

    message.messageType  = MODULE_MSG_TYPE_CMD;
    message.messageID    = MODULE_MSG_ID_CMD_HMC_DISPLAY_OLR_C;
    message.messagePtr   = &payload;
    message.rMessageType = MODULE_MSG_TYPE_CMD;
    message.rMessagePtr  = NULL;
    message.rMessageID   = MODULE_MSG_ID_CMD_HMC_DISPLAY_OLR_R;
    message.timeout      = CANChannel::CAN_MSG_TIMEOUT;

    log.debug("%s %s/%s send command(state %i)",
                __AT__, moduleID.toString().c_str(), this->getName(), state
              );

    return sendCANSendReply(message);
}


IOM_ERROR HMICANChannel::writeLCD(struct timeval &timeout, LCDData &data)
{
    IIOModule::sendReplyStr message;
    PayloadRAW payload;
    lu_uint8_t posRow;
    lu_uint8_t posColumn;
    lu_uint8_t len;

    if(checkActive("writeLCD") != IOM_ERROR_NONE)
    {
        return IOM_ERROR_OFFLINE;
    }

    /* Prepare command */
    data.getRAWBuffer(payload);

    /* Get header */
    data.getPosition(posRow, posColumn, len);

    message.messageType  = MODULE_MSG_TYPE_CMD;
    message.messageID    = MODULE_MSG_ID_CMD_HMC_DISPLAY_WRITE_C;
    message.messagePtr   = &payload;
    message.rMessageType = MODULE_MSG_TYPE_CMD;
    message.rMessagePtr  = NULL;
    message.rMessageID   = MODULE_MSG_ID_CMD_HMC_DISPLAY_WRITE_R;
    message.timeout      = timeout;

    log.debug("%s %s/%s send command(posRow: %i - posColumn: %i - Len: %i)",
                __AT__, moduleID.toString().c_str(), this->getName(),
                posRow, posColumn, len
              );

    return sendCANSendReply(message);
}


IOM_ERROR HMICANChannel::synchLCD(struct timeval &timeout, lu_bool_t fullSync)
{
    IIOModule::sendReplyStr message;
    PayloadRAW payload;
    HmiDisplaySyncStr syncCmd;

    if(checkActive("synchLCD") != IOM_ERROR_NONE)
    {
        return IOM_ERROR_OFFLINE;
    }

    /* Prepare command */
    syncCmd.forceFullUpdate = fullSync;

    payload.payloadPtr = reinterpret_cast<lu_uint8_t*>(&syncCmd);
    payload.payloadLen = sizeof(syncCmd);

    message.messageType  = MODULE_MSG_TYPE_CMD;
    message.messageID    = MODULE_MSG_ID_CMD_HMC_DISPLAY_SYNC_C;
    message.messagePtr   = &payload;
    message.rMessageType = MODULE_MSG_TYPE_CMD;
    message.rMessagePtr  = NULL;
    message.rMessageID   = MODULE_MSG_ID_CMD_HMC_DISPLAY_SYNC_R;
    message.timeout      = timeout;

    log.debug("%s %s/%s send command(full sync: %i)",
                __AT__, moduleID.toString().c_str(), this->getName(), fullSync
              );

    return sendCANSendReply(message);
}


IOM_ERROR HMICANChannel::clearLCD( struct timeval &timeout,
                                   lu_uint8_t posRow,
                                   lu_uint8_t posColumn,
                                   lu_uint8_t numChars
                                 )
{
    IIOModule::sendReplyStr message;
    PayloadRAW payload;
    HmiDisplayClearStr clearCmd;

    if(checkActive("clearLCD") != IOM_ERROR_NONE)
    {
        return IOM_ERROR_OFFLINE;
    }

    /* Prepare command */
    clearCmd.posRow    = posRow;
    clearCmd.posColumn = posColumn;
    clearCmd.numChars  = numChars;

    payload.payloadPtr = reinterpret_cast<lu_uint8_t*>(&clearCmd);
    payload.payloadLen = sizeof(clearCmd);

    message.messageType  = MODULE_MSG_TYPE_CMD;
    message.messageID    = MODULE_MSG_ID_CMD_HMC_DISPLAY_CLEAR_C;
    message.messagePtr   = &payload;
    message.rMessageType = MODULE_MSG_TYPE_CMD;
    message.rMessagePtr  = NULL;
    message.rMessageID   = MODULE_MSG_ID_CMD_HMC_DISPLAY_CLEAR_R;
    message.timeout      = timeout;

    log.debug("%s %s/%s send command(posRow: %i - posColumn: %i - numChars: %i)",
                __AT__, moduleID.toString().c_str(), this->getName(), posRow, posColumn, numChars
              );

    return sendCANSendReply(message);
}


IOM_ERROR HMICANChannel::clearLCD(struct timeval &timeout)
{
    return clearLCD(timeout, 0, 0, 0);
}


IOM_ERROR HMICANChannel::playSound( struct timeval &timeout,
                                    lu_int16_t frequencyHz,
                                    lu_int16_t durationMs
                                  )
{
    IIOModule::sendReplyStr message;
    PayloadRAW payload;
    HmiPlaySoundStr palySCmd;

    if(checkActive("playSound") != IOM_ERROR_NONE)
    {
        return IOM_ERROR_OFFLINE;
    }

    /* Prepare command */
    palySCmd.frequencyHz = frequencyHz;
    palySCmd.durationMs  = durationMs;

    payload.payloadPtr = reinterpret_cast<lu_uint8_t*>(&palySCmd);
    payload.payloadLen = sizeof(palySCmd);

    message.messageType  = MODULE_MSG_TYPE_CMD;
    message.messageID    = MODULE_MSG_ID_CMD_HMC_PLAY_SOUND_C;
    message.messagePtr   = &payload;
    message.rMessageType = MODULE_MSG_TYPE_CMD;
    message.rMessagePtr  = NULL;
    message.rMessageID   = MODULE_MSG_ID_CMD_HMC_PLAY_SOUND_R;
    message.timeout      = timeout;

    log.debug("%s %s/%s send command (frequencyHz: %i - durationMs: %i)",
                __AT__, moduleID.toString().c_str(), this->getName(), frequencyHz, durationMs
              );

    return sendCANSendReply(message);

}

HMICANChannel::HMI_EVENT_TYPE HMICANChannel::getLastEvent()
{
    /* Start critical region */
    LockingMutex lMutex(mutex);

    return lastEvent;
}

IOM_ERROR HMICANChannel::getButtonsEvent(lu_uint32_t &state)
{
    /* Start critical region */
    LockingMutex lMutex(mutex);
    state = buttonState;

    return IOM_ERROR_NONE;
}

IOM_ERROR HMICANChannel::getOlrRequest(HMI_OLR_STATE &olrRequest)
{
    /* Start critical region */
    LockingMutex lMutex(mutex);
    olrRequest = this->olrRequest;

    return IOM_ERROR_NONE;
}
/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
IOM_ERROR HMICANChannel::configure()
{
    IIOModule::sendReplyStr message;
    PayloadRAW payload;

    /* Create payload */
    payload.payloadPtr = reinterpret_cast<lu_uint8_t*>(&conf);
    payload.payloadLen = sizeof(conf);

    /* Prepare message */
    message.messageType     = MODULE_MSG_TYPE_CFG;
    message.messageID       = MODULE_MSG_ID_CFG_HMI_CONTROLLER_C;
    message.messagePtr      = &payload;
    message.rMessageType    = MODULE_MSG_TYPE_CFG;
    message.rMessagePtr     = NULL;
    message.rMessageID      = MODULE_MSG_ID_CFG_HMI_CONTROLLER_R;
    message.timeout.tv_sec  = CANChannel::CAN_MSG_TIMEOUT_SEC;
    message.timeout.tv_usec = CANChannel::CAN_MSG_TIMEOUT_USEC;

    return sendCANConfiguration(message, CONFIGURE_RETRY_TIMES);
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
IOM_ERROR HMICANChannel::handleButtonEvent(ModuleMessage *message)
{
    IOM_ERROR ret = IOM_ERROR_NONE;
    PayloadRAW payload;

    /* Validate message */
    message->getPayload(&payload);
    if(payload.payloadLen == MODULE_MESSAGE_SIZE(HmiButtonEventStr))
    {
        HmiButtonEventStr *eventPtr = (HmiButtonEventStr *)payload.payloadPtr;

       /* Start critical region */
       {
           LockingMutex lMutex(mutex);

           /* Update last event */
           lastEvent = HMICANChannel::HMI_EVENT_TYPE_BUTTON;

           /* Save current value */
           buttonState = eventPtr->state.buttonBitMask;
       }

       /* Update observers */
       updateObservers();
    }
    else
    {
       ret = IOM_ERROR_INVALID_MSG;
       log.error("HMICANChannel::handleButtonEvent: "
                   "HMI CAN channel(%i:%i). Message 0x%02x:0x%02x invalid size: %i",
                   message->header.source,
                   message->header.sourceID,
                   MODULE_MSG_TYPE_HMI_EVENT,
                   MODULE_MSG_ID_HMI_EVENT_BUTTON,
                   payload.payloadLen
                );
    }

    return ret;
}


IOM_ERROR HMICANChannel::handleOLREvent(ModuleMessage *message)
{
    IOM_ERROR ret = IOM_ERROR_NONE;
    PayloadRAW payload;

    /* Validate message */
    message->getPayload(&payload);
    if(payload.payloadLen == MODULE_MESSAGE_SIZE(HmiReqOLRChangeEventStr))
    {
        HmiReqOLRChangeEventStr *eventPtr = (HmiReqOLRChangeEventStr *)payload.payloadPtr;

       /* Start critical region */
       {
           LockingMutex lMutex(mutex);

           /* Update last event */
           lastEvent = HMICANChannel::HMI_EVENT_TYPE_OLR;

           /* Save current request */
           if(eventPtr->requestedHMIOLRState <= HMI_OLR_STATE_INVALID)
           {
               olrRequest = static_cast<HMI_OLR_STATE>(eventPtr->requestedHMIOLRState);
           }
       }

       /* Update observers */
       updateObservers();
    }
    else
    {
       ret = IOM_ERROR_INVALID_MSG;
       log.error("HMICANChannel::handleOLREvent: "
                   "HMI CAN channel(%i:%i). Message 0x%02x:0x%02x invalid size: %i",
                   message->header.source,
                   message->header.sourceID,
                   MODULE_MSG_TYPE_HMI_EVENT,
                   MODULE_MSG_ID_HMI_EVENT_REQ_OLR_CHANGE,
                   payload.payloadLen
                );
    }

    return ret;
}

/*
 *********************** End of file ******************************************
 */
