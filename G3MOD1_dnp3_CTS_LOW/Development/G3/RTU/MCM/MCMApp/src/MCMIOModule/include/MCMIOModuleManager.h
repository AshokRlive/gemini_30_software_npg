/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       DMCM Module manager interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   08/08/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_BCCC6193_FA4A_4fc3_8FB4_A0420B3B0F51__INCLUDED_)
#define EA_BCCC6193_FA4A_4fc3_8FB4_A0420B3B0F51__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "Thread.h"
#include "Timer.h"
#include "Logger.h"
#include "IIOModuleManager.h"
#include "MCMIOModule.h"
#include "IModuleStatusMonitor.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class MCMIOModuleManager : private Thread,
                            public IIOModuleManager,
                            public ISupportPowerSaving
{
public:
    /**
     * Custom constructor
     *
     * Initialises the MCM module manager
     *
     * \param schedType Schedule type for the associated thread
     * \param priority Priority of the associated thread
     * \param cfgMgr Reference to the Configuration Manager
     * \param memManager Reference to the Memory Manager
     * \param monProtocol Reference to the Monitor Protocol Manager
     */
    MCMIOModuleManager( SCHED_TYPE schedType ,
                        lu_uint32_t priority ,
                        ConfigurationManager &cfgMgr,
                        IMemoryManager& memManager,
                        MonitorProtocolManager& monProtocol
                      );
    virtual ~MCMIOModuleManager();


    /**
     * \brief Set the power Saving mode
     *
     * \param mode Power saving mode to be set
     */
    virtual void setPowerSaveMode(lu_bool_t mode);

    /**
     * \brief Configure the Module Status Monitor
     *
     * \param Module Status Monitor instance
     */
    virtual void setModuleStatusMonitor(IModuleStatusMonitor* moduleStatusMonitor);

     /**
     * \brief Create a module
     *
     * Creates a module even if it is not connected
     *
     * \param module Module type
     * \param moduleID Module ID number
     * \param iFace Interface where it is supposed to be connected
     *
     * \return pointer to the module. NULL if it wasn't successful.
     */
    virtual IIOModule *createModule(MODULE module, MODULE_ID moduleID, COMM_INTERFACE iFace);


    /**
     * \brief Get a module reference
     *
     * \param module Module type
     * \param moduleId Module Identifier (address)
     *
     * \return Pointer to the module. NULL if the module doesn't exist
     */
    virtual IIOModule * getModule(MODULE module, MODULE_ID moduleID);

    virtual std::vector<IIOModule*> getAllModules();

    /**
     * \brief Get a channel reference
     *
     * \param module Module type
     * \param moduleId Module Identifier (address)
     * \param chType Channel type
     * \param chNumber Channel Number
     *
     * \return Pointer to the channel. NULL if the channel/module doesn't exist
     */
    virtual IChannel *getChannel( MODULE module,
                                  MODULE_ID moduleID,
                                  CHANNEL_TYPE chType,
                                  lu_uint32_t chNumber
                                );

    /**
     * \brief Stop all modules
     *
     * \param none
     *
     * \return Error Code
     */
    virtual IOM_ERROR stopAllModules();

protected:
    /**
     * \brief Thread main loop
     *
     * This function is called automatically when a thread is started
     */
    virtual void threadBody();

private:
    static const struct timeval backgroundTime; //Time for tickevent, in (sec,usec)

private:
	/* overridden methods */
    THREAD_ERR stop();

private:
	MCMIOModule mcm;
	Logger& log;
	IModuleStatusMonitor* moduleStatus;

	Timer backGroundTimer;
};
#endif // !defined(EA_BCCC6193_FA4A_4fc3_8FB4_A0420B3B0F51__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
