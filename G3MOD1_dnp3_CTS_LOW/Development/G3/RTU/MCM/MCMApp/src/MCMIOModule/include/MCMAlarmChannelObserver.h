/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: MCMAlarmChannelObserver.h 9 Jan 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/MCMIOModule/include/MCMAlarmChannelObserver.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MCM Module Channels monitoring unit for Enabling related Module Alarm.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 9 Jan 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   9 Jan 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MCMALARMCHANNELOBSERVER_H__INCLUDED
#define MCMALARMCHANNELOBSERVER_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "IChannel.h"
#include "ChannelObserver.h"
#include "IIOModule.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
/* Forward declaration */
class MCMAlarmChannelObserver;

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
typedef std::vector<MCMAlarmChannelObserver*> MCMAlarmObserverListPtr;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 *   \brief MCM alarm monitoring unit
 *
 * This unit checks a channel to see if there is a condition related with it
 * that can trigger an alarm, and notifies it.
 */
class MCMAlarmChannelObserver: public ChannelObserver
{
public:
    /** \brief Type of Channel observer
     *
     * Supervision type: states which kind of change/event we are monitoring in
     * the channel.
     */
    typedef enum
    {
        CHANNELALARMTYPE_ALARMFLAG, //Notify when the channel's alarm flag is active
        CHANNELALARMTYPE_ALARMFLAG_AI, //Notify when the channel's alarm flag is active
                                    // and store AI value (x1000) in the parameter.
                                    // Parameter value is capped if reached max parameter capacity.
        CHANNELALARMTYPE_ALARMFLAG_AI_POS, //Notify when the channel's alarm flag is active
                                    // and only when the value is positive,
                                    // then store AI value in the parameter.
                                    // Parameter value is capped if reached max parameter capacity.
        CHANNELALARMTYPE_ALARMFLAG_AI_NEG, //Notify when the channel's alarm flag is active
                                    // and only when the value is negative,
                                    // then store AI value in the parameter as a positive value.
                                    // Parameter value is capped if reached max parameter capacity.
        CHANNELALARMTYPE_DIVALUE,   //Notify when the channel's  DI value is active
        CHANNELALARMTYPE_DIVALUEINV //Notify when the channel's  DI value is inactive (inverted)
    } CHANNELALARMTYPE;
public:
    /**
     * \brief Custom constructor
     *
     * \param channel Channel to be supervised
     * \param typeAlarm Type of supervision used in the channel
     * \param alarmRef Module alarm parameters used in case of issuing an alarm
     * \param moduleMCM Reference to the MCM Module (for alarm reporting)
     */
    MCMAlarmChannelObserver(//lu_uint32_t id,
                    IChannel& channel,
                    CHANNELALARMTYPE typeAlarm,
                    ModuleAlarm alarmRef,
                    IIOModule& moduleMCM
                    );
    virtual ~MCMAlarmChannelObserver();

    /**
     * \brief Gets the supervised channel ID
     *
     * \return Supervised channel ID
     */
    IChannel& getChannel() {return channel;};

    /**
     * \brief Gets the type of supervision we are using in the channel
     *
     * \return Type of supervision used over the channel.
     */
    CHANNELALARMTYPE getType() {return alarmType;};

private:
    //inherited from channelObserver
    virtual void update(IChannel *subject);

private:
    IIOModule& moduleMCM;       //MCM module for Module Alarm reporting
    IChannel& channel;          //Channel to supervise
    CHANNELALARMTYPE alarmType; //Type of supervision used in the channel in order to issue an alarm
    ModuleAlarm alarm;          //Keeps the Module Alarm parameters (ID, severity, param)
};

#endif /* MCMALARMCHANNELOBSERVER_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
