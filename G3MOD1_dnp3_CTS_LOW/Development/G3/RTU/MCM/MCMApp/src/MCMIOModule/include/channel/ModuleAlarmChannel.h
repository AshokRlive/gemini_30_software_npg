/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ModuleAlarmChannel.h 21 Jan 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/MCMIOModule/include/ModuleAlarmChannel.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       RTU Module Alarm monitoring channel.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 21 Jan 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   21 Jan 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MODULEALARMCHANNEL_H__INCLUDED
#define MODULEALARMCHANNEL_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "MCMVirtualDIChannel.h"
#include "ModuleManager.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 *   \brief RTU Module alarm monitoring channel
 *
 * This unit checks the module alarms and notifies in a channel value if there
 * is any alarm active in any of the modules of the RTU.
 * To be more efficient, it checks first MCM's alarms and then all of the Slave
 * Modules, stopping the checking just when an active alarm is found.
 */
class ModuleAlarmChannel : public MCMVirtualDIChannel
{
public:
    /**
     * Custom constructor
     *
     * \param ID Identification of this channel
     * \param observedSeverity Severity observed by this channel. MODULE_BOARD_ERROR_ALARM_* bit field.
     */
    ModuleAlarmChannel(IChannel::ChannelIDStr ID, lu_uint8_t observedSeverity);

    /* === Inherited from MCMChannel === */
    IOM_ERROR configure(ModuleManager& moduleMgr);
    IOM_ERROR configure(lu_uint32_t samplingRate_ms);
    virtual void update(TimeManager::TimeStr &timestamp, const lu_bool_t forced);

private:
    ModuleManager* moduleManager;
    lu_uint32_t samplingRate_ms;    //Time to check module alarms
    lu_uint8_t observedSeverity;
};

#endif /* MODULEALARMCHANNEL_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
