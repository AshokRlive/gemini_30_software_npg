/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: MCMMonitorDDIChannel.cpp 13 Jan 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/MCMIOModule/src/MCMMonitorDDIChannel.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       This is an implementation of an MCM Double Digital Input Channel that
 *       depends on MCM Monitor communication to get its value and status.
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 13 Jan 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13 Jan 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MCMMonitorDDIChannel.h"
#include "LockingMutex.h"
#include "timeOperations.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

MCMMonitorDDIChannel::MCMMonitorDDIChannel(IChannel::ChannelIDStr ID,
                                        lu_uint8_t monitorID_MSb,
                                        lu_uint8_t monitorID_LSb,
                                        lu_uint32_t debounce_Time_ms,
                                        MonitorProtocolManager& monProtocol
                                        ) :
                                            MCMChannel(ID.type, ID.id),
                                            monitorProtocol(monProtocol),
                                            timeManager(TimeManager::getInstance()),
                                            debounceTime_ms(debounce_Time_ms),
                                            debouncing(LU_FALSE),
                                            currentValue(0),
                                            accessLock(LU_FALSE)    //not shared
{
    monitorID[0] = monitorID_MSb;
    monitorID[1] = monitorID_LSb;

    for (lu_uint32_t i = 0; i < 2; ++i)
    {
        monitorObserver[i] = new DDIMonitorObserver(MON_CHANNEL_TYPE_DI, monitorID[i], *this);
        monitorProtocol.attach(monitorObserver[i]);

        /* Request read value for first time */
        monitorProtocol.send_req_di_msg(monitorID[i]);
    }
}

MCMMonitorDDIChannel::~MCMMonitorDDIChannel()
{
    monitorProtocol.detach(monitorObserver[0]);
    monitorProtocol.detach(monitorObserver[1]);
    stopUpdate();
}


void MCMMonitorDDIChannel::update(TimeManager::TimeStr &timeNow, const lu_bool_t forced)
{
    LU_UNUSED(forced);
    /* NOTE: Monitor channels are always updated, even if they are not configured
     * for that in channelDigitalConf.enable (XML Configuration)
     */

    /* Using tickevent for both inputs debounce */
    MasterLockingMutex lMutex(accessLock);
    if(debouncing == LU_TRUE)
    {
        if( timespec_elapsed_ms(&(channelRecord[DDISTATUS_FUTURE].timeStamp.relatime),
                                &(timeNow.relatime)) > debounceTime_ms )
        {
            //debounce finished: update values
            debouncing = LU_FALSE;

            IChannel::FlagsStr newFlags;
            newFlags = channelRecord[DDISTATUS_FUTURE].status[0].flags &
                        channelRecord[DDISTATUS_FUTURE].status[1].flags;

            lu_uint8_t newValue = 0;
            newValue = ((channelRecord[DDISTATUS_FUTURE].status[0].value & 0x01) << 1) + //MSb
                       (channelRecord[DDISTATUS_FUTURE].status[1].value & 0x01);  //LSb

            if( (flags != newFlags) || (currentValue != newValue) )
            {
                /* Change detected on settled new value: store and notify */
                if( (flags.online == LU_FALSE) && (newFlags.online == LU_TRUE) )
                {
                    /* Back to online: force update of all DI channels by requesting it from Monitor */
                    for (lu_uint32_t i = 0; i < 2; ++i)
                    {
                        monitorProtocol.send_req_di_msg(monitorID[i]);
                    }
                }
                flags = newFlags;
                currentValue = newValue;
                timeManager.getTime(timeStamp);
                channelRecord[DDISTATUS_CURRENT] = channelRecord[DDISTATUS_FUTURE];
                updateObservers();  //Notify
            }
        }
    }
}


IOM_ERROR MCMMonitorDDIChannel::read(IChannel::ValueStr& valuePtr)
{
    if(valuePtr.dataPtr == NULL)
    {
        return IOM_ERROR_PARAM;
    }

    /* Use the last updated value */
    LockingMutex lMutex(mutex);
    *(valuePtr.dataPtr) = currentValue;
    valuePtr.dataPtr->setTime(timeStamp);
    valuePtr.flags = flags;

    return IOM_ERROR_NONE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

void MCMMonitorDDIChannel::update(const lu_uint8_t monChannel, const lu_bool_t value, const lu_uint8_t flagStatus)
{
    //NOTE: This is a point update event, usually from an incoming message from MCM Monitor

    lu_int32_t index = getMonitorChannel(monChannel);

    if(index == -1)
        return;

    //Prevent updating
    MasterLockingMutex lMutex(accessLock, LU_TRUE);

    IChannel::FlagsStr newFlags = channelRecord[DDISTATUS_FUTURE].status[index].flags;
    newFlags.online = !(flagStatus & XMSG_VALUE_FLAG_OFF_LINE);
    newFlags.alarm = (flagStatus & XMSG_VALUE_FLAG_ALARM);

    if( (channelRecord[DDISTATUS_FUTURE].status[index].value != value) ||
        (channelRecord[DDISTATUS_FUTURE].status[index].flags != newFlags)
        )
    {
        //value or status change: store and start debouncing
        debouncing = LU_TRUE;
        if(channelRecord[DDISTATUS_FUTURE].status[index].flags.online != newFlags.online)
        {
            update(monChannel, newFlags.online);
        }
        channelRecord[DDISTATUS_FUTURE].status[index].value = value;
        channelRecord[DDISTATUS_FUTURE].status[index].flags = newFlags;
        channelRecord[DDISTATUS_FUTURE].status[index].flags.restart = LU_FALSE;
        timeManager.getTime(channelRecord[DDISTATUS_FUTURE].timeStamp);
    }
}

void MCMMonitorDDIChannel::update(const lu_uint8_t monChannel, const lu_bool_t online)
{
    lu_int32_t index = getMonitorChannel(monChannel);

    if(index < 0)
        return;

    MasterLockingMutex lMutex(accessLock, LU_TRUE);

    /* FIXME: pueyos_a - online changes must be debounced as well! */
    //Online status changes are immediate, not debounced
    if(channelRecord[DDISTATUS_FUTURE].status[index].flags.online != online)
    {
        channelRecord[DDISTATUS_FUTURE].status[index].flags.online = online;
        if( (channelRecord[DDISTATUS_FUTURE].status[0].flags.online == LU_TRUE) &&
            (channelRecord[DDISTATUS_FUTURE].status[1].flags.online == LU_TRUE)
            )
        {
            //changed to online
            flags.online = LU_TRUE;
        }
        else
        {
            //changed to offline
            flags.online = LU_FALSE;
        }
        updateObservers();  //Notify online change
    }
}


lu_int32_t MCMMonitorDDIChannel::getMonitorChannel(const lu_uint16_t monitorChannelID)
{
    for (lu_uint32_t i = 0; i < 2; ++i)
    {
        if(monitorID[i] == monitorChannelID)
            return i;
    }
    return -1;
}

/*
 *********************** End of file ******************************************
 */
