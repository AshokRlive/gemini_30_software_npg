/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: SensorFailAlarm.h 20 Jan 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/RTU/MCM/MCMApp/src/MCMIOModule/include/SensorFailAlarm.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       This is a definition of an MCM Virtual Digital Input Channel that
 *       calculates its value & flags from the online status of several other
 *       MCM's internal channels.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 20 Jan 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   20 Jan 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef SENSORFAILALARM_H__INCLUDED
#define SENSORFAILALARM_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <vector>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "IChannel.h"
#include "IIOModule.h"
#include "Logger.h"

//forward declaration
class OnlineChannelObserver;

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Alarm in charge to monitor a set of Channels
 *
 * This class provides a Module Alarm that indicates when any sensor (from a
 * list of channels) has failed.
 */
class SensorFailAlarm
{
public:
    /*!
     * \brief Custom constructor
     */
    SensorFailAlarm(ModuleAlarm::AlarmIDStr alarmRef, IIOModule& mcmModule);
    virtual ~SensorFailAlarm();

    /**
     * \brief Force update of the alarm
     *
     * This function could be called periodically and/or after an action like
     * alarm acknowledge. It does not re-check the channels.
     *
     * \return None
     */
    virtual void update();

protected:
    /**
     * \brief Update the channel status from one of the observers
     *
     * \param statusFail Set to LU_TRUE when the channel has failed
     * \param observerID ID of the observer reporting the change
     *
     * \return None
     */
    virtual void update(lu_bool_t statusFail, lu_uint32_t observerID);

protected:
    friend class OnlineChannelObserver;

private:
    template<typename ArrayType>
    void registerObservers(const CHANNEL_TYPE channelType, ArrayType channelArray[], const lu_uint32_t size);

private:
    IIOModule& moduleMCM;
    ModuleAlarm::AlarmIDStr alarmID; //Reference of alarm to be issued
    lu_bool_t currentValue;     //Last reported alarm value
    lu_bool_t isCurrentValid;   //Alarm value reported (LU_FALSE when not yet reported)
    lu_bool_t isInit;           //states if the main references are initialised

    /* FIXME: pueyos_a - FLAGS convert to simple mutex? */
    MasterMutex accessLock;     //Prevent concurrent access to common members of the same channel

    std::vector<OnlineChannelObserver*> channelList;  //List of channel observers
    std::vector<lu_bool_t> channelStateList;    //State of each channel observer

};


class OnlineChannelObserver : public ChannelObserver
{
public:
    OnlineChannelObserver(  lu_uint32_t observerID,
                            IChannel& channelRef,
                            SensorFailAlarm& observer) :
                                log(Logger::getLogger(SUBSYSTEM_ID_MCMCHANNEL)),
                                id(observerID),
                                channel(channelRef),
                                parent(observer)
    {};
    virtual ~OnlineChannelObserver() {};

    /**
     * \brief Gets the ID of the observed channel
     *
     * \return ID of the observed channel.
     */
    virtual IChannel::ChannelIDStr getChannelID()
    {
        return channel.getID();
    };

    //inherited from ChannelObserver
    virtual void update(IChannel *subject);

private:
    Logger& log;
    lu_uint32_t id;
    IChannel& channel;
    SensorFailAlarm& parent;
};


#endif /* SENSORFAILALARM_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
