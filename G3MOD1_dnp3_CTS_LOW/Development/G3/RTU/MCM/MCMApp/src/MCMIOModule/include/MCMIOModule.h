/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MCM IO Module interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   05/08/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_DDA5A6A2_D7E4_475d_9B6F_477BCD6989C4__INCLUDED_)
#define EA_DDA5A6A2_D7E4_475d_9B6F_477BCD6989C4__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <vector>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "Table.h"
#include "Logger.h"
#include "ModuleProtocolEnum.h"
#include "IIOModule.h"
#include "MCMChannel.h"
#include "ModuleProtocol.h"
#include "ConfigurationManager.h"
#include "TimeManager.h"
#include "MonitorProtocolManager.h"
#include "ModuleProtocol/ModuleProtocolEnumMCMInternal.h"
#include "MCMAlarmChannelObserver.h"
#include "SensorFailAlarm.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class MCMIOModule : public IIOModule
{
public:
    struct StatsCANStr : public CanStatsStr
    {
        lu_uint16_t validCANError : 1;
        lu_uint16_t validCANBusError : 1;
        lu_uint16_t validCANArbitrationError : 1;
        lu_uint16_t validCANDataOverrun : 1;
        lu_uint16_t validCANBusOff : 1;
        lu_uint16_t validCANErrorPassive : 1;
        lu_uint16_t validCANTxCount : 1;
        lu_uint16_t validCANRxCount : 1;
        lu_uint16_t validCANRxLost : 1;
        lu_uint16_t unused : 7;
    public:
        StatsCANStr() :
                         validCANError(0),
                         validCANBusError(0),
                         validCANArbitrationError(0),
                         validCANDataOverrun(0),
                         validCANBusOff(0),
                         validCANErrorPassive(0),
                         validCANTxCount(0),
                         validCANRxCount(0),
                         validCANRxLost(0),
                         unused(0)
        {};
        StatsCANStr& operator=(const CanStatsStr& canStats);
        void validateAll();
        std::string toString();
    private:
        template<typename T> static void putValidValue( std::stringstream &ss,
                                                        const char *name,
                                                        const lu_uint16_t valid,
                                                        T value
                                                        );
    };

public:
    /**
     * Custom constructor
     *
     * Initialises the MCM module
     *
     * \param moduleID MCM Module ID
     * \param cfgMgr Reference to the Configuration Manager
     * \param memManager Reference to the Memory Manager
     * \param monProtocol Reference to the Monitor Protocol Manager
     */
    MCMIOModule(MODULE_ID moduleID,
                ConfigurationManager &cfgMgr,
                IMemoryManager& memManager,
                MonitorProtocolManager& monitorProtocol
                );
    virtual ~MCMIOModule();

    virtual void setActive(lu_bool_t active) {this->status.active = active; };

    /**
     * \brief Return a channel reference
     *
     * \param type Channel type
     * \param number Channel number
     *
     * \return Channel pointer. NULL if channel is not present
     */
    virtual IChannel* getChannel(CHANNEL_TYPE type, lu_uint32_t number);

    IChannel& getChannelHMIPowerEnable();

    IChannel& getChannelHMIPowerGood();

    IChannel& getChannelHMIDectect();

    IChannel& getChannelDoorSwitch();

    /**
     * \brief Ping a module
     *
     * If the received payload is different from the sent payload an error is
     * reported
     *
     * \param payloadSize Payload size
     * \param timeout Timeout in ms (maximum timeout 1000 ms)
     *
     * \return Error code
     */
    virtual IOM_ERROR ping(lu_uint32_t timeout, lu_uint32_t payloadSize = 8);
    /**
     * \brief Get module time
     *
     * \param time Pointer where the time is saved
     * \param timeout Timeout in ms (maximum timeout 1000 ms)
     *
     * \return Error code
     */
    virtual IOM_ERROR getTime(TimeManager::TimeStr& time, lu_uint32_t timeout);

    /* === Inherited from IIOModule === */
    virtual IOM_ERROR sendMessage(ModuleMessage* messagePtr,lu_uint32_t retries= 0);
    virtual IOM_ERROR sendMessage(MODULE_MSG_TYPE messageType, MODULE_MSG_ID messageID, PayloadRAW* messagePtr,lu_uint32_t retries = 0);
    virtual IOM_ERROR sendMessage(sendReplyStr* messagePtr,lu_uint32_t retries= 0);
    virtual IOM_ERROR decodeMessage(ModuleMessage* message);
    virtual IOM_ERROR tickEvent(lu_uint32_t dTime);
    virtual void disableTimeout(lu_bool_t disable);
    virtual IOM_ERROR getInfo(IOModuleInfoStr& version);
    virtual IOM_ERROR getDetails(IOModuleDetailStr& moduleInfo);
    virtual void getSystemAPI(ModuleVersionStr& sysAPI);
    virtual IOM_ERROR readRAWChannel(IChannel::ChannelIDStr id, IChannel::ValueStr& data);
    virtual ModuleUID getModuleUID() {return mInfo.moduleUID;}
    virtual lu_bool_t getStatus(MSTATUS status);
    virtual IOM_ERROR clearAlarm(const ModuleAlarm::AlarmIDStr id)
    {
        ModuleAlarmExtension::clearAlarm(id);
        ModuleAlarmExtension::packAlarms();
        return IOM_ERROR_NONE;
    };

    /**
     * \brief Return module CAN statistics
     *
     * \param stats Where the module's all CAN stats are saved
     *
     * \return Error code
     */
    virtual IOM_ERROR getCANStats(std::vector<StatsCANStr>& stats);

    /**
     * \brief Stop all of this module's channels from updating
     *
     * \param none
     *
     * \return none
     */
    virtual void stop();

private:
    IOM_ERROR askMonAIChannel(MCM_CH_INTERNAL_AINPUT channelID,
                              TimeManager::TimeStr timestamp
                              );
    void askCANxStats(std::vector<lu_uint32_t>& channelIDList,
                      TimeManager::TimeStr timestamp
                      );
    IOM_ERROR getCANxStats(std::vector<lu_uint32_t>& channelIDList,
                            TimeManager::TimeStr timestamp,
                            StatsCANStr& canStats
                            );

private:
    /* Sampling rate for Module Alarms, in milliseconds */
    static const lu_uint32_t ALARMSAMPLINGRATE_MS = 1000;

private:
    /**
     * Structure for observed channels that can trigger Module Alarms
     */
    struct AlarmChannelStr
    {
        /* Alarm data of the alarm to be reported */
        SYS_ALARM_SUBSYSTEM subSystem;  //SYS_ALARM_SUBSYSTEM_*
        lu_uint32_t         alarmCode;  //Subsystem specific alarm codes e.g. SYSALC_SYSTEM_*
        SYS_ALARM_SEVERITY  severity;   //Severity of the alarm
        /* Channel data for the channel to be observed */
        MCMChannel*         channelPtr; //Related channel
        MCMAlarmChannelObserver::CHANNELALARMTYPE channelAlarmType; //Type of channel-related alarm
    };

private:
    Logger& log;
    MODULE_ID moduleID;

    /*Public channels */
    MCMChannel* DIChannel[MCM_CH_DINPUT_LAST ];
    //MCMChannel* DOChannel[MCM_CH_DOUTPUT_LAST];
    MCMChannel* AIChannel[MCM_CH_AINPUT_LAST];
    /* Internal channels */
    MCMChannel* IntDIChannel[MCM_CH_INTERNAL_DINPUT_LAST];
    MCMChannel* IntDOChannel[MCM_CH_INTERNAL_DOUTPUT_LAST];
    MCMChannel* IntDDIChannel[MCM_CH_INTERNAL_DDINPUT_LAST];
    MCMChannel* IntDDOChannel[MCM_CH_INTERNAL_DDOUTPUT_LAST];
    MCMChannel* IntAIChannel[MCM_CH_INTERNAL_AINPUT_LAST];

    std::vector< Table<MCMChannel*> > channel;  //array of all MCM channels
    static const lu_uint32_t sizeChannels = 8;  //Fixed size of the 1st dimension of the array (channel types)

    /* Module Alarm handling */
    TimeManager::TimeStr alarmSampling;
    MCMAlarmObserverListPtr alarmMonitored;  //List of monitored MCM channels for alarms

    SensorFailAlarm* sensorAlarm;

    IOModuleStatusStr status;
    ModuleInfoDef   mInfo;
};


#endif // !defined(EA_DDA5A6A2_D7E4_475d_9B6F_477BCD6989C4__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
