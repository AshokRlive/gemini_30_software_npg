/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: MCMDIStartChannel.h 25 Nov 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/src/MCMIOModule/include/channel/MCMDIStartChannel.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       This MCM Virtual Digital Input Channel is used to indicate that the
 *       initialisation procedure has finished.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 25 Nov 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25 Nov 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MCMDISTARTCHANNEL_H__INCLUDED
#define MCMDISTARTCHANNEL_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "MCMVirtualDIChannel.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief This channel provides a "RTU Start" / "End-of-initialisation" indication.
 *
 * Note that once it is set to 1, it automatically goes back to 0.
 */
class MCMDIStartChannel : public MCMVirtualDIChannel
{
public:
    MCMDIStartChannel(IChannel::ChannelIDStr ID);
    virtual ~MCMDIStartChannel();

    /* == Overrides MCMVirtualDIChannel == */
    virtual IOM_ERROR write(IChannel::ValueStr& valuePtr);

protected:
    /* === Inherited from MCMChannel === */
    virtual void update(TimeManager::TimeStr &timestamp, const lu_bool_t forced);

private:
    lu_bool_t lastValue;            //Last value set
    TimeManager::TimeStr startTime; //Time stamp of the time when value was set
};


#endif /* MCMDISTARTCHANNEL_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
