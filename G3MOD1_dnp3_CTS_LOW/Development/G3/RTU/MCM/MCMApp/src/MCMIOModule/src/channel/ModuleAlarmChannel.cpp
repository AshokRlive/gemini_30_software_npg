/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: ModuleAlarmChannel.cpp 16 Jan 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/MCMIOModule/src/ModuleAlarmChannel.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       This is an implementation of an MCM Virtual Digital Input Channel that
 *       calculates its value & flags from Module Alarms.
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 16 Jan 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   16 Jan 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "ModuleAlarmChannel.h"
#include "ModuleProtocolEnumMCMInternal.h"
#include "IIOModule.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
ModuleAlarmChannel::ModuleAlarmChannel(IChannel::ChannelIDStr ID, lu_uint8_t severity) :
                                            MCMVirtualDIChannel(ID),
                                            moduleManager(NULL),
                                            samplingRate_ms(10000),
                                            observedSeverity(severity)
{
    //Virtual channels are always online by default
    flags.online = LU_TRUE;
    timeManager.getTime(timeStamp);
}


IOM_ERROR ModuleAlarmChannel::configure(ModuleManager& moduleMgr)
{
    moduleManager = &moduleMgr;
    return IOM_ERROR_NONE;
}

IOM_ERROR ModuleAlarmChannel::configure(lu_uint32_t samplingRatems)
{
    if(samplingRatems < 100)
    {
        return IOM_ERROR_PARAM;
    }
    samplingRate_ms = samplingRatems;
    return IOM_ERROR_NONE;
}


void ModuleAlarmChannel::update(TimeManager::TimeStr &newTimestamp, const lu_bool_t forced)
{
    if(moduleManager == NULL)
    {
        return; //Not configured
    }
    if( (timeStamp.elapsed_ms(newTimestamp) < samplingRate_ms) && (forced == LU_FALSE) )
    {
        return; //Nothing to update yet
    }
    timeStamp = newTimestamp;
    IIOModule* module;
    std::vector<ModuleAlarm> alarmList;
    bool found = false;         //An alarm of this severity has been found
    IODataInt8 data;
    IChannel::ValueStr value(data);
    value.flags.restart = LU_FALSE;
    value.flags.online = LU_TRUE;
    data.setTime(newTimestamp);

    /* Review alarm status from modules */
    lu_uint8_t severity;
    module = moduleManager->getMCM();  //First the MCM
    severity = module->alarmStatus();
    if( (severity & observedSeverity) != 0 )
    {
        found = true;   //active alarm found
    }

    //When no alarm of any of these severities was found in the MCM: check other modules
    for (lu_uint32_t moduleType = 0; (!found) && (moduleType < MODULE_LAST); ++moduleType)
    {
        if(moduleType != MODULE_MCM)
        {
            for (lu_uint32_t moduleID = 0; (!found) && (moduleID < MODULE_ID_LAST); ++moduleID)
            {
                module = moduleManager->getModule((MODULE)moduleType, (MODULE_ID)moduleID);
                if(module != NULL)
                {
                    severity = module->alarmStatus();
                    if( (severity & observedSeverity) != 0 )
                    {
                        /* There is an alarm already active, no need to keep searching */
                        found = true;
                        break;
                    }
                }
            }
        }
    }
    //Get result, applying inversion if needed
    data = (channelConf.extEquipInvert == LU_TRUE)? ((found)? 0 : 1) :   //inverted
                                                    ((found)? 1 : 0);    //normal
    write(value);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
