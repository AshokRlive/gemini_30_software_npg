/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: MCMMonitorDDIChannel.h 13 Jan 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/RTU/MCM/MCMApp/src/MCMIOModule/include/MCMMonitorDDIChannel.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       This is a definition of an MCM Double Digital Input Channel that depends
 *       on MCM Monitor communication to get its value and status.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 13 Jan 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13 Jan 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MCMMONITORDDICHANNEL_H__INCLUDED
#define MCMMONITORDDICHANNEL_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "MCMChannel.h"
#include "MonitorProtocolManager.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

//Forward declarations
class DDIMonitorObserver;

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Double Digital Input Channel from MCM Monitor communication
 *
 * This class provides a MCM Channel that has a value originated from 2 of the
 * channels of the the MCM Monitor application.
 */
class MCMMonitorDDIChannel : public MCMChannel
{
public:
    /*!
     * \brief Custom constructor
     *
     * \param ID Identification number of this Double Digital Input Channel.
     * \param monitorID_MSb Monitor channel ID for the most significant bit value.
     * \param monitorID_LSb Monitor channel ID for the least significant bit value.
     * \param debounceTime_ms Time to consider a change settled.
     * \param monProtocol Monitor Protocol Manager object reference.
     */
    MCMMonitorDDIChannel(IChannel::ChannelIDStr ID,
                        lu_uint8_t monitorID_MSb,
                        lu_uint8_t monitorID_LSb,
                        lu_uint32_t debounceTime_ms,
                        MonitorProtocolManager& monProtocol
                        );
    virtual ~MCMMonitorDDIChannel();

    /* === Inherited from MCMChannel === */
    virtual IOM_ERROR read(IChannel::ValueStr& valuePtr);
    virtual void update(TimeManager::TimeStr &timestamp, const lu_bool_t forced);

protected:
    friend class DDIMonitorObserver;

private:
    struct ChannelStatusStr
    {
    public:
        lu_uint8_t value;
        IChannel::FlagsStr flags;
    public:
        ChannelStatusStr() : value(0)
        {}
    };

    struct ChannelRecordStr
    {
        ChannelStatusStr status[2];     //Monitor channels 1 & 2
        TimeManager::TimeStr timeStamp; //Last valid change time stamp
    };

    enum
    {
        DDISTATUS_CURRENT,
        DDISTATUS_FUTURE,
        DDISTATUS_LAST
    };
    MonitorProtocolManager& monitorProtocol; //reference of the Monitor Protocol Handler
    TimeManager& timeManager;

    lu_uint32_t debounceTime_ms;            //Debounce time (in ms)
    lu_uint16_t monitorID[2];               //ID of the channels in the MCM Monitor side
    DDIMonitorObserver* monitorObserver[2];  //Registered observer on the MCM Monitor channels
    ChannelRecordStr channelRecord[DDISTATUS_LAST]; //current/future status of the channel
    lu_bool_t debouncing;           //Currently debouncing values
    lu_uint8_t currentValue;        //last value read on both channels

    /* FIXME: pueyos_a - FLAGS convert to simple mutex for MonitorDI Channel */
    MasterMutex accessLock;     //Prevent concurrent access to common members of the same channel

private:
    virtual void update(const lu_uint8_t monChannel, const lu_bool_t value, const lu_uint8_t flags);
    virtual void update(const lu_uint8_t monChannel, const lu_bool_t online);
    lu_int32_t getMonitorChannel(const lu_uint16_t monitorChannelID);
};


/**
 * \brief Class in charge of observing a component channel, and report any change.
 */
class DDIMonitorObserver : public MonitorObserver
{
public:
    DDIMonitorObserver( const MON_CHANNEL_TYPE pointType,
                        const lu_uint8_t monChannel,
                        MCMMonitorDDIChannel& monitorChannelDDI
                        ) :
                            MonitorObserver(pointType, monChannel),
                            channelDDI(monitorChannelDDI),
                            monitorID(monChannel)
    {};

    /* === Inherited from MonitorObserver === */
    virtual void update(lu_float32_t value, lu_uint8_t flags)
    {
        channelDDI.update(monitorID, value, flags);
    }

    /* === Inherited from MonitorObserver === */
    virtual void updateOnline(lu_bool_t online)
    {
        channelDDI.update(monitorID, online);
    }


private:
    MCMMonitorDDIChannel& channelDDI;
    const lu_uint16_t monitorID;
};

#endif /* MCMMONITORDDICHANNEL_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
