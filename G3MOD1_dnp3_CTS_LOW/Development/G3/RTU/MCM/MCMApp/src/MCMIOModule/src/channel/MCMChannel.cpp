/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdio.h>
#include <errno.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MCMChannel.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static TimeManager& timeManager = TimeManager::getInstance();

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */


MCMChannel::MCMChannel(CHANNEL_TYPE type, lu_uint16_t id) :
                                IChannel(type,id),
                                log(Logger::getLogger(SUBSYSTEM_ID_MCMCHANNEL)),
                                timeStamp(TimeManager::getInstance().getInitialTime())
{
     timeManager.getTime(timeStamp); //Use creation time for 1st channel timestamp
}


IOM_ERROR MCMChannel::configure(InputDigitalCANChannelConf &conf)
{
    channelConf = conf;
    return IOM_ERROR_NONE;
}

IOM_ERROR MCMChannel::configure(InputAnalogueCANChannelConf &conf)
{
    channelAnalogConf = conf;
    return IOM_ERROR_NONE;
}

IOM_ERROR MCMChannel::read(IChannel::ValueStr& valuePtr)
{
    LU_UNUSED(valuePtr);

    return IOM_ERROR_NOT_SUPPORTED;
}

IOM_ERROR MCMChannel::write(IChannel::ValueStr& valuePtr)
{
    LU_UNUSED(valuePtr);

    return IOM_ERROR_NOT_SUPPORTED;
}

void MCMChannel::update(TimeManager::TimeStr &timestamp, const lu_bool_t forced)
{
    LU_UNUSED(timestamp);
    LU_UNUSED(forced);
    return;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
