/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: MCMMonitorDIChannel.h 09 Jan 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/RTU/MCM/MCMApp/src/MCMIOModule/include/MCMMonitorDIChannel.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       This is a definition of an MCM Digital Input Channel that depends
 *       on MCM Monitor communication to get its value and status.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 09 Jan 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   09 Jan 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MCMMONITORDICHANNEL_H__INCLUDED
#define MCMMONITORDICHANNEL_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "MCMChannel.h"
#include "MonitorProtocolManager.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Digital Input Channel from MCM Monitor communication
 *
 * This class provides an MCM Channel that has a value originated from the MCM
 * Monitor application.
 */
class MCMMonitorDIChannel : public MCMChannel, private MonitorObserver
{
public:
    /*!
     * \brief Custom constructor
     */
    MCMMonitorDIChannel(IChannel::ChannelIDStr ID,
                        lu_uint8_t monitorID,
                        MonitorProtocolManager& monProtocol
                        );
    virtual ~MCMMonitorDIChannel();

    /* === Inherited from MCMChannel === */
    virtual IOM_ERROR read(IChannel::ValueStr& valuePtr);

private:
    MonitorProtocolManager& monitorProtocol; //reference of the Monitor Protocol Handler
    TimeManager& timeManager;
    lu_uint16_t monitorID;      //ID of the channel in the MCM Monitor side
    lu_bool_t currentValue;     //last value read

    /* FIXME: pueyos_a - FLAGS convert to simple mutex for MonitorDI Channel */
    MasterMutex accessLock;     //Prevent concurrent access to common members of the same channel

private:
    /* === Inherited from MonitorObserver === */
    virtual void update(lu_float32_t value, lu_uint8_t flags);
    virtual void updateOnline(lu_bool_t online);

};

#endif /* MCMMONITORDICHANNEL_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
