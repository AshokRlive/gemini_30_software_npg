/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MCM module manager implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   08/08/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MCMIOModuleManager.h"
#include "timeOperations.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
const struct timeval MCMIOModuleManager::backgroundTime = {0, 50000}; //Time for tickevent, in (sec,usec)

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */


MCMIOModuleManager::MCMIOModuleManager( SCHED_TYPE schedType,
                                        lu_uint32_t priority,
                                        ConfigurationManager &cfgMgr,
                                        IMemoryManager& memManager,
                                        MonitorProtocolManager& monProtocol
                                      ) :  Thread(schedType, priority, LU_TRUE, "MCMIOModuleManager"),
                                           mcm(MODULE_ID_0, cfgMgr, memManager, monProtocol),
                                           log(Logger::getLogger(SUBSYSTEM_ID_MCMMANAGER)),
                                           moduleStatus(NULL),
                                           backGroundTimer( backgroundTime.tv_sec ,
                                                            backgroundTime.tv_usec,
                                                            Timer::TIMER_TYPE_PERIODIC
                                                          )
{
    /* Start thread */
    Thread::start();
}


MCMIOModuleManager::~MCMIOModuleManager()
{
    this->stop();    /* Stop thread */
}


void MCMIOModuleManager::setPowerSaveMode(lu_bool_t mode)
{
    //MCM module does not need to do anything with this command
    LU_UNUSED(mode);
}


void MCMIOModuleManager::setModuleStatusMonitor(IModuleStatusMonitor* moduleStatusMonitor)
{
    moduleStatus = moduleStatusMonitor;
}


IIOModule* MCMIOModuleManager::createModule(MODULE module, MODULE_ID moduleID, COMM_INTERFACE iFace)
{
    LU_UNUSED(iFace);

    if( (module == MODULE_MCM) && (moduleID == MODULE_ID_0) )
    {
        return &mcm;
    }
    else
    {
        log.error("MCMIOModuleManager::createModule: invalid board: %i:%i",
                    module, moduleID + 1
                    );
        return NULL;
    }
}


IIOModule* MCMIOModuleManager::getModule(MODULE module, MODULE_ID moduleID)
{
    if( (module == MODULE_MCM) && (moduleID == MODULE_ID_0) )
    {
        return &mcm;
    }
    else
    {
        log.error("MCMIOModuleManager::getModule: invalid board: %i:%i",
                        module, moduleID + 1
                      );
        return NULL;
    }
}


std::vector<IIOModule*> MCMIOModuleManager::getAllModules()
{
    std::vector<IIOModule*> ret;
    ret.push_back(&mcm);
    return ret;
}


IChannel* MCMIOModuleManager::getChannel( MODULE module,
                                          MODULE_ID moduleID,
                                          CHANNEL_TYPE chType,
                                          lu_uint32_t chNumber
                                        )
{
    /* Get module */
    if( (module == MODULE_MCM) && (moduleID == MODULE_ID_0) )
    {
        return mcm.getChannel(chType, chNumber);
    }
    else
    {
        /* Invalid module */
        return NULL;
    }
}


IOM_ERROR MCMIOModuleManager::stopAllModules()
{
    /* Loop over the modules, stopping them */
    std::vector<IIOModule*> moduleList;
    moduleList = getAllModules();
    for(std::vector<IIOModule*>::iterator it=moduleList.begin(); it!=moduleList.end(); ++it)
    {
        (*it)->stop();
    }
    return IOM_ERROR_NONE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
void MCMIOModuleManager::threadBody()
{
    /* start timer */
    lu_int32_t res;
    res = backGroundTimer.start();
    if(res < 0)
    {
        log.fatal("MCM Module BackGround Timer unavailable");
        return;
    }

    while(isRunning() == LU_TRUE)
    {
        /* Send periodic tick to the module */
        mcm.tickEvent(timeval_to_ms(&backgroundTime));

        /* Update modules monitor */
        if(moduleStatus != NULL)
        {
            moduleStatus->update();
        }

        /* Wait timer expiration */
        backGroundTimer.wait();
    }
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

THREAD_ERR MCMIOModuleManager::stop()
{
    THREAD_ERR result;
    result = Thread::stop();    //Signal thread to stop
    backGroundTimer.stop();     // stop timer
    return result;
}


/*
 *********************** End of file ******************************************
 */
