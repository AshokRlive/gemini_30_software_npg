/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MCM Binary output channel implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   05/08/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MCMBOChannel.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

MCMBOChannel::MCMBOChannel( IChannel::ChannelIDStr ID,
                            lu_uint8_t  gpio,
                            lu_uint8_t  initialValue
                          ): MCMChannel(ID.type, ID.id),
                             gpio(gpio),
                             fdValue(NULL),
                             value(initialValue)
{
    lu_int32_t ret;

    /* Initialise GPIO as output */
    ret = initGPIO(gpio, GPIOLIB_DIRECTION_OUTPUT, &fdValue);
    if(ret != 0)
    {
        log.error("MCMBOChannel init error %i", ret);
        //When failed to init, fdValue is returned as NULL
    }
    writeGPIO(fdValue, value);  /* Set initial value */
}

MCMBOChannel::~MCMBOChannel()
{
    closeGPIO(fdValue);
}

IOM_ERROR MCMBOChannel::read(IChannel::ValueStr& valuePtr)
{
    IOM_ERROR ret;
    TimeManager::TimeStr timestamp;

    if(valuePtr.dataPtr == NULL)
    {
        return IOM_ERROR_PARAM;
    }

    if(fdValue != NULL)
    {
        /* Valid local value. Lock mutex and read the local value */
        {
            LockingMutex lMutex(mutex);
            *(valuePtr.dataPtr) = value;
        }

        /* Set timestamp as current time */
        TimeManager::getInstance().getTime(timestamp);
        valuePtr.dataPtr->setTime(timestamp);

        /* Set flags */
        flags.online = LU_TRUE;
        flags.restart = LU_FALSE;
        ret = IOM_ERROR_NONE;
    }
    else
    {
        flags.online = LU_FALSE;
        ret = IOM_ERROR_READ;
    }
    valuePtr.flags = flags;

    return ret;
}


IOM_ERROR MCMBOChannel::write(IChannel::ValueStr& valuePtr)
{
    IOM_ERROR ret = IOM_ERROR_WRITE;
    lu_int32_t retGPIO;

    if(valuePtr.dataPtr == NULL)
    {
        return IOM_ERROR_PARAM;
    }

    if(fdValue != NULL)
    {
        /* Valid local value. Lock mutex */
        {
            LockingMutex lMutex(mutex);

            /* Update internal value and write to the GPIO */

            value = (lu_uint8_t)(*(valuePtr.dataPtr));
            retGPIO = writeGPIO(fdValue, (value==0)? LU_FALSE : LU_TRUE);
        }

        if(retGPIO < 0)
        {
            flags.online = LU_FALSE;
            log.error("%s - error %i", this->getName(), retGPIO);
        }
        else
        {
            flags.restart = LU_FALSE;
            ret = IOM_ERROR_NONE;
        }
    }
    else
    {
        flags.online = LU_FALSE;
    }
    return ret;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
