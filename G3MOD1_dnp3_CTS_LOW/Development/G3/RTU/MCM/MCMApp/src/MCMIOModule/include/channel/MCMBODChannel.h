/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MCM Binary Output differential channel Interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   21/03/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_20F790F3_834F_49a1_A6B3_7628221A9AB3__INCLUDED_)
#define EA_20F790F3_834F_49a1_A6B3_7628221A9AB3__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "MCMChannel.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class MCMBODChannel : public MCMChannel
{
public:
    MCMBODChannel( IChannel::ChannelIDStr ID,
                   lu_uint8_t gpioH,
                   lu_uint8_t gpioL,
                   lu_uint8_t initialValue
                 );
    virtual ~MCMBODChannel();

    /* === Inherited from MCMChannel === */
    virtual IOM_ERROR read(IChannel::ValueStr& valuePtr);
    virtual IOM_ERROR write(IChannel::ValueStr& valuePtr);

private:
    lu_uint8_t gpioH;
    lu_uint8_t gpioL;
    FILE *fdHValue;     //GPIO file to access channel value
    FILE *fdLValue;     //GPIO file to access channel value
    lu_uint8_t value;

private:
    /*
     * Closes all GPIO Value files.
     */
    void closeAll();

    /**
     * \brief Write a differential GPIO
     *
     * This method simply writes the value of the correspondent bit to the given
     * GPIO Value file, by using a bit mask.
     *
     * Input  High | Output
     *   0     0   |    1
     *   0     1   |    0
     *   1     0   |    0
     *   1     1   |    1
     *
     * \param fd file descriptor of the GPIO to write
     * \param high TRUE if the GPIO is the "high" GPIO
     *
     * \return GPIOWrite error code
     */
    inline lu_int32_t GPIODWrite(FILE *fd, lu_bool_t high);

};
#endif // !defined(EA_20F790F3_834F_49a1_A6B3_7628221A9AB3__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
