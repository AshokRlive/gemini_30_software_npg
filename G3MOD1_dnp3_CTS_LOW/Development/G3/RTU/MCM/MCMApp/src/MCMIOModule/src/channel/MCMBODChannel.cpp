/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MCM Binary Output differential channel Implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   21/03/12      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MCMBODChannel.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

MCMBODChannel::MCMBODChannel( IChannel::ChannelIDStr ID,
                              lu_uint8_t gpioH,
                              lu_uint8_t gpioL,
                              lu_uint8_t initialValue
                            ) : MCMChannel(ID.type, ID.id),
                                gpioH(gpioH),
                                gpioL(gpioL),
                                fdHValue(NULL),
                                fdLValue(NULL),
                                value(initialValue)
{
    lu_int32_t ret;

    /* Initialise GPIOs */
    ret = initGPIO(gpioH, GPIOLIB_DIRECTION_OUTPUT, &fdHValue);
    if(ret == 0)
    {
        ret = initGPIO(gpioL, GPIOLIB_DIRECTION_OUTPUT, &fdLValue);
        if(ret == 0)
        {
            /* Set initial value */
            GPIODWrite(fdHValue, LU_TRUE);
            GPIODWrite(fdLValue, LU_FALSE);
        }
    }
    if(ret != 0)
    {
        log.error("MCMBIChannel init error %i", ret);
        closeAll(); /* Close all file descriptors */
    }
}

MCMBODChannel::~MCMBODChannel()
{
    closeAll();
}


IOM_ERROR MCMBODChannel::read(IChannel::ValueStr& valuePtr)
{
    IOM_ERROR ret;
    TimeManager::TimeStr timestamp;

    if(valuePtr.dataPtr == NULL)
    {
        return IOM_ERROR_PARAM;
    }

    if( (fdHValue != NULL) && ((fdLValue != NULL)) )
    {
        /* Valid local value. Lock mutex and read the local value */
        {
            LockingMutex lMutex(mutex);
            *(valuePtr.dataPtr) = value;
        }

        /* Set timestamp as current time */
        TimeManager::getInstance().getTime(timestamp);
        valuePtr.dataPtr->setTime(timestamp);
        ret = IOM_ERROR_NONE;
    }
    else
    {
        flags.online = LU_FALSE;
        ret = IOM_ERROR_READ;
    }
    valuePtr.flags = flags;
    return ret;
}


IOM_ERROR MCMBODChannel::write(IChannel::ValueStr& valuePtr)
{
    IOM_ERROR ret = IOM_ERROR_WRITE;
    lu_int32_t retGPIO = 0;

    if(valuePtr.dataPtr == NULL)
    {
        return IOM_ERROR_PARAM;
    }

    if( (fdHValue != NULL) && (fdLValue != NULL) )
    {
        /* Valid file descriptors */
        {
            LockingMutex lMutex(mutex);

            /* Update internal value and write to both GPIOs */
            flags.online = LU_FALSE;
            value = (lu_uint8_t)(*(valuePtr.dataPtr));
            retGPIO = GPIODWrite(fdHValue, LU_TRUE);
            if(retGPIO >= 0)
            {
                retGPIO = GPIODWrite(fdLValue, LU_FALSE);
                if(retGPIO >= 0)
                {
                    flags.online = LU_TRUE;
                    flags.restart = LU_FALSE;
                    ret = IOM_ERROR_NONE;
                }
            }
        }
    }
    if(retGPIO < 0)
    {
        log.error("%s - error %i writing.", this->getName(), retGPIO);
    }
    return ret;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

void MCMBODChannel::closeAll()
{
    closeGPIO(fdHValue);
    closeGPIO(fdLValue);
    fdHValue = NULL;
    fdLValue = NULL;
}

inline lu_int32_t MCMBODChannel::GPIODWrite(FILE *fd, lu_bool_t high)
{
    lu_bool_t tmp = ((value & ((high == LU_TRUE)? 0x02 : 0x01)) != 0)? LU_TRUE : LU_FALSE;
    return writeGPIO(fd, tmp);
}


/*
 *********************** End of file ******************************************
 */
