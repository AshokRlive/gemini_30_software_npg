/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MCM Binary output channel interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   05/08/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#if !defined(EA_EE256FF5_ECA9_46bc_9164_DB5150125019__INCLUDED_)
#define EA_EE256FF5_ECA9_46bc_9164_DB5150125019__INCLUDED_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "MCMChannel.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

class MCMBOChannel : public MCMChannel
{

public:
    MCMBOChannel(IChannel::ChannelIDStr ID, lu_uint8_t gpio, lu_uint8_t initialValue);
    virtual ~MCMBOChannel();

    /* === Inherited from MCMChannel === */
    virtual IOM_ERROR read(IChannel::ValueStr& valuePtr);
    virtual IOM_ERROR write(IChannel::ValueStr& valuePtr);

private:
    lu_uint8_t gpio;
    FILE *fdValue;      //GPIO file to access channel value
    lu_uint8_t value;

};
#endif // !defined(EA_EE256FF5_ECA9_46bc_9164_DB5150125019__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
