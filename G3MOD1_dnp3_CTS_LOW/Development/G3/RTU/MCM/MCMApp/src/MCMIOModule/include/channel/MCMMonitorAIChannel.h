/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: MCMMonitorAIChannel.h 23 Dec 2013 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/RTU/MCM/MCMApp/src/MCMIOModule/include/MCMMonitorAIChannel.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       This is the definition of an MCM Analogue Input Channel that depends
 *       on MCM Monitor communication to get its value and status.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 23 Dec 2013 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   23 Dec 2013   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MCMMONITORAICHANNEL_H__INCLUDED
#define MCMMONITORAICHANNEL_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "MCMChannel.h"
#include "MonitorProtocolManager.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Analogue Input Channel from MCM Monitor communication
 *
 * This class provides a MCM Channel that has a value originated from the MCM
 * Monitor application.
 */
class MCMMonitorAIChannel : public MCMChannel, private MonitorObserver
{
public:
    /*!
     * \brief Custom constructor
     *
     * \param ID Analogue channel ID for the MCM Application
     * \param monitorID Analogue channel ID in the MCM Monitor
     * \param monProtocol Reference to the MCM Monitor Protocol Manager
     * \param samplingTime_ms Sampling time for this AI, in milliseconds
     */
    MCMMonitorAIChannel(IChannel::ChannelIDStr ID,
                        lu_uint8_t monitorID,
                        MonitorProtocolManager& monProtocol,
                        lu_uint32_t samplingTime_ms
                        );
    virtual ~MCMMonitorAIChannel();

    /* === Inherited from MCMChannel === */
    virtual IOM_ERROR read(IChannel::ValueStr& valuePtr);

    /* === Overridden from MCMChannel === */
    /**
     * \brief Update the value of the channel
     *
     * \param timestamp Current time
     * \param forced makes a mandatory refresh of the channel, if allowed
     */
    virtual void update(TimeManager::TimeStr &timestamp, const lu_bool_t forced = LU_FALSE);



private:
    MonitorProtocolManager& monitorProtocol; //reference of the Monitor Protocol Handler
    TimeManager& timeManager;
    lu_uint16_t monitorID;      //ID of the channel in the MCM Monitor side
    lu_uint32_t samplingTime;   //Sampling time for this AI, in milliseconds
    lu_float32_t currentValue;  //Last value read
    lu_bool_t waitingReply;     //When a non-sampling channel is waiting for a reply
    TimeManager::TimeStr sampleTime;    //Time stamp of the last AI request

    /* FIXME: pueyos_a - FLAGS convert to mutex for AI */
    MasterMutex accessLock;     //Prevent concurrent access to common members of the same channel

private:
    /* === Inherited from MonitorObserver === */
    virtual void update(lu_float32_t value, lu_uint8_t flags);
    virtual void updateOnline(lu_bool_t online);

};

#endif /* MCMMONITORAICHANNEL_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
