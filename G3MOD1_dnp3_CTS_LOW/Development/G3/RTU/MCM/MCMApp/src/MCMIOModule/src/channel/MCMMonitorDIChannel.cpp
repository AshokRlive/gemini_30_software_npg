/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: MCMMonitorDIChannel.cpp 09 Jan 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/MCMIOModule/src/MCMMonitorDIChannel.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       This is an implementation of an MCM Digital Input Channel that depends
 *       on MCM Monitor communication to get its value and status.
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 09 Jan 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   09 Jan 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MCMMonitorDIChannel.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

MCMMonitorDIChannel::MCMMonitorDIChannel(IChannel::ChannelIDStr ID,
                                        lu_uint8_t monID,
                                        MonitorProtocolManager& monProtocol
                                        ) :
                                            MCMChannel(ID.type, ID.id),
                                            MonitorObserver(MON_CHANNEL_TYPE_DI, monID),
                                            monitorProtocol(monProtocol),
                                            timeManager(TimeManager::getInstance()),
                                            monitorID(monID),
                                            currentValue(0),
                                            accessLock(LU_FALSE)    //not shared
{
    /* Read initial value */
    monitorProtocol.attach(this);
    monitorProtocol.send_req_di_msg(monitorID);
}

MCMMonitorDIChannel::~MCMMonitorDIChannel()
{
    monitorProtocol.detach(this);
    stopUpdate();
}


IOM_ERROR MCMMonitorDIChannel::read(IChannel::ValueStr& valuePtr)
{
    if(valuePtr.dataPtr == NULL)
    {
        return IOM_ERROR_PARAM;
    }

    /* Use the last updated value */
    LockingMutex lMutex(mutex);
    *(valuePtr.dataPtr) = currentValue;
    valuePtr.dataPtr->setTime(timeStamp);
    valuePtr.flags = flags;

    return IOM_ERROR_NONE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

void MCMMonitorDIChannel::update(lu_float32_t value, lu_uint8_t flagStatus)
{
    //NOTE: This is a point update event, usually from an incoming message from MCM Monitor
    IChannel::FlagsStr newFlags = flags;
    lu_bool_t boolValue = (value != 0)? LU_TRUE : LU_FALSE;

    //Prevent updating -- use the same value for all observers
    MasterLockingMutex lMutex(accessLock, LU_TRUE);

    //A message has been received:
    newFlags.online = !(flagStatus & XMSG_VALUE_FLAG_OFF_LINE);
    newFlags.alarm = (flagStatus & XMSG_VALUE_FLAG_ALARM);
    if( (currentValue != boolValue) || (newFlags != flags) )
    {
        //value or status change: store and notify
        timeManager.getTime(timeStamp);
        currentValue = boolValue;
        flags = newFlags;
        flags.restart = LU_FALSE;   //not the initial status anymore

        updateObservers();  //Notify
    }

}

void MCMMonitorDIChannel::updateOnline(lu_bool_t online)
{
    if( (flags.online == LU_FALSE) && (online == LU_TRUE) )
    {
        //Back to online: force update of DI channel by requesting it from Monitor */
        monitorProtocol.send_req_di_msg(monitorID);
    }
    update(currentValue, (online==LU_TRUE) ? 0 : XMSG_VALUE_FLAG_OFF_LINE);
}



/*
 *********************** End of file ******************************************
 */
