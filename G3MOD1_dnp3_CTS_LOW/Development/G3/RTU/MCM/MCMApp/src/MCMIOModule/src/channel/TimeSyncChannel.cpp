/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: TimeSyncChannel.cpp 16 Jan 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/MCMIOModule/src/TimeSyncChannel.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       This is an implementation of an MCM Virtual Digital Input Channel that
 *       calculates its value from the valid synchronisation time, changing when
 *       time synchronisation does change.
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 16 Jan 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   16 Jan 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "TimeSyncChannel.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
TimeSyncChannel::TimeSyncChannel(IChannel::ChannelIDStr ID) :
                                            MCMVirtualDIChannel(ID),
                                            accessLock(LU_FALSE)    //not shared
{}

TimeSyncChannel::~TimeSyncChannel()
{
    /* Remove observers */
    stopUpdate();
    timeManager.detachSynch(this);
}


IOM_ERROR TimeSyncChannel::configure(InputDigitalCANChannelConf &conf)
{
    IOM_ERROR ret = MCMChannel::configure(conf);
    if(conf.enable == LU_TRUE)
    {
        /* Register observer */
        timeManager.attachSynch(this);
    }
    return ret;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
void TimeSyncChannel::updateTime(TimeManager::TimeStr& currentTime)
{
    MasterLockingMutex mMutex(accessLock);
    IODataInt8 data;
    IChannel::ValueStr value(data);

    //Get result, applying inversion if needed (Note that normal is 1 when time is sync)
    data = (channelConf.extEquipInvert == LU_TRUE)? ((currentTime.synchronised)? 0 : 1) :   //inverted
                                                    ((currentTime.synchronised)? 1 : 0);    //normal
    data.setTime(currentTime);
    write(value);   //Notify
}


/*
 *********************** End of file ******************************************
 */
