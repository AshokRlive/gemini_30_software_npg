/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: MCMDIStartChannel.cpp 25 Nov 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/MCMIOModule/src/channel/MCMDIStartChannel.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       This MCM Virtual Digital Input Channel is used to indicate that the
 *       initialisation procedure has finished.
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 25 Nov 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25 Nov 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "MCMDIStartChannel.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static const lu_uint32_t PulseWidth_ms = 1000;  //Width of the point change, in milliseconds

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
MCMDIStartChannel::MCMDIStartChannel(IChannel::ChannelIDStr ID) :
                                                        MCMVirtualDIChannel(ID),
                                                        lastValue(LU_FALSE)
{
    //Virtual channels are always online by default
    flags.online = LU_TRUE;
}

MCMDIStartChannel::~MCMDIStartChannel()
{
    /* Remove observers */
    stopUpdate();
}


IOM_ERROR MCMDIStartChannel::write(IChannel::ValueStr& data)
{
    lastValue = *(static_cast<IODataInt8*>(data.dataPtr));

    IOM_ERROR ret = MCMVirtualDIChannel::write(data);
    if( (ret == IOM_ERROR_NONE) && (lastValue == LU_TRUE) )
    {
        timeManager.getTime(startTime); //Remember starting time stamp
    }
    return ret;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
void MCMDIStartChannel::update(TimeManager::TimeStr& timestamp, const lu_bool_t forced)
{
    LU_UNUSED(forced);

    if(lastValue == LU_TRUE)
    {
        if(startTime.elapsed_ms(timestamp) >= PulseWidth_ms)
        {
            //Was set to 1: go back to 0 automatically
            IODataUint8 newData;
            IChannel::ValueStr valData(newData);
            newData = LU_FALSE;
            MCMVirtualDIChannel::write(valData);
            lastValue = LU_FALSE;
        }
    }
}

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
