/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <cstring>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MCMBIChannel.h"
#include "timeOperations.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

MCMBIChannel::MCMBIChannel(IChannel::ChannelIDStr ID, lu_uint8_t gpio) :
                            MCMChannel(ID.type, ID.id),
                            gpio(gpio),
                            fdValue(NULL),
                            currentValue(0),
                            previousValue(0),
                            /* default debounce parameters */
                            timePrev(TIMESPEC_ZERO),
                            countHigh2Low(0),
                            countLow2High(0)
{
    lu_int32_t ret;

    /* Initialise GPIO as input */
    ret = initGPIO(gpio, GPIOLIB_DIRECTION_INPUT, &fdValue);
    if(ret != 0)
    {
        log.error("MCMBIChannel: board/channel %s/DI:%d init error %i",
                    MODULE_ToSTRING(MODULE_MCM), ID, ret
                    );
        //When failed to init, fdValue is returned as NULL
    }
    else
    {
        /* First read of current input channel value/state */
        TimeManager::TimeStr timeNow;
        TimeManager::getInstance().getTime(timeNow);
        //Please note that there is no observers yet, but any new observer
        // is notified at attaching time -- so updater only reads value&flags.
        update(timeNow, LU_FALSE);
        if(flags.online == LU_FALSE)
        {
            log.error("MCMBIChannel: board/channel %s/DI:%d initial read error.",
                        MODULE_ToSTRING(MODULE_MCM), ID);
        }
    }
}

MCMBIChannel::~MCMBIChannel()
{
    closeGPIO(fdValue);
}


void MCMBIChannel::update(TimeManager::TimeStr &timeNow, const lu_bool_t forced)
{
    LU_UNUSED(forced);

    lu_bool_t rawValue;
    lu_bool_t value;
    lu_int32_t ret;
    lu_bool_t changed = LU_FALSE;

    IChannel::FlagsStr oldFlags = flags;

    /* Read GPIO */
    ret = readGPIO(fdValue, &rawValue);

    //Apply inversion when needed
    rawValue = (channelConf.extEquipInvert == LU_TRUE)? !rawValue : rawValue;

    if(ret < 0)
    {
        flags.online = LU_FALSE;
        if( (oldFlags.online == LU_TRUE) && (flags.online != oldFlags.online) )
        {
            //Report error only once: when becoming offline
            log.error("%s - read error %i", this->getName(), ret);
        }
    }
    else
    {
        /* Read successfully. */
        flags.online = LU_TRUE;
        flags.restart = LU_FALSE;
        value = rawValue;   //default behaviour when no debounce
        /* Debounce digital input */
        if (channelConf.dbLow2HighMs && channelConf.dbHigh2LowMs)
        {
            //calculate elapsed time
            if(timespec_compare(&timePrev, &TIMESPEC_ZERO) == LU_TRUE)
            {
                timePrev = timeNow.relatime;    //1st time, no lapse
            }
            lu_uint64_t lapseMs = timespec_elapsed_ms(&timePrev, &(timeNow.relatime));
            timePrev = timeNow.relatime;    //keep the latest time for next lapse
            if (rawValue)
            {
                /* Low >>>>>>>> High */
                countLow2High += lapseMs;
                if (countLow2High >= channelConf.dbLow2HighMs)
                {
                    countLow2High = 0;
                }
                else
                {
                    value = currentValue;
                }
                countHigh2Low = 0;
            }
            else
            {
                /* High >>>>>>> Low */
                countHigh2Low += lapseMs;
                if (countHigh2Low >= channelConf.dbHigh2LowMs)
                {
                    countHigh2Low = 0;
                }
                else
                {
                    value = currentValue;
                }
                countLow2High = 0;
            }
        }

        if(currentValue != value)
        {
            changed = LU_TRUE;  //value change
        }
    }

    if(flags != oldFlags)
    {
        changed = LU_TRUE;  //quality change
    }

    /* Generate an event only if the value or status is changed */
    if(changed == LU_TRUE)
    {
        /*Save the value and update time. Lock mutex */
        LockingMutex lMutex(mutex);

        this->timeStamp = timeNow;
        previousValue = currentValue;
        currentValue = value;

        /* Update observers */
        updateObservers();
    }
}

IOM_ERROR MCMBIChannel::read(IChannel::ValueStr& valuePtr)
{
    IOM_ERROR ret;

    if(valuePtr.dataPtr == NULL)
    {
        return IOM_ERROR_PARAM;
    }

    if(fdValue != NULL)
    {
        /* Valid local value. Lock mutex */
        LockingMutex lMutex(mutex);

        /* Use the last updated value */
        *(valuePtr.dataPtr) = currentValue;
        valuePtr.dataPtr->setTime(timeStamp);
        ret = IOM_ERROR_NONE;
    }
    else
    {
        flags.online = LU_FALSE;
        ret = IOM_ERROR_READ;
    }
    valuePtr.flags = flags;
    return ret;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
