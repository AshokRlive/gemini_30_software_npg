/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: MCMMonitorOLRChannel.cpp 14 Jan 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/MCMIOModule/src/MCMMonitorOLRChannel.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       This is an implementation of an MCM Double Digital Output Channel that
 *       sets RTU's OLR state and depends on MCM Monitor communication to do it.
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 14 Jan 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14 Jan 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MCMMonitorOLRChannel.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

MCMMonitorOLRChannel::MCMMonitorOLRChannel(IChannel::ChannelIDStr ID,
                                        MonitorProtocolManager& monProtocol
                                        ) :
                                            MCMChannel(ID.type, ID.id),
                                            monitorProtocol(monProtocol),
                                            timeManager(TimeManager::getInstance()),
                                            currentValue(0)     //OFF by default
{}

MCMMonitorOLRChannel::~MCMMonitorOLRChannel()
{
    stopUpdate();
}


IOM_ERROR MCMMonitorOLRChannel::read(IChannel::ValueStr& valuePtr)
{
    if(valuePtr.dataPtr == NULL)
    {
        return IOM_ERROR_PARAM;
    }

    LockingMutex lMutex(mutex);

    /* Use the last set value */
    *(valuePtr.dataPtr) = currentValue;
    valuePtr.dataPtr->setTime(timeStamp);
    valuePtr.flags = flags;

    return IOM_ERROR_NONE;
}


IOM_ERROR MCMMonitorOLRChannel::write(IChannel::ValueStr& valuePtr)
{
    IOM_ERROR result = IOM_ERROR_NONE;

    if(valuePtr.dataPtr == NULL)
    {
        return IOM_ERROR_PARAM;
    }
    lu_uint32_t value = (lu_uint32_t)*(valuePtr.dataPtr);
    XMSG_VALUE_OLR_CODE stateOLR;
    switch (value)
    {
        case 0:
            stateOLR = XMSG_VALUE_OLR_CODE_OFF;
            break;
        case 1:
            stateOLR = XMSG_VALUE_OLR_CODE_LOCAL;
            break;
        case 2:
            stateOLR = XMSG_VALUE_OLR_CODE_REMOTE;
            break;
        default:
            return IOM_ERROR_PARAM;
            break;
    }
    LockingMutex lMutex(mutex);

    /* update state */
    currentValue = value;
    timeManager.getTime(timeStamp);
    flags = valuePtr.flags;
    flags.restart = LU_FALSE;
    if( monitorProtocol.send_req_offLocalRemote_msg(stateOLR) != MonitorProtocolManager::MONPROTOCOL_ERROR_NONE)
    {
        flags.online = LU_FALSE;
        result = IOM_ERROR_WRITE;
    }
    else
    {
        flags.online = LU_TRUE;
    }
    return result;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
