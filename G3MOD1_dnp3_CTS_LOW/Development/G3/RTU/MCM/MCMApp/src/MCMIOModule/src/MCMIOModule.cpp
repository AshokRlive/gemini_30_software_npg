/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MCM IO Module implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   05/08/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "GlobalDefinitions.h"
#include "versions.h"
#include "svnRevision.h"
#include "MCMIOMap.h"
#include "ModuleProtocol/ModuleProtocolEnumMCM.h"
#include "MonitorPointMapping.h"
#include "MCMIOModule.h"
#include "MCMBIChannel.h"
#include "MCMBOChannel.h"
#include "MCMBODChannel.h"
#include "MCMMonitorAIChannel.h"
#include "MCMMonitorDIChannel.h"
#include "MCMMonitorDDIChannel.h"
#include "MCMMonitorOLRChannel.h"
#include "MCMVirtualDIChannel.h"
#include "ModuleAlarmChannel.h"
#include "SensorFailAlarm.h"
#include "TimeSyncChannel.h"
#include "MCMDIStartChannel.h"
#include "SysAlarmCodeEnum.h"
#include "SysAlarmMCMEnum.h"

    /* XXX: pueyos_a - enable MCM's watchdog restart channel */
    #include "MCMWatchdogChannel.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define CANSTATS_MONREPLYTOUT_MS   100  //MAx amount of ms to wait for all CAN stats from Monitor

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

void deallocateChannels(MCMChannel* array[], lu_uint32_t sizeArray)
{
    /* NOTE: Do NOT use delete[] if there is any possibility of having an array
     *      element already pointing to NULL -- even when delete NULL is safe,
     *      delete [] tries to call the destructor of NULL.
     */

    /* Deallocate all the channels */
    for(lu_uint32_t i = 0; i < sizeArray; ++i)
    {
        if(array[i] != NULL)
        {
            delete array[i];
            array[i] = NULL;
        }
    }
}

void initArray(MCMChannel* array[], lu_uint32_t sizeArray)
{
    for(lu_uint32_t i = 0; i < sizeArray; ++i)
    {
        array[i] = NULL;   //init all to NULL
    }
}

void updateChannels(MCMChannel* array[], lu_uint32_t sizeArray, TimeManager::TimeStr timestamp)
{
    for(lu_uint32_t i = 0; i < sizeArray; ++i)
    {
        if(array[i] != NULL)
        {
            array[i]->update(timestamp);   //update all channels
        }
    }
}

void stopUpdate(MCMChannel* array[], lu_uint32_t sizeArray)
{
    for(lu_uint32_t i = 0; i < sizeArray; ++i)
    {
        if(array[i] != NULL)
        {
            array[i]->stopUpdate();   //notify to stop to all channels
        }
    }
}

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
MCMIOModule::StatsCANStr& MCMIOModule::StatsCANStr::operator=(const CanStatsStr& canStats)
{
    canError            = canStats.canError;
    canBusError         = canStats.canBusError;
    canArbitrationError = canStats.canArbitrationError;
    canDataOverrun      = canStats.canDataOverrun;
    canBusOff           = canStats.canBusOff;
    canErrorPassive     = canStats.canErrorPassive;
    canTxCount          = canStats.canTxCount;
    canRxCount          = canStats.canRxCount;
    canRxLost           = canStats.canRxLost;
    return *this;
}


void MCMIOModule::StatsCANStr::validateAll()
{
    validCANError = 1;
    validCANBusError = 1;
    validCANArbitrationError = 1;
    validCANDataOverrun = 1;
    validCANBusOff = 1;
    validCANErrorPassive = 1;
    validCANTxCount = 1;
    validCANRxCount = 1;
    validCANRxLost = 1;
}


std::string MCMIOModule::StatsCANStr::toString()
{
    std::stringstream ss;
    putValidValue(ss, "Error",           validCANError,          canError);
    putValidValue(ss, "BusError",        validCANBusError,       canBusError);
    putValidValue(ss, "ArbitError",      validCANArbitrationError, canArbitrationError);
    putValidValue(ss, "DataOverrun",     validCANDataOverrun,    canDataOverrun);
    putValidValue(ss, "BusOff",          validCANBusOff,         canBusOff      );
    putValidValue(ss, "ErrorPassive",    validCANErrorPassive,   canErrorPassive);
    putValidValue(ss, "TxCount",         validCANTxCount,        canTxCount     );
    putValidValue(ss, "RxCount",         validCANRxCount,        canRxCount     );
    putValidValue(ss, "RxLost",          validCANRxLost,         canRxLost      );
    return ss.str();
}


MCMIOModule::MCMIOModule(MODULE_ID moduleID,
                        ConfigurationManager &cfgMgr,
                        IMemoryManager& memoryManager,
                        MonitorProtocolManager& monitorProtocol) :
                            IIOModule(MODULE_MCM, moduleID),
                            log(Logger::getLogger(SUBSYSTEM_ID_MCMMODULE)),
                            moduleID(moduleID)
{
    //TODO PW default MCM status?
    status.active = LU_FALSE;
    status.configured = LU_FALSE;
    status.detected = LU_TRUE;
    status.disabled = LU_FALSE;
    status.present = LU_TRUE;
    status.registered = LU_TRUE;

    /* Get the moduleUID and feature revision from MCM's NVRAM */
    if(memoryManager.read(IMemoryManager::STORAGE_VAR_MODULEUID, (lu_uint8_t*)&(mInfo.moduleUID)) == LU_FALSE)
    {
        mInfo.moduleUID = DEFAULT_MODULE_UID;   //default values for when read operation fails
    }
    BasicVersionStr featRev;
    if(memoryManager.read(IMemoryManager::STORAGE_VAR_FEATUREREV, (lu_uint8_t*)&featRev) == LU_FALSE)
    {
        mInfo.featureRevision.major = 0;
        mInfo.featureRevision.minor = 0;
    }
    else
    {
        mInfo.featureRevision.major = featRev.major;
        mInfo.featureRevision.minor = featRev.minor;
    }

    /* NOTE: MCM Architecture change will require a different cross-compiler and
     *       a new .cmake toolchain file, like GCC_TX25_DI_toolchain.cmake so
     *       the architecture ID could be generated from CMake depending on the
     *       file that is being used for compilation */
    mInfo.systemArch = 1001;

    mInfo.application.software.relType = MCM_SOFTWARE_VERSION_RELTYPE;
    mInfo.application.software.version.major = MCM_SOFTWARE_VERSION_MAJOR;
    mInfo.application.software.version.minor = MCM_SOFTWARE_VERSION_MINOR;
    mInfo.application.software.patch = MCM_SOFTWARE_VERSION_PATCH;
    mInfo.application.systemAPI.major = MODULE_SYSTEM_API_VER_MAJOR;
    mInfo.application.systemAPI.minor = MODULE_SYSTEM_API_VER_MINOR;


    /* BootLoader revision number should be 0 since the MCM does not have one */
    mInfo.bootloader.systemAPI.major = 0;
    mInfo.bootloader.systemAPI.minor = 0;
    mInfo.bootloader.software.relType = 0;
    mInfo.bootloader.software.version.major = 0;
    mInfo.bootloader.software.version.minor = 0;
    mInfo.bootloader.software.patch = 0;

    mInfo.svnRevisionBoot = _SVN_REVISION;
    mInfo.svnRevisionApp = _SVN_REVISION;

    /* Initialise complete Channel table */
    channel.resize(sizeChannels);
    channel[0].setTable(DIChannel, MCM_CH_DINPUT_LAST);
    //channel[1].setTable(DOChannel, MCM_CH_DOUTPUT_LAST);
    channel[2].setTable(AIChannel, MCM_CH_AINPUT_LAST);
    channel[3].setTable(IntDIChannel, MCM_CH_INTERNAL_DINPUT_LAST);
    channel[4].setTable(IntDOChannel, MCM_CH_INTERNAL_DOUTPUT_LAST);
    channel[5].setTable(IntDDIChannel, MCM_CH_INTERNAL_DDINPUT_LAST);
    channel[6].setTable(IntDDOChannel, MCM_CH_INTERNAL_DDOUTPUT_LAST);
    channel[7].setTable(IntAIChannel, MCM_CH_INTERNAL_AINPUT_LAST);

    for (lu_uint32_t i = 0; i < sizeChannels; ++i)
    {
        initArray(channel[i].getTable(), channel[i].getEntries());
    }

    /* NOTE: MCM public (non-internal) Channels MCM_CH_xxx are:
     * - Generated from G3/common/xml/ModuleProtocol/ModuleProtocolEnumMCM.xml
     * - Defined in     G3/RTU/MCM/MCMApp/includeGenerated/ModuleProtocol/ModuleProtocolEnumMCM.h
     * MCM public (non-internal) Channels MCM_CH_xxx are:
     * - Generated from G3/common/xml/ModuleProtocol/ModuleProtocolEnumMCMInternal.xml
     * - Defined in     G3/RTU/MCM/MCMApp/includeGenerated/ModuleProtocol/ModuleProtocolEnumMCMInternal.h
     */

#if MCMBOARD_CURRENT == MCMBOARD_TYPE_XH
    /****************************************************/
    /* Channel IOMAP for the MCM G3 Board rev. XF       */
    /****************************************************/
    const lu_uint8_t ImportantSeverity = MODULE_BOARD_ERROR_ALARM_CRITICAL |
                                         MODULE_BOARD_ERROR_ALARM_ERROR |
                                         MODULE_BOARD_ERROR_ALARM_WARNING;
    DIChannel[MCM_CH_DINPUT_RTU_HEALTHY] =      new ModuleAlarmChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_DINPUT, MCM_CH_DINPUT_RTU_HEALTHY),    ImportantSeverity);
    DIChannel[MCM_CH_DINPUT_CRITICAL_ALARM] =   new ModuleAlarmChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_DINPUT, MCM_CH_DINPUT_CRITICAL_ALARM), MODULE_BOARD_ERROR_ALARM_CRITICAL);
    DIChannel[MCM_CH_DINPUT_ERROR_ALARM] =      new ModuleAlarmChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_DINPUT, MCM_CH_DINPUT_ERROR_ALARM),    MODULE_BOARD_ERROR_ALARM_ERROR);
    DIChannel[MCM_CH_DINPUT_WARNING_ALARM] =    new ModuleAlarmChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_DINPUT, MCM_CH_DINPUT_WARNING_ALARM),  MODULE_BOARD_ERROR_ALARM_WARNING);
    DIChannel[MCM_CH_DINPUT_DOOR_SWITCH] =      new MCMBIChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_DINPUT, MCM_CH_DINPUT_DOOR_SWITCH),  MCMInputGPIO_DOOR_SWITCH);
    DIChannel[MCM_CH_DINPUT_HMI_PWR_GOOD] =     new MCMBIChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_DINPUT, MCM_CH_DINPUT_HMI_PWR_GOOD), MCMInputGPIO_HMI_PWR_GOOD);
    DIChannel[MCM_CH_DINPUT_HMI_DETECT] =       new MCMBIChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_DINPUT, MCM_CH_DINPUT_HMI_DETECT),   MCMInputGPIO_HMI_DETECT);
    DIChannel[MCM_CH_DINPUT_OLR_BUTTON] =       new MCMBIChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_DINPUT, MCM_CH_DINPUT_OLR_BUTTON),   MCMInputGPIO_OLR_BUTTON);
    DIChannel[MCM_CH_DINPUT_TIME_QUALITY] =     new TimeSyncChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_DINPUT, MCM_CH_DINPUT_TIME_QUALITY));
    DIChannel[MCM_CH_DINPUT_RTU_STARTED] =      new MCMDIStartChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_DINPUT, MCM_CH_DINPUT_RTU_STARTED));
    /* TODO: pueyos_a - enable MCM's watchdog restart channel */
    DIChannel[MCM_CH_DINPUT_WATCHDOG_RESTART] = new MCMMonitorDIChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_DINPUT, MCM_CH_DINPUT_WATCHDOG_RESTART), MCM_MON_DI_WDOG_FAILED, monitorProtocol);
    //DIChannel[MCM_CH_DINPUT_WATCHDOG_RESTART] = new MCMWatchdogChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_DINPUT, MCM_CH_DINPUT_RTU_STARTED));

    /* NOTE: Monitor AI sampling values should match the ones stated in
     * ModuleProtocolEnumMCM.xml as "defaultEventRate" field values
     */
    AIChannel[MCM_CH_AINPUT_VLOGIC] =       new MCMMonitorAIChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_AINPUT, MCM_CH_AINPUT_VLOGIC),      MCM_MON_AI_VLOGIC_ADDR, monitorProtocol, 10000);
    AIChannel[MCM_CH_AINPUT_V5V0] =         new MCMMonitorAIChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_AINPUT, MCM_CH_AINPUT_V5V0),        MCM_MON_AI_V5_0_ADDR,   monitorProtocol, 10000);
    AIChannel[MCM_CH_AINPUT_V3V3] =         new MCMMonitorAIChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_AINPUT, MCM_CH_AINPUT_V3V3),        MCM_MON_AI_V3_3_ADDR,   monitorProtocol, 10000);
    AIChannel[MCM_CH_AINPUT_VLOCAL] =       new MCMMonitorAIChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_AINPUT, MCM_CH_AINPUT_VLOCAL),      MCM_MON_AI_VLOCAL,      monitorProtocol, 1000);
    AIChannel[MCM_CH_AINPUT_VREMOTE] =      new MCMMonitorAIChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_AINPUT, MCM_CH_AINPUT_VREMOTE),     MCM_MON_AI_VREMOTE,     monitorProtocol, 1000);
    AIChannel[MCM_CH_AINPUT_TEMPERATURE] =  new MCMMonitorAIChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_AINPUT, MCM_CH_AINPUT_TEMPERATURE), MCM_MON_AI_TEMP_ADDR,   monitorProtocol, 10000);
    AIChannel[MCM_CH_AINPUT_CAN0_LOAD] =    new MCMMonitorAIChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_AINPUT, MCM_CH_AINPUT_CAN0_LOAD),   MCM_MON_AI_CAN0_USAGE,  monitorProtocol, 10000);
    AIChannel[MCM_CH_AINPUT_CAN1_LOAD] =    new MCMMonitorAIChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_AINPUT, MCM_CH_AINPUT_CAN1_LOAD),   MCM_MON_AI_CAN1_USAGE,  monitorProtocol, 10000);

    /* Internal channels */
    IntDIChannel[MCM_CH_INTERNAL_DINPUT_SYSHEALTHY] =       new MCMMonitorDIChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_INTERNAL_DINPUT, MCM_CH_INTERNAL_DINPUT_SYSHEALTHY), MCM_MON_DI_GPIO_SYS_HEALTHY, monitorProtocol);
    IntDOChannel[MCM_CH_INTERNAL_DOUTPUT_HMI_PWR_ENABLE] =  new MCMBOChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_INTERNAL_DOUTPUT, MCM_CH_INTERNAL_DOUTPUT_HMI_PWR_ENABLE), MCMOutputGPIO_HMI_PWR_ENABLE, 0);
    IntDDIChannel[MCM_CH_INTERNAL_DDINPUT_OLR] =            new MCMMonitorDDIChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_INTERNAL_DDINPUT, MCM_CH_INTERNAL_DDINPUT_OLR), MCM_MON_DI_GPIO_REMOTE, MCM_MON_DI_GPIO_LOCAL, 100, monitorProtocol);
    IntDDOChannel[MCM_CH_INTERNAL_DDOUTPUT_OLR] =           new MCMMonitorOLRChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_INTERNAL_DDOUTPUT, MCM_CH_INTERNAL_DDOUTPUT_OLR), monitorProtocol);

    IntAIChannel[MCM_CH_INTERNAL_AINPUT_CAN0_ERROR_COUNT  ] =  new MCMMonitorAIChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_INTERNAL_AINPUT, MCM_CH_INTERNAL_AINPUT_CAN0_ERROR_COUNT  ), MCM_MON_AI_CAN0_ERRS,       monitorProtocol, 0);
    IntAIChannel[MCM_CH_INTERNAL_AINPUT_CAN0_RXDATAOVERRUN] =  new MCMMonitorAIChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_INTERNAL_AINPUT, MCM_CH_INTERNAL_AINPUT_CAN0_RXDATAOVERRUN), MCM_MON_AI_CAN0_OVERERR,    monitorProtocol, 0);
    IntAIChannel[MCM_CH_INTERNAL_AINPUT_CAN0_TXCOUNT      ] =  new MCMMonitorAIChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_INTERNAL_AINPUT, MCM_CH_INTERNAL_AINPUT_CAN0_TXCOUNT      ), MCM_MON_AI_CAN0_TX_PACKETS, monitorProtocol, 0);
    IntAIChannel[MCM_CH_INTERNAL_AINPUT_CAN0_RXCOUNT      ] =  new MCMMonitorAIChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_INTERNAL_AINPUT, MCM_CH_INTERNAL_AINPUT_CAN0_RXCOUNT      ), MCM_MON_AI_CAN0_RX_PACKETS, monitorProtocol, 0);
    IntAIChannel[MCM_CH_INTERNAL_AINPUT_CAN0_RXLOST       ] =  new MCMMonitorAIChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_INTERNAL_AINPUT, MCM_CH_INTERNAL_AINPUT_CAN0_RXLOST       ), MCM_MON_AI_CAN0_RXDROP,     monitorProtocol, 0);
    IntAIChannel[MCM_CH_INTERNAL_AINPUT_CAN1_ERROR_COUNT  ] =  new MCMMonitorAIChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_INTERNAL_AINPUT, MCM_CH_INTERNAL_AINPUT_CAN1_ERROR_COUNT  ), MCM_MON_AI_CAN1_ERRS,       monitorProtocol, 0);
    IntAIChannel[MCM_CH_INTERNAL_AINPUT_CAN1_RXDATAOVERRUN] =  new MCMMonitorAIChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_INTERNAL_AINPUT, MCM_CH_INTERNAL_AINPUT_CAN1_RXDATAOVERRUN), MCM_MON_AI_CAN1_OVERERR,    monitorProtocol, 0);
    IntAIChannel[MCM_CH_INTERNAL_AINPUT_CAN1_TXCOUNT      ] =  new MCMMonitorAIChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_INTERNAL_AINPUT, MCM_CH_INTERNAL_AINPUT_CAN1_TXCOUNT      ), MCM_MON_AI_CAN1_TX_PACKETS, monitorProtocol, 0);
    IntAIChannel[MCM_CH_INTERNAL_AINPUT_CAN1_RXCOUNT      ] =  new MCMMonitorAIChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_INTERNAL_AINPUT, MCM_CH_INTERNAL_AINPUT_CAN1_RXCOUNT      ), MCM_MON_AI_CAN1_RX_PACKETS, monitorProtocol, 0);
    IntAIChannel[MCM_CH_INTERNAL_AINPUT_CAN1_RXLOST       ] =  new MCMMonitorAIChannel(IChannel::ChannelIDStr(CHANNEL_TYPE_INTERNAL_AINPUT, MCM_CH_INTERNAL_AINPUT_CAN1_RXLOST       ), MCM_MON_AI_CAN1_RXDROP,     monitorProtocol, 0);


    /* Array of observed channels for different Module Alarms
     * Any of these entries specify that if a channel is in a condition that
     * should trigger an MCM Module Alarm, the Module Alarm have to be notified.
     */
    const MCMIOModule::AlarmChannelStr channelAlarm[] =
    {
        /* MCM Voltage measurement: */
        {SYS_ALARM_SUBSYSTEM_MCM, SYSALC_MCM_RTU_VOLTAGE_UNSAFE, SYS_ALARM_SEVERITY_WARNING,
            AIChannel[MCM_CH_AINPUT_VLOGIC], MCMAlarmChannelObserver::CHANNELALARMTYPE_ALARMFLAG_AI},
        {SYS_ALARM_SUBSYSTEM_MCM, SYSALC_MCM_CPU_VOLTAGE_UNSAFE, SYS_ALARM_SEVERITY_WARNING,
            AIChannel[MCM_CH_AINPUT_V5V0], MCMAlarmChannelObserver::CHANNELALARMTYPE_ALARMFLAG_AI},
        {SYS_ALARM_SUBSYSTEM_MCM, SYSALC_MCM_CPU_VOLTAGE_UNSAFE, SYS_ALARM_SEVERITY_WARNING,
            AIChannel[MCM_CH_AINPUT_V3V3], MCMAlarmChannelObserver::CHANNELALARMTYPE_ALARMFLAG_AI},
        /* MCM temperature measurement: */
        {SYS_ALARM_SUBSYSTEM_MCM, SYSALC_MCM_CPU_OVERTEMP, SYS_ALARM_SEVERITY_WARNING,
            AIChannel[MCM_CH_AINPUT_TEMPERATURE], MCMAlarmChannelObserver::CHANNELALARMTYPE_ALARMFLAG_AI_POS},
        {SYS_ALARM_SUBSYSTEM_MCM, SYSALC_MCM_CPU_UNDERTEMP, SYS_ALARM_SEVERITY_WARNING,
            AIChannel[MCM_CH_AINPUT_TEMPERATURE], MCMAlarmChannelObserver::CHANNELALARMTYPE_ALARMFLAG_AI_NEG},
        /* MCM Local/Remote lines Voltage measurement: */
        {SYS_ALARM_SUBSYSTEM_MCM, SYSALC_MCM_LOCALREMOTE_LINE, SYS_ALARM_SEVERITY_ERROR,
            AIChannel[MCM_CH_AINPUT_VLOCAL], MCMAlarmChannelObserver::CHANNELALARMTYPE_ALARMFLAG},
        {SYS_ALARM_SUBSYSTEM_MCM, SYSALC_MCM_LOCALREMOTE_LINE, SYS_ALARM_SEVERITY_ERROR,
            AIChannel[MCM_CH_AINPUT_VREMOTE], MCMAlarmChannelObserver::CHANNELALARMTYPE_ALARMFLAG},
    };
    const lu_uint32_t sizeChannelAlarm = sizeof(channelAlarm)/sizeof(MCMIOModule::AlarmChannelStr);

#elif MCMBOARD_CURRENT == MCMBOARD_TYPE_XF
    /****************************************************/
    /* Channel IOMAP for the MCM G3 Board rev. XF       */
    /****************************************************/
    DIChannel[MCM_CH_DINPUT_LOCAL] =        new MCMBIChannel(MCM_CH_DINPUT_LOCAL,       MCMInputGPIO_LOCAL);
    DIChannel[MCM_CH_DINPUT_REMOTE] =       new MCMBIChannel(MCM_CH_DINPUT_REMOTE,      MCMInputGPIO_REMOTE);
    DIChannel[MCM_CH_DINPUT_DOOR_SWITCH] =  new MCMBIChannel(MCM_CH_DINPUT_DOOR_SWITCH, MCMInputGPIO_DOOR_SWITCH);
    DIChannel[MCM_CH_DINPUT_HMI_PWR_GOOD] = new MCMBIChannel(MCM_CH_DINPUT_HMI_PWR_GOOD, MCMInputGPIO_HMI_PWR_GOOD);
    DIChannel[MCM_CH_DINPUT_HMI_DETECT] =   new MCMBIChannel(MCM_CH_DINPUT_HMI_DETECT,  MCMInputGPIO_HMI_DETECT);
    DIChannel[MCM_CH_DINPUT_OLR_BUTTON] =   new MCMBIChannel(MCM_CH_DINPUT_OLR_BUTTON,  MCMInputGPIO_OLR_BUTTON);
    DIChannel[MCM_CH_DINPUT_VOLTAGE_LOW] =  new MCMBIChannel(MCM_CH_DINPUT_VOLTAGE_LOW, 68);
    DIChannel[MCM_CH_DINPUT_CFG_IN] =       new MCMBIChannel(MCM_CH_DINPUT_CFG_IN,      MCMInputGPIO_CFG_IN);
    DIChannel[MCM_CH_DINPUT_FACTORY] =      new MCMBIChannel(MCM_CH_DINPUT_FACTORY,     MCMInputGPIO_FACTORY);

    DOChannel[MCM_CH_DOUTPUT_LOCAL] =       new MCMBOChannel(MCM_CH_DOUTPUT_LOCAL,          MCMOutputGPIO_LOCAL, 1);
    DOChannel[MCM_CH_DOUTPUT_REMOTE] =      new MCMBOChannel(MCM_CH_DOUTPUT_REMOTE,         MCMOutputGPIO_REMOTE, 1);

    AIChannel[MCM_CH_AINPUT_VLOGIC] =       new MCMMonitorAIChannel(MCM_CH_AINPUT_VLOGIC,       MCM_MON_AI_VLOGIC_ADDR, monitorProtocol);
    AIChannel[MCM_CH_AINPUT_V5V0] =         new MCMMonitorAIChannel(MCM_CH_AINPUT_V5V0,         MCM_MON_AI_V5_0_ADDR,   monitorProtocol);
    AIChannel[MCM_CH_AINPUT_V3V3] =         new MCMMonitorAIChannel(MCM_CH_AINPUT_V3V3,         MCM_MON_AI_V3_3_ADDR,   monitorProtocol);
    AIChannel[MCM_CH_AINPUT_VLOCAL] =       new MCMMonitorAIChannel(MCM_CH_AINPUT_VLOCAL,       MCM_MON_AI_VLOCAL,      monitorProtocol);
    AIChannel[MCM_CH_AINPUT_VREMOTE] =      new MCMMonitorAIChannel(MCM_CH_AINPUT_VREMOTE,      MCM_MON_AI_VREMOTE,     monitorProtocol);
    AIChannel[MCM_CH_AINPUT_TEMPERATURE] =  new MCMMonitorAIChannel(MCM_CH_AINPUT_TEMPERATURE,  MCM_MON_AI_TEMP_ADDR,   monitorProtocol);

    /* Internal DO channels */
    channelHMI =  new MCMBOChannel(MCM_CH_DOUTPUT_LAST,    MCMOutputGPIO_HMI_PWR_ENABLE, 0);

#elif MCMBOARD_CURRENT == MCMBOARD_TYPE_XE
/********************************************/
/* IOMAP for the MCM G3 Board rev. up to XE */
/********************************************/
    DIChannel[MCM_CH_DINPUT_LOCAL] =        new MCMBIChannel(MCM_CH_DINPUT_LOCAL,       27);   /* Active low */
    DIChannel[MCM_CH_DINPUT_REMOTE] =       new MCMBIChannel(MCM_CH_DINPUT_REMOTE,      28);   /* Active low */
    DIChannel[MCM_CH_DINPUT_DOOR_SWITCH] =  new MCMBIChannel(MCM_CH_DINPUT_DOOR_SWITCH, 83);
    DIChannel[MCM_CH_DINPUT_HMI_PWR_GOOD] = new MCMBIChannel(MCM_CH_DINPUT_HMI_PWR_GOOD, 79);
    DIChannel[MCM_CH_DINPUT_HMI_DETECT] =   new MCMBIChannel(MCM_CH_DINPUT_HMI_DETECT,  29);
    DIChannel[MCM_CH_DINPUT_OLR_BUTTON] =   new MCMBIChannel(MCM_CH_DINPUT_OLR_BUTTON,  67);   /* Active low */
    DIChannel[MCM_CH_DINPUT_VOLTAGE_LOW] =  new MCMBIChannel(MCM_CH_DINPUT_VOLTAGE_LOW, 68);
    DIChannel[MCM_CH_DINPUT_CFG_IN] =       new MCMBIChannel(MCM_CH_DINPUT_CFG_IN,      30);
    DIChannel[MCM_CH_DINPUT_FACTORY] =      new MCMBIChannel(MCM_CH_DINPUT_FACTORY,     37);

    DOChannel[MCM_CH_DOUTPUT_LOCAL] =           new MCMBODChannel(MCM_CH_DOUTPUT_LOCAL,          46, 45, 1);
    DOChannel[MCM_CH_DOUTPUT_REMOTE] =          new MCMBODChannel(MCM_CH_DOUTPUT_REMOTE,         48, 47, 1);

    DOChannel[MCM_CH_DOUTPUT_HWDOG_KICK] =      new MCMBOChannel(MCM_CH_DOUTPUT_HWDOG_KICK,     89, 1);
    DOChannel[MCM_CH_DOUTPUT_APPRAM_WEN] =      new MCMBOChannel(MCM_CH_DOUTPUT_APPRAM_WEN,     6, 1);
    DOChannel[MCM_CH_DOUTPUT_HMI_PWR_ENABLED] = new MCMBOChannel(MCM_CH_DOUTPUT_HMI_PWR_ENABLED, 80 ,1);
    DOChannel[MCM_CH_DOUTPUT_CONFIG_DTR] =      new MCMBOChannel(MCM_CH_DOUTPUT_CONFIG_DTR,     04, 1);
    DOChannel[MCM_CH_DOUTPUT_CONTROL_DTR] =     new MCMBOChannel(MCM_CH_DOUTPUT_CONTROL_DTR,    05, 1);
    DOChannel[MCM_CH_DOUTPUT_LED_OK_RED] =      new MCMBOChannel(MCM_CH_DOUTPUT_LED_OK_RED,     38, 1);
    DOChannel[MCM_CH_DOUTPUT_LED_OK_GREEN] =    new MCMBOChannel(MCM_CH_DOUTPUT_LED_OK_GREEN,   39, 1);
    DOChannel[MCM_CH_DOUTPUT_LED_CAN_RED] =     new MCMBOChannel(MCM_CH_DOUTPUT_LED_CAN_RED,    41, 1);
    DOChannel[MCM_CH_DOUTPUT_LED_CAN_GREEN] =   new MCMBOChannel(MCM_CH_DOUTPUT_LED_CAN_GREEN,  40, 1);
    DOChannel[MCM_CH_DOUTPUT_DUMMYR_RED] =      new MCMBOChannel(MCM_CH_DOUTPUT_DUMMYR_RED,     42, 1);
    DOChannel[MCM_CH_DOUTPUT_DUMMYR_GREEN] =    new MCMBOChannel(MCM_CH_DOUTPUT_DUMMYR_GREEN,   43, 1);
    DOChannel[MCM_CH_DOUTPUT_LED_EXTINGUISH] =  new MCMBOChannel(MCM_CH_DOUTPUT_LED_EXTINGUISH, 44, 1);
    DOChannel[MCM_CH_DOUTPUT_LED_LOCAL] =       new MCMBOChannel(MCM_CH_DOUTPUT_LED_LOCAL,      49, 1);
    DOChannel[MCM_CH_DOUTPUT_LED_REMOTE] =      new MCMBOChannel(MCM_CH_DOUTPUT_LED_REMOTE,     50, 1);
    DOChannel[MCM_CH_DOUTPUT_LED_OFF] =         new MCMBOChannel(MCM_CH_DOUTPUT_LED_OFF,        81, 1);
    DOChannel[MCM_CH_DOUTPUT_CFG_OUT] =         new MCMBOChannel(MCM_CH_DOUTPUT_CFG_OUT,        31, 0);

#elif MCMBOARD_CURRENT == MCMBOARD_TYPE_DEV
    /***************************************/
    /* IOMAP for the MCM Development Board */
    /***************************************/
    DIChannel[MCM_CH_DINPUT_LOCAL] =        new MCMBIChannel(MCM_CH_DINPUT_LOCAL,       38);   /* Active low */
    DIChannel[MCM_CH_DINPUT_REMOTE] =       new MCMBIChannel(MCM_CH_DINPUT_REMOTE,      40);   /* Active low */
    DIChannel[MCM_CH_DINPUT_DOOR_SWITCH] =  new MCMBIChannel(MCM_CH_DINPUT_DOOR_SWITCH, 41);
    DIChannel[MCM_CH_DINPUT_HMI_PWR_GOOD] = new MCMBIChannel(MCM_CH_DINPUT_HMI_PWR_GOOD, 42);

    DOChannel[MCM_CH_DOUTPUT_LOCAL] =           new MCMBOChannel(MCM_CH_DOUTPUT_LOCAL,          47, 1);
    DOChannel[MCM_CH_DOUTPUT_REMOTE] =          new MCMBOChannel(MCM_CH_DOUTPUT_REMOTE,         48, 1);
    DOChannel[MCM_CH_DOUTPUT_HWDOG_KICK] =      new MCMBOChannel(MCM_CH_DOUTPUT_HWDOG_KICK,     49, 1);
    DOChannel[MCM_CH_DOUTPUT_APPRAM_WEN] =      new MCMBOChannel(MCM_CH_DOUTPUT_APPRAM_WEN,     50, 1);
    DOChannel[MCM_CH_DOUTPUT_HMI_PWR_ENABLED] = new MCMBOChannel(MCM_CH_DOUTPUT_HMI_PWR_ENABLED, 51 ,1);
    DOChannel[MCM_CH_DOUTPUT_CONFIG_DTR] =      new MCMBOChannel(MCM_CH_DOUTPUT_CONFIG_DTR,     19, 1);
    DOChannel[MCM_CH_DOUTPUT_CONTROL_DTR] =     new MCMBOChannel(MCM_CH_DOUTPUT_CONTROL_DTR,    97, 1);
    DOChannel[MCM_CH_DOUTPUT_LED_OK_RED] =      new MCMBOChannel(MCM_CH_DOUTPUT_LED_OK_RED,     33, 1);
    DOChannel[MCM_CH_DOUTPUT_LED_OK_GREEN] =    new MCMBOChannel(MCM_CH_DOUTPUT_LED_OK_GREEN,   20, 1);
    DOChannel[MCM_CH_DOUTPUT_LED_CAN_RED] =     new MCMBOChannel(MCM_CH_DOUTPUT_LED_CAN_RED,    21, 1);
    DOChannel[MCM_CH_DOUTPUT_LED_CAN_GREEN] =   new MCMBOChannel(MCM_CH_DOUTPUT_LED_CAN_GREEN,  34, 1);
    DOChannel[MCM_CH_DOUTPUT_DUMMYR_RED] =      new MCMBOChannel(MCM_CH_DOUTPUT_DUMMYR_RED,     32, 1);
    DOChannel[MCM_CH_DOUTPUT_DUMMYR_GREEN] =    new MCMBOChannel(MCM_CH_DOUTPUT_DUMMYR_GREEN,   101, 1);
    DOChannel[MCM_CH_DOUTPUT_LED_EXTINGUISH] =  new MCMBOChannel(MCM_CH_DOUTPUT_LED_EXTINGUISH, 102, 1);
#endif


    /* get configuration for channels */
    ICANModuleConfigurationFactory &config = cfgMgr.getCANChannelFactory();
    for(lu_uint32_t i = 0; i < MCM_CH_DINPUT_LAST; i++)
    {
        if(DIChannel[i] != NULL)
        {
            /* Get configuration */
            InputDigitalCANChannelConf conf;
            config.getIDigitalConf(mid.type, mid.id, i, conf); //moduleUID number is ignored for MCM here

            //Force channel-level inversion for some MCM channels
            switch (i)
            {
                case MCM_CH_DINPUT_DOOR_SWITCH:
                case MCM_CH_DINPUT_HMI_DETECT:
                case MCM_CH_DINPUT_OLR_BUTTON:
                case MCM_CH_DINPUT_TIME_QUALITY:
                    conf.extEquipInvert = LU_TRUE;
                    break;
                default:
                    conf.extEquipInvert = LU_FALSE; //enforce that MCM channels cannot be inverted
                    break;
            }
            DIChannel[i]->configure(conf);
        }
    }
    for(lu_uint32_t i = 0; i < MCM_CH_AINPUT_LAST; i++)
    {
        if(AIChannel[i] != NULL)
        {
            /* Get configuration */
            InputAnalogueCANChannelConf conf;
            config.getIAnalogueConf(mid.type, mid.id, i,  conf);

            /* TODO: pueyos_a - Configure MCM AI, set eventing time (with a min val), and so on */
            AIChannel[i]->configure(conf);
        }
    }

    /* Register alarm observers */
    ModuleAlarm::AlarmIDStr sensorAlarmID(SYS_ALARM_SUBSYSTEM_MCM, SYSALC_MCM_SENSOR_FAILURE);
    sensorAlarm = new SensorFailAlarm(sensorAlarmID, *this);//Sensor failure specific alarm
    MCMAlarmChannelObserver* observer;
    ModuleAlarm alarmRef;   //Alarm reference (only for telling ID & Severity to observer)
    for (lu_uint32_t i = 0; i < sizeChannelAlarm; ++i)  //configure alarms from array
    {
        if(channelAlarm[i].channelPtr != NULL)
        {
            alarmRef.subSystem = channelAlarm[i].subSystem;
            alarmRef.alarmCode = channelAlarm[i].alarmCode;
            alarmRef.severity = channelAlarm[i].severity;
            observer = new MCMAlarmChannelObserver(*(channelAlarm[i].channelPtr),
                                            channelAlarm[i].channelAlarmType,
                                            alarmRef,
                                            *this
                                            );
            alarmMonitored.push_back(observer);
            (channelAlarm[i].channelPtr)->attach(observer);
        }
    }

    /* Set module status */
    //TODO to be fixed PW
//    moduleData.hasConfig = config.isConfigured(moduleData.type, moduleData.id);
//    moduleData.configured = moduleData.hasConfig;
//    moduleData.ok = LU_TRUE;
    status.configured = LU_TRUE;
}


MCMIOModule::~MCMIOModule()
{
    /* Remove all the alarm observers */
    for (lu_uint32_t i = 0; i < alarmMonitored.size(); ++i)
    {
        delete alarmMonitored[i];
    }
    delete sensorAlarm;
    /* Deallocate all the channels */
    for (lu_uint32_t i = 0; i < sizeChannels; ++i)
    {
        deallocateChannels(channel[i].getTable(), channel[i].getEntries());
    }
}


IChannel* MCMIOModule::getChannel(CHANNEL_TYPE type, lu_uint32_t number)
{
    IChannel* channel = NULL;

    switch(type)
    {
        case CHANNEL_TYPE_DINPUT:
            if(number < MCM_CH_DINPUT_LAST)
            {
                channel = DIChannel[number];
            }
        break;

        case CHANNEL_TYPE_DOUTPUT:
//            if(number < MCM_CH_DOUTPUT_LAST)
//            {
//                channel = DOChannel[number];
//            }
        break;

        case CHANNEL_TYPE_AINPUT:
            if(number < MCM_CH_AINPUT_LAST)
            {
                channel = AIChannel[number];
            }
        break;

        case CHANNEL_TYPE_INTERNAL_DINPUT:
            if(number < MCM_CH_INTERNAL_DINPUT_LAST)
            {
                channel = IntDIChannel[number];
            }
        break;

        case CHANNEL_TYPE_INTERNAL_DOUTPUT:
            if(number < MCM_CH_INTERNAL_DOUTPUT_LAST)
            {
                channel = IntDOChannel[number];
            }
        break;

        case CHANNEL_TYPE_INTERNAL_DDINPUT:
            if(number < MCM_CH_INTERNAL_DDINPUT_LAST)
            {
                channel = IntDDIChannel[number];
            }
        break;

        case CHANNEL_TYPE_INTERNAL_DDOUTPUT:
            if(number < MCM_CH_INTERNAL_DDOUTPUT_LAST)
            {
                channel = IntDDOChannel[number];
            }
        break;

        case CHANNEL_TYPE_INTERNAL_AINPUT:
            if(number < MCM_CH_INTERNAL_AINPUT_LAST)
            {
                channel = IntAIChannel[number];
            }
        break;

        default:
            log.debug("%s unsupported channel type: %i", __AT__, type);
        break;
    }
    return channel;
}


IChannel& MCMIOModule::getChannelHMIPowerEnable()
{
    return *IntDOChannel[MCM_CH_INTERNAL_DOUTPUT_HMI_PWR_ENABLE];
}

IChannel& MCMIOModule::getChannelHMIPowerGood()
{
    return *DIChannel[MCM_CH_DINPUT_HMI_PWR_GOOD];
}

IChannel& MCMIOModule::getChannelHMIDectect()
{

    return *DIChannel[MCM_CH_DINPUT_HMI_DETECT];
}

IChannel& MCMIOModule::getChannelDoorSwitch()
{

    return *DIChannel[MCM_CH_DINPUT_DOOR_SWITCH];
}



IOM_ERROR MCMIOModule::readRAWChannel(IChannel::ChannelIDStr channel, IChannel::ValueStr& data)
{
    //MCM Module get its channel values from the channel itself
    IChannel* channelPtr = getChannel(channel.type, channel.id);
    if(channelPtr != NULL)
    {
        return channelPtr->read(data);
    }
    return IOM_ERROR_NOT_FOUND;
}


IOM_ERROR MCMIOModule::sendMessage(ModuleMessage *messagePtr,lu_uint32_t retries)
{
    LU_UNUSED(messagePtr);
    LU_UNUSED(retries);
    return IOM_ERROR_NOT_SUPPORTED;
}


IOM_ERROR MCMIOModule::sendMessage( MODULE_MSG_TYPE  messageType,
                                    MODULE_MSG_ID    messageID  ,
                                    PayloadRAW      *messagePtr,
                                    lu_uint32_t retries
                                  )
{
    LU_UNUSED(messageType);
    LU_UNUSED(messageID);
    LU_UNUSED(messagePtr);
    LU_UNUSED(retries);
    return IOM_ERROR_NOT_SUPPORTED;
}


IOM_ERROR MCMIOModule::sendMessage(sendReplyStr* messagePtr,lu_uint32_t retries)
{
    LU_UNUSED(messagePtr);
    LU_UNUSED(retries);
    return IOM_ERROR_NOT_SUPPORTED;
}


IOM_ERROR MCMIOModule::decodeMessage(ModuleMessage* message)
{
    LU_UNUSED(message);

    return IOM_ERROR_NOT_SUPPORTED;
}


IOM_ERROR MCMIOModule::tickEvent(lu_uint32_t dTime)
{
    LU_UNUSED(dTime);
    TimeManager::TimeStr timestamp;

    /* Get current timestamp */
    TimeManager::getInstance().getTime(timestamp);

    for (lu_uint32_t i = 0; i < sizeChannels; ++i)
    {
        updateChannels(channel[i].getTable(), channel[i].getEntries(), timestamp);
    }

    /* Update alarm observers (requires slower sampling) */
    if(alarmSampling.elapsed_ms(timestamp) > ALARMSAMPLINGRATE_MS)
    {
        sensorAlarm->update();
    }
    return IOM_ERROR_NONE;
}


void MCMIOModule::disableTimeout(lu_bool_t disable)
{
    LU_UNUSED(disable);
}


IOM_ERROR MCMIOModule::getInfo(IOModuleInfoStr& info)
{
    info.mid = mid;
    info.status = status;
    info.moduleUID = getModuleUID();
    info.hBeatInfo.status = MODULE_BOARD_STATUS_ON_LINE;
    info.hBeatInfo.error = alarmStatus();
    info.hBeatInfo.moduleUID = getModuleUID();
    info.mInfo = this->mInfo;
    info.fsm = FSM_STATE_ACTIVE; //TODO variable?

    return IOM_ERROR_NONE;
}


IOM_ERROR MCMIOModule::getDetails(IOModuleDetailStr& moduleInfo)
{
    moduleInfo.mid = this->getID();
    moduleInfo.architecture = mInfo.systemArch;
    moduleInfo.svnRevisionBoot = 0; // Not available
    moduleInfo.svnRevisionApp = _SVN_REVISION;
    moduleInfo.uptime = TimeManager::getInstance().getAppRunningTime_ms() / 1000;
    return IOM_ERROR_NONE;
}

lu_bool_t MCMIOModule::getStatus(MSTATUS _status)
{
    switch (_status) {
        case MSTATUS_PRESENT     :
            return status.present;

        case MSTATUS_CONFIGURED  :
            return status.configured;

        case MSTATUS_DISABLED    :
            return status.disabled;

        case MSTATUS_DETECTED    :
            return status.detected;

        case MSTATUS_REGISTERED  :
            return status.registered;

        case MSTATUS_ACTIVE      :
            return status.active;

        default:
            log.error("CAN Module Status not found:%i",_status);
            return LU_FALSE;
    }
}


void MCMIOModule::getSystemAPI(ModuleVersionStr& sysAPI)
{
    sysAPI = mInfo.application.systemAPI;
}


IOM_ERROR MCMIOModule::ping(lu_uint32_t timeout, lu_uint32_t payloadSize)
{
    LU_UNUSED(timeout);
    LU_UNUSED(payloadSize);

    return IOM_ERROR_NOT_SUPPORTED;
}


IOM_ERROR MCMIOModule::getTime(TimeManager::TimeStr& time, lu_uint32_t timeout)
{
    LU_UNUSED(time);
    LU_UNUSED(timeout);

    return IOM_ERROR_NOT_SUPPORTED;
}


IOM_ERROR MCMIOModule::getCANStats(std::vector<StatsCANStr>& statsList)
{
    StatsCANStr canStats;
    TimeManager::TimeStr timestamp;
    lu_uint32_t canList[2][5] = {
                        {
                            MCM_CH_INTERNAL_AINPUT_CAN0_ERROR_COUNT,
                            MCM_CH_INTERNAL_AINPUT_CAN0_RXDATAOVERRUN,
                            MCM_CH_INTERNAL_AINPUT_CAN0_TXCOUNT,
                            MCM_CH_INTERNAL_AINPUT_CAN0_RXCOUNT,
                            MCM_CH_INTERNAL_AINPUT_CAN0_RXLOST
                        },
                        {
                            MCM_CH_INTERNAL_AINPUT_CAN1_ERROR_COUNT,
                            MCM_CH_INTERNAL_AINPUT_CAN1_RXDATAOVERRUN,
                            MCM_CH_INTERNAL_AINPUT_CAN1_TXCOUNT,
                            MCM_CH_INTERNAL_AINPUT_CAN1_RXCOUNT,
                            MCM_CH_INTERNAL_AINPUT_CAN1_RXLOST
                        }
                    };


    statsList.clear();
    TimeManager::getInstance().getTime(timestamp);

    /* iterate through CAN 0 and CAN1 */
    for (lu_uint32_t i = 0; i < 2; ++i)
    {
        std::vector<lu_uint32_t> listCAN(canList[i], canList[i] + (sizeof(canList[i]) / sizeof(canList[i][0])));
        askCANxStats(listCAN, timestamp);

        /* Get some parameters directly from the interface */
        COMM_INTERFACE iface;
        iface = (i==0)? COMM_INTERFACE_CAN0 :
                        COMM_INTERFACE_CAN1;
        CANInterface& canIface = CANInterface::getInstance(iface);
        struct can_device_stats statsCAN;
        if(can_get_device_stats(canIface.getName(), &statsCAN) == 0)
        {
            canStats.canBusError = statsCAN.bus_error;
            canStats.validCANBusError = 1;
            canStats.canArbitrationError = statsCAN.arbitration_lost;
            canStats.validCANArbitrationError = 1;
            canStats.canBusOff = statsCAN.bus_off;
            canStats.validCANBusOff = 1;
            canStats.canErrorPassive = statsCAN.error_passive;
            canStats.validCANErrorPassive = 1;
        }
        getCANxStats(listCAN, timestamp, canStats);
        statsList.push_back(canStats);
    }
    return IOM_ERROR_NONE;
}


IOM_ERROR MCMIOModule::askMonAIChannel(MCM_CH_INTERNAL_AINPUT channelID, TimeManager::TimeStr timestamp)
{
    if(channelID >= MCM_CH_INTERNAL_AINPUT_LAST)
        return IOM_ERROR_PARAM;

    MCMMonitorAIChannel* chan;
    chan = dynamic_cast<MCMMonitorAIChannel*>(IntAIChannel[channelID]);
    if(chan == NULL)
    {
        return IOM_ERROR_NOT_FOUND;
    }
    log.debug("Ask Mon for channel %i", channelID);
    chan->update(timestamp, LU_TRUE);   //force sampling
    return IOM_ERROR_NONE;
}


void MCMIOModule::askCANxStats(std::vector<lu_uint32_t>& channelIDList,
                                    TimeManager::TimeStr timestamp
                                    )
{
    for(std::vector<lu_uint32_t>::iterator it = channelIDList.begin(), end = channelIDList.end(); it != end; ++it)
    {
        askMonAIChannel(static_cast<MCM_CH_INTERNAL_AINPUT>(*it), timestamp);
    }
}


IOM_ERROR MCMIOModule::getCANxStats(std::vector<lu_uint32_t>& channelIDList,
                                    TimeManager::TimeStr timestamp,
                                    StatsCANStr& canStats
                                    )
{
    IODataFloat32 value;
    IChannel::ValueStr valueRead(value);
    TimeManager::TimeStr timeNow;   //current time for timeout calculation
    lu_uint32_t updateCount;    //Counts down updates of values

    TimeManager& timeManager = TimeManager::getInstance();
    updateCount = channelIDList.size();
    do
    {
        for(std::vector<lu_uint32_t>::iterator it = channelIDList.begin(),
                                               end = channelIDList.end(); it != end; ++it)
        {
            if(IntAIChannel[*it]->read(valueRead) == IOM_ERROR_NONE)
            {
                if(valueRead.flags.restart == LU_FALSE)
                {
                    //valid updated read
                    --updateCount;
                    switch(*it)
                    {
                        case MCM_CH_INTERNAL_AINPUT_CAN0_ERROR_COUNT  :
                        case MCM_CH_INTERNAL_AINPUT_CAN1_ERROR_COUNT  :
                            canStats.canError = *(valueRead.dataPtr);
                            canStats.validCANError = 1;
                            break;
                        case MCM_CH_INTERNAL_AINPUT_CAN0_RXDATAOVERRUN:
                        case MCM_CH_INTERNAL_AINPUT_CAN1_RXDATAOVERRUN:
                            canStats.canDataOverrun = *(valueRead.dataPtr);
                            canStats.validCANDataOverrun = 1;
                            break;
                        case MCM_CH_INTERNAL_AINPUT_CAN0_TXCOUNT      :
                        case MCM_CH_INTERNAL_AINPUT_CAN1_TXCOUNT      :
                            canStats.canTxCount = *(valueRead.dataPtr);
                            canStats.validCANTxCount = 1;
                            break;
                        case MCM_CH_INTERNAL_AINPUT_CAN0_RXCOUNT      :
                        case MCM_CH_INTERNAL_AINPUT_CAN1_RXCOUNT      :
                            canStats.canRxCount = *(valueRead.dataPtr);
                            canStats.validCANRxCount = 1;
                            break;
                        case MCM_CH_INTERNAL_AINPUT_CAN0_RXLOST       :
                        case MCM_CH_INTERNAL_AINPUT_CAN1_RXLOST       :
                            canStats.canRxLost = *(valueRead.dataPtr);
                            canStats.validCANRxLost = 1;
                            break;
                        default:
                            return IOM_ERROR_NOT_FOUND;
                    }
                }
            }
        }
        timeManager.getTime(timeNow);
    } while((updateCount > 0) && (timestamp.elapsed_ms(timeNow) < CANSTATS_MONREPLYTOUT_MS) );

    if(updateCount != 0)
    {
        return IOM_ERROR_OFFLINE;
    }
    return IOM_ERROR_NONE;
}


void MCMIOModule::stop()
{
    /* Stop all the physical channels */
    for (lu_uint32_t i = 0; i < sizeChannels; ++i)
    {
        stopUpdate(channel[i].getTable(), channel[i].getEntries());
    }
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
template<typename T> void MCMIOModule::StatsCANStr::putValidValue( std::stringstream &ss,
                                                                        const char *name,
                                                                        const lu_uint16_t valid,
                                                                        T value
                                                                        )
{
    ss << " " << name << ":";
    if(valid)
        ss << value;
    else
        ss << "--";
    ss << std::endl;
}


/*
 *********************** End of file ******************************************
 */

