/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: SensorFailAlarm.cpp 16 Jan 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/MCMIOModule/src/SensorFailAlarm.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       This is an implementation of an MCM Virtual Digital Input Channel that
 *       calculates its value & flags from the online status of several other
 *       MCM's internal channels.
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 16 Jan 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   16 Jan 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "SensorFailAlarm.h"
#include "ModuleProtocolEnumMCMInternal.h"
#include "MCMIOModule.h"
#include "Logger.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
SensorFailAlarm::SensorFailAlarm(ModuleAlarm::AlarmIDStr alarmRef, IIOModule& mcmModule) :
                                            moduleMCM(mcmModule),
                                            alarmID(alarmRef),
                                            currentValue(LU_FALSE),
                                            isCurrentValid(LU_FALSE),
                                            isInit(LU_FALSE)
{
    //Cast the generic module reference provided to an specific MCM Module.
    MCMIOModule* mcm = dynamic_cast<MCMIOModule*>(&mcmModule);
    if(mcm == NULL)
    {
        return; //If the provided module is not the MCM, there is nothing to do
    }

    isInit = LU_TRUE;  //MCM module available

    /* Lists of channels to be observed */
    MCM_CH_DINPUT publicDIChannel[] = {    //Array of observed channels for sensor failure
                                    MCM_CH_DINPUT_DOOR_SWITCH ,
                                    MCM_CH_DINPUT_HMI_PWR_GOOD,
                                    MCM_CH_DINPUT_HMI_DETECT  ,
                                    MCM_CH_DINPUT_OLR_BUTTON
                                    };
    MCM_CH_INTERNAL_DINPUT internalDIChannel[] = {MCM_CH_INTERNAL_DINPUT_SYSHEALTHY};
    MCM_CH_INTERNAL_DDINPUT internalDDIChannel[] = {MCM_CH_INTERNAL_DDINPUT_OLR};

    /* Register observers over related channels (sensors) */
    lu_uint32_t size; //Amount of channels in the array
    size = sizeof(publicDIChannel) / sizeof(MCM_CH_DINPUT);
    registerObservers(CHANNEL_TYPE_DINPUT, publicDIChannel, size);
    size = sizeof(internalDIChannel) / sizeof(MCM_CH_INTERNAL_DINPUT);
    registerObservers(CHANNEL_TYPE_INTERNAL_DINPUT, internalDIChannel, 1);
    size = sizeof(internalDDIChannel) / sizeof(MCM_CH_INTERNAL_DDINPUT);
    registerObservers(CHANNEL_TYPE_INTERNAL_DDINPUT, internalDDIChannel, 1);
}


SensorFailAlarm::~SensorFailAlarm()
{
    /* Remove observers, if any */
    for (lu_uint32_t i = 0; i < channelList.size(); ++i)
    {
        delete channelList[i];
    }
}


void SensorFailAlarm::update()
{
    lu_bool_t failed = LU_FALSE;
    lu_uint32_t pos = 0;    //array position of the offending observed channel

    if(isInit == LU_FALSE)
    {
        return;
    }

    /* Search for a channel in failed state */
    for(std::vector<lu_bool_t>::iterator it = channelStateList.begin(),
        end = channelStateList.end();
        (it != end);
        ++it, ++pos
        )
    {
        if( (*it) == LU_TRUE )
        {
            failed = LU_TRUE;
            break;      //found a channel that failed: no need to keep searching
        }
    }

    //Prevent currentValue concurrent update
    MasterLockingMutex mMutex(accessLock, LU_TRUE);

    //Note: alarm requires to be always refreshed
    MCMIOModule* mcm = dynamic_cast<MCMIOModule*>(&moduleMCM);
    if(mcm != NULL)
    {
        if(failed == LU_TRUE)
        {
            /* Compose parameter: e.g. 2001 is the module's DO number 1 */
            IChannel::ChannelIDStr chID = channelList[pos]->getChannelID();
            lu_uint16_t par = (chID.type * 1000) + chID.id;
            mcm->activateAlarm(alarmID, SYS_ALARM_SEVERITY_ERROR, par);
        }
        else
        {
            mcm->deactivateAlarm(alarmID);  //Notify
        }
    }

}


void OnlineChannelObserver::update(IChannel *subject)
{
    if(subject == NULL)
        return;

    lu_bool_t status = LU_TRUE;     //States if there is an error accessing the sensor
    IODataInt8 data;
    IChannel::ValueStr value(data);
    /* Sensor is OK if it is readable and it is online */
    if(subject->read(value) == IOM_ERROR_NONE)
    {
        status = (value.flags.online == LU_FALSE)? LU_TRUE : LU_FALSE;
    }
    else
    {
        log.error("MCM channel %s is unreadable", subject->getName());
    }
    parent.update(status, id);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */
void SensorFailAlarm::update(lu_bool_t statusFail, lu_uint32_t observerID)
{
    if(isInit == LU_FALSE)
    {
        return;
    }

    try
    {
        //update channel status in the list
        channelStateList.at(observerID) = statusFail;
    }
    catch(...)
    {
        return; //invalid ID: nothing to do
    }

    update();   //issue the alarm if needed
}


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
template<typename ArrayType>
void SensorFailAlarm::registerObservers(const CHANNEL_TYPE channelType, ArrayType channelArray[], const lu_uint32_t size)
{

    IChannel* channel;
    OnlineChannelObserver* observer;
    lu_uint32_t channelID = channelList.size(); //Continue filling the vector

    for (lu_uint32_t j = 0; j < size; ++j)
    {
        channel = moduleMCM.getChannel(channelType, channelArray[j]);
        if( channel != NULL)
        {
            observer = new OnlineChannelObserver(channelID++, *channel, *this);
            channelList.push_back(observer);        //add observer
            channelStateList.push_back(LU_FALSE);   //set its state
            channel->attach(observer);
        }
    }
}




/*
 *********************** End of file ******************************************
 */
