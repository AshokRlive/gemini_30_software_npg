/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: MCMVirtualDIChannel.cpp 16 Jan 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3Trunk/G3/RTU/MCM/MCMApp/src/MCMIOModule/src/MCMVirtualDIChannel.cpp $
 *
 *    FILE TYPE: C++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       This is an implementation of an MCM Virtual Digital Input Channel that
 *       takes its value & flags from internal (logical) status of the MCM.
 *
 *    CURRENT REVISION
 *
 *               $Revision: $: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 16 Jan 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   16 Jan 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "MCMVirtualDIChannel.h"
#include "timeOperations.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

MCMVirtualDIChannel::MCMVirtualDIChannel(IChannel::ChannelIDStr ID) :
                                            MCMChannel(ID.type, ID.id),
                                            timeManager(TimeManager::getInstance()),
                                            currentValue(0),
                                            accessLock(LU_FALSE)    //not shared
{
    //Virtual channels are always online by default
    flags.online = LU_TRUE;
}


IOM_ERROR MCMVirtualDIChannel::read(IChannel::ValueStr& valuePtr)
{
    if(valuePtr.dataPtr == NULL)
    {
        return IOM_ERROR_PARAM;
    }

    MasterLockingMutex mMutex(accessLock);

    /* Use the last updated value */
    *(valuePtr.dataPtr) = currentValue;
    valuePtr.dataPtr->setTime(timeStamp);
    valuePtr.flags = flags;

    return IOM_ERROR_NONE;
}


IOM_ERROR MCMVirtualDIChannel::write(IChannel::ValueStr& data)
{
    lu_bool_t value = *(data.dataPtr);

    //Prevent updating -- use the same value for all observers
    MasterLockingMutex mMutex(accessLock, LU_TRUE);

    if( (currentValue != value) || (data.flags != flags) )
    {
        //value or status change: store and notify
        timeManager.getTime(timeStamp);
        currentValue = value;
        flags = data.flags;
        flags.restart = LU_FALSE;   //not the initial status anymore
        flags.online = LU_TRUE;     //Virtual channels are always online by definition

        if(channelConf.eventEnable == LU_TRUE)
        {
            //Notify only when event-enabled
            updateObservers();
        }
    }

    return IOM_ERROR_NONE;
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
