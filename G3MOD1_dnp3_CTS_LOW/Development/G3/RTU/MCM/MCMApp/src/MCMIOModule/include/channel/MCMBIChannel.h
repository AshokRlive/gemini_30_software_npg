/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       MCM binary input channel interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   04/08/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(EA_6F17A293_23E3_464f_BAA6_F653EB6BA9E7__INCLUDED_)
#define EA_6F17A293_23E3_464f_BAA6_F653EB6BA9E7__INCLUDED_


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "MCMChannel.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */


class MCMBIChannel : public MCMChannel
{

public:
    MCMBIChannel(IChannel::ChannelIDStr ID, lu_uint8_t gpio);
    virtual ~MCMBIChannel();

    /* === Inherited from MCMChannel === */
    virtual IOM_ERROR read(IChannel::ValueStr& valuePtr);
    virtual void update(TimeManager::TimeStr &timestamp, const lu_bool_t forced);

private:
    lu_uint8_t gpio;            //GPIO ID
    FILE *fdValue;              //GPIO value file
    lu_bool_t currentValue;     //debounce
    lu_bool_t previousValue;    //debounce
    struct timespec timePrev;   //debounce
    lu_uint16_t countHigh2Low;  //debounce
    lu_uint16_t countLow2High;  //debounce

};
#endif // !defined(EA_6F17A293_23E3_464f_BAA6_F653EB6BA9E7__INCLUDED_)

/*
 *********************** End of file ******************************************
 */
