/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: MCMMonitorOLRChannel.h 14 Jan 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/RTU/MCM/MCMApp/src/MCMIOModule/include/MCMMonitorOLRChannel.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       This is a definition of an MCM Double Digital Output Channel that sets
 *       RTU's OLR state and depends on MCM Monitor communication to do it.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 14 Jan 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14 Jan 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MCMMONITOROLRCHANNEL_H__INCLUDED
#define MCMMONITOROLRCHANNEL_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "MCMChannel.h"
#include "MonitorProtocolManager.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief OLR state Double Digital Output Channel using MCM Monitor communication
 *
 * This class provides an MCM Channel that sets the OLR state using the
 * MCM Monitor application.
 */
class MCMMonitorOLRChannel : public MCMChannel
{
public:
    /*!
     * \brief Custom constructor
     */
    MCMMonitorOLRChannel(IChannel::ChannelIDStr ID,
                        MonitorProtocolManager& monProtocol
                        );
    virtual ~MCMMonitorOLRChannel();

    /* === Inherited from MCMChannel === */
    virtual IOM_ERROR read(IChannel::ValueStr& valuePtr);
    virtual IOM_ERROR write(IChannel::ValueStr& valuePtr);

private:
    MonitorProtocolManager& monitorProtocol; //reference of the Monitor Protocol Handler
    TimeManager& timeManager;
    lu_uint32_t currentValue;       //last value read

    /* FIXME: pueyos_a - FLAGS convert to simple mutex for MonitorDI Channel */
//    MasterMutex accessLock;     //Prevent concurrent access to common members of the same channel

};

#endif /* MCMMONITOROLRCHANNEL_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
