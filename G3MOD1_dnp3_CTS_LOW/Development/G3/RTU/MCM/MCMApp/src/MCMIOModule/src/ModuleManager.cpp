/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       G3 module manager implementation
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   08/08/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "ModuleManager.h"
#include "Debug.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
ModuleManager::ModuleManager( SCHED_TYPE schedType,
                              lu_uint32_t priority,
                              CANIOModuleFactory* factory,
                              ConfigurationManager& cManager,
                              IMemoryManager& memManager,
                              MonitorProtocolManager& monProtocol
                            ) :
                            #if MODBUS_MASTER_SUPPORT
                            mbdeviceManager(),
                            #endif
                            canManager(schedType, priority, factory, cManager),
                            mcmManager(schedType, priority-5, cManager, memManager, monProtocol),
                            moduleStatus(*this)
{
    mcmManager.setModuleStatusMonitor(&moduleStatus);
}

ModuleManager::~ModuleManager()
{}


IOM_ERROR ModuleManager::discover()
{

#if MODBUS_MASTER_SUPPORT
    DBG_INFO(".Starting ModbusMaster");
    mbdeviceManager.start();
#endif

    /* We don't need to run the discover on the MCM */
    return canManager.discover();
}

void ModuleManager::setBootloaderMode(lu_bool_t mode)
{
    canManager.setBootloaderMode(mode);
    /* TODO: pueyos_a - call monitor protocol to set BL mode LED */
}


void ModuleManager::setPowerSaveMode(lu_bool_t mode)
{
    canManager.setPowerSaveMode(mode);
    mcmManager.setPowerSaveMode(mode);
}

#if DEBUG_HBEAT_ENABLED
void ModuleManager::setHBeatEnable(lu_uint8_t x){
    canManager.setHBeatEnable(x);
}
#endif


void ModuleManager::grantAlarms()
{
    /* Get CAN slave boards */
    std::vector<IIOModule*> moduleList;
    moduleList = canManager.getAllModules();
    for(std::vector<IIOModule*>::iterator it=moduleList.begin(); it!=moduleList.end(); ++it)
    {
        (*it)->grantAlarms();
    }

    /* Get MCM board */
    IIOModule *mcmPtr = mcmManager.getModule(MODULE_MCM, MODULE_ID_0);
    if(mcmPtr != NULL)
    {
        mcmPtr->grantAlarms();
    }
}


IIOModule* ModuleManager::createModule(MODULE module, MODULE_ID moduleID, COMM_INTERFACE iFace)
{
    LU_UNUSED(iFace);

    switch(module)
    {
        case MODULE_MCM:
            return mcmManager.getModule(module, moduleID);
        case MODULE_BRD:
            return NULL;
        case MODULE_HMI:
            return canManager.createModule(module, moduleID, COMM_INTERFACE_CAN1);
        default:
            return canManager.createModule(module, moduleID, COMM_INTERFACE_CAN0);
    }
}


IIOModule* ModuleManager::getModule(MODULE module, MODULE_ID moduleID)
{
    if(module == MODULE_MCM)
    {
        return mcmManager.getModule(module, moduleID);
    }
#if MODBUS_MASTER_SUPPORT
    else if(module == MODULE_MBDEVICE)
    {
         return mbdeviceManager.getMBDevice(moduleID);
    }
#endif
    else
    {
        return canManager.getModule(module, moduleID);
    }
}


MCMIOModule* ModuleManager::getMCM()
{
    MCMIOModule* mcm = NULL;
    IIOModule* m = getModule(MODULE_MCM, MODULE_ID_0);

    if(m != NULL)
    {
        mcm = dynamic_cast<MCMIOModule*>(m);
    }

    return mcm;
}


std::vector<IIOModule*> ModuleManager::getAllModules()
{
    std::vector<IIOModule*> ret, ret2;
    ret = canManager.getAllModules();
    ret2 = mcmManager.getAllModules();
    ret.insert(ret.end(), ret2.begin(), ret2.end());
    return ret;
}


IChannel *ModuleManager::getChannel( MODULE module,
                                     MODULE_ID moduleID,
                                     CHANNEL_TYPE chType,
                                     lu_uint32_t chNumber
                                   )
{
    if(module == MODULE_MCM)
    {
        return mcmManager.getChannel(module, moduleID, chType, chNumber);
    }
#if MODBUS_MASTER_SUPPORT
    else if(module == MODULE_MBDEVICE)
    {
        return mbdeviceManager.getChannel(module, moduleID, chType, chNumber);
    }
#endif
    else
    {
        return canManager.getChannel(module, moduleID, chType, chNumber);
    }
}


IOM_ERROR ModuleManager::restartAllModules()
{
    return (canManager.restartAllModules() != CAN_ERROR_NONE)? IOM_ERROR_SEND : IOM_ERROR_NONE;
}


IOM_ERROR ModuleManager::stopAllModules()
{
    /* Stop all Modules updating
     * Ignore Error Code as we are closing and just want to stop modules
     * */
    canManager.stopAllModules();
    mcmManager.stopAllModules();


#if MODBUS_MASTER_SUPPORT
    mbdeviceManager.stopAllModules();
#endif

    return IOM_ERROR_NONE;
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
