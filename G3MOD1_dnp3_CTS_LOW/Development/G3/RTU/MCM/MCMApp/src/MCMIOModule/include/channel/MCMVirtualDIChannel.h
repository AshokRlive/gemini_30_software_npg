/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: MCMVirtualDIChannel.h 16 Jan 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/RTU/MCM/MCMApp/src/MCMIOModule/include/MCMVirtualDIChannel.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       This is a definition of an MCM Virtual Digital Input Channel that takes
 *       its value & flags from internal (logical) status of the MCM.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 16 Jan 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   16 Jan 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef MCMVIRTUALDICHANNEL_H__INCLUDED
#define MCMVIRTUALDICHANNEL_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "MCMChannel.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Digital Input Channel from MCM Monitor communication
 *
 * This class provides an MCM Channel that has a value originated from the MCM
 * Application itself.
 */
class MCMVirtualDIChannel : public MCMChannel
{
public:
    /*!
     * \brief Custom constructor
     */
    MCMVirtualDIChannel(IChannel::ChannelIDStr ID);

    /* === Inherited from MCMChannel === */
    virtual IOM_ERROR read(IChannel::ValueStr& valuePtr);
    virtual IOM_ERROR write(IChannel::ValueStr& valuePtr);

protected:
    TimeManager& timeManager;       //reference to the Time manager to get time stamps
    lu_bool_t currentValue;         //Current channel DI value

    /* FIXME: pueyos_a - FLAGS convert to simple mutex? */
    MasterMutex accessLock;     //Prevent concurrent access to common members of the same channel

};

#endif /* MCMVIRTUALDICHANNEL_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
