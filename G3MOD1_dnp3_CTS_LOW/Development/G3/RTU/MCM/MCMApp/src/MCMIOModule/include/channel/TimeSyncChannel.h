/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: TimeSyncChannel.h 21 Jan 2014 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/RTU/MCM/MCMApp/src/MCMIOModule/include/TimeSyncChannel.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       This is a definition of an MCM Virtual Digital Input Channel that
 *       calculates its value from the valid synchronisation time, changing when
 *       time synchronisation does change.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 21 Jan 2014 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   21 Jan 2014   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef TIMESYNCCHANNEL_H__INCLUDED
#define TIMESYNCCHANNEL_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "MCMVirtualDIChannel.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 * \brief Time quality Channel from Time Management
 *
 * This class provides an MCM Channel that is has a value of 0 when the RTU has
 * lost his external time synchronisation.
 */
class TimeSyncChannel : public MCMVirtualDIChannel, private TimeObserver
{
public:
    TimeSyncChannel(IChannel::ChannelIDStr ID);
    virtual ~TimeSyncChannel();

    /* ===Overrides MCMChannel== */
    virtual IOM_ERROR configure(InputDigitalCANChannelConf &conf);

private:
    /* FIXME: pueyos_a - FLAGS convert to simple mutex? */
    MasterMutex accessLock;     //Prevent concurrent access to common members of the same channel

private:
    /* === Inherited from TimeObserver === */
    virtual void updateTime(TimeManager::TimeStr& currentTime);
};


#endif /* TIMESYNCCHANNEL_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
