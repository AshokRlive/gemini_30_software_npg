#!/bin/bash
# copy axf of MCM board to export path, for sending to the MCM itself
APP="Debug/*.axf"
DEST="/exports/${USER}/gemini/application"
DEST_LIB_AUTOSCHEME="${DEST}/lib/automation/"
mkdir -p "${DEST_LIB_AUTOSCHEME}"

cd Debug
make $1
RESULT=$?
if [ $RESULT -eq 0 ]
then
    # successful make
    cd ..
    cp -v $APP $DEST
    cp -v Debug/ModbusDeviceManager/*.axf $DEST
    cp -v Debug/lib/automation/*.so $DEST_LIB_AUTOSCHEME
    cp -v Debug/lib/automation/*.il $DEST_LIB_AUTOSCHEME
    cp -v Debug/G3DBClient/*.so  ${DEST}/lib/
    cp -v Debug/104Slave/*.axf $DEST
	cp -v ../scripts $DEST -R
	if [ "$2" = "TX25" ]; 
    then 
        # stripped executable
        /usr/local/arm/cross-gcc-4.5.3-armv5te-soft/x86_64-pc-linux-gnu/bin/arm-926ejs-linux-gnueabi-strip -v -o ${DEST}/stripMCMBoard.axf  Debug/MCMBoard.axf
    fi
    echo copy done!
else
    exit $RESULT
fi
