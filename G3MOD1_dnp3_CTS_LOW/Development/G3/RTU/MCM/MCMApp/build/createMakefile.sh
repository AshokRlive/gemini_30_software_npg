#!/bin/bash

# ** DEBUG **
# check Debug directory and clean it
if [ -d Debug ]; then
    # Remove Debug contents
    rm -rf Debug/*
else
    # Remove Debug directory
    rm -rf Debug
    # Create Debug directory
    mkdir Debug
fi
# Run cmake and create Debug Makefiles
cd Debug
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ../../src/ -DCMAKE_INSTALL_PREFIX=./install $*
cd ..


# ** RELEASE ** 
# check Release directory and clean it
if [ -d Release ]; then
    # Remove Release contents
    rm -rf Release/*
else
    # Remove Release directory
    rm -rf Release
    # Create Release directory
    mkdir Release
fi
# Run cmake and create Release Makefiles
cd Release
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release ../../src/ -DCMAKE_INSTALL_PREFIX=./install $*
cd ..
exit

# ** TEST ** 
# check Test directory and clean it
if [ -d Test ]; then
    # Remove Test contents
    rm -rf Test/*
else
    # Remove Test directory
    rm -rf Test
    # Create Test directory
    mkdir Test
fi

# Run cmake and create Test Makefiles
cd Test
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ../../test/
cd ..
