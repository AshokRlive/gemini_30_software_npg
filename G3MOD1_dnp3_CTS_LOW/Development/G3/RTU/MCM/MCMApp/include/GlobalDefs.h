/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       G3 common global defines
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   29/06/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#if !defined(_GLOBAL_DEFS_INCLUDED_)
#define _GLOBAL_DEFS_INCLUDED_


/* TODO: pueyos_a - Eliminate all the conditional compilation flags in final version */
//#define AP_SESSION    //CTH session handling

#define CONFIGCRC     //Config file CRC report to Config Tool

#define ENABLECOUNTERS   1   //Counter VP support

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Thread.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/************************************/
/*  MACROS FOR DEBUGGING            */
/************************************/

/*
 * Enable ZMQ for ConfigTool.
 */
#define CT_ZMQ_SUPPORT 0

#define FIREWALL_SUPPORT 1


/**
 * Enable ModBus Master.
 */
#define MODBUS_MASTER_SUPPORT 1


#define IEC_104_SLAVE_SUPPORT 1

#define IEC_101_SLAVE_SUPPORT 1

/*
 * Set to 1 to enable commands for controlling the heart-beat.
 * E.g.
 * set heart-beat to bootloader mode;
 * start/stop heart-beat sending.
 */
#define DEBUG_HBEAT_ENABLED 1 //TODO apply this macro to related modules and commands.

#define SCM_MK2_SUPPORT 1

/************************************/
/*  THREAD PRIORITIES               */
/************************************/
/**
 * Priorities for threads defined in this application.
 * Values should be between Thread::PRIO_MIN (=1) and Thread::PRIO_MAX (=50).
 * Please refer all values as related to Thread::PRIO_MIN or Thread::PRIO_MAX
 */
#define MONITOR_PROTOCOL_THREAD             (Thread::PRIO_MAX)
#define MODULE_MANAGER_THREAD               (Thread::PRIO_MIN + 40)  //used by 2 threads: CANPeriodicData & CANEventManager
#define MODULE_MANAGER_MCM_THREAD           (MODULE_MANAGER_THREAD - 5) //reserved for mcmManager
#define GEMINI_DATABASE_THREAD              (Thread::PRIO_MIN + 30) //2 threads (Jobs and timedJobs)
#define SCADA_PMANAGER_THREAD               (Thread::PRIO_MIN + 20)
#define SCADA_CMANAGER_THREAD               (Thread::PRIO_MIN + 5)
#define EVENT_MEMORY_THREAD                 (Thread::PRIO_MIN + 10)
#define HTTP_CTH_LINKLAYER_THREAD           (Thread::PRIO_MIN + 9)
#define HMI_MANAGER_THREAD                  (Thread::PRIO_MIN + 8)
//#define CONSOLE_THREAD                      (Thread::PRIO_MIN + 25)   //5 threads (per connected Console client)
#define COMMSDEV_MANAGER_THREAD             (Thread::PRIO_MIN + 4)

/* TOTAL Threads max = 9 + timers + console + other temporal threads */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

#include "ControlLogicDef.h"

/* TODO: pueyos_a - Move to EventTypeDef.xml */

/**
 * \brief Sources for triggering an Off/Local/Remote State change
 */
typedef enum
{
    OLR_SOURCE_DEFAULT,     //default value
    OLR_SOURCE_MEMORY,      //recovered from memory
    OLR_SOURCE_CLI,         //Command Line Interpreter
    OLR_SOURCE_CTOOL,       //Configuration Tool
    OLR_SOURCE_HMI,         //HMI Mode button
    OLR_SOURCE_FBUTTON,     //MCM's Front pushbutton
    OLR_SOURCE_BKPLANESW,   //Backplane switch
    OLR_SOURCE_OTHER,       //Other source
    OLR_SOURCE_LAST
} OLR_SOURCE;

#ifndef ARRAYENUMSTRINGSTRUCT_DEFINED
#define ARRAYENUMSTRINGSTRUCT_DEFINED
typedef struct ArrayEnumStringStructDef
{
    lu_int32_t value;
    lu_char_t* string;
} ArrayEnumStringStruct;
#endif

#define OLR_SOURCE_ENUMSTRINGSIZE 9
static const ArrayEnumStringStruct OLR_SOURCE_EnumString [OLR_SOURCE_ENUMSTRINGSIZE] =
{
    {0x00, "Default"},
    {0x01, "Memory"},
    {0x02, "Console"},
    {0x03, "Config Tool"},
    {0x04, "HMI"},
    {0x05, "Front Button"},
    {0x06, "Backplane"},
    {0x07, "Other"},
    {0x08, "Unknown"}
};

#include <stddef.h>        /* include is needed for NULL symbol usage */
/**
 * \brief Function to extract Enum strings from VERSION_TYPE_EnumString
 * \param value Value of the Enum associated with string
 * \return Pointer to associated string
 */
#ifdef __GNUC__
__inline
#endif
static const lu_char_t* OLR_SOURCE_ToSTRING(lu_int32_t value)
{
    int i;
    for(i=0; i<OLR_SOURCE_ENUMSTRINGSIZE; i++)
    {
        if(OLR_SOURCE_EnumString[i].value == value)
        {
            return OLR_SOURCE_EnumString[i].string;
        }
    }
    return NULL;
}
//end AP move


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */



#endif /* _GLOBAL_DEFS_INCLUDED_ */

/*
 *********************** End of file ******************************************
 */
