/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: IMCMApplication.h 19 Sep 2013 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/MCM/MCMApp/include/IMCMApplication.h $
 *
 *    FILE TYPE: C++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Interface to MCM Application launching and general interaction.
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 19 Sep 2013 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19 Sep 2013   pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef IMCMAPPLICATION_H__INCLUDED
#define IMCMAPPLICATION_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "MonitorProtocol.h"
#include "MainAppEnum.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
class MCMApplication;	//Here for reference to the MCMBoard class only

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/*!
 *   \brief Class for interfacing the MCMApplication object
 */
class IMCMApplication
{
public:
    /**
     * \brief types for restart/reboot/shutdown actions
     */
    typedef enum
    {
        RESTART_TYPE_TRY,       //Try to proceed now
        RESTART_TYPE_FORCED,    //Proceed now in any case
        RESTART_TYPE_DELAY      //Proceed when ready
    } RESTART_TYPE;
public:
    IMCMApplication() {};
    virtual ~IMCMApplication() {};

    virtual APP_EXIT_CODE start(lu_bool_t noCRC = LU_FALSE,
                                lu_bool_t console = LU_FALSE
                                ) = 0;
    virtual bool reboot(RESTART_TYPE type = RESTART_TYPE_TRY) = 0;
    virtual bool restart(RESTART_TYPE type = RESTART_TYPE_TRY,
                         APP_EXIT_CODE exitcode = APP_EXIT_CODE_RESTART) = 0;
    virtual bool shutdown(RESTART_TYPE type = RESTART_TYPE_TRY) = 0;

    virtual lu_int32_t setSCADAService(lu_bool_t enable) = 0;

//    virtual MCM_ERROR factoryReset();
};


/*!
 *   \brief Singleton class to contain the instance of the MCMApplication object
 *
 *   This is meant to grant access to the MCMApplication functionality without
 *   requiring to pass around a reference of the MCMApplication itself
 */
class MCMBoard
{
public:
    /**
     * \brief Get an MCMBoard instance.
     *
     * Usage example:
     *  MCMBoard& mcmBoard = MCMBoard::getInstance();
     *
     * \return Reference to this singleton
     */
    static MCMBoard& getInstance()
    {
        static MCMBoard instance;
        return instance;
    };
    /**
     * Access to the MCM Application.
     *
     * Usage example:
     *  IMCMApplication* mcmApplication = MCMBoard::getInstance().getMCMApp();
     *
     * \return Reference to the main Application
     */
    IMCMApplication* getMCMApp()
    {
        return mcmApp;
    };
public:
    /* Please Note that:
     *      friend class IMCMApplication;
     * allows the application interface to call setMCMApp() but not to their
     * children, since C++ friendship is not inherited.
     */
    friend class MCMApplication; //This allows application objects to set the application

protected:
    /**
     * \brief Sets the MCM Application
     *
     * This is intended to be done once by the MCM Application itself
     *
     * \param app Reference to the MCM Application
     */
    void setMCMApp(IMCMApplication* app)
    {
        mcmApp = app;
    };

private:
    IMCMApplication* mcmApp;    //Pointer to application

private:
    MCMBoard() : mcmApp(NULL)
    {};
    virtual ~MCMBoard() {};
    /*
     * \brief Singleton: Private copy constructor
     */
    MCMBoard(const MCMBoard& mcmBoard) : mcmApp(NULL)
    {
        LU_UNUSED(mcmBoard);
    };
};


#endif /* IMCMAPPLICATION_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
