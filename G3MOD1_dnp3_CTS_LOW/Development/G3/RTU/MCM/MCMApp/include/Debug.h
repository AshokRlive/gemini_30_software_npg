/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:Debug.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   9 Oct 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef DEBUG_H_
#define DEBUG_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "dbg.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define DEBUG_IEC_101_SLAVE         0
#define DEBUG_IEC_104_SLAVE         0
#define DEBUG_DNP3_SLAVE            0

#define DEBUG_COMMS_DEVICE          0

#define DEBUG_MODBUS_MANAGER        0
#define DEBUG_MODBUS_MODULE         0

#define DEBUG_PORTS_MANAGER         0

#define DEBUG_PROTOCOLSTACK_TRAFFIC 0

#define DEBUG_CLOGIC                0
/**
 * Shows CPU time cost when initialising each application module.
 * WARNING: This can be left enabled in the RELEASE version! This is done for
 *          performance measurement reasons.
 */
#define DEBUG_CPU_TIME              0





#if DEBUG_CPU_TIME
#define PRINTCPUTIME(description)                                            \
    {                                                                        \
        struct timespec appTime;                                             \
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &appTime);                   \
        Logger::g_logger.info(description                                    \
                        " [CPU%lli][%lli].\n",                               \
                        timespec_to_ms(&appTime),                            \
                        TimeManager::getInstance().getAppRunningTime_ms());  \
    }
#else
#define PRINTCPUTIME(description)
#endif


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */



#endif /* DEBUG_H_ */

/*
 *********************** End of file ******************************************
 */
