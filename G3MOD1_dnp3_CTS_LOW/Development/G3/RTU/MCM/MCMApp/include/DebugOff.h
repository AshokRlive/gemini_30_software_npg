/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:DebugOff.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   16 Oct 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef DEBUGOFF_H_
#define DEBUGOFF_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

/* Removing definitions to prevent "warning: DBG_xxx redefined" compiler messages */
#ifdef DBG_ERR
#undef DBG_ERR
#undef DBG_EMPTY
#undef DBG_INFO
#undef DBG_WARN
#undef DBG_TRACK
#undef DBG_ERRNO
#undef DBG_CHECK
#undef DBG_CTOR
#undef DBG_DTOR
#undef DBG_THREAD
#undef DBG_RAW
#endif

#define DBG_EMPTY   do {} while(0)
#define DBG_TRACK(M, ...)       DBG_EMPTY
#define DBG_ERR(M, ...)         DBG_EMPTY
#define DBG_ERRNO(M, ...)       DBG_EMPTY
#define DBG_WARN(M, ...)        DBG_EMPTY
#define DBG_INFO(M, ...)        DBG_EMPTY
#define DBG_CHECK(A, M, ...)    DBG_EMPTY
#define DBG_CTOR()              DBG_EMPTY
#define DBG_DTOR()              DBG_EMPTY
#define DBG_THREAD(msg)         DBG_EMPTY
#define DBG_RAW(M, ...)         DBG_EMPTY

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */



#endif /* DEBUGOFF_H_ */

/*
 *********************** End of file ******************************************
 */
