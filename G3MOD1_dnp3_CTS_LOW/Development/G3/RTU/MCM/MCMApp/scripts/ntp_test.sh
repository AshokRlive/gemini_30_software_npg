#!/bin/bash

#  Purpose:
#      Check the list of configured NTP Servers for the last time they provided a time update to the RTU.  Compare this against a max value and return the result.
#  Params:
#      <max_update_period> - Time period within which the RTU must have had a time update from NTP
#  Returns:
#      0 - RTU time updated within <max_update_period>
#      1 - RTU time NOT updated wwithin <max_update_period>

# If -check is given as a param, only create the certificate if it doesn't already exist
if [ "$#" -ne 1 ]
then
    echo "Usage: $0 <max_update_period>"
    exit 1
fi

IFS_old=$IFS
export IFS=$'\n'

last_update=999999
max_update_period=$1
ntpserver_last_update=`ntpq -c op`    # Command to retrieve current status of NTP servers

for line in $ntpserver_last_update; do
    when=`echo "${line}" | awk '{print $5}'`    # Extract the 'when' field (last update)
    status=${line:0:1}  # The first char gives us the status of the server

#    echo -n "$line status = ${status} when=$when"

    if [[ $status =~ [*+#o] ]]  # Only take the 'when' field from a 'good' server
    then
#        echo -n " Good server "
        # Check if the value is not an integer (i.e. it has units)
        if ! [ "$when" -eq "$when" ] 2>/dev/null
        then
#            echo -n " has units!!!"
            whenunits=${when: -1}
            when=${when::-1}
#            echo -n "$when units [$whenunits]"

            case $whenunits in
                m)  # Minutes
                    let when=$when*60
                    ;;
                h)  # Hours
                    let when=$when*3600
                    ;;
                d)  # Days
                    let when=$when*86400
                                        ;;
			esac
#			 echo " $when seconds"
        fi

        # Store the lowest value
        if [ "$when" -lt "$last_update" ] 2>/dev/null
        then
            last_update=$when
        fi
    fi
#    echo ""

done # for...do

export IFS=$IFS_old
#echo "Last good update was $last_update"

if [ "$max_update_period" -gt "$last_update" ]
then
#    echo "NTP update the RTU OK"
    exit 0
else
#    echo "NTP update too old!!!"
    exit 1
fi
