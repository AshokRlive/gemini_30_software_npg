#!/bin/sh

# Include common
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source ${script_dir}/cert_common.sh $1

prepare_private_key
create_csr

# Sign csr to create a cert
openssl x509 -req -days ${arg_days} -in ${pendingdir}/${csrname} \
-signkey ${pendingdir}/${keyname} -out ${pendingdir}/${crtname} > /dev/null 2>&1

retval=$?
if [ $retval -ne 0 ]
then
  log_err "Failed to create self signed certificate, err:${retval}."   
else
	log_info "Created a self signed certificate."		
fi

exit_with_status $retval


