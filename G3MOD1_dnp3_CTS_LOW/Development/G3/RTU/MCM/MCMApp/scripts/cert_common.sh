#!/bin/sh

##### Common part of all certificate scripts ######

# Check arguments
if [ $# -ne 1 ]
  then
    echo "Missing argument:  <protocol>. E.g.'https', 'sdnp3/sessionA', 's104/sessionA', etc."
    exit -1
fi

# Set environment variables
alias openssl="/usr/lib/ssl/bin/openssl"
certsdir="/usr/local/gemini/certs/${1}"
pendingdir="${certsdir}/pending"

keyname="g3.key"
crtname="g3.crt"
csrname="g3.csr"
pemname="g3cert.pem"

logfile="${pendingdir}/.log"
statusfile="${pendingdir}/.status"

# Set common parameters
arg_keylen="2048"
arg_subj="/CN=g3-mcm/O=Lucy Electric (EMS) Ltd"
arg_days="3650"


# Create dir
mkdir -p ${certsdir}
mkdir -p ${pendingdir}

log_info(){
	echo "[INFO ] $1"
	echo "[INFO ] $1" >> ${logfile}
}

log_err(){
	echo "[ERROR] $1"
	echo "[ERROR] $1" >> ${logfile}
}

exit_with_status(){
	echo $1 > ${statusfile}
	exit $1
}

check_err(){
retval=$?
if [ $retval -ne 0 ]; then
	log_err "error detected:${retval}"	
	exit_with_status $retval
fi
}

# Create a new private key
create_private_key(){
	if [ $# -ne 1 ]; then
		  echo "Missing argument:  <target>."
		  exit_with_status -1
	fi

	openssl genrsa -out $1 ${arg_keylen}
	check_err
	log_info "Created a new private key."
}


# Prepare a private key in the pending directory. 
prepare_private_key() {
	log_info "Preparing private key..."
	
	if [ ! -f ${pendingdir}/${keyname} ]
	then		
		if [ -f ${certsdir}/${pemname} ]; then					
			cp ${certsdir}/${pemname}  ${pendingdir}/${keyname} -f
		  check_err
		  log_info "Copied existings private key."
		else		
			create_private_key ${pendingdir}/${keyname}
		fi
	fi
}

# Create pending csr with the pending private key
create_csr(){
	openssl req -new -key ${pendingdir}/${keyname} -out ${pendingdir}/${csrname} \
		-subj "${arg_subj}"
	check_err
	log_info "Created a new CSR."
}



