#!/bin/sh

# Include common
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source ${script_dir}/cert_common.sh $1


log_info "===== Install Certificate====="


# Verify private key
if [ -e ${pendingdir}/${csrname} ]; then
	log_info "Verifying private key..."
	openssl rsa -in ${pendingdir}/${keyname} -check -noout > /dev/null 2>&1
	retval=$?
	if [ $retval -ne 0 ] ; then
		 log_err "Failed to verify:${pendingdir}/${keyname}"
		 exit_with_status $retval
	fi
else
	log_err "${keyname} not found!"
	exit_with_status -1
fi


# Verify csr
if [ -e ${pendingdir}/${csrname} ]; then
	log_info "Verifying csr ..."
  openssl req -noout -verify -in ${pendingdir}/${csrname} > /dev/null 2>&1
  retval=$?
  if [ $retval -ne 0 ]; then
	  log_err "Failed to verify: ${pendingdir}/${csrname}"
		exit_with_status $retval
  fi
else
	log_err "${csrname} not found!"
	exit_with_status -1
fi



# Verify certificate against rootCA. Disabled temporiarily cause rootCA.pem is not available on RTU
#if [ -e ${pendingdir}/${crtname} ]; then
#	log_info "Verifying cert ..."
#  openssl verify -CAfile rootCA.pem ${pendingdir}/${crtname} > /dev/null 2>&1
#  retval=$?
#  if [ $retval -ne 0 ]; then
#  	log_err "Failed to verify: ${pendingdir}/${crtname}.err:$retval"
#  	exit_with_status $retval
#  fi
#fi


# Checking all files matched
log_info "Checking all certificate files' public key are matched..."
md5_0="$(openssl x509 -noout -modulus -in ${pendingdir}/${crtname}| openssl md5)"
md5_1="$(openssl rsa -noout -modulus -in ${pendingdir}/${keyname} | openssl md5)"
md5_2="$(openssl req -noout -modulus -in ${pendingdir}/${csrname} | openssl md5)"

if [ "${md5_0}" != "${md5_1}" ] || [ "${md5_0}" != "${md5_2}" ]; then
	log_err "Mismatched modulus of certificate files!"
	exit_with_status -1
fi


# Concatenate cert and private key to a pem file located in the 'live' folder
cat ${pendingdir}/${crtname} ${pendingdir}/${keyname} > ${certsdir}/${pemname}
retval=$?
if [ $retval -ne 0 ]; then
  	log_err "Failed to verify: ${pendingdir}/${crtname}"
  	exit_with_status $retval
fi

rm  ${pendingdir}/${crtname} ${pendingdir}/${keyname} ${pendingdir}/${csrname}
log_info "Installed certificate successfully."

exit_with_status $?

