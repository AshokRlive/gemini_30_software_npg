#!/bin/sh

# Include common
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source ${script_dir}/cert_common.sh $1

prepare_private_key
create_csr
exit_with_status $?


