#!/bin/sh

# Include common
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source ${script_dir}/cert_common.sh $1


# Clean log & status
rm ${logfile} -f -v
rm ${statusfile} -f -v
