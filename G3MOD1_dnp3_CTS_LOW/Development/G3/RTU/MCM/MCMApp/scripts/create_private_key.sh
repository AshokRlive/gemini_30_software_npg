#!/bin/sh

# Include common
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source ${script_dir}/cert_common.sh $1

create_private_key ${pendingdir}/${keyname}
exit_with_status $?


