#!/bin/bash
# Replaces NTP script with the one given in the parameter

DESTFOLDER="/usr/local/gemini/application/config/"
DEST="${DESTFOLDER}ntp.conf"
NTPCOMMAND="/etc/init.d/ntp"
RESULT=-2
SHOWHELP=0

# Check for help parameters. Note that the shell version of '[' seems to do not support '-o' (OR) parameter
#if [ -z $1 -o "$1" = "-?" -o "$1" = "?" -o "$1" = "--help" -o "$1" = "--help" ];
if [ -z $1 ];
then
        #No parameter given
        SHOWHELP=1
else
        if [ "$1" = "--?" -o "$1" = "-?" -o "$1" = "--help" -o "$1" = "-help" -o "$1" = "help" ];
        then
                SHOWHELP=1
        fi
fi
if [ $SHOWHELP -eq 1 ];
then
        echo "The $0 command handles configuration of the NTP script."
        echo " Usage: $0 [OPTION] [FILE]"
        echo "    When FILE is provided, it overwrites the current NTP configuration with the given one."
        echo "    WARNING: the original FILE is removed afterwards!!!"
        echo " Options:"
        echo "    --delete    Removes NTP configuration, disabling NTP functionality. "
        echo "                When this option is used, the FILE is ignored."
        echo "    --help      Shows this help."
        echo "                When this option is used, the FILE is ignored."
        echo " Example: $0 /tmp/myNTPConfig.txt"
        echo "            Removes current configuration, puts the given one in $DEST and applies the configuration by restarting the NTP service."
        echo " "
else
        if [ "$1" = "--delete" ];
        then
                # Remove config file
                rm -f $DEST
                $NTPCOMMAND stop
                RESULT=0
        else
                if [ -f $1 ];
                then
                        # It is a file and exists: proceed
                        if [ -d $DESTFOLDER ];
                        then
                                diff -E -B --text $DEST $1 > /dev/null
                                if [ $? -eq 0 ];
                                then
                                        echo "No need to overwrite $DEST"
                                        rm $1
                                        RESULT=0
                                else
                                        mv -f $1 $DEST
                                        if [ $? -ne 0 ];
                                        then
                                                echo "Error overwriting $DEST"
                                        else
                                                echo "configuration saved."
                                                $NTPCOMMAND restart
                                                RESULT=$?
                                        fi
                                fi
                        else
                                echo "The destination folder ${DESTFOLDER} does not exist."
                        fi
                else
                        # It is a folder or other: error
                        echo "Error - invalid option or file given: $1 "
                        exit -1
                fi
        fi

        if [ $RESULT -eq 0 ]
        then
                echo "Done successfully."
        else
                echo "Procedure FAILED!!"
        fi
        exit $RESULT
fi
