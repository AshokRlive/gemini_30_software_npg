#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/un.h>      // AF_UNIX
#include <netinet/in.h>  // AF_INET
#include <pthread.h>

#include "MonitorProtocol.h"
#include "QProtocolHandler.h"
#include "AppProtocolHandler.h"  // App Msg Format
#include "DataSockManager.h"

int main(void)
{
    lu_int32_t sockFd;
    lu_int32_t ret;

    int i, j;

    AppMsgStr outMsg;
    char buf[100];
    lu_int32_t optVal;
    struct sockaddr_un servAddr;


#if 0
    PLExitStr payLoad;
    payLoad.action = (lu_int8_t) XMSG_EXIT_ACTION_REBOOT;
    payLoad.reason = (lu_int8_t) XMSG_EXIT_CODE_OVERTEMP;
#endif

#if 1
    PLGetAIValStr payLoad;
    payLoad.channel = 1;
    payLoad.flags = 0xff;
    payLoad.value = 12.33;

    outMsg.header.stx = XMSG_STX;
    outMsg.header.appId = APP_ID_MCMPLUGIN1;
    outMsg.header.cmd = XMSG_CMD_GET_AI_VAL;
    outMsg.header.apiVersion.major = 1;
    outMsg.header.apiVersion.minor = 0;
    outMsg.header.crc32 = 0xffffffff;
    outMsg.header.payLoadLen = sizeof(payLoad);
    outMsg.payload = &payLoad;

#else
    buildRepAIMsg(&outMsg, XMSG_CMD_GET_AI_VAL, 1, 0xcc, (lu_float32_t) 77.5);
#endif

    sockFd = socket(AF_UNIX, SOCK_STREAM, 0);
    memset(&servAddr, 0, sizeof(struct sockaddr_un));
    servAddr.sun_family = AF_UNIX;
    strncpy(servAddr.sun_path, DATA_SOCKET_NAME, sizeof(servAddr.sun_path) - 1);

    if (connect(sockFd, (struct sockaddr *) &servAddr, sizeof(servAddr)) != 0)
    {
        perror("Error connecting!");
    }
    else
    {
        printf("CONNECTED!\n");

        while (1)
        {
            if (sendExtPacket(sockFd, &outMsg) > 0)
            {
                printf("Sent packet\n");
            }

            else
            {
                perror("Send FAILED errno!");
            }
#if 1
            printf("Receiving...\n");
            if ((ret = recvExtPacket(sockFd, buf, sizeof(buf), 100)) > 0)
            {
                printf("RX:[%d] bytes\n", ret);
#if 0
                for (j = 0; j < ret; j++)
                {
                    printf("%02X ", (unsigned char) buf[j]);
                }
                printf("\n");
#endif
            }
#endif
            usleep(50000);
        }
    }

    close(sockFd);

    return 0;
}
