/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCMMonitor
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Communicates with other threads over posix queues
 *
 *    CURRENT REVISION
 *
 *               $Revision::               $: (Revision of last commit)
 *               $Author::                 $: (Author of last commit)
 *       \date   $Date::                   $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   07/08/13      andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */



#ifdef __cplusplus
extern "C" {
#endif
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <mqueue.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"

#pragma pack(1)

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
// Message Queue Commands
#define QMSG_CMD_REP_OK            0x20   // OR with CMD to show the command was executed OK
#define QMSG_CMD_REP_ERROR         0x40   // OR with CMD to show there's an error
#define QMSG_CMD_ALARM             0x80   // OR with CMD to show ALARM

// Error Codes are returned if there's an error
#define QMSG_CMD_ERR_NONE          0x00 // No error
#define QMSG_CMD_ERR_NOT_SUPPORTED 0x01 // Command not supported
#define QMSG_CMD_ERR_OUT_OF_RANGE  0x02 // Supplied value out of range
#define QMSG_CMD_ERR_GENERAL       0x04 // General Error

// Commands
#define QMSG_CMD_REQ_PING      0x01
#define QMSG_CMD_REP_PING      (QMSG_CMD_REQ_PING      | QMSG_CMD_REP_OK)

#define QMSG_CMD_REQ_WDKICK    0x02
#define QMSG_CMD_REP_WDKICK    (QMSG_CMD_REQ_WDKICK    | QMSG_CMD_REP_OK)

#define QMSG_CMD_REQ_DI        0x03
#define QMSG_CMD_REP_DI        (QMSG_CMD_REQ_DI        | QMSG_CMD_REP_OK)

#define QMSG_CMD_REQ_AI        0x04
#define QMSG_CMD_REP_AI        (QMSG_CMD_REQ_AI        | QMSG_CMD_REP_OK)
//#define QMSG_CMD_ALM_AI        (QMSG_CMD_REQ_AI        | QMSG_CMD_ALARM)

#define QMSG_CMD_REQ_SET_DO    0x05
#define QMSG_CMD_REP_SET_DO    (QMSG_CMD_REQ_SET_DO    | QMSG_CMD_REP_OK)

#define QMSG_CMD_REQ_SET_DI    0x06
#define QMSG_CMD_REP_SET_DI    (QMSG_CMD_REQ_SET_DI    | QMSG_CMD_REP_OK)

#define QMSG_CMD_REQ_SET_OLR   0x07
#define QMSG_CMD_REP_SET_OLR   (QMSG_CMD_REQ_SET_OLR   | QMSG_CMD_REP_OK)

#define QMSG_CMD_REQ_SHUTDN    0x08
#define QMSG_CMD_REP_SHUTDN    (QMSG_CMD_REQ_SHUTDN    | QMSG_CMD_REP_OK)
#define QMSG_CMD_ALM_SHUTDN    (QMSG_CMD_REQ_SHUTDN    | QMSG_CMD_ALARM)

#define QMSG_CMD_REQ_RESTART   0x09
#define QMSG_CMD_REP_RESTART   (QMSG_CMD_REQ_RESTART   | QMSG_CMD_REP_OK)

#define QMSG_CMD_REQ_LEDSTATE  0x0A
#define QMSG_CMD_REP_LEDSTATE  (QMSG_CMD_REQ_LEDSTATE  | QMSG_CMD_REP_OK)

#define QMSG_CMD_REQ_STARTAPP  0x0B
#define QMSG_CMD_REP_STARTAPP  (QMSG_CMD_REQ_STARTAPP  | QMSG_CMD_REP_OK)

#define QMSG_CMD_REQ_STOPAPP   0x0C
#define QMSG_CMD_REP_STOPAPP   (QMSG_CMD_REQ_STOPAPP   | QMSG_CMD_REP_OK)

#define QMSG_CMD_REQ_POWERSAVE 0x0D
#define QMSG_CMD_REP_POWERSAVE (QMSG_CMD_REQ_POWERSAVE | QMSG_CMD_REP_OK)

#define QMSG_CMD_EVENT_ALL     0x0E   // IOManager to event all values

#define QMSG_CMD_HAT_MODE      0x0F   // Inform IOManager to stop driving GPIO

#define QMSG_CMD_REP_STATE     0x11   // Send a state change to the main thread
#define QMSG_CMD_NOTIFY_EXIT   0x12   // Notify AppManager of a request to exit from a client

// Command priority levels
#define QMSG_CMD_PRIORITY_HIGH         9
#define QMSG_CMD_PRIORITY_NORMAL       5
#define QMSG_CMD_PRIORITY_LOW          1

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/**
 * \brief Action codes for exiting/rebooting (same as XMSG version)
 */
typedef enum
{
    QMSG_EXIT_ACTION_SHUTDOWN = 0,    /* System shutdown */
    QMSG_EXIT_ACTION_REBOOT   = 1,    /* System Reboot */
    QMSG_EXIT_ACTION_RESTART  = 2,    /* Application termination/restart */
    QMSG_EXIT_ACTION_LAST
} QMSG_EXIT_ACTION;

/**
 * \brief Reason codes for exiting/rebooting (same as XMSG version)
 */
typedef enum
{
    QMSG_EXIT_CODE_OVERTEMP  = 0,      /* Temperature is getting too high */
    QMSG_EXIT_CODE_VOLTAGE_RANGE = 1,  /* Voltage out of range */
    QMSG_EXIT_CODE_WDOG_FAIL = 2,      /* Hardware watchdog stopped responding */
    QMSG_EXIT_CODE_APP_FAIL  = 3,      /* Application failure */
    QMSG_EXIT_CODE_LAST
} QMSG_EXIT_CODE;


// Data Type Union
typedef union qMsgDataDef
{
    lu_uint8_t    rawVal[4];
    lu_int32_t    intVal;
    lu_uint32_t   uIntVal;
    lu_int32_t    errVal;
    lu_float32_t  floatVal;
    lu_bool_t     boolVal;

} qMsgDataUnion;

// Message Structure
typedef struct QMsgDef
{
  lu_uint8_t    cmd;
  mqd_t         qSrc;
  mqd_t         qDest;
  lu_uint16_t   priority;

  // Data section
  qMsgDataUnion data1;
  qMsgDataUnion data2;
  qMsgDataUnion data3;

} QMsgType;

#define MSG_LEN (sizeof(QMsgType))

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Send a message
 *
 *   Sends a message using mq
 *
 *   \param msgPtr     Pointer to the message to send
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_int32_t sendQMsg(QMsgType *msgPtr);

/*!
 ******************************************************************************
 *   \brief Prepare and send an exit Msg
 *
 *   \param qSrc        fd of the message originator
 *          qDest       fd of the message recipient
 *          appId       Application ID of the sender application
 *          exitCode    The desired Exit Code
 *          timeoutMs   Time to wait for the exit code to arrive from the Application
 *
 *
 *   \return 0 on Success
 *
 ******************************************************************************
 */
lu_int32_t sendNotifyExitQMsg(mqd_t qSrc, mqd_t qDest, lu_uint8_t appId, lu_int32_t exitCode, lu_uint32_t timeoutMs);

/*!
 ******************************************************************************
 *   \brief Prepare and send an Event All Msg
 *
 *   \param qSrc        fd of the message originator
 *          qDest       fd of the message recipient
 *
 *
 *   \return 0 on Success
 *
 ******************************************************************************
 */
lu_int32_t sendEventAllQMsg(mqd_t qSrc, mqd_t qDest);

/*!
 ******************************************************************************
 *   \brief Prepare and send a HAT Mode Msg
 *
 *   \param qSrc        fd of the message originator
 *          qDest       fd of the message recipient
 *          enabled     true or false
 *
 *   \return 0 on Success
 *
 ******************************************************************************
 */
lu_int32_t sendHATModeQMsg(mqd_t qSrc, mqd_t qDest, lu_bool_t enabled);

/*!
 ******************************************************************************
 *   \brief Prepare and send a Power Save Msg
 *
 *   \param qSrc        fd of the message originator
 *          qDest       fd of the message recipient
 *          powerSave   1 = Power Save mode
 *                      0 = Not Power Save mode
 *
 *   \return 0 on Success
 *
 ******************************************************************************
 */
lu_int32_t sendReqPowerSaveQMsg(mqd_t qSrc, mqd_t qDest, lu_bool_t powerSave);

/*!
 ******************************************************************************
 *   \brief Prepare and send a reply to a Power Save Msg
 *
 *   \param qSrc        fd of the message originator
 *          qDest       fd of the message recipient
 *          powerSave   1 = Power Save mode
 *                      0 = Not Power Save mode
 *
 *   \return 0 on Success
 *
 ******************************************************************************
 */
lu_int32_t sendRepPowerSaveQMsg(mqd_t qSrc, mqd_t qDest, lu_bool_t powerSave);

/*!
 ******************************************************************************
 *   \brief Prepare and send a request to shutdown msg
 *
 *   \param qSrc        fd of the message originator
 *          qDest       fd of the message recipient
 *          action      What to do
 *          reason      Why do it
 *
 *   \return 0 on Success
 *
 ******************************************************************************
 */
lu_int32_t sendReqShutdownQMsg(mqd_t qSrc, mqd_t qDest, QMSG_EXIT_ACTION action, QMSG_EXIT_CODE reason);

/*!
 ******************************************************************************
 *   \brief Prepare and send a request to alarm shutdown msg
 *
 *   This is a HIGH PRIORITY version of sendReqPowerSaveQMsg()
 *
 *   \param qSrc        fd of the message originator
 *          qDest       fd of the message recipient
 *          action      What to do
 *          reason      Why to do it
 *
 *   \return 0 on Success
 *
 ******************************************************************************
 */
lu_int32_t sendReqAlarmShutdownQMsg(mqd_t qSrc, mqd_t qDest, QMSG_EXIT_ACTION action, QMSG_EXIT_CODE reason);

/*!
 ******************************************************************************
 *   \brief Prepare and send a request WDog Kick msg
 *
 *   \param qSrc        fd of the message originator
 *          qDest       fd of the message recipient
 *          kickWDog    Start or Stop kicking the WDOg
 *
 *   \return 0 on Success
 *
 ******************************************************************************
 */
lu_int32_t sendReqWDogKickQMsg(mqd_t qSrc, mqd_t qDest, lu_bool_t kickWDog);

/*!
 ******************************************************************************
 *   \brief Prepare and send a reply to a WDog Kick msg
 *
 *   \param qSrc        fd of the message originator
 *          qDest       fd of the message recipient
 *          wdogState   Current state of the dog kicking
 *
 *   \return 0 on Success
 *
 ******************************************************************************
 */
lu_int32_t sendRepWDogKickQMsg(mqd_t qSrc, mqd_t qDest, lu_bool_t wdogState);

#if 0
/*!
 ******************************************************************************
 *   \brief Prepare and send an AI Alarm msg
 *
 *   \param qSrc        fd of the message originator
 *          qDest       fd of the message recipient
 *          channel     Channel number
 *          alarmState  State of the channel
 *
 *   \return 0 on Success
 *
 ******************************************************************************
 */
lu_int32_t sendAIAlarmQMsg(mqd_t qSrc, mqd_t qDest, lu_int32_t channel, lu_int32_t alarmState, lu_float32_t value);
#endif
/*!
 ******************************************************************************
 *   \brief Prepare and send a request for AI value
 *
 *   \param qSrc        fd of the message originator
 *          qDest       fd of the message recipient
 *          channel     Channel number
 *
 *   \return 0 on Success
 *
 ******************************************************************************
 */
lu_int32_t sendReqDIQMsg(mqd_t qSrc, mqd_t qDest, lu_int32_t channel);

/*!
 ******************************************************************************
 *   \brief Prepare and send AI value
 *
 *   \param qSrc        fd of the message originator
 *          qDest       fd of the message recipient
 *          channel     Channel number
 *          value       Value of the channel
 *
 *   \return 0 on Success
 *
 ******************************************************************************
 */
lu_int32_t sendRepDIQMsg(mqd_t qSrc, mqd_t qDest, lu_int32_t channel, lu_uint8_t flags, lu_bool_t value);
/*!
 ******************************************************************************
 *   \brief Prepare and send a request for AI value
 *
 *   \param qSrc        fd of the message originator
 *          qDest       fd of the message recipient
 *          channel     Channel number
 *
 *   \return 0 on Success
 *
 ******************************************************************************
 */
lu_int32_t sendReqAIQMsg(mqd_t qSrc, mqd_t qDest, lu_int32_t channel);

/*!
 ******************************************************************************
 *   \brief Prepare and send AI value
 *
 *   \param qSrc        fd of the message originator
 *          qDest       fd of the message recipient
 *          channel     Channel number
 *          value       Value of the channel
 *
 *   \return 0 on Success
 *
 ******************************************************************************
 */
lu_int32_t sendRepAIQMsg(mqd_t qSrc, mqd_t qDest, lu_int32_t channel, lu_uint8_t flags, lu_float32_t value);

/*!
 ******************************************************************************
 *   \brief Prepare and send a request to set a DO value
 *
 *   \param qSrc        fd of the message originator
 *          qDest       fd of the message recipient
 *          channel     Channel number
 *          value       Value to output
 *
 *   \return 0 on Success
 *
 ******************************************************************************
 */
lu_int32_t sendReqSetDOQMsg(mqd_t qSrc, mqd_t qDest, lu_int32_t channel, lu_bool_t value);

/*!
 ******************************************************************************
 *   \brief Prepare and send a DO reply value
 *
 *   \param qSrc        fd of the message originator
 *          qDest       fd of the message recipient
 *          channel     Channel number
 *          value       Value that was output
 *
 *   \return 0 on Success
 *
 ******************************************************************************
 */
lu_int32_t sendRepSetDOQMsg(mqd_t qSrc, mqd_t qDest, lu_int32_t channel, lu_bool_t value);

/*!
 ******************************************************************************
 *   \brief Prepare and send a request to set DI value
 *
 *   \param qSrc        fd of the message originator
 *          qDest       fd of the message recipient
 *          channel     Channel number
 *          value       Value that was output
 *
 *   \return 0 on Success
 *
 ******************************************************************************
 */
lu_int32_t sendReqSetDIQMsg(mqd_t qSrc, mqd_t qDest, lu_int32_t channel, lu_bool_t value);

/*!
 ******************************************************************************
 *   \brief Prepare and send a DI reply value
 *
 *   \param qSrc        fd of the message originator
 *          qDest       fd of the message recipient
 *          channel     Channel number
 *          value       Value that was output
 *
 *   \return 0 on Success
 *
 ******************************************************************************
 */
lu_int32_t sendRepSetDIQMsg(mqd_t qSrc, mqd_t qDest, lu_int32_t channel, lu_bool_t value);

/*!
 ******************************************************************************
 *   \brief Prepare and send a request to set OLR
 *
 *   \param qSrc        fd of the message originator
 *          qDest       fd of the message recipient
 *          OLRState    OLR State required
 *
 *   \return 0 on Success
 *
 ******************************************************************************
 */
lu_int32_t sendReqSetOLRQMsg(mqd_t qSrc, mqd_t qDest, lu_uint8_t OLRState);

 /*!
 ******************************************************************************
 *   \brief Prepare and send a reply to a set OLR
 *
 *   \param qSrc        fd of the message originator
 *          qDest       fd of the message recipient
 *          OLRState    OLR State set
 *
 *   \return 0 on Success
 *
 ******************************************************************************
 */
lu_int32_t sendRepSetOLRQMsg(mqd_t qSrc, mqd_t qDest, lu_uint8_t OLRState);


/*!
 ******************************************************************************
 *   \brief Prepare and send a request to Stop the monitored Application
 *
 *   \param qSrc        fd of the message originator
 *          qDest       fd of the message recipient
 *
 *   \return 0 on Success
 *
 ******************************************************************************
 */
lu_int32_t sendReqStopAppQMsg(mqd_t qSrc, mqd_t qDest);

/*!
 ******************************************************************************
 *   \brief Prepare and send a reply to Stop the monitored Application
 *
 *   \param qSrc        fd of the message originator
 *          qDest       fd of the message recipient
 *          appId       Application ID
 *
 *   \return 0 on Success
 *
 ******************************************************************************
 */
lu_int32_t sendRepStopAppQMsg(mqd_t qSrc, mqd_t qDest, lu_uint8_t appId);

/*!
 ******************************************************************************
 *   \brief Prepare and send a request to change the OK LED state
 *
 *   \param qSrc        fd of the message originator
 *          qDest       fd of the message recipient
 *          state       State of the LED
 *
 *   \return 0 on Success
 *
 ******************************************************************************
 */
lu_int32_t sendReqLEDStateQMsg(mqd_t qSrc, mqd_t qDest, lu_int32_t state);


/*!
 ******************************************************************************
 *   \brief Prepare and send an OK LED state reply
 *
 *   \param qSrc        fd of the message originator
 *          qDest       fd of the message recipient
 *          state       State of the LED
 *
 *   \return 0 on Success
 *
 ******************************************************************************
 */
lu_int32_t sendRepLEDStateQMsg(mqd_t qSrc, mqd_t qDest, lu_int32_t state);

/*!
 ******************************************************************************
 *   \brief Prepare and send a Thread State Change msg
 *
 *   \param qSrc        fd of the message originator
 *          qDest       fd of the message recipient
 *          threadID    ID of the thread
 *          state       State of the thread
 *
 *   \return 0 on Success
 *
 ******************************************************************************
 */
lu_int32_t sendRepStateQMsg(mqd_t qSrc, mqd_t qDest, lu_int32_t threadID, lu_int32_t state);

/*!
 ******************************************************************************
 *   \brief Prepare and send an error msg
 *
 *   \param qSrc        fd of the message originator
 *          qDest       fd of the message recipient
 *          cmd         Command that generated the error
 *          errorCode   Error Code
 *
 *   \return 0 on Success
 *
 ******************************************************************************
 */
lu_int32_t sendErrorQMsg(mqd_t qSrc, mqd_t qDest, lu_uint8_t cmd, lu_int32_t errorCode);

#pragma pack()

#ifdef __cplusplus
}
#endif


/*
 *********************** End of file ******************************************
 */
