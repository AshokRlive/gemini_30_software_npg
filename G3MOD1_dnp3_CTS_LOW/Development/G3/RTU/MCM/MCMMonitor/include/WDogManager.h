/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCMMonitor
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Communicate with the Watchdog PIC
 *
 *    CURRENT REVISION
 *
 *               $Revision::               $: (Revision of last commit)
 *               $Author::                 $: (Author of last commit)
 *       \date   $Date::                   $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24/07/13      andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#if !defined(EA_D843F8DE_A0DF_48fa_96D3_A04DC63B8D09__INCLUDED_)
#define EA_D843F8DE_A0DF_48fa_96D3_A04DC63B8D09__INCLUDED_


#ifdef __cplusplus
extern "C" {
#endif
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define WDOG_SYS_UNHEALTHY       0
#define WDOG_SYS_HEALTHY         1
#define WDOG_HEALTHY_CHECK_MS    2000   // Time between SYS HEALTHY checks
#define WDOG_MAX_UNHEALTHY_READS 3     // Max number of SYS UNHEALTHY reads before rebooting

#define WDOG_KICK_LOW            0
#define WDOG_KICK_HIGH           1

#define WDOG_KICKBACK_LOW        WDOG_KICK_LOW
#define WDOG_KICKBACK_HIGH       WDOG_KICK_HIGH

#define WDOG_KICK_1HZ_MS         1000    // Delay between regular WDOG KICKS
#define WDOG_KICK_10HZ_MS        100     // Delay between rwdog reset WDOG KICKS
#define WDOG_KICK_TIMEOUT_MS     10000   // Timeout on WDog KICKBACK

#define WDOG_KICKBACK_MIN_READINGS 3     // Number of consecutive readings required to detect a kickback/wdog reset

#define WDOG_STOP_KICKING        LU_FALSE
#define WDOG_START_KICKING       LU_TRUE

#define WDOG_FAILED_FILE         "/usr/local/gemini/application/wdog.fail"
/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Thread's Main
 *
 *    *
 *   \param mainMqFdVoidPtr Main thread's file descriptor
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void *wdogThread(void *mainMqFdVoidPtr);

/*!
 ******************************************************************************
 *   \brief     Check if the Watchdog Fail file exists (and deletes it)
 *
 *   \param     None
 *
 *   \return    true/false
 *
 ******************************************************************************
 */
//lu_bool_t wDogWdogFailExists(void);


/*!
 ******************************************************************************
 *   \brief     Delete the file that indicates the Watchdog Sys Healthy went Low
 *
 *   \param     None
 *
 *   \return    None
 *
 ******************************************************************************
 */
//void wDogDeleteWdogFailFile(void);

#ifdef __cplusplus
}
#endif


#endif /*!defined(EA_D843F8DE_A0DF_48fa_96D3_A04DC63B8D09__INCLUDED_)*/

/*
 *********************** End of file ******************************************
 */
