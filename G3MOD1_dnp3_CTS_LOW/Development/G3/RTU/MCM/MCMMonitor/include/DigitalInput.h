/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCMMonitor
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Digital Input
 *
 *    CURRENT REVISION
 *
 *               $Revision::               $: (Revision of last commit)
 *               $Author::                 $: (Author of last commit)
 *       \date   $Date::                   $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25/07/13      andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#if !defined(DIGITALINPUT_H__INCLUDED_)
#define DIGITALINPUT_H__INCLUDED_

#ifdef __cplusplus
extern "C" {
#endif

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdio.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "Main.h"
#include "MCMIOMap.h"
#include "IO.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define MAX_DI_READ_RETRIES       3       // Number of tries to read digital input file

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
// Forward declaration
struct DigitalInputDef;

// Digital Input Configuration
typedef struct DigitalInputConfigDef
{
    lu_uint8_t   gpioNum;

    lu_uint32_t  scanIntervalMs;        // mS scan interval

    lu_bool_t    invert;
    lu_bool_t    notifyOnChange;        // True to send a notification to external App on change

    IO_ERROR     (*update_func)(struct DigitalInputDef *); // Function used to update this point (if virtual)

} MCMMonDIConfigStr;

// Define a handy structure for holding Digital Inputs
typedef struct DigitalInputDef
{
    lu_uint8_t      channel;
    FILE            *fd;

    MCMMonDIConfigStr *config;

    lu_bool_t       rawValue;
    lu_bool_t       value;      // Value after any invert has been applied
    lu_bool_t       lastValue;

    struct timespec nextScanTime;
    lu_uint32_t     currentScanIntervalMs;   // current scan interval (allows for dynamic rate changes)

    IO_FLAG         flags;    // Status Flags, OFFLINE etc
    IO_FLAG         lastFlags;

    lu_uint8_t      retries;  // Number of tries to retrieve value (MAX_AI_READ_RETRIES)

} DigitalInputType;



/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Closes the value file associated with the Digital Input
 *
 *   Detailed description
 *
 *   \param digitalInput structure defining the required input
 *
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
IO_ERROR digitalInputClose(DigitalInputType *digitalInputPtr);


/*!
 ******************************************************************************
 *   \brief Initialise digital input structure
 *
 *   Initialises a digital input structure
 *
 *   \param digitalInput  pointer to digital input to initialise
 *          address       address of the digitalInput
 *
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
IO_ERROR digitalInputInit(lu_uint8_t address, DigitalInputType* digitalInputPtr, MCMMonDIConfigStr *configPtr);


/*!
 ******************************************************************************
 *   \brief Updates a Digital Input's Value
 *
 *   \param digitalInput  pointer to digital input to initialise
 *          newValue      value to apply to Digital Input
 *
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
IO_ERROR digitalInputSetValue(DigitalInputType* digitalInputPtr, lu_bool_t newValue);


/*!
 ******************************************************************************
 *   \brief Updates a Digital Input's RawValue
 *
 *   Inverts as necessary and copies to Value
 *
 *   Initialises a digital input structure
 *
 *   \param digitalInput  pointer to digital input to initialise
 *          newRawValue   rawvalue to apply to Digital Input
 *
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
IO_ERROR digitalInputSetRawValue(DigitalInputType* digitalInputPtr, lu_bool_t newRawValue);


/*!
 ******************************************************************************
 *   \brief Update a GPIO digital input
 *
 *   This updates a digital input whose value comes from a GPIO
 *   NOTE: This function MUST be protected by a mutex as it write to both
 *   Value and Flags.
 *
 *   \param digitalInputPtr  pointer to digital input to initialise
 *
 *
 *   \return Error Code
 *
 ******************************************************************************
 */

IO_ERROR digitalInputUpdate(DigitalInputType *digitalInputPtr);

#ifdef __cplusplus
}
#endif


#endif /*!defined(DIGITALINPUT_H__INCLUDED_)*/
 

/*
 *********************** End of file ******************************************
 */
