/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCMMonitor
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Application Manager
 *
 *    CURRENT REVISION
 *
 *               $Revision::               $: (Revision of last commit)
 *               $Author::                 $: (Author of last commit)
 *       \date   $Date::                   $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24/07/13      andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#if !defined(EA_3FBB4F99_C799_4519_A032_DE32A19552A4__INCLUDED_)
#define EA_3FBB4F99_C799_4519_A032_DE32A19552A4__INCLUDED_


#ifdef __cplusplus
extern "C" {
#endif

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <sys/stat.h>  // chmod() & MODE constants


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Main.h"
#include "MonitorProtocol.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define NO_PID                       -2      /* -1 is a special pid */
#define CMDLINEBUFF_SIZE             255     /* Max size of the cmdline buffer */
#define STARTCOUNTER_RESET_TIME_MS   1800000 /* If process runs for 30 minutes reset the start counter */

#define APPMAN_STOP_APP              LU_FALSE
#define APPMAN_START_APP             LU_TRUE

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
typedef struct ProcessDef
{
    lu_char_t        **processName;          // Process Name + Arg List
    lu_char_t          cmdlineBuf[CMDLINEBUFF_SIZE]; // Buff to compare with /proc/<pid>/cmdline
    lu_char_t          cmdlineBufLen;        // Number of bytes in cmdlineBuf

    lu_bool_t          startRequest;         // Flag to determine if this process should be started
    lu_bool_t          started;              // Flag to determine if this process has been started
    pid_t              pid;                  // > 0 indicates the process is running
    lu_uint16_t        startCounter;         // #times this process returned exit code != 0 in succession (failed to run)
    struct timespec    startCounterResetTime;
    APP_EXIT_CODE      startCode;            // This ProcessDef is run if the monitored application returns this EXIT_CODE
    lu_int32_t         notifiedExitCode;     // Intended exit code sent by the process
    struct timespec    exitTimeout;          // Time by which the notifiedExitCode should have been received (process gets killed)
    lu_bool_t          killed;               // Identifies if the AppManager has attempted to kill the process
    lu_int32_t         exitCode;             // Exit code returned by monitored process
    pthread_t          tid;

    struct ProcessDef *next;                 // Pointer to the next process

} ProcessType;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

extern const lu_char_t MonitorWorkingPath[];
extern const lu_char_t MCMApplicationPath[];

extern const lu_char_t *MCMApplication[];
extern const lu_char_t *MCMUpdater[];
extern const lu_char_t *MCMUpdaterBackup[];
extern const lu_char_t *MCMUpdaterHAT[];


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Initialise the AppManager
 *
 *   This should be called from the main thread to open a message queue for the
 *   AppManager.  This MUST be called before forking() and running
 *   appManagerThread()
 * *
 *   \param
 *
 *
 *   \return    mqd_t   Message Queue descriptor
 *
 ******************************************************************************
 */
mqd_t appManagerInit(void);


/*!
 ******************************************************************************
 *   \brief Thread's Main
 *
 *
 *   \param mainMqFdVoidPtr Main thread's file descriptor
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void *appManagerThread(void *dummy);


#ifdef __cplusplus
}
#endif


#endif /*!defined(EA_3FBB4F99_C799_4519_A032_DE32A19552A4__INCLUDED_)*/
 
/*
 *********************** End of file ******************************************
 */
