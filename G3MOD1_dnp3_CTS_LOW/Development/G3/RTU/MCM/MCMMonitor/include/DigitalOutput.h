/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCMMonitor
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Digital Input
 *
 *    CURRENT REVISION
 *
 *               $Revision::               $: (Revision of last commit)
 *               $Author::                 $: (Author of last commit)
 *       \date   $Date::                   $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25/07/13      andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#if !defined(DIGITALOUTPUT_H__INCLUDED_)
#define DIGITALOUTPUT_H__INCLUDED_

#ifdef __cplusplus
extern "C" {
#endif

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdio.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "Main.h"
#include "MCMIOMap.h"
#include "IO.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define MAX_DO_WRITE_RETRIES       3   // Number of tries to write to digital output file
/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

// Digital Input Configuration
typedef struct DigitalOutputConfigDef
{
    lu_uint8_t   gpioNum;

} MCMMonDOConfigStr;

// Define a handy structure for holding Digital Inputs
typedef struct DigitalOutputDef
{
    lu_uint8_t      channel;
    FILE            *fd;

    MCMMonDOConfigStr *config;

    lu_bool_t       value;
    lu_bool_t       lastValue;

    IO_FLAG         flags;    // Status Flags, OFFLINE etc

} DigitalOutputType;



/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Initialise digital input structure
 *
 *   Initialises a digital output structure
 *
 *   \param digitalOutput pointer to digital output to initialise
 *          address       address of the digitalOutput
 *          config        configuration of the digital output
 *
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
IO_ERROR digitalOutputInit(lu_uint8_t address, DigitalOutputType* digitalOutput, MCMMonDOConfigStr *config);


/*!
 ******************************************************************************
 *   \brief Closes the value file associated with the Digital Output
 *
 *   Detailed description
 *
 *   \param digitalOutput structure defining the required output
 *
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
IO_ERROR digitalOutputClose(DigitalOutputType *digitalOutput);


/*!
 ******************************************************************************
 *   \brief Sets a digital output
 *
 *   \param digitalOutput structure defining the required Output
 *          value        value to set the output to
 *
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
IO_ERROR digitalOutputSet(DigitalOutputType* digitalOutput, lu_bool_t value);


#ifdef __cplusplus
}
#endif


#endif /*!defined(DIGITALOUTPUT_H__INCLUDED_)*/
 

/*
 *********************** End of file ******************************************
 */
