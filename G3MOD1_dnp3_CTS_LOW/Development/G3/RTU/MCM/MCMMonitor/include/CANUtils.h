/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:CanUtils.c
 *    FILE TYPE: c header
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14 Nov 2013     andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef CANUTILS_H_
#define CANUTILS_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define CAN0 "can0"
#define CAN1 "can1"

#define CAN0_BUS_SPEED 500000
#define CAN1_BUS_SPEED 500000


typedef enum
{
    CANBUS_0    = 0,
    CANBUS_1    = 1,
    CANBUS_LAST
} CANBUS;

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief     Create a CAN socket and bind it to the supplied interface
 *
 *   \param     iface   Interface name (e.g. "can0")
 *
 *   \return    socket handle or -1
 *
 ******************************************************************************
 */
lu_int32_t CANCreateSocket(const char *iface);

/*!
 ******************************************************************************
 *   \brief     Get the up/down status of the interface
 *
 *   \param     socketFd    Socket handle from CANCreateSocket
 *              iFacePtr    Interface name (e.g. "can0")
 *              statePtr    State of the interface (TRUE = UP, else FALSE)
 *
 *   \return    0 on success else -1
 *
 ******************************************************************************
 */
lu_int32_t CANGetIFaceState(lu_int32_t socketFd, const char *iFacePtr, lu_bool_t *statePtr);

/*!
 ******************************************************************************
 *   \brief     Set the up/down status of the interface
 *
 *   \param     socketFd    Socket handle from CANCreateSocket
 *              iFacePtr    Interface name (e.g. "can0")
 *              state       State of the interface (TRUE = UP, else FALSE)
 *
 *   \return    0 if the interface has successfully changed state else -1
 *
 ******************************************************************************
 */
lu_int32_t CANSetIFaceState(lu_int32_t socketFd, const char *iFacePtr, lu_bool_t state);

/*!
 ******************************************************************************
 *   \brief     Close a CAN socket
 *
 *   \param     socketFd    Socket handle from CANCreateSocket
 *
 *   \return    N/A
 *
 ******************************************************************************
 */
void CANCloseSocket(lu_int32_t socketFd);

IO_ERROR CAN0UpdateAITxBytes(AnalogueInputType *AIPointPtr);
IO_ERROR CAN0UpdateAIRxBytes(AnalogueInputType *AIPointPtr);
IO_ERROR CAN0UpdateAIUsage(AnalogueInputType *AIPointPtr);
IO_ERROR CAN0UpdateAITxErrs(AnalogueInputType *AIPointPtr);
IO_ERROR CAN0UpdateAIRxErrs(AnalogueInputType *AIPointPtr);
IO_ERROR CAN0UpdateAIErrs(AnalogueInputType *AIPointPtr);
IO_ERROR CAN0UpdateAITxPack(AnalogueInputType *AIPointPtr);
IO_ERROR CAN0UpdateAIRxPack(AnalogueInputType *AIPointPtr);
IO_ERROR CAN0UpdateAIRxDropped(AnalogueInputType *AIPointPtr);
IO_ERROR CAN0UpdateAIRxOverErr(AnalogueInputType *AIPointPtr);

IO_ERROR CAN1UpdateAITxBytes(AnalogueInputType *AIPointPtr);
IO_ERROR CAN1UpdateAIRxBytes(AnalogueInputType *AIPointPtr);
IO_ERROR CAN1UpdateAIUsage(AnalogueInputType *AIPointPtr);
IO_ERROR CAN1UpdateAITxErrs(AnalogueInputType *AIPointPtr);
IO_ERROR CAN1UpdateAIRxErrs(AnalogueInputType *AIPointPtr);
IO_ERROR CAN1UpdateAIErrs(AnalogueInputType *AIPointPtr);
IO_ERROR CAN1UpdateAITxPack(AnalogueInputType *AIPointPtr);
IO_ERROR CAN1UpdateAIRxPack(AnalogueInputType *AIPointPtr);
IO_ERROR CAN1UpdateAIRxDropped(AnalogueInputType *AIPointPtr);
IO_ERROR CAN1UpdateAIRxOverErr(AnalogueInputType *AIPointPtr);


#ifdef __cplusplus
}
#endif

#endif /* CANUTILS_H_ */

/*
 *********************** End of file ******************************************
 */
