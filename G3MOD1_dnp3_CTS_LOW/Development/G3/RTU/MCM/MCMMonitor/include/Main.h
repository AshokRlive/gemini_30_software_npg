/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCMMonitor
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Main header file for the MCMMonitor
 *
 *    CURRENT REVISION
 *
 *               $Revision::               $: (Revision of last commit)
 *               $Author::                 $: (Author of last commit)
 *       \date   $Date::                   $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24/07/13      andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#if !defined(EA_CE5014E1_1E31_4d26_A02D_9C56EA14C9DD__INCLUDED_)
#define EA_CE5014E1_1E31_4d26_A02D_9C56EA14C9DD__INCLUDED_

#ifdef __cplusplus
extern "C" {
#endif

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <signal.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "MonitorProtocol.h"
#include "QProtocolHandler.h"
#include "LogLevelDef.h"
#include "MCMIOMap.h"


//#ifndef EMC_TEST
//#define EMC_TEST
//#endif

#define LINUX_CAPABILITES 0


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
// Exit Codes
#define EXIT_SUCCESS               0
#define EXIT_FAILURE               1

#define OS_WDOG_DELAY_ON           500
#define OS_WDOG_DELAY_OFF          500

// The default STACK SIZE for a thread is 8MB.  This makes the Virtual Memory for
// the MCMMonitor 50MB!!  The Threads are not memory hungry so this has been reduced significantly.
#define THREAD_STACK_SIZE     (1024 * 512) // 512K
#define THREAD_LOW_PRIORITY   10
#define THREAD_HIGH_PRIORITY  80

#define MAX_MQ_NAME_SIZE      20

#define CHMOD_755           S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH

// Used to set the thread state
#define SET_STATE(current, new) ((current) = ((new > current) ? new : current));

#define CMD_CREATE_CERT      "/sbin/lucy_create_cert"  // Create new HTTPS certificate
#define CMD_SET_IP_STRING    "/sbin/setip -v -t %s -i %s -n %s -g %s"
#define CMD_RESTART_HTTP     "/etc/init.d/lighttpd restart"
#define DEFAULT_NET_INTFACE  "eth0"

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

// Thread Info array index
typedef enum
{
  THREAD_MAIN         = 0,
  THREAD_LED          = 1,
  THREAD_WDOG         = 2,
  THREAD_IOMAN        = 3,
  THREAD_APPMAN       = 4,
  THREAD_DATA_COMS    = 5,
  THREAD_APP          = 6,
  THREAD_LAST

} THREAD_ID;

typedef struct ThreadInfoDef
{
  lu_bool_t   realThread;    // real thread or a pseudo entry
  pthread_t   threadId;
  const char  mqName[MAX_MQ_NAME_SIZE];
  mqd_t       threadMqFd;
  OK_LEDSTATE threadState;   // Contributes toward the main MCM LED State
} ThreadInfoType;



/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
extern ThreadInfoType threadInfo[THREAD_LAST];
extern lu_bool_t Closedown;
extern lu_bool_t ArgIgnoreWatchdog;
extern lu_bool_t ArgNoStop;
extern lu_bool_t HATMode;

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Connect to an existing message equeue
 *
 *   Sends a message using mq
 *
 *   \param     namePtr     - Ptr to the queue's name
 *
 *   \return a mq file descriptor
 *
 ******************************************************************************
 */
mqd_t connectMsgQueue(const lu_int8_t *namePtr);

/*!
 ******************************************************************************
 *   \brief Process the internal message queue
 *
 *   Standardises the internal message queue handling.  This function will wait
 *   the defined timeoutMs number of Ms, repeatedly select()ing on the mq until
 *   the time is up.  It can be used as a loop sleep()
 *
 *   \param  mqFd 		- Descriptor for the thread's msg queue
 *   		 decodeQMsq - Ptr to function that handles incoming msgs
 *
 *
 *   \return a mq file descriptor
 *
 ******************************************************************************
 */
lu_int32_t processMsgQueue(mqd_t mqFd, void (*decodeQMsq)(QMsgType), lu_int32_t timeoutMs);

/*!
 ******************************************************************************
 *   \brief Initialise signal masks
 *
 *   \param
 *
 *
 *   \return    sigset_t - Previous signal mask set
 *
 ******************************************************************************
 */
sigset_t initSignalMasks(void);

/*!
 ******************************************************************************
 *   \brief Initialise thread stack size
 *
 *   \param threadAttrPtr   Pointer to a pthread_attr_t variable
 *          stackSize       Stack Size for thread, in bytes
 *          detached        True if thread is to be detached
 *          priority        Posix priority (Linux 0 [LOW] - 99 [HIGH])
 *
 *   \return
 *
 ******************************************************************************
 */
void initThreadAttributes(pthread_attr_t *threadAttrPtr, size_t stackSize, lu_bool_t detached, lu_uint8_t priority);


#ifdef __cplusplus
}
#endif


#endif /*!defined(EA_CE5014E1_1E31_4d26_A02D_9C56EA14C9DD__INCLUDED_)*/

/*
 *********************** End of file ******************************************
 */
