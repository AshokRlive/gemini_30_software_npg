/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCMMonitor
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Analogue Input
 *
 *    CURRENT REVISION
 *
 *               $Revision::               $: (Revision of last commit)
 *               $Author::                 $: (Author of last commit)
 *       \date   $Date::                   $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25/07/13      andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#if !defined(EA_720C325F_AFD2_4e9d_BD1A_4A2AE1B89563__INCLUDED_)
#define EA_720C325F_AFD2_4e9d_BD1A_4A2AE1B89563__INCLUDED_

#ifdef __cplusplus
extern "C" {
#endif

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdio.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "Main.h"
#include "MCMIOMap.h"
#include "IO.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define NUM_FILTERED_READINGS     5
#define FILTERED_READING_POS      3      // In the middle of the readings
#define MAX_AI_READ_RETRIES       3      // Number of tries to read analogue input file

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef enum
{
	AI_STATE_INITIALISING = 0,
	AI_STATE_HEALTHY      = 1,
	AI_STATE_ALARM        = 2,
	AI_STATE_INVALID      = 3,
	AI_STATE_LAST

} AI_ALARM_STATE;

// Define when the AI is in alarm condition (within the AlarmLimits or outside them)
typedef enum
{
    AI_ALARM_MODE_WITHIN_LIMITS  = 0,
    AI_ALARM_MODE_OUTSIDE_LIMITS = 1,
    AI_ALARM_MODE_NONE           = 2,
    AI_ALARM_MODE_LAST

} AI_ALARM_MODE;

// Forward declaration
struct AnalogueInputDef;

// Analogue Input Configuration
typedef struct AnalogueInputConfigDef
{
    lu_char_t    deviceId[MCMANALOGUEINPUT_DEVICE_PATH_LEN];    // e.g. mx25-tscadc/
    lu_char_t    deviceName[MCMANALOGUEINPUT_DEVICE_NAME_LEN];  // e.g. vLogic

    lu_uint32_t  scanIntervalMs;          // configured scan interval

    // these need to be signed but also need to store U32 numbers!
    lu_int64_t   rawZero;
    lu_int64_t   rawFull;

    lu_float32_t scaledZero;
    lu_float32_t scaledFull;

    lu_float32_t scaledAlarmLevelLow;
    lu_float32_t scaledAlarmLevelHigh;

    IO_ERROR     (*alarm_func) (struct AnalogueInputDef *); // Function used to alarm this point
    IO_ERROR     (*update_func)(struct AnalogueInputDef *); // Function used to update this point (if virtual)

} MCMMonAIConfigStr;

// Define a handy structure for holding Analogue Inputs
typedef struct AnalogueInputDef
{
    lu_uint8_t      channel;
    FILE            *fd;

    MCMMonAIConfigStr *config;

    lu_int64_t      rawValue;
    lu_float32_t    value;

    struct timespec nextScanTime;
    lu_uint32_t     currentScanIntervalMs;   // current scan interval (allows for dynamic rate changes)

    IO_FLAG         flags;    // Status Flags, OFFLINE etc
    lu_uint8_t      retries;  // Number of tries to retrieve value (MAX_AI_READ_RETRIES)

#ifdef AI_FILTERING
    lu_uint8_t      filterPos;
    lu_bool_t       filterReady;
    lu_float32_t    values[NUM_FILTERED_READINGS];
#endif

} AnalogueInputType;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Initialise analogue input structure
 *
 *   Initialises an analogue input structure
 *
 *   \param analogueInputPtr pointer to analogue input to initialise
 *          address          address of the analogueInput
 *
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
IO_ERROR analogueInputInit(lu_uint8_t address, AnalogueInputType* analogueInputPtr, MCMMonAIConfigStr *configPtr);


/*!
 ******************************************************************************
 *   \brief Closes the value file associated with the Analogue Input
 *
 *   Detailed description
 *
 *   \param analogueInputPtr structure defining the required input
 *
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
IO_ERROR analogueInputClose(AnalogueInputType *analogueInputPtr);


/*!
 ******************************************************************************
 *   \brief Takes a fresh reading for the supplied input
 *
 *   Takes a reading and enters it in to the filter array, sorts the filter
 *   array
 *
 *   \param analogueInputPtr structure defining the required input
 *
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
IO_ERROR analogueInputUpdate(AnalogueInputType* analogueInputPtr);


/*!
 ******************************************************************************
 *   \brief Alarm Function for Analogue Input
 *
 *   This is called from analogueInputUpdate() to mark an AI as being in alarm
 *   or not.
 *   This function will mark an AI as in Alarm if it's value is outside
 *   of the scaledAlarmLevelLow & scaledAlarmLevelHigh limits set in the
 *   config.
 *
 *   \param analogueInputPtr structure defining the required input
 *
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
IO_ERROR analogueInputAlarmOutLimits(AnalogueInputType *AIPointPtr);


/*!
 ******************************************************************************
 *   \brief Alarm Function for Analogue Input
 *
 *   This is called from analogueInputUpdate() to mark an AI as being in alarm
 *   or not.
 *   This function will mark an AI as in Alarm if it's value is inside
 *   of the scaledAlarmLevelLow & scaledAlarmLevelHigh limits set in the
 *   config.
 *
 *   \param analogueInputPtr structure defining the required input
 *
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
IO_ERROR analogueInputAlarmInLimits(AnalogueInputType *AIPointPtr);

#ifdef __cplusplus
}
#endif


#endif /*!defined(EA_720C325F_AFD2_4e9d_BD1A_4A2AE1B89563__INCLUDED_)*/
 

/*
 *********************** End of file ******************************************
 */
