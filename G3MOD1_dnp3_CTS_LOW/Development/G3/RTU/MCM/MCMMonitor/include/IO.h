/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCMMonitor
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Digital Input
 *
 *    CURRENT REVISION
 *
 *               $Revision::               $: (Revision of last commit)
 *               $Author::                 $: (Author of last commit)
 *       \date   $Date::                   $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25/07/13      andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#if !defined(IO_H__INCLUDED_)
#define IO_H__INCLUDED_

#ifdef __cplusplus
extern "C" {
#endif

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef enum
{
    IO_ERROR_NONE    = 0,
    IO_ERROR_NULL    = 1,
    IO_ERROR_CONFIG  = 2,
    IO_ERROR_READ    = 3,
    IO_ERROR_WRITE   = 4,
    IO_ERROR_RANGE   = 5,
    IO_ERROR_GENERAL = 6,
    IO_ERROR_LAST

} IO_ERROR;

typedef enum
{
    IO_FLAG_NONE         = 0x00,    /* Clear ALL Flags */
    IO_FLAG_ALARM        = 0x01,    /* Point is in ALARM */
    IO_FLAG_OFF_LINE     = 0x02,    /* Point off-line / not readable */
    IO_FLAG_OUT_OF_RANGE = 0x04,    /* Value out of range */
    IO_FLAG_FILTERED     = 0x08,    /* Value filtered */
    IO_FLAG_INVALID      = 0x10,    /* Channel is INVALID */
    IO_FLAG_RESCANNED    = 0x20,    /* This channel has been rescanned - e.g. vLogic change required rescan of vLocal/vRemote */
    IO_FLAG_LAST

} IO_FLAG;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

#ifdef __cplusplus
}
#endif


#endif /*!defined(IO_H__INCLUDED_)*/
 

/*
 *********************** End of file ******************************************
 */
