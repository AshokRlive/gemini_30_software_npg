/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCMMonitor
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Read MCM's on board Digital & Analogue Inputs and operates the
 *       Digital Ouputs
 *
 *    CURRENT REVISION
 *
 *               $Revision::               $: (Revision of last commit)
 *               $Author::                 $: (Author of last commit)
 *       \date   $Date::                   $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24/07/13      andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#if !defined(EA_44E6C5A5_099C_4b6a_A664_3AEEEC355253__INCLUDED_)
#define EA_44E6C5A5_099C_4b6a_A664_3AEEEC355253__INCLUDED_


#ifdef __cplusplus
extern "C" {
#endif
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "IO.h"
#include "AnalogueInput.h"
#include "DigitalInput.h"
#include "DigitalOutput.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

// This is here to allow external files to set the OLR State
// All possible OLR States
typedef enum
{
    MCMOLR_STATE_OFF    = 0,
    MCMOLR_STATE_LOCAL  = 1,
    MCMOLR_STATE_REMOTE = 2,
    MCMOLR_STATE_ERROR  = 3,
    MCMOLR_STATE_LAST

} MCMOLR_STATES;

#define OLR_SETTLE_TIME_MS        600      // Time it takes for the OLR readings to settle
#define DI_OLR_QUICK_SCAN_TIME_MS 100      // A fast scan time for use during OLR changes
#define AI_OLR_QUICK_SCAN_TIME_MS 100      // A fast scan time for use during OLR changes

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Thread's Main
 *
 *   \param mainMqFdVoidPtr Main thread's file descriptor
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void *ioManagerThread(void *mainMqFdVoidPtr);


/*!
 ******************************************************************************
 *   \brief Get value of an Analogue Input
 *
 *   \param channel  The channel number of the required AI
 *          valuePtr Pointer where the value will be placed (NULL if not required)
 *          flagsPtr Pointer where the flags will be placed (NULL if not required)
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
IO_ERROR ioManGetAnalogueInput(lu_uint8_t channel, lu_float32_t *valuePtr, lu_uint8_t *flagsPtr);


/*!
 ******************************************************************************
 *   \brief Thread's Main
 *
 *   \param channel  The channel number of the required DI
 *          valuePtr Pointer where the value will be placed (NULL if not required)
 *          flagsPtr Pointer where the flags will be placed (NULL if not required)
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
IO_ERROR ioManGetDigitalInput(lu_uint8_t channel, lu_bool_t *valuePtr, lu_uint8_t *flagsPtr);


/*!
 ******************************************************************************
 *   \brief Operate a Digital output
 *
 *   \param channel  The channel number of the required DO
 *          value    Value to write to the DO channel
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
IO_ERROR ioManSetDigitalOutput(lu_uint8_t channel, lu_bool_t value);


/*!
 ******************************************************************************
 *   \brief Set the value of a Digital input
 *
 *   \param channel  The channel number of the required DI
 *          value    Value to write to the DI channel
 *
 *   \return Error Code
 *
 ******************************************************************************
 */IO_ERROR ioManSetDigitalInput(lu_uint8_t channel, lu_bool_t value);


/*!
 ******************************************************************************
 *   \brief Interface to allow the retrieval of AI Config info
 *
 *   \param channel   The channel number of the required DO
 *          configPtr Pointer to an AI Config Structure
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
IO_ERROR ioManGetAnalogueInputConfig(lu_uint8_t channel, MCMMonAIConfigStr *configPtr);


/*!
 ******************************************************************************
 *   \brief Alarm Function for Analogue Input
 *
 *   This is called from analogueInputUpdate() to mark an AI as being in alarm
 *   or not.
 *
 *    This function used the value of vLogic as a basis for alarming - if this
 *    AI is less than scaledAlarmLevelLow % or greater than scaledAlarmLevelHigh%
 *    of vLogic's value then it will be marked as in ALARM.
 *
 *   \param analogueInputPtr structure defining the required input
 *
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
IO_ERROR ioManAlarmFuncPercOfvLogic(AnalogueInputType *AIPointPtr);


/*!
 ******************************************************************************
 *   \brief Update function for a Digital Input
 *
 *   This is called from digitalInputUpdate() if the DI has been configured as
 *   a virtual channel.  A pointer to this function should be placed in the
 *   DI's  config->update_func configuration in MCMIOMap.h)
 *
 *   It will be called from the IOManager's main digitalInputUpdate loop
 *
 *   \param digitalInputPtr structure defining the required input
 *
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
IO_ERROR ioManUpdateVirtualDigitalInput(DigitalInputType *digitalInputPtr);


/*!
 ******************************************************************************
 *   \brief Initialise and retrieve the state of the FActory pin
 *
 *   This is designed to be called from the main thread before any of the worker
 *   threads are launched.  If the Factory pin is enabled the Monitor should sit
 *   in a spin-sleep loop doing nothing.
 *
 *   \param     valuePtr    Pointer where the value will be written
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
IO_ERROR ioManGetFactoryPin(lu_bool_t *valuePtr);

//IO_ERROR ioManGetWdogRestartFile(DigitalInputType *digitalInputPtr);

#ifdef __cplusplus
}
#endif


#endif /*!defined(EA_44E6C5A5_099C_4b6a_A664_3AEEEC355253__INCLUDED_)*/

/*
 *********************** End of file ******************************************
 */
