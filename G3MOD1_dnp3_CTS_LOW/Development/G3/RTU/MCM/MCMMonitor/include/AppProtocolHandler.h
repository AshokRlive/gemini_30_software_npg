/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCMMonitor
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       External Protocol Handler
 *
 *    CURRENT REVISION
 *
 *               $Revision::               $: (Revision of last commit)
 *               $Author::                 $: (Author of last commit)
 *       \date   $Date::                   $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24/07/13      andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#if !defined(EA_9CBBAEAC_3ADF_4358_83B4_801B656FADF4__INCLUDED_)
#define EA_9CBBAEAC_3ADF_4358_83B4_801B656FADF4__INCLUDED_


#ifdef __cplusplus
extern "C" {
#endif
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Main.h"
#include "MonitorProtocol.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define API_VER_MAJOR            1
#define API_VER_MINOR            0

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Creates and binds a socket
 *
 *   Detailed description
 *
 *   \param name 	 - path of the domain socket
 *   		blocking - defines if blocking or not
 *
 *
 *   \return  socket descriptor or -1
 *
 ******************************************************************************
 */
lu_int32_t initExtSocket(lu_int8_t *name, lu_bool_t blocking);

/*!
 ******************************************************************************
 *   \brief Accepts incoming connection n external socket
 *
 *   Detailed description
 *
 *   \param
 *
 *
 *   \return  socket descriptor or -1
 *
 ******************************************************************************
 */
lu_int32_t acceptExtConnection(lu_int32_t sockFd, lu_int32_t *conFd, lu_int32_t timeout);

/*!
 ******************************************************************************
 *   \brief Receives a complete incoming packet or nothing from external app
 *
 *   Detailed description
 *
 *   \param fd          Socket handle
 *          buff        Buffer to hold the incoming data
 *          buffLen     Length of supplied buffer
 *          timeout     Timeout in mS
 *
 *
 *   \return  Number of bytes received or <0 Error
 *
 ******************************************************************************
 */
lu_int32_t recvExtPacket(lu_int32_t fd, lu_int8_t *buff, lu_uint32_t buffLen, lu_int32_t timeout);

/*!
 ******************************************************************************
 *   \brief Receives a defined number of bytes
 *
 *   Detailed description
 *
 *   \param fd          Socket handle
 *          buff        Buffer to hold the incoming data
 *          reqBytes    Length of supplied buffer
 *          timeoutPtr  Timeout Pointer in mS
 *
 *
 *   \return  Number of bytes received or <0 Error
 *
 ******************************************************************************
 */
lu_int32_t recvExtChunk(lu_int32_t fd, lu_int8_t *buff, lu_uint32_t reqBytes, lu_int32_t *timeoutPtr);


/*!
 ******************************************************************************
 *   \brief Sends a message to an external application
 *
 *   Detailed description
 *
 *   \param fd          Socket handle
 *          outXMsg     Formatted message to send
 *
 *   \return  0> Success, else <0 Error, 0 Lost Connection
 *
 ******************************************************************************
 */
lu_int16_t sendExtPacket(lu_int32_t fd, AppMsgStr *outXMsg);

/*!
 ******************************************************************************
 *   \brief Inserts the CRC in to the message header
 *
 *   Detailed description
 *
 *   \param outXMsg     Valid packet pointer
 *
 *   \return  0 Success, else <0 Error
 *
 ******************************************************************************
 */
lu_int32_t insertCRC(AppMsgStr *outXMsg);

/*!
 ******************************************************************************
 *   \brief External Socket Message Handler
 *
 *    Decodes messages received on the external socket and sends appropriate
 *    replies.
 *
 *   \param extConfd  Socket Handle
 *          mainMqFd  MQ Handle to the main thread
 *          inMsg     Incoming Message
 *
 *   \return N/A
 *
 ******************************************************************************
 */
lu_int32_t handleExtMsg(lu_int32_t extConfd, mqd_t mainMqFd, AppMsgStr *inMsg);

/*!
 ******************************************************************************
 *   \brief Sends an empty message to external application
 *
 *   Detailed description
 *
 *   \param fd          Socket handle
 *          cmd         Protocol command to insert in to header
 *
 *   \return  0 Success, else <0 Error
 *
 ******************************************************************************
 */
lu_int32_t sendEmptyMsg(lu_int32_t fd, lu_int16_t cmd);

/*!
 ******************************************************************************
 *   \brief Sends a reply to a request for an Analogue Input Value
 *
 *   Detailed description
 *
 *   \param fd          Socket handle
 *          channel     Analogue Input channel number
 *          flags       Flags
 *          value       Digital Input Value
 *
 *
 *   \return  0 Success, else <0 Error
 *
 ******************************************************************************
 */
lu_int32_t sendRepDIMsg(lu_int32_t fd, lu_int8_t channel, lu_uint8_t flags, lu_bool_t value);

/*!
 ******************************************************************************
 *   \brief Sends a reply to a request for an Analogue Input Value
 *
 *   Detailed description
 *
 *   \param fd          Socket handle
 *          channel     Analogue Input channel number
 *          flags       Flags
 *          value       Analogue Input Value
 *
 *
 *   \return  0 Success, else <0 Error
 *
 ******************************************************************************
 */
lu_int32_t sendRepAIMsg(lu_int32_t fd, lu_int8_t channel, lu_uint8_t flags, lu_float32_t value);

/*!
 ******************************************************************************
 *   \brief Sends a reply to a Power Save cmd
 *
 *   Detailed description
 *
 *   \param fd          Socket handle
 *          powerState  Power Save State
 *
 *
 *   \return  0 Success, else <0 Error
 *
 ******************************************************************************
 */
#ifdef MON_POWER_SAVE
lu_int32_t sendRepPowerSaveMsg(lu_int32_t fd, lu_bool_t powerState);
#endif

/*!
 ******************************************************************************
 *   \brief Sends a reply to a request for the application version
 *
 *   Detailed description
 *
 *   \param fd          Socket handle
 *
 *
 *   \return  0 Success, else <0 Error
 *
 ******************************************************************************
 */
lu_int32_t sendRepAppVerMsg(lu_int32_t fd);

/*!
 ******************************************************************************
 *   \brief Sends a Set Alarm message
 *
 *   Detailed description
 *
 *   \param fd          Socket handle
 *          alarm       Type of Alarm
 *
 *
 *   \return  0 Success, else <0 Error
 *
 ******************************************************************************
 */
// TODO _SKA - Remove
//lu_int32_t sendCmdSetAlarm(lu_int32_t fd, XMSG_ALARM alarm);

/*!
 ******************************************************************************
 *   \brief Sends an exit command to an external application
 *
 *   Detailed description
 *
 *   \param fd          Socket handle
 *          action      Action for external application to take
 *          reason      Reason code for the Exit message
 *
 *   \return  0 Success, else <0 Error
 *
 ******************************************************************************
 */
lu_int32_t sendExitMsg(lu_int32_t fd, XMSG_EXIT_ACTION action, XMSG_EXIT_CODE reason);

/*!
 ******************************************************************************
 *   \brief Decodes a request for DI value msg
 *
 *   Detailed description
 *
 *   \param msgPtr      Pointer to the message structure
 *          channelPtr  Set to the channel number requested in the msg
 *
 *   \return  0 Success, else <0 Error
 *
 ******************************************************************************
 */
lu_int32_t decodeReqDIMsg(AppMsgStr *msgPtr, lu_uint8_t *channelPtr);

/*!
 ******************************************************************************
 *   \brief Decodes a request for AI value msg
 *
 *   Detailed description
 *
 *   \param msgPtr      Pointer to the message structure
 *          channelPtr  Set to the channel number requested in the msg
 *
 *   \return  0 Success, else <0 Error
 *
 ******************************************************************************
 */
lu_int32_t decodeReqAIMsg(AppMsgStr *msgPtr, lu_uint8_t *channelPtr);

/*!
 ******************************************************************************
 *   \brief Decodes a notify exit msg
 *
 *   Detailed description
 *
 *   \param msgPtr       Pointer to the message structure
 *          exitCodePtr  Set to the exit code in the msg
 *          timeoutMsPtr Set to the timeout value in the msg
 *
 *   \return  0 Success, else <0 Error
 *
 ******************************************************************************
 */
lu_int32_t decodeNotifyExitMsg(AppMsgStr *msgPtr, lu_int32_t *exitCodePtr, lu_uint32_t *timeoutMsPtr);

/*!
 ******************************************************************************
 *   \brief Decodes a set IP msg
 *
 *   If the msg is decoded successfully, the IP address of the RTU is changed
 *   as appropriate.
 *
 *   Detailed description
 *
 *   \param msgPtr       Pointer to the message structure
 *
 *   \return  0 Success, else <0 Error
 *
 ******************************************************************************
 */
lu_int32_t decodeSetIPMsg(AppMsgStr *msgPtr);

/*!
 ******************************************************************************
 *   \brief Decodes a set NTP Server Address msg
 *
 *   If the msg is decoded successfully, the NTP address is changed
 *   as appropriate.
 *
 *   Detailed description
 *
 *   \param msgPtr       Pointer to the message structure
 *
 *   \return  0 Success, else <0 Error
 *
 ******************************************************************************
 */
lu_int32_t decodeSetNTPMsg(AppMsgStr *msgPtr);

/*!
 ******************************************************************************
 *   \brief Decodes a request Exit msg
 *
 *   Detailed description
 *
 *   \param msgPtr       Pointer to the message structure
 *          actionPtr    Set to the action code
 *          reasonPtr    Set to the reason code
 *
 *   \return  0 Success, else <0 Error
 *
 ******************************************************************************
 */
lu_int32_t decodeReqExitMsg(AppMsgStr *msgPtr, lu_int8_t *actionPtr, lu_int8_t *reasonPtr);

/*!
 ******************************************************************************
 *   \brief Decodes a Kick Wdog msg
 *
 *   Detailed description
 *
 *   \param startPtr     True to Start Kicking, else False
 *
 *   \return  0 Success, else <0 Error
 *
 ******************************************************************************
 */
lu_int32_t decodeReqKickWdogMsg(AppMsgStr *msgPtr, lu_bool_t *startPtr);

/*!
 ******************************************************************************
 *   \brief Decodes a Power Save msg
 *
 *   Detailed description
 *
 *   \param msgPtr        Pointer to the message structure
 *          powerStatePtr Set to the Power Save State
 *
 *   \return  0 Success, else <0 Error
 *
 ******************************************************************************
 */
#ifdef MON_POWER_SAVE
lu_int32_t decodePowerSaveMsg(AppMsgStr *msgPtr, lu_bool_t *powerStatePtr);
#endif

/*!
 ******************************************************************************
 *   \brief Decodes a restart HTTPServer request
 *
 *   Detailed description
 *
 *   \param msgPtr        Pointer to the message structure
 *
 *   \return  0 Success, else <0 Error
 *
 ******************************************************************************
 */
lu_int32_t decodeRestartHTTP(AppMsgStr *msgPtr);

/*!
 ******************************************************************************
 *   \brief Decodes a set OK LED State request
 *
 *   Detailed description
 *
 *   \param msgPtr        Pointer to the message structure
 *          ledStatePtr   Set to the OK LED State requested
 *
 *   \return  0 Success, else <0 Error
 *
 ******************************************************************************
 */
lu_int32_t decodeSetOKState(AppMsgStr *msgPtr, lu_int32_t *ledStatePtr);

/*!
 ******************************************************************************
 *   \brief Decodes a set OLR State request
 *
 *   Detailed description
 *
 *   \param msgPtr        Pointer to the message structure
 *          OLRStatePtr   State to set the OLR to
 *
 *   \return  0 Success, else <0 Error
 *
 ******************************************************************************
 */
lu_int32_t decodeSetOLRState(AppMsgStr *msgPtr, lu_uint8_t *OLRStatePtr);


#ifdef __cplusplus
}
#endif


#endif /*!defined(EA_9CBBAEAC_3ADF_4358_83B4_801B656FADF4__INCLUDED_)*/

/*
 *********************** End of file ******************************************
 */
