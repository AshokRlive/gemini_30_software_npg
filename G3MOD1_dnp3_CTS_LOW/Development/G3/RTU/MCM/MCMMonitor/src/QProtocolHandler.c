/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCMMonitor
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Communicates with other threads over posix queues
 *
 *    CURRENT REVISION
 *
 *               $Revision::               $: (Revision of last commit)
 *               $Author::                 $: (Author of last commit)
 *       \date   $Date::                   $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   07/08/13      andrews_s   Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "LogMessage.h"
#include "QProtocolHandler.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

lu_int32_t sendQMsg(QMsgType *msgPtr)
{
    if (msgPtr == NULL )
    {
        return -1;
    }

    if (mq_send(msgPtr->qDest, (char *) msgPtr, MSG_LEN, msgPtr->priority) < 0)
    {
        logMessageError(SUBSYSTEM_ID_MAIN,
                        "sendMsg Failed! Src[%d] Dest[%d] Cmd[0x%03X]\n",
                        msgPtr->qSrc, msgPtr->qDest, msgPtr->cmd);

        switch (errno)
        {
            case EAGAIN:
                logMessageError(SUBSYSTEM_ID_MAIN,
                                "sendMsg Failed : Queue FULL!");
            break;
            case ETIMEDOUT:
                logMessageError(SUBSYSTEM_ID_MAIN, "sendMsg Failed : Timeout!");
            break;
            case EMSGSIZE:
                logMessageError(SUBSYSTEM_ID_MAIN,
                                "sendMsg Failed : Message too big!");
            break;
            case EINVAL:
                logMessageError(SUBSYSTEM_ID_MAIN,
                                "sendMsg Failed : Timeout Invalid!");
            break;
            case EBADF:
                logMessageError(SUBSYSTEM_ID_MAIN,
                                "sendMsg Failed : Bad descriptor!");
            break;
            default:
                logMessageError(SUBSYSTEM_ID_MAIN,
                                "sendMsg Failed : Unknown error errno[%d]!",
                                errno);
        }
        return -1;
    }

    return 0;
}

lu_int32_t sendNotifyExitQMsg(mqd_t qSrc, mqd_t qDest, lu_uint8_t appId, lu_int32_t exitCode, lu_uint32_t timeoutMs)
{
    QMsgType msgOut;

    msgOut.cmd            = QMSG_CMD_NOTIFY_EXIT;
    msgOut.priority       = QMSG_CMD_PRIORITY_HIGH;
    msgOut.qSrc           = qSrc;
    msgOut.qDest          = qDest;
    msgOut.data1.uIntVal  = appId;
    msgOut.data2.intVal   = exitCode;
    msgOut.data3.uIntVal  = timeoutMs;

    return sendQMsg(&msgOut);
}

lu_int32_t sendEventAllQMsg(mqd_t qSrc, mqd_t qDest)
{
    QMsgType msgOut;

    msgOut.cmd            = QMSG_CMD_EVENT_ALL;
    msgOut.priority       = QMSG_CMD_PRIORITY_NORMAL;
    msgOut.qSrc           = qSrc;
    msgOut.qDest          = qDest;

    return sendQMsg(&msgOut);
}

lu_int32_t sendHATModeQMsg(mqd_t qSrc, mqd_t qDest, lu_bool_t enabled)
{
    QMsgType msgOut;

    msgOut.cmd            = QMSG_CMD_HAT_MODE;
    msgOut.priority       = QMSG_CMD_PRIORITY_NORMAL;
    msgOut.qSrc           = qSrc;
    msgOut.qDest          = qDest;
    msgOut.data1.boolVal  = enabled;

    return sendQMsg(&msgOut);
}

lu_int32_t sendReqPowerSaveQMsg(mqd_t qSrc, mqd_t qDest, lu_bool_t powerSave)
{
    QMsgType msgOut;

    msgOut.cmd            = QMSG_CMD_REQ_POWERSAVE;
    msgOut.priority       = QMSG_CMD_PRIORITY_HIGH;
    msgOut.qSrc           = qSrc;
    msgOut.qDest          = qDest;
    msgOut.data1.boolVal  = powerSave;

    return sendQMsg(&msgOut);
}

lu_int32_t sendRepPowerSaveQMsg(mqd_t qSrc, mqd_t qDest, lu_bool_t powerSave)
{
    QMsgType msgOut;

    msgOut.cmd            = QMSG_CMD_REP_POWERSAVE;
    msgOut.priority       = QMSG_CMD_PRIORITY_NORMAL;
    msgOut.qSrc           = qSrc;
    msgOut.qDest          = qDest;
    msgOut.data1.boolVal  = powerSave;

    return sendQMsg(&msgOut);
}

lu_int32_t sendReqShutdownQMsg(mqd_t qSrc, mqd_t qDest, QMSG_EXIT_ACTION action, QMSG_EXIT_CODE reason)
{
    QMsgType msgOut;

    msgOut.cmd            = QMSG_CMD_REQ_SHUTDN;
    msgOut.priority       = QMSG_CMD_PRIORITY_HIGH;
    msgOut.qSrc           = qSrc;
    msgOut.qDest          = qDest;
    msgOut.data1.intVal   = action;
    msgOut.data2.intVal   = reason;

    return sendQMsg(&msgOut);
}

lu_int32_t sendReqAlarmShutdownQMsg(mqd_t qSrc, mqd_t qDest, QMSG_EXIT_ACTION action, QMSG_EXIT_CODE reason)
{
    QMsgType msgOut;

    msgOut.cmd            = QMSG_CMD_ALM_SHUTDN;
    msgOut.priority       = QMSG_CMD_PRIORITY_HIGH;
    msgOut.qSrc           = qSrc;
    msgOut.qDest          = qDest;
    msgOut.data1.intVal   = action;
    msgOut.data2.intVal   = reason;

    return sendQMsg(&msgOut);
}

lu_int32_t sendReqWDogKickQMsg(mqd_t qSrc, mqd_t qDest, lu_bool_t kickWDog)
{
    QMsgType msgOut;

    msgOut.cmd            = QMSG_CMD_REQ_WDKICK;
    msgOut.priority       = QMSG_CMD_PRIORITY_HIGH;
    msgOut.qSrc           = qSrc;
    msgOut.qDest          = qDest;
    msgOut.data1.intVal   = kickWDog;

    return sendQMsg(&msgOut);
}

lu_int32_t sendRepWDogKickQMsg(mqd_t qSrc, mqd_t qDest, lu_bool_t wdogState)
{
    QMsgType msgOut;

    msgOut.cmd            = QMSG_CMD_REP_WDKICK;
    msgOut.priority       = QMSG_CMD_PRIORITY_NORMAL;
    msgOut.qSrc           = qSrc;
    msgOut.qDest          = qDest;
    msgOut.data1.intVal   = wdogState;

    return sendQMsg(&msgOut);
}

#if 0
lu_int32_t sendAIAlarmQMsg(mqd_t qSrc, mqd_t qDest, lu_int32_t channel, lu_int32_t alarmState, lu_float32_t value)
{
    QMsgType msgOut;

    msgOut.cmd            = QMSG_CMD_ALM_AI;
    msgOut.priority       = QMSG_CMD_PRIORITY_HIGH;
    msgOut.qSrc           = qSrc;
    msgOut.qDest          = qDest;
    msgOut.data1.intVal   = channel;
    msgOut.data2.intVal   = alarmState;
    msgOut.data3.floatVal = value;

    return sendQMsg(&msgOut);
}
#endif

lu_int32_t sendReqDIQMsg(mqd_t qSrc, mqd_t qDest, lu_int32_t channel)
{
    QMsgType msgOut;

    msgOut.cmd            = QMSG_CMD_REQ_DI;
    msgOut.priority       = QMSG_CMD_PRIORITY_NORMAL;
    msgOut.qSrc           = qSrc;
    msgOut.qDest          = qDest;
    msgOut.data1.intVal   = channel;

    return sendQMsg(&msgOut);
}

lu_int32_t sendRepDIQMsg(mqd_t qSrc, mqd_t qDest, lu_int32_t channel, lu_uint8_t flags, lu_bool_t value)
{
    QMsgType msgOut;

    msgOut.cmd            = QMSG_CMD_REP_DI;
    msgOut.priority       = QMSG_CMD_PRIORITY_NORMAL;
    msgOut.qSrc           = qSrc;
    msgOut.qDest          = qDest;
    msgOut.data1.intVal   = channel;
    msgOut.data2.uIntVal  = (lu_uint32_t) flags;
    msgOut.data3.boolVal  = value;

    return sendQMsg(&msgOut);
}

lu_int32_t sendReqAIQMsg(mqd_t qSrc, mqd_t qDest, lu_int32_t channel)
{
    QMsgType msgOut;

    msgOut.cmd            = QMSG_CMD_REQ_AI;
    msgOut.priority       = QMSG_CMD_PRIORITY_NORMAL;
    msgOut.qSrc           = qSrc;
    msgOut.qDest          = qDest;
    msgOut.data1.intVal   = channel;

    return sendQMsg(&msgOut);
}

lu_int32_t sendRepAIQMsg(mqd_t qSrc, mqd_t qDest, lu_int32_t channel, lu_uint8_t flags, lu_float32_t value)
{
    QMsgType msgOut;

    msgOut.cmd            = QMSG_CMD_REP_AI;
    msgOut.priority       = QMSG_CMD_PRIORITY_NORMAL;
    msgOut.qSrc           = qSrc;
    msgOut.qDest          = qDest;
    msgOut.data1.intVal   = channel;
    msgOut.data2.uIntVal  = (lu_uint32_t) flags;
    msgOut.data3.floatVal = value;

    return sendQMsg(&msgOut);
}

lu_int32_t sendReqSetDOQMsg(mqd_t qSrc, mqd_t qDest, lu_int32_t channel, lu_bool_t value)
{
    QMsgType msgOut;

    msgOut.cmd            = QMSG_CMD_REQ_SET_DO;
    msgOut.priority       = QMSG_CMD_PRIORITY_NORMAL;
    msgOut.qSrc           = qSrc;
    msgOut.qDest          = qDest;
    msgOut.data1.intVal   = channel;
    msgOut.data2.boolVal  = value;

    return sendQMsg(&msgOut);
}

lu_int32_t sendRepSetDOQMsg(mqd_t qSrc, mqd_t qDest, lu_int32_t channel, lu_bool_t value)
{
    QMsgType msgOut;

    msgOut.cmd            = QMSG_CMD_REP_SET_DO;
    msgOut.priority       = QMSG_CMD_PRIORITY_NORMAL;
    msgOut.qSrc           = qSrc;
    msgOut.qDest          = qDest;
    msgOut.data1.intVal   = channel;
    msgOut.data2.boolVal  = value;

    return sendQMsg(&msgOut);
}

lu_int32_t sendReqSetDIQMsg(mqd_t qSrc, mqd_t qDest, lu_int32_t channel, lu_bool_t value)
{
    QMsgType msgOut;

    msgOut.cmd            = QMSG_CMD_REQ_SET_DI;
    msgOut.priority       = QMSG_CMD_PRIORITY_NORMAL;
    msgOut.qSrc           = qSrc;
    msgOut.qDest          = qDest;
    msgOut.data1.intVal   = channel;
    msgOut.data2.boolVal  = value;

    return sendQMsg(&msgOut);
}

lu_int32_t sendRepSetDIQMsg(mqd_t qSrc, mqd_t qDest, lu_int32_t channel, lu_bool_t value)
{
    QMsgType msgOut;

    msgOut.cmd            = QMSG_CMD_REP_SET_DI;
    msgOut.priority       = QMSG_CMD_PRIORITY_NORMAL;
    msgOut.qSrc           = qSrc;
    msgOut.qDest          = qDest;
    msgOut.data1.intVal   = channel;
    msgOut.data2.boolVal  = value;

    return sendQMsg(&msgOut);
}

lu_int32_t sendReqSetOLRQMsg(mqd_t qSrc, mqd_t qDest, lu_uint8_t OLRState)
{
    QMsgType msgOut;

    msgOut.cmd            = QMSG_CMD_REQ_SET_OLR;
    msgOut.priority       = QMSG_CMD_PRIORITY_NORMAL;
    msgOut.qSrc           = qSrc;
    msgOut.qDest          = qDest;
    msgOut.data1.intVal   = OLRState;

    return sendQMsg(&msgOut);
}

lu_int32_t sendRepSetOLRQMsg(mqd_t qSrc, mqd_t qDest, lu_uint8_t OLRState)
{
    QMsgType msgOut;

    msgOut.cmd            = QMSG_CMD_REP_SET_OLR;
    msgOut.priority       = QMSG_CMD_PRIORITY_NORMAL;
    msgOut.qSrc           = qSrc;
    msgOut.qDest          = qDest;
    msgOut.data1.intVal   = OLRState;

    return sendQMsg(&msgOut);
}

lu_int32_t sendReqStopAppQMsg(mqd_t qSrc, mqd_t qDest)
{
    QMsgType msgOut;

    msgOut.cmd            = QMSG_CMD_REQ_STOPAPP;
    msgOut.priority       = QMSG_CMD_PRIORITY_HIGH;
    msgOut.qSrc           = qSrc;
    msgOut.qDest          = qDest;

    return sendQMsg(&msgOut);
}

lu_int32_t sendRepStopAppQMsg(mqd_t qSrc, mqd_t qDest, lu_uint8_t appId)
{
    QMsgType msgOut;

    msgOut.cmd            = QMSG_CMD_REQ_STOPAPP;
    msgOut.priority       = QMSG_CMD_PRIORITY_NORMAL;
    msgOut.qSrc           = qSrc;
    msgOut.qDest          = qDest;
    msgOut.data1.uIntVal  = appId;

    return sendQMsg(&msgOut);
}

lu_int32_t sendReqLEDStateQMsg(mqd_t qSrc, mqd_t qDest, lu_int32_t state)
{
    QMsgType msgOut;

    msgOut.cmd            = QMSG_CMD_REQ_LEDSTATE;
    msgOut.priority       = QMSG_CMD_PRIORITY_HIGH;
    msgOut.qSrc           = qSrc;
    msgOut.qDest          = qDest;
    msgOut.data1.intVal   = state;

    return sendQMsg(&msgOut);
}

lu_int32_t sendRepLEDStateQMsg(mqd_t qSrc, mqd_t qDest, lu_int32_t state)
{
    QMsgType msgOut;

    msgOut.cmd            = QMSG_CMD_REP_LEDSTATE;
    msgOut.priority       = QMSG_CMD_PRIORITY_NORMAL;
    msgOut.qSrc           = qSrc;
    msgOut.qDest          = qDest;
    msgOut.data1.intVal   = state;

    return sendQMsg(&msgOut);
}

lu_int32_t sendRepStateQMsg(mqd_t qSrc, mqd_t qDest, lu_int32_t threadID, lu_int32_t state)
{
    QMsgType msgOut;

    msgOut.cmd            = QMSG_CMD_REP_STATE;
    msgOut.priority       = QMSG_CMD_PRIORITY_NORMAL;
    msgOut.qSrc           = qSrc;
    msgOut.qDest          = qDest;
    msgOut.data1.intVal   = threadID;
    msgOut.data2.intVal   = state;

    return sendQMsg(&msgOut);
}

lu_int32_t sendErrorQMsg(mqd_t qSrc, mqd_t qDest, lu_uint8_t cmd, lu_int32_t errorCode)
{
    QMsgType msgOut;

    msgOut.cmd            = cmd | QMSG_CMD_REP_ERROR;
    msgOut.priority       = QMSG_CMD_PRIORITY_NORMAL;
    msgOut.qSrc           = qSrc;
    msgOut.qDest          = qDest;
    msgOut.data1.errVal   = errorCode;

    return sendQMsg(&msgOut);
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
