/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCMMonitor
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Communicates with the Watchdog PIC
 *
 *    CURRENT REVISION
 *
 *               $Revision::               $: (Revision of last commit)
 *               $Author::                 $: (Author of last commit)
 *       \date   $Date::                   $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25/07/13      andrews_s   Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/prctl.h> // prctl()
#include <sys/types.h> // open()
#include <sys/stat.h>  // open()
#include <fcntl.h>     // open()

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "Main.h"
#include "LogMessage.h"
#include "WDogManager.h"
#include "IOManager.h"

#include "MCMIOMap.h"
#include "MonitorPointMapping.h"
#include "timeOperations.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

typedef enum
{
    WDOG_STATE_KICK = 0,
    WDOG_STATE_CHECK_KICKBACK = 1,
    WDOG_STATE_LAST

} WDOG_STATE;

typedef struct wdogStateDef
{
    lu_bool_t  value;
    lu_bool_t  new_value;
    lu_uint8_t filter;
    lu_bool_t  published;

} wdogStateStr;
/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

static lu_int8_t   wDogManInit(void);
static lu_int8_t   wDogGPIOInit(void);
static void        wDogClose(void);
static void        wDogDecodeQMsq(QMsgType msgIn);
static void        wdogCheckSysHealthy(void);


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static OK_LEDSTATE myState     = OK_LEDSTATE_INITIALISING;
static OK_LEDSTATE myLastState = OK_LEDSTATE_INITIALISING;
static lu_uint8_t  retry;

// GPIO file descriptors
static FILE *fdWDogKickValue;

static mqd_t mqFd; // Local MQ descriptor
static mqd_t mainMqFd;
static lu_bool_t startKicking; // Always kick, unless main thread says otherwise
static lu_bool_t kick_value = WDOG_KICK_HIGH;

static wdogStateStr wdog_restarted; // Stores the wdog restarted value

// State Machine State
static WDOG_STATE wdogState;

// Timers
static struct timespec timeNow;                // Updated once per loop
static struct timespec checkSysHealthyTimer;   // Controls the SYS HEALTHY read frequency
static struct timespec kickTimer;              // Controls the WDOG KICK frequency

static lu_uint32_t     sysUnhealthyCount = 0;      // Counts unhealthy reads

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

static void runStateMachine(void)
{
    lu_bool_t gpioVal;
    lu_bool_t fastKick = LU_FALSE; //Type of increase of the kickTimer (fast/regular)
    static lu_bool_t reported = LU_FALSE;  //Tells when the error is already reported

    if (startKicking != LU_TRUE)
    {
        return;
    }

    // Simple state machine for kicking the watchdog
    switch (wdogState)
    {
        case WDOG_STATE_KICK:
            // Wait for the 1 second timeout before kicking
            if (checkTimeout(&timeNow, &kickTimer) == LU_TRUE)
            {
                // Kick WDOG HIGH
                if (writeKLED(fdWDogKickValue, kick_value) > 0)
                {
                    kick_value = !kick_value;
                    reported = LU_FALSE;
                    fastKick = (wdog_restarted.value == LU_TRUE)?
                                    LU_TRUE :   // Fast 'reset' kick
                                    LU_FALSE;   // Regular kick
                }
                else
                {
                    if (reported == LU_FALSE)     //avoid repeating the message
                    {
                        logMessageError(SUBSYSTEM_ID_WDOG_MANAGER, "Error KICKING WDOG");
                        reported = LU_TRUE;
                    }
                }
                //Adjust the timer to control the next kick
                kickTimer = timeNow;
                timespec_add_ms(&kickTimer, (fastKick == LU_TRUE)? WDOG_KICK_10HZ_MS : WDOG_KICK_1HZ_MS);

                wdogState = WDOG_STATE_CHECK_KICKBACK;
            }
        break;

        case WDOG_STATE_CHECK_KICKBACK:
            wdogState = WDOG_STATE_KICK;

            /* If the filter is empty and the wdog restarted signal is clear us then stop
               reading the kickback - we don't care */
            if ((wdog_restarted.filter == 0) && (wdog_restarted.value == LU_FALSE))
            {
                break;
            }

            if (ioManGetDigitalInput(MCM_MON_DI_GPIO_WDOG_HANDSHAKE, &gpioVal, NULL) == 0)
            {
                if (gpioVal != kick_value)
                {
                    // This is a normal kick back
                    wdog_restarted.new_value = LU_FALSE;
                }
                else
                {
                    // This is a watchdog restart indication (inverted kickback)
                    wdog_restarted.new_value = LU_TRUE;
                }

                if (wdog_restarted.value == wdog_restarted.new_value)
                {
                    wdog_restarted.filter--;

                    if (wdog_restarted.filter == 0)
                    {
                        // publish wdog_restarted value ONCE!
                        if (wdog_restarted.published == LU_FALSE)
                        {
                            ioManSetDigitalInput((lu_uint8_t)MCM_MON_DI_WDOG_FAILED, wdog_restarted.value);
                            wdog_restarted.published = LU_TRUE;
                        }

                        /* Keep resetting the filter if restarted inidcation is true
                          (allows recovery) */
                        if (wdog_restarted.value == LU_TRUE)
                        {
                            wdog_restarted.filter = WDOG_KICKBACK_MIN_READINGS;
                        }
                    }
                }
                else
                {
                    wdog_restarted.value = wdog_restarted.new_value;
                    wdog_restarted.filter = WDOG_KICKBACK_MIN_READINGS;
                }
            }
            else
            {
                logMessageError(SUBSYSTEM_ID_WDOG_MANAGER, "Error reading KICKBACK state");
            }
        break;

        default:
            logMessageError(SUBSYSTEM_ID_WDOG_MANAGER, "WDogManager Illegal State! [%d]", wdogState);

            wdogState = WDOG_STATE_KICK;
        break;
    }
}

void *wdogThread(void *dummy)
{
    LU_UNUSED(dummy);

    // Set the thread name (ps -T)  - ignore errors
    prctl(PR_SET_NAME, "WDogManager", 0, 0, 0);

    // Initialise Message Queues
    mqFd     = connectMsgQueue(threadInfo[THREAD_WDOG].mqName);
    mainMqFd = connectMsgQueue(threadInfo[THREAD_MAIN].mqName);
    if ((mqFd < 0) || (mainMqFd < 0))
    {
        logMessageFatal(SUBSYSTEM_ID_WDOG_MANAGER,
                        "Error initialising Msg Queues [%d] [%d]", mqFd, mainMqFd);
        exit(1);
    }

    if (wDogManInit() != 0)
    {
        logMessageFatal(SUBSYSTEM_ID_WDOG_MANAGER,
                        "Watchdog initialisation FAILED");
        exit(2);
    }

    // Initialise our Thread's state to HEALTHY
    myState = OK_LEDSTATE_HEALTHY;

    retry = 0;

    while (!Closedown)
    {
        if (ArgIgnoreWatchdog == LU_FALSE)
        {
            // Update the time
            clock_gettimespec(&timeNow);

            runStateMachine();

            // Check the state of the SYS_HEALTHY GPIO
            wdogCheckSysHealthy();
        }

        // Handle messages coming in on the message queue
        if (processMsgQueue(mqFd, &wDogDecodeQMsq, 100) < 0)
        {
            if ((mqFd = connectMsgQueue(threadInfo[THREAD_WDOG].mqName)) < 0)
            {
                logMessageFatal(SUBSYSTEM_ID_WDOG_MANAGER, "Error sending Registration msg to Main");
                exit(1);
            }
        }

        // If there's been a change of state we must notify the main thread
        if (myState != myLastState)
        {
            if (sendRepStateQMsg(threadInfo[THREAD_WDOG].threadMqFd, mainMqFd, THREAD_WDOG, myState) < 0)
            {
                logMessageError(SUBSYSTEM_ID_WDOG_MANAGER, "Error sending STATE change to MAIN!");
            }

            myLastState = myState;
        }
    }

    mq_close(mqFd);
    mq_close(mainMqFd);

    wDogClose();

    return NULL ;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */
/*!
 ******************************************************************************
 *   \brief     Message Queue Decoder
 *
 *    Handles an incoming message on the queue
 *
 *   \param
 *
 *
 *   \return
 *
 ******************************************************************************
 */
static void wDogDecodeQMsq(QMsgType msgIn)
{
    QMsgType   msgOut;
    lu_uint8_t command;

    // Extract the command (removing error flag)
    command = msgIn.cmd & ~QMSG_CMD_REP_ERROR;

    // See if there's anything in our Queue
    switch (command)
    {
        case QMSG_CMD_REQ_WDKICK:
            startKicking = msgIn.data1.intVal;

            if (startKicking == LU_FALSE)
            {
                logMessageWarn(SUBSYSTEM_ID_WDOG_MANAGER,
                                "Request to STOP kicking Watchdog");
            }
            else
            {
                logMessageInfo(SUBSYSTEM_ID_WDOG_MANAGER,
                                "Request to START kicking Watchdog");
            }

            if (sendRepWDogKickQMsg(threadInfo[THREAD_WDOG].threadMqFd, msgIn.qSrc, startKicking) < 0)
            {
                logMessageError(SUBSYSTEM_ID_WDOG_MANAGER,
                                "Error sending RepWDogKick [%d]", msgOut.cmd,
                                msgIn.qSrc);
            }
        break;

        default:
            logMessageWarn(SUBSYSTEM_ID_WDOG_MANAGER, "Invalid msg received [0x%03X]", command);
    }

}


/*!
 ******************************************************************************
 *   \brief     Initialise the watchdog GPIO
 *
 *   \param     None
 *
 *   \return    ErrorCode
 *
 ******************************************************************************
 */
lu_int8_t wDogGPIOInit(void)
{
    lu_int8_t ret = 0;


    // If we are ignoring the watchdog, leave the OS gpio in auto
    // This means the OS will continue to kick it
    if (ArgIgnoreWatchdog == LU_TRUE)
    {
        return 0;
    }

    if (setKLEDManual(KLED_WDOG_1HZ_KICK_NAME) != 0)
    {
        ret = -1;
    }

    if (initKLED(KLED_WDOG_1HZ_KICK_NAME, &fdWDogKickValue) != 0)
    {
        ret = -1;
    }

    // Initialise the KickBack to Low
    if (writeKLED(fdWDogKickValue, WDOG_KICK_LOW) <= 0)
    {
        ret = -1;
    }

    return ret;
}

/*!
 ******************************************************************************
 *   \brief     Initialise timers and GPIO
 *
 *   \param     None
 *
 *   \return    ErrorCode
 *
 ******************************************************************************
 */
lu_int8_t wDogManInit(void)
{
    // Initialise timers to now
    clock_gettimespec(&timeNow);
    kickTimer = checkSysHealthyTimer = timeNow;

    // Initialise State Machine
    wdogState = WDOG_STATE_KICK;

    // Default to start kicking
    startKicking = LU_TRUE;

    // Initialise the wdog_restarted values
    wdog_restarted.value         = LU_FALSE;
    wdog_restarted.new_value     = LU_FALSE;
    wdog_restarted.filter        = WDOG_KICKBACK_MIN_READINGS;
    wdog_restarted.published     = LU_FALSE;

    // Initialise all the GPIO
    return wDogGPIOInit();
}

/*!
 ******************************************************************************
 *   \brief     Close WDog related file descriptors
 *
 *   \param     None
 *
 *   \return    None
 *
 ******************************************************************************
 */
void wDogClose(void)
{
    // Close any open file descriptors
    if (fdWDogKickValue != NULL)
    {
        fclose(fdWDogKickValue);
    }
}


/*!
 ******************************************************************************
 *   \brief     Checks SYS_HEALTHY
 *
 *   \param     None
 *
 *   \return    None
 *
 ******************************************************************************
 */
void wdogCheckSysHealthy(void)
{
    lu_bool_t  gpioVal = LU_FALSE;
    lu_uint8_t flagsVal = IO_FLAG_NONE;

    // Read the SYS HEALTH input
    if (checkTimeout(&timeNow, &checkSysHealthyTimer) == LU_FALSE)
    {
        return;
    }

    // Set the healthy timer
    checkSysHealthyTimer = timeNow;
    timespec_add_ms(&checkSysHealthyTimer, WDOG_HEALTHY_CHECK_MS);

    if (ioManGetDigitalInput(MCM_MON_DI_GPIO_SYS_HEALTHY, &gpioVal, &flagsVal) == 0)
    {
        myState = OK_LEDSTATE_HEALTHY;

        if (gpioVal == WDOG_SYS_UNHEALTHY)
        {
            sysUnhealthyCount++;

            SET_STATE(myState, OK_LEDSTATE_WARNING);
            logMessageWarn(SUBSYSTEM_ID_WDOG_MANAGER, "System Healthy from WDOG is UNHEALTHY Count:[%d]!!!", sysUnhealthyCount);

            // Reboot if we have read UNHEALTHY multiple times consecutively
            if (sysUnhealthyCount >= WDOG_MAX_UNHEALTHY_READS)
            {
                logMessageFatal(SUBSYSTEM_ID_WDOG_MANAGER, "System Healthy is UNHEALTHY.  Shutting down!");
                if (sendReqAlarmShutdownQMsg(threadInfo[THREAD_WDOG].threadMqFd, mainMqFd, QMSG_EXIT_ACTION_REBOOT, QMSG_EXIT_CODE_WDOG_FAIL) < 0)
                {
                    // If we fail to send the msg to the main thread we can initiate clean shutdown of the application here
                    logMessageFatal(SUBSYSTEM_ID_WDOG_MANAGER, "Error sending SHUTDOWN to main Thread. Setting Closedown = 1!");
                    Closedown = LU_TRUE;
                }
            }
        }
        else
        {
            // Reset the Unhealthy Counter
            sysUnhealthyCount = 0;
        }
    }
    else
    {
        /* Only display error if the IO point has been initialised */
        if ((flagsVal & (IO_FLAG_INVALID | IO_FLAG_OFF_LINE)) == 0)
        {
            logMessageError(SUBSYSTEM_ID_WDOG_MANAGER, "Error reading SYS_HEALTHY state");
        }
    }
}

/*
 *********************** End of file ******************************************
 */
