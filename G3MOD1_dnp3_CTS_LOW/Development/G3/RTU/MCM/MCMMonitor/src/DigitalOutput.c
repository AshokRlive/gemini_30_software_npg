/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCMMonitor
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Defines a Digital Output
 *
 *    CURRENT REVISION
 *
 *               $Revision::               $: (Revision of last commit)
 *               $Author::                 $: (Author of last commit)
 *       \date   $Date::                   $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25/07/13      andrews_s   Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <errno.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "DigitalOutput.h"
#include "LogMessage.h"
#include "timeOperations.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

IO_ERROR digitalOutputInit(lu_uint8_t address, DigitalOutputType* digitalOutput, MCMMonDOConfigStr *config)
{
  if (digitalOutput == NULL)
  {
      return IO_ERROR_NULL;
  }

  // Initialise the analogue Output attributes
  digitalOutput->channel                       = address;
  digitalOutput->fd                            = NULL;

  digitalOutput->config                        = config;

  digitalOutput->value                         = LU_FALSE;
  digitalOutput->flags                         = IO_FLAG_OFF_LINE;

  // Initialise the GPIO
  if (initGPIO(digitalOutput->config->gpioNum, GPIOLIB_DIRECTION_OUTPUT, &(digitalOutput->fd)) != 0)
  {
      return IO_ERROR_READ;
  }

  return IO_ERROR_NONE;
}


IO_ERROR digitalOutputClose(DigitalOutputType *digitalOutput)
{
    if (digitalOutput == NULL)
    {
        return IO_ERROR_NULL;
    }

    return closeGPIO(digitalOutput->fd);
}


IO_ERROR digitalOutputSet(DigitalOutputType* digitalOutput, lu_bool_t value)
{
  IO_ERROR ret;


  if (digitalOutput == NULL)
  {
      return IO_ERROR_NULL;
  }

  if ((ret = writeGPIO(digitalOutput->fd, value)) <= 0)
  {
      // Only log the message if the value was previously ONLINE
      if ((digitalOutput->flags & IO_FLAG_OFF_LINE) == 0)
      {
          logMessageWarn(SUBSYSTEM_ID_IO_MANAGER, "Unable to write DO GPIO:[%d] marking as OFFLINE", digitalOutput->config->gpioNum);
      }

      // Close the file descriptor
      digitalOutputClose(digitalOutput);
      digitalOutput->fd = NULL;

      // Try to reopen the DO file - ignore errors
      digitalOutputInit(digitalOutput->channel, digitalOutput, digitalOutput->config);

      // Unable to write the DO value file, set the OFFLINE flag
      digitalOutput->flags |= IO_FLAG_OFF_LINE;

      return IO_ERROR_WRITE;
  }
  else
  {
      if (digitalOutput->flags & IO_FLAG_OFF_LINE)
      {
          logMessageInfo(SUBSYSTEM_ID_IO_MANAGER, "DO GPIO:[%d] Recovered!", digitalOutput->config->gpioNum);
      }

      digitalOutput->lastValue = digitalOutput->value;
      digitalOutput->value = value;

#if 0
      printf("DO: GPIO[%d] : Val:[%d]\n", digitalOutput->config->gpioNum, digitalOutput->value);
#endif

      // Clear the Off-line Flag
      digitalOutput->flags &= ~IO_FLAG_OFF_LINE;
  }

  return IO_ERROR_NONE;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
