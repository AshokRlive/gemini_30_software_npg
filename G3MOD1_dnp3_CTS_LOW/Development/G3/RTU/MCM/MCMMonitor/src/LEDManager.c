/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCMMonitor
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Controls the MCM's OK LED
 *
 *    CURRENT REVISION
 *
 *               $Revision::               $: (Revision of last commit)
 *               $Author::                 $: (Author of last commit)
 *       \date   $Date::                   $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25/07/13      andrews_s   Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <sys/prctl.h> // prctl()
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "Main.h"
#include "LEDManager.h"
#include "LogMessage.h"
#include "MCMIOMap.h"
#include "timeOperations.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define LED_OFF             0
#define LED_ON              1

#define LED_COLOUR_OFF      0
#define LED_COLOUR_GREEN    1
#define LED_COLOUR_RED      2
#define LED_COLOUR_ORANGE   3

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
typedef struct LEDFlashDef
{
    lu_bool_t   state; // Unused
    lu_uint16_t onTimeMs;
    lu_uint16_t offTimeMs;

    lu_uint8_t  onColour;
    lu_uint8_t  offColour;

} LEDFlashType;

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

static void setLED(LEDFlashType led, lu_bool_t state, lu_bool_t powerSave);
static lu_int8_t initLEDs(void);
static void decodeQMsq(QMsgType msgIn);
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
// Define the LED States
//                                            State           onTime offTime  ON Colour         OFF Colour
static const LEDFlashType LEDFlash[] =  { { OK_LEDSTATE_HEALTHY,      1000, 1000, LED_COLOUR_GREEN, LED_COLOUR_OFF },
                                          { OK_LEDSTATE_SPECIAL1,      200, 200,  LED_COLOUR_GREEN, LED_COLOUR_RED },
                                          { OK_LEDSTATE_SPECIAL2,      250, 250,  LED_COLOUR_ORANGE,LED_COLOUR_OFF },
                                          { OK_LEDSTATE_INITIALISING,  500, 500,  LED_COLOUR_GREEN, LED_COLOUR_RED },
                                          { OK_LEDSTATE_WARNING,       250, 250,  LED_COLOUR_GREEN, LED_COLOUR_OFF },
                                          { OK_LEDSTATE_ALARM,        1000, 1000, LED_COLOUR_RED,   LED_COLOUR_OFF },
                                          { OK_LEDSTATE_CRITICAL,      200, 200,  LED_COLOUR_RED,   LED_COLOUR_OFF }
                                        };


static FILE *fdLedOKRedValue;
static FILE *fdLedOKGreenValue;

static OK_LEDSTATE flashState = OK_LEDSTATE_INITIALISING;
static lu_bool_t powerSave = LU_FALSE;
static lu_bool_t stateVal = LED_ON; // ON or OFF State
static mqd_t mainMqFd;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

void *ledThread(void *dummy)
{
    LU_UNUSED(dummy);
    mqd_t mqFd;

    OK_LEDSTATE myState = OK_LEDSTATE_INITIALISING;
    OK_LEDSTATE myLastState = OK_LEDSTATE_INITIALISING;

    struct timespec timeNow;
    struct timespec timer;

    // Set the thread name (ps -T)  - ignore errors
    prctl(PR_SET_NAME, "LEDManager", 0, 0, 0);

    // Get the time now
    clock_gettime(CLOCK_MONOTONIC, &timer);
    timespec_add_ms(&timer, LEDFlash[OK_LEDSTATE_INITIALISING].onTimeMs);

    // Initialise Message Queues
    mqFd     = connectMsgQueue(threadInfo[THREAD_LED].mqName);
    mainMqFd = connectMsgQueue(threadInfo[THREAD_MAIN].mqName);
    if ((mqFd < 0) || (mainMqFd < 0))
    {
        logMessageFatal(SUBSYSTEM_ID_LED_MANAGER,
                        "Error initialising Msg Queues [%d] [%d]", mqFd, mainMqFd);
        exit(1);
    }

    // Initialise everything
    if (initLEDs() != 0)
    {
        logMessageFatal(SUBSYSTEM_ID_LED_MANAGER, "LED initialisation FAILED");
        exit(2);
    }

    // Initialise our Thread's state to HEALTHY
    myState = OK_LEDSTATE_HEALTHY;


    while (!Closedown)
    {
        // See if the timer has expired
        clock_gettime(CLOCK_MONOTONIC, &timeNow);
        if (checkTimeout(&timeNow, &timer) > 0)
        {
            // Invert stateVal
            stateVal = !stateVal;

            if ((powerSave == LU_FALSE) && (HATMode == LU_FALSE))
            {
                // Set LED and update timer as appropriate
                setLED(LEDFlash[flashState], stateVal, powerSave);
            }

            // Alter the timer
            timer = timeNow;
            timespec_add_ms(&timer,
                            stateVal ? LEDFlash[flashState].onTimeMs :
                                       LEDFlash[flashState].offTimeMs);
        }

        // TODO - SKA - adjust the timeoutMs arg to less than 100 if the next flash time is due sooner.
        // Check for incoming messages
        if (processMsgQueue(mqFd, &decodeQMsq, 100) < 0)
        {
            if ((mqFd = connectMsgQueue(threadInfo[THREAD_LED].mqName)) < 0)
            {
                logMessageFatal(SUBSYSTEM_ID_LED_MANAGER, "Error sending Registration msg to Main");
                exit(1);
            }
        }

        // Inform the main thread that my state has changed
        if (myState != myLastState)
        {
            if (sendRepStateQMsg(threadInfo[THREAD_LED].threadMqFd, mainMqFd, THREAD_LED, myState) < 0)
            {
                logMessageError(SUBSYSTEM_ID_LED_MANAGER,
                                "Error sending STATE change to MAIN!");
            }

            myLastState = myState;
        }

//        lucy_usleep(50000);
    }

    mq_close(mqFd);
    mq_close(mainMqFd);

    return NULL;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Sets the behaviour of the OK LED
 *
 *   Controls the ON/OFF State of the RED & GREEN OK LED
 *
 *   \param led        LED definition structure
 *          state      Desired ON/OFF state
 *          powerSave  Flag to determine if powerSave is in operation
 *
 *   \return
 *
 ******************************************************************************
 */
static void setLED(LEDFlashType led, lu_bool_t state, lu_bool_t powerSave)
{
    if (powerSave == LU_TRUE)
    {
        // Turn off BOTH LEDs
        writeKLED(fdLedOKRedValue, LED_OFF);
        writeKLED(fdLedOKGreenValue, LED_OFF);
    }
    else
    {
        switch (state ? led.onColour : led.offColour)
        {
            case LED_COLOUR_OFF:
                // Turn off BOTH LEDs
                writeKLED(fdLedOKRedValue, LED_OFF);
                writeKLED(fdLedOKGreenValue, LED_OFF);
            break;

            case LED_COLOUR_RED:
                // Turn on RED
                // Turn off GREEN
                writeKLED(fdLedOKRedValue, LED_ON);
                writeKLED(fdLedOKGreenValue, LED_OFF);
            break;

            case LED_COLOUR_GREEN:
                // Turn off RED
                // Turn on GREEN
                writeKLED(fdLedOKRedValue, LED_OFF);
                writeKLED(fdLedOKGreenValue, LED_ON);
            break;

            case LED_COLOUR_ORANGE:
                // Turn on RED
                // Turn on GREEN
                writeKLED(fdLedOKRedValue, LED_ON);
                writeKLED(fdLedOKGreenValue, LED_ON);
            break;
        }
    }
}

/*!
 ******************************************************************************
 *   \brief Initialises the LEDs
 *
 *   Initialises the LED's interface
 *
 *   \param led        LED definition structure
 *          state      Desired ON/OFF state
 *          powerSave  Flag to determine if powerSave is in operation
 *
 *   \return
 *
 ******************************************************************************
 */
static lu_int8_t initLEDs(void)
{
    lu_int8_t ret = 0;

    if (setKLEDManual(KLED_OK_RED_NAME) != 0)
    {
        ret = -1;
    }

    if (initKLED(KLED_OK_RED_NAME, &fdLedOKRedValue) != 0)
    {
        ret = -1;
    }

    if (setKLEDManual(KLED_OK_GREEN_NAME) != 0)
    {
        ret = -1;
    }

    if (initKLED(KLED_OK_GREEN_NAME, &fdLedOKGreenValue) != 0)
    {
        ret = -1;
    }

    return ret;
}

/*!
 ******************************************************************************
 *   \brief Message Queue Decoder
 *
 *    Handles an incoming message on the queue
 *
 *   \param
 *
 *
 *   \return
 *
 ******************************************************************************
 */
static void decodeQMsq(QMsgType msgIn)
{
    lu_uint8_t command;

    // Extract the command (removing error flag)
    command = msgIn.cmd & ~QMSG_CMD_REP_ERROR;

    // See if there's anything in our Queue
    switch (command)
    {
        case QMSG_CMD_REQ_LEDSTATE:
            flashState = msgIn.data1.intVal;
            sendRepLEDStateQMsg(msgIn.qSrc, mainMqFd, flashState);
        break;

        default:
            logMessageWarn(SUBSYSTEM_ID_LED_MANAGER, "Invalid msg received [0x%03X]", command);
    }

}

/*
 *********************** End of file ******************************************
 */
