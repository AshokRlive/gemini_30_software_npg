/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:CanUtils.c
 *    FILE TYPE: c source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14 Nov 2013     andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

//TODO - SKA - It might be that libsocketcan would be a better interface to
// retrieve a lot of the CAN Stats from - using the file system is quite
// expensive...

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <unistd.h>  // close()
#include <stdio.h>   // fopen()
#include <string.h>

#include <sys/ioctl.h>
//#include <sys/socket.h>
#include <net/if.h>
#include <linux/can.h>
#include <linux/can/raw.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IOManager.h"
#include "AnalogueInput.h"
#include "MonitorPointMapping.h"
#include "CANUtils.h"

#include "LogMessage.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define MAX_CAN_STAT_FILE_LEN 200


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static IO_ERROR CANOpenFile(AnalogueInputType *AIPointPtr, CANBUS bus, const char *fileFormatPtr);
static IO_ERROR CANCloseFile(AnalogueInputType *AIPointPtr);
static IO_ERROR CANGetStatsFromFile(AnalogueInputType *AIPointPtr, CANBUS bus, const char *fileFormatPtr);
static IO_ERROR CANReadValFromFile(FILE *fd, lu_int64_t *val);
static IO_ERROR CANUsagePerc(lu_uint32_t numBytes, lu_uint32_t busRate, lu_uint32_t periodMs, lu_float32_t *usageRatePerc);
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

const char ifaceTxBytesFileName[]   = "/sys/class/net/%s/statistics/tx_bytes";
const char ifaceRxBytesFileName[]   = "/sys/class/net/%s/statistics/rx_bytes";
const char ifaceTxPacketsFileName[] = "/sys/class/net/%s/statistics/tx_packets";
const char ifaceRxPacketsFileName[] = "/sys/class/net/%s/statistics/rx_packets";
const char ifaceRxDroppedFileName[] = "/sys/class/net/%s/statistics/rx_dropped";
const char ifaceRxOverFileName[]    = "/sys/class/net/%s/statistics/rx_over_errors";
/*
 * tx_errrors & rx_errors are totals of all errors in the given direction
 * There are other files, such as 'rx_window_errors' that are included
 *
 */
const char ifaceTxErrsFilename[]    = "/sys/class/net/%s/statistics/tx_errors";
const char ifaceRxErrsFilename[]    = "/sys/class/net/%s/statistics/rx_errors";


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

lu_int32_t CANCreateSocket(const char *iface)
{
    lu_int32_t      ret;
    struct ifreq    ifr;
    lu_int32_t      socketFd;
    struct sockaddr_can address;

    if (iface == NULL)
    {
        return -1;
    }

    /* Open RAW CAN socket */
    socketFd = socket(PF_CAN, SOCK_RAW, CAN_RAW);
    if (socketFd < 0)
    {
        return socketFd;
    }

    /* Get interface name */
    strncpy(ifr.ifr_name, iface, IFNAMSIZ);

    if ((ret = ioctl(socketFd, SIOCGIFINDEX, &ifr)) < 0)
    {
        return ret;
    }

    /* Initialize CAN address struct */
    address.can_family = AF_CAN;
    address.can_ifindex = ifr.ifr_ifindex;

    /* Bind the interface to the local socket */
    if ((ret = bind(socketFd, (struct sockaddr *)&address, sizeof(address))) < 0)
    {
        return ret;
    }

    return socketFd;
}


lu_int32_t CANGetIFaceState(lu_int32_t socketFd, const char *iFacePtr, lu_bool_t *statePtr)
{
    lu_int32_t      ret;
    struct ifreq    ifr;


    if ((socketFd < 0) || (iFacePtr == NULL) || (statePtr == NULL))
    {
        return -1;
    }

    /* Get interface name */
    strncpy(ifr.ifr_name, iFacePtr, IFNAMSIZ);

    /* Read interface flags */
    if ((ret = ioctl(socketFd, SIOCGIFFLAGS, &ifr)) < 0)
    {
        return ret;
    }

    /* Check the UP flag */
    if (ifr.ifr_flags & IFF_UP)
    {
        *statePtr = LU_TRUE;
    }
    else
    {
        *statePtr = LU_FALSE;
    }

    return 0;
}


lu_int32_t CANSetIFaceState(lu_int32_t socketFd, const char *iFacePtr, lu_bool_t state)
{
    lu_int32_t      ret;
    struct ifreq    ifr;
    lu_bool_t       actualState;


    if ((socketFd < 0) || (iFacePtr == NULL))
    {
        return -1;
    }

    /* Get interface name */
    strncpy(ifr.ifr_name, iFacePtr, IFNAMSIZ);

    /* Read interface flags */
    if ((ret = ioctl(socketFd, SIOCGIFFLAGS, &ifr)) < 0)
    {
        return ret;
    }

    /* Set the UP flag as required */
    if (state == LU_TRUE)
    {
        ifr.ifr_flags |= IFF_UP;
    }
    else
    {
        ifr.ifr_flags &= ~IFF_UP;
    }

    /* Set the interface to the desired state */
    if ((ret = ioctl(socketFd, SIOCSIFFLAGS, &ifr)) < 0)
    {
        return ret;
    }

    /* Read back the state of the interface */
    if (CANGetIFaceState(socketFd, iFacePtr, &actualState) == 0)
    {
        /* If the state matches the requested state, return 0 */
        if (actualState == state)
        {
            return 0;
        }
    }

    return -1;
}

void CANCloseSocket(lu_int32_t socketFd)
{
    /* close the socket */
    if (socketFd >= 0)
    {
        close(socketFd);
    }
}

/* CAN0
 *
 */
IO_ERROR CAN0UpdateAITxBytes(AnalogueInputType *AIPointPtr)
{
    return CANGetStatsFromFile(AIPointPtr, CANBUS_0, ifaceTxBytesFileName);
}

IO_ERROR CAN0UpdateAIRxBytes(AnalogueInputType *AIPointPtr)
{
    return CANGetStatsFromFile(AIPointPtr, CANBUS_0, ifaceRxBytesFileName);
}

IO_ERROR CAN0UpdateAIUsage(AnalogueInputType *AIPointPtr)
{
    IO_ERROR ret;
    lu_uint64_t  numBytesTotal = 0;
    lu_int64_t   numBytes = 0;
    lu_float32_t usagePerc = 0;
    lu_float32_t val0, val1;
    lu_uint8_t flags0, flags1;


    if ((ret = ioManGetAnalogueInput(MCM_MON_AI_CAN0_TX_BYTES, &val0, &flags0)) == IO_ERROR_NONE)
    {
        ret = ioManGetAnalogueInput(MCM_MON_AI_CAN0_RX_BYTES, &val1, &flags1);
    }
    AIPointPtr->flags = flags0 | flags1;

    // Simply sum the TX Errors from both CAN busses
    numBytesTotal = (lu_uint64_t) val0 + val1;

    // If this AI was OFFLINE previously we'll copy numBytes to rawValue;
    if (AIPointPtr->flags & IO_FLAG_OFF_LINE)
    {
        AIPointPtr->rawValue = numBytesTotal;
    }

    // If the TX or RX count rolls over we will simply skip this scan and update the AI's
    // rawValue (since we do not know at what value this rollover may occur).  This will ensure
    // the rate is correct on the next scan
    if (numBytesTotal < ((lu_uint64_t) AIPointPtr->rawValue))
    {
        AIPointPtr->rawValue = (lu_int64_t) numBytesTotal;
        return IO_ERROR_NONE;
    }

    numBytes = numBytesTotal - AIPointPtr->rawValue;

    if (CANUsagePerc(numBytes, CAN0_BUS_SPEED, AIPointPtr->config->scanIntervalMs, &usagePerc) != IO_ERROR_NONE)
    {
        AIPointPtr->flags |= IO_FLAG_OFF_LINE;
        return IO_ERROR_GENERAL;
    }

    // Clear the OFFLINE flag
    AIPointPtr->flags &= ~IO_FLAG_OFF_LINE;

    // Store the previous value of numBytes in rawValue (use it like a lastValue!)
    AIPointPtr->rawValue = numBytesTotal;

    // Update the value and mark AI as ON LINE
    AIPointPtr->value    = usagePerc;

    return IO_ERROR_NONE;
}

IO_ERROR CAN0UpdateAITxErrs(AnalogueInputType *AIPointPtr)
{
    return CANGetStatsFromFile(AIPointPtr, CANBUS_0, ifaceTxErrsFilename);
}

IO_ERROR CAN0UpdateAIRxErrs(AnalogueInputType *AIPointPtr)
{
    return CANGetStatsFromFile(AIPointPtr, CANBUS_0, ifaceRxErrsFilename);
}

IO_ERROR CAN0UpdateAIErrs(AnalogueInputType *AIPointPtr)
{
    // Note that this is not protected by a Mutex.  It should be called from
    // a critical section (e.g. from a ioManGetAnalogueInput() )
    IO_ERROR ret;
    lu_float32_t val0, val1;
    lu_uint8_t flags0, flags1;

    if ((ret = ioManGetAnalogueInput(MCM_MON_AI_CAN0_TX_ERRS, &val0, &flags0)) == IO_ERROR_NONE)
    {
        ret = ioManGetAnalogueInput(MCM_MON_AI_CAN0_RX_ERRS, &val1, &flags1);
    }

    // Simply sum the TX Errors from both CAN busses
    AIPointPtr->value = val0 + val1;
    AIPointPtr->flags = flags0 | flags1;

    return ret;
}

IO_ERROR CAN0UpdateAITxPack(AnalogueInputType *AIPointPtr)
{
    return CANGetStatsFromFile(AIPointPtr, CANBUS_0, ifaceTxPacketsFileName);
}

IO_ERROR CAN0UpdateAIRxPack(AnalogueInputType *AIPointPtr)
{
    return CANGetStatsFromFile(AIPointPtr, CANBUS_0, ifaceRxPacketsFileName);
}

IO_ERROR CAN0UpdateAIRxDropped(AnalogueInputType *AIPointPtr)
{
    return CANGetStatsFromFile(AIPointPtr, CANBUS_0, ifaceRxDroppedFileName);
}

IO_ERROR CAN0UpdateAIRxOverErr(AnalogueInputType *AIPointPtr)
{
    return CANGetStatsFromFile(AIPointPtr, CANBUS_0, ifaceRxOverFileName);
}


/* CAN1
 *
 */
IO_ERROR CAN1UpdateAITxBytes(AnalogueInputType *AIPointPtr)
{
    return CANGetStatsFromFile(AIPointPtr, CANBUS_1, ifaceTxBytesFileName);
}

IO_ERROR CAN1UpdateAIRxBytes(AnalogueInputType *AIPointPtr)
{
    return CANGetStatsFromFile(AIPointPtr, CANBUS_1, ifaceRxBytesFileName);
}

IO_ERROR CAN1UpdateAIUsage(AnalogueInputType *AIPointPtr)
{
    IO_ERROR ret;
    lu_uint64_t  numBytesTotal = 0;
    lu_int64_t   numBytes = 0;
    lu_float32_t usagePerc = 0;
    lu_float32_t val0, val1;
    lu_uint8_t flags0, flags1;


    if ((ret = ioManGetAnalogueInput(MCM_MON_AI_CAN1_TX_BYTES, &val0, &flags0)) == IO_ERROR_NONE)
    {
        ret = ioManGetAnalogueInput(MCM_MON_AI_CAN1_RX_BYTES, &val1, &flags1);
    }
    AIPointPtr->flags = flags0 | flags1;

    // Simply sum the TX Errors from both CAN busses
    numBytesTotal = (lu_uint64_t) val0 + val1;

    // If this AI was OFFLINE previously we'll copy numBytes to rawValue;
    if (AIPointPtr->flags & IO_FLAG_OFF_LINE)
    {
        AIPointPtr->rawValue = numBytesTotal;
    }

    // If the TX or RX count rolls over we will simply skip this scan and update the AI's
    // rawValue (since we do not know at what value this rollover may occur).  This will ensure
    // the rate is correct on the next scan
    if (numBytesTotal < ((lu_uint64_t) AIPointPtr->rawValue))
    {
        AIPointPtr->rawValue = (lu_int64_t) numBytesTotal;
        return IO_ERROR_NONE;
    }

    numBytes = numBytesTotal - AIPointPtr->rawValue;

    if (CANUsagePerc(numBytes, CAN1_BUS_SPEED, AIPointPtr->config->scanIntervalMs, &usagePerc) != IO_ERROR_NONE)
    {
        AIPointPtr->flags |= IO_FLAG_OFF_LINE;
        return IO_ERROR_GENERAL;
    }

    // Clear the OFFLINE flag
    AIPointPtr->flags &= ~IO_FLAG_OFF_LINE;

    // Store the previous value of numBytes in rawValue (use it like a lastValue!)
    AIPointPtr->rawValue = numBytesTotal;

    // Update the value and mark AI as ON LINE
    AIPointPtr->value    = usagePerc;

    return IO_ERROR_NONE;
}

IO_ERROR CAN1UpdateAITxErrs(AnalogueInputType *AIPointPtr)
{
    return CANGetStatsFromFile(AIPointPtr, CANBUS_1, ifaceTxErrsFilename);
}

IO_ERROR CAN1UpdateAIRxErrs(AnalogueInputType *AIPointPtr)
{
    return CANGetStatsFromFile(AIPointPtr, CANBUS_1, ifaceRxErrsFilename);
}

IO_ERROR CAN1UpdateAIErrs(AnalogueInputType *AIPointPtr)
{
    // Note that this is not protected by a Mutex.  It should be called from
    // a critical section (e.g. from a ioManGetAnalogueInput() )
    IO_ERROR ret;
    lu_float32_t val0, val1;
    lu_uint8_t flags0, flags1;

    if ((ret = ioManGetAnalogueInput(MCM_MON_AI_CAN1_TX_ERRS, &val0, &flags0)) == IO_ERROR_NONE)
    {
        ret = ioManGetAnalogueInput(MCM_MON_AI_CAN1_RX_ERRS, &val1, &flags1);
    }

    // Simply sum the TX Errors from both CAN busses
    AIPointPtr->value = val0 + val1;
    AIPointPtr->flags = flags0 | flags1;

    return ret;
}

IO_ERROR CAN1UpdateAITxPack(AnalogueInputType *AIPointPtr)
{
    return CANGetStatsFromFile(AIPointPtr, CANBUS_1, ifaceTxPacketsFileName);
}

IO_ERROR CAN1UpdateAIRxPack(AnalogueInputType *AIPointPtr)
{
    return CANGetStatsFromFile(AIPointPtr, CANBUS_1, ifaceRxPacketsFileName);
}

IO_ERROR CAN1UpdateAIRxDropped(AnalogueInputType *AIPointPtr)
{
    return CANGetStatsFromFile(AIPointPtr, CANBUS_1, ifaceRxDroppedFileName);
}

IO_ERROR CAN1UpdateAIRxOverErr(AnalogueInputType *AIPointPtr)
{
    return CANGetStatsFromFile(AIPointPtr, CANBUS_1, ifaceRxOverFileName);
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Open a file for reading
 *
 *   Detailed description
 *
 *   Opens a CAN Stats file and stored the fd in the Analogue Input's fd
 *
 *   \param AIPointPtr      Pointer to Analogue Input Point
 *          bus             CAN Bus identifier
 *          fileFormatPtr   Filename to open (with %s in place of canbus)
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
static IO_ERROR CANOpenFile(AnalogueInputType *AIPointPtr, CANBUS bus, const char *fileFormatPtr)
{
    char   ifaceFileName[MAX_CAN_STAT_FILE_LEN];

    if ((AIPointPtr == NULL) || (fileFormatPtr == NULL))
    {
        return IO_ERROR_NULL;
    }

    snprintf(ifaceFileName, MAX_CAN_STAT_FILE_LEN, fileFormatPtr, (bus == CANBUS_0) ? CAN0 : CAN1);

    if ((AIPointPtr->fd = fopen(ifaceFileName, "r")) == NULL)
    {
        logMessageWarn(SUBSYSTEM_ID_MAIN,
                        "Unable to read can stats file [%s]!", ifaceFileName);
        return IO_ERROR_READ;
    }

    fseek(AIPointPtr->fd, 0, SEEK_SET);

    return IO_ERROR_NONE;
}
/*!
 ******************************************************************************
 *   \brief Close a file
 *
 *   Detailed description
 *
 *   Closes a CAN Stats file stored in the fd in the Analogue Input's fd
 *
 *   \param AIPointPtr      Pointer to Analogue Input Point
 *
 *   \return Error Code
 *        AIPointPtr->flags |= IO_FLAG_OFF_LINE;
 *
 ******************************************************************************
 */
static IO_ERROR CANCloseFile(AnalogueInputType *AIPointPtr)
{
    if (AIPointPtr == NULL)
    {
        return IO_ERROR_NULL;
    }

    if (AIPointPtr->fd != NULL)
    {
        fclose(AIPointPtr->fd);
        AIPointPtr->fd = NULL;
    }

    return IO_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Retrieves an integer value from a CanBus stats file
 *
 *   Detailed description
 *
 *   \param AIPointPtr      Pointer to AI object
 *          bus             The Can bus
 *          fileFormatPtr   Format string for the stats file, with %d where can
 *                          bus number goes
 *
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
static IO_ERROR CANGetStatsFromFile(AnalogueInputType *AIPointPtr, CANBUS bus, const char *fileFormatPtr)
{
    lu_int32_t ret;

    if (AIPointPtr == NULL)
    {
        return IO_ERROR_NULL;
    }

    // If the CAN Stats file has not already been opened, open it
    if (AIPointPtr->fd == NULL)
    {
        if ((ret = CANOpenFile(AIPointPtr, bus, fileFormatPtr)) != IO_ERROR_NONE)
        {
            AIPointPtr->flags |= IO_FLAG_OFF_LINE;
            return ret;
        }
    }

    if (CANReadValFromFile(AIPointPtr->fd, &(AIPointPtr->rawValue)) == IO_ERROR_NONE)
    {
        AIPointPtr->value = (lu_float32_t) AIPointPtr->rawValue;
        AIPointPtr->flags &= ~IO_FLAG_OFF_LINE;
    }
    else
    {
        AIPointPtr->flags |= IO_FLAG_OFF_LINE;
    }

    return IO_ERROR_NONE;
}

static IO_ERROR CANReadValFromFile(FILE *fd, lu_int64_t *val)
{
    if (fd == NULL)
    {
        return IO_ERROR_NULL;
    }

    fseek(fd, 0, SEEK_SET);
    if (fscanf(fd, "%lld\n", val) <= 0)
    {
        return IO_ERROR_READ;
    }

    return IO_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Calculates the CAN bus usage as a percentage
 *
 *   Detailed description
 *
 *   \param bus             The Can bus
 *          transmitted     Flag to determins if TX or RX stats are required
 *          numBytesPtr     The number of bytes are returned here
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
static IO_ERROR CANUsagePerc(lu_uint32_t numBytes, lu_uint32_t busRate, lu_uint32_t periodMs, lu_float32_t *usageRatePerc)
{
    lu_float32_t usageRate;

    if ((usageRatePerc == NULL) || (busRate == 0))
    {
        return IO_ERROR_NULL;
    }

    usageRate = (((lu_float32_t)numBytes) * 8.0) / (lu_float32_t)(periodMs / 1000.0);

    *usageRatePerc = (usageRate / (lu_float32_t)busRate) * 100.0;

    return IO_ERROR_NONE;
}


/*
 *********************** End of file ******************************************
 */
