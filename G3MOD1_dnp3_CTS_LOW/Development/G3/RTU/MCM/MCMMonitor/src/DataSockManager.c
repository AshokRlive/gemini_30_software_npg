/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCMMonitor
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Controls other MCM Applications over a socket
 *
 *    CURRENT REVISION
 *
 *               $Revision::               $: (Revision of last commit)
 *               $Author::                 $: (Author of last commit)
 *       \date   $Date::                   $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   07/08/13      andrews_s   Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/un.h>      // AF_UNIX
#include <sys/prctl.h> // prctl()

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "timeOperations.h"

#include "Main.h"
#include "LogMessage.h"
#include "AppProtocolHandler.h"
#include "MonitorPointMapping.h"
#include "DataSockManager.h"
#include "MonitorProtocol.h"
#include "IOManager.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
typedef enum
{
    SM_STATE_INIT = 0,
    SM_STATE_CONNECT,
    SM_STATE_MAIN,
    SM_STATE_WAIT_ON_DATA
} SM_STATE;

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static int dataSockWaitOnData(void);
static void decodeQMsq(QMsgType msgIn);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static lu_int32_t extConFd;
static lu_int8_t extRxBuf[XMSG_CONFIG_MAX_MSG_LEN];
static mqd_t mqFd;
static mqd_t mainMqFd;
static struct timespec pingTimeout;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

void *dataSockThread(void *dummy)
{
    LU_UNUSED(dummy);
    OK_LEDSTATE myState = OK_LEDSTATE_INITIALISING;
    OK_LEDSTATE myLastState = OK_LEDSTATE_INITIALISING;

    lu_int32_t extSockFd = -1;

    SM_STATE sm_State = SM_STATE_INIT;

    struct timespec timeNow;
    struct timespec pingTimer;

    // Set the thread name (ps -T)  - ignore errors
    prctl(PR_SET_NAME, "DataSockManager", 0, 0, 0);

    // Initialise Message Queues
    mqFd     = connectMsgQueue(threadInfo[THREAD_DATA_COMS].mqName);
    mainMqFd = connectMsgQueue(threadInfo[THREAD_MAIN].mqName);
    if ((mqFd < 0) || (mainMqFd < 0))
    {
        logMessageFatal(SUBSYSTEM_ID_DATA_PORT,
                        "Error initialising Msg Queues [%d] [%d]", mqFd, mainMqFd);
        exit(1);
    }

    while (!Closedown)
    {
        switch (sm_State)
        {
            case SM_STATE_INIT:
                if (extSockFd != -1)
                {
                    close(extSockFd);
                    extSockFd = -1;
                }

                if ((extSockFd = initExtSocket(DATA_SOCKET_NAME, LU_FALSE)) < 0)
                {
                    logMessageFatal(SUBSYSTEM_ID_DATA_PORT,
                                    "Error initialising socket [%s]!",
                                    DATA_SOCKET_NAME);

                    // Sleep here so we don't spin
                    lucy_usleep(1000000);
                    break;
                }

                // Listen - only one connection at a time
                if (listen(extSockFd, 0) < 0)
                {
                    logMessageError(SUBSYSTEM_ID_DATA_PORT,
                                    "Error listening on socket [%s] [%d]!",
                                    DATA_SOCKET_NAME, errno);

                    // Sleep here so we don't spin
                    lucy_usleep(1000000);
                    break;
                }
                sm_State = SM_STATE_CONNECT;
            break;

            case SM_STATE_CONNECT:
                SET_STATE(myState, OK_LEDSTATE_WARNING);
                sm_State = SM_STATE_INIT;

                close(extConFd);
                extConFd = -1;
                if (acceptExtConnection(extSockFd, &extConFd, 1000) >= 0)
                {
                    if (extConFd >= 0)
                    {
                        myState = OK_LEDSTATE_HEALTHY;
                        sm_State = SM_STATE_MAIN;

                        logMessageDebug(SUBSYSTEM_ID_DATA_PORT,
                                        "New Connection fd:[%d]", extConFd);

                        // Set the ping schedule and timeout
                        clock_gettimespec(&pingTimer);
                        pingTimeout = pingTimer;
                        timespec_add_ms(&pingTimer,   PING_INTERVAL_MS);
                        timespec_add_ms(&pingTimeout, PING_TIMEOUT_MS);

                        // Inform the IOManager to event ALL values to the Main App
                        sendEventAllQMsg(threadInfo[THREAD_DATA_COMS].threadMqFd,
                                         threadInfo[THREAD_IOMAN].threadMqFd);
                    }
                }
            break;

            case SM_STATE_MAIN:
                sm_State = SM_STATE_WAIT_ON_DATA;

                // See if the timer has expired
                clock_gettimespec(&timeNow);

                // Build a ping message to send to the application
                if (checkTimeout(&timeNow, &pingTimer) == LU_TRUE)
                {
                    sm_State = SM_STATE_WAIT_ON_DATA;

                    sendEmptyMsg(extConFd, XMSG_CMD_REQ_ALIVE);

                    // Alter the timer
                    clock_gettimespec(&pingTimer);
                    timespec_add_ms(&pingTimer, PING_INTERVAL_MS);
                }

                // Check if we've received a ping reply recently
                if (checkTimeout(&timeNow, &pingTimeout) == LU_TRUE)
                {
                    // Send message to App Thread to kill the App (restart)
                    logMessageWarn(SUBSYSTEM_ID_DATA_PORT, "No ping reply received.  Request to kill app.");
                    sendReqStopAppQMsg(threadInfo[THREAD_DATA_COMS].threadMqFd, mainMqFd);

                    // Close the connection in case there was a problem with the connection
                    // The client should detect the closure and reconnect
                    sm_State = SM_STATE_CONNECT;
                }
            break;

            case SM_STATE_WAIT_ON_DATA:
                sm_State = dataSockWaitOnData();
            break;

            default:
                logMessageError(SUBSYSTEM_ID_DATA_PORT, "DataSockThread Illegal State! [%d]", sm_State);
                sm_State = SM_STATE_INIT;
            break;
        }

        // If there's been a change of state we must notify the main thread
        if (myState != myLastState)
        {
            if (sendRepStateQMsg(threadInfo[THREAD_DATA_COMS].threadMqFd, mainMqFd, THREAD_DATA_COMS, myState) < 0)
            {
                logMessageError(SUBSYSTEM_ID_DATA_PORT,
                                "Error sending STATE change to MAIN!");
            }

            myLastState = myState;
        }
    }

    // Tidy Up
    if (extConFd != -1)
    {
        close(extConFd);
    }

    if (extSockFd != -1)
    {
        close(extSockFd);
    }

    unlink(DATA_SOCKET_NAME);

    mq_close(mqFd);
    mq_close(mainMqFd);

    return NULL ;
}
/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Wait for incoming Data
 *
 *   Waits for data on either the internal message queue or the external
 *   socket.
 *
 *   \param  N/A
 *
 *
 *   \return Next State
 *
 ******************************************************************************
 */
static int dataSockWaitOnData(void)
{
    lu_int32_t     maxfd;
    lu_int32_t     stateRet;
    lu_int32_t     selRet;
    lu_int32_t     extRxBytes;
    fd_set         readfds;
    struct timeval timeout = { 1, 0 };  // Default to 1 second wait
    AppMsgStr *inXMsg = NULL;
    lu_int32_t msgId;


    // Calculate the maxFd
    maxfd = (mqFd > extConFd) ? mqFd : extConFd;
    maxfd++;

    stateRet = SM_STATE_MAIN;

    FD_ZERO(&readfds);
    FD_SET(mqFd, &readfds);
    FD_SET(extConFd, &readfds);

    if ((selRet = select(maxfd, &readfds, NULL, NULL, &timeout)) > 0)
    {
        if (FD_ISSET(mqFd, &readfds))
        {
            // Handle messages coming in on the message queue
            processMsgQueue(mqFd, &decodeQMsq, 1); // Note we don't need a large wait here
        }

        if (FD_ISSET(extConFd, &readfds))
        {
            // Handle messages coming in on the external socket
            if ((extRxBytes = recvExtPacket(extConFd, extRxBuf, sizeof(extRxBuf), 0)) > 0)
            {
                // Received a full packet - now parse it
                if (extRxBuf != NULL )
                {
                    inXMsg = (AppMsgStr *) extRxBuf;
                    msgId = handleExtMsg(extConFd, mainMqFd, inXMsg);

                    // Reset the PING TIMEOUT
                    if (msgId == XMSG_REP_REQ_ALIVE)
                    {
                        // Increment the pingTimeout as we've received a ping
                        clock_gettimespec(&pingTimeout);
                        timespec_add_ms(&pingTimeout, PING_TIMEOUT_MS);
#if DEBUG
                        logMessageFatal(SUBSYSTEM_ID_DATA_PORT, "RX: %ld PING MSG:[0x%03X]\n", pingTimeout.tv_sec, msgId);
#endif
                    }
                }
            }
            else if (extRxBytes < 0)
            {
                // Error receiving - reconnect
                stateRet = SM_STATE_CONNECT;
                logMessageDebug(SUBSYSTEM_ID_DATA_PORT, "Lost Connection fd:[%d]", extConFd);
            }
        }
    }
    else if (selRet < 0)
    {
        logMessageError(SUBSYSTEM_ID_DATA_PORT, "select() Error on fd:[%d]", extConFd);
        stateRet = SM_STATE_CONNECT;
    }

    return stateRet;
}

/*!
 ******************************************************************************
 *   \brief Message Queue Decoder
 *
 *    Handles an incoming message on the queue
 *
 *   \param  msgIn  QMessage to decode
 *
 *
 *   \return N/A
 *
 ******************************************************************************
 */
static void decodeQMsq(QMsgType msgIn)
{
    lu_uint8_t command;
    lu_bool_t  error;

    // Extract the command
    command = msgIn.cmd;

    // TODO - SKA - Handle Errors Better!!
    error = (command & QMSG_CMD_REP_ERROR) ? LU_TRUE : LU_FALSE;

    if (error == LU_TRUE)
    {
        logMessageError(SUBSYSTEM_ID_DATA_PORT, "QMsg Error received [0x%03X]", command);
        return;
    }

    // See if there's anything in our Queue
    switch (command)
    {
        case QMSG_CMD_REP_DI:
            sendRepDIMsg(extConFd, msgIn.data1.intVal,        /* Channel */
                            (lu_uint8_t) msgIn.data2.uIntVal, /* Flags */
                            msgIn.data3.boolVal);             /* Value */
        break;

        case QMSG_CMD_REP_AI:
            sendRepAIMsg(extConFd, msgIn.data1.intVal,        /* Channel */
                            (lu_uint8_t) msgIn.data2.uIntVal, /* Flags */
                            msgIn.data3.floatVal);            /* Value */
        break;

        case QMSG_CMD_REQ_SHUTDN:
            // Nothing we can do if this fails
            if (sendExitMsg(extConFd, msgIn.data1.intVal, msgIn.data2.intVal)
                            < 0)
            {
                logMessageError(SUBSYSTEM_ID_DATA_PORT,
                                "Unable to send SHUTDOWN request to application");
            }
        break;

        // Ignore these messages
        case QMSG_CMD_REP_SET_OLR:
        case QMSG_CMD_REP_STOPAPP:
        break;

        default:
            logMessageWarn(SUBSYSTEM_ID_DATA_PORT,
                            "Invalid msg received [0x%03X]", command);
    }
}

/*
 *********************** End of file ******************************************
 */
