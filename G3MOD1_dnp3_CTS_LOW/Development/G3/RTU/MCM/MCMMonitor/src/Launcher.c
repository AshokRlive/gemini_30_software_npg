/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCMMonitor
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       forks() and exec()s external programs
 *
 *    CURRENT REVISION
 *
 *               $Revision::               $: (Revision of last commit)
 *               $Author::                 $: (Author of last commit)
 *       \date   $Date::                   $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25/07/13      andrews_s   Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <string.h> // strncpy
#include <sys/prctl.h> // prctl()


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "Main.h"
#include "Launcher.h"
#include "AppManager.h"
#include "LogMessage.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
void *launchThread(void *procPtr)
{
    ProcessType *processPtr = (ProcessType *) procPtr;

    char *envp[] = { NULL };

    pid_t pid;
    lu_int32_t status;

    // Set the thread name (ps -T)  - ignore errors
    prctl(PR_SET_NAME, "Launcher", 0, 0, 0);

    // Do not die when the parent dies
    signal(SIGHUP, SIG_IGN );

    pid = fork();
    if (pid == 0)
    {
        // Disassociate with controlling terminal (async-signal-safe)
        setsid();

        execve(processPtr->processName[0], processPtr->processName, envp);

        _exit(0);
    }
    else if (pid > 0)
    {
        processPtr->pid = pid;
        do
        {
            if (waitpid(pid, &status, WUNTRACED | WCONTINUED) < 0)
            {
                if (errno == EINTR)
                {
                    continue;
                }
                else
                {
                    processPtr->exitCode = 0;
                    return NULL ;
                }
            }

            // Check if the process was signalled to stop
            if (WIFEXITED(status))
            {
                // Exit code from exit() or return()
                processPtr->exitCode = WEXITSTATUS(status);
            }
            else if (WIFSIGNALED(status))
            {
                // Signal that stopped the process
                processPtr->exitCode = WTERMSIG(status);
            }
            else if (WIFCONTINUED(status))
            {
                // Signal that stopped the process
                processPtr->exitCode = WSTOPSIG(status);
            }
            else
            {
                // Exit code from the process
                processPtr->exitCode = status;
            }
        }
        while (!WIFEXITED(status) && !WIFSIGNALED(status));
    }
    else
    {
        logMessageWarn(SUBSYSTEM_ID_APP_MANAGER, "fork() failed to run [%s]", processPtr->processName);
    }

    return NULL;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

 /*
  *********************** End of file ******************************************
 */
