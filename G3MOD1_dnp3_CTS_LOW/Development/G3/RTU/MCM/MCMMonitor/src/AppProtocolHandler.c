/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCMMonitor
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Communicates with other MCM Applications over a socket
 *
 *    CURRENT REVISION
 *
 *               $Revision::               $: (Revision of last commit)
 *               $Author::                 $: (Author of last commit)
 *       \date   $Date::                   $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25/07/13      andrews_s   Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/un.h>      // AF_UNIX

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "LogMessage.h"
#include "Main.h"
#include "AppProtocolHandler.h"
#include "MonitorProtocol.h"
#include "IOManager.h"
#include "crc32.h"
#include "timeOperations.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
#define NSEC_PER_SEC      1000000000
#define USEC_PER_SEC      1000000
#define NSEC_PER_MSEC     1000000

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

lu_int32_t initExtSocket(lu_int8_t *name, lu_bool_t blocking)
{
    lu_int32_t sockFd;
    lu_int32_t sockFlags;
    lu_int32_t optVal;

    struct sockaddr_un servAddr;

    // Prevent exiting when writing on closed socket
    signal(SIGPIPE, SIG_IGN);

    // Remove previous AF socket
    unlink(name);

    // Create a local socket
    if ((sockFd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0)
    {
        logMessageError(SUBSYSTEM_ID_MT_HANDLER, "Could not create socket errno:[%d]", errno);
        return sockFd;
    }

    memset(&servAddr, 0, sizeof(struct sockaddr_un));
    servAddr.sun_family = AF_UNIX;
    strncpy(servAddr.sun_path, name, sizeof(servAddr.sun_path) - 1);

    // Reuse address - AF_INET Only??
    setsockopt(sockFd, SOL_SOCKET, SO_REUSEADDR, &optVal, sizeof(optVal));

    if (blocking == LU_FALSE)
    {
        // Set to non-blocking mode
        sockFlags = fcntl(sockFd, F_GETFL);
        fcntl(sockFd, F_SETFL, sockFlags | O_NONBLOCK);
    }

    if (bind(sockFd, (struct sockaddr *) &servAddr, sizeof(servAddr)) != 0)
    {
        logMessageError(SUBSYSTEM_ID_MT_HANDLER, "Bind Error sockfd:[%d] errno:[%d]", sockFd, errno);
        close(sockFd);
        return -1;
    }

    return sockFd;
}

lu_int32_t acceptExtConnection(lu_int32_t sockFd, lu_int32_t *conFd,
                lu_int32_t timeout)
{
    fd_set readfds;
    lu_int16_t maxfd;
    struct timeval tv;
    lu_int32_t ret;

    // Set the timeout
    tv.tv_sec = timeout / 1000;
    tv.tv_usec = (timeout % 1000) * 1000;

    maxfd = sockFd + 1;
    FD_ZERO(&readfds);
    FD_SET(sockFd, &readfds);

    *conFd = -1;

    ret = select(maxfd, &readfds, NULL, NULL, &tv);
    if (ret > 0)
    {
        if (FD_ISSET(sockFd, &readfds) > 0)
        {
            if ((*conFd = accept(sockFd, NULL, NULL)) < 0)
            {
                if ((errno != EAGAIN) && (errno != EWOULDBLOCK))
                {
                    logMessageError(SUBSYSTEM_ID_MT_HANDLER, "Accept Error sockfd:[%d] errno:[%d]", sockFd, errno);
                    return -1;
                }
            }
        }
    }
    else if (ret < 0)
    {
        return -1;
    }

    return 0;
}

lu_int32_t recvExtPacket(lu_int32_t fd, lu_int8_t *buff, lu_uint32_t buffLen,
                lu_int32_t timeout)
{
    //TODO - SKA - improve this mechanism, it's a bit clunky

    AppMsgStr *msg = NULL;
    lu_int32_t packetSize;
    lu_int32_t payloadSize;
    lu_uint32_t rxCRC32; // Received CRC
    lu_uint32_t calcCRC32; // Calculated CRC
    lu_int32_t timeoutCopy;

    timeoutCopy = timeout;

    if ((buff == NULL) || (buffLen < sizeof(AppMsgHeaderStr)))
    {
        return -2;
    }

    // TODO - SKA - recv() until STX is found or timeout reached

    if ((packetSize = recvExtChunk(fd, buff, sizeof(AppMsgHeaderStr),
                    &timeoutCopy)) > 0)
    {
        if (*buff == XMSG_STX)
        {
            msg = (AppMsgStr*) buff;
            if (msg->header.payLoadLen > 0)
            {
                if (buffLen
                                < (sizeof(AppMsgHeaderStr)
                                                + msg->header.payLoadLen))
                {
                    logMessageError(SUBSYSTEM_ID_MT_HANDLER,
                                    "Supplied buffer too small Got [%d] need [%d]!!!",
                                    buffLen,
                                    sizeof(AppMsgHeaderStr)
                                                    + msg->header.payLoadLen);
                    return -2;
                }

                // TODO - SKA - Improve the way packets are read
                // Sometimes the header gets read just within the timeout, leaving no time for the payload to be read
                timeoutCopy = timeout;

#if 0
                if ((ret = recvExtChunk(fd, buff + sizeof(AppMsgHeaderStr), msg->header.payLoadLen, &timeoutCopy)) == msg->header.payLoadLen)
                {
                    packetSize += ret;
                }
                else
                {
                    return -2;
                }
#endif

                // If we received the header we want to have a go at receiving the payload
                payloadSize = recvExtChunk(fd, buff + sizeof(AppMsgHeaderStr),
                                msg->header.payLoadLen, &timeoutCopy);
                if (payloadSize > 0)
                {
                    packetSize += payloadSize;
                }
                else
                {
                    logMessageError(SUBSYSTEM_ID_MT_HANDLER,
                                    "Expected payload, but failed to receive it (error %i) payload size expected=%i.",
                                    payloadSize, msg->header.payLoadLen);
                    return -5;
                }
            }
        }
        else
        {
            logMessageError(SUBSYSTEM_ID_MT_HANDLER,
                            "Malformed packet, expected [0x%02u] got [0x%02u]!!!",
                            (lu_uint8_t) XMSG_STX, (lu_uint8_t) *buff);
            return -4;
        }

        // Check CRC
#if DEBUG
        fprintf(stdout, "RX: ");
        for (int i = 0; i < packetSize; i++)
        {
            fprintf(stdout, "%02X ", (unsigned char) (buff[i] & 0xff));
        }
        fprintf(stdout, " - [raw]\n");
#endif

        // Initialise the received CRC to 0 before recalculating the CRC
        rxCRC32 = msg->header.crc32;
        msg->header.crc32 = (lu_uint32_t) 0;

        // Recalculate the CRC
        crc32_calc32((lu_uint8_t *) buff, packetSize, &calcCRC32);

#if DEBUG
        fprintf(stdout, "RC: ");
        for (int i = 0; i < packetSize; i++)
        {
            fprintf(stdout, "%02X ", (unsigned char) (buff[i] & 0xff));
        }
        fprintf(stdout, " - [%s]\n", (rxCRC32 == calcCRC32) ? "OK" : "CRC ERR!");
#endif

        if (rxCRC32 != calcCRC32)
        {
            logMessageError(SUBSYSTEM_ID_MT_HANDLER,
                            "CRC mismatch RX:[0x%08x] CALC:[0x%08x] PacketSize:[%d]!!!",
                            rxCRC32, calcCRC32, packetSize);

            return -3;
        }
    }

    return packetSize;
}

lu_int32_t recvExtChunk(lu_int32_t fd, lu_int8_t *buff, lu_uint32_t reqBytes,
                lu_int32_t *timeoutPtr)
{
    lu_int32_t ret;
    fd_set masterReadSet; //Set of file descriptors for read
    fd_set readfds; //copy of the set -- to be modified by select()
    lu_int16_t maxfd;
    struct timeval masterTv;

    struct timespec startTs;
    struct timespec endTs;
    struct timespec elapsed;

    lu_int32_t rxBytes = 0;

    if (buff == NULL)
    {
        return -2;
    }

    maxfd = fd + 1;
    FD_ZERO(&masterReadSet);
    FD_SET(fd, &masterReadSet);

    // Set up the initial timeout value for the select()
    masterTv.tv_sec = *timeoutPtr / 1000;
    masterTv.tv_usec = (*timeoutPtr % 1000) * 1000;

    while ((rxBytes < (lu_int32_t) reqBytes) && (masterTv.tv_sec >= 0)
                    && (masterTv.tv_usec >= 0))
    {
        clock_gettimespec(&startTs);

        readfds = masterReadSet;
        ret = select(maxfd, &readfds, NULL, NULL, &masterTv);

        if (ret > 0)
        {
            if (FD_ISSET(fd, &readfds) > 0)
            {
                ret = recv(fd, (void *) (buff + rxBytes), reqBytes - rxBytes,
                                0);
                rxBytes += ret;
                if (ret == 0)
                {
                    return -1;
                }
            }
        }
        else if (ret == 0)
        {
            // Timeout
            return 0;
        }
        else
        {
            // Error
            logMessageInfo(SUBSYSTEM_ID_MT_HANDLER,
                            "Select() error on fd:[%d]!", fd);

            return -1;
        }

        clock_gettimespec(&endTs);

        elapsed.tv_sec = (endTs.tv_sec - startTs.tv_sec);
        elapsed.tv_nsec = (endTs.tv_nsec - startTs.tv_nsec);
        timespec_normalise(&elapsed);

        // Adjust the master timeval timeout for the next select
        masterTv.tv_sec -= elapsed.tv_sec;
        masterTv.tv_usec -= (elapsed.tv_nsec / 1000);
        timeval_normalise(&masterTv);

#if DEBUG
        printf("took %ld.%06lu; time left %ld.%06lu\n", elapsed.tv_sec, elapsed.tv_nsec/1000, masterTv.tv_sec, masterTv.tv_usec);
#endif
    }

    // Adjust timeout value
    *timeoutPtr = masterTv.tv_sec * 1000;
    *timeoutPtr += (masterTv.tv_usec / 1000);

    // It would be invalid to return a negative timeout
    if (*timeoutPtr < 0)
    {
        *timeoutPtr = 0;
    }

    return rxBytes;
}

lu_int16_t sendExtPacket(lu_int32_t fd, AppMsgStr *outXMsg)
{
    lu_int16_t sentBytes;
    lu_uint8_t *sendBuff;

    if (outXMsg == NULL)
    {
        return -1;
    }

    // Insert the CRC
    insertCRC(outXMsg);

    if ((sendBuff = calloc(sizeof(AppMsgHeaderStr) + outXMsg->header.payLoadLen,
                    sizeof(lu_uint8_t))) == NULL)
    {
        logMessageInfo(SUBSYSTEM_ID_DATA_PORT, "IP Set request");

        return -1;
    }

    // Make a local copy of the buffer so we can send it in one chunk
    memcpy(sendBuff, outXMsg, sizeof(AppMsgHeaderStr));

    if ((outXMsg->header.payLoadLen > 0) && (outXMsg->payload != NULL))
    {
        memcpy(sendBuff + sizeof(AppMsgHeaderStr), outXMsg->payload, outXMsg->header.payLoadLen);
    }

    // Send the buffer
    sentBytes = send(fd, (void *) sendBuff, sizeof(AppMsgHeaderStr) + outXMsg->header.payLoadLen, 0);

    if (sendBuff != NULL)
    {
        free(sendBuff);
    }

    return sentBytes;
}

lu_int32_t insertCRC(AppMsgStr *outXMsg)
{
    lu_uint16_t packetLen = 0;
    lu_uint32_t calcCRC32 = 0;
    lu_uint8_t *ptr;
    lu_uint16_t i;

    if (outXMsg == NULL)
    {
        return -1;
    }

    // Clear the CRC prior to calculation
    outXMsg->header.crc32 = (lu_uint32_t) 0x00000000UL;

    packetLen = sizeof(AppMsgHeaderStr) + outXMsg->header.payLoadLen;

    // Initialise the CRC
    crc32_init(&calcCRC32);

    // As the AppMsgStr structure is not in contiguous memory we will have
    // to calculate the CRC byte by byte across the header and payload

    // Start calculating the header portion
    ptr = (lu_uint8_t *) outXMsg;
    for (i = 0; (ptr != NULL) && (i < sizeof(AppMsgHeaderStr)); i++)
    {
        crc32_byte(*(ptr++), &calcCRC32);
    }

    // Continue calculating the payload portion
    ptr = (lu_uint8_t *) outXMsg->payload;
    for (i = 0; (ptr != NULL) && (i < outXMsg->header.payLoadLen); i++)
    {
        crc32_byte(*(ptr++), &calcCRC32);
    }

    // Set the CRC
    calcCRC32 ^= 0XFFFFFFFFL;
    outXMsg->header.crc32 = calcCRC32;

#if DEBUG
    printf("Mon: TX: CRC Calc [0x%08X]\n", calcCRC32);
#endif

    return 0;
}

lu_int32_t handleExtMsg(lu_int32_t extConfd, mqd_t mainMqFd, AppMsgStr *inMsg)
{
    // Variables extracted from messages
    lu_uint8_t channel;
    lu_uint8_t OLRState;
    lu_int32_t ledState;

    lu_int32_t exitCode;
    lu_uint32_t timeoutMs;

    lu_int8_t action;
    lu_int8_t reason;

    lu_bool_t boolVal;
    lu_float32_t floatVal;
    lu_uint8_t flagsVal;

    if (inMsg == NULL)
    {
        return XMSG_FLAGS_NACK;
    }

    switch (inMsg->header.cmd)
    {
        case XMSG_CMD_GET_DI_VAL:
            if (decodeReqDIMsg(inMsg, &channel) == 0)
            {
                if (ioManGetDigitalInput(channel, &boolVal, &flagsVal) == 0)
                {
                    sendRepDIMsg(extConfd, channel, flagsVal, boolVal);
                }
                else
                {
                    sendRepDIMsg(extConfd, channel, IO_FLAG_OFF_LINE, LU_FALSE);
                }
            }
        break;

        case XMSG_CMD_GET_AI_VAL:
            if (decodeReqAIMsg(inMsg, &channel) == 0)
            {
                if (ioManGetAnalogueInput(channel, &floatVal, &flagsVal) == 0)
                {
                    sendRepAIMsg(extConfd, channel, flagsVal, floatVal);
                }
                else
                {
                    logMessageWarn(SUBSYSTEM_ID_DATA_PORT, "Invalid AI channel requested [%d]", channel);
                    sendRepAIMsg(extConfd, channel, IO_FLAG_OFF_LINE, 0.0);
                }
            }
        break;

        case XMSG_CMD_GET_APP_VER:
            sendRepAppVerMsg(extConfd);
        break;

        case XMSG_CMD_SET_IP:
            logMessageInfo(SUBSYSTEM_ID_DATA_PORT, "IP Set request");
            decodeSetIPMsg(inMsg);
        break;

        case XMSG_CMD_SET_NTP:
            logMessageInfo(SUBSYSTEM_ID_DATA_PORT, "NTP Set request");
            decodeSetNTPMsg(inMsg);
        break;

        case XMSG_CMD_SET_PPP:
            logMessageInfo(SUBSYSTEM_ID_DATA_PORT, "PPP Set request");
        break;

        case XMSG_CMD_SET_OK_LED:
            decodeSetOKState(inMsg, &ledState);

            // Send the request to update threadInfo for THREAD_APP
            if (sendRepStateQMsg(threadInfo[THREAD_DATA_COMS].threadMqFd,
                            mainMqFd, THREAD_APP, ledState) < 0)
            {
                logMessageError(SUBSYSTEM_ID_DATA_PORT,
                                "Error sending REP STATE change to MAIN!");
            }

        break;

        case XMSG_CMD_SET_OLR_VAL:
            decodeSetOLRState(inMsg, &OLRState);

            // Send the request to update threadInfo for THREAD_APP
            if (sendReqSetOLRQMsg(threadInfo[THREAD_DATA_COMS].threadMqFd,
                            threadInfo[THREAD_IOMAN].threadMqFd, OLRState) < 0)
            {
                logMessageError(SUBSYSTEM_ID_CONTROL_PORT,
                                "Error sending SET OLR to IOManager!");
            }
        break;

        case XMSG_CMD_REQ_ALIVE:
            sendEmptyMsg(extConfd, XMSG_REP_REQ_ALIVE);
        break;

        // Ignore
        case XMSG_REP_REQ_ALIVE:
        case QMSG_CMD_REP_SET_OLR:
        break;

        case XMSG_CMD_NOTIFY_EXIT:
            decodeNotifyExitMsg(inMsg, &exitCode, &timeoutMs);

            logMessageInfo(SUBSYSTEM_ID_DATA_PORT,
                            "DECODE NOTIFY EXIT Exit Code:[%d]", exitCode);

            if (sendNotifyExitQMsg(threadInfo[THREAD_DATA_COMS].threadMqFd,
                            mainMqFd, inMsg->header.appId, exitCode, timeoutMs)
                            < 0)
            {
                logMessageError(SUBSYSTEM_ID_DATA_PORT,
                                "Error sending EXITCODE change to MAIN!");
            }
        break;

        case XMSG_CMD_REQ_EXIT:
            decodeReqExitMsg(inMsg, &action, &reason);
            logMessageWarn(SUBSYSTEM_ID_DATA_PORT,
                            "Exit request action:[%d] reason:[%d]", action,
                            reason);

            if (sendReqShutdownQMsg(threadInfo[THREAD_DATA_COMS].threadMqFd,
                            mainMqFd, action, reason) < 0)
            {
                logMessageError(SUBSYSTEM_ID_DATA_PORT,
                                "Error sending REQ EXIT change to MAIN!");
            }
        break;

        case XMSG_CMD_HTTP_RESTART:
            logMessageInfo(SUBSYSTEM_ID_DATA_PORT, "Restarting HTTP...");
            decodeRestartHTTP(inMsg);
        break;

        case XMSG_CMD_SET_KICK_WDOG:
            decodeReqKickWdogMsg(inMsg, &boolVal);
            logMessageInfo(SUBSYSTEM_ID_DATA_PORT, "Kick WDog Command: [%d]", boolVal);

            sendReqWDogKickQMsg(threadInfo[THREAD_DATA_COMS].threadMqFd,
                                threadInfo[THREAD_WDOG].threadMqFd,
                                boolVal);
        break;


        default:
            logMessageWarn(SUBSYSTEM_ID_DATA_PORT,
                            "Unexpected msg received. APPID[0x%02X] CMD[0x%02X] PLLEN[%d]",
                            inMsg->header.appId, inMsg->header.cmd,
                            inMsg->header.payLoadLen);

            return XMSG_FLAGS_NACK;
    }

    return inMsg->header.cmd;
}

lu_int32_t sendEmptyMsg(lu_int32_t fd, lu_int16_t cmd)
{
    AppMsgStr msg;

    msg.header.stx = XMSG_STX;
    msg.header.appId = APP_ID_MCMMONITOR;
    msg.header.cmd = cmd;
    msg.header.apiVersion.major = API_VER_MAJOR;
    msg.header.apiVersion.minor = API_VER_MINOR;
    msg.header.crc32 = 0x00000000;
    msg.header.payLoadLen = 0;

    msg.payload = NULL;

    // Insert the CRC
    insertCRC(&msg);

#if DEBUG
    printf("TX: PING!\n");
#endif

    // Send the message
    return (sendExtPacket(fd, &msg) > 0) ? 0 : -1;
}

lu_int32_t sendRepDIMsg(lu_int32_t fd, lu_int8_t channel, lu_uint8_t flags,
                lu_bool_t value)
{
    AppMsgStr msg;
    PLGetDIValStr payload;

    msg.header.stx = XMSG_STX;
    msg.header.appId = APP_ID_MCMMONITOR;
    msg.header.cmd = XMSG_REP_GET_DI_VAL;
    msg.header.apiVersion.major = API_VER_MAJOR;
    msg.header.apiVersion.minor = API_VER_MINOR;
    msg.header.crc32 = 0x00000000;
    msg.header.payLoadLen = sizeof(PLGetDIValStr);

    payload.channel = channel;
    payload.flags = flags;
    payload.value = value;

    msg.payload = (PLGetDIValStr *) &payload;

    // Send the message
    return (sendExtPacket(fd, &msg) > 0) ? 0 : -1;
}

lu_int32_t sendRepAIMsg(lu_int32_t fd, lu_int8_t channel, lu_uint8_t flags,
                lu_float32_t value)
{
    AppMsgStr msg;
    PLGetAIValStr payload;

    msg.header.stx = XMSG_STX;
    msg.header.appId = APP_ID_MCMMONITOR;
    msg.header.cmd = XMSG_REP_GET_AI_VAL;
    msg.header.apiVersion.major = API_VER_MAJOR;
    msg.header.apiVersion.minor = API_VER_MINOR;
    msg.header.crc32 = 0x00000000;
    msg.header.payLoadLen = sizeof(PLGetAIValStr);

    payload.channel = channel;
    payload.flags = flags;
    payload.value = value;

    msg.payload = (PLGetAIValStr *) &payload;

    // Send the message
    return (sendExtPacket(fd, &msg) > 0) ? 0 : -1;
}

lu_int32_t sendRepAppVerMsg(lu_int32_t fd)
{
    AppMsgStr msg;
    PLAppVerStr payload;

    msg.header.stx = XMSG_STX;
    msg.header.appId = APP_ID_MCMMONITOR;
    msg.header.cmd = XMSG_REP_GET_APP_VER;
    msg.header.apiVersion.major = API_VER_MAJOR;
    msg.header.apiVersion.minor = API_VER_MINOR;
    msg.header.crc32 = 0x00000000;
    msg.header.payLoadLen = sizeof(PLAppVerStr);

    payload.relType = MCM_MONITOR_SOFTWARE_VERSION_RELTYPE;
    payload.version.major = MCM_MONITOR_SOFTWARE_VERSION_MAJOR;
    payload.version.minor = MCM_MONITOR_SOFTWARE_VERSION_MINOR;
    payload.patch = MCM_MONITOR_SOFTWARE_VERSION_PATCH;

    msg.payload = (PLAppVerStr *) &payload;

    // Send the message
    return (sendExtPacket(fd, &msg) > 0) ? 0 : -1;
}

// TODO SKA - Remove
#if 0
lu_int32_t sendCmdSetAlarm(lu_int32_t fd, XMSG_ALARM alarm)
{
    AppMsgStr msg;
    PLSetAlarmStr payload;

    msg.header.stx = XMSG_STX;
    msg.header.appId = APP_ID_MCMMONITOR;
    msg.header.cmd = XMSG_CMD_SET_ALARM;
    msg.header.apiVersion.major = API_VER_MAJOR;
    msg.header.apiVersion.minor = API_VER_MINOR;
    msg.header.crc32 = 0x00000000;
    msg.header.payLoadLen = sizeof(PLSetAlarmStr);

    payload.alarmState = (lu_int8_t) alarm;

    msg.payload = (PLSetAlarmStr *) &payload;

    // Send the message
    return (sendExtPacket(fd, &msg) > 0) ? 0 : -1;
}
#endif

lu_int32_t sendExitMsg(lu_int32_t fd, XMSG_EXIT_ACTION action,
                XMSG_EXIT_CODE reason)
{
    AppMsgStr msg;
    PLExitStr payload;

    msg.header.stx = XMSG_STX;
    msg.header.appId = APP_ID_MCMMONITOR;
    msg.header.cmd = XMSG_CMD_REQ_EXIT;
    msg.header.apiVersion.major = API_VER_MAJOR;
    msg.header.apiVersion.minor = API_VER_MINOR;
    msg.header.crc32 = 0x00000000;
    msg.header.payLoadLen = sizeof(PLExitStr);

    payload.action = (lu_int8_t) action;
    payload.reason = (lu_int8_t) reason;

    msg.payload = (PLExitStr *) &payload;

    // Send the message
    return (sendExtPacket(fd, &msg) > 0) ? 0 : -1;
}

lu_int32_t decodeReqDIMsg(AppMsgStr *msgPtr, lu_uint8_t *channelPtr)
{
    PLGetDIValStr *payloadPtr;

    if ((msgPtr == NULL) || (channelPtr == NULL))
    {
        return -1;
    }

    payloadPtr = (PLGetDIValStr *) &(msgPtr->payload);

    *channelPtr = payloadPtr->channel;

    return 0;
}

lu_int32_t decodeReqAIMsg(AppMsgStr *msgPtr, lu_uint8_t *channelPtr)
{
    PLGetAIValStr *payloadPtr;

    if ((msgPtr == NULL) || (channelPtr == NULL))
    {
        return -1;
    }

    payloadPtr = (PLGetAIValStr *) &(msgPtr->payload);

    *channelPtr = payloadPtr->channel;

    return 0;
}

lu_int32_t decodeNotifyExitMsg(AppMsgStr *msgPtr, lu_int32_t *exitCodePtr,
                lu_uint32_t *timeoutMsPtr)
{
    PLNotifyExitStr *payloadPtr;

    if ((msgPtr == NULL) || (exitCodePtr == NULL) || (timeoutMsPtr == NULL))
    {
        return -1;
    }

    payloadPtr = (PLNotifyExitStr *) &(msgPtr->payload);

    *exitCodePtr = payloadPtr->exitCode;
    *timeoutMsPtr = payloadPtr->timeoutMs;

    return 0;
}

lu_int32_t decodeSetIPMsg(AppMsgStr *msgPtr)
{
    PLSetIPStr *payloadPtr;
    lu_char_t ipAddr[16];
    lu_char_t ipMask[16];
    lu_char_t ipGw[16];
    lu_char_t commandBuf[100];
    lu_int32_t ret = 0;

    if (msgPtr == NULL)
    {
        return -1;
    }

    payloadPtr = (PLSetIPStr *) &(msgPtr->payload);

    ret += ipToString(payloadPtr->ipAddr, ipAddr, 16);
    ret += ipToString(payloadPtr->mask, ipMask, 16);
    ret += ipToString(payloadPtr->gwAddr, ipGw, 16);


    if (ret == 0)
    {
        ret = -1;
        if (snprintf(commandBuf, 100, CMD_SET_IP_STRING, payloadPtr->device, ipAddr, ipMask, ipGw) > 0)
        {
            logMessageInfo(SUBSYSTEM_ID_MT_PROTOCOL,
                            "About to issue command [%s]", commandBuf);
            ret = system(commandBuf);
        }
    }

    return ret;
}

lu_int32_t decodeSetNTPMsg(AppMsgStr *msgPtr)
{
    PLSetNTStr *payloadPtr;
    lu_char_t ipAddr[16];
//    lu_char_t   commandBuf[100];
    lu_int32_t ret = 0;

    if (msgPtr == NULL)
    {
        return -1;
    }

    payloadPtr = (PLSetNTStr *) &(msgPtr->payload);

    ret += ipToString(payloadPtr->serverIPAddr, ipAddr, 16);

    // TODO SKA - What do we do with the information?
#if 0
    if (ret == 0)
    {
        ret = -1;
        if (snprintf(commandBuf, 100, "/sbin/setntp -v -i %s", ipAddr) > 0)
        {
            logMessageInfo(SUBSYSTEM_ID_MT_PROTOCOL, "About to issue command [%s]", commandBuf);
            ret = system(commandBuf);
        }
    }
#endif

    return ret;
}

lu_int32_t decodeReqExitMsg(AppMsgStr *msgPtr, lu_int8_t *actionPtr,
                lu_int8_t *reasonPtr)
{
    PLExitStr *payloadPtr;

    if ((msgPtr == NULL) || (actionPtr == NULL) || (reasonPtr == NULL))
    {
        return -1;
    }

    payloadPtr = (PLExitStr *) &(msgPtr->payload);

    *actionPtr = payloadPtr->action;
    *reasonPtr = payloadPtr->reason;

    return 0;
}

lu_int32_t decodeReqKickWdogMsg(AppMsgStr *msgPtr, lu_bool_t *startPtr)
{
    PLSetKickWdogStr *payloadPtr;

    if ((msgPtr == NULL) || (startPtr == NULL))
    {
        return -1;
    }

    payloadPtr = (PLSetKickWdogStr *) &(msgPtr->payload);

    *startPtr = payloadPtr->kick;

    return 0;
}

#ifdef MON_POWER_SAVE
lu_int32_t decodePowerSaveMsg(AppMsgStr *msgPtr, lu_bool_t *powerStatePtr)
{
    PLSetPowerSaveStr *payloadPtr;

    if ((msgPtr == NULL) || (powerStatePtr == NULL))
    {
        return -1;
    }

    payloadPtr = (PLSetPowerSaveStr *) &(msgPtr->payload);

    *powerStatePtr = payloadPtr->powerState;

    return 0;
}
#endif

lu_int32_t decodeRestartHTTP(AppMsgStr *msgPtr)
{
    if (msgPtr == NULL)
    {
        return -1;
    }

    logMessageInfo(SUBSYSTEM_ID_MT_PROTOCOL, "About to issue command [%s]",
                    CMD_RESTART_HTTP);
    system(CMD_RESTART_HTTP);

    return 0;
}

lu_int32_t decodeSetOKState(AppMsgStr *msgPtr, lu_int32_t *ledStatePtr)
{
    PLSetOKLEDStr *payloadPtr;

    if ((msgPtr == NULL) || (ledStatePtr == NULL))
    {
        return -1;
    }

    payloadPtr = (PLSetOKLEDStr *) &(msgPtr->payload);

    *ledStatePtr = payloadPtr->ledState;

    return 0;
}

lu_int32_t decodeSetOLRState(AppMsgStr *msgPtr, lu_uint8_t *OLRStatePtr)
{
    PLSetOLRStr *payloadPtr;

    if ((msgPtr == NULL) || (OLRStatePtr == NULL))
    {
        return -1;
    }

    payloadPtr = (PLSetOLRStr *) &(msgPtr->payload);

    *OLRStatePtr = payloadPtr->OLRState;

    return 0;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
