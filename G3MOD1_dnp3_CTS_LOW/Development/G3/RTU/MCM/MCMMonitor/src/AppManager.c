/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCMMonitor
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Controls the execution of external processes
 *
 *    CURRENT REVISION
 *
 *               $Revision::               $: (Revision of last commit)
 *               $Author::                 $: (Author of last commit)
 *       \date   $Date::                   $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25/07/13      andrews_s   Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h> //stat

#include <dirent.h>

#include <mqueue.h>
#include <sys/prctl.h> // prctl()

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "timeOperations.h"
#include "LogMessage.h"

#include "Main.h"
#include "Launcher.h"
#include "AppManager.h"
#include "IOManager.h"
#include "MonitorProtocol.h"
#include "MonitorPointMapping.h"
#include "CANUtils.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define MAX_PROCESS_RETRIES  3
#define NUM_PROCESSES        3

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
// Error Codes
typedef enum
{
    APPMAN_ERROR_NONE           = 0,
    APPMAN_ERROR_TOOMANYRETRIES = 1,
    APPMAN_ERROR_NOEXEC         = 2,
    APPMAN_ERROR_ACCESS         = 3,
    APPMAN_ERROR_NULL           = 4
} APPMAN_ERROR;

// Define a structure to hold a lookup table of Exit Codes to Applications
typedef struct ExitCodeToAppDef
{
    APP_EXIT_CODE       exitCode;
    APP_ID              applicationID;
    const lu_char_t   **applicationWithArgs;  /* Arguments for process */

} ExitCodeToAppStr;


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static pid_t appManGetPid(ProcessType *procSearch);
#if 0
static void appManGetApps(void);
#endif
static void appManDecodeQMsq(QMsgType msgIn);
static void appManInitProcList(void);
static void appManKillProcess(ProcessType *procPtr, lu_int32_t exitCode);
static APPMAN_ERROR appManCheckProcess(lu_int32_t process);
static APPMAN_ERROR appManProcessExitCode(lu_int32_t process);
static APPMAN_ERROR appManStartProcess(lu_int32_t process);
static APPMAN_ERROR appManRevertToBackupUpdater(void);
static lu_char_t *appManStripPath(lu_char_t *pathPtr);
static APPMAN_ERROR appManCreateCmdLine(ProcessType *processPtr);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

const lu_char_t MonitorWorkingPath[]     = "/usr/local/gemini/application";
const lu_char_t MonitorConfigPath[]      = "/usr/local/gemini/etc/apps";

const lu_char_t *MCMApplication[]        = { "MCMBoard.axf",   NULL };
const lu_char_t *MCMUpdater[]            = { "MCMUpdater.axf", NULL };
const lu_char_t *MCMUpdaterBackup[]      = { "/usr/local-ro/gemini/application/MCMUpdater.axf", NULL };
const lu_char_t *MCMUpdaterHAT[]         = { "MCMUpdater.axf", "-f", NULL };

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static mqd_t            mqFd;
static mqd_t            mainMqFd;
static ProcessType     *procList;
static OK_LEDSTATE      myState     = OK_LEDSTATE_INITIALISING;
static pthread_attr_t   threadAttr;    // thread attribute
static lu_int32_t       CANSocketFd = -1;

// The process order should match the applications specified in the ENUM - APP_EXIT_CODE
// defined in MonitorProtocol.h
static ExitCodeToAppStr ExitCodeToAppMapping[NUM_PROCESSES] =
{
    { APP_EXIT_CODE_START_MCM_APPLICATION, APP_ID_MCMAPP,     MCMApplication },
    { APP_EXIT_CODE_START_MCM_UPDATER,     APP_ID_MCMUPDATER, MCMUpdater     },
    { APP_EXIT_CODE_START_MCM_UPDATER_HAT, APP_ID_MCMUPDATER, MCMUpdaterHAT  }
};

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

void *appManagerThread(void *dummy)
{
    LU_UNUSED(dummy);
    OK_LEDSTATE      myLastState = OK_LEDSTATE_INITIALISING;
    lu_int32_t       currentProcess;
    lu_bool_t        justStarted = LU_TRUE;


    // Set the thread name (ps -T)  - ignore errors
    prctl(PR_SET_NAME, "AppManager", 0, 0, 0);

    // Initialise the process monitoring status array
    appManInitProcList();

    // Initialise signal masks
    initSignalMasks();

    // Initialise Thread Attributes to
    // For the main launched application allow it to run as a DETACHED thread with all the
    // default thread attributes.  Trying to restrict the stack size or priority has leas
    // to problems with the main app using 100% CPU.
    // So, do not call initThreadAttributes() here
    if (pthread_attr_setdetachstate(&threadAttr, PTHREAD_CREATE_DETACHED) != 0)
    {
        logMessageFatal(SUBSYSTEM_ID_MAIN,
                        "Unable to pthread_attr_setdetachstate()!");
    }

    // Initialise Message Queues
    mqFd     = connectMsgQueue(threadInfo[THREAD_APPMAN].mqName);
    mainMqFd = connectMsgQueue(threadInfo[THREAD_MAIN].mqName);
    if ((mqFd < 0) || (mainMqFd < 0))
    {
        logMessageFatal(SUBSYSTEM_ID_APP_MANAGER,
                        "Error initialising Msg Queues [%d] [%d]", mqFd, mainMqFd);
        exit(1);
    }

    // Get a handle to CAN1
    if ((CANSocketFd = CANCreateSocket(CAN1)) < 0)
    {
        logMessageError(SUBSYSTEM_ID_APP_MANAGER, "Error creating CAN socket!!");
    }

    // Main Loop
    while(!Closedown)
    {
        // Do some process monitoring....
        for (currentProcess = 0; (currentProcess < NUM_PROCESSES) && (!Closedown); currentProcess++)
        {
            // If this thread is marked as started, check its status
            if (procList[currentProcess].started == LU_TRUE)
            {
                appManCheckProcess(currentProcess);
            }
            else if (procList[currentProcess].startRequest == LU_TRUE)
            {
                if (appManStartProcess(currentProcess) == APPMAN_ERROR_TOOMANYRETRIES)
                {
                    // If we have too many tries running the updater we must revert to the backup version
                    if (ExitCodeToAppMapping[currentProcess].applicationID == APP_ID_MCMUPDATER)
                    {
                        // Too many retries for Updater - revert to backup
                        appManRevertToBackupUpdater();
                    }
                }
            }
            else if (justStarted == LU_TRUE)
            {
                // If the process is neither STARTED or REQUESTED to START it should not be running
                // See if it is running and deal with it
                if ((procList[currentProcess].pid = appManGetPid(&procList[currentProcess])) != NO_PID)
                {
                    // This process really should not be running!
                    logMessageWarn(SUBSYSTEM_ID_APP_MANAGER, "Process [%s] should NOT be running.  Killing it!", procList[currentProcess].processName[0]);
                    appManKillProcess(&(procList[currentProcess]), SIGTERM);
                    procList[currentProcess].pid = NO_PID;
                }
            }

            // Handle messages coming in on the message queue
            if (processMsgQueue(mqFd, &appManDecodeQMsq, 750) < 0)
            {
                if ((mqFd = connectMsgQueue(threadInfo[THREAD_APPMAN].mqName)) < 0)
                {
                    logMessageFatal(SUBSYSTEM_ID_APP_MANAGER, "Error sending Registration msg to Main");
                    exit(1);
                }
            }

            // If parent dies we should die too (Parent PID goes to 1)
            if (getppid() == 1)
            {
                logMessageFatal(SUBSYSTEM_ID_APP_MANAGER, "Parent process has died!");
                Closedown = 1;
            }

        } // for

        // If there's been a change of state we must notify the main thread
        if (myState != myLastState)
        {
            if (sendRepStateQMsg(threadInfo[THREAD_APPMAN].threadMqFd, mainMqFd, THREAD_APPMAN, myState) < 0)
            {
                logMessageError(SUBSYSTEM_ID_APP_MANAGER, "Error sending STATE change to MAIN!");
            }

            myLastState = myState;
        }

        // We have now completed one cycle of the monitored processes
        justStarted = LU_FALSE;

    } // while

    logMessageInfo(SUBSYSTEM_ID_APP_MANAGER, "AppManagerThread finishing");

    // Tidy Up
    pthread_attr_destroy(&threadAttr);
    CANCloseSocket(CANSocketFd);

    if (procList != NULL)
    {
        free(procList);
    }

    mq_close(mqFd);
    mq_close(mainMqFd);

    logMessageInfo(SUBSYSTEM_ID_APP_MANAGER, "AppManagerThread finished.  Killing parent...");

    // Kill our parent (last ditch attempt to exit properly)
    kill((getppid()), SIGTERM);

    return NULL;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Check the status of a process that should be running
 *
 *   \param     process  Process Number on the procList array
 *
 *   \return    Error Code
 *
 ******************************************************************************
 */
static APPMAN_ERROR appManCheckProcess(lu_int32_t process)
{
    struct timespec  timeNow;


    // If we have no record of its PID, see if we can find it ourselves
    if (procList[process].pid == NO_PID)
    {
        procList[process].pid = appManGetPid(&procList[process]);

// TODO - SKA - What if the process never gets a PID?
// Implement a startupTimer to allow a predefined time for a process to start and be detected
// The application may have immediately exited without the pid being recorded
        if (procList[process].pid == NO_PID)
        {
            logMessageInfo(SUBSYSTEM_ID_APP_MANAGER, "Process [%s] has no PID!", procList[process].processName[0]);
        }

        return APPMAN_ERROR_NONE;
    }

    clock_gettimespec(&timeNow);

    // If there's an exit timeout, check it hasn't expired
    if (procList[process].exitTimeout.tv_sec > 0)
    {
        // Check to see if the exitTimeout has expired!
        if (checkTimeout(&timeNow, &(procList[process].exitTimeout)) == LU_TRUE)
        {
            logMessageWarn(SUBSYSTEM_ID_APP_MANAGER, "Process [%s] notified ExitCode:[%d] but timed out.  Kill SIGINT pid:[%d]!!\n",
                            procList[process].processName[0], procList[process].notifiedExitCode, procList[process].pid);

            appManKillProcess(&(procList[process]), procList[process].notifiedExitCode);
        }
    }

    // If there's an exit timeout, check it hasn't expired
    if (procList[process].startCounterResetTime.tv_sec > 0)
    {
        // Check to see if the exitTimeout has expired!
        if (checkTimeout(&timeNow, &(procList[process].startCounterResetTime)) == LU_TRUE)
        {
            // Reset startCounter variables
            procList[process].startCounter = 0;
            procList[process].startCounterResetTime.tv_sec  = 0;
            procList[process].startCounterResetTime.tv_nsec = 0;

            logMessageInfo(SUBSYSTEM_ID_APP_MANAGER, "Process [%s] is stable, reset startCounter\n",
                            procList[process].processName[0]);
        }
    }


    // If the PID is running, that's great; return
    if (kill(procList[process].pid, 0) == 0)
    {
        return APPMAN_ERROR_NONE;
    }

    // If the PID no longer exists, examine the exit code
    return appManProcessExitCode(process);
}

/*!
 ******************************************************************************
 *   \brief Perform functions related to the exit code of a process
 *
 *   \param     process  Process Number on the procList array
 *
 *   \return    Error Code
 *
 ******************************************************************************
 */
static APPMAN_ERROR appManProcessExitCode(lu_int32_t process)
{
    lu_int16_t       i;
    lu_int32_t       exitCode;

    // Send IOManager a msg to clear wdog restarted channel
    if (sendReqSetDIQMsg(threadInfo[THREAD_APPMAN].threadMqFd, threadInfo[THREAD_IOMAN].threadMqFd, MCM_MON_DI_WDOG_FAILED, LU_FALSE) < 0)
    {
        logMessageError(SUBSYSTEM_ID_APP_MANAGER, "Error sending Watchdog Restart change to MAIN!");
    }
    logMessageInfo(SUBSYSTEM_ID_APP_MANAGER, "Cleared Watchdog Restart");

    // Make a copy of exitCode in case it changes
    exitCode = procList[process].exitCode;

    // Reset the process attributes
    procList[process].startRequest                  = LU_FALSE;
    procList[process].started                       = LU_FALSE;
    procList[process].pid                           = NO_PID;
    procList[process].exitTimeout.tv_sec            = 0;
    procList[process].exitTimeout.tv_nsec           = 0;
    procList[process].startCounterResetTime.tv_sec  = 0;
    procList[process].startCounterResetTime.tv_nsec = 0;


    logMessageInfo(SUBSYSTEM_ID_APP_MANAGER, "Process [%s] has STOPPED running Exit:[%d] Runs:[%d]!!\n",
                    procList[process].processName[0], exitCode, procList[process].startCounter);

    // If the process notified an exit code we should use it
//    if ((exitCode == SIGTERM) && (procList[process].killed == LU_TRUE))
    if (procList[process].notifiedExitCode != -1)
    {
        logMessageInfo(SUBSYSTEM_ID_APP_MANAGER, "Applying notified exit code [%d] from [%s]", procList[process].notifiedExitCode, procList[process].processName[0]);
        exitCode = procList[process].notifiedExitCode;
        procList[process].notifiedExitCode = -1;
    }

    switch(exitCode)
    {
        case APP_EXIT_CODE_RESTART:
            procList[process].startCounter = 0;
            procList[process].startRequest = LU_TRUE;
            logMessageInfo(SUBSYSTEM_ID_APP_MANAGER, "Process [%s] gave EXIT CODE: RESTART PROCESS!\n", procList[process].processName[0]);
            break;

        case APP_EXIT_CODE_REBOOT_RTU:
            procList[process].startRequest = LU_FALSE;
            logMessageWarn(SUBSYSTEM_ID_APP_MANAGER, "Process [%s] gave EXIT CODE: REBOOT RTU!\n", procList[process].processName[0]);

            if (sendReqShutdownQMsg(threadInfo[THREAD_APPMAN].threadMqFd, mainMqFd, QMSG_EXIT_ACTION_REBOOT, QMSG_EXIT_CODE_APP_FAIL) < 0)
            {
              // If we fail to send the msg to the main thread we can initiate clean shutdown of the application here
              logMessageError(SUBSYSTEM_ID_APP_MANAGER, "Error sending RTU REBOOT to main Thread. Setting Closedown = 1!");
              Closedown = LU_TRUE;
            }
            break;

        case APP_EXIT_CODE_SHUTDOWN_RTU:
            procList[process].startRequest = LU_FALSE;
            logMessageWarn(SUBSYSTEM_ID_APP_MANAGER, "Process [%s] gave EXIT CODE: SHUTDOWN RTU!\n", procList[process].processName[0]);

            if (sendReqShutdownQMsg(threadInfo[THREAD_APPMAN].threadMqFd, mainMqFd, QMSG_EXIT_ACTION_SHUTDOWN, QMSG_EXIT_CODE_APP_FAIL) < 0)
            {
              // If we fail to send the msg to the main thread we can initiate clean shutdown of the application here
              logMessageError(SUBSYSTEM_ID_APP_MANAGER, "Error sending SHUTDOWN to main Thread. Setting Closedown = 1!");
              Closedown = LU_TRUE;
            }
            break;

        case APP_EXIT_CODE_EXIT_MCM_MONITOR:
            logMessageWarn(SUBSYSTEM_ID_APP_MANAGER, "Process [%s] gave EXIT CODE: EXIT MONITOR!\n", procList[process].processName[0]);
            Closedown = LU_TRUE;
            break;

            // All other Exit Codes are for running specific processes
        default:
            // Loop around the process list, looking for a startCode that matches the exit code of the current process
            for (i = 0; i < NUM_PROCESSES; i++)
            {
                if (procList[i].startCode == (APP_EXIT_CODE) exitCode)
                {
                    procList[process].startRequest = LU_FALSE;  //Caller does not need to restart
                    procList[i].startRequest = LU_TRUE;
                    procList[i].startCounter = 0;
                    break;
                }
            }
            if(i >= NUM_PROCESSES)
            {
                // If the startCode was not found, the return code is unhandled - assume we restart the application
                // NOTE: The startCounter will increment
                if (procList[process].startRequest != LU_TRUE)
                {
                    logMessageError(SUBSYSTEM_ID_APP_MANAGER, "Unhandled exit code [%d]\n", exitCode);
                    procList[process].startRequest = LU_TRUE;
                }
                break;
            }
    }

    // Signal the IOManager that we are in or out of HAT Mode
    if (exitCode == APP_EXIT_CODE_START_MCM_UPDATER_HAT)
    {
        if (sendHATModeQMsg(threadInfo[THREAD_APPMAN].threadMqFd, mainMqFd, LU_TRUE) < 0)
        {
            logMessageError(SUBSYSTEM_ID_APP_MANAGER, "Error sending HAT Mode status");
        }
    }
    else
    {
        if (sendHATModeQMsg(threadInfo[THREAD_APPMAN].threadMqFd, mainMqFd, LU_FALSE) < 0)
        {
            logMessageError(SUBSYSTEM_ID_APP_MANAGER, "Error sending HAT Mode status");
        }
    }

    return APPMAN_ERROR_NONE;
}


/*!
 ******************************************************************************
 *   \brief Start a process if it's marked for starting
 *
 *   \param     process  Process Number in the procList array
 *
 *   \return    N/A
 *
 ******************************************************************************
 */
static APPMAN_ERROR appManStartProcess(lu_int32_t process)
{
    procList[process].started = LU_FALSE;
    procList[process].startCounter++;

    if (procList[process].startCounter > MAX_PROCESS_RETRIES)
    {
        SET_STATE(myState, OK_LEDSTATE_ALARM);

        logMessageError(SUBSYSTEM_ID_APP_MANAGER, "Too many retries running process [%s]!\n", procList[process].processName[0]);

        // Fool the checkProcess() to run the Updater Application if we have tried to run this application too many times

        procList[process].started              = LU_TRUE;
        procList[process].startRequest         = LU_FALSE;
        procList[process].notifiedExitCode     = APP_EXIT_CODE_START_MCM_UPDATER;
        procList[process].exitCode             = APP_EXIT_CODE_START_MCM_UPDATER;

        // Artificially process the exit Code
        appManProcessExitCode(process);

        return APPMAN_ERROR_TOOMANYRETRIES;
    }

    // Check if the process is already running; we don't want to launch another instance.
    // As we will not be monitoring the exit code of the application we will assume
    // it needs a restart when it exits
    if ((procList[process].pid = appManGetPid(&procList[process])) != NO_PID)
    {
        myState = OK_LEDSTATE_HEALTHY;

        procList[process].started      = LU_TRUE;
        procList[process].startRequest = LU_FALSE;
        procList[process].exitCode     = APP_EXIT_CODE_RESTART;

        // The process is already running - great
        return APPMAN_ERROR_NONE;
    }

    procList[process].pid = NO_PID;

    // Make sure the process has the correct permissions - ignore return code
    chmod(procList[process].processName[0], CHMOD_755);

    // Check the application exists and has execute permissions
    // If not, mark for Restarting and let the retry logic sort things out
    if (access(procList[process].processName[0], F_OK | X_OK) != 0)
    {
        SET_STATE(myState, OK_LEDSTATE_CRITICAL);

        // The file either does not exist or is not executable
        procList[process].exitCode = APP_EXIT_CODE_RESTART;
        logMessageError(SUBSYSTEM_ID_APP_MANAGER , "Error running process [%s] - Does it exist?\n", procList[process].processName[0]);

        return APPMAN_ERROR_NOEXEC;
    }

    // Request that the OLR State is changed to OFF before App Launch
    if (sendReqSetOLRQMsg(threadInfo[THREAD_DATA_COMS].threadMqFd, threadInfo[THREAD_IOMAN].threadMqFd, MCMOLR_STATE_OFF) < 0)
    {
        logMessageError(SUBSYSTEM_ID_APP_MANAGER, "Error sending OLR STATE change to IOManager!");
    }

    myState = OK_LEDSTATE_HEALTHY;
    procList[process].started      = LU_TRUE;
    procList[process].startRequest = LU_FALSE;
    procList[process].exitTimeout.tv_sec = 0;
    procList[process].exitTimeout.tv_nsec = 0;

    /* Set the StartCounterResetTime */
    clock_gettimespec(&procList[process].startCounterResetTime);
    timespec_add_ms(&procList[process].startCounterResetTime, STARTCOUNTER_RESET_TIME_MS);

    logMessageInfo(SUBSYSTEM_ID_APP_MANAGER, "Starting [%s]!\n", procList[process].processName[0]);

    // This will be a DETACHED THREAD, so no JOIN required.
    if (pthread_create(&(procList[process].tid), &threadAttr, &launchThread, &(procList[process])) != 0)
    {
        logMessageFatal(SUBSYSTEM_ID_APP_MANAGER, "Unable to create thread!");
        exit(EXIT_FAILURE);
    }

    // If we are running the Updater, set the LED pattern to SPECIAL1
    if (ExitCodeToAppMapping[process].applicationID == APP_ID_MCMUPDATER)
    {
        // Create a new HTTPS certificate
        logMessageInfo(SUBSYSTEM_ID_APP_MANAGER, "Creating new HTTPS Certificate");
        system(CMD_CREATE_CERT);
        myState = OK_LEDSTATE_SPECIAL1;
    }

    return APPMAN_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Return the pid of an executable
 *
 *    *
 *   \param procSearch Name of executable
 *
 *
 *   \return Process ID
 *
 ******************************************************************************
 */
static pid_t appManGetPid(ProcessType *procSearch)
{
    DIR             *dir;
    struct dirent   *dirEntry;
    lu_int8_t        pidFile[255];
//    lu_char_t        procName[255];
    pid_t            procPid = NO_PID;
    lu_char_t        cmdlineBuff[CMDLINEBUFF_SIZE];
    lu_char_t       *fileNamePtr = NULL;
    lu_int32_t       fd = -1;
    lu_uint16_t      bytesRead = 0;


    if (procSearch == NULL)
    {
        return NO_PID;
    }

    if (procSearch->cmdlineBufLen == 0)
    {
        appManCreateCmdLine(procSearch);
    }

    if ((dir = opendir("/proc")) == NULL )
    {
        logMessageError(SUBSYSTEM_ID_APP_MANAGER, "Cannot open /proc filesystem");
        return NO_PID;
    }

    while ((dirEntry = readdir(dir)))
    {
        /* We are only interested in numerical folders (pids) */
        if ((procPid = (pid_t) strtol(dirEntry->d_name, NULL, 10)) > 0)
        {
//            procName[0] = '\0';

            snprintf(pidFile, sizeof(pidFile), "/proc/%d/cmdline", procPid);

            fileNamePtr = cmdlineBuff;
            if ((fd = open(pidFile, O_RDONLY)) != -1 )
            {
                bytesRead = read(fd, fileNamePtr, (ssize_t)CMDLINEBUFF_SIZE);
                close(fd);
            }

            if (bytesRead > 0)
            {
                // Strip the path from the process name we're looking for
                fileNamePtr = appManStripPath(cmdlineBuff);
                bytesRead = bytesRead - (fileNamePtr - cmdlineBuff);
            }

            if ((bytesRead == procSearch->cmdlineBufLen) && (memcmp(fileNamePtr, procSearch->cmdlineBuf, bytesRead) == 0))
            {
                logMessageDebug(SUBSYSTEM_ID_APP_MANAGER, "FOUND [%s] with a pid of [%d]\n",
                                procSearch->processName[0], procPid);

                closedir(dir);
                return procPid;
            }
        }
    }

    closedir(dir);
    return NO_PID;
}

#if 0
/*!
 ******************************************************************************
 *   \brief Build array of external monitored programs
 *
 *   \param
 *
 *   \return
 *
 ******************************************************************************
 */
static void appManGetApps(void)
{
    DIR             *dir;
    FILE            *fd;
    struct dirent   *dirEntry;
    lu_char_t       appPath[255];
    struct stat     appPathStats;
    size_t          nBytes;
    char            *p;

    ProcessType     *appArray = NULL;
    ProcessType     *newApp = NULL;
    ProcessType     *lastApp = NULL;
    ProcessType     *loopApp = NULL;
    ProcessType     *previousApp = NULL;



    if ((dir = opendir(MonitorConfigPath)) == NULL )
    {
        logMessageInfo(SUBSYSTEM_ID_APP_MANAGER, "Cannot open [%s] folder", MonitorConfigPath);
        return;
    }

    // Loop around the files in this folder, loading them in to a linked list
    while ((dirEntry = readdir(dir)))
    {
        snprintf(appPath, 255, "%s/%s", MonitorConfigPath, dirEntry->d_name);
        if (stat(appPath, &appPathStats) == -1)
        {
            printf("Unable to read [%s]\n", appPath);
            continue;
        }

        // Only open regular files
        if ((appPathStats.st_mode & S_IFMT) == S_IFREG)
        {
            printf("Opening [%s]...\n", appPath);

            if ((fd = fopen(appPath, "r")) != NULL)
            {
                printf("Opened [%s]\n", appPath);

                if ((nBytes = fread(appPath, 1, 255, fd)) > 0)
                {
                    appPath[nBytes] = '\0';

                    // Only use the first line
                    while ((p = strrchr(appPath, '\n'))) *p = '\0';

                    printf("App is [%s]\n", appPath);

                    newApp = calloc(1, sizeof(ProcessType));
                    newApp->next = NULL;

                    newApp->processName = calloc(255, sizeof(lu_char_t));
                    strncpy(newApp->processName, appPath, 255);

                    newApp->startCounter = 0;
                    newApp->startRequest = LU_TRUE;
                    newApp->started = LU_FALSE;
                    newApp->killed = LU_FALSE;

                    if (appArray == NULL)
                    {
                        appArray = newApp;
                    }
                    else
                    {
                        // Loop over the linked list, inserting this entry in alphabetical order based on filename
                        lu_bool_t appInserted = LU_FALSE;
                        previousApp = NULL;
                        for (loopApp = appArray; loopApp->next != NULL; loopApp = loopApp->next)
                        {
                            // If the new app name is alphabetically before the
                            if (strncmp(newApp->processName, loopApp->processName, 255) < 0)
                            {
                                newApp->next = loopApp;
                                if (previousApp == NULL)
                                {
                                    // New Application gets inserted at the beginning of the linked list
                                    newApp->next = appArray;
                                    appArray = newApp;
                                }
                                else
                                {
                                    previousApp->next = newApp;
                                }
                                appInserted = LU_TRUE;
                                printf("Inserted [%s]\n", newApp->processName);
                                break;
                            }
                            previousApp = loopApp;
                        }

                        // This will insert the new app at the end of the linked list
                        if (appInserted == LU_FALSE)
                        {
                            loopApp->next = newApp;
                            printf("Inserted [%s] at end\n", newApp->processName);
                        }

                    }

                    // Allocate mem and copy Application Path to structure
                }

                fclose(fd);
            }
        }
    }


    printf("\n\nApp Linked List:\n\n");
    for (loopApp = appArray; loopApp != NULL; loopApp = loopApp->next)
    {
        printf("App [%s]\n", loopApp->processName);
    }
}
#endif




/*!
 ******************************************************************************
 *   \brief Message Queue Decoder
 *
 *    Handles an incoming message on the queue
 *
 *   \param
 *
 *
 *   \return
 *
 ******************************************************************************
 */
static void appManDecodeQMsq(QMsgType msgIn)
{
    struct timespec timer;
    lu_int32_t      process;
    lu_uint8_t      command;

    // Extract the command (removing error flag)
    command = msgIn.cmd & ~QMSG_CMD_REP_ERROR;

    // See if there's anything in our Queue
    switch (command)
    {
        case QMSG_CMD_REQ_STARTAPP:
// TODO SKA - Do we stop all other Apps???
//            procList[msgIn.data1.intVal].startRequest = LU_TRUE;
        break;

        case QMSG_CMD_REQ_STOPAPP:
            logMessageWarn(SUBSYSTEM_ID_APP_MANAGER,
                            "Received msg to kill monitored processes!");

            if (ArgNoStop == LU_TRUE)
            {
                logMessageWarn(SUBSYSTEM_ID_APP_MANAGER, "Ignore STOP -s option used!\n");
            }
            else
            {
                for (process = 0; process < NUM_PROCESSES; process++)
                {
                    // Kill the process and use SIGTERM as the exitCode
                    appManKillProcess(&(procList[process]), SIGTERM);
                }
            }
        break;

        case QMSG_CMD_NOTIFY_EXIT:
            for (process = 0; process < NUM_PROCESSES; process++)
            {
                if (ExitCodeToAppMapping[process].applicationID == msgIn.data1.uIntVal)
                {
                    // Set the requested ExitCode of the current process
                    procList[process].notifiedExitCode = msgIn.data2.intVal;

                    // Set the time (from Epoch) when the process needs killing
                    clock_gettimespec(&timer);
                    timespec_add_ms(&timer, msgIn.data3.uIntVal);
                    procList[process].exitTimeout = timer;

                    logMessageDebug(SUBSYSTEM_ID_APP_MANAGER,
                                    "[%s] notifying exit code [%d] timeout [%d]",
                                    procList[process].processName[0],
                                    procList[process].notifiedExitCode,
                                    msgIn.data3.uIntVal);

                    /* Don't break here in case there are multiple Apps with the same ID */
                }
            }
            // TODO - SKA - Handle invalid applicationID
        break;

        /* Ignore */
        case QMSG_CMD_REP_SET_DI:
        case QMSG_CMD_REP_SET_OLR:
        break;

            // Send the results back to the sender
        default:
            logMessageWarn(SUBSYSTEM_ID_APP_MANAGER, "Invalid msg received [0x%03X]", command);
    }
}

/*!
 ******************************************************************************
 *   \brief Initialise Process Array
 *
 *   \param  None
 *
 *
 *   \return None
 *
 ******************************************************************************
 */
static void appManInitProcList(void)
{
    lu_int32_t i;

    if ((procList = calloc(NUM_PROCESSES, sizeof(ProcessType))) == NULL)
    {
        logMessageFatal(SUBSYSTEM_ID_APP_MANAGER, "Unable to malloc()");
        exit(EXIT_FAILURE);
    }

    for (i = 0; i < NUM_PROCESSES; i++)
    {
        procList[i].processName                   = (lu_char_t **)ExitCodeToAppMapping[i].applicationWithArgs;
        procList[i].cmdlineBufLen                 = 0;
        procList[i].startCode                     = ExitCodeToAppMapping[i].exitCode;

        // Defaults
        procList[i].startRequest                  = LU_FALSE;
        procList[i].notifiedExitCode              = -1;
        procList[i].exitTimeout.tv_sec            = 0;
        procList[i].exitTimeout.tv_nsec           = 0;
        procList[i].killed                        = LU_FALSE;
        procList[i].exitCode                      = APP_EXIT_CODE_RESTART;
        procList[i].pid                           = NO_PID;
        procList[i].startCounter                  = 0;
        procList[i].startCounterResetTime.tv_sec  = 0;
        procList[i].startCounterResetTime.tv_nsec = 0;
        procList[i].started                       = LU_FALSE;
    }

    // Start the first process in the list
    if (NUM_PROCESSES > 0)
    {
        procList[0].startRequest = LU_TRUE;
    }

#if 0
    // Load the monitored applications configuration from ...gemini/etc/apps
    appManGetApps();
#endif
}

/*!
 ******************************************************************************
 *   \brief Kill a process
 *
 *   Kills the process pointed to by procPtr.  This will result in the monitored
 *   process returning an exit code of SIGTERM.  We can check for this and use
 *   the supplied exitCode instead.
 *
 *   \param  procPtr    Pointer to a procList array entry
 *           exitCode   The exit Code to be used for the process when it dies
 *
 *   \return None
 *
 ******************************************************************************
 */
static void appManKillProcess(ProcessType *procPtr, lu_int32_t exitCode)
{
    lu_int32_t try;


    // Send a SIGTERM to the process and mark it as killed (We do not know if this succeeded in killing the process!)
    // NOTE: killing the negative of the PID kills all the process's children (ideal if monitoring a script)

    if (procPtr->pid != NO_PID)
    {
        for (try = 0; try < 3; try++)
        {
            if (kill((procPtr->pid), SIGTERM) == 0)
            {
                procPtr->killed = LU_TRUE;
                procPtr->notifiedExitCode = exitCode;
                break;
            }
            logMessageWarn(SUBSYSTEM_ID_APP_MANAGER, "Failed to kill [%s] try:[%d]", procPtr->processName[0], try);
        }
    }

    // Reset the exitTimeout
    // This is done here because the application may have exited between the exitTimeout expiring and the
    // kill() above being called, resulting in the kill() failing.
    procPtr->exitTimeout.tv_sec  = 0;
    procPtr->exitTimeout.tv_nsec = 0;
}

/*!
 ******************************************************************************
 *   \brief Revert to a backup version of MCMUpdater
 *
 *   Deletes the current Updater and replaces it with a soft link to the
 *   backup version in /usr/local-ro/...
 *
 *   \param  None
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
static APPMAN_ERROR appManRevertToBackupUpdater(void)
{
    logMessageInfo(SUBSYSTEM_ID_APP_MANAGER, "Attempting to revert to MCMUpdater backup [%s]", MCMUpdaterBackup[0]);

    if (unlink(MCMUpdater[0]) == 0)
    {
        if (symlink(MCMUpdaterBackup[0], MCMUpdater[0]) == 0)
        {
            logMessageInfo(SUBSYSTEM_ID_APP_MANAGER, "Successfully reverted to MCMUpdater backup at [%s]", MCMUpdaterBackup[0]);
        }
        else
        {
            logMessageError(SUBSYSTEM_ID_APP_MANAGER, "Unable to create soft link from [%s]->[%s]", MCMUpdaterBackup[0], MCMUpdater[0]);
            return APPMAN_ERROR_ACCESS;
        }
    }
    else
    {
        logMessageError(SUBSYSTEM_ID_APP_MANAGER, "Unable to delete [%s]", MCMUpdater[0]);
        return APPMAN_ERROR_ACCESS;
    }

    return APPMAN_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Strip the leading path from a filename
 *
 *   Returns a pointer to the filename (after any leading path)
 *
 *   \param  pathPtr
 *
 *   \return Pointer to filename
 *
 ******************************************************************************
 */
static lu_char_t *appManStripPath(lu_char_t *pathPtr)
{
    lu_char_t *fileNamePtr;

    if (pathPtr == NULL)
    {
        return NULL;
    }

    // Strip the path from the process name we're looking for
    if ((fileNamePtr = strrchr(pathPtr, '/')) == NULL)
    {
        fileNamePtr = (char *) pathPtr;
    }
    else
    {
        fileNamePtr++; // Skip the '/'
    }

    return fileNamePtr;
}

/*!
 ******************************************************************************
 *   \brief Create a /proc/<pid>/cmdline type string
 *
 *   Creates a command-line buffer compatible with the format found in
 *   /proc/<pid>/cmdline.  This can be used when searching for a running process.
 *   This includes all parameters.
 *
 *   \param  pathPtr
 *
 *   \return Pointer to filename
 *
 ******************************************************************************
 */
static APPMAN_ERROR appManCreateCmdLine(ProcessType *processPtr)
{
    lu_char_t *pBuff;
    lu_int16_t bytesRead = 0;
    char *p;
    char **arg;


    if (processPtr== NULL)
    {
        return APPMAN_ERROR_NULL;
    }

    pBuff = processPtr->cmdlineBuf;

    /* Clear the buffer */
    memset(pBuff, 0, CMDLINEBUFF_SIZE);

    /* Loop over the arguments, adding them to the buffer */
    for (arg = processPtr->processName; ((*arg != NULL) && (bytesRead < CMDLINEBUFF_SIZE)); arg++)
    {
        p = *arg;
        while (*p && (bytesRead < CMDLINEBUFF_SIZE))
        {
            *pBuff++ = *p++;
            bytesRead++;
        }

        /* Separate the arguments with NULL chars */
        if ((bytesRead != 0) && (bytesRead < CMDLINEBUFF_SIZE))
        {
            *pBuff++ = '\0';
            bytesRead++;
        }
    }

    /* Set the number of bytes written to cmdlineBuf */
    processPtr->cmdlineBufLen = bytesRead;

    return APPMAN_ERROR_NONE;
}
/*
 *********************** End of file ******************************************
 */
