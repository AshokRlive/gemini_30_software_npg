/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCMMonitor
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Useful Timer related functions
 *
 *    CURRENT REVISION
 *
 *               $Revision::               $: (Revision of last commit)
 *               $Author::                 $: (Author of last commit)
 *       \date   $Date::                   $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   07/08/13      andrews_s   Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <string.h>  // memset()

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Timer.h"
#include "timeOperations.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
lu_int32_t initTimer(lu_int32_t signal, timer_handler handler, lu_bool_t periodic, lu_uint32_t durationMs, timer_t *timerIdPtr)
{
    lu_int32_t              ret;
    sigset_t                signalSet;
    struct itimerspec       timerValue;
    static struct sigevent  timerEvent;
    static struct sigaction timerAction;
    struct timespec         timeDelay;


    /* Initialise time event struct */
    timerEvent.sigev_notify          = SIGEV_SIGNAL;
    timerEvent.sigev_signo           = signal;
    timerEvent.sigev_value.sival_int = getpid();

    /* Set up timer interval */
    timeDelay.tv_sec  = durationMs / 1000;
    timeDelay.tv_nsec = MSEC_TO_NSEC(durationMs % 1000);

    timerValue.it_value = timeDelay;

    if (periodic == LU_TRUE)
    {
        timerValue.it_interval = timeDelay;
    }
    else
    {
        timerValue.it_interval.tv_sec  = 0;
        timerValue.it_interval.tv_nsec = 0;
    }

    /* Initialise wait mask */
    sigemptyset(&signalSet)        ;
    sigaddset(&signalSet, signal);

    /* Set up signal handler */
    memset(&timerAction, 0, sizeof(timerAction));
    timerAction.sa_sigaction = handler;
    timerAction.sa_flags = SA_SIGINFO;
    ret = sigaction(signal, &timerAction, NULL);

    if (ret != 0)
    {
        return ret;
    }

    /* Unblock signal */
    pthread_sigmask(SIG_UNBLOCK, &signalSet, NULL);

#ifdef CLOCK_MONOTONIC_RAW
    ret = timer_create(CLOCK_MONOTONIC_RAW, &timerEvent, timerIdPtr);
#else
    ret = timer_create(CLOCK_MONOTONIC, &timerEvent, timerIdPtr);
#endif

    if (ret == 0)
    {
        ret = timer_settime(*timerIdPtr , 0 , &timerValue , NULL);
        if (ret != 0)
        {
            return ret;
        }
    }

    return ret;
}


lu_int32_t updateTimer(timer_t timerid, lu_bool_t periodic, lu_uint32_t durationMs)
{
    struct itimerspec timerValue;
    struct timespec timeDelay;


    /* Set up timer interval */
    timeDelay.tv_sec  = durationMs / 1000;
    timeDelay.tv_nsec = MSEC_TO_NSEC(durationMs % 1000);

    timerValue.it_value    = timeDelay;

    if (periodic == LU_TRUE)
    {
        timerValue.it_interval = timeDelay;
    }
    else
    {
        timerValue.it_interval.tv_sec  = 0;
        timerValue.it_interval.tv_nsec = 0;
    }

    return timer_settime(timerid , 0 , &timerValue , NULL);
}


/*
 *********************** End of file ******************************************
 */
