/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCMMonitor
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Read MCM's on board Digital & Analogue Inputs and operates the
 *       Digital Ouputs
 *
 *    CURRENT REVISION
 *
 *               $Revision::               $: (Revision of last commit)
 *               $Author::                 $: (Author of last commit)
 *       \date   $Date::                   $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25/07/13      andrews_s   Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>
#include <unistd.h>  // sleep()
#include <time.h>
#include <pthread.h>
#include <sys/prctl.h> // prctl()

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "Main.h"
#include "MonitorPointMapping.h"
#include "WDogManager.h"
#include "MCMIOMap.h"
#include "IOManager.h"
#include "CANUtils.h"  // CAN Usage Virtual AIs

#include "LogMessage.h"
#include "timeOperations.h"


#include "NVRAMBlock.h"  // Read NVRAM Header Info

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define SHUTDOWN_OVER_TEMP  90.0  // Temperature we will shutdown (not reboot!)
/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

// Structure to hold the states of the local and remote lines
typedef struct MCMOLRStateDef
{
    lu_bool_t localState;
    lu_bool_t remoteState;
    lu_char_t olrDesc[8];

} MCMOLRStateStr;

typedef struct MCMIDNRAMFileDef
{
    NVRAM_TYPE NVRam;
    lu_int8_t  fileName[128];

} MCMIDNRAMFileStr;

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static IO_ERROR ioManInit(void);
static void ioManUpdatePoints(void);
static IO_ERROR ioManInitAnalogueInputs(void);
static IO_ERROR ioManInitDigitalInputs(void);
static IO_ERROR ioManInitDigitalOutputs(void);
static void ioManDecodeQMsq(QMsgType msgIn);
static IO_ERROR ioManGetCurrentOLRState(MCMOLR_STATES *OLRStatePtr);
static IO_ERROR ioManSetOLRState(MCMOLR_STATES OLRState);
static lu_bool_t OLRQuickScan(lu_bool_t enable);
static MCMOLR_STATES ioManCalcOLRState(void);
static void ioManCheckOLRState(void);
static IO_ERROR ioManUpdateAnalogueInputs(void);
static IO_ERROR ioManUpdateDigitalInputs(void);
static IO_ERROR ioManNotifyDigitalInputChange(lu_uint8_t channel, lu_bool_t force);
static IO_ERROR ioManReadNVRAMIDHeader(NVRAM_TYPE NVRam, NVRAMInfoStr *dataPtr);


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static mqd_t mainMqFd;
static mqd_t mqFd; // Local MQ descriptor

static pthread_mutex_t ioLock;

// Analogue Input Configuration
// NOTE: These macros come from MCMIOMap.h
// Also update MonitorPointMapping.h!
static MCMMonAIConfigStr analogueInputConfig[] =
{
    MCMMONAI_VLOGIC_CONFIG,
    MCMMONAI_5V_CONFIG,
    MCMMONAI_3V3_CONFIG,
    MCMMONAI_VLOCAL_CONFIG,
    MCMMONAI_VREMOTE_CONFIG,
    MCMMONAI_CORETEMP_CONFIG,

    MCMMONAI_CAN0TXBYTES_CONFIG,
    MCMMONAI_CAN0RXBYTES_CONFIG,
    MCMMONAI_CAN0USAGE_CONFIG,
    MCMMONAI_CAN0TXERRS_CONFIG,
    MCMMONAI_CAN0RXERRS_CONFIG,
    MCMMONAI_CAN0ERRS_CONFIG,
    MCMMONAI_CAN0TXPACKETS_CONFIG,
    MCMMONAI_CAN0RXPACKETS_CONFIG,
    MCMMONAI_CAN0RXDROPPED_CONFIG,
    MCMMONAI_CAN0RXOVERERR_CONFIG,

    MCMMONAI_CAN1TXBYTES_CONFIG,
    MCMMONAI_CAN1RXBYTES_CONFIG,
    MCMMONAI_CAN1USAGE_CONFIG,
    MCMMONAI_CAN1TXERRS_CONFIG,
    MCMMONAI_CAN1RXERRS_CONFIG,
    MCMMONAI_CAN1ERRS_CONFIG,
    MCMMONAI_CAN1TXPACKETS_CONFIG,
    MCMMONAI_CAN1RXPACKETS_CONFIG,
    MCMMONAI_CAN1RXDROPPED_CONFIG,
    MCMMONAI_CAN1RXOVERERR_CONFIG
};

// Digital Input Configuration
static MCMMonDIConfigStr digitalInputConfig[] =
{
    MCMMONDI_LOCAL_CONFIG,
    MCMMONDI_REMOTE_CONFIG,
    MCMMONDI_SYSHEALTHY_CONFIG,
    MCMMONDI_WDOG_HANDSHAKE,
    MCMMONDI_FACTORY,
    MCMMONDI_WDOG_FAILED
};

// Digital Output Configuration
static MCMMonDOConfigStr digitalOutputConfig[] =
{
    MCMMONDO_LOCAL_CONFIG,
    MCMMONDO_REMOTE_CONFIG,
};

// Table containing the Local/Remote line states for each OLR State
const MCMOLRStateStr MCMOLRStates[MCMOLR_STATE_LAST] = { { LU_FALSE, LU_FALSE, "OFF"     },// OFF
                                                         { LU_TRUE,  LU_FALSE, "LOCAL"   }, // LOCAL
                                                         { LU_FALSE, LU_TRUE,  "REMOTE"  }, // REMOTE
                                                         { LU_TRUE,  LU_TRUE,  "ERROR"   }   // ERROR
                                                       };

const MCMIDNRAMFileStr IdNVRamFiles[2] = { { NVRAM_TYPE_IDENTITY,    "/sys/bus/i2c/devices/0-0050/eeprom" },
                                           { NVRAM_TYPE_APPLICATION, "/sys/bus/i2c/devices/0-0054/eeprom" }
                                         };


static MCMOLR_STATES   currentOLRState;
static MCMOLR_STATES   previousOLRState;
static MCMOLR_STATES   requestedOLRState;

static struct timespec OLRCheckDelay;  // OLR Delay
static OK_LEDSTATE     myState;
static OK_LEDSTATE     myLastState;

static lu_bool_t       OLRQuickScanState;
static lu_bool_t       eventAllValues;
static lu_bool_t       ready = LU_FALSE;

static AnalogueInputType  analogueInputs[MCM_MON_AI_LAST];
static DigitalInputType   digitalInputs[MCM_MON_DI_LAST];
static DigitalOutputType  digitalOutputs[MCM_MON_DO_LAST];

static NVRAMInfoStr       IDNVRamInfo;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
void *ioManagerThread(void *dummy)
{
    LU_UNUSED(dummy);

    // Initialise this thread's state
    myState = OK_LEDSTATE_INITIALISING;
    myLastState = OK_LEDSTATE_INITIALISING;


    // Set the thread name (ps -T)  - ignore errors
    prctl(PR_SET_NAME, "IOManager", 0, 0, 0);

    // Initialise Message Queues
    mqFd = connectMsgQueue(threadInfo[THREAD_IOMAN].mqName);
    mainMqFd = connectMsgQueue(threadInfo[THREAD_MAIN].mqName);
    if ((mqFd < 0) || (mainMqFd < 0))
    {
        logMessageFatal(SUBSYSTEM_ID_IO_MANAGER,
                        "Error initialising Msg Queues [%d] [%d]", mqFd,
                        mainMqFd);
        exit(1);
    }

    // Initialise our Thread's state to HEALTHY
    myState = OK_LEDSTATE_HEALTHY;

    // Initialise all the IO channels
    if (ioManInit() != 0)
    {
        SET_STATE(myState, OK_LEDSTATE_WARNING);
    }

    ready = LU_TRUE;

    // Main Loop
    while (!Closedown)
    {
        myState = OK_LEDSTATE_HEALTHY;

        ioManUpdatePoints();

        // See if my state has changed
        if (myState != myLastState)
        {
            if (sendRepStateQMsg(threadInfo[THREAD_IOMAN].threadMqFd, mainMqFd, THREAD_IOMAN, myState) < 0)
            {
                logMessageError(SUBSYSTEM_ID_IO_MANAGER, "Error sending STATE change to MAIN!");
            }
            myLastState = myState;
        }

        // Handle messages coming in on the message queue
        if (processMsgQueue(mqFd, &ioManDecodeQMsq, 250) < 0)
        {
            if ((mqFd = connectMsgQueue(threadInfo[THREAD_IOMAN].mqName)) < 0)
            {
                logMessageFatal(SUBSYSTEM_ID_IO_MANAGER, "Error sending Registration msg to Main");
                exit(1);
            }
        }
    }

    // Tidy Up
    pthread_mutex_destroy(&ioLock);
    mq_close(mqFd);
    mq_close(mainMqFd);

    return NULL;
}

IO_ERROR ioManGetAnalogueInput(lu_uint8_t channel, lu_float32_t *valuePtr, lu_uint8_t *flagsPtr)
{
    IO_ERROR ret = IO_ERROR_NONE;


    if (channel >= MCM_MON_AI_LAST)
    {
        // If a flagsPtr is given set it to INVALID
        if (flagsPtr != NULL)
        {
           *flagsPtr = (lu_uint8_t) IO_FLAG_INVALID;
        }

        logMessageWarn(SUBSYSTEM_ID_IO_MANAGER,
                        "Request for AI channel [%d] is OUT OF RANGE",
                        channel);

        return IO_ERROR_CONFIG;
    }

    pthread_mutex_lock(&ioLock);

    /* If this point has a scan interval of 0 it should be updated "on request"
     */
    if (analogueInputs[channel].config->scanIntervalMs == 0)
    {
        ret = analogueInputUpdate(&(analogueInputs[channel]));
    }

    if (valuePtr != NULL)
    {
        *valuePtr = analogueInputs[channel].value;
    }

    if (flagsPtr != NULL)
    {
        /* If IOManager has not initialised yet, report all flags as offline
         */
        if (ready == LU_TRUE)
        {
            *flagsPtr = (lu_uint8_t) analogueInputs[channel].flags;
        }
        else
        {
            *flagsPtr = (lu_uint8_t) IO_FLAG_OFF_LINE;
        }
    }

    pthread_mutex_unlock(&ioLock);

    return ret;
}

IO_ERROR ioManGetDigitalInput(lu_uint8_t channel, lu_bool_t *valuePtr, lu_uint8_t *flagsPtr)
{
    IO_ERROR ret = IO_ERROR_NONE;


    if (channel >= MCM_MON_DI_LAST)
    {
        // If a flagsPtr is given set it to INVALID
        if (flagsPtr != NULL)
        {
           *flagsPtr = (lu_uint8_t) IO_FLAG_INVALID;
        }

        logMessageWarn(SUBSYSTEM_ID_IO_MANAGER,
                        "Request for DI channel [%d] is OUT OF RANGE",
                        channel);

        return IO_ERROR_CONFIG;
    }

    pthread_mutex_lock(&ioLock);

    /* If there's no scanrate, force an update to the channel (update on request)
     */
    if (digitalInputs[channel].currentScanIntervalMs == 0)
    {
        ret = digitalInputUpdate(&digitalInputs[channel]);
    }

    if (valuePtr != NULL)
    {
        *valuePtr = digitalInputs[channel].value;
    }

    if (flagsPtr != NULL)
    {
        /* If IOManager has not initialised yet, report all flags as offline
         */
        if (ready == LU_TRUE)
        {
            *flagsPtr = (lu_uint8_t) digitalInputs[channel].flags;
        }
        else
        {
            *flagsPtr = (lu_uint8_t) IO_FLAG_OFF_LINE;
        }
    }

    pthread_mutex_unlock(&ioLock);

    return ret;
}


IO_ERROR ioManSetDigitalOutput(lu_uint8_t channel, lu_bool_t value)
{
    IO_ERROR ret;

    if (channel >= MCM_MON_DO_LAST)
    {
        logMessageWarn(SUBSYSTEM_ID_IO_MANAGER,
                        "Error setting DO [%d] GPIO [%d]!\n",
                        channel, digitalOutputs[channel].config->gpioNum);

        return IO_ERROR_CONFIG;
    }

    if (ready == LU_TRUE)
    {
        pthread_mutex_lock(&ioLock);

        ret = digitalOutputSet(&(digitalOutputs[channel]), value);

        pthread_mutex_unlock(&ioLock);
    }
    else
    {
        return IO_ERROR_GENERAL;
    }

    return ret;
}


IO_ERROR ioManSetDigitalInput(lu_uint8_t channel, lu_bool_t value)
{
    IO_ERROR ret;

    if (channel >= MCM_MON_DI_LAST)
    {
        return IO_ERROR_CONFIG;
    }

    pthread_mutex_lock(&ioLock);

    ret = digitalInputSetValue(&(digitalInputs[channel]), value);
    if (ret == IO_ERROR_NONE)
    {
        ret = ioManNotifyDigitalInputChange(channel, LU_FALSE);
    }

    pthread_mutex_unlock(&ioLock);


    return ret;
}

IO_ERROR ioManGetAnalogueInputConfig(lu_uint8_t channel, MCMMonAIConfigStr *configPtr)
{
    if (channel >= MCM_MON_AI_LAST)
    {
        return IO_ERROR_CONFIG;
    }

    if (configPtr == NULL)
    {
        return IO_ERROR_NULL;
    }

    // NOTE: No mutex is required to protect the config (it doesn't change)

    configPtr = analogueInputs[channel].config;

    return IO_ERROR_NONE;
}

IO_ERROR ioManAlarmFuncPercOfvLogic(AnalogueInputType *AIPointPtr)
{
    AnalogueInputType *vLogicPtr;
    lu_float32_t lowThreshold;
    lu_float32_t highThreshold;
    lu_float32_t perc;

    lu_bool_t    tempAlarmState = LU_TRUE;


    // QuickScan is used to try and detect when an Analogue Input value has reached
    // it's intended value; hopefully more quickly than waiting for a fixed period of time.
    // Thus, during QuickScan Mode we ignore alarms - this is because the
    // value of vLocal/vRemote may be rising or falling to its desired level
    // during this time.
    if (OLRQuickScanState == LU_TRUE)
    {
        return IO_ERROR_NONE;
    }

    vLogicPtr = &(analogueInputs[MCM_MON_AI_VLOGIC_ADDR]);

    // If vLogic is OFFLINE we can't use it's value to determine our Alarm State
    if ((vLogicPtr->flags & IO_FLAG_OFF_LINE) != 0)
    {
        return IO_ERROR_NONE;
    }

    // Calculate the thresholds based on the value of vLogic
    perc          = vLogicPtr->value / 100.0;
    lowThreshold  = (perc * AIPointPtr->config->scaledAlarmLevelLow);
    highThreshold = (perc * AIPointPtr->config->scaledAlarmLevelHigh);

    // The AI is in ALARM if it is WITHIN the AlarmLevels!
    if ((AIPointPtr->value > lowThreshold) &&
        (AIPointPtr->value < highThreshold))
    {
        tempAlarmState = LU_TRUE;

        // If the point is IN alarm then this may be due to vLogic suddenly changing value
        // (i.e. Battery voltage<>Mains Voltage (24V<>36V) change which is valid and we don't
        // want this to cause an alarm!

        // Let's use the FILTERED flag to highlight the fact that we have detected an error but are ignoring it.
        // If the flag is not set, set it and start OLRQuickScan to update the proper status of the OLR
        // If the flag is already set then we've ignored this error already and it's definitely an error.
        if ((AIPointPtr->flags & IO_FLAG_RESCANNED) == 0)
        {
            // Set the FILTERED flag to indicate that we have filtered out this ALARM
            AIPointPtr->flags |= IO_FLAG_RESCANNED;

            // Put the IOManager in to QuickScan Mode to allow all the vLocal/vRemote/vLogic lines to settle
            logMessageWarn(SUBSYSTEM_ID_IO_MANAGER, "Possible vLogic change detected was:[%f V] Going to QuickScan Mode", vLogicPtr->value);
            OLRQuickScanState = OLRQuickScan(LU_TRUE);

            tempAlarmState = LU_FALSE;
        }
    }
    else
    {
        // No alarm , clear the RESCAN flag
        tempAlarmState = LU_FALSE;
        AIPointPtr->flags &= ~IO_FLAG_RESCANNED;
    }

    if (tempAlarmState == LU_TRUE)
    {
        if ((AIPointPtr->flags & IO_FLAG_ALARM) == 0)
        {
            logMessageWarn(SUBSYSTEM_ID_IO_MANAGER, "Channel:[%d] IN ALARM (Percentage of vLogic:[%2.2f])", AIPointPtr->channel, vLogicPtr->value);
        }

        // Set the ALARM flag
        AIPointPtr->flags |= IO_FLAG_ALARM;
    }
    else
    {
        if (AIPointPtr->flags & IO_FLAG_ALARM)
        {
            logMessageInfo(SUBSYSTEM_ID_IO_MANAGER, "Channel:[%d] RECOVERED (Percentage of vLogic:[%2.2f])", AIPointPtr->channel, vLogicPtr->value);
        }

        // Clear the ALARM flag
        AIPointPtr->flags &= ~IO_FLAG_ALARM;
    }

    return IO_ERROR_NONE;
}

IO_ERROR ioManUpdateVirtualDigitalInput(DigitalInputType *digitalInputPtr)
{
    lu_bool_t    boolValue;
    lu_float32_t percOfVLogic;


    if ((digitalInputPtr == NULL) || (digitalInputPtr->config == NULL))
    {
        return IO_ERROR_NULL;
    }

    // This function is designed for virtual DI points, if there's no update_func it has been
    // configured incorrectly
    if (digitalInputPtr->config->update_func == NULL)
    {
        return IO_ERROR_CONFIG;
    }

    // Lock the mutex
    pthread_mutex_lock(&ioLock);

    switch (digitalInputPtr->channel)
    {
        case MCM_MON_DI_GPIO_LOCAL: // LOCAL Line
            boolValue = LU_FALSE;

            percOfVLogic = (analogueInputs[MCM_MON_AI_VLOCAL].value / analogueInputs[MCM_MON_AI_VLOGIC_ADDR].value) * 100.0;
            if (percOfVLogic > analogueInputs[MCM_MON_AI_VLOCAL].config->scaledAlarmLevelHigh)
            {
                // DI is TRUE if the AI is reporting a High Alarm Level
                boolValue = LU_TRUE;
            }

            digitalInputSetValue(digitalInputPtr, boolValue);

            // Copy the flags from the Analogue
            digitalInputPtr->flags = (IO_FLAG) analogueInputs[MCM_MON_AI_VLOCAL].flags;
        break;

        case MCM_MON_DI_GPIO_REMOTE: // REMOTE Line
            boolValue = LU_FALSE;

            percOfVLogic = (analogueInputs[MCM_MON_AI_VREMOTE].value / analogueInputs[MCM_MON_AI_VLOGIC_ADDR].value) * 100.0;

            if (percOfVLogic > analogueInputs[MCM_MON_AI_VREMOTE].config->scaledAlarmLevelHigh)
            {
                // DI is TRUE if the AI is reporting a High Alarm Level
                boolValue = LU_TRUE;
            }

            // DI is TRUE if the AI is reporting a High Alarm Level else FALSE
            digitalInputSetValue(digitalInputPtr, boolValue);

            // Copy the flags from the Analogue
            digitalInputPtr->flags = (IO_FLAG) analogueInputs[MCM_MON_AI_VREMOTE].flags;
        break;
    }

    // Unlock the mutex
    pthread_mutex_unlock(&ioLock);

    return IO_ERROR_NONE;
}


IO_ERROR ioManGetFactoryPin(lu_bool_t *valuePtr)
{
    IO_ERROR ret;

    if (valuePtr == NULL)
    {
        return IO_ERROR_NULL;
    }

    // Initialise the Digital Input
    if ((ret = digitalInputInit(MCM_MON_DI_GPIO_FACTORY,
                                &(digitalInputs[MCM_MON_DI_GPIO_FACTORY]),
                                &(digitalInputConfig[MCM_MON_DI_GPIO_FACTORY]))) != IO_ERROR_NONE)
    {
        logMessageError(SUBSYSTEM_ID_IO_MANAGER, "Error opening FACTORY INPUT GPIO [%d]",
                        digitalInputConfig[MCM_MON_DI_GPIO_FACTORY].gpioNum);

        return ret;
    }

    *valuePtr = digitalInputs[MCM_MON_DI_GPIO_FACTORY].value;

    // Close the fd
    digitalInputClose(&(digitalInputs[MCM_MON_DI_GPIO_FACTORY]));

    return IO_ERROR_NONE;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Initialise all the IO
 *
 *   \param
 *
 *   \return Error Code
 *
 ******************************************************************************
 */

static IO_ERROR ioManInit(void)
{
    IO_ERROR            ret = IO_ERROR_NONE;
    pthread_mutexattr_t mta;


    /* Initialise some globals */
    eventAllValues    = LU_FALSE;
    OLRQuickScanState = LU_FALSE;
    clock_gettimespec(&OLRCheckDelay);

    /* Initialise the locking mutex */
    if ((ret = pthread_mutexattr_init(&mta)) != 0)
    {
        logMessageError(SUBSYSTEM_ID_IO_MANAGER, "Failed to initialise mutex attributes");
        return IO_ERROR_GENERAL;
    }

    if ((ret = pthread_mutexattr_setprotocol(&mta, PTHREAD_PRIO_NONE)) != 0)
    {
        logMessageError(SUBSYSTEM_ID_IO_MANAGER, "Failed to set mutex protocol");
        return IO_ERROR_GENERAL;
    }

    /* This mutex MUST be RECURSIVE since it may be locked more than once from within
     * this thread. e.g. A virtual DI using the value of an AI will lock the mutex
     * twice when updating (once for the DI update and again for getting the AI value)
     */
    if ((ret = pthread_mutexattr_settype(&mta, PTHREAD_MUTEX_RECURSIVE)) != 0)
    {
        logMessageError(SUBSYSTEM_ID_IO_MANAGER, "Failed to set mutex to recursive");
        return IO_ERROR_GENERAL;
    }

    if ((ret = pthread_mutex_init(&ioLock, &mta)) != 0)
    {
        logMessageError(SUBSYSTEM_ID_IO_MANAGER, "Failed to initialise mutex");
        return IO_ERROR_GENERAL;
    }

    /*  Lock the mutex */
    pthread_mutex_lock(&ioLock);

    /* If there's an error initialising any type of IO we don't want to return,
     * we want to continue to try and initialise the remaining I/O
     */

    /* Initialise the points */
    if (ioManInitAnalogueInputs() != IO_ERROR_NONE)
    {
        logMessageError(SUBSYSTEM_ID_IO_MANAGER, "Analogue Input initialisation FAILED");
        ret = IO_ERROR_CONFIG;
    }

    if (ioManInitDigitalInputs() != IO_ERROR_NONE)
    {
        logMessageError(SUBSYSTEM_ID_IO_MANAGER, "Digital Input initialisation FAILED");
        ret = IO_ERROR_CONFIG;
    }

    if (ioManInitDigitalOutputs() != IO_ERROR_NONE)
    {
        logMessageError(SUBSYSTEM_ID_IO_MANAGER, "Digital Output initialisation FAILED");
        ret = IO_ERROR_CONFIG;
    }

    /* Unlock mutex */
    pthread_mutex_unlock(&ioLock);

    /* Initialise the OLR State variables */
    currentOLRState = previousOLRState = requestedOLRState = MCMOLR_STATE_ERROR;

    /* Force the OLR to go OFF initially */
    ioManSetOLRState((MCMOLR_STATES) MCMOLR_STATE_OFF);

    /* Read the NVRam ID Header - potentially we could use the Feature/Revision to change IO map etc... */
    if (ret == IO_ERROR_NONE)
    {
        if ((ret = ioManReadNVRAMIDHeader(NVRAM_TYPE_IDENTITY, &IDNVRamInfo)) != IO_ERROR_NONE)
        {
            logMessageWarn(SUBSYSTEM_ID_IO_MANAGER, "Unable to read ID from ID NVRAM!");

            if ((ret = ioManReadNVRAMIDHeader(NVRAM_TYPE_APPLICATION, &IDNVRamInfo)) != IO_ERROR_NONE)
            {
                logMessageWarn(SUBSYSTEM_ID_IO_MANAGER, "Unable to read ID from APP NVRAM!");
            }
        }

        if (ret == IO_ERROR_NONE)
        {
            logMessageDebug(SUBSYSTEM_ID_IO_MANAGER, " Feature Major [%d]\n", IDNVRamInfo.moduleFeatureMajor);
            logMessageDebug(SUBSYSTEM_ID_IO_MANAGER, " Feature Minor [%d]\n", IDNVRamInfo.moduleFeatureMinor);
            logMessageDebug(SUBSYSTEM_ID_IO_MANAGER, " Serial Number [%d]\n", IDNVRamInfo.serialNumber);
        }
    }

    return ret;
}


/*!
 ******************************************************************************
 *   \brief Wrapper to update all point types
 *
 *    Update all point types
 *
 *   \param
 *
 *   \return N/A
 *
 ******************************************************************************
 */
static void ioManUpdatePoints(void)
{
    /* Update the Analogue Inputs */
    ioManUpdateAnalogueInputs();

    /* Update the Digital Inputs */
    ioManUpdateDigitalInputs();

    /* Check the OLR State */
    ioManCheckOLRState();

    /* If a request to event all values was received, we can clear it now */
    eventAllValues = LU_FALSE;
}

/*!
 ******************************************************************************
 *   \brief Initialise the Analogue Inputs
 *
 *    Open the analogue input value files
 *
 *   \param
 *
 *   \return Error Code
 *
 ******************************************************************************
 */

static IO_ERROR ioManInitAnalogueInputs(void)
{
    lu_int32_t channel;
    lu_int8_t ret = IO_ERROR_NONE;

    for (channel = 0; channel < MCM_MON_AI_LAST; channel++)
    {
        if (analogueInputInit(channel, &(analogueInputs[channel]), &(analogueInputConfig[channel])) != 0)
        {
            logMessageError(SUBSYSTEM_ID_IO_MANAGER, "Error opening AI [%s]",
                            analogueInputConfig[channel].deviceName);

            /* Set the return value here so this function returns any error */
            ret = IO_ERROR_CONFIG;
        }
        else
        {
            logMessageInfo(SUBSYSTEM_ID_IO_MANAGER, "Opened AI Channel:[%d]:[%s]",
                            channel, analogueInputConfig[channel].deviceName);
        }
    }

    return ret;
}

/*!
 ******************************************************************************
 *   \brief Initialise the Digital Inputs
 *
 *    Open the digital input value files
 *
 *   \param
 *
 *   \return Error Code
 *
 ******************************************************************************
 */

static IO_ERROR ioManInitDigitalInputs(void)
{
    lu_int32_t channel;
    lu_int8_t ret = IO_ERROR_NONE;

    for (channel = 0; channel < MCM_MON_DI_LAST; channel++)
    {
        if (digitalInputInit(channel, &(digitalInputs[channel]), &(digitalInputConfig[channel])) != 0)
        {
            logMessageError(SUBSYSTEM_ID_IO_MANAGER, "Error opening INPUT GPIO [%d]",
                            digitalInputConfig[channel].gpioNum);

            // Set the return value here so this function returns any error
            ret = IO_ERROR_CONFIG;
        }
        else
        {
            if (digitalInputConfig[channel].update_func == NULL)
            {
                logMessageInfo(SUBSYSTEM_ID_IO_MANAGER, "Opened INPUT GPIO [%d]",
                                digitalInputConfig[channel].gpioNum);
            }
        }
    }

    return ret;
}

/*!
 ******************************************************************************
 *   \brief Initialise the Digital Outputs
 *
 *    Open the digital output value files
 *
 *   \param
 *
 *   \return Error Code
 *
 ******************************************************************************
 */

static IO_ERROR ioManInitDigitalOutputs(void)
{
    lu_int32_t channel;
    lu_int8_t ret = IO_ERROR_NONE;

    for (channel = 0; channel < MCM_MON_DO_LAST; channel++)
    {
        if (digitalOutputInit(channel, &(digitalOutputs[channel]), &(digitalOutputConfig[channel])) != 0)
        {
            logMessageError(SUBSYSTEM_ID_IO_MANAGER, "Error opening OUTPUT GPIO [%d]",
                            digitalOutputConfig[channel].gpioNum);

            // Set the return value here so this function returns any error
            ret = IO_ERROR_CONFIG;
        }
        else
        {
            logMessageInfo(SUBSYSTEM_ID_IO_MANAGER, "Opened OUTPUT GPIO [%d]",
                            digitalOutputConfig[channel].gpioNum);
        }
    }

    return ret;
}

/*!
 ******************************************************************************
 *   \brief Message Queue Decoder
 *
 *    Handles an incoming message on the queue
 *
 *   \param msgIn  Message to decode
 *
 *   \return N/A
 *
 ******************************************************************************
 */
static void ioManDecodeQMsq(QMsgType msgIn)
{
    lu_int32_t channel;
    lu_int32_t value;
    lu_int32_t ret;

    lu_bool_t    boolValue;
    lu_float32_t floatValue;
    lu_uint8_t   flags;

    lu_int32_t error = QMSG_CMD_ERR_NONE;

    channel = msgIn.data1.intVal;

    switch (msgIn.cmd)
    {
        case QMSG_CMD_REQ_DI:
            if (ioManGetDigitalInput(channel, &boolValue, &flags) == IO_ERROR_NONE)
            {
                sendRepDIQMsg(msgIn.qSrc, mainMqFd, channel, flags, boolValue);
            }
            else
            {
                error = QMSG_CMD_ERR_GENERAL;
            }
        break;

        case QMSG_CMD_REQ_AI:
            if (ioManGetAnalogueInput(channel, &floatValue, &flags) == IO_ERROR_NONE)
            {
                sendRepAIQMsg(msgIn.qSrc, mainMqFd, channel, flags, floatValue);
            }
            else
            {
                error = QMSG_CMD_ERR_GENERAL;
            }
        break;

        case QMSG_CMD_REQ_SET_DI:
            boolValue = msgIn.data2.boolVal;

            if (ioManSetDigitalInput(channel, boolValue) != IO_ERROR_NONE)
            {
                error = QMSG_CMD_ERR_GENERAL;
            }
            else
            {
                sendRepSetDIQMsg(mqFd, msgIn.qSrc, channel, boolValue);
            }
        break;

        case QMSG_CMD_REQ_SET_DO:
            boolValue = msgIn.data2.boolVal;

            if (ioManSetDigitalOutput(channel, boolValue) != IO_ERROR_NONE)
            {
                error = QMSG_CMD_ERR_GENERAL;
            }
            else
            {
                sendRepSetDOQMsg(msgIn.qSrc, mainMqFd, channel, boolValue);
            }
        break;

        case QMSG_CMD_REQ_SET_OLR:
            value = msgIn.data1.intVal;
            ret = 0;

            logMessageInfo(SUBSYSTEM_ID_IO_MANAGER, "Request to set OLR to [%s]", MCMOLRStates[value].olrDesc);
            ret = ioManSetOLRState((MCMOLR_STATES) value);
            if (ret == IO_ERROR_NONE)
            {
                sendRepSetOLRQMsg(msgIn.qDest, msgIn.qSrc, value);

                // Force the values of the LOCAL/REMOTE Virtual Digital Inputs for OLR to the requested values
                ioManSetDigitalInput(MCM_MON_DI_GPIO_LOCAL,  MCMOLRStates[value].localState);
                ioManSetDigitalInput(MCM_MON_DI_GPIO_REMOTE, MCMOLRStates[value].remoteState);
            }
            else
            {
                logMessageWarn(SUBSYSTEM_ID_IO_MANAGER,
                               "Error setting OLR to [%s]\n", MCMOLRStates[value].olrDesc);

                error = QMSG_CMD_ERR_GENERAL;
            }
        break;

        case QMSG_CMD_EVENT_ALL:
            logMessageDebug(SUBSYSTEM_ID_IO_MANAGER, "Command to event ALL values");
            eventAllValues = LU_TRUE;
        break;

        default:
            error = QMSG_CMD_ERR_NOT_SUPPORTED;
            logMessageWarn(SUBSYSTEM_ID_IO_MANAGER,
                            "Invalid msg received [0x%03X]", msgIn.cmd);
    }
}


/*!
 ******************************************************************************
 *   \brief Get the current state of the OLR
 *
 *   If the voltages are not within their expected ranges the OLR lines will
 *   be fored to the OFF state.
 *
 *   \param  OLRStatePtr  pointer where the current OLR state will be stored
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
static IO_ERROR ioManGetCurrentOLRState(MCMOLR_STATES *OLRStatePtr)
{
    static lu_bool_t err_logged = LU_FALSE;
    MCMOLR_STATES    OLRState   = MCMOLR_STATE_OFF;

    if (OLRStatePtr == NULL)
    {
        return IO_ERROR_NULL;
    }

    // Read status of OLR Virtual Digital Points to determine what the input GPIO are set to
    OLRState = ioManCalcOLRState();

    // If the LOCAL/REMOTE points are in alarm we must show OFF state
    if ((digitalInputs[MCM_MON_DI_GPIO_LOCAL].flags  & IO_FLAG_ALARM)    ||
        (digitalInputs[MCM_MON_DI_GPIO_LOCAL].flags  & IO_FLAG_OFF_LINE) ||
        (digitalInputs[MCM_MON_DI_GPIO_REMOTE].flags & IO_FLAG_ALARM)    ||
        (digitalInputs[MCM_MON_DI_GPIO_REMOTE].flags & IO_FLAG_OFF_LINE))
    {
        if (err_logged == LU_FALSE)
        {
            logMessageError(SUBSYSTEM_ID_IO_MANAGER, "ALARM/OFFLINE: Reporting OFF State vLogic:[%f] vLocal:[%f] vRemote:[%f]!\n",
                            analogueInputs[MCM_MON_AI_VLOGIC_ADDR].value,
                            analogueInputs[MCM_MON_AI_VLOCAL].value,
                            analogueInputs[MCM_MON_AI_VREMOTE].value);

            err_logged = LU_TRUE;
        }

        // If either vLocal or vRemote is in alarm we must default to the OFF state!
        OLRState = MCMOLR_STATE_OFF;
    }
    else
    {
        err_logged = LU_FALSE;
    }

    *OLRStatePtr = OLRState;

    return IO_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief  Set the OLR State
 *
 *   This function drives the appropriate GPIO outputs to achieve the desired
 *   OLR State.  It is protected with a mutex to guarantee both Local/Remote
 *   lines are operated together.
 *
 *   NOTE: The Next Scan Time of the vLocal & vRemote AIs are adjusted to allow
 *   a better response to a change in their value.
 *
 *   \param  OLRState  Required OLR State
 *
 *   \return OLR state
 *
 ******************************************************************************
 */
static IO_ERROR ioManSetOLRState(MCMOLR_STATES OLRState)
{
    lu_int32_t ret   = IO_ERROR_NONE;

    logMessageInfo(SUBSYSTEM_ID_IO_MANAGER, "Attempting to set OLR to [%s]\n", MCMOLRStates[OLRState].olrDesc);

    // Lock mutex to prevent another thread setting OLR at the same time
    pthread_mutex_lock(&ioLock);

    // Record the current OLR State
    ioManGetCurrentOLRState(&previousOLRState);

    // Record the requested OLR State
    requestedOLRState = OLRState;

    // Send the outputs to the GPIOs to control the OLR State
    // No need to use ioManSetDigitalOutput() as this operation is already protected by the mutex
    switch (OLRState)
    {
        case MCMOLR_STATE_OFF:
        case MCMOLR_STATE_LOCAL:
        case MCMOLR_STATE_REMOTE:
            // Only operate the second DO if the first succeeds
            if ((ret = digitalOutputSet(&(digitalOutputs[MCM_MON_DO_GPIO_LOCAL]),  MCMOLRStates[OLRState].localState)) == IO_ERROR_NONE)
            {
                ret = digitalOutputSet(&(digitalOutputs[MCM_MON_DO_GPIO_REMOTE]), MCMOLRStates[OLRState].remoteState);
            }
        break;

        default:
            ret = IO_ERROR_RANGE;
            digitalOutputSet(&(digitalOutputs[MCM_MON_DO_GPIO_LOCAL]),  MCMOLRStates[MCMOLR_STATE_OFF].localState);
            digitalOutputSet(&(digitalOutputs[MCM_MON_DO_GPIO_REMOTE]), MCMOLRStates[MCMOLR_STATE_OFF].remoteState);
    }


    // Enable OLR Quick Scan
    // This mode speeds up the scanning of OLR monitoring inputs
// TODO - Review if this is required
    OLRQuickScanState = OLRQuickScan(LU_TRUE);

    // Release mutex
    pthread_mutex_unlock(&ioLock);

    return ret;
}

/*!
 ******************************************************************************
 *   \brief  Set OLR Quick Scan Mode
 *
 *   This function speeds up the scanning of the OLR Analogue and Digital Input
 *   points for two reasons:
 *
 *   1. Quicker OLR indication on the HMI
 *   2. Allows slower scan rates normally, that will minimise CPU usage
 *
 *   \param  enable  Required Quick Scan Mode
 *
 *   \return Quick Scan Mode
 *
 ******************************************************************************
 */
static lu_bool_t OLRQuickScan(lu_bool_t enable)
{
    struct timespec timeNow;

    logMessageDebug(SUBSYSTEM_ID_IO_MANAGER, "QuickScan %s", enable ? "Enabled" : "Disabled");

    // Set the Analogue and Digital Input Scan Intervals and Next Scan Times
    if (enable == LU_TRUE)
    {
        // Set the next scan time of the Local/Remote AIs & virtual DI channels to take in to account the
        // settling time of the analogue input channels  (this allows for a slower background scan rate)
        clock_gettimespec(&timeNow);

        OLRCheckDelay = timeNow;
        timespec_add_ms(&OLRCheckDelay, OLR_SETTLE_TIME_MS);

        analogueInputs[MCM_MON_AI_VLOGIC_ADDR].currentScanIntervalMs  = AI_OLR_QUICK_SCAN_TIME_MS;
        analogueInputs[MCM_MON_AI_VLOCAL].currentScanIntervalMs       = AI_OLR_QUICK_SCAN_TIME_MS;
        analogueInputs[MCM_MON_AI_VREMOTE].currentScanIntervalMs      = AI_OLR_QUICK_SCAN_TIME_MS;

        analogueInputs[MCM_MON_AI_VLOGIC_ADDR].nextScanTime           = timeNow;
        analogueInputs[MCM_MON_AI_VLOCAL].nextScanTime                = timeNow;
        analogueInputs[MCM_MON_AI_VREMOTE].nextScanTime               = timeNow;

        digitalInputs[MCM_MON_DI_GPIO_LOCAL].currentScanIntervalMs    = DI_OLR_QUICK_SCAN_TIME_MS;
        digitalInputs[MCM_MON_DI_GPIO_REMOTE].currentScanIntervalMs   = DI_OLR_QUICK_SCAN_TIME_MS;

        digitalInputs[MCM_MON_DI_GPIO_LOCAL].nextScanTime             = timeNow;
        digitalInputs[MCM_MON_DI_GPIO_REMOTE].nextScanTime            = timeNow;
    }
    else
    {
        // Restore the default (background) scanning
        analogueInputs[MCM_MON_AI_VLOGIC_ADDR].currentScanIntervalMs  = analogueInputs[MCM_MON_AI_VLOGIC_ADDR].config->scanIntervalMs;
        analogueInputs[MCM_MON_AI_VLOCAL].currentScanIntervalMs       = analogueInputs[MCM_MON_AI_VLOCAL].config->scanIntervalMs;
        analogueInputs[MCM_MON_AI_VREMOTE].currentScanIntervalMs      = analogueInputs[MCM_MON_AI_VREMOTE].config->scanIntervalMs;

        digitalInputs[MCM_MON_DI_GPIO_LOCAL].currentScanIntervalMs    = digitalInputs[MCM_MON_DI_GPIO_LOCAL].config->scanIntervalMs;
        digitalInputs[MCM_MON_DI_GPIO_REMOTE].currentScanIntervalMs   = digitalInputs[MCM_MON_DI_GPIO_REMOTE].config->scanIntervalMs;
    }

    return enable;
}

/*!
 ******************************************************************************
 *   \brief Calculate OLR State based on Local & Remote States
 *
 *   \param  N/A
 *
 *   \return OLR state
 *
 ******************************************************************************
 */
static MCMOLR_STATES ioManCalcOLRState(void)
{
    MCMOLR_STATES state;
    MCMOLR_STATES OLRState = MCMOLR_STATE_OFF;

    for (state = 0; state < MCMOLR_STATE_LAST; state++)
    {
        if ((MCMOLRStates[state].localState  == digitalInputs[MCM_MON_DI_GPIO_LOCAL].value) &&
            (MCMOLRStates[state].remoteState == digitalInputs[MCM_MON_DI_GPIO_REMOTE].value))
        {
            OLRState = (MCMOLR_STATES) state;
        }
    }

    return OLRState;
}
/*!
 ******************************************************************************
 *   \brief Monitor the OLR State and adjust as required
 *
 *   This function checks the status of the OLR.  During a change in OLR State
 *   the OLRCheckDelay will be used to introduce a delay to enable the Local/
 *   Remote voltage lines to settle.
 *   If the voltages are not within their expected ranges the OLR lines will
 *   be forced to the detected state.  This allows for an external OFF switch
 *   to force the OLR state to OFF.
 *
 *   If the external OFF switch is set to OFF this code will force the outputs
 *   to the OFF state.  When the switch is set to NOT OFF, the outputs will
 *   continue to force the OFF state until the user selects a new mode from the
 *   front panel OLR button.
 *
 *   \param
 *
 *   \return N/A
 *
 ******************************************************************************
 */

static void ioManCheckOLRState(void)
{
    struct timespec timeNow;


    if (HATMode == LU_TRUE)
    {
        return;
    }

    // Initialise the timers
    clock_gettimespec(&timeNow);

    // This allows the OLR check to be delayed when in QuickScan
    if (checkTimeout(&timeNow, &OLRCheckDelay) == LU_TRUE)
    {
        // Disable Quick Scan
        if (OLRQuickScanState == LU_TRUE)
        {
            OLRQuickScanState = OLRQuickScan(LU_FALSE);
        }

        // Store the state of the OLR
        ioManGetCurrentOLRState(&currentOLRState);

        if (currentOLRState != previousOLRState)
        {
            logMessageInfo(SUBSYSTEM_ID_IO_MANAGER,
                           "Change in OLR State Voltages! From:[%s] to:[%s]\n",
                           MCMOLRStates[previousOLRState].olrDesc, MCMOLRStates[currentOLRState].olrDesc);

            previousOLRState = currentOLRState;
        }

        // See if we're in the correct state now - if not, go back to our previous state
        if (currentOLRState != requestedOLRState)
        {
            logMessageWarn(SUBSYSTEM_ID_IO_MANAGER,
                            "OLR MISMATCH! Setting OLR to detected state:[%s] from:[%s] vLocal[%f v] vRemote[%f v]\n",
                            MCMOLRStates[currentOLRState].olrDesc, MCMOLRStates[requestedOLRState].olrDesc,
                            analogueInputs[MCM_MON_AI_VLOCAL].value,
                            analogueInputs[MCM_MON_AI_VREMOTE].value);

            // This is where we now attempt to set the outputs to the same value as the GPIO inputs.
            // We don't want to stress the outputs
            ioManSetOLRState((MCMOLR_STATES) currentOLRState);
        }
    }
}

/*!
 ******************************************************************************
 *   \brief Update the Analogue Input Status'
 *
 *   \param
 *
 *   \return Error Code
 *
 ******************************************************************************
 */

static IO_ERROR ioManUpdateAnalogueInputs(void)
{
    IO_ERROR        aiRet = IO_ERROR_NONE;
    IO_ERROR        ret   = IO_ERROR_NONE;
    lu_uint8_t      inputPointNum;
    struct timespec timeNow;


    // Initialise the timers
    clock_gettimespec(&timeNow);

    // Loop around the Analogue Inputs seeing who needs to scan
    for (inputPointNum = 0; inputPointNum < MCM_MON_AI_LAST; inputPointNum++)
    {
        // If there's no scan interval this input is an "update on request" point
        if (analogueInputs[inputPointNum].config->scanIntervalMs == 0)
        {
            continue;
        }

        if (checkTimeout(&timeNow, &(analogueInputs[inputPointNum].nextScanTime)) == LU_TRUE)
        {
            // Adjust the AI nextScanTime
            analogueInputs[inputPointNum].nextScanTime = timeNow;
            timespec_add_ms(&(analogueInputs[inputPointNum].nextScanTime), analogueInputs[inputPointNum].currentScanIntervalMs);

            // Lock the mutex
            pthread_mutex_lock(&ioLock);

            aiRet = analogueInputUpdate(&(analogueInputs[inputPointNum]));

            // Unlock the mutex
            pthread_mutex_unlock(&ioLock);

            // Store the last error code for returning to caller
            if (aiRet != IO_ERROR_NONE)
            {
                ret = aiRet;
            }

            // Special Checks

            // If the Temperature is greater than the SHUTDOWN temperature we MUST shutdown!!
            if ((inputPointNum == MCM_MON_AI_TEMP_ADDR) && (analogueInputs[inputPointNum].value > SHUTDOWN_OVER_TEMP))
            {
                logMessageFatal(SUBSYSTEM_ID_IO_MANAGER, "Over temperature [%f C]!! Shutting Down...\n", analogueInputs[inputPointNum].value);
                SET_STATE(myState, OK_LEDSTATE_CRITICAL);

                // Over Temperature - send a shutdown request!!
                if (sendReqShutdownQMsg(threadInfo[THREAD_DATA_COMS].threadMqFd, mainMqFd, QMSG_EXIT_ACTION_SHUTDOWN, QMSG_EXIT_CODE_OVERTEMP) < 0)
                {
                    // If we fail to send the msg to the main thread we can initiate clean shutdown of the application here
                    logMessageError(SUBSYSTEM_ID_IO_MANAGER, "Error sending RTU REBOOT to main Thread. Setting Closedown = 1!");
                    Closedown = LU_TRUE;
                }
            }

            // See if this Analogue is ONLINE or not
            if (analogueInputs[inputPointNum].flags & IO_FLAG_OFF_LINE)
            {
                SET_STATE(myState, OK_LEDSTATE_WARNING);
            }
        } // timeout
    } // for

    return ret;
}

/*!
 ******************************************************************************
 *   \brief Update the Digital Input Status'
 *
 *   \param
 *
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
static IO_ERROR ioManUpdateDigitalInputs(void)
{
    IO_ERROR diRet = IO_ERROR_NONE;
    IO_ERROR ret   = IO_ERROR_NONE;
    lu_uint8_t      inputPointNum;
    struct timespec timeNow;


    // Initialise the timers
    clock_gettimespec(&timeNow);

    // Loop around the Digital Inputs seeing who needs to scan
    for (inputPointNum = 0; inputPointNum < MCM_MON_DI_LAST; inputPointNum++)
    {
        // If the eventAllValues flag is set, simply send the value without checking
        // scan times etc
        if (eventAllValues == LU_TRUE)
        {
            // Lock the mutex
            pthread_mutex_lock(&ioLock);

            diRet = digitalInputUpdate(&(digitalInputs[inputPointNum]));

            // Force update of the value to the Main App
            diRet = ioManNotifyDigitalInputChange(inputPointNum, LU_TRUE);

            // Unlock the mutex
            pthread_mutex_unlock(&ioLock);

            continue;
        }

        // If there's no scan interval this input is an "update on request" point
        if (digitalInputs[inputPointNum].config->scanIntervalMs == 0)
        {
            continue;
        }

        if (checkTimeout(&timeNow, &(digitalInputs[inputPointNum].nextScanTime)) == LU_TRUE)
        {
            // Adjust the DI nextScanTime
            digitalInputs[inputPointNum].nextScanTime = timeNow;
            timespec_add_ms(&(digitalInputs[inputPointNum].nextScanTime), digitalInputs[inputPointNum].currentScanIntervalMs);

            // Lock the mutex
            pthread_mutex_lock(&ioLock);

            diRet = digitalInputUpdate(&(digitalInputs[inputPointNum]));

            // If the Digital Value changes we should inform the Data Coms Thread to tell the MCM App
            diRet = ioManNotifyDigitalInputChange(inputPointNum, LU_FALSE);

            // Store the last error code for returning to caller
            if (diRet != IO_ERROR_NONE)
            {
                ret = diRet;
            }

            // Unlock the mutex
            pthread_mutex_unlock(&ioLock);

            // See if this Digital is ONLINE or not
            if (digitalInputs[inputPointNum].flags & IO_FLAG_OFF_LINE)
            {
                SET_STATE(myState, OK_LEDSTATE_WARNING);
            }
        }  // timeout
    } // for

    return ret;
}


/*!
 ******************************************************************************
 *   \brief Notify Digital Input change as required
 *
 *   Requires mutex protection
 *
 *   \param channel Channel Number
 *          force   Force notification of current value
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
static IO_ERROR ioManNotifyDigitalInputChange(lu_uint8_t channel, lu_bool_t force)
{
    /*
     * If the value or flags have changed, notify the Main App
     *
     */
    if (((digitalInputs[channel].config->notifyOnChange == LU_TRUE) &&
            ((digitalInputs[channel].value != digitalInputs[channel].lastValue)   ||
             (digitalInputs[channel].flags != digitalInputs[channel].lastFlags))) ||
         (force == LU_TRUE))
    {
        digitalInputs[channel].lastValue = digitalInputs[channel].value;
        digitalInputs[channel].lastFlags = digitalInputs[channel].flags;

        // If the flags indicate the point is in RESCAN, do not publish any changes
        if ((!(digitalInputs[channel].flags & IO_FLAG_RESCANNED)) && (!(digitalInputs[channel].flags & IO_FLAG_INVALID)))
        {
            // Send a DI Update to the Main Thread (destined for the Data Socket)
            sendRepDIQMsg(threadInfo[THREAD_DATA_COMS].threadMqFd,
                          mainMqFd, channel,
                          digitalInputs[channel].flags,
                          digitalInputs[channel].value);
        }
    }

    return IO_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief  Read NVRAM Header and place in static
 *
 *   \param dataPtr - NVRAM Data returned in pointer
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
static IO_ERROR ioManReadNVRAMIDHeader(NVRAM_TYPE NVRam, NVRAMInfoStr *dataPtr)
{
    IO_ERROR     ret = IO_ERROR_NONE;
    FILE        *IDNVRamFd = NULL;
    lu_uint32_t  i;
    lu_uint16_t  offset;
    lu_uint16_t  size;
    lu_uint8_t   *NVRamBasePtr = NULL;  // calloc'd - must be freed when exiting
    NVRAMInfoStr *NVRamInfoPtr = NULL;


    /* Attempt to find the supplied NVRam file location */
    for (i = 0; i< sizeof(IdNVRamFiles)/sizeof(MCMIDNRAMFileStr); i++)
    {
        if (IdNVRamFiles[i].NVRam == NVRam)
        {
            break;
        }
    }

    if (IdNVRamFiles[i].NVRam != NVRam)
    {
        return IO_ERROR_GENERAL;
    }

    if (dataPtr == NULL)
    {
        return IO_ERROR_NULL;
    }

    if ((IDNVRamFd = fopen(IdNVRamFiles[i].fileName, "r+")) == NULL)
    {
        return IO_ERROR_READ;
    }

    do
    {
        if (getNVRAMBlockOffsetAndSize(IdNVRamFiles[i].NVRam, NVRAM_ID_BLK_INFO, &offset, &size) != LU_TRUE)
        {
            ret = IO_ERROR_GENERAL;
            break;
        }

        if ((NVRamBasePtr = (lu_uint8_t *) calloc(size, sizeof(lu_uint8_t))) == NULL)
        {
            ret = IO_ERROR_GENERAL;
            break;
        }

        if (fseek(IDNVRamFd, (long)offset, SEEK_SET) != 0)
        {
            ret = IO_ERROR_READ;
            break;
        }

        if (fread(NVRamBasePtr, 1, size, IDNVRamFd) != size)
        {
            ret = IO_ERROR_READ;
            break;
        }

        if (NVRAMBlockCheckGoodCRC(NVRamBasePtr, IdNVRamFiles[i].NVRam, NVRAM_ID_BLK_INFO) != LU_TRUE)
        {
            ret = IO_ERROR_READ;

            break;
        }

        // Set the NVRamPtr
        NVRamInfoPtr = (NVRAMInfoStr *) (NVRamBasePtr + sizeof(NVRAMBlkHeadStr));

        *dataPtr = *NVRamInfoPtr;

    } while(0);


    if (IDNVRamFd)
    {
        fclose(IDNVRamFd);
    }

    // Free NVRam copy buffer
    if (NVRamBasePtr)
    {
        free(NVRamBasePtr);
    }

    return ret;
}



/*
 *********************** End of file ******************************************
 */
