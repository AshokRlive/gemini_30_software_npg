/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCMMonitor
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       The main of MCMMonitor
 *
 *    CURRENT REVISION
 *
 *               $Revision::               $: (Revision of last commit)
 *               $Author::                 $: (Author of last commit)
 *       \date   $Date::                   $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25/07/13      andrews_s   Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <sched.h>
#include <string.h>

#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/prctl.h> // prctl()


#include <mqueue.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "GlobalDefinitions.h"
#include "svnRevision.h"
#include "timeOperations.h"

#include "Main.h"
#include "MCMIOMap.h"
#include "AppManager.h"
#include "LEDManager.h"
#include "IOManager.h"
#include "WDogManager.h"
#include "DataSockManager.h"
#include "LogMessage.h"

#if LINUX_CAPABILITES
#include "Capabilities.h"
#endif

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define QUEUE_MAIN_SIZE       100   // Queue size for main thread
#define QUEUE_THREAD_SIZE     50    // Queue size for child threads


/* Define AF_CAN and PF_CAN if not yet defined in libc */
#ifndef AF_CAN
#define AF_CAN 29
#endif

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
pid_t AppManPID = 0;


/*!
 ******************************************************************************
 *   \brief Handles the command line arguments
 *
 *
 *   \param argc    argc from main
 *          argv    argv from main
 *
 *
 *   \return
 *
 ******************************************************************************
 */
static void commandLineArgs(int argc, char *argv[]);

/*!
 ******************************************************************************
 *   \brief Set up the message queue
 *
 *
 *   \param queueNamePtr    Pointer to the message queue name
 *
 *
 *   \return
 *
 ******************************************************************************
 */
static mqd_t createMsgQueue(const lu_int8_t *queueNamePtr);

/*!
 ******************************************************************************
 *   \brief Initialise the threadInfo array
 *
 *   \param     None
 *
 *
 *   \return
 *
 ******************************************************************************
 */
static void initThreadInfo(void);

/*!
 ******************************************************************************
 *   \brief     Close all the thread message queues
 *
 *   \param     None
 *
 *
 *   \return
 *
 ******************************************************************************
 */
static void closeThreadInfo(void);

/*!
 ******************************************************************************
 *   \brief Sets the Main Thread's State
 *
 *   Sets the main thread's state, and ultimately the state of the OK LED
 *
 *   \param     None
 *
 *
 *   \return
 *
 ******************************************************************************
 */
static void setMCMState(void);

/*!
 ******************************************************************************
 *   \brief Message Queue Handler
 *
 *    Handles an incoming message on the queue
 *
 *   \param     msgIn   QProtocol Message
 *
 *
 *   \return
 *
 ******************************************************************************
 */
static void decodeQMsq(QMsgType msgIn);

/*!
 ******************************************************************************
 *   \brief Run the ApplicationManager
 *
 *   The application manager does a fork()/exec() to run applications - this is
 *   not so reliable in a multi-threaded program (such as this).  This function
 *   forks the main application before any other threads have been run.
 *
 *   \param     None
 *
 *   \return
 *
 ******************************************************************************
 */
static void launchAppManager(void);

/*!
 ******************************************************************************
 *   \brief Check the child process is still running
 *
 *   The child is connected to the parent process by a pipe.  While the pipe is
 *   readable the child is running, else it has closed.
 *
 *   \param     None
 *
 *   \return    0 - Child is Running
 *              1 - Child has Stopped
 *             -1 - Error
 *
 ******************************************************************************
 */
static lu_int32_t checkAppManager(void);


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
lu_bool_t   Closedown         = LU_FALSE;

// Command-line arguments settings
lu_bool_t   ArgIgnoreWatchdog = LU_FALSE; // Ignore WDOG Kick and Sys Healthy
lu_bool_t   ArgNoStop         = LU_FALSE; // Ignore PING Timeout from APP in DataSocket Thread

lu_bool_t   HATMode           = LU_FALSE; // HAT Mode Enabled or Disabled

lu_char_t*  ArgWorkingPath    = NULL;

ThreadInfoType threadInfo[THREAD_LAST] = { { LU_TRUE,  THREAD_MAIN,         "/mon",     -1, OK_LEDSTATE_INITIALISING},
                                           { LU_TRUE,  THREAD_LED,          "/led",     -1, OK_LEDSTATE_INITIALISING},
                                           { LU_TRUE,  THREAD_WDOG,         "/wdog",    -1, OK_LEDSTATE_INITIALISING},
                                           { LU_TRUE,  THREAD_IOMAN,        "/ioman",   -1, OK_LEDSTATE_INITIALISING},
                                           { LU_TRUE,  THREAD_APPMAN,       "/appman",  -1, OK_LEDSTATE_INITIALISING},
                                           { LU_TRUE,  THREAD_DATA_COMS,    "/data",    -1, OK_LEDSTATE_INITIALISING},
/* Pseudo Thread for Application Status */ { LU_FALSE, THREAD_APP,          "",         -1, OK_LEDSTATE_HEALTHY}
                                         };

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static const char CmdShutdownRTU[] = "/sbin/halt -d"; // Halts RTU (WDog must be kicked by OS)
static const char CmdRebootRTU[]   = "/sbin/reboot";  // Reboots the RTU
//Runs script after update. Note that scripts runs once and deletes itself afterwards.
// Note that it runs in the background.
static const char CmdPostUpdateMCM[] = "./post_update_mcm.sh &";
static OK_LEDSTATE RTUState;
static lu_int32_t childPipeFd[2];


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

mqd_t connectMsgQueue(const lu_int8_t *namePtr)
{
    mqd_t mqFd;

    // Set up the Queue Attributes
    struct mq_attr mqAttr;

    mqAttr.mq_flags   = 0;
    mqAttr.mq_curmsgs = 0;
    mqAttr.mq_maxmsg  = QUEUE_THREAD_SIZE;
    mqAttr.mq_msgsize = MSG_LEN;

    // Open a queue for incoming commands
    if ((mqFd = mq_open(namePtr, O_RDWR, 0777, &mqAttr)) < 0)
    {
        logMessageError(SUBSYSTEM_ID_MAIN,
                        "registerThread: Error opening [%s] queue!\n", namePtr);
        return mqFd;
    }
    else if (mqFd == 0)
    {
        // fd of zero makes mq_send fail!!!!  Try to get another one
        // Do not close 0 or we will end up reusing it!
        //  mq_close(mqFd);
        if ((mqFd = mq_open(namePtr, O_RDWR, 0777, &mqAttr)) < 0)
        {
            logMessageError(SUBSYSTEM_ID_MAIN,
                            "registerThread: Error opening [%s] queue!\n",
                            namePtr);
            return mqFd;
        }
    }

    return mqFd;
}

lu_int32_t processMsgQueue(mqd_t mqFd, void (*decodeQMsq)(QMsgType), lu_int32_t timeoutMs)
{
    QMsgType msgIn;

    lu_int32_t ret = 0;
    fd_set readfds;
    lu_int16_t maxfd;
    struct timeval timeoutVal;

    struct timespec start;
    struct timespec end;
    struct timespec elapsed;
    struct timespec timeoutCopy;

    // Adjust the microsecond timeout to a timeval
    timeoutVal.tv_sec = timeoutMs / 1000;
    timeoutVal.tv_usec = (timeoutMs % 1000) * 1000;

    // Linux can change the timeoutVal in select(), use a local copy
    timeoutCopy.tv_sec  = timeoutVal.tv_sec;
    timeoutCopy.tv_nsec = (timeoutVal.tv_usec * 1000);

    maxfd = mqFd + 1;
    FD_ZERO(&readfds);
    FD_SET(mqFd, &readfds);

    while ((timeoutVal.tv_sec >= 0) && (timeoutVal.tv_usec > 0))
    {
        // Log when the select() started
        clock_gettimespec(&start);

        if ((ret = select(maxfd, &readfds, NULL, NULL, &timeoutVal)) > 0)
        {
            if (FD_ISSET(mqFd, &readfds) > 0)
            {
                if (mq_receive(mqFd, (lu_int8_t *) &msgIn, MSG_LEN, 0) > 0)
                {
                    decodeQMsq(msgIn);
                }
            }
        }
        else if (ret < 0)
        {
            // Any thread that is using signals could cause the select() to return prematurely.
            // This is probably the correct thing to do, so don't return an error code.
            if (errno == EINTR)
            {
                ret = 0;
            }
            else
            {
                logMessageWarn(SUBSYSTEM_ID_MAIN, "Select() error on mq_fd:[%d] errno:[%d]!", mqFd, errno);

                // Prevent processor hogging on error
                lucy_usleep(timeoutMs * 1000);
                break;
            }
        }

        // Adjust the timeout and go around again if there's time left
        clock_gettimespec(&end);

        // Store the length of time the select() took
        timespec_elapsed(&start, &end, &elapsed);

        // Reduce timeout by the length of time the select() took
        timeoutCopy.tv_sec -= elapsed.tv_sec;
        timeoutCopy.tv_nsec -= elapsed.tv_nsec;

        // Put the new calculated timeout back in to select()
        timeoutVal.tv_sec = timeoutCopy.tv_sec;
        timeoutVal.tv_usec = (timeoutCopy.tv_nsec / 1000);
    }

    return ret;
}

sigset_t initSignalMasks(void)
{
    sigset_t newSignalMask;
    sigset_t oldSignalMask;

    // Mask most signals for us and child threads
    sigfillset(&newSignalMask);
    sigdelset(&newSignalMask, SIGINT);
    sigdelset(&newSignalMask, SIGTERM);

    if (pthread_sigmask(SIG_BLOCK, &newSignalMask, &oldSignalMask) != 0)
    {
        logMessageFatal(SUBSYSTEM_ID_MAIN,
                        "Unable to pthread_sigmask()!");
        exit(EXIT_FAILURE);
    }

    return oldSignalMask;
}

void initThreadAttributes(pthread_attr_t *threadAttrPtr, size_t stackSize, lu_bool_t detached, lu_uint8_t priority)
{
    size_t             threadStackSize;
    struct sched_param schp;


    // Ensure the stack size is not too small
    if (stackSize < PTHREAD_STACK_MIN)
    {
        stackSize = PTHREAD_STACK_MIN;
    }

    // Make sure the requested stack size is on a block boundary
    threadStackSize = (size_t) (stackSize / sysconf(_SC_PAGESIZE));
    threadStackSize = threadStackSize * sysconf(_SC_PAGESIZE);

    // Set the stack size for the threads.  It's not a show-stopper if these fail.
    if (pthread_attr_init(threadAttrPtr) != 0)
    {
        logMessageFatal(SUBSYSTEM_ID_MAIN,
                        "Unable to pthread_attr_init()!");
    }

    if (detached == LU_TRUE)
    {
        if (pthread_attr_setdetachstate(threadAttrPtr, PTHREAD_CREATE_DETACHED) != 0)
        {
            logMessageFatal(SUBSYSTEM_ID_MAIN,
                            "Unable to pthread_attr_setdetachstate()!");
        }
    }

    if (pthread_attr_setstacksize(threadAttrPtr, threadStackSize) != 0)
    {
        logMessageFatal(SUBSYSTEM_ID_MAIN,
                        "Unable to pthread_attr_setstacksize()!");
    }

    // Set the scheduling through through the attributes
    if (pthread_attr_setinheritsched(threadAttrPtr, PTHREAD_EXPLICIT_SCHED) != 0)
    {
        logMessageFatal(SUBSYSTEM_ID_MAIN,
                        "Unable to pthread_attr_setinheritsched()!");
    }

    // Set scheduling policy
    if (pthread_attr_setschedpolicy(threadAttrPtr, SCHED_FIFO) != 0)
    {
        logMessageFatal(SUBSYSTEM_ID_MAIN,
                        "Unable to pthread_attr_setschedpolicy()!");
    }

    // Set the thread priority
    if (priority > sched_get_priority_max(SCHED_FIFO))
    {
        priority = sched_get_priority_max(SCHED_FIFO);
    }
    if (priority < sched_get_priority_min(SCHED_FIFO))
    {
        priority = sched_get_priority_min(SCHED_FIFO);
    }

    memset(&schp, 0, sizeof(schp));
    schp.sched_priority = priority;
    if (pthread_attr_setschedparam(threadAttrPtr, &schp) != 0)
    {
        logMessageFatal(SUBSYSTEM_ID_MAIN,
                        "Unable to pthread_attr_setschedparam()!");
    }
}


int main(int argc, char *argv[])
{
    OK_LEDSTATE myState = OK_LEDSTATE_INITIALISING;
    OK_LEDSTATE myLastState = OK_LEDSTATE_INITIALISING;
    lu_int32_t intVal;
    sigset_t oldSignalMask;
    pthread_attr_t threadAttrNormPriority;
    pthread_attr_t threadAttrHighPriority;
    lu_bool_t factoryPinStatus;


    // Initialise the logMessage library (syslog)
    logMessageInit(0, MCM_LOG_FACILITY_MCMMONITOR, "MCMMon");
    logMessageInfo(SUBSYSTEM_ID_MAIN, "MCMMonitor - Started");

    // Handle Command Line Arguments
    commandLineArgs(argc, argv);

    if (ioManGetFactoryPin(&factoryPinStatus) != IO_ERROR_NONE)
    {
        logMessageWarn(SUBSYSTEM_ID_MAIN, "Failed to read Factory Pin Status.  Assuming it is NOT set");
        factoryPinStatus = LU_FALSE;
    }

    // Set our PWD
    if (chdir(ArgWorkingPath == NULL ? MonitorWorkingPath : ArgWorkingPath) != 0)
    {
        logMessageWarn(SUBSYSTEM_ID_MAIN, "Error setting PWD to [%s]!",
        		ArgWorkingPath == NULL ? MonitorWorkingPath : ArgWorkingPath);
    }

    // Set the thread name (ps -T)  - ignore errors
    prctl(PR_SET_NAME, "Main", 0, 0, 0);

    // Only start the worker threads if the factory pin is not asserted
    if (factoryPinStatus == LU_TRUE)
    {
        logMessageInfo(SUBSYSTEM_ID_MAIN, "Factory Pin detected");
    }
    else
    {
        //Launch post update script
        system(CmdPostUpdateMCM); //Return code ignored

        // Initialise the threadInfo array
        initThreadInfo();

        // Initialise the thread attributes
        // Some threads may need to be higher priority
        initThreadAttributes(&threadAttrNormPriority, THREAD_STACK_SIZE, LU_FALSE, THREAD_LOW_PRIORITY);
        initThreadAttributes(&threadAttrHighPriority, THREAD_STACK_SIZE, LU_FALSE, THREAD_HIGH_PRIORITY);

        // Initialise the signal masks
        oldSignalMask = initSignalMasks();

        // Launch the Application Manager (fork())
        launchAppManager();

#if LINUX_CAPABILITES
        // Limit our capabilities
        cap_disable(CAP_AUDIT_CONTROL);
        cap_disable(CAP_AUDIT_WRITE);
        cap_disable(CAP_CHOWN);
        cap_disable(CAP_DAC_OVERRIDE);
        cap_disable(CAP_DAC_READ_SEARCH);
        cap_disable(CAP_FOWNER);
        cap_disable(CAP_FSETID);
        cap_disable(CAP_IPC_LOCK);
        cap_disable(CAP_IPC_OWNER);
//        cap_disable(CAP_KILL);  // Required for kill()!
        cap_disable(CAP_LEASE);
        cap_disable(CAP_LINUX_IMMUTABLE);
        cap_disable(CAP_MAC_ADMIN);
        cap_disable(CAP_MAC_OVERRIDE);
        cap_disable(CAP_MKNOD);
        cap_disable(CAP_NET_ADMIN);
        cap_disable(CAP_NET_BIND_SERVICE);
        cap_disable(CAP_NET_BROADCAST);
        cap_disable(CAP_NET_RAW);
        cap_disable(CAP_SETFCAP);
        cap_disable(CAP_SETGID);
        cap_disable(CAP_SETPCAP);
        cap_disable(CAP_SETUID);
        cap_disable(CAP_SYSLOG);
        cap_disable(CAP_SYS_ADMIN);
        cap_disable(CAP_SYS_BOOT);
        cap_disable(CAP_SYS_CHROOT);
        cap_disable(CAP_SYS_MODULE);
//        cap_disable(CAP_SYS_NICE);  // Required for pthread_attr_setdetachstate
        cap_disable(CAP_SYS_PACCT);
        cap_disable(CAP_SYS_PTRACE);
        cap_disable(CAP_SYS_RAWIO);
        cap_disable(CAP_SYS_RESOURCE);
        cap_disable(CAP_SYS_TIME);
        cap_disable(CAP_SYS_TTY_CONFIG);
#endif

        // Launch the remaining worker threads
        // Critical threads MUST be HIGH PRIORITY or the threads will block while the Main Application applies the configuration
        // during a restart
        intVal = 0;
        intVal += pthread_create(&(threadInfo[THREAD_WDOG].threadId),         &threadAttrHighPriority, &wdogThread,        NULL);
        intVal += pthread_create(&(threadInfo[THREAD_IOMAN].threadId),        &threadAttrHighPriority, &ioManagerThread,   NULL);
        intVal += pthread_create(&(threadInfo[THREAD_LED].threadId),          &threadAttrNormPriority, &ledThread,         NULL);
        intVal += pthread_create(&(threadInfo[THREAD_DATA_COMS].threadId),    &threadAttrHighPriority, &dataSockThread,    NULL);

        if (intVal != 0)
        {
            logMessageFatal(SUBSYSTEM_ID_MAIN, "Unable to start all threads");
            exit(EXIT_FAILURE);
        }
    }

    RTUState = OK_LEDSTATE_INITIALISING;
    myState = OK_LEDSTATE_HEALTHY;

    // Main Loop
    while (!Closedown)
    {
        // If the factory pin is asserted we should just sleep
        if (factoryPinStatus == LU_TRUE)
        {
            lucy_usleep(1000000);
            continue;
        }

        // Check that the AppManager child is still running
        if (checkAppManager() != 0)
        {
            if (!Closedown)
            {
                logMessageFatal(SUBSYSTEM_ID_MAIN, "AppManager child has exited.");
                Closedown = 1;
                continue;
            }
        }

        processMsgQueue(threadInfo[THREAD_MAIN].threadMqFd, &decodeQMsq, 500);

        // Now set the LED State
        setMCMState();

        if (myState != myLastState)
        {
            logMessageInfo(SUBSYSTEM_ID_MAIN, "State changed from [%d] to [%d]", myLastState, myState);

            threadInfo[THREAD_MAIN].threadState = myState;
            myState = myLastState;
        }

        lucy_usleep(10000);
    }

    logMessageInfo(SUBSYSTEM_ID_MAIN, "MCM Monitor closing...");

    if (factoryPinStatus == LU_FALSE)
    {
        // Tidy up Message Queue
        closeThreadInfo();

        // Wait for our child threads to terminate...
        pthread_join(threadInfo[THREAD_LED].threadId, NULL );
        pthread_join(threadInfo[THREAD_WDOG].threadId, NULL );
        pthread_join(threadInfo[THREAD_IOMAN].threadId, NULL );
        pthread_join(threadInfo[THREAD_DATA_COMS].threadId, NULL );
    }

    // Wait for AppManager to exit
//    int status;
//    wait(&status);

    logMessageInfo(SUBSYSTEM_ID_MAIN, "MCM Monitor Exited");

    return EXIT_SUCCESS;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */
static void commandLineArgs(int argc, char *argv[])
{
    lu_int32_t arg;


#ifdef EMC_TEST
    fprintf(stdout, "MCMMonitor: EMC_TEST Version\n");
    logMessageInfo(SUBSYSTEM_ID_MAIN, "EMC_TEST Version");
#endif

    logMessageInfo(SUBSYSTEM_ID_MAIN, "v%i.%i.%i svn:%ld\n",
                    MCM_MONITOR_SOFTWARE_VERSION_MAJOR,
                    MCM_MONITOR_SOFTWARE_VERSION_MINOR,
                    MCM_MONITOR_SOFTWARE_VERSION_PATCH,
                    _SVN_REVISION);

    if (argc > 1)
    {
        fprintf(stdout, "\nMCMMonitor - (c) Lucy Electric\n");
    }

    // Handle command line arguments
    while ((arg = getopt(argc, argv, "hvwsp:")) != -1)
    {
        switch (arg)
        {
            case 'v':
                fprintf(stdout, "v%i.%i.%i svn:%ld\n",
                                MCM_MONITOR_SOFTWARE_VERSION_MAJOR,
                                MCM_MONITOR_SOFTWARE_VERSION_MINOR,
                                MCM_MONITOR_SOFTWARE_VERSION_PATCH,
                                _SVN_REVISION);
                exit(0);
            break;

            case 'w':
                // Ignore WDog KickBack and Sys Healthy
                ArgIgnoreWatchdog = LU_TRUE;
            break;

            case 's':
                // Ignore PING Timeout from APP in DataSocket Thread
                ArgNoStop = LU_TRUE;
            break;

            case 'p':
            	ArgWorkingPath = optarg;
			break;

            default:
                fprintf(stdout,
                                "Options:\n-h Display this help\n-v Display version\n-w Ignore Watchdog\n-s "
                                "No Application Stopping\n-p <PATH> Set working path\n");
                exit(0);
        }
    }
}

static mqd_t createMsgQueue(const lu_int8_t *queueNamePtr)
{
    struct mq_attr mqAttr;
    mqd_t          mqFd = -1;

    // Set the attributes for the message queue
    mqAttr.mq_flags = 0;
    mqAttr.mq_curmsgs = 0;
    mqAttr.mq_maxmsg = QUEUE_MAIN_SIZE;
    mqAttr.mq_msgsize = MSG_LEN;

    // Delete any existing Queues
    mq_unlink(queueNamePtr);

    // Open a queue for incoming commands
    if ((mqFd = mq_open(queueNamePtr, O_RDWR | O_CREAT, 0777, &mqAttr)) < 0)
    {
        logMessageFatal(SUBSYSTEM_ID_MAIN, "Error opening [%s] queue!",
                        queueNamePtr);
        exit(3);
    }
    else if (mqFd == 0)
    {
        // fd of zero makes mq_send fail!!!!  Try to get another one
        // Do not close 0 or we will end up reusing it!
        //  mq_close(mqFd);
        if ((mqFd = mq_open(queueNamePtr, O_RDWR | O_CREAT, 0777, &mqAttr)) < 0)
        {
            logMessageError(SUBSYSTEM_ID_MAIN,
                            "registerThread: Error opening [%s] queue!\n",
                            queueNamePtr);
        }
    }

    return mqFd;
}

static void initThreadInfo(void)
{
    lu_int32_t i;

    // Loop around the threads, creating their msg queues
    for (i = 0; i < THREAD_LAST; i++)
    {
        // Only create MsgQueues for actual Threads
        if (threadInfo[i].realThread == LU_TRUE)
        {
            threadInfo[i].threadMqFd = createMsgQueue(threadInfo[i].mqName);
            threadInfo[i].threadState = OK_LEDSTATE_INITIALISING;
        }
    }
}

static void closeThreadInfo(void)
{
    lu_int32_t i;

    // Loop around the threads, closing and deleting their msg queues
    for (i = 0; i < THREAD_LAST; i++)
    {
        if (threadInfo[i].threadMqFd >= 0)
        {
            close(threadInfo[i].threadMqFd);
        }

        if (threadInfo[i].realThread == LU_TRUE)
        {
            unlink(threadInfo[i].mqName);
        }
    }
}

static void setMCMState(void)
{
    OK_LEDSTATE newState, lastState;
    lu_uint16_t i;

    // Loop around the threadState array, storing the worst state in the newState STATE
    lastState = RTUState;
    newState = OK_LEDSTATE_HEALTHY;

    // Check all the threads have at least registered their msg q fds
    for (i = 0; i < THREAD_LAST; i++)
    {
        // Only set to INITIALISING if this is a real Thread (ie had an mqName)
        if ((threadInfo[i].realThread == LU_TRUE) && (threadInfo[i].threadMqFd < 0))
        {
            newState = OK_LEDSTATE_INITIALISING;
        }
    }

    for (i = 0; i < THREAD_LAST; i++)
    {
        if (threadInfo[i].threadState > newState)
        {
            newState = threadInfo[i].threadState;
        }
    }

    // No harm in reaffirming the MCM State? - the LED Thread may not have been registered when the first change came in
    if (newState != lastState)
    {
        RTUState = newState;
        lastState = newState;

        // Send message to the LEDManager thread to set the State!
        if (threadInfo[THREAD_LED].threadMqFd >= 0)
        {
            sendReqLEDStateQMsg(threadInfo[THREAD_MAIN].threadMqFd,
                            threadInfo[THREAD_LED].threadMqFd, RTUState);
        }
    }
}

static void decodeQMsq(QMsgType msgIn)
{
    lu_uint8_t  command;

    // Extract the command (removing error flag)
    // The error flag will remain in the command when forwarded
    command = msgIn.cmd & ~QMSG_CMD_REP_ERROR;

    switch (command)
    {
        case QMSG_CMD_REP_STATE:
            // Update a Thread's State
            if ((msgIn.data1.intVal >= 0) && (msgIn.data1.intVal < THREAD_LAST))
            {
                threadInfo[msgIn.data1.intVal].threadState = msgIn.data2.intVal;
            }
        break;

        case QMSG_CMD_REQ_DI: // Must be from external app
        case QMSG_CMD_REQ_AI: // Must be from external app
        case QMSG_CMD_REQ_SET_DO: // Must be from external app
        case QMSG_CMD_REQ_SET_OLR: // Must be from external app
            if (threadInfo[THREAD_IOMAN].threadMqFd >= 0)
            {
                msgIn.qDest = threadInfo[THREAD_IOMAN].threadMqFd;
                sendQMsg(&msgIn);
            }
        break;

        case QMSG_CMD_REP_AI:
        case QMSG_CMD_REP_DI:
        case QMSG_CMD_REP_SET_DO:
        case QMSG_CMD_REP_SET_OLR:
        case QMSG_CMD_REP_POWERSAVE:
            // Forward the response to the requester
            if (msgIn.qSrc != threadInfo[THREAD_MAIN].threadMqFd)
            {
                msgIn.qDest = msgIn.qSrc;
                msgIn.qSrc  = threadInfo[THREAD_MAIN].threadMqFd;

                sendQMsg(&msgIn);
            }
        break;

        case QMSG_CMD_REQ_SHUTDN:
            logMessageWarn(SUBSYSTEM_ID_MAIN,
                            "RTU NORMAL SHUTDOWN/REBOOT REQUESTED. Reason:[%d]!", msgIn.data2.intVal);

            // Send HP Mesg to DataComs to shutdown external app
            sendReqShutdownQMsg(threadInfo[THREAD_MAIN].threadMqFd,
                            threadInfo[THREAD_DATA_COMS].threadMqFd,
                            msgIn.data1.intVal, msgIn.data2.intVal);

            lucy_usleep(1000000);

            // Stop all monitored applications
            sendReqStopAppQMsg(threadInfo[THREAD_MAIN].threadMqFd, threadInfo[THREAD_APPMAN].threadMqFd);

            // Allow time for the message to get to the application
            lucy_usleep(1000000);

            // Inform the threads to close
            Closedown = LU_TRUE;
            logMessageFatal(SUBSYSTEM_ID_MAIN,
                            "Issuing shutdown command in 1 second...");

            // Allow the threads a little time to close
            lucy_usleep(1000000);

            // data1 holds the Action Code which determines if we shutdown or reboot
            if (msgIn.data1.intVal == QMSG_EXIT_ACTION_SHUTDOWN)
            {
                // Put the WDog Kick back in the hands of the kernel
                logMessageWarn(SUBSYSTEM_ID_MAIN, "Putting Watchdog Kick back to OS control");
                setKLEDTimer(KLED_WDOG_1HZ_KICK_NAME, OS_WDOG_DELAY_ON, OS_WDOG_DELAY_OFF);

                // Issue the halt command
                logMessageFatal(SUBSYSTEM_ID_MAIN, "Issuing [%s]", CmdShutdownRTU);
                system(CmdShutdownRTU);
            }
            else
            {
                // Issue the reboot command
                logMessageWarn(SUBSYSTEM_ID_MAIN, "Issuing [%s]", CmdRebootRTU);
                system(CmdRebootRTU);
            }
        break;

        case QMSG_CMD_ALM_SHUTDN:
            logMessageWarn(SUBSYSTEM_ID_MAIN, "RTU ALARM SHUTDOWN REQUESTED. Reason:[%d]!", msgIn.data2.intVal);

            // Send HP Mesg to DataSockManager to shutdown external app
            sendReqShutdownQMsg(threadInfo[THREAD_MAIN].threadMqFd,
                            threadInfo[THREAD_DATA_COMS].threadMqFd,
                            msgIn.data1.intVal, msgIn.data2.intVal);

            // Put the WDog Kick back in the hands of the kernel
            logMessageWarn(SUBSYSTEM_ID_MAIN, "Putting Watchdog Kick back to OS control");
            setKLEDTimer(KLED_WDOG_1HZ_KICK_NAME, OS_WDOG_DELAY_ON, OS_WDOG_DELAY_OFF);

            // Inform the threads to close
            Closedown = LU_TRUE;

            // Issue the shutdown command
            logMessageFatal(SUBSYSTEM_ID_MAIN, "Issuing [%s]", CmdShutdownRTU);
            system(CmdShutdownRTU);
        break;

        case QMSG_CMD_NOTIFY_EXIT:
        case QMSG_CMD_REQ_STOPAPP:
        case QMSG_CMD_REQ_STARTAPP:
            // Forward the request to the AppManager
            if (threadInfo[THREAD_APPMAN].threadMqFd >= 0)
            {
                msgIn.qDest = threadInfo[THREAD_APPMAN].threadMqFd;
                sendQMsg(&msgIn);
            }
            else
            {
                // Reply with a general error if the thread is not registered
                sendErrorQMsg(threadInfo[THREAD_MAIN].threadMqFd, msgIn.qSrc, command, QMSG_CMD_ERR_GENERAL);
            }
        break;

            // Don't care about the following messages
        case QMSG_CMD_REP_LEDSTATE:
        case QMSG_CMD_REP_SET_DI:
        break;

        case QMSG_CMD_HAT_MODE:
            if (HATMode != msgIn.data1.boolVal)
            {
                HATMode = msgIn.data1.boolVal;
                logMessageWarn(SUBSYSTEM_ID_MAIN, "HAT Mode %s", HATMode ? "Enabled" : "Disabled");
            }
            break;

        default:
            logMessageError(SUBSYSTEM_ID_MAIN, "MAIN: ERROR - Unknown command [0x%03X]\n", msgIn.cmd);
    }

}

static void launchAppManager(void)
{
    // Create a non-blocking pipe to monitor the Child process
    pipe(childPipeFd);
    fcntl(childPipeFd[0], F_SETFL, O_NONBLOCK);

    AppManPID = fork();
    if (AppManPID == 0)
    {
        // Close the read end of the pipe
        close(childPipeFd[0]);

        // CHILD
        // Run the application manager
        appManagerThread(NULL);

        close(childPipeFd[1]);

        _exit(0);
    }
    else if (AppManPID < 0)
    {
        exit(999);
    }

    // PARENT

    // Close the write end of the pipe
    close(childPipeFd[1]);

    logMessageInfo(SUBSYSTEM_ID_MAIN, "AppManager process is [%d]", AppManPID);
}

static lu_int32_t checkAppManager(void)
{
    lu_int32_t ret;
    fd_set readfds;
    lu_int16_t maxfd;
    struct timeval tv = { 0, 5000 };

    maxfd = childPipeFd[0] + 1;
    FD_ZERO(&readfds);
    FD_SET(childPipeFd[0], &readfds);

    ret = select(maxfd, &readfds, NULL, NULL, &tv);

    return ret;
}


/*
 *********************** End of file ******************************************
 */
