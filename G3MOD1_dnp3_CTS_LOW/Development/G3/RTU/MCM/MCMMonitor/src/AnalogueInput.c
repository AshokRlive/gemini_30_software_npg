/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 MCMMonitor
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Defines an Analogue Input
 *
 *    CURRENT REVISION
 *
 *               $Revision::               $: (Revision of last commit)
 *               $Author::                 $: (Author of last commit)
 *       \date   $Date::                   $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25/07/13      andrews_s   Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <errno.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "AnalogueInput.h"
#include "LogMessage.h"
#include "timeOperations.h"

// To access one of the update functions
#include "CANUtils.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Scales the Analogue Input
 *
 *   It takes the value stored in rawValue and translates this to value
 *
 *   \param analogueInput pointer to analogue input to initialise
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
static IO_ERROR scaleAnalogueInput(AnalogueInputType* analogueInputPtr);
static IO_ERROR analogueInputGPIOUpdate(AnalogueInputType* analogueInputPtr);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

IO_ERROR analogueInputInit(lu_uint8_t address, AnalogueInputType* analogueInputPtr, MCMMonAIConfigStr *configPtr)
{
  if (analogueInputPtr == NULL)
  {
      return IO_ERROR_NULL;
  }

  // Initialise the analogue input attributes
  analogueInputPtr->channel                       = address;
  analogueInputPtr->fd                            = NULL;

  analogueInputPtr->config                        = configPtr;


  analogueInputPtr->rawValue                      = (lu_float32_t) -1;
  analogueInputPtr->value                         = (lu_float32_t) -1;

  analogueInputPtr->flags                         = IO_FLAG_OFF_LINE;
  analogueInputPtr->retries                       = 0;

  analogueInputPtr->nextScanTime                  = TIMESPEC_ZERO;  // Default to scan immediately
  analogueInputPtr->currentScanIntervalMs         = configPtr->scanIntervalMs;

  // Only if the AI has a sensible deviceName do we initialise it
  if (strlen((const char *)analogueInputPtr->config->deviceName) > 0)
  {
      // Initialise the AI by opening its value file
      if (initAI(analogueInputPtr->config->deviceName, analogueInputPtr->config->deviceId, &(analogueInputPtr->fd)) != 0)
      {
          return IO_ERROR_READ;
      }
  }

  // Get a fresh reading so all inputs are valid at startup
  return analogueInputUpdate(analogueInputPtr);
}


IO_ERROR analogueInputClose(AnalogueInputType *analogueInputPtr)
{
    if (analogueInputPtr == NULL)
    {
        return IO_ERROR_NULL;
    }

    if (fclose(analogueInputPtr->fd) == 0)
    {
        analogueInputPtr->fd = NULL;
        return IO_ERROR_NONE;
    }

    return IO_ERROR_CONFIG;
}


IO_ERROR analogueInputUpdate(AnalogueInputType* analogueInputPtr)
{
  if ((analogueInputPtr == NULL) || (analogueInputPtr->config == NULL))
  {
      return IO_ERROR_NULL;
  }

  // Clear INVALID flag once we've updated the value/flags
  analogueInputPtr->flags &= ~IO_FLAG_INVALID;

  // If this AI point has a special update function (it's probably a virtual AI) call it
  if (analogueInputPtr->config->update_func != NULL)
  {
      analogueInputPtr->config->update_func(analogueInputPtr);

      // TODO - SKA - Determine if the Analogue's alrm_func() should be called here
      return IO_ERROR_NONE;
  }

  // Default to the standard GPIO Update function if none is specified
  return analogueInputGPIOUpdate(analogueInputPtr);

}

IO_ERROR analogueInputAlarmOutLimits(AnalogueInputType *AIPointPtr)
{
//    printf("analogueInputAlarmOutLimits\n");

    if ((AIPointPtr->value > AIPointPtr->config->scaledAlarmLevelHigh) ||
        (AIPointPtr->value < AIPointPtr->config->scaledAlarmLevelLow))
    {
        // Set the ALARM flag
        AIPointPtr->flags |= IO_FLAG_ALARM;
    }
    else
    {
        // Clear the ALARM flag
        AIPointPtr->flags &= ~IO_FLAG_ALARM;
    }

    return IO_ERROR_NONE;
}

IO_ERROR analogueInputAlarmInLimits(AnalogueInputType *AIPointPtr)
{
//    printf("analogueInputAlarmInLimits\n");

    if ((AIPointPtr->value < AIPointPtr->config->scaledAlarmLevelHigh) &&
        (AIPointPtr->value > AIPointPtr->config->scaledAlarmLevelLow))
    {
        // Set the ALARM flag
        AIPointPtr->flags |= IO_FLAG_ALARM;
    }
    else
    {
        // Clear the ALARM flag
        AIPointPtr->flags &= ~IO_FLAG_ALARM;
    }

    return IO_ERROR_NONE;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Scales an AI based on rawValue
 *
 *   \param analogueInputPtr    Analogue Input Pointer
 *
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
static IO_ERROR scaleAnalogueInput(AnalogueInputType* analogueInputPtr)
{
  lu_float32_t floatVal;

  if (analogueInputPtr == NULL)
  {
      return IO_ERROR_NULL;
  }

  // Prevent divide by zero
  if ((analogueInputPtr->config->rawZero == analogueInputPtr->config->rawFull) ||
      (analogueInputPtr->config->scaledZero == analogueInputPtr->config->scaledFull))
  {
      // If the scalings are not set we will simply store rawVal in value
      analogueInputPtr->value = (lu_float32_t) analogueInputPtr->rawValue;

      return IO_ERROR_CONFIG;
  }

  // Limit rawValue to its boundaries
  if (analogueInputPtr->rawValue < analogueInputPtr->config->rawZero)
  {
      analogueInputPtr->rawValue = analogueInputPtr->config->rawZero;
  }

  if (analogueInputPtr->rawValue > analogueInputPtr->config->rawFull)
  {
      analogueInputPtr->rawValue = analogueInputPtr->config->rawFull;
  }

  // Calculate the scaled value
  floatVal = (lu_float32_t) ((lu_float32_t)(analogueInputPtr->rawValue - analogueInputPtr->config->rawZero) / (lu_float32_t)(analogueInputPtr->config->rawFull - analogueInputPtr->config->rawZero));
  floatVal *= (analogueInputPtr->config->scaledFull - analogueInputPtr->config->scaledZero);
  floatVal += analogueInputPtr->config->scaledZero;

  analogueInputPtr->value    = floatVal;

  return IO_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Updates an Analogue Input point from sys fs
 *
 *   \param analogueInputPtr    Analogue Input Pointer
 *
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
static IO_ERROR analogueInputGPIOUpdate(AnalogueInputType* analogueInputPtr)
{
  lu_int32_t tempVal = -99999;  // Easily recognisable number
  lu_int32_t ret;


  if ((analogueInputPtr == NULL) || (analogueInputPtr->config == NULL))
  {
      return IO_ERROR_NULL;
  }

  if (analogueInputPtr == NULL)
  {
      return IO_ERROR_NULL;
  }

  // Update the AI in the usual manner
  if ((ret = readAIInt(analogueInputPtr->fd, &tempVal)) <= 0)
  {
      if (analogueInputPtr->retries < MAX_AI_READ_RETRIES)
      {
          // We failed to read the Analogue Input (this occasionally happens
          // with the lm73 temperature sensor for some reason)  Close the fd
          // and reinitialise the analogue object up to MAX_AI_READ_RETRIES times.
          // After which we will mark this channel as OFFLINE
          if (analogueInputPtr->fd != NULL)
          {
              fclose(analogueInputPtr->fd);
              analogueInputPtr->fd = NULL;
          }

          analogueInputPtr->retries++;
          logMessageInfo(SUBSYSTEM_ID_IO_MANAGER, "Unable to read AI chan:[%d]:[%s] ret:[%d] try[%d]", analogueInputPtr->channel, analogueInputPtr->config->deviceName, ret, analogueInputPtr->retries);

          // Try to reopen the AI file
          if ((ret = initAI(analogueInputPtr->config->deviceName, analogueInputPtr->config->deviceId, &(analogueInputPtr->fd))) != 0)
          {
              logMessageInfo(SUBSYSTEM_ID_IO_MANAGER, "Unable to initAI [%s] ret:[%d]", analogueInputPtr->config->deviceName, ret);
              return IO_ERROR_READ;
          }

          // Return with no error during retry period
          return IO_ERROR_NONE;
      }

      // Only log the message if the value was previously ONLINE
      if ((analogueInputPtr->flags & IO_FLAG_OFF_LINE) == 0)
      {
          logMessageWarn(SUBSYSTEM_ID_IO_MANAGER, "Unable to read AI [%s] marking as OFFLINE", analogueInputPtr->config->deviceName );
      }

      // Unable to read the AI value file, set the OFFLINE flag
      analogueInputPtr->flags |= IO_FLAG_OFF_LINE;

      // Never stop trying
      analogueInputPtr->retries = 0;

      return IO_ERROR_READ;
  }

  // If we were offline, we are now back online
  if (analogueInputPtr->flags & IO_FLAG_OFF_LINE)
  {
      // Clear the Off-line Flag
      analogueInputPtr->flags &= ~IO_FLAG_OFF_LINE;

      logMessageInfo(SUBSYSTEM_ID_IO_MANAGER, "AI [%s] ONLINE!", analogueInputPtr->config->deviceName);
  }

  // Value is unfiltered at this point
  analogueInputPtr->retries = 0;
  analogueInputPtr->rawValue = tempVal;

  // Scale the value
  if ((ret = scaleAnalogueInput(analogueInputPtr)) != IO_ERROR_NONE)
  {
      return ret;
  }

  // If the AI has an alarm function, run it
  if (analogueInputPtr->config->alarm_func != NULL)
  {
      analogueInputPtr->config->alarm_func(analogueInputPtr);
  }

// TODO - SKA - Decide if this is required?!
  // Check to see if the value is out of range
  if ((analogueInputPtr->value > analogueInputPtr->config->scaledFull) ||
      (analogueInputPtr->value < analogueInputPtr->config->scaledZero))
  {
      // Set the OUT_OF_RANGE FLAG
      analogueInputPtr->flags |= IO_FLAG_OUT_OF_RANGE;
  }
  else
  {
      // Clear the OUT_OF_RANGE FLAG
      analogueInputPtr->flags &= ~IO_FLAG_OUT_OF_RANGE;
  }

#if DEBUG
  printf("%s : Val:[%f] ", analogueInputPtr->config->deviceName, analogueInputPtr->value);
  printf("Chan:[%d] RawVal:[%llu] Val:[%f]\n", analogueInputPtr->channel, analogueInputPtr->rawValue, analogueInputPtr->value);
#endif


  return IO_ERROR_NONE;
}


/*
 *********************** End of file ******************************************
 */
