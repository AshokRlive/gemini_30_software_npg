/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:lucycrypto.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   10 Mar 2016     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef LUCYCRYPTO_H_
#define LUCYCRYPTO_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "tmwscl/utils/tmwdefs.h"
#include "tmwscl/utils/tmwtypes.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define LUCYCRYPTO_MAX_USERS  16
#define LUCYCRYPTO_MAX_KEY_LEN 32

typedef struct {
  TMWTYPES_USHORT userNumber;
  TMWTYPES_USHORT userRole;
  TMWTYPES_USHORT keyLen;
  TMWTYPES_USHORT asymPubKeyLen;
  TMWTYPES_USHORT asymPrvKeyLen;
  TMWTYPES_USHORT oldRetainedKeyLen;
  TMWTYPES_UCHAR  key[LUCYCRYPTO_MAX_KEY_LEN]; // Update key.
  TMWTYPES_UCHAR  oldRetainedKey[LUCYCRYPTO_MAX_KEY_LEN];
  TMWTYPES_UCHAR  asymPubKey[LUCYCRYPTO_MAX_KEY_LEN];
  TMWTYPES_UCHAR  asymPrvKey[LUCYCRYPTO_MAX_KEY_LEN];
} LUCYCRYPTO_USER;


typedef struct {
    LUCYCRYPTO_USER users[LUCYCRYPTO_MAX_USERS];

    TMWTYPES_UCHAR  OSAsymPubKey[LUCYCRYPTO_MAX_KEY_LEN];
    TMWTYPES_USHORT OSAsymPubKeyLen;

    TMWTYPES_UCHAR  OSAsymPrvKey[LUCYCRYPTO_MAX_KEY_LEN];
    TMWTYPES_USHORT OSAsymPrvKeyLen;

    TMWTYPES_UCHAR  AuthAsymPubKey[LUCYCRYPTO_MAX_KEY_LEN];
    TMWTYPES_USHORT AuthAsymPubKeyLen;

    TMWTYPES_UCHAR  AuthAsymPrvKey[LUCYCRYPTO_MAX_KEY_LEN];
    TMWTYPES_USHORT AuthAsymPrvKeyLen;

//    TMWSDNPAuthErrorListStr     authErrors;
//    TMWSDNPAuthSecStatsListStr  authSecStats;

    TMWTYPES_USHORT authNextUserNumber;
    TMWDEFS_CLASS_MASK authClassMask;
} LUCYCRYPTO_USER_DB;

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */
/**
 * Creates a user database.
 */
void* TMWDEFS_GLOBAL lucycrypto_init();

void TMWDEFS_GLOBAL lucycrypto_commitKey(
  void                *pCryptoHandle,
  TMWCRYPTO_KEYTYPE    keyType,
  void *               keyHandle,
  TMWTYPES_BOOL        commit);

TMWTYPES_BOOL TMWDEFS_GLOBAL lucycrypto_getKey(
  void                *pCryptoHandle,
  TMWCRYPTO_KEYTYPE    keyType,
  void *               keyHandle,
  TMWCRYPTO_KEY       *pKey);

TMWTYPES_BOOL TMWDEFS_GLOBAL lucycrypto_setKeyData(
  void                *pCryptoHandle,
  TMWCRYPTO_KEYTYPE    keyType,
  void *               keyHandle,
  TMWTYPES_UCHAR      *pKeyData,
  TMWTYPES_USHORT      keyLength);

#endif /* LUCYCRYPTO_H_ */

/*
 *********************** End of file ******************************************
 */
