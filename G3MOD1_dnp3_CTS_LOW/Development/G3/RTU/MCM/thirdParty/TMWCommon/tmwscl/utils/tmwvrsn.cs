using System;

namespace TMW.tmwvrsnVersion
{
  /// <summary> Version info </summary>
  public class tmwvrsnVersionInfo
  {
    /// <summary> Version string </summary>
    public const String managedVersion = "3.19.0000.0000";
    /// <summary> Version string </summary>
    public const String version = "3.19.0000.0";
    /// <summary> Build Date </summary>
    public const String buildDate = "Mon Jan 11 11:00:25 2016";
    /// <summary> Build Number </summary>
    public const String buildNumber = "523";
    /// <summary> Build Year </summary>
    public const int buildYear = 2016;
    /// <summary> Build Month </summary>
    public const int buildMonth = 1;
    /// <summary> Build Day </summary>
    public const int buildDay = 11;
  };
}
