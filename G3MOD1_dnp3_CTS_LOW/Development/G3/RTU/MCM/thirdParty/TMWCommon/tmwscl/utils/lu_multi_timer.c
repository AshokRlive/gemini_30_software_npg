/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:lu_multi_timer.c
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   8 Jul 2014     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include "tmwdefs.h"
#include "tmwtypes.h"
#include "lu_multi_timer.h"

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
/* function: tmwtest_startMultiTimer */
void * TMWDEFS_GLOBAL tmwtest_startMultiTimer(void *pHandle,
                TMWTYPES_MILLISECONDS msTimeout, TMWTYPES_CALLBACK_FUNC callBackPtr,
                void *callBackParamPtr)
{
    MY_TIMER_TYPE *timePTR = (MY_TIMER_TYPE *)pHandle;

    timePTR->callBackPtr = callBackPtr;
    timePTR->callBackParamPtr = callBackParamPtr;
    timePTR->timeout = tmwtarg_getMSTime() + msTimeout;
    timePTR->isActive = TMWDEFS_TRUE;

    return(timePTR);
}


void TMWDEFS_GLOBAL tmwtest_cancelMultiTimer(void *pHandle)
{
    MY_TIMER_TYPE *timePTR = (MY_TIMER_TYPE *)pHandle;
    timePTR->isActive = TMWDEFS_FALSE;
}

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
