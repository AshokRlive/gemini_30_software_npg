/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:lucycrypto.c
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   10 Mar 2016     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include "tmwscl/utils/tmwtarg.h"
#include "tmwscl/utils/tmwcnfg.h"
#include "tmwscl/utils/tmwcrypto.h"
#include "tmwscl/utils/lucycrypto.h"

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


#if TMWCNFG_SUPPORT_CRYPTO


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/* This is the default Auth Cert key the test harness uses to simulate the Authority */
TMWTYPES_UCHAR  AuthCertKey[] = {
  0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
  0x09, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06,
  0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
  0x09, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06
};
TMWTYPES_USHORT AuthCertKeyLen = 32;


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
void dbg(char* message);
void* TMWDEFS_GLOBAL lucycrypto_init();

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */





/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
void* TMWDEFS_GLOBAL lucycrypto_init()
{
    LUCYCRYPTO_USER_DB* pDb = malloc(sizeof(LUCYCRYPTO_USER_DB));

  {
    int i;
    for(i=0; i<LUCYCRYPTO_MAX_USERS; i++)
    {
      pDb->users[i].userNumber = 0;
      pDb->users[i].userRole = 0;
      pDb->users[i].keyLen = 0;
      pDb->users[i].oldRetainedKeyLen = 0;
      pDb->users[i].asymPubKeyLen = 0;
      pDb->users[i].asymPrvKeyLen = 0;
    }
  }
  pDb->OSAsymPubKeyLen   = 0;
  pDb->OSAsymPrvKeyLen   = 0;
  pDb->AuthAsymPubKeyLen = 0;
  pDb->AuthAsymPrvKeyLen = 0;

  // Create example key for testing.
  pDb->OSAsymPubKeyLen = (TMWTYPES_USHORT)strlen("TMWTestOSAsymPubKey.pem");
  memcpy(pDb->OSAsymPubKey, "TMWTestOSAsymPubKey.pem", pDb->OSAsymPubKeyLen);

  pDb->OSAsymPrvKeyLen = (TMWTYPES_USHORT)strlen("TMWTestOSAsymPrvKey.pem");
  memcpy(pDb->OSAsymPrvKey, "TMWTestOSAsymPrvKey.pem", pDb->OSAsymPrvKeyLen);

  pDb->AuthAsymPubKeyLen = (TMWTYPES_USHORT)strlen("TMWTestRsa2048PubKey.pem");
  memcpy(pDb->AuthAsymPubKey, "TMWTestRsa2048PubKey.pem", pDb->AuthAsymPubKeyLen);

  pDb->AuthAsymPrvKeyLen = (TMWTYPES_USHORT)strlen("TMWTestRsa2048PrvKey.pem");
  memcpy(pDb->AuthAsymPrvKey, "TMWTestRsa2048PrvKey.pem", pDb->AuthAsymPrvKeyLen);

  return (void*)pDb;
}

TMWTYPES_BOOL TMWDEFS_GLOBAL lucycrypto_getKey(
  void                *pCryptoHandle,
  TMWCRYPTO_KEYTYPE    keyType,
  void *               keyHandle,
  TMWCRYPTO_KEY       *pKey)
{
  int             i;
  TMWTYPES_UCHAR *pUpdateKey;
  TMWTYPES_USHORT keyLen;
  LUCYCRYPTO_USER_DB *pDB;

  pDB = (LUCYCRYPTO_USER_DB *)pCryptoHandle;
  pUpdateKey = TMWDEFS_NULL;
  keyLen = 0;

  pKey->ivLength = 0;
  pKey->keyType = keyType;

  switch(keyType)
  {
  case TMWCRYPTO_USER_UPDATE_KEY:
  case TMWCRYPTO_USER_ASYM_PUB_KEY:
  case TMWCRYPTO_USER_ASYM_PRV_KEY:
    /* see if we have a key that matches that handle */
    for(i=0; i< LUCYCRYPTO_MAX_USERS; i++)
    {
      if(pDB->users[i].userNumber == keyHandle)
      {
        if(keyType == TMWCRYPTO_USER_UPDATE_KEY)
        {
          pUpdateKey = (TMWTYPES_UCHAR*)&(pDB->users[i].key);
          keyLen = pDB->users[i].keyLen;
        }
        else if(keyType == TMWCRYPTO_USER_ASYM_PUB_KEY)
        {
          pUpdateKey = (TMWTYPES_UCHAR*)&(pDB->users[i].asymPubKey);
          keyLen = pDB->users[i].asymPubKeyLen;
        }
        else if(keyType == TMWCRYPTO_USER_ASYM_PRV_KEY)
        {
          pUpdateKey = (TMWTYPES_UCHAR*)&(pDB->users[i].asymPrvKey);
          keyLen = pDB->users[i].asymPrvKeyLen;
        }
        break;
      }
    }

    if(pUpdateKey == TMWDEFS_NULL)
      return(TMWDEFS_FALSE);

    break;

  case TMWCRYPTO_OS_ASYM_PUB_KEY:
    pUpdateKey = (TMWTYPES_UCHAR*)pDB->OSAsymPubKey;
    keyLen = pDB->OSAsymPubKeyLen;
    break;

  case TMWCRYPTO_OS_ASYM_PRV_KEY:
    pUpdateKey = (TMWTYPES_UCHAR*)pDB->OSAsymPrvKey;
    keyLen = pDB->OSAsymPrvKeyLen;
    break;

  case TMWCRYPTO_AUTH_CERT_SYM_KEY:
    pUpdateKey = (TMWTYPES_UCHAR*)AuthCertKey;
    keyLen = AuthCertKeyLen;
    break;

  case TMWCRYPTO_AUTH_ASYM_PRV_KEY:
    pUpdateKey = (TMWTYPES_UCHAR*)pDB->OSAsymPrvKey;
    keyLen = pDB->OSAsymPrvKeyLen;
    break;

  case TMWCRYPTO_AUTH_ASYM_PUB_KEY:
    pUpdateKey = (TMWTYPES_UCHAR*)pDB->OSAsymPubKey;
    keyLen = pDB->OSAsymPubKeyLen;
    break;

  default:
    return TMWDEFS_FALSE;
  }

  // Clear key
  memset(pKey->value, 0, TMWCRYPTO_MAX_KEY_LENGTH);

  // Set key
  memcpy(pKey->value, pUpdateKey, keyLen);
  pKey->length = keyLen;

  return(TMWDEFS_TRUE);
}



TMWTYPES_BOOL TMWDEFS_GLOBAL lucycrypto_setKeyData(
  void                *pCryptoHandle,
  TMWCRYPTO_KEYTYPE    keyType,
  void *               keyHandle,
  TMWTYPES_UCHAR      *pKeyData,
  TMWTYPES_USHORT      keyLength)
{
  {
  TMWTYPES_UCHAR *pSimKey = TMWDEFS_NULL;
  TMWTYPES_USHORT *pKeyLen = TMWDEFS_NULL;
  LUCYCRYPTO_USER_DB *pDB;

  pDB = (LUCYCRYPTO_USER_DB *)pCryptoHandle;

  switch(keyType)
  {
  case TMWCRYPTO_USER_UPDATE_KEY:
  case TMWCRYPTO_USER_ASYM_PUB_KEY:
  case TMWCRYPTO_USER_ASYM_PRV_KEY:
    {
    int i;
    int index = -1;

    /* find an entry that matches or find an unused entry */
    for(i=0; i< LUCYCRYPTO_MAX_USERS; i++)
    {
      if(pDB->users[i].userNumber == keyHandle)
      {
        index = i;
        break;
      }
      else if((index == -1) && (pDB->users[i].userNumber == 0))
      {
        index = i;
      }
    }

    if(index != -1)
    {
      if(keyType == TMWCRYPTO_USER_UPDATE_KEY)
      {
        pSimKey = (TMWTYPES_UCHAR*)&(pDB->users[index].key);
        pKeyLen = &(pDB->users[index].keyLen);

        /* retain old user update key in case commitKey says to revert back */
        if(pDB->users[index].keyLen>0)
        {
          memcpy(pDB->users[index].oldRetainedKey, pDB->users[index].key, pDB->users[index].keyLen);
          pDB->users[index].oldRetainedKeyLen = pDB->users[index].keyLen;
        }
      }
      else if(keyType == TMWCRYPTO_USER_ASYM_PUB_KEY)
      {
        pSimKey = (TMWTYPES_UCHAR*)&pDB->users[index].asymPubKey;
        pKeyLen = &pDB->users[index].asymPubKeyLen;
      }
      else if(keyType == TMWCRYPTO_USER_ASYM_PRV_KEY)
      {
        pSimKey = (TMWTYPES_UCHAR*)&pDB->users[index].asymPrvKey;
        pKeyLen = &pDB->users[index].asymPrvKeyLen;
      }
    }

    if(pSimKey == TMWDEFS_NULL)
      return(TMWDEFS_FALSE);

    pDB->users[index].userNumber = keyHandle;
    }
    break;

  case TMWCRYPTO_OS_ASYM_PUB_KEY:
    pSimKey = (TMWTYPES_UCHAR*)pDB->OSAsymPubKey;
    pKeyLen = &pDB->OSAsymPubKeyLen;
    break;

  case TMWCRYPTO_OS_ASYM_PRV_KEY:
    pSimKey = (TMWTYPES_UCHAR*)pDB->OSAsymPrvKey;
    pKeyLen = &pDB->OSAsymPrvKeyLen;
    break;

  case TMWCRYPTO_AUTH_CERT_SYM_KEY:
    pSimKey = (TMWTYPES_UCHAR*)AuthCertKey;
    pKeyLen = AuthCertKeyLen;
    break;

  case TMWCRYPTO_AUTH_ASYM_PRV_KEY:
    pSimKey = (TMWTYPES_UCHAR*)pDB->AuthAsymPrvKey;
    pKeyLen = &pDB->AuthAsymPrvKeyLen;
    break;

  case TMWCRYPTO_AUTH_ASYM_PUB_KEY:
    pSimKey = (TMWTYPES_UCHAR*)pDB->AuthAsymPubKey;
    pKeyLen = &pDB->AuthAsymPubKeyLen;
    break;

  default:
    return TMWDEFS_FALSE;
  }

  memcpy(pSimKey, pKeyData, keyLength);
  *pKeyLen = keyLength;

  return TMWDEFS_TRUE;
  }
}

void TMWDEFS_GLOBAL lucycrypto_commitKey(
  void                *pCryptoHandle,
  TMWCRYPTO_KEYTYPE    keyType,
  void *               keyHandle,
  TMWTYPES_BOOL        commit)
{
    LUCYCRYPTO_USER_DB *pDB;

    pDB = (LUCYCRYPTO_USER_DB *)pCryptoHandle;

  /* Only User Update Key requires this commit==false functionality */
  if((keyType == TMWCRYPTO_USER_UPDATE_KEY)
    && (commit == TMWDEFS_FALSE))
  {
    /* find the entry that matches */
    int index;
    for(index=0; index< LUCYCRYPTO_MAX_USERS; index++)
    {
      if(pDB->users[index].userNumber == keyHandle)
      {
        /* revert back to old retained user update key */
        if(pDB->users[index].oldRetainedKeyLen>0)
        {
          memcpy(pDB->users[index].key, pDB->users[index].oldRetainedKey, pDB->users[index].oldRetainedKeyLen);
          pDB->users[index].keyLen = pDB->users[index].oldRetainedKeyLen;
        }
      }
    }
  }

}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */

#endif /* TMWCNFG_SUPPORT_CRYPTO */
/*
 *********************** End of file ******************************************
 */
