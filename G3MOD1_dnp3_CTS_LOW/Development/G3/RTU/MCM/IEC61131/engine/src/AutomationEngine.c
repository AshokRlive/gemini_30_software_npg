/*! \filead
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:AutomationEngine.c
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   27 Apr 2016     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>
#include <unistd.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "AutomationEngine.h"
#include "LoggerInterface.h"
#include "DynamicLibraryUtil.h"
#include "dbgserver/Debugger.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define MIN_DELAY_MS 100
/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static void s_cb_log_err(void* context, lu_char_t* errMsg);
static void s_cb_log_info(void* context, lu_char_t* infoMsg);
static void s_cb_delay(void* context, lu_uint32_t ms);
static void s_cb_checkpoint(void* context);
static void s_cb_set_line_num(void* context, lu_uint32_t lineNumber);
static void s_cb_set_cr(void* context, lu_int32_t crvalue);


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */





/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

lu_bool_t automationengine_init(AutomationContext* ctxt, const lu_char_t* schemeFilePath)
{
    void *handle = NULL;

    /* Load the shared library from file*/
    if (dl_load_shared_lib(&handle, schemeFilePath) == LU_FALSE)
    {
        logger_err("Failed to load shared library file: \"%s\"!", schemeFilePath);
        return LU_FALSE;
    }

    /* Checks API*/
    p_autoscheme_get_api getApi= (p_autoscheme_get_api)dl_load_function(handle, FUNC_NAME_GET_API);
    AutoSchemeAPI api;
    api.major = 0;
    api.minor = 0;
    if(getApi != NULL)
    {
        getApi(&api);
    }
    if(api.major != AUTO_API_MAJOR)
    {
        logger_err("Auto scheme API mismatched! Expected: %d.x actual:%d.%d",
                        AUTO_API_MAJOR, api.major, api.minor);

        dl_close_shared_lib(handle);
        return LU_FALSE;
    }
    else
    {
        logger_info("Loaded shared library file: \"%s\"[API:%d.%d].",
                        schemeFilePath,
                        api.major,
                        api.minor);
    }


    /* Load required functions and store them in context*/
    p_autoscheme_register_callback  registerCallback = NULL;
    p_autoscheme_check_ready        checkReady = NULL;
    p_autoscheme_run         		run = NULL;
    registerCallback = (p_autoscheme_register_callback)dl_load_function(handle, FUNC_NAME_REG_CALLBACK);
    checkReady = (p_autoscheme_check_ready)dl_load_function(handle, FUNC_NAME_CHECK_SCHEME_READY);
    run = (p_autoscheme_run)dl_load_function(handle, FUNC_NAME_RUN_AUTO_SCHEME);

    if(registerCallback != NULL && run != NULL && checkReady != NULL)
    {
        /* Create context*/
        ctxt->registerCallback = registerCallback;
        ctxt->checkReady= checkReady;
        ctxt->run = run;
        ctxt->libHandler = handle;

        /* Register callbacks*/
        ctxt->registerCallback(AS_INTERN_CALLBACK_ID_LOG_ERROR,     (void*) s_cb_log_err);
        ctxt->registerCallback(AS_INTERN_CALLBACK_ID_LOG_INFO,      (void*) s_cb_log_info);
        ctxt->registerCallback(AS_INTERN_CALLBACK_ID_DELAY,         (void*) s_cb_delay);
        ctxt->registerCallback(AS_INTERN_CALLBACK_ID_CHECKPOINT,    (void*) s_cb_checkpoint);
        ctxt->registerCallback(AS_INTERN_CALLBACK_ID_SET_LINE_NUM,  (void*) s_cb_set_line_num);
        ctxt->registerCallback(AS_INTERN_CALLBACK_ID_SET_CR,        (void*) s_cb_set_cr);

        return LU_TRUE;
    }
    else
    {
        logger_err("Failed to load required functions from the library!");
        return LU_FALSE;
    }

}

void automationengine_de_init(AutomationContext* ctxt)
{
    if(ctxt != NULL)
    {
        /*Notify debugger that the scheme stops running*/
        debugger_set_scheme_running(ctxt->dbgctxt, LU_FALSE);

        dl_close_shared_lib(ctxt->libHandler);
        ctxt->registerCallback = NULL;
        ctxt->run = NULL;
        ctxt->checkReady = NULL;
        ctxt->libHandler = (void*)(-1);
        logger_info("Closed shared library.");
    }
}

lu_int32_t automationengine_run(AutomationContext* ctxt)
{
	if(ctxt == NULL)
	{
        logger_err("Automation scheme cannot run cause the context is NULL");
        return -1;
	}

    if(ctxt->run == NULL)
    {
        logger_err("Automation scheme cannot run cause function not found:%s",
                        FUNC_NAME_RUN_AUTO_SCHEME);
        return -1;
    }

    AS_ERROR err;

    err = (*ctxt->checkReady)();
    if(err != AS_ERROR_NONE)
    {
        logger_err("Automation scheme not ready for run.AS_ERROR:%d",err);
        return -1;
    }

    /*Notify debugger that the scheme starts running*/
    debugger_set_scheme_running(ctxt->dbgctxt, LU_TRUE);

    /* Run automation scheme*/
    logger_info("Start running automation scheme...");
    err = (*ctxt->run)((void*)ctxt);
    if(err != AS_ERROR_NONE)
    {
        logger_err("Automation scheme exit with AS_ERROR:%d",err);
        return -1;
    }

    return 0;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*Implementation of optional callbacks*/

static void s_cb_log_err(void* context, lu_char_t* errMsg)
{
	LU_UNUSED(context);
	logger_err("SCHEME >> %s", errMsg);
}

static void s_cb_log_info(void* context, lu_char_t* infoMsg)
{
	LU_UNUSED(context);
	logger_info("SCHEME >> %s", infoMsg);
}

static void s_cb_delay(void* context, lu_uint32_t ms)
{
	LU_UNUSED(context);

	/* Set to min delay to avoid overload CPU*/
	if(ms < MIN_DELAY_MS)
	    ms = MIN_DELAY_MS;

	usleep(ms * 1000);
}

static void s_cb_checkpoint(void* context)
{
	debugger_checkpoint(((AutomationContext*)context)->dbgctxt);
}

/**
 * Set the current line number.
 * args
 *  number - line number, start from 0.
 */
static void s_cb_set_line_num(void* context, lu_uint32_t lineNumber)
{
    debugger_set_line_num(((AutomationContext*)context)->dbgctxt, lineNumber);
}

static void s_cb_set_cr(void* context, lu_int32_t crvalue)
{
    debugger_set_cr(((AutomationContext*)context)->dbgctxt, crvalue);
}

/*
 *********************** End of file ******************************************
 */
