/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: DebugSocket.c
 *    FILE TYPE: c source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   8 Oct 2015     pwang     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <errno.h>
#include <stddef.h>
#include <string.h>
#include <netdb.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <arpa/inet.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "dbgserver/DebuggerSocket.h"
#include "LoggerInterface.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define SOCK_RW_BUF_SIZE 1024
#define BACKLOG MAX_CONNECTION_NUM  // how many pending connections queue will hold

#define DEBUG_SOCKET_LOG_ENABLED    0

#if DEBUG_SOCKET_LOG_ENABLED
#include <stdarg.h>
#endif

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

typedef struct
{
    /* The socket file descriptor for our "listening" socket*/
    lu_int32_t listenSock;

    /* Array of connected sockets so we know who we are talking to*/
    lu_int32_t connectlist[MAX_CONNECTION_NUM];

    /* The number of existing connections.*/
    lu_int32_t connectNum;

    /* Socket file descriptors we want to wake up for, using select()*/
    fd_set socks;

    /* Highest file descriptor, needed for select()*/
    lu_int32_t highsock;

    /* For handling request from client.*/
    debugsocket_request_handler requestHandler;

    /* For handling connection changes.*/
    debugsocket_connchg_handler connHandler;

    /* Reference to user object passed from init()*/
    void*           userContext;

} SocketContext;



/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static void s_compose_error_reply(lu_uint8_t *replyPtr, lu_int32_t* replySize);
static void s_build_select_list(SocketContext* ctxt);
static void s_read_socks(SocketContext* ctxt);
static void s_handle_new_connection(SocketContext* ctxt);
static void s_deal_with_data(SocketContext* ctxt, const lu_int32_t listnum);
static lu_int32_t s_make_server_socket(lu_int32_t port);
static lu_int32_t s_close_socket(SocketContext* ctxt, lu_int32_t listnum);

static void s_logger_info(const lu_char_t* msg, ...);
static void s_logger_err(const lu_char_t* msg,  ...);
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

void debugsocket_process(void* socketContext)
{
    lu_int32_t result;
	lu_uint16_t listnum;

	SocketContext* ctxt = (SocketContext*)socketContext;

    s_build_select_list(ctxt);

    s_logger_info("\n\nWaiting for request....");

    result = select(ctxt->highsock+1, &(ctxt->socks), (fd_set *) 0, (fd_set *) 0, /*&timeout*/ NULL);

    if (result == 0)
    {
        /* Select timeout*/
        s_logger_info("select() timed out!");
    }
    else if( (result < 0) && (errno != EINTR) )
    {
        /* Select error*/
        s_logger_info("error in select(): %s", strerror(errno));

        /* Close related sockets*/
        for (listnum = 0; listnum < MAX_CONNECTION_NUM; listnum++)
        {
            if(ctxt->connectlist[listnum] >= 0)
            {
                if (FD_ISSET(ctxt->connectlist[listnum], &(ctxt->socks)))
                {
                    s_logger_info("closed socket:%d", ctxt->connectlist[listnum]);
                    s_close_socket(ctxt, listnum);
                }
            }
        }

    }
    else if (result > 0)
    {
        s_logger_info("selected socket. num:%d", result);
        s_read_socks(ctxt);
    }
}

void debugsocket_register_connchg_handler(void* socketContext,
                debugsocket_connchg_handler connchg_handler)
{
    SocketContext* ctxt = (SocketContext*)socketContext;
    ctxt->connHandler = connchg_handler;
}



void* debugsocket_init(lu_int32_t port, void* userContext,
                debugsocket_request_handler requestHandler)
{
    lu_uint32_t i;
    lu_int32_t listenSock;

    listenSock = s_make_server_socket(port);
    if(listenSock <=0 )
        return NULL;

    /* Since we start with only one socket, the listening socket,
           it is the highest socket so far. */
    SocketContext* ctxt = (SocketContext*)calloc(1, sizeof(SocketContext));
    ctxt->highsock = listenSock;
    ctxt->listenSock = listenSock;
    ctxt->requestHandler = requestHandler;
    ctxt->userContext  = userContext;
    ctxt->connHandler = NULL;

    /* Init all connections on the list to an invalid socket value*/
    for (i = 0; i < MAX_CONNECTION_NUM; ++i)
    {
        ctxt->connectlist[i] = -1;
    }
    ctxt->connectNum = 0;

    return ctxt;
}

void debugsocket_destroy(void* context)
{
    lu_int32_t i;
    if(context != NULL)
    {
        SocketContext* ctxt = (SocketContext*)context;
        for(i =0; i < MAX_CONNECTION_NUM; i++)
        {
            close(ctxt->connectlist[i]);
            ctxt->connectlist[i] = -1;
        }
        close(ctxt->listenSock);

        free(ctxt);
    }
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

static void s_set_nonblock(lu_int32_t sockfd)
{
    lu_int32_t flags;

    /* Set socket to non-blocking */

    if ((flags = fcntl(sockfd, F_GETFL, 0)) < 0)
    {
        s_logger_err("Failed to set nonblock");
    }


    if (fcntl(sockfd, F_SETFL, flags | O_NONBLOCK) < 0)
    {
        s_logger_err("Failed to set nonblock");
    }
}


static lu_int32_t s_make_server_socket(lu_int32_t port)
{
    struct addrinfo hints;
    struct addrinfo* servinfo;
    lu_int32_t rv;
    lu_int32_t listenSock = -1;
    lu_char_t portStr[20];
    lu_int32_t yes = 1;

    sprintf(portStr,"%i",port);// converts to port string.

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;// AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = AI_PASSIVE; // use my IP

    if ((rv = getaddrinfo(NULL, portStr, &hints, &servinfo)) != 0)
    {
        s_logger_err("getaddrinfo: %s", gai_strerror(rv));
    }
    else
    {
        // Create socket
        listenSock = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);
        if(listenSock < 0)
        {
            perror("failed to create socket");
        }
        else
        {
            // Set socket reusable
            if (setsockopt(listenSock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(lu_int32_t)) == -1)
            {
                close(listenSock);
                listenSock = -1;
                perror("failed to setsockopt");
            }
            else
            {
                // Set socket to non-blocking
                s_set_nonblock(listenSock);

                // Bind socket
                if(bind(listenSock, servinfo->ai_addr, servinfo->ai_addrlen) != 0)
                {
                    close(listenSock);
                    listenSock = -1;
                    perror("failed to bind");
                }
            }
        }
        freeaddrinfo(servinfo); // all done with this structure

        // listen to socket
        if(listenSock >= 0)
        {
            if(listen(listenSock, BACKLOG) != 0)
            {
                close(listenSock);
                listenSock = -1;
                perror("failed to listen");
            }
        }
    }

    return listenSock;
}


static lu_int32_t s_recv_data(lu_int32_t socket, lu_uint8_t* recvBuffer, lu_int32_t bufSize)
{
    lu_int32_t result = 0;
    s_logger_info("receiving data from sock:%d ",socket);

    do
    {
        /* Receive a reply from the server*/
        result = recv(socket, (char*)recvBuffer, bufSize, 0);
    }
    while( (result == -1) && (errno == EINTR) );


    return result;
}


static lu_int32_t s_send_data(lu_int32_t socket, lu_uint8_t* sendBuffer, lu_int32_t sendSize)
{

    lu_int32_t result;      //Partial result
    lu_int32_t sent = 0;    //Final result of the operation

    s_logger_info("sending data size:%d, content:%s", sendSize, sendBuffer);

    do
    {
        result = send(socket, sendBuffer + sent, sendSize - sent, MSG_NOSIGNAL);
        if (result > 0)
        {
            sent += result;
        }
        else if( (result < 0) && (errno != EINTR) )
        {
            break;// failure
        }
    }
    while (sendSize > sent);


    return sent;
}


static lu_int32_t s_close_socket(SocketContext* ctxt, lu_int32_t listnum)
{
    int err;

    if(ctxt->connectlist[listnum] >= 0)
    {
        err = close(ctxt->connectlist[listnum]);
        ctxt->connectlist[listnum] = -1;
        ctxt->connectNum --;
        if(ctxt->connHandler != NULL)
            ctxt->connHandler(ctxt->userContext, ctxt->connectNum);
    }

    return err;
}

static void s_compose_error_reply(lu_uint8_t *replyPtr, lu_int32_t* replySize)
{
    const lu_char_t* err = "internal error occurred";
    
    s_logger_info("compose_error_reply() called");
    strcpy((lu_char_t*)replyPtr, err);
    *replySize = strlen(err);
}

static void s_build_select_list(SocketContext* ctxt)
{
    lu_int32_t listnum;         /* Current item in connectlist for for loops */

    FD_ZERO(&(ctxt->socks));
    FD_SET(ctxt->listenSock, &(ctxt->socks));

    /* Loops through all the possible connections and adds
        those sockets to the fd_set */
    for (listnum = 0; listnum < MAX_CONNECTION_NUM; listnum++)
    {
        if (ctxt->connectlist[listnum] >= 0)
        {
            //Add socket to the connection list
            FD_SET(ctxt->connectlist[listnum], &(ctxt->socks));
            if (ctxt->connectlist[listnum] > ctxt->highsock)
            {
                ctxt->highsock = ctxt->connectlist[listnum];
            }
        }
    }
}


static void s_handle_new_connection(SocketContext* ctxt)
{
    lu_int32_t listnum;        /* Current item in connectlist for for loops */
    lu_int32_t connection;     /* Socket file descriptor for incoming connections */
    const lu_char_t* errTxt = "Sorry, Automation Debug Server is too busy. Try again later!\r\n";
    const lu_char_t* welcomeTxt = "You are connected to Automation Debug Server.\r\n";

    /* We have a new connection coming in!  We'll try to find a spot for it in connectlist. */
    connection = accept(ctxt->listenSock, NULL, NULL);
    if (connection < 0)
    {
        perror("failed to accept");
        return;
    }

    s_set_nonblock(connection);

    /* Add new connection to the queue. */
    for (listnum = 0; (listnum < MAX_CONNECTION_NUM) && (connection != -1); listnum++)
    {
        if (ctxt->connectlist[listnum] < 0)
        {
            s_send_data(connection, (lu_uint8_t*)welcomeTxt, strlen(welcomeTxt) );
            s_logger_info("new connection accepted:   FD=%d; Slot=%d", connection, listnum);
            ctxt->connectlist[listnum] = connection;
            connection = -1;    //Break loop
            ctxt->connectNum ++;

            if(ctxt->connHandler != NULL)
                ctxt->connHandler(ctxt->userContext, ctxt->connectNum);
        }
    }

    /* Failed to add new connections to queue. */
    if (connection > 0)
    {
        //A connection was accepted but not added to the queue
        s_logger_err("no room left for new connection");
        s_send_data(connection, (lu_uint8_t*)errTxt, strlen(errTxt) );
        close(connection);
    }
}


static void s_deal_with_data(SocketContext* ctxt, const lu_int32_t listnum)
{
    static lu_uint8_t readBuf [SOCK_RW_BUF_SIZE];    /* Buffer for socket reads */
    static lu_uint8_t writeBuf[SOCK_RW_BUF_SIZE];    /* Buffer for socket reads */
    lu_int32_t writeSize = 0;
    lu_int32_t readSize = 0;
    lu_int32_t sentSize = 0;

    struct sockaddr_in address;
    lu_int32_t addrlen;
    addrlen = sizeof(address);

    readSize = s_recv_data(ctxt->connectlist[listnum],readBuf,SOCK_RW_BUF_SIZE);

    //s_logger_info("deal_with_data() listnum:%d",listnum);

    if (readSize < 0)
    {
        /* Connection closed, close this end and free up entry in connectlist */
        s_logger_err("connection lost: FD=%d;  Slot=%d", ctxt->connectlist[listnum],listnum);
        s_close_socket(ctxt, listnum);

    }
    else if(readSize > 0)
    {
        s_logger_info(" received data size: %d. start processing request...",readSize);
        if ((*(ctxt->requestHandler))(ctxt->userContext, readBuf, readSize, writeBuf, &writeSize) != 0)
        {
            s_compose_error_reply(writeBuf, &writeSize);
        }

        s_logger_info("finished processing request, sending reply now.");
        sentSize = s_send_data(ctxt->connectlist[listnum], writeBuf, writeSize);
        s_logger_info("sent out data. sent size:%d expected:%d",sentSize, writeSize);
        //s_close_socket(ctxt, listnum);
    }
    else
    {
        getpeername(ctxt->connectlist[listnum], (struct sockaddr*)&address , (socklen_t*)&addrlen);
        s_logger_info("peer[%s:%d] is shutdown. closing connection index:%d.",
                        inet_ntoa(address.sin_addr) ,
                        ntohs(address.sin_port),
                        listnum);
        /* Peer disconnected , get his details and print*/
        s_close_socket(ctxt,listnum);
    }
}

static void s_read_socks(SocketContext* ctxt)
{
    lu_int32_t listnum;         /* Current item in connectlist for for loops */

    /* Deal with listenSock */
    if(ctxt->listenSock >= 0)
    {
        if (FD_ISSET(ctxt->listenSock, &(ctxt->socks)))
        {
            s_handle_new_connection(ctxt);
        }
    }
    /* Deal with connection sockets */
    for (listnum = 0; listnum < MAX_CONNECTION_NUM; listnum++)
    {
        if(ctxt->connectlist[listnum] >= 0)
        {
            if (FD_ISSET(ctxt->connectlist[listnum], &(ctxt->socks)))
            {
                s_deal_with_data(ctxt, listnum);
            }
        }
    }
}

static void s_logger_err(const lu_char_t* msg,  ...)
{
#if DEBUG_SOCKET_LOG_ENABLED
    char buffer[256];
    memset(buffer,0, 256);

    va_list args;
    va_start (args, msg);

    vsprintf(buffer, msg, args);
    logger_err("dbg socket:  >> %s", buffer);

    va_end (args);
#endif
}

static void s_logger_info(const lu_char_t* msg, ...)
{
#if DEBUG_SOCKET_LOG_ENABLED
    char buffer[256];
    memset(buffer,0, 256);

    va_list args;
    va_start (args, msg);

    vsprintf(buffer, msg, args);
    logger_info("dbg socket:  >> %s", buffer);

    va_end (args);
#endif
}
/*
 *********************** End of file ******************************************
 */
