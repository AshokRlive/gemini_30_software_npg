/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:DebugHandler.c
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   28 Apr 2016     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "LoggerInterface.h"
#include "dbgserver/IDebuggerProtocol.h"
#include "dbgserver/Debugger.h"
#include "dbgserver/DebuggerSocket.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
#define MAX_NUM_BREAKPOINT 100
#define MAX_NUM_STEP 5
#define BP_TYPE lu_uint32_t

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

typedef struct
{
    lu_uint32_t          schemeID;
    lu_char_t            schemeFileName[100];
    volatile DBG_STATE   curState;
    volatile lu_uint32_t curLine; /*line number starts from 0*/
    volatile lu_int32_t  cr; /*Current register value*/
    volatile lu_uint32_t nextStep;
    volatile BP_TYPE     breakpoints[MAX_NUM_BREAKPOINT];
    volatile lu_uint32_t breakpointCount;
    void* socketctxt;
    void* invoker;
}DebugContext;

/**
 * Implementation of "request_handler" in DebugSocket.h for processing
 * the debug messages from client.
 */
static lu_int32_t debugger_request_handler(
                void* dbgCtxt,
                const lu_uint8_t* requestPtr,
                const lu_int32_t requestSize,
                lu_uint8_t *replyPtr,
                lu_int32_t* replySize);
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

void* debugger_init(lu_uint32_t port, lu_int32_t schemeID, const lu_char_t* libName, void* invoker)
{
    /* Create a debug context*/
    DebugContext* ctxt = (DebugContext*)calloc(1, sizeof(DebugContext));
    ctxt->schemeID = schemeID;
    strncpy(ctxt->schemeFileName, libName, strlen(libName));

    /* Initialise socket*/
    void* sockctxt = debugsocket_init(port, ctxt, debugger_request_handler);
    if(sockctxt == NULL)
    {
        logger_err("Failed to initialise socket at port:%d",port);
        free(ctxt);
        return NULL;
    }

    ctxt->socketctxt = sockctxt;
    ctxt->invoker = invoker;
    ctxt->curLine = 0;
    ctxt->curState = DBG_STATE_NOT_RUNNING;
    ctxt->cr = 0;

    return (void*)ctxt;
}

void*  debugger_get_socket_contxt(void* dbgctxt)
{
    return ((DebugContext*)dbgctxt)->socketctxt;
}

void*  debugger_get_invoker(void* dbgctxt)
{
    return ((DebugContext*)dbgctxt)->invoker;
}


void  debugger_destroy(void* _context)
{
    if(_context!= NULL)
    {
        DebugContext* context = (DebugContext*)_context;
        debugsocket_destroy(context->socketctxt);
        context->socketctxt = NULL;
        free(context);
    }
}

lu_int32_t debugger_run(void* dbgctxt)
{
    DebugContext* ctxt = (DebugContext*)dbgctxt;

    if(ctxt != NULL)
    {
        do
        {
            debugsocket_process(ctxt->socketctxt);
        }
        while (1);
        debugsocket_destroy(ctxt->socketctxt);
        ctxt->socketctxt = NULL;
        return 0;
    }
    else
    {
        return -1;
    }

}

DBG_ERR debugger_set_breakpoint(void* _dbgctxt, lu_line_t lineNum)
{
    lu_uint32_t i;
    DebugContext* dbgctxt =  (DebugContext*)_dbgctxt;

    /* Check if already exists */
    for (i = 0; i < dbgctxt->breakpointCount; ++i) {
        if(dbgctxt->breakpoints[i] == lineNum)
        {
            /*Already exists*/
            return DBG_ERR_BP_ALREADY_EXIST;
        }
    }

    /* Check if reaches maxmium*/
    if (dbgctxt->breakpointCount >= MAX_NUM_BREAKPOINT)
    {
        return DBG_ERR_BP_REACH_MAX;
    }

    /* Add the new breakpoint to the pool*/
    dbgctxt->breakpoints[dbgctxt->breakpointCount++] = lineNum;
    logger_info("Set breakpoint at line:%d, count:%d", lineNum, dbgctxt->breakpointCount);

    return DBG_ERR_NONE;
}

DBG_ERR debugger_delete_breakpoint(void* _dbgctxt,lu_line_t lineNum)
{
    lu_uint32_t i;
    lu_uint32_t k;
    DebugContext* dbgctxt =  (DebugContext*)_dbgctxt;

   /* Check if already exists */
   for (i = 0; i < dbgctxt->breakpointCount; ++i) {
       if(dbgctxt->breakpoints[i] == lineNum)
       {
           /*Delete breakpoint*/
           for (k = i; k < dbgctxt->breakpointCount; k++)
           {
               dbgctxt->breakpoints[k] = dbgctxt->breakpoints[k+1];
           }
           dbgctxt->breakpointCount --;
           logger_info("Deleted breakpoint at line:%d, count:%d",
                           lineNum, dbgctxt->breakpointCount);
           return DBG_ERR_NONE;
       }
   }

   /* Breakpoint not found*/
   return DBG_ERR_BP_NOT_EXIST;
}

DBG_ERR debugger_pause(void* _dbgctxt)
{
    DebugContext* dbgctxt =  (DebugContext*)_dbgctxt;

    if(dbgctxt->curState != DBG_STATE_NOT_RUNNING)
        dbgctxt ->curState = DBG_STATE_SUSPENDED;

    return DBG_ERR_NONE;
}

DBG_ERR debugger_resume(void* _dbgctxt)
{
    DebugContext* dbgctxt =  (DebugContext*)_dbgctxt;

    if(dbgctxt->curState != DBG_STATE_NOT_RUNNING)
        dbgctxt ->curState = DBG_STATE_RUNNING;

    return DBG_ERR_NONE;
}

void debugger_checkpoint(void* _dbgctxt)
{
    if(_dbgctxt == NULL)
        return;

    DebugContext* dbgctxt =  (DebugContext*)_dbgctxt;

    switch(dbgctxt->curState)
    {
        case DBG_STATE_RUNNING:
            /* Nothing to do. */
            break;

        case DBG_STATE_STOPPED:
            /* Sleep permanently, Todo: notify automation scheme to exit.*/
            logger_info("STOPPED!");
             while(1)
             {
                sleep(100000);//10000ms
             }
            break;

        case DBG_STATE_SUSPENDED:
            logger_info("SUSPENDED!");
            /* Sleep until suspended state is changed or requires to move to the next step.*/
            while(dbgctxt->curState == DBG_STATE_SUSPENDED)
            {
                if(dbgctxt->nextStep > 0)
                {
                    dbgctxt->nextStep --;
                    break;
                }
                usleep(100000);// 100 ms
            }
            break;

        default: break;
    }

}

DBG_ERR debugger_step(void* _dbgctxt)
{

    DebugContext* dbgctxt =  (DebugContext*)_dbgctxt;

    if(dbgctxt->curState == DBG_STATE_NOT_RUNNING)
        return DBG_ERR_NONE;

    if(dbgctxt->curState == DBG_STATE_SUSPENDED)
    {
        if(dbgctxt->nextStep < MAX_NUM_STEP)
            dbgctxt->nextStep ++;
        else
            return DBG_ERR_STEP_FAIL;
    }

    return DBG_ERR_NONE;
}

DBG_ERR debugger_get_state(void* _dbgctxt, DebugState* statePtr)
{
    DebugContext* dbgctxt =  (DebugContext*)_dbgctxt;

    statePtr->cr        = dbgctxt->cr;
    statePtr->curLine   = dbgctxt->curLine;
    statePtr->curState  = dbgctxt->curState;
    return DBG_ERR_NONE;
}

const lu_char_t*  debugger_get_scheme_filename(void* dbgctxt)
{
	return ((DebugContext*)dbgctxt)->schemeFileName;
}

lu_int32_t  debugger_get_scheme_id(void* dbgctxt)
{
    return ((DebugContext*)dbgctxt)->schemeID;
}

void debugger_set_cr(void* _dbgctxt, lu_int32_t crValue)
{
    DebugContext* dbgctxt =  (DebugContext*)_dbgctxt;

    dbgctxt->cr = crValue;
}

void debugger_set_line_num(void* _dbgctxt, lu_line_t lineNumber)
{
    if(_dbgctxt == NULL)
           return;

    lu_uint32_t i;
    DebugContext* dbgctxt =  (DebugContext*)_dbgctxt;

    dbgctxt->curLine = lineNumber;

    /*
     * Check if the current line is set as a breakpoint.
     * if it is, changes state from running to suspended.
     */
    if(dbgctxt->curState == DBG_STATE_RUNNING)
    {
        for (i = 0; i < dbgctxt->breakpointCount; i++)
        {
            if(dbgctxt->curLine == dbgctxt->breakpoints[i])
            {
                dbgctxt->curState = DBG_STATE_SUSPENDED;
                logger_info("Reached breakpoint line:%d ", dbgctxt->breakpoints[i]);
            }
        }
    }
}

DBG_ERR debugger_reset_all(void* _dbgctxt)
{

    DebugContext* dbgctxt =  (DebugContext*)_dbgctxt;

    /* Delete all breakpoints */
    memset((void*)dbgctxt->breakpoints, 0, sizeof(BP_TYPE) * dbgctxt->breakpointCount);
    dbgctxt->breakpointCount = 0;

    /* Delete all pending steps*/
    dbgctxt->nextStep = 0;

    return DBG_ERR_NONE;
}

void debugger_set_scheme_running(void* _dbgctxt, lu_bool_t isRunning)
{
    DebugContext* dbgctxt =  (DebugContext*)_dbgctxt;

    if(isRunning)
    {
        dbgctxt->curState = DBG_STATE_RUNNING;
    }
    else
    {
        dbgctxt->curState = DBG_STATE_NOT_RUNNING;
    }
}
/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */
static lu_int32_t debugger_request_handler(
                void* dbgctxt,
                const lu_uint8_t* requestPtr,
                const lu_int32_t requestSize,
                lu_uint8_t *replyPtr,
                lu_int32_t* replySize)
{
    lu_uint32_t* requestID;
    const lu_uint8_t* requestPL;
    lu_uint32_t* replyID;
    lu_uint8_t* replyPL;
    DebugState state;
    lu_uint32_t requestPLSize;
    lu_uint32_t replyPLSize;

    requestPLSize = requestSize - DBG_HEADER_SIZE;
    replyPLSize = 0;
    requestID = (DBG_CMD*)requestPtr;
    requestPL = requestPtr + DBG_HEADER_SIZE;
    replyID = (DBG_REPLY*)replyPtr;
    replyPL = replyPtr + DBG_HEADER_SIZE;


    /* Check context*/
    if(dbgctxt == NULL)
    {
        strcpy(replyPtr, "internal error: context not available!");
        *replySize = strlen(replyPtr);
        return 0;
    }

    /* Validate request */
    if(requestSize <  DBG_HEADER_SIZE)
    {
        strcpy(replyPtr, "Invalid Request!");
        *replySize = strlen(replyPtr);
        return 0;
    }

    /* Initialise reply message*/
    DBG_ERR err = DBG_ERR_NONE;
    *replyID = DBG_REPLY_ACK;
    switch(*requestID)
    {
        case DBG_CMD_STEP:
            err = debugger_step(dbgctxt);
            break;

        case DBG_CMD_PAUSE:
            err = debugger_pause(dbgctxt);
            break;

        case DBG_CMD_RESUME:
            err = debugger_resume(dbgctxt);
            break;

        case DBG_CMD_SET_BREAKPOINT:
            if(requestPLSize == sizeof(lu_line_t))
                err = debugger_set_breakpoint(dbgctxt, *((lu_line_t*)requestPL));
            else
                err = DBG_ERR_INVALID_PAYLOAD;
            break;

        case DBG_CMD_DEL_BREAKPOINT:
            if(requestPLSize == sizeof(lu_uint32_t))
                err = debugger_delete_breakpoint(dbgctxt, *((lu_line_t*)requestPL));
            else
                err = DBG_ERR_INVALID_PAYLOAD;
            break;

        case DBG_CMD_GET_STATE:
            *replyID = DBG_REPLY_STATE;             /*Fill reply ID*/

            debugger_get_state(dbgctxt, &state);
            memcpy(replyPL, &state, sizeof(state)); /*Fill reply payload*/
            replyPLSize = sizeof(state);
            break;

        case DBG_CMD_GET_SCHEME_ID:
            *replyID = DBG_REPLY_SCHEID;             /*Fill reply ID*/
            lu_int32_t sid;
            sid = debugger_get_scheme_id(dbgctxt);

            memcpy(replyPL, &sid, sizeof(sid)); /*Fill reply payload*/
            replyPLSize = sizeof(sid);
            break;

        case DBG_CMD_GET_VAR:
        {
            *replyID = DBG_REPLY_VAR;
            lu_int32_t size =  requestPLSize / sizeof(DebugVarRef);
            if(requestPLSize % sizeof(DebugVarRef) == 0 && size < 100)
            {
                lu_int32_t i;
                DebugVarRef* varRefArray = (DebugVarRef*)requestPL;
                DebugVar* varArray = (DebugVar*)replyPL;
                replyPLSize = sizeof(DebugVar) * size;

                for (i = 0; i < size; ++i)
                {
                    varArray[i].ref.index = varRefArray[i].index;
                    varArray[i].ref.type= varRefArray[i].type;
                    varArray[i].value = debugger_get_variable(
                                    debugger_get_invoker(dbgctxt),
                                    varArray[i].ref);
                }
            }
            else
            {
                err = DBG_ERR_INVALID_PAYLOAD;
            }
        }
        break;

        case DBG_CMD_GET_LIB_FILE_NAME:
        {
        	const lu_char_t* fnamePtr = debugger_get_scheme_filename(dbgctxt);
        	replyPLSize = strlen(fnamePtr);
        	strncpy(replyPL, fnamePtr, replyPLSize);
        	*replyID = DBG_REPLY_LIB_FILE_NAME;
        	logger_info("Running auto lib name:%s len:%d", replyPL,replyPLSize);
        }
        	break;

        default:
            *replyID = DBG_REPLY_NACK;
            err = DBG_ERR_UNSUPPORTED;
            break;
    }

    if(err != DBG_ERR_NONE)
    {
        /* Reply NACK with err payload*/
        *replyID = DBG_REPLY_NACK;
        memcpy(replyPL, &err, sizeof(err));
        replyPLSize = sizeof(err);
    }

    *replySize = DBG_HEADER_SIZE +replyPLSize;

    return 0;
}

/*
 *********************** End of file ******************************************
 */
