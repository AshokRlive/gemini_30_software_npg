/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:DynamicLibraryUtil.c
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   27 Apr 2016     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <dlfcn.h>
#include <stddef.h>
/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "DynamicLibraryUtil.h"
#include "LoggerInterface.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static void s_dl_check_error(void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */


void* dl_load_function(void *handle, const char* functionName)
{
	void* ret = dlsym(handle, functionName);

    if(ret == NULL)
    {
        s_dl_check_error();
    }

    return ret;
}

lu_bool_t dl_load_shared_lib(void **pHandle, const char* schemeFileName)
{
    /* Load shared library file*/
    *pHandle = dlopen(schemeFileName, RTLD_NOW);

    if (*pHandle == NULL)
    {
        /* Failed*/
        s_dl_check_error();
        return LU_FALSE;
    }
    else
    {
        /* Succeeded*/
        return LU_TRUE;
    }
}

lu_bool_t dl_close_shared_lib(void *pHandle)
{
    lu_int32_t ret;
    ret = dlclose(pHandle);
    if(ret != 0)
    {
        s_dl_check_error();
    }

    return ret == 0 ? LU_TRUE : LU_FALSE;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */
static void s_dl_check_error()
{
    const char* error = dlerror();
    if(error != NULL)
    {
    	logger_err("%s", error);
    }
}
/*
 *********************** End of file ******************************************
 */
