/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:Debugger.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   28 Apr 2016     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef DEBUGGER_H_
#define DEBUGGER_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "dbgserver/DebuggerSocket.h"
#include "dbgserver/IDebuggerProtocol.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */
extern lu_int32_t debugger_get_variable(void* invoker, const DebugVarRef ref);

/**
 * Initialises the debugger.
 * return - debugger context pointer.
 */
void*     debugger_init(lu_uint32_t port, lu_int32_t schemeID, const lu_char_t* libName, void* invoker);
void      debugger_destroy(void* context);

/**
 * Runs the debugger with the context created from init.
 * return - error code. 0 if no error.
 */
lu_int32_t debugger_run(void* dbgctxt);

void*  debugger_get_socket_contxt(void* dbgctxt);
void*  debugger_get_invoker(void* dbgctxt);
lu_int32_t  debugger_get_scheme_id(void* dbgctxt);
const lu_char_t*  debugger_get_scheme_filename(void* dbgctxt);


/**
 * Checks the state of debug.
 */
void      debugger_checkpoint(void* dbgctxt);

/**
 * Set the current line number.
 * args
 *  number - line number, start from 0.
 */
void      debugger_set_line_num(void* dbgctxt, lu_line_t lineNumber);
void      debugger_set_cr(void* _dbgctxt, lu_int32_t crValue);

DBG_ERR   debugger_set_breakpoint(void* dbgctxt, lu_line_t lineNum);
DBG_ERR   debugger_delete_breakpoint(void* dbgctxt, lu_line_t lineNum);
DBG_ERR   debugger_step(void* dbgctxt);
DBG_ERR   debugger_pause(void* dbgctxt);
DBG_ERR   debugger_resume(void* dbgctxt);
DBG_ERR   debugger_get_state(void* _dbgctxt, DebugState* statePtr);
DBG_ERR   debugger_reset_all(void* dbgctxt);
void      debugger_set_scheme_running(void* _dbgctxt, lu_bool_t isRunning);

#endif /* DEBUGGER_H_ */

/*
 *********************** End of file ******************************************
 */
