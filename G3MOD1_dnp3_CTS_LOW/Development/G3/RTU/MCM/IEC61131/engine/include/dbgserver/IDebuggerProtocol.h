/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:DebuggerProtocol.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   28 Apr 2016     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef DEBUGGERPROTOCOL_H_
#define DEBUGGERPROTOCOL_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define DEFAULT_DEBUG_PORT 30023

#define DBG_HEADER_SIZE   4

#pragma pack(push)
#pragma pack(1) //Note: versions.h disables packing

typedef lu_uint32_t lu_line_t;


typedef struct
{
    lu_uint32_t curState;   /*DBG_STATE*/
    lu_line_t   curLine;    /*Current line number, start from 0.*/
    lu_int32_t  cr;         /*Current register value*/
} DebugState;

/* The references of debug variable.*/
typedef struct
{
    lu_uint8_t  type;    /*DBG_VAR*/
    lu_uint8_t  index;
} DebugVarRef;

/* The struct of debug variable.*/
typedef struct
{
    DebugVarRef  ref;
    lu_int32_t   value;
} DebugVar;

typedef enum
{
    /*Request message ID*/    /* Payload Type */
    DBG_CMD_STEP = 0,         /* No payload */
    DBG_CMD_PAUSE,            /* No payload */
    DBG_CMD_RESUME,           /* No payload */
    DBG_CMD_SET_BREAKPOINT,   /* lu_int32_t, line number */
    DBG_CMD_DEL_BREAKPOINT,   /* lu_int32_t, line number */
    DBG_CMD_GET_STATE,        /* No payload */
    DBG_CMD_GET_SCHEME_ID,    /* lu_int32_t, auto scheme index */
    DBG_CMD_GET_VAR,          /* A list of DebugVarRef */
    DBG_CMD_GET_LIB_FILE_NAME,/* Get the file name of current running automation library.*/
} DBG_CMD;

typedef enum
{
    DBG_ERR_NONE = 0,
    DBG_ERR_BP_ALREADY_EXIST,
    DBG_ERR_BP_NOT_EXIST,
    DBG_ERR_BP_REACH_MAX,
    DBG_ERR_STEP_FAIL,
    DBG_ERR_UNSUPPORTED,
    DBG_ERR_INVALID_PAYLOAD
}DBG_ERR;

typedef enum
{
    DBG_VAR_INPUT= 0,
    DBG_VAR_CONSTANT,
    DBG_VAR_OUTPUT,
}DBG_VAR;

typedef enum
{
    /*Reply message ID*/     /* Payload Type */
    DBG_REPLY_ACK = 0,       /* No payload   */
    DBG_REPLY_NACK,          /* DBG_ERR      */
    DBG_REPLY_STATE,         /* DebugState   */
    DBG_REPLY_SCHEID,        /* Scheme ID    */
    DBG_REPLY_VAR,           /* A list of DebugVar */
	DBG_REPLY_LIB_FILE_NAME  /* The running library file name */
} DBG_REPLY;

typedef enum  {
    DBG_STATE_NOT_RUNNING,
    DBG_STATE_RUNNING,
    DBG_STATE_SUSPENDED,
    DBG_STATE_STOPPED,
    DBG_STATE_LAST,
}DBG_STATE;

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */
#pragma pack(pop)

#endif /* DEBUGGERPROTOCOL_H_ */

/*
 *********************** End of file ******************************************
 */
