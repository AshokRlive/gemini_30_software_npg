/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:AutomationEngine.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   3 Jun 2015     pwang     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef AUTOMATIONENGINE_H_
#define AUTOMATIONENGINE_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "AutomationScheme.h"



/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef struct
{
    /* The file name of the shared library.*/
	//const char* schemeFileName;

	/* Debug context.*/
	void* dbgctxt;

	/* Invoker of the automation engine.*/
	const void* invoker;

	/* The filer handler of the loaded shared library.*/
	void* libHandler;

	/* APIs of the loaded library*/
	p_autoscheme_register_callback registerCallback;
	p_autoscheme_check_ready checkReady;
	p_autoscheme_run run;

} AutomationContext;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
 * Initialises automation engine and creates the context for running automation scheme.
 * arguments:
 * 	invoker - the pointer to the caller of the engine.
 * 	schemeFileName - the library name of the automation scheme to be run.
 * return
 * 	the context pointer. NULL if fails to initialise.
 */
lu_bool_t automationengine_init(AutomationContext* ctxt, const lu_char_t* schemeFilePath);

void automationengine_de_init(AutomationContext* ctxt);

/**
 * Runs the automation engine with the context created by autoengine_init().
 * return: 0 - success
 *         1 - failure
 */
lu_int32_t automationengine_run(AutomationContext* ctxt);

#endif /* AUTOMATIONENGINE_H_ */

/*
 *********************** End of file ******************************************
 */
