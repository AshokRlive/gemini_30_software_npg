/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:DynamicLibraryUtil.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   27 Apr 2016     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef DYNAMICLIBRARYUTIL_H_
#define DYNAMICLIBRARYUTIL_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/**
 * Loads a shared library.
 * arguments:
 * 	pHandle - the pointer to the shared library handle, which is to be set to
 * 				non-null if the library is loaded successfully.
 * 	libFileName - the file to be loaded. The path should be relative to the
 * 				current working path.
 */
lu_bool_t dl_load_shared_lib(void **pHandle, const char* libFilePath);


/**
 * Loads a function from a shared library.
 * arguments:
 * 	handle - the handle of the shared library.
 * 	functionName - the name of the function.
 * return:
 * 	the pointer to the function. NULL if fails.
 */
void* dl_load_function(void *handle, const char* functionName);

/**
 * Closes the loaded shared library.
 */
lu_bool_t dl_close_shared_lib(void *pHandle);

#endif /* DYNAMICLIBRARYUTIL_H_ */

/*
 *********************** End of file ******************************************
 */
