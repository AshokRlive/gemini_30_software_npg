/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: DebugSocket.h
 *    FILE TYPE: c header
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   28 April 2016     pwang     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef DEBUGSOCKET_H_
#define DEBUGSOCKET_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define MAX_CONNECTION_NUM  1

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
/**
 * A callback to be called when receiving a request from client.
 */
typedef lu_int32_t (*debugsocket_request_handler)(
                                    void* userHandler,
                                    const lu_uint8_t* requestPtr,
                                    const lu_int32_t requestSize,
                                    lu_uint8_t *replyPtr,
                                    lu_int32_t* replySize);

/**
 * A callback to be called when the number of existing connections changes.
 */
typedef void (*debugsocket_connchg_handler)(void* userContext, lu_int32_t existingConnNum);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */
/**
 * Initialise socket.
 * return the context pointer of the created socket .
 */
void* debugsocket_init(lu_int32_t port, void* userContext,
                debugsocket_request_handler requestHandler);

/**
 * Process incoming message and send reply.
 * arguments:
 *  context - The context pointer returned from init function.
 */
void debugsocket_process(void* context);

/**
 * Registers a connection change handle to observe the change of connections.
 */
void debugsocket_register_connchg_handler(void* socketContext,
                debugsocket_connchg_handler connchg_handler);

/**
 * Destroy the context.
 */
void debugsocket_destroy(void* context);

#endif /* DEBUGSOCKET_H_ */

/*
 *********************** End of file ******************************************
 */
