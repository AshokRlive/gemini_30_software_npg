#!/bin/bash
# copy axf of MCM board to export path, for sending to the MCM itself
APP="Debug/*.axf"
DEST="/home/opt/exports/${USER}/gemini/application"

cd Debug
make $1
RESULT=$?
if [ $RESULT -eq 0 ]
then
    # successful make
    cd ..
else
    exit $RESULT
fi
