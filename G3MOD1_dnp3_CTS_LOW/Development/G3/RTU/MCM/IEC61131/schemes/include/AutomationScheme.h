/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 Automation
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       This header file defines the API that Automation scheme should implement.
 *
 *    CURRENT REVISION
 *
 *               $Revision::               $: (Revision of last commit)
 *               $Author::                 $: (Author of last commit)
 *       \date   $Date::                   $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24/12/15      wang_p    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */
#ifndef _AUTOMATION_SCHEMA_INCLUDED
#define _AUTOMATION_SCHEMA_INCLUDED


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define FUNC_NAME_GET_API       		"autoscheme_get_api"
#define FUNC_NAME_REG_CALLBACK    		"autoscheme_register_callback"
#define FUNC_NAME_CHECK_SCHEME_READY 	"autoscheme_check_ready"
#define FUNC_NAME_RUN_AUTO_SCHEME 		"autoscheme_run"

#define AUTO_API_MAJOR   1
#define AUTO_API_MINOR   0

typedef struct
{
    lu_uint32_t major;
    lu_uint32_t minor;
} AutoSchemeAPI;

/**
 * The enum to identify automation scheme internal callback functions.
 * Internal callbacks are supposed to be registered via automation engine instead
 * of application.
 */
typedef enum {
	AS_INTERN_CALLBACK_ID_LOG_INFO      = 0,
	AS_INTERN_CALLBACK_ID_LOG_ERROR     ,
	AS_INTERN_CALLBACK_ID_CHECKPOINT    ,
	AS_INTERN_CALLBACK_ID_DELAY         ,
	AS_INTERN_CALLBACK_ID_SET_LINE_NUM  ,
	AS_INTERN_CALLBACK_ID_SET_CR        ,
	AS_INTERN_CALLBACK_ID_LAST
}AS_INTERN_CALLBACK_ID;

/**
 * The enum to identify automation scheme callback functions.
 */
typedef enum {
    AS_CALLBACK_ID_GET_CONSTANT    = AS_INTERN_CALLBACK_ID_LAST,
    AS_CALLBACK_ID_GET_INPUT       ,
    AS_CALLBACK_ID_SET_POINT       ,
    AS_CALLBACK_ID_OPERATE         ,
    AS_CALLBACK_ID_LAST,
}AS_CALLBACK_ID;

/**
 * Automation Scheme Error Code.
 */
typedef enum {
    AS_ERROR_NONE = 0,
    AS_ERROR_INVALID_INDEX,
    AS_ERROR_GENERAL,
    AS_ERROR_FAILED,
    AS_ERROR_CALLBACK_NOT_REGISTERED,
} AS_ERROR;

/*
 ******************************************************************************
 * Signature of all callback functions.
 ******************************************************************************/
typedef AS_ERROR (*p_cb_get_constant   )(void* context, lu_uint32_t index, lu_int32_t* pValue);
typedef AS_ERROR (*p_cb_get_input      )(void* context, lu_uint32_t index, lu_uint32_t* pValue);
typedef AS_ERROR (*p_cb_set_point      )(void* context, lu_uint32_t index, lu_uint32_t value);
typedef AS_ERROR (*p_cb_operate        )(void* context, lu_uint32_t index, lu_uint16_t operationCode);
typedef AS_ERROR (*p_cb_set_line_number)(void* context, lu_uint32_t lineNumber); /* Note lineNumber start form 0*/
typedef AS_ERROR (*p_cb_set_cr         )(void* context, lu_int32_t crValue);


/*
 ******************************************************************************
 * Signature of all optional callback functions.
 ******************************************************************************/
typedef AS_ERROR (*p_cb_delay          )(void* context, lu_uint32_t milliseconds);
typedef void	 (*p_cb_checkpoint     )(void* context);
typedef void     (*p_cb_log_err		   )(void* context,lu_char_t* errMsg);
typedef void     (*p_cb_log_info       )(void* context,lu_char_t* infoMsg);


/*
 ******************************************************************************
 * APIs of automation scheme library.
 ******************************************************************************/

/**
 * Gets the API of the auto scheme library.
 */
void autoscheme_get_api(AutoSchemeAPI* api);

/**
 * Registers a callback function which will be called by automation scheme.
 */
AS_ERROR autoscheme_register_callback(AS_CALLBACK_ID cbFuncIndex,  void* cbFuncPtr);


/**
 * Checks if the automation scheme is ready for run.
 */
AS_ERROR autoscheme_check_ready(void);


/**
 * Runs automation scheme.
 */
AS_ERROR autoscheme_run(void* context);


/******************************************************************************
* Signature of all API functions.
******************************************************************************/
typedef void     (*p_autoscheme_get_api)(AutoSchemeAPI* api);
typedef AS_ERROR (*p_autoscheme_register_callback)(AS_CALLBACK_ID cbFuncId,  void* cbFuncPtr);
typedef AS_ERROR (*p_autoscheme_check_ready)(void);
typedef AS_ERROR (*p_autoscheme_run)(void* context);

#endif
