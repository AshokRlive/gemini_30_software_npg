/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:AutomationSchemeLib.c
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   27 Apr 2016     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <stddef.h>
#include <unistd.h> /*sleep*/

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "AutomationSchemeLib.h"
#include "AutomationScheme.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static void** callbacksArray = NULL;
static void* context = 0;


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Implementation of APIs in AutomationSchemeLib.h
 ******************************************************************************
 */
void DelayMs(lu_uint32_t ms) {
	((p_cb_delay)callbacksArray[AS_INTERN_CALLBACK_ID_DELAY])(context, ms);
    ((p_cb_checkpoint)callbacksArray[AS_INTERN_CALLBACK_ID_CHECKPOINT])(context);
}

void SetLineNum(lu_uint32_t number) {
    ((p_cb_set_line_number)callbacksArray[AS_INTERN_CALLBACK_ID_SET_LINE_NUM])(context, (lu_uint32_t)number - 1);
}

void SetCurrentRegister(lu_int32_t number){
    ((p_cb_set_cr)callbacksArray[AS_INTERN_CALLBACK_ID_SET_CR])(context, (lu_int32_t)number);
}


void Operate(lu_uint32_t operate_id, lu_uint32_t value) {
    ((p_cb_operate)callbacksArray[AS_CALLBACK_ID_OPERATE])(context, operate_id, value);
}

void SetOutput(lu_uint32_t output_id, lu_uint32_t value) {
    ((p_cb_set_point)callbacksArray[AS_CALLBACK_ID_SET_POINT])(context, output_id, value);
}

lu_uint32_t GetInput(lu_uint32_t input_id) {
	lu_uint32_t value = 0;
	AS_ERROR err;

	err = ((p_cb_get_input)callbacksArray[AS_CALLBACK_ID_GET_INPUT])(context, input_id, &value);

	return value;
}

lu_int32_t GetConstant(lu_uint32_t constant_id) {
	lu_int32_t value = 0;
	((p_cb_get_constant)callbacksArray[AS_CALLBACK_ID_GET_CONSTANT])(context, constant_id, &value);
	return value;
}

void LogInfo(lu_char_t* msg, ...)
{
	lu_char_t buffer[256];
	memset(buffer,'\0',256);

	va_list args;
	va_start (args, msg);

	vsprintf(buffer, msg, args);
	((p_cb_log_info)callbacksArray[AS_INTERN_CALLBACK_ID_LOG_INFO])(context, buffer);

	va_end (args);
}

void LogErr(lu_char_t* msg, ...)
{
	lu_char_t buffer[256];
	memset(buffer,'\0',256);

	va_list args;
	va_start (args, msg);

	vsprintf(buffer, msg, args);
	((p_cb_log_err)callbacksArray[AS_INTERN_CALLBACK_ID_LOG_ERROR])(context, buffer);

	va_end (args);
}


/*
 ******************************************************************************
 * Implementation of APIs in AutomationScheme.h
 ******************************************************************************
 */
void autoscheme_get_api(AutoSchemeAPI* api)
{
    api->major = AUTO_API_MAJOR;
    api->minor = AUTO_API_MINOR;
}

AS_ERROR autoscheme_register_callback(AS_CALLBACK_ID cbFuncIndex,  void* cbFuncPtr) {
    /* Initialise callback array*/
    if(callbacksArray == NULL)
    {
        callbacksArray = calloc(AS_CALLBACK_ID_LAST, sizeof(void*));
    }


    if(cbFuncIndex>=0 && cbFuncIndex < AS_CALLBACK_ID_LAST)
    {
        callbacksArray[cbFuncIndex] = cbFuncPtr;
        return AS_ERROR_NONE;
    }
    else
    {
        return AS_ERROR_INVALID_INDEX;
    }
}

void* autoscheme_get_registered_callback(AS_CALLBACK_ID cbFuncIndex)
{
    if(cbFuncIndex>=0 && cbFuncIndex < AS_CALLBACK_ID_LAST)
        return callbacksArray[cbFuncIndex];
    else
        return NULL;
}


AS_ERROR autoscheme_check_ready(void)
{
	/* Check all callback registered*/
	if(callbacksArray == NULL)
	   return AS_ERROR_CALLBACK_NOT_REGISTERED;

	lu_uint32_t var;
	for (var = 0; var < AS_CALLBACK_ID_LAST; ++var) {
		if(callbacksArray[var] == NULL)
			return AS_ERROR_CALLBACK_NOT_REGISTERED;
	}

	return AS_ERROR_NONE;
}

AS_ERROR autoscheme_run(void* _context) {
    context = _context;

    AS_ERROR err = autoscheme_check_ready();

    if(err == AS_ERROR_NONE)
    	runGeneratedCode();

	return err;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
