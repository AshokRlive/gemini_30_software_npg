=== File Description ==== 
AutomationSchema.xsd 	- the schema of all Auomation template XML.[ConfigTool, *Template.xml]
AutomationEnum.xml - the XML-defined enum used by AutomationTemplate.xsd and ConfigTool.[XSLT]
AutomationEnum.xsd - a module of AutomationTemplate schema. [AutomationTemplate.xsd, ConfigTool]
GenAutomationEnumXSD.bat - for generating AutomationEnum.xsd from AutomationEnum.xml.[XSLT]


AutoClose/AutoClose.il - automation scheme source file.[61131 Compiler]
AutoClose/AutoClose.c  -  C file generated from AutoClose.il[MCMApp]
AutoClose/AutoCloseTemplate.xml - the template for initialising the Automation Logic in ConfigTool[ConfigTool]	

Note: [] indicates which project require this resource.



=== How to Add New Automation Scheme ==== 
Here is an example of how to add a new Automation Scheme "Sectionaliser"
1. Add a new folder "Sectionaliser" which contains "Sectionaliser.il" and "SectionaliserTemplate.xml"
   (Tips: you can create the new folder by copying&modifiying the existing one,e.g. "AutoOpen")

2. Generate "Sectionaliser.c" from "Sectionaliser.il" using 61131 Eclipse Tool.

3. Change AutomationEnum.xml, add new enum item to "AutoSchemeType", "AutoSchemeLib", "AutoSchemeTemplate" for "Sectionaliser".

4. Change lib/CMakeLists.txt, add the following content:
	add_library( Sectionaliser SHARED
		${AutomationCommonSrc}         
		../Sectionaliser/Sectionaliser.c
	)  
	
5. Update "SectionaliserTemplate.xml" to make sure all inputs/outputs/points are defined properly 
	and consistent with the "SectionaliserTemplate.il" in the same folder.
	
	