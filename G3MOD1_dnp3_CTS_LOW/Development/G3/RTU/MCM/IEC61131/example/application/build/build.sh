#!/bin/bash
# copy axf of MCM board to export path, for sending to the MCM itself
APP="Debug/*.axf"
DEST="/home/opt/exports/${USER}/gemini/application"

cd Debug
make $1
RESULT=$?
if [ $RESULT -eq 0 ]
then
    # successful make
    cd ..
    cp -v $APP $DEST
    cp -v Debug/schemes/*.so $DEST/lib/automation/
    echo copy done!
else
    exit $RESULT
fi
