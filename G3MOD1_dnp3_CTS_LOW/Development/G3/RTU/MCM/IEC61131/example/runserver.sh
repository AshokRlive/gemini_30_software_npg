#!/bin/bash
cd ./application/build/

# build
./createMakefile.sh
./build.sh

cd -

# run
./application/build/Debug/Automation*.axf ./application/build/Debug/schemes/libAutoChangeover.so 