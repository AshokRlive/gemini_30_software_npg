 /******************************************************************************
 *                Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [package automation.debugger.client;]
 *
 *    FILE NAME: 
 *               $$Id$$
 *               $$HeadURL$$
 *
 *    FILE TYPE: java source
 *
 *    DESCRIPTION:
 *       
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $$Rev:: $$: (Revision of last commit)
 *               $$Author:: $$: (Author of last commit)
 *          	 $$Date:: $$: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        			Details
 *   --------------------------------------------------------------------------
 *   Fri Apr 29 08:26:26 BST 2016      Julian Fryers        	Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

package com.lucy.automation.debugger.client.ui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.automation.debugger.client.CommandController;
import com.lucy.automation.debugger.client.CommandController.Reply;
import com.lucy.automation.debugger.client.IDebuggerProtocol;

/**
 * @author Julian Fryers
 */
public class MainFrame extends JFrame implements IDebuggerProtocol {
  private final CommandController controller = new CommandController();
  
  public MainFrame() {
    initComponents();
    pack();
  }

  private void executeCommand(DBG_CMD cmd, int payloadData) {
    try{
      Reply result;
      result = controller.sendCommand(cmd, payloadData);
      lblResult.setText(result.toString());
      
    } catch (Exception e) {
      lblResult.setText("");
      e.printStackTrace();
      JOptionPane.showMessageDialog(this, 
          "Error:"+e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
    }
  }
  
  private void btnStepActionPerformed(ActionEvent e) {
    executeCommand(DBG_CMD.STEP, 0);
  }

  private void btnPauseActionPerformed() {
    executeCommand(DBG_CMD.PAUSE, 0);
  }

  private void btnResumeActionPerformed() {
    executeCommand(DBG_CMD.RESUME, 0);
  }

  private void btnSetBPActionPerformed() {
    int line = Integer.valueOf(tfBP.getText());
    executeCommand(DBG_CMD.SET_BREAKPOINT, line);
  }

  private void btnGetLineActionPerformed() {
    executeCommand(DBG_CMD.GET_LINE, 0);
  }

  private void btnDelBPActionPerformed() {
    int line = Integer.valueOf(tfBP.getText());
    executeCommand(DBG_CMD.DEL_BREAKPOINT, line);
  }

  private void okButtonActionPerformed() {
    System.exit(0);
  }

  private void btnConnectActionPerformed(ActionEvent e) {
	  controller.setIp(tfHost.getText());
	  controller.setPort(Integer.valueOf(tfPort.getText()));
	  controller.connect();
  }

  private void btnDisconnectActionPerformed(ActionEvent e) {
	  controller.disconnect();
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
	dialogPane = new JPanel();
	contentPanel = new JPanel();
	panel1 = new JPanel();
	label1 = new JLabel();
	tfHost = new JTextField();
	label3 = new JLabel();
	tfPort = new JTextField();
	btnConnect = new JButton();
	btnDisconnect = new JButton();
	label2 = new JLabel();
	lblResult = new JLabel();
	btnStep = new JButton();
	btnPause = new JButton();
	btnResume = new JButton();
	btnSetBP = new JButton();
	btnDelBP = new JButton();
	tfBP = new JTextField();
	btnGetLine = new JButton();
	lblCurLine = new JLabel();
	buttonBar = new JPanel();
	okButton = new JButton();

	//======== this ========
	setTitle("IEC 61131 Debugger");
	setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	Container contentPane = getContentPane();
	contentPane.setLayout(new BorderLayout());

	//======== dialogPane ========
	{
		dialogPane.setBorder(Borders.DIALOG_BORDER);
		dialogPane.setLayout(new BorderLayout());

		//======== contentPanel ========
		{
			contentPanel.setLayout(new FormLayout(
				"2*(default, $lcgap), [50dlu,default], $lcgap, default:grow",
				"default, $ugap, default, $pgap, 4*(default, $lgap), default"));
			((FormLayout)contentPanel.getLayout()).setColumnGroups(new int[][] {{1, 3}});

			//======== panel1 ========
			{
				panel1.setLayout(new FormLayout(
					"default, $lcgap, [80dlu,default], $lcgap, default, $lcgap, [50dlu,default], 2*($lcgap, default)",
					"default"));

				//---- label1 ----
				label1.setText("Host Address:");
				panel1.add(label1, CC.xy(1, 1));

				//---- tfHost ----
				tfHost.setText("10.11.11.102");
				panel1.add(tfHost, CC.xy(3, 1));

				//---- label3 ----
				label3.setText("Port:");
				panel1.add(label3, CC.xy(5, 1));

				//---- tfPort ----
				tfPort.setText("8888");
				panel1.add(tfPort, CC.xy(7, 1));

				//---- btnConnect ----
				btnConnect.setText("Connect");
				btnConnect.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						btnConnectActionPerformed(e);
					}
				});
				panel1.add(btnConnect, CC.xy(9, 1));

				//---- btnDisconnect ----
				btnDisconnect.setText("Disconnect");
				btnDisconnect.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						btnDisconnectActionPerformed(e);
					}
				});
				panel1.add(btnDisconnect, CC.xy(11, 1));
			}
			contentPanel.add(panel1, CC.xywh(1, 1, 7, 1));

			//---- label2 ----
			label2.setText("Result:");
			contentPanel.add(label2, CC.xy(1, 3));
			contentPanel.add(lblResult, CC.xywh(2, 3, 6, 1));

			//---- btnStep ----
			btnStep.setText("Step");
			btnStep.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					btnStepActionPerformed(e);
				}
			});
			contentPanel.add(btnStep, CC.xy(1, 5));

			//---- btnPause ----
			btnPause.setText("Pause");
			btnPause.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					btnPauseActionPerformed();
				}
			});
			contentPanel.add(btnPause, CC.xy(1, 7));

			//---- btnResume ----
			btnResume.setText("Resume");
			btnResume.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					btnResumeActionPerformed();
				}
			});
			contentPanel.add(btnResume, CC.xy(1, 9));

			//---- btnSetBP ----
			btnSetBP.setText("Set BP");
			btnSetBP.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					btnSetBPActionPerformed();
				}
			});
			contentPanel.add(btnSetBP, CC.xy(1, 11));

			//---- btnDelBP ----
			btnDelBP.setText("Del BP");
			btnDelBP.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					btnDelBPActionPerformed();
				}
			});
			contentPanel.add(btnDelBP, CC.xy(3, 11));
			contentPanel.add(tfBP, CC.xy(5, 11));

			//---- btnGetLine ----
			btnGetLine.setText("Get  Line");
			btnGetLine.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					btnGetLineActionPerformed();
				}
			});
			contentPanel.add(btnGetLine, CC.xy(1, 13));
			contentPanel.add(lblCurLine, CC.xywh(3, 13, 5, 1));
		}
		dialogPane.add(contentPanel, BorderLayout.CENTER);

		//======== buttonBar ========
		{
			buttonBar.setBorder(Borders.BUTTON_BAR_GAP_BORDER);
			buttonBar.setLayout(new FormLayout(
				"$glue, $button",
				"pref"));

			//---- okButton ----
			okButton.setText("OK");
			okButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					okButtonActionPerformed();
				}
			});
			buttonBar.add(okButton, CC.xy(2, 1));
		}
		dialogPane.add(buttonBar, BorderLayout.SOUTH);
	}
	contentPane.add(dialogPane, BorderLayout.CENTER);
	pack();
	setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel dialogPane;
  private JPanel contentPanel;
  private JPanel panel1;
  private JLabel label1;
  private JTextField tfHost;
  private JLabel label3;
  private JTextField tfPort;
  private JButton btnConnect;
  private JButton btnDisconnect;
  private JLabel label2;
  private JLabel lblResult;
  private JButton btnStep;
  private JButton btnPause;
  private JButton btnResume;
  private JButton btnSetBP;
  private JButton btnDelBP;
  private JTextField tfBP;
  private JButton btnGetLine;
  private JLabel lblCurLine;
  private JPanel buttonBar;
  private JButton okButton;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
