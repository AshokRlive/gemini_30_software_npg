package com.lucy.automation.debugger.client;

import java.io.IOException;
import java.net.UnknownHostException;

import com.lucy.automation.debugger.client.ui.MainFrame;

public class Main {

 
	public static void main(String[] args) throws UnknownHostException, IOException {
	  new MainFrame().setVisible(true);
	}
  
}

