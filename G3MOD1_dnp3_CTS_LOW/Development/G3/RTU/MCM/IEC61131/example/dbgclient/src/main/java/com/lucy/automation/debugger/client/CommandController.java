
package com.lucy.automation.debugger.client;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Arrays;

import javax.swing.JOptionPane;


public class CommandController implements IDebuggerProtocol{
  private String ip;
  private int port;
  
  private Socket socket;
  
	public void connect() {
		if (socket == null || socket.isClosed()) {
			try {
				socket = new Socket();
				socket.connect(new InetSocketAddress(ip, port) , 1000);
				System.out.println(new String(readSocket()));
							
			} catch (Exception e) {
				if(socket != null){
					try {
						socket.close();
					} catch (IOException e1) {
					}
					socket = null;
				}
				JOptionPane.showMessageDialog(null, e.getMessage(), "Connect Failed", JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
			}
		}
	}
  
	public void disconnect() {
		if (socket != null) {
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			socket = null;
		}
	}

  public String getIp() {
    return ip;
  }

  
  public void setIp(String ip) {
    this.ip = ip;
  }
  
  public int getPort() {
    return port;
  }

  public void setPort(int port) {
    this.port = port;
  }

  public Reply sendCommand(DBG_CMD cmd, int payloadData) throws UnknownHostException, IOException {
	  if(socket == null){
		  throw new IOException("No conneciton!");
	  }
	  
    final int command = cmd.ordinal();
    ByteBuffer buf = createbuf();
    buf.putInt(command);
    buf.putInt(payloadData);
    
    byte[] rcv = sendData(buf.array());

    Reply reply = new Reply();
    
    if(rcv.length == DBG_MESSAGE_SIZE) {
      buf = createbuf(rcv);
      reply.id = DBG_REPLY.values()[buf.getInt()];
      if(reply.id == DBG_REPLY.LINE) {
        reply.lineNum = buf.getInt();
      }
    }else{
    	System.err.println("Invalid reply length:"+rcv.length+" expected:"+DBG_MESSAGE_SIZE);
    	throw new IOException("No reply!");
    }
    
    return reply;
  }
  
  public byte[] sendData(byte[] data) throws UnknownHostException, IOException {
    writeSocket(data);
    return readSocket();
  }

private void writeSocket(byte[] data) throws IOException {
	OutputStream out = socket.getOutputStream();
    out.write(data);
}

private byte[] readSocket() throws IOException {
	BufferedInputStream in = 
        new BufferedInputStream(socket.getInputStream());
    byte[] buffer = new byte[1024];
    int nbytes = in.read(buffer, 0, buffer.length);
    
    if(nbytes > 0 )
    	return Arrays.copyOf(buffer, nbytes);
    return
		new byte[0];
}

  private static ByteBuffer createbuf() {
    ByteBuffer buf = ByteBuffer.allocate(DBG_MESSAGE_SIZE);
    buf.order(RTU_BYTE_ORDER);
    return buf;
  }
  
  private static ByteBuffer createbuf(byte[] data) {
    ByteBuffer buf = ByteBuffer.wrap(data);
    buf.order(RTU_BYTE_ORDER);
    return buf;
  }
  
  public final static class Reply {
    private DBG_REPLY id;
    private int lineNum = -1;
    
    public String toString() {
      return String.format("%s %s", 
          id == null?"Invalid":id.name(), 
          lineNum >= 0 ? lineNum :"");
    }
  }
  
}

