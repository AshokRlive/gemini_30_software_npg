
package com.lucy.automation.debugger.client;

import java.nio.ByteOrder;

public interface IDebuggerProtocol {

  int DBG_MESSAGE_SIZE = 20;
  ByteOrder RTU_BYTE_ORDER = ByteOrder.LITTLE_ENDIAN;


  enum DBG_CMD {
    STEP,
    PAUSE,
    RESUME,
    SET_BREAKPOINT,
    DEL_BREAKPOINT,
    GET_LINE,
    GET_STATE,
  };

  enum DBG_REPLY {
    ACK,
    NACK,
    LINE,
  };
  
  enum  DBG_STATE{
    NOT_RUNNING,
    RUNNING,
    SUSPENDED,
    STOPPED,
  };
}
