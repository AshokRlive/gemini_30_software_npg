#!/bin/bash
# This script is for building all MCM software into a zip.

# Environment variables
CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
INSTALL_DIR=$CURRENT_DIR/installed
BUILD_TYPE=Release
if [ $# -eq 2 ]
  then
INSTALL_DIR=$1
BUILD_TYPE=$2
fi

CT_INSTALL_DIR=${INSTALL_DIR}/html/
ALL_PROJECTS=(MCMApp MCMUpdater MCMConsole MCMMonitor)



# Function to check the return code, exit if there is an error.
checkErr(){
RESULT=$?
if [ $RESULT -ne 0 ]; then
	exit $RESULT
fi
}

#Clean install dir
rm -r ${INSTALL_DIR}
mkdir -p ${INSTALL_DIR}

# Build MCM software applications
for PROJECT in "${ALL_PROJECTS[@]}"
do	
	bash $CURRENT_DIR/createMakefile.sh $PROJECT $BUILD_TYPE $INSTALL_DIR	
	checkErr	
	
	bash $CURRENT_DIR/build.sh $PROJECT $BUILD_TYPE	
	checkErr
done


# Build FORTE
FORTE_BUILD_DIR=${BUILD_TYPE}/forte/
rm -fr ${FORTE_BUILD_DIR}
mkdir ${FORTE_BUILD_DIR}
cd ${FORTE_BUILD_DIR}
cmake -DCMAKE_TOOLCHAIN_FILE=../../../cmake/GCC_TX25_DI_toolchain.cmake \
	../../../../thirdParty/FORTE \
	-DCMAKE_BUILD_TYPE=${BUILD_TYPE} \
	-DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} \
	-DFORTE_ARCHITECTURE=Posix \
	-DFORTE_COM_ETH=ON \
	-DFORTE_COM_LOCAL=OFF \
	-DFORTE_TESTS=OFF \
	-DFORTE_MODULE_CONVERT=OFF \
	-DFORTE_MODULE_IEC61131=OFF \
	-DFORTE_MODULE_UTILS=OFF \
	-DFORTE_MODULE_SysFs=OFF \
	-DFORTE_MODULE_G3RTU=ON \
	-DFORTE_TRACE_EVENTS=ON \
	-DFORTE_EXTERNAL_MODULES_DIRECTORY=../../../IEC61499/modules/
make install/strip
cd -


# Build ConfigTool 
echo "Building ConfigTool software to:${CT_INSTALL_DIR}"
mvn -f $CURRENT_DIR/../../../RTUTools/G3ConfigTool/pom.xml -q -pl com.lucy:g3.configtool.app -am package -DDIST_DIR=${CT_INSTALL_DIR} -DskipTests 
checkErr


#Build zip package
echo "Creating MCM software package..."
cd ${INSTALL_DIR}
zip -r -m MCMBoard.zip *
checkErr
cd -

echo "The MCM package was generated to:\r\n ${INSTALL_DIR}/MCMBoard.zip"
exit 0
