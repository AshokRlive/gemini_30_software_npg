#!/bin/bash
if [ $# -ne 2 ]
  then
    echo "Usage: ./build.sh PROJECT RELEASE_TYPE. E.g. ./build.sh MCMApp Release"
    exit -1
fi

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PROJECT=$1
BUILD_TYPE=$2
BUILD_DIR=${CURRENT_DIR}/${BUILD_TYPE}/${PROJECT}/

echo "Building $PROJECT...[${BUILD_TYPE}]"
cd ${BUILD_DIR}
make install/strip

# Check error
RESULT=$?	
if [ $RESULT -ne 0 ]; then
	cd -
	exit $RESULT
fi

# Done
cd -

