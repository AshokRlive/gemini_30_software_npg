#!/bin/bash
if [ $# -ne 3  ]
  then
    echo "Usage: ./build.sh PROJECT RELEASE_TYPE INSTALL_DIR. E.g:\"./build.sh MCMApp Release /home/usr/export/install\""
    exit -1
fi

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PROJECT=$1
BUILD_TYPE=$2
BUILD_DIR=${CURRENT_DIR}/${BUILD_TYPE}/${PROJECT}/
INSTALL_DIR=$3

echo "Creating makefile for "$PROJECT"..."
echo "Build directory: "$BUILD_DIR""
# Clean
rm -rf ${BUILD_DIR}

# Create directory
mkdir -p ${BUILD_DIR}

# Run cmake and create Makefiles
cd ${BUILD_DIR}
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=${BUILD_TYPE} ${CURRENT_DIR}/../${PROJECT}/src/ -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR
RESULT=$?

# Check error
if [ $RESULT -ne 0 ]; then
	cd -
	exit $RESULT
fi

#done
cd -

