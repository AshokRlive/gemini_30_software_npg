/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:Utilities.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   18/09/2013    wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef G3_BASEREUSE_UTILITIES_H_
#define G3_BASEREUSE_UTILITIES_H_
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string>
#include <vector>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define FDVector std::vector<lu_int32_t>

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef enum
{
    FILECRC_ERROR_NONE = 0,
    FILECRC_ERROR_PARAM,    //Invalid parameter
    FILECRC_ERROR_ACCESS,   //error accessing file
    FILECRC_ERROR_READ,     //error reading file
    FILECRC_ERROR_MEMORY,   //not enough memory for operation
    FILECRC_ERROR_LAST
} FILECRC_ERROR;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
static const timeval TIMEOUT_IMMEDIATE = {0,0};

/*
 ******************************************************************************
 * EXPORTED - Classes
 ******************************************************************************
 */

/**
* \brief check if there is available space for a file.
*
* \param Path to file
* \param Required file size
*
* \return Whether there is space available for the file or not.
*/
lu_bool_t checkSpaceAvail(const char *fileNameStr, const lu_uint32_t fsize);

/**
* \brief Checks if a file exists in file system.
*
* \return true if exist, false otherwise
*/
lu_bool_t checkFileExistence(const std::string& fname);

/**
* \brief Gets the size of a file.
*
* \return the size of the file. 0 if the file doesn't exist.
*/
lu_uint32_t getFileSize(const std::string fname);

/*!
 *   \brief Get the CRC of a file
 *
 *   This function uses the following fixed polynomial:
 *   x^32+x^26+x^23+x^22+x^16+x^12+x^11+x^10+x^8+x^7+x^5+x^4+x^2+x+1
 *
 *   \param     filenamePtr  Name of the file to calculate CRC of
 *              crcPtr       Pointer to where CRC is returned
 *
 *   \return    Error code
 */
extern lu_int32_t calcFileCRC(const char *filenamePtr, lu_uint32_t *crcPtr);

/**
 * \brief Implements a read select over the file descriptor set provided
 *
 * \param timeout Time out config if no read operation found
 * \param fileDescriptors Set of file descriptors to check in select
 * \param resultFDs Set of file descriptors ready for read operation
 *
 * \return Number of files ready; 0 when timeout; <0 when error (abs val being errno)
 */
lu_int32_t fileSelect(const timeval timeout, const FDVector fileDescriptors, FDVector resultFDs);

/**
 * \brief Forces content in the file to be flushed ("real" write)
 *
 * \param fileDesc File descriptor of the implied file.
 * \param filePtr Pointer to the file handler of the implied file.
 */
void fileFlush(const lu_int32_t fileDesc);
void fileFlush(FILE* filePtr);

/**
 * \brief Clears the input buffer of a file
 *
 * This function is intended to discard any present data before an operation. For
 * example, before writing a command and expect a reply, we might want to discard
 * previous replies still present in the input buffer.
 *
 * \param fileDesc File descriptor of the implied file.
 * \param filePtr Pointer to the file handler of the implied file.
 * \param timeout Time to allow in each read operation. Optional.
 */
void fileIgnore(const lu_int32_t fileDesc, const timeval timeout = TIMEOUT_IMMEDIATE);
void fileIgnore(FILE* filePtr, const timeval timeout = TIMEOUT_IMMEDIATE);


#endif /* G3_BASEREUSE_UTILITIES_H_ */

/*
 *********************** End of file ******************************************
 */
