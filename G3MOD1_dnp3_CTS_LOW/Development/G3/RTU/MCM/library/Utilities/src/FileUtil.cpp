/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME: Utilities
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       File utilities used for
 *
 *    CURRENT REVISION
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   18/09/13      wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdlib.h>
#include <zlib.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "FileUtil.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define READ_CHUNK_SIZE   1024

#define VARIADIC_NUMARGS(...)  (sizeof((int[]){__VA_ARGS__})/sizeof(int))
/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */

lu_bool_t checkFileExistence(const std::string& fname)
{
    struct stat buffer;
    return (stat (fname.c_str(), &buffer) == 0)?LU_TRUE : LU_FALSE;
}


lu_uint32_t getFileSize(const std::string fname)
{
   struct stat fileSysStatus;
   lu_int32_t result = stat(fname.c_str(), &fileSysStatus);
   if (result == 0)
   {
       /* check if it is a file */
       if( S_ISREG(fileSysStatus.st_mode) )
       {
           return fileSysStatus.st_size;
       }
   }

   return 0;
}


lu_bool_t checkSpaceAvail(const char *fileNameStr,
                          const lu_uint32_t fsize)
{
    /* Check file space needed */
    lu_bool_t freeSpaceAvail = LU_FALSE; //no space available by default
    struct statvfs fileSystemStat;
    if ( statvfs(fileNameStr, &fileSystemStat) == 0 )
    {
        //check if free space is enough
        if( (fileSystemStat.f_bsize * fileSystemStat.f_bavail) > fsize)
        {
            freeSpaceAvail = LU_TRUE;
        }
    }
    return freeSpaceAvail;
}


lu_int32_t calcFileCRC(const char *filenamePtr, lu_uint32_t *crcPtr)
{
    FILE          *fd = NULL;
    lu_uint8_t    *buff = NULL;
    lu_int32_t     n;
    lu_int32_t     ret;


    ret = FILECRC_ERROR_NONE;

    do
    {
        if ((crcPtr == NULL) || (filenamePtr == NULL))
        {
            ret =  FILECRC_ERROR_PARAM;
            break;
        }

        *crcPtr = crc32(0L, Z_NULL, 0);

        if ((fd = fopen(filenamePtr, "r")) == NULL)
        {
            ret =  FILECRC_ERROR_ACCESS;
            break;
        }

        if ((buff = (lu_uint8_t *) malloc(READ_CHUNK_SIZE * sizeof(lu_uint8_t))) == NULL)
        {
            ret =  FILECRC_ERROR_MEMORY;
            break;
        }

        while((n = fread(buff, 1, READ_CHUNK_SIZE, fd)) > 0)
        {
            *crcPtr = crc32(*crcPtr, buff, n);
        }

        // Read error
        if (n < 0)
        {
            ret = FILECRC_ERROR_READ;
        }
    } while(0);

    if(fd != NULL)
        fclose(fd);

    // Release Memory
    if (buff)
    {
        free(buff);
    }

    return ret;
}


lu_int32_t fileSelect(const timeval timeout, const FDVector fileDescriptors, FDVector resultFDs)
{
    fd_set rfds;            //file descriptors set
    lu_int32_t maxFD = 0;   //biggest of them to offer it to select
    lu_int32_t retval;      //Result of select

    struct timeval timeoutLocal = timeout;

    /* Initialise file descriptors set to be used by select */
    /* NOTE: Always check the File Descriptors before select() since they
        * can change at any time
        */
    FD_ZERO(&rfds); //Reset the file descriptors waiting list
    resultFDs.clear();

    /* Add socket file descriptors to waiting list */
    for(FDVector::const_iterator it = fileDescriptors.begin(); it!=fileDescriptors.end(); it++)
    {
        FD_SET(*it, &rfds);
        /* Get maximum file descriptor number */
        maxFD = LU_MAX(maxFD, *it);
    }

    /***** Wait for incoming fileDescriptors data *****/
    retval = select(maxFD + 1, &rfds, NULL, NULL, &timeoutLocal);
    if(retval < 0)
    {
        //select error: errno
        return -errno;
    }
    else if(retval > 0)
    {
        /* No errors. Mark FDs for read */
        for(FDVector::const_iterator it = fileDescriptors.begin(); it!=fileDescriptors.end(); it++)
        {
            if(FD_ISSET(*it, &rfds))
            {
                resultFDs.push_back(*it);
            }
        }
    }
    //else if(retval == 0) {timed out: nothing to do!}
    return retval;
}


void fileFlush(const lu_int32_t fileDesc)
{
    //flush after write
    fsync(fileDesc);
}

void fileFlush(FILE* filePtr)
{
    //flush after write
    fflush(filePtr);
}

void fileIgnore(const lu_int32_t fileDesc, const timeval timeout)
{
    const lu_uint32_t BUFFSIZE = 100;
    lu_char_t buffer[BUFFSIZE];   /* Input buffer */
    FDVector result;
    FDVector fds;
    fds.push_back(fileDesc);

    /* Flush read buffer */
    while(fileSelect(timeout, fds, result) > 0)
    {
        read(fileDesc, buffer, BUFFSIZE);
    }
}

void fileIgnore(FILE* filePtr, const timeval timeout)
{
    lu_int32_t fd = fileno(filePtr);
    if(fd >= 0)
    {
        fileIgnore(fd, timeout);
    }
}

/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
