/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:StringUtil.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   18 Sep 2013     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <ctype.h>
#include <algorithm>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "StringUtil.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */
void toUpper(std::string &str)
{
    std::transform(str.begin(), str.end(), str.begin(), ::toupper);
}

void toLower(std::string &str)
{
    std::transform(str.begin(), str.end(), str.begin(), ::tolower);
}


std::string trim_front(const std::string& source, const bool removeNewLine, const bool removeSpaces)
{
    if(!removeSpaces && !removeNewLine)
    {
        return source;
    }
    std::string toRemove;
    if(removeNewLine)
    {
        toRemove += "\n\r";
    }
    if(removeSpaces)
    {
        toRemove += " \t";
    }
    //Note that we use length() as a max value: substr will take less chars if needed
    std::string ret = source.substr( (source.find_first_not_of(toRemove)), source.length() );
    return ret;
}


std::string trim_back(const std::string& source, const bool removeNewLine, const bool removeSpaces)
{
    if(!removeSpaces && !removeNewLine)
    {
        return source;
    }
    std::string toRemove;
    if(removeNewLine)
    {
        toRemove += "\n\r"; //new lines and carriage returns
    }
    if(removeSpaces)
    {
        toRemove += " \t";  //spaces and tabs
    }
    //Note that we use length() as a max value: substr will take less chars if needed
    std::string ret = source.substr(0, (source.find_last_not_of(toRemove) + 1) );
    return ret;
}


std::string trim_all(const std::string& source, const bool removeNewLine, const bool removeSpaces)
{
    std::string ret;
    ret = trim_front(source, removeNewLine, removeSpaces);
    return trim_back(ret, removeNewLine, removeSpaces);
}


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
