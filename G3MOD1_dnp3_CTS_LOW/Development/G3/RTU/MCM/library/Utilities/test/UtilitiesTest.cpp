/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:UtilitiesTest.cpp
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   18 Sep 2013     wang_p     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Utilities.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Public Methods
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Protected Methods
 ******************************************************************************
 */

void testToUpper()
{
    const std::string TITLE = "testToUpper: ";
    std::string test = "This is a sTring 123";
    std::string expected = "THIS IS A STRING 123";

    std::cout << "String Converted from:\""<<test<<"\"";
    toUpper(test);
    std::cout<<" to:\""<<test<<"\""<<std::endl;

    if(test.compare(expected) != 0)
    {
        std::cerr <<TITLE<< "FAILED" << std::endl;
    }
    else
    {
        std::cout << TITLE<< "SUCCESS" << std::endl;
    }
}

void testToLower()
{
    const std::string TITLE = "testToLower: ";
    std::string test = "ThIS IS A STring 123";
    std::string expected = "this is a string 123";

    std::cout << "String Converted from:\""<<test<<"\"";
    toLower(test);
    std::cout<<" to:\""<<test<<"\""<<std::endl;

    if(test.compare(expected) != 0)
    {
        std::cerr <<TITLE<< "FAILED" << std::endl;
    }
    else
    {
        std::cout << TITLE<< "SUCCESS" << std::endl;
    }
}

void testFileExist()
{
    const std::string TITLE = "testFileExist: ";
    lu_bool_t failed = false;

    std::string name = "MCMUpdater.axf";
    if(checkFileExistence(name) == LU_FALSE)
    {
        std::cout<<name<<"  doesn't exist: "<<std::endl;
        failed = LU_TRUE;
    }

    name = "../update/Slaves/Slave.fw";

    if(checkFileExistence(name) == LU_FALSE)
    {
        std::cout<<name<<"  doesn't exist: "<<std::endl;
        failed = LU_TRUE;
    }

    name = "MCMUpdaternonDFAN.axf";
    if(checkFileExistence(name) == LU_TRUE)
    {
        std::cout<<name<<"  does exist: "<<std::endl;
        failed = LU_TRUE;
    }


    if(failed)
        std::cerr <<TITLE<< "FAILED" << std::endl;
    else
        std::cout << TITLE<< "SUCCESS" << std::endl;
}
/*
 ******************************************************************************
 * Private Methods
 ******************************************************************************
 */
int main()
{
    testToUpper();
    std::cout<<std::endl;
    testToLower();
    std::cout<<std::endl;
    testFileExist();
    return 0;
}



/*
 *********************** End of file ******************************************
 */
