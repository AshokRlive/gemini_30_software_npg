

#include <stdio.h>
#include <syslog.h>
#include <pthread.h>
#include "LogMessage.h"
#include "LogLevelDef.h"

int main(void)
{
    int i = 0;

    logMessageInit(LOG_PERROR, LOG_LOCAL1, "LogTest");

    logMessageFatal(SUBSYSTEM_ID_TEST, "This is a [%s] for [%d] things", "fatal message", i);
    logMessageError(SUBSYSTEM_ID_TEST, "This is a [%s] for [%d] things", "error message", i);
    logMessageWarn(SUBSYSTEM_ID_TEST,  "This is a [%s] for [%d] things", "warning message", i);
    logMessageInfo(SUBSYSTEM_ID_TEST,  "This is a [%s] for [%d] things", "info message", i);
    logMessageDebug(SUBSYSTEM_ID_TEST, "This is a [%s] for [%d] things", "debug message", i);

    printf("Done!\n");

    return 0;
}
