/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision::               $: (Revision of last commit)
 *               $Author::                 $: (Author of last commit)
 *       \date   $Date::                   $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/09/13      andrews_s   Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <pthread.h>
#include <stdarg.h>
#include <syslog.h>
#include <stdio.h>  //snprintf
#include <string.h> //strlen

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "LogMessage.h"
#include "lu_types.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
typedef struct G3ToSysLogMapDef
{
    LOG_LEVEL  lucyLogLevel;
    lu_int32_t sysLogLevel;

} G3ToSysLogMapStr;

/*
    This table is used to map Lucy LOG_LEVELS to Linux's syslog levels
 */
static G3ToSysLogMapStr G3ToSysLogLevelMapping[LOG_LEVEL_LAST] =
{
    { LOG_LEVEL_ERR_FATAL,  LOG_EMERG },
    { LOG_LEVEL_ERR,        LOG_ERR },
    { LOG_LEVEL_WARNING,    LOG_WARNING },
    { LOG_LEVEL_INFO,       LOG_INFO },
    { LOG_LEVEL_DEBUG,      LOG_DEBUG }
};

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

//Added missing function prototype to get rid of compiler warning "Implicit declaration of function"
void vsyslog (int facility_priority, const char *format, va_list arglist);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static lu_int32_t       logMessageOptions;
static lu_int32_t       logMessageFacility; // MCM_LOG_FACILITY

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

void logMessageInit(const lu_int32_t options, const lu_int32_t facility, const lu_char_t* identity)
{
    logMessageOptions  = options;
    logMessageFacility = facility;

    openlog(identity, logMessageOptions, logMessageFacility);
}

void vlogMessage(SUBSYSTEM_ID subSystem, LOG_LEVEL logLevel, const lu_char_t *fmtPtr, va_list argsList)
{
    if (logLevel >= LOG_LEVEL_LAST)
    {
        logLevel = LOG_LEVEL_INFO;
    }

    if (subSystem >= SUBSYSTEM_ID_LAST)
    {
        subSystem = SUBSYSTEM_ID_MAIN;
    }

    /** NOTE: we are using a char array in the stack in order to avoid needing
     *      to free() the memory afterwards, since vsyslog() has a cancellation
     *      point. After cancellation and function exit, the char array is
     *      ensured to be freed.
     *      In addition, no mutex/lock could be used since when vsyslog() is
     *      cancelled because that may cause the lock to never be freed.
     */
    lu_uint32_t formatSize = strlen(SUBSYSTEM_ID_ToSTRING(subSystem)) + 5 + strlen(fmtPtr);
    lu_char_t formatPtr[formatSize];
    snprintf(formatPtr, formatSize, "[%s]: %s", SUBSYSTEM_ID_ToSTRING(subSystem), fmtPtr);
#ifdef USE_SYSLOG
    vsyslog(G3ToSysLogLevelMapping[logLevel].sysLogLevel, formatPtr, argsList);
#else
    fprintf(stdout, formatPtr, argsList);
#endif
}

void logMessageFatal(SUBSYSTEM_ID subSystem, const lu_char_t *fmtPtr, ...)
{
    va_list argsList;
    va_start (argsList, fmtPtr);

    vlogMessage(subSystem, LOG_LEVEL_ERR_FATAL, fmtPtr, argsList);

    va_end (argsList);
}

void logMessageError(SUBSYSTEM_ID subSystem, const lu_char_t *fmtPtr, ...)
{
    va_list argsList;
    va_start (argsList, fmtPtr);

    vlogMessage(subSystem, LOG_LEVEL_ERR, fmtPtr, argsList);

    va_end (argsList);
}

void logMessageWarn(SUBSYSTEM_ID subSystem, const lu_char_t *fmtPtr, ...)
{
    va_list argsList;
    va_start (argsList, fmtPtr);

    vlogMessage(subSystem, LOG_LEVEL_WARNING, fmtPtr, argsList);

    va_end (argsList);
}

void logMessageInfo(SUBSYSTEM_ID subSystem, const lu_char_t *fmtPtr, ...)
{
    va_list argsList;
    va_start (argsList, fmtPtr);

    vlogMessage(subSystem, LOG_LEVEL_INFO, fmtPtr, argsList);

    va_end (argsList);
}

void logMessageDebug(SUBSYSTEM_ID subSystem, const lu_char_t *fmtPtr, ...)
{
    va_list argsList;
    va_start (argsList, fmtPtr);

    vlogMessage(subSystem, LOG_LEVEL_DEBUG, fmtPtr, argsList);

    va_end (argsList);
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
