/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision::               $: (Revision of last commit)
 *               $Author::                 $: (Author of last commit)
 *       \date   $Date::                   $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/09/13      andrews_s   Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _C_LOGMESSAGE_INCLUDED
#define _C_LOGMESSAGE_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <syslog.h>
#include <stdarg.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "LogLevelDef.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */
/*!
 ******************************************************************************
 *   \brief Initialise the logMessage
 *
 *   This sets the syslog options and initialises the supplied mutex
 *
 *   \param mutexPtr    Pointer to a mutex
 *          options     options as per man 3 syslog
 *          facility    facility as per man 3 syslog
 *
 *   \return    N/A
 *
 ******************************************************************************
 */
void logMessageInit(const lu_int32_t options, const lu_int32_t facility, const lu_char_t* identity);

/*!
 ******************************************************************************
 *   \brief Log a message to syslog (using a va_list)
 *
 *   Logs a message to syslog as the supplied level for the supplied subsystem.
 *
 *   Thread-safe function.
 *
 *   \param subSystem   SubSystem as defined in LogLevelDef.h
 *          logLevel    G3 Log Level (mapped to SysLog log level)
 *          fmtPtr      Format string for the va_list
 *          argsList    va_list of variables
 *
 *   \return    N/A
 *
 ******************************************************************************
 */
void vlogMessage(const SUBSYSTEM_ID subSystem, const LOG_LEVEL logLevel, const lu_char_t *fmtPtr, va_list argsList);

/*!
 ******************************************************************************
 *   \brief Log a message to syslog
 *
 *   Logs a Fatal level Message to syslog
 *
 *   \param subSystem   SubSystem as defined in LogLevelDef.h
 *          fmtPtr      printf() style format string
 *          ...         printf() style variables
 *
 *   \return    N/A
 *
 ******************************************************************************
 */
void logMessageFatal(SUBSYSTEM_ID subSystem, const lu_char_t *fmtPtr, ...);


/*!
 ******************************************************************************
 *   \brief Log a message to syslog
 *
 *   Logs a n Error level Message to syslog
 *
 *   \param subSystem   SubSystem as defined in LogLevelDef.h
 *          msgPtr      The message to log
 *
 *   \return    N/A
 *
 ******************************************************************************
 */
void logMessageError(SUBSYSTEM_ID subSystem, const lu_char_t *fmtPtr, ...);

/*!
 ******************************************************************************
 *   \brief Log a message to syslog
 *
 *   Logs a Warning level Message to syslog
 *
 *   \param subSystem   SubSystem as defined in LogLevelDef.h
 *          fmtPtr      printf() style format string
 *          ...         printf() style variables
 *
 *   \return    N/A
 *
 ******************************************************************************
 */
void logMessageWarn(SUBSYSTEM_ID subSystem, const lu_char_t *fmtPtr, ...);

/*!
 ******************************************************************************
 *   \brief Log a message to syslog
 *
 *   Logs an Info level Message to syslog
 *
 *   \param subSystem   SubSystem as defined in LogLevelDef.h
 *          fmtPtr      printf() style format string
 *          ...         printf() style variables
 *
 *   \return    N/A
 *
 ******************************************************************************
 */
void logMessageInfo(SUBSYSTEM_ID subSystem, const lu_char_t *fmtPtr, ...);

/*!
 ******************************************************************************
 *   \brief Log a message to syslog
 *
 *   Logs a Debug level Message to syslog
 *
 *   \param subSystem   SubSystem as defined in LogLevelDef.h
 *          fmtPtr      printf() style format string
 *          ...         printf() style variables
 *
 *   \return    N/A
 *
 ******************************************************************************
 */
void logMessageDebug(SUBSYSTEM_ID subSystem, const lu_char_t *fmtPtr, ...);

#ifdef __cplusplus
}
#endif


#endif /* _C_LOGMESSAGE_INCLUDED */

/*
 *********************** End of file ******************************************
 */
