cmake_minimum_required(VERSION 2.8)

add_library( Zip STATIC
             Zip.c
           ) 

ADD_DEPENDENCIES(Zip globalHeaders)

include_directories(../include/)

# Generate Doxygen documentation
#gen_doxygen("Zip" "")
