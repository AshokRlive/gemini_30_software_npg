/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:zip.c
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   9 Dec 2013     andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */



/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <stdio.h>
#include <stdlib.h>
#include <zlib.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Zip.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define READ_CHUNK_SIZE   1024

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */





/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
lu_int32_t unzipFileToBuffer(const char *zippedFilePtr, char *buffPtr, lu_int32_t buffLen)
{
    gzFile         gzFile;
    lu_int32_t     ret;
    lu_int32_t     chunkReadSize;
    lu_int32_t     buffReadSize;
    lu_int32_t     readChunkSize;
    char          *p = buffPtr;


    ret = ZIP_ERROR_NONE;

    if ((buffPtr == NULL) || (zippedFilePtr == NULL))
    {
        return ZIP_ERROR_NULL_BUFFER;
    }

    do
    {
        if ((gzFile = gzopen(zippedFilePtr, "rb")) == NULL)
        {
            ret = ZIP_ERROR_FILE_OPEN;
            break;
        }

        // If buffLen is 0 we will read the file in chunks assuming the supplied
        // buffer is large enough (useful for writing to C++ String)
        readChunkSize = READ_CHUNK_SIZE;

        // Reduce readChunkSize if it's bigger than the supplied buffer
        if ((buffLen > 0) && (buffLen < readChunkSize))
        {
            readChunkSize = buffLen;
        }

        buffReadSize = 0;

        // Loop, expanding the gzip file to the supplied buffer
        while((chunkReadSize = gzread(gzFile, p, readChunkSize)) > 0)
        {
            buffReadSize += chunkReadSize;
            if ((buffLen > 0) && (buffReadSize > buffLen))
            {
                ret = ZIP_ERROR_BUFFER_TOO_SMALL;
                break;
            }

            // Adjust Chunk Size as appropriate
            if ((buffLen > 0) && (((buffLen - buffReadSize) < READ_CHUNK_SIZE)))
            {
                readChunkSize = (buffLen - buffReadSize);
            }

            // Reposition the buffer pointer
            p += chunkReadSize;
        }

        // Read error
        if (chunkReadSize < 0)
        {
            ret = ZIP_ERROR_READ;
        }
        else
        {
            ret = buffReadSize;
        }
    } while(0);

    gzclose(gzFile);

    return ret;
}



lu_int32_t unzipFile(const char *zippedFilePtr, const char *unzippedFilePtr)
{
    gzFile         gzFile;
    FILE          *unzipFd = NULL;
    lu_uint8_t    *buff = NULL;
    lu_int32_t     chunkReadSize;
    lu_int32_t     ret;


    ret = ZIP_ERROR_NONE;

    if ((unzippedFilePtr == NULL) || (zippedFilePtr == NULL))
    {
        return ZIP_ERROR_NULL_FILE;
    }

    do
    {
        if ((gzFile = gzopen(zippedFilePtr, "rb")) == NULL)
        {
            ret =  ZIP_ERROR_FILE_OPEN;
            break;
        }

        if ((unzipFd = fopen(unzippedFilePtr, "w")) == NULL)
        {
            ret =  ZIP_ERROR_FILE_OPEN;
            break;
        }

        if ((buff = (lu_uint8_t *) malloc(READ_CHUNK_SIZE * sizeof(lu_uint8_t))) == NULL)
        {
            ret =  ZIP_ERROR_NO_MEMORY;
            break;
        }

        while((chunkReadSize = gzread(gzFile, buff, READ_CHUNK_SIZE)) > 0)
        {
            // Unzip the buffer and append to the unzippedFilePtr file
            if (fwrite(buff, 1, chunkReadSize, unzipFd) == 0)
            {
                ret = ZIP_ERROR_WRITE;
                break;
            }
        }

        // Read error
        if (chunkReadSize < 0)
        {
            ret = ZIP_ERROR_READ;
        }
    } while(0);

    gzclose(gzFile);
    fclose(unzipFd);

    // Release Memory
    if (buff)
    {
        free(buff);
    }

    return ret;
}


lu_int32_t zipFile(const char *originalFilePtr, const char *zippedFilePtr)
{
    gzFile         gzFile;
    FILE          *unzipFd = NULL;
    lu_uint8_t    *buff = NULL;
    lu_int32_t     chunkReadSize;
    lu_int32_t     ret;


    ret = ZIP_ERROR_NONE;

    if ((originalFilePtr == NULL) || (zippedFilePtr == NULL))
    {
        return ZIP_ERROR_NULL_FILE;
    }

    do
    {
        if ((gzFile = gzopen(zippedFilePtr, "wb")) == NULL)
        {
            ret =  ZIP_ERROR_FILE_OPEN;
            break;
        }

        if ((unzipFd = fopen(originalFilePtr, "r")) == NULL)
        {
            ret =  ZIP_ERROR_FILE_OPEN;
            break;
        }

        if ((buff = (lu_uint8_t *) malloc(READ_CHUNK_SIZE * sizeof(lu_uint8_t))) == NULL)
        {
            ret =  ZIP_ERROR_NO_MEMORY;
            break;
        }

        while((chunkReadSize = fread(buff, 1, READ_CHUNK_SIZE, unzipFd)) > 0)
        {
            // Zip the buffer and append to the zip file
            if (gzwrite(gzFile, buff, chunkReadSize) == 0)
            {
                ret = ZIP_ERROR_WRITE;
                break;
            }
        }

        // Read error
        if (chunkReadSize < 0)
        {
            ret = ZIP_ERROR_READ;
        }
    } while (0);

    fclose(unzipFd);
    gzclose(gzFile);

    // Release Memory
    if (buff)
    {
        free(buff);
    }

    return ret;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
