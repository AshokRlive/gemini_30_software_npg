#include <stdio.h>
#include "lu_types.h"
#include "Zip.h"

#define BIG_BUFF_LEN    204800

const char originalGZFile[] = "/usr/local/gemini/application/G3Config.xml.gz";
const char unzippedFile[] = "/usr/local/gemini/application/G3Config.xml.unzipped";
const char zippedFile[] = "/usr/local/gemini/application/G3Config.xml.zipped";

const char crcFile[] = "/usr/local/gemini/application/MCMBoard.axf";

char BIGBuff[204800];


int main(void)
{

//    lu_uint32_t crc;
//    if (calcFileCRC(crcFile, &crc) == ZIP_ERROR_NONE)
//    {
//        printf("CRC of [%s] is [0x%08X]\n", crcFile, crc);
//    }

    if (unzipFile(originalGZFile, unzippedFile) == 0)
    {
        printf("Successfully unzipped [%s] to [%s]\n", originalGZFile, unzippedFile);
    }
    else
    {
        printf("Unable to unzip [%s] to [%s]\n", originalGZFile, unzippedFile);
    }

    if (zipFile(unzippedFile, zippedFile) == 0)
    {
        printf("Successfully zipped [%s] to [%s]\n", unzippedFile, zippedFile);
    }
    else
    {
        printf("Unable to zip [%s] to [%s]\n", unzippedFile, zippedFile);
    }


    if (unzipFileToBuffer(zippedFile, BIGBuff, BIG_BUFF_LEN) == 0)
    {
        printf("Successfully unzipped [%s] to buffer\n", unzippedFile);
    }
    else
    {
        printf("Unable to unzipped [%s] to buffer\n", unzippedFile);
    }

    return 0;
}

/*
 *********************** End of file ******************************************
 */
