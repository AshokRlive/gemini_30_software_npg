/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:zip.h
 *    FILE TYPE: c++ source
 *
 *    DESCRIPTION:
 *       \brief
 *       
 *
 *    CURRENT REVISION:
 *
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   9 Dec 2013     andrews_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef ZIP_H_
#define ZIP_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <zlib.h>   //To allow use of gzFile type

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

typedef enum {
    ZIP_ERROR_NONE             = 0,
    ZIP_ERROR_NULL_FILE        = -1,
    ZIP_ERROR_NULL_BUFFER      = -2,
    ZIP_ERROR_BUFFER_TOO_SMALL = -3,
    ZIP_ERROR_FILE_OPEN        = -4,
    ZIP_ERROR_NO_MEMORY        = -5,
    ZIP_ERROR_READ             = -6,
    ZIP_ERROR_WRITE            = -7,
    ZIP_ERROR_LAST
} ZIP_ERROR;


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Unzip a file to a buffer
 *
 *   Detailed description
 *
 *   If BuffLen is set to 0 this function does NO BOUNDS CHECKING and relies
 *   on the buffer being large enough to house the expanded file!!!
 *
 *   \param     zippedFilePtr      Name of the zip file to unzip
 *              buffPtr            Ptr to buffer
 *              buffLen            Size of buffer or 0 for no checking
 *
 *   \return    Number of bytes extracted or ZIP_ERROR
 *
 ******************************************************************************
 */
extern lu_int32_t unzipFileToBuffer(const char* zippedFilePtr, char *buffPtr, lu_int32_t buffLen);

/*!
 ******************************************************************************
 *   \brief Unzip a file
 *
 *   Detailed description
 *
 *   Note that this function will create a new file and will not delete
 *   the zipped file.
 *
 *   \param     zippedFilePtr      Name of the zip file to unzip
 *              unzippedFilePtr    Name of the unzipped file
 *
 *
 *   \return    ZIP_ERROR
 *
 ******************************************************************************
 */
extern lu_int32_t unzipFile(const char *zippedFilePtr, const char *unzippedFilePtr);

/*!
 ******************************************************************************
 *   \brief Zip a file
 *
 *   Detailed description

 *   Note that this function will create a new file and will not delete
 *   the original file.
 *
 *   \param     originalFilePtr  Name of the file to zip
 *              zippedFilePtr    Name of the zip file to generate
 *
 *
 *   \return    ZIP_ERROR
 *
 ******************************************************************************
 */
extern lu_int32_t zipFile(const char *originalFilePtr, const char *zippedFilePtr);


#ifdef __cplusplus
}
#endif

#endif /* ZIP_H_ */

/*
 *********************** End of file ******************************************
 */
