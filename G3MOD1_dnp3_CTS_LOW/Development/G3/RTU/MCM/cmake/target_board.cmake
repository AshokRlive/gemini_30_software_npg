#------------------------------TARGET BOARD --------------------------------------------
set(TARGET_BOARD_G3_MCM_TX25        "TARGET_BOARD_G3_MCM_TX25" CACHE INTERNAL "")
set(TARGET_BOARD_G3_SYSTEMPROC      "TARGET_BOARD_G3_SYSTEMPROC" CACHE INTERNAL "")

# Set board type
set(CONFIG_TARGET_BOARD  ${TARGET_BOARD_G3_MCM_TX25} CACHE STRING "Selected target board")
message(STATUS "Configured target board: ${CONFIG_TARGET_BOARD}")
message(STATUS "Tips: change configured board with option: -DCONFIG_TARGET_BOARD, e.g.\n"
"    $: cmake -DCONFIG_TARGET_BOARD=TARGET_BOARD_G3_SYSTEMPROC")

# Set board specific options
if(${CONFIG_TARGET_BOARD} STREQUAL ${TARGET_BOARD_G3_MCM_TX25})
    set(CROSS_COMPILE ON)
    set(MCM_TOOLCHAIN_FILE "${CMAKE_G3_PATH_ROOT}/RTU/MCM/cmake/GCC_TX25_DI_toolchain.cmake")
    set(MCM_GCC_FLAGS_FILE "${CMAKE_G3_PATH_ROOT}/RTU/MCM/cmake/GCC_TX25_app_flags.cmake")
    add_definitions(-DUSE_SYSLOG)
elseif(${CONFIG_TARGET_BOARD} STREQUAL ${TARGET_BOARD_G3_SYSTEMPROC})
    add_definitions(-DTARGET_BOARD_G3_SYSTEMPROC)
    add_definitions(-DTMW_LINUX_TARGET)
    set(CROSS_COMPILE OFF)
    set(MCM_GCC_FLAGS_FILE "${CMAKE_G3_PATH_ROOT}/RTU/MCM/cmake/GCC_HOST_app_flags.cmake")
else()
    message(FATAL_ERROR "Unknown board:${CONFIG_TARGET_BOARD}, select one of supported boards: "
    "${TARGET_BOARD_G3_MCM_TX25},${TARGET_BOARD_G3_SYSTEMPROC}")
endif()
message(STATUS "CROSS_COMPILE:${CROSS_COMPILE}")
message(STATUS "MCM_TOOLCHAIN_FILE:${MCM_TOOLCHAIN_FILE}")
message(STATUS "MCM_GCC_FLAGS_FILE:${MCM_GCC_FLAGS_FILE}")


