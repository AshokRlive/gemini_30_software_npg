# The project that needs to generate headers from XML/SVN should include this cmake file.
# How to use:
#  1. Make sure the required varible "CMAKE_G3_GENERATED_HEADER_BUILD_PATH" is defined
#  2. Include this cmake file in your CMakeLists.txt


# Set the path of the header files generated from XMLs, svn ,etc
if(NOT DEFINED CMAKE_G3_GENERATED_HEADER_BUILD_PATH)
message(SEND_ERROR "!!!Please set the path:CMAKE_G3_GENERATED_HEADER_BUILD_PATH in your project CMakeLists.txt")
endif()

set(CMAKE_G3_GENERATED_HEADER_INCLUDE 
    "${CMAKE_G3_GENERATED_HEADER_BUILD_PATH}/"
    "${CMAKE_G3_GENERATED_HEADER_BUILD_PATH}/SysAlarm/"
    "${CMAKE_G3_GENERATED_HEADER_BUILD_PATH}/ModuleProtocol/"
    "${CMAKE_G3_GENERATED_HEADER_BUILD_PATH}/NVRAMDef/"
    )
# Build generated directory    
add_subdirectory(${CMAKE_G3_GENERATED_HEADER_BUILD_PATH} common/includeGenerated/)

# Include all generated header 
include_directories(${CMAKE_G3_GENERATED_HEADER_INCLUDE}/)

