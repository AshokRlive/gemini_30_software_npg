if(${CMAKE_INSTALL_PREFIX} STREQUAL "/usr/local")
	message(WARNING "The CMAKE_INSTALL_PREFIX has not been specified! Default prefix(e.g. /usr/local/) will be applied")
endif()

install(TARGETS ${EXE_NAME} RUNTIME DESTINATION ./ )