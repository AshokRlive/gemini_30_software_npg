# The project that is built with MCM common components should include this cmake file.
# How to use:
#  1. Make sure those varibles are defined: CMAKE_G3_PATH_MCM_COMMONLIB, CMAKE_G3_PATH_RTU_COMMON_LIBRARY
#  2. Include this cmake file in your CMakeLists.txt
#  3. Link your EXE to CMAKE_G3_PATH_MCM_COMMONLIB_LINK. 
include_directories(SYSTEM ${CMAKE_INCLUDE_PATH})

# Set the path of the header files generated from XMLs, svn ,etc
if(NOT DEFINED CMAKE_G3_PATH_MCM_COMMONLIB)
message(SEND_ERROR "!!!Please set the CMAKE_G3_PATH_MCM_COMMONLIB in your project CMakeLists.txt")
endif()

if(NOT DEFINED CMAKE_G3_PATH_RTU_MCM_LIBRARY)
message(SEND_ERROR "!!!Please set the CMAKE_G3_PATH_RTU_MCM_LIBRARY in your project CMakeLists.txt")
endif()

set(CMAKE_G3_PATH_MCM_COMMONLIB_INCLUDE
        "${CMAKE_G3_PATH_MCM_COMMONLIB}/BaseReuse/include"
        "${CMAKE_G3_PATH_MCM_COMMONLIB}/Logger/include"
        "${CMAKE_G3_PATH_MCM_COMMONLIB}/MCMIOMap/include"
        "${CMAKE_G3_PATH_MCM_COMMONLIB}/ModuleProtocol/include"
        "${CMAKE_G3_PATH_MCM_COMMONLIB}/MemoryManager/include"
        "${CMAKE_G3_PATH_MCM_COMMONLIB}/MonitorProtocolHandler/include"
        "${CMAKE_G3_PATH_MCM_COMMONLIB}/ConfigurationToolHandler/include"
        "${CMAKE_G3_PATH_MCM_COMMONLIB}/CANFraming/include"
        "${CMAKE_G3_PATH_MCM_COMMONLIB}/TimeManager/include"
        "${CMAKE_G3_PATH_MCM_COMMONLIB}/CommandLineInterpreter/include"
        "${CMAKE_G3_PATH_MCM_COMMONLIB}/LEDMirror/include"
        "${CMAKE_G3_PATH_MCM_COMMONLIB}/RTUInfo/include"
    )

# Include all common lib header 
include_directories(${CMAKE_G3_PATH_MCM_COMMONLIB_INCLUDE})
include_directories(${CMAKE_G3_PATH_COMMON_LIBRARY}/util/include)

# Build all common lib components
add_subdirectory(${CMAKE_G3_PATH_RTU_MCM_LIBRARY}/LogMessage/src/ common/LogMessage/) # Required by Logger component.
add_subdirectory(${CMAKE_G3_PATH_MCM_COMMONLIB}/BaseReuse/src BaseReuse)
add_subdirectory(${CMAKE_G3_PATH_MCM_COMMONLIB}/Logger/src Logger)
add_subdirectory(${CMAKE_G3_PATH_MCM_COMMONLIB}/ConfigurationToolHandler/src AbstractCTHandler)
add_subdirectory(${CMAKE_G3_PATH_MCM_COMMONLIB}/CommandLineInterpreter/src CommandLineInterpreter)
add_subdirectory(${CMAKE_G3_PATH_MCM_COMMONLIB}/MonitorProtocolHandler/src MonitorCommunication)
add_subdirectory(${CMAKE_G3_PATH_MCM_COMMONLIB}/TimeManager/src TimeManager)
add_subdirectory(${CMAKE_G3_PATH_MCM_COMMONLIB}/CANFraming/src CANFraming)
add_subdirectory(${CMAKE_G3_PATH_MCM_COMMONLIB}/MCMIOMap/src MCMIOMap)
add_subdirectory(${CMAKE_G3_PATH_MCM_COMMONLIB}/MemoryManager/src IMemoryManager)
add_subdirectory(${CMAKE_G3_PATH_MCM_COMMONLIB}/LEDMirror/src LEDMirror)
add_subdirectory(${CMAKE_G3_PATH_MCM_COMMONLIB}/RTUInfo/src RTUInfo)

# Define the link list which will be used by the current project
set(CMAKE_G3_PATH_MCM_COMMONLIB_LINK
     # NOTE: TOP COMPONENTS DEPENDS ON LOWERS IN THIS LIST
     CommandLineInterpreter
     MonitorCommunication
     CANFraming
     RTUInfo
     LEDMirror
     IMemoryManager
     MCMIOMap
     AbstractCTHandler
     Logger
     TimeManager
     LogMessage
     BaseReuse
 )

