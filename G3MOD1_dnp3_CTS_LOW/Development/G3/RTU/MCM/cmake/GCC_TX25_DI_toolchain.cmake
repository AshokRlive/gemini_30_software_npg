# This cmake file is for the Direct Insight Compiler

# this one is important
SET(CMAKE_SYSTEM_NAME Linux) #Linux, Windows, and Darwin for Mac OS X 
#this one not so much
SET(CMAKE_SYSTEM_VERSION 1)

# specify the cross compiler
SET(CMAKE_C_COMPILER   /usr/local/arm/cross-gcc-4.5.3-armv5te-soft/x86_64-pc-linux-gnu/bin/arm-926ejs-linux-gnueabi-gcc)
SET(CMAKE_CXX_COMPILER /usr/local/arm/cross-gcc-4.5.3-armv5te-soft/x86_64-pc-linux-gnu/bin/arm-926ejs-linux-gnueabi-g++)

SET(CMAKE_SIZE /usr/local/arm/cross-gcc-4.5.3-armv5te-soft/x86_64-pc-linux-gnu/bin/arm-926ejs-linux-gnueabi-size)

# where is the target environment 
SET(CMAKE_FIND_ROOT_PATH  /usr/local/arm/cross-gcc-4.5.3-armv5te-soft/x86_64-pc-linux-gnu/arm-926ejs-linux-gnueabi/sys-root/)

# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

