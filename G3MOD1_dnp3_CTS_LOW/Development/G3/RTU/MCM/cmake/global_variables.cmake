#Set platform operating system
SET (CMAKE_SYSTEM_NAME Linux)

#Enable shared library
SET_PROPERTY(GLOBAL PROPERTY TARGET_SUPPORTS_SHARED_LIBS TRUE)

# Set the name for the executable
SET (EXE_NAME ${PROJECT_NAME}.axf)

# Set the name for the firmware binary file
SET (BIN_NAME ${PROJECT_NAME}.bin)

# Set the name of the MAP file
SET (MAP_NAME ${PROJECT_NAME}.map)

#-------------------------------------------------------

# Set project $CMAKE_PROJECT_NAME specific variables -- 
# Note: !!! CMAKE_G3_PATH_ROOT should be defined in the main CMakeLists.txt !!!

if(NOT DEFINED CMAKE_G3_PATH_ROOT)
    message(" XX - Warning!!!! G3 root path variable NOT defined when calling global_variables.cmake!!!")
endif(NOT DEFINED CMAKE_G3_PATH_ROOT)

set(CMAKE_G3_PATH_COMMON_INCLUDE     
     "${CMAKE_G3_PATH_ROOT}/common/include/"
     "${CMAKE_G3_PATH_ROOT}/common/include/SysAlarm/"
     "${CMAKE_G3_PATH_ROOT}/common/include/ModuleProtocol/"
     "${CMAKE_G3_PATH_ROOT}/common/include/NVRAMDef/"
     )
set(CMAKE_G3_PATH_COMMON_LIBRARY      "${CMAKE_G3_PATH_ROOT}/common/library/")
set(CMAKE_G3_PATH_RTU_COMMON_INCLUDE  "${CMAKE_G3_PATH_ROOT}/RTU/common/include/")

set(CMAKE_G3_PATH_MCM                 "${CMAKE_G3_PATH_ROOT}/RTU/MCM/")
set(CMAKE_G3_PATH_MCM_CMAKE           "${CMAKE_G3_PATH_MCM}/cmake/")
set(CMAKE_G3_PATH_MCM_THIRDPARTY      "${CMAKE_G3_PATH_MCM}/thirdParty/")
set(CMAKE_G3_PATH_MCM_COMMONLIB       "${CMAKE_G3_PATH_MCM}/common/")
set(CMAKE_G3_PATH_RTU_MCM_LIBRARY     "${CMAKE_G3_PATH_MCM}/library/")

#-------------------------------------------------------

# Add a file to the list of file removed in the clean command.
macro (clean_file file)
    SET (CLEAN_FILE_LIST "${CLEAN_FILE_LIST};${file}")
    set_directory_properties(PROPERTIES ADDITIONAL_MAKE_CLEAN_FILES "${CLEAN_FILE_LIST}")
endmacro (clean_file)

# Clean map file
clean_file(${MAP_NAME})

# Create binary file from executable file. Also print size of the executable
macro (generate_bin)
    add_custom_command( TARGET ${EXE_NAME} POST_BUILD
                        COMMAND ${CMAKE_OBJCOPY} -O binary ${EXE_NAME} ${BIN_NAME} &&
                                ${CMAKE_SIZE} ${EXE_NAME}
                      )
    clean_file(${BIN_NAME})
endmacro (generate_bin)

macro(check_lib_found libvar)
if(EXISTS ${libvar})
  message(STATUS "Found libary: ${libvar}")
else()
  message(FATAL_ERROR "Missing library: ${libvar}")
endif()
endmacro(check_lib_found)