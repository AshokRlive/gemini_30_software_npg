# Common FLAGS
# Options:
#   11/09/2013: -Wno-psabi added to get rid of GCC message "note: the mangling of 'va_list' has changed in GCC 4.4" 
#   26/06/2014: -pedantic removed for C++ to get rid of GCC messages when Non-standard extensions used -- in particular dbg.h
#               Please note that this option only gives warnings about non-compliance of the standard, but does not
#               affect the functionality of the code. These warnings are useful only for portability -- in the case
#               of porting the code, could be enabled to check potential problems.
SET (CMAKE_C_COMMON_FLAGS "-fmessage-length=0 -fdiagnostics-show-option -fsigned-char -msoft-float -g3 -std=c99 -Wall -pedantic -Wextra -Wstrict-prototypes -Wmissing-prototypes -Wmissing-declarations -Wno-long-long -Wno-variadic-macros -mlittle-endian -D_XOPEN_SOURCE=600 -D_SVID_SOURCE")
SET (CMAKE_CXX_COMMON_FLAGS "-fmessage-length=0 -fdiagnostics-show-option -fsigned-char -msoft-float -g3 -std=gnu++98 -Wall -Wextra -Wno-long-long -Wno-variadic-macros -Wno-psabi -mlittle-endian -D_XOPEN_SOURCE=600 -D_SVID_SOURCE")

# Common Linker FLAGS
SET (CMAKE_COMMON_EXE_LINKER_FLAGS "-Xlinker -Map=${PROJECT_NAME}.map")

# Debug FLAGS
SET(CMAKE_C_FLAGS_DEBUG "-O0 ${CMAKE_C_COMMON_FLAGS}")
SET(CMAKE_CXX_FLAGS_DEBUG "-O0 ${CMAKE_CXX_COMMON_FLAGS}")
SET(CMAKE_EXE_LINKER_FLAGS_DEBUG  "${CMAKE_COMMON_EXE_LINKER_FLAGS}")

# Release FLAGS
SET(CMAKE_C_FLAGS_RELEASE "-O2 ${CMAKE_C_COMMON_FLAGS}")
SET(CMAKE_CXX_FLAGS_RELEASE "-O2 ${CMAKE_CXX_COMMON_FLAGS}")
SET(CMAKE_EXE_LINKER_FLAGS_RELEASE  "${CMAKE_COMMON_EXE_LINKER_FLAGS}")


if(${CMAKE_BUILD_TYPE} STREQUAL "Release")
add_definitions(-DNDEBUG)
endif(${CMAKE_BUILD_TYPE} STREQUAL "Release")
