# Common C FLAGS
SET (CMAKE_C_COMMON_FLAGS "-fmessage-length=0 -fdiagnostics-show-option -fsigned-char -g3 -std=c99 -Wall -pedantic -Wextra -Wstrict-prototypes -Wmissing-prototypes -Wmissing-declarations -Wno-long-long -Wno-variadic-macros  -D_XOPEN_SOURCE=600 -D_SVID_SOURCE")
SET (CMAKE_CXX_COMMON_FLAGS "-fmessage-length=0 -fdiagnostics-show-option -fsigned-char -g3 -std=gnu++98 -Wall -pedantic -Wextra -Wno-long-long -Wno-variadic-macros -Wno-psabi -D_XOPEN_SOURCE=600 -D_SVID_SOURCE")

# Common Linker FLAGS
#SET (CMAKE_COMMON_EXE_LINKER_FLAGS "-Xlinker -Map=${PROJECT_NAME}.map")

# Debug FLAGS
SET(CMAKE_C_FLAGS_DEBUG "-O0 ${CMAKE_C_COMMON_FLAGS}")
SET(CMAKE_CXX_FLAGS_DEBUG "-O0 ${CMAKE_CXX_COMMON_FLAGS}")
SET(CMAKE_EXE_LINKER_FLAGS_DEBUG  "${CMAKE_COMMON_EXE_LINKER_FLAGS}")

# Release FLAGS
SET(CMAKE_C_FLAGS_RELEASE "-O2 ${CMAKE_C_COMMON_FLAGS}")
SET(CMAKE_CXX_FLAGS_RELEASE "-O2 ${CMAKE_CXX_COMMON_FLAGS}")
SET(CMAKE_EXE_LINKER_FLAGS_RELEASE  "${CMAKE_COMMON_EXE_LINKER_FLAGS}")
