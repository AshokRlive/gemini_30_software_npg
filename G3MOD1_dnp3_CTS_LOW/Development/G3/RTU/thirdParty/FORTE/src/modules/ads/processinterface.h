/*******************************************************************************
 * Copyright (c) 2018 TU Wien/ACIN
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Martin Melik-Merkumians - initial contribution
 *******************************************************************************/

#ifndef SRC_MODULES_ADS_PROCESSINTERFACE_H_
#define SRC_MODULES_ADS_PROCESSINTERFACE_H_

#include "CAdsProcessInterface.h"

typedef forte::ads::CAdsProcessInterface CProcessInterface;

#endif /* SRC_MODULES_ADS_PROCESSINTERFACE_H_ */
