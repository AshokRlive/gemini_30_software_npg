<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE FBType SYSTEM "http://www.holobloc.com/xml/LibraryElement.dtd">
<FBType Comment="Function block for updating the value of a DIGITAL G3 output" Name="G3OutputD">
  <Identification ApplicationDomain="G3" Description="This function block is copied from G3DOutput but with default value to be used when intialised." Standard="61499-2"/>
  <VersionInfo Author="wang_p" Date="2017-07-13" Organization="Lucy Electric" Version="1.0"/>
  <InterfaceList>
    <EventInputs>
      <Event Comment="Initialization request" Name="INIT" Type="Event">
        <With Var="QI"/>
        <With Var="NAME"/>
        <With Var="TYPE"/>
        <With Var="DEFAULT_INIT"/>
        <With Var="DEFAULT_VALUE"/>
      </Event>
      <Event Comment="Normal execution request" Name="REQ" Type="Event">
        <With Var="QI"/>
        <With Var="ID"/>
        <With Var="VALUE"/>
      </Event>
    </EventInputs>
    <EventOutputs>
      <Event Comment="Initialization confirm" Name="INITO" Type="Event">
        <With Var="QO"/>
        <With Var="STATUS"/>
      </Event>
      <Event Comment="Execution confirm" Name="CNF" Type="Event">
        <With Var="QO"/>
        <With Var="STATUS"/>
      </Event>
    </EventOutputs>
    <InputVars>
      <VarDeclaration Comment="Input event qualifier" Name="QI" Type="BOOL"/>
      <VarDeclaration Comment="Output name" Name="NAME" Type="WSTRING"/>
      <VarDeclaration Comment="[Analogue, Binary, DoubleBinary, Counter]" InitialValue="Binary" Name="TYPE" Type="STRING"/>
      <VarDeclaration Comment="Output identifier" Name="ID" Type="UINT"/>
      <VarDeclaration Name="VALUE" Type="DINT"/>
      <VarDeclaration Comment="Indicates if initialise output with the DEFAULT_VALUE" Name="DEFAULT_INIT" Type="BOOL"/>
      <VarDeclaration Comment="The default output value to be initialised" Name="DEFAULT_VALUE" Type="DINT"/>
    </InputVars>
    <OutputVars>
      <VarDeclaration Comment="Output event qualifier" Name="QO" Type="BOOL"/>
      <VarDeclaration Comment="Service Status" Name="STATUS" Type="WSTRING"/>
    </OutputVars>
  </InterfaceList>
  <Service Comment="Function block for updating the value of a DIGITAL G3 output" LeftInterface="APPLICATION" RightInterface="RESOURCE">
    <ServiceSequence Name="normal_establishment">
      <ServiceTransaction>
        <InputPrimitive Event="INIT+" Interface="APPLICATION" Parameters="PARAMS"/>
        <OutputPrimitive Event="initialize" Interface="RESOURCE" Parameters="PARAMS"/>
        <OutputPrimitive Event="INITO+" Interface="APPLICATION"/>
      </ServiceTransaction>
    </ServiceSequence>
    <ServiceSequence Name="unsuccessful_establishment">
      <ServiceTransaction>
        <InputPrimitive Event="INIT+" Interface="APPLICATION" Parameters="PARAMS"/>
        <OutputPrimitive Event="initialize" Interface="RESOURCE" Parameters="PARAMS"/>
        <OutputPrimitive Event="INITO-" Interface="APPLICATION" Parameters="STATUS"/>
      </ServiceTransaction>
    </ServiceSequence>
    <ServiceSequence Name="request_confirm">
      <ServiceTransaction>
        <InputPrimitive Event="REQ+" Interface="APPLICATION" Parameters="SD"/>
        <OutputPrimitive Event="request" Interface="RESOURCE" Parameters="SD"/>
        <OutputPrimitive Event="CNF+" Interface="APPLICATION" Parameters="RD"/>
      </ServiceTransaction>
    </ServiceSequence>
    <ServiceSequence Name="request_inhibited">
      <ServiceTransaction>
        <InputPrimitive Event="REQ-" Interface="APPLICATION" Parameters="SD"/>
        <OutputPrimitive Event="CNF-" Interface="APPLICATION" Parameters="STATUS"/>
      </ServiceTransaction>
    </ServiceSequence>
    <ServiceSequence Name="request_error">
      <ServiceTransaction>
        <InputPrimitive Event="REQ+" Interface="APPLICATION" Parameters="SD"/>
        <OutputPrimitive Event="request" Interface="RESOURCE" Parameters="SD"/>
        <OutputPrimitive Event="CNF-" Interface="APPLICATION" Parameters="STATUS"/>
      </ServiceTransaction>
    </ServiceSequence>
    <ServiceSequence Name="indication_response">
      <ServiceTransaction>
        <InputPrimitive Event="indicate" Interface="RESOURCE" Parameters="RD"/>
        <OutputPrimitive Event="IND+" Interface="APPLICATION" Parameters="RD"/>
      </ServiceTransaction>
      <ServiceTransaction>
        <InputPrimitive Event="RSP" Interface="APPLICATION" Parameters="QI,SD"/>
        <OutputPrimitive Event="response" Interface="RESOURCE" Parameters="QI,SD"/>
      </ServiceTransaction>
    </ServiceSequence>
    <ServiceSequence Name="indication_inhibited">
      <ServiceTransaction>
        <InputPrimitive Event="indicate" Interface="RESOURCE" Parameters="RD,QI=FALSE"/>
        <OutputPrimitive Event="inhibited" Interface="RESOURCE"/>
      </ServiceTransaction>
    </ServiceSequence>
    <ServiceSequence Name="error_indication">
      <ServiceTransaction>
        <InputPrimitive Event="error" Interface="RESOURCE" Parameters="STATUS"/>
        <OutputPrimitive Event="IND-" Interface="APPLICATION" Parameters="STATUS"/>
      </ServiceTransaction>
    </ServiceSequence>
    <ServiceSequence Name="application_initiated_termination">
      <ServiceTransaction>
        <InputPrimitive Event="INIT-" Interface="APPLICATION"/>
        <OutputPrimitive Event="terminate" Interface="RESOURCE"/>
        <OutputPrimitive Event="INITO-" Interface="APPLICATION" Parameters="STATUS"/>
      </ServiceTransaction>
    </ServiceSequence>
    <ServiceSequence Name="resource_initiated_termination">
      <ServiceTransaction>
        <OutputPrimitive Event="INITO-" Interface="APPLICATION" Parameters="STATUS"/>
      </ServiceTransaction>
    </ServiceSequence>
  </Service>
</FBType>
