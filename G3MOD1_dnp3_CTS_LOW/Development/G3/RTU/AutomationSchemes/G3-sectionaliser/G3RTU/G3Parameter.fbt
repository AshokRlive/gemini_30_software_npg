<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE FBType SYSTEM "http://www.holobloc.com/xml/LibraryElement.dtd">
<FBType Comment="Get the value of a parameter" Name="G3Parameter">
  <Identification ApplicationDomain="G3" Description="TYPE: INT, REAL, BOOL" Standard="61499-2"/>
  <VersionInfo Author="wang_p" Date="2017-07-05" Organization="Lucy Electric" Version="1.0"/>
  <CompilerInfo/>
  <InterfaceList>
    <EventInputs>
      <Event Comment="Initialization request" Name="INIT" Type="Event">
        <With Var="QI"/>
        <With Var="NAME"/>
        <With Var="TYPE"/>
      </Event>
      <Event Comment="Normal execution request" Name="REQ" Type="Event">
        <With Var="QI"/>
        <With Var="ID"/>
      </Event>
    </EventInputs>
    <EventOutputs>
      <Event Comment="Initialization confirm" Name="INITO" Type="Event">
        <With Var="QO"/>
        <With Var="STATUS"/>
      </Event>
      <Event Comment="Excecution confirm" Name="CNF" Type="Event">
        <With Var="QO"/>
        <With Var="STATUS"/>
        <With Var="VALUE"/>
      </Event>
    </EventOutputs>
    <InputVars>
      <VarDeclaration Comment="Event Input Qualifier" Name="QI" Type="BOOL"/>
      <VarDeclaration Comment="Parameter name" Name="NAME" Type="WSTRING"/>
      <VarDeclaration Comment="[REAL, INT, BOOL]" InitialValue="INT" Name="TYPE" Type="STRING"/>
      <VarDeclaration Comment="Parameter identifier" Name="ID" Type="UINT"/>
    </InputVars>
    <OutputVars>
      <VarDeclaration Comment="Event Output Qualifier" Name="QO" Type="BOOL"/>
      <VarDeclaration Comment="Service Status" Name="STATUS" Type="WSTRING"/>
      <VarDeclaration Comment="Parameter value" Name="VALUE" Type="DINT"/>
    </OutputVars>
  </InterfaceList>
  <Service Comment="Get the value of a parameter" LeftInterface="APPLICATION" RightInterface="RESOURCE">
    <ServiceSequence Name="normal_establishment">
      <ServiceTransaction>
        <InputPrimitive Event="INIT+" Interface="APPLICATION" Parameters="PARAMS"/>
        <OutputPrimitive Event="initialize" Interface="RESOURCE" Parameters="PARAMS"/>
        <OutputPrimitive Event="INITO+" Interface="APPLICATION"/>
      </ServiceTransaction>
    </ServiceSequence>
    <ServiceSequence Name="unsuccessful_establishment">
      <ServiceTransaction>
        <InputPrimitive Event="INIT+" Interface="APPLICATION" Parameters="PARAMS"/>
        <OutputPrimitive Event="initialize" Interface="RESOURCE" Parameters="PARAMS"/>
        <OutputPrimitive Event="INITO-" Interface="APPLICATION" Parameters="STATUS"/>
      </ServiceTransaction>
    </ServiceSequence>
    <ServiceSequence Name="request_confirm">
      <ServiceTransaction>
        <InputPrimitive Event="REQ+" Interface="APPLICATION" Parameters="SD"/>
        <OutputPrimitive Event="request" Interface="RESOURCE" Parameters="SD"/>
        <OutputPrimitive Event="CNF+" Interface="APPLICATION" Parameters="RD"/>
      </ServiceTransaction>
    </ServiceSequence>
    <ServiceSequence Name="request_inhibited">
      <ServiceTransaction>
        <InputPrimitive Event="REQ-" Interface="APPLICATION" Parameters="SD"/>
        <OutputPrimitive Event="CNF-" Interface="APPLICATION" Parameters="STATUS"/>
      </ServiceTransaction>
    </ServiceSequence>
    <ServiceSequence Name="request_error">
      <ServiceTransaction>
        <InputPrimitive Event="REQ+" Interface="APPLICATION" Parameters="SD"/>
        <OutputPrimitive Event="request" Interface="RESOURCE" Parameters="SD"/>
        <OutputPrimitive Event="CNF-" Interface="APPLICATION" Parameters="STATUS"/>
      </ServiceTransaction>
    </ServiceSequence>
    <ServiceSequence Name="indication_response">
      <ServiceTransaction>
        <InputPrimitive Event="indicate" Interface="RESOURCE" Parameters="RD"/>
        <OutputPrimitive Event="IND+" Interface="APPLICATION" Parameters="RD"/>
      </ServiceTransaction>
      <ServiceTransaction>
        <InputPrimitive Event="RSP" Interface="APPLICATION" Parameters="QI,SD"/>
        <OutputPrimitive Event="response" Interface="RESOURCE" Parameters="QI,SD"/>
      </ServiceTransaction>
    </ServiceSequence>
    <ServiceSequence Name="indication_inhibited">
      <ServiceTransaction>
        <InputPrimitive Event="indicate" Interface="RESOURCE" Parameters="RD,QI=FALSE"/>
        <OutputPrimitive Event="inhibited" Interface="RESOURCE"/>
      </ServiceTransaction>
    </ServiceSequence>
    <ServiceSequence Name="error_indication">
      <ServiceTransaction>
        <InputPrimitive Event="error" Interface="RESOURCE" Parameters="STATUS"/>
        <OutputPrimitive Event="IND-" Interface="APPLICATION" Parameters="STATUS"/>
      </ServiceTransaction>
    </ServiceSequence>
    <ServiceSequence Name="application_initiated_termination">
      <ServiceTransaction>
        <InputPrimitive Event="INIT-" Interface="APPLICATION"/>
        <OutputPrimitive Event="terminate" Interface="RESOURCE"/>
        <OutputPrimitive Event="INITO-" Interface="APPLICATION" Parameters="STATUS"/>
      </ServiceTransaction>
    </ServiceSequence>
    <ServiceSequence Name="resource_initiated_termination">
      <ServiceTransaction>
        <OutputPrimitive Event="INITO-" Interface="APPLICATION" Parameters="STATUS"/>
      </ServiceTransaction>
    </ServiceSequence>
  </Service>
</FBType>
