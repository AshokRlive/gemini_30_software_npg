function remainder = MathFunctions_Remainder(dividend, divisor)
%#codegen
% 
%
%
coder.inline('always');

remainder = rem(dividend, divisor);
end