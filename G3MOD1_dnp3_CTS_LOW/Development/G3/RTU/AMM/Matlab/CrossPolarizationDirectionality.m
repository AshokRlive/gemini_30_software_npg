function TypeOfFault = CrossPolarizationDirectionality( TypeOfFault,...
                                                        VoltagePhaseAngle,...
                                                        CurrentPhaseAngle,...
                                                        DirectionalityConfig)
%#codegen
% Cross Polarization Directionality
% Calculating angle difference for cross polarizing method.
% Determine direction based on the angle difference between faulted current(s) angle(s)
% and respective polarization element.

coder.inline('always');

angleDifference = single(0);

% Faulted phase on phase A only
%if TypeOfFaultStr.LineA.UnknownDir == 1
if TypeOfFault.SinglePhaseOnA(1, Direction.UnknownDir) == 1
    angleDifference = CurrentPhaseAngle(WithReferenceTo.LineA)...
                       - VoltagePhaseAngle(WithReferenceTo.LineBC)...
                       - DirectionalityConfig.AngleOfConnection;                 

    % Normalize to 360 degrees
    angleDifference = mod(angleDifference, 360); 
                       
    TypeOfFault.SinglePhaseOnA = DetermineDirection( angleDifference,...
                                              TypeOfFault.SinglePhaseOnA,...
                                              DirectionalityConfig);       
end    
% Faulted phase LineB only   
if TypeOfFault.SinglePhaseOnB(1, Direction.UnknownDir)== 1
    angleDifference = CurrentPhaseAngle(WithReferenceTo.LineB)...
                       - VoltagePhaseAngle(WithReferenceTo.LineCA)...
                       - DirectionalityConfig.AngleOfConnection;

    % Normalize to 360 degrees
    angleDifference = mod(angleDifference, 360);
    
    TypeOfFault.SinglePhaseOnB = DetermineDirection( angleDifference,...
                                              TypeOfFault.SinglePhaseOnB,...
                                              DirectionalityConfig);                 
end                        
% Faulted phase LineC only   
if TypeOfFault.SinglePhaseOnC(1, Direction.UnknownDir) == 1
    angleDifference = CurrentPhaseAngle(WithReferenceTo.LineC)...
                      - VoltagePhaseAngle(WithReferenceTo.LineAB)...
                      - DirectionalityConfig.AngleOfConnection;
%                        - DirectionalityConfigStr.CharacteristicAnglePF...

    angleDifference = mod(angleDifference, 360);
    TypeOfFault.SinglePhaseOnC = DetermineDirection( angleDifference,...
                                              TypeOfFault.SinglePhaseOnC,...
                                              DirectionalityConfig);                        
end                        
% Phase-to-phase fault on LineA, LineB
if TypeOfFault.PhaseToPhaseOnAB(1, Direction.UnknownDir) == 1
    angleDifference =  CurrentPhaseAngle(WithReferenceTo.LineA)...
                        - VoltagePhaseAngle(WithReferenceTo.LineAB)...
                        + VoltagePhaseAngle(WithReferenceTo.LineBC)...
                        - 270;

    angleDifference = mod(angleDifference, 360);            
    TypeOfFault.PhaseToPhaseOnAB = DetermineDirection( angleDifference,...
                                              TypeOfFault.PhaseToPhaseOnAB,...
                                              DirectionalityConfig);   
end                                                    
% Phase-to-phase fault on LineB, LineC                   
if TypeOfFault.PhaseToPhaseOnBC(1, Direction.UnknownDir) == 1
    angleDifference =  CurrentPhaseAngle(WithReferenceTo.LineB)...
                        - VoltagePhaseAngle(WithReferenceTo.LineBC)...
                        + VoltagePhaseAngle(WithReferenceTo.LineCA)...
                        - 150;
                    
    angleDifference = mod(angleDifference, 360);
    TypeOfFault.PhaseToPhaseOnBC = DetermineDirection( angleDifference,...
                                              TypeOfFault.PhaseToPhaseOnBC,...
                                              DirectionalityConfig);                        
end                        
% Phase-to-phase fault on LineC, LineA                     
if TypeOfFault.PhaseToPhaseOnCA(1, Direction.UnknownDir) == 1
    angleDifference = CurrentPhaseAngle(WithReferenceTo.LineC)...
                        - VoltagePhaseAngle(WithReferenceTo.LineCA);
    
    angleDifference = mod(angleDifference, 360);
    TypeOfFault.PhaseToPhaseOnCA = DetermineDirection( angleDifference,...
                                              TypeOfFault.PhaseToPhaseOnCA,...
                                              DirectionalityConfig);                       
end
% Three phase fault on LineA, LineB, LineC                    
if TypeOfFault.ThreePhaseOnABC(1, Direction.UnknownDir) == 1
    angleDifference =  VoltagePhaseAngle(WithReferenceTo.LineBC)...
                       - CurrentPhaseAngle(WithReferenceTo.LineA)...
                       + VoltagePhaseAngle(WithReferenceTo.LineCA)...
                       - CurrentPhaseAngle(WithReferenceTo.LineB)...
                       + VoltagePhaseAngle(WithReferenceTo.LineAB)...
                       - CurrentPhaseAngle(WithReferenceTo.LineC)...
                       + DirectionalityConfig.AngleOfConnection;
                   
    angleDifference = mod(angleDifference, 360);  
    TypeOfFault.ThreePhaseOnABC = DetermineDirection( angleDifference,...
                                              TypeOfFault.ThreePhaseOnABC,...
                                              DirectionalityConfig);
end                                          
% TODO Phase-to-ground fault                   
if TypeOfFault.Earth(1, Direction.UnknownDir) == 1
    % Vector comparison method
    if DirectionalityConfig.EnableWattmetricMthd == 0
        angleDifference = CurrentPhaseAngle(WithReferenceTo.Neutral)...
                            + 270 ...
                            - VoltagePhaseAngle(WithReferenceTo.Neutral);
                        
        angleDifference = mod(angleDifference, 360);
        TypeOfFault.Earth = DetermineDirection( angleDifference,...
                                                    TypeOfFault.Earth,...
                                                    DirectionalityConfig); 
                                                    
    % Resonant Ground Network or Isolated Ground Network and Wattmetric Method
    elseif (DirectionalityConfig.EnableWattmetricMthd == 1) &&...
            ((DirectionalityConfig.ResonantGroundNetwork == 1) ||...
            (DirectionalityConfig.IsolatedGroundNetwork == 1))
    %TODO Code Phase-to-ground Wattmetric Method
    
    end
end        
end