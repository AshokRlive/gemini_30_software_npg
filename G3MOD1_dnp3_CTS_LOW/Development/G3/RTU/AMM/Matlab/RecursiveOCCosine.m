% Recursive Orthogonal Component Cosine 
% 
% 
% 

function yc = RecursiveOCCosine(rYcOld, ycCurrent, ycOld)
% Recursive Orthogonal Component Cosine
% param 
% 
% return

yc = rYcOld + (ycCurrent - ycOld);
%yc = yc/1.4142;
end