% Orthogonal Component based Magnitude Measurement 
% Function located here is for the evalution of Magnitude Measurement
% algorithms per "8.2.1 Measurement of Magnitude of Voltage or Current" from the book
% Digital Signal Processing in Power System Protection and Control - W. Rebizant et al. (Springer, 2011)

function magnitude = OCMagnitudeMeasurement2(ys, yc,... 
                                            ycMinusOne, ysMinusOne,...
                                            delayK, windowLength)

magnitude = 0;

magnitude = sqrt(((yc * ycMinusOne) + (ys * ysMinusOne))/...
    ( cos((delayK * 2 * pi)/windowLength)));
%filterGainSine * filterGainCos *
end