% Recursive Orthogonal Component Sine 
% 
% 
% 

function ys = RecursiveOCSine( rYsOld, ysCurrent, ysOld)
% Recursive Orthogonal Component Sine
% param 
% 
% return

ys = rYsOld + (ysCurrent - ysOld);
%ys = ys/1.4142;
end