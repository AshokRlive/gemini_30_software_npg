% Fault inception point monitor - Two-sided cumulative sum of the difference signal 
% S. R. Mohanty, A. K. Pradhan and A. Routray 
% �A Cumulative Sum-Based Fault Detector for Power System Relaying Application�
% IEEE TRANSACTIONS ON POWER DELIVERY, VOL. 23, NO. 1, JANUARY 2008

function [transientMonitorPos, transientMonitorNeg] = CUMSUM( currentWaveformDataPoint, driftParameter, transientMonitorPos, transientMonitorNeg)
% CUMSUM Fault inception point monitor
% param 
% 
% return

transientMonitorPos = transientMonitorPos + currentWaveformDataPoint - driftParameter;
transientMonitorNeg = transientMonitorNeg - currentWaveformDataPoint - driftParameter;

if transientMonitorPos < 0
    transientMonitorPos = 0;
end

if transientMonitorNeg < 0
    transientMonitorNeg = 0;
end
end