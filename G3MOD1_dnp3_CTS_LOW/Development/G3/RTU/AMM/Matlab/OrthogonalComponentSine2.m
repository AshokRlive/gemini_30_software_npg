% Orthogonal Component Sine 
% Function located here are for the evalution of frequency measurement
% algorithms per "8.2.1 Measurement of Magnitude of Voltage or Current" from the book
% Digital Signal Processing in Power System Protection and Control - W. Rebizant et al. (Springer, 2011)

function ys = OrthogonalComponentSine2(ys, waveformDataPoints, windowLength, pointsPerCycle)
% Orthogonal component Sine
% param 
% 
% return
coder.inline('always');
ys = single(0);

for index = 1:windowLength   
    ys = ys + (waveformDataPoints(windowLength+1-index) * sin(single(((1/(pointsPerCycle/2)) * pi * (((windowLength-1)/2) - index))))); 
end

ys = ys /(windowLength/1.41421);
end