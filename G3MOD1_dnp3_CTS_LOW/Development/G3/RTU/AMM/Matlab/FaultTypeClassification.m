

function [FpiCfgAndParams, TypeOfFault] = FaultTypeClassification(FpiCfgAndParams,...
                                                                            TypeOfFault,...
                                                                            phaseACurrent,...
                                                                            phaseBCurrent,...
                                                                            phaseCCurrent,...
                                                                            neutralCurrent,...
                                                                            minReliableCurrentAmplitudeLevel)
%#codegen 
% Identify the Type of Fault (Single phase, Two phase, Three phase, etc..).
% This info will be passed on to the Directionality function which requires
% this as each fault type is processed in particular way.
%

coder.inline('always');

% Variable that contains the value during the previous half cycle
persistent oldFaultEventDetected;

if isempty(oldFaultEventDetected)                                                                                                                                                                                                                                                      
    oldFaultEventDetected = zeros(1,uint32(WithReferenceTo.Neutral));
end
% Check for Single Phase Faults (A)
if (phaseACurrent > minReliableCurrentAmplitudeLevel)
    % Set the fault type
    TypeOfFault.SinglePhaseOnA(1, Direction.UnknownDir) = 1;
end
% Check for Single Phase Faults (B)
if (phaseBCurrent > minReliableCurrentAmplitudeLevel)
    % Set the fault type
    TypeOfFault.SinglePhaseOnB(1, Direction.UnknownDir) = 1;
end
% Check for Single Phase Faults (C)
if (phaseCCurrent > minReliableCurrentAmplitudeLevel)
    % Set the fault type
    TypeOfFault.SinglePhaseOnC(1, Direction.UnknownDir) = 1;
end
% Check for Earth Fault
if (neutralCurrent > minReliableCurrentAmplitudeLevel)
    % Set the fault type
    TypeOfFault.Earth(1, Direction.UnknownDir) = 1;
end

% Check for Two Phase Faults (AB)
if (TypeOfFault.SinglePhaseOnA(1, Direction.UnknownDir) == 1) &&...
      (TypeOfFault.SinglePhaseOnB(1, Direction.UnknownDir) == 1) &&...
      (TypeOfFault.SinglePhaseOnC(1, Direction.UnknownDir) == 0)
  
  TypeOfFault.PhaseToPhaseOnAB(1, Direction.UnknownDir) = 1;
  % Clear other two phases
  TypeOfFault.SinglePhaseOnA(1, Direction.UnknownDir) = 0;
  TypeOfFault.SinglePhaseOnB(1, Direction.UnknownDir) = 0;
end

% Check for Two Phase Faults (BC)
if (TypeOfFault.SinglePhaseOnA(1, Direction.UnknownDir) == 0) &&...
      (TypeOfFault.SinglePhaseOnB(1, Direction.UnknownDir) == 1) &&...
      (TypeOfFault.SinglePhaseOnC(1, Direction.UnknownDir) == 1)  
  
  TypeOfFault.PhaseToPhaseOnBC(1, Direction.UnknownDir) = 1;
  % Clear other two phases
  TypeOfFault.SinglePhaseOnB(1, Direction.UnknownDir) = 0;
  TypeOfFault.SinglePhaseOnC(1, Direction.UnknownDir) = 0;
end

% Check for Two Phase Faults (CA)
if (TypeOfFault.SinglePhaseOnA(1, Direction.UnknownDir) == 1) &&...
      (TypeOfFault.SinglePhaseOnB(1, Direction.UnknownDir) == 0) &&...
      (TypeOfFault.SinglePhaseOnC(1, Direction.UnknownDir) == 1)  
  
  TypeOfFault.PhaseToPhaseOnCA(1, Direction.UnknownDir) = 1;
  % Clear other two phases
  TypeOfFault.SinglePhaseOnC(1, Direction.UnknownDir) = 0;
  TypeOfFault.SinglePhaseOnA(1, Direction.UnknownDir) = 0;
end

% Check for Three Phase faults
% Three phase faults are particular in that current presence alone, on the three phases
% can not be used. Thus the "faultEventDetected" and "alarm" variables 
% that are set and cleared by the FPI (/FaultPassageIndication) function are used.
if (( oldFaultEventDetected(1,WithReferenceTo.LineA) ~=...
      FpiCfgAndParams(WithReferenceTo.LineA).faultParams.faultEventDetected) ||...
      (FpiCfgAndParams(WithReferenceTo.LineA).faultParams.alarm == 1)) &&...
      ((oldFaultEventDetected(1,WithReferenceTo.LineB) ~=...
      FpiCfgAndParams(WithReferenceTo.LineB).faultParams.faultEventDetected) ||...
      (FpiCfgAndParams(WithReferenceTo.LineB).faultParams.alarm == 1)) &&...
      ((oldFaultEventDetected(1,WithReferenceTo.LineC) ~=...
      FpiCfgAndParams(WithReferenceTo.LineC).faultParams.faultEventDetected) ||...
      (FpiCfgAndParams(WithReferenceTo.LineC).faultParams.alarm == 1))
  
  TypeOfFault.ThreePhaseOnABC(1, Direction.UnknownDir) = 1;
  % Clear other phases
  TypeOfFault.SinglePhaseOnA(1, Direction.UnknownDir) = 0;
  TypeOfFault.SinglePhaseOnB(1, Direction.UnknownDir) = 0;
  TypeOfFault.SinglePhaseOnC(1, Direction.UnknownDir) = 0;
end

% Assign the new fault event detected value to the old one
for idxPhase = 1:1:(uint32(WithReferenceTo.Neutral))

    oldFaultEventDetected(1,idxPhase) =...
        FpiCfgAndParams(idxPhase).faultParams.faultEventDetected;
end

% For phase faults:
% Single phase faults: Only one phase crosses threhold. 
% The other two phases synchronize and are 180 degree out of phases with the faulted one.

% Two phase faults: Both faulted phases cross the threhold. Third one goes to zero.

% Three phase fault: All three phases cross threhold.

% For Earth faults:
% Balance earth fault: Neutral current crosses threshold. All three phases carry a current of varying magnitude.

% Single phase to ground fault: Neutral current crosses threshold. One phase carries all the neutral current. The other two phases drop to zero.

end