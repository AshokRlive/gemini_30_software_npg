function AMMConfigStr = InitAMM2(AMMConfigStr)
%#codegen
% 
% 
% 
%

coder.inline('always');
% % TODO Code Generation
% % AMMConfigStr
 
% 
% 
% AMMConfigStr = struct(  'HardwareModel', zeros(1,1),...
%                         'NominalFrequency', zeros(1,1),...
%                         'nominalSamplingRate', zeros(1,1),...
%                         'SamplingTime', zeros(1,1),...
%                         'PointsPerCycle', zeros(1,1),...
%                         'minReliableCurrentAmplitudeLevel', zeros(1,1),...
%                         'minReliableVoltageAmplitudeLevel', zeros(1,1));
% 
% % TODO Code Generation
% coder.cstructname(AMMConfigStr, 'AMMConfigStr');
% HardwareModel = coder.opaque('lu_uint32_t', '0', 'HeaderFile', '"lu_types.h"');                        
% NominalFrequency = coder.opaque('lu_uint32_t', '0', 'HeaderFile', '"lu_types.h"');                       
% nominalSamplingRate = coder.opaque('lu_float32_t', '0.0', 'HeaderFile', '"lu_types.h"');                        
% SamplingTime = coder.opaque('lu_uint32_t', '0', 'HeaderFile', '"lu_types.h"'); 
% PointsPerCycle = coder.opaque('lu_uint32_t', '0', 'HeaderFile', '"lu_types.h"');
% minReliableCurrentAmplitudeLevel = coder.opaque('lu_float32_t', '0.0', 'HeaderFile', '"lu_types.h"'); 
% minReliableVoltageAmplitudeLevel = coder.opaque('lu_float32_t', '0.0', 'HeaderFile', '"lu_types.h"');

% AMMConfigStr
% Number of Voltage and Current
AMMConfigStr.HardwareModel = uint32(1); % GX switch - Six Voltage and Four Currents                   
                                    
% Nominal frequency (mains frequency 50 or 60Hz)
AMMConfigStr.NominalFrequency = single(50);

% Selected ADCs' Sampling Rate (Hz)
AMMConfigStr.NominalSamplingRate = single(1000);

% Selected ADCs' Sampling Time 
AMMConfigStr.SamplingTime = single(1 / AMMConfigStr.NominalSamplingRate);

% Number of samples per cycle
AMMConfigStr.PointsPerCycle = single(AMMConfigStr.NominalSamplingRate / AMMConfigStr.NominalFrequency);                                    

% Minimum operating current to allow directional criteria
AMMConfigStr.MinReliableCurrentAmplitudeLevel = single(0.1);

%Minimum operating voltage to allow directional criteria
AMMConfigStr.MinReliableVoltageAmplitudeLevel = single(0.1);



end