%

classdef LineToEarth < uint32
   enumeration
      La   (1)           % Line to Earth Phase 1
      Lb   (2)           % Line to Earth Phase 2
      Lc   (3)           % Line to Earth Phase 3
      Ne   (4)           % Neutral
      LAST (5)
   end
end