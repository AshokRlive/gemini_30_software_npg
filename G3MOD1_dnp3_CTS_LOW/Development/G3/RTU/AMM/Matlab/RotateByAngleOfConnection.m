% Rotate by Angle of Connection
%
%

function DirectionalityParamsStr = RotateByAngleOfConnection(   DirectionalityParamsStr,...
                                                                DirectionalityConfigStr,...
                                                                PhasorStr,...
                                                                AMMConfigStr)
% Neutral voltage displacemnt
DirectionalityParamsStr.PhaseAngleStr.PhiVne = PhasorStr.PhiVne + DirectionalityConfigStr.AngleOfConnection;
if (DirectionalityParamsStr.PhaseAngleStr.PhiVne < 0) &&...
        (DirectionalityParamsStr.PhaseAngleStr.PhiVne > AMMConfigStr.ZeroNegThreshold)
    DirectionalityParamsStr.PhaseAngleStr.PhiVne = 0;
end 
DirectionalityParamsStr.PhaseAngleStr.PhiVne = mod(DirectionalityParamsStr.PhaseAngleStr.PhiVne, 360);
 
% Phase to Phase voltages
% If the voltage magnitude has fallen below a safe phase angle measurement value
% we need to use the phase angle value from memory
if PhasorStr.UseMemPhiVL1_2 == 1
    %TODO Determine and code voltage phase angle from memory method
    %PhaseAngleMemStr.UseMemPhiVL1_2 = 1;
    
else
    DirectionalityParamsStr.PhaseAngleStr.PhiVL1_2 = PhasorStr.PhiVL1_2 + DirectionalityConfigStr.AngleOfConnection;
    if (DirectionalityParamsStr.PhaseAngleStr.PhiVL1_2 < 0) &&...
            (DirectionalityParamsStr.PhaseAngleStr.PhiVL1_2 > AMMConfigStr.ZeroNegThreshold)
        DirectionalityParamsStr.PhaseAngleStr.PhiVL1_2 = 0;
    end                                                 
    DirectionalityParamsStr.PhaseAngleStr.PhiVL1_2 = mod(DirectionalityParamsStr.PhaseAngleStr.PhiVL1_2, 360); 
end

if PhasorStr.UseMemPhiVL2_3 == 1
    %TODO Determine and code voltage phase angle from memory method
    %PhaseAngleMemStr.UseMemPhiVL2_3 = 1;
    
else
    DirectionalityParamsStr.PhaseAngleStr.PhiVL2_3 = PhasorStr.PhiVL2_3 + DirectionalityConfigStr.AngleOfConnection;
    if (DirectionalityParamsStr.PhaseAngleStr.PhiVL2_3 < 0) &&...
            (DirectionalityParamsStr.PhaseAngleStr.PhiVL2_3 > AMMConfigStr.ZeroNegThreshold)
        DirectionalityParamsStr.PhaseAngleStr.PhiVL2_3 = 0;
    end  
    DirectionalityParamsStr.PhaseAngleStr.PhiVL2_3 = mod(DirectionalityParamsStr.PhaseAngleStr.PhiVL2_3, 360);
end

if PhasorStr.UseMemPhiVL3_1 == 1
    %TODO Determine and code voltage phase angle from memory method
    %PhaseAngleMemStr.UseMemPhiVL3_1 = 1;
    
else
    DirectionalityParamsStr.PhaseAngleStr.PhiVL3_1 = PhasorStr.PhiVL3_1 + DirectionalityConfigStr.AngleOfConnection;
    if (DirectionalityParamsStr.PhaseAngleStr.PhiVL3_1 < 0) &&...
            (DirectionalityParamsStr.PhaseAngleStr.PhiVL3_1 > AMMConfigStr.ZeroNegThreshold)
         DirectionalityParamsStr.PhaseAngleStr.PhiVL3_1 = 0;
    end                                                   
    DirectionalityParamsStr.PhaseAngleStr.PhiVL3_1 = mod(DirectionalityParamsStr.PhaseAngleStr.PhiVL3_1, 360);
end

% The Phase and Neutral currents are NOT rotated 
DirectionalityParamsStr.PhaseAngleStr.PhiIL1 = PhasorStr.PhiIL1;
DirectionalityParamsStr.PhaseAngleStr.PhiIL2 = PhasorStr.PhiIL2;
DirectionalityParamsStr.PhaseAngleStr.PhiIL3 = PhasorStr.PhiIL3;
DirectionalityParamsStr.PhaseAngleStr.PhiIn = PhasorStr.PhiIn;

end