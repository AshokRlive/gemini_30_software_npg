function argumentInDegrees = MathFunctions_SinDegrees(argumentInDegrees)
%#codegen
% 
%
%

coder.inline('always');
if coder.target('MATLAB')
    % Executing in MATLAB, call function below
    argumentInDegrees = sind(argumentInDegrees);

else
    % For code generation use sin function, which has been code generated
    % from MathFuctions reference model
    coder.ceval('MathFunctions_SinDegrees', coder.ref(argumentInDegrees));
end