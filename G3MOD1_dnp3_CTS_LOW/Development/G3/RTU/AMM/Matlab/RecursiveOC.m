% Recursive Orthogonal Components 
% 
% 
% 

function [yc, ys]= RecursiveOC(waveformDataPoints, ycOld, ysOld, index, windowLength, pointsPerCycle)
% Recursive Orthogonal Components
% param 
% 
% return

%for index = 1:windowLength
    yc = ycOld * cos(2*pi/pointsPerCycle) + ysOld * sin(2*pi/pointsPerCycle) +...
        (waveformDataPoints(index) - (waveformDataPoints(windowLength + index)) *...
        (-cos(0.5*(windowLength+1)*2*pi/pointsPerCycle)));
                       
    ys = ycOld * (-sin(2*pi/pointsPerCycle)) + ysOld * cos(2*pi/pointsPerCycle) +...
        (waveformDataPoints(index) - (waveformDataPoints(windowLength + index)) *...
        (cos(0.5*(windowLength+1)*2*pi/pointsPerCycle)));
    
%    ysOld = ys;                       
                                        
%    ycOld = yc;  
%end 
yc = yc/(2);
ys = ys/(2);
end