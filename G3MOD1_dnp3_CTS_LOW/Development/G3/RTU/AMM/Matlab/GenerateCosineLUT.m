% Generate Cosine Look Up Table
% for Cosine Algorithm per "8.2.1 Measurement of Magnitude of Voltage or Current" from the book
% Digital Signal Processing in Power System Protection and Control - W. Rebizant et al. (Springer, 2011)

function cosineLUT = GenerateCosineLUT( windowLength, pointsPerCycle)
% Generate Cosine Look Up Table
% param 
% 
% return

cosineLUT = 0;

for index = 1:windowLength
   cosineLUT(index) = cos((1/(pointsPerCycle/2)) * pi * (((windowLength-1)/2) - index));
end

end