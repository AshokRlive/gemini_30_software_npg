%

classdef OC < uint32
   enumeration
      Sine      (1)           % Orthogonal Component Sine
      Cosine    (2)           % Orthogonal Component Cosine
      LAST      (3)
   end
end