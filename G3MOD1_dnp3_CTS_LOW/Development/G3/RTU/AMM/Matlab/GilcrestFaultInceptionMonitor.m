% Fault inception point monitor 
% G. B. Gilcrest, G. D. Rockefeller, and E. A. Udren, �High speed distance
% relaying using a digital computer�Part I: System description,� IEEE
% Trans. Power Apparat. Syst., vol. PAS-91, pp. 1235�1243, 1972

function transientMonitor = GilcrestFaultInceptionMonitor( waveformDataPoints, PointsPerCycle)


%transientMonitor = 0;
firstDerivative = 0;
secondDerivative = 0;

firstDerivative = diff(waveformDataPoints,1);
secondDerivative = diff(waveformDataPoints,2);
omega = 2*pi/PointsPerCycle;

for index = 1:PointsPerCycle-2
    transientMonitor(index) = sqrt( ((secondDerivative(index)/(omega.^2)).^2) +...
                                ((firstDerivative(index)/(2*omega)).^2) );
end
end