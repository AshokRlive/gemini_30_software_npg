% Recursive Orthogonal Component Sine 
% 
% 
% 

function ys = RecursiveOCSine2(waveformDataPoints, ysOld, index, windowLength, numOfHarmonics)
% Recursive Orthogonal Component Sine
% param 
% 
% return

ys = ysOld + (waveformDataPoints(windowLength + index) - waveformDataPoints(index)) *...
                    (sin(2 * pi * index / windowLength))/numOfHarmonics;
%ys = ys/windowLength; 
end