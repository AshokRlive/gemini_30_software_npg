% Mock a phase or earth fault
%
%

function  FaultedPhasesStr = MockAFault(FaultedPhasesStr, totalMainsCycleNumber)
% 
%
indexMainsCycles = 0;


% [x,x,x,x] == [Unknown, Forward, Reverse, Both]

% Simulate Overcurrent Fault on phases A OR B OR C
% for indexMainsCycles = 1:1:totalMainsCycleNumber
%     FaultedPhasesStr.La(indexMainsCycles, :) = [1,0,0,0];
%     FaultedPhasesStr.Lb(indexMainsCycles, :) = [1,0,0,0];
%     FaultedPhasesStr.Lc(indexMainsCycles, :) = [1,0,0,0];
%     FaultedPhasesStr.Ne(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Lab(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Lbc(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Lca(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Labc(indexMainsCycles, :) = [0,0,0,0];
% end

% Simulate Overcurrent Fault on phases A AND B
for indexMainsCycles = 1:1:totalMainsCycleNumber
    FaultedPhasesStr.La(indexMainsCycles, :) = [1,0,0,0];
    FaultedPhasesStr.Lb(indexMainsCycles, :) = [1,0,0,0];
    FaultedPhasesStr.Lc(indexMainsCycles, :) = [1,0,0,0];
    FaultedPhasesStr.Ne(indexMainsCycles, :) = [0,0,0,0];
    FaultedPhasesStr.Lab(indexMainsCycles, :) = [0,0,0,0];
    FaultedPhasesStr.Lbc(indexMainsCycles, :) = [0,0,0,0];
    FaultedPhasesStr.Lca(indexMainsCycles, :) = [0,0,0,0];
    FaultedPhasesStr.Labc(indexMainsCycles, :) = [0,0,0,0];

    % Fault occurs at cycle number below
    if indexMainsCycles >= 4
        FaultedPhasesStr.La(indexMainsCycles, :) = [0,0,0,0];
        FaultedPhasesStr.Lb(indexMainsCycles, :) = [0,0,0,0];
        FaultedPhasesStr.Lc(indexMainsCycles, :) = [0,0,0,0];
        FaultedPhasesStr.Ne(indexMainsCycles, :) = [0,0,0,0];
        FaultedPhasesStr.Lab(indexMainsCycles, :) = [1,0,0,0];
        FaultedPhasesStr.Lbc(indexMainsCycles, :) = [0,0,0,0];
        FaultedPhasesStr.Lca(indexMainsCycles, :) = [0,0,0,0];
        FaultedPhasesStr.Labc(indexMainsCycles, :) = [0,0,0,0];    
    end
end

% Simulate Overcurrent Fault on phases B AND C
% for indexMainsCycles = 1:1:totalMainsCycleNumber
%     FaultedPhasesStr.La(indexMainsCycles, :) = [1,0,0,0];
%     FaultedPhasesStr.Lb(indexMainsCycles, :) = [1,0,0,0];
%     FaultedPhasesStr.Lc(indexMainsCycles, :) = [1,0,0,0];
%     FaultedPhasesStr.Ne(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Lab(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Lbc(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Lca(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Labc(indexMainsCycles, :) = [0,0,0,0];
% 
%     % Fault occurs at cycle number below
%     if indexMainsCycles >= 4
%         FaultedPhasesStr.La(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Lb(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Lc(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Ne(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Lab(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Lbc(indexMainsCycles, :) = [1,0,0,0];
%         FaultedPhasesStr.Lca(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Labc(indexMainsCycles, :) = [0,0,0,0];    
%     end
% end

% Simulate Overcurrent Fault on phases C AND A
% for indexMainsCycles = 1:1:totalMainsCycleNumber
%     FaultedPhasesStr.La(indexMainsCycles, :) = [1,0,0,0];
%     FaultedPhasesStr.Lb(indexMainsCycles, :) = [1,0,0,0];
%     FaultedPhasesStr.Lc(indexMainsCycles, :) = [1,0,0,0];
%     FaultedPhasesStr.Ne(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Lab(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Lbc(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Lca(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Labc(indexMainsCycles, :) = [0,0,0,0];
% 
%     % Fault occurs at cycle number below
%     if indexMainsCycles >= 4
%         FaultedPhasesStr.La(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Lb(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Lc(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Ne(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Lab(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Lbc(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Lca(indexMainsCycles, :) = [1,0,0,0];
%         FaultedPhasesStr.Labc(indexMainsCycles, :) = [0,0,0,0];    
%     end
% end

% Simulate Overcurrent Fault on all three phases (A,B, AND C)
% for indexMainsCycles = 1:1:totalMainsCycleNumber
%     FaultedPhasesStr.La(indexMainsCycles, :) = [1,0,0,0];
%     FaultedPhasesStr.Lb(indexMainsCycles, :) = [1,0,0,0];
%     FaultedPhasesStr.Lc(indexMainsCycles, :) = [1,0,0,0];
%     FaultedPhasesStr.Ne(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Lab(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Lbc(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Lca(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Labc(indexMainsCycles, :) = [0,0,0,0];
% 
%     % Fault occurs at cycle number below
%     if indexMainsCycles >= 4
%         FaultedPhasesStr.La(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Lb(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Lc(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Ne(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Lab(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Lbc(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Lca(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Labc(indexMainsCycles, :) = [1,0,0,0];    
%     end
% end

% Simulate Earth Fault on phases A, B AND C
% ie. 3I0 type fault
% for indexMainsCycles = 1:1:totalMainsCycleNumber
%     FaultedPhasesStr.La(indexMainsCycles, :) = [1,0,0,0];
%     FaultedPhasesStr.Lb(indexMainsCycles, :) = [1,0,0,0];
%     FaultedPhasesStr.Lc(indexMainsCycles, :) = [1,0,0,0];
%     FaultedPhasesStr.Ne(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Lab(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Lbc(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Lca(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Labc(indexMainsCycles, :) = [0,0,0,0];
% 
%     % Fault occurs at cycle number below
%     if indexMainsCycles >= 4
%         FaultedPhasesStr.La(indexMainsCycles, :) = [1,0,0,0];
%         FaultedPhasesStr.Lb(indexMainsCycles, :) = [1,0,0,0];
%         FaultedPhasesStr.Lc(indexMainsCycles, :) = [1,0,0,0];
%         FaultedPhasesStr.Ne(indexMainsCycles, :) = [1,0,0,0];
%         FaultedPhasesStr.Lab(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Lbc(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Lca(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Labc(indexMainsCycles, :) = [0,0,0,0];    
%     end
% end

% Simulate Earth Fault on phase A
% ie. Phase A to Ground
% for indexMainsCycles = 1:1:totalMainsCycleNumber
%     FaultedPhasesStr.La(indexMainsCycles, :) = [1,0,0,0];
%     FaultedPhasesStr.Lb(indexMainsCycles, :) = [1,0,0,0];
%     FaultedPhasesStr.Lc(indexMainsCycles, :) = [1,0,0,0];
%     FaultedPhasesStr.Ne(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Lab(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Lbc(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Lca(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Labc(indexMainsCycles, :) = [0,0,0,0];
% 
%     % Fault occurs at cycle number below
%     if indexMainsCycles >= 4
%         FaultedPhasesStr.La(indexMainsCycles, :) = [1,0,0,0];
%         FaultedPhasesStr.Lb(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Lc(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Ne(indexMainsCycles, :) = [1,0,0,0];
%         FaultedPhasesStr.Lab(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Lbc(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Lca(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Labc(indexMainsCycles, :) = [0,0,0,0];    
%     end
% end

% Simulate Earth Fault on phase B 
% ie. Phase B to Ground
% for indexMainsCycles = 1:1:totalMainsCycleNumber
%     FaultedPhasesStr.La(indexMainsCycles, :) = [1,0,0,0];
%     FaultedPhasesStr.Lb(indexMainsCycles, :) = [1,0,0,0];
%     FaultedPhasesStr.Lc(indexMainsCycles, :) = [1,0,0,0];
%     FaultedPhasesStr.Ne(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Lab(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Lbc(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Lca(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Labc(indexMainsCycles, :) = [0,0,0,0];
% 
%     % Fault occurs at cycle number below
%     if indexMainsCycles >= 4
%         FaultedPhasesStr.La(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Lb(indexMainsCycles, :) = [1,0,0,0];
%         FaultedPhasesStr.Lc(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Ne(indexMainsCycles, :) = [1,0,0,0];
%         FaultedPhasesStr.Lab(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Lbc(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Lca(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Labc(indexMainsCycles, :) = [0,0,0,0];    
%     end
% end

% Simulate Earth Fault on phase C
% ie. Phase C to Ground
% for indexMainsCycles = 1:1:totalMainsCycleNumber
%     FaultedPhasesStr.La(indexMainsCycles, :) = [1,0,0,0];
%     FaultedPhasesStr.Lb(indexMainsCycles, :) = [1,0,0,0];
%     FaultedPhasesStr.Lc(indexMainsCycles, :) = [1,0,0,0];
%     FaultedPhasesStr.Ne(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Lab(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Lbc(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Lca(indexMainsCycles, :) = [0,0,0,0];
%     FaultedPhasesStr.Labc(indexMainsCycles, :) = [0,0,0,0];
% 
%     % Fault occurs at cycle number below
%     if indexMainsCycles >= 4
%         FaultedPhasesStr.La(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Lb(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Lc(indexMainsCycles, :) = [1,0,0,0];
%         FaultedPhasesStr.Ne(indexMainsCycles, :) = [1,0,0,0];
%         FaultedPhasesStr.Lab(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Lbc(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Lca(indexMainsCycles, :) = [0,0,0,0];
%         FaultedPhasesStr.Labc(indexMainsCycles, :) = [0,0,0,0];    
%     end
% end

end