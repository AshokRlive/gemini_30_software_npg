% Least Square Error based method
% A NEW ALGORITHM FOR DIGITAL IMPEDANCE RELAYS
% M.S. Sachdev, M.A. Baribeau,
% IEEE Transactions on Power Apparatus and Systems, Vol. PAS-98, No.6 Nov./Dec. 1979

function residualSumSquared = LeastSquareError( waveformDataPoints, PointsPerCycle, projectionMatrix)

residualSumSquared = 0;
tmp = 0;

% A residual is defined as the difference between the actual value of the 
% dependent variable (raw waveform data) and the value predicted by the model.
% Matrix M represents the projection matrix 
for indexData = 1:PointsPerCycle
    tmp = projectionMatrix * waveformDataPoints(indexData);
    residualSumSquared = residualSumSquared + tmp(indexData, indexData);
end

residualSumSquared = abs(residualSumSquared);
end