% Calculate Orthogonal Sine and Cosine Components
% based on the recursive algorithm
%

function [cosineCmpnt,sineCmpnt] = CalculateOC2( waveformData, windowLength, cosineLUT ,sineLUT)
% Calculate the Recursive Orthogonal Sine and Cosine Components
% param 
% 
% return

    cosineCmpnt = OrthogonalComponentCosine3(waveformData, windowLength, cosineLUT);
                                    
    sineCmpnt = OrthogonalComponentSine3(waveformData, windowLength, sineLUT);


end