%

classdef Direction < uint32
   enumeration
      UnknownDir (1)
      Forward    (2)
      Reverse    (3)
      BothDir    (4)
   end
end